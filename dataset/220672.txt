Hazel Technologies, working with Cornell University, has completed research into extending the shelf life of pears.
The research involved a simulated retail environment. D’anjou pears lasted on shelves three times longer than a control group of the fruit, according to a news release.
Hazel Technologies CEO Adam Preslar said Hazel Pear, formulated to prolong pear life, has caught the attention of “some of the largest pear brands in the industry,” according to a news release.
The pear formulation joins a similar product designed for okra shipments.
Chief operating officer at Hazel Technologies, Adam Preslar, said in a news release that some of the industry’s fresh pear shippers are interested in the product.
“Growers and packers want to see third-party data on new products from trusted academic institutions,” Preslar said in the release, referring to Cornell’s participation.