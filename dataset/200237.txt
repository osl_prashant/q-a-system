The "steam-up" ration for pre-fresh cows has essentially become a thing of the past. In its place is a more measured approach to dry-cow nutrition to meet the metabolic and immunological needs of the transition cow, according to Cornell University professor Tom Overton."Through intensive research, we've learned and implemented a lot about transition care in the past 10 to 15 years," Overton told an audience at the 2016 American Association of Bovine Practitioners Annual Conference. "One of the most important findings is that cows don't need, and really can't handle, an abundance of protein and energy in the dry period."
Overton said cows metabolically do not process excess nitrogen well at the time of calving. He advised focusing pre-fresh protein supplementation on rumen-undegradable protein sources, with additional amino acid supplementation. He shared guidelines promoted by nutritionist Patrick French of 1,300 g/day of metabolizable protein, supplemental methionine at 30g/day, and supplemental lysine at 90 g/day for pre-fresh Holsteins.
He also suggested keeping energy low in the far-off dry-cow ration (0.59 to 0.63 Mcal/kg of net energy of lactation [NEL]; 110 to 120% of energy requirements; and <13% starch). In a one-group, close-up ration, he advised:
Low to moderate energy (0.64 to 0.66 Mcal/lb.; 1.40 to 1.45 Mcal/kg of NEL; 110 to 130% of energy requirements; and 16 to 18% starch)
Supplemental rumen-undegradable protein (metabolizable protein of 1,200 to 1,400 g/day for Holsteins)
Ensuring inclusion of macrominerals (K, Mg, Na, S, Cl and possibly Ca), Vitamins D and E, and trace elements
Calcium status is critical, said Overton -- even in herds with low incidence of clinical milk fever. He said herds with low blood calcium (<8.4-8.6 mg/dL) in the first week post-calving would be classified as having subclinical hypocalcemia, and have been shown to have:
Decreased neutrophil function (signaling lower immunity)
Increased blood non-esterified fatty acids and beta-hydroxybutyrate concentrations (likely as a result of lower dry-matter intake)
Higher incidence of metritis and endometritis
Higher rates of displaced abomasum
Lower milk production compared to herdmates
Lower subsequent reproductive performance
Overton advised a strategy of lowering dietary cation-anion difference (DCAD) in the close-up ration to promote calcium availability at calving. This can be achieved by using low-potassium forages and anionic supplements. He said supplemental magnesium (about 0.45% dietary target) also is required, and supplemental calcium may be necessary, depending on the level of DCAD. Urine pH monitoring is becoming an important, routine practice to monitor DCAD strategies. Overton recommended the following targets for effective DCAD management:
Dietary prepartum DCAD level: -10 to -15 mEg/100g of DM
Prepartum dietary calcium: 1.4 to 1.5% of DM
Target urine pH: 5.5 to 6.0
Overton added there also are a number of non-nutritional factors that contribute to transition success, including stocking density, grouping strategies, pen moves, segregating older cows from first-calf heifers, and heat abatement.