Jeff Burroughs has been hired as vice president of business development for Wenatchee, Wash.-based Keyes Packaging Group.
Burroughs will be based in Monterey, Calif., according to a news release.
He will be responsible for developing new business with a focus on the national avocado, wine and egg industries, the release said.
His previous work experience includes director of sales for Tosca/Georgia-Pacific LLC’s produce packaging sales staff and sales manager for CHEP, according to his LinkedIn account and news release.
“He will bring new opportunities to expand our business as we look to the future,” Ted Kozikowski, CEO, said in the release.