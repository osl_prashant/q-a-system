Among the “biggies” of pathogens that cause scours in both pre- and post-weaned calves is coccidia, a parasitic protozoa that sets up shop in the intestinal tract of cattle. Eggs are produced internally and pass into the environment via shedding in manure.
Shedding of coccidia “eggs” or oocysts by infected calves and heifers usually peaks about 3 weeks after initial exposure. In one study the peak numbers of oocysts shed per day by untreated infected calves was 50,000,000 on day 21! Older, immune animals continue to contaminate their environment at a much lower -- but consistent -- rate. These facts tell us that from the moment a calf is born, she is very likely to get some of these oocysts in her mouth.
Don’t fall behind – reduce exposure of newborn and older calves
Once a newborn calf stands up, she is in a perfect situation to begin getting coccidia eggs in her mouth. Licking the dam’s hair coat, searching for a teat to suck, and licking on anything in her environment, unfortunately, all are generous sources of coccidia eggs. Moving her to a cleaner space does work to cut exposure.
Among older calves, we should think about ways to reduce their shedding rates (that is, the rate that she passes coccidia eggs in her feces):

Create clean, well-bedded resting space for calves.
Optimize ventilation in the barn and calf or heifer pens.
Provide adequate feed space per animal.
Minimize weight and age variation between animals in the group.
Avoid feeding on the ground unless it is at a bunk.
Provide 12" of linear water space per 10 animals.
Treat infected animals.
Maximize time between successive occupants of the same pen.

Managing infections – building immunity
On nearly all dairies, all animals will eventually be exposed to coccidia. Through natural exposure, they will build immunity that suppresses infection. If the exposure of young calves can be managed to maintain a low level of infection, they can build immunity without excessive damage to their gut and loss of normal growth.
So, what can we do if natural exposure rates are uncontrolled (and likely to be high)? Use one of the feed additives that act to control coccidia activity in the calf after exposure. The four additives approved for use in the United States include:

Deccox®-M [decoquinate] – available in milk replacer, or powder, mixed with milk to make suspension
Bovatec® [lasalocid] – available in milk replacer, liquid additive to mix with milk
Rumensin® [monensin] – added to dry feeds like calf starter grain
Corid® [amprolium] – liquid can be added to milk or milk replacer, or dry crumble

When used as prescribed, all of these additives limit the population of coccidia in the gut. Their effectiveness is shown in studies in which the shedding rates have been reduced about 96 to 98 percent.
Preclinical use of the additives is recommended. Damage in the gut due to uncontrolled growth of coccidia will occur as early as 5 days after coccidia exposure. Thus, don’t wait until clinical symptoms are present to begin using the additive that you and your veterinarian believe is best for your situation.
For additional advice from Dr. Leadley on reducing the severity of clinical coccidiosis, visit his Calving Ease newsletter.