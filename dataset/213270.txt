Members of the Wisconsin Potato and Vegetable Growers Association have donated a truckload of potatoes and onions to the Harry Chapin Food Bank in Fort Myers, Florida.
“Wisconsin farmers have a history of being generous, especially to those in need or suffering through an emergency hardship,” association executive director Tamas Houlihan, said in a news release. “Through a coordinated effort by Wisconsin Potato and Vegetable Growers Association members, we are proud to be a part of delivering ‘Something Special from Wisconsin’ to our friends in the southern U.S.”
More than 30,000 pounds of potatoes were donated, according to the release. Wisconsin potato farms that made donations of potatoes included: 

Wysocki Produce Farm/RPE, Bancroft;
Bushmans’ Inc., Rosholt;
Okray Family Farms, Plover;
Alsum Farms, Friesland; and 
Worzella & Sons, Plover. 

Dean Kincaid Inc., Palmyra, donated 8,000 pounds of onions, according to the release.
In addition, Bula-Gieringer Farms, Friendship, contributed the cost of the freight, picking up product and delivering the load to Florida, according to the release.
A number of organizations also donated, according to the release. Those firms were:

Roberts Irrigation, Plover;
JW Mattek and Sons, Antigo;
Bula Potato Farms, Antigo;
Brenda and Dennis Bula, Antigo;
Coloma Farms, Coloma; and
Heartland Farms, Hancock.

The donation to relief efforts is among several efforts that association members take part in, according to the release. Members have helped provide meals and labor for the charitable organization Feed My Starving Children, donated salad bars to Wisconsin schools and given thousands of pounds of potatoes annually to Feeding America food banks. among other causes.