<!--*/

<!--
p.MsoNormal {margin:0in;
margin-bottom:.0001pt;
font-size:12.0pt;
font-family:"Times New Roman";
}
-->
/*-->*/

Following are final Pro Farmer Midwest Crop Tour results from Nebraska: 




Nebraska Corn




2017 District


Ear Count in 60 ft of Row


Grain Length
			(inches) 


Kernel Rows Around


Row Spacing
			(inches) 


Yield
			(per bu.)


Samples



NE2

86.50


6.30


16.10


30.00


146.46


4



NE3

85.52


6.93


15.98


29.78


160.63


65



NE5

102.15


7.05


16.13


32.89


177.27


27



NE6

89.78


7.09


16.47


30.10


173.73


98



NE8

98.00


7.33


16.70


32.14


186.34


14



NE9

82.83


6.90


16.43


30.17


155.69


107



NE Avg.

87.92


6.99


16.33


30.39


165.42


315 




3-year avg. by district


Ear Count in 60 ft of Row


Grain Length
			(inches) 


Kernel Rows Around


Row Spacing
			(inches) 


Yield
			(per bu.)


Samples



NE 2

90.79


6.93


16.02


28.97


174.26


7



NE 3

84.46


7.10


16.19


29.69


163.79


60



NE 5

93.49


7.04


16.15


31.76


166.29


24



NE 6

89.69


7.01


16.20


30.36


167.64


86



NE 8

93.70


7.01


15.96


31.26


166.97


13



NE 9

86.45


6.94


16.10


30.20


153.35


81



NE Avg.

87.12


7.01


16.14


30.30


162.51


271




 




Nebraska Soybeans




2017 District


Pod Count in
			3 feet


Soil Moisture


Growth Stage


Row Spacing
			(inches) 


Pod Count in
			3 X 3 Square


Samples



NE2

336.30


5.00


5.00


22.50


495.36


2



NE3

753.70


3.88


4.84


24.70


1139.02


64



NE5

923.51


4.80


5.00


28.70


1253.33


25



NE6

823.67


4.68


4.77


25.92


1155.93


97



NE8

743.22


4.43


5.00


26.71


1036.30


14



NE9

768.17


4.38


4.89


25.79


1099.64


108



NE Average

791.16


4.41


4.86


25.86


1131.02


310 




3-year Avg. by district


Pod Count in
			3 feet


Soil Moisture


Growth Stage


Row Spacing
			(inches) 


Pod Count in
			3 X 3 Square


Samples



NE 2

576.89


3.95


4.67


19.03


1061.18


4



NE 3

746.64


3.98


4.86


22.38


1250.71


60



NE 5

977.07


4.42


4.83


28.20


1260.65


21



NE 6

831.40


4.31


4.76


25.87


1184.45


86



NE 8

786.70


4.27


4.91


25.42


1131.81


12



NE 9

758.70


4.16


4.80


24.82


1127.63


80



NE Average

794.63


4.19


4.80


24.82


1182.12


264