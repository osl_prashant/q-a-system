The five top chefs in the 2017 Blended Burger Project — which features burgers made from a mushroom/meat blend — will cook and serve their recipes Jan. 23 during the Blended Burger Bun’anza at the James Beard House in New York.
The 2017 project — the third annual — featured burgers by chefs from 414 restaurants in 45 states, according to a news release. More than 400,000 consumers voted online for their favorite burgers.
“The Blended Burger Project has helped to support the national movement toward chefs developing more plant-forward, sustainably produced dishes, and patrons seeking healthier, more earth-friendly foods,” Kristopher Moon, vice president of the James Beard Foundation, said in the release. 
The winners:

Chef Bud Taylor, The Bistro at Topsail, Surf City, N.C. Taylor’s Goomba Burger uses local grass-fed beef blended with confit oyster and portabella mushrooms, topped with havarti, shiitake “bacon,” lemongrass aioli, heirloom tomato and baby arugula on a house-baked bun.
Chef Petros Jaferis, Houston Yacht Club, La Porte, Texas. Jaferis’ Greeklish Burger uses a beef chuck, crimini mushroom and caper patty on a brioche bun, with basil aioli with feta melted over the patty, topped with a mini Greek salad and paprika oil.
Chef Toni Elkhouri, Cedar’s Café, Melbourne, Fla. Elkhouri’s Brevard’s Taste of Summer Burger uses blended mushrooms and lamb on an apricot and liquid smoke-glazed bun with sour cherry mustard, basil, brie cheese, and an herb lemon caper walnut relish.
Chef Jon Lemon, Bareburger, New York City. Lemon’s Porchetta Burger is a 50/50 mushroom and wild boar blend, topped with a black garlic aioli, broccoli rabe, oven-roasted tomatoes and aged provolone cheese on a ciabatta bun. 
Chef Phillip Thomason, Vintage Kitchen, Norfolk, Va. Thomason’s Backyard Burger uses grass-fed beef blended with confit oyster mushrooms, aged double cheddar and charred ramp mayo, bacon rust, cab franc molasses, Hayman potato straws, coffee salt, demi-pain perdu, and brown butter mornay.

The Blend mushroom/meat concept was developed by The Mushroom Council and the Culinary Institute of America.