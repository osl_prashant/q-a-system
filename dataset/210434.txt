Cattle producers in North Dakota have been losing young calves to coccidiosis this spring, according to Gerald Stokka, the North Dakota State University Extension Service’s veterinarian.
Coccidiosis is an intestinal disease that affects several animal species. In cattle, it may produce clinical symptoms in animals from 1 month to 1 year of age, but it can infect all age groups.
Coccidia is a protozoan parasite that has the ability to multiply rapidly and cause clinical disease.
“Coccidia are very host-specific; that is, only cattle coccidia will cause disease in cattle,” Stokka says. “Other species-specific coccidia will not cause disease in cattle.”
The major damage to calves is the result of the rapid multiplication of the parasite in the intestinal wall and the subsequent rupture of the cells of the intestinal lining.
Several stages of multiplication occur before the final stage, the oocyst (egg), is passed in the feces. Oocysts are extremely resistant to environmental stress and are difficult to remove from the environment completely. Oocysts must undergo a final process called sporulation before they are infective again.
Oocysts frequently contaminate feed and water. When the sporulated oocysts are ingested by other animals, they start their life cycle over in the new host.
Symptoms
In young (3 to 6 weeks of age), suckling calves, clinical signs of coccidiosis may develop following stressful events such as weather changes, or if the calves are in unsanitary conditions.
“Symptoms or signs of coccidiosis will depend on the stage of the disease at the time of observation,” says Karl Hoppe, the Extension area livestock systems specialist at NDSU’s Carrington Research Extension Center.
In general, coccidiosis affects the intestinal tract and creates symptoms associated with it. In mild cases, calves only have watery diarrhea, but in most cases, blood is present in the feces. Straining, along with rapid dehydration, weight loss and anorexia (off feed), may be evident.
Animals that survive for 10 to 14 days may recover; however, permanent damage may occur. The lesions associated with coccidiosis that are found after death generally are confined to the cecum, colon, ileum and rectum.
Laboratory findings should be correlated with clinical signs for a diagnosis because other infectious diseases such as salmonella and bovine viral diarrhea virus also may lead to blood in the stool, Stokka notes.
The susceptibility of animals to coccidiosis varies.
“Coccidiosis frequently is referred to as an opportunist, which is a disease that will develop when other stress factors are present or in the young calves when exposure to the oocysts is overwhelming,” Stokka says.
“The life cycle of coccidiosis in calves is approximately 21 days,” he adds. “This means that if a 3-week-old calf is showing signs and symptoms of coccidiosis, the calf was exposed to the oocysts at birth. The logical conclusion to young calf coccidiosis is that calving grounds are highly contaminated.”
Treatment
Infected animals must be treated for the infection and to correct dehydration. Producers should select the proper drugs in consultation with their veterinarian. Sulfa drugs and a therapeutic dose of amprolium are available to treat coccidiosis. Antibiotics may be necessary if secondary bacterial infections are suspected,
Products also are available for treating the entire group of calves, but the logistics of medicating all the calves in beef herds is difficult, Stokka says. Treatment and prevention are most effective when started early.
Prevention
Stokka and Hoppe suggest these steps to prevent coccidiosis:
Move calving grounds to a clean area free of contamination.
Increase the amount of space per cow during the calving season.
Feed an additive that can reduce the presence of coccidia.
“Feeding a coccidiostat (decoquinate) or an ionophore (monensin or lasalocid) to the herd prior to and during calving may help,” Hoppe says. “Be sure to follow label claims because monensin and lasalocid have slightly different label claims.
“Feeding an ionophore to the cows for reducing the overall coccidia parasites present in the environment also has the benefit of improving feed efficiency,” he adds.