Crop calls 
Corn: 3 to 5 cents higher
Soybeans: 1 to 2 cents higher
Winter wheat: Mixed
Spring wheat: 2 to 5 cents higher

Corn futures favored a firmer tone overnight on weather concerns. Weather models are not in agreement about how soon and how far south rains will travel. But of more concern is the heat advisory for much of the central and western Corn Belt as the crop pollinates. Soybean futures saw choppy trade overnight, but ended the session on a firmer note. Crop concerns lifted spring wheat futures, but winter wheat was mixed, with buying limited by a firmer tone in the dollar.
Livestock calls
Cattle: Weaker
Hogs: Mixed
Cattle futures are vulnerable to followthrough from yesterday's selloff, but pressure should be limited as traders wait on this morning's online auction for more cash market direction. News of an atypical BSE case in Alabama should not impact the market. Meanwhile, hog futures are expected to see mixed trade again amid spreading, with deferreds firmer. The cash hog market is expected to see a steady to slightly weaker tone this morning.