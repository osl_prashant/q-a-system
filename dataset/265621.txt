BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Monday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2656  Up    14May         2612    2643    2597    2629  Up    14Jul         2641    2669    2623    2656  Up    14Sep         2655    2685    2639    2672  Up    15Dec         2660    2687    2640    2675  Up    16Mar         2646    2670    2627    2660  Up    16May         2650    2670    2630    2663  Up    15Jul         2658    2675    2658    2670  Up    15Sep         2665    2686    2652    2678  Up    16Dec         2675    2686    2668    2686  Up    16