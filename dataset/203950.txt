In 2011, in response to the deadly Jensen Farms cantaloupe listeria outbreak, cantaloupe growers in Colorado's Rocky Ford region came together to make sure something similar never happened again.
 
The irony, of course, was that Jensen Farms was 90 miles from Rocky Ford. But that didn't protect Rocky Ford growers from the outbreak's fallout.
 
Five years later, though, investments made by growers and the Rocky Ford Growers Association are paying off, growers and industry officials say - though there's still progress to be made.
 
Commerce City, Colo.-based Ringer & Son Brokerage Co. Inc. expects to begin shipping Rocky Ford fruit in mid-August, said Joshua Johnson, the company's president.
 
"Inside Colorado, they've come back just fine," Johnson said of demand for Rocky Ford melons. "Outside of the state, volumes are still lower."
 
Robert Sakata, president of the Colorado Fruit & Vegetable Growers Association, said that after Jensen Farms, packers made seven-figure upgrades to their sheds, even though they weren't to blame for the outbreak.
 
"I give them a lot of credit for coming back like they have. Every year, demand has grown."
 
Growers in Rocky Ford must now meet minimum standards to ship under the post-Jensen Farms Rocky Ford Cantaloupe trademark. Officials with Irvine, Calif.-based Western Growers lent a big hand helping the association develop those standards, Sakata said.
 
Sakata also has his own growing operation to keep him busy. 
 
Brighton, Colo.-based Sakata Farms' season, like that of many grower-shippers in Colorado, got off to a great start with excellent snowpack in the Rockies, Sakata said.
 
Water was so abundant, he said, Sakata Farms didn't have to start irrigating until mid-July.
 
Later in the summer, a series of storms rolled through northeast Colorado, Sakata said. Hail damage caused Sakata Farms to abandon 20 acres of sweet corn.
 
Sakata called it "typical Colorado" - in some fields, half of the crop was fine, the other half destroyed.
 
For the most part, though, Sakata's outlook on sweet corn and onions was still positive heading into August. Despite the hail damage, Sakata Farms expects to be up about 10% on corn.
 
Many Colorado growers should start shipping onions by Sept. 1, but Sakata Farms will likely start in mid-September, Sakata said.
 
Two things Sakata Farms won't be growing this year are broccoli and cabbage. 
 
Limited labor is one reason why. But with the food safety bar raised so much higher for all commodities, Sakata also became too worried about the risks of growing a ground crop that people often eat raw - and growing it too close to Denver.
 
"People who don't want to pay to dispose of their old TVs drive out and dump them at the edge of a field. We've also seen mattresses and couches."
 
Andy Nelson is The Packer's markets editor. E-mail him at anelson@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.