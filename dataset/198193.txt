With the New Year here, producers can take this opportunity to think back on their successes and challenges that occurred in 2015 and begin making plans to implement changes in certain management areas in 2016. As calving season looms in the not so distant future, the first things that come to my mind is considering plans for pre-calving vaccination of pregnant cows, and thinking about controlling calf scours. Attention to these areas is warranted as is critical to both performance of cows and health of newborn calves.Pre-Calving Vaccinations 
Calves are born without the protective immunity of antibodies to common environmental disease organisms. The "first milk", or colostrum, is what transfers protective antibodies to the calf, and is the reason why ensuring adequate colostrum ingestion is so critical for calves. Producers can improve the quality of colostrum by providing strategic vaccination to the dam while also providing adequate nutrition throughout the winter. A main goal of giving pre-calving vaccinations to pregnant females is to maximize protection against the calf scours complex. In order for this type of vaccination to be effective in transmitting protection to the newborn calf, it must be delivered to the pregnant female at the correct dose, and be within an appropriate window of time. Pre-calving vaccinations stimulate production of antibodies in the mother's body, which are then directed towards the mammary gland when she begins producing colostrum. Increased levels of antibodies targeted to scours organisms in the blood of the mother while she produces colostrum, results in more of these specific antibodies in the colostrum. These antibodies are then passively delivered to the calf through its first drink of colostrum, providing a first line of immunity to the newborn calf within hours after birth.
When should you administer vaccines?
In order to ensure the most effective transfer of high levels of antibodies, you must administer the vaccine at the proper time/s. Pregnant females begin to form colostrum by pulling antibodies from the blood and storing them in the mammary tissue 3 - 5 weeks before calving, and this process continues until birth. Performing booster vaccinations prior to 6 - 7 weeks of the expected calving date may not be effective in aiding high quality colostrum production or in preventing calf scours. Depending on the age of the female and type of product used, there are different requirements that need to be followed. Heifers require an initial vaccination followed by a booster at least 3 weeks later (follow label recommendations), whereas cows that have been vaccinated in previous years only require an annual booster. A comparison of different products is provided below. Always consult with your herd veterinarian for assistance in developing a pre-calving vaccination program specific to your cow herd needs.
Product NameDose; Admin. method
Bred Heifers
Cows
Scours Organisms(in Vaccine)
ScourGuard®4KC; 2 mL; IM
2 Doses:3 weeks apart
Booster Dose:3 - 6 weeks before calving
1 Dose:3 - 6 weeks before calving
Virus:Rotavirus, Coronavirus
Bacteria:K99 E.coli, C.perfringens type C
Scour Bos‚Ñ¢9; 2 mL; IM

1st Dose:8 - 16 weeks prior to calving.
Booster Dose:4 weeks before calving with Scour Bos‚Ñ¢ 4

1 Dose:8 - 10 weeks before calving
Virus:Rotavirus, Coronavirus
Bacteria:K99 E.coli, C.perfringens type C
Guardian®2 mL; SQ
1st Dose:3 months prior to calving
Booster Dose:3 - 6 weeks following initial dose
1 Dose:5 - 7 weeks before calving
Virus:Rotavirus, Coronavirus
Bacteria:K99 E.coli, C.perfringens type C & D

Management Factors
Scours is a complex and costly disease issue and should be confronted from many angles in order to limit outbreaks. Maternal vaccination can significantly increase the level of antibodies present in the colostrum, but in order to be effective calves must ingest an adequate amount of colostrum within the first 12 - 24 hours of life. Also, unsanitary and contaminated conditions may overwhelm the maternal antibodies calves receive. By keeping calving areas clean and by purposefully limiting cross contamination between age groups of calves you can ensure that this year's calf crop will have the best chance to stay healthy. The Sandhill's calving system is a management style that can help limit scours. For more information, check outManaging Newborn Calf Healthon iGrow.
If you have any questions regarding any of the concepts in this article or have any other production related questions do not hesitate to contact your local veterinarian, SDSU ExtensionVeterinarianorCow/Calf Field Specialist. Happy New Year! All the best in 2016!