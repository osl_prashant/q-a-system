Expanded radio advertising and a Chicago Marathon sponsorship are among the highlights of the Michigan Apple Committee's 2016-17 marketing plan.
This will be the first year the Lansing-based committee is a sponsor of the Bank of America Chicago Marathon, which will be held Oct. 9, said Diane Smith, the group's executive director.
As part of the sponsorship, committee staff will distribute Michigan apples at the finish line as a post-race snack, Smith said. About 50,000 runners are expected to participate.
By being a sponsor, the committee received two entrants into the race. Smith said that two employees of Sparta, Mich.-based Riveridge Produce Marketing would likely run in Michigan apple-themed attire.
In addition, the committee will exhibit at the race's health and fitness expo Oct. 7-8.
The Michigan apple industry also is partnering with the Detroit Tigers, the Detroit Red Wings and Michigan State University on sports-themed promotions this season, Smith said.
Also on tap for 2016-17, the committee is expanding its participation in a Pure Michigan radio advertising campaign.
The committee has been a partner with Pure Michigan, the state's official travel and tourism campaign, since 2011.
As in the past, ads touting Michigan apples will run in the Detroit and Chicago metro areas. This year, St. Louis and Milwaukee will be added to the mix, as the industry seeks to expand its presence in those markets, Smith said.
On the retail side, the committee is making a big push to take advantage of consumer loyalty programs, Smith said.
"We've seen a big increase in loyalty programs," she said. "There are always great response rates - 20, 30%."
Coupons, by contrast, typically don't generate those high of rates, she said.
The committee also will make an effort to get more custom signage in stores educating consumers about Michigan apples, Smith said.
"Probably the No. 1 thing we hear from consumers is they don't know how to identify different varieties. They see all these apples and say, 'What's that?'"
The committee also has secured a social media block grant for the 2016-17 marketing season that will help the Michigan apple industry in its outreach with both consumers and retailers, Smith said.
Earlier this summer, the committee notched one of its most successful social media posts ever, with 4,400 shares tallied.
And this fall, the committee will release the results of new consumer research, some conducted by A.C. Nielsen, on consumer preferences related to apples, Smith said.