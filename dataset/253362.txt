Grains and livestock mixed
Grains and livestock mixed

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mixed Friday in early trading on the Chicago Board of Trade.
Wheat for March delivery declined .6 cent at $4.914 a bushel; March corn was up 7.2 cents at $3.832 bushel; March oats dropped .4 cent at $2.636 a bushel while March soybeans declined 9.4 cents at $10.4620 a bushel.
Beef was mixed,pork was higher on the Chicago Mercantile Exchange.
April live cattle rose .55 cents at $1.2275 a pound; March feeder cattle lost 1.11 cent at $1.4248 a pound; April lean hogs rose 0.0058 cent at $.6750 a pound.