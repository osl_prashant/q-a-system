Sponsored Content
As we inch closer to spring, we are reminded that it’s time to check for winter damage to alfalfa, and it’s especially important to check on newly seeded stands. According to Jim Shroyer, Kansas State University Crop Production Specialist Emeritus, the two main concerns for alfalfa are winterkill and heaving. 
 
Winterkill Injury 
There is a wide range of winterhardiness among alfalfa varieties. Some varieties may have suffered winterkill injury this winter, especially where the crop had no snow cover. Like in wheat, winterkill in alfalfa occurs when the crown is frozen. When this occurs, the taproot will turn soft and mushy. In the early spring, check for bud and new shoot vigor. Healthy crowns are large, symmetrical and have many shoots. Examine them for delayed green-up, lopsided crowns and uneven shoot growth. If any of these characteristics are present, check the taproots for firmness. Some plants may even begin to green-up and then die. Plants putting out second leaves are likely unaffected. 
Interseeding alfalfa to thicken an alfalfa stand will generally not work. If the stand is one year old or less, plants will generally come up and then be outcompeted by the survivors from last year. Large dead spots should be disked first and then seeded. If the stand is two or more years old, interseeding alfalfa will not work because of autotoxicity. 
 
Heaving Effect 
As the soil freezes and thaws, alfalfa stands can be damaged by the heaving effect. This will be more likely to occur where soils are not under continuous snow or ice cover and where temperatures have been in the single digits at night. This winter has been cold enough to freeze the soil where it is not under snow cover. Soils with high levels of clay are especially prone to winter heaving. 
If heaving has occurred, dig up some plants to determine if the taproot is broken. Plants with broken taproots may green-up, but they perform poorly and eventually die. Slightly heaved plants can survive, but their longevity and productivity will be reduced. Crowns that heaved 1" or less are not as likely to have a broken taproot. With time, these plants can reposition themselves. Raised crowns are susceptible to weather and mechanical damage. Raise cutterbars to avoid damaging exposed crowns. 
 
Evaluating Plants and Stands
Producers should start to evaluate the health of their alfalfa stands as soon as the soil thaws. 

Look at the crowns and roots. 
Buds should be firm, and white or pink in color if they have survived with good vigor. 
The bark of roots should not peel away easily when scratched with a thumbnail. 
When cut, the interior of healthy roots will be white or cream in color. 

When alfalfa growth reaches 4 to 6", producers can use stems per square foot to assess density measure. A density of 55 stems per sq. ft. has good yield potential. There will probably be some yield loss with stem counts between 40 and 50 per sq. ft. Consider replacing the stand if there are less than 40 stems per sq. ft., and the crown and root health are poor. 
If an established stand was injured by winterkill or heaving, and large patches are dead, producers may want to buy some time before replacing the stand by temporarily thickening the bare areas with red clover. Red clover is not as susceptible as alfalfa to the plant toxins released by alfalfa (allelopathy) and helps provide good quality forage.
 
Sponsored by Lallemand Animal Nutrition