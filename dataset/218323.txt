It’s a convenience far too easy to take for granted. A quick run to the grocery store for a gallon of milk, package of cheese or quart of ice cream. Always available, always fresh.

An entire industry makes that purchase possible — hundreds of thousands of dairy cattle and their caretakers who provide quality products for consumers worldwide.

Holstein Association USA pays tribute to dairy farmers from coast to coast during the premiere episode of Holstein America, 9 p.m. CST, Thursday, Feb. 8 on RFD-TV. Mark the calendar or set your DVR to record this anticipated television broadcast.

“The Holstein breed’s story is among the most successful in U.S. agriculture — and it’s written by generations of passionate dairy producers,” says John Meyer, CEO of Holstein Association USA. “We’re honored to introduce these individuals and families in the Holstein America series.”

The hour-long program, sponsored by Merck Animal Health, shines a spotlight on the nation’s Holstein producers — from California’s lush central valley to the fall treetops of Vermont. Meet those who have dedicated their lives to U.S. Registered Holsteins; each cow an improvement on the past.

“No other breed can do the things that the Holstein breed can do,” Meyer says. “That’s why it’s the world’s perfect cow.”

The iconic, black-and-white Holstein cow provides the world with high-quality, nutritious dairy products that are the cornerstone to most modern diets. Today, the breed accounts for more than 90% of milk production in the United States. Levels made possible thanks to continuous improvements in efficiency and productivity.

Like each distinctive animal, each Registered Holstein operation is unique. Each family motivated by their own goals and aspirations. In Holstein America, learn about modern-day dairy production and hear from those with a passion for the Holstein breed of cattle.

A few featured in the upcoming show are:

Crave Brothers Farm near Waterloo, Wis., a family operation that’s a key cheese supplier for Whole Foods Market; Maple Grove Farm in Derby, Vt., where a young couple labors long hours together raising Registered Holsteins; Maddox Dairy near Riverdale, Calif., a large-scale operation at the heart of California’s central valley; Pappy’s Farm, Farr West, Utah, home to a family of Greek immigrants who are living out their American dream; and many more.

Again, join us for Holstein America at 9 p.m. CST, Thursday, Feb. 8.

RFD-TV is a leading independent cable channel available on DISH Network, DIRECTV, AT&T U-Verse, Charter Spectrum, Cox, Comcast, Mediacom, Suddenlink and many other rural cable systems. Reference your local listings for more information.

After the show, visit www.holsteinusa.com to find a complete collection of the videos online.