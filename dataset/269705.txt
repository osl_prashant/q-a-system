New plan shrinks salvage logging on land charred by wildfire
New plan shrinks salvage logging on land charred by wildfire

The Associated Press

MEDFORD, Ore.




MEDFORD, Ore. (AP) — New plans call for reduced salvage logging on land that burned during a wildfire last summer in southwestern Oregon.
The Rogue River-Siskiyou National Forest had eyed about 20 square miles (52 square kilometers) of land in January, but forest officials Monday released a new draft environmental study that calls for a salvage-logging area of about 6 square miles (16 square kilometers), the Mail Tribune reported .
The lightning-sparked wildfire, known as the Chetco Bar fire, burned more than 300 square miles (777 square kilometers) last summer. Most of the forestland will either be left alone or tapped for removal of hazardous trees near roads, trails and recreation areas.
Forest assessment teams had targeted lands for salvage that were already tapped for timber production and where the blaze charred more than half the canopy cover, said Jessie Berner, the forest's Chetco Bar coordinator.
The areas were narrowed down after removing sections that were too hard to reach, on steep slopes or next to streams, Berner said.
The possible salvage areas are scattered in the Chetco and Pistol river basins in Curry County. The areas account for about 2.5 percent of the U.S. Forest Service lands that burned in the fire.
The draft proposal also calls for 13.5 miles (22 kilometers) of short, temporary roads and opening about 6 miles (10 kilometers) of closed roads for log hauling, Berner said.
While it's too early to determine how many timber sales would result from the logging, Berner said the areas could produce about 70 million board-feet of timber, which translates to the amount wood needed to construct 2,300 homes.
Officials will take public comment on the draft proposal before determining the final scope of the project in June.
___
Information from: Mail Tribune, http://www.mailtribune.com/