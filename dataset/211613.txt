A Seattle startup is using technology to make it easier for growers to find workers and vice versa. 
Called Ganaz, the smartphone app and website is designed to help growers recruit, engage and retain their labor force, according to the co-founder.
 
The app and web platform — now available nationwide — was tested this summer with tree fruit and berry growers in Oregon. 
 
“We had a great summer testing it with cherry growers and blueberry and pear growers,” said Hannah Freeman, co-Founder of Ganaz. Freeman and Sri Artham co-founded the company in April.
 
For a fee of $29 for one job posting and discounted rates for multiple listings, growers can post work opportunities on the app and website. 
 
Ganaz reaches workers about the opportunity through the app and social media platforms like Facebook. “It works nicely to get them to connect with each other in a way they hadn’t before,” she said.
 
So far, Ganaz has worked with about 20 operations on the West Coast and the app has engaged thousands of farmworkers about job opportunities, Freeman said. 
 
“As blueberry farmers, my family relies on people that are between jobs to harvest our fruit. They may have just finished picking cherries or are waiting to pick pears,” Fletcher Hukari of Hukari Orchards in Hood River, Ore., said in a news release. “I see the Ganaz app as a way to connect those folks with us, creating a mutually beneficial and hopefully lasting relationship.”
 
Freeman said surveys the company has done indicate about 90% of farmworkers have a smartphone, though many may have a short-term prepaid smartphone plan and change numbers yearly. 
 
By connecting through the app, workers can have access to more information and employers have a deeper labor pool to draw on.
 
Ganaz posts job openings on Facebook targeted at communities where employers are seeking workers and then encourages workers to download the app.
 
“Some of our most enthusiastic workers are the ones moving from California and coming up to Washington for the cherries and then going to Oregon because there is that much more uncertainty with them about where they are going and what the jobs are paying,” she said. 
 
Other farmworkers may not move locations but they may change employers a few times per year, she said. Ganaz seeks to help employers evaluate why they may not be getting some of their workers back and what they can do to change that, she said.
 
The app doesn’t include any digital rating tool for either employers or workers but does enable employers to take private notes in the app about particular workers to help them remember who they would want to return.
 
“What we really want to be sure of is that we are gaining the trust of both the workers and the growers,” she said.
 
While the tool is limited to traditional seasonal workers already in the U.S., Freeman said the company eventually expects to build a tool to help employers manage workers in the H-2A program.