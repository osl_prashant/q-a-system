Are your cattle performing poorly during the summer?
Do your cattle show symptoms of fescue toxicosis during the late spring and through the summer?
Are you wanting to increase pasture forage production, quality and cattle carrying capacity through the summer?
"One easy way to increase pasture forage production is to incorporate legumes into your fescue pasture, and it is the season to begin to do that," said Dr. Patrick Davis, a livestock specialist with University of Missouri Extension.
Why legumes and not grasses?
"The majority of our grass base in this area is fescue and legumes seem to interseeded well into that grass base," said Davis.
Also, the legumes will produce a higher quality forage than the grasses because of the lower stem to leaf ratio. Lower stem to leaf ratio results in less neutral detergent fiber and more protein, which means cattle, can consume more higher quality forage.
Adding Legumes
Adding legumes to your fescue pasture has many benefits to grazing cattle. The legumes make better pasture for grazing year round according to Davis.
The main legumes to incorporate in this area are red clover, white clover, and lespedeza, which will increase forage production and quality during the late spring and summer when the fescue is lower performing or not growing.
"Because forage production and quality is improved, you will see an improvement in forage intake and cattle performance, which has potential to improve the profitability of the cattle operation," said Davis.
According to Davis, adding legumes to the fescue pasture is going to dilute the consumption of toxic fescue, resulting in less ergovaline consumption, fewer symptoms of fescue toxicosis and better performance of the cattle on pasture.
"When incorporating legumes into fescue pastures it is important to do a soil test and make sure soil pH and mineral levels are adequate for the legumes to grow," said Davis.
For the clover legumes mentioned above, a pH of approximately 6 is adequate while lespedza can tolerate a lower pH of approximately 5.5.
Seeding and Drilling
The two methods of seeding are frost seeding or drilling.
For frost seeding a producer will broadcast seed and allow cattle trampling and the freezing-thawing process to work the seed into the ground.
Drilling the seed will lead to good seed to soil contact, which is important for proper legume establishment.
For seeding rates, seeding dates, and depth of the legumes mentioned above refer to MU Guide Sheet G4652 as well as your local MU Extension agronomy specialist.
"Always inoculate your legumes before seeding. The goal is to incorporate legumes such that they account for 30 percent of the forage coverage," said Davis. "Legume stand management is key to the persistence of the legumes. Newly growing legume plants need time to grow and need grass canopy reduced so that they grow without competition."
Rotational grazing systems are helpful in keeping from overgrazing the legumes. Once cattle begin to graze, young legume plants need a four to five week rest period to allow root reserves to build-up.
"Periodically I get a cattle producer that contacts me asking about bloat related to cattle grazing legumes. If the legume has a lot of protein and forage is highly digestible there is potential for bloat. Of the forage species mentioned, white clover is the most likely legume to lead to bloat," said Davis.
The goal is 30 percent of the forage base to be legume to make bloat less likely. Using lespedeza or red clover, the cattle are less likely to have bloat.
Any producer that thinks legumes might cause their cattle bloat problems here are some things to do to reduce the likelihood of bloat:

Restrict grazing legume fields at first until cattle get use to the legumes.
Before cattle turn out to legume pastures, provide dry hay, so they are not hungry at the turnout, which reduces legume intake.
Provide cattle bloat blocks that contain proxolene or if you are supplementing the cattle mix proxolene in the supplement

"Legume incorporation into fescue pastures is one way to deal with summer slump, increase pasture production and quality in late spring and summer, as well as improve cattle performance and potential profitability of the cattle operation," said Davis.