The Packer traveled to Florida and spoke with vegetable growers about what they are dealing with in the wake of Hurricane Irma. The above photo shows a West Coast Tomato field Sept. 27 that was flooded by the storm, with dirt burying plastic that was laid earlier and then ripped up by the hurricane. 
We also spoke with citrus growers in Hendry County, one of the areas worst affected by the storm.

LABELLE, Fla. — Hurricane Irma proved a setback for some vegetable growers in Southwest Florida.
The storm hit when many plants were not yet in the ground, but it still caused quite a headache, more for some than others.
At a West Coast Tomato farm in LaBelle, wind ripped up more than 500 acres of plastic that had already been laid, and flooding covered it with dirt. Then weeds sprouted once the water receded.
Tony Jennison, farm production manager for Palmetto-based West Coast Tomato, said the company worked for almost a week to dig up the buried plastic, but the process was taking too long, so the company removed the drip tape and disced the plastic under.
West Coast Tomato had already made considerable progress Sept. 27 in re-prepping the field, but was still 10 days to two weeks behind schedule.
“We had a beautiful farm here, all set to just plant and roll, and we have to start from scratch — worse than scratch,” Jennison said.
Bobby Williams, owner of BWJ Farms, Immokalee, said flooding killed some young plants along with undoing field prep.
The hurricane put the company behind about five weeks, Williams estimated, pushing the start of harvest from late October to December.
“This is as bad a hurricane as (I’ve seen),” Williams said. “This is worse than Wilma. Wilma was bad.
“We’ve had storms come in and lose stuff, but usually there’s a little bit left,” Williams said.
Paul Meador, owner of Everglades Harvesting & Hauling, LaBelle, said Sept. 28 that he was not sure if he would be able to plant the 100 acres of vegetables he grows because of the storm and heavy rain that followed. The ground had been too wet to lay plastic.
“We may have missed our window of opportunity,” Meador said. “If we planted now, it would be for Christmas, and we’re still not in a position to plant.”
Mobley Plant World, LaBelle, lost 19 of the 51 buildings used to grow vegetable transplants.
Like other companies in the area, it was well into cleanup in late September and had 10 replacement buildings coming soon.
People who work in the fields were also affected by the storms. Irma not only caused widespread and lengthy power outages but also destroyed housing in and around Immokalee, a community largely made up of immigrant field workers.