Rochester, Minn.-based Reichel Foods’ line of Pro2snax to the Max is now available nationwide in four flavors, according to a news release.
The line is designed as a protein- and produce-packed meal replacement or shareable snack, with up to 17 grams of protein and fewer than 350 calories. 
The flavors:

Baby carrots and mild cheddar cheese with turkey sausage bites and almonds;
Sweet gala apples and white cheddar cheese, hard boiled egg, dried cranberries and almonds;
Sliced apples, white cheddar cheese, dried cranberries and turkey sausage bites; and
Sliced apples and mild cheddar cheese with hard boiled egg, dried cranberries and cashews.

“Consumers want more variety combined with fresh produce and proteins. These products give them both!” president and CEO Craig Reichel said in the release.