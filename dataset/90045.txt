Crop calls
Corn: Fractionally mixed
Soybeans: 4 to 6 cents higher
Winter wheat: Fractionally to 1 cent lower
Spring wheat: 3 to 4 cents higher
Soybean futures enjoyed short-covering overnight, with some additional support coming from concerns about flooding in Argentina as another burst of heavy rain is in the forecast early next week. The firmer tone in soybeans limited selling in the corn and winter wheat markets overnight, with spring wheat firmer amid short-covering and spreading with winter wheat. Also this morning, outside markets are generally positive for grains, with crude oil and gold stronger. The dollar favored a weaker tone overnight, but came off its lows.
 
Livestock calls
Cattle: Higher
Hogs: Mixed
Cattle futures are called higher on followthrough from yesterday's sharp gains as well as improvement in the beef market. Nearby futures still hold a discount to the cash market and attitudes are improving toward this week's cash prospects. Meanwhile, hog futures are called mixed amid spreading, with buying limited by yesterday's $1.23 drop in pork cutout values and ongoing weakness in the cash hog market.