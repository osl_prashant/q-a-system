When USDA's weekly crop condition ratings are plugged into Pro Farmer's weighted Crop Condition Index (CCI; 0 to 500 point scale, with 500 representing perfect), the HRW wheat crop climbed 3.39 points from last week to 338.87 points. Gains in the HRW crop were led by Kansas, as it was up nearly 2 points from last week. But the Oklahoma crop declined marginally.
Meanwhile, the SRW crop improved by 1.55 points from lsat week to 375.92 points. Minor improvement was noted in Ohio, Missouri, Illinois and Michigan compared to last week.




 
Pro Farmer Crop Condition Index





HRW




This week



Last
			week 



Year-ago


 


SRW




This week



Last
			week



Year-ago





Kansas *(39.07%)

131.67
129.72

130.74


Missouri *(9.94%)

36.97
36.47

35.46




Oklahoma (10.67%)

34.58
34.79

38.76


Illinois (9.79%)

37.21
36.92

36.52




Texas (9.94%)

33.01
32.82

35.39


Ohio
			(10.12%)

40.18
40.08

34.20




Colorado (10.39%)

33.25
33.46

31.57


Arkansas (3.84%)

14.68
14.57

19.83




Nebraska (7.08%)

24.79
24.37

24.63


Indiana (5.68%)

21.58
21.53

21.13




S. Dakota (6.26%)

21.78
21.71

19.96


N. Carolina (7.72%)

27.78
28.17

31.02




Montana (10.87%)

40.45
39.47

40.95


Michigan (10.70%)

38.84
38.62

31.88




HRW total

338.87
335.48

347.64


SRW total

375.92
374.37

372.30





* denotes percentage of total national HRW/SRW crop production.
Following are details from USDA's National Ag Statistics Service (NASS) crop and weather reports for key HRW wheat states:
Kansas: For the week ending April 16, 2017, temperatures were six to ten degrees above normal, according to the USDAs National Agricultural Statistics Service. Central and eastern counties continued to receive rainfall, while most western counties remained dry. The additional rainfall aided pasture and wheat development, but continued to delay corn planting in many areas. Powdery mildew and stripe rust have been identified in some wheat fields. There were 4.0 days suitable for fieldwork. Topsoil moisture rated 4% very short, 13% short, 70% adequate, and 13% surplus. Subsoil moisture rated 5% very short, 20% short, 70% adequate, and 5% surplus.
Winter wheat condition rated 4% very poor, 12% poor, 33% fair, 45% good, and 6% excellent. Winter wheat jointed was 65%, behind 75% last year, but ahead of the five-year average of 58%. Headed was 9%, ahead of 3% last year, and near 6% average.
Oklahoma: Light to moderate precipitation was experienced across the state last week, with areas of the Northeast and Southwest districts averaging more than 1.58 inches. Central Oklahoma and the Southeast district averaged nearly 1.24 inches of rainfall, while the rest of the state recorded less than an inch. According to the OCS Mesonet, drought conditions were intensifying due to minimal rainfall. Severe conditions were rated 13. 65% and mostly appeared across the eastern half of the state. Topsoil and subsoil moisture conditions were rated adequate to short. There were 4.5 days suitable for fieldwork.
Winter wheat jointing reached 92%, up 2 points from the previous year and up 4 points from normal. Winter wheat headed reached 40%, up 19 points from the previous year and up 15 points from normal.
Texas: Not yet available.
Following are details from USDA's NASS crop and weather reports for key SRW wheat states:
Ohio: There were 2.7 days suitable for fieldwork in Ohio during the week ending April 16, according to Cheryl Turner, Ohio State Statistician with the USDAs NASS. Warmer temperatures this week helped to dry out some soils and created more fieldwork opportunities. Farmers were able to top dress wheat, apply herbicides, and till fields where moisture surpluses had receded. Planting was very limited this week.
Michigan: There were 1.8 days suitable for fieldwork in Michigan during the week ending April 16, 2017, according to Marlo Johnson, Director of the Great Lakes Regional Office of NASS. For many in the state, persistent heavy rainfall kept producers out of their fields last week, as most ground was still too wet for planting or tillage equipment. Areas with sandy soil saw the start of some preliminary fieldwork, and a few producers were able to spread manure and make progress on oat planting. Overall, winter wheat had responded to the warmer temperatures and was beginning to green up and show growth where conditions were conducive. Some wheat fields were dry enough to be sprayed for weeds and receive fertilizer applications.
Missouri: Temperatures averaged 63.7 degrees, 9.4 degrees above normal. Precipitation averaged 0.32 inches statewide, 0.75 inches below normal. There were 4.4 days suitable for fieldwork for the week ending April 16. Topsoil moisture supply was rated 2% very short, 12% short, 72% adequate, and 14% surplus. Subsoil moisture supply was rated 5% very short, 18% short, 68% adequate, and 9% surplus.
Winter wheat headed reached 23%. Winter wheat condition was rated 2% poor, 32% fair, 58% good, and 8% excellent.