Inter-Korean summit food will have touch of Swiss
Inter-Korean summit food will have touch of Swiss

By KIM TONG-HYUNGAssociated Press
The Associated Press

SEOUL, South Korea




SEOUL, South Korea (AP) — North Korean leader Kim Jong Un will be served a nod to his Swiss school days following a high-stakes meeting with South Korean President Moon Jae-in over the future of his nukes.
A Korean "re-interpretation" of the Swiss fried potato dish rosti will highlight the food served at a planned banquet after Friday's summit at the border truce village of Panmunjom.
Other items on the menu include fish and rice using produce from the hometowns of Moon and his liberal predecessors who sought rapprochement with Pyongyang. North Korea will bring its famous cold noodles to the dinner.
The meeting between Kim and Moon is just the third-ever summit between the rival Koreas, who are now trying to restore ties after a decade of animosity over the North's nuclear and missile program. The two countries remain technically at war because the 1950-53 Korean War ended with an armistice, not a peace treaty.
A look at the food to be served at Panmunjom:
___
A TOUCH OF SWISS
While not many things are known about Kim's childhood, one thing Seoul is certain about is that he spent several years in his teens being schooled in Switzerland. That means a lot of Swiss-inspired dishes for Kim at the banquet.
Aside from the Korean twist on rosti, which basically will be fried grated potato from South Korea's Gangwon province topped with cheese, the South Koreans will serve Swiss chocolate, macaroons and cheesecake in a set of desserts that Seoul's presidential Blue House has named "Memories of Swiss." Also for dessert will be a mango mousse decorated with a blue map symbolizing a unified Korean Peninsula.
The map will include a dot representing a small island off the peninsula's eastern coast that has been the subject of a territorial dispute between South Korea and Japan. Japanese media reported that Tokyo lodged a protest to Seoul over the island being represented on the mango mousse, but South Korea's Foreign Ministry couldn't immediately confirm.
Kim, 34, did display a European palate last month in a Pyongyang banquet he hosted for Moon's envoys, who were served wine paired with different kinds of cheese.
After setting up the summit between Kim and Moon, South Korean officials in a subsequent trip to Washington brokered a potential meeting between Kim and President Donald Trump, which is anticipated in May or June.
___
SOUTH KOREAN MEAT AND FISH
South Korean chefs at the banquet will also serve baked John Dory fish, a tribute to the South Korean port of Busan, where the 65-year-old Moon was born. Other items on the menu include nods to the birth towns of former presidents Kim Dae-jung and Roh Moo-hyun, who met with Kim Jong Un's father, Kim Jong Il, in the two previous inter-Korean summits in the 2000s.
The chefs will serve croaker caught from Kim Dae-jung's hometown in southwestern Gageo island and rice from Roh's southern hometown of Gimhae. They also will serve grilled beef made from cattle at a ranch in the central town of Seosan. The ranch became famous in 1998 when late Hyundai founder Chung Ju-yung sent 1,001 cattle from the ranch in two separate convoy across the border to the North in an effort to aid reconciliation between the rivals.
"The welcoming banquet will reflect the desire of people who attempted to achieve peace and unification of our nation," Moon's spokesman Kim Eui-kyeom said.
There will be no dishes from South Korea's Daegu or Japan's Osaka, the respective birth towns of former presidents Park Geun-hye and Lee Myung-bak, who led a decade of conservative rule in Seoul before Moon took office but are now both in jail over separate corruption cases. Inter-Korean relations were terrible during the governments of Park and Lee, who employed a hard line against Pyongyang over its nuclear ambitions.
___
FAMOUS NORTH KOREAN NOODLES
Seoul's presidential Blue House said North Korea has also agreed to bring to the dinner "naengmyeon," or cold buckwheat noodles, made by the head chef of Pyongyang's famous Okryugwan restaurant.
Moon's office said Moon proposed the restaurant's noodles to be included on the menu, to which the North "gladly" obliged. Okryugwan is arguably North Korea's best-known restaurant and has branches in China.
___
Follow Kim Tong-hyung on Twitter at @KimTongHyung.