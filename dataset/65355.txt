Tax reform proposal could result in higher taxes for many family farmers, other small businesses






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






Tax reform bills are never easy and that was made clear during a House Agriculture Committee hearing.
Rep. Bob Gibbs (R-Ohio) pounced on the House Republican proposal to eliminate the tax deduction for interest expenses, which is included in the conference's blueprint to overhaul the tax code, calling the concept "ludicrous." The House Agriculture Committee is holding a hearing today in which several members noted the need for an exception.
The House Republican plan could result in higher taxes for many family farmers and other small businesses. The potential change would allow small businesses to deduct net interest expense, a measure the House GOP tax plan would eliminate in exchange for the ability to immediately write off equipment purchases.
"I can’t understand why anyone would come up with that," Gibbs said during a House Agriculture Committee hearing on tax reform. "They've obviously never run a business." Rep. Steve King (R-Iowa) also blasted the proposal.
Agricultural operations are substantially debt-financed and incur a substantial amount of interest expenses each year, such as on equipment and land, said Patricia Wolff, senior director of congressional relations at the American Farm Bureau Federation. Losing the deduction would hurt farmers' and ranchers' cash flow and ability to make large purchases, and be "even worse for young and beginning farmers who have to buy land and equipment to get started," Wolff said.
The Farm Bureau is one of the loudest voices pushing for an exception in an overhaul of the tax code. About 95 percent of farm sector debt in 2015 was held by banks, life insurance companies and government agencies, meaning that losing the interest deduction would harm their liquidity and make it difficult to purchase land and production inputs, the group said.
As it stands, many small businesses do not need an overhaul of the tax code to be able to deduct the cost of machinery or software. Companies that buy less than $2 million worth of equipment in 2017 can fully deduct the first $500,000 under tax code Section 179.
Some Republicans on the House Ways and Means Committee are looking for a way to save the tax benefits these companies already have without siphoning away too much revenue needed to cut business and individual tax rates. The House GOP plan would cut the corporate rate from 35 percent to 20 percent and the pass-through rate from a top rate of 39.6 percent to 25 percent.
The Republicans on the committee have mostly come to an agreement that there will be some sort of small business exception. They have decided there will be a carveout that mirrors current Section 179 language. The details on how it will be structured or how large to make it have yet to be decided.
Outlook: There is a long way to go before any tax reform proposal is near the end zone. Expect several other proposals impacting the business of agriculture. A major tax reform is equivalent to at least four farm bills. That ought to get your attention.


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.