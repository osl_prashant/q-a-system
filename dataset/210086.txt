Climate change is expected to reduce the growth and stature of big bluestem, a dominant prairie grass, by up to 60% over the next 75 years. That’s the consensus of scientists from Kansas State University, Missouri Botanical Gardens and Southern Illinois University collaborating on a study published in the peer-reviewed journal Global Change Biology. 
 
“Our results predict that climate change could greatly impact the tallgrass prairie as we currently know it, reducing forage for cattle in the drier parts of grasslands, place like Kansas,” said Loretta Johnson, professor of biology at Kansas State. 
 
Big bluestem is a common grass in natural and restored prairies across the central Midwestern region that includes Kansas, Nebraska, Oklahoma, Missouri and Iowa. Big bluestem is readily in found Kansas’ Flint Hills, a region of tallgrass prairie covering 9,936 square miles. The region’s economy is largely dependent on agriculture and cattle ranching. 
 
Big bluestem – or Andropogon gerardii – can grow to four to six feet tall, but the researchers found that could be reduced by up to 60%. As a result, the form of big bluestem that grows in the central Midwest could come to resemble the form that currently inhabits eastern Colorado on the edge of the species’ range. The tall forms of big bluestem could shift to the Great Lakes region where the grass is currently less common. 
 
The research team, in addition to Johnson, included Mary Knapp, associate agronomist and state climatologist; and Jacob Alsdurf, master’s student in biology. They found most of the change was because of alterations in rainfall that are expected to occur across the area, not because of increase temperatures. 
 
The authors are concerned the dramatic reduction in size of big bluestem foretells a fundamental shift in the nature of the Midwestern grassland ecosystem.
 
"Because big bluestem is currently a dominant grass species of the Great Plains and makes up to 70 percent of the plant biomass in places, how the ecosystem works could be affected by predicted changes in growth of this species," Johnson said.
 
"It was said in the past that the tallgrass prairies were so tall that a person riding a horse could literally get lost," said Adam Smith, assistant scientist in global change at the Missouri Botanical Garden. "Big bluestem is an iconic species in this system owing in part to its stature. If smaller forms come to dominate it could cause a fundamental shift in the habitat and ecosystem services prairies provide, such as forage for cattle."
 
Big bluestem grass can live several decades, so prairie restoration projects will need to consider the form of plants that would thrive at a site several decades into the future, researchers said.
 
The analysis also highlights the effects of climate change on common species that typically are not expected to be as vulnerable to anticipated climate change. Worldwide, 1 in 5 plants is already on the brink of extinction and climate change is only expected to add pressure on species struggling to survive. This study indicates that common species also may be vulnerable, researchers said.