Just over $325,000 in scholarships, awards and other support was given to veterinary students and graduate veterinarians at the 2017 50th Annual Conference of the American Association of Bovine Practitioners held September 14–16 in Omaha, Neb. The awards, funded by AABP members, AABP partners and the AABP Foundation, will enable recipients to further pursue
their careers in bovine medicine.
“A total of $211,500 in scholarships and travel stipends were award during the AABP Annual
Conference” notes AABP Executive Vice President Dr. K. Fred Gingrich, II. “These scholarships are supported by generous donations from Zoetis, Merck Animal Health, the Eli Lily Foundation and donations from individual members including the Amstutz Live and Silent Auctions and the 5K Stampede sponsored by Boehringer Ingelheim Vetmedica, Inc.”
AABP also provides over $40,000 for the Student Delegate program with support from
Norbrook, as well as awarding $4,500 to the student case competition winners, $6,000 to the graduate student research summary competition winners (with support from the BRD Symposium), $20,000 in 2017 education grants for students, $3,000 in student chapter funding and $40,000 in externship grants.
Amstutz Scholarships ($7,500 each)
·         Brandon Colby, The Ohio State University
·         Jessica Gaska, University of Wisconsin
·         Rebecca Gibbs, Colorado State University
·         Amy Kraus, University of Pennsylvania
·         Danielle Mzyk, North Carolina State University
·         Mark Spare, Kansas State University
·         Robert Stenger, Mississippi State University
·         Bryan Welly, University of California-Davis
·         Find out more about the Amstutz Scholarship at http://aabp.org/about/amstutz_winners.asp
Merck Animal Health Student Recognition Awards ($5,000 each)
·         Max Beal, Kansas State University
·         Ben Bennett, Kansas State University
·         Justin Casares, Texas A&M University
·         Taylor Crandall, Kansas State University
·         Alex Gander, University of Wisconsin
·         Marissa Horton, Cornell University
·         Cade Luckett, Texas A&M University
·         Tara Lynch, Oklahoma State University
·         Randall Morgan, University of Wisconsin
·         Rachel O’Leary, University of Wisconsin
·         Jenna Porter, Iowa State University
·         Delaine Quaresma, University of California-Davis
·         Cassandra Rice, Iowa State University
·         Travis Roberts, Ross University
·         Brian Schnell, University of Wisconsin
·         Kristy Shaw, Ohio State University
·         Lauren Thompson, Texas A&M University
·         Kendra Wells, University of Wisconsin
·         Find out more about the award at http://aabp.org/about/student_rec_winners.asp.
·         AABP Foundation-Zoetis Scholarships ($5,000 each)
·         Max Beal, Kansas State University
·         Ben Bennett, Kansas State University
·         Sarah Cook, Louisiana State University
·         Kevin Gavin, Washington State University
·         Lora Gurley, Michigan State University
·         Joseph Hammes, University of Minnesota
·         Cade Luckett, Texas A&M University
·         Ethan McEnroe, University of California-Davis
·         Michelle Mitchell, Purdue University
·         Delaine Quaresma, University of California-Davis
·         Brian Schnell, University of Wisconsin
·         Lauren Thompson, Texas A&M University
Find out more about the award at http://aabp.org/about/zoetis_winners.asp.
2017 AABP Research Summaries Graduate Student Awards
These awards are funded in part by the Bovine Respiratory Disease Symposium.
·         First Place: Dr. Oscar Benitez, Michigan State University
·         Second Place: Dr. Kathryn Bach, Cornell University
·         Third Place: Dr. Charlotte Winder, University of Guelph
Student Case/Research Presentation Winners
Student Clinical Case Presentation:
·         Overall Student Case/Research Presentation Winner and First Place Case Presentation Winner: Justin Casares, Texas A&M
·         Second Place, Student Case Presentation: Lauren Thompson, Texas A&M
Student Research Presentation:
First Place: Kristen Livengood, North Carolina State University
Second Place: Joseph Hammes, University of Minnesota
Find out more about the Student Case Competition at
http://aabp.org/Students/case/default.asp?year=2014&type=StudentCase.
2017 AABP Foundation Competitive Research Grants
·         Dr. Pat Gorden, Iowa State University, Comparison of analgesics for control of lameness associated pain in lactating dairy cattle
·         Dr. Craig McConnel, Washington State University, Comparison of group versus individual calf
·         rearing for a summary measure of health
Find out more about the Competitive Research Grants at
http://foundation.aabp.org/research_proposal/default.asp.
2017 AgriLabs Dr. Bruce Wren CE Awards ($5,000 each)
·         (Beef) Dr. Holt Tripp, Okotoks, Alberta
·         (Dairy) Dr. Sarah Geibel, Canyon, Texas
Find out more about the award at http://aabp.org/Members/ce_award/default.asp.