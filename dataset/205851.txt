Crop calls 
Corn: Fractionally to 1 cent firmer
Soybeans: Fractionally to 1 cent firmer
Wheat: 1 to 3 cents higher

Grain and soybean futures benefited from light short-covering overnight, but saw two-sided trade. Corn is severely oversold following five straight sessions of posting contract lows and due for a corrective bounce. But traders are concerned about Hurricane Harvey damaging ports along the Gulf and slowing exports. Traders are also concerned demand for U.S. wheat will wane amid stepped up competition from ample Black Sea region supplies. The U.S. dollar index is firmer this morning, but remains in its multi-month downtrend. Also this morning, USDA announced China has purchased another 131,0000 MT new-crop soybeans.
Livestock calls
Cattle: Lower
Hogs: Lower
Livestock futures are called lower on followthrough from yesterday's losses, but pressure on cattle should be limited as traders wait to see if cash trade occurs on the Fed Cattle Exchange online auction this morning. This week's smaller showlist and uptick in beef values and movement should help to stabilize the cash market. Meanwhile, hog packers are having no difficulty securing supplies. Yesterday's hefty estimated daily hog kill of 450,000 head has week-to-date kill running 25,000 head above the previous week. Additionally, the latest weight data from the Iowa-southern Minnesota market signals marketings are not current, as weights rose by 1.3 lbs. from the previous week.