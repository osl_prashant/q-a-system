TORONTO — Ontario’s new minimum wage is affecting the city’s entire food industry.
On Jan. 1 the province’s minimum wage rose to $14 an hour, after increasing to $11.25 an hour last April.
“We hire more than 600 people across all our businesses,” said Steve Bamford, CEO of Fresh Advancements, “so we have to raise prices to cover our costs, which inflates the cost to the end consumer.”
Tom Kioussis, vice president sales and marketing for Gambles Produce, said the impact of the new rate on retail and foodservice customers has been immediate.
“Our wage rate at Gambles is already one of the highest at the food terminal, so it’s not a huge impact on our daily operation,” Kioussis said.
“Most of our customers, however, rely on a large pool of part-time labor, which traditionally is compensated at minimum wage,” he said.
“This has spurred an increased interest in fresh-cut produce as both retailers and foodservice operators look to offset labor costs in-store and in the back room.” 
 
Local impact
Peter Streef, president of grower/shipper Streef Produce at the Ontario Food Terminal, said prices on local produce must go up at least 10% to cover the new rate.
“We anticipate pricing at retail is going to be stronger from now on,” Streef said.
Mimmo Franzone, produce director for Longo’s, sees the rate having an effect on Longo’s and on local suppliers.
“As growers invest more to automate planting and harvesting and make pack lines more efficient with fewer people, the cost of goods will spike,” Franzone said.
Greenhouse grower Fred Koornneef, whose Koornneef Produce Ltd. represents more than 20 local fruit and greenhouse growers, is upset with the governing provincial Liberals and “their careless disregard of agribusiness, which has raised the minimum wage by 25% in one year with no thought to an industry that relies on minimum wage labor.”
“What else goes up 25% in one year?” asked Koornneef, who had hoped the legislation would exempt offshore workers.
Grower John Den Boer, who parks his tractor trailers daily at the terminal’s outdoor farmers market, expects the new minimum wage to add $230,000 to his labor costs this year.
“Our Mexican workers will get $14 an hour plus housing, which adds up to $18.50 for an hour’s work,” Den Boer said.
“What we pay in an hour they get in a day at home,” he said, “and most of that wage increase will go abroad.”
Paul Sawtell, co-owner of local foodservice distributor 100km Foods, which now pays its staff above the minimum wage, said he hasn’t yet heard of any price increases from growers, “but I’m sure they will have to pass along increased costs.”
Sawtell expects sales to restaurants to be particularly hard hit, and worries that owners feeling pressure from labor expenses will cut corners on food purchases.
“I feel bad for local producers because they aren’t competing with their neighbors, they’re competing with global markets with artificially low labor costs that make it hard to compete,” he said.
“There’s going to be a big adjustment, probably consolidation of farms.”
Steve Crawford, business development specialist for produce at Milton, Ontario-based Gordon Food Service, said his foodservice customers are concerned about the effect of the minimum wage on their operations.
“Profit margins aren’t that high, so this isn’t helping any,” said Crawford, adding that more customers are looking at value-added items for their kitchen.
“I think they’ll have to live with it for a few months and learn how to adjust.”
The next minimum wage increase, to $15 an hour, is scheduled for Jan. 1.