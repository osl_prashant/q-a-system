Q.        How can I minimize losses if it rains during harvest?
A.         Rain during harvest and filling can be exasperating, especially after you"ve probably spent time carefully determining your forage"s moisture content. Weather events can affect both quality and yield due to:

Leaching, which is the movement of water-soluble components out of the forage. Many of the compounds are highly digestible carbohydrates. Up to a third of the dry matter (DM) leached by rain is soluble carbohydrates.
Respiration losses that are caused by the breakdown of soluble carbohydrates by plant enzymes. These losses occur while crop moisture levels are higher than about 30 percent and can be up to four percent of the DM.
Leaf loss in crops such as alfalfa. In one study, losses ranged from eight to more than 20 percent of the initial forage DM during a rainfall of 1 to 2.5 inches.

If possible, ensile rained-on forage separately from forages without rain damage. When ensiling higher protein crops like grasses and legumes with moisture levels greater than 70 percent, it can be common to have butyric fermentation. This occurs when butyric acid is produced by anaerobic bacteria called clostridia, which are present on the crop at harvest and also can be "inoculated" into the silage due to soil contamination.
Generally, lactic acid bacteria (LAB) produce sufficient acid and reduce pH quickly enough to keep clostridia from growing: the major species of Clostridium that cause silage spoilage cannot grow at a pH below 5.0. However, a quick pH drop is more difficult to achieve when forage is wet and/or low in sugars due to overcast conditions or even rain-leaching. Adding a research-proven inoculant that contains Pediococcus pentosaceus 12455 and Lactobacillus buchneri 40788, plus enzymes to ensure sugars for the fermentation, can help inhibit undesirable microbes during feedout and maintain proper silage quality, retaining important DM and nutrients. The two strains work together to drive a fast, efficient fermentation initially and reduce heating and spoilage during feedout.
I hope this information helps, and I wish you excellent weather during harvest.
Sincerely,
The Silage Dr.
Question about silage management? Ask the Silage Dr. on Twitter, Facebook or visit www.qualitysilage.com