Swine smooch: Principal loses bet with students, kisses pig
Swine smooch: Principal loses bet with students, kisses pig

By JEFF BOBOThe Kingsport Times-News
The Associated Press

CHURCH HILL, Tenn.




CHURCH HILL, Tenn. (AP) — Carters Valley Elementary School Principal Denise McKee might have solved the centuries-old quandary of how to get students excited about learning math.
Spoiler alert: It involves a farm animal and lipstick.
McKee challenged her kindergarten through fourth-grade students to show up for an afterschool Math Night at the Church Hill Food Lion, where they were asked to solve a series of age-appropriate problems involving groceries.
Whether any of those questions involved bacon, sausage or ham would have been purely coincidental.
McKee bet her students that if 50 of them showed up at Food Lion for Math Night, she would call an assembly of the entire student body and kiss a live pig.
Within 20 minutes of the event's start, the 50-student goal was met, and overall 69 showed up.
On Feb. 28, it was time for McKee to peck the pig and/or smooch the swine.
The pig was a 12-week-old female Hereford owned by CVES first-grade teacher Felicia Carter and her husband, who operate a farm.
Although the porker currently weighs in at 20 pounds, by the time she is full grown 10 months from now, she will be 450 pounds.
During an assembly, McKee and CVES family engagement coordinator Amie Lawson recognized Food Lion manager Janet Hammonds and her staff for hosting Math Night and providing some of the math problems for students.
One student from each grade also addressed the assembly, describing the problems they solved. Those students included kindergartner Landon Ireson, first-grader Paylan Boggs, second-grader Payton Bellamy, third-grader Alex Brown and fourth-grader Luke Bradshaw.
With all the formalities completed, it was time for McKee to pucker up.
She dabbed on a generous amount of lipstick, much of which was quickly imprinted on the pig's forehead and cheek after about a half-dozen smooches.
Her students gave McKee a rousing ovation, after which she asked if anyone could give her a mint or piece of gum.
___
Information from: Kingsport Times-News, http://www.timesnews.net