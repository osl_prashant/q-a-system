Crop calls
Corn: 1 to 2 cents higher
Soybeans: Mixed
Wheat: Fractionally to 3 cents higher

Corn futures favored a firmer tone overnight in reaction to corn planting slipping back below the five-year average pace at 47% complete as of Sunday. Additionally, with some rains in the near-term forecast, planting could slip further behind the average pace this week. Meanwhile, soybean futures didn't stray too far from unchanged overnight as traders begin to even positions ahead of tomorrow's USDA Supply & Demand Report. Wheat favored a firmer tone overnight, but SRW futures were choppy. Our weighted Crop Condition Index reflects notable deterioration in the crop from the previous week.
 
Livestock calls
Cattle: Mostly higher
Hogs: Higher
Cattle futures are called higher in all but the front-month contract as funds role out of June futures and into August and October contracts. June live cattle, however, now hold around a $17 discount to the average of last week's cash trade and traders remain impressed with the beef market. Meanwhile, bulls have the clear advantage in the hog market after futures posted new-for-the-move highs to start the week amid tightening market-ready supplies and firming pork values.