Cokes or cucumbers? Bananas or Butterfingers?
 
The fascination with the purchases of Americans receiving food stamps is a long-standing semi-obsession.
 
The thought process is something like, "When I'm buying mac and cheese on a shoestring budget, is the food stamp customer ahead of me buying lobsters and steak? 
 
Of course, not. SNAP participants are a lot like you and I.
 
In November, the USDA released a detailed analysis of the food purchased by SNAP households.
 
The report examined point-of-sale food purchase data to determine for what foods SNAP households have the largest expenditures and how SNAP purchases compare with non-SNAP households.
 
The point of sale data provided more detail on food expenditure patterns than previous studies.
 
The 49-page report, titled simply enough "Foods Typically Purchased by Supplemental Nutrition Assistance Program Households," tracks transaction data from January 1, 2011 through December 31, 2011 from a leading (but unnamed) grocery retailer.
 
What did the study find? From the report: 
 
Overall, the findings from this study indicate that SNAP households and non-SNAP households purchased similar foods in the retail outlets in these data. The findings hold true after assessing food expenditure patterns of SNAP and non-SNAP households using multiple categorization methods. Both groups of households spent about 40 cents of every dollar of food expenditures on basic items such as meat, fruits, vegetables, milk, eggs, and bread. Another 20 cents out of every dollar was spent on sweetened beverages, desserts, salty snacks, candy and sugar. The remaining 40 cents were spent on a variety of items such as cereal, prepared foods, dairy products, rice, and beans.
 
Marketers will want to take a deep dive into the report and closely look at SNAP sales of specific fruits and vegetables, as the report lists the top 25 vegetables and top 25 fruits in SNAP sales. 
 
For fruits, orange juice, bananas and strawberries were the top three purchased items for both SNAP and non-SNAP households.
 
For vegetables, the top purchase for SNAP households was russet potatoes, followed by frozen vegetables and pizza sauce. For non-SNAP households, the top three purchases in the vegetable category were russet potatoes, followed by frozen vegetables and then salad mix.
 
 
While the USDA is loathe to restrict food choices, recent research indicates that SNAP participants may need better controls on their expenditures to preserve their health.
 
According to coverage in Philly.com, a study from the Tufts University's School of Nutrition Science and Policy in Boston indicates that Americans who use or are eligible for food stamps have a higher risk of premature death than people who aren't eligible for them.
 
According to the study, investigators said people who used food stamps had a three times higher risk of death from diabetes, and twice the risk of death from heart disease or any cause than those who don't get food stamps.
 
Providing monetary incentives - such as doubling SNAP benefits if they are redeemed at a farmers market - has proven effective in boosting fruit and vegetable SNAP purchases. More healthy eating is needed.
 
With 20 cents out of every SNAP dollar spent on sweetened beverages, desserts, salty snacks, candy and sugar, the USDA should allow states to restrict SNAP purchases of unhealthy food. The health of the SNAP program and its recipients are at stake.