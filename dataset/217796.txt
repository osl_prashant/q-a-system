As President Donald Trump takes the stage at the 2018 American Farm Bureau Federation Convention in Nashville, the first president to do so since George H.W. Bush in 1992, he will unveil plans for improving rural infrastructure with a focus on broadband internet. But the message many of those in the seats of the Opryland Hotel ballroom will be looking for is reassurance that the North American Free Trade Agreement (NAFTA) will be improved, not abandoned. Those Farm Bureau members are worried the president will follow through on his repeated threats to withdraw from the 23-year-old agreement with the United State’s two largest ag trading partners.
“I’m going to make sure that he's aware he's going to be in front of several thousand people that's really nervous about the rhetoric around trade,” Farm Bureau President Zippy Duvall told Farm Journal. “We’re here as an organization to support him to make sure that he can fulfill his promise that he made to me and farmers that met with him in the Roosevelt Room in the White House when he said that the renegotiation of NAFTA would be even better for agriculture after they went through with it. If he could just give us something to hang our hat on and feel a little better because uncertainty in that situation is what makes us so nervous.”





Watch what Farm Bureau President Zippy Duvall plans to tell President Donald Trump in the video above.
Duvall had high praise for the new administration in his opening address telling the Farm Bureau membership that there has been a tremendous change in Washington D.C. with new access to the administration for farm interests. “It has been a breath of fresh air to be able to advocate for getting things done instead of having to constantly defend agriculture against a steady stream of challenges from our own government,” he said.
And while Duvall said he expects President Trump to improve, not scuttle NAFTA, he hedged his bet saying Farm Bureau will make sure the White House follows through on that promise. He then called members of the Farm Bureau “Go Team” on stage to instruct Farm Bureau members on how to flood the capital with tweets urging NAFTA support.
Concerns over trade were echoed by Senator Jerry Moran (R-KS). “Should the administration decide their negotiating tactic is to withdraw the risks are huge,” Moran told Farm Journal after meeting with a group of state Farm Bureau presidents. “Farm income is so low that it’s a risk I hope the president doesn’t take.”





Watch Senator Jerry Moran's comments ahead of President Trump's Farm Bureau speech in the video above.
Canada’s Agriculture Minister Lawrence MacAuley traveled south of the border to Nashville to lobby Farm Bureau members to influence their trade-negotiator-in-chief. The Canadian ag chief pointed out his small town of Prince Edward Island benefits from a quarter million dollars of agricultural trade with the U.S. each year. On the U.S. side, MacAuley said NAFTA is responsible for 9 million jobs, and that will ultimately win the preservation of the trade agreement. “Mostly people want to make money,” MacAuley explained. “And NAFTA has put money in the pockets of businesses right across North America.”





Watch Minister of Agriculture and Agri-Food Canada Lawrence MacAuley's speech to the American Farm Bureau Federation in the player above.
President Trump’s Monday speech is expected to center around the release of his Agriculture and Rural Prosperity Task Force report which calls for significant investment in rural infrastructure.  According to the Associated Press reporting on a speech preview by White House agriculture aid Ray Starling, the President will call for relaxed regulation to ease expansion of broadband access in rural areas including increased access to build cell towers on federal lands. The plan also calls for increased resources to fight a growing opioid epidemic in rural communities.
President Trump’s speech at the Farm Bureau Convention is scheduled for 3 p.m. central and will be streamed live on AgWeb.com.