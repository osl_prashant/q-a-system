No changes were made, not even minor ones, in Friday's USDA crop production report.
Crop production numbers and ending stocks were the same as November. Ending stocks were close to analysts' estimates.
"Overall, it's a very quiet and neutral report as far as U.S. numbers," said Rich Nelson, chief strategist of Allendale, in McHenry, Ill. "There are no changes to any of corn, soybeans or wheat in production, yield, or ending stocks."
Key numbers from Friday's WASDE report:
New corn ending stocks of unchanged from November at 2.403 billion bushels, which is slightly more than the trade guess of 2.413 billion bushels.
New soybean ending stocks unchanged at 480 million bushels, which is more than the average trade guess of 470 million bushels.
New wheat ending stocks unchanged at 1.143 billion bushels, which is more than the trade guess of 1.139 billion bushels.
Languid markets showed little immediate reaction to the WASDE report. After its release, by late morning, January soybeans edged up 1 ¾ cents at $10.28 ¾. December corn was up ½ cent at $3.46 ½.