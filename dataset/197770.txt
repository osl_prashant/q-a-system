No fertilizer posted a higher price this week as Urea led the downward slide.
Our Nutrient Composite Index fell 2.06 points on the week marking the most dramatic move we have seen in weeks.

Nitrogen

Urea led declines falling $3.98 on the week.
All four of the nitrogen products in our survey were lower this week after having firmed just slightly last week.
Anhydrous ammonia fell $1.16, UAN28% fell 57 cents per short ton and UAN32% softened $2.34.
This week's declines were far more aggressive than last week's mild gains, supporting our idea that nitrogen has topped.

Phosphate

Phosphates were softer with DAP 37 cents lower and MAP down 32 cents.
DAP and MAP fell by basically the same amount this week, maintaining the current spread between the two.
As with nitrogen, this week's phosphate price action seems to indicate a near-term top has been placed.

Potash 

Potash fell in kind with DAP and MAP, down 46 cents per short ton regionally.
We wrote about the world potash oversupply in an Inputs Monitor article last week titled, "Potash Producers Optimistic Despite Price-negative Fundamentals," click to read the post. 
World supplies and U.S. supply/demand fundamentals remain price-negative for potash and we believe potash will continue mildly lower near-term.

Corn Futures 

December 2017 corn futures closed Friday, May 19 at $3.90 putting expected new-crop revenue (eNCR) at $618.31 per acre -- higher $3.99 on the week.
With our Nutrient Composite Index (NCI) at 534.93 this week, the eNCR/NCI spread widened 5.45 points and now stands at -83.38. This means one acre of expected new-crop revenue is priced at a 83.38 premium to our Nutrient Composite Index.





Fertilizer


5/8/17


5/15/17


Week-over Change


Current Week

Fertilizer



Anhydrous


$525.23


$525.77


+54 cents


$524.61

Anhydrous



DAP


$447.48


$448.04


+56 cents


$447.56

DAP



MAP


$468.31


$467.89


-41 cents


$467.57

MAP



Potash


$335.41


$335.55


+14 cents


$335.09

Potash



UAN28


$251.58


$252.06


+47 cents


$251.49

UAN28



UAN32


$277.36


$278.19


+82 cents


$275.85

UAN32



Urea


$355.55


$356.06


+50 cents


$352.06

Urea



Composite


536.38


536.99


+2.06


534.93

Composite