USDA reports show 10.3 million cattle on feed for the slaughter market in feedlots with a capacity of 1,000 or more head. While steers and steer calves dropped 2% from 2015 to 6.83 million, heifers and heifer calves rose 4% from 2015 to 3.44 million.Placements in feedlots tumbled to the lowest rate since 1996 in September. Totalling 1.91 million head, which is 2% lower than in 2015.
However, marketing of fed cattle in September jumped to 1.73 million, up 5% from 2015.
See the full report here. 


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.