The U.S. Department of Agriculture has restricted four produce businesses for violating the Perishable Agricultural Commodities Act.
 
The following businesses and individuals are currently barred from operating in the produce industry for not paying reparation awards issued under the PACA:
Jonathan Mendez, doing business as 3 Mendez Produce, South Gate, Calif., for failing to pay a $2,580 award in favor of a California seller. Mendez was listed as the sole proprietor of the business.
JLP Farms Wholesale Retail Produce Transportation LLC, Tifton, Ga., for failing to pay a $165,302 award in favor of a New Jersey seller. John Pettiford, Diane Royster and Anthony White were listed as the members of the business.
Global Connection VA LLC, Dallas, for failing to pay a $101,533 award in favor of a Washington seller. Jose Luis Aguero and Alberto Vazquez were listed as the members of the business.
De La Cruz Produce, Gillette, Wyo., for failing to pay a $2,419 award in favor of a California seller. Francisco De La Cruz was listed as the officer, director and major stockholder of the business.