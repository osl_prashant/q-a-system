Often, Machinery Pete focuses on the highest prices he sees at auctions across the country.

This week, he shifts his attention to the sales on the lower end of the spectrum.

Watch Machinery Pete every Monday on AgDay.