When you hire a new employee, you have two responsibilities. Make sure the employee fills out a W-2 and a form I-9, and then verify, to the best of your knowledge, the identification cards they show you are real. That’s it. You don’t need to be a private investigator, you just need to verify they appear to be real.
Here’s a trick: Did you know there are a few social security numbers the Social Security Administration will never issue? It’s true. According to the Social Security Administration website, they will never issue:

SSNs starting with the number 9.
SSNs beginning with 666 or 000 in positions 1-3.
SSNs with the number 00 in positions 4-5.
SSNs with the number 0000 in positions 6-9.

If you hire a new employee and notice they have a social security number that breaks those rules, you can bet it’s not government issued.
 
Note: This story appears in the December 2017 magazine issue of Dairy Herd Management.