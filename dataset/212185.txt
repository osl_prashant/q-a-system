Suppliers watch trends to determine up and coming favorites in the ever-changing specialties category.For example, Sarah Pau, director of marketing, Pure Hothouse Foods Inc., Leamington, Ontario, said bite-size produce is popular right now.
She lists proportioned meals, great snacking options and convenience in preparation as specific trends.
Many of today’s trends start online.
“Thanks to the often colorful and sometimes weird-looking specialty produce, we have found that social media, especially Instagram, drives the popularity of certain items,” said Karen Caplan, president and CEO of Los Alamitos, Calif.-based Frieda’s Specialty Produce.
“For example, dragon fruit has become a staple garnish of smoothie bowls, and that gets people talking about dragon fruit offline and coming to the produce department looking for them,” Caplan said.
Foodservice also has a big influence.
Robert Schueller, director of public relations for Los Angeles-based World Variety Produce, which markets under the Melissa’s brand, said the trend for consumers to attempt to recreate restaurant-style dishes at home has made a big impact on specialty sales.
“People who have had an enjoyable experience when dining out want to take and duplicate that experience at home,” Schueller said.
 
Tropicals
Tropicals are one subcategory of the specialty department that have seen huge growth based on recent trends.
“In years past, consumers wanted to know ‘What is it?’ when they saw a star fruit or dragon fruit. Today Brooks’ most visited web pages and favorite social media posts are all about how-tos,” said Mary Ostlund, marketing director for Homestead, Fla.-based Brooks Tropicals.
Tropicals top the sales charts for specialty fruit, with mangoes up almost 7% to pull in 36% of specialty fruit dollars, according to Sarah Schmansky, director, Nielsen Fresh.
Next on the list is kiwifruit, which makes up 20% of specialty fruit dollars, followed by papaya at 12%.
Guava is a rising star in the specialty fruit category, with an impressive 138% increase over last year. Tamarindo is up 100%, cherimoya is up 86%, and passion fruit is up 38%.
Jackfruit isn’t on the official list of rising stars from Nielsen, but Caplan said they are an item to watch.
“Jackfruit has been consistently gaining popularity. Shoppers are starting to ask for them now,” she said.
 
Ethnic items
Ethnic specialties are also seeing a lot of increased interest.
“The U.S. is the most ethnically diverse country in the world. Many cultures are already familiar with different types of specialty produce items and the knowledge of sharing their country’s food culture is more frequent,” Schueller said.
“There is a new consumer and culinary interest in global flavors and produce items from all corners of the world,” said Jill Overdorf, director of business and culinary development and corporate executive chef for Coosemans L.A. Shipping, Vernon, Calif.
Overdorf says the items that shippers bring into the U.S. as “specialty” are the same as they were several years ago, but the demand is changing as consumers are more open to unfamiliar items.
“It is so exciting that this is our business, because what has truly changed is the consumer’s perspective of specialty — not the product itself,” she said.
For example, Overdorf said consumer acceptance of chilies and peppers has increased.
“Chilies like the ghost pepper, the Carolina reaper, aji amarillo and others are surprisingly ‘hot’ items in our market,” she said.
Jicama tops the specialty vegetable category. Up 3%, it now makes up 45% of the specialty vegetable category in dollars.
The “Mexican turnip” is followed by cactus leaves, which are up 5.6% and 35% of vegetable dollars. Cardoon is the rising star of the vegetable side, though, up 46% in sales dollars over last year.