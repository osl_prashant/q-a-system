Registration is open for the 2017 United Fresh Show and the Global Cold Chain Expo.
 
Information and event registration is available online for the United Fresh show and the Global Cold Chain Expo.
 
The June 13-15 event is set for the McCormick Place Convention Center in Chicago, according to a news release. The new United FreshTEC Expo and United FreshMKT Expo are co-located with the Global Cold Chain Expo and the International Floriculture Expo.
 
"We're excited to welcome attendees and exhibitors to experience the targeted FreshTEC and FreshMKT Expos & Conferences this June," president and CEO Tom Stenzel said in the release. "These new formats will address the innovative solutions we're seeing more of in our industry in response to the challenges faced throughout the supply chain." 
 
FreshTEC showcases the latest developments in technology, robotics, and other supply chain solutions while FreshMKT highlights the marketing and product development trends for fresh produce.
 
Early bird rates for the 2017 United Fresh convention are available through April 25, according to the release.