Remaining deer at Winona County farm found to have disease
Remaining deer at Winona County farm found to have disease

The Associated Press

ST. PAUL, Minn.




ST. PAUL, Minn. (AP) — The Minnesota Board of Animal Health has released updated results on a Winona County deer farm where chronic wasting disease was discovered in November.
The board says in a release that the last seven remaining white-tailed deer harvested at the farm were found to have the disease. Samples from deer that the producer had moved from the farm to the Winona city park did show the disease.
The board is working with the owner to clean and decontaminate the enclosure that contained the deer.
The board says it is monitoring one other farmed deer herd in the state, in Crow Wing County. That herd is quarantined and being tested for the disease.
Chronic wasting disease is fatal to deer and elk, and there are no known treatments or vaccines.