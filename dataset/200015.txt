Farmers across the Corn Belt made impressive progress in planting corn this week.According to the latest Crop Progress report, 13% of corn has been planted as of April 17, compared to 7% last year and 4% last week. The pace is significantly above the national five-year average of 8%.
Fifteen of the top 18 corn-producing states have reported at least 1% of corn in the ground. When compared to 2015, four states in particular are making impressive progress - Kansas (+15%) Kentucky (+21%), Missouri (+51%) and Tennessee (+29%).
See Figure 1 below for a more complete look at the state-specific progress compared to last year's report. Click the image to enlarge.

To learn more about your state's progress, click here to access AgWeb's interactive corn planting map or here for the full USDA report.