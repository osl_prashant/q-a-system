Some Oklahoma residents see subtle hiring discrimination
Some Oklahoma residents see subtle hiring discrimination

By GRANT D. CRAWFORDTahlequah Daily Press
The Associated Press

TAHLEQUAH, Okla.




TAHLEQUAH, Okla. (AP) — Across the country, private businesses and educational institutions are asserting their First Amendment privilege of faith when it comes to hiring employees and student admission. Some issues are gray areas, still awaiting a definitive ruling from the U.S. Supreme Court on the fine line between freedom of religion and civil rights.
But as cultures continue to evolve, many administrators and entrepreneurs are becoming more open-minded in their hiring and admission practices, accepting people whose personal beliefs and lifestyles may run counter to their own.
Only months ago, Attorney General Jeff Sessions reversed a policy from the President Barack Obama years, contending that federal civil rights law does not protect transgender people from discrimination. According to the Out and Equal Workplace Advocates, 28 states still have laws on the books that could allow an employer to fire someone for being gay, and other laws may permit the firing of transgender people in 30 states. One of those is Oklahoma.
However, business owners in Tahlequah say when it comes to hiring, they don't take into account the potential employee's personal life. They're just looking for the best people.
At Ned's, one of the more popular watering holes in town, owner Gary Kirkpatrick said someone's sexual orientation or gender identity has no effect on whether he would hire the individual.
"In fact, we've always been pretty open about it," he said. "We've had a drag show here before and it was pretty busy, actually."
It is not uncommon for an employee's personal life to affect his or her work life. But the term "personal life" might be too broad to include specific matters like sexual orientation, gender identity, race or religion. Most businesses, after all, are in the business of making money, not discriminating or discrediting certain segments of the population.
At Meigs Jewelry, owner Todd Mutzig said personal considerations, like sexual orientation, don't play a part in business.
"We at the jewelry store — really our mission, and the culture of the store — is to treat everyone with respect and build relationships with them, regardless of race or sexual orientation," said Mutzig, who is outgoing president of the Tahlequah Area Chamber of Commerce. "That's not even a factor. We're talking about humanity, and we as a store — that's not our place to judge."
Though Tahlequah businesses seem open to hiring members of the LGBTQ community, it doesn't mean some people haven't felt uncomfortable during interviews, or suspected their sexual or gender orientation could have cost them a job.
TahlEquality President Carden Crow said he has experienced "an incredible amount of discrimination in workplaces," the Tahlequah Daily Press reported .
One summer, Crow was searching for a job after college, taking part in more than 50 interviews. Even with his education, he said, the only job he could get was bartending. After nearly three years of working there, a management position became available.
"Basically, they said that I didn't look the part, so I was never promoted, whatsoever," said Crow. "In fact, a bar back who had worked there for three weeks was promoted as manager ahead of me."
Crow said discrimination is a fact of life for many LGBT individuals, and that "beggars can't be choosers" when it comes to employment.
"You get what you can take, and a lot of transgender individuals get stuck in that cycle where they are maybe being a little bit mistreated and a little bit overlooked, but they still have to work," said Crow. "They still have to live and eat and pay the rent. So they take it on the chin and they have to move forward."
Crow said that during his job interviews, he usually tells prospective employers up front that he is transgender. But not all people feel comfortable revealing gender identity or sexual orientation.
Tahlequah resident Leon Beck said he felt uncomfortable during an interview at The Drip, and suspected he would have been turned away had he told the owner he was gay.
"I knew that if I did, it would determine the outcome of the job," said Beck. "It wasn't that I was scared to tell him; it's just that I was disappointed to tell him, because I knew I wouldn't get the job. My mindset was there to get a job."
Al Soto, owner of the The Drip, is an outspoken conservative Christian in public and on social media. But Soto said his personal beliefs do not affect his professional behavior and attitude. He said his coffee shop "filters coffee, not people."
"I never ask people their sexual preferences in an interview," he said. "That's never even in my mind. I don't hire based on religion, sex, gender — none of that."
Soto said at least half of his staff members don't attend church, and at one point, he had a Wiccan employee.
"Half of my staff or more are females and all of my managers are female. It's not an issue with me. I'm a confident person and I hire the person who can best do the job," he said. "Putting my moral values on people would result in only my family working for me. You can't run a successful business like that."
He has two requirements for his employees, he said. They have to "love coffee and love serving people."
___
Information from: Tahlequah Daily Press, http://www.tahlequahdaailypress.com


An AP Member Exchange shared by the Tahlequah Daily Press.