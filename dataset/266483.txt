The grain markets screamed higher on March 29 after USDA released its annual Prospective Plantings report. This report typically holds a surprise or two and this year was no exception.
Here are the numbers:

Corn: 88.0 million acres in 2018, which is down 2% from 2017
Soybeans: 89.0 million acres, which is down 1% from 2017
Cotton: 13.5 million acres, which is up 7% from 2017.
All Wheat: 47.3 million acres, which is up 3% from 2017
Spring Wheat: 12.6 million acres, which is up 15% from 2017.

The corn acres weren’t a surprise, says Jerry Gulke, president of the Gulke Group.
“We were a little lower than that, I didn’t think they would come down that low,” he says. “The corn acres were just exactly what we needed—to have corn acres drop because we have too much. That took corn prices immediately higher.”
Soybeans held the surprise. Going into the Prospective Plantings report, the average trade guess for soybean aces was 91.1 million acres, with a range of 89.9 million to 92.6 million.
Gulke thought soybean acres would at least be 91 million, his estimate, based on client surveys, was closer to 92 million.
“But yet, we still had prices go up about 25 cents,” he says. “The good news is that we can plant just about anything we want and make money this year.”
The Prospective Plantings report provides the first official estimate of farmers’ acreage mix. It was based on surveys conducted during the first two weeks of March from a sample of 82,900 farm operators across the United States. On June 30 is when USDA will release the actual planted acres.
“In the meantime, this is a whole new ballgame,” he says. “We may have a pseudo acreage war to make sure we don’t lose more corn acres to soybean acres. This is one of the most exciting days we’ve had in a long time. It was a good day for agriculture.”
On Mondays, Gulke provides an in-depth market analysis with accompanying charts. Read his first installment of this new feature: The Rest of the Story: Global Demand or Lack Thereof
For next Monday’s edition, Gulke will discuss the long-term implications of the Prospective Plantings report. How bullish was this report for soybeans? What are the implications for corn? What could we as a potential carryover? He’ll also dive into the Grain Stocks report, looking at how much of last year’s crop is being stored on-farm versus off-farm.
To access Jerry’s comments, see AgWeb.com/Gulke.
Contact Jerry directly at info@gulkegroup.com or 707-365-0601.