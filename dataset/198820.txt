Heading into July, it makes sense for producers to focus on four factors driving the commodities markets, says Naomi Blohm, Stewart-Peterson.With so many moving parts‚Äîincluding USDA's major June 30 reports, the Fourth of July holiday and the growing season in full swing‚Äîit makes sense for farmers to take a wait-and-see approach during the week of June 27 as they attempt to walk the line between fundamentals and technicals.
"We have long-term uptrends still intact for the soybean market," explains Blohm in an "AgDay" Agribusiness Update segment. "The corn market just poked below it a little bit ‚... but it has the ability to come right back above it ‚... . I don't think hope is lost, however those four things that we have to be watching are so important because this is not over and done with quite yet."
The four factors Blohm will watch closely are:
Factor #1: U.S. dollar value. "That affects our export markets," she says.
Factor #2: Fund movement. "With the Commitment of Traders report this week, we'll see a big reduction," Blohm says. "But what we're going to be watching then is if they come back into the marketplace, into the third quarter [at the] start of July."
Factor #3: USDA reports. Farmers should pay attention to the reports themselves as well as the market's reaction to published data.
Factor #4: Weather. "The weather could still finally trump things if the weather should turn negative for grain production," Blohm says. "But of course the weather could trump things if everything is rosy and perfect. Then it's a totally different story."
Click the play button to watch the complete "AgDay" interview with Blohm

<!--

    function delvePlayerCallback(playerId, eventName, data) {
        var id = "limelight_player_351368";
        if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
            DelvePlayer.registerPlayer(id);
        }

        switch (eventName) {
            case 'onPlayerLoad':
                var ad_url = 'http://oasc14008.247realmedia.com/RealMedia/ads/adstream_sx.ads/agweb.com/multimedia/prerolls/agwebradio/@x30';
                var encoded_ad_url = encodeURIComponent(ad_url);
                var encoded_ad_call = 'url='   encoded_ad_url;
                DelvePlayer.doSetAd('preroll', 'Vast', encoded_ad_call);
                break;
        }
    }

//-->


<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->

Want more video news? Watch it on AgDay.