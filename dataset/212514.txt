Managing the spread of laurel wilt has become a full-time job for Florida avocado growers and researchers.
The fungus is spread by the redbay ambrosia beetle, which kills members of the laurel family including avocado trees.
Since 2012, the industry has lost 4% of its 700,000 commercial trees to the disease, or about 280 acres, said Jonathan Crane, tropical fruit crop specialist at the University of Florida’s Tropical Research and Education Center in Homestead.
Edward “Gilly” Evans, the center’s interim director, said local scientists are working to combat the fungus.
“In the meantime we have provided growers with a set of recommendations that enable them to cope with the disease and still generate a profit,” he said.
While the industry is anxious to find a cure, Crane said it’s more realistic at this point to try and manage laurel wilt through strategies such as early detection and destruction of affected trees, insecticides, fungicides and biological controls.
Developing disease-resistant avocado cultivars could take at least 10 years, he said, while a molecular breakthrough could take at least five years.
Manny Hevia, president of Miami-based M&M Farm Inc., said he’s losing two to three avocado trees every few weeks.
“All we can do is try and stay ahead of the disease as much as possible,” Hevia said. “It’s quite devastating and sad — some abandoned or poorly managed groves have completely disappeared.”
Jessie Capote, executive vice president and co-owner of Miami grower-shipper J&C Tropicals Inc., has also seen the devastation first-hand.
“If you drive around South Florida you see the impact,” Capote said. “I’ve seen huge groves taken down and burned, and I’ve watched trees shrivel and die in a few months. I keep hearing we’re losing 5% of the crop year over year but that seems high.
“Some people are replanting but we haven’t started,” he said. “Why replant when there’s no end in sight to the disease? It’s a conversation everyone’s having, whether you’re a small or large player.”
Eddie Caram, general manager of Princeton, Fla.-based New Limeco, said he’s concerned but managing the disease by cutting down trees and replanting.
“If we let it go, if nobody cares, in the next 15 years it could destroy the industry,” Caram said.
Mary Ostlund, marketing director at Homestead, Fla.-based Brooks Tropicals, Florida’s largest grower, said Brooks has been able to keep the beetle cornered by watching groves closely and acting quickly when an infected tree is discovered.
“Keeping groves as healthy as possible seems to deter the beetle,” Ostlund said. “It means working to make sure the trees are healthy, pruned and well fed.”
Evans said growers who don’t manage their groves properly or don’t follow the scientific recommendations accurately will lose their business.