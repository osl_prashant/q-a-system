Wisconsin dairy says owner among Indiana plane crash victims
Wisconsin dairy says owner among Indiana plane crash victims

The Associated Press

ROSSVILLE, Ind.




ROSSVILLE, Ind. (AP) — A Wisconsin dairy owner and his son-in-law were among at least three people killed when a small plane crashed into a central Indiana farm field, the business said Friday.
Kewaunee, Wisconsin-based Pagel's Ponderosa Dairy identified those who died as business owner John T. Pagel, his son-in-law, Steven Witcpalek, and pilot Nathan Saari.
No one survived the crash that happened about 7:30 p.m. Thursday just north of the small town of Rossville, about 60 miles (96.5 kilometers) northwest of Indianapolis, according to Indiana State Police. It had taken off from the Eagle Creek Airport in Indianapolis and was headed to Green Bay, Wisconsin.
Multiple people were on board the plane when it crashed into the muddy field, but state police spokesman Sgt. Kim Riley said investigators hadn't confirmed by midday Friday how many people it was carrying.
"The wreckage is spread over quite a big area," Riley said.
A statement from the business said the Pagel family is grieving the loss. "We appreciate the outpouring of the communities' thoughts and prayers," the statement said.
State police identified the plane as a Cessna 441 Conquest Turboprop. Such twin-propeller planes can carry up to 10 people.
There was no severe weather in the area at the time of the crash and a possible cause wasn't immediately known.
Investigators from the Federal Aviation Administration and the National Transportation Safety Board arrived at the scene Friday, Riley said.