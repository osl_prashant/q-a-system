DENVER - Borrowing a page from Gatorade, Potatoes USA will use elite endurance athletes to promote its performance message.
The group, convening March 14-16 for its annual meeting, has settled on performance as a key health benefit it wants to communicate to consumers in a long-term strategy (see related coverage, A14).
Athletes can play a key role in accelerating the popularity and acceptance of products, said Blair Richardson, president and CEO.
"Gatorade was not the drink of every child and family member out there when they started," he said.
"They started with athletes and the athletes changed perceptions."
Richardson said Potatoes USA thinks the industry can change public perceptions, using the help of performance athletes.
"Why are marathon runners putting in their pouches small potatoes that they have roasted ahead of time and eat them along the way as they are running the marathon?" he said.
"Because they already know that (potatoes) are a really awesome product."
The path to use athletes as spokespeople for spuds is under development, Potatoes USA leaders said.
"We don't have a full road map yet, but we know there is an opportunity," said John Toaspern, chief marketing officer for Potatoes USA.
He said U.S. endurance athletes, who number about 22 million, consume much more potatoes than average consumers, he said.
Another 38 million people work out four to five times per week, care about what they eat and emulate performance athletes, he said.
When you add in people that work out at least once a week, Toaspern said that the number of people who care about their athletic performance numbers 112 million in the U.S.
Even white-collar workers need strong cognitive performance, and Toaspern said good carbs from potatoes allow those people to perform better.
Kim Breshears, director of marketing programs for Potatoes USA, said the focus on performance is a long-term strategy.
The first steps will be to invest in research on how potatoes provide consumers with an important part of a healthy lifestyle.
"We have a lot of good evidence that is available today, but we need to build on that foundation," she said.
Eventually, she said Potatoes USA may seek out sponsorship of high-profile athletic events and leverage the power of influencers to enhance the appeal of potatoes.