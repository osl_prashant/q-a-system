USDA will release its first survey-based winter wheat estimate at 11:00 a.m. CT Wednesday. Analysts expect this figure to come in around 1.293 billion bu., down 379 million bu. (22.7%) from year-ago. The April 29-May 1 snowstorm will make it difficult for USDA to assess the size of the crop. Therefore, it will likely be its biggest wheat estimate of the year.




2017 Wheat Production


Avg.


Range


2016




in billion bushels




All wheat


1.859


1.776-1.984


2.310




All winter


1.293


1.200-1.474


1.672




HRW


0.769


0.686-0.915


1.082




SRW


0.305


0.260-0.335


0.345




White winter


0.217


0.180-0.263


0.286




USDA will also update its 2016-17 balance sheet and issue its first 2017-18 projections on Wednesday. Analysts surveyed by Reuters expect just modest tweaks to old-crop carryover, with corn expected at 2.326 billion bu., soybeans at 438 million bu., and wheat at 1.162 billion bushels.
Looking ahead to 2017-18, soybean ending stocks are expected to climb to around 563 million bu., while corn and wheat carryovers are expected to fall to 2.122 billion bu. and 934 million bu., respectively.




Commodity


2016-17 Carryover


2017-18 Carryover






Avg.


Range


USDA April


Avg.


Range




in billion bushels




Corn


2.326


2.269-2.500


2.320


2.122


1.835-2.400




Soybeans


0.438


0.417-0.466


0.445


0.563


0.420-0.759




Wheat


1.162


1.145-1.200


1.159


0.934


0.842-1.000