Court: Decision ending sea otter relocation program legal
Court: Decision ending sea otter relocation program legal

The Associated Press

SAN FRANCISCO




SAN FRANCISCO (AP) — A federal appeals court has upheld a decision by federal wildlife officials to end a program to relocate endangered sea otters off the California coast.
The 9th U.S. Circuit Court of Appeals on Thursday rejected lawsuits by fishing industry groups that argued Congress required the U.S. Fish and Wildlife Service to continue the program indefinitely.
Starting in 1987, wildlife officials relocated southern sea otters to San Nicolas Island off Southern California to try to create a population that could survive an environmental disaster. The program also created an area where fishing groups would be exempt from liability for inadvertently harming otters.
The program ended in 2012 after the San Nicolas otter population failed to take off.
An email to an attorney for the fishing groups was not immediately returned.