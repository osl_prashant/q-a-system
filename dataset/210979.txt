During a recent Calf and Heifer VIP event hosted by Purina at the company’s Animal Nutrition Center in Gray Summit, Mo., calf experts offered advice on calf feeding:It takes 85% less time for a calf to get 2 quarts of milk from a bucket compared to straight off the cow. It is 35% quicker on a nipple bottle versus off the cow
Feeding through a bottle with a nipple will help slow down the passage rate and increase salvia. Having saliva adds more enzymes that promote protein and fat digestion
Slowing down milk intake helps move digestion from the small intestine into the abomasum where nutrients will more readily be picked up
The hole in a nipple should not be too large in diameter. If it is too large it defeats the purpose of feeding with a nipple. This might require replacing nipples regularly when holes become too wide
If the nipple leaks milk when you turn it upside down, get a new nipple
“Full bucket syndrome” tends to happen with calves when there is more grain in the bucket than needed. For instance, a newborn or week-old calf isn’t going to eat much feed so there should be none to very little grain available starting out
A starter bucket that is shallow might be a better fit for younger calves to reduce feed waste or overfeeding. It also helps encourage calves to eat because they aren’t sticking their heads down where they can’t see
“Empty bucket syndrome” is another problem seen on dairies for older calves. Not having enough feed regularly to calves throughout the day can lead to calves eating too fast when they are fed causing digestive upsets or bloat
Use bright colored water buckets to help feeders see what is at the bottom of the bucket. Dark or black colored buckets are harder to see at the bottom if dirt or debris is present
Put a physical divider like plywood or plastic between the water and feed buckets
A trial from Purina showed that separating the feed and water with a divider increased average daily gain by 0.3 lb. 
 
Note: This story appears in the May 2017 issue of Dairy Herd Management.