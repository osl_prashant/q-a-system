BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Tuesday:
Open  High  Low  Settle   Chg.ORANGE JUICE                          15,000 lbs.; cents per lb.                May     138.10 138.35 136.20 137.70   —.20Jun                          138.00   —.40Jul     138.40 138.60 137.00 138.00   —.40Aug                          138.60   —.65Sep     138.35 138.95 138.05 138.60   —.65Nov     139.55 139.55 139.30 139.45   —.80Jan     139.95 140.10 139.95 140.10   —.85Feb                          140.65  —1.10Mar                          140.65  —1.10May                          141.40  —1.10Jul                          141.50  —1.10Sep                          141.60  —1.10Nov                          141.70  —1.10Jan                          141.80  —1.10Feb                          141.90  —1.10Mar                          141.90  —1.10May                          142.00  —1.10Jul                          142.10  —1.10Sep                          142.20  —1.10Nov                          142.30  —1.10Jan                          142.40  —1.10Est. sales 518.  Mon.'s sales 821      Mon.'s open int 13,177,  up 45