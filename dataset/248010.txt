The U.S. Department of Agriculture has filed a Perishable Agricultural Commodities Act administrative complaint against Greendom Corp.
It’s not the first time the USDA has taken action under PACA against the company. In October 2016 USDA sanctioned the Miami company for failing to pay a $5,698 award in favor of a Georgia seller.
The current complaint alleges Greendom Corp. failed to pay $160,712 to seven produce sellers from October 2014 through June 2016, according to a news release.
The company can request a hearing, but if USDA finds the company guilty of the alleged violations, Greendom would be barred from the industry for two years and its principals for a year.
The company handles mixed melons, squash, okra and Asian vegetables, according to its Facebook page.