Salinas, Calif.-based Taylor Farms has launched a line of stir-fry kits.
The vegetables in the kits are washed and sliced, and the kits take five minutes to cook, according to a news release.
The stir-fry kits have been at retail since early August.
"The customer response has been very positive," Bryan Jaynes, vice president of product management for Taylor Farms Retail, said in an e-mail. "Our retail partners are telling us that these products bring the same convenience to the veg category as salad kits bring to the package salad category. In fact, many customers are merchandising stir-fry kits with salad kits."
The four kits available are teriyaki, ginger garlic, sesame chile, and orange. The kits include trendy vegetables such as Brussels sprouts, kale and bok choy along with snow peas, broccoli, carrots and other vegetables.
The suggested retail price of a kit is $3.99.