The combination of midweek rains across the Corn Belt and USDA’s July 12 reports caused major price reversals in the corn, soybean and wheat markets. But traders recognize dry spots remain, especially in the western Belt, and next week’s forecast is warmer and drier. This helped corn and soybean futures to pare weekly losses on Friday. Spring wheat futures also pulled the winter wheat markets higher on Friday as traders noted the forecast for weekend temps topping 100°F and the likelihood of another drop in condition ratings on Monday. But winter wheat still posted losses for the week.
Pro Farmer News Editor Meghan Vick provides weekly highlights:




Live and feeder cattle futures rallied this week as lower beef prices spurred impressive movement. Plus, cash cattle prices rose. In contrast, cash hog prices slipped and pork movement was lackluster, which weighed on most lean hog contracts.
Click here for this week's newsletter.