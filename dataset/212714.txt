Although heavy winter rains disrupted the planting season for some produce, California’s Kern County has enjoyed favorable growing conditions this winter and spring.Despite gaps in planting for some, growers expect little to no interruption in supply.
Growers will harvest carrots, table grapes, cabbage, citrus and melons, among other commodities, in the upcoming months.
The citrus market is looking strong this year, as the yield was lower than last year’s, said Derek Vaughn, salesman with Bakersfield, Calif.-based Johnston Farms.
Average prices are up $4 on 40-pound cartons of navel oranges over 2016 prices.
“Last year was a heavy year. This is more of a normal crop,” he said.
Johnston Farms planted bell peppers in early April, with an expected harvest in June.
Bakersfield-based Dan Andrews Farms LLC replaced lettuce with green and red cabbage this spring cycle, said owner Danny Andrews. 
Cabbage prices are nearly double what they were last year, he said, because of a shortage caused by the combination of extreme heat in the south and rain and acreage reductions in the north. 
“Hopefully we’ll have enough volume to have a good return on investment because the yields aren’t as high but the prices are higher,” he said.
The company began planting honeydew and watermelon in early April for a June harvest.
“The weather’s ideal right now for planting,” Andrews said.
At Bakersfield-based Top Brass Marketing Inc., table grapes will be available no later than early July and should continue through November despite heavy winter rains, said Brett Dixon, president and sales manager.
All colors will be available throughout the season.
New varieties this season include Allisons, a red seedless, and Sweet Globes, a green seedless. The firm is also allotting more acreage to sweet scarlets because of strong demand.
“New varieties are being planted for future years to continue to add to our selection of varieties offered in table grapes,” Dixon said.
Another table grape grower, Delano, Calif.-based Jasmine Vineyards Inc., expects to start packing fruit on time in July, said president Jon Zaninovich.
This season, the company will harvest Summer Crunch, a new variety of green seedless, in mid-July.
Another green seedless, Sweet Globe, is in production for 2018.
The firm no longer grows red globes or crimson seedless, two varieties that were once central to its operations.
“Labor costs are so high, we’re trying to find grapes that are less labor intensive. The new varieties have been fantastic in that respect,” Zaninovich said.
Acreage is up about 15% to 20% from last year, Zaninovich estimated. However, the crop is forecasted to be lighter than last year’s, which was unusually heavy.
At Arvin, Calif.-based Kern Ridge Growers LLC, which grows and ships conventional and organic carrots year-round, the quality and volume looked comparable to previous years, said Andrew Bianchi, sales manager.
“Prices are down a bit compared to last year due to an increase in supply,” he said.
The firm in mid-April was growing carrots in the Imperial Valley and planned to transition back to Bakersfield in five or six weeks.
No pests or disease-related crop issues have been recorded, Bianchi said.
Another conventional and organic carrot grower-shipper, Bakersfield-based Grimmway Farms, also expects a steady output throughout the season, communications and engagements manager Valorie Sherman said.
“Crop volumes and prices are consistent with those from this time last year,” she said. 
Sherman and Dixon from Top Brass observed an increased demand for organics.
“We see more and more acreage going organic to fill those demands,” Dixon said.