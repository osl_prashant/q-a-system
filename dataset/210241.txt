Major banana suppliers don’t anticipate any supply gaps or problems with quality this fall, despite the hurricanes that struck late this summer.
“We’ve had quite a few weather events through Mexico through the different growing areas,” said Rick Feighery, vice president of sales for Philadelphia-based Procacci Bros. Sales Corp.
But no major hits were reported where bananas destined for the U.S. are grown.
He said he expects good quality and good availability at least through the end of the year.
“We should be OK, barring any new unforeseen storms,” he said. But he cautioned that hurricane season isn’t over yet.
He expected prices to remain “mostly consistent.”
F.o.b. prices for 40-pound cartons of bananas out of Colombia and Costa Rica were mostly $18 at the Atlanta terminal market Sept. 25, according to the U.S. Department of Agriculture. Last year at the same time, they were $17.
“Overall, the industry enjoys good productivity levels, and volumes should remain consistent with recent years in both conventional and organic varieties,” said Bil Goldfield, director of corporate communications for Dole Food Co., Westlake Village, Calif.
“Year-after-year, consumer demand for bananas is consistently strong, regardless of the crop season,” he said.
Dole sources bananas from Costa Rica, Ecuador, Guatemala, Honduras, Colombia and Peru.
San Diego-based Organics Unlimited Inc. expects to expand its acreage over the next year, said Mayra Velazquez de Leon, president and CEO.
The expansion project began in 2015, when the company had 1,087 acres, and should reach 1,865 acres by the end of 2018.
Organics Unlimited grows most of its bananas in Mexico but will supplement that fruit with product from Ecuador during the winter, she said.
As of late September, none of the company’s banana growing areas had been affected by any major storms, she said.
“Organics Unlimited does not foresee any impact to the crop in the next few months,” she said.
Although banana plantations in the Dominican Republic reportedly suffered flooding from Hurricane Maria in late September, growers said little of that country’s product was destined for the U.S.
Conditions have vastly improved this year in Peru, which endured extensive flooding a year ago as a result of the El Niño weather pattern, said Nicole Vitello, president of Oke USA Fruit Co., West Bridgewater, Mass., the importing arm of the Equal Exchange brand.
“It was really an extreme situation,” she said. “We had some quality problems.”
Fortunately, the plants survived and the company was able to meet its production needs by sourcing from growers in Ecuador.
Oke USA actually increased its East Coast business, Vitello said.
Miami-based Del Monte Fresh Produce offers bananas year-round, sourcing from Costa Rica, Ecuador, Guatemala, Nicaragua and Colombia and imports organic bananas from Mexico, said Dennis Christou, vice president of marketing.
“We do not foresee gaps in product availability during the fall,” he said in late September.
Despite importers’ optimism, Feighery of Procacci Bros. said a number of factors could cause shortages in the U.S., including weather in the Caribbean or in the tropics or changes in demand in other parts of the world.
“It really is a world market,” he said, and there typically are only three growing regions producing at any one time.
“If there’s a shortage in Europe, we feel it here,” he said.
Vitello of Oke USA said shipping costs also are on everyone’s minds.
Shipping costs have been very low during the past couple of years, she said, but there has been heavy consolidation in the shipping industry, and fewer choices means more control by the shipping companies and potential price increases.
The good news is that there has been ample container capacity, which tends to keep prices down, she said.