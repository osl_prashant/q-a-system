Chicago Mercantile Exchange live cattle futures finished in bullish territory for a third straight session on Thursday, helped by firmer wholesale beef values, said traders.Technical buying and futures' discounts to cash prices so far this week provided additional market support, they said.
August ended 0.700 cent per pound higher at 115.225 cents per pound, and above the 20-day moving average of 115.064 cents. October finished up 0.150 cent per pound at 114.825 cents.
Wholesale beef prices, or cutout, have traded mixed to higher since July 28, stabilizing a market that has slumped seasonally in recent weeks in the face of plentiful pork and chicken.
"A short-term bottom is in for the beef cutout. But I'm still concerned about increased supplies for September and October," said Allendale Inc chief strategist Rich Nelson.
So far this week, packers in the U.S. Plains paid $117 to $118 for a small number of market-ready or cash cattle. Those prices were nearly in line with last week's sales in the region.
Feedlots priced remaining cattle at $120 per cwt based on firmer wholesale beef prices and still profitable packer margins.
Buy stops and live cattle futures advances lifted CME feeder cattle for a fourth consecutive session.
August feeders ended up 0.400 cent per pound to 150.650 cents. 
Most Hog Contracts End Weaker
The CME's August lean hogs contract was bullishly undervalued based on the exchange's hog index for Aug. 1 at 87.35 cents, said traders.
But less money paid by packers for hogs, and retailers for pork, tempered buying in the August contract, traders said.
Ample seasonal supplies and increased numbers in the months ahead pulled down late 2017 and early 2018 trading months, they said.
"I'm looking for much lower cash prices by the time that supply period arrives," said Allendale's Nelson, referring to weaker deferred hog contracts.
August, which expires on Aug. 14, closed up 0.025 cent per pound to 81.975 cents. Most actively traded October ended 0.725 cent lower at 65.500 cents, and December down 0.100 cent to 60.525 cents.