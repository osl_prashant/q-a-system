Fresh produce matters a lot. But that's about the only constant to the changing appeal of supermarkets as consumers begin to reshape how their food retailer serves them.
 
A new report by Acosta, a marketing agency, again reaffirms the role of fresh produce in supermarkets and says supermarkets must try to keep pace with younger consumers who are coming to the store less and buying online more.
 
Called "The Revolution of Grocery Shopping,"  the study revealed:
Shoppers rank fresh produce (89%) as a more important feature than competitive pricing (86%) and product selection (84%) in their grocery store experience;
Nearly one-third of shoppers report their perception of a store skews negatively if it does not have a dedicated section for natural or organic options;
Almost half of shoppers consider leaving a store if fresh produce and healthy options are not available;.
Half of shoppers admit they decide what's for dinner within two hours of mealtime;
Millennials are doing the least amount of meal planning with 68% waiting until a few hours before dinner to make plans.
When buying prepared foods while grocery shopping, shoppers report making their selections based on variety (72%), if it is ready-to-eat (66%) and healthy options (62%); 
72% of Millennials enjoy grocery shopping versus just 60% of total U.S. shoppers;
Nearly half of Millennials - representing more than 10% of all U.S. shoppers - said they would use an app allowing them to pay for their groceries;
Nearly one-third of shoppers say they would use various forms of digital technology if it were offered at their grocery store;
Most desired offering shoppers would like to see is an app that provides the ability to order items not available in store and the ability to scan items as they shop in order to bypass checkout;
More than 40% of shoppers report buying grocery purchases online at least once a month;
While center store accounts for 70% of a store's profit, the perimeter area is expanding its share of space, driven by the increased interest in health and wellness as well as Millennials' influence;
62% of shoppers frequent the produce aisle and 61% visit the dairy section at least once a week, versus only 19% shopping in the Health Beauty Care aisles at least once a week;  and
Online shopping and blurred channel lines are impacting where consumers shop for pet food, paper products, over-the-counter drugs, vitamins and beauty/skin care, further contributing to the erosion of center store.
 
Even though supermarkets have "stayed relatively the same," the study says, that won't last much longer. It's a new tipping point and time to adapt or die to new megatrends, according to the authors. Raising the prospect of robotic stockers, indoor mapping apps, price comparison tools, shelf sensors and digital coupons, the report's conclusions portend a brave new world for those supermarkets who dare to embrace the future.
 
But there's a risk. Baby boomers are be the most reliable supermarket shoppers - and we don't like change. So change everything, just don't move the Greek yogurt.