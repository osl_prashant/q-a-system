Produce for Kids, founded in 2002, continued to grow its outreach in 2017.The cause marketing organization, founded by onion supplier Shuman Produce Inc., will maintain some of its main programs like Power Your Lunchbox Pledge while adding new retailer campaigns.
Trish James, vice president of Produce for Kids, said that they take advantage of consumer smart phone and social media use to encourage families to buy produce and cook healthier food.
“We realize shoppers utilize their smart phones in making purchase decisions. This is why all of our point-of-sale signage drives shoppers back to campaign-specific web pages that offer produce supplier branded recipes, coupons and additional information on the campaign.”
 

New programs

Produce for Kids’ retailer programs include a yearlong campaign with Associated Wholesale Grocer’s Springfield division with signs featured in the produce department directing shoppers to digital promotions.
“Our in-store campaigns are based on a 360-marketing strategy that crosses multiple touchpoints, generating incremental sales and brand awareness for our partners,” James said.
Educational and consumer outreach programming will continue to focus on targeting produce buyers both through registered dietitians via the We Heart RD initiative and directly with produce information online, she said.
In March, Produce for Kids launched a Produce Tips section of their website, an inclusive listing of produce categories with tips on choosing, storing and cooking commodities.
 

Lunchbox pledge

The Power Your Lunchbox program expanded this year beyond the back-to-school season for the first time after three years.
Produce for Kids partners with produce companies to combine industrywide marketing and charitable donations.The winter campaign’s 21,394 consumer or classroom pledges to eat and pack healthier lunches have been matched with a collective $1 donation per pledge from, to date:
Avocados from Mexico — Mexican hass avocados;
Bee Sweet Citrus;
Crispy Fruit Freeze-Dried Fruit Snacks from Crispy Green;
Eat Smart fresh-cut vegetables;
Earthbound Farm baby kale;
Marie’s dressings;
Natalie’s Orchid Island Juice Co.;
Pero Family Farms mini sweet peppers and snipped green beans; and
Wish Farms strawberries.
Since 2002, Produce for Kids has raised more than $6 million for programs that benefit families and children, James said.