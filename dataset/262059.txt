Organic group opposes dismissal of animal welfare rules
Organic group opposes dismissal of animal welfare rules

The Associated Press

AUGUSTA, Maine




AUGUSTA, Maine (AP) — A group that advocates for Maine's organic farmers says it opposes a federal move to get rid of organic animal welfare rules.
Maine Organic Farmers and Gardeners Association said late Tuesday the U.S. Department of Agriculture's decision to stop using a set of rules called federal Organic Livestock and Poultry Practices is misguided.
The USDA announced Monday that it has withdrawn the rules, which were published a little more than a year ago. The agency says the rule would have increased federal regulations in organic animal agriculture.
MOFGA says the rules were meant to "create stronger, more specific language ensuring consistent animal welfare standards for certified organic animals." It says the rules were a good idea because they would've ensured appropriate space, diets and access to the outdoors for animals.