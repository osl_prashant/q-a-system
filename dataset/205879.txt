UAN32% led gains along with urea and DAP.
Fertilizer prices were generally lower with UAN28% leading to the downside. Our Nutrient Composite Index softened 0.23 points to 498.06.

Nitrogen

Anhydrous ammonia was once again our greatest decliner in the nitrogen segment, and recaptured the position of our least expensive form of nitrogen.
After firming $8.53 this week, UAN32% is priced very close to parity with urea on an indexed basis.
Technically, the last time we saw UAN32% priced at the bottom of our nitrogen segment, it presaged strength in all forms of nitrogen with urea leading the way higher.
While we have not issued purchase advice for nitrogen just yet, we are very aware that there are some bargain prices floating around corn country right now. Do yourself a favor and compare your local bids for fall or spring nitrogen to the prices listed in your crop district on our map and be prepared to book accordingly.

Phosphate

Phosphates were mixed with MAP falling as DAP firmed.
Our DAP/MAP spread is in-line with expectations at $20.46 this week.
Both DAP and MAP remain priced at the top of our indexed fertilizer prices, but are still priced below expected new-crop revenue based on Dec. 2018 corn futures.
Industry watchers believe the fourth quarter of 2017 will hold price pressure for phosphates. Unless your preferred retailer is offering a drastically lower price than that listed for your crop district on the Inputs Monitor, hold off on booking phosphate for now as we await a better price opportunity later in the year.

Potash 

Potash prices are still well above anhydrous ammonia on an indexed basis, but remain below phosphate prices and expected new-crop corn revenue.
Industry watchers now believe downside potential is growing in potash, leading us to hold out for lower prices.
Stay hand-to-mouth on potash for fall and spring K applications.

PERSPECTIVE -- Your Editor was consumed by duties related to last week's Midwest Crop Tour and by participation this week in the Farm Progress Show in Decatur, Illinois. Allow me to briefly sum up our current line of thought on fertilizer prices. We feel no urgency currently to book any of the fertilizers in our survey despite the element of seasonal price risk that often accompanies the harvest season.
Having said that, I want to reiterate what I said above about bargain prices, especially on nitrogen, scattered across the Midwest. If you are looking at a bid that is as low as you have seen in awhile, do not hesitate to take advantage. The Inputs Monitor does a great job of reporting average baseline prices in the 12-state area, but bulk buyers and preferred customers' prices are not reflected in what is reported to us on a weekly basis. That means, some deals will go under our radar.
It is prudent to call your preferred retailer right away and find out what sort of deal you can lock in right now. Some farmers in Iowa have found NH3 for less than $350 per short ton, and my advice for a bid like that is simple... book it all day long, be it for fall application, or for spring 2018. Weigh the cost of storage for spring applied nutrient as you run the numbers, but any bid below $400 per short ton (including storage costs) is worth booking.
I do not expect a dramatic recovery in nitrogen prices, although harvest demand will likely support a short-term bump in prices. One we get to mid-December, that hump will likely reverse itself, and prices may begin to sag once again during the winter. But nitrogen prices, in particular, are as low as we have seen them for the life of the Monitor and while we do not want to chase downtrending prices lower, odds are nitrogen will bottom somewhere very near where we are today.
So check your local bids on all of the fertilizers you expect to apply for the upcoming crop and if you find a deal you cannot refuse, I would advise you take advantage immediately.
Corn Futures 

December 2018 corn futures closed Friday, August 25 at $3.93 putting expected new-crop revenue (eNCR) at $623.39 per acre -- lower $15.24 per acre on the week.
With our Nutrient Composite Index (NCI) at 498.06 this week, the eNCR/NCI spread narrowed 15.01 points and now stands at -125.33. This means one acre of expected new-crop revenue is priced at a $125.33 premium to our Nutrient Composite Index.





Fertilizer


8/14/17


8/21/17


Week-over Change


Current Week

Fertilizer



Anhydrous


$470.37


$462.79


-$3.55

$459.25
Anhydrous



DAP


$439.30


$438.48


+$1.01

$439.49
DAP



MAP


$460.13


$459.95


-92 cents

$459.03
MAP



Potash


$332.57


$335.62



+$3.32


$332.29
Potash



UAN28


$237.03


$230.32


-$5.84

$224.49
UAN28



UAN32


$250.38


$243.93


+$8.53

$252.46
UAN32



Urea


$324.02


$316.62


+$1.90

$318.52
Urea



Composite



504.19



498.29


-0.23

498.06
Composite