The Farm Journal Midwest Crop Tour isn’t all clear skies, sunshine and 240 bushel corn. In fact, today, on the Eastern leg of the tour, scouts were met with severe weather and extreme variability in the fields.
“Boots are pretty muddy here,” Ted Seifried told AgriTalk host Mike Adams on Tuesday. “We actually did see some severe weather, but it must have come through a few hours before we did. A couple of corn fields got flattened.”
According to Seifried, severe weather isn’t the only thing plaguing corn and soybean fields this year.
He says corn variability in the field is “really off the charts.”
“We had a 120 followed by a 238 and they were 10 miles apart from each other,” he said.
In addition, low ear count has been a common theme.
“A lot of ear counts in the 70s, some in the 80s and a few in the 90s,” he said.
According to Seifried, achieving the USDA August Crop Production report estimate of 169.5 bushels to the acre national average will be difficult.
“[Variability] is a word that Brian [Grete] was saying last night he didn’t want to use a lot, but it’s a word that we’ve had to use a lot,” Seifried explained. “To think that we have national average 170 corn yield with as much variability as we’re seeing is kind of hard to fathom.  Is it possible? Sure. But it’s a tough way to get there.”

Soybean maturity struck Seifried as a potential issue given we’re reaching the end of August.
“Beans are not doing quite as well as what I thought,” he says. “I’ve seen a lot of immature soybeans that are going to have a hard time getting mature unless we have a really nice Indian summer.”
While scouts report they haven’t seen much disease on the tour, Seifried thinks there’s still quite a bit of risk for the soybean crop.