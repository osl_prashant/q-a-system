Crop calls
Corn: 1 to 3 cents higher
Soybeans: 5 to 8 cents higher
Wheat: 4 to 8 cents higher
Corn and soybean futures firmed overnight in reaction to the disappointing rain event, as precip wasn't as widespread and generous as previously forecast, especially across Iowa. Northwest Iowa missed out on yesterday's rain and totals in south-central Iowa -- where severe drought has been introduced on the Drought Monitor -- were disappointing. Also this morning, USDA announced an unknown buyer has purchased 198,000 MT of soybeans for 2016-17 and 66,000 MT for 2017-18. Wheat futures were lifted overnight by results from the second day of the Wheat Quality Council's spring wheat tour. Scouts measured an average yield of 35.8 bu. per acre, which was down from the five-year average of 46.6 bu. per acre.
 
Livestock calls
Cattle: Mixed
Hogs: Firmer
Cattle futures are expected to see a choppy start after cash cattle trade began in Kansas and Nebraska at $117, which is steady to $2 lower last week. Futures already hold a discount to the cash market, which should limit pressure. Meanwhile, hog futures are expected to benefit from followthrough from yesterday's gains, with traders also focused on narrowing the discount futures hold to cash. However, pork cutout values have softened this week, which makes traders comfortable with the discount.