<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->



KANSAS CITY - With about 170 buyers in attendance and improved traffic over the 2015 show in Minneapolis, The Packer's Midwest Produce Expo garnered momentum for a return trip to Kansas City in 2017.
 
The 2016 expo, featuring keynote speaker Bill Self and an opening reception at the College Basketball Experience downtown Aug. 15, gave The Packer the opportunity to showcase the city in its own backyard, said Shannon Shuman, publisher of The Packer.
 
"We have been excited for the entire year for the show to come to Kansas City," Shuman said. "It is our hometown, and, being the fifth year of the show, it is very fitting for us to have a celebration of five years here in our city and be able to celebrate with everyone else," he said. Strong support from local and regional retailers and wholesalers helped make the show a success, he said.
 
Shuman said Self's appearance was very well received.
 
"He is kind of a heroic figure around these parts, an inspirational speaker and obviously very successful in what he does," Shuman said. "Sharing some of that with the produce industry was a real treat."
 
Kansas City's central and accessible location for buyers favored it as the choice for the 2017 show, he said.
 
Matthew Little, a salesman for Huron Produce, Frederick, Colo., said traffic at the company's booth was steady. "Our greenhouse is in O'Neill, Neb., locally here and a lot of folks we are doing business here for Midwest grown produce," he said. The company shared news of a recently opened facility in Edinburg, Texas.
 
"It was a great venue and we are having a lot of good meetings, and you get good interaction from people who stop by," he said.
 
Tab Freeland, produce specialist with Affiliated Foods Midwest, Norfolk, Neb., said that he enjoyed the knowledge gained on the show floor.
 
"This is my first show and I'm really enjoying it very much," Freeland said.
 
The show provided the opportunity to speak to a few key buyers about upcoming deals and varieties, said John Pandol, director of special projects for Delano, Calif.-based Pandol Bros.
 
"It is a good day in the Midwest today," he said Aug. 16.
 
The date for the 2017 event hasn't been finalized.