Albert Pieri, 90, a key leader from one of the founding families of Ocean Mist Farms, has died.
 
Pieri, an employee of the company for nearly 50 years, was a leader in the early days of Castro, Calif.-based California Artichoke and Vegetable Growers - renamed Ocean Mist Farms in 1995, according to a news release.
 
Daniel Pieri and cousins Amerigo and Angelo Del Chiaros launched the California Artichoke & Vegetable Growers Corporation in 1924; Albert Pieri was a second generation leader of the company.
 
Known as "Big Al" for his size and strength of personality, Pieri was honored with the UnitedAg Lifetime Achievement Award in 2014, according to the release. 
 
"Al's contributions to our organization, the agricultural industry, and his community will be remembered and appreciated by all for many years to come," Ocean Mist CEO Joe Pezzini said in the release. 
 
Pieri learned the business as a young man, starting at California Artichoke and Vegetable Growers by loading and icing rail cars, driving trucks and packing broccoli, according to the release. By 1954, Pieri was sales manager, adding to the company's commodity portfolio. His commitment to quality and his customers helped direct the company to invest in cutting-edge hydro and vacuum coolers, forced-air storage and other technology, according to the release.
 
A graveside memorial service is planned for May 6 at St. Joseph's Catholic Cemetery in Woodland, Calif. In lieu of sending flowers, the family requests a donation to the Ocean Mist Farms Academic Scholarship Program.