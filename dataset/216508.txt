As 2017 comes to a close producers around the country are looking to 2018 milk prices. Will prices improve in the New Year? A massive national herd, burgeoning supplies, declining domestic demand and a strong dollar should indicate prices might dip lower before they improve.
Supply outpacing demand
“We’re at an incredible crossroads in the dairy industry. Today we have as many cows as we did in 1996, shortly after I got into this business,” says Mike North of Commodity Risk Management Group. “There is a growing supply of products pretty much across the board.”
According to North we are heading into a time of year when consumption generally declines, but processors continue making product adding to the already growing stocks of dairy products.
“We’re making product and adding to inventory, and so there’s this massive collision of increased supply,” he explains.
According to Scott Brown from the University of Missouri, it’s not just the supply situation in the U.S. that is causing the problem, but milk supplies around the globe continue to increase.
“We have a lot of stocks overhanging markets globally,” he explains. “It’s hard to find that demand side that is going to pull us now domestically.”
Increasing domestic consumption
Demand for fluid milk continues to decline despite new innovations like Fairlife’s ultra-filtered milk. Still, domestic demand should be part of the strategy to eat away at supply. Michael Dykes of the International Dairy Foods Association says he and Jim Mulhern of the National Milk Producers Federation are working to find creative ways to increase consumption in America.
“One of the things we’re looking at is can we do something with the current SNAP program to incentivize consumption of dairy products,” Dykes says. “We just had a new bill introduced last week on school milk with Congressman G.T. Thompson and Congressman Crowley.”
Dykes admits there won’t be a huge turnaround in the way Americans consume dairy products, but says there are incremental changes that can be made in conjunction with new and innovative products he thinks can make a difference. In addition, Americans are returning to full-fat diets. Brown says that will help the price outlook.
“I think we have seen some very strong demand for dairy fat,” he says. “I don’t care if you look at ice cream, butter or whole milk, you name the product, we like it with more dairy fat in it.”
As we look to 2018, he argues fat demand has the potential to buffer U.S. producers from what otherwise would be “a much tougher price situation.”
Global markets 
Exports continue to play a big role in the U.S. milk market. According to Secretary Tom Vilsack, CEO of the U.S. Dairy Export Council (USDEC), since 2003 nearly half of all new milk that has been produced by U.S. farmers has gone into the export market.
“Without that we would have had a serious problem in dairy,” he says. According to Vilsack, USDEC is fixated on creating more export opportunities for U.S. dairy.
“I think it’s fair to say the chances are very good we won’t be able, just with domestic consumption alone, to deal with the increased production,” he says. “So we’re going to have to increase exports.”
Shifting diets toward increased protein consumption could mean a boost in sales for U.S. dairy products, Dykes says.
“USDA projects we’re going to have more milk coming at us, but we’re also going to have another 2 billion people on the planet that we’re going to feed as well,” he explains. “So it’s going to require more of us as global growth [and] economic growth occurs.”
Increasing exports will be tough with a strong dollar, but fortunately the dollar is softening some.
“Our dollar has weakened, so we’re hopeful that opens some doors as we head into 2018,” North says. “Will China be the rescuer of U.S. inventory and come in and scoop up a whole lot of product this January when they get into their new calendar year, their new fiscal year? Perhaps.”
The market mindset 
“The old saying is known fundamentals are useless fundamentals,” says Bryan Doherty of Stewart-Peterson.
What he means is the market is aware of all the issues, which could lead one to believe those factors are already priced into the market.
“Markets move on perception, they move on momentum, they move on attitude,” Doherty explains. “Right now the perception is ‘Hey there’s a lot of inventory, so I as an end user, I don’t have to jump. There’s no urgency, there’s more of it coming.’”
He says until the mindset of the market shifts, it’s unlikely for prices to move higher.
Could prices move lower?
“I think you need to be defensive on milk prices,” Doherty says. “How much lower can they go? If you’ve been around a while they can go lower. Never try to guess what the market can do.”
According to North, the market is in a similar place as it was heading into 2016.
“At that point in time, we were showing a lot of $16 to $17 prices [this time of year],” he explains. “You now have to admit that 2018 futures are only showing us $15.25, $15.50. You know the back end of the year is at $16, but we’re not getting the picture that those are going to go higher. We could go lower.”
Producers around the country are trying to out produce the current price Doherty says. It’s a prime example of how high prices cure high prices and low prices cure low prices, he says.
“I’m not sure we’ve gotten low enough,” he says. “We should be prepared for lower prices before higher prices.”
 
Note: This story appears in the December 2017 magazine issue of Dairy Herd Management.