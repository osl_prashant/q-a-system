Sponsored Content
 
Some damage to alfalfa, wheat and grass fields is expected due to the bitter-cold temperatures much of the Midwest experienced in early January.
Snow Insulates
“Snow is a good thing for perennial crops,” says Dan Undersander, Extension forage agronomist at the University of Wisconsin. “Our data shows that when we have temperatures like we had in January and when we have 6 to 8" of snow, the soil temperature at a 4" depth was still near or, in some cases, even above freezing. So, the snow can easily insulate soil to 40°F air temperature.” 

Dan Undersander, University of Wisconsin   
Even during the coldest years when fields have good snow cover, there is seldom damage to over-wintering crops. Snow is a tremendous insulator, he says. 
Cold Kills
“When the crown area of the plant gets down to about 13°F, it will oftentimes kill alfalfa, but air temperature must remain cold for at least three to five days for the soil temperature to get that low,” Undersander says. “A single day or two of cold weather, even without snow cover, and the soil will insulate the plant enough to protect it, but as the temperature stays cold longer, then the soil is unable to insulate and cold can kill the plant.”
Despite the extended cold snap in early January that hit a lot of the U.S., much of the Midwest had snow cover at the time. Areas without snow cover that could see damage include North and South Dakota, Central Minnesota, and parts of Wisconsin and Southern Minnesota. 
In terms of damage, Undersander says it depends partly on the condition of the field at the end of the last season. Some fields could have large kill spots, while stands that are older and/or maybe didn’t have the potassium fertilization last fall will suffer more severely. However, many fields may only experience general thinning of the stand. 
“Farmers should watch in the spring to see if at least 50 stems per square foot are coming up. If not, then it would be appropriate to consider killing that stand and replanting new,” he says. 
Standing Water Suffocates
Standing water over winter is not likely to kill by itself. However, if it stays in an area long enough, it can reduce the oxygen levels in the soil and cause a suffocation of the plants the same as ice sheeting.
“If you aren’t familiar with ice sheeting, we can have a solid sheet of ice over soil, and roots are still respiring or they’re using energy in those roots to keep themselves alive,” Undersander explains. “To respire, they need oxygen. If we have ice over a field for more than three weeks, the ice will suffocate the plants underneath by reducing the oxygen that is able to penetrate from the air into the soil for the roots to use. Essentially, perennial plants still need access to oxygen over the winter.”
Areas of flooding in the eastern U.S. have raised erosion concerns. This flooding can also result in a buildup of ash on fields. 
“Flooded pastures and hayfields will likely have elevated levels of ash, which can reduce the quality of the forage that’s being fed to animals,” says Undersander. “Higher ash levels mean less energy in the forage, so producers should take that into account with their ration.”
 
Sponsored by Lallemand Animal Nutrition