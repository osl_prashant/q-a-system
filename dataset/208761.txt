We’ve all dealt with “plumbing problems” at some point in our lives.A bathroom sink that doesn’t drain properly, hot water pipes that rattle every time the pressure drops, or worst of all, the classic scene that’s fortunately mostly one depicted in movies, where the sewage line backs up into a brown, bubbling mess in someone’s backyard.
The common denominator in all those unpleasant scenarios is the presence of some material that restricts the normal flow through the pipes. It is similar to the condition in human health known as atherosclerosis of blood vessels.
The difference is that clogged household pipes can severely damage one’s checking account; clogged arteries can destroy one’s health and well-being.
According to standard medical textbooks, the clinical definition of clogged arteries is called “arteriosclerosis,” a thickening, hardening and loss of elasticity of the arterial walls that gradually restricts the blood flow to organs and tissues. One specific form of arteriosclerosis is called “atherosclerosis,” which is caused by the buildup of plaques formed by an accumulation of white blood cells crystallized cholesterol and triglycerides.
That’s an over-simplification of a complex clinical syndrome, of course, but the fact that the arterial plaques are known colloquially as “fatty streaks,” because they resemble marbling in beef (allegedly), aligns nicely with the longstanding theory that by consuming foods with cholesterol — only found in animal foods, let’s stipulate — people end up with cholesterol-filled plaques in their arteries that lead to serious plumbing problems, like heart attacks and strokes.
Refuting a Biased TheoryNow, however, a comprehensive review of medical literature disputes the theory that foods with saturated fat and cholesterol lead inevitably to clogged arteries and an eventual demise from coronary heart disease.
Here’s the lead, as reported in the British Journal of Sports Medicine: “Saturated fat does not clog the arteries: coronary heart disease is a chronic inflammatory condition, the risk of which can be effectively reduced from healthy lifestyle interventions.”
But wait, there’s more.
“Coronary artery disease pathogenesis and treatment urgently requires a paradigm shift,” article stated. “Despite popular belief among doctors and the public, the conceptual model of dietary saturated fat clogging a pipe is just plain wrong.”
It doesn’t get any more explicit than that.
The BJSM report, which was based on what researchers call a “meta-analysis” of the literature — and by literature, we’re talking about such peer-reviewed journals as Nature, the British Medical Journal, and the American Journal of Clinical Nutrition—concluded that there is “no association between saturated fat consumption” and the following medical conditions:
·         Stroke
·         Type 2 diabetes (in healthy adults)
·         Coronary heart disease
·         Mortality from coronary heart disease
·         All causes of mortality
That about covers the waterfront.
On the basis of the data they reviewed, the researchers were able to state that there is no benefit from reducing the intake of dietary fat, including saturated fat, on myocardial infarction, cardiovascular disease, or in fact any other causes of mortality.
Need further evidence? How about this?
“It is instructive to note that in an angiographic study of postmenopausal women with CHD [coronary heart disease], greater intake of saturated fat was associated with less progression of atherosclerosis whereas carbohydrate and polyunsaturated fat intake were associated with greater progression.”
In other words, eating animal foods with saturated fat wasn’t just detrimental, it was an improvement over diets based on substituting carbs and vegetable oils for meat and dairy.
It’s kind of gross, but the pathogenesis, as medical texts love to reference, of atherosclerosis is similar to that of a pimple. The obstruction of arteries from the presence of plaques isn’t the problem; arterial walls are quite elastic unlike those household pipes.
Cardiac events, like a heart attack or stroke, occur when the “pimple” bursts, another event with which we’re all familiar.
As the researchers stated, “When plaques rupture, coronary thrombosis and myocardial infarction can occur within minutes. The limitation of the current plumbing approach (‘unclogging a pipe’) to the management of coronary disease is revealed by a series of randomized controlled trials, which prove that stenting significantly obstructive stable lesions fail to prevent myocardial infarction or to reduce mortality.”
Unclogging the pipes works wonders for a sink that doesn’t drain quickly.
It’s next to useless in human medicine.
As is the conventional “wisdom” that eating foods containing saturated fat and cholesterol inevitably lead to coronary heart disease.
Time we let go of both the intervention and the theory behind it.
The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.