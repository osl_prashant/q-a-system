Calf and yearling markets continued to rally during January, adding 5% onto gains of 7% to 10% recorded in December. A mid-January storm across the Northern Plains canceled some sales and severely hampered auction receipts. Buyers were aggressive after the storm filling orders from feedlots and grazing customers. Optimism was fueled by gains in the fed cattle market.
<!--
if("undefined"==typeof window.datawrapper)window.datawrapper={};window.datawrapper["vbZg8"]={},window.datawrapper["vbZg8"].embedDeltas={"100":701,"200":701,"300":701,"400":701,"500":701,"600":701,"700":701,"800":701,"900":701,"1000":701},window.datawrapper["vbZg8"].iframe=document.getElementById("datawrapper-chart-vbZg8"),window.datawrapper["vbZg8"].iframe.style.height=window.datawrapper["vbZg8"].embedDeltas[Math.min(1e3,Math.max(100*Math.floor(window.datawrapper["vbZg8"].iframe.offsetWidth/100),100))]+"px",window.addEventListener("message",function(a){if("undefined"!=typeof a.data["datawrapper-height"])for(var b in a.data["datawrapper-height"])if("vbZg8"==b)window.datawrapper["vbZg8"].iframe.style.height=a.data["datawrapper-height"][b]+"px"});
//-->



<!--
if("undefined"==typeof window.datawrapper)window.datawrapper={};window.datawrapper["Aaan4"]={},window.datawrapper["Aaan4"].embedDeltas={"100":686,"200":686,"300":686,"400":686,"500":686,"600":686,"700":686,"800":686,"900":686,"1000":686},window.datawrapper["Aaan4"].iframe=document.getElementById("datawrapper-chart-Aaan4"),window.datawrapper["Aaan4"].iframe.style.height=window.datawrapper["Aaan4"].embedDeltas[Math.min(1e3,Math.max(100*Math.floor(window.datawrapper["Aaan4"].iframe.offsetWidth/100),100))]+"px",window.addEventListener("message",function(a){if("undefined"!=typeof a.data["datawrapper-height"])for(var b in a.data["datawrapper-height"])if("Aaan4"==b)window.datawrapper["Aaan4"].iframe.style.height=a.data["datawrapper-height"][b]+"px"});
//-->


<!--
if("undefined"==typeof window.datawrapper)window.datawrapper={};window.datawrapper["BuZY7"]={},window.datawrapper["BuZY7"].embedDeltas={"100":701,"200":701,"300":701,"400":701,"500":701,"600":701,"700":701,"800":701,"900":701,"1000":701},window.datawrapper["BuZY7"].iframe=document.getElementById("datawrapper-chart-BuZY7"),window.datawrapper["BuZY7"].iframe.style.height=window.datawrapper["BuZY7"].embedDeltas[Math.min(1e3,Math.max(100*Math.floor(window.datawrapper["BuZY7"].iframe.offsetWidth/100),100))]+"px",window.addEventListener("message",function(a){if("undefined"!=typeof a.data["datawrapper-height"])for(var b in a.data["datawrapper-height"])if("BuZY7"==b)window.datawrapper["BuZY7"].iframe.style.height=a.data["datawrapper-height"][b]+"px"});
//-->


<!--
if("undefined"==typeof window.datawrapper)window.datawrapper={};window.datawrapper["jwGuJ"]={},window.datawrapper["jwGuJ"].embedDeltas={"100":701,"200":701,"300":701,"400":701,"500":701,"600":701,"700":701,"800":701,"900":701,"1000":701},window.datawrapper["jwGuJ"].iframe=document.getElementById("datawrapper-chart-jwGuJ"),window.datawrapper["jwGuJ"].iframe.style.height=window.datawrapper["jwGuJ"].embedDeltas[Math.min(1e3,Math.max(100*Math.floor(window.datawrapper["jwGuJ"].iframe.offsetWidth/100),100))]+"px",window.addEventListener("message",function(a){if("undefined"!=typeof a.data["datawrapper-height"])for(var b in a.data["datawrapper-height"])if("jwGuJ"==b)window.datawrapper["jwGuJ"].iframe.style.height=a.data["datawrapper-height"][b]+"px"});
//-->

<!--
if("undefined"==typeof window.datawrapper)window.datawrapper={};window.datawrapper["4hQRe"]={},window.datawrapper["4hQRe"].embedDeltas={"100":701,"200":701,"300":701,"400":701,"500":701,"600":701,"700":701,"800":701,"900":701,"1000":701},window.datawrapper["4hQRe"].iframe=document.getElementById("datawrapper-chart-4hQRe"),window.datawrapper["4hQRe"].iframe.style.height=window.datawrapper["4hQRe"].embedDeltas[Math.min(1e3,Math.max(100*Math.floor(window.datawrapper["4hQRe"].iframe.offsetWidth/100),100))]+"px",window.addEventListener("message",function(a){if("undefined"!=typeof a.data["datawrapper-height"])for(var b in a.data["datawrapper-height"])if("4hQRe"==b)window.datawrapper["4hQRe"].iframe.style.height=a.data["datawrapper-height"][b]+"px"});
//-->

Note: This story appears in the February 2017 issue of Drovers.