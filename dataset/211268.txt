Is there a single artifact of modern society more demonized than fast food?Not counting “fake news,” of course.
Nutritionists decry our love affair with burgers and fries, judging them to be too fat, too salty, too calorie-laden for anything other than occasional (meaning, almost never) consumption by consumers.
Consumer groups condemn both the ingredients and the system of production that makes it possible for burger chains to offer “cheap” (and unhealthy) food to the masses, although these days, the notion of an inexpensive fast food meal is something of an endangered species.
Animal activists, of course, hate the fact that millions of people on any given day are wolfing down hamburgers, cheeseburgers, double and triple burgers, bacon burgers, bacon cheeseburgers, bacon double cheese burgers, jalapeno double cheese burgers, Whoppers, Big Macs and Grand Macs.
There are even environmental advocates who specialize in attacking fast-food operators, bashing on the existence of drive-thrus, the amount of food waste and the (allegedly) excessive leading chains’ consumption of energy and resources. This incluces one ironically titled group from the ’90s called WARPED (Warriors Against Ridiculous Packaging and Environmental Destruction), whose members would collect fast-food containers, wrappers and cups that ended up as litter, then drag a big plastic bag of the stuff into a local outlet and beat a hasty retreat.
 The fast-food industry is truly an equal opportunity offender as far as many advocacy types are concerned.
But maybe they’ve been picking on the wrong culprits on the menu board.
A Cascade of Effects
According to a new study, although it’s hardly new information, consuming a sugar-sweetened beverage with a high-protein meal creates a plethora of negative effects, including disrupted energy balance, altered food preferences and accelerated conversion of simple carbohydrates into adipose tissue.
In other words, you end up consuming excess calories, developing a carving for sugar and packing on extra body fat you don’t need.
The study, titled, “Postprandial energy metabolism and substrate oxidation in response to the inclusion of a sugar- or non-nutritive sweetened beverage with meals differing in protein content,” and published in the journal BMC Nutrition, was conducted by Dr. Shanon Casperson from USDA’s Agricultural Research Service-Grand Forks Human Nutrition Research Center.
“We found that about a third of the additional calories provided by the sugar-sweetened drinks were not expended, fat metabolism was reduced, and it took less energy to metabolize the meals, Casperson said in a report published on the Medical Xpress website. “This decreased metabolic efficiency may ‘prime the body’ to store more fat.”
Ya think?
Casperson and the research team found that drinking a sugar-sweetened beverage increased the amount of energy used to metabolize the meal, and the increased expenditure did not even out the consumption of additional calories from the drink.
“We were surprised by the impact that the sugar-sweetened drinks had on metabolism when they were paired with higher-protein meals,” Casperson said.
I’m not.
Sucking down soda is the single worst dietary habit in existence. It most certainly affects caloric balance, distorts taste sensitivity and apparently has a surprising effect on metabolism.
Not to mention that if you’re a fast-food hater, you really should be taking your ire out on the soft drinks, not the sandwiches.
Because also not surprising: That’s where they make their money. Most of the profit in running a quick-service restaurant is selling sucker-- uh, I mean patrons – those giant cups of sugar-sweetened beverages. The old adage about the cups costing more than the drinks is still true, so filling up on soft drinks not only packs unwanted pounds on the customer, it puts unnecessary profits into the pockets of the franchise owners.
What’s worse, Casperson noted that the study was conducted among “healthy weight adults” only.
If you’re overweight, your results may differ.
Editor’s Note: The opinions in this commentary are those of Dan Murphy a veteran journalist and commentator.