House out, Senate in | Lighthizer nomination vote in Senate | Key USDA reports on Wednesday






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






The House is out but the Senate is in during a relatively quiet week ahead for Washington. 
The Senate will hold confirmation hearings on several sub-Cabinet posts. Senate panels will also look into potential Russian interference with the US elections, maritime transportation issues and the housing finance system a decade after the housing sector meltdown that led to the financial crisis.
The Trump administration will probably still make trade policy headlines as officials have noted there will be new trade announcements nearly every week ahead. There could be some additional names surface for posts at USDA now that Sonny Perdue is getting up to speed in his office.
The Senate is expected to vote on the nomination of Robert Lighthizer to be the next US Trade Representative (USTR). That could also result in the administration finally delivering the notice to Congress that they intend to renegotiate NAFTA.
Inflation and consumer updates will be attention-getters ahead, with other updates also on tap. Today opens with the Labor Market Conditions Index followed by Wholesale Trade on Tuesday. Updates on Wednesday include Import and Export Prices, Atlanta Fed Business Inflation Expectations and Treasury Budget statement, followed by Weekly Jobless Claims and PPI-FD on Thursday. The week closes out with several notable releases on Friday – Consumer Price Index, Retail Sales, Business Inventories and Consumer Sentiment.
The main focus will be on whether the updates from April will paint a more-hopeful picture on the US economy ahead after a disappointing opening quarter to the year.
On the global side, China starts releasing some of its barrage of monthly economic updates, but the attention will be squarely on the US data. And markets will reflect the results of French presidential elections which saw the centrist candidate win.
Markets will have a chance to hear from several more Fed officials ahead, looking for their assessment of the economy and what their thoughts on what and when relative to adjusting the $4.5 trillion balance sheet held by the Fed. First up today will be St Louis Fed's Bullard (2019 voter) and Cleveland Fed's Mester (2018 voter) with appearances Tuesday by Boston Fed's Rosengren (2019 voter) and Dallas Fed's Kaplan (current voter). Rosengren is on the schedule again Wednesday with New York Fed's Dudley scheduled to deliver remarks Thursday. His comments often carry extra weight since the New York Fed President is always a voter. The week closes out Friday with two current voters – Chicago Fed's Evans and Philadelphia Fed's Harker. How they assess the jobs data and any other views they offer from the FOMC meeting will also be potential market factors.
Weather still is important but USDA data will probably take center stage. USDA will issue some fresh updates on Wednesday via the Crop Production report which will have the first 2017 winter wheat production estimate. That could be faded a bit by markets since it may not include much of the impact of the April 29-30 snow and cold event in Plains HRW wheat areas. The monthly Supply/Demand report will also be notable as it will include the first official 2017/18 balance sheets for US and world crops. Carryover levels remain the biggest focus as the production figures on corn and soybeans in particular will not be based on survey results. The Weekly Crop Progress update today will be expected to show condition ratings for winter wheat will have declined given the unfavorable weather conditions from the final weekend in April.
On the trade side, today’s Grain Inspections report will be eyed for whether it shows another week of more than one million tonnes of corn inspected to be sent to foreign buyers. And the Thursday update on sales of US corn, soybeans and wheat to those foreign buyers will also be key. Weather, however, still is key since traders will want to see if rains this past week in the eastern Belt were offset by open weather in the western Belt.


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.