Prolonged dry weather conditions, low humidity and high winds means farmers and ranchers face another year of precarious risk of wildfires in the Plains.
In the past two days, nearly 50 wildfires have been reported across the state, burning 25,000 acres, Kansas officials told The Witchia Eagle. Of the reports, 21 were put out; 10 are in final clean-up and 16 are reported contained.
Monday, a wildfire burned 1,500 to 2,000 acres in Clark County, Kan., one day shy of the one-year anniversary of the massive Starbuck wildfire.
Tuesday, a fire Ellis County led to evacuation of farms near Hays, Kan., north of Catharine about 5 miles.
 

This is the fire NE of Hays or north of Catharine about 5 miles. Fire departments are set up north of Catharine to try to keep it from getting into town but if you live in or around Catharine, be prepared!

KHP is going from farm to farm evacuating residents in its path. ?? pic.twitter.com/AY1fNeeeeu
— Trooper Tod (@TrooperTodKHP) March 6, 2018

Kansas Highway Patrol Trooper Tod Hileman helped block traffic as ranchers herded cattle out of the wildfire’s path.
 

I’m blocking traffic so these ranchers can get their very frightened cattle to safety! Poor things ? pic.twitter.com/Iu7CuNTSNx
— Trooper Tod (@TrooperTodKHP) March 6, 2018

 
Four UH-60 Black Hawk helicopters and about 16 soldiers from the Kansas National Guard were deployed to support the control efforts, reports The Witchia Eagle.
Traffic on I-70 was rerouted between MP 157 and 163. This was only one of several road closures area, as smoke blanketed the area, making travel hazardous.
 

Traffic on I-70 being rerouted between mp 157 and 163 or U-183 bypass and Toulon Ave (the circles) then take Old Hwy 40 around the south end of Hays. The fire is approximately where the square is.

There's heavy traffic so GO SLOW! pic.twitter.com/Hb6sVCC1gU
— Trooper Tod (@TrooperTodKHP) March 6, 2018

 
See the current number of active wildfires below:

.embed-container {position: relative; padding-bottom: 67%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}



 
 
 
Kansas State University scientists and the National Interagency Coordination Center began warning producers and fire managers earlier this year that drought in the central and southern Plains is causing a higher than normal risk of wildfire through April, stretching through Texas, Oklahoma and Kansas.

A Repeat of 2017 and 2016?
Ranchers and farmers in the area faced similar situations in 2017, when massive wildfires covered more than a 1.6 million acres in Texas, Oklahoma, Kansas and Colorado, on March 6, 7 and 8. Later in the year, wildfires affecting cattlemen were reported in North and South Dakota, Montana and California.