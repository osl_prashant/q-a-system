Customers are open to suggestion.
 
In fact, in some ways you aren't just a produce manager, but a psychologist. 
 
I mention this because you create a weekly offering to your customers that falls under what's known as classical conditioning. They hope for displays that appeal to their senses, and you as a produce professional are eager to oblige.
 
You may have heard about Ivan Pavlov. He's the famous Russian physiologist who, back in the day, examined dogs salivating while being fed. 
 
While there's a lot more to his study, he discovered that certain stimuli created a positive response. Are behavior scientists on to something?
 
I believe so. After all, when your produce department is in terrific stock condition on a regular basis, your customers are conditioned to expect the best quality, the best selection. 
 
If your produce department has inconsistent standards, your customers may not be as conditioned, and therefore you sell less produce than your store's potential. 
 
It's our job to improve the stimuli in order to trigger the best shopper response.
 
Think of the simple stimulus of our senses in a produce department.
 
Normal sounds associated with stocking, chopping melons, or merely friendly chatting with your customers engage them and put them in a potential shopping mode. 
 
When running my noisy orange juice machine on the sales floor, it was a curiosity draw from customers.
 
Smells are powerful shopping stimuli as well. The front door ventilation at my local store is connected to the deli fryers, and not coincidentally. A strong waft of hot fried chicken always makes me hungry. 
 
That orange juice machine smelled great on Sunday mornings. We sold more OJ in that morning than all the other days combined.
 
An example of stimuli-conditioned response? Absolutely.
 
Touch is another strong sense. When produce is absolutely fresh, people love to handle everything, be it a nice, smooth skin on a watermelon or the pebbly surface of a navel orange. 
 
When things are fresh, and not dehydrated or decayed, the stage is set to sell plenty of produce.
 
Taste is even a more powerful sense. Do you engage this stimulus in your department? Offering samples, getting customers to try something common, definitely sells produce.
 
I saved the best for last: Sight. Customers buy with their eyes, or so I've heard from just about every produce manager I've ever worked with. 
 
If you make displays pleasing to the eye - use of color breaks, neat, clean, level, culled and hand-stacked - you're going to be successful.
 
You may not get your customers to salivate, but you can certainly whet their appetite.
 
Armand Lobato works for the Idaho Potato Commission. His 40 years' experience in the produce business span a range of foodservice and retail positions. E-mail armandlobato@comcast.net.
 
What's your take? Leave a comment and tell us your opinion.