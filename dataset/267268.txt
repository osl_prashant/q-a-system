Stocks surge as market escapes early plunge on trade fears
Stocks surge as market escapes early plunge on trade fears

By MARLEY JAYAP Markets Writer
The Associated Press

NEW YORK




NEW YORK (AP) — After an early jolt, stocks rallied and finished higher Wednesday as investors bet that back-and-forth tariff threats between the U.S. and China won't blossom into a bigger dispute that damages global commerce.
The Dow Jones industrial average plunged 501 points after the opening bell but made it all back, and more. Household goods makers, retailers and homebuilders led the way while technology companies reversed some early losses. But two major targets of China's possible tariffs, aerospace company Boeing and farm equipment maker Deere, finished lower.
The early declines followed an announcement by the Chinese government that it plans to impose tariffs of 25 percent on a list of U.S. goods worth $50 billion, including soybeans and aircraft. The U.S. plans to place tariffs on a similar amount of Chinese goods, including industrial robots and telecom gear, subject to potential tariffs to protest Beijing's alleged theft of U.S. technology.
But investors relaxed as both sides emphasized a willingness to talk. President Donald Trump's top economic adviser, Larry Kudlow, suggested the U.S. tariffs won't be implemented if China lowers barriers to trade. Others noted the two countries have too much to lose from a trade war.
"The most likely outcome is smoke, but no fire," said Bill Adams, senior international economist at PNC Financial. "The amount that both countries have invested in bilateral trade cooperation and economic cooperation is so significant that the costs of going back would be very painful, and more than either country would want to bear."
U.S. trade policy has loomed over the markets since early March. Over the last five weeks stocks have plunged numerous times as investors reacted to tariff developments with shock and concern that an increase in protectionism will hurt international trade and company profits. But often, investors have caught their breath and decided that a full-blown trade war is unlikely, resulting in sharp recoveries.
On Wednesday, both of those things appeared to happen in the same day.
The Dow Jones industrial average advanced 230.94 points, or 1 percent, to 24,264.30, after a swing of more than 700 points. The S&P 500 index climbed 30.24 points, or 1.2 percent, to 2,644.69. The Nasdaq composite rose 100.83 points, or 1.5 percent, to 7,042.11. The Russell 2000 index of smaller-company stocks gained 19.51 points, or 1.3 percent, to 1,531.66.
Boeing, which delivered one-fourth of all its planes to China last year, fell as much as 5.7 percent early on and finished with a loss of $3.38, or 1 percent, at $327.44.
Adams, of PNC Financial, said the tariffs would be especially painful for companies in agriculture: machinery makers in the U.S. would pay more for imported components, and they wouldn't sell as much food in China because their products would be more expensive. He said that will stir up political pressure against the trade sanctions.
Farm equipment maker Deere lost $4.47, or 2.9 percent, to $148.57, after an early drop of 6.2 percent. Futures for Soybeans, a big U.S. export to China, fell 2.2 percent on the CBOT.
However Adams said that there was good news for food producers, as the Chinese government proposed duties on imported beef, but not pork or chicken. Hormel jumped $1.65, or 4.8 percent, to $35.87.
European stocks fell. Germany's DAX lost 0.4 percent while the CAC 40 in France dipped 0.2 percent. The FTSE 100 in Britain gained 0.1 percent.
Most Asian indexes closed before China announced its tariff plan, but Hong Kong's Hang Seng was still trading and slumped 2.2 percent.
The biggest worry for investors is that an escalating trade war will derail a global economy that is largely growing in unison. The global economy is expected to grow 3.9 percent this year, which would be its strongest showing in seven years, according to the International Monetary Fund.
Elsewhere, homebuilders rose following strong quarterly report from Lennar, which gained $5.73, or 10 percent, to $62.82.
Tech stocks have added to the recent volatility, mostly due to controversies surrounding technology companies like Facebook. On Wednesday, Facebook closed with a small decline, but other big tech names such as Apple and Microsoft closed higher.
After a big early loss, U.S. crude dipped 14 cents to $63.37 a barrel in New York while Brent crude, used to price international oils, fell 10 cents to $68.02 a barrel in London.
Wholesale gasoline stayed at $1.98 a gallon. Heating oil lost 2 cents to $1.98 a gallon. Natural gas rose 2 cents to $2.72 per 1,000 cubic feet.
Bond prices turned lower. The yield on the 10-year Treasury note rose to 2.80 percent from 2.77 percent. Gold prices jumped as much as 0.9 percent early on, but finished up just $2.90, or 0.2 percent, at $1,340.20 an ounce. Silver fell 14 cents to $16.25 an ounce and Copper lost 5 cents to $3.01 a pound.
After an early loss, the dollar rose to 106.74 yen from 106.61 yen. The euro rose to $1.2280 from $1.2267.
____
AP Business Writers Stan Choe contributed from New York and Kelvin Chan contributed from Hong Kong.
____
AP Markets Writer Marley Jay can be reached at http://twitter.com/MarleyJayAP . His work can be found at https://apnews.com/search/marley%20jay