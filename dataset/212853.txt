Organic produce grower-shippers needn’t apologize for pricing their wares a bit higher than conventional counterparts, marketers say.They earn the return, they say, and shoppers don’t mind.
“The story is in the numbers,” said Jacob Shafer, marketing and communications specialist with Salinas, Calif.-based Mann Packing Co.
“Consumers have shown they are willing to pay premiums for organics because they generally view the products as healthier compared to their conventional counterparts.”
Steve Lutz, senior strategist with Wenatchee, Wash.-based apple and pear grower-shipper CMI Orchards, says a key factor in building organic sales in supermarkets is in leveraging natural “entry points” for shoppers.
“Retailers can best leverage the organic opportunity by focusing sales programs first on the widely popular core varieties like gala and red delicious,” he said.
The price difference between organic and conventional produce items has been called a “price premium,” but it’s a misnomer, said Mayra Velazquez de Leon, president and CEO of San Diego-based Organics Unlimited Inc.
“It’s narrowing quite a bit, but I wouldn’t call it a premium. I’d say it’s the cost of growing healthier food,” she said.
Chris Ford, Salinas, Calif.-based organics category manager for the Vancouver, British Columbia-based Oppenheimer Group, said consumers accept a price differential, within certain limits.
“My magic number is 20%,” said Ford, who has been in the organic produce business since the early 1990s.
“Once you go over that premium, you start to lose consumers. But I’d say it is narrowing and, at certain times of the year, it’s about the same.”
Supply and demand also figures into the price gap between organic and conventional, Ford said.
“That’s not necessarily a good thing for the grower, and as supplies come and go, there will be some attrition,” he said.
“It’s not easy to grow organic and, as some people jump into the supply game, they may find it may not be sustainable long-term, because inherently you’re going to get less yield and it does deserve a price premium.”
Growers are doing what they can to keep the price difference at bay, said Roger Pepperl, marketing director with Wenatchee-based apple, pear and cherry grower-shipper Stemilt Growers LLC.
“We’re getting more efficient at organic farming,” he said.
“We’re also mainstreaming organics through our plants, which gives them more efficiencies. These plants running once a week, it gets expensive to run small volumes.”
The price gap also can vary from region to region and due to seasonal, weather and crop-timing factors, said Gary Wishnatzki, owner of Plant City, Fla.-based Wish Farms, which grows and ships organic and conventional berries.
The organic and conventional categories often are not comparable, he said.
“There really are two different markets, if you will,” he said.
“There’s some coattail effect of the conventional price, but not too much. Sometimes you see double or triple the market of the conventional. Sometimes, you see organics selling for less than conventional. It’s all over the board. It’s really dependent on supply and demand.”