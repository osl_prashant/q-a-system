Limoneira adds two to sales staff
John Caragliano is joining Santa Paula, Calif.-based Limoneira’s global sales team.
He will be based in Limoneira’s new Swedesboro, N.J., office and warehouse and will focus on the eastern U.S. and eastern Canada.
He will deal with both California and Arizona citrus as well as citrus imports from Chile, Argentina, South Africa and Mexico.
Caragliano comes from Chiquita Brands where he was director of sales, said John Chamberlain, director of marketing for Limoneira, Santa Paula, Calif.
In addition, Dan Bissett will join Limoneira’s sales management team in Santa Paula. Bissett joins the company from Primex Farms, where he was the grower relations manager.
 
Greenyard to open packing facility
Greenyard Logistics, sister company of Vero Beach, Fla.-based Seald Sweet, is scheduled to open a packing facility this summer.
Summer citrus varieties will be the first commodities packed in the new facility.
“We’re really looking forward to it,” said Kim Flores, marketing manager for Seald Sweet.