Colorado long has been a mecca for organic produce consumers, and, now, more growers there are getting into the category, said Jonathan Allen, president of Rogers Mesa Fruit Co./FirstFruits International, a Hotchkiss, Colo.-based tree fruit grower-shipper.
Allen said his company has been heavily involved in organics for years.
“We have been the No. 1 organic tree fruit shipper in Colorado,” he said.
In fact, the company took on conventional fruit only a couple of years ago, Allen said.
“One advantage of growing organically in Colorado is, generally speaking, we have low humidity,” he said. “Humid conditions attract insects and fungus and all kinds of issues, so, due to our low humidity and warm days and cool nights, we have a very nice microclimate for growing organics that gives us an advantage to minimize problems a lot of people out East have.”
Support among retailers is vigorous, Allen said.
“Colorado has one of the strongest retail support for organics,” he said.
He cited the Kroger Co. as an example.
The Rocky Mountain region is their No. 1 sales region for organics, he said. “Their King Soopers are tremendously successful in organics because there’s such demand and following for organics in the Mountain States. Add that to the local-grown thing, and there’s just tremendous demand in our region for the product.”
The Front Range, the region along the eastern edge of the mountains, which includes Denver and Colorado Springs, is especially keen on organics, Allen said.
“The Front Range takes a good chunk of our fruit,” he said. “On the organic side, it’s the No. 1 state for organic consumption per-capita, and there are tremendous market opportunities for local organic growers.”
There’s organic production of most, if not all, items grown in Colorado, said Robert Sakata, president of Brighton, Colo.-based Brighton Farms and president of the four-year-old Colorado Fruit & Vegetable Growers Association.
“There’s a lot of smaller growers that are supplying fruit stands locally that do grow organic,” Sakata said.
Hirakata Farms, a Rocky Ford, Colo.-based melon grower-shipper, is in its second year of limited organic production this summer, said Glenn Hirakata, co-owner.
The company had 60 acres of organic watermelons, cantaloupes and honeydews in 2016 and is up to about 70 this year, Hirakata said.
“It was a challenge,” he said. “Anytime you start organics, it’s always something new to learn. What we did learn is we need to watch our timing of planting a little better.”
The company also learned that demand, though strong in places, isn’t generally as high as conventional, “so when you go to pick, you want it staggered out.”
Hirakata said he expects his organic program to continue to develop.
“It’s a learning curve. There’s small demand, but like anything else, you have to satisfy the consumers.”
Denver-based Ringer & Son grows organic carrots, to go with about 1,000 acres of conventional sweet corn, said Joshua Johnson, president.
“We do pretty good-sized acreage,” he said. “We’re doing a ton with Whole Foods and Albert’s Organics.”
The crop started in late July and will run through October, he said.
Olathe, Colo.-based Tuxedo Corn Co., handles some organic sweet corn production, said Erik Westesen, operations manager.
Organic comprises only about 2% of the company’s volume, Westesen said.