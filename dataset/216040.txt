Sponsored Content
Late-season weed flushes were a widespread problem in 2017. If uncontrolled weeds went to seed, controlling tough broadleaf weeds may be even more challenging next year. Weed management programs for the 2018 season should be two things: proactive and aggressive.
The first step toward a proactive program is a burndown. A combination of 2,4-D and dicamba, Spitfire® is a powerful herbicide for application ahead of corn, soybeans and cotton. Spitfire provides true below-ground burndown; it kills troublesome, herbicide-resistant broadleaf weeds, roots and all.
In university trials, Spitfire showed 95%+ control at 3-4 weeks after treatment.[1] This aggressive burndown action controls weeds with a level of performance your growers can benefit from:

Easier planting conditions
Possibly eliminating another herbicide application, which is valuable for resistance management 
Making post-emergence applications of other herbicides more effective, due to smaller weed size at treatment

If resistant weeds in soybeans are a concern, Spitfire gives great flexibility to you and your growers. It can be applied up to 7 days before planting to control actively growing, emerged broadleaf weeds.
For more information about Spitfire, view this product brochure online. 


[1] 2013 trials measuring marestail control in Tennessee and cutleaf evening primrose control in Arkansas
 


Sponsored by Nufarm