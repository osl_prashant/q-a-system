In an era in which many people like to talk about what they eat, restaurants want to offer “shareable” fare.
The National Restaurant Association just released its annual trend report, and a definite theme was talking-point potential. 
Certified master chef Russell Scott referenced that element when discussing the produce-specific trends list that included exotic fruit, heirloom vegetables and fruits, and hybrid fruits and vegetables like plumcots and broccoflower.
Millennials have gained a reputation for using Instagram, Twitter and Facebook to parade their culinary adventures, but even traditional conversations seem to include food more than in years past.
Scott said people desire something different, not just for the sake of their own curiosity, but for its value for later conversations.
The overall concept trends noted by the association also spoke to more than flavor or visual appeal. Hyperlocal, clean menus, food waste reduction, environmental sustainability and farm/estate-branded items were on the list.

Ashley and editor Greg Johnson discussed the National Restaurant Association's trends list. Watch their conversation here.

For the most part, communicating about these topics is more difficult for foodservice operators than for retailers, who have the benefit of signage with displays and packaging on products. 
Restaurants can put such information on their websites and point people to that content using social media, of course, and that makes sense as a starting point. 
For individual operators or small chains, maybe the next step is using a weekly newsletter, mostly for advertising specials and new items but also to feature suppliers, sustainable elements of the business, or stories behind new items.
Regional or national chains could include that kind of information in their mobile apps, ideally linked to features that users value.
 
For instance, if Panera Bread were to offer a free entree item after every $50 a customer spends, the notification that they have earned a free salad could also pop up a window that offers, “Find out who grew the lettuce and the apples in your salad.”
Everyone loves to hear a good story — and just as much or more, they love having a good story to tell.
Retailers are already taking advantage of this, including by displaying photos of growers next to their product on the shelves.
 
Foodservice operators should follow suit in ways that make sense for them.
Ashley Nickle is a staff writer for The Packer. E-mail her at anickle@farmjournal.com.