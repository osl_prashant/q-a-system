The Academy of Veterinary Consultants (AVC) will hold their winter conference December 1, 2 and 3 in Denver. The program is approved for 15 CE hours.The meeting will be held at the Renaissance Denver Hotel, 3801 Quebec Street, with the AVC group room rate of $99/night for single or double occupancy if booked by November 7. A late registration fee of $50 also applies to participants who register after Nov. 7. Non-members may attend the meeting by paying $165 registration fee. Arrangements include a free airport shuttle, free parking at the hotel and free Internet access.
Register online toady at www.avc-beef.org/meetings/registration.aspor contact the AVC office at 913-766-4373, email at Paula@AVC-Beef.org.
The conference kicks off on Thursday, December 1, with AVC committee meetings, board meeting and a dinner and seminar, Hosted by Merck Animal Health, with Dr. Marshall N. Streeter discussing "Growth and Composition of Steers and Heifers Late in the Finishing Period." Following dinner, AVC and Norbrook, Inc. will sponsor a reception honoring retiring AVC Executive VP Dr. Bill Swafford.
Friday's full day of programming includes
¬? Breakfast, Hosted by Multimin USA, Inc., "Influence of Multimin90 Injection on Trace Mineral Status of Cattle" - Dr. Stephanie L. Hansen
¬? "Physical Examination of Cattle" -Dr. Allen Roussel
¬? "The Cattle and Protein Cycle - Where Do We Go from Here" -Mr. Randy Blach
¬? "U.S. Beef Industry in 20 Years" -Dr. Glynn Tonsor
¬? "Arrival MLV Vaccination of High-risk Beef Calves: Beneficial or Detrimental?" -Dr. John Richeson 
¬? "Helping People Get Cattle Out of the Pens" -Dr. Randy Hunter 
¬? "New Technologies for BRD Detection and Confirmation"-Dr. Edouard Timsit
¬? "Feedyard Animal Health Management Systems" - Dr. Carter King 
¬? "Hospital and Sick Cattle Management: Factors that Influence Cattle Recovery" -Dr. Oliver Schunicht 
¬? Merial will host the Friday dinner, followed by a New Member Reception hosted by Norbrook, Inc. 
Saturday's half-day program includes
¬? Breakfast, Hosted by Zoetis, "Basics of Implant Generated Growth Promotion in Beef Cattle" - Dr. Gary Sides
¬? "Prevalence and Quinolone Susceptibilities of Salmonella and Campylobacter in Feedlot Cattle Administered a Fluoroquinolone for the Treatment and Control of Bovine Respiratory Disease" - Ms. Ashley Smith
¬? "A Recurrent Theme: Not all strains of a "pathogenic" microbial species associate equally with disease and antibiotic resistance - the Mannheimia haemolytica example and its implications for combatting bovine respiratory disease in the future" - Dr. Mike Clawson 
¬? "Following the VFD: Antibiotic use Monitoring, Continuous Feeding, and Reduction Goals" - Dr. Michael Apley  
¬? "Implementation of VFDs: Filling Out Forms" - Dr. Marilyn Corbin 
¬? "Antibiotic Use in Animal Health 'Understanding FDA's Final VFD Ruling' and Lessons Learned" - Dr. Guy Hufstedler 
Find more meeting and registration information on the AVC website.