High costs, regulatory hurdles and eventual resistance make new discoveries daunting
As weed resistance spreads across more species, herbicide groups and states, the industry demands new alternatives. The only problem is, creating a new herbicide group is harder than you might think, and some companies aren’t sure it’s worth the risk.

“We haven’t had a new herbicide class (group) since 1992, HPPD inhibitors (group 27),” says William Vencill, University of Georgia Extension weed scientist. “If we create another one, it needs to be able to control resistant weeds to be an actual game changer.”

Chemical companies face numerous challenges when creating new herbicide groups. First, it takes more than $250 million and 10 years to clear testing and regulatory hurdles. Then, once it’s exposed to weeds, it’s only a matter of time before resistance sets in and profit takes a hit. Plus, if a company finds a new herbicide group and it’s only effective on grasses, which don’t have the resistance problem broadleaf weeds do, it won’t solve the problems farmers currently face and likely won’t be a big seller.

“New herbicides for corn, cotton or soybeans have to be cheaper than glyphosate and better for the environment as well as have better weed control and crop tolerance,” Vencill says. “If you do find a new site of action that does those things, you have to create a crop tolerant to it, which costs another $250 million to $350 million.”

Most of the cost comes during the regulatory process. The Environmental Protection Agency’s (EPA) review and approval process must be completed before marketing a new herbicide. During the EPA’s open comment periods, all concerns must be addressed, which often means more testing, time and money. In addition, EPA can be sued, and activists use litigation as a major opposition tactic.
Once a new herbicide is approved, if good stewardship practices are not maintained, it might only take a few short years for weeds to develop a resistance. Years of research and investment would go down the drain as weed resistance spreads—a risk over which chemical companies have no control.

“We have to keep the conversation about mixing modes of action going,” Vencill says. “One of the main reasons we see fewer sites of action come to market is these obstacles—investment and weed resistance—to success.”

But companies aren’t giving up. Recently Syngenta and DuPont filed a joint patent focused on developing a new herbicide chemistry class. They began their collaboration in 2015 with their recently published patent, “Substituted Cyclic Amides and Their Use as Herbicides.” Both companies declined to comment on the new class because it’s early in the process. In a previous joint statement, they said the product is in pre-development with an expected launch in 2023.

Whether new herbicide groups come to the table or not, one fact remains: Farmers must be diligent to ward off more resistance. The chemistries today need to last to fight weeds, and the chemistries of tomorrow won’t be valuable if stewardship is ignored.








 


Creating a novel herbicide is a challenge, but when you consider the cost, it becomes more apparent why it’s been nearly 30 years since the last herbicide group was introduced.