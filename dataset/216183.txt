Sponsored Content
Resistant marestail, palmer amaranth and giant ragweed are forcing retailers to step up their weed control programs. Dr. Bill Johnson, professor of weed science at Purdue University, shares some thoughts on controlling resistant weeds, as well as avoiding additional resistance issues.


Plan on programs costing up to $50/acre if resistant waterhemp or palmer amaranth is present. This cost can be partly attributed to the need for layering multiple applications of different residual herbicides at planting and post-emergence.  


Residual herbicides used at the front-end of the season give post-emergence herbicides the best chance possible to be successful. With better control early on, weed size should be smaller for the post-emergence application.


Giant ragweed can emerge from “seed buried as deep as 4 inches.”[1] From that seed depth, it might take a while for the seed to emerge. If resistant giant ragweed has been in a field before, plan on post-emergence herbicides. 


The crop’s canopy is the most powerful weed control tool you have.


A fall-seeded cover crop can provide reliable help for controlling marestail, which usually emerges in the fall. If the cover crop can eliminate a spray, that is even better for curbing additional herbicide resistance issues. The effectiveness of cover crops, however, to suppress summer annual weeds, such as waterhemp and palmer amaranth, has been mixed.


Fall sprays can be a good practice if resistant marestail is present, but ONLY if you are using a different active ingredient than what you’ll use in-season.


Diversity is the buzzword. Don’t limit yourself to diversity of herbicides but also look at diversity of practices (could tillage help?) and diversity of crops.


[1] https://www.extension.purdue.edu/extmedia/BP/GWC-12.pdf 
Sponsored by Nufarm