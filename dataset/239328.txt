Former preacher finds calling as Alabama folk artist
Former preacher finds calling as Alabama folk artist

By JARED BOYDAL.com
The Associated Press

MOBILE, Ala.




MOBILE, Ala. (AP) — In the midst of an otherwise eerily still Theodore neighborhood, Alabama singer-songwriter Abe Partridge sits at a dining room table. With one hand, he nibbles on steamed blue crab, a delicacy coveted by his wife, Cathy, a Maryland native who swears by the dish, as it is prepared in her home near the Chesapeake Bay.
With his other hand, the musician points across the room to a wall covered with records, memorabilia from his career, hanging guitars and artwork.
"That's Son House," Partridge says of one framed print. "He's one of the guys who made me really want to do what I'm doing."
"He was a Baptist preacher (like Abe)," Cathy interjects.
"I don't know if you've ever watched him, but his eyes would roll back in his head. He would commit every part of his being into playing that song and telling his story. There was something about that that resonated way down deep in me," Abe continues.
"I was like, man. I gotta figure that out. However he's doing that, I gotta figure it out. You can tell it was probably the same thing he did when he was a preacher."
Partridge leans back in his wooden seat, resting his hands on his family's warm-colored paisley tablecloth, in a contemplative manner.
"Now, when I play my songs, I have the same kinda mindset I had, back when I was waving handkerchiefs or walking to the back of pews and screamin' at folks. It was about something that I really believed in. Now, I just do it in song."
Abraham Partridge was raised in Mobile, before leaving home to begin theology school at 17-years-old. He says he was in search of truth.
Visiting church communities of all varieties, he landed in neighboring Florida at Pensacola Christian College, a school for ministers of the Independent Fundamental Baptist denomination. What drew him, he says, is the certainty with which the pastors in the organization spoke.
"It's kind of passionate, the way that they preach," Cathy says. "(It was) something that felt real - not boring, monotone stuff."
However, Abe didn't stay long, transferring after one semester to Tennessee Temple, a similar school in Chattanooga. His accounts of his college years include white-glove inspections of his dorm area, restrictions on the music he listened to and instruments he could play and punishments that forced him to work as a janitor to work off demerits. These strict rules caused him to bounce from school to school, although each college had similar guidelines. He moved again to an IFB-affiliated institution in Knoxville, Crown College, where he met Cathy. She followed him to another school in Georgia, where he finished his degree. He and Cathy wed the day after his graduation.
At the time, he was already having reservations about his career choice.
"(Second thoughts) are always in the back of your mind, but you try to squash 'em. But, you're in this community that reinforces it all," he says.
Before long, though, Partridge moved once again. This time, it was due to a calling to lead his very own congregation in Middlesboro, Kentucky.
"It was about 60 people there. Then we took it. By the second Sunday, there was like 20," Abe says, laughing. The numbers continued to dwindle, resulting in a sparse environment of religious fellowship. "There were times I'd show up to church and just be preaching to Cathy. Not a soul there."
Additionally, Partridge had to work odd jobs to sustain, while Cathy stayed home and raised their two children.
"We lived in abject poverty, because of what the church taught. I wasn't allowed to work a job, because I was a preacher's wife. We were taught that was the right thing to do," Cathy says. "We lived in a little single-wide trailer, down this old, country road. We basically lived like someone would have in the early 1900s. We grew our own food. We had chickens."
"Chickens were our entertainment," Partridge says with a straight face.
"If I had to choose between live football and chickens, I'd choose chickens every time," he says, finally breaking into laughter. "And I love football."
Partridge credits his isolation from the peers he studied with as the greatest factor in his choice to switch gears.
"When you're in that kind of belief structure, everything is certainty. There is no doubt. Doubt is sin."
"One day, I'm in Kentucky and I think, 'Ehhhh... that's probably not right.' It's like a spring. All you have to do is take the tension off one spot and you have nine years of pressure break loose"
At his wit's end, he told the church's founder that he was ready to move on, and he says that looking back, he feels guilty for the commitment he made.
"There's nothing that I can say I was really proud of. For a long time, for many years, I had lots of regrets."
"Basically, you scare people into heaven," Cathy says. "Everything we ever did was never fake. We thought we were doing the right thing."
On their last day in town, Abe loaded a moving truck with their belongings, while Cathy watched their two young children.
"I pastored two years in that community. I pastored a lot of folks in that time. My neighbor, who was a Mormon, came over to help me load up my U-Haul. No one else even came over. That'll tell you a little about how bad it was leavin'."
The couple moved in with Abe's mother in Mobile.
With no skills and a theology degree, Abe went to an Air Force recruiter who told him he'd have to get down to 200 pounds in order to enlist. Abe enlisted the help of his younger brother, a police officer, to shed forty pounds within two months.
He joined a military avionics program, serving tours during Operation Iraqi Freedom and Operation Enduring Freedom.
Today, he continues to serve as a reservist.
In his period of transition from Kentucky to Mobile, he started writing songs, drawing from a secret passion he cultivated in secret as a college student.
At the time, Abe says, he was forced to avoid any popular American, save for recordings done prior to the rock n' roll era of the 1950s and '60s.
"Elvis Presley was like the 'anti-Christ'. But I could go before Elvis and find all this cool stuff."
In private, Partridge studied roots music musicians such as Charlie Patton and Robert Johnson, Stanley Brothers and Roscoe Holcome.
"It was heart music," he says.
His 'heart music' eventually led him to Dylan.
"I'm listening to these songs, and they're lyrical. The blues had a feel, that got you 'here'," he said, pointing to his chest. "Dylan took that feel and added lyrics to it that got you 'here'."
Although free from the alleged constraints of his former church, he only felt comfortable playing his songs in front of Cathy.
That was until he came across an advertisement for a songwriter's competition in Gulf Shores. Having only played a couple open mics, he entered, performed and received a standing ovation.
Following the show, a Nashville producer approached him and asked him to cut a record. Two months later, they were in the studio working on what became Partridge's debut, "White Trash Lipstick".
A year later, Partridge and producer Shawn Byrne teamed up again to create "Cotton Fields and Blood for Days", his debut on the Baldwin County based label Skate Mountain Records.
Both records are laced with Partridge's distinct, bluesy growl, lo-fi technique and deep, lyrical appeal.
"I try to write from a real place and perform from a real place. I never fell for polished music. I never did. It doesn't matter what genre it is, if I feel like it was choreographed or planned, I just don't dig it."
"I can appreciate almost any type of art: music or otherwise that comes from an authentic place. 'Genre' is just kinda window dressing for what I want. I want to hear the human heart. I want to hear that part of your soul come out - in whatever medium you choose for it to come out. It thrills me, y'know."
The album's lead single, builds on the artist's spiritual past. It is a song, he says, God wrote to him while driving to work one day.
"I want to title my next record, 'Songs I Wrote to God and Songs God Wrote to Me.' That's the way I feel about 'Colors'. If I could credit 'the eternal spirit' or God or whatever with writing a song, it'd be that one."
Another standout, "Ghosts of Mobile" is inspired by his hometown's Antebellum past. His goal wasn't to be heavy-handed, however, hoping his fans would listen to the history and make their own minds up about how to feel.
"Here's this. Look at it. Until you see it, you can't make any progress," he says.
Along with his vocal awakening, Partridge unveiled another hidden talent while preparing the release of his sophomore LP. The album's January release party doubled as a gallery show, of sorts, for paintings he carved from tar on wooden boards.
He'd kept his visual art a secret for almost as long as his music.
"I couldn't even hang 'em on my walls for the longest time, because people would walk in and say, 'What is that?!'" he jokes, noting that he felt his art style was a bit unsettling to guests.
His first ever piece now hangs proudly on the wall of his home office.
It's a portrait of a familiar face.
"That's Son House," he says.
"He had to choose between the Devil and the blues and he chose the blues. In a sense, I've done the same thing."