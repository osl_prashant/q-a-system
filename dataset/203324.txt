A survey released in December by Pew Research Center has some positive news for the organic industry and somewhat less favorable results for those who produce genetically modified crops.
The survey reflects what researchers describe as a "pronounced shift in Americans' eating habits over the past 20 years," which has resulted in "new food divides" when it comes to how people assess the health effects of organic and genetically modified foods.
The survey indicated that 55% of Americans believe organically grown produce is more healthful than conventionally grown varieties, while 41% say there is no difference.
Three percent of those surveyed said conventionally grown produce is better than organic.
Four-in-10 Americans say that most or at least some of the food they eat is organically grown, and 75% of those believe that organic foods are more healthful than conventionally grown foods.
A significant minority of Americans - 39% - consider genetically modified foods worse for a person's health than other foods.
However, 48% of adults say genetically modified foods are no different from non-genetically modified foods, and 10% say genetically modified foods are better for health.
"In a way, these choices reflect personalized 'ideologies' that shape how people think about and consume food," the study said.
The divides over food don't seem to fall along political lines, and they're not related to education, income, geography or having minor children.
"Rather, they tie to individual concerns and philosophies about the relationship between food and about the issue of GM foods," the report said.
For example, roughly equal shares of Republicans (39%) and Democrats (40%) feel that genetically modified foods are worse for people's health. And, half of Republicans and 60% of Democrats have positive views about the health benefits of organic foods.

The report also looks at how consumer opinions relate to their feelings about eating healthful foods.
"The 18% of Americans who are particularly focused on healthy and nutritious eating are especially likely to consider organic produce healthier than conventionally grown produce," the survey says.
"They follow news about GM foods more closely, though their views about the health effects of GM foods are similar to those with less focus on eating healthy and nutritious foods."
Age also matters. Younger consumers are more likely than older ones to see health benefits in organic produce and health risks in genetically modified foods, the survey indicates.
The study was conducted from May 10 to June 6, with a nationally representative survey of 1,480 adults.