Oxnard, Calif.-based mango and avocado supplier Freska Produce International plans to open a distribution center in the McAllen, Texas, area and has hired a produce veteran from the region as general manager to run it.
Mary Velasquez, Edinburg, Texas, who was with Los Angeles-based Coast Tropical for more than 20 years, will help Freska grow the company's regional and national accounts, said Gary Clevenger, managing member and company co-founder, in a news release.
Freska will lease space in time for the start of the company's Mexican mango deal Jan. 18, Clevenger said.
"It's our intention over the next year or so to find a place of our own," he said.
Freska has been distributing mangoes and avocados nationwide for more than 12 years from its Oxnard location and has been applying a strategy to grow as opportunities arise. Clevenger estimated 25-30% of the company's total volume in recent years has come through Texas.
"We do a considerable amount through Texas only during the Mexican season; it's our intention to do it even more so, now that (Velasquez) is based there," Clevenger said.
Velasquez started her new job Jan. 2, Clevenger said.
Freska said it plans to have Velasquez focus on retail accounts in the U.S., Australia, Japan and New Zealand.