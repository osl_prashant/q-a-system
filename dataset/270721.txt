The United Fresh Produce Association announced new officers and members serving on its board of directors, effective April 26.Susan Reimer-Sifford, director of produce and bakery purchasing and distribution for Darden Restaurants Inc., Orlando, Fla., is United Fresh’s chairwoman of the board.
Chairwoman-elect is Cindy Jewell, vice president of marketing for California Giant Berry Farms, Watsonville.  Secretary-treasurer is Danny Dumas, vice president of sales and product management at Del Monte Fresh Produce, Coral Gables, Fla.
Their terms become effective Aprl 26, during United Fresh’s annual show.
Tony Freytag, senior vice president of sales and marketing for Crunch Pak, Cashmere, Wash., moves to immediate past chairman and remains on the board and the executive committee.
The following industry leaders have been nominated as new members of the United Fresh board for a two-year term beginning April 2017:
•Jeff Cady, director of produce and floral for Tops Friendly Markets, Buffalo, N.Y.;
•Jerry Callahan, group vice president of produce and floral for Albertsons Cos., Boise, Idaho;
•Michael Castagnetto, vice president of global sourcing for Robinson Fresh, Eden Prairie, Minn.;
•Chris Ciruli, chief operations officer for Ciruli Brothers LLC, Tubac, Ariz.;
•Michael Cochran, merchandise manager of produce for Wal-Mart Stores Inc., Bentonville, Ark.;
•Mark Girardin, president of North Bay Produce Inc., Traverse City, Mich.;
•Christian Harris, vice president of fresh produce for US Foods, Rosemont, Ill. ;
•Jim Hay, North America regional business director, DuPont Crop Protection, Wilmington, Del.;
•Brian Jenny, vice president and general manager of value-added fresh, Naturipe Farms LLC, Sugar Hill, Ga.;
•Drew McDonald, vice president of quality, food safety and regulatory for Church Brothers Farms, Salinas, Calif.;
•Phil Muir, president and CEO of Muir Copper Canyon Farms, Salt Lake City;
•Mark Munger, vice president of sales and marketing for 4Earth Farms, Los Angeles;
•Raina Nelson, vice president of sales for Renaissance Food Group, Oldsmar, Fla.; and
•Mayda Sotomayor-Kirk, CEO for Seald Sweet International/Greenyard USA, Vero Beach, Fla.
“Our organization thrives on the strength and diversity of the leadership on the United Fresh Board of Directors,” Tom Stenzel, president and chief executive officer of United Fresh, in the release. “This slate of new Board members, along with those returning to the board, will provide a strong representation and leadership of our industry.”