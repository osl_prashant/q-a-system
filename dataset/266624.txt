Industrial-scale pork on trial in federal nuisance lawsuits
Industrial-scale pork on trial in federal nuisance lawsuits

By EMERY P. DALESIOAP Business Writer
The Associated Press

RALEIGH, N.C.




RALEIGH, N.C. (AP) — A federal lawsuit starting next week in the country's No. 2 pork-producing state is the first of a string of cases deciding whether open-air animal waste pits are such a nuisance that neighbors can't enjoy their own property.
The North Carolina trial's outcome could shake the profits and change production methods of pork producers who have enjoyed legislative protection and promotion in one of the nation's food centers.
Pork producers and their supporters see industrial-scale hog complexes as generating jobs and revenues in rural communities.
More than 500 neighbors have sued a division of Chinese-owned Smithfield Foods, arguing that the storage cesspools that proliferated in the past generation cause intense smells and clouds of flies so foul that neighbors can't enjoy their own property.