Do you wear your heart on your sleeve?
 
A better question might be, does your uniform or apron distinguish you as a produce professional?
 
The "heart upon one's sleeve" phrase imparts the image of openly displaying some message to those around you. Wiktionary explains knights in the Middle Ages often wore the "colors" of the lady they were supporting.
 
With Valentine's Day around the corner, doesn't that inspire a collective "Aww...?"
 
Recently I pondered the subject of store-level attire and wondered, how important is it? After all, I try to steer attention to produce merchandising, to department management, and of course the star of our show, fresh produce.
 
I do think, assuming everything else is managed superbly in today's business environment, that a distinctive apron or uniform helps a produce department stand out.
 
In fact, I recall as a youngster tagging along with mom on shopping trips at the nearby, curved-ceiling Safeway. It was rich with aromas of ripe bananas and freshly ground coffee. I was mesmerized by the produce clerk's bright white shirt, his funny bow tie, and especially his crisp green apron, as he carefully stocked apples. 
 
"When I grow up, I wanna do that!" I said.
 
I know. Be careful what you wish for, right?
 
Some of the best grocers in the country provide distinctive uniforms or aprons for their employees. I think it adds a level of class, an air of professionalism, not unlike in fine restaurants, where seasoned wait staff, renowned chefs, or wine sommeliers present themselves in traditional attire, including name tags. 
 
These grocers, which range from discount to gourmet, approach the subject in a manner that instills pride and confidence. They wish to convey the same message: top quality and service.
 
My idea of an ideal grocer's dress code is simple: Minimal clothing restrictions, as long as it's clean and business-appropriate. Meaning collared shirts for guys (no ties, save for store managers), and nothing offensive or unsafe. Dress safety shoes, no sneakers.
 
And a distinctive, crisp apron with the company logo.
 
Each department would sport similar, albeit different colored aprons, distinguishing one department from another: A neutral color for grocery and service departments, white for the bakery, red for our friends the meatheads, and a glorious kelly green for the produce department.
 
For whatever it's worth, this humble, produce-peddling scribe believes that a green apron enhances a great produce department. It makes us stand out in the crowd, in our daily battle.
 
Like modern-day produce knights, with trim knives in scabbards (instead of swords).
 
Armand Lobato works for the Idaho Potato Commission. His 40 years' experience in the produce business span a range of foodservice and retail positions. E-mail armandlobato@comcast.net.
 
What's your take? Leave a comment and tell us your opinion.