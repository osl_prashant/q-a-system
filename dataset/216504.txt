Jamie Phillips, director of scientific affairs for SPINS Marketing, will give the opening keynote address Jan. 26 at the start of the education track at The Packer’s Global Organic Produce Expo.
“We’ll present consumer interaction and trends that haven’t yet hit the mainstream,” she said. “And we’ll look at the attributes that motivate consumer behaviors.”
Phillips is a registered dietitian and has worked with many retailers on how to motivate and educate consumers to eat healthier, such as more fruits and vegetables.
Chicago-based SPINS works with retailers to increase the presence and accessibility of natural and organic products. 
The Packer’s GOPEX event is scheduled for Jan. 25-27 at the Diplomat Beach Resort in Hollywood, Fla. 
For more information, visit www.globalorganicexpo.com.