Greener Fields Together, Monterey, Calif., through its local farm grant program, Cultivating Change, has awarded $90,000 in grants to local growers participating in the Produce Regional Operators Advancing Cooperative Trade initiative.
"We are proud to give back to farms that make such a large contribution to their local community," said Max Yeater, president and CEO of Pro*Act in a news release. "The Cultivating Change grants give these local farmers an opportunity to fund projects that will allow them to invest in their current and future operations. Through these improvements they are paving a way for the next generation of farmers."
The grant program, in its second year, was funded by Pro*Act's distributer network. Greener Fields Together is Pro*Act's sustainability and local produce initiative.
Sixteen farms received grants ranging from $3,000 to $10,000. Winners were chosen by a vote and panel discussion of industry professionals.
The following farms received grants:
$10,000 to Bella Verdi Farms for rainwater collection system expansion;
$10,000 to Fable: From Farm to Table for cold storage and packing rooms;
$10,000 to R. Potano Produce for greenhouse/hydroponic system;
$10,000 to Working Landscapes to bring chopped greens from farm to school;
$5,000 to Daniels Produce for kitchen for processed local foods;
$5,000 to MicroFarms for expansion project;
$5,000 to Barberry Hill Farms for walk-in refrigerator installation;
$5,000 to J&C Farms & Groves for large seed bank project;
$5,000 to Jaemore Farms for a homegrown produce box;
$5,000 to Poche Family Farm for sustainable produce containers;
$5,000 to Cul2vate for greenhouse installation;
$3,000 to Dawson's Orchards to transition to plastic bins;
$3,000 to Fullei Fresh for cleanliness verification;
$3,000 to M&M Farms for a tractor upgrade;
$3,000 to Coveyou Scenic Farm for an organic produce geothermal walk in cooler and greens processing;
$3,000 to Swain Farms to purchase a reefer truck.
Applications for next years' awards will be accepted Sept. 1 thru Oct. 31. Profiles of the winners are available online at greenerfieldstogether.org.