Country Fresh Orlando has recalled nearly 6,000 cases of fresh-cut vegetable products due to potential listeria contamination.Wal-Mart, Publix and Southeastern Grocers locations in Alabama, Florida, Georgia, Louisiana and Mississippi received the items in question.
The products were sold under store brands including Marketside and SEG as well as the Country Fresh brand.
“We are treating this incident very seriously because we want to ensure that our customers are provided with only the safest, most wholesome and high-quality products available,” Max Payen, director of food safety for Country Fresh, said in a recall notice posted to the Food and Drug Administration website.
The company has stopped distributing the affected products and is investigating, along with the FDA, what caused the issue.
The potential for contamination was identified during routine sampling at a store in Georgia.
Products subject to the recall include:
12-ounce fajita blend;
stir-fry vegetable;
23-ounce vegetable kabob;
6-ounce diced green pepper;
6-ounce creole mix;
6-ounce tri-pepper dice;
6-ounce fajita mix;
15-ounce veggie kabob;
7-ounce tri-pepper dice; and
23-ounce kabob.
More information on which products were sold at which banners in which states can be found here.
The items are packaged either in clear plastic containers or in Styrofoam trays overwrapped with clear plastic film and have best-by dates between Aug. 12 and Aug. 20.
The recall by Country Fresh spurred a secondary recall by Southeastern Grocers, which owns the Winn-Dixie, Bi-Lo, Harveys and Fresco y Más banners.
No illnesses have been linked to the recall.