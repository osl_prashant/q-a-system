BC-Cash Prices, 1st Ld-Writethru,0342
BC-Cash Prices, 1st Ld-Writethru,0342

The Associated Press

NEW YORK




NEW YORK (AP) — Wholesale cash prices Wednesday
?                                      ??Wed.???????Tue.
F
Foods

?Broilers national comp wtd av           ?1.0874?????1.0874
?Eggs large white NY Doz.                ???1.44???????1.60
?Flour hard winter KC cwt                ??16.20??????16.25
?Cheddar Cheese Chi. 40 block per lb.    ?2.1550?????2.1350
?Coffee parana ex-dock NY per lb.        ?1.1995?????1.1955
?Coffee medlin ex-dock NY per lb.        ?1.4019?????1.3946
?Cocoa beans Ivory Coast $ metric ton    ???2834???????2834
?Cocoa butter African styl $ met ton     ???7558???????7558
?Hogs Iowa/Minn barrows & gilts wtd av   ??48.45??????46.87
?Feeder cattle 500-550 lb Okl av cwt     ?167.50?????167.50
?Pork loins 13-19 lb FOB Omaha av cwt    ??79.23??????78.32
Grains
?Corn No. 2 yellow Chi processor bid     ?3.70????????3.89¼
?Soybeans No. 1 yellow                   10.17¾??????10.20?
?Soybean Meal Cen Ill 48pct protein-ton  388.40??????392.60
?Wheat No. 2  Chi soft                   ?4.87¼???????4.92?
?Wheat N. 1 dk  14pc-pro Mpls.           ?7.59????????7.56½
?Oats No. 2 heavy or Better              ?2.52½???????2.51½
Fats & Oils
?Corn oil crude wet/dry mill Chi. lb.    ?.31??????????.31?
?Soybean oil crude Decatur lb.           ?.30¼?????????.30?
Metals
?Aluminum per lb LME                     0.9849??????0.9584
?Antimony in warehouse per ton           ??8500????????8580
?Copper Cathode full plate               3.1242??????3.0693
?Gold Handy & Harman                     1350.75????1338.95
?Silver Handy & Harman                   ?16.807?????16.608
?Lead per metric ton LME                 2401.00????2397.00
?Molybdenum per metric ton LME           ?15,500?????15,500
?Platinum per troy oz. Handy & Harman    ?933.00?????929.00
?Platinum Merc spot per troy oz.         ?928.80?????927.60
?Zinc (HG) delivered per lb.             ?1.4695??????1.4589
Textiles, Fibers and Miscellaneous
?Cotton 1-1-16 in. strict low middling   ??80.06??????79.77
Raw Products
?Coal Central Appalachia $ per short ton ??61.10??????61.10
?Natural Gas  Henry Hub, $ per mmbtu     ???2.69???????2.71
b-bid a-asked
n-Nominal
r-revised
n.q.-not quoted<29
n.a.-not available