When prices are low, making quality milk is more important than ever to capture any quality premiums. Below is a list of the five primary indicators of milk quality and where measurements should be on your farm.
Standard Plate Count (SPC): Estimates the total number of viable aerobic bacteria present in raw milk. A high SPC can indicate poor cleaning practices, udder hygiene or milking procedures.
Bulk Tank Somatic Cell Count (BTSCC): A measure of somatic cells present in milk. The BTSCC gauges the udder infection statistics of the herd and indicates the potential loss of production due to mastitis.
Laboratory Pasteurization Count (LPC): Estimates the number of bacteria that can survive laboratory pasteurization for 30 minutes. High LPC numbers indicate persistent cleaning problems, faulty milking machines or worn-out milking equipment.
Coliform Count (CC): Estimates the number of bacteria that originate from manure or a contaminated environment. This can come from milking dirty cows or dropping the milking claw into manure during milking.
Preliminary Incubation Count (PIC): A measure of the number of cold-loving bacteria in milk. This can indicate inadequate holding temperatures in the bulk tank.
The chart below shows optimal levels of each of the five categories.

 
Note: This story ran in the March 2018 magazine issue of Dairy Herd Management.