When USDA's weekly crop condition ratings are plugged into Pro Farmer's weighted (by production) Crop Condition Index (CCI; 0 to 500 point scale, with 500 representing perfect), the corn crop climbed 2.68 points to 360.17 points. Conditions ticked up in Indiana and Illinois, but declined slightly in Iowa. Soybean condition ratings also climbed nearly a point to 349.16 points, with steady to higher ratings in most Midwest states helping to offset a decline in Iowa.





Pro Farmer Crop Condition Index






Corn




This week



Last week



Year-ago





Colorado (1.03%*)


3.89


3.55


3.91




Illinois (15.40%)


55.45


54.37


63.01




Indiana (6.64%)


23.04


22.64


26.67




Iowa (17.72%)


62.91


64.32


68.20




Kansas (4.29%)


14.90


14.81


14.70




Kentucky (1.57%)


6.15


6.20


6.45




Michigan (2.35%)


8.29


8.59


8.44




Minnesota (9.66%)


37.96


37.77


38.12




Missouri (3.81%)


13.90


13.90


14.06




Nebraska (11.62%)


42.20


41.15


45.50




N. Carolina (0.71%)


2.75


2.79


2.74




N. Dakota (2.70%)


8.76


8.36


9.71




Ohio (3.80%)


13.65


13.69


13.55




Pennsylvania (0.98%)


4.27


4.31


3.79




S. Dakota (5.62%)


16.51


15.89


19.51




Tennessee (0.89%)


3.69


3.69


3.48




Texas (2.06%)


8.13


7.86


6.92




Wisconsin (3.61%)


13.78


13.78


14.59




Corn total


360.17


357.49


387.08









Pro Farmer Crop Condition Index






Soybeans




This week



Last week



Year-ago





Arkansas *(3.78%)


14.28


14.32


14.39




Illinois (13.85%)


49.87


49.73


55.28




Indiana (7.41%)


25.87


25.72


28.53




Iowa (13.35%)


45.93


47.13


52.79




Kansas (3.96%)


13.67


13.71


13.60




Kentucky (2.14%)


8.17


8.04


8.77




Louisiana (1.59%)


6.19


6.24


6.30




Michigan (2.38%)


8.27


8.53


8.29




Minnesota (8.82%)


33.62


33.62


33.71




Mississippi (2.38%)


9.17


9.29


9.23




Missouri (5.86%)


21.27


21.27


21.97




Nebraska (7.46%)


26.57


26.20


29.36




N. Carolina (1.50%)


5.66


5.69


5.85




N. Dakota (5.24%)


16.88


16.14


17.78




Ohio (6.14%)


21.50


21.26


21.39




S. Dakota (5.93%)


18.33


17.62


20.32




Tennessee (1.86%)


1.00


1.00


1.00




Wisconsin (2.29%)


8.87


8.84


8.77




Soybean total


349.16


348.36


374.16




 
* denotes percentage of total national corn crop production.
Iowa: All of Iowa experienced cooler than normal temperatures with very little precipitation scattered across the state during the week ending August 13, 2017, according to the USDA, National Agricultural Statistics Service (NASS). Statewide there were 6.3 days suitable for fieldwork. Activities for the week included applying fungicides and insecticides, hauling grain, and haying.
Topsoil moisture levels fell to 30% very short, 33% short, 37% adequate and 0% surplus. According to the Aug. 8, 2017 U.S. Drought Monitor, Iowa's region of drought expanded to 40% of the state including portions of 23 counties in severe drought. Subsoil moisture levels rated 25% very short, 33% short, 42% adequate and 0% surplus. That is the highest percentage of very short subsoil moisture supplies since the first week of November 2013.
Sixty-two percent of the corn crop was in or beyond the dough stage, 6 days behind last year. Eight percent of the corn crop has reached the dent stage, one week behind average. Corn condition declined to 3% very poor, 9% poor, 27% fair, 52% good and 9% excellent.
Soybeans blooming reached 94%, 5 days behind last year and 4 days behind average. Eighty-two percent of soybeans were setting pods, 3 days behind last year but 2 days ahead of average. Soybean condition rated 4% very poor, 11% poor, 29% fair, 49% good and 7% excellent.
Illinois: Dry, cool weather prevailed across most of the state last week. There were 6.2 days suitable for fieldwork during the week ending Aug. 13. Statewide, the average temperature was 69.2 degrees, 4.5 degrees below normal. Precipitation averaged 0.40 inches, 0.43 inches below normal. Topsoil moisture supply was rated at 7% very short, 35% short, and 58% adequate. Subsoil moisture supply was rated at 5% very short, 32% short, 62% adequate, and 1% surplus.
Corn dough was at 77%, compared to 76% last year. Corn dented was at 26%, compared to 30% for the five-year average. Corn condition was rated 3% very poor, 8% poor, 27% fair, 50% good, and 12% excellent.
Soybeans blooming reached 97%, compared with 94% last year. Soybeans setting pods was at 83%, compared to 79% last year. Soybean condition was rated 3% very poor, 8% poor, 26% fair, 52% good, and 11% excellent.
Indiana: Soil moisture has decreased as a result of the lack of rain over the past couple of weeks, according to Greg Matli, Indiana State Statistician for the USDA's NASS. Spotty showers have been inadequate for proper growth of crops leading farmers to continue irrigating fields. Cooler temperatures helped growing crops use less water. The statewide average temperature was 69.1 degrees, 3.7 degrees below normal. Statewide precipitation was 0.24 inches, below average by 0.65 inches. There were 6.3 days available for fieldwork for the week ending Aug. 13, up 0.9 days from the previous week.
Regionally, corn was 58% in dough in the North, 69% in Central, and 74% in the South. Corn was 15% dented in the North, 21% in Central, and 26% in the South. Corn rated in good to excellent condition was 60% in the North, 48% in Central, and 55% in the South.
Soybeans were 93% blooming in the North, 94% in Central, and 90% in the South. Soybeans were 76% setting pods in the North, 81% in Central, and 76% in the South. Soybeans rated in good to excellent condition were 62% in the North, 52% in Central, and 56% in the South. Some areas are starting to show signs of drought stress. Pollination is nearly complete with issues being reported in some areas.
Other activities included spraying soybean and corn fields for diseases, preparing bins for fall storage, hauling grain, mowing roadsides, scouting for pests, and attending the Indiana State Fair.
Minnesota: Rainfall and below normal temperatures allowed for 4.6 days suitable for fieldwork during the week ending August 13, 2017, according to USDA's NASS. Activities for the week included small grains harvesting and spraying of pesticides. Topsoil moisture supplies rated 4% very short, 17% short, 73% adequate and 6% surplus. Subsoil moisture supplies rated 3% very short, 17% short, 76% adequate and 4% surplus.
Corn silking was nearly complete at 96%. Fifty-four percent of the crop was at the dough stage. Corn crop condition rated 81% good to excellent, up 1 percentage point from the previous week.
Eighty-four percent of the soybean crop was setting pods, one day ahead of the five year average. Soybean condition remained at 74% good to excellent.