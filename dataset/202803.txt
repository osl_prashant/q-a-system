Repeating a familiar theme, the U.S. General Accounting Office has called for a more unified approach to food safety issues among federal agencies.
 
In a report called "Food safety: A national Strategy is needed to address fragmentation in federal oversight," the GAO said that the
U.S. Department of Agriculture has not fully implemented the GAO's December 2014 recommendation that it specifically detail its cooperation with other federal agencies on food safety.
 
For more than four decades, the GAO has reported on the fragmented federal food safety oversight system, according to a summary of the report.
 
"Because of potential risks to the economy and to public health and safety, food safety has remained on GAO's list of high-risk areas since 2007," according to the report.
 
The GAO also said the Office of Management and Budget has not addressed GAO's March 2011 recommendation to develop a government-wide plan for the federal food safety oversight system.
 
The report prompted a letter from Democratic U.S. senators asking President Donald Trump to improve the food safety inspection system.
Sens. Kirsten Gillibrand, D-N.Y., Richard Durbin, D-Ill., Richard Blumenthal, D-Conn., and Dianne Feinstein, D-Calif., noted that many proposals have been made over the years to streamline or consolidate the U.S. food safety system.
 
At least 30 food safety laws are collectively administered by 15 federal agencies, according to the letter, and the U.S. Department of Agriculture has primarily responsible for the safety of meat, poultry and eggs, while the FDA is responsible for all other food, including produce.
 
Under that regulatory framework, the letter said that a frozen pepperoni pizza must meet USDA and FDA standards, whereas a cheese pizza must only meet FDA standards.
 
"Today's GAO report reinforces the urgent need to improve the food safety inspection system and opportunities to enact meaningful steps toward regulatory reform," the senators wrote.
 
The senators asked that Trump:
Establish a national strategy to address existing shortcomings in food safety oversight;
Create a government-wide performance plan that contains provisions for purpose, leadership, resources, monitoring and actions; and
Work with Congress to request any additional statutory or budget consideration to implement the government performance plan.
 
Jennifer McEntire, vice president of food safety and technology for the United Fresh Produce Association, said efforts by Congress to create single oversight to food safety may involve a lot of time and bureaucracy but with little payoff.
 
"There would be a lot of difficulty getting there and I don't think it will substantially improve public health," she said.