Nor'easter grounds flights, halts trains along East Coast
Nor'easter grounds flights, halts trains along East Coast

By SARAH BETANCOURTAssociated Press
The Associated Press

BOSTON




BOSTON (AP) — A relentless nor'easter pounded the Atlantic coast with hurricane-force winds and sideways-blown rain and snow Friday, flooding streets, grounding flights, stopping trains and leaving hundreds of thousands of people without power from North Carolina to Maine.
Airlines canceled more than 2,800 flights, mostly in the Northeast. LaGuardia Airport in New York City grounded all flights, and John F. Kennedy, also in the city, grounded all but a few departures.
The Eastern Seaboard was hammered by wind gusts exceeding 50 mph, with possible winds of 80 to 90 mph on Cape Cod. Hurricane-force winds of 78 mph were reported off of Wellfleet, Massachusetts.
Powerful winds forced President Donald Trump to fly out of Dulles International Airport on Friday instead of Joint Base Andrews in Maryland, where Air Force One is housed. The airport in Northern Virginia has runways that are more closely aligned with the gusty winds that were kicked up Friday by the nor'easter. Trump flew to Charlotte, North Carolina, to attend the funeral of the Rev. Billy Graham.
The impact of the storm was widely felt. Heavy snow fell in Ohio and upstate New York as the storm spun eastward. Boston south to Rhode Island was forecast to get 2 to 5 inches of snow from the late-winter storm.
Amtrak suspended service along the Northeast Corridor, from Washington to Boston. In New Jersey, a downed tree that hit overhead wires and suspended some New Jersey Transit commuter service.
The storm knocked out power to 700,000 homes and businesses from Michigan to North Carolina. More than 100,000 customers lost power in Washington alone. The federal government all offices in the area for the day. The Smithsonian museums also shut its doors. Pennsylvania utilities report at least 65,000 outages in that state.
The poweroutage.us website, which tracks utilities across the nation, reported early Friday afternoon that Pennsylvania was the hardest hit state with more than 292,700 homes and businesses without electricity.
The front edge of the storm dumped up to a foot of snow on northeast Ohio, with strong winds leading to power outages and school closings. Portions of New York also saw more than a foot of snow Thursday night, downing power lines.
In the Western New York town of Hornell, 30-year-old Anna Stewart was milking the 130 cows of her dairy farm in a barn powered by a generator hooked up to a tractor. Stewart lost power Thursday night. Hornell has gotten more than 14 inches of snow in the past day.
"The snow is pretty wet and heavy. It's taken down a lot of lines," Stewart said. "There's more snow than I've seen in quite a few years."
New York Gov. Andrew Cuomo issued a travel advisory for all areas north of New York City, requesting limited travel due to dangerous driving conditions.
Flooding was reported along a large swath of the East Coast, with coastal Massachusetts seeing among the worst. In Scituate, Massachusetts, officials asked residents in flooded areas to evacuate. Waves higher than two-story houses battered the area, making roads impassable and turning parking lots into small ponds. The Boston Globe reports 1,855 people alerted Scituate officials that they had evacuated.
On the very tip of Cape Cod, Provincetown resident Andy Towle took video of a 50-foot fishing boat breaking free from its mooring, beginning to tip onto the rocks of the breakwater.
"I've never seen anything like that," the 50-year-old resident said. "The Harbormaster was down there with police, and they didn't know what to do."
WFXT-TV caught submerged cars in Quincy, Massachusetts, south of Boston, as residents ignored police urgings to remain off roads. Police rescued many people in partially submerged and trapped cars.
Massachusetts Gov. Charlie Baker activated 200 National Guard members to help with the storm.
"We're expecting to see more severe flooding issues here than we did in the Jan. 4 storm," when a nor'easter lashed the region with heavy snow and rain, he said.
The weather service said all of Rhode Island was under flood and high wind watches through Sunday morning. A truck toppled over on the Newport Pell Bridge due to high winds, prompting officials to close all major bridges in Rhode Island to commercial vehicles, including school buses. A trailer also tipped over on New York's Zappan Zee Bridge, snarling traffic for hours.
___
Associated Press writers Michael Hill in Albany, New York, Michelle Smith in Providence, Rhode Island, and Susan Haigh in Hartford, Connecticut, contributed to this report.