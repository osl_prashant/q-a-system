BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Wednesday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2836  Up    21May         2875    2875    2856    2856  Up    21Jul                                 2847  Up    20Jul         2820    2865    2786    2836  Up    21Sep         2834    2873    2803    2847  Up    20Dec         2817    2855    2788    2829  Up    16Mar         2783    2815    2755    2791  Up    11May         2769    2795    2747    2782  Up    11Jul         2786    2790    2754    2780  Up    11Sep         2784    2793    2777    2781  Up    11Dec         2783    2783    2777    2780  Up    13Mar                                 2788  Up    13