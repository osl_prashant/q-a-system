Stressful weather conditions across the Corn Belt hasn’t been the only thing impacting crops.
States in multiple parts of the country have reported issues with dicamba drift and are raising concerns about crop damage.
Recently, problems with dicamba and drift have been most prominent in Missouri and Arkansas. Illinois is seeing widespread problems with the herbicide.
According to Dr. Aaron Hager, extension specialist with the University of Illinois, says it doesn’t appear there’s any geographical area in Illinois that’s seeing more or less response to dicamba compared to other parts of the state.
“Many of these instances, without a doubt, are caused by dicamba,” Hager told U.S. Farm Report host Tyne Morgan. “Sometimes you walk out into a field and it’s fairly evident that the physical drift has taken place. In other instances, we’re seeing fields where the entire field is affected.”
He said in those instances, there’s no distinctive drift pattern to suggest movement in the area.
“Dicamba symptoms usually show up on the new growth of the soybean,” said Hager. “We’ve had instances where [in] as short as seven days after the exposure, the symptoms develop.”
Depending on environmental conditions, Hager said he has seen it take 21 days between exposure and when symptoms develop.
Will dicamba-tolerant technology and non-dicamba tolerant technology co-exist? Hear Hager’s response on AgDay above.