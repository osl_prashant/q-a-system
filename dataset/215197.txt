Leaving requirements for fruits and vegetables unchanged, the U.S. Department of Agriculture is allowing flexibility in school meal rules for milk, whole grain and sodium.
Effective for the 2018-19 school year, the School Meal Flexibility Rule reflects USDA’s commitment to work with school foodservice operators and other stakeholders to make sure that school nutrition standards are both healthful and practical, according to a news release.

“Schools need flexibility in menu planning so they can serve nutritious and appealing meals,” Agriculture Secretary Sonny Perdue said in the release. “These flexibilities give schools the local control they need to provide nutritious meals that school children find appetizing.”
The USDA said in a news release that it will accept public comments on the rule through late January at www.regulations.gov.
Mollie Van Lieu, senior director of nutrition policy for the United Fresh Produce Association, said in an e-mail that the rule is in line what Perdue has stated before.
“We’re pleased (USDA officials) are continuing to support fruit and vegetable access for students,” Van Lieu said.
The School Nutrition Association was supportive of the USDA rule but said even more flexibility may be needed.
“School nutrition professionals have achieved tremendous progress, modifying recipes, hosting student taste tests and employing a wide range of other tactics to meet regulations while also encouraging students to enjoy healthier school meals,” SNA President Lynn Harvey said in a news release. “Despite these efforts, school nutrition professionals continue to report challenges with sodium and whole grain mandates, as well as limited access to whole grain waivers. 
Harvey said in the release that SNA will provide comments on how to improve the final rule.
The interim final rule will:

Give schools the option to serve low-fat (1%) flavored milk;
Allow states to grant “hardship” exemptions to schools encountering supply problems with whole grain products acceptable to students during the 2018-19 school year; and 
Schools that meet certain sodium limits will be considered compliant.