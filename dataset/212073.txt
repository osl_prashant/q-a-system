A new study reports that urban agriculture in Boston would only reduce food related carbon emissions by less than 3% per year. 
The study, published by the American Chemical Society’s journal Environmental Science & Technology, is called “Contributions of Local Farming to Urban Sustainability in the Northeast United States,” was conducted by Benjamin Goldstein, Morten Birkved and other colleagues, according to a news release.
 
Researchers determined that under optimal conditions, urban agriculture (such as gardens on rooftops and in vacant lots) in Boston would only reduce food-related carbon emissions by 1.1% to 2.9% per year.
 
“The analysis showed that consumers in the northeast who are really determined to lower their food-related carbon footprint should minimize purchases of meat and dairy products,” according to the release.
 
However, the study acknowledged that growing fruits and vegetables in high-density areas could have other benefits, such as giving city residents easier access to fresh produce and increasing local farming income.