BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Friday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMay                              119.40  Down 1.80May      118.90  119.15  116.50  117.20  Down 1.80Jul      121.00  121.30  118.70  119.40  Down 1.80Sep      123.40  123.55  121.00  121.70  Down 1.70Dec      126.65  126.95  124.45  125.15  Down 1.65Mar      130.45  130.45  127.95  128.65  Down 1.60May      132.55  132.60  130.60  131.00  Down 1.45Jul      134.25  134.35  132.75  133.10  Down 1.40Sep      136.05  136.15  134.65  135.00  Down 1.30Dec      138.70  138.80  137.35  137.75  Down 1.25Mar      140.25  140.40  139.95  140.40  Down 1.25May                              142.15  Down 1.25Jul                              143.85  Down 1.25Sep                              145.40  Down 1.25Dec                              147.80  Down 1.25