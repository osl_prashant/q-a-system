Windbreaks, both constructed and planted, can improve conditions for livestock in windy and cold conditions. Increasing the effective temperature that an animal is exposed to during cold weather keeps them comfortable, more efficient users of feed, and at a lower risk of cold stress which can lead to disease.
Design Considerations
The main considerations of windbreak design are windbreak height, orientation, length, and density.

Height
	Windbreak height is the highest point on the structure or tallest row of trees. Generally, the protected zone of the windbreak will extend out 10-15 heights of the windbreak with a 50% reduction in wind-speed. Figure 1shows a conservative example of the protected area calculation for a windbreak.
Orientation
	Orientation of the windbreak is ideally perpendicular to the cold winter wind, given that wind patterns fluctuate around the state, wind roses can be used to evaluate the frequency of wind direction in your area and can be found on the SD Mesonet website.
Length
	Windbreak length is the uninterrupted distance between roads or paths through the trees. Ideally the ratio of windbreak length and tree/windbreak height is 10:1, which means that to develop a full protected zone a 10’ tall windbreak should be 100’ long2.
Density
	The last major design consideration is density, which is the ratio or fraction of solid space in relation to total space. Density impacts the effectiveness of a windbreak by controlling how much wind blows through the windbreak versus blowing over the windbreak. One impact of density is that the denser the windbreak the greater the initial reduction in wind speed, but the wind speed increases faster on the downwind side of the windbreak which decreases the protected area. Additionally, very dense shelterbelts and solid fences create a larger negative pressure area just behind the windbreak which causes snow to build up in large drifts. The target for livestock windbreak density is 60-80%1.

Figure 1

 
Constructing Windbreaks
Windbreaks can be built to be mobile or permanent. The biggest considerations to take into account are the wind load that the structure needs to withstand and the density of the windbreak. Wind pressure loads for a 10 ft high windbreak can exceed 20 lb/ft2 if winds exceed 85 mph. This means that for a solid windbreak (most extreme condition) with posts in the ground every 10 ft that the wind can exert over 2000 lbs of force on each post. Posts of diameter 8” or greater with underground portion below the frost line (3-5 ft depending on location) should be adequate in permanent systems. In mobile systems, the base needs to be broad enough and heavy enough not to tip over or move. Figure 2 is an example of a mobile wind break that is used at the SDSU Cow Calf Unit. An important note is that they connect together and can be set up to create a corner which provides greater protection for multiple wind directions and reinforces each individual section.
Figure 2

 
Determining Density
The density of the windbreak is important to control to increase the size of the protected area, reduce the physical load on the windbreak, and limit snow drift formation on the downwind side. To target 80% density measure the width of the solid material you are using for the windbreak panels, and divide by 0.80, this will give you the center to center spacing that you need to reach 80% density. For example below, using 1 x 8’s for the windbreak we find a spacing of 9.0625 inches. Feel free to round up or down to the nearest quarter inch, if rounding up the density is 78%, if down the density is 80.5%. Which means that between each board there will be a 1.75-2 inch space.

 
Planting Windbreaks
When planting windbreaks the principles described above still apply, but we have less control over the growth characteristics of the plants regarding density. Density is controlled by the types of shrubs and trees planted, their spacing, and how many rows are used. Coniferous trees maintain their leaves throughout the winter and improve the winter time density. Deciduous trees lose their leaves and provide less density in the winter time. Another benefit of living shelterbelts is that they can provide significant snow storage capacity, especially if they are wide; this can also be a detriment however if there is inadequate drainage out of the shelterbelt in the spring thaw. When sizing a shelterbelt determine how many head will be in the pasture at one time through the winter and multiply by the amount of space per head depending on how much space you would like to give them (the lower the space the higher the likelihood of muddy conditions in spring - each situation will be different). Once you have the desired area you can calculate the planting length required using the equation below.

 
The articles in the recommended references section do a good job of describing species selection, spacing, and maintenance as well as further explain uses with livestock.
Conclusion
The purpose of this article is to give you a good starting point and some guidelines on how to begin planning your livestock windbreak. As well as to provide you with some other useful resources as you move forward. The big take-aways are that density, height, orientation, and length determine the protected area. Also, target density should be between 60-80% for wind protection, and that width and species selection determine density in planted windbreaks. One final and important note regarding windbreaks for livestock is that roads, feed alleys and cattle alleys should never be less than 75 ft downwind of a shelterbelt, or 50 ft upwind, as snow accumulation and storage usually occurs within these areas and could cause unnecessary snow blowing/moving duties3. If you have any questions or comments do not hesitate to contact Joe Darrington, the SD NRCS, or any SDSU Extension Beef Educator.

References:

Quam, V., Johnson, L., Wright, B., Brandle, J. (1994) Windbreaks for Livestock Operations. University of Nebraska – Lincoln.
Wilson, J., Josiah, S. (2004) Windbreak Design. University of Nebraska – Lincoln.
SD Department of Agriculture. (2006) Windbreaks and Snow Management. State of SD.
SD Department of Agriculture. (2005) How Windbreaks Work. State of SD.