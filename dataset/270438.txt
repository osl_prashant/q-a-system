Grains lower and livestock mixed
Grains lower and livestock mixed

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were lower Friday in early trading on the Chicago Board of Trade.
Wheat for May delivery lost 8.20 cents at $4.6820 a bushel; May corn was off 3.80 cents at $3.7860 a bushel; May oats fell 1.60 cents at $2.3240 a bushel while May soybeans was down 5.60 cents at $10.3140 a bushel.
Beef was lower and pork was higher on the Chicago Mercantile Exchange.
April live cattle fell 1.35 cents at $1.1720 a pound; Apr feeder cattle lost 1.40 cents at $1.3650 a pound; April lean hogs was up .37 cent at .7025 a pound.