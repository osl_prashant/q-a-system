Johnny White (from left), sales manager for Reidsville, Ga.-based Shuman Produce, and Brandon Parker, sales manager, chat with Randall Levine of Bozzuto’s at last year’s New York Produce Show. 

The Eastern Produce Council will announce on Dec. 13 the start of a new program designed to help young produce professionals.
The Foundation for Excellence Program will be announced at the morning breakfast session of the New York Produce Show on Dec. 13, said Vic Savanello, chairman of the New York Produce Show, Eastern Produce Council president and director of produce and floral for Allegiance Retail Services LLC, Iselin, N.J.
The program, designed by the Eastern Produce Council in conjunction with Rutgers University, will educate a group of young produce industry leaders through the year with various activities.
The program will culminate with graduation of the first class at next year’s New York Produce Show, Savanello said.
 
Show progress
As the event has matured, Savanello said some of the challenges of the earlier years are now a well-practiced routine.
“It really allows you to concentrate on doing the little special things to take the show to the next level,” he said, noting the strong content of the related conferences, the general session breakfast and industry tours.
“We are trying to make (all) those memorable and a part of the show that no one would want to miss,” he said.
On Dec. 12, the Global Trade Symposium will be packed with informative speakers from early in the morning to about 4:30 p.m., he said.
Last year, 11 speakers from across the globe addressed attendees of the event, and attendees from about 30 countries are expected at this year’s show, he said.
“The (Global Trade Symposium) is really my favorite piece of the show, aside from the trade show floor,” Savanello said.
 
Marketplace changing
Savanello said topics related to the changing retail environment will be addressed in the New York Produce Show programming.
With the challenges of online shopping and home delivery, retail competition is fierce in the East and all parts of the country.
“It is a train running almost off the tracks in terms of trying to keep up with trends and technology,” he said.
The strong buyer audience for the show is one of its hallmarks. Buyers receive free admission to the expo but no other special incentives to come to the show, he said.
Asking buyers — or anyone — to come to New York in the Christmas season for the show is attractive enough, he said.
“The show itself has become so relevant to our industry — it is the second largest produce show in the Western Hemisphere next to PMA (Fresh Summit) — it has become one of those ‘can’t miss’ events,” he said.
Susan McAleavey Sarlund, executive director of the Eastern Produce Council, Short Hills, N.J., said members are interested in keeping up with retail industry changes, from the growth of Lidl to Amazon’s purchase of Whole Foods to changing consumer shopping habits.
“That is something that everybody is watching and everybody has to respond to,” she said.
Event organizers say the New York Produce Show will help the industry catch up and perhaps get ahead of some of those trends.