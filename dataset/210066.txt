Florida Gov. Rick Scott and Florida Agriculture Commissioner Adam Putnam will be in Washington D.C. to talk about the recent hurricane damage to citrus.
They are scheduled to meet Oct. 11 with members of the Florida congressional delegation to review the issues caused by Irma, according to a news release.
The Florida Department of Agriculture and Consumer Services released a preliminary damage report Oct. 4, including $760 million in losses for citrus.
The counties hit hardest were Collier and Hendry, which The Packer recently visited to see the damage and talk with growers about the effect on their businesses.
In addition to fruit drop and infrastructure damage, growers are concerned that flooding will affect the long-term health of trees.
The Florida citrus industry has contracted severely in the last decade due to citrus greening disease, but sales are still about $1 billion annually.