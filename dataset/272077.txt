The GDP looks as though it will grow 3.3 percent over the next year, which could be a 15-year according to the Congressional Budget Office. However the ag economy is on a different path.

Wednesday marks the one-year mark that Secretary of Agriculture Sonny Perdue was sworn into office. During the Senate Committee on Agriculture, Nutrition and Forestry, Perdue took the stand to testify on the state of the rural economy.

Perdue said farm finances are still a worry to many people, and there are shared concerns about the fate of the farm bill and rural infrastructure.

“There’s a lot of anxiety out there, [and] you’ve enumerated some of that,” said Perdue during his testimony. 

The panel questioned Perdue about the Renewable Fuel Standard (RFS) and trade policies, both of which impact the farm economy. 

Perdue said he continues to applaud President Trump for standing up to China regarding the trade deficit and intellectual property theft from U.S. companies.

"I have to go back to the president looking me in the eye and saying, ‘Sonny, I know farmers out there are worried about what we're doing, but you need to tell them that I'm not going to let them be the casualties in this trade dispute,’” said Perdue. “[Farmers] like people who play by the rules and that’s what the president is doing with calling China’s hand.”