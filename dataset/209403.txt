Sponsored Content
Soil seems like a long way from the milk aisle but one Wisconsin dairyman is making caring for the land a top priority for producing a top product and turning into a partnership between rural and urban.
For Jeff Endres of Endres BerryRidge Farms, the responsibility of a good producer goes further than just working with cattle -it starts in the soil.
“We always need to be striving and doing things and looking for new ideas that help with water quality,” said Jeff.
His perspective runs in the family, passed down through the generations from Jeff’s grandfather, who made his own conservation plan in the 1940s. Now, this fourth-generation farmer is making sure the land will see longevity well past his daughter, Sarah.
“In order for that to happen, we need to be good stewards of the land, be progressive and innovative with our new technologies,” said Sarah.
It’s more than a thought- it’s an effort.
“You have so much more to gain by being out in front,” said Jeff. “[That includes] leading it and being proactive, looking for the new ideas and new answers.”
Jeff has taken steps to preserve water quality and get others involved. That way regulations don’t regulate farmers out of business.
“If you regulate a farmer, now you’ve taken away a number of options that we may never had even thought of before,” said Jeff.
He founded a group called “Yahara Pride Farms” in 2011.
He lives within the Yahara Watershed, a 540 square mile water shed now threatened by runoff, urban storm water and waste water.
Unlike the recent battle in Des Moines, Ia, where it was a rural versus urban battle over runoff, the city of Madison, Wisc. and other communities are collaborating with farmers, environmental services and residents to manage the problem. The partnership shows rural and urban interests.
“I think that’s really unique and key to be successful in the future,” said Jeff.
Producers like Chuck Ripp with Ripp’s Dairy Valley also lives in the watershed. He joined the Yahara Pride Farm’s effort too.
“The dairy farmers here are very aggressive and progressive,” said Ripp. “We’ll make the moves we need for us to be able to stay profitable and to be able to stay in business for generations to come.”
In addition to the collaboration, a project to reduce phosphorus over a 20- year period was created. Financial contributions are given to farmers for storm water retention ponds, harvestable stream buffers, strip tillage and manure injection. 
“We got a couple of practices that we brought to the watershed,” said Jeff. “The county and others are supporting cost share programs for farmers to upgrade equipment so they can actually do those practices on the land.”
Participating farmers document confidential data, the farm’s energy use and greenhouse gas emissions measuring to see how they can improve and what they’re doing well.
“It took time,” said Jeff. “We would get 30 to 40 percent of data from farmers on what was happening out in the fields. In 2016, we finished our phosphorus report and we had 100 percent participation with the farmers involved in our program.”
On his own operation, Jeff is stock piling and composting liquid and solid manure together. It’s a sustainable way to manage manure, creating a profit that’s perfect for farm fields.
“We do see a little bit of a yield bump where we use the compost,” said Jeff.
It’s not a run off risk if used correctly.
“There’s no longer a threat for run off,” said Jeff. “It actually absorbs moisture.”
Jeff has a pride for his farm, now reaching beyond the barn.
Jeff encourages farmers who want to focus more on environmental efforts to do something as simple as share practices they’re doing with customers or get involved in their local government. He says it’s critical because the local government makes decisions for your region.
Sponsored by Balchem