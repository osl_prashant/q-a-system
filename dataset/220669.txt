Australia is one of the U.S. beef industry’s biggest competitors, and dry weather is stressing the Australian herd.

The Hunter Valley region in the southeastern corner of the country is dealing with severe drought, and livestock owners are fighting to save their animals, culling some that are too weak to move.

Hear ranchers talk about how the drought isn’t only impacting the livestock, but farmers’ mental health as well on AgDay above.