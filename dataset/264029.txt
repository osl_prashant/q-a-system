A dozen Haggen grocery stores in Washington are recalling deli products that contained diced yellow onions from Taylor Farms.
The items, including potato salad, cashew chicken croissants/wraps/salads and fresh tartar sauce, all made with raw fresh-cut onions from Taylor Farms, may be contaminated with salmonella, according to a news release from Haggen.
No illnesses have been reported in connection with the onions, according to the release. Taylor Farms and the Food and Drug Administration have not posted recall notices in connection with the Haggen announcement. Other retailers have not issued recalls as well.
The products, partial Universal Product Codes and sell by dates are:

Old fashioned potato salad, 202932, March 15-19;
Cashew chicken salad, 202928, March 15-19;
Turkey curry salad, 202986, March 15-19;
Cashew chicken salad wrap, 203681, March 15-19;
Cashew chicken croissants (2-pack), 203908, March 15-19;
Cashew chicken croissant tray, 203812, March 15-19;
Tartar sauce, 202804, March 15-19; and
Tartar sauce (served as condiment with fish and chips), 206000/206001/206004/206006, March 13-17.

The items were available from self-service and full-service deli areas at the stores, according to the release.
The Washington stores are in Oak Harbor, Bellingham (2 stores), Burlington, Olympia, Stanwood, Snohomish, Ferndale, Woodinvile, Mount Vernon, Lake Stevens and Marysville.