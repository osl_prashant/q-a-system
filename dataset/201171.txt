Crop calls 
Corn: 4 to 6 cents higher
Soybeans: 4 to 6 cents higher
Winter wheat: 10 to 15 cents higher
Spring wheat: 15 to 20 cents higher

Spring wheat futures ended the overnight session mostly around 14 to 15 cents higher, but we anticipate stepped up fund buying at the start of daytime trade due to the hot and dry forecast for early July across the Northern Plains and Canadian Prairies. Corn, soybean and winter wheat futures once again benefited from spillover overnight from spring wheat futures. However, with traders also evening positions ahead of the 11:00 a.m. CT, USDA Acreage and Grain Stocks Reports, we can't rule out a highly volatile day of price action.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Spreading is expected in both cattle and hog futures today as traders close their books on the second quarter of the year. Cash cattle trade is thought to be mostly complete, largely occurring between $118 and $120 -- down from $121.50 last week. June futures, which expire today at noon CT, are trading in line with the cash market, while August futures hold around a $3 discount to the cash market. Yesterday's Hogs & Pigs Report didn't hold any big surprises, which keeps traders' focus on the cash market that firmed yesterday.