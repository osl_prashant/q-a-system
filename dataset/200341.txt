As you are all well aware, the new FDA ruling will begin on January 1, 2017. This is a good time to cover all of the bases once more before everything goes into full regulations.What Does This Mean for Producers?
FDA has taken action to promote the judicious use of medically important antimicrobial drugs in food animals. The goal of the strategy is:
To phase out the use of medically important antibiotics in food animals for production purposes (e.g., to enhance growth or improve feed efficiency)
Bring the therapeutic uses of such drugs (to treat, control, or prevent specific diseases) under the oversight of licensed veterinarians.
What Types of Drugs Will be Targeted?
This action focuses on Over the Counter (OTC)feed additive antibioticsthat are considered "medically important" human drugs such as:
Penicillin & Tetracycline
These products were used with the intent of enhancing growth or improving feed efficiency.
Examples of an OTC feed additive that a producer can purchase are:
Aureomycin, Aureo S 700 (CTC, Chlortetracycline) (Also mixed in minerals)
Terramycin (Oxytetracycline)
Tylan (Tylosin)
Neomycin (Neomycin Sulfate)

How will producers be able to purchase feed additive antibiotics?
The producer will have to have a valid client patient relationship (VCPR) with a veterinarian. Since there only a couple of weeks until these regulations begin, it is highly recommended that you establish a VCPR with a veterinariannow. There are a few steps required to establish a VCPR. These are:
The licensed veterinarian has assumed clinical responsibility for the animals and the owner of the animals has agreed to follow the veterinarian's instructions.
The veterinarian has sufficient direct knowledge of the animal's condition and their care and has examined the producer's animals in the past 12 months , or annual visits to the premises where the animals are kept
The veterinarian is available for follow-up evaluation, or has arranged for emergency coverage in the event of adverse reactions or the failure of the treatment.
A VCPRcannotbe established nor maintainedsolelyby telephone or other electronic means.
Veterinarians will still be able to prescribe feed grade antibiotics, ONLY if deemed necessary to treat, or control a current disease outbreak. After the prescribed time period has elapsed, the antibiotic will have to be removed from the feed. Using a feed grade medication for any other purpose than is on the label is extra label use and is illegal. One way to view this is; the bag label trumps everything!
What is Extra-Label Drug Use?
Extra-label drug use can only be prescribed by a veterinarian.
A cattle producer cannot use drugs (including OTC drugs) in an extra-label manner without a veterinarian's prescription. This isOFF-LABELandILLEGAL.
Administering products according to label directions includes dosage, route of administration, reason for administration, adhering to proper withdrawal times, etc.
A Veterinarian-Client-Patient-Relationship (VCPR) must exist for extra-label drug use:
The veterinarian knows the producer and is familiar with the farm or ranch and its practices, and is involved in the herd health practices on the operation.
The veterinarian and producer must make sure the animal is properly identified, assign meat and milk withdrawal times, and abide by these withdrawal times to ensure no illegal residues occur.

Extra-label use of feed medications is prohibited.Even for the veterinarian!!Under the new FDA VFD regulations, it will be imperative that the producer and the veterinarian complies exactly with the instructions on the feed grade antibiotic label
What is a Veterinary Feed Directive (VFD)?
A VFD is actually an order from your veterinarian to add a feed grade antibiotic to the ration to control, prevent, or treat a disease. There will be some rules that the producer will need to pay close attention to. These are:
The VFD will only be valid for 6 months, then a new VFD will need to be written.
If the VFD expires, and there is medicated feed remaining, it cannot be fed until a new VFD is written for that mixture of feed.
If the producer has cattle in separate physical locations, and each separate group of cattle is being fed a medicated feed, a separate VFD will be required for each physical location.
The producer, veterinarian, and licensed feed dealer will be required to keep a copy of the VFD for 2 years.
A VFD will be required for minerals/or mixes that contain an antibiotic e.g. Chlortetracycline (CTC).
What about OTC Injectables?
As ofright nowOTC injectableswill not be involvedin this ruling, but they are next in line to become script items. Most likely this will not occur anytime soon as a time frame has not been established as to when this will occur.
What Won't Change?
Producers will still be able to obtain and use medications that are considered non-"medically important" feed grade products. These include:
Ionophores such as Rumensin, Bovatec,
Coccidiosis medications. These medications are rarely used in human medicine, so their use will not be changed.