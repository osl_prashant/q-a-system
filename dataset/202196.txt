Livestock economists agree that the record volume of protein anticipated this fall will have an impact on prices, and the big question is whether or not export markets will remain viable. PORK Network visited with Don Close, vice president of food and agribusiness marketing for Rabo Agrifinance.Close said the quantity of protein will be a challenge but in spike of the fact, "we've done well considering the amount of storage we have anticipated.
"There are concerns on the pork side," he added. Production increased in anticipation of new packing capacity, but those plants aren't going to be on-line until next year, or later in the case of the Prestage plant in Wright County, Iowa.
Exports Tenuous"The recovery of the dollar since the Brexit vote has raised a red flag," Close said. "But even though the relationship of the dollar has shifted, we feel it will correct itself."
China to Re-EngageClose feels the U.S. livestock industries have "the opportunity to grow with China." Even though the country is seeing recessionary pressure, he expects China to ramp up its exports from the U.S. in both grains and meats.
To remain viable, Close, who specializes in animal protein markets, said producers will need to exercise discipline to keep their cost of production in line.
Watch the video here:


<!--
LimelightPlayerUtil.initEmbed('limelight_player_499020');
//-->