Growers can learn about technology that can aid them in optimizing data to forecast yields and manage their operations better, through Western Growers’ next Tech Talk.
The Oct. 24 event, in person or online, features Cyrille Habis, CEO and co-founder of AgriData. The company is one of 48 ag-tech startups in the Western Growers Center for Innovation & Technology in Salinas, Calif.
AgriData provides technology to better estimate yields in vineyards and orchards several weeks ahead of harvest, according to a news release, through data and images captured from cameras mounted to farm equipment.
The Tech Talk will focus on:

Measuring crops with precision;
Evaluating attributes such as maturity, diseases and pests; and
Managing operations better by identifying low-yield areas.

The free Tech Talk will be 3-4 p.m. Pacific Oct. 24 at the center in Salinas, or online. Attendees must register online.