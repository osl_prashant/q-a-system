Wondering what to get dear old dad for Father's Day?Here's a suggestion that would likely be a big hit ‚Äî with the whole family.
A New Zealand scientist and an Auckland chocolate company, Devonport Chocolates, have developed what is arguably the most intriguing combination of elements since hydrogen hooked up with oxygen: A chocolate confection that is 50% beef.
According to a report in the New Zealand Farmer magazine, this prototype of a meat lover's chocolate ‚Äî or should that be, a chocolate lover's meat? ‚Äî uses dairy beef that is processed and encased in chocolate.
Well, not exactly. The beef doesn't come from older cows that typically get turned into hamburger, but according to the developer. from younger dairy bulls that are only 18 to 20 months old ‚Äî all Waikato-farmed, 100% bull beef," according to the developer.
He is Mustafa Farouk, a scientist on staff with a New Zealand organization known as AgResearch, and his innovative combination has created a "candy" with the consistency similar to a Turkish taffy, he claimed.
AgResearch is described as an organization that "partners with the agricultural sector to deliver the innovation needed to create value for New Zealand farmers and producers. Its scientists and researchers develop projects in such areas as pasture-based animal production systems, biosecurity and soil and water resource management.
Oh, and one other area: "agri-food and bio-based products."
Which is how the chocolate-meat confection came about.
Where's the beef?
The idea of adding meat to chocolate emerged as Farouk was trying to develop ways of adding value to beef, "with an eye on how it could be consumed by people in the future," the magazine reported.
Future? We've got to wait for the future to start eating chocolate beef?
"We knew we could turn meat into different forms, but whether we could actually fool people by making it look like chocolate is what we didn't know," Farouk said. "When you try it, you don't know what you're eating."
The innovative product could have "huge potential benefits" for athletes in various sports people or for elderly people wanting an easily consumed, high-protein product, he claimed.
But here's the weird part of this experiment: All the meat flavor and texture has been removed, according to Farouk. He told New Zealand Farmer that, "Unless [they were] told, people would have no idea they just ate something containing beef."
Here's how he did it.
The basic substrate is lean meat from the hind quarters of a grassfed animal, from which the protein is extracted. Turmeric and other spices are added to give the product a distinctive flavor. It is turned into a "chocolate butter" before being sent to Devonport Chocolates, an Auckland-area family business specializing in hand-dipped chocolates, pralines and truffles, where the squares are coated with a chocolate casing.
Farouk estimated that each square would contain about 25% protein, as well as iron and zinc.
"All the goodness of meat will be in there," he said. "So far, when we have talked to people about it, and they understand its advantages over other chocolates, they get very excited."
We'll have to take Farouk at his word on the alleged excitement.
Intrigued? Probably. Curious? Very likely.
And chocolate beef's price point seems to be reasonable enough to encourage its adoption as a go-to confection.
The beef used in manufacturing the chocolate covered beef costs about $17 a kilo ($7.72 a pound). When converted into a chocolate-covered treat, it would retail at about $2.50 a cube, not all that different from prices for Devonport's hand-dipped specialties not containing meat.
It may be awhile before this invention hits the streets in the USA, but there's more good news for that untapped segment of consumers who've been waiting for a product that combines the nutritional value of meat with the indulgent taste of chocolate.
Farouk is now turning his attention to lamb and venison.
Dan Murphy is a food-industry journalist and commentator