BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Tue.      Mon.No. 2 Soft wheat              4.77½     4.81¾No. 1 Yellow soybeans        10.23¾    10.16  No. 2 Yellow Corn             3.71¾e   3.78¾eNo. 2 Yellow Corn             3.85¾p   3.84¾p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted