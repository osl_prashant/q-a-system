Buoyed by big supply of red delicious and gala apples, May 1 fresh market apple holdings were 38.7 million cartons, up 16% from a year ago and 7% more than the five-year average of 36 million cartons. 
The U.S. Apple Association reported fresh market holdings on May 1 were 38.7 million cartons, up from 33.4 million cartons in 2016 but down from 44.9 million cartons to 2015.
 
Washington fresh apple holdings of 35.9 million cartons accounted for 93% of total U.S. fresh supplies, according to the apple association.
 
Red delicious inventories on May 1 were 16.2 million cartons, up 41% from 11.5 million cartons a year ago but down 10% from 18 million cartons two years ago.
 
The U.S. Department of Agriculture reports the average shipping point price for Washington traypack red delicious apples in early May was $14.40 per carton, down from $20 per carton at the same time last year but up from $13.60 per carton two years ago.
 
Fresh market gala inventories were 6.5 million cartons, up 55% from 4.2 million cartons a year ago but down 6% from 6.9 million cartons two years ago.
 
Big inventories of galas will dampen demand for imported New Zealand and Chilean galas this summer, said Brent Steensma, sales and marketing representative for Honey Bear Fruit Co., Wenatchee, Wash.
 
The USDA reported that the average price for Washington gala traypack apples in early May $18.90 per carton, down from about $30 per carton a year ago but up slightly from $18.30 per carton two years ago.
 
Fresh market fuji inventories on May 1 were 4.1 million cartons, up 35% from 4.2 million cartons a year ago but 5% lower than 4.3 million cartons two years ago.
 
Inventories of granny smith apples on May 1 totaled 3.3 million cartons, down 46% from 6.06 million cartons a year ago and off 39% from 5.39 million cartons two years ago.