Hemp gains powerful ally to free it from marijuana ties
Hemp gains powerful ally to free it from marijuana ties

The Associated Press

FRANKFORT, Ky.




FRANKFORT, Ky. (AP) — Hemp advocates have a powerful ally in Senate Majority Leader Mitch McConnell. The Kentucky Republican said he will introduce legislation to legalize hemp as an agricultural commodity by removing the plant — prized for its oils, seeds and fiber —from the list of controlled substances.
A look at hemp's tortured past and its prospects for the future:
WHAT'S THE DIFFERENCE BETWEEN HEMP AND MARIJUANA?
Hemp and marijuana come from the same species of plant, but hemp has a negligible amount of THC, the psychoactive compound that gives marijuana users a high.
THEN WHY IS HEMP ILLEGAL?
The family connection sidelined hemp production for decades. Growing hemp without a federal permit was banned due to its classification as a controlled substance related to marijuana.
The crop got a limited reprieve when Congress passed the 2014 Farm Bill. A provision pushed by McConnell allowed state agriculture departments to designate hemp research and development projects.
HOW IS HEMP USED?
Supporters tout the versatility of a crop valued for its oils, seeds and fiber. Hemp has historically been used for rope but has hundreds of other uses, including clothing and mulch from the fiber, hemp milk and cooking oil from the seeds, as well as soap and lotions. Other uses include building materials, animal bedding and biofuels.
WHAT WOULD McCONNELL'S PROPOSAL DO?
McConnell wants a full pardon for hemp by removing it from the list of controlled substances. That would restore hemp as a legal agricultural commodity, he says, and "allow this industry to continue to flourish here in Kentucky."
ARE MORE FARMERS GROWING HEMP?
Since the 2014 federal Farm Bill allowed experimental hemp production, crop production has spread but remains tiny.
So far, 34 states have authorized hemp research, according to the advocacy group Vote Hemp. Actual production took place in 19 states last year. Hemp production totaled 25,541 acres (10,336 hectares) in 2017, more than double the 2016 output, said Eric Steenstra, president of Vote Hemp.
In McConnell's home state of Kentucky, agriculture officials recently approved more than 12,000 acres (4,856 hectares) to be grown in the state this year, and 57 Kentucky processors help convert the raw product for other uses.
HOW WOULD MCCONNELL'S BILL WORK?
McConnell said his bill seeks to build on pilot program successes and has bipartisan backing in the Senate.
The U.S. Department of Agriculture would have to approve of states' implementation plans, but once that happened, states would be the primary regulators of hemp. The bill also would allow hemp researchers to apply for competitive federal grants from USDA.