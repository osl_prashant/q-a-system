GlobalVetLINK (GVL®), a leader in digital animal health compliance solutions, today announced a significant investment led by Lead Edge Capital. 
 
The GVL platform connects animal health stakeholders to simplify regulatory compliance by offering real-time, web-based solutions to improve animal wellness and safety.
 
In the past 24 months, GlobalVetLINK has experienced unprecedented growth and is on a continued upswing – due to a pressing need for technology-based solutions for regulatory compliance within the animal health industry, and GlobalVetLINK’s dedicated investment to its infrastructure and SaaS platform. This growing demand for technology-based solutions in agriculture and animal health markets makes GVL optimally poised for continued growth. 
 
“GlobalVetLINK is the leader of the veterinary compliance space, and we see huge opportunities for their technology on an even broader scale,” Nimay Mehta, Partner at Lead Edge Capital, said. “Its success is the result of the company's bulletproof leadership, and clear vision to solve complex regulatory processes that veterinarians consistently deal with. We're eager to support this customer-centric approach and provide the resources and guidance to help GlobalVetLINK continue on this path.”
 
“Lead Edge Capital shares our vision and enthusiasm to further serve the animal health market,” Clifford Smith, GlobalVetLINK CEO, said.“The partnership will enable GlobalVetLINK to focus on owning our key markets and relationships with our customers, while maintaining the GlobalVetLINK mission and vision.”
 
As part of this transaction, Nimay Mehta, Paul Bell, and Brian Lobdell of Lead Edge Capital will take a seat on the GlobalVetLINK Board of Directors. Lead Edge Capital invests in growth stage software and internet businesses and was represented by Goodwin Procter LLP. 
 
Learn more about GlobalVetLINK at www.globalvetlink.com.