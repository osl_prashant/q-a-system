The Common Swine Industry Audit (CSIA) was announced in October 2014 by National Pork Board. In October 2015, it was revised by the Industry Audit Task Force with feedback from producers, packers, auditors and customers. CSIA was developed by a task force of industry stakeholders of producers, veterinarians, animal scientists, packers, processors, retail and foodservice representatives. The task force developed the audit considering scientific evidence, ethics and economics, which must be balanced for sustainability.
Each processing plant using the CSIA do not have a specific passing score that producers are required to meet, but this does not mean a producer should not comply with all aspects of the audit. For every part of the audit the producer do not pass, they are required to fill out a corrective action sheet. Each corrective action sheet requires the producer to give a description of nonconformance, the root cause for the nonconformance and what what the corrective action is with expected date of completion.
Auditors are looking to make sure everyone working in the barn canarticulate to the auditor what their jobs are in the barn and how they perform them. Auditorsalso will be looking atrecord keeping, Standard Operating Procedures (SOPs) and making sure everyone in the barn is performing their jobs according to the farms SOPs and industry standards. Other areas include animal care, animal benchmarks and facilities.
Required Standard Operating Procedures (SOPs)
Piglet Processing (if applicable)
Feed and Water SOP
Animal Handling Procedures
Daily Observations
Caretaker Training
Treatment Management
Needle usage
Biosecurity
Rodent Control
Visitor log
Records
12 months of mortality records
12 months of daily observation
Written emergency action plan
Emergency contact numbers posted
Written euthanasia plan
Valid Veterinarian Client Patient Relationship (VCPR)
12 months of medication and treatment records
Veterinarian Feed Directive records (according to FDA guidelines)
Other Documentation Needed
Willful acts of abuse/zero tolerance policy
Abuse reporting
Annual caretaker training
PQA Plus certification for all current employees. New employees must be certified within 90 days of employment
TQA certification for recent transporter loading or delivering pigs at site
Valid PQA Plus Site Assessment (required before pigs are marketed or sold; renew every three years like PQA Plus)
Internal site assessments - quarter for sow units and semi-annually at nurseries and finishing barns
Emergency action plan - must be posted
Biosecurity signage or other means to restrict access
Goal of Audit
To provide consumers greater assurance of the care taken by farmers and pork processors to improve animal well-being and food safety. The audit tool builds on the existing Pork Quality Assurance®Plus (PQA Plus®) program and expands it to serve as a single, common audit platform for the pork industry.
Questions
Please contact Sandra Kavan at (402) 472-0493 orvia email