BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Tuesday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeMar                                12.62  Down .31Apr        12.93   12.95   12.53   12.62  Down .31May                                12.89  Down .27Jun        13.16   13.19   12.80   12.89  Down .27Sep        13.51   13.53   13.18   13.27  Down .24Dec                                14.21  Down .12Feb        14.32   14.34   14.05   14.21  Down .12Apr        14.40   14.50   14.22   14.39  Down .12Jun        14.54   14.56   14.35   14.51  Down .13Sep        14.83   14.83   14.67   14.81  Down .12Dec                                15.31  Down .09Feb        15.25   15.31   15.15   15.31  Down .09Apr        15.20   15.32   15.17   15.32  Down .06Jun        15.26   15.41   15.26   15.41  Down .02Sep        15.58   15.65   15.45   15.65  unch