The crisis at Huishan Dairy, one of China's biggest dairy companies, is a stark reminder of what can lurk in the dark corners of corporate China, where rapid growth can go hand in hand with tangled finances and heavy debt.China Huishan Dairy Holdings Co Ltd embraced what its executives called "innovative financing", from the sale and leaseback of its cows, to selling wealth management products for rich investors - financial antics that seem incongruous with the dusty fields, tin-roofed sheds and plastic greenhouses of Zhangwu county in northeast China.
Now it is battling swollen liabilities, a short-term debt squeeze and considerable unwanted attention. After a late 2016 short-selling attack, it saw an 85 percent drop in its shares in a single day last month, wiping $4 billion off its value and triggering a stock suspension.
It has reported a key finance executive missing.
Its misfortunes are a reminder that even as banks' bad debt numbers stabilize, there remain many question marks over the quality of their balance sheets. Those exposed to Huishan include Industrial and Commercial Bank of China, Agricultural Bank of China, regional lenders, leasing companies and online loans firms.
"When you move down to the local lenders in less developed provinces and counties, there could be hundreds and thousands of similar cases to Huishan, albeit at a smaller scale," said Shawlin Chaw, Control Risks analyst focused on Greater China.
Missed Payments
In 2013, retail investors flocked to Huishan's $1.3 billion (¬£1.04 billion) Hong Kong listing, which was priced at the top of its forecast range.
The provincial government was vocal in its support.
"Next year, we can all go to work at Huishan Dairy!" Liaoning government slogans proclaimed, in reference to the promised creation of tens of thousands of local jobs.
Now that Huishan has missed debt payments, that same government has brokered meetings between the dairy and its 23 creditor banks, including big names such as Bank of China Ltd, AgBank and Ping An Bank Co Ltd.
Local officials declined to comment for this story.
Falling milk prices and rising feed costs had caused problems for the region, local farmers said.
"This year is the worst I've seen for the dairy cattle industry," said Shan Jiawu, a dairy farmer in Zhangwu, who has been in the business for over a decade.
Around the county where Huishan has dozens of farms, guards at two of three operations visited by Reuters said the farms were functioning normally, though reporters were not allowed in. A third was shut.
"All the company's operational activities are being carried out in an orderly manner. I believe we will quickly solve the problems we are currently facing," said an official in Huishan's publicity department.
Huishan's latest official statement, issued late on Friday, said it would need more time to verify its financial position.
Creative Accounting
Huishan has been open about its creative finances.
In November it pledged 40,000 dairy cows to a financial leasing firm for a 750 million yuan (¬£87.26 million) loan, the second such deal it attempted. It said it would repay in 10 instalments from May.
Its accounts indicate it sold wealth management products.
China's banking regulator, CBRC, did not comment.
Its top shareholder Champ Harvest, controlled by Huishan chairman Yang Kai, pledged nearly all the shares it owns to secure loans and margin financing.
Huishan's accounts paint a picture of accumulating short-term loans and a deteriorating current ratio that indicates its liabilities are larger than its assets.
Last year, finance executive Ge Kun, in charge of treasury functions and cash, said the company, fresh from the cow leasing deal, would consider adopting "innovative tools in the future".
She is now missing.
Ge, who managed relationships with Huishan's banks, was named a "model worker" by the provincial capital in 2012 and a year later was a representative for the district People's Congress, according to Huishan's IPO prospectus.
But Huishan said she was suffering from work stress, would take a leave of absence and did not wish to be contacted. It has now filed a missing person report in Hong Kong.
Reuters' attempts to contact Ge, including a visit to a listed address for her in Shenyang, were unsuccessful.
"If similar stories are anything to go by, it is likely that the stock will remain suspended, and the issue is unlikely to be resolved for a long time," said Robert Medd, forensic accountant at Bucephalus Research.
For some in Zhangwu, such problems seem outlandishly remote.
"As long as they have good products and we can sell like normal, that's OK," said Bi Junkui, 45, a wholesaler of Huishan products in Zhangwu's main town.
"If the bosses lose so much money, it's not my business."