I must confess. I dislike grocery shopping.I like to cook. And I love to eat. But grocery shopping is not my forte.
It was grueling as I made my weekly trek to the grocery store. Pulp-free or extra pulp orange juice? Crunchy or smooth peanut butter? Vanilla or chocolate ice cream!? (I went with both.)
After spending 45 minutes browsing the aisles, I was ready to check out. Looking around, I noticed the different types of foods that are available. Conventional, organic, GMO-free. The choices abound. Just like our farming practices.
It made me think about food price versus food preference.
There's a market for every product. Consumers, like you and me, are willing to pay for our preferences. Some preferences are more expensive.
There's nothing wrong with buying organic or GMO-free foods. They're both grown by farmers who have the same goal‚Äîgrowing safe, healthy foods. The same can be said of farmers who grow food conventionally. Although conventional foods are usually less expensive.
It's all about choice. We have the safest, most affordable and abundant food supply. And, as shoppers, we should feel good about our choices.
But price oftentimes plays a factor. And so does preference.
Which comes first?
That's a question each of us answers as we reach for our food choices. Farmers balance the interests of each. There's not a right or wrong answer. It's about deciding what's best for us individually. And the grocery store provides that opportunity every time.