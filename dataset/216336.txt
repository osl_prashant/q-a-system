California saw about half its avocado crop in 2017 compared to the previous year, and growers were optimistic for a big 2018 to capture ever-growing demand.
Then fires hit production areas in December.
Overall, 2017 was a year of lower-than-hoped-for volumes and higher prices, but consumer demand continued to grow. Growers in the U.S. and top export markets expect record volumes in 2018, which should make retail and foodservice buyers happy.
 
Dec. 18 – Damage to avocados could be significant as fire blazes on
By Tom Karst
More than a week after it began, firefighters reported California’s massive Thomas fire was only 25% contained the evening of Dec. 12.
Damage to the avocado industry was still undetermined but thought to be significant.
“The fire has stayed very active in Ventura and Santa Barbara counties,” said Tom Bellamore, president of the California Avocado Commission, Irvine.
Ventura County has more than a third of California’s avocado acreage and Santa Barbara County also has avocado groves. Bellamore said it is hard to know how many groves were destroyed and how many were merely damaged.
 
Nov. 6 – Volume out of Mexico to top last season’s
By Tom Burfield
Avocado volume out of Mexico should reach nearly 2 billion pounds during the current season — July 1 to June 30 — an increase from last year, despite a slow start during the summer.
Volume could be 15% more than last year, said Rob Wedin, vice president of sales and marketing for Calavo Growers Inc., Santa Paula, Calif.
But since fruit size will be slightly smaller, the number of avocados actually could be 20% greater than last year, he said.
 
Oct. 23 – Avocado sales outpace other produce in August
By Tom Karst
U.S. retail avocado sales growth for the four-week period ending Aug. 13 was more than three times the sale growth of all produce sales.
The Hass Avocado Board reported that avocado dollar sales growth for the four-week period ending Aug. 13 was up 9.8%, compared with 3.1% growth for all produce sales.
Average selling prices for avocados for the four-week period ending Aug. 13 were up an average of 7.2% compared with year-ago level.
By region, the Hass Avocado Board reported that average selling prices were up 15.1% in the Plains, 15% in the Southeast and up 7.8% in the South Central region.
Smallest average retail price increases were observed in the Great Lakes region and mid-South (both up 2.7%).
 
Sept. 11 – Hot avocado market squeezes consumers
By Tom Karst
Hass avocado prices may continue at dizzying levels through at least late September as reduced volume from Mexico has pushed shipping point prices above $80 per carton on some sizes and sent wholesale prices soaring near $100 per carton.
With 40-count Mexican hass priced at $83 per carton on Sept. 5, the U.S. Department of Agriculture reported the average shipping point price for all sizes of hass fruit in early September was $73 per carton for conventional fruit, up about $20 per carton from a year ago.
Total domestic and imported supply of avocados for the week of Aug. 27 to Sept. 2 totaled 27 million pounds, down 30% from the same week last year. Mexico accounted for 74% of total avocado supply for the week of Aug. 27, followed by 15% from Peru and 10% from Chile.
 
Aug. 14 – Avocado supplies expected to rise
By Tom Burfield
Sources in the avocado industry differ on whether U.S. avocado volume will reach the projected volume increase over last year, but everyone seems to agree that 2018 will be a big one for avocado lovers.
Conventional avocado volume is projected to reach 2.28 billion pounds in 2017, according to the Mission Viejo, Calif.-based Hass Avocado Board — a 90 million-pound increase from 2.19 billion pounds in 2016.
“We’re in really good shape through the first six months (of 2017),” Emiliano Escobedo, executive director, said in late July.
Those figures include fruit from California, Peru, Chile and the Dominican Republic.
 
June 5 – Peru looks to double avocado shipments to U.S.
By Jim Offner
The U.S. avocado market is ripe for the start of the Peruvian season, as Mexico’s crop winds down and California volumes are about half of what they were in 2016.
Peru is expecting to ship about 150 million pounds of avocados to the U.S. for its June-August season — about twice its total of about 70 million pounds last year.
It’s shaping up to be Peru’s largest avocado crop to date, and it comes at a time when short supplies and growing demand are combining to boost prices to record levels, marketers said.