The2017 Great Lakes Forage and Grazing Conferenceis scheduled for Wednesday, March 1, at AgroLiquid Fertilizer,3055 W. M-21, St. Johns, MI 48879. Everyone is invited to join forage producers, farmers, graziers, custom forage harvesters, livestock owners of all types and forage enthusiasts for this annual conference and trade show. The event is a partnership betweenMichigan State University Extension,Michigan Forage CouncilandNorth CentralSARE(Sustainable Agriculture Research and Education).The keynote speaker for the event will be Tom Kilcer, former Cornell Cooperative Extension field crop and soils educator. His keynote address is "Primo Feed from Winter Forages - More Than a Cover Crop." Following lunch and the Michigan Forage Council Annual Meeting, Kilcer will present "Harvesting Forages for Profit" in our afternoon session.
Breakout sessions that focus on hay and silage management and grazing management will round out the afternoon.
Hay and silage management topics:
Dairy Producers Panel‚ÄîHow to Use Forages for Maximum Return on Investment
Understanding Forage Sample Analysis - Steve Adsmond
Forage Research Update - Kim Cassida
Grazing management topics:
Grassfed Beef Project Update - Jason Rowntree
Grazing Multi-specie Cover Crops - Jerry Lindquist
Producer Panel‚ÄîSecrets of Successful Dairy Graziers
The cost of the event during early enrollment is $45 for Michigan Forage Council members, $55 for non- members and $20 for students. Early registration deadline is Friday, Feb. 24. Add $10 to the registration fee for registrations after Feb. 24 and at-the-door registration (lunch not guaranteed).
To register or pay online, go to2017 Great Lakes Forage and Grazing Conference. To pay by mail,download the conference brochure.
For more information, contact Tina House at 810-667-0341 orhouset@msu.edu, or Phil Kaatz atkaatz@anr.msu.edu.