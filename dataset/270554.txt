The National Dairy Farmers Assuring Responsible Management (FARM) Program is partnering with the Beef Quality Assurance (BQA) program to collect nominations for the first-ever joint FARM/BQA Dairy Award for 2019. The deadline to apply is June 1, 2018.
The awards honor outstanding beef and dairy producers and marketers that demonstrate the best animal care and handling principles as part of their day-to-day operations. This is an opportunity for NMPF member cooperatives and FARM participants to recognize dairy farmers that they believe demonstrate a strong commitment to quality animal care. In 2017, NMPF member Chris Kraft and his family were recognized for the care provided on their two operations in northern Colorado, Badger Creek Farm and Quail Ridge Dairy.
“The FARM Program is excited to continue working with BQA by jointly presenting this award,” said Emily Yeiser Stepp, director of the FARM Animal Care program. “By partnering with BQA, we can grow our pool of nominations and celebrate even more dairy farmers for their commitment to the highest animal care standards.”
The winner of the BQA/FARM Dairy Award is selected by a committee of animal scientists, FARM staff, BQA staff and industry representatives. The winning dairy operation will be chosen based on a set of five criteria:

The farm’s collective BQA and FARM practices, accomplishments and goals;
Relevant local, regional and national BQA and/or dairy promotion group or cooperative leadership;
Promotion and improvement of animal care practices, BQA or FARM program and consumer perception of beef or dairy industries;
Effectiveness in promoting and implementing BQA practices; and
Completion of the FARM Version 3.0 Animal Care Evaluation and implementation of program requirements.

The award was previously offered solely by BQA, whose awards recognize outstanding members of the beef industry in five categories: Cow-Calf, Feedyard, Dairy, Marketer and Educator.
Any individual, group or organization can nominate a single dairy operation for the award. Individuals and families may not nominate themselves, though they can be involved when preparing the application.
NMPF and its National Dairy Farmers Assuring Responsible Management (FARM) Animal Care Program partners with both NCBA and BQA, working closely to create valuable producer resources on stockmanship, dairy beef welfare and quality, and animal care.