April showers bring May flowers, as well as a lot of anxious farmers ready to hit the field for spring planting. Will spring weather support fieldwork? What factors could keep planters in the shed?
“Soil conditions today are adequately to abundantly moist across the Midwest, Delta and interior southeastern states,” says Drew Lerner, founder, president and senior agricultural meteorologist of World Weather, Inc.
Here is the rainfall amounts for the last 30 days:  

While many areas of the Midwest and eastern U.S. are flush with moisture, the west-central and southwestern Plains are experiencing dryness.
This Climate Prediction Center map shows the precipitation shortfalls, compared to normal amounts, for December through February.

“More precipitation will come to the northern Plains where further easing of long-term dryness is expected this spring,” Lerner says. “April will be the best chance for getting moisture improvements in the Plains.”
April also should be the wettest month in the Midwest and in the central Plains, Lerner says, although timely rain will occur in May. 
“The best summer weather is expected in the eastern Midwest, southeastern states and lower Delta as well as the northern Plains—where rainfall should be most frequent,” he says. “There is concern that ridge building in the western U.S. will become strong enough to limit rainfall in the western Corn Belt, with a lower risk to the northern plains and Canada’s prairies.” 
Until recently, the La Nina weather pattern was prevailing, Lerner says. It is now weakening. Although, he encourages farmers to keep an eye out this summer, as the pattern could return in July or August. 
World Weather, Inc. recently completed a study of seven years, in which La Nina abated in the first calendar quarter and was followed by neutral ENSO conditions throughout the growing season. The study showed a moderately strong tendency for below-average precipitation and slightly warmer-than-usual temperatures during the late spring and summer in the Plains and western Corn Belt. Such conditions could bring some crop stress to the western Corn Belt, Lerner says. 
Lerner’s spring and summer forecast highlights include:

Western Corn Belt: May come under some dryness pressure during the last spring and summer.
Eastern Corn Belt: Experience period showers and a cooler-then-normal bias.
Southwest Plains: Continue drier than usual throughout the growing season 

The Weather’s Impact on Grain Markets
Lerner provided his weather predictions at the Gulke Group’s late-March outlook conference in Chicago. Jerry Gulke, president of the Gulke Group, key takeaway from the presentation was: this year will not be like the last three years that provided good crop potential in all areas of our growing regions.  
“With the potential for a heat ridge this summer, a wet/cool start to the eastern Corn Belt and drier in the northern Plains, it should give us at least one weather market in months ahead,” he says. “Given the lower acres estimated by NASS of both corn and soybeans, there seems to be little, if any, weather premium in prices.”
Extrapolating USDA’s 2018 acre estimates into yields less than last year further reduces the ending stocks for 2018/19 in both corn and soybeans, Gulke notes. 
“While that is a long way off, we don't want to give up profit potential while looking back at the last three years expecting abundant crops,” he says. “The risk looks to be on the upside in corn especially at least into June. December corn futures have already exceeded $4, a level believed over the past months to be resistance!”
 
Read More:
The Rest of the Story: Crop Report Opportunities
Last week’s “The Rest of the Story” ended with thinking there would be a surprise(s) in last Thursday’s report. There were surprises on all accounts: acreage of corn and soybeans as well as stocks as of March 1.
Gulke: Welcome To A Whole New Ballgame
The grain markets screamed higher on March 29 after USDA released its annual Prospective Plantings report. This report typically holds a surprise or two and this year was no exception.
The Rest of the Story: Global Demand or Lack Thereof 
Jerry Gulke provides the rest of the story behind some of the issues in agricultural commodity markets in hopes it will provide information to help readers make better