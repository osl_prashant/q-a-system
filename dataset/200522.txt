Milk production for 2016 is raised on slightly larger milk cow numbers. The 2017 milk production forecast is raised from last month as improved returns support increases in both cow numbers and milk per cow.Fat and skim-solids basis exports for 2016 are raised on recent trade data. Imports are unchanged.
Exports on a fat basis are reduced slightly for 2017, but are raised on a skim-solids basis. Import forecasts are unchanged for 2017.
Dairy product prices and Class prices for 2016 are adjusted to incorporate December price data.
For 2017, butter, cheese, nonfat dry milk, and whey prices are raised from last month on demand strength.
Class prices for 2017 are raised, reflecting higher product price forecasts. The all milk price range is raised to $17.60 to $18.40 per cwt.
Read the latest World Agricultural Supply and Demand Estimates Report here.