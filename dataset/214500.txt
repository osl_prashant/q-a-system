La Cañada Flintridge, Calif.-based Allen Lund Co. is once again participating in Navidad en el Barrio, a program providing Christmas dinners to those in need. 
Navidad was started in 1972 by former NFL kicker Danny Villanueva and serves more than 75,000 people annually, according to a news release.
“It’s so wonderful to be able to provide fresh produce and meals to these families during the Christmas season,” said Nora Trueblood, director of marketing for Allen Lund Co. 
“The shippers and carriers we work with year-round are so generous.”
The company provides logistical support to Navidad as well as produce donated by customers, according to the release.
Allen Lund Co. plans to collect and transport donations for Navidad food baskets the week of Dec. 11, and baskets will be delivered Dec. 16, according to the release. All donations must be usable past the distribution date.
“Navidad en el Barrio is such an important effort and one that we are able to actually see the impact, by participating in the distribution of the dinners, and meeting the grateful families,” company vice president Kenny Lund said in the release.
Allen Lund Co. has participated in the program since 2004.