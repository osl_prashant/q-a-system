In spite of heavy rains that flooded some strawberry fields in late winter, this year’s Salinas Valley crop appears to be on a normal track, said Carolyn O’Donnell, spokeswoman for the Watsonville-based California Strawberry Commission. 
“We’re pretty much on track in the Watsonville-Salinas area because we weren’t in production during that rain,” O’Donnell said.
 
The latest projections had this year’s crop in the normal range, O’Donnell said.
 
As of April 1, 91,306 crates of strawberries had been shipped out of the Watsonville-Salinas District, compared to 161,640 by the same date a year earlier, according to the commission. 
 
By comparison, Santa Maria had shipped 3.6 million crates as of April 1, which was down slightly from 4 million in 2016.
 
As a state, 2017 fresh strawberry volume of 4.34 million trays was below the projected total of 5.59 million for that date.
 
Some replanting was required after the heavy rains in February and March, said Joe Ange, purchasing director with Salinas, Calif.-based Markon Cooperative Inc., a foodservice-focused grower-shipper with seven member distributors in the U.S. and Canada.
 
“I think the heavy rains we received in January, February and March affected plantings and are going to affect strawberries, as well as lettuce and leaf crops in the Salinas Valley,” Ange said.
 
Planting schedules were interrupted, and that will affect the timing of this year’s crop, Ange said.
 
“They had to replant, so the biggest impact is the timing. When products are ready to be harvested will be delayed in many cases,” he said.
 
The “Mother’s Day pull” is a focal point of the strawberry season, particularly in foodservice, Ange said.
 
Most of the berries available for the holiday will come out of California’s Santa Maria region, with Salinas-Watsonville providing some product, Ange said.
 
“There will be strawberries in Salinas and Watsonville for Mother’s Day. However, Santa Maria is the main growing region for Mother’s Day,” he said.
 
Supplies have trended northward in recent years, and the Watsonville-Salinas area is becoming more prominent for Mother’s Day, Ange said.
 
“In the past, it was Oxnard, and over the past several years, it has started moving further north, and we’re seeing a shorter season in Oxnard and we’re seeing Salinas and Watsonville coming in for that pull,” he said.
 
The outlook for the crop is positive, said Vinnie Lopes, vice president of sales-west with Naturipe Farms LLC in Salinas.
 
“(It’s) very good. Should be a very great, high-quality crop,” he said.
 
The rains caused some havoc for strawberry plantings in the Watsonville area, said Cindy Jewell, vice president of marketing with Watsonville-based California Giant Berry Farms.
 
“We’re behind just because of the fruit we lost during the rain, but in this Salinas-Watsonville area, the fields look great, the plants are healthy,” Jewell said. “It’s definitely not going to affect northern districts.”
 
There will be plenty of fruit available for Mother’s Day, though, she said.
 
“It’s delayed the start here a bit,” she said. “They’re talking some pretty good volume from this area by Easter — our luck will be after Easter. But that gets us full swing to Mother’s Day.”