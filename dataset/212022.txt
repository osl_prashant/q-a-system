Guadalupe, Calif.-based Apio has committed to “clean” labels for all its Eat Smart products by the end of 2018.The company defines clean as free from artificial colors, flavors and preservatives.
The move is a response to customer and retailer demand, according to a news release.
“Our move to 100% Clean Label is designed to make nutrition easier to understand,” Anne Byerly, vice president of marketing, said in the release. “We expect 100% Clean Label to become a mainstream standard.”
Nearly 90% of Eat Smart items already meet that standard, including salad blends, vegetable salad kits and cut vegetable products.
Apio positions its clean-label salad kits as an affordable alternative to organic.
The number of ingredients in many salad kits “makes it difficult to source all ingredients organically at a price that consumers are willing to pay,” according to the release.
Eat Smart products can be found in more than 100 club and retail chains in the U.S. and Canada.
Want more to the story? Read more here:
Apio wins CPMA award for Salad Shake Ups
Apio relabels salad kits