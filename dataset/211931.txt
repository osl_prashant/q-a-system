Produce marketers across the country say “local” now is trendier than organic. A Minnesota apple grower-shipper says it offers the best of both categories. 
“Growing commercial-scale organic apples in the Midwest can present its challenges due to climate conditions, but consumers are demanding it not just locally but across the country,” said Don Roper, vice president of sales & marketing with Elgin, Minn.-based Wescott Orchard & Agri Products Inc. and its Honeybear Brands marketing arm.
 
The Twin Cities are especially supportive of organics, Roper said.
 
“Where it makes economic and commercial sense to grow organically, everyone is doing it,” he said.
 
Roper said his company has placed special emphasis on organic fruit, having developed its own sustainable and best practices growing protocol called TruEarth, which all of its Midwest growers follow. 
 
“This means that all the fruit we produce, organic or otherwise, is grown using best practices to preserve the land, protect natural habitat and support local growing communities wherever possible,” Roper said.
 
Organics are easy to find in the Twin Cities — but combining organic and local is a trick all its own, said Nathan Mata, produce manager at The Wedge Community Co-op, a Minneapolis retail store.
 
“We buy direct from the farmers,” Mata said. “We do a lot of extra work to source from farms, where somebody like Whole Foods, it’s probably just more convenient to order from the warehouse.”
 
The competition is fierce in the organic category, said Tom Rodmyre, warehouse director at Twin Cities Co-op Partners Inc., which supplies The Wedge.
 
“Everybody’s trying to do organics,” he said. “Everybody’s trying to beat each other up on price. It’s competitive.”
 
It’s bound to get more competitive, with online retailer Amazon’s recent purchase of organic retail chain Whole Foods, Rodmyre said.
 
“Whole Foods is very big on their employee base, and Amazon is very big on their customer base, which is kind of two different things,” he said. “Amazon is pretty good at what they do and they usually figure it out pretty quick. In this market, there’s so much competition.”