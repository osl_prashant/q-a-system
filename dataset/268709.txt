Vidalia onions are primarily a retail item, but suppliers aren’t counting out foodservice completely.
Some say there’s room for plenty of growth in foodservice. Others say they’re already seeing signs of progress.
Bob Stafford, interim director of the Vidalia Onion Committee, is one of the latter.
“It’s doing real good,” he said. “I’m from the old school, and it took a good bit of convincing, but that is a big thing. It does work, so we’re into that (marketing to foodservice) pretty heavy.”
Stafford said the product is getting increased TV exposure on cooking shows, which has helped boost foodservice sales.
“I didn’t realize people were watching those cooking shows as much as they are, but when you get into the position of watching a bit more, I see it trending that way,” he said.
“People are really into that stuff. It’s not just the ladies, it’s the men cooks. That seems to be a big push. We’re seeing a lot of Vidalias on those shows.”
The committee is working with chefs who are affiliated with the Georgia Grown program, which also helps to create an inlet to restaurants, Stafford said.
“We’re going to have several demos up the East Coast this spring,” he said.
Some growers say they have seen an increase in foodservice business.
“No question, we are, as an industry, seeing an increase in interest from the foodservice side of things,” said Walt Dasher, co-owner of Glennville, Ga.-based grower-shipper G&R Farms.
“A lot of that has to do with the advertising the Vidalia association has done and continues to pour into it. It has really opened the eyes of a lot of independent restaurants to take an interest and given them a chance to see what they’re all about.”
There are challenges, though, including the issue of price, said Dave Munson, director of culinary development and a corporate chef with Greencastle, Pa.-based Keystone Fruit Marketing Inc.
“There isn’t much going on in foodservice, as price continues to be a hurdle for the majority of operators,” he said. “Most don’t have a steady item on menus that would warrant sustained purchases.”
Restaurants that do buy sweet onions use them primarily as a feature in salads, dressings and sauces, Munson said.
“(It is) still very much a regional commodity in foodservice — an example being Burgerville (a restaurant chain) and Walla Walla sweet onions in the Northwest,” he said.
Some grower-shippers say they bypass foodservice.
“That’s nonexistent for Vidalias,” said Delbert Bland, president of Glennville-based Bland Farms LLC.
“Everything we do is at retail. If you run the numbers, a restaurant is going to buy the more inexpensive product.”
Lyons, Ga.-based L.G. Herndon Jr. Farms Inc., has broken through in foodservice to a degree, said John Williams, sales and marketing manager.
“We have our foodservice customers that take Vidalias, but it’s nowhere near what we do with retail,” he said.