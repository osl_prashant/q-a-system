Reproductive efficiency on U.S. dairy farms finally is beginning to improve, according to University of Wisconsin professor of dairy science Milo Wiltbank. The researcher believes this is due largely to improvements in dairy cow genetics, reproductive management programs, heat-stress abatement, total mixed ration formulation and delivery, and facilities that promote cow comfort.
Wiltbank sees the fine-tuning of nutrition as one of the "final frontiers" in elevating dairy cow fertility and reproductive efficiency. "Body-condition scoring (BCS) still is an easy and affordable way to monitor nutritional status of fresh cows," he said. He cited data showing that thin, early lactation cows with BCS of 2.5 or less do not cycle well and will have reduced reproductive function.
Change in BCS early postpartum also has been implicated in reduced oocyte quality. Wiltbank and his colleagues recently completed a large, 1,887-cow study examining the relationship between change in body condition in the first 21 days postcalving and fertility. The average BCS for both herds in the study was about 3.0, indicating that the animals were not over-conditioned at the beginning of the trial. Cows that are overconditioned before calving (above 3.25) generally have a greater BCS loss after calving and more metabolic and reproductive problems.
The researchers classified the animals at 21 days as either losing, maintaining or gaining body condition. All cows were synchronized using a Double Ovsynch program with timed AI. "This helped to reduce problems from high metabolism of hormones that occur in high-producing dairy cows and eliminated cherry picking in the reproductive protocol,"" Wiltbank noted.
The differentiation between the three groups for pregnancies per insemination were surprisingly distinct:




Time of pregnancy diagnosis


Lost body condition, 0-21 days


Maintained body condition, 0-21 days


Gained body condition, 0-21 days




Days after breeding


% Pregnant


% Pregnant


% Pregnant




40


25


38


84




70


23


36


78




Wiltbank attributed the slight decline in pregnancies for all three groups at 70 days post-breeding to early embryonic death. "Still," he noted, "the significantly higher pregnancy rate in the group that gained weight in early lactation was remarkable. Thus, nutrition early in lactation definitely does impact fertility, we are currently exploring the dynamics of why that happens."
One of Wiltbank's theories is that cows struggling to maintain body weight may have lower amounts of methyl donors in their blood - that can impact the addition of methyl groups to their DNA and to the DNA of their oocytes and embryos. Methyl groups on DNA regulate gene expression and thereby alter many important functions including energy production, immune response, and, of particular importance for reproduction, proper development of the pregnancy.
His current research is focusing on whether supplementing fresh cows with methionine can supply additional methyl groups that could positively influence pregnancy achievement. In one early study, he and his colleagues found the embryos of methionine-supplemented cows were larger than those from cows that received no supplementation and were less likely to be lost during early pregnancy.
While their work in this area is ongoing, Wiltbank believes there is promise in not just delivering more nutrients to cows pre-breeding but also in delivering specific nutrients. "We should be able to alter the gene expression in the embryo, and possibly physical function later in life, using nutritional manipulation of the dam," he said.