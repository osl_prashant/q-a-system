Organic products are all the rage. 
 
With the growth of grocery stores like Whole Foods and Trader Joe's, organic products are more accessible than ever before, but what does that mean for your typical supermarket, more particularly the produce department? 
 
Well, it means you gotta keep up!
 
I have worked in a supermarket's produce department for the past six years, and I have noticed a lot of consumer change over that time period. 
 
Organic produce was definitely a thing, but nowhere near where it is now. 
 
At the moment, my produce department stocks more than 120 organic items. There is even a sign hanging up in the department with an exact count of the organic produce we stock. I think it stands at 123. Not too shabby.
 
Having a great variety of organic produce is important to a lot of customers. By seemingly keeping up with specialty produce outlets, my supermarket can entice more customers to come into the store because of the organic produce we carry, instead of pushing out customers because we don't have it. 
 
I have had numerous customers compliment the department and the amount of organic produce we stock. To some, food shopping is a fun experience, as they can pick out fresh, organic produce and it is that sort of excitement that builds a good consumer base. 
 
And not only does the produce department benefit from this type of customer satisfaction, but the entire supermarket as a whole benefits, as presumably that customer will shop in other departments as well. 
 
So it is a win all around.
 
However, there is a downside to keeping stock of the organic produce. Because of its organic nature, its shelf life is not as long as conventionally grown produce, so if there is a lull in sales, then the department takes a hit on rotten produce. 
 
A little anecdote here: We do not get trucks on Sundays, so Saturday trucks are quite large, since they must compensate for the entire weekend. 
 
Well, I remember one weekend where we sold an entire case of fresh, organic kale and ended up running out of the item completely by Sunday afternoon. 
 
So, the next weekend my manager ordered an extra case for Saturday, but we did not even sell the one and were left with a whole extra case. You cannot anticipate sales, and that makes selling organic produce tricky.
 
Fortunately, we were able to sell that kale before it went bad, and my manager took note of that instance, but the moral of the story is that sales can fluctuate wildly. 
 
I've learned to find a pattern or trend and order along those lines, because even though not having an item could disappoint a customer, a rotten item is even worse, at least from my experience. 
 
Many of the customers who come into a grocery store to purchase organic produce are the same customers who hold the produce to higher standards.
 
They do not want fruits and vegetables that do not look fresh, and you know what? I don't blame them. 
 
The produce department can be a fickle beast. With the inclusion of a plethora of organic items, the job of running a produce department becomes that much more challenging, but if done right, the organic items can add another layer to the department, help sales in total, and keep people eating healthy. 
 
Seems like a success story to me.
 
Matthew Golda is a freelance writer for The Packer and produce associate at a Foodtown store in Long Island, N.Y.
 
What's your take? Leave a comment and tell us your opinion.