Lingering issues remain, but agreement will go into force






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






U.S. sugar producer and processor lobbyists have been among the best in the ag sector for quite a while. That appears to be the case with the agreement in principle announced regarding updating the U.S.-Mexico sugar trade suspension agreement, despite U.S. interests withholding support.
Some highlights of the agreement:

Lowers the percentage of refined sugar Mexico can send to the U.S. to 30 percent of its exports, from 53 percent.
Lowers the level of purity (polarity) at which Mexican sugar can be sold as refined sugar to 99.2 from 99.5 degree polarity. As a result, “estandar” — a common variety of sugar from Mexico — will count against the 30 percent limit on refined sugar. With a minimum sucrose content of 99.4 percent, estandar can replace refined sugar in many food and beverage applications.
Mexico has option to supply 100 percent of any excess demand from the U.S. after April 1 of each year. Under the farm bill, from the Oct. 1 start of the “quota year” until April 1, USDA can only let in the minimum quantity of sugar required under WTO commitments and those in U.S. trade agreements.
Price at which raw sugar must be sold at the mill in Mexico increases from 22.25 cents per pound to 23 cents per pound and from 26 cents per pound to 28 cents per pound for refined sugar. With the higher prices mandated by the deal and steep penalties if the terms are violated, Mexico will have much less incentive to dump sugar on the U.S. market when it has a surplus production year.

Other issues and related items include:

The American Sugar Alliance objects to the conditions under which Mexico would meet that excess demand. The group wants to provide USDA the authority to specify the type and purity (polarity) of any additional sugar needed by the U.S. market. USDA would lose that authority unless the current language is changed, and Mexican firms would not have to ship or export raw sugar when the U.S. industry has additional raw sugar needs. U.S. sugar grower lobbyists think the last suspension agreements failed in large part because Mexico shipped too much refined and shorted refineries of raw sugar — US sugar refiners said a 99.5 percent polarity threshold on raw sugar will allow producers in Mexico to sell their major product, estandar, as refined rather than raw sugar because it can go into food and beverages with minimal processing.
The Sweetener Users Association (SUA) blasted the modifications to the suspension pacts, saying they “will make two bad agreements far worse.” The changes will further increase the reference prices and continue the “flawed formula” for Mexican access, resulting “in a deliberate shorting of the U.S. market,” the SUA said. “It is ironic that after the administration conducted exhaustive negotiations with Mexico, and obtained virtually every demand of the U.S. domestic sugar industry, the sugar lobby still refuses to endorse the modifications. Their ostensible reason — that provisions for Mexico to supply additional U.S. needs after April 1 of each year constitute a ‘loophole’ — is absurd on its face and demonstrates that the sugar lobby is simply trying to extract additional concessions, despite having gotten what it asked for.”
SUA said, “The fact is, a 99.5 dividing line is not a loophole, it is the law. The Harmonized Tariff Schedule of the United States follows international conventions in defining raw sugar as having less than 99.5 polarity. It is the 99.2 polarity breakpoint in the U.S.-Mexico deal that is the exception and inconsistent with international trade conventions.”
Success of the agreement will depend on enforcement by USDA, according to House Agriculture Committee Chairman Mike Conaway (R-Texas). He said that the agreement “will require diligence on the part of the U.S. government, particularly USDA, to ensure the injury to American sugar farmers is remedied and that Mexico is in full compliance with this agreement.”
Juan Cortina Gallardo, head of the Mexican Sugar Chamber, said his industry would keep pressing the Mexican government to move ahead with a dumping investigation on imports of U.S. high-fructose corn syrup. The Mexican government has not officially opened an investigation, and no one in the U.S. industry has been notified. Sucroliq, a Mexican producer of liquid sugar, filed a petition for antidumping duties against U.S. corn sweeteners two years ago but the Mexican government decided not to open an investigation.
The U.S. industry does not have to sign off on the deal for it to go into force. But the U.S. industry can ask the U.S. International Trade Commission for a review of the deal at any time.

 


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.