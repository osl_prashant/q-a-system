Mount Vernon, Wash.-based Viva Tierra Organic Inc. has hired Erik Lee as a salesman.
 
Lee joins the staff to sell and market certified organic fresh apples, pears, onions, and other commodities to customers across the U.S. and Canada.
 
Lee recently was a sales manager in the Global Fruit Division of Earthbound Farm.
 
"His longstanding career in the produce business and in organics for over 25 years will enable our company to reach out to a more diverse customer base as our domestic and import supplies of organic produce increase," Viva Tierra's president, Luis Acuna, said in a news release.