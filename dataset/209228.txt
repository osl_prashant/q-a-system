All cattle and calves in the United States as of January 1, 2015 totaled 89.8 million head, 1% above the 88.5 million as of January 1, 2014. Prior to 2014, the U.S. cattle inventory had not been under 90 million head for 60 plus years (since 1952). The U.S. cattle inventory was greater than 100 million head as recently as 1997.Texas has the most cattle in the United States followed by Nebraska and Kansas. The cattle inventory in Texas is 11.8 million head, roughly 13% of all the cattle in the United States. Nebraska has 6.3 million head of cattle, slightly more than Kansas at 6 million head. Nearly 14% of the cattle in the United States are in Nebraska and Kansas.
The top 10 states with the most cattle include: Texas, Nebraska, Kansas, California, Oklahoma, Missouri, Iowa, South Dakota, Wisconsin & Colorado. All states in the top 10 have more than 2.5 million head of cattle. Fifty-seven percent (57%) of the cattle in the United States are in the top 10 states.
Rhode Island has the fewest cattle in the United States followed by Alaska and Delaware. Rhode Island has 5,000 head of cattle, less than .01% of the cattle in the United States. Texas has 369 times as many cattle as Rhode Island, Alaska and Delaware combined.


United States


89,800,000


Rank


State


2015


% Of U.S.


1


Texas


11,800,000


13.14%


2


Nebraska


6,300,000


7.02%


3


Kansas


6,000,000


6.68%


4


California


5,150,000


5.73%


5


Oklahoma


4,600,000


5.12%


6


Missouri


4,000,000


4.45%


7


Iowa


3,900,000


4.34%


8


South Dakota


3,700,000


4.12%


9


Wisconsin


3,500,000


3.90%


10


Colorado


2,600,000


2.90%


11


Montana


2,500,000


2.78%


12


Minnesota


2,330,000


2.59%


13


Idaho


2,300,000


2.56%


14


Kentucky


2,060,000


2.29%


15


Tennessee


1,730,000


1.93%


16


Florida


1,700,000


1.89%


17


North Dakota


1,650,000


1.84%


18


Arkansas


1,640,000


1.83%


19


Pennsylvania


1,530,000


1.70%


20


Virginia


1,470,000


1.64%


21


New York


1,450,000


1.61%


22


New Mexico


1,340,000


1.49%


23


Oregon


1,300,000


1.45%


24


Wyoming


1,300,000


1.45%


25


Ohio


1,250,000


1.39%


26


Alabama


1,220,000


1.36%


27


Washington


1,150,000


1.28%


28


Illinois


1,140,000


1.27%


29


Michigan


1,140,000


1.27%


30


Georgia


1,040,000


1.16%


31


Mississippi


910,000


1.01%


32


Arizona


880,000


0.98%


33


Indiana


870,000


0.97%


34


North Carolina


800,000


0.89%


35


Louisiana


790,000


0.88%


36


Utah


780,000


0.87%


37


Nevada


435,000


0.48%


38


West Virginia


370,000


0.41%


39


South Carolina


335,000


0.37%


40


Vermont


260,000


0.29%


41


Maryland


185,000


0.21%


42


Hawaii


135,000


0.15%


43


Maine


85,000


0.09%


44


Connecticut


47,000


0.05%


45


Massachusetts


38,000


0.04%


46


New Hampshire


30,000


0.03%


47


New Jersey


28,000


0.03%


48


Delaware


17,000


0.02%


49


Alaska


10,000


0.01%


50


Rhode Island


5,000


0.01%