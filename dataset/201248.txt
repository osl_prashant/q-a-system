Farm Diesel

Farm diesel was unchanged on the week.
Only Ohio posted a higher price as all eleven others were unchanged.
This week's price action reflects the confusion currently running through world crude oil markets.
Distillate supplies fell 1.8 million barrels last week, now 1.5 million barrels above the year-ago peg.
Our farm diesel/heating oil futures spread is at 0.53 which indicates mild near-term price pressure for farm diesel.

 
Propane

Propane firmed a penny per gallon this week.
Ten of twelve states were unchanged on the week.
Propane supplies firmed 2.118 million barrels on the week but remain 24.219 million barrels below year-ago.
Retailers are beginning to post summer fill offering prices. Check your local average bid reported to your Inputs Monitor before you pull the trigger for guidance and, perhaps, a bit of bargaining power, if need be.






Fuel


6/26/17


7/3/17


Week-over Change


Current Week

Fuel



Farm Diesel


$1.91


$1.90


Unchanged


$1.90

Farm Diesel



LP


$1.20


$1.18


+1 cent


$1.19

LP