Researchers hope to bring technology to small farms in developing countries, say reporters on the Learning English website.Ranveer Chandra grew up in India and now works for Microsoft Corporation. He remembers spending four months a year on his family's farm, where there was no water or electricity. Animals pulled plowing equipment through fields, as they have for centuries.
Now, Chandra wants to bring precision farming technology to small farms. His – and other researchers – goal is to feed the world while focusing on sustainability and sound management practices.
The main idea of precision farming, notes the Crop Society of America (CSA), is that there are natural differences in the soil – even in the same field. Farmers who understand these differences are better able to produce crops.
Precision farming is became popular in the 1990s and is common practice on many U.S. farms, thanks to advancements in Global Positioning System (GPS) technology.
“GPS information can tell farmers where they planted seeds, used fertilizers, chemical pesticides, and so on,” the article said.
Not Just for Large Scale FarmersIn developing countries, farmers have neither the financing nor access to high-tech equipment. Often in those places, crop production levels are low. But now, high-tech tools are bringing a new level of precision to large-scale agriculture.
Farm equipment can plant different amounts of seeds and leave different amounts of fertilizer in different parts of a field. Water measuring equipment can tell farmers how much water their plants are getting.
Still, the cost is still prohibitive for many small farmers, especially in developing countries.
Artificial Intelligence on the FarmChandra is working to develop low-cost, high-tech tools for farmers. He developed a system that connects soil sensors to other sensing equipment through unused television (TV) channels.
These unused TV signals, known as "white space" frequencies, can provide broadband internet connection over long distances.
He notes that some hospitals and schools in rural areas are already online with white-space connections.
Each soil sensor provides information about conditions on the farm. To connect all the areas into a big picture, a drone aircraft takes photos.
Artificial intelligence (AI) compares these aerial photos to the sensor data, and can then create maps of where the soil is too acidic or needs more water.
Similar technology can also help farmers watch their animals. Webcams can help farmers identify sick cattle, for example.
Fertile Business“Chandra hopes to bring the cost of a simple system under $100. He notes the existence of low-cost replacements for the drones. A balloon tied to a mobile phone can serve the same purpose,” the article said.
He added that Microsoft is considering different business models. Farmers could share a system, or a local farm business could rent systems to farmers.
Microsoft isn’t the only company looking to help small farmers. Google invested in a small company that gathers farmers' data on how different crops perform from farm to farm and year to year.
Many other companies are trying to enter the high-tech farm business, notes John Fulton of Ohio State University.
Fulton himself has helped develop a free phone app for farmers. The software program helps them compare different field management techniques.
"Everyone's trying to figure out how they play in agriculture," he said in the article. "We're really at an early stage of this digital agriculture revolution."
Uncertain FutureDemands on agriculture are increasing. As the world’s population grows, farmers will need to make more food without cutting down more forests or polluting the land and water. Productivity has continued to increase year over year, but existing farms will need to produce more food, while using resources judiciously.
Chandra says that what farmers need is more information, so they can give the land what it needs.
"Like, apply water only where it is needed,” he said. “Apply fertilizer only where it is needed. Apply pesticide only where it's needed.”
However, the level and speed at which new farming techniques and technologies will spread remains unclear.
Bruce Erickson with Purdue University says one problem faced by any new farming technology is getting people to use it with their existing methods. Different countries have different farming methods and different ways of doing business.
"It takes people to implement new farming practices," Erickson said.
Precision farming was adopted in the U.S. because farmers here recognized the value it provides. It might be a harder sell in countries that are unfamiliar with the technology. But just as the Green Revolution changed the worldwide view of agriculture, precision farming has the potential to do the same thing.