The farm economy has been struggling for a while now, especially with weak cattle prices and disease issues in wheat.
On U.S. Farm Report, Mark Gold of Top Third Ag told host Tyne Morgan for the last eight months, there have been bounces in both the fats and feeders market. The cycle of dropping prices with a rally has been too common, causing frustrations.
Gold said he hasn't seen any indication of which low will be the "real low" that will cause the market to improve, and he thinks there's $10 of risk in both cattle markets.
He believes the cattle market could turn a corner, but there has to be more "pain" in order to see improvement in prices.
As for wheat, milling quality has been poor in some places, especially in the Pacific Northwest, causing both a drop in prices and possibly acreage in 2017.
"The acreage is going to crash, and it was already down last year," said Kevin Duling of KD Investors. "I think we fell in that paradigm where things were just too cheap."
There's talk of the federal government raising interest rates. According to meetings of a federal meeting Duling read, if rates aren't raised, this signals a "lack of credibility."
"Do you really want to raise rates to maintain credibility?" Duling asked.
Gold foresees a raise in interest rates, and he predicts after the election, "Obamacare will go through the roof."


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.