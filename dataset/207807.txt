SALINAS, Calif. — An electric hybrid system for semi trailers and trucks will lead to greater fuel efficiency.  
Thomas Healy, founder and CEO of Hyliion, on June 29 at the Forbes AgTech Summit, was interviewed during an “Innovation Spotlight” by Heather Shavey, assistant vice president and general merchandising manager for Costco.
 
With fuel consumption accounting for 13% of total costs, Healy said the trucking industry has embraced the potential of the technology, which could cut fuel costs 30%.
 
“The fact that we are going in and having a big impact on their No. 1 pain point has caused a tremendous response from the industry,” he said.
 
Hyliion is receiving a second round of funding to expand the business.
 
“The next big stage for us is going into production and actually putting them in fleets that are out there.”
 
Currently, Hyliion is testing the hybrid technology on its own trucks, but five fleets plan to use the technology by the end of the summer, he said.
 
The hybrid technology can be retrofitted on semi trucks and trailers and includes a battery pack, electric motor and a propulsion system that replaces one of the passive axles in the semi trailer.
 
“The way it works is when a vehicle is slowing down, or going down hill, we capture the wasted energy — stored in a lithium ion battery pack — and then we use that to help move the vehicle down the road to help reduce emissions,” he said.
 
The two-year-old Pittsburgh, Pa., company has 13 employees.
 
“When we looked at trucking, it is one of those industries that really hasn’t had a revolution or quantum leap forward.”
 
Shavey said energy savings and reduced costs would be important to Costco, which had 1,500 produce trucks inbound for the week of July 4.
 
“I think we are always looking for cost savings, efficiency and a win-win for both us and our partners,” she said.
 
A member of the audience asked why the time is right for the hybrid electric system for truckers.
 
Healy said the battery and motor technology has advanced to the point of making it feasible for big rig trucks.
 
“You have an 80,000-pound rig going down a hill. That’s a tremendous amount of energy behind that vehicle, and so in order to capture that and recharge the battery, the battery technology needs to be very advanced,” he said. “Technology has gotten to the point where you can apply it to tractor trailers.”