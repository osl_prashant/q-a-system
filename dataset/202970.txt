As new tomato, pepper and cucumber plants grow through the winter in climate-controlled Ontario greenhouses, ripening up for the traditional first harvest in March, demand for the produce remains strong.
"We're in production in certain items and relying on our growing partners offshore," said Joe Spano, vice president of sales and marketing for Kingsville, Ontario-based Mucci Farms.
"Prices are not ridiculous by any means compared to past years, and there are no major spikes or shortages. We're fulfilling our customer's needs and keeping shelves full," Spano said.
According to the Leamington-based Ontario Greenhouse Vegetable Growers, the province's greenhouse vegetable production continues to grow about 6% per year, with about 1,000 acres devoted to peppers this year and another 1,000 acres in tomatoes.
Growers are scheduled to add 174 acres under glass in 2017, some with the latest in lighting and automation.
Just across Lake Erie, however, the same growers are taking advantage of lower electricity rates to build high-tech lit greenhouses that give them easy access to U.S. chains and offer a consistent, year-round supply of greenhouse produce.
"Hydro rates, and the province's new cap and trade program, are our biggest challenges this year and represent a big expense for growers," said Danny Mucci, vice president of Mucci Farms.
Mucci plans to start construction this spring on the first 30 acres of a new greenhouse in Erie County, Ohio, with room for a potential 180 acres under glass.


U.S. attraction

"Electricity is the draw," Glen Snoek, marketing and economic policy analyst for the greenhouse growers' organization.
"The calculation is tricky, but up here they're paying fairly high rates delivered at about 14 cents a kilowatt hour," Snoek said.
"We've heard of rates in Ohio that are below 6 cents per kwh and growers are being welcomed with open arms," he said. "It's a difficult dynamic for us to compete with."
On Jan. 25, Kingsville-based Red Sun Farms announced the lights are on and the first seedlings planted in its 20-acre greenhouse in Wapakoneta, Ohio, its second U.S. expansion.
"This facility is equipped to provide fresh greenhouse produce within a 10-hour transport radius, reaching nearly 60% of the U.S. population," co-owner Paul J. Mastronardi said in a news release.
Leamington-based NatureFresh Farms now has 45 acres of greenhouses in Delta, Ohio, to augment its 130 acres in Ontario, said director of marketing Chris Veillon, while Kingsville-based Mastronardi Produce is growing strawberries along with traditional hydroponic vegetables in its Coldwater, Mich. facility, completed in 2012.
"Our concern is that we're unnecessarily creating a competitor that's going to be more innovative and have a competitive advantage based on electrical costs," Snoek said.
He estimates Ontario is losing around $500 million in investment that's being spent on lit construction in the U.S.
Spano said Ontario's high electricity rates aren't the only reason Mucci is expanding south of the border.
"Transporting product back and forth across the border has becomes more tedious and expensive, and having multiple locations with our partners in the U.S. is a strategic move," he said.
"Proximity to market is also important, but if we can get cheaper energy in the process, even better," he said.
Snoek said the Ontario government doesn't seem concerned about potential problems the industry may face in five or six years because it's not addressing the electrical cost differentials now.