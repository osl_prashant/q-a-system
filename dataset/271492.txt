Australian authorities are crediting a 17-year-old deaf and partly blind blue heeler with saving a toddler’s life after the little girl went missing in the mountains of Queensland.
The girl, named Aurora, wandered away from her grandmother’s house last week, prompting a search by police and family. Fifteen hours after Aurora went missing, her grandmother heard Max, the aging blue heeler, barking in the distance. She and rescuers came running and Max led them to the missing child.
“The dog camped with her,” Aurora’s grandmother’s partner explained to a local television station. Max was named an honorary police dog by the local department.
 

SUCH A GOOD BOY, MAX! He stayed with his 3-year-old human who was lost near Warwick last night while we frantically searched for her. For keeping her safe, you're now an honorary police dog. ?https://t.co/QiszGFP4gg via @ABCNews pic.twitter.com/xxRc6ndeaK
— Queensland Police (@QldPolice) April 21, 2018