Kansas man gives back to community through soup
Kansas man gives back to community through soup

By KATHERINE BURGESSThe Wichita Eagle
The Associated Press

WICHITA, Kan.




WICHITA, Kan. (AP) — In 2016, Walter Clemons had a heart attack.
God saved his life, he said, so now he's giving back, one pot of soup at a time.
"It's a blessing to be able to help them," Clemons told The Wichita Eagle . "It just does something for your heart."
On Tuesdays and Thursdays, Clemons spends the mornings cooking up multiple pots of soup. He then makes three stops along South Broadway: at Broadway and Mt. Vernon, Broadway and Kellogg, and Broadway and Harry.
The people who stop by his truck and homemade signs call him "The Soup Guy." He feeds up to 125 people a day in the summer, his busiest time. Anyone can stop for a free cup of soup and a hotdog. He recently offered potato soup and a spicy three bean and rice soup.
Many of the people fed by Clemons are homeless. Some tell him about how they struggle to find jobs. Some bring their children to eat Clemons' soup.
"The Soup Guy" said the ministry started with a dream in which God told him "to feed his people."
At first, Clemons didn't know if God meant preaching or literal feeding, but eventually the soup ministry became clear.
Now, he has a shed outside his house stocked with frozen bread, cans of vegetables and bottles of ketchup. Sam's Club and Dillons donate food and he also buys in bulk using donated money.
He makes a variety of soups: chili, chicken and rice, veggie and more.
The former Spirit worker and car mechanic said he couldn't run the ministry on his own. The food is free because God provides it, he said.
He's been giving out cups of soup for 14 months.
"God sent me to feed you," Clemons said he tells people. "He knew you were going to be hungry this day at this particular time. That's why I'm here."
Ty Shaddy recently stopped by to donate a dime — all the money in his possession, he said — and to have a bowl of soup.
Homeless since November, Shaddy said he'd seen Clemons and his "free homemade soup" signs before, but had never stopped. After having a cup of potato soup, Shaddy returned to Clemons' truck to tell him how good the soup was, "almost as good as my mom's."
When Clemons had his dream, he saw himself feeding people out of a window, like a food truck or trailer.
"I know that this vision that he's given me is going to come to pass, because he (God) doesn't do anything halfway," Clemons said.
He believes there's someone with a food truck or trailer going unused who will donate it, or allow Clemons to purchase it using donated money. With a trailer and more donations, he could expand his ministry to more of Broadway on more days of the week.
For now, Clemons has stood outside in the snow, the heat of summer and 60 mph winds. In the summer he sometimes rigs up a tent over his truck.
Not only does Clemons provide food to people who are homeless, but he'll also have good conversations with them, said Charles Fallot, who is working to get out of homelessness.
Some people do things for the attention, but not Clemons, Fallot said.
"He's a good man," Fallot said.
___
Information from: The Wichita (Kan.) Eagle, http://www.kansas.com


An AP Member Exchange shared by The Wichita Eagle.