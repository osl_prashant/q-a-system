Chicago Mercantile Exchange nearby live cattle contracts on Monday settled with modest gains, supported by their discounts to last week's futures prices and robust wholesale beef demand, said traders.They said profit-taking, after futures spiked to new highs earlier in the session, capped nearby market advances and pressured other trading months.
June ended up 0.100 cent per pound to 124.125 cents and posted a new high of 125.325 cents. August closed up 0.125 cent to 120.175 cents and hit a new high of 121.150 cents.
Monday morning's average wholesale beef price surged $2.42 per cwt to $224.20 from Friday. Select cuts rose $1.32 to $209.00, the U.S. Department of Agriculture said.
Last week market-ready, or cash, cattle in the U.S. Plains fetched $134 to $136 per cwt, up from $130 to $133 a week earlier.
Blizzard conditions and heavy rains in parts of the Plains over the weekend slowed cattle deliveries, said analysts and traders. They said excessive moisture created mud in feedlots, making it difficult to sort and load animals.
USDA estimated Monday's cattle slaughter at 107,000 head, 8,000 fewer than a week ago.
Higher cash prices last week shaved packer profits, making them less likely to actively compete for cattle this week, a trader said.
Investors await Wednesday's Fed Cattle Exchange sale of roughly 1,600 animals. Cattle there last week, on average, brought as much as $131.68 per cwt.
Profit-taking and deferred-month live cattle futures weakness dropped CME feeder cattle.
May feeder cattle ended 0.950 cent per pound lower at 148.600 cents. 
Lower Hog Settlement
Profit-taking and Monday morning's lower cash prices pressured CME lean hogs, said traders.
They said futures' premiums to the exchange's hog index for April 27 at 59.64 cents exerted additional market pressure.
May closed 0.850 cent per pound lower at 66.125 cents. Most-actively traded June ended 0.725 cent lower at 73.275 cents. 
Monday morning's average cash hog price in Iowa/Minnesota was at $55.37 per cwt, down 87 cents from Friday in extremely light volume, the USDA said.
Hog numbers have declined seasonally which could support cash prices, a trader said.
USDA quoted Monday morning's average wholesale pork price up 43 cents per cwt from Friday to $74.89, mostly helped by $4.28 higher pork bellies.
Packers on Monday processed 421,000 hogs, 21,000 fewer than a week ago, according to USDA estimates.