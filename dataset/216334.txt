There have been worse years for weather, but 2017 was one where Mother Nature’s hand changed many fruit and vegetable markets.
The year started with heavy snows in the Northwest collapsing buildings that held storage onions, among other items. Then a freeze in the Southeast just before it turned spring cost vast acres of summer fruit, hitting peaches and blueberries hardest.
But it was hurricane season that pummled all sides of the Gulf Coast region, first with Harvey and then Irma. Damage estimates topped $2.5 billion in Florida alone.
It wasn’t all bad news on weather though. Winter rains eased the California drought significantly.
 
Hurricanes
Nov. 13 – Florida governor proposes $21 million for citrus
By Ashley Nickle
Florida Gov. Rick Scott plans to recommend a $21 million investment in the citrus industry in his 2018-19 budget proposal.
Scott has suggested $10 million for citrus greening research, $7 million for replacing citrus trees destroyed by Hurricane Irma, and $4 million for Department of Citrus promotion of the industry, according to a news release.
Irma is estimated to have caused at least $760 million in losses to citrus, with many growers reporting they lost at least half of their crop.
 
Oct. 23 – USDA declares disaster for 19 Florida counties
By Ashley Nickle
The U.S. Department of Agriculture has issued a natural disaster declaration for 19 Florida counties, acknowledging widespread damage by Hurricane Irma.
The declaration lets farmers and ranchers in those areas seek support, including emergency loans, from the Farm Service Agency, according to a news release.
Florida agriculture commissioner Adam Putnam said in a news release, “Our preliminary estimates peg the total damage at more than $2.5 billion, but it’s important to recognize that the damage is still unfolding.”  
 
Oct. 9 – Industry losses estimated at $760M
By Ashley Nickle
In the wake of Hurricane Irma, Florida citrus industry losses have been estimated at more than $760 million.
More than 420,000 acres were affected by the storm, according to a report by the Florida Department of Agriculture and Consumer Services. Losses were heaviest in Collier and Hendry counties, which estimated losses at $2,500 an acre for more than 94,000 acres.
In addition to fruit drop and infrastructure damage, growers have “major concerns” about tree mortality due to flooding, which is not included in the estimate.
 
Sept. 18 – Growers assess Irma damage, seek federal aid
By Tom Karst
Hurricane Irma left Sunshine State citrus groves with dropped fruit, standing water and dashed hopes.
While tomato, strawberry and vegetable growers came through the storm in comparatively better shape, no part of the Florida produce industry was untouched by the Sept. 10-12 storm.
Industry leaders will lobby for federal relief efforts as loss estimates are determined, said Mike Stuart, president of the Florida Fruit & Vegetable Association, based in Maitland.
 
Sept. 4 – Supply chain rebounds, but Houston hurts
By Tom Karst
While many supermarkets and restaurants are reopening, devastation caused by Hurricane Harvey and resulting flood waters will be felt for years in Houston.
“Whatever you see or hear about the storm here, multiply it times 100 — that’s how bad it is here,” Craig Butler, president of North Side Banana Co. Inc., Houston, said Aug. 31. “The city is underwater and it is still flooding.”
Harris County officials said 30,000 to 40,000 homes were destroyed by flooding caused by Harvey, which struck Texas Aug. 25. Killing at least 31, the storm dumped 50 inches of rain east of Houston, the most ever recorded in the continental U.S. from one storm.
As bad as the storm was, the produce supply chain has been resilient.
Butler said Aug. 31 there have been small disruptions in the banana supply, with shipments diverted from Galveston to Florida during the storm. Galveston had reopened by Aug. 31 but the Port of Houston remained closed, he said.
 
Southeast freezes
July 3 – Georgia deals with 70% loss of peach crop
By Jim Offner
A mid-March freeze knocked out more than blueberries in Georgia this year.
The freeze, which came as a knockout punch after a milder-than-normal winter, will cost the Peach State about 70% of its peach crop this year, said Gary Black, the state’s agriculture commissioner.
“It’s a tough year for our peach producers in mid-Georgia,” he said, noting the middle region is the heart of Georgia’s peach production.
Growers in the northern part of the state didn’t fare as poorly, Black said.
 
March 27 – Southern freeze slashes blueberry, peach crops
By Jim Offner
A recent freeze may cost Georgia three-quarters of its $400 million blueberry crop, agriculture officials there say.
“Conservatively, blueberries are where we have our greatest challenge,” Georgia Agriculture Commissioner Gary Black said. “Conservatively right now, we pretty much believe 60% of the crop is gone ... That could grow to 70% to 75%.”
It will be a while before officials can arrive at a more precise total, Black said.
“There’s no doubt — it’s a pretty devastating blow to our producers,” he said.
Similar reports were coming out of south-central Georgia, the heart of the state’s blueberry production.
“Within 50 miles of my area, you’ve got 60% to 70% of the blueberry crop in Georgia, and we’ve probably lost 70% to 90% of the crop, and some farms, 100% of the rabbiteye crop,” said Allen Miles, owner of Miles Berry Farms in Baxley, Ga.
 
Drought eases
April 24 – Rains caused delays, relieve drought conditions
By Jim Offner
Wave after wave of late-winter rains flooded fields, caused crop delays and played havoc with planting schedules.
But Salinas Valley produce grower-shippers say they couldn’t be happier.
“For the Salinas Valley, it has brought a much-needed cleansing of the soil by helping to leach unwanted salts below the farmed portions of the soil,” said Martin Jefferson, production manager for Northern California with Oviedo, Fla.-based Duda Farm Fresh Foods, which grows vegetables in Salinas, Calif.
Years of parched conditions had turned worries about water shortages into a yearly routine for growers, Jefferson said.
Dan Canales, vice president of sales for Salinas-based Ippolito International, said the drought is not over.
“We’ve heard it’s going to take at least another two or three events like this year to get us out of the drought situation, but there’s no doubt it has helped,” he said.
 
SNOW
Jan. 23 – Onion prices on rise as snow stymies Idaho, Oregon
By Ashley Nickle
Grower-shippers in the Treasure Valley region said onion prices are likely to spike as snow continues to hamper production.
Idaho and eastern Oregon received a second severe snowstorm in less than two weeks Jan. 18 and Jan. 19, prompting more building collapses.
Eight onion companies confirmed to The Packer that they had lost at least one building; at least five had three or more cave in.
Numerous grower-shippers shut down production after the second storm to dedicate all resources to shoveling snow off roofs of remaining buildings. Some had lost packing sheds, and others lost storage sheds with onions inside.
“This is a bona fide catastrophe,” said Kay Riley, general manager of Nyssa, Ore.-based Snake River Produce.
Ontario, Ore.-based Murakami Produce was one of few that had escaped damage immediately after the second storm.
“We’re crossing our fingers,” onion sales manager Chris Woo said Jan. 20. “It’s terrible down here.”
With the storms causing so much loss, some companies have invoked “act of God” clauses in contracts because they cannot fill orders, Woo said.