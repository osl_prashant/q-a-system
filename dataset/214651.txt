Tyson Foods Inc shares climbed to their highest level in more than a year on Monday as the No. 1 U.S. meat processor said low prices for livestock feed will help boost results again next year.
Tyson, the maker of Ball Park hot dogs and Jimmy Dean sausages, reported higher earnings and revenue than analysts expected for the quarter ended Sept. 30, sending shares up 1.4 percent in afternoon trading.
The company’s feed costs fell $65 million in the quarter as net income attributable to Tyson rose by $3 million to $394 million. For fiscal 2017, feed costs dropped by $80 million after four years of bumper harvests lowered prices.
Profits have also soared this year for other chicken processors. Pilgrim’s Pride Co last week reported quarterly income attributable to the company more than doubled.
Feed costs should stay flat in 2018, Tyson Chief Executive Tom Hayes said, because this year’s corn and soybean harvests will again be at or near record levels.
“Certainly feed remains a huge component,” he told reporters on a conference call. “We are always looking at corn and soy.”
Tyson’s poultry business benefits most from cheap feed because grain accounts for more than half the cost of growing a chicken. The company also processes hogs and cattle.
JPMorgan said Tyson reduced its estimate for 2018 feed costs by about $100 million.
While livestock producers are benefiting from cheap feed, U.S. crop handlers such as Bunge Ltd and Archer Daniels Midland Co have struggled because large global grain supplies have reduced export demand.
By contrast, strong beef and pork exports helped Tyson’s earnings, Hayes said.
U.S. beef shipments from January to September jumped 14 percent over a year earlier and pork exports were up 8 percent, according to the U.S. Department of Agriculture.
Japan and Mexico are buying Tyson’s pork, while Asia was a main market for its beef, Hayes said.
Sales in Tyson’s beef business, its largest by revenue, rose 9.5 percent in the quarter, while operating income more than doubled.
Chicken sales rose 8 percent. In 2018, they are set to increase three percent by volume and adjusted operating margins should rise to 11 percent from 10.6 percent, the company said.
Tyson forecast total sales of $41 billion for the year ending September 2018, ahead of analysts’ average expectation of $40.36 billion, according to Thomson Reuters I/B/E/S.
Excluding one-time items, the company earned $1.43 cents per share in the fourth quarter, ahead of analysts’ estimates of $1.38. The Springdale, Arkansas-based company’s revenue rose 10.8 percent to $10.15 billion, above expectations for $9.89 billion.