Bakersfield, Calif.-based Grimmway Farms awarded 65 college scholarships at its annual company picnic. 
The spring “Farm to Table” picnic honors the company’s employees and their families, according to a news release.
 
“We’re proud to help these young people pursue their academic dreams at colleges and universities across the nation. By investing in them, we are investing in the future success of our company, our industry, and our community,” Jeff Huckaby, president and CEO of Grimmway Farms, said in the release.
 
The company has awarded scholarships since 2005. Scholarships are based on achievement and provide support to students attending two- and four-year colleges and universities.