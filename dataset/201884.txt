Farm Diesel

Farm diesel is unchanged on the week at $1.94.
Two weeks ago we advised subscribers book 70% of expected harvest diesel needs and to prepare to book the remaining 30% soon.
Distillate supplies fell 1.7 million barrels last week, now 3.5 million barrels below the year-ago peg.
Our farm diesel/heating oil futures spread is at 0.31 which is a strong indication of pending near-term farm diesel price support, but is slightly softer than last week.

 
Propane

Propane is unchanged at $1.16 cents per gallon this week.
No state posted any movement whatsoever this week on propane pricing which strongly suggests prices have bottomed and will stay at currnet levels until bids are reset on Sept. 1.
Propane supplies firmed 0.025 million barrels on the week but remain 24.273 million barrels below year-ago.
Propane may be consolidating at this price point and preparing for a spring higher in response to seasonal demand. Prepare to book harvest and winter propane needs soon.

 





Fuel


7/31/17


8/7/17


Week-over Change


Current Week

Fuel



Farm Diesel


$1.93


$1.94


Unchanged


$1.94

Farm Diesel



LP


$1.16


$1.16


Unchanged


$1.16

LP