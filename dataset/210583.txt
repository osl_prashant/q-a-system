For dairy managers dealing with metabolic issues during transition, subclinical hypocalcemia (SCH) can be one of the more frustrating challenges to overcome. It’s one of those issues that you know exists, but the extent is often unclear and the impact unknown.You can quickly spot cows with clinical hypocalcemia, however cows with SCH are difficult to identify.
That’s because cows with SCH do not show the clinical symptoms associated with milk fever. The cow’s blood calcium level has not yet fallen low enough to cause clinical milk fever, but it’s low enough to cause a cow health and production challenges. The threshold at which this occurs is still unknown.
SCH usually occurs within the first 24 to 48 hours after calving as the cow’s lactation begins.
Get Back to the Basics
There are several fundamental practices you should implement to set cows up for success and minimize incidence of SCH. These include:
Clearly define and record transition cow events. This includes difficult calvings, retained placentas, ketosis, metritis, mastitis and start-up milk production. Use this information to establish a baseline for herd monitoring so that you can assess whether your actions are successful.
Test forages: Consistently analyze forages and byproduct commodity feeds for sodium, potassium, chloride and sulfur by wet chemistry analysis so that dietary cation-anion difference (DCAD) can be calculated.
Formulate the prepartum ration for a negative DCAD. Aim for a ration DCAD of -8 to -12 meq/100g dry matter for 21 days prepartum. This practice is proven to adequately acidify cows and help reduce the risk of clinical hypocalcaemia and SCH postpartum. As a result, more of the total blood calcium becomes available in ionized form, which reduces the risk of SCH and milk fever. Identifying the right anion source to lower DCAD while also delivering metabolizable protein (MP) is vitally important as you formulate prepartum rations.
Monitor urine pH. Urine pH serves as a reflection of blood pH, which assesses the implementation of the negative DCAD ration. Take urine samples from cows that have been fed the close-up diet for at least five days. Consistently collect samples at the same time postfeeding, recognizing that urine pH will vary during the day. Target the following urine pH levels:
 

Breed


Targeted Urine pH Levels


Holstein


6.0 – 6.8


Jersey


5.8 – 6.5

 
If you are adjusting DCAD to meet the targeted levels, increase or decrease DCAD until 80 percent of cows fall in this range. Usually a DCAD of -8 to -12 meq/100g DM should accomplish this goal.
To learn more, visit AHanimalnutrition.com.