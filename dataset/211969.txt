For an updated version of this story, see Trump muses on terminating NAFTA, despite talks 
(UPDATED, Aug. 22)  Negotiations are fast and furious on the North American Free Trade Agreement, but there are few details about proposals made and countered after the first round of negotiations concluded Aug. 20.
 
A trilateral statement from U,S. Trade Representative Robert Lighthizer, Canadian Foreign Affairs Minister Chrystia Freeland and Mexican Secretary of the Economy Ildefonso Guajardo said five days of meetings covered more than two dozen different negotiation topics, but did not disclose what those topics were and if any progress was made.
 
“The scope and volume of proposals during the first round of the negotiation reflects a commitment from all three countries to an ambitious outcome and reaffirms the importance of updating the rules governing the world’s largest free trade area,” according to the statement.
 
How a renegotiated NAFTA would affect the produce industries in the three countries is unknown.
 
U.S tree fruit exporters hope to preserve and extend their substantial export gains in Mexico since the start of NAFTA in 1994. Florida and Southeast growers, in contrast, have complained that NAFTA has led to cheap Mexican imports and has hurt them severely. Those growers want new anti-dumping protections for seasonal/perishable crop producers, and the Trump administration included that domestic protection tool as part of their original NAFTA goals.
 
“We are guardedly optimistic to hear U.S. Trade Representative Lighthizer acknowledge that, despite what he and the president believe about manufacturing, the agriculture sector has benefited greatly from NAFTA,” Jim Bair, president and CEO of the U.S. Apple Association, said in an e-mail Aug. 21. Bair said the U.S. Apple Outlook and  in Chicago, Aug. 24-25 will take a close look at the process, with top diplomats from Mexico and the U.S. to speak on the subject.
 

Reports indicate very little relating to agriculture provisions of NAFTA was discussed during the opening round of discussions, said  Richard Owen, Produce Marketing Association’s vice president of global business development.  The U.S. did not put forward any proposals forward about seasonal perishable trade protections in the first round of talks, he said.
 
Mike Stuart, president of the Florida Fruit and Vegetable Association, said the inclusion of seasonal/perishable trade protection in the reworked NAFTA is important to Florida growers. “The bottom line is that no one should fear it unless they are dumping into an individual market,” he said. Victims of unfair trade practices should have access to trade remedies, he said.
 

Talks will reconvene in Mexico Sept. 1-5.
 
Negotiations will move to Canada in late September and return to the U.S. in late October. Additional rounds are in planning stage through the end of the year, according to the statement.