Fairbanks Scales Inc., Kansas City, Mo., debuted a new line of mobile weighing products that next-day and same-day deliveries.  
The product line includes pallet jacks, forklift scales, and hydraulic weighing devices for faster processing throughout the material handling process. 
 
Fairbanks Scales is the oldest scale company in the U.S., according to a company news release, and it counts produce growers and wholesalers in its customer base.
 
It says the new line of Fairbanks Scales mobile weighing products speed material movement in warehouses, cross-docks, manufacturing facilities and transportation hubs. 
 
Visit the company’s website for product details.