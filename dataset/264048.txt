Sprouts that were distributed to retailers in Minnesota and Wisconsin have been recalled after the Food and Drug Administration detected salmonella in samples.
River Valley Sprouts, Houston, Minn., has ceased production of the sprouts in question, according to a news release posted on the FDA’s recall page. No illnesses have been linked to the sprouts, according to the release.
The recalled products are:

Alfalfa sprouts, 5-ounce package;
Alfalfa sprouts, 4-ounce package;
Alfalfa/garlic sprouts, 5-ounce package; and
Variety sprouts, 5-ounce package.

The River Valley Sprout products were packed and shipped from March 6-15, and distributed to retailers in Minnesota and Wisconsin in plastic cups or clamshells. The release did not identify the retailers.
According to the River Valley Sprouts release, all of the company’s sprout seeds are sanitized at 20,000 parts per million of calcium hypochlorite. Irrigation water is tested for salmonella and E. coli.