Speculators reduced bearish bets across most Chicago grain and oilseed markets last week, but pessimism in the corn market continues to build.In combining Chicago-traded corn, hard and soft red winter wheat, soybeans, soybean oil and meal, and Minneapolis-traded spring wheat, hedge funds and other money managers hold an overall net short position of 345,395 futures and options contracts in the week ended May 9, according to data from the U.S. Commodity Futures Trading Commission.

This is a modest retraction from last week’s combined net short of 386,377 contracts, though the full complex was held back by speculators’ increasing bearishness over corn, as futures prices have been weighed down by improving U.S. weather for planting and declining wheat prices.
Money managers now hold a net short position of 208,642 contracts in CBOT corn futures and options, compared with 184,630 in the week prior. This marks the funds’ most bearish view on the yellow grain since March 8, 2016.

Both Chicago and K.C. July wheat futures tumbled more than 5 percent between May 3 and May 9, but speculators were still on the defense as short-covering was in control last week.
In the week ended May 9, funds cut their net short position in Chicago wheat to 107,892 futures and options contracts from 124,638 contracts in the week prior. The view is still record bearish for the time of year but it has now caught up with 2015.

Funds are now decidedly bullish on K.C. wheat as they extended their net long to 13,692 contracts from 512 in the previous week, as the market is still trying to decide if the western Kansas wheat crop came out of the historic snowstorm on April 29 and 30 unscathed. The new stance is the most bullish for the date since 2014.

July Minneapolis wheat futures were down about 3 percent during the period, but funds still hold a slightly bullish view of 4,670 contracts, a tiny extension over last week’s net long of 4,377 contracts.

The latest Commitment of Traders report stopped one day short of the U.S. Department of Agriculture’s May 10 data dump on the market, as it updated old-crop grain and oilseed supply and demand and published the first look at the new-crop balance. A lower-than-expected U.S. winter wheat production number and sharply declining global corn stocks were generally friendly to the grains, but trade has been somewhat choppy in the days after.
Still, trade sources indicate that funds were mostly been net buyers of corn and wheat last Wednesday through Friday.
Signs of Life in the Soy Complex
After a plummeting nearly 9 percent in March, CBOT July soybean futures prices have been slowly trending upwards since the beginning of April, gaining more than 2 percent by May 9. As such, speculative pessimism has since stalled out and has begun turning the corner as growth to the South American soybean harvest has begun to taper and demand for the U.S. product has been steady.
Money managers ditched bearish soybean sentiments to the tune of 13,332 futures and options contracts in the week ended May 9, which was the biggest weekly surge of potential confidence in the oilseed since Feb. 14, when funds were net long 170,668 contracts.
The funds’ new net position in soybeans stands at 34,335 contracts on the short side, compared with 47,667 in the week prior.

Pessimism in soybean oil contracted for the fourth week in a row, mostly on short-covering. Funds now hold a net short of 12,851 futures and options contracts against 29,388 last week, which was the largest weekly move to the positive side in the vegoil since Nov. 1.

Speculators reversed their down-trending stance on soybean meal for the first time since Feb. 21, when they were nearly record long for the date at 73,465 futures and options contracts. Funds now hold a nearly neutral view of the protein-rich animal feed at just 37 contracts net short, compared with 4,943 contracts short in the previous week.

Soybean futures finished the week under pressure as bearish news took the spotlight, and further cuts to the shorts might not be in the cards next time around as trade sources estimated that funds were mostly net sellers across the soy complex late in the week.
Items weighing on trader’s minds late last week included USDA’s boost in global soybean carryout for the old crop year by almost 3 million tons, Brazilian agency Conab’s revised soybean harvest estimate of 113 million tons, and private analytics firm Informa’s call for 89.7 million U.S. soybean acres over USDA’s 89.5 million. Conab’s target stands above the revised USDA figure of 111.6 million tons.