In what appears to be part of a long-running attempt to unionize Taylor Farms' Tracy, Calif., facility, the International Brotherhood of Teamsters is promising to hand out leaflets at grocery stores across the country to highlight what the union says are poor working conditions at the plant.
 
Taylor Farms said in a statement that the union's claims were false and that it was proud of its treatment of employees.
 
The (Salinas) Californian reported Aug. 5  that the union said the group's representatives will hold signs reading "Taylor Farms Makes Bad Salads" and hand out fliers critical of Taylor Farms at retail stores in central and southern California, in addition to supermarkets in Boston, Chicago, Oakland, Denver and New York City.
 
The company issued a statement, e-mailed to The Packer, that refuted the allegations.
 
"Taylor Farms has an award-winning, industry-leading, food safety record. These allegations are baseless and simply a veiled attempt by the Teamsters to impose their will and force bargaining with the union.
 
According to the statement, the company supports employee rights to join a union through a "secret ballot election process." In a vote in March 2014 on the subject , employees decided against representation by the union.
 
"Our offer to the Teamsters has been simple and clear: Win an election of the employees they want to represent and Taylor Farms will negotiate a contract," according to the statement. The company said employees receive competitive wages and benefits, and that worker - and food - safety is a priority.