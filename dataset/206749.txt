The results of a recent producer survey took Purdue University by surprise, according to a recent statement issued by the school. Purdue expected a larger share of respondents would be using dealer financing for capital equipment purchases.Just over half of respondents report not using dealer financing for any capital equipment purchases. That’s about a 5% increase since 2012. Purdue speculates that as farmers have scaled back on equipment purchases, they are also working to pay down existing balances.
 


Capital Item Purchases


© Purdue



 
Meantime, about 60% of respondents said they use no dealer financing to purchase expendable inputs like seed, crop protection or animal nutrition, according to Purdue.
“This is just a 2% increase since 2013,” according to the statement. “The share of producers getting all of their financing for these inputs from dealers continues to decline since first asking the question in 2008.”
 


Expendable Item Purchases


© Purdue



 
Purdue is hosting the National Conference for Food and Agribusiness in November that will take a deeper look at producer procurement behaviors, buying preferences, risk management strategies and more.
?