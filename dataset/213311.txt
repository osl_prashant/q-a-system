Employers are always on the lookout for ways to keep their workers invested and to keep spirits high. They can search every how-to book on employee engagement and or emotional intelligence, but sometimes miss a key factor that is right in front of them: positive reinforcement. 
We at Accelerated Performance Technicians know positive reinforcement increases performance and retention of your team. Negative behavior not addressed will repeat, but positive behavior not reinforced might stop.
According to Cleverism, only 30% of workers across the globe are inspired by their jobs. This is startling, especially with research that shows businesses with engaged workers attain twice the annual net income of those who aren’t as engrossed. 
Luckily, there is a solution. 
Business owners can create a productive and enjoyable culture. Harvard Business designed the ideal praise-to-criticism ratio On average, there should be five compliments for every criticism. 



Creative Ways to Reward Great Employees
To maintain morale, come up with ideas to jazz up the workplace. You can spend some money or no money at all. Celebrate an individual’s success or a team’s victory. 
Here are some examples to help get you started:

VIP Parking spot: Give a model employee a great parking spot for the week.
Working at home: If an employee has reached his or her quota, allow that person to work from home for the day. 
Early dismissal: Allow the productive employee to leave early.
Little breakfast: Bring in food from your local restaurant or coffee shop.
Personal notes: Leave handwritten messages, thanking employees for their service.
Public recognition: Give credit where credit is due at a business meeting.
Volunteer as tribute: Offer to do their least favorite task.
Sports head: Have a conversation about sports they like.
Plus one: Get tickets to an event, like a concert, play or ballgame.
Late night bite: take them and their significant other out to dinner.
Behind the scenes: Honor someone whose contributions aren’t typically spotlighted.
Elated E-mails: Send an email congratulating them on something not work-related, like a birth, wedding or anniversary.

The emotional tone used when delivering positive reinforcement is as crucial as the news itself. In an experiment from Daniel Goleman’s book “Focus: The Hidden Driver of Excellence,” employees rated negative feedback given in a positive tone higher than good news delivered in a negative tone. The boss’s state of emotions affects the workplace atmosphere and is contagious. 
“Good job! Way to go!” These words brighten just about anyone’s day. Simple yet powerful, your wallet and workplace environment will improve as a result.