BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Monday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2553  Up    55May         2455    2522    2417    2502  Up    49Jul                                 2579  Up    60Jul         2502    2571    2474    2553  Up    55Sep         2523    2592    2496    2579  Up    60Dec         2528    2599    2504    2587  Up    62Mar         2510    2590    2499    2579  Up    62May         2504    2591    2504    2581  Up    59Jul         2512    2593    2512    2584  Up    55Sep         2523    2595    2518    2588  Up    52Dec         2565    2601    2564    2591  Up    49Mar                                 2609  Up    49