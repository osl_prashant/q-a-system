Bell pepper
According to the U.S. Department of Agriculture, 1 1/9-bushel cartons of jumbos in late August from western North Carolina sold for $8-10.35 for jumbos and extra large.
Last year in late August, the USDA reported $12-14.95 for extra large and $12-13.95 for large.

Cucumbers
In late August, the USDA reported 1 1/9-bushel cartons of waxed medium cucumbers from Michigan selling for $14.85-16.85 for mediums with cartons of 24s at $6.35-7.35. Last year in late August from Michigan, the 1 1/9-bushel cartons sold for $16-18.85 with cartons of 24s fetching $8-8.85.

Cabbage
In late August, the USDA reported $10 for 50-pound cartons of round green medium from New York.
Last year in late August, those brought $6-7.

Corn
In late August, the USDA reported wirebound crates of 4-4½ dozen from New York selling for $7-8.55 for yellow and bicolor.

Last year in late August, the USDA reported $9-11.85 for yellow and bicolor.

Eggplant
In late August, the USDA reported 1 1/9-bushel cartons mediums from Michigan selling for $12-14.85.
Last year in late August, the USDA reported $18-20.85.

Green beans
In late August, the USDA reported bushel cartons/crates of machine-picked round green from central and western New York selling for $14.
Last year in late August, those fetched $20-24.

Squash
In late August, the USDA reported these prices from western North Carolina: Half- and 5/9-bushel cartons of small zucchini $14-16.35, medium $12-14.35. Small yellow straightneck sold for $14-18.35 and medium $12-16.35. Three-quarter-bushel small yellow straightneck sold for $16-20.35 and medium for $10-16.35.
Last year in late August, the USDA reported these prices from western North Carolina: Half- and 5/9-bushel cartons of small zucchini $12-14.95, medium $10-12.95. Small yellow straightneck sold for $16-18.85 and medium $14-16.95. Three-quarter-bushel small yellow straightneck sold for $22-22.95 and mediums for $16-18.95.