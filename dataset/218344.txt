New Zealand-based Enzed Exotics expects good volume and quality for its kiwano melon — aka horned melon —this season.
The first sea shipment to the U.S. left Jan. 19, according to a news release. The company expects to ship up to 50,000 trays of kiwano melon this year.
Enzed reports that sales of the fruit have grown rapidly in the last couple of years. The company sends the majority of its kiwano melons to the U.S.
“Now is the time for the produce industry to really capitalize on the movement towards wellness and plant-based eating,” owner Vanessa Hutchings said in the release.
The company is also seeking sales opportunities in other regions.
“We’ve expanded our sales and marketing team recently, and we’re using our extra resources to explore new opportunities,” Hutchings said in the release.
Kiwano quality has been solid in part because of cooperative weather.
“Because it’s originally a desert fruit, it doesn’t much like the rain,” Hutchings said in the release. “December was ideal growing conditions to create a lush leaf cover to protect the fruit growing underneath. A storm over the New Year damaged a very small number of fruit on the exterior of the canopy. However, the main growth was protected and it’s looking beautiful.”