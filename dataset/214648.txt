Photo courtesy Georgia Ports Authority
The Georgia Ports Authority has approved projects to increase capacity at the Garden City Terminal through rail and gate expansion projects.
The terminal is the largest container terminal in North America, according to a news release, and the authority recently reported a 32% growth in container volume in October, a record for the Savannah facility.
The Georgia Ports Authority Board recently approved expenditures of $42.27 million as part of a $128 million Mason Mega Rail Terminal, according to the release. The project will double rail capacity and $90.7 million has been allocated to the project so far, according to the release.
Construction is set to begin in December, with a late 2020 completion date.
“What makes Georgia and the Georgia Ports Authority a continuing success story is the relentless effort to stay one step ahead of the curve and the competition,” authority board chairman Jimmy Allgood said in the release.
Another $13.2 million has been allocated to expand a gate at the Garden City Terminal to meet future growth, according to the release. The project is designed to make it easier for trucks to access a nearby interstate.