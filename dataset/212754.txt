(UPDATED MAY 8) Avocado prices are running twice as high as they were a year ago, with little or no relief in sight, as Cinco de Mayo kicked off another peak consumption season in the U.S.“We expect (prices) to stay strong all through June and July, and maybe the market will start seeing more supply from Mexico in August and September,” Chris Varvel, sales and logistics director with Escondido, Calif.-based Henry Avocado Corp., said on May 5.
An off-bearing year in California — about half of last year’s crop — and less volume in Mexico than recent years, combined with continued growth in demand across the U.S., have propelled prices to about twice what they were a year ago, marketers said.
“You go through crop cycles, and we happen to be going through a cycle where supply from Mexico and California are both lower, so we have lower availability in the U.S. market than there has been in years,” said Rob Wedin, vice president of sales and marketing for Santa Paula, Calif.-based Calavo Growers Inc.
As of May 4, according to the U.S. Department of Agriculture, two-layer cartons of Hass avocados from Mexico were $50.25-54.25 for size 32s, 36s and 40s, $52.25-55.25 for 48s, $44.25-47.25 for 60s; $38.25-43.25 for size 70s; and $28.25-30.25 for 84s.
A year earlier, the same product was $25.25-28.25 for size 32s and 36s; $26.25-29.25 for 40s; $26.25-30.25 for 48s; $21.25-24.25 for 60s; $19.25-22.25 for 70s; and $12.25-15.25 for 84s.
California production is peaking, but it is generating predominantly small fruit, said Josh Underseth, salesman with Fallbrook, Calif.-based Del Rey Avocado Co. Inc.
“California is going strong; we just don’t have any big fruit at all,” Underseth said.
Sixties are most common, he said.
“It’s probably going to be 60s all season,” Underseth said the first week of May. “We haven’t packed a single box of 32s or 36s all this week and, in 40s, we’re getting a couple of pallets here and there and a decent amount of 48s. But, where I’ve got 1,000 boxes of 48s, I have 6,000 boxes of 60s. and, it’s not like we’re getting a ton of 70s or 84s, either.”
“We created demand with good supply and good prices, and this year we have less supply and good demand, so prices are higher,” Varvel said.
Mexico, which shipped 1.9 billion pounds of avocados to the U.S. in 2016 and holds a U.S. market share of about 80%, likely will top out at about 1.6 billion this year, Wedin said, noting that California’s projected output is about 200 million pounds, or about half of its 2016 volume.
Meantime, demand in the U.S. continues to rise about 15% per year, Wedin said.
“Now, volume is down and if that demand is continuing to increase 15%, that’s going to drive prices high,” he said. “The core users of avocados are really set on avocados being an extremely important part of their diet.”
Supplies will recover; it simply will take some time, Wedin said.
“Mexico’s (next) season starts July 1, but we believe by September, we’re going to see good supplies from Mexico and then, next year, very good supplies from California.
California’s season generally runs from late February or early March to September, Wedin noted.
An official crop estimate is expected from Mexico “in the next couple of weeks,” Wedin said.
Peru, which has dealt with heavy rains and flooding, has begun to ship some fruit in small volumes, but volumes from there during its June-August peak likely won’t provide much price relief, Wedin said.
Peru ships fruit to other markets, Wedin said May 5.
“I’m getting word that maybe we’ll see increases from them about three weeks from now.”
Peru’s volume — perhaps 160 million pounds this year — may help to “stabilize things,” but it won’t do much to pull down prices, said Robb Bertels, vice president of marketing with Oxnard, Calif.-based Mission Produce Inc.
Peru, which has dealt with heavy rains and flooding, may have a total avocado volume of 500 million pounds this year, but it also has large markets in Europe and Asia, Bertels said.
“I think it will kind keep (prices) where they are but not bring a whole lot of relief,” said Bertels, who said he visited Mission’s three Peruvian ranches in the first week of May. “It will help fill in some of the (gaps in) the new crop, but prices may not come down until the new Mexican crop starts to peak in earnest in September.”
South America’s avocado growers often look to European and Asian markets, so as not to depend heavily on the U.S., Wedin said, noting that Chile’s U.S.-bound volume in 2016 was about 55 million pounds.
Peru may bring a bit of a cushion on price, Underseth said.
“Peru has started landing on the East Coast, and we may see some price relief, but we’re not planning to bring any out West; we’ll see how it goes,” he said.
Mexico likely will start shipping more volume before its season winds down, Underseth said.

“We’re hearing they have a decent amount of fruit left and will go hard until they start slowing down in middle of June,” he said.“We expect (prices) to stay strong all through June and July, and maybe the market will start seeing more supply from Mexico in August and September,” Chris Varvel, sales and logistics director with Escondido, Calif.-based Henry Avocado Corp., said on May 5.