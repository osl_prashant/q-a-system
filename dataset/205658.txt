Efficient nitrogen fertilizer applications closely coincide with plant needs to reduce the likelihood that nutrients are lost to the environment before they can be taken up by the crop.Fall nitrogen application occurs during the fall months before the crop is planted, spring application occurs in the spring months (before planting for spring-planted crops), and after-planting application occurs while the crop is growing.
The most appropriate timing of nitrogen applications depends on the nutrient needs of the crop being grown.
In general, applying nitrogen in the fall for a spring-planted crop leaves nitrogen vulnerable to runoff over a long period of time.
Applying nitrogen after the crop is already growing, when nitrogen needs are highest, generally minimizes vulnerability to runoff and leaching.
Cotton farmers applied a majority of nitrogen -59 percent -after planting.
Winter wheat producers applied 45 percent of nitrogen after planting.
Corn farmers applied 22 percent of nitrogen after planting, while spring wheat farmers applied 5 percent after planting.
Farmers applied a significant share of nitrogen in the fall for corn (20 percent) and spring wheat (21 percent).
Fall nitrogen application is high for winter wheat because it is planted in the fall.
This chart is found in the ERS report, Conservation-Practice Adoption Rates Vary Widely by Crop and Region, December 2015.