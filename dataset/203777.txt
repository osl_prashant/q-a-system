Giro Pack Inc. has introduced new bag styles that feature increased product visibility.
The Window Ultrabag and Window Compact bags join the Vidalia, Ga.-based Giro's lineup of mesh bags.
Window shapes are easily customizable, which allows users to choose from conventional shapes including round, square and rectangular, or create designs to make their packaging distinctive, according to a news release.
The Window Ultrabag is a package "with a view" while the Compact includes a horizontal design that incorporates a large window, according to the release.
The Window Ultrabag is a new concept based on the popular Ultrabag system. The new version has increased product visibility, according to the release.
The Window Ultrabag weight, when filled, can vary from 17.5 ounces to 105 ounces and was designed for produce including apples, avocados, citrus, onions and potatoes.
Options include finger holes or handles for ease of gripping and transportation.
Existing GirBaggers customers can run the bag without additional expense, according to the release.
The Window Compact was designed for similar commodities to the Ultrabag and can weigh between 10.5 ounces and 35 ounces when filled.
It features product breathability and the compact design allows it to be easily displayed in grocery stores, according to the release.
By buying a modification kit, current GirBaggers customers can run the bag, according to the release.