Produce marketing leaders give “ripe and rotten” marketing ideas at BrandStorm 2017, Nov. 14. From left, Dan’l Mackey Almy, Steven Muro, Mike Caplan, Karen Nardozza, Missy McDill and podcaster Jay Acunzo.
 
SAN FRANCISCO — Consumers make most decisions emotionally and then use rational reasons to justify them, produce marketers learned at United Fresh’s BrandStorm 2017.
More than 225 produce industry members, mostly in marketing, were challenged Nov. 13-14 to tap into consumers’ emotions among other ways to both differentiate the brands they represent and grow overall consumption.
In this third annual BrandStorm, Mary Coppola, senior director of marketing communications for the Washington, D.C.-based United Fresh Produce Association, credited the more than 40 members of its marketing and merchandising council for identifying critical produce marketing themes for this year’s event.
“The council is the industry, and it builds the program,” she said.
In one session, five marketing executives discussed the “ripe and rotten” marketing ideas they see from the produce industry.
Steve Muro, president of Fusion Marketing, said it’s good that the industry is moving away from a commodity mindset, but companies still have to be different.
“We can’t just have a farmer and his dirt like everyone else,” he said.
Mike Caplan, co-founder of Fiction Tribe, said content marketing is good, but it’s even better when it co-brands with retail partners.
Dan’l Mackey Almy, CEO of DMA Solutions, said she sees too much reliance on scan data.
“That’s just what happened at one point,” she said.
She said produce companies need to see beyond their category competitors and consider competition to be whatever we’re not doing and eating.
Karen Nardozza, CEO of Moxxy Marketing, said produce companies feel compelled to be on social media, but without a clear strategy, it’s often not worth the investment.
Stop copying competitors, said Missy McDill, CEO/creative director for McDill Associates.
Jay Acunzo, who runs the podcast Unthinkable, said because of so many best practices for everything, it’s never been easier to be average.
“You have to question conventional thinking in your own context,” he said. “Trust your intuition.”