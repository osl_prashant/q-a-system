Recent editorials published in Iowa newspapers
Recent editorials published in Iowa newspapers

By The Associated Press
The Associated Press



Des Moines Register. April 26, 2018
Iowa Legislature once again imposes unnecessary job licensing
One-third of Iowa's workforce must obtain a license from state government to earn a living. This is a higher percentage than any other state and three times higher than some states, according to a 2015 White House report on occupational licensing.
Iowans need approval from Big Brother to legally pluck an eyebrow, massage a back, interpret sign language and perform dozens of other tasks for pay. Some licenses have nothing to do with protecting public health and safety and everything to do with protecting current industry workers from competition.
In short, state-mandated job licensing requirements have run amok in Iowa. Both Democrats and Republicans, including former Gov. Terry Branstad, have acknowledged this. Overzealous and frequently unnecessary government requirements stifle economic growth, make it more difficult for Iowans to get jobs and discourage entrepreneurs.
If you want to open a bar for blow-drying hair, you abandon the idea when you find out wielding a dryer requires 2,100 hours of training at a for-profit cosmetology school. Small business owners offering simple teeth-whitening services were forced to close because dentists persuaded the Iowa Legislature that people need years of formal training to bleach teeth.
Elected officials should be scrambling to reduce onerous licensing laws. So what were Iowa lawmakers thinking when they passed Senate File 2228 this session with almost unanimous support? What was Gov. Kim Reynolds thinking when she signed it?
They weren't thinking. They were doing exactly what their predecessors did for decades that landed the state in this mess: wantonly requiring licenses for whatever group of workers wanted to be recognized and legitimized by state law.
Senate File 2228 adds "genetic counselors" to the already lengthy list of workers under the thumbs of state-sanctioned boards made up of private-sector industry insiders. A University of Iowa press release on the new law, which goes into effect Jan. 1, 2019, describes the estimated 25 genetic counselors in the entire state as health care workers "who advise patients and family members about hereditary health risks."
So much for smaller, less burdensome government. Here comes more language in Iowa Code, more administrative rules, more hoops for future workers to jump through, and more work for the Iowa Board of Medicine, which is charged with oversight.
Then there is the issue elected officials may not have anticipated with this new law: infringing on Iowans' constitutional rights, specifically the right to free speech.
A Register editorial writer sent the bill language to Paul Sherman, an attorney at the Institute for Justice. This is the libertarian organization that sued Iowa for requiring people to have a cosmetology license to braid hair, which prompted legislators to amend the law. This is the group that has filed numerous lawsuits challenging licensing laws in numerous states — and prevailed.
The wording of the bill "raises very serious First Amendment concerns," Sherman said. "Part of the problem is the scope of practice is written so broadly" it could prohibit, for example, the mother of a child with a genetic disorder from counseling another mother with a child with a genetic disorder.
"It comes down to whether the board will enforce this to the letter of the law. We have learned through hard experience that relying on boards to exercise discretion is a recipe for overreach," Sherman said.
He cited the example of John Rosemond, a newspaper advice columnist in Kentucky who received a cease and desist order from the state attorney general to stop publishing his column or face fines and jail. The attorney general and Kentucky's psychologist licensing board said the column constituted the "unlicensed practice of psychology."
The wording of Iowa's new genetic counseling licensing law "has the potential to sweep up speech that should be protected," Sherman said.
Then there is the simple irony of lawmakers requiring licenses for more workers a mere 14 months after the GOP-controlled Legislature seemed to finally grasp the need to reform and reduce licensing laws. A bill last legislative session sought, among other changes, to prohibit the executive branch from imposing regulations on any profession it had not already regulated.
The same group of politicians turned around and added another license to state law.
Why?
Iowans were not complaining that the few dozen genetic counselors in the state were unqualified. The new law has less to do with protecting consumers than it has to do with legitimizing a small group of professionals. Perhaps a license will make it easier for them to bill health insurers, Sherman observed.
The hypocrisy of "smaller government" politicians is on full display. And Iowa's embarrassing ranking in national studies about the burdens of occupational licensing is secure.
____
Dubuque Telegraph Herald. April 27, 2018
IBM's jobs slump not all bad newsEmployment at IBM's operation in Dubuque is less than one-third its peak of 1,300 in 2012. The tech giant's IT service center in the Roshek Building employs just over 400 people these days, a fraction of the impact it once had here.
Echoing from some segments of the community is the chorus, "I told you so."
Before IBM opened here in 2009, promising to build a workforce of 1,300, it required plenty of coaxing in the form of substantial state and local incentives.
The development agreement included a $52 million incentive package from the city and state, $24.5 million of which went to Dubuque Initiatives for the renovation of the Roshek Building.
It took a deadline extension, but IBM did hit its employment requirement of 1,300. That occurred during 2012, after which the number has steadily declined.
It's true, things have changed.
But if we had known then what we know now, would we still invite IBM into our community? The smart money says yes.
The package to win over IBM likely wouldn't have been as sweet, but even in retrospect, it's clear that IBM has brought value beyond jobs.
In return for the incentive package at the time — during the worst economic times the nation experienced since the Great Depression — the global brand invested more than $100 million to open a huge office in a beautifully restored building in the heart of downtown Dubuque.
As IBM has cut workforce, other employers adding jobs, including Dubuque-based Cottingham & Butler and Heartland Financial, have moved in. In much of the office space IBM formerly occupied, there are employees taking home paychecks.
Having a beautifully restored space ready for these employers has been a positive. What would be occupying the Roshek Building today if not for the massive renovation project?
Sure, it would be great if IBM were at capacity. But its cutbacks underscore the importance of a diversified employment landscape. Dubuque knows all too well what it is like when the community relies too heavily on one or two businesses — as Dubuque did in the 1980s with John Deere Dubuque Works and Dubuque Pack.
Today, we see the economic ebb and flow of business. When one company pulls back, another might be ready to double down. That's what has happened, and that's what we hope will continue.
Besides, 400-plus jobs is not nothing. That still ranks IBM among the top 25 largest employers in town. If we had an opportunity to bring a company with 400 jobs to Dubuque, we would pave the way to make that happen.
Next year marks the end of the 10-year lease IBM has at the Roshek Building. Discussions about extending that lease will begin this summer. We can't predict what IBM will choose to do, but certainly the dedicated workforce, beautiful building and cooperative local officials will weigh in favor of the company remaining in Dubuque.
____
Sioux City Journal. April 25, 2018
Peggy Whitson: An inspiration to all Iowans
In a year full of partisan contention at the Statehouse in Des Moines, lawmakers came together in commendable fashion this month for deserved recognition of an accomplished native daughter.
Peggy Whitson — who grew up on a farm near Beaconsfield and graduated from Mount Ayr High School and Iowa Wesleyan College in Mount Pleasant — holds the record for most days in space, 665, by an American astronaut (the old record was 534 days). She finished her third deployment — a 289-day stint — to the International Space Station in September.
In a resolution adopted by both chambers, the House and Senate: "Congratulates Dr. Peggy Whitson on her remarkable lifetime of accomplishments; commends Dr. Whitson for her outstanding contributions to the development of international cooperation and for exemplifying that successful endeavors require the teamwork of those with diverse skills and backgrounds and relationships built on mutual trust and respect, whether on the International Space Station or on Earth; and thanks Dr. Whitson for steadfastly promoting interest in STEM and inspiring both girls and boys, no matter the size of their hometown, to dream big, work hard, and reach for the (moon and) stars."
During her visit to the Capitol, Whitson said she never forgets her Iowa roots.
"Everyone's always asking me what made you successful and I would say learning the work ethic of a farmer was a key to that," Whitson said for an Iowa Public Radio story. "My parents ... hardest-working people I know."
Since the "Right Stuff" days of John Glenn, Americans have venerated those who possess the courage, skills and knowledge necessary for manned missions to space. We imagine many Americans have, in fact, dreamed at one time or another of life as an astronaut and we know those dreams rest in the hearts and minds of boys and girls in classrooms of Iowa schools today.
In other words, Whitson inspires Iowans, regardless of background or age.
We join our legislators in honoring a woman all of us in this state should be proud to claim as one of our own.
___
Fort Dodge Messenger. April 25, 2018
There's good news from Japan
Iowa's growing ethanol industry just got some very good news. Japan has opened its huge marketplace to ethanol manufactured from corn. Until the recent decision by the Japanese government to make this change most of the ethanol sold there was produced from sugarcane.
This is a major boost for the Hawkeye State because Japan has an enormous economy. Its GDP ranks third in the world behind only the United States and China.
Iowa Gov. Kim Reynolds issued a statement Friday welcoming this important development.
"Today is a great day for Iowa farmers and our state's renewable fuels industry," Reynolds said. "With Japan opening its market to U.S. ethanol, Iowa can further expand our trade opportunities in that region of the world. Agriculture at the hands of hardworking farmers is a vital piece of the U.S. economy. Now, more than ever, ethanol production strengthens Iowa's ability to continue as an agricultural leader by giving farmers another market for their commodities."
Iowa leads the U.S. in ethanol production. Our state has 43 ethanol refineries. According to the Iowa Renewable Fuels Association, they are capable of generating 4.4 billion gallons of ethanol per year.
The ability of Iowa ethanol producers to sell in Japan should strengthen an already robust industry. It currently contributes about $4.6 billion to our state's economy each year. The ability to market Iowa-produced ethanol in Japan should help increase that impressive figure.
The Messenger welcomes this exciting economic news. International sales are important to the prosperity of our state. The addition of a major marketplace for our ethanol producers is an exceptionally positive development.
____