BC-Merc Table
BC-Merc Table

The Associated Press

CHICAGO




CHICAGO (AP) — Futures trading on the Chicago Mercantile Exchange Wed:
Open  High  Low  Settle   Chg.CATTLE                                  40,000 lbs.; cents per lb.                Apr      118.57 119.07 118.47 118.97   +.92Jun      105.30 105.97 104.95 105.27   +.37Aug      104.72 105.30 104.65 105.15   +.70Oct      109.05 109.50 109.00 109.47   +.67Dec      113.37 113.67 113.17 113.55   +.63Feb      115.25 115.57 114.95 115.22   +.47Apr      116.00 116.40 115.85 116.10   +.53Jun      109.37 109.97 109.37 109.57   +.60Aug      108.00 108.75 108.00 108.45   +.70Est. sales 39,940.  Tue.'s sales 51,465 Tue.'s open int 344,649,  up 44        FEEDER CATTLE                        50,000 lbs.; cents per lb.                Apr      138.92 139.32 138.32 138.75   +.03May      140.60 141.40 140.00 140.67   +.42Aug      145.35 146.35 145.12 146.05   +.98Sep      146.80 147.55 146.42 147.30   +.80Oct      147.25 148.20 146.97 147.97  +1.05Nov      147.07 147.87 146.62 147.60  +1.05Jan      143.82 144.00 143.30 144.00   +.90Mar      141.50 141.50 141.10 141.10   +.50Est. sales 10,762.  Tue.'s sales 13,285 Tue.'s open int 49,750                 HOGS,LEAN                                     40,000 lbs.; cents per lb.                May       68.00  69.97  67.85  69.85  +1.95Jun       76.80  78.95  76.77  78.52  +1.77Jul       79.40  81.30  79.35  81.05  +1.83Aug       79.05  81.00  79.05  80.55  +1.58Oct       67.95  68.92  67.92  68.72   +.80Dec       62.10  62.85  62.10  62.77   +.72Feb       65.75  66.40  65.75  66.37   +.67Apr       69.22  69.85  69.22  69.80   +.55May       74.00  74.40  74.00  74.40   +.40Jun       78.00  78.20  78.00  78.20   +.80Jul                            77.65   +.65Aug                            77.25   +.65Est. sales 51,423.  Tue.'s sales 32,003 Tue.'s open int 240,464                PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.