When your bulk tank average is 120 lb/cow/day, the biggest challenge is not getting it to that level, it’s keeping it there.The key to doing that, believe Jeff and Connie Horsens, Cecil, Wis., is consistency: Consistency in how cows are fed and milked and handled every day. “The cow doesn’t know who is doing the feeding or the milking, she just knows if things are the same or different,” says Jeff. “So we try to make sure feed is dropped at the same time every day and pushed up every two hours, and that milking routines are always the same.”
The Horsens, along with their sons Curtis and Ryan, and their herdsman Brad Volkman, manage 600 cows. At the time of our visit in April, the herd average was just shy of 40,000 lb, at 39,991 pounds. First-calf heifers average 110 to 115 lb. and second and later lactation cows average 15 to 20 lb above. The herd is milked 3X in a double-8 parlor.
Consistent milk output is driven by consistent management. “All of our protocols are followed to the ‘T,’” says Connie.
For example, over the course of any week, three different people might mix feed for the herd. So the Horsens use a feeding software to track ingredient loading and mixing times. “Every time we mix a batch of feed, we put our name into the feed record,” says Ryan. “That way, we know who’s mixing the feed each day and whether it’s being done consistently and according to the ration our nutritionist developed for our cows.”
Dry matters of forages are checked on a weekly basis so that adjustments can be made if need be. First-lactation heifers are fed for 63.5 lb. of dry matter intake; second and older lactation cows are fed for 75 lb. of intake.
“We feed for 5% weigh backs to make sure cows never run out of feed,” explains Jeff. The weigh backs are blended into the growing heifer rations, which also are housed at the site.
Other rules: “We stick with basic nutrition, and we don’t change our rations due to economics,” states Curtis.
Even when milk prices fall, the Horsens won’t pull fat or other, higher-priced ingredients out of the ration. Those ingredients are in the diet for a reason. Pulling them out could jeopardize intakes, nutrient densities and digestive patterns which ultimately could affect milk production, he says.
Ration Energy a Challenge
Bill Matzke, a private nutritionist who has been working with the Horsens since 2011, agrees. “At their level of milk production, the challenge is to get enough energy into the diet,” he says. “We do use a fat source, and pulling it out could create issues.”
“If you look at the Horsens, I don’t do a lot different than I do with my other clients,” he says. But he says they excel at five things:
Grow, harvest and store phenomenal forage.
Employ good people to manage and work with the cows.
Provide excellent cow comfort with large, sand bedded stalls that are not over-crowded.
Excel at parlor management.
Use good genetics to underpin it all.
Forage quality and highly digestible corn silage help ration nutrient density. Their milking ration is 58% forage. The Horsens have been feeding BMR corn silage for five years, for example, and are now flying on fungicide at tasseling to maintain forage quality through harvest.
They’re still growing and feeding conventional alfalfa. But they maintain a four-cut schedule even though they’re in the northern half of the state just 45 minutes west of Green Bay.
The high-quality forage allows cows to derive more energy from fiber, says Matzke. “They have exceptionally high feed intakes.”   
“The one thing they do differently is the amount of space they provide cows in the pre-fresh pen,” says Matzke. They never over-crowd the pen, and typically have a 70% stocking rate.
Both springing heifers and older cows are housed together in the pen. But with that low stocking rate, the young heifers have enough space to avoid being bullied by older cows. And with the low numbers, everybody in the pen has plenty of bunk space and access to feed, he says.
High sidewalls for air movement
The curtain-sided barn for dry, pre- and post-fresh cows was built in 2012. With 15’ sidewall openings, it has plenty of air movement. Even in winter with 0°F temperatures, Horsens leave at least 12” of curtain opening to ensure fresh air movement.
“It’s all about cow comfort; the cows depend on us to care for them,” says Connie. Every pen, for milking, dry and pre-fresh, has large fans over the freestall beds and sprinklers over the feed area.
Horsens also maintain a strict fresh-cow protocol. “We pay special attention to cows with retained placentas and those that gave birth to twins, maybe temping up to four days after calving,” says herdsman Brad Volkman. That’s done to ensure any post-calving infections are detected and treated.
Every second and later lactation fresh cow is tested for ketosis four and eight days after calving. Each will also receive a calcium bolus the day of freshening and the day after.
“We also CMT all cows between four and seven days in milk,” says Volkman. “If they’re positive, we’ll culture those and treat the gram positive cows.”
“That approach has cut back dramatically on the number of fresh cows we treat,” says Curtis. “The CMT and culturing is a cheap investment, but it has been a game changer.”
Along with the management team, the Horsens employ 7 additional individuals, 2 of which are part-time.  They credit them for their hard work and attention to detail.
A philosophy for the Horsens is to explain “why” and not just “how” things must be done in a certain way explained Volkman. The biggest thing is protocol compliance.
The Horsens state, “Our employees like cows. And they’re willing to do the things we need done in certain way if we explain how it helps the cow. Then they’re willing to do it, and they become the reason for our success.”
In the end, the Horsens have found a formula that works for them. “We’re not a very quick farm to change things,” says Jeff. “But we do try to pick out one bottleneck at a time and try to fix that. It can be a simple thing, like adding water to cool cows in the holding pen.
“We try to look at it from the cow’s perspective,” he says. His reasoning: If it improves her life, it improves the farm.
Ultimately, that’s the key, says Matzke. “The Horsens are very much about the cows,” he says. “In fact, they are all about the cows.”
 
Note: This story appears in the July 2017 issue of Dairy Herd Management.