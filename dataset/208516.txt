After several months of stagnation, the Purdue/CME Group Ag Economy Barometer demonstrated optimism for its July reading.The rating at 139 is the highest the barometer reading since January 2017, and it’s the second highest level since data collection started in October 2015.
According to researchers, the eight-point increase is because of producers’ perceptions of the ag economy.
Attitudeds have edged higher, primarily by the wheat crop sending futures higher in June and July, said David Widmar, senior researcher at Purdue University.
Compared to summer 2016, attitudes are more positive in 2017.
Watch Widmar's comments on the barometer below.