Earlier this week Secretary of Agriculture Sonny Perdue released USDA’s Farm Bill and Legislative Principles for this year. He outlined his priorities and said they were based on his travels around the country to meet farmers.
“The conversations we had and the people we came across helped us craft USDA’s Farm Bill and Legislative Principles for 2018,” Perdue says. “These principles will be used as a road map—they are our way of letting Congress know what we’ve heard from the hard-working men and women of American agriculture.”
He also committed to answering questions and providing counsel as Congress makes decisions regarding the Bill. Purdue provided a detailed list of what he wants to see happen with the Farm Bill. They fell into these larger categories (see full listing here):

Farm production and conservation
Trade and foreign agricultural affairs
Food, nutrition and consumer services
Marketing and regulatory programs
Food safety and inspection services
Research, education and economics
Rural development
Natural resources and environment
Management

“Agriculture Secretary Sonny Perdue today released his roadmap for the 2018 Farm Bill, and it is a good one for farmers,” says Zippy Duvall, president of the American Farm Bureau Federation (AFBF). “We are pleased the secretary and his team have highlighted not just the importance of risk management on the farm, but also rural development, research and development, trade, conservation and nutrition.”
While AFBF says it’s off to a good start, Jim Wiesmeyer, Pro Farmer Washington analyst, reminds farmers it’s not going to happen right away and there is still much to be negotiated.
“It could be April—no later than May to get it through the House and Senate is often three to four months later,” Wiesmeyer says. “The biggest issues many farmers [are concerned about] is getting cotton back into the Title 1 safety net, making the dairy program more effective since the 2014 Farm bill wasn’t and corn, soybeans and wheat programs need fixes in the ARC program, but those could be expensive.”
He says PLC programs need target prices to be higher, but again it comes back to funding. There is backing for expansion in the Conservation Reserve Program to raise it from 24 million acres to 32 million acres. To achieve this, new acres will likely receive lower rental payments—and if that happens Wiesmeyer thinks the CRP changes will occur.
“We’re going to see if politics come into play with the Farm Bill, I hope not,” Wiesmeyer says. “Some people think we’re not going to see it until after the November election.”