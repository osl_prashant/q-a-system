Experts optimistic for Texas cotton crop, gins busy
Experts optimistic for Texas cotton crop, gins busy

By JEFF FARRISAmarillo Globe-News
The Associated Press

AMARILLO, Texas




AMARILLO, Texas (AP) — When the Carson County Gin is cranked up and the cotton is flowing — being transformed from a harvest crop full of sticks, seeds and field trash into the white fibers ready to be sold at market — it's hard to carry on a normal conversation.
The Amarillo Globe-News reports lately, workers do more shouting than talking on the floor of the gin located along U.S. Highway 60 between Panhandle and White Deer.
"This is the largest crop we've ever ginned," said Keith Mixon, the general manager of the Carson County Gin. "Corn, milo and wheat are not bumper crops right now. Cotton is the only thing really working."
Last season, Mixon said his operation, which runs around the clock during this time of year, ginned 111,215 bales of cotton. He said he's expecting to gin more than 140,000 bales this season. A cotton season is from October to early April.
Carson County Gin, one of four of the largest gins in the Texas Panhandle, is not alone.
The leader of the group is Adobe Walls Gin, which is located south of Spearman. It ginned about 142,000 bales of cotton last season, and it will double that figure this time, a gin official said.
The Moore County Gin, located north of Dumas, ginned more than 103,000 bales during the 2016-17 cotton season. This season, it has ginned more than 188,000 bales.
The Top of Texas Gin, located near Hereford, ginned about 108,000 bales last season. Assistant general manager Steven Birkenfeld said he expects to gin about 130,000 bales before the season ends.
He said a lot of the older cotton producers used to tell him that the rule of thumb was farmers couldn't plant cotton north of Plainview.
"Then it became you couldn't plant north of Tulia," Birkenfeld said. "Then it became you couldn't plant north of I-40. Now you can plant all the way into Kansas.
"A lot of it boils down to acreage. We've had a drastic explosion of acres in this area."
He said other factors for a bountiful crop included good rainfall in the fall of 2016 and spring of 2017 and genetics.
"We have varieties with a quick enough turnaround that we are growing them up here," Birkenfeld said. "That's why the yields are so good with dryland farmers."
And recent news from Washington has only helped the area's cotton farmers.
It was announced this month that the U.S. Department of Agriculture has revamped its ginning assistance program. It's not a tremendous windfall for cotton farmers, but, Mixon said, "we will take any help farmers can get, especially with this drought."
According to the National Weather Service in Amarillo, 157 of the past 158 days have been dry and the only measurable precipitation was 0.01 of an inch on Feb. 17.
"We are in dire need of moisture for next year's crops," Mixon said. "Our subsoil has no moisture. The drought really hasn't affected our harvest this year, but it's time to get some rain."
DeDe Jones, the Extension Risk Management Economist for the Texas A&M Agrilife Extension office in Amarillo, said the new ginning assistance program is "a safety net to help offset losses in low markets. We have had for the past three to four years very low prices."
The plan calls for cotton farmers to get a one-time payment of $19.65 per acre with a cap of $40,000 per farmer.
"Basically, it is reimbursing farmers for their ginning cost," Jones said.
The Cotton Ginning Cost Share program, as the measure is called, comes on the heels of better news for cotton farmers. For the first time in four years, cotton farmers have been included in the new farm bill that provides crop insurance and Title I program subsidies to compensate for market dips.
Jones said, although cotton is back in the program, producers will not be eligible for a Title I payment for at least a year.
The sign-up period for the ginning assistance program runs through May 11.
"It's going to make bankers feel confident about loaning money to farmers to grow cotton," Mixon said.
___
Information from: Amarillo Globe-News, http://www.amarillo.com


This is an AP Member Exchange shared by the Amarillo Globe-News