WENATCHEE, Wash. — The right answer on how to approach ripening depends on whom one asks.That observation came from Randy Abhold, executive vice president of sales and business development for Selah-based Rainier Fruit.
“Some (marketers) focus on educating consumers on how to handle the product once you get it home to make sure that they’re taking it from the store in good condition, and others are focused on trying to grow consumption by having a product that’s ready to eat right now,” Abhold said.
Kevin Moffitt, CEO of Milwaukie, Ore.-based Pear Bureau Northwest, has been pushing for years for retailers to carry conditioned fruit. He believes offering ripe fruit is probably the best way to increase consumption.
Not all retailers want to go that route, however, and concern about shrink is one reason.
“The difficulty with pears is who does the pre-ripening, and when we do the pre-ripening. Then it gets to the stores and there’s a little bit shorter shelf life, and then they have shrink, and then they don’t like that,” said David Garcia, president of Hood River, Ore.-based Diamond Fruit Growers.
“We’re trying to work with our stores a little bit more (on), ‘OK, what stage do we pre-ripen, how far do we go in the pre-ripening?’” Garcia said. “That’s continuing to be researched.”
Since shrink is such a concern, grower-shippers are mostly focused on delivering pears in whatever condition is desired by each customer.
“We really tailor it with the individual retailer,” said Ed Weathers, vice president and sales manager for Hood River, Ore.-based Duckwall Fruit.
“We condition for some retailers. We also have some partners who condition on their own.”
Depending on how much attention can be devoted to the category, sometimes unripe fruit is a better fit.
“(It) depends on how well the retailer can execute the program,” said Steve Lutz, senior strategist for Wenatchee-based CMI Orchards. “Ripe fruit is great for consumers looking for fruit for immediate consumption. On the other hand, retailers have to be aware that ripe fruit can turn much quicker on the supermarket shelf, leading to increased shrink.”
Wenatchee-based Stemilt Growers plans to take a strong stance on the merits of ripening this season, marketing director Roger Pepperl said.
Stemilt is asking its customers for a chance to prove that conditioning can make a huge difference in the pear category, and it wants retailers to go all in.
“We need all their pear business,” Pepperl said. “You can’t go into a store and have them carry two different types of T-bone steaks — one that’s tough and one that’s tender and beautiful — because the chances are if you buy the bad one you’ll never come back again.”
Stemilt has new ripening rooms for the 2017-18 crop, and it plans to condition fruit in retail-ready Euro boxes.
Timing is a key variable for the program.
“You need projections,” Pepperl said. “We need to have a retail partnership where we know what their retail calendar is for pears, when they’re going to promote them, when they’re just regular business, and how many they use per week.”
Stemilt realizes that it will not be the sole pear supplier for all its customers — particularly national chains — but it is optimistic about its pitch.
“We’ve talked to some already, and I think we’re getting a fairly good response, and the reason why is because people are failing at pears,” Pepperl said.
“If you do the same thing you did last year, it’s going to go down again.
“We feel that we can grow that category substantially, even within a year, if we’re given that opportunity,” Pepperl said.
“It’s all about consistency. Customers, they don’t want a miracle. They want something really good, every week ... I think we’re going to have a lot of luck with this and we’re already out working on getting it done.”