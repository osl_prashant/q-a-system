Editor's Note: The article that follows is part of the 13th edition of The Packer 25 Profiles in Leadership. These reports offer some insight into what drives successful people in produce. Please congratulate these industry members when you see them and tell them to keep up the good work.

To say Rick Alcocer has a lot on his plate is a delicious understatement.
As senior vice president of sales for Oviedo, Fla.-based Duda Farm Fresh Foods, Alcocer manages sales offices in the East and on the West Coast where Duda is a major vegetable and citrus producer.
He also works on plans and contracts with large retailers and foodservice companies, and he’s deeply involved in the annual planning process when it comes to which crops and how much of each the company plants.
On top of his day job he’s the 2017 chairman of the Ottawa-based Canadian Produce Marketing Association.
When it comes to being a good leader, the personable VP considers honesty, integrity and consistency as essential to forming relationships and building a workforce that can work together and work with customers.
Like a chef with a box full of knives, he chooses his leadership style according to what is needed and what is the best tool for the job.
“Sometimes you need to be energetic and inspiring to motivate team members,” he said. “At other times quietly leading by example works well, allowing employees to see that the boss is working hard and travelling the continent bringing in new business.”
Hiring strong sales managers and regional directors that one trusts and respects invites a more laissez-faire approach, as they can work on their own and don’t require a lot of oversight.
One challenge for today’s leaders is finding ways to communicate with 26- to 30-year-olds more comfortable with texting cryptic messages than group discussions, he said.
“If you don’t have good communication skills it’s very difficult to be a sales person because most of our relationship-building is verbal,” he said. “It’s amazing how written words and tones can be misunderstood.”
As for goals, Alcocer would love to find the next kale or the next new value-added product and expand Duda’s product line. He’d also like to see Duda supply proprietary seed to more growers in Europe and Asia so they can sell the produce to their major retailers and perhaps turn to the U.S. when they need product.
Before retirement, Alcocer hopes to leave the company in great shape by training the next generation and making sure they share his values.
CPMA president Ron Lemaire has no doubt Alcocer will achieve his goals.
“Rick is a strong listener, which allows him to truly understand the leadership path before he delivers results with a passionate and collaborative vision,” Lemaire said. “In a world of global regulatory change, disruptive technology advancements and an ever-evolving marketplace, Rick has led not only his sales team to success but an entire industry through his work at CPMA.”