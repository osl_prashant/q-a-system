AP-TX--Texas News Digest 5 pm, TX
AP-TX--Texas News Digest 5 pm, TX

The Associated Press



Good afternoon! Here's a look at AP's general news coverage in Texas at this hour. Questions about coverage plans are welcome and should be directed to the Dallas AP at 972-991-2100, or, in Texas, 800-442-7189. Email: aptexas@ap.org. Terry Wallace is at the desk.
A reminder: This information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
— UPDATES Texas Primary-Guns.
— UPDATES House Explosion-Evacuations.
— ADDS Houston Firefighters-Sex Discrimination.
— ADDS Students Sickened-Fumes.
— ADDS Plane Crash-Field.
— ADDS Fatal Hit-and-Run.
.....................
TOP STORIES:
TEXAS PRIMARY-GUNS
AUSTIN, Texas — The first primary of 2018 could offer a test of the re-energized gun-control movement, but the issue has not dominated races in gun-loving Texas, even though the state recently endured a massacre that was deadlier than the Florida high school shooting. The results of the Texas primary on Tuesday might be expected to give some early indication of whether voter attitudes toward weapons have changed in the two weeks since a gunman killed 17 people at Marjory Stoneman Douglas High School in Parkland, Florida. But that now seems less likely because the attack has not emerged as a decisive factor in the final stages of the Texas campaign. The state has some of the most relaxed gun laws in the U.S. and hosts the National Rifle Association's national convention in May. "It's hard. Texas is hard," said Elva Mendoza, a leading Texas organizer with Moms Demand Action for Gun Sense in America. By Paul J. Weber. SENT: 710 words.
AROUND THE STATE & NATION:
HOUSE EXPLOSION-EVACUATIONS
DALLAS — Natural gas service was shut down Thursday to thousands of Dallas homes following a series of leaks that has brought repeated evacuations in the wake of a house explosion that killed a 12-year-old girl. Gas service will be discontinued for up to three weeks to about 2,800 homes northwest of downtown as gas lines are replaced and other work is done by more than 120 Atmos Energy crews, authorities said at a news conference. Dallas-based Atmos, the country's largest natural gas distributor, will compensate residents who choose to stay in hotels or incur other expenses during the disruption. By David Warren. SENT: 530 words, photos.
EXXON MOBIL-ROSNEFT
MOSCOW — U.S. oil company Exxon Mobil says it will withdraw from its joint venture with Russia's state-controlled Rosneft due to U.S. and European sanctions against the country. Texas-based Exxon Mobil had signed a deal with Rosneft, Russia's biggest oil producer, in 2011 that aimed to drill in difficult terrain, like Russia's Arctic waters. It combined Exxon's technology with Rosneft's access. The deal came under strain, however, after the U.S. sanctioned Russia in 2014 over the invasion of Ukraine and the Crimean Peninsula. SENT: 500 words, photos. Moved on general and financial news services.
OBIT-WILLIAM HT BUSH
MIAMI — William H.T. "Bucky" Bush, a wealthy investor and the brother and uncle of presidents, has died. He was 79. Former Florida Gov. Jeb Bush confirmed Thursday in an email to The Associated Press that his uncle died Wednesday. He did not describe the cause. The brother of President George H.W. Bush and uncle of President George W. Bush — both live in Texas — was co-founder and chairman of Bush O'Donnell Investment Advisers in St. Louis, Missouri, and served on the board of directors of WellPoint Inc. Known as "Bucky" Bush, he also active in Republican politics. By Freida Frisaro and Gary Fineout. SENT: 420 words, photos.
OBIT-HARVEY SCHMIDT
NEW YORK — Harvey Schmidt, the Texas-born composer of "The Fantasticks," which made its debut when Dwight D. Eisenhower was still president and became the longest running musical in history, has died. He was 88. His death on Wednesday was confirmed by Dan Demello, a publicist for the off-Broadway show. Schmidt, who came to New York from Dallas, wrote the melody of "Try to Remember" in 5 minutes. Schmidt told Michael Riedel in his book "Razzle Dazzle," that he had rented a rehearsal room in the Steinway Building because he couldn't afford to own a piano. By Entertainment Writer Mark Kennedy. SENT: 600 words, photos.
EXCHANGE-FERAL CHICKENS
BASTROP, Texas — The Capital of Texas Zoo counts hippos, peacocks, panthers, tigers and camels as residents at its facility in western Bastrop County. And soon it might be welcoming into its fold many of the free-ranging Farm Street chickens, whose constant clucking, cawing and feculence have driven many Bastrop residents to their wits' end. The Austin American-Statesman reports the city's animal services division is putting final touches on a contract that would hire outside help to trap and rehome many of the fowl that have strutted too far from the Farm Street Historic Chicken Sanctuary, a 1,500-foot stretch of road where the chickens have enjoyed legal protection for nearly a decade. By Brandon Mulder, Austin American-Statesman. SENT: 950 words, with photos.
IN BRIEF:
— HOUSTON FIREFIGHTERS-SEX DISCRIMINATION — Houston officials are disputing allegations in a Justice Department lawsuit accusing the city's fire department of discriminating and harassing two female firefighters because of their gender. SENT: 130 words.
— STUDENTS SICKENED-FUMES — Houston Fire Department officials say more than 40 students have been taken to hospitals after being sickened by fumes possibly from tar work on nearby roofs. SENT: 120 words, photos.
— PLANE CRASH-FIELD — Officials say a plane that apparently lost power in both engines has crashed in a muddy North Texas field in an accident that left the pilot slightly hurt. SENT: 120 words, photos.
— COACH REBUKED-COLORADO MARIJUANA — Texas Wesleyan University has fired its baseball coach after he told a high school player from Colorado that the team doesn't recruit from the state because players there fail drug tests. Moved on general and sports news services.
— FATAL HIT-AND-RUN — A Texas man has been arrested in a hit-and-run accident that killed a man in Kansas in November. SENT: 110 words.
— HARVEY-HOUSTON FIRE DEPARTMENT — The Houston Fire Department will receive $2 million to buy about 20 more boats and high-water rescue vehicles after Hurricane Harvey swamped parts of the nation's fourth largest city. SENT: 130 words, photos.
— HOUSTON-MOTEL SHOOTING — Houston police say one person has been killed and two others were wounded when an assailant fired shots through a motel room door and then fled. SENT: 110 words.
— FATAL WRECK — A 20-year-old Galveston County woman has been charged with two counts of intoxicated manslaughter after authorities say her speeding SUV slammed into a car on an Interstate 45 service road, killing a Houston woman and her 3-month-old son. SENT: 130 words.
— OKLAHOMA LAWMAKERS-THREATS — The Wind Coalition denies that it hired a Texas political consultant to follow an Oklahoma legislator.
— GAS PRICES — Retail gasoline prices in Texas and across the country rose an average 2 cents per gallon this week. SENT: 110 words.
SPORTS REFER:
HKN--LIGHTNING-STARS
DALLAS — The Dallas Stars on Thursday night host the Tampa Bay Lightning. UPCOMING: 600 words, with photos. Game starts at 7:30 p.m. CST.
____
If you have stories of regional or statewide interest, please email them to aptexas@ap.org
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867.
For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
The AP.