Minnesota farmers endured 5th straight year of thin profits
Minnesota farmers endured 5th straight year of thin profits

By STEVE KARNOWSKIAssociated Press
The Associated Press

MINNEAPOLIS




MINNEAPOLIS (AP) — A new report says bumper crops and occasional upticks in prices weren't enough to save most Minnesota farmers from a fifth straight year of thin profits in 2017.
The annual analysis from University of Minnesota Extension and the Minnesota State colleges and universities system finds that nearly one-third of Minnesota farmers saw their net worth decrease in 2017. Median farm income was just over $28,500, down from about $36,000 in 2016.
Extension economist Dale Nordquist says dairy farmers faced some of the toughest challenges. Milk prices tanked in the second half, and a lot of producers are now losing $2 on every hundred pounds. He says pork producers were the one group that enjoyed higher profits thanks to somewhat higher prices.
High yields have helped crop farmers withstand low prices.