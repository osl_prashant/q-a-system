University of Nebraska may close Haskell Ag Lab to save $1M
University of Nebraska may close Haskell Ag Lab to save $1M

The Associated Press

CONCORD, Neb.




CONCORD, Neb. (AP) — The University of Nebraska may close an agriculture laboratory to save $1 million a year.
Closing the 320-acre Haskell Agricultural Laboratory outside of Concord is among $9 million in proposed budget cuts put forward by university President Hank Bounds at Gov. Pete Ricketts' request, the Sioux City Journal reported . Ricketts asked the university system to cut its current budget by $11 million and plan to cut next year's budget by another $23 million.
Researchers at the lab have studied crop and livestock production, crop nutrition, irrigation and water management, soil science and weed, disease and pest management.
The lab has conducted important research that's relevant to the area, said Kent Bearnes, an independent agronomist and seed sales representative who is president of the Northeast Nebraska Experimental Farm Association, which provides support to the lab.
"We have a resource here, and we need to find out a better way to use this resource," he said. "Everybody I've talked to doesn't want to lose it. I think it's a resource that once we lose it, it isn't coming back. Once it's gone, it's gone."
While Haskell has had a positive effect in northeast Nebraska, the lab is at the top of the proposed cut list because the university doesn't own the land and $1.5 million is needed in maintenance and improvements, said Mike Boehm, vice chancellor of the Institute of Agriculture and Natural Resources, which oversees Haskell.
The lab also lacks a clear strategy and the university hasn't replaced seven professors who retired from Haskell over the last decade, Boehm said.
"If it was absolutely critical, we would have been refilling positions, and that isn't the case," Boehm said.
The 10 faculty and staff members at the lab would be terminated or asked to relocate if the lab is closed, he said.
"I think it'll have a detrimental effect on agriculture as a whole," Bearnes said. "Education is a pathway to opportunity, and research is a part of that. If we lose our research opportunity, we lose that pathway."
___
Information from: Sioux City Journal, http://www.siouxcityjournal.com