Consumers today are very different than they used to be, yet have a strong interest in where their food comes from, including how food animals are raised and handled. To help build consumer trust in dairy products, the Dairy Cattle Welfare Council (DCWC) is pleased to offer the webinar series for 2018. The one-hour live educational sessions are available to attendees from across the United States as well as around the world (recordings are available to DCWC members). Webinars are free of charge, but you must register.
The upcoming webinar is titled: “Moving forward with group housing of dairy calves: What you need to know and important considerations”
By: Dr. Joao Costa; Associate Professor, University of Kentucky
Date: March 14, 2018
Time: 5:00 p.m. Eastern Daylight Time
REGISTER HERE

In this session: Milk-fed calves are typically housed individually and fed restricted milk allowances, but group housing and high milk allowances are becoming very common in North America. This webinar will review the key management practices that are fundamental for a successful group-housing of dairy calves. Dr. Costa will describe group housing practices, benefits, and problems, and suggest practical solutions for common problems faced on farms.
Webinars are geared toward dairy farmers, veterinarians, consultants, industry, Extension/academia, and government representatives interested in many aspects of dairy cattle welfare. Professional, undergraduate, and graduate students are strongly encouraged to participate.
3rd Dairy Cattle Welfare Symposium
Early Bird Registration is open for the 3rd Dairy Cattle Welfare Symposium to be held May 31st to June 1st in Scottsdale, Arizona. New this year is the inclusion of a one-half day Spanish-speaking session targeted towards farm managers, herdsmen, and key employees. You may register and find more details at https://www.dcwcouncil.org/symposium