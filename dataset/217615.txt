For the new year, the question is this: What is one thing you would do to make the industry better?
I put the question recently to the LinkedIn Fresh Produce Industry Discussion Group in this way.
If you were ruler of the fresh produce world and would have unlimited power, what is the one thing you would do to make the industry better for everyone in 2018?
The topic struck a nerve and has generated 27 responses so far. Here are a few, in their own words:
> Regulate dimensions and materials used for packaging to improve the quality of produce during transportation. 
> Standardize date coding between all retailers. Use same date format, same fonts and stop blocking allowed date codes based on opening hours — if a product has a 4-day shelf life don’t be afraid to mark it with 25 DEC just because the shop is shut that day. This practice causes havoc throughout the industry.
> Implement mandatory assessments on all products to be consumed as fresh produce, use these funds to promote consumption of fresh produce. This would increase demand, which may lower inventories on slower-moving items.
> MANDATORY FARM VISITS for all produce buyers, and end-users too if possible. Too few people understand what it takes to get food from field to fork. The farmers make the least of all involved in the entire chain, but stand the most to be lost. Field visits would help ALL to realize the cost, effort, and, therefore, the resulting value of a crop.
> I would ban use of genetically modified seeds, chemicals, fertilizers and pesticides on farmland, fresh produce coating and use recyclable material from farm to consumer’s fridge to protect public health and environment.
> Safe and fair working conditions for the men and women who work in the fields, greenhouses and packing houses in this country and others.
> Create a law that sets the standard that packaging has to be recyclable and also set how much packaging can be allowed per product. Plastic is ruining the world and there is loads of it in fresh produce.
> I would make the major retail chains sell produce in accordance within the naturally occurring ebbs and flows of production and get rid of their failed can’t work for under 40% markup, which is usually much more.
> Strict guidelines in order to obtain a PACA license: $500k-$1mill surety bond, credit/background checks, etc.
There are several can’t-miss ideas in this group, though a few also that would be hotly disputed. If I were to wear the king’s crown, I like the idea of promoting fresh produce consumption using assessments on all fresh produce. 
That probably wouldn’t pass muster with most. What about you? If you were “ruler” of the fresh produce kingdom, what would be your first action?
Tom Karst is The Packer’s national editor. E-mail him at tkarst@farmjournal.com.