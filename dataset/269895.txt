It only takes a Tennessee landscape one generation to transform from a grassland to a thick forest, and that’s the hope of a new project by University of Tennessee’s Institute of Agriculture and the Tennessee Valley Authority (TVA).

The groups are working diligently to restore oak trees at the Norris Dam, 25 miles from Knoxville.

Charles Denney from the UT Institute of Agriculture has the story on AgDay above.