BC-USDA-SD Direct Fdr Catl
BC-USDA-SD Direct Fdr Catl

The Associated Press



SF—LS160Sioux Falls, SD    Fri Mar 16, 2018    USDA-SD Dept of Ag Market NewsSouth and North Dakota Direct Feeder CattleFeeder Cattle Weighted Average Report for 03/16/2018Receipts: 0        Last Week 1,000           Last Year 1,055Compared to last week:  Feeder steers and heifers not tested this week.Supply includes 0 percent over 600 lbs and 0 percent heifers.  All sales FOBNorth and South Dakota with a 2-3 percent shrink or equivalent and a 4-8cent slide on yearlings and an 8-12 cent slide on calves from base weights.Feeder Steers: No Test.Feeder Heifers: No Test.Source:  USDA Market News Oklahoma City, OKJoe Massey 405-232-542524 Hour Market Report 1-405-636-2691www.ams.usda.gov/mnreports/sf—ls160.txt