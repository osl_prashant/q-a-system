World food prices fell fractionally in November from the month before, as a sharp rise in sugar and vegetable oil values was largely offset by a fall in dairy, the United Nations food agency said on Thursday.
The Food and Agriculture Organization’s (FAO) food price index, which measures monthly changes for a basket of cereals, oilseeds, dairy products, meat and sugar, averaged 175.8 points in November, down 0.5 percent from October.
Food prices on international markets were 2.3 percent higher than last November.
Agricultural commodities have emerged from a highly volatile period and FAO has said it expects them to remain stable over the next decade.
FAO raised its forecast for global cereals output in 2017 to 2.627 billion tons, 16.8 million tonnes higher than last year’s level and pushing worldwide supplies to an all-time high.
The agency also raised its forecast for global wheat production to 754.8 million tonnes, 1 percent lower than 2016.