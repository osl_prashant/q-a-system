In a meeting with Pennsylvania coal miners at the Harvey Mine in Sycamore, Pa., EPA administrator Scott Pruitt announced the agency’s new Back-to-Basics agenda. This agenda will refocus EPA on its intended mission, siphon off some of its current power to the states and create an environment in which jobs can grow, according to Pruitt.
“What better way to launch EPA’s agenda than visiting the hard-working coal miners who help power America?” he says. “The coal industry was nearly devastated by years of regulatory overreach, but with new direction from President Trump, we are helping to turn things around for these miners and for many other hardworking Americans.”
Pruitt says EPA will work with state, local and tribal partners to create “sensible regulations that enhance economic growth.”
Pruitt also spoke with coal miners about the Trump administration’s Energy Independence Executive Order, which directs EPA and others to review the Clean Power Plan and work towards energy independence by revising regulatory barriers that may impede that goal.
President Trump also has set forth a budgetary blueprint that would cut EPA’s total budget by about one-fourth. The New York Times recently looked at which proposed cuts will be “likely to face resistance” by the time the budget reaches Congress, including:
Decreasing grants by almost one-third that help states monitor public water systems.
Reduces spending on civil and criminal enforcement of environmental penalties by 60%.
Eliminating regional cleanup initiatives, including with the Chesapeake Bay and the Great Lakes.
Eliminating the Climate Protection Program.
Eliminating research and screening effort for endocrine disruptors (which have been linked to cancer and birth defects).
Meantime, some energy experts have called into question the Trump administration’s hard push for the coal industry. That’s because coal use continues to decline globally, and while this “megatrend” hurts the coal industry, it benefits almost everyone else, according to David Tuttle, research fellow in the University of Texas Energy Institute, and Robert Hebner, the director of the Center for Electromechanics at the University of Texas. The two recently tackled the “war on coal” in a recent edition of UT News.
The two draw parallels between the coal industry today and the semiconductor industry in the 1980s and 1990s. That industry was on the verge of collapse when key players collaborated to prepare for global changes and make technological breakthroughs to save itself, rather than try to recapture the past.
“The coal industry needs to take the same approach, but it does not appear to have the industrywide leadership needed to try cooperative innovation to make coal a less costly and more desirable form of energy,” they write. “A way forward is needed for the industry to develop a credible cooperative plan for the beneficial use of coal itself or for the resources of the coal industry. That would provide governments an effective way to help.”
It is important to remember the real pain in the coal industry as people see livelihoods disappear, Tuttle and Hebner add.
“Overall, in the U.S., the number of wind and solar energy related jobs may well be greater than employment in coal producing regions, but the reality is that well-paying jobs are not being created in coal country to replace the ones lost,” they write. “It is unlikely that many traditional coal jobs will return, but we need to use this time of decline in the use of coal to build jobs that improve the quality of life for those in the coal industry.”
Aside from reviewing the Clean Power Plan, the EPA’s Back-to-Basics agenda has eight additional directives.
1. Review the “Waters of the U.S.” (WOTUS) rule.
2. Clear the backlog of chemicals awaiting EPA approval.
3. Help states with air quality, toxic waste and water infrastructure issues.
4. Rescind fuel economy standards for model year 2022-2025 vehicles, and review all vehicle standards jointly with the U.S. Department of Transportation.
5. Review the Oil and Methane New Source Performance Standards.
6. Allocate funds for vital environmental projects, such as $100 million to upgrade drinking water infrastructure in Flint, Mich.
7. Stop the methane Information Collection Request (ICR), which the agency says costs U.S. businesses more than $42 million per year.
8. Launch an EPA Regulatory Reform Task force to “undergo extensive reviews of the misaligned regulatory actions.”
For more information, visit www.epa.gov.