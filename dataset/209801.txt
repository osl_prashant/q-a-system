My fellow Americans, ask not what your country can do for you, ask what your country cannot do to you.
With apologies to John F. Kennedy, President Trump promised less regulation and it appears he is delivering so far.
The Competitive Enterprise Institute has put some stats together that said the Trump Administration’s has cut back  Federal Register rulemaking by quite a bit this year.
In a blog post titled “Red Tape Rollback: Trump Least-Regulatory President Since Reagan,” author Clyde Wayne Crews outlines three main ways that Trump is cutting the red tape.
First, Trump issued an executive order in January, requiring agencies to cut two rules for every new regulation adopted. He also said that net new regulatory costs were to be zero.
Trump also issued a Reorganization Executive Order that requires the Office of Management and  Budget to submit a plan aimed at reducing the size of federal agencies.
According to Crews, a memorandum from the new Office of Information and Regulatory Affairs asks agencies to propose a net reduction in total incremental regulatory costs for fiscal year 2018.
The number of trees felled for government regs is down.
By Sept. 30, the Federal Register tallied 45,678 pages, down 32% from a year ago.
The number of significant rules put in place by Trump, from Jan. 20 to Sept. 30, total 116, down from 274 for the same period in 2016. The total number of rules issued were 2,183, down from 2,686 last year.
Finally, the number of economically significant proposed rules also are way down, from 290 from January through September last year to just 65 so far this year. The American Action Forum also has its own scorecard for regulatory savings.
If nothing else, Trump has pumped the brakes on the expansion of regulation in the federal government. And that’s something.