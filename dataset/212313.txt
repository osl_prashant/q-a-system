E-commerce is booming for Wal-Mart Stores Inc., and here in Texas, we love our grocery pickup. 
The Dallas Morning News just had a fantastic article about the popularity of the service in Dallas-Fort Worth, which Wal-Mart says is its biggest, most well-established market. 
 
Since 2015, Wal-Mart has expanded its grocery pickup in the area to about 50 stores. Nationwide, it offers pickup at 500 stores and plans to add 600 more this year.
 
The article says North Texas stores are fulfilling up to 900 orders a week for grocery pickup, and the average for the region’s stores is 500-600 per week, according to Jason Fogerty, Wal-Mart’s Dallas area market coach for e-commerce. 
 
I live about three hours south of Dallas-Forth Worth, and one of the two suburban Austin stores near my house started offering grocery pickup just over a year ago.
 
I tried it. I loved it.
 
I didn’t go back. 
 
Why? I don’t know, man ...
 
I’m not a planner, I guess? It wasn’t part of my routine? 
 
Grocery delivery and pick-up are gaining a lot of ground with time-strapped consumers, though. 
 
Fresh Direct’s Devika Kumar said during a panel at the Canadian Produce Marketing Association’s show last month that you’ve got to get consumers to try it twice, or even three times, to get them hooked.
 
It was time for me to try again, so I loaded up some produce in my shopping cart and set my pick-up for the next afternoon.
 
The pickup
First of all, I screwed up. I went to the wrong store. I hadn’t noticed that Wal-Mart automatically selected the closest store to my house as my default location. Oops. 
 
When I showed up to the wrong store for pickup, they were very friendly and said it’s happened periodically. The store I’d imagined myself picking up at was newer and fancier than the store they chose for me.
 
Once I got to the right store, I was really surprised at the staging area. Parking spots were covered (a primo choice here in blistering hot Texas), and there were a lot of them. 
 
The clerk who helped me was very friendly, and apologized that they were out of their “new customer” gift bags (what?!) and that I was marked in their system to receive one next time.
 
I asked her a bit about how busy they were while she loaded my groceries in my trunk. Apparently, they’re seeing lots of repeat customers, and are busier than she expected them to be.
 
I even had a cute welcome note from my picker.
 
This is what Wal-Mart was selling online — Champagne mangoes, specifically.
The produce
I had only one substitution, and, funny enough, it was the same substitution I had a year ago — instead of two pounds of loose Jazz apples, they gave me a 3-pound bag for the same price.
 
And while my order was beyond expectations last time, this time, I’d give them a B-. Call it a sophomore slump.
 
Issues that need to be addressed:
They’re selling “Champagne mangos” on the website, and delivering honey mangoes in the order. Nope. Not OK. Champagne mangoes are a brand and this needs to be fixed. 
I ordered a “1-pound bag” of cherries for $3.28 on the site, and received almost 2½ pounds of cherries in a random weight pouch. From a customer standpoint, I won. I got $4.50 worth of cherries I didn’t pay for. From a retailer standpoint — ouch.
My bananas were two stages too green. I didn’t see anywhere I could specify what ripeness I wanted when I ordered, but if I wanted to eat them the day I picked up, it’s a problem. I also asked for 3 pounds and got 3.5. Bonus for me, not them.
My pineapple was several days away from edible.
Avocados were green and hard — at least three days from ready. Last time they were ripe and perfect. 
Peaches were a mixed bag — three were hard and one was mushy.
This is what I got — generic honey mangoes, not Champagne-brand mangoes.
The bottom line
Would I go back to Wal-Mart’s site and officially complain about the issues I found? Well, I don’t know. One peach out of four? Is that really worth my time?
 
Green bananas, unripe pineapple? Is that really worth the hassle of complaining? How much do I have invested in Wal-Mart’s grocery pickup? I didn’t pay for the service. I didn’t tip the clerk. (Is that even allowed?)
 
If I was a dedicated customer, I’d give them feedback and another shot, and I bet if I did, I’d get some kind of compensation for my time and hassle.
 
As it is, I’ll probably just let it go and order again some other time. 
 
Would my attitude be different if I got that same A+ experience I had last time? Probably. A retailer wants their online grocery customers to be happy.
 
Online shoppers typically have much bigger baskets overall, there’s a lot of loyalty in this sector and there’s a lot of money to be made there, too. 
 
Pamela Riemenschneider is editor of Produce Retailer magazine. E-mail her at pamelar@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.