Billboard connects Wisconsin man to Oklahoma kidney donor
Billboard connects Wisconsin man to Oklahoma kidney donor

By NATHAN PHELPSGreen Bay Press Gazette
The Associated Press

MILWAUKEE




MILWAUKEE (AP) — A billboard along a northern Wisconsin highway was a dramatic gesture to try to find a long-sought kidney donor for Terrence Rice.
The 36-year-old from Marinette had faced failing kidney function for years. Three days a week, for more than three years, he spent hours at a clinic for kidney dialysis.
"I was always tired and worn out," Rice told the USA Today Network-Wisconsin . "I couldn't work full-time anymore and sporting events for my kids, I had a hard time taking them ... because I'd be too tired."
Divorced with two children, ages 11 and 9, Rice's condition forced him to leave his job as a marine electrician at shipbuilder Fincantieri Marinette Marine for a job in retail. He was continually fatigued and three days a week, every week, he spent several hours hooked up to a dialysis machine.
His O blood type limited the pool of potential donors, stretching his time on a kidney wait list to four years.
Last summer his mother, Linda Zajac, paid to have the billboard put up on state Route 64. The plea was in the style of an Old West wanted poster. "Wanted," it read, "a new kidney for my only son, 35-year-old father of two."
It included a phone number.
A photo of the billboard was shared on Facebook by O'Ryan's Pub and Grub in late August and spread. Thanks to a friend in Marinette who shared the image, Rice's plight showed up on Noah Torres' screen, some 800 miles away in Prague, Oklahoma.
The 27-year-old knew he wanted to help.
"I just called the number to see what would happen," Torres said.
That call began the process of testing to see if he was a match: He was.
Torres is a frequent blood donor and is registered to donate bone marrow if needed. He said he had given thought to organ donation prior to seeing Rice's plea.
"I was excited for the opportunity," said Torres, who is also a father of two. "I got a phone calling saying I was a match and just took it from there."
Rice had more than 20 people tested to be a donor who didn't match.
"He saved my life and extended my life by 20 years probably," Rice said. "I can watch my kids grow up and have a normal life again."
The transplant was scheduled for Feb. 8 at Aurora St. Luke's Medical Center in Milwaukee, one of four transplant hospitals in the state. Both men were excited about the operation, and though their memories are hazy about the specifics of the day, they both remember yelling encouragement to each other as they were prepped for the operating room.
"This is the first time I've had a story like this unfold," said Dr. Ajay Sahajpal, a transplant surgeon who is the medical director of the transplant program at Aurora St. Luke's, of the long-distance, billboard-via-Facebook connection the two men forged.
Most living donations come from a family member or friend. Sahajpal said about 5 to 10 percent of living kidney donations come from donors who have no direct tie to the recipient, like Torres, and are doing it to help a stranger in need.
More than 400 people underwent kidney transplants in Wisconsin last year, according to the Organ Procurement and Transplantation Network. Nationwide, more than 116,500 people are in need of a transplant. On average, about 20 people die each day around the country while waiting.
"There's a fixed number of (deceased) donors," Sahajpal said. "It's just not growing, even as the number of people on the wait list continues to grow. So the wait times get longer and longer."
And living donors provide better kidneys that last longer and offer the recipient a better quality of life, Sahajpal said. That's why he and other doctors encourage those in need of a transplant to talk to church groups, friends, family, social media.
Some transplant patients put messages on their vehicles, or on billboards, calling attention to their need for donation and a phone number for potential donors can call.
A plea on family vehicles found Cathy Strom the kidney donor she sought.
"I found a donor, a stranger, all because of the ads on our cars," said Strom who lives near Green Bay. "I had people all over the United States who wanted to be a donor."
The catalyst to her transplant was a woman driving down the street who saw the car and took a photo.
Strom's donor, who took 10 months to find, lives two blocks from her home. Social media, TV coverage and friends help spread the word that helped make the March 2017 transplant at University of Wisconsin Health happen.
"I was so persistent to get it out there and find a kidney right away," she said. "Do whatever you can to get it out there with signs, T-shirts, 5K runs, just get it out there as much as you can. Get it on social media, newspaper, TV. Just get it out there."
Zajac, who paid for the billboard for three months, is beyond grateful to Torres for his help.
"You can't put a price on a kidney to save a life," she said. "I'm speechless as to how much I can thank him."
After hours of the surgery Feb. 8, both Rice and Torres emerged healthy and on the road to recovery, the operation a success. One week after the operation, the pair sat side by side in a hospital conference room discussing their unexpected connection and operation.
The men were at ease with one and another and said, thanks to common interests, hit it off well and had an immediate connection. They're now connected by a kidney, too.
Torres said he was excited by the opportunity to help, despite oversleeping by about an hour — his biggest fear — on the day of the operation.
"I came running into the hospital and past registration into the day surgery pre-op," he said.
After a few weeks recovering in Milwaukee, Rice expects to return to Marinette and his job at Sears. Torres returned to Oklahoma with his finance Melissa Gallagher and his job as a service technician at a Ford dealership. The couple is planning a May 2019 wedding.
"It was kind of a last-ditch effort because we couldn't find anyone to donate," Rice said about the billboard. "(Torres) was a miracle from heaven."
___
Information from: Press-Gazette Media, http://www.greenbaypressgazette.com


An AP Member Exchange shared by the Green Bay Press Gazette.