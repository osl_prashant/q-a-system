The Latest: House approves college savings plan change
The Latest: House approves college savings plan change

The Associated Press

LITTLE ROCK, Ark.




LITTLE ROCK, Ark. (AP) — The Latest on Arkansas' special legislative session (all times local):
10:36 a.m.
The Arkansas House has approved a measure allowing 529 college savings funds in the state to be withdrawn for tuition at K-12 schools, including private and religious institutions.
The House on Wednesday approved the measure by a 62-16 vote. It's intended to mirror a change in the federal tax overhaul that supporters say is needed to ensure Arkansans don't face state tax penalties if they withdraw the funds for K-12 tuition.
Opponents have cited concerns about the cost of the program, which finance officials say could cost up to $5.2 million a year.
A dispute over the proposal held up the state treasurer's budget last week before it was removed to be considered during the special session.
The bill now heads to the Senate, which is considering an identical measure.
___
10:21 a.m.
A committee of the Arkansas Senate has approved a bill designed to designate when and for how long third-parties can protest against farms that require liquid animal waste permits.
Farmers say state law should spell out that, if state regulators grant a waste permit, others should not be able to mount continuous challenges. The bill's supporters say the proposal would give banks a certain peace of mind if they know their clients aren't being sued often.
The Senate Public Health, Welfare and Labor Committee approved the bill on a voice vote Wednesday. An identical version of the bill passed a House panel.
Arkansas legislators are meeting in a special session that started Tuesday. Gov. Asa Hutchinson also wants them to look at reimbursement rates for prescription drugs, and other matters.