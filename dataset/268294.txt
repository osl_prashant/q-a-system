AP-MI--Michigan News Digest 6 pm, MI
AP-MI--Michigan News Digest 6 pm, MI

The Associated Press



Good evening! Here's a look at how AP's general news coverage is shaping up in Michigan at 6 p.m. Questions about today's coverage plans are welcome, and should be directed to the AP-Detroit bureau at 800-642-4125 or 313-259-0650 or apmichigan@ap.org. Ken Kusmer is on the desk, followed by Herbert McCann. AP-Michigan News Editor Roger Schneider can be reached at 313-259-0650 or rschneider@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date. All times are Eastern.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
UPCOMING TOMORROW:
MICHIGAN SCHOOLS-GUNS
DETROIT — Can Michigan public schools adopt gun restrictions that are tougher than in state law? The state Supreme Court hears arguments. Developing from afternoon arguments.
HILLARY CLINTON-MICHIGAN
GRAND RAPIDS, Mich. — Hillary Clinton is scheduled to appear in Grand Rapids, Michigan, for an event honoring former first lady Betty Ford, who would have turned 100 this year. Developing from event scheduled to begin at 1:30 p.m.
NEW SINCE LAST DIGEST: GOVERNOR'S RACE-THANEDAR ADS, CHEMICAL COMPANY-EVACUATIONS, HKN--RED WINGS-BLASHILL, BBA--TIGERS-BOSIO
TOP STORIES:
GOVERNOR'S RACE-THANEDAR ADS
LANSING, Mich. — Democrat Shri Thanedar said Tuesday he will spend $1 million to air 10 new TV ads in the governor's race over the next two months, with an emphasis on giving positions on issues such as roads, education and the economy. The new spots will air across the state starting Wednesday. The self-funded businessman and political novice had spent at least $1.2 million to run ads on network stations through late March — not including cable networks, according to the Michigan Campaign Finance Network. By David Eggert. SENT: 510 words.
GOVERNMENT AND POLITICS:
— XGR--TEMPORARY RESIDENT-DRIVER'S LICENSE: Bills that would require Michigan driver's licenses and state ID cards to show certain immigration statuses are headed to the House floor. SENT: 130 words.
AROUND THE STATE:
FERTILITY CLINIC FAILURES
The supplier of an Ohio fertility clinic's storage tank says its investigation shows its equipment didn't malfunction or cause the loss of more than 4,000 eggs and embryos. Custom Biogenic Systems of Michigan says human error is to blame for the failure in March at the clinic run by University Hospitals in suburban Cleveland. The hospital earlier said the storage tank was having trouble for weeks and an alarm system had been turned off. By John Seewer. SENT: 410 words.
DAIRY FARMER-MICHIGAN
ZEELAND TOWNSHIP, Mich. — A young Michigan dairy farmer has auctioned off his herd of 230 milking cows after being unable to absorb losses brought about by an oversupply of milk in the state. The Grand Rapids Press reports that Daybreak Dairy in Ottawa County has shuttered after 32-year-old dairy farmer Nate Elzinga and his family couldn't keep up with maintenance costs for the herd. Elzinga says they weren't getting paid enough for their milk. SENT: 280 words, photos.
BUSINESS:
BMW-CHINA TARIFFS
DETROIT — If a trade dispute between the U.S. and China escalates and both countries raise tariffs, American automakers won't suffer that much. But German luxury automakers BMW and Mercedes will. BMW exports about 87,000 SUVs from a factory near Spartanburg, South Carolina. Mercedes ships up to 75,000 more from Tuscaloosa County, Alabama. Analysts say if tariffs are doubled as China has threatened, BMW and Mercedes could be forced to move production to China or elsewhere. That could cost American jobs. By Tom Krisher. SENT: 1,060 words, photos.
VOLKSWAGEN
FRANKFURT, Germany — Volkswagen Group says it is contemplating a management reshuffle that raises questions about CEO Matthias Mueller's future with the company. The company said it is considering reassigning responsibilities among executives that "could include a change in the position of the chairman of the board of management," the German term for CEO. German news media report that Mueller would step aside as soon as Friday for Herbert Diess, head of the core Volkswagen brand. By David McHugh. SENT: 460 words, photos.
— TENNECO-FEDERAL MOGUL-ACQUISITION: The debt heavy auto parts company Federal-Mogul, owned by Carl Icahn's company, is being sold in a deal worth about $5.4 billion. SENT: 115 words.
IN BRIEF:
— CHEMICAL COMPANY-EVACUATIONS: Homes and businesses near a southern Michigan chemical manufacturer have been evacuated after a tanker truck hauling the chemical n-Butyl Methacrylate began to heat up.
— KALAMAZOO PARK FOUNTAIN: Removal of a southwestern Michigan park fountain that some say celebrates white supremacy is expected in the coming weeks.
— FLINT GOVERNANCE: The city of Flint has been officially released from state oversight after roughly six years — a period including a public health crisis spurred by high lead levels in its water.
— BANK ROBBERIES: A 70-year-old man is charged with robbing three banks in northern Michigan while distracting police with phony reports of crimes, including a school shooting.
— FATAL CRASH-MANHUNT: A man charged after fleeing a high-speed crash that killed a woman and critically injured her 5-year-old twin boys in western Michigan is awaiting sentencing. Photos.
— POLICE PURSUIT-FATAL CRASH: A southwestern Michigan man has been sentenced to more than 20 years in prison after pleading guilty in a police pursuit crash that killed an Indiana motorist.
— SPRING SNOW-MICHIGAN: A spring storm that moved through Michigan dropped snow on parts of the state, creating slippery driving conditions in places while missing others entirely.
— DEAD COWS-MICHIGAN: A Michigan farmer charged with animal cruelty after about 70 cows were found dead on his properties has been sentenced to 15 days in jail and ordered to pay nearly $20,000 in restitution. Photo.
— STATE PARK-DETROIT: Officials are seeking residents' thoughts on how to improve and what to add to Michigan's first urban state park.
— TROUT STOCKING: More than 4,000 adult trout have been stocked in three southeastern Michigan rivers, giving anglers an opportunity for some good springtime catches.
— DETROIT RIVERFRONT: A New York-based architectural firm has been selected to transform a park along the Detroit River.
— NEIGHBORHOODS PLAN-DETROIT: Detroit officials are seeking consultants to help create a child-centric framework for a west side neighborhood.
— TORNADO DRILL-MICHIGAN: Michigan residents are being asked by state officials to participate in a voluntary tornado drill.
SPORTS:
BBA--TIGERS-INDIANS
CLEVELAND — The Cleveland Indians' Josh Tomlin allowed a career-high four homers in a start last week against Los Angeles. He had his outing pushed back one day because of back soreness but faces the Detroit Tigers on Tuesday, who will start Matthew Boyd. The Tigers look for a win after dropping the first game of the four-game series Monday. UPCOMING: 600 words, photos. Game starts at 6:10 p.m.
With:
— BBA--TIGERS-BOSIO: Detroit Tigers pitching coach Chris Bosio is missing a game in Cleveland because of a health issue. SENT: 115 words.
HKN--RED WINGS-BLASHILL
DETROIT — The Detroit Red Wings are bringing coach Jeff Blashill for a fourth season, sticking with him while they rebuild after missing the postseason for the second year in a row. "I'm big on experience," Red Wings general manager Ken Holland said after announcing the decision on Tuesday. By Hockey Writer Larry Lage. SENT: 310 words, photo.
___
If you have stories of regional or statewide interest, please email them to apmichigan@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.