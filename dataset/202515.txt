Good weather and steady demand have made things ideal for kale growers this year.
"Weather has been unseasonably warm this year, and the product has reacted with higher yields per acre," Bruce Bolton, category manager at supplier Robinson Fresh in Bentonville, Ark., said in an e-mail. "We've seen higher yields in certain areas due to favorable growing conditions this year."
"The weather's been good," Bill Nardelli Jr., vice president of Cedarville, N.J. grower-shipper Nardelli Bros. Inc., said. "We've had good production and good yield and good volume on all the greens."
Kale spent the last few years as a consumer darling, with increasing demands each year. The demands has hit a plateau, but it's not declining.
"Kale over the years has seen some real increases with the health kicks and the healthy benefits," Nardelli said. "It's definitely picked up in volume in the last five to 10 years. Now what we've seen is it's really become a staple in people's diets, whereas it used to be more of a trend.
"They're using it in their daily lives, whether it's in their smoothies or in their kale salads, it's become a real mainstream item, and it looks to stay that way going forward," he said.
"The attention factor has gone away, it's not necessarily dwindled in volume, per se," Jonathan Steffy, director of sales and retail services at distributor Four Seasons Produce Inc., Ephrata, Penn., said. "As far as people talking about it and asking us about it, those questions have dwindled and now it's an established item."
Including kale in salad kits is still a big draw, according to Kori Tuggle, vice president of marketing and product development at grower-shipper Church Brothers Farms, Salinas, Calif.
"In drilling down in the retail data trends, salad kits that include Kale as a salad ingredient are often outpacing the rest of the category," Tuggle said in an e-mail. "I believe this trend is still happening for many reasons.
"Kale has a strong shelf life, eats well as it holds up to dressing, gives the salad kits a health halo and consumer preferences continue to shift towards earthy, heartier leafy greens."
While retail demand for kale may be leveling off, restaurants are still adding more kale to their menus.
"Kale demands at restaurants continue to grow as well. Menu mentions increased 37% over last year," Tuggle said in the e-mail, citing a report by Technomics.
Production to meet these demands is still growing, with kale acreage increasing for conventional and organic.
"The thing we've seen in the last year is the continued growth of people we can get it from," Steffy said. "The biggest proliferation of options has been people growing it organically, adding that to their items."
"I would say over the past five years we've had a 30% increase in acreage," Nardelli said. "We continue to put in more varieties of kale to meet the demand that's gone on the last few years."
The increased production and excellent weather have not been great for prices, though.
The U.S. Department of Agriculture on March 13 reported shipping point f.o.b.s for cartons of 24 bunches from Georgia to be mostly $7-7.50, a drop from last year's price of $11-12.
"Pricing has depressed year-over-year," Bolton said. "Most regions are producing excellent crops and yields are outstanding due to favorable weather conditions."
"This past year and so far in 2017 prices have been lower, I think that reflects the amount of production that has caught up to the kale demand," Steffy said. "The big spike in kale pricing we often see during diet season wasn't there this winter, it jumped a little this year, but not much."
That early year health rush and the high volume can create a buffer against low prices.
"There's been an abundance of product in the country, partially because of the good weather, so markets have been a little less than we like to see them," Nardelli said. "Our yield will help make up for some of the loss due to price."