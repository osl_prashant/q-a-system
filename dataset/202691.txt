Winning endorsement from produce industry leaders, the White House has appointed Ray Starling as special assistant to the president for agriculture, trade and food assistance. 
 
Starling was one of several individuals appointed to the White House National Economic Council in late February.
 
He was previously chief of staff for U.S. Sen. Thom Tillis, R-N.C., according to a news release. Starling also served as Sen. Tillis' chief counsel and then-speaker Tillis' general counsel and senior agriculture advisor in the North Carolina General Assembly, according to the release.
 
Starling is familiar with the Food Safety Modernization Act and served on a food safety task force in North Carolina, said Jim Gorny, vice president for food safety and technology for the Newark, Del.-based Produce Marketing Association. 
 
"He is well familiar with FSMA and all the issues of implementation," Gorny said. 
 
The fact that North Carolina is an agriculturally diverse state with a wide variety of high value specialty crops gives Starling good knowledge of the fresh produce industry, Gorny said.
 
Sen. Tillis is an important member of the Senate Agriculture Committee, and Gorny said Starling is well versed on all U.S. agricultural issues. 
 
"He is, quite frankly, a great choice," Gorny said.
 
The United Fresh Produce Association released a statement that Starling has a deep understanding of agriculture and food policy.
 
"He most certainly will be a critical voice to President Trump and the National Economic Council on how important food and agriculture interconnect with regulatory and policy decisions made by Congress and the administration," the group said in the statement.
 
Starling also had been the general counsel for the N.C. Department of Agriculture and Consumer Services, and also has taught numerous agricultural and food law courses, according to the release. Growing up on a family farm in North Carolina, Starling earned a B.S. in agricultural education from North Carolina State University and a J.D. from University of North Carolina at Chapel Hill, according to the release.