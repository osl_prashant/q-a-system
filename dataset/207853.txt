Consolidated West adds organic items
Watch for more organic items from Consolidated West Distributing Inc., Commerce, Calif., during the coming months.
“We are expanding into organic apples, pears and cherries,” said Elvia Menendez, owner and president.
Organic apples and pears should be available starting in August or September, she said, while organic cherries will be shipped starting next season.
 
Great West focuses on San Francisco
Commerce, Calif.-based Great West Produce Co. Inc. is strengthening its focus on the firm’s San Francisco location that was opened about 18 months ago, president Sean Villa said.
Called Great West Gourmet, the Northern California location focuses on foodservice, he said.
“This year has given us the opportunity to look into new product lines, different packs than we have traditionally offered and more interesting, more upscale items than we have traditionally done to support that market,” he said.
The company has stepped up its programs involving specialty oils, specialty dressings and some protein items — such as shrimp, ahi poke and fresh and frozen beef and chicken — that had never been part of its offerings, he said.
Also, Khamis Zananiri, who has five years’ experience in the industry, has been hired as a salesman at the San Francisco office, and the company has increased the size of its fleet of trucks more than 30% to keep pace with the growth, he said.
 
Harvest Sensations conducts redesign
Robin Osterhues, who has stewarded such brands as Chiquita and Calavo, has been named director of marketing for Los Angeles-based Harvest Sensations LLC, and the company has hired Alejandro Viveros as controller.
“Harvest Sensations is in a very exciting time right now, as we are undergoing a positioning and optic update, including redesign of logo, consumer-facing packaging, website and all (marketing communications) initiatives to build up our line of branded retail products, some of which will be in the value-added category,” Osterhues said.
The company will introduce more of its core and organic items under the Harvest Sensations Sensationally Fresh banner, she said.
“With procurement teams located in both Los Angeles and Miami, we are fully able to utilize our direct grower relationships from around the world to provide foodservice, home delivery and retailers with year-round product,” she said.
 
Melissa’s debuts two products
Los Angeles-based World Variety Produce Inc. has released two new Melissa’s brand products — a new variety of hybrid cauliflower called Fioretto and green Hatch chili powder shakers, said Robert Schueller, director of public relations.
Fioretto has a softer texture and sweeter flavor than standard cauliflower, he said.
It retains its freshness for up to a week when refrigerated and can be added to stir-fries and soups. It also can be blanched, mashed, pickled, steamed or roasted.
The chili powder shakers are made from dried Hatch chili peppers picked in their green stage, before ripening to red, so the flavor is “light and fresh with a spicy aftertaste that lingers pleasantly on the palate,” he said. They’re available in mild and hot versions.
 
Pura Vida moves headquarters
Pura Vida Farms, formerly based in Scottsdale, Ariz., has moved its headquarters to Brea, Calif., owner Wes Liefer said.
All the functions of the business now are handled out of Brea, he said.
The company focuses on cantaloupes, honeydews, watermelons, mixed melons and pineapples.
 
Veg-Land upgrades refrigeration
Veg-Land Inc. has upgraded the refrigeration system at its facilities in Fullerton, Calif., said Jimmy Matiasevich, co-owner.
All of the firm’s loading docks and warehouses now are refrigerated in order to maintain the cold chain, he said.
The company also has hired Caitlin Stryker, a recent graduate of Brigham Young University in Provo, Utah, as food safety manager. Stryker has a bachelor’s degree in food science. And Rob Gurney, formerly a salesman for the firm, has been promoted to sales manager.
Finally, the company has played a vital role in providing emergency lunches for California Department of Forestry and Fire Protection firefighters engaged in combating wildfires throughout the state, said Melissa Herson, the firm’s emergency services coordinator.
Veg-Land may expand the program to include other emergencies, as well, Matiasevich said.