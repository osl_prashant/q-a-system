Possible suspect in death of pregnant goat barred from farm
Possible suspect in death of pregnant goat barred from farm

The Associated Press

SCARBOROUGH, Maine




SCARBOROUGH, Maine (AP) — A man questioned in the death of a pregnant goat killed with a crossbow has been barred from the Maine farm where the animal was found.
The Portland Press Herald reports that a judge issued a protective order for the owner of Smiling Hill Farm against 40-year-old Daniel Arnold.
Police investigating the goat's death arrested Arnold last month on suspicion of possessing a crossbow in violation of a previous probation order. He has not been charged with killing the animal.
Investigators say he was seen near the farm on Feb. 17, the night before the goat was found dead. Police say he has denied shooting the animal and says he did not have his crossbow with him that night.