BC-USDA-Okla City Catl Actn
BC-USDA-Okla City Catl Actn

The Associated Press



KO—LS150 Oklahoma City, OK    Mon  Apr  16,  2018    USDA-OK Dept of Ag Market NewsOklahoma National Stockyards - Oklahoma City, OKLivestock Auction Report for 4/16/2018 AUCTION                          Receipts           Week Ago             Year Ago--------------------------------------------------------------------------------- Total Receipts:                  8,100                 4,295              5,699------------------------------------------------------------------------------ Feeder Cattle:                   8,100(100           4,295(100%)        5,699(100%)---------------------------------------------------------------------------------*** Open ***Compared to last week:  Feeder steers and heifers are trading 3.00-6.00 higher with instances up to 8.00 higher, on 800-900 lb steers.Steer and heifer calves to lightly tested for an accurate market test but a higher undertone is noted.  Demand good to very good for all classes. Quality average to attractive.  Feeder cattle supply included 62 percent Steers, 34 percent Heifers, and  4 percent Bulls. Feeder cattle supplyover 600 lbs was 72 percent.--------------------------------------------------------------------------------FEEDER CATTLE--------------------------------------------------------------------------------Feeder Steers(Per CWT):  Medium and Large 1 400-500 lbs 181.00-194.50; 500-600 lbs 164.00-178.00, 540 lbs Fleshy 166.50, Thin Fleshed 175.50; 600-700 lbs152.50-166.50, Calves 142.00-159.50; 763 lbs 151.50, 782 lbs Fleshy 141.00, 776lbs Thin Fleshed 154.25; 800-900 lbs 130.00-140.50; 900-1000 lbs 121.50-125.00; 1029 lbs 118.25. Medium and Large 1-2 515 lbs 165.00; 600-700 lbs 150.50-153.00, Calves 140.00-147.00; 700-800 lbs 135.50-142.50; 863 lbs 132.00; 924lbs 121.00. Medium and Large 2 756 lbs 135.00.Feeder Heifers(Per CWT):  Medium and Large 1 289 lbs Thin Fleshed 211.00;381 lbs 177.00; 400-500 lbs 159.00-168.50; 508 lbs 159.00, 592 lbs Fleshy 147.00; 600-700 lbs 137.75-144.00, 641 lbs Calves 136.00; 700-800 lbs 128.00-135.25;800-900 lbs 123.50-125.00; 900-1000 lbs 117.50-119.50. Medium and Large 1-2 489lbs 155.00; 535 lbs 149.00; 681 lbs 132.00, 659 lbs Calves 138.25; 790 lbs126.75; 854 lbs 119.25; 911 lbs 116.00. Medium and Large 2 847 lbs 115.00.Feeder Bulls(Per CWT):  Medium and Large 1 261 lbs Thin Fleshed 213.00; 334lbs Thin Fleshed 226.00; 432 lbs 175.00; 528 lbs 155.50. Medium and Large 1-2507 lbs 167.00.Please Note:The above USDA LPGMN price report is reflective of the majority of classes and grades of livestock offered for sale.  There may be instances where some salesdo not fit within reporting guidelines and therefore will not be included in the report.  Prices are reported on an FOB basis, unless otherwise noted. Source:  USDA-OK Dept of Ag Market News ServiceTina Colby, Market Reporter, N/A24 Hour Market Report 1-573-522-9244www.ams.usda.gov/mnreports/KO—LS150.txtwww.ams.usda.gov/market-news/livestock-poultry-grain