Starting Jan. 1, J. David Smith will be president and chief marketing office for The Oppenheimer Group. 
The Vancouver, British Columbia-based company said in a news release that Smith will have strategic oversight of sales, categories and marketing. Smith joined Oppy as a sales representative in 1989 and since 2014 he has been executive vice president and chief marketing officer for the company.
 
Operations, finance and human resources will continue to report to Oppy chairman, president and CEO John Anderson, according to the release. Smith’s promotion will allow Anderson to delegate more of the day-to-day operation of the business and focus on the strategic growth of the organization, according to the release.
 
“It’s difficult to articulate David Smith’s immense contributions to our growth and success,” Anderson said in the release. “He’s a strong leader with unparalleled knowledge of our business and a proven acumen for connecting our grower partners’ yields with the demands of the market to the benefit of all.” 
 
In other moves at Oppenheimer, James Milne has been elevated to vice president of marketing, David Nelley and Steve Woodyear-Smith are the company’s new vice presidents of categories, according to the release. Fernando Caudillo has been named category director of tropicals, according to the release.
 
“I’m very proud to announce these promotions,” Anderson said in the release. “Our company has enjoyed significant strategic growth over the past several years, thanks in large part to the proven leadership of these individuals and the teams they guide and empower. In their new roles, they can focus their energies even more specifically to the achievement of our future goals.”
 
Milne took over Oppy’s entire apple and pear category as its pipfruit category director in 1997 and became Oppy’s director of marketing in 2001. Most recently, Milne has been executive director of marketing for Oppy.
 
Nelley and Woodyear-Smith have been promoted from executive category director roles to vice president of category roles, according to the release.
 
Nelley will now be vice president of Oppy’s apple, pear and cherry categories, and oversee the company’s export strategy for products originating in North America, according to the release.
 
The release said Woodyear-Smith, who most recently served as executive category director of tropicals, will take on the role of vice president of categories for tropicals, citrus and avocados.