A training course to become a Food Safety Preventative Controls Alliance designated Preventative Controls Qualified Individual will be held in Ames, Iowa from Jan. 9-11.

This course is the standardized training required by FSPCA for facilities that are processing any type of animal food (complete feed or ingredients). It is sponsored by the Iowa Grain Quality Initiative and the American Feed Industry Association.

The Food Safety Modernization Act requires processing facilities to comply with the new current good manufacturing practices and to implement a written animal food safety plan developed and overseen by a preventative controls qualified individual.

Individuals who operate an animal food facility are encouraged to attend this course to obtain their designated PCQI training certification. Certifications will be given by the FSPCA to attendees who complete all sessions of the course.

The course is being taught by Charles Hurburgh, professor and grain quality and handling specialist with Iowa State University Extension and Outreach; Gary Huddleston, director of feed manufacturing and special affairs with the American Feed Industry Association; Connie Hardy, program specialist in Value Added Agriculture with ISU Extension and Outreach; and Kim Anderson, program manager with ISU Extension and Outreach.

For more information, contact Hurburgh at 515-294-8629 or tatry@iastate.edu or visit http://www.aep.iastate.edu/animalfood/.