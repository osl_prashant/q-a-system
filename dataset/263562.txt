Court partially upholds penalties in Utah child-labor case
Court partially upholds penalties in Utah child-labor case

By BRADY McCOMBSAssociated Press
The Associated Press

SALT LAKE CITY




SALT LAKE CITY (AP) — A U.S. appeals court is upholding a ruling that a Utah contractor with ties to a polygamous group must still set aside $200,000 to pay back wages to children who prosecutors say were forced to pick pecans.
But the 10th Circuit Court of Appeals said in a ruling issued Tuesday that the lower court judge erred in assigning Paragon Contractors a special master to monitor the company.
Attorneys for Paragon didn't immediately return emails and phone calls seeking comment.
The Department of Labor didn't return an email seeking comment. The agency accused Paragon of violating a 2007 agreement by putting nearly 200 children to work picking pecans for long hours in the cold, without pay.
Paragon attorneys said the kids were volunteering with their families to pick up fallen nuts for the needy.