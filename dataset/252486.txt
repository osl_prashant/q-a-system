Forest Service chief retires following harassment reports
Forest Service chief retires following harassment reports

By MATTHEW BROWNAssociated Press
The Associated Press

BILLINGS, Mont.




BILLINGS, Mont. (AP) — President Donald Trump's administration moved quickly Thursday to install a female wildland firefighter to lead the U.S. Forest Service after the agency's former chief stepped down amid sexual misconduct allegations.
The appointment of Vickie Christiansen came as lawmakers from both parties called for more aggressive efforts to combat a culture of harassment and retaliation within the Forest Service. The problems mirror recent misconduct within the nation's other major public lands agency, the Interior Department.
Christiansen has been with the Forest Service for seven years and became a deputy chief in 2016. Before joining the federal government she'd worked in forestry for 30 years at the state level, in Arizona and Washington.
She did not return an emailed message from The Associated Press seeking comment.
Agriculture Secretary Sonny Perdue said in an email to the Forest Service's approximately 35,000 employees that it had been "a difficult week," punctuated by Wednesday's abrupt retirement of Tony Tooke.
Tooke's departure came just days after PBS NewsHour reported he was under investigation following relationships with subordinates prior to his appointment last August.
The leadership changes at the agency were first reported by The Missoulian.
Abby Bolt, a Forest Service employee in California with a pending sexual discrimination complaint against her male supervisors, told The Associated Press that rumors of Tooke's relationships had started circulating within the agency as soon as he was appointed.
Bolt, a fire battalion chief now on leave, said she was hopeful Christiansen would bring some "fresh eyes" to the Forest Service's problems, but also wants Tooke held to account for any wrongdoing.
"If we just have somebody retire and step down, then we don't get to see that," Bolt said.
Preliminary results of a sexual harassment audit released Thursday by the Agriculture Department's inspector general said that almost half of employees interviewed had expressed distrust in the process of reporting complaints.
Perdue said more steps already were being taken to protect victims from retaliation. Those include using outside investigators for at least the next year to investigate sexual misconduct complaints, according an Agriculture Department inspector general's audit released Thursday.
Representatives of the Forest Service and its parent agency, the Department of Agriculture, did not answer repeated questions Thursday about whether the investigation into Tooke would continue. They also declined to answer if an outside investigator was handling the case.
Tooke said in a final note to employees that the agency deserved a leader with "moral authority" as it addresses reports of rampant misconduct and bullying of female employees. He did not directly deny the allegations against him but said he "cannot combat every inaccuracy that is reported."
Lawmakers expressed outrage over the events and called for a hearing and investigation.
"I plan to use every tool to ensure all bad actors are held accountable," said Republican U.S. Sen. Steve Daines of Montana, who chairs the Senate agriculture subcommittee that oversees the Forest Service. He said he'll hold a hearing on sexual harassment in the agency.
Rep. Jackie Speier of California, a Democrat and leading voice in Congress against sex harassment, said a wide investigation was needed of into the Forest Service's "toxic culture" by the inspector general of the U.S. Department of Agriculture.
The Forest Service has about 35,000 employees and manages more than 300,000 square miles (777,000 square kilometers) of forests and grasslands in 43 states and Puerto Rico.
Lawmakers in Congress held hearings on sex harassment at agencies in 2016. Senior officials have repeatedly vowed to address the problem, both during the administration of former President Barack Obama and more recently under President Donald Trump.
Tooke, a native of Alabama who joined the Forest Service at the age of 18, had worked in Florida, Alabama, Georgia and Mississippi. Prior to becoming chief he served as regional forester for the southern U.S.
In announcing his appointment in August, Perdue cited Tooke's knowledge of forestry and his dedication to the "noble cause" of being a steward of public forests.
"Tony has been preparing for this role his whole life," Perdue said at the time. "His transition into leadership will be seamless."
___
Follow Matthew Brown on Twitter at www.twitter.com/matthewbrownap .