Frieda's Specialty Produce, Los Alamitos, Calif., is promoting horseradish root for Passover.
"Horseradish and parsley sales go through the roof the week before Passover as they are the most traditional produce items used for the holiday meal," said Karen Caplan, president and CEO of Frieda's Specialty Produce, in a news release.
 Retailers can call Frieda's account managers to pre-order horseradish roots, as well as other Passover items including parsley, beets/baby beets, fingerling potatoes, parsnips, parsley root, specialty carrots and baby apples, according to the release.

Horseradish is gaining popularity as part of a growing weekend brunch trend as well.
"Weekend brunch has become a part of the culture, and fresh horseradish is going to grow along with it," Caplan said."Once shoppers experience fresh horseradish while dining out, they will try to replicate it at home."