Global dairy prices slipped for the second time in a row at a fortnightly auction held early on Wednesday morning as an influx of supply from New Zealand curbed buying.
The Global Dairy Trade Price index fell 0.6 percent, with an average selling price of $3,593 per tonne, said auction platform GDT Events.
The index had edged down 0.5 pct at the previous sale, snapping three consecutive auctions of gains.
Whole milk powder (WMP), the most widely traded item, fell 0.8 percent after New Zealand dairy giant Fonterra increased the amount of powder on offer, though that was still a better result than the 2.5 percent drop expected by derivatives markets.
"Fonterra had increased its WMP offer volumes ahead of this event as milk flows start to improve, so buyers are unlikely to feel as much urgency to secure product," said Amy Castleton, dairy analyst at AgriHQ.
New Zealand, the world's largest dairy exporter, had suffered from curbed supply caused by unusually dry weather late last year, which had pushed up prices. Wetter weather in recent weeks led to a more favorable outlook for supply, which meant further price gains were likely to be muted.
"From here, we expect prices to ease further through to the end of the season. We expect NZ production to improve on the back of the increased rainfall. In turn, this improved production should put modest downward pressure on prices," said Nathan Penny, ASB economist.
The auction results can affect the New Zealand dollar as the dairy sector generates more than 7 percent of the nation's gross domestic product.
However, Wednesday's auction had little impact on the currency, which had soared almost 1 percent overnight on improved global risk sentiment and a weaker U.S. dollar.
A total of 19,292 tonnes was sold at the latest auction, falling 4.8 percent from the previous one, the auction platform said on its website.
GDT Events is owned by Fonterra but operates independently from the dairy giant.
U.S.-listed CRA International Inc is the trading manager for the twice-monthly Global Dairy Trade auction.
A number of companies, including Dairy America and Murray Goulburn , use the platform to sell milk powder and other dairy products.
The auctions are held twice a month, with the next one scheduled for March 20.