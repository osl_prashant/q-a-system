Tuesday was a busy day for the USDA with the release of many key reports, including the Annual Crop Production summary, World Agricultural Supply and Demand Estimates (WASDE) report and Grain Stocks.Below is a closer look at what these reports said. For a more in-depth look at the reports, visit our friends at AgWeb.com.
Annual Crop ProductionThe USDA estimated corn for grain production in 2015 at 13.5 billion bushels, down 4 percent from the 2014 estimate. The harvested area, reported at 80.7 million acres, was down from 2014 by 3 percent.
On the flipside, soybean production in 2015 was one for the record books. A record 3.93 billion bushes were harvested with the average yield per acre estimated at 48 bushels, 0.5 bushels above the 2014 yield. Harvested area for soybeans was down by roughly 1 percent from 2014's record acreage to 81.8 million acres.
Read more from the USDA. 
WASDEThe forecast for the 2016 total meat production was raised from last month's report as higher pork, broiler and turkey production offset lower beef production.
For 2016, the USDA increased pork production slightly. Forecasters noted pork producers indicated in the Quarterly Hogs and Pigs report intentions to farrow fewer pigs on average through the first half of 2016, limiting growth in the pig crop.
"However, higher carcass weights will support increased pork production," the USDA added.
Hog prices for the next year have been reduced as large supplies of hogs and competing meats will likely pressure prices through early 2016.
For grain, the USDA projected the average corn price for the 2015-2016 season to range between $3.30 and $3.90 per bushel - down slightly from last month's report. The season-average soybean price was projected at $8.05 to $9.55 per bushel, down 10 cents at midpoint.
Read more from the USDA. 
Grain StocksThe USDA showed an increase in both corn and soybean stocks from 2014. Corn, stored in all positions as of Dec. 1, totaled 11.2 billion bushels - up slightly from December 2014. Soybeans stored in all positions were up 7 percent from December 2014 at 2.71 billion bushels.
Read more from the USDA.