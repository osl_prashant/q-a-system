Anhydrous is $83.44 below year-ago pricing -- lower $5.34/st this week at $519.27.
Urea is $24.29 below the same time last year -- lower $1.09/st this week to $350.97.
UAN28% is $26.84 below year-ago -- higher $1.10/st this week to $252.58.
UAN32% is priced $34.33 below last year -- lower $2.98/st this week at $272.87.

Anhydrous ammonia led declines in the nitrogen segment as led by Minnesota which fell $40.22 and Nebraska which shucked $18.79. Other states were around $2-$3 lower. Four states were unchanged as Ohio was our only gainer, adding $9.59.
UAN32% was led lower by Minnesota which fell $6.54 as Illinois softened $3.96.Six states were unchanged as Indiana firmed $5.61 and Iowa added 44 cents per short ton.
Urea was lower on a $5.74 decline in North Dakota as Minnesota softened $5.18. Four states were unchanged as Michigan added $6.67 and Indiana firmed $1.72.
UAN28% posted the only higher overall price in the nitrogen segment. That was led by Minnesota which added $5.79, Ohio gained $4.50 and Michigan firmed $4.18. Five states were unchanged as Illinois fell $3.56 and Nebraska dropped $1.05.
The overall downtrend in nitrogen pricing continues and while we believe there is still upside risk in play, the calendar flip to June will mean that many acres that need to be replanted will go to beans instead of corn. The deeper we get into June, the more that upside risk will diminish, but sidedress season is expected to feature strong demand for replacement nitrogen on the corn that has emerged. We continue to urge growers who expect to apply extra sidedressed nitrogen to communicate their needs with preferred retailers and lock in today's price in order to stay ahead of that upside price risk.
The general message from nitrogen this week, however, is that Midwestern supplies are ample and as the season fully winds down, nitrogen's downward trajectory suggests fall prices may actually come in below today's. We are not nearly as anxious to prebook our fall nitrogen as we were just a few weeks ago and advise caution as we head into the summer offseason. If we are to see a price pop, it will be directly related to sidedress activity and we expect any upside swings to exhaust themselves in a hurry.
December 2017 corn closed at $3.92 on Friday, May 26. That places expected new-crop revenue (eNCR) per acre based on Dec '17 futures at $621.70 with the eNCR17/NH3 spread at -102.43 with anhydrous ammonia priced at a discount to expected new-crop revenue. The spread widened 8.73 points on the week.





Nitrogen pricing by pound of N 6/1/17


Anhydrous $N/lb


Urea $N/lb


UAN28 $N/lb


UAN32 $N/lb



Midwest Average

$0.31 3/4


$0.39


$0.45 1/4


$0.42 1/2



Year-ago

$0.37 


$0.41 1/2


$0.50


$0.47 3/4





The Margins by lb/N -- UAN32% is at a 1 1/2 cent premium to NH3. Urea is 2 1/4 cents above anhydrous ammonia; UAN28% solution is priced 3/4 cent above NH3. The margins widened slightly this week.





Nitrogen


Expected Margin


Current Price by the Pound of N


Actual Margin This Week


Outstanding Spread




Anhydrous Ammonia (NH3)


0


31 3/4 cents


0


0




Urea


NH3 +5 cents


39 cents


+7 1/4 cents


+2 1/4 cents




UAN28%


NH3 +12 cents


45 1/4 cents


+13 1/2 cents


+1 1/2 cent




UAN32%


NH3 +10 cents


42 1/2 cents


+10 3/4 cents


+3/4 cents