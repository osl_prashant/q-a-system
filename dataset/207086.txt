Missouri beef producers have it good when it comes to feed resources, says Eric Bailey, University of Missouri Extension nutritionist.On Sept. 21 at the MU Thompson Farm, Spickard, he'll share his good news. Bailey, new to Missouri, can tell what cow owners face in New Mexico, Texas and Kansas. "I've seen lots of prairie hay with no nutrient value at all," Bailey says.
"Missourians make top-notch hay," he says. Also, Show-Me producers have access to many distillers byproducts and alternative feeds.
Other speakers at the Thompson Farm event will talk genetic advances. They know the part nutrition plays in expression of genetic potential.
Research on fixed-time artificial insemination (FTAI) will be updated.
The annual event at the MU research farm west of Spickard will be held in the evening, a change. Sign-in starts at 3:30 p.m. with farm tours to follow. Talks begin at 6 p.m. and dinner is at 7:15. Last talk, on price premiums for selling quality beef, will be at 8:15 p.m.
"Missouri has lots of high-quality cattle," Bailey says. "I see that, driving up and down the roads." He's been across the state on get-acquainted tours.
Bailey doesn't plan to get deep into details at first. "I want herd owners to think feeding systems. First, they must think feed intake."
Owners must have some idea on what is enough. Also, they need to know what is too much, he says.
For example, a 1,400-pound cow eats 36 pounds of feed per day. Multiply that by 30 for forage for a month. Then extend that for a year. That's 12,960 pounds. "Now think 7 tons per cow for the year. Then allow for waste loss," Bailey says.
To put a frame on mineral mixes, do similar figures. Mineral bag labels state average consumption of 4 ounces per head per day. That's 91.25 pounds for a year. That's two 50-pound bags of supplement.
"Start with getting feed intake right," Bailey says. That's for grazing or rolling out hay bales.
"Get feed out in front of them," he says. "If you feed a cow only 2 pounds a day, it doesn't matter how good the ration. She'll lose body condition."
Next, Bailey wants producers to know their feed costs. A cow pays for her feed with one calf per year. "If you spend more than the price of one calf, you lose money."
Nutrition must stay in the framework of one cow having one calf. "If a calf sells for $750, that's all you can spend for all costs of production that year."
After starting with the big picture, particularly on feed intake and cost, Bailey can help refine a ration. That can improve production while keeping expenses under control.
Later, Bailey will give lessons on cutting waste. Instead of worrying too much about mineral mix, start with a rainproof feeder. Or even bigger, farmers can learn to cut waste when feeding hay.
The Thompson Research Center, a part of the MU College of Agriculture, Food and Natural Resources, Columbia, is at the end of state Highway C, 7 miles west of Spickard. That's off U.S. Highway 65 in northwestern Grundy County.
The event and dinner are free. Sponsors will set up exhibits in the breeding barn.