Fishermen of baby eels expect high price as stocks dry up
Fishermen of baby eels expect high price as stocks dry up

By PATRICK WHITTLEAssociated Press
The Associated Press

ROCKPORT, Maine




ROCKPORT, Maine (AP) — Members of Maine's baby-eel fishing industry are expecting high prices for the tiny fish this year because of a shortage on the international market, and sushi lovers could end up feeling the pinch.
Maine is the only U.S. state with a significant fishery for baby eels, or elvers. The tiny, translucent eels are sold to Asian aquaculture companies to be raised to maturity for use as food. They're a key piece of the worldwide supply chain for Japanese dishes such as unagi, and some eventually make it back to the U.S.
The eels sold for about $1,300 per pound at the docks last year, about on par with an ounce of gold, and are already one of the most lucrative fisheries in the country on a per-pound basis. Fishermen in Asia are seeing a poor harvest this year, and European eel fisheries are cracking down on poaching, said state Rep. Jeffrey Pierce, a Dresden Republican and consultant to the elver fishery.
That means Maine's elvers will be in higher demand, and prices could be higher for consumers.
"It was just a bad year in Asia," Pierce said. "With Europe tightening up that market, and us already having tightened up, it should be a good year."
The elver fishing season begins March 22 and ends June 7. They are fished by nets from rivers and streams and sold to dealers in a tightly regulated fishery that uses a swipe-card system to deter poaching. It takes more than 2,000 elvers to make a single pound.
Richie Akizaki, a sushi chef at Benkay in Portland, said he buys his eels from a New York distributor who has told him prices will likely be double the normal amount this summer.
"Eel prices are going to be really high this year. We're having problems with the harvest of baby eels," Akizaki said. "Serious problems with catching baby eels in Japan."
Maine's elver fishery must also abide by a strict quota system set by an interstate commission. The Atlantic States Marine Fisheries Commission limits the fishery to 9,688 pounds of elvers per year and unveiled new rules in February that could increase that total to 11,479 pounds. But an increase won't be approved in time for this fishing year, if it happens at all.
Whether fishermen make quota typically depends on weather and the thaw of Maine's waterways, because they can't harvest elvers from frozen rivers. Fishermen have fallen a few hundred pounds short of quota in two consecutive years.
The fishery is a source of reliable income in rural Maine. Julie Keene, an elver fishermen based in Lubec, is looking forward to a good harvest this year.
"And I'll be able to support my children," she said. "That's what it's about for me."