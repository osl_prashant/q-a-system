Ashley Arrington of Agri Authority shares the following observations with "Top Producer Podcast" host Pam Fretwell aboutthe one thing young farmers need to know."What I see the most with young farmers is growing too big too fast. That can really take on a bunch of mistakes and a bunch of problems‚Äîtaking on too much debt, overleveraging, cashing out equity in order to grow too rapidly. I find that when young farmers fail to analyze their operation financially as well as the operational side before expanding, that can be the most detrimental mistake.
"You've got to spot check things and see what you're good at. See how much you can take on before you expand. Carpet cleaner says you need to test on a small spot before you spray it all over the floor and mess up the whole floor.
"Working capital is really something you have to look at, and understanding your cash-flow needs, what time of year you need your cash, and your access to the cash. I was in ag banking for a decade. I look at debt-service coverage a good bit, your ability to repay your debt and how much excess cash you'll have left over after that, which also is your additional debt capacity if you need to borrow more money, if you need to buy more equipment, if you need to have access to additional cash.
Click the image below to download our free e-book:

"Being able to track those things to see how they fluctuate with the way you spend your money is critical to running your operation for the year."
Click here to download your copy of our free e-book, "One Thing Every Young Farmer Must Know".
More from Top Producer:
Register for the 2016 Tomorrow's Top Producer conference June 16-17 in Nashville.
Elevate Your Farm Financial Plan This June
Combat Business Conflict
Diversify From Your Core
Sharpen Your Farm Management Toolset
Understand Commodity Market Pacing
Craft a Strategic Plan for Your Farm Business
Why You Should Think About Your Farm Business Daily
Propel Your Farm Forward