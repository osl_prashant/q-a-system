For many, it’s not Christmas Eve until the somber strains of ‘O Holy Night’ ring through the sanctuary. But the venerable Christmas hymn was once banned from churches across its native France. In fact, according to author Ace Collins, ‘O Holy Night’ has possibly the most colorful and lengthy history of all the Christmas favorites.
Collins, author of “Stories Behind the Best-Loved Songs of Christmas” and a series of books about Christmas songs and traditions, has been a December staple on the AgriTalk Radio Show for a decade. Collins says the song started as a collaboration among friends in France, and the song quickly became widely popular across the country, but when church officials found out the collaborator behind the music was Jewish, it was banned as secular. Later, Collins says, the song made its way to America, not as a Christmas carol, but as an abolitionist anthem. Then it made history as the first song ever played on the radio.
Hear the amazing journey of ‘O Holy Night’ from Ace Collins on AgriTalk in the player below.

 
‘O Little Town of Bethlehem:’ The song that sprung from a pastor’s loss of faith after giving Abraham Lincoln’s eulogy.

 
‘Rudolph the Red Nose Reindeer:’ A little girl’s desperate Christmas leads to marketing magic.

 
‘The Christmas Song:’ A song written in California’s sweltering summer heat helps break the color barrier.

 
‘Away in a Manger:’ A simple lullaby from a Pennsylvania farm

 
‘Mary Did You Know?;’ What if a reporter covered the birth of Jesus?

 
AgriTalk will air a special encore of Ace Collins’ Christmas stories on Christmas morning. You can find out more about Ace Collins’ books at www.AceCollins.com.