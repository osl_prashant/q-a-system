Meet Paul Pittman, CEO of Farmland Partners Inc.Company: Farmland Partners, a publicly traded (NYSE: FPI) real estate company, was founded byPaul Pittman in 2014. Today, the company owns approximately $1 billion of farmland across 17 states,on which more than 110 tenants are growing about 30 commercial crops.
One book managers must read: Any books on the fundamentals of valuation of assets, suchas Ben Graham’s “Intelligent Investor.”
Favorite quote: “If you are going through hell, keep going.” —Winston Churchill (attributed)
What is your personal background, and why did you start Farmland Partners?
I’m an unusual combination of ag guy and finance guy. I would have become a farmer, but when I left collegein 1985, it wasn’t the right time to go into production agriculture unless you had a pretty good land base,which I didn’t. So after the University of Illinois, I went to Harvard University, where I got a master’s in publicpolicy, and then to law school at the University of Chicago. Eventually, I landed at Wall Street, first as alawyer and then as a banker.




Paul Pittman will speak on Wednesday, Jan. 25 at Top Producer Seminar.His presentation is titled, “Farmland Investors: The Emerging Disruptor.”


© Farmland Partners



Starting in the mid-1990s, I took all my capital and bought farmland. I think it is a misunderstood assetclass. People confuse farming risk with farmland risk. Farming is a risky business. Farmland is very different.You will never hit the home run owning farmland.
Why did you take Farmland Partners public?
Farming and agriculture need outside capital. No industry survives without new capital, new investors and new ideas. I asked: How do you bring urban capital back into agriculture? Most farms are family-ownedbusinesses. Nobody wants to be a minority owner of a family-owned business. So the way to get the capital isthrough land ownership.
Everyone thinks we are buying land from farmers and renting it back to them. But two-thirds of our deals arebuying land from a remote landowner, someone who inherited the land a few generations ago who doesn’t live on the farm. We are a new source of capital when landowners decide to sell. I think that is good for the agindustry, and it’s good for the investor.
From the investor side, you get exposure to one of the most important industries in the U.S. Until recently,there was no way to be an investor in this asset class through the stock market. Investing in farmland isn’tjust about making money. There are 1 billion people in the world who are still starving. Those people need tobe fed. Attracting additional capital, ideas and technology to the industry is how those people get fed.
What qualities do you look for in farmland?
We like to buy high-quality farms with good water and tenants in the area. Otherwise, you have risk. What do you see as the makeup of farmland ownership and management 20 years from now? Many commentators think you will see operations and land ownership separate. I don’t think that will or should happen. I believe farmers should own 20% or 30% of what they farm and rent the rest from long-term, stable landowners, some of whom will be institutions or wealthy families who continue to own land but not farm.
Then you have farmers owning substantial parts of the land they farm so they also share in the long-term land appreciation. What makes U.S. agriculture so productive is it’s an industry full of reasonably small holders who own their own farms and are their own business people. I hope that doesn’t go away.
Why has Farmland Partners seen large growth?
The reason we’re growing so fast is farmland is a huge untapped market. There are $30 billion of farmland forsale every year. Finding good deals is not hard. Even if only 10% of those deals were good ones (way moreare good deals than that), that’s still $3 billion. We grew $500 million to $600 million of new assets in the pastyear. We’re just a drop in the bucket in a really big market.