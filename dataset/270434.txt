3 charged in deaths of dozens of cattle at Nebraska farm
3 charged in deaths of dozens of cattle at Nebraska farm

The Associated Press

OVERTON, Neb.




OVERTON, Neb. (AP) — Three family members have been charged with animal cruelty and neglect following the deaths of dozens of cattle on their Nebraska farm.
Acting under search warrants, deputies found at least 65 carcasses on the property near Overton, about 150 miles (240 kilometers) west of Lincoln. No information has been released on the cause of death, but many of the animals were emaciated.
Fifty-nine-year-old Eugene Wempen Sr., his wife Diane, and their 33-year-old son, Eugene Wempen Jr., are charged with four felony and misdemeanor counts. Their next court hearing is set for May 1 in Lexington.
Eugene Wempen Sr.'s attorney hasn't returned a call seeking comment Friday. A phone listed for his son's attorney rang unanswered. Court records don't list an attorney who can comment on behalf of Diane Wempen.