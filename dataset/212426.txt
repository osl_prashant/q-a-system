A new way to market tiny bits of cauliflower is bringing the rice industry to a boil.The core of the issue is “cauliflower rice,” a “grain substitute” that now is drawing not only the attention of broadcast and online food channels but the marketing power of Green Giant Fresh, among other produce suppliers.
The rice industry is steamed.
“Vegetables that have gone through a ricer are still vegetables, just in a different form,” President and CEO Betsy Ward of the USA Rice Federation, a lobbying group, said in a May news release. “Only rice is rice, and calling ‘riced vegetables,’ ‘rice,’ is misleading and confusing to consumers. We may be asking the (Food and Drug Administration) and other regulatory agencies to look at this.”
The FDA has done so before, helping consumers to delineate boundaries established various food categories. The agency says, for example, it has 300 identity standards in 20 food categories. FDA standards of identity specify minimum and maximum requirements, optional ingredients, and prohibited ingredients. There are rules governing what qualifies as ice cream or a “frozen dessert.” The rules over “bottled water” has six categories.
The dairy industry has had objected to non-dairy products bearing “milk” in their labeling, with soy milk attracting much of its attention.
Cauliflower rice is a relative newcomer, but it’s attracting the same type of attention.
“I think if any regulation would happen, it should be in truth in labeling standards,” said Gina Nucci, director of healthy culinary innovation with Salinas, Calif.-based Mann Packing Co., which sells Cauli-Rice Curry in its Nourish Bowls line.
Other recent additions to the produce aisle include Santa Maria, Calif., Gold Coast Packing Inc.’s, Caulifornia Snow riced cauliflower, and Green Giant Fresh’s Cauliflower Crumbles line with a Fried Rice blend.
The rice lobby says the term “cauliflower rice” is potentially confusing to consumers, but such “call-outs” can be even more confusion to shoppers, Nucci said.
“I don’t think using ‘cauliflower rice’ is confusing to consumers in reference to rice, since consumers are using cauliflower as a replacement for rice-style dishes,” she said.
Shoppers who want “cauliflower rice” are looking for rice substitutes, Nucci noted.
It’s a credit to the industry to give them what they want, she said.
“I think produce companies who grow cauliflower or process cauliflower are innovating to meet the increased demand for ready-to-eat products that limits the prep time for popular recipes using cauliflower,” Nucci said. “If you look online, you’ll see videos showing a food processor chopping up the cauliflower into bits.”
Consumers are smart and won’t mistake broccoli bits for rice any more than they would mistake “milk” made from soybeans for the kind from cows, Nucci said.
“And no one would be complaining if they didn’t see sales declining or shelf space disappearing,” she said.
Nucci said Canada prohibits to term “milk” from non-dairy beverage labels made from almonds, soy and coconuts, “but sales are still booming, since consumers are looking for those alternatives.”
As for further label regulations?
 “I think FDA has more important work to manage,” she said, citing the Food Safety Modernization Act as one example.
Credit cauliflower marketers for coming up with a rice alternative for consumer who want it, said Michele Simon, executive director of the San Francisco-based Plant Based Foods Association.
“This is capitalism at its finest,” said Simon, whose year-old group representing plant-based foods, has 88 members.
There’s no chance of consumer confusion over the tiny bits of cauliflower.
“It says it’s cauliflower,” she said. “I don’t see that’s a problem at all. The rice industry seems to think that is. Consumers these days are looking for all kinds of interesting, innovative alternatives for foods they perceive as not healthy. Eating a lot of white rice may not be healthy, so cauliflower has come along to capitalize, I guess, to lower carb intake.”
The rice industry needn’t be concerned, Simon said.
“It’s kind of silly for the rice lobby to be worried; it still has its place on the plate,” she said.