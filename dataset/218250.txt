A broad group of stakeholders in the food system joined together to create a new coalition to advocate for America’s involvement in free trade agreements, specifically the North American Free Trade Agreement (NAFTA).
Stakeholders involved in the new organization, called Americans for Farmers and Families, range from the American Farm Bureau Federation to Walmart.
“Farm Belt voters supported President Trump by a three-to-one margin in the last election and they are counting on President Trump to improve NAFTA in the modernization negotiations,” said John Bode, president and CEO of the Corn Refiners Association and a member of AFF’s leadership committee. “It’s not an exaggeration to say many farmers are still farming today because of NAFTA. We know that President Trump has a lot of experience negotiating good deals. We support him in updating and improving NAFTA.”
In his speech at the American Farm Bureau Federation Convention, President Trump said that he plans to use NAFTA to help farmers and manufacturers do better.
“To level the playing field for our great American exporters, our farmers and ranchers, as well as our manufacturers, we're reviewing all of our trade agreements to make sure that they are fair and reciprocal,” he said. “On NAFTA, I am working very hard to get a better deal for our country and for our farmers and for our manufacturers. It's under negotiation as we speak. It's not the easiest negotiation, but we're going to make it fair for people again.”
Since being signed in 1994, NAFTA has been attributed to boosting the economy, creating jobs and helping Mexico and Canada become the United States’ largest trading partners.
Here’s a list of groups involved in the coalition:

American Bakers Association
American Farm Bureau Federation
American Frozen Food Institute
American Fruit and Vegetable Processors and Growers Coalition
American Peanut Product Manufacturers, Inc.
American Soybean Association
Association of American Railroads
Corn Refiners Association
Distilled Spirits Council
Food Marketing Institute
Fresh Produce Association of the Americas
Global Cold Chain Alliance
Grocery Manufacturers Association
Indiana Dairy Producers
International Dairy Foods Association
Midwest Food Products Association
Missouri Rice Research and Merchandising Council
National Association of State Departments of Agriculture
National Corn Growers Association
National Grain and Feed Association
National Grocers Association
National Milk Producers Federation
National Oilseed Processors Association
National Pork Producers Council
National Renderers Association
National Restaurant Association
National Turkey Federation
North American Millers Association
Retail Industry Leaders Association
SNAC International
Sweetener Users Association
Texas Rice Council
The Hardwood Federation
The National Association of Wheat Growers
US Apple Association
US Rice Producers Association
USA Rice
Walmart
Wisconsin Potato & Vegetable Growers Association