Amid Trump visit, it's business as usual for border towns
Amid Trump visit, it's business as usual for border towns

By ELLIOT SPAGATAssociated Press
The Associated Press

CALEXICO, Calif.




CALEXICO, Calif. (AP) — The daily commute from Mexico to California farms is the same as it was before Donald Trump became president. Hundreds of Mexicans cross the border and line the sidewalks of Calexico's tiny downtown by 4 a.m., napping on cardboard sheets and blankets or sipping coffee from a 24-hour doughnut shop until buses leave for the fields.
For decades, cross-border commuters have picked lettuce, carrots, broccoli, onions, cauliflower and other vegetables that make California's Imperial Valley "America's Salad Bowl" from December through March. As Trump visits the border Tuesday, the harvest is a reminder of how little has changed despite heated immigration rhetoric in Washington.
Trump will inspect eight prototypes for a future 30-foot border wall that were built in San Diego last fall. He made a "big, beautiful wall" a centerpiece of his campaign and said Mexico would pay for it.
But border barriers extend the same 654 miles (1,046 kilometers) they did under President Barack Obama and so far Trump hasn't gotten Mexico or Congress to pay for a new wall.
Trump also pledged to expand the Border Patrol by 5,000 agents, but staffing fell during his first year in office farther below a congressional mandate because the government has been unable to keep pace with attrition and retirements. There were 19,437 agents at the end of September, down from 19,828 a year earlier.
In Tijuana, tens of thousands of commuters still line up weekday mornings for San Diego at the nation's busiest border crossing, some for jobs in landscaping, housekeeping, hotel maids and shipyard maintenance. The vast majority are U.S. citizens and legal residents or holders of "border crossing cards" that are given to millions of Mexicans in border areas for short visits. The border crossing cards do not include work authorization but some break the rules.
Even concern about Trump's threat to end the North American Free Trade Agreement is tempered by awareness that border economies have been integrated for decades. Mexican "maquiladora" plants, which assemble duty-free raw materials for export to the U.S., have made televisions, medical supplies and other goods since the 1960s.
"How do you separate twins that are joined at the hip?" said Paola Avila, chairwoman of the Border Trade Alliance, a group that includes local governments and business chambers. "Our business relationships will continue to grow regardless of what happens with NAFTA."
Workers in the Mexicali area rise about 1 a.m., carpool to the border crossing and wait about an hour to reach Calexico's portico-covered sidewalks by 4 a.m. Some beat the border bottleneck by crossing at midnight to sleep in their cars in Calexico, a city of 40,000 about 120 miles (192 kilometers) east of San Diego.
Fewer workers make the trek now than 20 and 30 years ago. But not because of Trump.
Steve Scaroni, one of Imperial Valley's largest labor contractors, blames the drop on lack of interest among younger Mexicans, which has forced him to rely increasingly on short-term farmworker visas known as H-2As.
"We have a saying that no one is raising their kids to be farmworkers," said Scaroni, 55, a third-generation grower and one of Imperial Valley's largest labor contractors. Last week, he had two or three buses of workers leaving Calexico before dawn, compared to 15 to 20 buses during the 1980s and 1990s.
Crop pickers at Scaroni's Fresh Harvest Inc. make $13.18 an hour but H-2As bring his cost to $20 to $30 an hour because he must pay for round-trip transportation, sometimes to southern Mexico, and housing. The daily border commuters from Mexicali cost only $16 to $18 after overhead.
Scaroni's main objective is to expand the H-2A visa program, which covered about 165,000 workers in 2016.  On his annual visit to Washington in February to meet members of Congress and other officials, he decided within two hours that nothing changed under Trump.
"Washington is not going to fix anything," he said. "You've got too many people - lobbyists, politicians, attorneys - who make money off the dysfunction. They make money off of not solving problems. They just keep talking about it."
Jose Angel Valenzuela, who owns a house in Mexicali and is working his second harvest in Imperial Valley, earns more picking cabbage in an hour than he did in a day at a factory in Mexico. He doesn't pay much attention to news and isn't following developments on the border wall.
"We're doing very well," he said as workers passed around beef tacos during a break. "We haven't seen any noticeable change."
Jack Vessey, whose family farms about 10,000 acres in Imperial Valley, relies on border commuters for about half of his workforce. Imperial has only 175,000 people and Mexicali has about 1 million, making Mexico an obvious labor pool.
Vessey, 42, said he has seen no change on the border and doesn't expect much. He figures 10 percent of Congress embraces open immigration policies, another 10 percent oppose them and the other 80 percent don't want to touch it because their voters are too divided.
"It's like banging your head against the wall," he said.