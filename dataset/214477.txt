A judge on Tuesday delayed the trial of Nevada rancher Cliven Bundy for a week to give the government time to determine whether recordings exist from an FBI surveillance camera positioned near the family’s ranch before an armed standoff in 2014.
Opening statements in the trial, expected to last through February, had been scheduled to begin Tuesday, but U.S. District Judge Gloria Navarro granted the delay after attorneys for Bundy and three other defendants successfully argued they needed any such video to defend their clients.
Navarro expressed skepticism that the camera feed existed but no recordings were made.
“If it has potentially useful information, then the defense is entitled to it,” Navarro said. “I‘m not convinced that it doesn’t exist.”
The standoff against federal agents became a rallying point for militia groups challenging U.S. government authority in the American West.
Bundy, two of his sons and a third follower are accused of conspiracy, assault, firearms offenses and other charges in the latest of several trials stemming from the confrontation near Bunkerville, Nevada, 75 miles (120 km) northeast of Las Vegas.
The revolt was sparked by the court-ordered roundup of Bundy’s cattle by government agents over his refusal to pay fees required to graze the herd on federal land.
Hundreds of supporters, many heavily armed, rallied to Bundy’s cause demanding that his livestock be returned. Outnumbered law enforcement officers ultimately retreated rather than risk bloodshed.
Bundy’s son Ryan Bundy, who is representing himself at trial, raised the issue of possible surveillance camera recordings in a motion several weeks ago.
“They may not be aware of the recording, but that doesn’t mean it does not exist,” he said Tuesday.
Acting U.S. Attorney Steven Myhre said he knows of no recording by the camera and it was common for such cameras to be used without making recordings.
“We have turned over all video surveillance that the government is aware of or in possession of,” Myhre said.
The 2014 face-off was a precursor to Cliven Bundy’s two sons leading an armed six-week occupation of a federal wildlife center in Oregon two years later.
Defense lawyers have generally argued the Bunkerville defendants were exercising constitutionally protected rights to assembly and to bear arms, casting the showdown as a patriotic act of civil disobedience against government overreach.
Prosecutors have argued armed gunmen were using force and intimidation to defy the rule of law.
Standing trial with Cliven Bundy, 71, are sons, Ammon and Ryan, and a fourth defendant, Ryan Payne, a Montana resident linked by prosecutors to a militia group called Operation Mutual Aid.