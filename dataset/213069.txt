The bulk of Chicago Mercantile Exchange lean hogs finished in negative territory, pressured by profit-taking after contracts peaked at new highs earlier in the session, said traders. They said investors sold deferred months and bought the June contract in a trading strategy known as bull spreads. 
June closed up 0.050 cent per pound to 80.200 cents. July finished 0.175 cent lower at 79.850 cents, and August down 0.050 cent to 79.850 cents. 
Market participants resisted buying futures knowing packers require fewer hogs with plants scheduled to close on Monday, a trader said. 
He said grocers are buying less pork as they wait to see how much product sold over the three-day holiday weekend. 
However, Archer Financial Services broker Dennis Smith expects cash hog and wholesale pork values to soon move higher as supplies decline seasonally.
CME Live Cattle Retreat Following Initial Cash Prices
CME live cattle contracts slumped on Wednesday, pulled down by technical selling and lower preliminary cash prices, said traders. 
June closed 0.950 cent per pound lower at 122.125 cents. August closed 1.400 cents lower at 119.925 cents, and below the 10-day moving average of 120.130 cents. 
Wednesday morning's Fed Cattle Exchange sale yielded an average top price of $132.54 per cwt, down for last week's $135.16 average high. Market-ready, or cash, cattle in Texas and Kansas later trickled in at $132 per cwt, compared to $133 to $134 in the region a week ago, said feedlot sources. Remaining feedlots in the U.S. Plains held out for at least $136. 
The Memorial Day holiday-shortened workweek reduced the number of animals needed by packers, despite their good margins and tight cattle supplies, traders and analysts said. 
They said beef demand may suffer when grocers begin featuring more pork at the conclusion of National Beef Month in May. 
Wednesday morning's average wholesale beef price was up 18 cents per cwt to $245.92 from Tuesday. Select cuts fell 82 cents to $220.35, the U.S. Department of Agriculture said. 
The average beef packer margins on Wednesday were a positive $142.10 per head, up from a positive $121.65 last week, as calculated by HedgersEdge.com. 
Some investors adjusted positions in advance of Friday's USDA monthly Cattle-On-Feed report. 
Technical selling and live cattle futures liquidation sank CME feeder cattle. 
May feeders, which will expire on Thursday, settled down 0.250 cents per pound at 144.000 cents. Most actively-traded August ended 2.825 cents lower at 148.675 cents.