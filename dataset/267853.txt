The Latest: Agreement reached on medical pot bill
The Latest: Agreement reached on medical pot bill

The Associated Press

ANNAPOLIS, Md.




ANNAPOLIS, Md. (AP) — The Latest on developments in the Maryland General Assembly (all times local):
4:22 p.m.
House lawmakers have approved a measure aimed at increasing diversity in Maryland's medical marijuana industry.
The legislation approved Saturday on a 118-to-15 vote increases the number of grower licenses from 15 to 22.
Two of the new licenses are earmarked for two companies that sued over the licensing process. A third is earmarked for a black-owned company that already has been awarded a processor license.
Under the legislation, preferences for the four remaining new licenses will be granted to minority-owned companies.
The legislation, which awaits Senate approval, also increases the number of marijuana processor licenses from the 15 currently authorized to 28.
___
1:13 p.m.
A Maryland House panel has passed a measure to improve school safety.
The bill approved Saturday by the House Ways and Means Committee would require public high schools to establish plans for the upcoming school year to have either school resource officers or adequate law enforcement coverage.
Other schools, such as middle and elementary schools, would need to have the plans in place for the school year that begins in 2019.
The committee also amended a Senate bill to include an additional $10 million a year to help pay for the added school security in future years.
The bill now goes to the House floor.
Saturday's House session was convened as the General Assembly prepares to end its 90-day legislative session Monday at midnight.