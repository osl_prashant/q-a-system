BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Thu.Wed.No. 2 Soft wheat4.69¾4.79¾No. 1 Yellow soybeans10.15¾10.07¼No. 2 Yellow Corn3.66¾e3.68¾eNo. 2 Yellow Corn3.80¾p3.82¾p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted