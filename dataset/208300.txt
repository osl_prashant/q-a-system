Anemic grain prices continue to weigh heavily on grain farmers perception about the economy. Purdue University and the CME Group released its monthly Ag Economy Barometer. The economic indicator dipped to 132 points, seven points below the July reading. While the sentiment index dropped, it's still stronger than a year ago when the index was sitting at just 95 points.The index is based on a monthly survey of 400 agricultural producers from across the U.S.
According to the Purdue economists who conduct the survey, the change in sentiment in August is largely driven by a decline in the Index of Current Conditions, one of the sub-indices of the overall barometer. That index saw a 20 point drop compared to a month earlier. That July reading peaked at 142 points. The monthly drop was not unexpected given the downtrend in grain and oilseed prices during the survey period.
 
 

<!--
if("undefined"==typeof window.datawrapper)window.datawrapper={};window.datawrapper["RbK2T"]={},window.datawrapper["RbK2T"].embedDeltas={"100":464,"200":425,"300":400,"400":400,"500":400,"600":400,"700":400,"800":400,"900":400,"1000":400},window.datawrapper["RbK2T"].iframe=document.getElementById("datawrapper-chart-RbK2T"),window.datawrapper["RbK2T"].iframe.style.height=window.datawrapper["RbK2T"].embedDeltas[Math.min(1e3,Math.max(100*Math.floor(window.datawrapper["RbK2T"].iframe.offsetWidth/100),100))] "px",window.addEventListener("message",function(a){if("undefined"!=typeof a.data["datawrapper-height"])for(var b in a.data["datawrapper-height"])if("RbK2T"==b)window.datawrapper["RbK2T"].iframe.style.height=a.data["datawrapper-height"][b] "px"});
//-->

 

Despite the decline in the Index of Current Conditions, the Purdue/CME Ag Barometer seems to indicate that agricultural producers remain more upbeat about prospects for their own farms as well as the agricultural sector than they were at this time last year.