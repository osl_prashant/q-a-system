Chicago Mercantile Exchange live cattle futures on Tuesday bowed to fund liquidation that wiped out Monday's sharp gains, including the August contract's settlement up its 3.000-cent per pound daily price limit, traders said.They attributed further selling on lower wholesale beef values that could weigh on this week's cash prices.
June, which will expire on Friday, closed 1.875 cents per pound lower at 119.600 cents. Most actively traded August finished 3.250 cents lower at 115.025 cents, and below its 10-day moving average of 116.375 cents.
CME live cattle on Wednesday will resume its normal 3.000-cent limit after failing to settle at the expanded 4.500-cent limit on Tuesday.
Last week, market-ready, or cash, cattle in the U.S. Plains moved at $118 to $123 per cwt.
Investors await Wednesday morning's Fed Cattle Exchange sale of 2,554 animals. A week ago there, a small number of cattle at the exchange brought $123 per cwt.
Bearish investors expect packers to pay less for market-ready, or cash, cattle than last week given seasonally sluggish wholesale beef demand.
Packers will also need fewer cattle as plants prepare to close during the U.S. Independence Day holiday.
Market bulls believe extremely profitable packer margins, and fewer cattle for sale than last week, might underpin some cash prices.
CME feeder cattle contracts reversed Monday's 4.500-cent limit-up close pressured by technical selling, steady-to-lower cash feeder cattle prices and heavy live cattle futures losses.
Feeder cattle's limit on Monday will return to 4.500-cent price limit after not settling at Tuesday's expanded limit of 6.750 cents.
August feeders ended 4.575 cents per pound lower at 144.875 cents. 
Hogs Finish Weaker
Spillover live cattle market pressure and caution before the U.S. Department of Agriculture's quarterly hog report on Thursday pressured CME lean hog futures, said traders.
They said futures discounts to the exchange's hog index for June 23 at 90.62 cents mitigated market losses.
July ended down 0.550 cent per pound to 86.475 cents, and August down 0.125 cent to 78.550 cents.
Processors paid slightly less for hogs before holiday plant closures, while retailers bought pork for holiday grilling advertisements, said traders and analysts.