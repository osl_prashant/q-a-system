To promote Florida strawberries, Wish Farms has launched a consumer e-mail campaign and rolled out a festive recipe video.
Consumers who have signed up for the company’s e-mail newsletter Berry Lovers are entered to win $100 every week, according to a news release.
“We are looking forward to engaging our Berry Lovers,” Amber Maloney, director of marketing for Wish Farms, said in the release. 
“This is an exciting way that we can thank our longtime Wish Farms fans, and welcome newcomers to the program.”
New and existing subscribers qualify, according to the release.
Berry Lovers offers seasonal updates, contests and recipes. A new recipe this year is Dipped Strawberry Rudolph.
Wish Farms, Plant City, Fla., grows and markets strawberries from nearly 2,000 acres in Florida. Peak production is in February.