Overnight temperatures dipping into the low 20s March 15-16 hit some of Georgia's berry and peach crops, although state agriculture officials weren't yet sure how extensive the damage was.
The state's $400 million blueberry crop appeared to be hardest hit by the hard freeze, said Georgia Agriculture Commissioner Gary Black.
"I'm not so encouraged by some of the blueberry reports we're hearing," Black said while surveying a 140-acre peach orchard that lost an estimated half of its blossoms in 21-degree overnight cold March 16 at Jaemor Farms in Alto, Ga.
In Alma, Ga., which Black described as ground zero for Georgia's blueberry production, he said one producer had conveyed fear that temperatures ranging between 22 and 25 degrees killed all rabbiteye and a significant portion of highbush blueberry crops.
"Everyone that had freeze protection, those systems have been running all night," Black said. "We've taken many March 'winter wonderland' pictures."
Black said he and his staff would travel across the state March 17 to gather further information on damages.
"We'll probably know a little more about percentages and more depth (then)," he said.
The damage notwithstanding, Black said he was amazed that as many peach blossoms at Jaemor had survived.
"Their peach crop was 90% bloom, and they had 21-degree weather last night," he said. "The Lord had his hand over these trees. There's probably half a crop left. It's pretty amazing."
For the short term, conditions were forecast to moderate somewhat, but Drew Echols, owner of Jaemor Farms, remained anxious.
"We have a couple more weeks of potential cold and don't have a lot left to gamble with," Echols said. "I feel blessed to have what I have left here."
The peach crop still could have a dramatic swing, either way, Echols said.
"The thing about it is, I may have a 75% crop; I may have a 25% crop. It's gonna take a day or two to get a complete assessment," he said. "I feel comfortable that we have at least a 50% crop."
Black said there's still ample hope for a successful peach crop, even if it's a bit shorter this year.
"Generally speaking, right now it appears we're still going to have a marketable crop, but we still have some vulnerable days ahead of us," Black said.
On the other hand, Echols said he saw significant damage on his 18 acres of strawberries.
"We had them under blankets, and it didn't work out," he said. "They actually got hammered yesterday morning (March 15) with really cold wind."
Black said Georgia's strawberry production was ahead of schedule, too.
"We have a lot of dead fruit," he said.
Strawberry production can recover, however, while peach production, once damaged, is out for the year, Black noted.
Still, he said, "This strawberry production will look significantly different."
Black said there was "mixed information" coming in on Georgia's vegetable crops.
"I've heard anecdotally from one producer (they've) been hit pretty hard, but another producer, with specialty peppers, they seem to have survived the night," Black said. "But you can't help but believe, with pepper and cucumbers and squash - and we've already got some watermelons out - we've had some damage, for sure."
The Vidalia onion crop appeared to be safe, Black said.
That was the case at Bland Farms in Glennville, Ga., said Delbert Bland, owner.
"In Vidalia, it got only to about 30 degrees - that's not a problem," Bland said. "The wind blew, so we didn't have much frost. The only thing you have to worry about this time is frost getting on and burning them. It didn't settle, so we shouldn't have a problem. So far, so good."