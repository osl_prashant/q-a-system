Southeast Produce Council members are proud of their association's Southern Exposure show and the opportunities it affords the produce industry.
"There's an intimacy to the show by having a limited amount of booths, so we're able to really engage in meaningful conversations in a relaxed atmosphere," said Harold Paivarinta, an SEPC board of directors member and senior director of sales and business development for Red Sun Farms, Kingsville, Ontario.
"We love the fact that everybody is in a 10x10-foot booth. It enables a multinational company and a small, regional grower to be equally represented."

Connection

Industry members agree that the show facilitates meaningful interaction.
Diana McClean, director of marketing for Ocean Mist Farms, Castroville, Calif., said Southern Exposure represents an ideal format for her company to talk with retail partners.
"It is an intimate setting with everyone having the same booth size, so it's not about the size of your booth or it's not a tremendous expense to bring your equipment across the country," she said.
"We have great opportunity to engage with the retailers either through the exhibit or the other many functions of the weekend."
Andrew Scott, vice president of business development and marketing for Nickey Gregory Co. LLC, Atlanta, said the main benefits he and his company derive from the Southern Exposure show are "having the chance to meet new customers and vendors while we are exhibiting, as well as spending time with our current customers and vendor base. We have a lot of our vendors attending Southern Exposure."
Sheila Carden, retail merchandiser for the Orlando, Fla.-based National Watermelon Promotion Board, called the SEPC a "terrific" organization.
"The networking is awesome," she said. "It's easy to meet people, and there are many instances do so, such as the golf tournament, exhibits or at the parties on Friday and Saturday nights."

Location

Besides the intimate and relaxed nature of Southern Exposure, its location in central Florida in late winter is a drawing card for many attendees.
Paivarinta indicated he will gladly make the 1,180-mile trek from Canada to Orlando.
"It's probably the premier regional produce show," he said.
"That's due to the location - I'm looking out my window at snow on the ground - and its timing. It's a 'who's who' of retail and foodservice attendees as well. The response we get from retail and foodservice buyers is incredible."
"Florida, in early March, features really great weather, probably the best weather in the U.S. at that time," said David Sherrod, executive director of the Millen, Ga.-based SEPC.
Scott agreed.
"Being in central Florida, especially in Orlando at a beautiful Disney property, is what I am looking forward to the most," he said.
"This will be the best Southern Exposure yet, led by David Sherrod and his hard-working team. I am also looking forward to the golf tournament.
"Southern Exposure is unquestionably the best produce show in the industry for its quality of attendees, locations each year in Florida and for the SEPC people behind the scenes that make Southern Exposure so successful."