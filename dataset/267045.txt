US proposes tariffs on $50 billion in Chinese imports
US proposes tariffs on $50 billion in Chinese imports

By PAUL WISEMANAP Economics Writer
The Associated Press

WASHINGTON




WASHINGTON (AP) — The Trump administration on Tuesday escalated its aggressive actions on trade by proposing 25 percent tariffs on $50 billion in Chinese imports to protest Beijing's policies that require foreign companies to hand over their technology.
China immediately threatened to retaliate against the new tariffs target high-tech industries that Beijing has been nurturing, from advanced manufacturing and aerospace to information technology and robotics.
The Office of the U.S. Trade Representative issued a list targeting 1,300 Chinese products, including industrial robots and telecommunications equipment. The suggested tariffs wouldn't take effect right away: A public comment period will last until May 11, and a hearing on the tariffs is set for May 15. Companies and consumers will have the opportunity to lobby to have some products taken off the list or have others added.
The latest U.S. move risks heightening trade tensions with China, which on Monday had slapped taxes on $3 billion in U.S. products in response to earlier U.S. tariffs on steel and aluminum imports.
"China's going to be compelled to lash back," warned Philip Levy, a senior fellow at the Chicago Council on Global Affairs and an economic adviser to President George W. Bush.
Early Wednesday in Beijing, China's Commerce Ministry said it "strongly condemns and firmly opposes" the proposed U.S. tariffs and warned of retaliatory action.
"We will prepare equal measures for U.S. products with the same scale" according to regulations in Chinese trade law, a ministry spokesman said in comments carried by the official Xinhua News Agency.
The U.S. sanctions are intended to punish China for deploying strong-arm tactics in its drive to become a global technology power. These include pressuring American companies to share technology to gain access to the Chinese market, forcing U.S. firms to license their technology in China on unfavorable terms and even hacking into U.S. companies' computers to steal trade secrets.
The administration sought to draw up the list of targeted Chinese goods in a way that might limit the impact of the tariffs — a tax on imports — on American consumers while hitting Chinese imports that benefit from Beijing's sharp-elbowed tech policies. But some critics warned that Americans will end up being hurt.
"If you're hitting $50 billion in trade, you're inevitably going to hurt somebody, and somebody is going to complain," said Rod Hunter, a former economic official at the National Security Council and now a partner at Baker & McKenzie LLP.
Kathy Bostjancic of Oxford Economics predicted that the tariffs "would have just a marginal impact on the U.S. economy" — unless they spark "a tit-for-tat retaliation that results in a broad-based global trade war."
Representatives of American business, which have complained for years that China has pilfered U.S. technology and discriminated against U.S. companies, were nevertheless critical of the administration's latest action.
"Unilateral tariffs may do more harm than good and do little to address the problems in China's (intellectual property) and tech transfer policies," said John Frisbie, president of the U.S.-China Business Council.
Even some technology groups that are contending directly with Chinese competition expressed misgivings.
"The Trump administration is right to push back against China's abuse of economic and trade policy," said Robert Atkinson, president of the Information Technology and Innovation Foundation think tank.
However, he said the proposed U.S. tariffs "would hurt companies in the U.S. by raising the prices and reducing consumption of the capital equipment they rely on to produce their goods and services."
"The focus should be on things that will create the most leverage over China without raising prices and dampening investment in the kinds of machinery, equipment, and other technology that drives innovation and productivity across the economy," Atkinson added.
The United States has become increasingly frustrated with China's aggressive efforts to overtake American technological supremacy. And many have argued that Washington needed to respond aggressively.
"The Chinese are bad trading partners because they steal intellectual property," said Derek Scissors, a China specialist at the conservative American Enterprise Institute.
In January, a federal court in Wisconsin convicted a Chinese manufacturer of wind turbines, Sinovel Wind Group, of stealing trade secrets from the American company AMSC and nearly putting it out of business.
And in 2014, a Pennsylvania grand jury indicted five officers in the Chinese People's Liberation Army on charges of hacking into the computers of Westinghouse, US Steel and other major American companies to steal information that would benefit their Chinese competitors.
To target China, Trump dusted off a Cold War weapon for trade disputes: Section 301 of the U.S. Trade Act of 1974, which lets the president unilaterally impose tariffs. It was meant for a world in which much of global commerce wasn't covered by trade agreements. With the arrival in 1995 of the Geneva-based World Trade Organization, Section 301 largely faded from use.
Dean Pinkert of the law firm Hughes Hubbard & Reed, found it reassuring that the administration didn't completely bypass the WTO: As part of its complaint, the U.S. is bringing a WTO case against Chinese licensing policies that put U.S. companies at a disadvantage.
China has been urging the United States to seek a negotiated solution and warning that it would retaliate against any trade sanctions. Beijing could counterpunch by targeting American businesses that depend on the Chinese market: Aircraft manufacturer Boeing, for instance, or American soybean farmers, who send nearly 60 percent of their exports to China.
Rural America has been especially worried about the risk of a trade war. Farmers are especially vulnerable targets in trade spats because they rely so much on foreign sales.
"Beijing right now is trying to motivate US stakeholders to press the Trump Administration to enter into direct negotiations with China and reach a settlement before tariffs are imposed," the Eurasia Group consultancy said in a research note.
"The next couple of weeks will be very interesting," says Kristin Duncanson, a soybean, corn and hog farmer in Mapleton, Minnesota.
____
AP writer Gillian Wong in Beijing contributed to this report.
____
On Twitter follow Wiseman at https://twitter.com/paulwisemanAP