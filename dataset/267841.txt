Area organic dairy farmers struggle with low milk prices
Area organic dairy farmers struggle with low milk prices

By JULIA-GRACE SANDERSSkagit Valley Herald
The Associated Press

MOUNT VERNON, Wash.




MOUNT VERNON, Wash. (AP) — Just off Cook Road east of Interstate 5 sits an unused 80-acre plot of land with the outline of a 49,000-square-foot building drawn in gravel.
The plot was meant to become a state-of-the-art expansion for the Dykstra organic dairy farm, complete with an automated milking system.
"Now it's a big, expensive parking lot," said third-generation dairy farmer Charlie Dykstra.
The construction was put on hold indefinitely after the price of organic milk tanked during the past year and a half, Dykstra said.
The Dykstra farm isn't the only organic dairy farm struggling.
"You won't see a single dairy doing anything crazy right now," said Dykstra, who manages the farm with his father and brother.
Conventional dairy farms have been struggling for years. Until recently, Dykstra said it seemed organic dairy farms were somewhat shielded from the volatility of the conventional market.
Now, organic dairies are struggling alongside conventional dairies, further adding to the impact on the local agriculture economy.
As of 2014, there were 29 dairy farms operating in Skagit County, down from 52 in the beginning of 2003, according to statistics compiled by the Washington State University Skagit County Extension.
Five of those are organic, according to the U.S. Department of Agriculture.
HARD TIMES
Mount Vernon organic dairy farmer Alan Mesman said the price he's getting for milk from processors has been dropping about a dollar a month for about the past year and a half.
"We went from the mid 30s down the high 20s," Mesman said, referring to the price per 100 pounds of milk (slightly less than 12 gallons). "Things got really bad last spring."
But in stores, consumers don't see the price change, Dykstra said, as the shelf price for organic milk has hardly moved.
Organic dairy farmers attribute the price drop to a surplus of organic milk being produced as more dairy farmers have entered the organic market and consumer preferences have changed.
"It's such a small market that it doesn't take much to oversupply it," said Bow organic dairy farmer Dean Wesen.
Until the past year, Dykstra said his farm had seen a fairly stable milk price compared to the roller-coaster of conventional milk prices.
"If you talk to most organic farmers, the reason they went organic is to get away from the volatility of conventional," said Dykstra, whose farm went organic in 2004.
Even with a largely robotic milking operation, Mesman said he's had to lay off two part-time employees as a result of the drop in the price he can get for milk.
"We're screaming tight on money," he said.
In addition to being affected by the surplus of organic milk, organic dairy farmers are also being hurt by the low price of conventional milk.
That's because organic dairies are given a quota by their processors, and anything over that quota is sold at conventional milk price, which is substantially lower than the price of organic milk.
"We can't even afford to produce that," Dykstra said.
A demand for high-fat dairy products has also thrown dairy farmers for a loop.
"It's a fundamental shift in the market," said Andrew Dykstra, Charlie Dykstra's father. "From a low-fat product to everyone wanting high-fat."
The new demand isn't necessarily a bad thing, Wesen said, but it requires farmers to adapt by making adjustments such as tweaking feed to get more butter fat in their products.
And while milk prices are going down, Mesman said the price of the grain that is fed to the cows is going up.
Charlie Dykstra said he has heard from suppliers that feed could go up as much as 33 percent in the coming months.
"You just have a whole lot of bad things happening at the same time," Mesman said.
Unlike Canada and Western Europe, Mesman said the United States doesn't have central supply management for dairy.
"Other countries keep the price up to make sure farmers can make a living," he said. "There's no real supply management in this country. It's a free market so you just ride the roller-coaster. It's just usually not as bad as this."
Jay Gordon, policy director for the Washington State Dairy Federation, said he believes the organic dairy market will level out. The market gained traction in the 1990s and early 2000s, Gordon said, and now it's settling into the patterns seen in more mature markets.
"This is the first time we've seen organic dairy not just have an uptrend," Gordon said. "I'm optimistic that Organic Valley and the market has internal mechanisms to put supply and demand back in balance."
In Washington, Gordon said much of the price is based on what will be paid by Organic Valley, a cooperative to which all the organic dairy farms in Skagit County sell their milk.
"But even Organic Valley — significant as it is — doesn't exist in a bubble," Gordon said.
Organic Valley Vice President of Farmer Affairs Travis Forgues said four years ago the co-op didn't have enough organic milk, so they invited conventional producers to go organic.
"Last year, many of those farms came into organic," Forgues said in an email. "So suddenly we had a big increase in the supply across the industry. That's what we're dealing with mainly."
For now, Charlie Dykstra said all organic dairies can do is hunker down and wait out the price drop.
It's a familiar tune for Andrew Dykstra, who started working on the family farm when he was in the second grade.
"There's been hard times before," he said. "And here we are."
AN ESSENTIAL ROLE
Dairy farms play an essential role in Skagit Valley agriculture, Andrew Dykstra said, and a threat to the dairy farms could affect crop farmers.
Without organic dairy farms and their need for organic feed, Charlie Dykstra said local organic crop farmers would lose a nutrient-rich crop rotation.
The Dykstras use about 900 acres, he said, and they're constantly trading that land with crop farmers.
Dairy farms add nutrients to the ground, Gordon said, which is good for both organic and conventional crop production.
"The dairies are supporting the grains, which support operations like the Bread Lab and (Cairnspring Mills), which in turn supports the dairies by using their land to grow grains," Gordon said.
Even if a dairy farm doesn't go out of business, Gordon said the dairy farmers' financial troubles can affect crop farmers.
"(Dairy) farmers have to start asking for grass or barley to feed the cows instead of organic corn," Gordon said, which brings in less money for crop farmers.
Without dairy farms, Charlie Dykstra said crop farmers would also have to buy their own fertilizer.
"Personally, I think organic vegetables would be about twice as expensive in the stores if dairy manure wasn't used as fertilizer," Charlie Dykstra said.
LOOKING FORWARD
Despite the tough times, Andrew Dykstra said there's hope for the county's organic dairy industry.
Organic Valley has been able to sell more of the milk they get from farmers at an organic premium rather than having to sell excess product at conventional price.
Andrew Dykstra said that's a sign the market is looking up.
In the midst of low milk prices for both conventional and organic dairies, Andrew Dykstra said there is a fundamental difference that makes organic dairy farming more favorable than conventional. The Organic Valley board of directors is made up of dairy farmers, making them more conscious of how prices affect farms.
While fluid milk sales are more or less flat, Forgues said Organic Valley is seeing steady growth in other dairy products such as yogurt and cheese.
Forgues said that as millennials start having children, he expects the demand for organic dairy products to increase.
"Many millennials grew up with organic and will want their kids to have the same quality dairy, produced by family farms in their region," he said.
Charlie Dykstra said the coming year will be a defining time for the organic dairy industry.
"2018 is going to be the year that will tell," he said. "Only the strongest will be able to survive the current milk prices."
___
Information from: Skagit Valley Herald, http://www.skagitvalleyherald.com