"Anyone who says saccharin is injurious to health is an idiot."
 
President Theodore Roosevelt said that in 1911. He was in a heated discussion with Harvey Wiley, the first commissioner of the Food and Drug Administration.
 
Saccharin, an artificial sweetener that is 300-400 times sweeter than table sugar, was discovered by a chemist in 1879. It was commercialized a couple of years later and became an inexpensive substitute for sugar. 
 
The U.S. government began investigating saccharin in 1907. Four years later the FDA ruled that food produced with saccharin must be labeled as adulterated (impure, unsafe or unwholesome).  
 
President Roosevelt regularly consumed saccharin to help control his weight. In 1912, following the advice of a board appointed by Roosevelt, the U.S. government made a new ruling that saccharin was not harmful to human health.
 
Although ruled to be not harmful, the government experts said excessive use of saccharin could cause "gastric disturbances."  
 
The confusing ruling prohibited use of saccharin in food unless it was labeled to be used by people whose physicians prohibited them from eating sugar.
 
More than a half century later, the FDA banned saccharin in 1977. Researchers had found that some rats fed large doses of saccharin developed bladder cancer.
 
Public opposition to the ban led to a compromise. Saccharin would be allowed but it must carry a label with the warning: "Use of this product may be hazardous to your health. This product contains saccharin, which has been determined to cause cancer in rats."
 
By 2000, other researchers had found that, unlike rats, humans are not susceptible to cancer from saccharin. It was time for another president to step in.
 
In December 2000 President Bill Clinton signed legislation that removed the saccharin warning label.
 
The issue of presidential involvement in food labeling is not new and is now being revisited.
 
In July, just before leaving for summer vacation, Congress passed a biotech food labeling bill and sent it to President Barack Obama, who signed it Aug. 1. 
 
The act of Congress supersedes the GMO labeling law in Vermont - the only state that had passed biotech-labeling legislation.  
 
Several food companies had begun to use the Vermont labels "produced with genetic engineering" and "partially produced with genetic engineering." They will not be required to use those labels in Vermont or any other state now that Obama has signed the new law.
 
The federal legislation specifies that USDA will develop new labeling standards. The approved labels are likely to include sources for more information including toll-free numbers and quick-response codes, but will not require that the words "genetic engineering" appear on any part of the packaging.
 
The law is a compromise between those who advocated voluntary labeling only and those who wanted a "badge of shame" placed on products made with biotechnology.
 
We know that biotech foods are not a threat to human health. There have been no studies linking biotech to cancer.  
 
We also know that biotech foods are not a threat to the environment. In fact, biotech crops that require less pesticide application reduce environmental risks.
 
We know these facts because of a report released in May by the National Academy of Sciences. Fifty scientific experts working for two years analyzed 900 reports from 20 years of biotech crop research. They found no evidence of biotechnology causing any harm to people or the environment.
 
Why is biotech labeling an issue if there is no proven risk? For some it is not a scientific issue, it is a political issue.
 
The current political result is the bill Congress placed on President Obama's desk. I doubt he called anyone an idiot like President Roosevelt did regarding saccharin. One reason is that no one in FDA, EPA or USDA has ruled that biotech foods are injurious to health.
 
Joe Guenthner is an emeritus professor of agricultural economics at the University of Idaho.
 
What's your take? Leave a comment and tell us your opinion.