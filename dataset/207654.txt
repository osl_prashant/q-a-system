While news media coverage of the issue is titanic, shopping online for produce is not something “we” do very much of.  
In fact, the 2017 Food Marketing Institute Power of Produce Report shows that 3% of all consumers said they ordered fresh produce online. By generation, the survey said that 6% of Millennials shopped for produce online. That’s a mob compared with Gen X and Boomers; only 2% of those older consumers shopped for produce online, according to the survey. 
 
That 3% overall number for buying produce online compares with 37% of fresh produce consumers who said they had occasionally shopped a farmers’ market, 16% who said they stopped at a roadside stand, 17% who had visited an organic specialty store, 9% who shopped at an ethnic specialty store and 3% who tried meal kit delivery.
 
All in all, these are numbers that shouldn’t cause fresh-oriented food retailers to quake. 
 
A new survey looking at consumers engagement with online grocery shopping has been issued by Gallup, and it also shows smallish numbers of online grocery consumers. 
 
The Gallup report, from the group’s annual Consumption Habits survey, said that 9% of U.S. adults report their household orders groceries online for pickup or delivery at least once a month.
 
That includes 4% who report at least weekly online grocery purchases, according to Gallup.
 
In contrast, nearly all Americans say they shop for groceries “in person” at least monthly. Gallup said 83% of consumers shop for groceries in the traditional way at least weekly.
 
A whopping 84% of Americans polled by Gallup said they have never shopped for groceries online for pickup or delivery. Those who do shop online often do it “in addition to” regular grocery shopping, not “instead of.”
 
So why the buzz about online grocery shopping? 
 
Gallup notes that young consumers are much more likely to shop for groceries online, with 15% of the 18 to 29-year-old set saying they shop online for groceries at least once or twice a month. Only 2% of grandmas and grandpas (65 and above), reported shopping for groceries online at least once or twice a month.
 
So a shift is slowly happening. How fast and how far the trend could go are open questions.
 
From Gallup, a prophetic conclusion:
 
The retail grocery industry is gearing up for a period of rapid change, and major retail grocers have already begun shifting to online ordering for pickup or delivery, as well as in-store dining and on-site food preparation, to position themselves to be on top of, rather than reacting to, pending shifts in consumer behavior. Amazon’s purchase of Whole Foods serves as a reminder that this change may come sooner than some might have expected.
 

 
On a related note, check out this thoughtful piece from wtop.com that addresses why market trends are forcing grocers “to sell an experience” and questions if that approach is sustainable.