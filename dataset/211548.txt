Sales of organic agricultural products soared in 2016, with a 23% increase in from the previous year, according to the U.S. Department of Agriculture.Crops account for 56% of sales in organic agricultural sector, according to the USDA’s National Agricultural Statistics Service, and livestock and poultry and their products account for 44%.
While 2016 sales of milk and eggs were $2.2 billion, produce sales were greater, with $1.6 billion from vegetables and $1.4 billion from fruits and tree nuts.
Overall farm sales of organic agricultural products were $7.6 billion, up from $6.2 billion in 2015.
Overall acreage devoted to organic production increased 15%, to 5 million acres, according to a news release from NASS.Top agriculture categories, in terms of sales, were:
Milk — $1.4 billion, up 18%;
Eggs — $816 million, up 11%
Broiler chickens — $750 million, up 78%;
Apples — $327 million, up 8%; and
Lettuce — $277 million, up 6%
Other top organic crops were strawberries, grapes, tomatoes, corn, potatoes, spinach and mushrooms, according to the release.
California continues to lead in organic agricultural production, with $2.9 billion in certified organic sales, according to the release. That’s 38% of the USDA’s total for all states. California, Wisconsin and New York lead the nation in the number of certified organic farms, at 2,713, 1,276 and 1,059, respectively. The number of certified organic farms rose 11%, to 14,217, according to the release.
“The results of the 2016 Certified Organic Survey show the continued interest and growth in organic foods,” Hubert Hamer, NASS administrator, said in the release. “The survey provides the only comprehensive source of national and state date on certified organic production.”
The survey expanded in 2016 to include fresh and processed information separately for all fruit and berry crops, and grape data by variety.