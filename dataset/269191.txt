E-cigarette brand's 2.6M power units being recalled
E-cigarette brand's 2.6M power units being recalled

The Associated Press

WINSTON-SALEM, N.C.




WINSTON-SALEM, N.C. (AP) — A big cigarette name is recalling the power units used for about 2.6 million electronic cigarettes.
R.J. Reynolds Vapor Co. has issued a nationwide safety recall of all Vuse Vibe power units after consumer reports that batteries malfunctioned and caused the units to overheat and create a fire risk. No injuries have been reported.
R.J. Reynolds Vapor markets the Vuse brand of e-cigarettes. The subsidiary of British American Tobacco says owners should stop using the product or charging the power unit and contact the company to receive a refund.