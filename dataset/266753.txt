Dow dives as much as 750 as China puts tariffs on US goods
Dow dives as much as 750 as China puts tariffs on US goods

By MARLEY JAYAP Markets Writer
The Associated Press

NEW YORK




NEW YORK (AP) — U.S. stocks are tumbling Monday after China officially raised import duties on U.S. pork, apples and other products. It's too soon to call it the beginning of a trade war, but for now, investors aren't sticking around to find out.
Formerly high-flying technology companies like Microsoft and Alphabet fell, and Intel took a steep drop following a report in Bloomberg News that Apple plans to start using its own chips in Mac computers as early as 2020, replacing Intel.
China raised import duties on a $3 billion list of U.S. goods in response to the tariffs on imported steel and aluminum that President Donald Trump ordered last month. The Chinese tariff hit meat producer Tyson Foods hard, but investors are mostly concerned that both countries will take further steps that ultimately harm global commerce and company profits. The Dow Jones industrial average fell as much as 750 points.
Other market leaders like industrial giant Boeing and streaming video service Netflix also slumped. Amazon got a double dose of bad news. On top of the broader market worries, the online retailer has been the target of numerous critical tweets from President Donald Trump over the last few days and investors and investors are selling its stock.
After a month of public negotiations between the U.S. and several other countries, Monday marked the first time another country has formally placed tariffs on U.S. goods in response to the Trump administration's recent trade sanctions. Kate Warne, an investment strategist for Edward Jones, said the step by China is small but significant.
"The fact that a country has actually raised tariffs in retaliation is an important step in the wrong direction," she said. "The tariffs imposed by China today lead to greater worries that we will see escalating tariffs and the possibility of a much bigger impact than investors were anticipating last week. And that could be true for Mexico as well as for China."
The Standard & Poor's 500 index skidded 80 points, or 3 percent, to 2,560 as of 2:30 p.m. Eastern time. The benchmark index is on track for its lowest close since late October.
The Dow lost 694 points, or 2.9 percent, to 23,408. The Nasdaq composite slumped 238 points, or 3.4 percent, to 6,824. The Russell 2000 index of smaller-company stocks fell 43 points, or 2.8 percent, to 1,486.
A bigger dispute looms over Trump's approval of possible higher duties on Chinese goods. There are a number of points of contention between China and Washington, Europe and Japan over a state-led economic model they complain hampers market access, protects Chinese companies and subsidizes exports in violation of Beijing's free-trade commitments.
The price of gold climbed 1.2 percent to $1,343.60 an ounce and silver jumped 2 percent to $16.60 an ounce as some investors took money out of stocks and looked for safer investments.
China raised import duties on a $3 billion list of U.S. goods in response to U.S. tariffs on imported steel and aluminum. Tyson Foods, which could see its sales in China dip as prices rise, slumped $4.42, or 6 percent, to $68.77.
The price of gold climbed $19.60, or 1.5 percent, to $1,346.90 an ounce and silver jumped 40 cents, or 2.5 percent, to $16.67 an ounce as some investors took money out of stocks and looked for safer investments.
Amazon fell another $70.84, or 4.9 percent, to $1,376.50. After peaking at almost $1,600 a share last month, Amazon has slumped with the market recently. Trump repeatedly criticized the company lately over issues including taxes and Amazon's shipping deals with the U.S. Postal Service.
Warne, of Edward Jones, said investors are being cautious for now, but it's not clear if anything will come of Trump's badmouthing the company. "There isn't an agency that goes through Trump's tweets and acts on them," she said.
Despite its recent losses, Amazon stock is still up about 18 percent in 2018. It wasn't the only market favorite to fall out of favor Monday. Intel plunged $4.05, or 7.8 percent, to $48.04.
Microsoft dropped $2.97, or 3.3 percent, to $88.30 and Google's parent company, Alphabet, shed $31.13, or 3 percent, to $1,006.01. Boeing slid $8.25, or 2.5 percent, to $319.63.
Health insurer Humana rose following continued reports Walmart could buy the company or create a new partnership with it. Humana is a major provider of Medicare Advantage coverage for people 65 and older. Humana gained $10.99, or 4.1 percent, to $279.82 and Walmart slid $3.47, or 3.9 percent, to $85.50.
Tesla declined after the electric car maker said Friday that the vehicle in a fatal crash last week in California was operating on Autopilot mode, making it the latest accident to involve a semi-autonomous vehicle. Tesla fell $13, or 4.9 percent, to $253.13.
Bond prices turned higher. The yield on the 10-year Treasury note fell to 2.72 percent from 2.74 percent after a sharp decline last week.
Benchmark U.S. crude lost $1.76, or 2.7 percent, to $63.18 a barrel in New York. Brent crude, used to price international oils, slid $1.52, or 2.2 percent, to $67.82 a barrel in London.
Copper rose 2 cent to $3.05 a pound.
The dollar declined to 105.67 yen from 106.50 yen. The euro edged up to $1.2308 from $1.2306.
Trading in France, Germany and Britain was closed for Easter. Japan's benchmark Nikkei 225 lost 0.3 percent and South Korea's Kospi fell almost 0.1 percent. The Hang Seng in Hong Kong was closed as well.
____
AP Markets Writer Marley Jay can be reached at http://twitter.com/MarleyJayAP . His work can be found at https://apnews.com/search/marley%20jay