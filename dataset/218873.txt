National Mastitis Council (NMC) honored elite dairy producers at its Feb. 2 Awards Luncheon, held in conjunction with the NMC 57th Annual Meeting in Tucson, Ariz. Recognized for outstanding quality milk production through NMC’s National Dairy Quality Awards (NDQA) program, the Platinum winners are: Bailey's Cherry Valley Dairy LLC (Mike, Jean, Brent, Brock and Nelda Bailey), Tomah, Wis.; Country Aire Farms (Tom and Mike Gerrits), Kaukauna, Wis.; Butterwerth Dairy Farm LLC (Larry, Paulette, Jeremy and Paul Werth ), Alpena, Mich.; Tollgate Holsteins (Jim and Karen Davenport), Ancramdale, N.Y.; Folts Farms LLC (Joshua and Virginia Folts), North Collins, N.Y.; and SunRidge Dairy (Mike Siegersma and Adrian and Ryan Kroes), Nampa, Idaho.
In addition to the six Platinum winners, there were 16 Gold and 24 Silver NDQA winners (listed below). These farms were selected from 130 applications submitted for the 2017 awards. Farms were nominated by professionals, such as dairy plant field staff, veterinarians, extension specialists and Dairy Herd Improvement supervisors, who serve the dairy industry.
 
Gold

Don Beattie, Holton, Mich.
Greg Bingham, Weston, Idaho
Hans Breitenmoser, Merrill, Wis.
Ron Brooks, Waupaca, Wis.
Brad, Mark and Larry Crandall, Battle Creek, Mich.
Gordon Dick, McBain, Mich.
Brad and Debby Kartes, West Branch, Mich.
Jerry, Carolyn, Mike, Eric, Jenny, Jon and Brandy Leahy, Brandon, Wis.
Conrad, Lisa and Brian Liebergen, Greenleaf, Wis.
Jeff Orr and Paige Mier, Prescott, Mich.
David and Mark Miller, Millersburg, Ohio
Brian and Heather Richardson, Warren, Mass.
Brent Simon, Westphalia, Mich.
University of Connecticut Dairy, Storrs, Conn.
Ken, Duane, Anna and Laurie VanPolen, Marion, Mich.
Douglas Warner, Charlevoix, Mich.

 
Silver

Karl Bontrager, Wolcottville, Ind.
Michael Bosscher, McBain, Mich.
Norm and Mark Buning, Falmouth, Mich.
John Christian and Rhoda Chupp, Sugarcreek, Ohio
Terry and Paul Fritz, West Branch, Mich.
Dave, Don and Joanne Hall, Tomah, Wis.
Andy and Laura Hecht, Cumberland, Wis.
Robert L. Hecker, Sheldon, Wis.
Charles Hoellerer, Cincinnatus, N.Y.
Devin Johnston, Tillamook, Ore.
Harley and Marietta Lambright, LeRoy, Mich.
Ann Nelson, Lupton, Mich.
William Pirman, Skandia, Mich.
Melvin and Patricia Pittman, Plum City, Wis.
Jerry and Sharron Powers, Pentwater, Mich.
Mark and Bob Rau, West Branch, Mich.
Walter and William Selke, Dakota, Minn.
Allen and Aaron Slater, Holton, Mich.
Terry and Ryan Stinson, Dakota, Minn.
Ken and Carol Tebos, Falmouth, Mich.
Cortney VanOeffelen, Conklin, Mich.
Marco Verhaar, Bad Axe, Mich.
Robert and Kathy Waite, Middleburg, Pa.
James Winkler, Sterling, Ohio

NDQA judges considered many criteria when reviewing finalists’ applications. In addition to milk quality indicators, such as somatic cell count (SCC) and standard plate count (SPC), judges looked at specific details about each operation, including milking routine, cow comfort, udder health monitoring programs, treatment and prevention programs, strategies for overall herd health and welfare, and adherence to drug use and record keeping regulations.
This year’s NDQA sponsors included Boehringer Ingelheim, GEA, Ecolab, IBA Inc., QualiTru Sampling Systems, Hoard's Dairyman and NMC. This summer, nominee information for the 2018 NDQA program will be available on the NMC website (www.nmconline.org) and in Hoard’s Dairyman magazine.