York man piloted the longest nonstop blimp flight ever
York man piloted the longest nonstop blimp flight ever

By MIKE ARGENTO, York Daily Record
The Associated Press

YORK, Pa.




YORK, Pa. (AP) — Robert Bowser joined the Navy in November 1941, a month before the Japanese attack on Pearl Harbor.
He was just 17 - he needed his parents to sign off when he enlisted - and hadn't even finished high school. He just wanted to the join the Navy. He didn't have any specific plans and certainly didn't foresee that, one day, he would become a part of Navy, and aviation, history.
He trained as an LTA pilot - LTA standing for lighter than air, a blimp, in other words. The Navy used blimps during the war to patrol the coastline and keep an eye out for enemy submarines, filling the dead space left by lookouts on shore-based towers and ships farther out at sea.
He also trained as an aviator and eventually would be carrier-certified in five different aircraft. He also earned his high school diploma and took some college-level courses, qualifying for a commission as an officer, what they called a "90-day wonder" in the early days of the war when the armed forces were expanding at an exponential rate.
He was a test pilot, of sorts. He flew in the one of the Navy's first jets, the FH-1 Phantom. He was among the first pilots to test the steam-powered catapult system that flung aircraft off the decks of aircraft carriers. He tested the Corsair when the Navy had to beef up the landing gear to withstand hard landings on aircraft carriers; his task was to land the plane hard and see whether the landing gear collapsed. Sometimes it did.
He eventually rose to the rank of lieutenant commander. Not bad for a kid who dropped out of York High.
He joined the Navel Air Development Unit (NADU) in South Weymouth, Mass., in 1954, working with the Navy's development of lighter than air aircraft. The base was essentially a testing ground for naval aircraft, a pretty free-wheeling place, as described in an official history of the unit. For instance, the base, while testing blimps during night flights, was a source of most of the UFO sightings off the coast of Massachusetts, something that greatly amused members of the unit. Another time, a blimp on a test flight spooked a rafter of turkeys on a local farm, touching off a stampede in which many turkeys perished. The Navy altered the course of test flights to avoid the farm, but the blimp would cast a shadow on the barnyard, leading to more turkey deaths and prompting the farmer to take a potshot at the blimp. (Later, when Bowser was stationed in Toms River, New Jersey, he would fly a blimp over his house and wave to his children playing in the yard.)
It was in South Weymouth, as a member of NADU, where Bowser would enter history.
As his sons recall it, the Navy was hot to break the endurance record for a nonstop blimp flight set by the Germans. It was a venerable record, set in 1929 by the Graf Zeppelin, covering 7,297 miles from Germany to Tokyo in 101 hours and 49 minutes.
The Naval effort began as an exercise to prove the all-weather capabilities of lighter-than-air craft, part of an ongoing operation code named Meteor. It soon became a quest to break the Graf Zeppelin's nonstop flight record because, why not?
The ship was the ZPG-2, manufactured by Goodyear (which is still in the blimp business) and nicknamed the Snow Bird. It was 300 feet long and 97 feet tall. It contained nearly a million cubic feet of helium.
The crew numbered 14. Among them was Bowser, who served as one of two pilots during the trip.
The ship left South Weymouth on March 4, 1957, and set a course east across the North Atlantic. The biggest problem facing the crew was boredom. It was a long flight, and there wasn't a lot to do on the aircraft. Bowser joked around, posting a sign that announced the in-flight movie would be "Gone With The Wind," an inside joke about the effect of wind on flying what was essentially a large balloon powered by two huge rotary airplane engines.
They saw some sights. One of the crew, Frank Maxymillian, now retired in Massachusetts, recalled calling other crew members to the windows to gaze upon a pod of whales or an iceberg. The more memorable sight would come later in the trip.
One day into the journey, they suffered their only equipment malfunction - the thermostat for their oven burning to a cinder. They made due with the stove and got pretty creative with their frozen dinners and C-rations. One of the crew had a birthday while they were over the Atlantic, and another crewman baked him a cake in a frying pan on the stove, combining, seriously, Chiclets and chocolate rations to make the "batter" and then decorating the cake with cigarettes instead of candles.
As they skirted the west coast of Africa, passing by Cape Yubi, one of the crew announced that he had captured an African sea bat that had flown in through an open window. He said it was in a box, which had a peephole cut in its top. When crew members would bend over to gaze upon the mythical creature, other crew members would whack his posterior with a broom.
During one night shift, a brightly moonlit night, Bowser was piloting and Maxymillian was on the radio. Bowser asked Max, as they called him, whether he wanted to fly the blimp. There wasn't much to it, he explained. You hold onto the yoke and keep the ship level, using the guy wires dangling from the nose as a guide.
Max took the wheel. He thought that he had the ship under control, that he had mastered flying a blimp in one easy lesson. After a few minutes, Bowser told Max to get up. He was getting complaints from guys in their bunks about getting seasick as the ship rolled all over the sky. "I was reluctant to give it up, but he was bigger than me," Max said later. "I don't understand it, but I was not invited to drive again, which I attributed to the fact that they understood I was too busy and had no time to fool around with that kind of nonsense."
The ship crossed the North Atlantic to Portugal and then went south along the west African coast before turning and heading back over the Atlantic toward its destination, Key West.
As they approached Florida, they had some time to kill, so they flew over Miami Beach. As they flew over the hotels that lined the beach, members of the crew noticed something. The rooftops of the hotels had been sectioned off into private booths for sunbathing, and many of them were occupied by women in various stages of undress or completely naked. The crew scrambled for the binoculars and waved to the sunbathers. Some of the women waved back.
On March 15, the Snow Bird landed in Key West as the sun set.
It had been airborne for 246.2 hours and had traveled 9,448 miles without stopping - obliterating the record of the Graf Zeppelin. When it began its journey, according to a report by a Goodyear engineer, it had 22,675 pounds of fuel, about 3,600 gallons. When it completed its journey, it had less than 80 gallons of fuel left.
The crew, after being welcomed by Fleet Admiral William "Bull" Halsey and receiving decorations for the record-setting flight, retired to Key West and celebrated at a bar that, according to Max, had tables just large enough to set their drinks on.
Bowser retired from the Navy on Jan. 1, 1962, and went to work for an armored car company in York. He retired for good in 1978 and moved to the mountains in Potter County. His sons said he never much liked the water or the beach.
The record he helped set still stands.
Not bad for a York High dropout.
His son David said, "He was just dad, but when you think about it, this caliber of human being just doesn't exist anymore."
___
Online:
http://bit.ly/2FytXRm
___
Information from: York Daily Record, http://www.ydr.com