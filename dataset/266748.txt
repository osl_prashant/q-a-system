In the past five articles we talked about how to properly drive cattle. So lets assume we’ve driven our cattle somewhere and we had good movement and we didn’t do anything to interfere with it so the cattle trailed out nicely, and we’ve gotten to our destination—say a summer pasture—so now what do we do? Conventionally, we stuff them through the gate and leave—“We’re done, lets go home.” But what happens?
At least in the case of yearlings, they often beat a race track around the perimeter and get the reputation that they’re fence walkin’, fence crawlin’ little buggers. 
Right? Ever heard that? Ever experience that? It kind of looks like Figure A. 

But it needn’t be that way. If we learn how to settle our cattle properly they will be happy and content where they are and won’t walk the fence (see Figure B). 
Why Do You Think Yearlings Walk the Fence?
For one thing, we’ve just taught them to drive, and we should have good movement, which is almost self-perpetuating, so we have to stop that. If we don’t, the animals are likely to keep moving unless their heads are deep in fresh grass. On the contrary, if we don’t drive our cattle properly their minds are on going back to where they came from. Also, they can literally hate being where we’ve taken them, so they’re going to be looking for a way out. Third, we don’t tell them that this is their new home. 

What we should do is tell them that it’s time to stop and that this is their new home. So how do we do that?
The wrong thing to do is to try to actively stop the herd’s movement. When we do that it’s like putting a cork on a steaming pot—as soon as we leave, the movement is going somewhere. What we should do is dissipate the movement by doing one or all of three things:
1. Stop driving. When we get close to our destination we should stop any active driving and release all pressure. An apt metaphor is shuffle board: We want to put just enough energy into the puck (herd) so it will glide to its target and come to rest. 
2. Ride forward-parallel within their pressure zone. Riding forward-parallel on one or both sides of the herd will tend to slow and stop movement.   
3. Turn the lead. If the herd overshoots our destination we might have to make a gradual turn back to our target while continuing to dissipate movement.
4. Wait and observe. What we want to see is calm, relaxed animals grazing in random orientation, some laying down and none wanting to walk off as depicted in Figure C.  

With new cattle this whole settling process could take 15 to 30 minutes. With experienced cattle it might take only a few minutes.
With very experienced cattle that are used to being moved a lot to new grazes you might not have to do it all (see Figure D), but read your cattle and do what they need.

And Where Are We Going to Do All This?
Well, it should be after they’ve walked past someone at the gate (maybe to get a count or to prepare them for future production events, like sorting and fenceline weaning).
In water-challenged pastures we might need to show them water. But then we need to take them to where we want them to graze, not only because we want that area grazed, but because it’s a good time to reinforce our leadership; that is, instead of letting them stop where they want in a new pasture, we tell them what to do by driving them to where we want. Also, if every time we get to a new graze we just put them through the gate and let them stop and start grazing, what are we inadvertently training them to do? We’re teaching them that’s what we want, that they should stop when we go through gates. No wonder many producers have difficulty driving cattle through gates between pastures and complain that their animals want to stop and eat. This is one way we teach our cattle to be hard to drive.
In the case of cows and calves, if we don’t go through this same process but just dump them through the gate and leave, they might hang on the gate or in the low-lands and riparian areas and ignore the uplands.
So, regardless of the class of cattle, it’s prudent to take them to a target destination in any new pasture and settle them properly. This will result in more manageable cattle and prevent undesirable behavior (e.g., fence walking), and help achieve our range management objectives.