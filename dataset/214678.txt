The World Health Organization (WHO) recently released recommendations regarding the use of antibiotics in agriculture, and Dr. Chavonda Jacobs-Young, USDA Acting Chief Scientist, had a response in a news release from USDA:

"The WHO guidelines are not in alignment with U.S. policy and are not supported by sound science,” Jacobs-Young said in the release. “The recommendations erroneously conflate disease prevention with growth promotion in animals.

"The WHO previously requested that the standards for on-farm antibiotic use in animals be updated through a transparent, consensus, science-based process of CODEX,” she wrote. “However, before the first meeting of the CODEX was held, the WHO released these guidelines, which according to language in the guidelines are based on 'low-quality evidence,' and in some cases, 'very low-quality evidence.'"

Under current Food and Drug Administration (FDA) policy, medically important antibiotics are not used for growth promotion in animals. And the veterinary feed directive that went into effect on Jan. 1 of this year makes clear the veterinary-client-patient relationship.  
“In the U.S., the FDA allows for the use of antimicrobial drugs in treating, controlling, and preventing disease in food-producing animals under the professional oversight of licensed veterinarians. While the WHO guidelines acknowledge the role of veterinarians, they would also impose unnecessary and unrealistic constraints on their professional judgement," Jacobs-Young wrote in her response.

She indicated that USDA agrees more data is needed to assess progress on antimicrobial use and resistance, and alternative therapies for the treatment, control, and prevention of disease in animals should be pursued.