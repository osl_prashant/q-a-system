A footbath is one of the most important tools used on farms to prevent lameness and maintain hoof health. When used properly and paired with a disinfectant, a footbath can prevent and control foot rot and digital dermatitis on dairy farms. Recently, the recommendations for footbath dimensions have changed after new research was conducted at the University of Wisconsin (UW) School of Veterinary Medicine.In the factsheet Ideal Footbath UW-Extension Outagamie County Dairy & Livestock Agent Zen Miller describes the proper installation and use of foot baths on dairy farms and the prevention of lameness in dairy cattle. Specific topics covered include:
New footbath recommendations
Footbath location
Retrofitting your current footbath
Ideal Footbath is one of several factsheets in a hoof health series called “Walking Strong” developed by UW-Extension Dairy Team members.  Other factsheets include:
Economics of Dairy Cattle Hoof Health
Footbath Management
Hoof Health & Housing
Hoof Health & Nutrition
Proper Foot Wrap Application
Recording Hoof Health Events
For more information regarding hoof health and/or animal well-being, visit UW-Extension Animal Well-being and Herd Health.