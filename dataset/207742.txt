Fresh Del Monte Produce Inc.’s second quarter, ending June 30, saw gains in net sales but lower net profit compared to the second quarter of 2016.  
While the company’s second quarter earnings were down from last year, they mark an increase in net sales and net profit from the first quarter of 2017.
 
Fueled by higher net sales in its fresh produce and banana business, Fresh Del Monte Produce Inc. reported net sales for the second quarter were $1.147 billion, compared with $1.089 billion in the second quarter of 2016.
 
Del Monte’s fresh produce and banana business growth partially offset lower net sales in the company’s prepared food segment, according to a Del Monte news release.
 
“External challenges such as oversupply of bananas in Asia and the Middle East markets, lower sales volume of canned pineapple and lower pineapple concentrate selling prices negatively impacted our overall performance,” Mohammad Abu-Ghazaleh, chairman and CEO of the Coral Gables, Fla.-based marketer, said in the release. “However, our broad range of products and businesses, and geographic reach helped us mitigate these challenges, while continuing to grow our business.”
 
Gross profit for the second quarter was $123.2 million, compared with $145.4 million in the second quarter of 2016, the release said.
 
The decrease was principally due to lower net prepared foods sales, lower selling prices in the banana business segment, higher fruit cost in the company’s other fresh produce business segment and unfavorable exchange rates.
 
 
Bananas
Del Monte reported net sales rose to $499.5 million, compared with $497.4 million a year ago.
 
Volume was 3% higher than last year, while gross profit decreased to $58.1 million, compared with $66.5 million in the second quarter of 2016.
 
 
Other fresh produce
Net sales for the quarter increased to $568.1 million, compared with $496.8 million in the second quarter of 2016.
 
Del Monte attributed the increase primarily to its fresh-cut, avocado and pineapple product lines, according to the release.
 
Gross profit fell to $55.0 million, compared with $59.9 million in the second quarter of 2016.
 
Avocados: Net sales jumped 48% to $85.4 million, compared with the prior year. Volume decreased 6%, and pricing was up 58%.
 
Fresh-cut: Net sales increased 25% to $168.7 million. Volume was up 23%, while pricing inched up 1%.
 
Gold pineapple: Net sales increased 9% to $134.8 million. Volume increased 23%. Pricing was 11% lower.
 
Non-tropical: Net sales decreased 10% to $78.3 million, volume fell 15%, and pricing increased 6%.