A number of pepper suppliers have added mini sweet peppers to their product mix, and most report that the peppers have been well received.“Year over year, our acreage continues to inch its way up,” said Mike Aiton, director of marketing for Prime Time International, Coachella, Calif.
The company has increased its mini pepper acreage in Arvin, Calif., and in the Oxnard area while maintaining acreage in the Coachella Valley, he said.
“The tricky part is finding three varieties (red, yellow and orange) that look and taste the same and are harvested at same time period,” he said.
“It’s a pretty steep learning curve,” he said, but he believes Prime Time has come up with the right mix.
“It’s been very successful for us,” Aiton said.
Pasha Marketing LLC, Mecca, Calif., a division of Richard Bagdasarian Inc., packs multi-colored mini peppers to order, said Franz De Klotz, vice president of marketing.
A couple of customers have asked for them, he said.
“We’re not going out and planting those on spec,” he said. “We’re just growing them specifically for customers who ask for them. We’re lining out the acres for our customers prior to planting.”
Despite the limited program, he said volume continues to grow.
Bailey Farms, Oxford, N.C., offers mini sweet peppers and BellaFinas — yellow, red and orange baby bell peppers that are about one-third the size of regular bells, said Randy Bailey, president and co-founder.
“They’re all grown and harvested in a similar fashion,” he said. “We try to focus on this and be the best we can be at it.”
Mini peppers have a significant following, he said.
The company plants 22 different varieties and has them available year-round from Florida, Georgia and North Carolina.
Bailey Farms also has established year-round production in Mexico to serve the western part of the U.S. with its BellaFina product.
BellaFinas are grown in the Culiacan region of Sinaloa in the winter, in Sonora in the spring and fall and in Baja California during the summer.
They’re grown in the eastern part of the U.S. for markets there.
“We’re increasing acreage as we need to,” Bailey said.
Mini sweet peppers and BellaFinas are available in three colors — yellow, orange and red.
During the fall and winter, The Oppenheimer Group, Vancouver, British Columbia, ships Divemex conventional and organic mini peppers in 1-, 1.5- and 2-pound bags, said Aaron Quon, executive category director, greenhouse and vegetables.
“Mini pepper demand continues to grow,” he said.
All Divemex mini peppers are Fair Trade certified, he added.
Last year, Leamington, Ontario-based Pure Hothouse Foods Inc. and seed company Enza Zaden announced a partnership that made Pure Hothouse Foods one of the companies that can grow and market the Tribelli branded mini pepper for North American consumers, said Sarah Pau, director of marketing for the company’s Pure Flavor brand.
“This has been a great success, since Tribelli is a well-known brand in Europe and signifies the outstanding seed varieties and characteristics that Enza has worked hard to put into these peppers,” she said.
“Taste, color, brix and consumer awareness are all highlighted under the Tribelli logo, and this will enhance the flavor profile under the Aurora Bites name from Pure Flavor,” she said.
Leamington-based NatureFresh Farms started growing mini sweet peppers this year, said Chris Veillon, marketing director.
“To date, consumers have commented that the sweetness of the mini sweet pepper is surprising,” he said.
“Some have even said that they have sent a handful to school with their children for a snack at recess.”
Mini peppers are a fun, nutritious item for kids’ lunch boxes or to be enjoyed after school, Prime Time’s Aiton said.
“It’s a handy little item to be eaten fresh,” he said, and has a fairly long shelf life.
Mini peppers also are ideal to cut up and toss into a salad, he added.