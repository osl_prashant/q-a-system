AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:15 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
TOP STORIES:
HOME EXPLOSION-GAS WELL
DENVER — An oil and gas company has reactivated more than 1,350 wells in northern Colorado that were shut down last year after a fatal house explosion blamed on a severed pipeline. Anadarko Petroleum said Tuesday it inspected and tested pipelines connected to the wells before returning them to service. SENT: 350 words.
SEXUAL MISCONDUCT-STATE LEGISLATURES
A 50-state review by The Associated Press has revealed that the majority of state legislative chambers lack publicly available records of sexual misconduct claims. That is true even in states where lawmakers have been disciplined or forced from office over harassment allegations. Female lawmakers say the lack of transparency and clear policies discourages victims from coming forward. By David A. Lieb. SENT: 1,060 words, photos.
INTERIOR REASSIGNMENTS-INVESTIGATION
BILLINGS — Sixteen senior employees at the U.S. Department of Interior reassigned to new duties under President Donald Trump's administration viewed their moves as political retribution or punishment for their work on climate change, energy or conservation, according to the results of an internal investigation released Wednesday. However, investigators said they were unable to determine if anything illegal occurred because the agency leaders did not document their rationale for the reassignments. By Matthew Brown. SENT: 490 words, photo.
SANCTUARY CITIES
SAN FRANCISCO — President Donald Trump's comments about so-called "sanctuary cities" were scrutinized at a federal appeals court hearing Wednesday to determine whether the president's executive order threatening to cut funding from states and cities that limit cooperation with U.S. immigration authorities is legal. Ninth U.S. Circuit Court of Appeals Chief Judge Sidney Thomas asked what the court was to make of statements by Trump and his administration that the president wants to withhold money from sanctuary cities. By Sudhin Thanawala. SENT: 540 words.
With: SANCTUARY CITIES-THE LATEST
CALIFORNIA OFFSHORE OIL-VIOLATIONS
LOS ANGELES — Oil and gas companies drilling in state waters off Southern California violated regulations nearly 400 times in the past three years, according to a report being released Wednesday by an environmental group. Records compiled by the Center for Biological Diversity showed state violations ranging from severe corrosion to failed and missing tests required to gauge the strength of wells. No civil penalties were issued for any of the violations, according to a spokesman for the state agency responsible for overseeing oil operations. By Brian Melley. SENT: 710 words, photos.
IN BRIEF:
— COLORADO BUDGET — The $28.9 billion Colorado state budget proposal has cleared another key hurdle.
— NEW UNIVERSITY PROFESSOR — A top administrator at San Jose State University will become the University of Northern Colorado's next president.
— TEENS KILLED— A Colorado Springs man charged in connection to the gang-related executions of two Coronado High School students has agreed to testify against the other defendants.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for May lost 4.75 cents at 4.8725 a bushel; May corn was off 2.35 cents at 3.87 a bushel; May oats was up 1 cent at $2.3750 a bushel; while May soybeans fell 2.25 cents at $10.4775 a bushel.
SPORTS:
PADRES-ROCKIES
DENVER — Rockies star Nolan Arenado charged the mound after a fastball from Luis Perdomo sailed behind him, setting off a heated brawl that resulted in five ejections during a testy game in which Colorado beat the San Diego Padres 6-4 on Wednesday. Pedromo flung his glove at the rushing Arenado and missed. The big-hitting Arenado then threw a couple of huge punches at the backpedaling Perdomo, but didn't land anything squarely. By Pat Graham. SENT: 580 words, photos.
With: PADRES MOVES — The San Diego Padres have placed outfielder Manuel Margot on the 10-day disabled list with bruised ribs. SENT: 130-word APNewsNow.
PADRES-ROCKIES BRAWL
DENVER — Colorado Rockies standout Nolan Arenado charged the mound after a fastball from San Diego starter Luis Perdomo sailed behind him, setting off a wild brawl that resulted in five ejections Wednesday during a testy series at Coors Field. Perdomo tossed his glove at a furious Arenado, and the big-hitting third baseman started throwing punches at the pitcher in the third inning. By Pat Graham. SENT: 330 words, photos.
NUGGETS-TIMBERWOLVES
MINNEAPOLIS — The Minnesota Timberwolves haven't reached the playoffs in 14 years. Here's an appetizer: a play-in game against the Denver Nuggets, who haven't been there in five years themselves. The final game on the schedule for both teams will send the winner to the Western Conference tournament and the loser home for the offseason. By Dave Campbell. UPCOMING: 700 words, photos from 6 p.m. game.
AVALANCHE-LANDESKOG'S DIRECTION
DENVER — There were moments in such a sour season a year ago when Gabriel Landeskog doubted himself as a player. It just wore on him that much. Never once did he ever question this: His ability to lead the Colorado Avalanche. By Pat Graham. SENT: 860 words, photo.
AVALANCHE-PREDATORS
NASHVILLE, Tenn. — The moment the Predators have been waiting for since June 11 finally is here. After failing to force a Game 7 for the Stanley Cup, Nashville is back in the postseason eager to chase hockey's ultimate prize once again. This time, the Predators are the Presidents' Trophy winners for their first-round series starting Thursday night at home against the Colorado Avalanche. Nashville forward Filip Forsberg sees the NHL's best in the regular season as a better team than the group that made a thrilling run to the franchise's first final as the lowest-seeded team in the playoffs. By Teresa M. Walker. SENT: 800 words, photos.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.