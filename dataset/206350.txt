There is optimism to be found in the younger generation’s attitudes toward animal agriculture and meat consumption.
Cautious optimism, perhaps, because the overwhelming majority of media coverage concerning young people’s attitudes toward livestock production and animal foods tends to echo the animal rights/veggie activist mantra that raising animals for food is inherently wrong.
And immoral.
The morality of meat was precisely the theme of a recent student debate at Cornell University titled, “The Morality of Meat.” Now, I began reading the report in the student newspaper, the Cornell Daily Sun, with some trepidation, because the debate was sponsored by Cornell Students for Animal Rights, the Cornell Vegan Society and a group apparently called Debate in Science and Health.
With that lineup, I was expecting the usual heart-wrenching screed about poor, suffering animals “crammed into cages so small they can’t even turn around,” “pumped full of drugs,” and maybe even a reference to the always popular “slaughtered while still alive” accusation.
Indeed, the story began by quoting the president of the Cornell Students for Animal Rights group recounting the (allegedly) horrific story of a pig that was “rescued,” shipped off to the Farm Sanctuary property in upstate New York, and now lives a life of luxury and leisure.
That pig’s story “has a happy ending,” the student told the audience of about 100 people, “but it’s very unlike most pigs.”
Very unlike, she stressed.
After some more verbiage about how the meat industry ruins not just the lives of animals but also the people working in animal agriculture, the story shifted to the rebuttal presentation, delivered by an undergraduate named Caleb, who opened his argument by stating that, “The existence of confinement farms does not make the entire meat industry immoral.”
They don’t make any of the industry immoral, but that’s just me.
According to the article, Caleb then discussed “new developments in the meat industry that have increased the welfare of animals,” mentioning organic farming, which he explained is a system that “regulates outside ties, makes space for animals so that they can stand up and move around [and] regulates environmental conditions.”
Close, although not quite accurate, and certainly not new. But he made the essential point: Like any industry, improvements can be (and have been) made, which is where criticism should be properly directed. In the end, however, activists always fall back on a rejection of the idea killing any animal for any reason — by people, of course, not other animals — no matter how humanely it might be done.
 “The meat industry is an irreplaceable form of income for many people, like small family farms,” the student said. “It is also an irreplaceable part of diets. There are substitutes that are non-meat, but ultimately these are not the same.”
Amen, brother.
Dog vs. Lamb
The debate then switched to a student who serves as the vice president of Cornell Students for Animal Rights. She began her presentation with a story.
“The story concerns a post on Facebook,” she said. “A post of a picture that contained a skinned animal, and something along the lines of, ‘This is my dinner, and it’s a dog.’ Facebook flipped out.”
Ya think?
The man who made this post was identified by police, and the skinned body was taken for analysis to Cornell’s veterinary college. The lab ran tests, and discovered that the carcass was not a dog, but a lamb.
“Why should we be relieved?” the student asked. “Is there a morally relevant quality that distinguishes a farm animal from any other animal? What about a cow makes it more killable than a dog?”
Well, a cow isn’t more “killable,” but it certainly is more edible.
The final debater supported meat consumption, and the article noted that he brought the audience’s attention back to the question of how people should value animal life next to human life.
“The U.S. produces 19% of the meat for the entire world,” he said. “Every year we export close to 1.3 billion pounds of meat to over 110 different countries around the world. If we were to stop producing this meat, we wouldn’t just be hurting ourselves, but we would be hurting all these underdeveloped countries that can’t provide for themselves.”
The student concluded that we are “morally justified in prioritizing human needs first, and then animal needs.”
The audience was polled at the beginning of the debate with the question: “Is eating meat immoral?” The vote was 41 “Yes,” 43 “No,” and 15 “Undecided.”
After the debate, the poll was 49 “Yes,” 30 “No,” and 12 “Undecided.”
Not great, but given that the audience was the prime demographic for the vegetarian movement, not bad, either.
Editor’s Note: The opinions I this commentary are those of Dan Murphy, A veteran journalist and commentator.