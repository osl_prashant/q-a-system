US Sen. Hoeven named to Defense Appropriations Committee
US Sen. Hoeven named to Defense Appropriations Committee

The Associated Press

BISMARCK, N.D.




BISMARCK, N.D. (AP) — North Dakota U.S. Sen. John Hoeven says he has been named to the Defense Appropriations Committee.
Hoeven said Tuesday the key position is responsible for funding the Department of Defense, including North Dakota's Air Force bases and National Guard.
Hoeven says given North Dakota's defense installations, the position is important to the state "and will help us to better support these defense communities."
The Republican senator will continue as chairman of the Senate Appropriations Committee on Agriculture, Rural Development, Food and Drug Administration and Related Agencies.