Lawmakers quibble over details of $1.3T US spending bill
Lawmakers quibble over details of $1.3T US spending bill

By ANDREW TAYLORAssociated Press
The Associated Press

WASHINGTON




WASHINGTON (AP) — Top-level congressional talks on a $1.3 trillion catchall spending bill are reaching a critical stage as negotiators confront immigration, abortion-related issues and a battle over a massive rail project that pits President Donald Trump against his most powerful Democratic adversary.
The bipartisan measure is loaded with political and policy victories for both sides. Republicans and Trump are winning a long-sought budget increase for the Pentagon while Democrats obtain funding for infrastructure, the opioid crisis and a wide swath of domestic programs.
The bill would implement last month's big budget agreement, providing 10 percent increases for both the Pentagon and domestic agencies when compared with current levels. Coupled with last year's tax cut measure, it heralds the return of trillion-dollar budget deficits as soon as the budget year starting in October.
While most of the funding issues in the enormous measure have been sorted out, fights involving a number of policy "riders" — so named because they catch a ride on a difficult-to-stop spending bill — continued into the weekend. Among them are GOP-led efforts to add a plan to revive federal subsidies to help the poor cover out-of-pocket costs under President Barack Obama's health law and to fix a glitch in the recent tax bill that subsidizes grain sales to cooperatives at the expense of for-profit grain companies.
Trump has privately threatened to veto the whole package if a $900 million payment is made on the Hudson River Gateway Project, a priority of top Senate Democrat Chuck Schumer of New York. Trump's opposition is alarming northeastern Republicans such as Gateway supporter Peter King, R-N.Y., who lobbied Trump on the project at a St. Patrick's luncheon in the Capitol on Thursday.
The Gateway Project would add an $11 billion rail tunnel under the Hudson River to complement deteriorating, century-old tunnels that are at risk of closing in a few years. It enjoys bipartisan support among key Appropriations panel negotiators on the omnibus measure who want to get the expensive project on track while their coffers are flush with money.
Most House Republicans voted to kill the funding in a tally last year, however, preferring to see the money spread to a greater number of districts.
"Obviously, if we're doing a huge earmark ... it's troubling," said Rep. Mark Meadows, R-N.C., a leader of House conservatives. "Why would we do that? Schumer's pet project and we pass that under a Republican-controlled Senate, House and White House?"
Schumer has kept a low profile, avoiding stoking a battle with the unpredictable Trump.
There's also a continuing battle over Trump's long-promised U.S.-Mexico border wall. While Trump traveled to California on Tuesday to inspect prototypes for the wall, what's pending now is $1.6 billion for earlier designs involving sections in Texas that double as levees and 14 miles (23 kilometers) of replacement fencing in San Diego.
It appears Democrats may be willing to accept wall funding, but they are battling hard against Trump's demands for big increases for immigration agents and detention beds they fear would enable wide-scale roundups of immigrants illegally living in the U.S.
Meanwhile, a White House trial balloon to trade additional years of wall funding for a temporary reprieve for immigrants brought to the country illegally as children — commonly called "Dreamers" — landed with a thud last week.
Republicans are holding firm against a provision by Sen. Patty Murray, D-Wash., designed to make sure that Planned Parenthood, intensely disliked by anti-abortion Republicans, receives a lion's share of federal family planning grants.
But another abortion-related provision — backed by House Speaker Paul Ryan, R-Wis. — that would strengthen "conscience protection" for health care providers that refuse to provide abortions remained unresolved heading into the final round of talks, though Democrats opposing it have prevailed in the past.
Chances for an effort to attach legislation to permit states to require out-of-state online retailers to collect sales taxes appear to be fading. And Senate Majority Leader Mitch McConnell, R-Ky., faces strong opposition from Democrats on a change to campaign finance laws to give party committees like the National Republican Senate Committee the freedom to work more closely with their candidates and ease limits to permit them to funnel more money to the most competitive races.
One item that appears likely to catch a ride on the must-pass measure is a package of telecommunications bills, including a measure to free up airwaves for wireless users in anticipation of new 5G technology.