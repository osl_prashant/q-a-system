Ohio Gov. John Kasich talks to a McLane Co. employee at the company's giant distribution center at an opening ceremony Dec. 1. The facility uses robots and artificial intelligence to efficiently store and distribute produce at high speeds. Photo courtesy McLane Co.
Foodservice and retail supply chain services provider McLane Co. has opened a distribution center in Ohio with a focus on technology.
Robots will use bar codes on products to transfer pallets, at speeds of up to 60 mph when fulfilling orders, according to a news release. Labor will still be critical, with up to 500 employees in the future.
“Of our 80 grocery and foodservice distribution centers nationwide, the Findlay facility is the most technologically advanced, combining teammates, state-of-the art automation, advanced robotics and artificial intelligence,” Tony Frankenberger, president of McLane Grocery, said in the release.
Ohio Governor John Kasich toured the $150 million, 417,000-square-foot facility at an opening ceremony Dec. 1.
More than 700 suppliers will have products at the facility, which will store and distribute about 16,000 items to convenience stores, mass merchandisers, warehouse clubs and drug stores in Ohio, Michigan, Indiana and Pennsylvania, according to the release.
The company is transferring jobs and other McLane locations, and has already hired almost 100 truck drivers, with plans to hire 75 more.
 
Check out this video of the robotics and artificial intelligence at work inside McLane's Ozark Distribution Center.