Eleven companies are receiving Cultivating Change farm grants to tackle projects ranging from produce traceability, organic vegetable production, transportation and packinghouse upgrades.
Cultivating Change is a grant program from Greener Fields Together, which is a sustainability initiative from Pro*Act.
Ten companies are receiving $5,000 to $10,000 in the latest round of Cultivating Change grants, and Choctaw Fresh Produce, Philadelphia, received the $20,000 grand prize. Choctaw Fresh plans to use the grant to construct raised bed growing platforms, improving production on tribal lands, according to a news release.
Proposals are selected based on improving marketing, infrastructure, capacity building or certifications.
Pro*Act’s aim is to “really ‘cultivate change’ in communities through effective support of these local farmers who provide fresh produce, jobs and inspiration to those they serve,” Pro*Act CEO Max Yeater said in the release.
Other grant recipients are:

PL88 Farm, Prentiss, Miss., $10,000 to purchase transportation and shed for watermelons and vegetables;
PCC Farms, Mission, Texas, $10,000 for floating row covers in organic brassica/cucurbit production;
Merchant’s Garden AgroTech Inc., Tucson, Ariz., $10,000 to install and implement traceability processes;
Reeves Family Farms, Princeton, Texas, $6,000 for a refrigerated deliver truck;
Arizona Microgreens, Phoenix, $5,000 for germination optimization;
East Texas Aquaponics, Mineola, Texas, $5,000 to improve food safety of aquaponics;
Costa Farm & Greenhouse, White Bear Lake, Minnesota, $5,000 to streamline packing facility;
City Roots, Columbia, S.C., $3000 to help farmers feed families;
Lone Tree Foods, Crete, Neb., $1,000 for a larger truck to expand local food network; and
Long and Scott Farms, Mount Dora, Fla., $1,000 to purchase processing equipment.

“Our team was amazed by the quality of the applications, ingenuity of projects and ultimately enthusiasm from the community,” Anne Nichols, Greener Fields Together sustainability manager, said in the release.