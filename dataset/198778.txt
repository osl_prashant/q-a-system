A typical discussion of farm safety may focus on equipment operation principles, the use of personal protective equipment, or safe animal handling, but there are other aspects of farm safety that we sometimes neglect to include in our discussions.
Stress is an inherent part of many jobs, and farming is no exception. Maybe some of this stress could be relieved if we were able to control the weather, markets, changing regulations, consumer demands, and the list continues. But unfortunately, we can't. A very recent study of Finnish dairy farmers reported the most common stressors were issues beyond their control including agricultural policies of the European Union, treatment of farmers in society and the media, and the future of the agricultural sector (Kallioniemi et al., 2016). Several researchers have explored the relationship of safety and the health of farmers beyond the physical aspects. 

Not everyone was born to be a leader. As they get away from the daily operations of the farm and increasingly deal with human resource management and occupational health, many farm owners and managers find themselves in a role for which they have not received formal training according to a review published by Hagevoort et al. (2013). Stepping away from one's comfort zone and taking on these additional tasks can be stressful. Further, the reviewers reported that leadership style may influence the safety environment in the workplace with transformational leadership demonstrating positive results. Transformational leadership encourages workers to evolve into leaders through motivation, taking ownership, understanding, and modeling the behavior for others. This should start with farm owners and managers. Investing in your workforce, whether that is one person or fifteen, by educating and practicing safe behaviors can have impact beyond the physical safety of employees. Safety can be incorporated into training for new employees, team meetings, or periodic review sessions.  

Producers should not feel alone in the task of relaying and practicing the farm safety message. A study of Wisconsin dairy farmers demonstrated increased awareness and adoption of farm safety practices resulting from a concerted effort to convey this information through various channels (Chapman et al., 2013). The safety messages focused on three areas that provided both safety and economic benefits to the farms adopting the practices. The various routes of distributing the information that were utilized included mass media, educational events, expositions, and other gatherings, industry and education professionals, websites, and farmer-to-farmer exchanges. Farm safety remains a priority for many agricultural organizations and industries. In Pennsylvania, Penn State Extension, the Center for Dairy Excellence, and the Professional Dairy Managers of Pennsylvania are all promoting farm safety.

A bottom line with economic impacts tends to catch producers' attention. While herd health has a clear economic link, the relationship of farm workers' health and behavior to animal health is still being examined. A study conducted in Sweden explored the possible link between dairy farmer health and herd health (Lunner Kolstrup and Hultgren, 2011). In herds where the owners and managers reported more physical health symptoms, the cattle were diagnosed with fewer of the diseases specified in the study. However, the incidence of diseases was higher on farms where the workers reported a negative psychosocial work environment. Poor herd health may take a toll on the workers due to the physical strain and additional time required in caring for the animals or possibly empathy for the sick animals. To keep our farm workforce in better health, an understanding of the risk factors can be useful in reducing exposure to diseases and possible injuries. Another study that was conducted in Finland identified increased age, smaller herd size, lack of mental breaks from work, and inadequate leisure time as risk factors (Karttunen and Rautiainen, 2011). The authors recommend counteracting these risks by making time for leisure and mental breaks from work and getting adequate rest.

While many stressors are beyond dairy farmers' control, some stressors can be reduced through training, a safer working environment, and making time for necessary rest for the mind and body. 

References:



Chapman, L.J., Brunette, C.M., and A.D. Taveira. 2013. A seven-year intervention to diffuse economic innovations with safety benefits to Wisconsin dairy farmers. J of Ag Safety and Health 19(3):147-162.


Hagevoort, G.R., Douphrate, D.I., and S.J. Reynolds. 2013. A review of health and safety leadership in managerial practices on modern dairy farms. J of Agromed 18:265-273.


Kallioniemi M.K, Simola, A., Kaseva, J., and H. Kym?§l?§inen. 2016. Stress and burnout among Finnish dairy farmers. J of Agromed. Accessed online ahead of print on 6/10/16.


Karttunen, J.P. and R.H. Rautiainen. 2011. Risk factors and prevalence of declined work ability among dairy farmers. J of Ag Safety and Health 17(3):243-257.


Lunner Kolstrup, C. and J. Hultgren. 2011. Perceived physical and psychosocial exposure and health symptoms of dairy farm staff and possible associations with dairy cow health. J of Ag Safety and Health 17(2):111-125.