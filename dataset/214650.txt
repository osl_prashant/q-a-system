I define strategy as, “the formulation, communication and refinement of a vision and the determination of the optimal angle, focus and primary source of leverage that will ideally lead to divergence and defensible points of advantage in the marketplace.”
A Strong Start. The beginning point of an effective strategy is, therefore, vision. In fact, vision isn’t just the beginning point of good strategy—it is the ongoing and evolving foundation. And it’s one of the seven key aspects to good strategy.
A vision, as the word connotes, paints a picture of a future state of the organization and what the fulfillment of that vision has done for the customer and society. It is compelling. It captures the interest of those working and supporting its accomplishment. It is a conception of what the future will look like, feel like and be like. 
Inspire, Imagine And Invest. A compelling vision, especially when the turnaround mindset can be employed, drives the strategy formulation process. The more compelling the vision, the more invested team members will be in creating and implementing a robust strategy that fulfills the vision. The acts of creating, communicating and refining the vision constitute that first phase of great strategy.
When a vision is clear and compelling enough, it captures the imaginations of stakeholders. It causes thought, conversation and further strategy dialogues. It inspires the team to work harder to improve tactics and strategy.
When you think about communicating vision, you can simplify it as follows: focus on talking about the future. Talk about the positive aspects of the organization’s future and how it will affect others positively, and do this frequently—as often as you appropriately can. Too often, leadership is focused on talking about recent results or very imminent issues or actions, and it doesn’t focus enough on the more distant future. It is difficult for most people to think three or more years out, and this is why communicating vision is one of the highest priorities of leadership.
A vision reflects these eight things:
1.            future-focused
2.            positive
3.            hopeful
4.            clear
5.            concise
6.            picture-oriented
7.            emotionally evocative and
8.            visceral (involving the senses).
Based on the above, consider what the organization looks like and feels like in the market and from within the organization itself.
Sometimes, timelines are discouraged; at other times, they give leverage. You’ll need to determine this for your situation.
Think About What’s Possible. Look inside your organization, and outside at society, in every way in which your organization might have a possible impact—economically, technologically, on your industry and on the secondary customers served.
If all of the stars were to align, then what could you imagine your organization creating? How would all of these areas be different because you accomplished your mission?
In the short term, imagine you and me speaking three years from today, and you are looking back at all of your progress. What would that progress look like?
In current terms, what is your business? What should it be? What should it not be? Considering what is going on in society—outside of your business—what is needed? How could we make the greatest impact?
Strategy Dialogue Questions. To kick off a strategy dialogue about vision use questions such as:
1.            What do we need and want our organization to look like and feel like in the market and from within the organization itself?
2.            What can we imagine as the most optimistic outcomes 10 years out? If all of the stars align and we become and accomplish as much as we imagine, what is that 10-year picture? Three-year picture?
Bottom line, to have a good strategy, you need a bold and clear vision.