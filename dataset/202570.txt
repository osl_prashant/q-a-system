LENEXA, Kan. - Kareem Abdul-Jabbar, a 19-time NBA All-Star recognized as one of the best in the history of the game, will keynote The Packer's West Coast Produce Expo.
His talk on strategy and leadership will follow the annual Fresh Trends Quiz Show by Produce Retailer editor Pamela Riemenschneider and The Packer's editor Greg Johnson.
Abdul-Jabbar won six championships, including five with the Los Angeles Lakers, and he was inducted into the Naismith Memorial Basketball Hall of Fame in 1995.
"A world-class event deserves an iconic, A-list speaker, and we're thrilled to secure this American legend for one of the produce industry's most premiere events," said Shannon Shuman, publisher of The Packer and vice president of produce for Farm Journal Media.
"After 50 years as a world-class athlete, author and now Presidential Medal of Freedom recipient, Kareem will no doubt have some strong perspectives on success and leadership."
Abdul-Jabbar is the NBA's all-time leading scorer with 38,387 points, ahead of Karl Malone, Kobe Bryant, Michael Jordan and Wilt Chamberlain.
He was also named the league's Most Valuable Player a record six times, more than Jordan, Bill Russell, Chamberlain, LeBron James and Larry Bird.
Abdul-Jabbar played in 1,560 games and averaged 24.6 points, 11.2 rebounds and 3.6 assists.
Before his tremendous success in the NBA, Abdul-Jabbar dominated the college game, winning three national titles with the University of California-Los Angeles under the guidance of John Wooden, one of the most revered college coaches in history for winning 10 national championships in a 12-year span.
Abdul-Jabbar, in addition to his status as a basketball icon, is also an author and regular contributing columnist for the Washington Post and Time magazine.
Before his address, The Packer's Fresh Trends Quiz Show will examine consumer trends and retail strategies, focusing on Western consumers.
The interactive session will give attendees the chance to vote and direct the discussion.
The Packer's West Coast Produce Expo is May 18-20 at the Palm Springs JW Marriott.
Recognized as one of the most important events in produce, with 150 exhibitors and 1,000 attendees, the expo attracts a broad spectrum of industry buyers and sellers.