Do today’s grain prices make you cringe at the thought of marketing grain? Don’t be discouraged. “The market will give you opportunity every year,” says Angie Setzer, vice president of Citizens LLC. “There is a new trading range, $3.50 to $4.50, in corn, which is a bit wider and something to be aware of.” Strong prices tend to be $4 for corn, $10 for soybeans and $5 for wheat, she adds.
To take advantage of marketing opportunities in 2017 and 2018, you must be proactive, agile and realistic. Follow these strategies and tips from market analysts. 
1. Keep local demand in mind.
“Watch basis—the barometer of the market,” says Bob Utterback, president of Utterback Marketing. “Also watch the economic demand for products called spreads.” Depending on end users, some seasons require they secure more grain, which results in higher prices.
2. Use the right marketing tools.
You have a variety of marketing tools in your toolbox—everything from doing nothing to puts and calls. For the past couple years, Matt Bennett, farmer and owner of Bennett Consulting, says his strategy has included having calls in place in the summer to give the producer the courage to make sales during potential summer rallies.
3. Make your grain bins pay.
“When you look at the D [December 2017 futures] in corn, March is a 12.5¢ premium,” says Chip Flory, Farm Journal economist, of the early September market prices. “May is at a 20¢ premium to the D and July is at a 26.5¢ premium.”
Storing grain on-farm could allow you to capture market appreciation. For example, Utterback says, if you sell grain off the combine in the fall, prices could be at one of the lowest points in the year. If you hold out a few months you could put yourself in a more profitable position.
4. Max out storage capacity.
With the current market environment, several signs point to storing grain for a few months. “Hold the maximum amount of inventory you can and wait for basis to tighten,” Utterback says. “If you’re short you want to be short when you have the least downside potential.”
5. Capture the market carry.
Storing grain allows you to capture the market carry, which is the premium distant month futures contracts offer to “carry” the grain for later sales. Forward contracting, selling futures, buying put options or speculating are strategies to capture the carry.
“I like the idea of 26¢ of carry on corn,” says Brian Splitt, senior trader at Allendale Inc. “Buy the December corn and sell July corn as a spread with trading at minus 26¢, which means you know you have 26¢ locked in.”
6. Sell a portion of 2018 crop.
The first sale can often be the hardest, so ease into them. “If it’s a no-brainer sale, start with 5% of 2018’s crop and sell in small increments,” Setzer says. Review price trends for the past few years, so you know a good range for making sales. For example, above $4.15 in corn has been in the top quarter of price ranges since 2014.