Nebraska farmers encouraged to respond to USDA census
Nebraska farmers encouraged to respond to USDA census

The Associated Press

LINCOLN, Neb.




LINCOLN, Neb. (AP) — Nebraska farmers and ranchers still have time to respond to the Agriculture Department's annual Census that will help shape farm policy.
The USDA's National Agricultural Statistics Service is collecting the information. So far, only about 46 percent of the questionaires that were sent out in December have been returned.
Dean Groskurth with the Agriculture Department says it's important to gather the most complete data possible in this census because the data influences important decisions on policy, disaster relief and insurance programs.
More information about this effort is available online at www.agcensus.usda.gov .