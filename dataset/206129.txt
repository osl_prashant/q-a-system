Click here for related charts.

CORN COMMENTS
The national average corn basis widened 3 1/4 cents from last week to 35 1/2 cents below December futures. The national average cash corn price firmed 1/4 cent to $3.18 1/2.
Basis is weaker than the three-year average, which 20 cents below futures for this week.
 
SOYBEAN COMMENTS
The national average soybean basis softened 16 1/2 cents from last week to stand 41 1/4 cents below November futures. The national average cash price declined 18 cents from last week to $9.24 1/4.
Basis is well below the three-year average, which is 4 1/4 cents above futures for this week.
 
WHEAT COMMENTS
The national average soft red winter (SRW) wheat basis softened 3/4 cents from last week to 22 cent below December futures. The average cash price improved 11 cents from last week to $4.39 1/2. The national average hard red winter (HRW) wheat basis softened 3/4 cent from last week to 85 cents below December futures. The average cash price improved 10 3/4 cent from last week to $3.73 1/4.
SRW basis is firmer than the three-year average of 47 cents under futures, while HRW basis is weaker than the three-year average, which is 70 cents under futures for this week.
 
CATTLE COMMENTS
Cash cattle trade averaged $108.50 last week, up $2.61 from the previous week. No cash cattle trade has been reported yet this week, but expectations are for $1 to $2 higher trade given profitable packer margins. Last year at this time, the cash price was $105.89.
Choice boxed beef prices firmed $3.26 from last week at $195.30. Last year at this time, Choice boxed beef prices were $190.75.
 
HOG COMMENTS
The average lean hog carcass bid dropped $4.57 over the past week to $45.25. Last year's lean carcass price on this date was $54.85.
The pork cutout value declined $3.76 from last week to $71.99.  Last year at this time, the average cutout price stood at $74.70.