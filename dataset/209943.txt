The winners of the 2017 Top 10 Dairy Innovation Awards and the People’s Choice winner were announced at World Dairy Expo this week. Dairy Herd Management’s Innovation Awards recognize the top new dairy products introduced during the past year, based on their ability to help producers manage their herds better and more cost effectively.

This year’s winners were selected by a panel of nine judges: six farmers and three leading university technology experts. Entries were ranked for their usefulness, uniqueness, return on investment and ability to improve herd performance or day-to-day dairy farm management.

The top 10 products in the 2017 Dairy Herd Management Innovation Awards are:

Cargill Animal Nutrition, Reveal Moisture Tester

Reveal is a convenient, hand-held forage analysis tester that allows farmers to instantly check forage dry matter. The service uses SCiO, the world’s smallest microspectrometer, to transmit scans to Cargill Forage Lab proprietary calibrations via a smartphone app. Reveal provides cost-effective and precise forage dry matter analysis in real time. For more information, go to www.cargill.com/reveal.

Dairy Margin Tracker, Feed Tool

The Feed Tool is a web-based smartphone app that allows a producer to make dry matter adjustments, update rations, track dry matter intake and monitor feed usage. It eliminates clipboards and waiting for feed sheets. The app allows farmers to take control of their feeding program without purchasing expensive computer hardware or software. To learn more, visit www.dairymargintracker.com/feed-tool.

DeLaval, CF1000S Auto Calf Feeder

The CF1000S automatic calf feeder from DeLaval offers complete control over milk feeding each calf, and its connectivity to CalfApp and CalfCloud allows remote access to a calf’s feeding pattern. The app allows farmers to access feeding information from a smartphone or tablet, where they can monitor calf data and service machines remotely, reducing labor costs. For more information, go to www.delaval.com/en-us/our-solutions/feeding/delaval-calf-feeder-cf1000s/.

FutureCow, DairyAir Direct Drive Scrubber

The DairyAir Direct Drive scrubber is known to reduce prep time and labor while still providing superior milk quality results. This scrubber is perfect for a fast-powered rotary or a parlor pushing maximum capacity. The DairyAir taps into the compressed air so when the milker is on the dry cycle of the teat scrubber, the teats get perfectly dry allowing milk quality and time management to work cohesively. For more information, go to www.futurecow.com.

Livestock Water Recycling Inc., First Wave System

The First Wave System from Livestock Water Recycling Inc., separates manure solids from liquids, resulting in a nutrient-rich liquid, containing ammonia and potassium, and a solid fertilizer component containing organic nitrogen and phosphorus. The stainless-steel screen in the system is mounted on an epoxy-coated skid and arrives pre-plumbed and wired for installation. For more information, go to www.livestockwaterrecycling.com.

McLanahan, Bedding Dryer

The McLanahan Bedding Dryer can reduce moisture content to just 2% in sand and 55% in manure solids, while drying up to 90 tons of sand per hour and up to 18 tons of manure solids per hour. The Bedding Dryer is designed for easy installation with maximum thermal efficiency. Output is monitored and adjusted automatically. For more information, go to mclanahan.com/products/bedding-dryer/.

My Dairy Dashboard

Through My Dairy Dashboard, farmers and their consultants have one place to look at all their data, connected, across platforms and delivered to their mobile devices. The information is combined in visual form so farmers and their advisers can act faster. For example, the system combines herd and feed software, co-op milk portals and weather data that can then be visualized on any mobile device. For more information, go to www.mydairydashboard.com.

Professional Dairy Producers, Dairy AdvanCE

Dairy AdvanCE is an online continuing education resource that allows farmers and allied industry members to prove they are continually improving and learning as dairy professionals. Dairy AdvanCE provides vetted trainings from trusted third-party educators and allows participants to track and manage their classes through an online dashboard. Dairy AdvanCE is free for dairy farm owners, farm managers and on-farm employees. For more information, go to www.dairyadvance.org.

SCR by Allflex, eSense ear tag

The new eSense ear tag from SCR by Allflex offers a special shock-absorbing head that makes it more resilient. The ear tag is also made of a UV-resistant material, making it more durable. It’s designed to perform and survive in all farm environments and temperatures. The eSense tag transmits actionable information back to SCR software. With that information, farmers can make more accurate management decisions. For more information, go to www.scrdairy.com.

SCR by Allflex, SenseTime

SenseTime electronic cow monitoring system allows farmers to use either ear or neck tags and offers industry-leading heat detection and precise insemination timing guidance. SenseTime’s algorithm has the ability to monitor a combination of activity and rumination, allowing for a more holistic look at the cow and her health. For more information, go to www.scrdairy.com.

“Almost all of this year’s winners showcase breakthrough technologies. Technology affects every aspect of today's dairy operation from cow comfort, nutrition, genetics, animal health and milk quality,” said Cliff Becker, Vice President and Publishing Director. “We're pleased to recognize the R&D investments and product applications these companies are making to enhance the viability and profitability of dairies across the globe.”