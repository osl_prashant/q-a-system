There are many different selection parameters dairy farmers can focus on when looking at genetic improvement. The two primary goals of genetic selection in dairy cows are to produce large quantities of milk efficiently and increase cow longevity.There have been some massive changes overtime due to selective breeding, says Kent Weigel, genetics specialist at University of Wisconsin.
"Selection is a long-term process," he says. You want to be sure you are going in the right direction when making changes in your genetic program.
1. Physical type
There is some conflict between management and type traits. "I would argue it would be better to use measurements like body condition scores and locomotion scoring, rather than using a type classifier," Weigel says.
Feet and leg traits are low in heritability. Weigel believes there is some potential to work with hoof trimmers to not only improve hoof health, but collect management data.
Body size has continued to increase (about 50 lb. per decade) despite having a negative weight in genetic indexes like Net Merit. A base change occurs approximately every five years with the index raising the average body size.
"It is important that we stop this trend (of increasing cow size)," Weigel says. He cautions feed efficiency does matter when looking at cow size; big cows aren't necessarily efficient cows.
2. Calving ease
Until the last decade, bulls with higher birth weights or incidence of dystocia were bred almost exclusively to older cows. In doing so, it slowed genetic progress because those bulls weren't mated to the latest heifer genetics in herds.
There are direct sire and maternal components that cause dystocia, Weigel says. He points out there have been a few families in the Holstein breed that have a difficult time producing live calves compared to others. There might be 20% of cows that aren't able to have calves from a particular sire line.
"We need to get those genes out of the population rather than correctively mating around dystocia," Weigel says.
3. Gestation
Lowering the length of gestation has some possibilities with it being 40% heritable. The Holstein bull O MAN is an example of this, having calves hit the ground 3.9 days earlier and helping reduce dystocia.
On the other hand, Weigel cautions the industry doesn't want to go too far down the path of reducing gestation length because it could lead to more still births. "We could probably push it three to five days earlier without having any more problems. That could be of some value bumping up the calving interval," Weigel says.
4. Milk
There is more than 100 years' worth of data based on milk yield and composition. During that time, selection preferences have changed from milk volume to butterfat to protein, but Weigel says there haven't been major developments.
Breeding for specific proteins or fatty acids could happen in the future. "There is a lot of potential with infrared spectrum data to figure out which direction to go," Weigel says.
These types of measurements could transform milk component genetics and would be driven by consumer preference.
Closing points
There are more traits being analyzed than ever before, but the relative values of those traits are decreasing. At some point, the more traits being measured adds noise. "You're either double counting measurements we already have or counting things that don't matter," Weigel says of the eventual peak of genetic traits being met.
He hopes to see more genetic work on early postpartum health, milk product quality and feed efficiency.
"I think we can get where we want to go very fast. But before we hammer the pedal we should make sure that is where we want to go," he says.
Note: This story appears in the February 2017 issue of Dairy Herd Management.