India-based Uflex Ltd. has introduced a new shelf life extension product for exotic fruits. 
 
The product, called FlexFresh, is a box liner film that controls the respiration of exotic fruit such as lychee, rambutan and mangosteen, according to a news release.
 
Under normal conditions, rambutans can lose 8% moisture per day, but treatment with Flexfresh reduces those moisture losses and preserves quality of the fruit, according to the release.
 
A trial of the product found the weight loss for rambutan fruit after a week was below 0.5% and the quality was still good, according to the release.
 
Flexfresh is sold in liner bags suitable for international standard-size open top boxes and crates, according to the release. In addition, the product also is available as flow wrap for large products such as papaya and melons, according to the release.