BC-KS--Kansas News Digest 1 pm, KS
BC-KS--Kansas News Digest 1 pm, KS

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Kansas.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
TOP STORY:
Kansas-School Funding
TOPEKA, Kan. — Kansas lawmakers are tackling the smaller part of a public school funding plan meant to comply with a court mandate, with a committee taking up a bill 11 weeks into their annual session to revise the formula for distributing education funds so that it's fairer to poor school districts. By John Hanna. UPCOMING: 350 words.
AROUND THE STATE:
KANSAS-SOMALIS TARGETED
WICHITA, Kan. — Three militia members plotted to bomb a Kansas mosque and apartment complex housing Somali immigrants to kill as many Muslims as possible, a federal prosecutor said Thursday. In her opening statement at their trial, Risa Berkower told jurors that the three men described the Somalis as "cockroaches" and planned to carry out an attack in the meatpacking town of Garden City, Kansas, about 220 miles (350 kilometers) west of Wichita, until a fourth man revealed their plot the FBI and authorities arrested them. By Roxana Hegeman. SENT: 250 words. Will be updated.
— KANSAS-SOMALIS TARGETED-THE LATEST — The Latest on the trial of three militia members accused of plotting to bomb a Kansas mosque and apartment complex
UNIVERSITY STUDENTS-SEXUAL ASSAULTS
ST. LOUIS — Four days before a Kansas State University graduate was due in court on charges that he sexually assaulted an unconscious man at a campus fraternity last fall, he sexually assaulted a University of Missouri-St. Louis student at gunpoint in an on-campus apartment, prosecutors allege. Devonta Bagley, 23, of Belton, Missouri, is jailed on $500,000 bond on charges of sodomy, armed criminal action and burglary in the Missouri case, the St. Louis Post-Dispatch reported. He faces similar charges in Riley County, Kansas, where prosecutors say he sexually assaulted a 20-year-old inside the Sigma Chi fraternity house on Sept. 9 while the man was "unconscious or physically powerless." SENT; 312 words.
EXCHANGE-WALKING ON LIFE SUPPORT
KANSAS CITY, Mo. — Almost every day in Kansas City, a 16-year-old girl does the equivalent of hiking in the thin air around Mount Everest. In the process, she's changing what staff at Children's Mercy Hospital and elsewhere believed was possible. Zei Uwadia was airlifted to the hospital from Wichita in October with lung failure that, even now, no one can explain, The Kansas City Star reports. By Andy Marso. Sent: 1,700 words.
EXCHANGE-TEXAS COTTON CROP
AMARILLO, Texas — When the Carson County Gin is cranked up and the cotton is flowing — being transformed from a harvest crop full of sticks, seeds and field trash into the white fibers ready to be sold at market — it's hard to carry on a normal conversation. The Amarillo Globe-News reports lately, workers do more shouting than talking on the floor of the gin located along U.S. Highway 60 between Panhandle and White Deer. "This is the largest crop we've ever ginned," said Keith Mixon, the general manager of the Carson County Gin. Last season, Mixon said his operation, which runs around the clock during this time of year, ginned 111,215 bales of cotton. He said he's expecting to gin more than 140,000 bales this season. A cotton season is from October to early April. By Jeff Farris, Amarillo Globe-News. SENT: 680 words, pursuing photos.
IN BRIEF:
— BARBECUE OWNER THEFT — A barbecue restaurant owner in eastern Kansas is accused of racking up more than $7,300 in charges on a customer's credit card left at the business.
— BAND NOROVIRUS — Lawrence school officials say about a third of the 150 Lawrence High School band members who recently went to Orlando, Florida, became sick with after apparently contracting a norovirus.
— WIRE FRAUD — A former treasurer has been accused of stealing from the Jewish Federation of Lincoln.
SPORTS:
BKC--NCAA-Kansas St-Kentucky
ATLANTA — No. 5 Kentucky and its latest wave of one-and-done freshman are the highest-seeded team still standing in the bracket-busting NCAA South Regional, setting up a semifinal game against defensive-minded Kansas State in the city known as "Cat-Lanta." By Paul Newberry. UPCOMING: 750 words, photos. Game starts at approximately 9:37 p.m. EDT.
BKC--NCAA-Tipping Off
UNDATED — Back to the Madness of March, starring Loyola-Chicago, Kansas State, Syracuse and whomever else you might want to throw in there as the NCAA Tournament resumes on Thursday night. By John Kekis. UPCOMING: 600 words, photos.
BKC--NCAA-Midwest Region
OMAHA, Nebraska — Kansas once again finds itself as a top seed needing just two more wins in a friendly gym close to Lawrence to retutn to the Final Four. But even if the Jayhawks get past fifth-seeded Clemson on Friday, a possible date with second-seeded Duke — to many, the favorite to win it all — on Sunday. By Luke Meredith. UPCOMING: 650 words, photos.
BKC--NCAA Midwest-ACC Invasion
OMAHA, Neb. — The Atlantic Coast Conference has invaded the NCAA Midwest Region. Clemson, Syracuse and Duke are here, making the ACC the first conference since the Southeastern in 1986 to have three teams in the same regional. By Eric Olson. UPCOMING: 600 words, photos.
BKW--NCAA-Kansas City
KANSAS CITY, Mo. — The top four seeds of the Kansas City Region have reached the semifinals, and now top-seeded Mississippi State will face North Carolina State and second-seeded Texas will meet UCLA for a spot in the Elite Eight. By Dave Skretta. UPCOMING: 700 words, photos
BBA-ROYALS PREVIEW
KANSAS CITY — This was supposed to be the season that the Kansas City Royals, after spending the last half decade in contention, were finally forced to rebuild as their core group of players hit free agency. Turns out half of them came back. By Sports Writer Dave Skretta. SENT: 733 words.
AP Photos NY151, NY150.
BBN-CARDINALS PREVIEW
ST. LOUIS — Adam Wainwright believes he can still be the best pitcher in baseball, even at 36 years old and coming off an injury-riddled season that was the worst of his illustrious career.  Whether the right-hander's assessment is anywhere near correct could determine not only his future in baseball, but that of the St. Louis Cardinals. By Sports Writer Kurt Voigt. SENT: 705 words.
AP Photos AP Photos NY151, NY150.NY225, NY226, NY227.
___
If you have stories of regional or statewide interest, please email them to apkansascity@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Missouri and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.