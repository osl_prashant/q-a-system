USDA Weekly Grain Export Inspections
			Week Ended March 23, 2017




Corn



Actual (MT)
1,556,091


Expectations (MT)

1,200,000-1,500,000



Comments:
Inspections are climbed 198,102 MT from the previous week but the tally topped expectations. Inspections for 2016-17 are up 71.6% from year-ago, compared to 72.3% ahead the previous week. USDA's 2016-17 export forecast of 2.225 billion bu. is up 17.2% above the previous marketing year.



Wheat



Actual (MT)
541,799


Expectations (MT)
450,000-650,000 


Comments:

Inspections are down 107,284 MT from the previous week and within expectations. Inspections for 2016-17 are running 28.9% ahead of year-ago compared to 28.2% ahead last week. USDA's export forecast for 2016-17 is at 1.025 billion bu., up 32.3% from the previous marketing year.




Soybeans



Actual (MT)
555,012


Expectations (MMT)
500,000-700,000 


Comments:
Export inspections fell 189,049 MT from the previous week, and the tally was near the lower end of expectations. Inspections for 2016-17 are running 11.4% ahead of year-ago, compared to 11.6% ahead the previous week. USDA's 2016-17 export forecast is at 2.050 billion bu., up 5.9% from year-ago.