Weekly weather outlook from Michael Clark of BAMWX.com
The outlook into early to mid-April is a volatile one with ups and downs and plenty of rainfall on the table, especially for the eastern ag belt. Between now and the next two weeks we expect the pattern to be dominated by a strong Alaskan ridge. (-EPO) This will drive the cold you see in the outlook for this week and next, which means winter storms are not out of the question over the next two weeks.

Beyond week two we anticipate some of this blocking to subside, and with that will come some warmer risks especially across the southern and central tier of the ag belt.

Analogs and perhaps a favorable state of the MJO supports the idea of some warmth to build around April 7th and beyond. With that being said, it’s possible the eastern ag belt will remain active during this time with heavy rainfall risks and some severe weather into weeks three and four. You can see from our outlooks over the next four weeks that they tell this story nicely.

 
Forecast information and maps provided by BamWX.com