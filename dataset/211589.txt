Sonia Jimenez is the new deputy administrator for the Specialty Crops Program starting Sept. 17, according to a news release from the U.S. Department of Agriculture’s Agricultural Marketing Service. 
Jimenez has held a variety of positions with the AMS and the Foreign Agricultural Service over the past 25 years, serving as the deputy administrator for AMS’ management and analysis program for the past three years, according to the release.
 
Jimenez’ appointment to deputy administrator for specialty crops is part of USDA organizational changes Agriculture Secretary Sonny Perdue announced the week of Sept. 4.
 
Melissa Bailey and Christopher Purdy continue as associate deputy administrators, according to the release.