Crop calls
Corn: 5 to 7 cents lower
Soybeans: 5 to 7 cents lower
Wheat: 6 to 8 cents lower

Grain and soybean futures ended the overnight session lower amid profit-taking and forecasts for rains. Futures sank into the end of the session despite weekend heat and only scattered showers across the Northern Plains and upper Midwest. But weather models signal better rain chances from Tuesday through Friday across a broad area of the Corn Belt. Winter wheat harvest has moved into southern Kansas, which added to pressure on wheat futures overnight. Meanwhile, USDA just announced an unknown buyer has purchased 130,000 MT of old-crop U.S. soybeans.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Livestock futures are expected to see a mixed start as traders reevaluate positions and wait for direction from the cash market. Cash cattle trade was mixed last week on light volume. But nearby live cattle futures remain at a discount to the cash market, which should limit pressure, especially given last week's improvement in the beef market. Meanwhile, bulls have the near-term advantage in the hog market and packer demand is strong thanks to profitable margins. Traders will be watching the pork market closely this week for clues about summer features.