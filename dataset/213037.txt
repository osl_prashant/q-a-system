Farrow-to-finish pork producers earned $28 profit per hog last week, a $3 per head decline from the previous week. A month ago, farrow-to-finish pork producers showed a loss of about $2 per head.Lean carcass values remained steady to 8 cents higher.
Feed costs for this week's finishing placements were relatively unchanged.
Pork packers saw their margins increase $8 to $30 per head. Negotiated prices for lean hogs were $73.58 per cwt last week, steady with the previous week. Cash prices for fed cattle are $6 per cwt higher than last year and prices for lean hogs are about $5 per cwt lower.

Beef packer margins increased $18 per head last week while cattle feeding margins dipped $27 per head, according to the Sterling Beef Profit Tracker. Read more from Drovers