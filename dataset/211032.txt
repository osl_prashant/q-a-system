It’s no secret farmers have long wanted to know just how often the same seed reappears in more than one bag. One California-based data company says they’ve cracked the code.
“What we’ve found is that the majority of seed companies (71% in corn, 79% in soybeans) sell varieties that are also sold by other brands,” says Charles Baron, co-founder of Farmers Business Network (FBN). “Ninety-five percent of our farmer–members bought from one of these companies and 63% planted relabeled seed (seed available in other brands).”
FBN’s study gathered about 7,500 seed tags that represented 110 seed companies and 2,550 unique seeds on the market. The company requested this information by sending an email to farmers asking them to send in seed tags so FBN could then learn the brand and variety number information. FBN claims 38% of corn and 45% of soybeans tested were sold by more than one seed brand—about $2.5 billion of the U.S. seed market.
“The implications for farmers are vast. First, many are overpaying for genetics since the same variety can be available by as many as 12 other brands with totally different pricing,” Baron says. “Second, the genetic diversity of the crop is much lower than assumed. And third, the potential for accidental genetic duplication and concentration is high, affecting disease resistance issues.”
It’s not just small seed companies with shared genetics. FBN says nearly every multinational brand has some, too (see charts).
Farmers who want to access this information, and the new Seed Finder system that finds any variety’s cross-matches, need to join FBN for $600 per year and submit at least one seed tag. FBN says they want farmers to use this information to help empower their decisions. “We found identical corn seeds available for $97 per bag less in the same state in different brands,” Baron says. “That’s $40 per acre; everyone needs to check their seeds before they buy.”
 






Licensing means some varieties show up in more than one bag; however, some brands within companies you can only buy from their respected seed dealers, in that specific brand's bag such as DeKalb, Asgrow, Channel corn and Pioneer corn and soybeans.© Farmers Business Network






Seed companies say sharing variety numbers is good for farmers.
The biggest reason farmers see shared genetics is because of licensing.
“The development of corn inbreds and soybean varieties has become so expensive and complex that very few companies develop their own germplasm today,” says Chuck Lee, Syngenta head of seeds product marketing. “For the most part in corn there are only four companies that spend the money necessary to develop genetic material—hundreds of millions of dollars. There’s even fewer in soybeans, really only three.”
Licensing means genetics reach a larger area. “For Dow AgroSciences seed companies, there is some overlap in genetics,” says a company-prepared statement. “Each company brings a unique portfolio of products that is locally tested and specifically selected to meet the needs of farmers in their own geographies.”
“While there are some shared genetics among these brands [PROaccess], each company serves growers with a unique portfolio of products that is tested and selected to meet the needs of customers in their local geography and market segments,” says Jeff Burnsion, senior marketing manager, PROaccess genetics. 
Licensing provides opportunities for smaller brands to access the latest genetics. “We choose to broadly license our traits and genetics whenever possible so that farmers can purchase them in the brands they wish,” says Jeff Neu, Monsanto product communications lead. Each company has the opportunity to differentiate.
Seed conditioning, growing conditions, seed treatment, germination rates and other handling factors come into play to differentiate the quality of seed you buy.
“My wife, Julie, and I started our seed company 22 years ago and we take great pride in the quality of our seed. We label all soybeans at 94% germination, for example, because of the care we show that seed,” says Carl Peterson, founder of Peterson Farm Seeds in North Dakota. “To say that there are the same genetics, or close to the same is one thing, but that doesn’t mean they’re the same product.”
 






Licensing means some varieties show up in more than one bag; however, some brands within companies you can only buy from their respected seed dealers, in that specific brand's bag such as DeKalb, Asgrow, Channel corn and Pioneer corn and soybeans.© Farmers Business Network






Eliminating overlapping genetics could have drawbacks.
“Licensing is key to choice and diversity,” says Andy LaVigne, CEO and president of the American Seed Trade Association. “Look at Canada where only one provider can sell a variety—it reduces choice and competition.”
In Canada farmers know they’re getting the best price, but there are fewer companies on the market.
“Freedom to choose their buying experience is how we ensure farmers do what’s best for their operations,” says Mark Herrman, AgReliant Genetics president and CEO.
If the U.S. took an approach like Canada, farmers would see a drastically different seed-buying landscape.
“Licensing is the life blood of independent seed companies and a driver of competition,” says Todd Martin, CEO of the Independent Professional Seed Association. “Without licensing, independents and competition languish and farmers see higher prices.”
Seed companies support genetic licensing.“What FBN is trying to spin as a questionable thing is actually a great thing for growers,” Lee says. “I would say if we don’t license, or ‘relabel’ as FBN says, the industry will be down to three seed companies.”