Reed canary grass (Phalaris arundinacea L.) and its many subtypes are a common introduced/planted and native/wild grass in South Dakota that is often associated with wetland edges, saturated soils, and occasionally on moist to dry slopes extending out from wet or saturated soils. While considered a valuable forage species in some reports with qualities such as good crude protein, high digestibility, drought and flood tolerance, high yield, erosion control, and cool season growth, reed canary is also often considered undesirable as a forage or habitat plant. In many cases, it outcompetes other vegetation and thus is utilized accordingly.
Case Study:
Reed Canary Grass & Prussic Acid Poisoning
This article addresses a little-known but interesting aspect of the biology of reed canary grass. The US Department of Agriculture reports reed canary grass as a naturally occurring species in North America, Europe and Asia, and both wild and naturalized varieties, along with active breeding programs, have resulted in specific cultivars of the plant for forage and landscapes (USDA NRCS).
For the most part, prussic acid issues with reed canary grass are poorly understood and may go unrecognized if they occur. There is very little data and limited records that might help our understanding on this topic, but it is my speculation, based on conversations with area producers, that it is at least plausible that livestock deaths on wet pastures may be attributed to foraging on reed canary grass under certain early-rapid growth and/or freezing conditions in the late fall.
The following information was compiled by the author in relation to the health of an individual 18-month-old angus heifer that occurred in late October 2016.
Heifer Deaths Noticed
On October 24, 2016 I discovered one (of four) 18-month old angus heifers weighing approximately 950 lbs. dead in my pasture lying in a patch of reed canary grass. Generally, this area is a mix of wet/saturated loam and clay soil next to an intermittent stream that rises quickly and transitions to a dry/sandy hillside and thus harbors a variety of cool and warm season grasses. Typical species in this pasture include exotic species such as reed canary grass, smooth bromegrass, Kentucky bluegrass, quack grass, and narrow leaf cattail; native species include green needlegrass, needle and thread, big bluestem, sideoats grama, indian grass, and prairie cordgrass. Typical native forbs include common milkweed, Canada goldenrod, and cudweed sagewort while exotic forbs include Canada thistle, wormwood sage, and musk thistle. The heifers had been on this type of pasture since approximately early September. All were healthy with no indicators of poor health or disease. All heifers were vaccinated as calves with standard vaccinations.

 
Temperature & Grass Observations
The reed canary grass patches in this pasture were partly cured and dry due to the late season, however new green fall growth was present and a series of overnight frosts had preceded discovery of the heifer. Weather monitoring indicated light frosts (down to about 25 degrees F) occurred approximately one week prior to this date (about Oct. 17), again on Oct. 22 and/or Oct. 23. In total, approximately six nights of freezing or near freezing weather had occurred since early October, but none were considered severe. I did not witness any icing of even the smallest ponds, but vegetation has clearly shown frost effects in certain areas.
 
Prussic Acid Poisoning Suspected
I generally offered fresh grass to my heifers every one or two days in a modified, small scale, management intensive grazing program. On the morning of Monday, Oct. 24, I discovered the dead heifer, approximately 66 hours since I had last checked them. I discovered the heifer lying on her left side, head slightly down hill, in a patch of reed canary grass. The heifer was just on the outside of the 2-strand electric fence, and the bottom fence strand was broken. The fence had been on and there was no history of this heifer ever testing the hot wire fence. All indications appeared that she fell into and through the fence, breaking the lower wire (speculation). There were some signs of thrashing of the legs based on some of the vegetation near her.
Since the only variable in the overall grazing program was the frost over the weekend, I suspected possible prussic acid poisoning, but had no prior knowledge if such in reed canary grass. A quick internet search indicated that certain varieties of wild/invasive reed canary grass can have prussic acid issues, although there are few sources available that clearly document prussic acid levels in reed canary grass. I also immediately collected samples of reed canary leaves that appeared to have been frozen during the previous evening.
Rigor mortis had already occurred and due to a miscommunication within the veterinary clinic, and the veterinarian never received the request to draw blood when conducting the postmortem analysis. The post-mortem examination yielded no indications of ill health. All organs were in healthy condition, and there was no indication of black leg (which had been showing up in some non-vaccinated animals in the Watertown Region at that time). Blood work may or may not have helped to determine cause of death in this case since the exact time of death was unknown and it would have been impossible to determine whether the test would have been within the desirable 24-hour period recommended for prussic acid detection.
Grass Sampling & Testing
After speaking with various veterinarians and SDSU colleagues it became evident that there was little clear indication of the cause of death, but prussic acid did appear plausible. I contacted American Agricultural Laboratory in McCook, NE, which agreed to perform the necessary tests on the samples, which I then expressed shipped (on ice) for testing. Initial results on both samples tested positive for prussic acid. A sample from the canary grass patch where the heifer was dead on the outside of the pasture fence tested at 148 mg/kg. The sample that was taken inside the pasture near where she likely grazed prior to death was 209 mg/kg.
The chart below from Oklahoma State University is consistent with most information on prussic acid toxicity and indicates that prussic acid levels likely need to be above 500 ppm (mg/kg) to be of lethal concern.
Table 1. Prussic acid toxicity.



ppm HCN
			(dry matter basis)
Interpretation


0-250
Very low - safe to graze.


250-500
Low - safe to graze.


500-750
Medium - doubtful to graze.


750-1000
High - dangerous to graze.


>1000
Very high - very dangerous to graze.




Continued Testing & Findings
If reed canary grass is the dominant source of forage, issues with prussic acid concentration may arise, but it is not entirely clear how much total consumption would be necessary to cause death at what appeared to be non-lethal levels. In the case of my pasture where this heifer died, I would estimate that reed canary comprised over 70% of total dry matter available. However, it comprised greater than 90% of the green forage available, and thus may have been specifically selected by the animal. In addition, according to the staff at American Agricultural Laboratory, the fact that prussic acid was present in a pasture sample at all could be cause for future concern and was enough justification to pursue additional testing of samples collected as new growth followed subsequent frosts.
I continued to collect samples as the freeze/warm weather pattern continued into November 2016, producing lush fall regrowth on many cool season species. Prussic acid samples from new reed canary grass shoots from the pasture creek collected on November 3rd, 2016 measured 181 mg/kg while random samples from two separate pastures collected on November 14, 2016 had prussic acid levels of 241 mg/kg and 218 mg/kg. While all tests indicated that the prussic acid level in the samples was below the level of concern and cannot be definitively identified as the cause of mortality, the facts that prussic acid was present and persistent, that no major change in diet occurred, and that a heifer suffered mortality while foraging primarily on reed canary grass is worth a note of caution if using reed canary grass as an early spring or late fall forage.
Potential Alkaloid Issues
Reed canary grass also has some potential issues with alkaloids, which is documented much more prevalently in the literature than is prussic acid. However, these issues are typically associated with spring growth, but cannot be ruled out in fall growth. A 2010 report by Binder et al. published in the Journal of Veterinarian Diagnostic Investigation provides a synopsis of tryptamine alkaloids present in reed canary grass which are known to cause livestock ‘staggers’ and death. Additional information on reed canary grass (Phalaris arundinacea L.) from Binder et al. (2010)

Toxicosis has been associated with different species of canary grass since 1942 on various continents.
Reed Canary grass contains alkaloids that can result in sudden death or neurological ‘staggers’ in ruminants such as cattle and are usually associated with spring growth. This study reported the death of 18 cows over a two-month period in West Virginia.
A single gene controls the production of tryptamine alkaloids in canary grass, and there have been attempts to develop less toxic varieties.
There is no treatment for animals affected with staggers.
Producers considering planting of reed canary grass for its desirable characteristics should seek out a cultivar that does not produce tryptamine alkaloids.

Prussic Acid Poisoning Information
A 1998 NDSU Extension report also provides key information on prussic acid poisoning (V1150, Stoltenow and Lardy, 1998). Their general comments on prussic acid poisoning in plants included:

Leaves usually produce two to 25 times more prussic acid than do stems in forage grasses
Young, upper leaves have more prussic acid than lower leaves.
New shoots often contain high concentrations of prussic acid.
New shoots produced after frost can be especially hazardous.
Seeds contain no prussic acid.


Additional Resources:

Prussic Acid Poisoning, Forages and Livestock: Causes and Effects
Prussic Acid Poisoning of Livestock
Nitrate Poisoning: Symptoms Resemble Those of Prussic Acid