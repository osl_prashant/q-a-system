Fowler, Calif.-based Bee Sweet Citrus’ domestic season is fully underway, and the company expects its page mandarins to be available through January.
“This year’s page crop has been great,” Bee Sweet Citrus salesman Jason Sadoian said in a news release. 
“The fruit eats well and its flavor and bright color really stands out amongst other varieties.”
Page mandarins are often seedless and can be used for snacking, in salads or for juicing, according to the release.
“Bee Sweet’s page mandarins are grown in California’s Central Valley and are incredibly sweet,” Sadoian said in the release.