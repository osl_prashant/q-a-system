In March, Avocados From Mexico is joining dietitians in Canada to promote healthy eating during Nutrition Month. 
 
Avocados From Mexico will participate in Nutrition Month, a national campaign organized by Dietitians of Canada. Avocados from Mexico will use a series of public relations and information campaigns with media integration, to spread the word, according to a news release. 
 
Avocados From Mexico plans a 4-week media campaign, a mobile app with 12 recipes, a website and in-store activities with Canadian retailers.
 
The health and nutritional benefits of avocados will also be highlighted by a wide range of bilingual teaching tools developed in partnership with Dietitians of Canada distributed across the country during the month of March, according to the release.
 
To learn more about the Nutrition Month campaign, visit www.NutritionMonth2017.ca