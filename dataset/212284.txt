The new Food Marketing Institute U.S. Grocery Shopping Trends is 126 pages long, so it is no use talking about the collective highlights of such a comprehensive report.But consider just a few factoids from the report about online shopping.
Table A7 of the report speaks to the frequency of outlets shopped for groceries.
For the option of online-only food or grocery service source, such as Amazon or FreshDirect or a subscription to meals or produce, the responses showed increased use of that option compared with 2016.
For 2017, 4% of consumers say they shop online “almost every time” and 7% said they shop online only grocery fairly often. Those percentages are up from 1% and 4% last year, respectively.
The percentage of consumers that never shop online grocery dropped from 80% in 2016 to 75% for 2017.
For the question of formats shopped in the past 30 days, 13% of consumers said they shopped online grocery in 2017, up from 10% in 2016. That 37% year to year increase for online compares favorably with other formats.
The 2017 percentage of consumers who shopped at selected formats, with percentage change compared with 2016:
Grocery 86% (-2%)
Supercenter 58% (-8%)
Warehouse club 32% (-3%)
Limited assortment channel 26% (+3%)
Drug store channel 21% (-17%)
Dollar store 20% (-11%)
Natural and organic channel 18% (-2%)
Convenience 13% (+14%)
 
Still, the FMI report points out that online stores are the primary store for just 1% of all consumers, though 4% of Millennials indicate that online grocery is their primary grocery shopping format.
 The FMI survey said that 18% of all consumers purchased fresh fruits and vegetables from an online grocery store in the past 12 months. While that may not be a “tipping point,” clearly the trend for online purchases of food – and fresh produce – is headed higher.