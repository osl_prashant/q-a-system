The U.S. border with Mexico spans 1,954 miles, and ranchers are on the front lines for most of it.
For the past few decades, border enforcement and security has increased to halt illegal immigration and drug smuggling. In 1989, construction on the first major border fence began in San Diego stretching 46 miles east.
Later, the Secure Fence Act of 2006 was signed by President George W. Bush on Oct. 26, 2006, adding nearly 700 miles of fencing structures and more enforcement officials.
When President Donald Trump started campaigning, part of his platform was border security, much of it hinging on building a wall. The focus is still on building a wall despite pushback from members of Congress.
At the State of the Union, Trump laid out a four pillar plan on immigration with the second pillar focusing on securing the border. “That means building a wall on the Southern border,” Trump said, and hiring more border security.

There are 354 miles of primary pedestrian fence along the southern border, similar to this bollard-style fence near Nogales, Ariz. Secondary and tertiary fencing for pedestrian traffic totals 51 miles, and the remaining 300 miles of fence only halt vehicle traffic.
The debate is still ongoing with several legislative pieces being proposed that involve funding for more security. Wall prototypes have been built at a testing area in California with the wall sections 30' high and made of solid concrete.
In Arizona, hundreds of miles of fencing were built with the Secure Fence Act forcing traffic to more remote areas of the border. Building a wall is welcomed by some ranchers, but they say it won’t do any good without boots on the ground.
Roads First, Walls Later
Operation Hold the Line in Texas was a successful effort by Border Patrol to bring more enforcement and technology to the region starting in 1993. Th e following year Operation Gatekeeper in California began and illegal entries through San Diego were reduced by more than 75%. This was when ranchers like Dan Bell of Nogales, Ariz., started seeing an uptick in movement across the border.
“Border cities started to fortify and that forced a lot of traffic onto us,” Bell says. “We had always seen traffic from drug smugglers and aliens, but it compounded it as soon as they put in those border security measures.”
ZZ Cattle Corporation was established by Bell’s grandfather in 1938 and is now 35,000 acres. Most of the ranch is on U.S. Forest Service land west of Nogales and Interstate 19. Bell’s grazing allotments in the Coronado National Forest run along the border 10 miles past where Arizona’s border angles northwest.
Driving across Bell’s ranch takes less time than it did a few years ago because gravel roads have been built, widened and regularly maintained for Border Patrol agents. But it is still a rugged, hour-long drive to get from the ranch headquarters to the gate of the western pasture. 

Dan Bell standing with some of his commercial Angus replacement heifers, just a few miles from the Mexican border.
“It takes quite a bit of time to get around out here,” Bell says. “That is the biggest misconception about securing the border.”
Even a Border Patrol agent with a mobile surveillance capable vehicle, says it’s difficult to get to an ideal area for reception due to the lack of roads.
Bell’s neighbors to the west are Jim and Sue Chilton from Arivaca, Ariz., who also graze land along the border and have an even worse road issue.
“Just to go from our ranch house on the north end of the ranch to the international boundary it takes an hour and a half,” Jim says.
“That’s about 19.5 miles. In other words it’s not a freeway, it’s a rough, ranch road,” Sue adds.
The Chiltons would like to see better roads in and around their 50,000- acre ranch so enforcement officials can access the border faster.
Building a wall or any permanent fencing first requires roads for construction crews to reach the border. At the minimum a road needs to be built to run along the border to better patrol illegal crossings.

Jim Chilton walking the four-strand, barbed-wire fence that goes along his ranch’s southern border with Mexico.
“No Man’s Land”
Much of the traffic across the Tucson Corridor of the border is controlled by the Sinaloa Cartel. Jim and Sue estimate more than 200 trails go through their ranch and all are under the rule of the Sinaloa Cartel.
“About 20 miles into the U.S. is controlled by the Sinaloa Cartel. That’s where our ranch is, it’s in no man’s land,” Jim says.
Trail cameras are set up along three paths to help the Chiltons monitor activity on their ranch. They aren’t catching pictures of wildlife—mostly smugglers carrying large bundles of drugs. Last year 360 images of drug packers were captured.

Drug traffickers are spotted moving across the Chilton Ranch via a trail camera placed along one of an estimated 200 trails on the property.
Jim and Sue want more officers at the border. They have proposed leasing 10 acres of their property in the Forest Service allotments to the Border Patrol to set up forward operating bases. Their rental fee: $1 per year.
“The offer was made five years ago, and they are still studying it. If you can’t find the dollar, I’ll lend it to you,” Jim adds.
The next neighbor to the west made a similar offer, they add, and nothing has happened.
“Bottom line, the Tucson station is located 80 miles from the international boundary,” Jim says.
To check out a gun and be briefed, it might be an hour before an officer leaves the office. It then takes three hours to get to the border where multiple Cartel backpacker trails cross, leaving little time for the job.
Impacts on Ranching
Having unwanted visitors at their ranches on a regular basis has presented some interesting challenges for the Arizona ranchers.
The Bells and Chiltons have installed drinking fountains at their water sites for migrants. Prior to this, cattle waterers would be broken routinely, draining stock tanks, as migrants searched for water. Even now, plastic piping is cut open when groups can’t quickly locate a tank.
Another problem is the trash left behind. In some cases, cattle have eaten plastic bags while grazing.
“It stuffs up their digestive system and they die a horrible, cruel death. There is nothing you can do for them,” Jim says.

Dan Bell has to regularly fix the barbed-wire fence along the international border of his ranch. Most of the damage is from drug smugglers.
Wildfires started by those crossing illegally are also a problem. In 2011, three-quarters of Bell’s grazing allotments burned from 13 separate fires and only one wasn’t man-made. He attributes the majority of the fires to illegal immigrants warming themselves with a fire or drug smugglers trying to evade authorities.
After fires, the Bells and the Chiltons have to change their grazing programs because the Forest Service doesn’t want cattle grazing burned pasture, sometimes for two years.
Then, the cost of putting out fires is often passed to tax payers. Jim estimates at least $2 million was spent on firefighting on his ranch in 2017 because of fires started by smugglers and migrants.
“If you took out the cost to the Forest Service just from fighting Arizona border fires of recent years, you’d find that the border wall would pay for itself,” Jim adds.
“No Silver Bullet”
A solid wall can be seen in very few parts of Nogales. The border around the metro-area is primarily bollardstyle fence at least 18' tall made from 4"-square tubing or drill stem pipe. Th is is a preferred design because agents can see through the other side, and it is difficult to climb. It was built through the Secure Fence Act prior to the Trump administration.
The Bells have 2 miles of bollard fence running from the western edge of Nogales until it hits a steep hill and turns into a four wire fence with a Normandy-style fence in a water gap to prevent vehicle traffic.
The bollard fence has made a big impact on Bell’s ranch management.
“Before, we had a lot of problems with the fence being let down, almost on a nightly basis,” he says.
The neighbor’s cattle would cross from Mexico and graze his Forest Service allotments. “It really made an improvement when the bollard fence went in, and we could see the change from one growing season to the next.”

Accessing the border with roads is the most pressing issue to help secure remote areas. Another problem is communication and surveillance; truck-mounted mobile sensors are making a difference.
On the western half of the Bells’ ranch, the border is just a four-strand, barbed-wire fence. It continues west about 25 miles past the Chilton Ranch and a few others.
“We definitely need a wall. We definitely need roads, forward operating bases and better communications at the international boundary,” Jim says.
Jim and Sue believe a wall of some sort is necessary, but they understand a bollard fence might be the best route because agents can see who might be coming from Mexico. It should also be cheaper.
Feasibility and practicality is a concern for Bell when it comes building a wall. “There is no real silver bullet to solving the border security issue,” Bell says. “It is going to take man power, aerial assets and technology. My main big push is accessing the border with roads.”