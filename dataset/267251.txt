Grain lower and livestock higher
Grain lower and livestock higher

The Associated Press



Wheat for May fell 1.75 cents at 4.5575 a bushel; May corn was off 7.50 cents at 3.81 a bushel; May oats lost 2.50 cents at $2.30 a bushel; while May soybeans declined 22.75 cents at $10.1525 a bushel.
Beef and pork were higher on the Chicago Mercantile Exchange. April live cattle was up 1.05 cents at $1.1302 a pound; April feeder cattle rose 3.98 cent at 1.3450 a pound; while April lean hogs gained 1.40 cents at $.5307 a pound.