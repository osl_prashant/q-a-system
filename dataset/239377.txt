BC-OK--Oklahoma Weekend Planner,ADVISORY, OK
BC-OK--Oklahoma Weekend Planner,ADVISORY, OK

The Associated Press



Editors,
Wire Editors,
Photo Editors,
The AP's updated plan for the weekend:
............
MEMBER FEATURES:
FOR USE Sunday, March 4 and thereafter:
EXCHANGE-ADOPTED FOOTBALL PLAYER
TULSA, Okla. —A former University of Oklahoma captain has reunited with his biological parents. Ahmad Thomas says he was anxious when he met his biological parents face-to-face in early February. The reunion was more than two decades in the making. Thomas began digging into his biological family's information last winter. Thomas met his biological parents, Michael Samuel and Michelle Hooshing, on Feb. 2 in Wisconsin. Hooshing gave birth to her son when she was 13. By Eric Bailey, Tulsa World. SENT IN ADVANCE: 1,417 words.
EXCHANGE-BECOMING HOMELESS
OKLAHOMA CITY —Hundreds of families with children experience homelessness annually in Oklahoma City. The number of homeless families with children living in shelters and on Oklahoma City streets has grown steadily since 2014, and advocates expect that trend to continue this year. They point to the rapidly rising cost of rent as one of the key drivers of that trend. By Silas Allen, The Oklahoman. SENT IN ADVANCE: 1,408 words.
...
MEMBER FEATURES:
FOR USE Monday, March 5 and thereafter:
EXCHANGE-DISABLED SWIMMER
HARRAH, Okla. —An Oklahoma high school student, who's missing part of her left leg, says she finds hope in the pool. Seventeen-year-old Gabby Clabes is a senior and a swimmer at Choctaw High School. She says "swimming has kept my drive going." Her mother, Sandy Clabes, was 43 when she was pregnant with Gabby. She was advised to abort the baby after test results showed Gabby had three degenerative conditions that would affect the growth of her bones and brain. By Jacob Unruh, The Oklahoman. SENT IN ADVANCE: 876 words.
EXCHANGE-CATTLE BRANDING
OKLAHOMA CITY —The Oklahoma Cattlemen's Association says the number of conversations in the ranching industry about mandatory branding is increasing as beef prices rise. Oklahoma doesn't have a statewide standard for livestock identification. The Oklahoma Department of Agriculture, Food and Forestry urges ranchers to mark their property just as they would a license plate on a car. But some opposed to branding say "it should remain a personal decision." By Brian Brus, The Journal Record. SENT IN ADVANCE: 627 words.
The AP, Oklahoma City