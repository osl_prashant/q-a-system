Grain lower and livestock lower
Grain lower and livestock lower

The Associated Press



Wheat for May fell 17 cents at 4.5075 a bushel; May corn fell 7.75 cents at 3.75 a bushel; May oats was off 7.25 cents at $2.35 a bushel; while May soybeans declined 27 cents at $10.2250 a bushel.
Beef and pork were lower on the Chicago Mercantile Exchange. April live cattle was off 1.03 cents at $1.2022 a pound; March feeder cattle lost 1.52 cents at $1.3845 a pound; while April lean hogs fell 2.30 cent at $.6315 a pound.