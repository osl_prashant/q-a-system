Where marketing potatoes is concerned, "who" matters as much as "how," marketers say.
Demographics and cultures are factors in marketing strategies, they say.
"There are some demographic differences. There's also the changing population of the U.S. through immigration from other countries, and a changing taste profile," said Ralph Schwartz, vice president of sales for Idaho Falls, Idaho-based Potandon Produce.
"America is huge for russet potatoes, but if you go anywhere else in the world, you don't see russets sold to this degree," he said.
Some newcomers to the U.S. may be seeing russets for the first time, Schwartz said.
"There's times we have to get people used to some of the different potato varieties you can find here," he said.
Potato marketers are aware of the changing cultural face of the U.S. market and are responding assertively, Schwartz said.
"I think the world's changing and the potato industry has done a great job of introducing new varieties, responding to consumer wants on smaller, more unique potatoes and different flavor-profile potatoes," he said.
"No one in the industry is sitting on their heels."
Marketing strategies also vary for different age groups, uses and family sizes, said Randy Shell, vice president of business development for Russet Potato Exchange, Bancroft, Wis.
"Consumers are more aware of product waste and are turning toward smaller pack sizes," Shell said.
That trend is evident in generational fragments, Shell said.
"Millennials have moved toward smaller pack sizes as the average family size has gotten smaller and waste awareness is more predominant," he said.
"Although boomers typically buy larger amounts of potatoes, we have seen this age demographic shift towards smaller pack sizes, as well," he said.
The market's demographic mosaic presents opportunities for marketers to offer more product choices to fit consumer lifestyles, as well as variety preferences, said Christine Lindner, national saleswoman with Friesland, Wis.-based Alsum Farms & Produce Inc.
"Grower-packer-shippers are responding to the changing consumer taste and preferences by introducing value-added potato products to provide the consumer with ready-to-serve healthy potato offerings to meet the growing trends for fresh foods fast and flavorful," Lindner said.
Cross-merchandising covers an array of consumer tastes, said Ryan Wahlen, sales manager with Pleasant Valley Potato Inc., Aberdeen, Idaho.
"I think there has been some efforts made in terms of cross-branding and that type of thing. Especially with the growing Hispanic population, efforts have been made to introduce Spanish on the packaging," he said.