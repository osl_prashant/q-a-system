<!--
LimelightPlayerUtil.initEmbed('limelight_player_606547');
//-->

The Wisconsin dairy industry creates 78,000 jobs, including a unique family business that helps keep cheese curds coming to consumers.
For more on this story watch the video above from AgDay.