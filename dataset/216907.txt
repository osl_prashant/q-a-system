With 2018 right around the corner, we're looking back at some of our biggest stories from this year. Here's the top 10 most read stories on AgProfessional in 2017.

 
10. Monsanto Pulls Nemastrike for Skin Irritation Issues - November 1, 2017 - Sonja Begemann
9. AGCO Unveils Its IDEAL Global Axial Combines - September 8, 2017 - Margy Eckelkamp
8. A Look at the Weather Forecast for the Next 36 Hours - June 29, 2017 - Rhonda Brooks
6. AGCO to Acquire Precision Planting from The Climate Corporation - July 26, 2017 - Margy Eckelkamp
5. Insights on 2017 Cash Rents and Farm Operating Budgets - January 2, 2017 - Margy Eckelkamp
4. Pros and Cons of Granular and Liquid Fertilizer - January 10, 2017 - Rhonda Brooks
3. Deere Acquires Blue River Technology - September 6, 2017 - Margy Eckelkamp 
2. Case IH Introduces Trident 5550 Applicator - July 31, 2017 - Margy Eckelkamp
1. John Deere Unveils S700 Combines and New Corn Head and Platform - June 1, 2017 - Margy Eckelkamp
 
What stories had the biggest impact on you this year? What topics do you think will drive the conversation in 2018? Tell us in the comments, or on social media.