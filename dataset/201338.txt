USDA's Cattle on Feed Report showed all three categories on the bearish side of the average pre-report estimates. The July 1 feedlot inventory at 10.821 million head rose 465,000 head (4.5%) from last year and came in 164,000 head more than anticipated. The bigger-than-expected jump in feedlot supplies was driven by a stronger placements pace (up 16.0%) and slower-than-anticipated marketings (down 4.0%) compared to year-earlier levels.




Cattle on Feed


Avg. Trade Estimate


Avg. Trade Estimate


Range 




 
 
% of year-ago levels



On Feed

104


102.9


102.1-103.7



Placements

116


106.1


103.1-108.2



Marketings

104


104.7


104.1-105.2




A breakdown of June placements showed feedlots aggressively put lighterweight calves into feedlots. Lightweight placements (under 600 lbs.) jumped 29.3%, 6-weights were up 23.5% and 7-weight placements were up 26.5% from year-ago levels. Feedlots placed 2.7% more 8-weights on feed, while 9-weight and heavyweight placements were even with year-earlier levels. The report data is negative compared to expectations and combined with a big jump in the July 1 U.S. cattle inventory, will pressure cattle futures Monday.
Meanwhile, USDA's Cattle Inventory Report showed 102.6 million head of cattle in the U.S. as of July 1, up 4.5% from two years ago. USDA didn't release its midyear inventory report last year due to budgetary cutbacks. That figure was 1.6 million head more than we anticipated. The number of beef cows that calved is estimated at 32.5 million head, up 6.6% from two years ago. The 2017 calf crop is estimated at 36.3 million head, up 2.2 million head from 2015 and 400,00 head more than our forecast.





Cattle Inventory Report 




USDA




USDA





million head 
 


percent of 2015
 




All cattle & calves



102.6




104





2017 calf crop



36.3




106





Total cows/heifers calved



41.9




105





beef cows/heifers calved



32.5




107





milk cows/heifers calved



9.4




101





Heifers 500 lbs. and over



16.2




103





Beef replacement heifers



4.7




98





Milk replacement heifers



4.2




100





Other heifers



7.3




109





Steers 500 pounds and over



14.5




103





Bulls 500 pounds and over



2.0




105





Calves under 500 pounds



28.0




105





While the U.S. cattle herd continued to build into midyear, there are signs the expansion phase is ending. Beef replacement heifers at 4.7 million head are down 2.1% from two years ago and 400,000 head less than we anticipated. USDA's Cattle on Feed Report also showed heifer numbers in feedlots as of July 1 up 10.6% from year-earlier levels, signaling cow/calf operators have slowed retention rates and are selling more females for beef production.