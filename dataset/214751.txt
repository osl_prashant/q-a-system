Organic citrus isn’t common in Texas, and Dennis Holbrook has been one of the few organic grower-shippers in the region for decades.
It’s a lot of work, but Holbrook, president of South Tex Organics LC, says it’s worth the trouble.
“I just kind of keep my head down and do my own work,” said Holbrook, whose primary focus on his 500 acres is citrus.
Eighty-seven percent of Holbrook’s citrus production is grapefruit, with oranges and a limited volume of meyer lemons comprising the rest.
“I think we’re pretty much on a status quo or slight growth over the last year or so,” he said. “There hasn’t been a lot of growth in the acreage at this point.”
After Hurricane Irma knocked out as much as half of Florida’s citrus production in early September, Holbrook found himself in an enviable position on two fronts — he had product that already was in high demand and a near-lock on the organic category in his region.
“When you look at the overall picture with citrus, with loss of acreage in Florida due to greening and the hurricane knocking so much fruit off, it looks like citrus is going to be in shorter supply — that’s good for Texas growers,” he said.
South Tex also has an organic juicing plant, which enhances its revenue potential, Holbrook said.
“There seems to be a lot of interest in the juice market,” he said. “That’s good for the fresh side, as well as the process side. Demand is going to be strong for citrus, partly because of the limited supplies.”
More retailers are calling for organic product these days, Holbrook said.
“We’ve dealt with chains that are not traditionally the Whole Foods or Sprouts (Farmers Market) or those types of markets,” he said.
“Some of our distributors go through Kroger and some other larger chains, and we also do some business with chains in Canada.”
South Tex works to meet that growing demand, Holbrook said.
“I think the main thing is, as the demand increases, people have to go to find supplies for that, so we’re in a growing mode trying to find acreage for citrus, as well as other crops we’re looking at expanding into,” he said.
Dale Murden, president of the Mission-based Texas Citrus Mutual, estimated south Texas has about 1,000 acres of organic citrus in a region that totals 27,000 acres of citrus production.
“I’ve actually got a grove that’s organic. The quality in that grove is really good,” Murden said.
Others have voiced interest in organic citrus production, Murden said.
“There are a couple of guys talking about converting to organics, so it could grow,” he said. “There is interest in it, obviously.”

Want to know more about organic produce? Register for The Packer’s inaugural Global Organic Produce Expo, Jan. 25-27, in Hollywood, Fla.