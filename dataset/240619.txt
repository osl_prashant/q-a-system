Relentless winter conditions pose risks for Montana cattle
Relentless winter conditions pose risks for Montana cattle

The Associated Press

GREAT FALLS, Mont.




GREAT FALLS, Mont. (AP) — Ranchers in parts of Montana hit by a relentless winter are struggling to keep to their cattle alive as snow is hindering access to feed.
The Great Falls Tribune reports a series of winter storms that began in January and continued through February have caused extreme temperatures and record amounts of snow in areas of northern Montana.
The Blackfeet Reservation, where 38,000 head of cattle graze, is among the areas hardest hit.
The blizzard-like conditions have stranded people in remote areas and prevented livestock producers from reaching their cattle with feed.
Blackfeet Nation Stockgrowers Association Chairman Joe Kipp says this winter has the potential to kill thousands of cows.
Montana State University extension agent Verna Billedeaux says the number of cows that have died isn't known yet.
___
Information from: Great Falls Tribune, http://www.greatfallstribune.com