With the delayed harvest and the wind over the last few weeks a lot of corn ears are on the ground. This means a lot of energy remains in corn fields, creating potential issues with founder/acidosis. Founder can have long-term consequences of reduced cow longevity.
Opportunity: Capitalize on the grain (energy) by grazing calves.
Calves do not initially seek out ears in the field as this is a learned behavior. They are more likely to self-adapt by slowly increasing the amount of gain they consume each day. Although there is some risk of acidosis, feeding them corn and working them up to 5 lbs/calf of grain before turnout should help. Calves eating only corn grain and residue will not have enough protein in the diet to make full use of the energy available. Feeding 2 lbs of distillers can increase gain substantially and increase returns. Using the calves to glean the majority of grain and moving them from field to field before the cows may be a great way to reduce risk and make money at the same time.
Opportunity: Using cull cows to glean the fields?
Putting weight on cull cows can increase revenue and using them in corn fields ahead of the rest of the cow herd can reduce the risk of long-term negative effects of founder/acidosis in the remaining cows in the herd. It might even be worth buying thin cull cows, grazing them through, and selling fat cull cows at the end. Before turnout, producers should start feeding grain and work them up to at least 7 to 10 lbs/hd of grain over a week to 10 days. Then give them access to the number of acres to limit to 10 lbs/cow for five to seven days then move to 15 lbs/cow for five to seven days (see table). After this point, they can have unlimited access to the field. These cows can be moved to the next field once most of the corn grain has been grazed. There is still risk of acidosis/founder, but it will be less with this adaptation period.
 Corn Ear Drop Allocation
Don't want to feed grain before turn out? You can strip graze the downed ears to adapt cattle to the grain. Download the spreadsheet, input the bu/ac of down corn in the field as well as the number of animals in the group to calculate the amount of acres to allocate for various corn intake amounts.

Quick guide to allocating corn residue with excessive ear drop to cows


EARS IN 300 FT
GRAIN (BU/AC)
GRAIN (LB/AC)
LIMIT 10 (LBS/DAY)
LIMIT 15 (LBS/DAY)




20
10
560
1.8 (Acres per 100 cows)
2.7 (Acres per 100 cows)


30
15
840
1.2 (Acres per 100 cows)
1.8 (Acres per 100 cows)


40
20
1120
0.9 (Acres per 100 cows)
1.3 (Acres per 100 cows)


50
25
1400
0.7 (Acres per 100 cows)
1.1 (Acres per 100 cows)


60
30
1680
0.6 (Acres per 100 cows)
0.9 (Acres per 100 cows)


70
35
1960
0.5 (Acres per 100 cows)
0.8 (Acres per 100 cows)


80
40
2240
0.4 (Acres per 100 cows)
0.7 (Acres per 100 cows)


90
45
2520
0.4 (Acres per 100 cows)
0.6 (Acres per 100 cows)


100
50
2800
0.4 (Acres per 100 cows)
0.5 (Acres per 100 cows)



Problem: Feeding spring-calving cows.
In addition to the concern with having cows founder or get acidosis, there is also an issue with cows getting fat. Not weaning before turnout is an option to help increase the cow's energy requirement.
The best option, regardless of whether you wean, is to limit access to corn by strip grazing daily. Those with a pivot fence can readily use this system. Allowing the pairs access to 10 lbs of corn and feeding 2 lbs of DM from distillers grains should allow the cows to maintain bodyweight and the calves to gain 1.0 to 1.5 lbs/day. If cows are weaned, limiting cows to no more than 10 lbs/day of grain will allow them to increase their body condition score (BCS) by 0.5 to 1 over the winter. Before turnout, producers should start feeding grain and work cows up to at least 7 lbs/head of grain over a week to 10 days.
If daily allotment is just not an option, the next best option is to work cows up to a full grain diet as suggested above with the cull cow feeding. However, if you are going to need to move to a new field over the winter (based on stocking rate and the amount of residue in the field), there will likely be an issue when all the grain has been grazed and the cows are eating only residue. When they are moved to a new field, they will have full access to grain again but their rumen will no longer be adapted. Thus, there are two options:

Once cows are acclimated to full grain, they could be split into groups on multiple fields that they will graze for the rest of the winter.
All cows could be quickly moved through all fields, allowing them to harvest most of the grain. Once most of the grain has been gleaned from all the fields, the cows can be moved back through the fields to make use of the rest of the residue. 

Regardless of the class of animals grazed or the method used, providing monensin can be beneficial. If no supplemental feed is provided, using a free-choice mineral with monensin in it can help. 
For information on how to estimate the amount of ear drop, see: Estimating Bushels of Corn on the Ground by Counting Ears Prior to Grazing with Cattle. For more information on using a pivot fence to limit access, see: Feeding Cattle Forage Using Electric Fence as a Management Tool.