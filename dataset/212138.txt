The first quarter 2017 United Fresh Produce Association FreshFacts on Retail Report shows a dip in produce sales in the first quarter as price declines for both fresh fruit and vegetables led to total produce sales falling about 1% compared with the first quarter last year. 
The report, available for free online download for United Fresh members and $50 for non-members, features category reports on melons, berries, stone fruit and a closer look at produce purchases at convenience stores, according to a news release.
 
The report, sponsored by Del Monte Fresh Produce and produced in partnership with Nielsen Fresh, reported organic produce contributed to just shy of 10% of all produce sales. All of the top ten organic commodities seeing increased sales in the first quarter, according to the release.
 
The report said fresh produce marketers notched about $425 million in convenience store sales in 2016, but a decline in distribution of items carried in some categories stalled sales after two years of growth.
 
The report said sales could grow further as offerings expand beyond traditional items.
 
“Consumers are seeking healthy options, but in today’s ‘on the go’ lifestyle, they also prize convenience,” Jeff Oberman, United Fresh vice president of trade relations and United Fresh’s Retail-Foodservice Board liaison, said in the release. “There is great potential for produce companies to find success in the convenience sector, and United Fresh will continue to work with our members and convenience and alternative channel retailers to help drive success for fresh produce in these stores.”