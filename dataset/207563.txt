Aldi has partnered with Instacart to test grocery delivery in several markets, and analysts see the move as a signal that Aldi is upping its game.“It gives you an idea that their customer base and the company has evolved significantly in the last three to five years,” said Scott Mushkin, managing director for Wolfe Research. “Aldi competes more and more with Wal-Mart, more and more with Kroger.
“Those companies are offering pickup and delivery, and so I think Aldi did this as a way to say, ‘We can do that too if that’s what you want,’” Mushkin said.
Aldi has already commenced a $5 billion spending spree to remodel existing stores and increase its store count from 1,600 to 2,500 in the next five years.
Grocery delivery is simply the next step, according to Kantar Retail analyst Elley Symmes.
“It’s no longer a fringe capability,” Symmes said. “You really have to have some sort of service and offering in this space no matter if you’re Whole Foods or Aldi or Kroger or Amazon. Everyone needs to be playing, and it’s moving at a rapid pace.
“Aldi finally recognized that, and they’re trying to elevate themselves in this space and resonate with shoppers who are demanding this service,” Symmes said.
The retailer plans to pilot grocery delivery in Atlanta, Dallas and Los Angeles, with the possibility of later expansion.
“This is a really smart move by Aldi,” said Mike Paglia, director of retail insights at Kantar. “It shows how much, just how potentially disruptive they can be, and I think you can expect more of that in the future.”
 
Converting consumers
Analysts expect that increased awareness of Aldi prices, through their listing on the Instacart app, will likely be beneficial for the retailer.
“In many cases, this will be a big surprise to (consumers) and probably prompt trial,” said Bill Bishop, chief architect at retail consulting firm Brick Meets Click. “Given the importance of digital communications, particularly with younger customers, this could be a big boost to Aldi sales.
“My guess is that it could easily be worth a 3%-5% bump in sales over the first month or so, which is a pretty big deal,” Bishop said.
Paglia had a similar view.
“This could be a bit of an unveiling, I think, for Aldi on the national stage,” Paglia said.
 
Instacart smart
Some retailers have built their own infrastructure for delivery, but Symmes said Aldi had at least two big reasons to outsource.
First, working with Instacart allows Aldi to dip its toe in the water, so to speak, without making a massive investment in delivery. Second, it allows Aldi to enter the delivery space quickly, without the lag time it would take to develop a system in-house. The same goes for other retailers that work with Instacart and similar companies.
There is a potential downside to this approach, however.
“They’re relinquishing this control of their business in exchange for speed,” Symmes said. “What we’re seeing on a mass scale with all sorts of grocery retailers and now discounters is that they’re willing to take on this risk — and that the risk of not playing in the space and not offering this service to shoppers, almost overnight, is higher than some of the risk that could come with having someone else be given a portion of their brand equity in the delivery space.”
Mushkin said working with Instacart also makes sense because only a small percentage of Aldi shoppers will likely be buying online from the retailer.
 
Long-term outlook
More and more retailers are adding delivery to their suite of services, and analysts do not foresee the movement slowing.
“What (Aldi’s entry) shows is that it’s not a fad,” Paglia said. “I think it’s growing more and more popular by the year, and every time another retailer partners with Instacart, whomever, it’s just a reflection of the growing momentum.”
Erick Taylor, president and CEO of Pyramid Foods, said during the Midwest Produce Expo that he envisions 30-40% of grocery will be e-commerce in the next decade.