Air is the unseen, pervasive enemy of silage. After carefully growing and ensiling forage, growers can still experience substantial dry matter (DM) losses through mismanagement of the silo face. Each time silage is exposed to air, yeasts that have survived the ensiling process can begin to grow and initiate aerobic spoilage.
These avoidable DM losses can have a significant effect on the bottom line. For example, 1,000 tons of bunk silage at 35% DM with a 10% DM loss overall will lose 35 tons of silage DM. This lost feed is worth around $5,000 on a lost DM basis. In fact, this loss is even more valuable since the most digestible, highest-value nutrients are lost first. This loss of nutrients means producers will likely need to purchase additional feed to compensate.
Managing the silo face during feedout is an important factor in the overall stability of the feed and can help avoid unnecessary oxygen exposure. Tips to effectively managing the silage face include:

Use a block cutter/defacer/rake. Using a bucket to “knock silage down” will loosen the silage behind the face, allowing air to penetrate. This can lead to increased yeast activity, greater DM and feed value losses, and enhance the probability of heating. On the other hand, using a defacer, or a rake, or a block cutter, or “shaving” the face from side to side will minimize the penetration of air into the silage. 
Keep the pile face tidy. Maintaining a flat bunker face reduces the area exposed to air and avoids dangerous silage overhangs that can fall and cause injury.
Only remove silage to be fed at a single feeding. Avoid leaving silage sitting in piles. Piles can quickly become compost heaps!
Discard spoiled silage. Feeding moldy silage can result in health/reproductive problems for the herd and production losses.

Additionally, consider including an inoculant during ensiling that is proven to help increase the stability of the silage at feedout. Some inoculant strains, like Lactobacillus buchneri 40788, can improve feed-out stability and reduce spoilage when used as directed. With good face management techniques and a proven inoculant, producers can help prevent DM losses and improve profitability.
Ask the Silage Doctor at QualitySilage.com, @TheSilageDoctor and at facebook.com/TheSilageDoctor if you have questions about face management.
 
Sponsored by Lallemand Animal Nutrition