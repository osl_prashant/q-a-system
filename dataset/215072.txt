“Where DO you come up with your merchandising ideas?”
Every produce marketer I’ve even been associated with has, at one time or another, had that question presented to them. Some shrug their shoulders and try to hide a modest smile. For many of these produce retailers, their talent, their merchandising flair comes naturally.
For the rest of us mere mortals, there is hope.
If I had to advise an up-and-coming produce clerk or assistant manager on the topic, I’d simply offer this: Pay attention.
As in, do you receive print or online produce publications? If not, invest in your career and subscribe. Most trade journals or newspapers (including The Packer, of course) often highlight exceptional produce displays throughout the country and beyond. You can find ready inspiration here.
Do you pay attention in your competition walks? Even if not mandated, visit your competitors often — at least once a week. Squeeze in a stop on your way to or from work. Look around and see what they’re up to. You may discover a produce display with some elements that you can incorporate into your own. If not right away, then at some point in the future.
Do I mean steal ideas? Yeah, steal away. But add your own touch to the finished product. 

From the humble farm look to the abstract, merchandising is an art form.

I had a produce manager friend who once built a massive pineapple display for a contest. He decorated, using a tropical theme, with faux palm trees and electric Tiki torches. Nothing new there. But he dimmed the overhead lights, added a large paper mache, dry ice “smoking” volcano, and played a continuous flute band music loop. He won first prize.  
Sometimes produce merchandising ideas come from unlikely sources too.
For example, I recently received my annual Monte Package Co. catalogue. They supply all sorts of packaging products for produce grower-shippers. 

Yet I look through it and see all sorts of unique items that can be used for display ideas. Props such as wirebound crates, bushel baskets, orchard bins, field crates, barrels, wood hampers, trays, baskets, etc. Most of these are relatively inexpensive, reusable, and your produce buyer likely has contacts for this or comparable suppliers.
It’s applicable for that special display, grand opening touch or for everyday use.
Produce merchandising ideas can spark from so many sources (I even spied ideas from, of all places, clothing stores): shipper containers, farm implements, burlap bags. From the humble farm look to the abstract, merchandising is an art form. Yet one that can be adopted and adapted.
If you pay attention, that is.  
Armand Lobato works for the Idaho Potato Commission. His 40 years’ experience in the produce business span a range of foodservice and retail positions. E-mail lobatoarmand@gmail.com.
What's your take? Leave a comment and tell us your opinion.