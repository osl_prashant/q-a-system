When Missouri cattleman Nick Jones sets out to try his hand at something new, he goes all in — whether it’s team roping or building his cow/calf operation from the ground up in southwestern Missouri.
Jones returned home from college one summer, and with not much to do, he and his uncle set out to the local roping arena. While on the back of his uncle’s horse, a gentleman approached him, saying, “The next time you come, you either have your own horse or bring your own rope and I’ll have you a horse.”
“So, I did it. The next week I brought my own rope and one of my uncle’s roping horses,” Nick Jones said, reminiscing. “It took off from there. Roping just came naturally to me since I played baseball; it was all hand-eye coordination.”
Jones grew up helping his father with cattle on the ranch, but his interest in building his own cow/calf operation developed in college. Soon after he first swung a rope on his uncle’s horse, he was awarded a full-ride rodeo scholarship for team roping.
“I went to Fort Scott Community College in Kansas on a rodeo scholarship and started helping some feedlots out there and got more involved in the industry,” Jones said. “Kansas was a different lifestyle than Missouri. It was all about farming and ranching. Anytime someone needed wild cows caught, we’d go catch them and help round up cattle. Anything we could do to make extra money in college. I think that sparked my interest [in raising cattle] maybe more than growing up on the farm.”
Building a successful operation
After college, Jones set off to build a farm of his own, beginning with 12 longhorn cows.
“We rented ground for six years until we had enough cows paid for that we could buy our own land,” Jones said. “I was driving an hour one way just to feed cows so we could sell and save enough to buy our first farm. We’ve put three farms together since then.”
Now, Jones runs Black and Red Angus cow/calf pairs across nearly 500 acres. To help guarantee his herd’s health and performance, Jones works closely with his veterinarian, Max Hartman, DVM, and the team at Animal Medical Center in Marshfield, Missouri.
“When you’re buying farms and have to find enough money to make farm payments, you find ways to sell bigger calves at market to bring more money in,” Jones said. “Needing to find more money to help make farm payments sparked my interest in implants.”
Jones talked with Dr. Hartman to see what others were doing to increase gain in calves.
Implanting calves, increasing gain
Dr. Hartman and team helped Jones implement an implant program for his calves. For three years now, his 9-week-old steer and heifer calves have been implanted with Synovex® C. At weaning, steers receive Synovex S. He credits the implant program for added gains and is pleased with not only the added weight gain but also the successful heifer conception rates he continues to see.
Jones keeps back 10 to 30 heifers every year as replacements, and he’s seen great breed-back results following implanting. Synovex C is safe when used in suckling heifer calves, allowing cattlemen the flexibility to increase gain and profit for their operation without affecting reproduction.
“If your goal is to get your heifers to breed at 15 months of age and have a calf at 24 months — and that should be our goal in the commercial beef industry — there’s nothing that’s going to impact your bottom line more than having a heifer that doesn’t breed,” Dr. Hartman said. “If they don’t breed because they didn’t develop well, or didn’t obtain an optimum body weight, then that’s huge.”
“I don’t see why more people aren’t implanting when you can spend $2 to gain almost 20 pounds,” Jones said. “This is better odds than making money from a scratch-off ticket. Between genetics and Synovex, we’re weaning bigger calves than most people around here.”
Five tips from Nick Jones and Dr. Hartman on developing a successful implant program:

Disregard any misconceptions you might have heard. It is not “wrong” to implant.
Have a defined breeding season for more uniform calves.
Take advantage of time spent vaccinating and processing calves by implanting to help increase gain.
Prioritize nutrition at both the cow and calf levels. Cows require energy and protein to provide ample milk to help calves achieve a healthy, productive start.
Talk with your local veterinarian about developing an implant program that is right for your cattle.

When it comes to team roping, Jones hasn’t hung up his hat. He still ropes after church on Sunday and he raises roping stock, Corrientes, and credits his implant program for getting calves to roping weight earlier than anyone around.
To learn more, visit with your local veterinarian and Zoetis representative. For more resources on implanting, check out GrowWithSYNOVEX.com.
Do not use SYNOVEX products in veal calves. Refer to label for complete directions for use, precautions, and warnings.