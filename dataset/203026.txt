(UPDATED, Feb. 2) As controversy swirls around the idea of an import tax (among other things) to pay for a border wall with Mexico, a group of U.S. agricultural groups is urging President Trump to preserve and extend the gains of the North American Free Trade Agreement.
 
A Jan. 23 letter to Trump - organized under the U.S. Food and Agriculture Dialogue for Trade - was signed by United Fresh Produce Association, Western Growers, the U.S. Apple Association, the Northwest Horticultural Council, the National Potato Council, the Fresh Produce Association of the Americas and many other commodity and agricultural organizations.
 
"Although some important gaps in U.S. export access still remain, increased market access under NAFTA has been a windfall for U.S. farmers, ranchers and food processors," the letter said. U.S. food and agriculture exports to Canada and Mexico have more than quadrupled, growing from $8.9 billion in 1993 to $38.6 billion in 2015, the letter said.
 
Trade with Mexico and Canada is generally free of tariffs and quotas, the letter said, which helps U.S. growers expand exports.
 
"The U.S. food and agriculture industry looks forward to working with the new administration in preserving and expanding upon the gains our sector has achieved within the North American market and strengthening our competitiveness around the globe," according to the letter.
 
NAFTA has been a "slam dunk win" for U.S. agriculture and the apple industry, said Diane Kurrle, senior vice president for the U.S. Apple Association.
 
Prior to NAFTA, U.S. apples faced a 20% tariff; now they enter duty free. From 1993 to 2015, U.S. apple exports to Mexico tripled while exports to Canada nearly doubled, according to trade numbers. In 2015, Mexico purchased 27% of all U.S. apple exports, up from 19% in 1993, according to the USDA, while Canada's of U.S. apple exports share stayed about the same, at about 17% in 2015 compared with 18% in 1993.
 
Mexico is the leading export market for U.S. apples, taking more than 15 million 40-pound cartons in 2015.
 
Many importers in Mexico now are mainly concerned about the instability of the peso, said  Tom Riggan, CEO and general manager of Chelan Fresh, Chelan, Wash. The current exchange rate for the peso with the U.S. dollar is 22 to 1, he said. That compares with an exchange rate of less than 15 pesos to $1 about two years ago.  
 
"That has hurt their returns," he said. "They are selling the apples for pretty good prices but the valuation of their currency reduces their return. They want stability and want to know what is going to happen."
 
Importers in Mexico don't want to lose the opportunity to buy U.S. apples at zero duty, he said.  
 
NAFTA successfully removed trade barriers and tariffs, Ron Lemaire, president of the Ottawa-based Canadian Produce Marketing Association.
 
"We have a tariff-free market for fruits and vegetables and we have seen great success on the growth of our industry, and while NAFTA isn't the sole catalyst, it is a component," he said.
 
Canadians are enjoying more imported fruits and vegetables than ever before.
 
"We see the consumer drive for continuous year-round supply of product, and for the trade component necessary to achieve that, NAFTA was successful," he said. 
 
Lemaire says the NAFTA agreement has contributed to the tri- and bi-national entities in the produce industry, such as greenhouse companies that operate in all three countries.
 
Harmonization efforts, including the Dispute Resolution Corporation, were created after the NAFTA agreement was signed, he said. 
 
If the agreement is renegotiated, Lemaire said there could be an opportunity to improve and modernize the agreement relative to food safety regulations and allowed maximum pesticide residue levels.
 
 "It is important when renegotiation happens , and we are asked for opinion, that we are very forceful to validate open and free market for trade is vital," he said.
 
Lemaire said any change in the trade agreement that would remove that benefit to buyers and would invite produce supplies from other regions in the world.
 
The Florida Fruit and Vegetable Association does not have a position on the NAFTA renegotiation, according to a spokesperson. One Florida marketer said another look at the trade agreement may be worthwhile.
 
It is a good thing for the Trump administration to look to protect U.S. interests, said Jack Gautier sales representative with Homestead Pole Bean Cooperative Inc., Homestead, Fla. At the same time, he said the U.S. needs good trade relationships with its neighbors.