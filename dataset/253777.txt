Colorado's quest to tackle dangerously unhealthy forests
Colorado's quest to tackle dangerously unhealthy forests

By JACY MARMADUKEThe Coloradoan
The Associated Press

FORT COLLINS, Colo.




FORT COLLINS, Colo. (AP) — There is life after death for Colorado's forests.
But to get there, the people who manage them must solve an economic quandary.
Colorado's 834 million dead trees can start anew as your favorite rocking chair, the mulch in your garden, heat for Front Range cities — you name it. The problem is: The cost of removing and transporting them can dwarf the worth of their wood.
Dead standing trees make up about 1 in 15 standing trees on Colorado's 24.4 million forested acres, according to 2016 data from the Colorado State Forest Service. And in 2017, invasive pests like the spruce beetle continued to whittle away the state's forests. The spruce beetle infested 206,000 acres last year, bringing the pest's toll to 1.78 million acres since 1996.
"You can't remove dead tree material at this scale," said Seth Davis, an assistant professor of forest and rangeland stewardship at Colorado State University, referring to trees killed by the spruce beetle. "It gets to be such a significant event that there's really no way for management agencies to deal with this material."
But forest workers and landowners say they have to: In a state where overcrowded forests exacerbate wildfire risk and threaten devastation to key watersheds, dead standing trees are everybody's problem.
"There is a way out of this," Colorado State Forester Mike Lester said. "But it's not going to be easy, and it's not cheap."
Colorado is home to 102 sawmills, about one-third of which use beetle-killed trees to create lumber and other products, according to the Colorado State Forest Service's 2017 Report on the Health of Colorado's Forests. But the state's larger mills tend to be farther from the supply of trees, which makes transportation more costly. Add in the high costs of extracting trees from high-elevation and rocky terrain and you end up with a steep bill for wood that generally isn't worth much to begin with.
"Here, if you want to sell timber, you're probably going to have to pay someone to take it off your hands," Lester said. "That's unique to the interior Rockies."
New mills in strategic locations could be the answer, but Lester said capital for those projects can be elusive because of uncertainties about supply. Some would-be lenders or operators might worry that the supply of trees could dry up in a few decades, which isn't exactly a recipe for long-term success in the milling business.
Simply leaving trees alone can lead to fires that burn more intensely in beetle-killed stands and spread more rapidly through overcrowded areas with dangerous jackstraw patterns. That's not a risk foresters are prepared to take in Colorado, where more than one-third of residents live in what's known as the wildlife-urban-interface, Lester said.
Tempering the risk of overcrowded forests can run the gamut, from cutting down and selling trees to burning piles of them or starting controlled fires in forested areas. Faced with limited funding and a small workforce, though, forest agencies have to be strategic.
That means focusing their efforts on areas more prone to fire with highly erodible soil — forests that can be especially damaging to rivers and reservoirs if set ablaze, Lester said. The Colorado State Forest Service, U.S. Forest Service and private landowners have also gotten better at working and planning together across property lines and partnering with nonprofits, water districts and other stakeholders, he added.
One example is the recent release of a document designed to help agencies prioritize their efforts to remove dangerous fuels from Front Range forests and guide restoration of damaged areas. The paper's 17 authors, who work for a range of government agencies, nonprofits and schools, set out to define best practices for a unique part of the country where priorities can sometimes conflict with each other, lead author Rob Addington of The Nature Conservancy said. What's best for the ecology of a forest might not be best for the concerns of the people who live near it, for example.
"There are invariably trade-offs," Addington said. "In a lot of ways, it comes down to what's most important in a given landscape."
Efforts to fend off the spruce beetle — Colorado's most destructive forest pest for six years running — must be similarly strategic. Treatments exist to combat the spruce beetle, but on a grand scale they can be expensive, labor-intensive and bad for the environment, Davis said.
But a researcher in Davis's lab, CSU graduate student Andrew Mann, is studying the biological makeup of an insect-slaying fungus that could eventually be used as a preventive tool on a small scale. Workers could one day use it to prevent infestation in particularly vulnerable stands where winds have knocked over trees, Davis said.
Still, challenges persist: Changes in drought patterns and rising temperatures are expected to increase fire risk and alter the makeup of forests. The U.S. Forest Service spends more than half of its budget fighting wildfires, leaving little money for management efforts that could prevent fires.
"It's a losing situation," said Lesli Allison, executive director of the Western Landowners Alliance. "The less we can invest up front to reduce the threat of fires, the bigger the fires become and the more we have to spend suppressing them."
Various congressional proposals could address the U.S. Forest Service budget to allow more money for management, Allison said. If the forest service had more money to remove trees, the resulting wood supply could incentivize new mills — and more mills would make management efforts cheaper, she said.
But she also called for the support of private landowners, who oversee 7 million acres of Colorado forests and face economic challenges similar to those that hamper government agencies.
Managing overcrowded forestland can cost anywhere from a few hundred to a few thousand dollars an acre, Allison and Lester said, and grant money covers a fraction of landowner costs. Allison said she'd like to see more money in the federal Farm Bill to support forest stewardship, which could include tax incentives for new mills.
Colorado's residents and government need to invest in the state's forests, Lester said. Colorado forests were largely unscathed last year as historic wildfires raged across the West. But that's because we got lucky, he said, not because our forests are in fighting shape.
Lester doesn't want to rely on luck.
"We have a lot of things we need to do in this state, and it's really easy to put the health of our forests in the background," he said. "But I don't think we can do that forever. if we're not careful, there will be a time when we'll wish we'd done the work we needed to do."
But he hopes it doesn't come to that.
"I hope we have enough vision to say this is something we need to pay attention to."
___
Information from: Fort Collins Coloradoan, http://www.coloradoan.com