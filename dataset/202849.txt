Calling the Arctic apple a pivotal case for biotechnology companies, Rabobank has issued a report looking at the prospects for the genetically engineered non-browning apple.
 
The Rabobank report said limited marketing of the Arctic apple in 10 retail stores in the U.S. Midwest in February may be a key development for the future of GMO produce.
 
"If successful, it could trigger a wave of innovation in the fruit sector," according to the report. "Product innovation is, and will continue to be, a key success factor in the competitive fruit market, especially in high-cost production countries."
 
Neal Carter, founder and president of Arctic apple developer Okanagan Specialty Fruits Inc., Summerland, British Columbia, said in January the company was preparing to test-market its first non-browning apples as a sliced product in retail stores in the Midwest.
 
A wider launch is anticipated for the fall, Carter said in January. Carter did not name the stores in the Midwest, locations or specific dates when the Arctic apple will be sold.
 
According to the company, it had planted 70,000 trees of both Arctic Golden and Arctic Granny apples by the end of 2016. It has 300,000 Arctic Golden and 500,000 Arctic
Granny trees under contract in North America for planting in 2017 and 2018, according to a news release from Okanagan Specialty Fruits.
 
When the trees reach maturity, they will produce 30 million pounds of apples annually, or about 750,000 40-pound cartons, according to the release.
 
A test for GMO
 
Rabobank analysts said the Arctic apple will test consumers' willingness to purchase a fresh, genetically engineered produce item which delivers a "consumer-centric characteristic."
 
"Organic, 'clean label' and natural foods - regardless of what those terms actually mean - are thriving, while GM foods are under fire," Rabobank fruit and vegetable analyst Cindy van Rijswick said in a news release. "Consumers are skeptical when it comes to GM food, despite the fact that it already constitutes part of their daily diets in the form of GM food ingredients and animal feed."
 
Rabobank researchers said that Okanagan is putting a lot of effort into marketing the apple.
 
"If the apple doesn't turn out to be a success, it is not for a lack of openness," according to the report.
 
The apple packages do not contain specific wording about genetically modified/engineered, but they have a quick-response code which leads consumers to information regarding the development of the apple. The Arctic apple website  says the technique of "gene silencing" is used to reduce the expression of the enzyme responsible for browning and provides links further explaining agriculture biotechnology.
 
How consumers process the information will be important, the report said. 
 
"Some in the apple industry fear that, if the Arctic apple is not clearly labeled and distinguished from non-GMO apples, then the entire apple category will suffer due to consumer fear and uncertainty," Roland Fumasi, Rabobank fruit and vegetable analyst, said in the release.