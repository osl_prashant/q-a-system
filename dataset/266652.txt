UMaine's top blueberry scientists to retire in early 2019
UMaine's top blueberry scientists to retire in early 2019

The Associated Press

ORONO, Maine




ORONO, Maine (AP) — The University of Maine says a horticulturist who studies one of the most important crops in the state is getting ready to retire.
David Yarborough is the University of Maine Cooperative Extension's wild blueberry specialist. He's a well-known authority on the fruit, which is one of the biggest agriculture products in the state and the source of a handful of summer festivals.
The university says Yarborough is transitioning to retirement in early 2019. It says he will be replaced by Lily Calderwood, who is a new wild blueberry specialist and assistant professor of horticulture with UMaine.
Calderwood says she's looking forward to learning under Yarborough in the coming year and identifying the needs of Maine's wild blueberry growers. Maine's the biggest wild blueberry state in the country by far.