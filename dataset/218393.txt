The Produce Safety Alliance has been a part of hundreds of grower training workshops on the produce safety rule, and there are many more training courses ahead.
The alliance reports that since September 2016, its trainers-of-trainers, lead trainers and trainers have hosted 563 grower trainings, 12 integrated trainings (Grower & Trainer Course in one), and 36 train-the-trainer courses.
Grower trainings on the produce safety rule are available in more than 20 states and two foreign countries in just the period from Jan. 16 to Jan. 31.
A complete list of upcoming training courses is available at the Produce Safety Alliance website.
 
Course materials
The alliance said that its staff reviews and evaluates all training materials to make sure they are accurate and helpful.
So far, the alliance has issued its original training materials and a revised Version 1.1 of the PSA Grower Training Manuals.
The alliance does not have any future timeline for additional edits or versions of the manuals, according to the alliance website.
“If and when any updates occur to provisions within the FSMA Produce Safety Rule (e.g., agricultural water provisions), we will revisit the curriculum content to ensure its alignment with the regulation,” the group said on its website.
Powerpoint presentations and printable photos that help educate growers on produce safety issues are available online.