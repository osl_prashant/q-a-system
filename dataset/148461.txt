President Trump calls Canada and Mexico leaders to say he is not ending NAFTA  






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






It is always important to know who is telling what regarding potential Trump administration policy moves. 
President Trump told the leaders of Mexico and Canada that he would not immediately seek to end the North American Free Trade Agreement (NAFTA), only hours after some in the White House had signaled that it would begin pulling the U.S. out.
In what the White House described as “pleasant and productive” Wednesday evening phone calls with President Enrique Peña Nieto of Mexico and Prime Minister Justin Trudeau of Canada, Trump said he would quickly start the process of renegotiating NAFTA — not abandon it, as he said he would do during the 2016 presidential campaign if he could not rework the deal to his satisfaction. “It is my privilege to bring NAFTA up-to-date through renegotiation,” Trump said in a statement issued by the White House at 10:33 p.m. ET. “I believe that the end result will make all three countries stronger and better.”
Earlier on Wednesday, several media outlets (initially one by Politico) quoted officials signaling Trump was laying the groundwork to pull out of NAFTA — a move apparently intended to increase pressure on Congress to authorize new negotiations, and on Canada and Mexico to accede to American demands.
Brannon and Navarro vs the moderates. The apparent executive order to get out of NAFTA had been submitted to the White House staff secretary for the final stages of review, Politico reported, saying it was drafted by Peter Navarro, head of Trump's National Trade Council, in close cooperation with chief White House strategist Steve Bannon. Those two are very aggressive when it comes to any policy, but especially trade policy.
Several farm commodity groups quickly released statements as well as some lawmakers noting their alarm at the development, which again was altered by Trump’s late-evening statement.
Washington must give Canada and Mexico six months’ notice before exiting the trade agreement, which came into force in 1994. Any action to that effect would start the clock. But the prospect of the United States’ pulling out obviously alarmed the Canadian and Mexican leaders and prompted their calls to the White House.
“Canada is ready to come to the table at any time,” Alex Lawrence, a spokesman for the Canadian foreign minister, Chrystia Freeland, told Reuters on Wednesday.
Mexico responds. In an interview with the Financial Times, Ildefonso Guajardo Villarreal, Mexico’s economy secretary, said his government was consulting with Washington and conscious of the new president’s desire to bring manufacturing jobs back to the U.S. “We are trying to work with his team,” he said. Mexican officials, he said, had also tried to explain to Trump and his Cabinet that any time he lashed out at NAFTA and the peso fell it only made producing in Mexico cheaper and therefore more attractive to US companies.
The Trump administration must send a final version of a letter to Congress to start a 90-day waiting period before negotiations for a new NAFTA. This would allow the White House to begin negotiations with Mexico and Canada.
Canadian House of Commons member criticizes Trump’s trade policy stance. Conservative Canadian House of Commons Member Gerry Ritz, the country's official opposition critic for international trade, blasted President Donald Trump's repeated criticism of Canada's dairy supply management program. "Well, this is typical President Trump," Ritz said in an interview on Canadian-based www.RealAgRadio.com. "It's all shock and awe. You know, throw down the gauntlet and then really not have a lot of details that his assumptions are based on. ... The biggest problem in the U.S. is overproduction and until they come to terms with that, we're not a dumping ground for anybody, especially for dairy and a few other commodities."
Ritz, who was Canada's trade minister from 2007 until 2015, warned the Trudeau administration against letting Trump divide Canada and Mexico. "I think we're stronger together," he said. "We proved that in our fight with them on [Country-of-Origin Labeling]. ... It's also important that we sign on to, as soon as possible, things like the Trans-Pacific Partnership. With the Americans out, it gives us and Mexico strength in that group of countries. ... So get that done. That gives us an ace up our sleeve when we're dealing with America on NAFTA."
Comments: When you see something you do not like or think does not meet the logic test, wait a while and there will probably be a course adjustment, especially from a president that literally is learning on the job and in some ways getting better at it. While you should know this about the general media it needs repeating: big media love big stories and friction and a lot of “hits” on their publications. They love to suck in targeted statements by various stakeholders who are so willing to issue statements. Lawmakers ought to be as quick to react on actual bills as they are verbalizing against something that was not fully vetted. Sound familiar?
As for comments by Canada’s Gerry Ritz… The overproduction in the U.S. according to him, is negatively affecting Canada’s milk production cutback program. NAFTA in large part brought into place a North American market. The United States does not have a restrictive milk production scheme like Canada, which again shows that production-diversion programs do not work – producers elsewhere will boost production. But are some U.S. domestic policies boosting some agricultural production when markets say otherwise? That is a topic Ritz may want to contemplate.


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.