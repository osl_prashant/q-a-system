Joe Cardenas is Terra Exports’ new U.S. West Coast sales director, focusing on the company’s grape division. 
Cardenas will focus on domestic sales as the fall harvest begins in California, but he’ll expand into Las Vegas-based Terra Exports’ other categories, including avocados, limes, blueberries, pineapples, mangoes and papayas, according to a news release.
 
He brings experience from different backgrounds in the produce industry, including sales, operations, buying, quality control and shipping, according to the release.
 
“I was introduced to table grapes and it’s one product I have really learned to handle and sell,” Cardenas said in the release. “Several of the positions I’ve held previously contributed to this knowledge, specifically one role where I led a Chilean program moving mostly grapes.”
 
In the past. Cardenas has worked for The Giumarra Cos., Unique Produce Co. and RJ Produce Inc., according to his LinkedIn profile.