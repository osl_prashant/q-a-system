Arkansas AG appealing judges' rulings on herbicide ban
Arkansas AG appealing judges' rulings on herbicide ban

The Associated Press

LITTLE ROCK, Ark.




LITTLE ROCK, Ark. (AP) — Arkansas' attorney general is appealing two judges' rulings exempting some farmers from the ban of an herbicide that took effect this week.
Attorney General Leslie Rutledge on Wednesday filed notice that she's appealing to the state Supreme Court temporary restraining orders issued last week in Mississippi and Phillips counties preventing the state Plant Board from enforcing its dicamba ban against some farmers. The state Plant Board's dicamba ban took effect Monday and will run through October 31. The ban was issued after the board received nearly 1,000 complaints last summer that the herbicide drifted onto crops and caused damage.
Rutledge has also asked the judges to dissolve their temporary restraining orders.
The state Supreme Court last week stayed a Pulaski County judge's ruling exempting six farmers from the ban.