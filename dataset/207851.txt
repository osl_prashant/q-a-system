On Nov. 1, 1985, Markon Cooperative was introduced to the produce community at a Produce Marketing Association convention by our two key founders — Paul Gordon of Gordon Food Service and Norman McClelland of Shamrock Foods.  
Today, Gordon Food Service is the largest independent foodservice distributor in the country; Shamrock Foods is the seventh largest distributor.
 
What made these companies so successful? It comes down to leadership and foresight, and these men had both. 
 
Paul died in 2008, Norman died in July. Paul’s spirit of collaboration and view that “the customer is king” shaped our early years. Norman, passionate and driven, chose his words carefully and had a knack for knowing when to step forward and lead. The two men were both driven by foundational Christian values and had complementary styles. They led us to continue innovating through three business cycles focused on three areas: products, processes and people.  
 
Our first decade saw us focused on products. Securing the right packs for foodservice in a market dominated by retail was no slam dunk. Suppliers had to be convinced there would be a market for their products, so we saw Tanimura & Antle develop trimmed lettuce with the excess leaves removed, Mann broccoli spears that fit a steam tray right out of the box, Sunkist “mini-packs,” and an increasing line of pre-cut products from Freshco (now Fresh Express). 
 
Our second decade was focused on processes in technology, workflow and processing methods. This was the era that saw AOL emerge and e-mail and websites become everyday parts of our business. Operational improvements and food safety emerged as areas of focus. 
 
Our third decade saw the rise of people. Affected by food safety crises, consumers curtailed spinach purchases and demanded safety improvements. Foodservice became customer-centric, with customization expected. Labor shortages emerged and power began to shift from businesses to the employees. Consumers wanted greater transparency, demanding agriculture address its impact on the environment. Some ran to organic, while others expected “sustainability” would prove positive. The lesson was that consumers are in charge. 
 
As Markon enters its fourth decade, what’s next? I wish Paul and Norman were here to help answer that question. Purpose has emerged as a motivator for millennials — they want to know what companies stand for and purchase products that align with their values. 
 
As business leaders we’ve been challenged to ask ourselves “why does this business exist and what societal value does it bring?” New challengers are coming for share of stomach. New competitors are entering the foodservice distribution business and we’d better learn to demonstrate and deliver value — as defined by our operator customers and their consumers.   
 
In reflecting on Paul and Norman’s legacy, two things remain clear. We must be focused on our customers with products, processes and people … and values matter. Values sometimes get short shrift. But without core values that drive our decision-making, we won’t generate the legacy that leaders like Paul and Norman have left us. And perhaps that would be the greatest loss — our unfulfilled potential. 
 
Tim York is CEO of Salinas, Calif.-based Markon Cooperative. Centerplate is a monthly column on “what’s now and next” for foodservice and the implications for produce. E-mail him at timy@markon.com.
 
What's your take? Leave a comment and tell us your opinion.