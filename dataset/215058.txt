Time is money to J.D. Dulaney, but he was losing time constantly running from field to shop. A single trip out of the field carried little consequence, but over the course of a season, the efficiency waste mounted to a slow bleed that ultimately affected all areas of his operation.
Dulaney, who farms 4,000 acres with Gen 4 Farms in Clarksdale, Miss., was driving a typical large pickup with a Pack Rat toolbox in the bed. The toolbox, along with an air compressor, took up most of the bed and left little room for hauling. “Even if it was just a roll of poly, I’d have to use a trailer. I kept asking myself, ‘Where can I make myself more efficient?’”
A flatbed truck option made sense to Dulaney, but he knew tool placement was the key to efficiency. For six months he thought over the design, picked his placement, ordered the parts, and in just under two days, assembled what serves as a moving office. Dulaney did the labor by himself in the Gen 4 shop. The result was a skirted, KnapHeide flatbed on a 2015 Chevrolet 2500HD crewcab, rigged with farm tool essentials.
Dulaney mounted a Quick Draw toolbox at the front of the bed and welded a wire rack on top. The toolbox slides out 60% on either truck side, allowing total access. It includes a tool organizer and Dulaney sectioned it for sockets, big wrenches, hand tools, drills, spray cans, and much more. On the driver’s side of the bed, he set a tailgate box beside the Quick Draw for air gauges, snap wring pliers, and Allen wrenches. Beside the tailgate box, he built a catch-all basket for ratchet straps, hammers and utility items. A hose reel is mounted beside the tailgate box. “The bed cost $4,200 and the Quick Draw box cost me $1,000. Everything else was in hand, but just had to be configured,” Dulaney says.






Dulaney fitted his cooler with a hydraulic adapter and ball valve with a 1’ hose. “Now I have a place to wash my hands in the middle of a field and I’ve always got clean water to rinse a part.”
© Chris Benentt






Beside the hose reel sits a K2 cooler held by two eye bolts and turnbuckles to prevent sliding and theft. On the backside of the K2 cooler, the drain plug is fitted with half-inch straight threads. “I screwed in a hydraulic adapter and ball valve with a 1’ hose. Now I have a place to wash my hands in the middle of a field and I’ve always got clean water to rinse a part.”
On the passenger bed-side, and air compressor is mounted on a metal plate atop a rubber mat. The compressor is easily removable: When Dulaney pulls a gooseneck, he takes out the four bolts, unplugs the power supply, and it’s off in minutes. The bed is covered with a tough horse stall mat which prevents the sliding and bunching of normal truck mats. At the end of the bed sits a vice – on-site no matter where Dulaney goes on the farm.
Dulaney has a Ranch Hand Bullnose front bumper on the Chevrolet 2500HD – no more worries about collision damage from deer or wild pigs across the operation. A football goal-style rack can be inserted in seconds into a front receiver to carry pipe or suction lines along the headache rack – no trailer required.






“The bed cost $4,200 and the Quick Draw box cost me $1,000. Everything else was in hand, but just had to be configured,” Dulaney says.
© Chris Benentt






“The time savings are stacking up to an incredible degree. It cuts down drastically on my travel time. I wish I’d have done this 10 years ago. I’ll never have another regular bed truck to work my farm. Some guys might want to consider a long-wheel base instead of a short-wheel base like mine, but that’s just something everybody has to decide for themselves.”
At harvest, Dulaney’s flatbed truck follows combines and 18-wheelers as the last vehicle into the fields. “If something breaks, everything we need to make the fix is there. Between my truck and the cellphone, that’s all I need to get back up and running.”
Jeff Talley, Talley Planting Co., farms 6,000 acres across four Mississippi counties and loses big chunks of time each season driving between fields and shop. After seeing Dulaney’s truck setup, Talley decided to craft a similar design. “I’ll have a flatbed system close to J.D.’s next year. I need what his truck offers – a rolling shop.”
Labor and efficiency sometimes mix like oil and water on a farm, an imbalance that often surfaces at pressure points – planting, critical irrigation periods, and harvest. At Gen 4 Farms, Dulaney can’t hold back the time clock, but he’s rigged a work-truck with a purpose-driven design that allows him to attack problems across his operation without ever leaving the fields.






“I wish I’d have done this 10 years ago. I’ll never have another regular bed truck to work my farm,” Dulaney says.
© Chris Benentt






Any downside to the flatbed system? “Yes. My wife says it looks ugly,” Dulaney laughs. “But I don’t care what it looks like as long as it gets the job done.”