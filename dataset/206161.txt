Dennis Donohue (in white shirt), director of the Western Growers Center for Innovation and Technology in Salinas, speaks to participants of the Cal Poly freshPACKmoves 2017 conference during a May 24 field trip. Photo by Tom Karst.
 
The International Economic Development Council honored the city of Salinas, Calif., with a Gold Excellence in Ecomonic Development award during its annual conference Sept. 19 in Ontario, Canada.
The award was given to the city for its Salinas Ag Tech Innovation Ecosystem campaign, according to a news release.
Salinas’ Ag Tech Innovation Ecosystem focuses on entrepreneurial development, workforce training and marketing in the AgTech industry, the release said.
Since its inception in 2012, the campaign has led to an accelerator program, a downtown incubator and youth technology development programs. The city has also hosted three Forbes AgTech Summits during that time.
“I am so proud that our city has won such a prestigious international award,” Mayor Joe Gunter said in the release. “It’s a wonderful validation of our vision of Salinas as an AgTech capital, and the great work of our economic development team in moving that vision forward.”