Chicago Mercantile Exchange live cattle futures were mixed on Thursday, with the front-month contract edging higher after hitting a 2-1/2-week low on Wednesday.
Deferred cattle contracts were weaker, remaining under pressure from weak cash markets.
Strong export data provided support to the nearby contracts. The U.S. Agriculture Department reported weekly beef export sales of 21,900 tonnes, up from 9,400 tonnes last week.
Most-active CME April live cattle settled up 0.050 cent at 123.325 cents per pound. Deferred live cattle contracts were flat to down 0.325 cents.
CME April feeder cattle settled down 0.250 cent at 146.750 cents per pound and front-month March feeders finished 0.300 cent higher at 145.050.
CME hog futures closed lower, weighed by falling wholesale pork prices weakness in the cash hogs market.
CME April lean hogs ended down 0.250 cent at 66.975 cents per pound.
The pork cutout that tracks values of cuts such as hams was up $0.57 to $78.01 per cwt, rebounding from a nearly $6 drop on Wednesday. Prices for pork bellies, used to make bacon, rose $5.98 to $124.66 per cwt, the USDA reported.