The cost of raising dairy replacement calves is a significant cost on Wisconsin dairy farms in the
production of milk. Calculating the costs associated with raising dairy calves is an essential part
of dairy business management. To augment individual dairy calf cost of production analysis, the
dairy industry also requires a set of benchmark costs whereby individual business costs and labor
and management efficiencies can be compared.
In 2017 UW-Extension Dairy Team conducted an on-farm survey on the cost  and labor efficiency of raising dairy replacements individually or through an automated group feeding system.  Highlights of the findings include:

Operations using automated feeders had higher liquid feeding costs than individual
	systems due to their ability to easily feed higher milk amounts. Use of pasteurized whole
	milk reduced costs when feeding higher milk volumes even when partially using salable
	milk.
Labor costs were lower for farms using automated feeding systems than individual
	systems and compensated for the higher milk feeding amounts. Unpaid labor costs were
	higher for individual fed operations with these operations generally being smaller and
	having less hired labor.
Management costs were similar between operation types emphasizing the importance of
	calf management in either system.
Housing costs were higher for those using automated feeding due to newer, larger
	facilities. This difference may normalize as depreciation will occur as facilities age.
	Operations with the highest housing cost also had the highest total allocated costs.
	Planning for sufficient but not excessive facility space save costs of raising calves. Use
	of a renovated facility also may be an option for certain operations.
Variation exists for labor and housing costs for farms using automated feeding systems;
	therefore, suggesting different strategies to manage calves and improve employee comfort.

To view the full white paper, please visit http://fyi.uwex.edu/heifermgmt/rearing-costs/.