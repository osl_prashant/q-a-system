(UPDATED, Sept. 13 ) Florida’s produce industry losses from Hurricane Irma will be grim, and vegetable crops near harvest in Moultrie, Ga., were also hurt, industry leaders contacted Sept. 12 said. 
“While the full extent of the impact is still being explored, this is definitely an event with very significant damage to the Florida citrus industry,” Shannon Shepp, executive director of the Florida Department of Citrus, said in an email statement. “Before Hurricane Irma, there was a good chance we would have more than 75 million boxes of oranges on the trees this season. We now have much less.” 
 
Shepp said some growers are dealing with trees out of the ground. “Agricultural emergency declarations exist for types of natural disasters like this,” she said in the statement.
 

Based on grower reports, citrus crop loss may be in the 50% to 70% range in some areas, Lisa Lochridge, director of public affairs with the Florida Fruit & Vegetable Association, said in a Sept. 13 e-mail.
 
Tomatoes, other fruit
“As for other fruits and vegetables — particularly tomatoes and strawberries — there was damage to fields that were about to be planted in south and central Florida — primarily plastic ground covering and irrigation systems ripped up,” she said. 
 
Because of the storm, Lochridge said the tomato crop is expected to be light at the first part of November, but volume should build and industry leaders expect a “solid December.”
 
Likewise, she said strawberry growers expect to be able to recover quickly and stay on their timetable to be harvesting on time. Florida strawberry harvest typically begins in late November and continues into March.
 
“A big concern for growers is finding available workers to help them in their recovery efforts,” Lochridge said. “The labor supply was already very tight, so this is also an issue they’re dealing with.”
 

In Homestead, Fla., Mary Ostlund, director of marketing for Brooks Tropicals, said in an e-mail that most of Florida’s crops were severely impacted by Hurricane Irma. 
 
“Brooks Tropicals is assessing the extent of these damages,” Ostlund said. With offshore supply, she said Brooks Tropicals will continue to supply tropicals, including Caribbean Red papayas from Guatemala, SlimCado avocados from Dominican Republic and Solo papayas from Brazil.
 
Tomato marketer DiMare Homestead Inc., Homestead, said in a recorded telephone message that the company expects to open Sept. 14 with minimal staffing that day.
 
Florida vegetables
In southwest Florida, one grower said Sept. 11 that Irma resulted in 100% loss of anything that was planted. 
 
“We had a lot of specialty peppers, eggplants and tomatoes in the ground and we had just planted some summer squash last week,” said Steve Veneziano, vice president of sales and operations at Oakes Farms Inc., Naples, Fla. 
 
Those crops are lost, he said, and replanting those vegetables won’t be easy, he said.
 
“You can’t just throw the seed in the ground — you have get the seed, bring it to the greenhouses, seed it and after 42 days you can pull it and plant it,” he said. After that, it is another 70 to 90 days before the vegetable is harvested.
 
“Everybody cut their greenhouses down to avoid destruction, but by the time greenhouses are ready to start seeding again, it could be three to four weeks on average,” he said. 
 
“This will set things back for the entire month of November, December and most of January on items like peppers, tomatoes and eggplants out of south Florida,” he said.
 
Veneziano said the company had planted close to 20% of their land when Irma hit, while other growers had planted only 8% to 10% of their crops.
 
Georgia report
There are no firm estimates on crop damage in the Moultrie-Tifton region in Georgia, but a lot of tomatoes, peppers, eggplant that were staked were blown over, said Charles Hall, executive director of the Georgia Fruit & Vegetable Growers Association. 
 
“Right now I think everybody is evaluating whether they can stand stuff back up that got blown over and whether some of the younger stuff can recover after being pushed over,” he said Sept. 12.
Packing had recently started for earlier squash and cucumbers. Harvest usually continues in the region until mid- to late November. 
 
Blueberry fields in southeast Georgia in Homerville and Alma suffered some damage to very young blueberry plantings, but the fruit was mostly spared. “I don’t think we had too much damage in the blueberry area,” he said.
 
Calvert Cullen, owner of Northampton Growers Produce Sales, Inc., Cheriton, Va., said the company’s operations in Boynton Beach, Fla. had not yet planted cabbage, peppers, cucumbers and squash when the storm hit. 
 
However, in Georgia, where the company has operations near Moultrie, he said the same crops did suffer damage. 
 
Harvest for the company’s Moultrie operations were expected to start by late September, but 65 mph winds and about 3 inches of rain damaged some of the crop. 
 
“We’re not wiped out by any means, we just don’t know the damage,” he said Sept. 12. One of the unknowns is if the hurricane brought much salt to the fields. 
 
The company’s Georgia harvest will typically continue until the first week of January, he said.