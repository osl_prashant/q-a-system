Production challenges posed by heat stress in dairy cows can be reduced by acknowledging that dry matter (DM) intake decreases as temperatures rise.Whenever feed intake drops due to heat stress, nutrient concentration should increase to maintain adequate intake of all required nutrients. Low-quality "stemmy" forages generate more heat by fermentation inside the rumen. High-quality forages are digested faster and result in less heat being produced.
Increasing the energy density might entail the use of greater amounts of concentrate or byproducts.
Care should be taken, however, to balance diets properly to avoid digestive disorders such as acidosis and displaced abomasums.
Factors that can contribute to rumen acidosis problems are:
Decreased DM intake with lower proportion of forage and higher levels of fermentable carbohydrates.
Decreased rumination.
Decreased saliva to the gut, a source of bicarbonate, with a reduction of its buffering power due to increased CO2 expelled (panting).
Decreased rumen pH, which impairs digestion efficiency of fiber (rumen fibrolytic bacteria are the most affected when rumen pH drops below 6.0.
Poor mixing of feed, allowing cows to selectively eat.
All of these factors contribute to decreasing feed efficacy, and, consequently, lower milk yield and often lower milk fat.
Moreover, acidosis has been shown to affect the animals' overall health status, fertility, and longevity.
Changing feeding times, feeding 60% of ration between 8 p.m. and 8 a.m., for example, can be helpful. Cows will tend to consume more feed at night when it is cooler, slug feed, sort feed and choose feeds that produce less heat during digestion, choosing grains and proteins over forages.
With a system like this, cows, in turn, will tend to eat less during the day, but more often and in small quantities.
Provision of water is critical. Cows are unlikely to walk more than 275 yd. to drink so it is essential that all fields and buildings are adequately supplied.
As temperatures rise, cows will drink more, often in excess of 25 gal. per day, even the lower-yielding cows. In hot weather, water intakes can increase by 10% to 20% so it is essential that yards, buildings, grazing areas and dispersal areas are well-supplied by water troughs.
If cows have access to outside yards or grazing, it is very important that the water is close to shade and a source of feed.
The most practical methods to reduce heat stress can be grouped into shade, ventilation and cooling. Common areas where cows congregate that will benefit from a reduction in heat exposure are holding areas, feed areas and loafing areas. The installation of fans, combined with spraying water onto cows, can dramatically reduce the effects of heat stress. Results of research in the U.S. suggest that airflows as low as 6 mph can reduce respiration rates in heat-stressed animals by as much as 50%.
Spraying should be in cycles combined with improved air movement. Water can be applied more easily within the collecting yard while cows wait for milking. This is a particularly important time because when cows are closely confined in the collecting yard, ambient temperatures can rise rapidly.
Although roof lights are beneficial in reducing lighting costs, they can increase the heat within a building, especially when large numbers are fitted to south-facing buildings.The easy solution is to minimize the number on the south, fitting more on the north.
To minimize potential problems with animal welfare, dairy farmers need to recognize and monitor the symptoms of heat stress and actively manage their systems accordingly.
To read this full article, click here. For more related information, visit http://www.nadis.org.uk/.