The Atlanta Flatbed office of La Cañada Flintridge, Calif.-based Allen Lund Co. has donated to Mission Haiti for the eighth year in a row.
The office paid to ship a container of school supplies, farm and medical equipment, clothing and other items from Pasadena, Calif., to the Port of Houston, according to a news release. From Houston the container was shipped to Haiti.
“We are happy to assist Mission Hati by supplying the transportation to the port,” Chris Clelland, manager of the flatbed office, said in a news release. 
“It is nice to know that the kids in Haiti will be getting these supplies to make their life better!”
Mission Haiti is an outreach program of South Pasadena, Calif.-based Holy Family Church.