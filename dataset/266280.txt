BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Wednesday:
Open  High  Low  Settle   Chg.ORANGE JUICE                          15,000 lbs.; cents per lb.                May     138.00 141.45 137.55 141.10  +3.40Jun                          141.20  +3.20Jul     138.70 141.30 137.90 141.20  +3.20Aug                          141.40  +2.80Sep     139.30 141.50 139.30 141.40  +2.80Nov     140.30 142.15 140.30 142.15  +2.70Jan     141.00 142.85 141.00 142.85  +2.75Feb                          143.40  +2.75Mar                          143.40  +2.75May                          144.15  +2.75Jul                          144.25  +2.75Sep                          144.35  +2.75Nov                          144.45  +2.75Jan                          144.55  +2.75Feb                          144.65  +2.75Mar                          144.65  +2.75May                          144.75  +2.75Jul                          144.85  +2.75Sep                          144.95  +2.75Nov                          145.05  +2.75Jan                          145.15  +2.75Est. sales 805.  Tue.'s sales 518       Tue.'s open int 13,240,  up 63