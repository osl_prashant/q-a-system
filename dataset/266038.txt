Vidalia onion grower-shipper Bland Farms, Glennville, Ga., has been recognized for its export business by the state of Georgia — but not for what you think.
Georgia Gov. Nathan Deal presented Bland Farms the GLOBE Award (Georgia Launching Opportunities by Exporting) at a March 20 reception in Atlanta. The award honors Georgia companies that add a new export market in the previous year, according to a news release.
The product, however, is not the company’s Vidalia onions. Bland Farms started growing sweet potatoes in 2014.
“The opportunity to expand our business by exporting sweet potatoes not only helps us, but it takes a Georgia-grown product to consumers worldwide,” CEO Jeff Bailey said in the release.
Georgia announced 40 GLOBE Award recipients this year, compared to 24 last year.
“The Go Global event highlights the importance of international partnerships for maintaining our competitive edge and generating new investments across the state,” Gov. Deal said in the release.