Jin Ju Wilder is new to the chairmanship of the Produce Marketing Association, but the 23-year veteran of the produce industry is no stranger to leadership.
Wilder’s passion for produce has reached into various industry and community leadership roles for decades.
“I’ve always felt it was really important to get involved, engaged,” said Wilder, director of marketing with LA & SF Specialty Produce, a Los Angeles-based wholesale produce, dairy and specialty foods distributor in California, Arizona and Nevada.
As marketing director with the company, Wilder is responsible for the company’s marketing strategy and activities, as well as brand management and company communications.
She also leads the development and implementation of customer relationship management systems and data discovery and reporting solutions.
“I’ve always been a bit of a change agent,” Wilder said. “I like strategic thinking of ‘what if?’ and how can we improve. Over the last year, they have given me the opportunity to do that with some wonderful thought leaders in the industry.”
Wilder has been active in discussions about PMA’s vision and direction, which includes soliciting input from all sectors of the industry.
“We’ve made sure this group of leaders was diverse, from all over the world, represented different segments of the industry, experiences, genders — to try to make sure we had the most diverse group ever,” she said.
“I felt that diversity of thought leads to a much stronger outcome,” she said.
“They gave me the opportunity to work with these people over the last year, and it really plays to my strengths.”
Wilder’s philosophy has served her in various industry roles in a career that began in 1994.
Before joining LA & SF Specialty Produce, she was the director of corporate strategy for Los Angeles-based Valley Fruit & Produce, where she leveraged executive management experience, branding and marketing expertise, industry knowledge and networking for Valley clients.
She is also a longtime PMA board member, which should serve her well as chairwoman, she said.
“I’ve had the opportunity to learn from some of the past chairs,” she said.
 Wilder said she hopes to borrow the “collaborative” approach of her immediate predecessor, John Oxford.
“He makes sure everyone is heard and contributes to discussions in board meeting,” Wilder said. “He’s good at looking at issues from all sides. He does not have that knee-jerk reaction. That’s definitely something I’m hoping to take from him.”
PMA also has adopted the collaborative approach, which has benefited members, Wilder said.
“I think PMA has reached a point as well to realize there are strengths and experts in certain areas, but that PMA doesn’t have to be the leader in every area on every issue.”
Wilder said her priority as PMA chairwoman is to continue to bring leaders together around a new vision and direction and provide staff with information, direction and feedback on the organization’s annual business plan.
“Because of the new vision and direction we have, it’s going to be even more important that, as a board, we continue to bring the industry expertise, experience and knowledge to staff,” she said.
“My job is to continue to bring everybody together, think strategically, make sure everyone is heard and (can) contribute.”
Oxford said Wilder’s background, in the industry and within PMA, will prove invaluable.
“Jin Ju has been in and around the board for years, served on boards, task forces for the strategic planning efforts. She also was involved in an earlier iteration of our strategic plan a decade ago,” Oxford said.
“She’s fair, straightforward, articulate and a person people really want to follow. I have no doubt she’ll do an exceptional job, not only as a leader of PMA, but a strong voice for the fresh produce and floral industry.”