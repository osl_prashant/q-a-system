Salinas, Calif.-based The Nunes Co. ships about 35 conventional items and a similar number of Foxy Organic vegetables, says Doug Classen, vice president of sales. The company’s product line includes broccoli, shown here in a field in Yuma, Ariz., iceberg lettuce, romaine, cauliflower and celery. Growing conditions have been good, Classen says. (Photo courtesy The Nunes Co.)

A number of desert growers are responding to increased consumer demand for organic produce by expanding their organic product lines.
Salinas, Calif.-based Tanimura & Antle Inc. has returned to the organic business after pulling out in 2010, said Mark Adamek, director of romaine, artisan and mixed leaf production.
The company is growing organic romaine hearts, Artisan Romaine, red onions, celery and broccoli in the Yuma, Ariz., region, he said.
“We got out of it because there was no money in it,” Adamek said.
But that doesn’t seem to be the case today.
“We think the organic marketplace is changing,” he said.
Salinas, Calif.-based The Nunes Co., which markets the Foxy Organic brand, offers about 35 organic items, about the same number as conventional commodities, said Doug Classen, vice president of sales.
Peter Rabbit Farms, Coachella, Calif., started packing organic medjool dates for the first time Oct. 24, said John Burton, general manager of sales and cooler.
Baloian Farms, Fresno, Calif., plans to launch an organic spinach program Dec. 1, which should continue through March 10, said Jeremy Lane, sales manager.
Baloian Farms will offer organic green beans until mid-November.
The company has sold them in the spring, Lane said, but this will be its first fall crop.
Castroville, Calif.-based Ocean Mist Farms Inc. is growing organic Brussels sprouts and broccoli in the Coachella Valley for the first time this season, said Jeff Percy, vice president of production for the southern desert region.
He estimated that 5% of the company’s products are organically grown, and he said that number is climbing.
Pasha Marketing LLC, Mecca, Calif., is adding Brussels sprouts to its organic product line starting in December after conducting trials last year, said Franz De Klotz, vice president of marketing.
They’ll be available until the first week of February.
The company also ships organic eggplant, green and red bell peppers, green beans and a few jalapeño peppers, he said.

Want to know more about organic produce? Register for The Packer’s inaugural Global Organic Produce Expo, Jan. 25-27, in Hollywood, Fla.