Growers and importers of Mexican grapes are gearing up for the season’s start, which they predict should be back to normal after delays last year.
“I expect harvest to start the first week in May but chances are we could see few table grape trucks crossing into the U.S. the last week of April,” Miky Suarez, president of MAS Melons & Grapes, said in a news release.
“During the first week or so, we are going to see lower crossings than the average we are used to.”
The Mexican table grape crop is estimated at about 16 million boxes this season, but favorable weather could allow greater volume, according to the release.
MAS Melons & Grapes plans to market the Ivory variety from Spain’s SNFL Group and the Early Sweet ARRA variety for the first time this season, according to the release, as well as perlette, flame, sugraone, black seedless and red globe.
The company will also market grapes for an additional grower this season.
“We are proud that, this season, the Ortiz Ciscomani family’s Agricola Orqui SA de CV has decided MAS Melons & Grapes to market a reasonable portion of their table grapes production, as we have had the privilege to distribute their honeydew melons and squashes over the past years,” Suarez said in the release.
Suarez expects to harvest good quality grapes this season with a larger than normal berry size, according to the release.