Preclosing
Preclosing

The Associated Press

CHICAGO




CHICAGO (AP) — Futures trading on the Chicago Board of Trade Mon:
Open  High  Low   Last   Chg. WHEAT                                   5,000 bu minimum; cents per bushel    May      464¼  469    458¾  463½   + ¼Jul      478    483¼  472¼  476½   — ¾Sep      496¼  501½  491    495     — ¾Dec      521    525½  515    519     —1  Mar      538¾  543½  534    538     — ¾May      546    550    545    548     + ½Jul      548    554    546    548¾   — ¼Sep      557¼  557¼  555    556¼   —1  Dec      574¾  574¾  568    571½   — ¼Est. sales 148,431.  Fri.'s sales 154,951Fri.'s open int 479,295,  up 358        CORN                                     5,000 bu minimum; cents per bushel    May      376½  379¼  376¼  378¾   +2¼Jul      385¼  388    385¼  387¾   +2¼Sep      393    395½  392¾  395     +2  Dec      402¼  405    402¼  404¼   +1¾Mar      410¼  412½  410    412¼   +2  May      415½  416¼  414¾  416     +1½Jul      419    420½  418¼  419¾   +1¼Sep      407¾  407¾  406    406¼   + ¼Dec      408¼  410½  408¼  409¾   +1¼Mar      417½  418    417½  418     +1¼Dec      415    415    415    415     — ¾Est. sales 313,932.  Fri.'s sales 408,092Fri.'s open int 1,811,839               OATS                                      5,000 bu minimum; cents per bushel    May      232¾  235    221¾  226¾   —5¾Jul      235½  236½  220¾  227¼   —8¾Sep      242½  242½  230    232½  —10¼Dec      251    251    240    245     —8½Mar      252¾  252¾  252¾  252¾   —7  Est. sales 2,518.  Fri.'s sales 536     Fri.'s open int 6,114                   SOYBEANS                               5,000 bu minimum; cents per bushel    May     1027½ 1033¼ 1017¼ 1022½   —6¼Jul     1038¾ 1045   1029   1034     —6¼Aug     1040¼ 1046¼ 1031   1036¼   —5¾Sep     1036¼ 1041   1027¾ 1032¾   —4¾Nov     1033   1039½ 1025¾ 1030¼   —4¾Jan     1038¼ 1044   1030¾ 1035¼   —4½Mar     1028½ 1033¼ 1021   1025¾   —3¼May     1031¼ 1032   1020¼ 1024¾   —2¾Jul     1036   1036¾ 1025   1029½   —3  Aug     1028½ 1028½ 1026¼ 1026¾   —2½Nov      998¼ 1003¾  993¼  997     —1½Nov      981½  982    981½  982     +4¾Est. sales 201,515.  Fri.'s sales 249,779Fri.'s open int 921,984