To be certified as a master cheesemaker in Wisconsin, you need 13 years of hard work: 10 years of honing the craft and three to exhibit mastery through advanced courses and exams.

Several cheesemakers from America’s Dairyland are getting closer to retirement, and are focusing on training the next generation of cheesemakers.

According to the Wisconsin Milk Marketing Board, this is the only such program of its kind outside of Europe.

Hear from an aspiring master cheesemaker on AgDay above.