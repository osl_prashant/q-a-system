Eleven organizations are being inducted into Food Bank for New York City’s new Million Meals Club.The inaugural event on June 22 honors donors that have donated the equivalent of 1 million meals to the organization. It also honors participants who are active in the fight against hunger in the city.
This year’s inductees are:
Baldor Specialty Foods,
E. Armata Inc.,
Katzman Produce,
Fresh Direct,
ShopRite,
Farm Fresh,
Krasdale Foods Inc.,
Target Corp.,
HP-Hood,
Boar’s Head Frank Brunckhorst Co., and
Duane Reade.
The food bank plans to honor individuals as well, including Josephine Infante, former president of the Hunts Point Economic Development Corp.
Banners acknowledging honorees will be displayed at the food bank’s distribution center at the Hunts Point Produce Market.