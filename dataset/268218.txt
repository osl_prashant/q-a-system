Lobstermen will get extension on medical waivers
Lobstermen will get extension on medical waivers

The Associated Press

AUGUSTA, Maine




AUGUSTA, Maine (AP) — A Maine proposal is now on the books to extend the amount of time some commercial fishing license holders can receive a medical waiver.
Harpswell Democratic Rep. Jay McCreight's proposal allows the Maine Department of Marine Resources commissioner to renew a temporary medical waiver for lobster and crab fishing licenses up to one year.
McCreight says she was motivated to make the proposal at the request of a Harpswell lobsterman who needed help because he's struggling with a terminal illness, but still needs to work.
McCreight say she also plans to work on a potential solution next year that would allow qualified family members to fish using the license of a sick relative.
The bill became a law on April 1 without the signature of Republican Gov. Paul LePage.