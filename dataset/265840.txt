Farmers looking to spend less time in the field planting small grains and oilseeds will welcome the introduction of John Deere’s new 60 ft. (18.29m) 1895 no-till drill with ProSeries openers. The sleek new air disk drill is 40% wider than the company’ 43-foot air seeder, currently its largest seeding system available in the marketplace.
“We’re advertising that farmers can gain up to 100-plus acres per day in productivity with our 60-ft. 1895,” says Emily Klemmer, seeding product manager for John Deere. The extra 100 acres of productivity is based on the drill being paired with a C850 air cart and a seeding window with farmers planting 10-hours a day, seven days a week to get the job completed.
“Another thing farmers will appreciate is that on a quarter section they’ll make nine fewer passes with the 60-ft 1895,” Klemmer adds.
In the process of covering more ground in less time, she says the 60-ft. seeder can help farmers avoid a 1% to 1.7% loss per day in yield potential by helping them stay within their targeted seeding window.
Other key features of the 60-ft. air disk drill include the new ProSeries openers, RelativeFlow blockage warning system, TruSet in-cab downforce pressure control, and an upgraded tire package, featuring high flotation tires (30 psi mainframe and wing) to decrease surface compaction and minimize seed emergence issues.
“This is a pretty substantial increase, higher from where we were, and all tires are going to come equipped with walking beams and castors as well,” Klemmer says. “This will help with ground following—the drill will be able to handle undulating terrain while keeping both tires on the ground at the same time, and that’s ultimately going to minimize compaction levels.”
The seed rows on the new drill are on 10-inch (25.4 cm) spacing with separate fertilizer rows on 20-inch (50.8 cm) spacing. Seed and fertilizer rates are controlled by the SectionCommand metering system and powered by an AirPower 2 dual-fan for variable rate and prescription seeding applications. The twin fans provide a 40% improvement in flow of seed and fertilizer to the openers across the entire seeding compared to a single-fan system.

Operators can control and set downforce pressure accurately and conveniently from the cab with Deere’s exclusive TruSet closed-loop downforce pressure control system. This feature provides uniform and precise seed and fertilizer placement across the working width of the toolbar, under variable soil and field conditions, for more uniform plant emergence and crop maturity. TruSet offers six customizable pre-sets for seed and six pre-sets for fertilizer, or the operator can dial in the exact downforce pressure settings they desire.
 



The RelativeFlow Blockage Monitoring System is another John Deere exclusive technology that will now be offered on the 60-ft. (18.29m) 1895 Seeders to provide operators more accurate seeding performance information in the field. Sensors in the primary tower and on secondary hoses monitor the relative flow rate of both seed and fertilizer across the drill, from opener to opener, and this information is displayed on the monitor in the cab.

In addition, the 1895 air drill has a floating front hitch and wings with 25 degrees of flex.

For the first time, Deere is introducing the new ProSeries openers, which replace the 90 Series openers. The ProSeries openers will be available for the first time on the 60-ft. 1985, then expanded to other models in the future. These performance-driven openers feature a serrated closing wheel option for better seed and fertilizer sealing, narrow and flexible press wheels along with a narrow seed boot for reduced soil disturbance while ensuring more precise seed placement.
 




“The ProSeries openers help seal more anhydrous ammonia in the ground and improve seed-to-soil contact compared to previous openers,” Klemmer explains. “These new openers also feature sealed bushings and extended service life on ground-engaging parts such as the boot, seed tab, and press wheel bearing, along with fewer grease points to reduce maintenance time, resulting in lower service costs and more time operating in the field.”
Klemmer says the seeder will be available to farmers in time for the 2019 season with pricing to be determined by sometime this summer.
This story includes content from a John Deere-distributed press release.