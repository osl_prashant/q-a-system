Both total and grocery-specific comparable sales were down at retail giant Target Corp. in the second quarter of 2016.
Total comparable sales fell 1.1% in the second quarter, officials of Minneapolis-based Target revealed in an investor conference call.
Also during the call, Brian Cornell, Target's chairman and CEO, said that despite recent improvements in the chain's grocery departments, grocery also saw small comparable sales declines in the quarter, which he called "disappointing."
Food deflation played a role in grocery's decline, but Target also deserves some of the blame, Cornell said.
"We clearly have work to do to unlock our growth potential" in grocery, he said.
Target's commitment to produce and the grocery department in general has been lukewarm, said retail consultant David Livingston, and lackluster performance has been the result.
"Target's going nowhere," he said. "For one, they don't have much - it's a token program. They have 25% of the volume of what Wal-Mart's doing. And it's 300 feet from the entrance."
Many Targets that are located near traditional grocers "don't even bother with produce," he said.
"Their moves have been very lateral, they're not making a lot of progress. I don't think Target has the wherewithal to pull anything off in grocery."
David Marcotte, senior vice president of Boston-based Kantar Retail, said that up until 2015, Target used its grocery department as a "trip driver" for consumers. That's why they were located at the far end of stores.
"It was to get people in, move them around," he said.
That model has changed, and now Target is no longer as willing to accept lower margins just to generate traffic, Marcotte said. With that has come disruptions and, as a result, the poor performance of grocery in the second quarter wasn't a big surprise, he said.
But he expects the company to turn it around.
"They are very methodical, and they will make changes accordingly. I don't expect (grocery numbers) to stay down."
On produce in particular, Target needs to focus on fast-turning items like bananas, apples and oranges and re-examine its commitment to items at greater risk of shrink, Marcotte said.
On the positive side, Target has seen significant gains in grocery's performance at its new LA25 concept stores in the Los Angeles area, with comparable sales up 2-3% in the quarter, Cornell said.
Produce's performance at the LA25 stores was even better, with 5% comparable sales gains, he said.
The grocery departments in the LA25 stores are more separate from the other store departments, providing a more "intimate" shopping experience for shoppers, Cornell said.
Target will use LA25 concepts in efforts to increase sales in its existing grocery departments, with a focus on improving presentation, assortment and promotions, Cornell said.