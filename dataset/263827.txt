USDA rejects Mercy for Animals humane bird slaughter request
USDA rejects Mercy for Animals humane bird slaughter request

The Associated Press

DES MOINES, Iowa




DES MOINES, Iowa (AP) — The U.S. Department of Agriculture has rejected a petition from an animal rights group that sought more humane treatment for turkeys and chickens sent to slaughter.
California-based Mercy For Animals filed a petition in November asking the USDA to include poultry in the Humane Methods of Slaughter Act, a 1958 law that makes it a crime to abuse or neglect pigs and cows during slaughter.
The head of the USDA's Office of Food Safety said in denying the petition that other regulations ensure humane poultry treatment.
Mercy for Animals says it has proof chickens are abused or scalded to death in tanks of hot water and sometimes have legs and wings cut off while still conscious.
The group's attorney said Friday legal options to overturning the decision are under consideration.