Miami-based Fullei Specialty is now doing business as The Fullei Fresh Marketplace and focusing on promoting health and wellness.
Fullei Specialty, the sister company of Fullei Fresh, will no longer sell Asian produce. Instead it will operate a community space that it plans to renovate for monthly events including speakers, workshops, cooking classes and more.
The change is part of Fullei Fresh celebrating its 40th anniversary, according to a news release. Fullei Fresh will continue to sell sprouts, shoots and wheat grass.
The grand opening of The Fullei Fresh Marketplace in Miami is scheduled for April 14. Chefs, yoga instructors and health enthusiasts in the area can apply to participate.
Retail packs from Fullei Fresh will be available for purchase during Fresh Marketplace events.
Fullei Fresh has applied for a Cultivating Change grant from Greener Fields Together to assist in the renovation, but it plans to complete the project regardless of whether it receives the grant.