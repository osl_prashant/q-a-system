Producers in Montana, North Dakota and South Dakota could be buying hay or feeding donated hay this year because of drought and wildfire in the region.
Receiving hay from an unknown source makes testing even more important, says Rachel Endecott, beef specialist for Montana State University Extension.  
“Regardless if it is a good or bad year I recommend sending a sample into the laboratory to get a nutrient analysis,” Endecott says.
If feeding an annual forage hay, such as barley or sudangrass, run a nitrate test to limit the risk of toxicity.
“It is hard to manage if you don’t measure first,” she adds.
Once you have an idea of hay quality, build your nutrition strategy from there.

In wildfire-damaged pastures, Endecott reminds producers to keep an eye out for weeds in the spring.
In addition, feeding grain will likely be necessary in areas impacted by drought and wildfire. She says producers should not overfeed grain and suggests feeding less than 6 lb. of corn per cow per day. Current feeding equipment on an operation such as feed bunks, hay feeders, tractor-pulled mixer wagons or truck-mounted cake feeders will help determine what feed options are best to utilize.
“We all know feed is the biggest expense for a cow-calf operation, so finding ways to lessen that expense can be beneficial,” Endecott says.
Extending the grazing season is the best way to reduce those costs when moisture conditions are adequate. A cover crop program might be an option in better years.
“If we can keep cows out on pasture before snow covers it up in our part of the world then we feel pretty good about it,” Endecott says.
 
For more on winter feeding in corn growing regions of the U.S. read the following story:
Feeding Corn and Grazing Stalks Affordable Winter Feeding Options
For more on grain buying advice read the following story:
Removing Guesswork from Feed Buying