Grain mixed and livestock mixed
Grain mixed and livestock mixed

The Associated Press



Wheat for May rose 2.25 cents at 4.5575 a bushel; May corn was up 1 cent at 3.76 a bushel; May oats was off 2.75 cents at $2.2875 a bushel; while May soybeans was unchanged at $10.2975 a bushel.
Beef was higher and pork was lower on the Chicago Mercantile Exchange. April live cattle was up .45 cent at $1.1815 a pound; March feeder cattle rose .75 cent at 1.3752 a pound; while April lean hogs fell 1.25 cents at $.6132 a pound.