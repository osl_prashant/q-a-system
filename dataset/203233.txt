Somebody has to be last.
 
The fact that the agriculture secretary is the president-elect's last cabinet appointment (still unannounced as of Jan. 12) is somewhat disheartening.
 
Hey, what about us?
 
Is the U.S. Department of Agriculture last and least in Trump's eyes? Or is his deliberation in making the pick a carefully calculated approach that signifies big changes may be ahead for the agency? 
 
The lack of priority - or, alternatively, the extreme caution - given to the pick appears to show that Donald Trump is uncertain what he wants to accomplish with federal agricultural policies.
 
Is Trump going to swing for the fences and seek to decouple farm policies and programs from the federal treasury? 
 
Will he put forward a whole-hearted attempt to put in place a "market-driven" approach to agricultural policy, the likes of which has not been seen since the 1930s? 
 
That would be folly. Trump may find farm-state legislators won't easily be convinced that big changes to the USDA are needed, and Trump can't afford to burn his political equity on crop insurance payments.
 
Trump is a political outsider to begin with, and his lack of specificity on farm policy and goals is worrying.
 
No doubt, Trump's one-on-one interviews with multiple candidates for the agriculture secretary post will help him understand the complexity of agricultural issues. 
 
But there are many questions.
 
Does Trump endorse rolling back school nutrition gains and blocking grant reform to the food stamp program? 
And, of course, what is his stance on processed fruits and vegetables in the Fresh Fruits and Vegetable Program? Perhaps some policy direction can be inferred, but the candidate has not explicitly stated his views.
 
Trump has a 40,000-foot view of America but has been speaking in 140-character tweets about what he wants to do.
 
The choice for agriculture secretary will also influence Trump's immigration policies. 
 
If the new agriculture secretary is solidly connected to agriculture, whether in the Midwest, South or West, that person should be a voice of reason in Trump's cabinet on immigration policy.
 
The supply of Mexican-born immigrant labor may be declining already, even without the reality of Trump's ramped-up enforcement efforts.
 
Tom Hertz, economist with the USDA's Economic Research Service, spoke at a recent convention of the American Farm Bureau Federation about the challenges of the farm labor supply.
 
The USDA has published a study, according to a Farm Bureau news release, that finds that H-2A expansion over the long-term would increase the farm workforce and expand outputs and exports overall.
 
On the other hand, the USDA study states an enforcement-only approach would likely reduce the workforce and overall farm output, to the detriment of farmers and workers.
 
I spoke with Hertz Jan. 12 about the labor challenges facing growers. 
 
"The long-run demographic trends in Mexico imply there will probably be fewer people entering the country illegally," Hertz said.
 
The number of Mexico-born people living in the country has declined since 2007. Even if it stabilizes, he said that population will eventually age out of farming and the labor supply will become even tighter in the U.S.
 
"That would suggest (growers) will keep turning to H-2A," he said. 
 
In fact, the number of certified H-2A workers has climbed from 80,000 in 2011 to about 160,000 in 2016, he said. 
 
In my view, Trump can help growers "big league" if he makes it easier to use the H-2A program in the years ahead. 
 
The program has sometimes confounded growers with bureaucratic delays and high costs. Here's hoping the new agriculture secretary - whoever he or she may be - can help move Trump in that direction.
 
It will be an upset if Trump appoints anyone other than a Midwest agricultural leader to the post, though he has interviewed former California Republican Lt. Gov. Abel Maldonado and others outside of the Corn Belt.
 
If Maldonado, a grower of wine grapes, is appointed to the USDA's top post, that would be a coup for the specialty crops industry, but I don't think it will happen. Something has got to give soon, in any case.
 
Current agriculture secretary Tom Vilsack was nominated in mid-December 2008, shortly after Barack Obama was elected president. Vilsack was confirmed by the Senate just hours after Obama was sworn in as president.
 
Compared to his predecessor, Trump's secretary of agriculture will be playing catch-up, and that isn't the best news for America's farming community.
 
Tom Karst is The Packer's national editor. E-mail him at tkarst@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.