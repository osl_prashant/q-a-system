Amco enjoys steady demand
Leamington, Ontario-based Amco Produce reports continued steady demand for its vegetables, which are grown in Canada and Mexico.
“The volume is lower than production in the summer because of our customers are buying directly from Mexican growers,” said marketing coordinator Kurvin Soobrayen. “The quality is comparable to Canadian products, but the produce is usually shipped out to Canada. This sometimes affects its freshness because of truck delays.
“However, Amco makes use of HPS grow lights to keep production steady,” Soobrayen said. “So far we have (tomatoes on the vine) under the light and accommodate production 365 days a year. Amco has started planting new crops (the week of Jan. 15) and should start picking mid-March.”
 
Double Diamond launches biodegradable packs
Kingsville, Ontario-based Double Diamond Farms has started offering biodegradable packaging options for all the commodities it carries.
“We’re finding that consumers that are paying for organic produce are willing to spend more money to know that the packaging that it’s packaged in is environmentally responsible as it can be,” said procurement and sales manager Caitlyne Johns.
The company began offering that option a few months ago.
At the recent Global Organic Produce Expo, Double Diamond also featured its single flow-wrapped organic bell peppers.
“We found that we were able to extend the shelf life between 5-7 days by wrapping it,” Johns said. “People pay good money for their organic produce; they should be able to eat all of it.”
 
Greenbelt Microgreens seeks more partners
Business has been strong for Lynden, Ontario-based Greenbelt Microgreens, said vice president Michael Curry.
The company, which offers arugula, broccoli, pea shoots and numerous mixes of those and other products, has its main greenhouse in Ontario but is also growing in British Columbia and in New York.
To expand to other markets without expending quite as much capital, Greenbelt partnered with other companies to produce in their greenhouses.
“They’re expert growers who know what they’re doing, and so we don’t have to be on site every day,” Curry said.
The company continues to seek partners with growing knowledge and greenhouses in key locations to keep its growth going.
Greenbelt recently received a Premier’s Award for Agri-Food Innovation Excellence.
“Because we’ve been able to get that shelf life, the food safety and the quality of the product, we’ve been able to partner with some major retailers and really bring this product to the mainstream where consumers can buy it in over 500 stores now in Ontario,” Curry said.
 
Mucci Farms plans expansions
Several expansion projects are in the works for Kingsville, Ontario-based Mucci Farms.
Thirty acres of lit culture in Ohio will be coming online in March. For strawberries, 12 more acres — bringing the total to 36 — will be ready for harvest this fall, said vice president of sales and marketing Joe Spano.
The company has also seen great interest in its living lettuce program.
“We can’t keep up,” Spano said. “We’re completely sold out and are breaking ground in the spring for an expansion. We’re going to go from a production facility that right now we’re producing in the neighborhood of 6,700 heads of butter lettuce a day, and we’re going to move to 50,000 heads of butter lettuce a day by October, so that’s another space that we’re very, very excited (about).”
While the traditional Canadian greenhouse season is roughly March through November, growing under lights has allowed Mucci to continue producing in the winter.
“We have right now probably around 50, 55 acres of greenhouses under artificial light, so we’re picking cucumbers, mini cucumbers, beefsteak tomatoes, tomatoes on the vine and cocktail tomatoes out of Ontario right now,” Spano said. “It’s kind of significant for what’s going on in the industry, and by the end of 2018, early 2019, we’ll have all of our own, Mucci-owned glasshouses all in artificial lights.”
 
NatureFresh gets ready for TomBerry
In Ohio, production has been steady and quality has been high for Leamington, Ontario-based NatureFresh this winter, said marketing project manager Kara Badder.
The new product it introduced late last year, the TomBerry tomato, will make its debut at retail this year.
“Education and greater consumer interaction will continue to be a main marketing focus throughout 2018,” Badder said. “We aim to encourage individuals to live a healthier lifestyle and provide transparency about where their food comes from.”
Like others in the industry, NatureFresh is navigating a variety of challenges this season.
“Cold weather has had an effect on electricity and heating costs, moving above the seasonal norm for the region,” Badder said. “Transportation has forced changes to our arrivals and deliveries. Along with the current shortage of drivers and recent changes to regulations in the industry, we are facing some difficult challenges in the years to come with all logistic needs.”
The company has been increasing automation in its packinghouse to help make up for a labor shortage there as well.
 
Ontario Greenhouse gets new manager
Joe Sbrocchi joined Leamington-based Ontario Greenhouse Vegetable Growers in September as the new general manager for the organization.
He had worked for retailers Walmart and Sobeys before joining Kingsville-based Mastronardi Produce, where he worked for the past eight years.
“We are pleased to have a quality leader join the OGVG at a point where his experience, skills and leadership can significantly support our sector,” OGVG chairman George Gilvesy said in a news release.
Lighting and automation are two of the areas in which Sbrocci expects to see the industry advance in coming years.
“Just changing the lighting that helps the plant grow has other impacts too — the plant all of a sudden needs more water and food, it changes the ecosystem within that greenhouse ... all of that science is coming about,” Sbrocchi said. “There’s all kinds of things that are being done with seed that is totally like traditional seed breeding, (but) the difference is you use biotechnology to read them quicker.
“You still do the traditional plant breeding like we have for the last 100 years, but now you have the benefit of the biotech side to actually allow you to read the results much more quickly than you would have in the past,” Sbrocchi said.
 
Pure Hothouse Foods rebrands
Leamington, Ontario-based Pure Hothouse Foods, which markets under the Pure Flavor brand, is at the start of its planting season for many crops, with the first of the long English cucumbers in Ontario expected to be ready to pick in early February.
Pure Hothouse Foods is going through a total rebrand, with an aim to present itself as a lifestyle company rather than solely a vegetable company.
“We spent a great deal of time and effort building our new packaging strategy — it wasn’t just about correcting what we didn’t like in the current packaging,” said chief marketing officer Chris Veillon.
“The goal of the packaging audit was to identify how the brand could be more impactful at retail and by doing so be more relevant to both retailers and consumers alike.” “Beyond the creative update, consideration was given to production efficiencies across all packaging items to reduce overall costs while still delivering the best quality product available,” Veillon said.
Pure Hothouse Foods plans to launch a new website in late spring.
 
TopLine gears up for local deal
Leamington, Ontario-based Westmoreland, which markets under the TopLine Produce brand, is seeing steady volumes and nice quality on its vegetables, said account and marketing manager Jimmy Coppola.
“Demand is typical and follows the same curve we normally experience for this time (of) year,” Coppola said. “We have seen our organic business continually grow and we have increased our organic plantings in preparation for our local season to keep up with the growing demand.
“We do have some local items in production through the winter such as seedless mini-cucumbers and seedless cucumbers,” Coppola said.
“The rest of our local offerings will be in production starting (in) late February.”