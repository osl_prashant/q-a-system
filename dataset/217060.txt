Astin Strawberry expects big January volume
Astin Strawberry Exchange plans to harvest about 1,250 acres of conventional fruit and about 150 acres of organic, said Shawn Pollard, salesman for the Plant City, Fla.-based company.
Organic and conventional strawberry volume will pick up the second week of January. By variety, Astin Strawberry Exchange features 80% about radiance, 15% sweet sensation and the balance with the Florida beauty variety.
“The crop looks good — we are going to pick a pile of berries in January,” Pollard said. “Once we hit mid-January we will really hit our stride, weather depending.”
 
California Giant’s acreage holds steady in Florida
For its Florida strawberry volume, Cindy Jewell, vice president of marketing for California Giant Berry Farms, Watsonville, said the company has very similar acreage this year, though varieties have shifted based on past performance.
The company expects less volume of the radiance variety and more of the sweet sensation variety, Jewell said.
“We have planted Florida beauty for the first time this season and will be assessing it to determine how it performs,” she said.
“Ultimately we are working with our grower partners to provide the best flavor experience for our consumer and best performance in the field for the grower.”
The company packs most of its volume in 1-pound or 2-pound containers, which Jewell said are the main drivers of strawberry sales in the produce department.
Jewell said the company publishes a weekly blog that provides timely and detail crop reports with photos and weather conditions to the company’s trading partners.
“This e-newsletter has become quite popular and the database has doubled in the last two years as more people learn about the report,” Jewell said.
“We are very transparent and publish reports straight from our field personnel, sharing their forecasts and the photos they take weekly in the field.”
 
C&D Fruit & Vegetable boosts sweet sensation acreage
C&D Fruit & Vegetable Co., Bradenton, Fla., is increasing acreage of the sweet sensation variety, said Tom O’Brien, president of C&D Fruit & Vegetable Co.
“We feel the sensation is the more popular strawberry,” O’Brien said.
“It has a more consistent taste and size and went 100% with the sweet sensation variety in our organic acreage.”
The company started shipping Nov. 15 and expects to continue through April 1, he said.
The company stickers its clamshells with an O’Brien Family Farms logo. The sticker adds to the local feel of the fruit, he said.
 
Dave’s Specialty Imports adds Florida strawberries
Dave’s Specialty Imports will have its own strawberries from Florida this winter, as it will ship from about 100 acres.
Leslie Simmons, marketing manager for the Coral Springs, Fla.-based company, said Dave’s will have light supplies in December with peak volume January through March.
“We hope it complements our Mexican strawberry program and gives more options for our customers who desire a local or domestic product,” she said. 
Simmons said Dave’s has partnered with Florida growers in the past, but having its own acreage in Zolfo Springs in central Florida is a big advantage.
Most of the berries will be packed in 1-pound clamshells, she said.
 
Strawberry festival sets lineup
The Florida Strawberry Festival is scheduled for March 1-11 this year.
Tickets for entertainment acts for the event are available online and Wish Farms is the again sponsor of the sound stage at the event, according to Amber Maloney, director of marketing for Wish Farms, Plant City, Fla.
Acts appearing this year include The Oak Ridge Boys; Earth, Wind and Fire; Charley Pride; Sawyer Brown and others.
The festival began in 1930, when the Plant City Lions Club created the event to celebrate the local harvest of strawberries in Hillsborough County.
According to the event’s website, the festival attracts 500,000 visitors each year.
 
Grimes Produce invests in land
Seeking options as development pressures increase, Shawn Butler, salesman with Grimes Produce Co. LLC, Plant City, Fla., said the firm has invested in land outside of the Plant City region.
“If we were to have any issues here in Dover, we would farm in other areas,” he said.
 
Naturipe Farms adds snacks
Naturipe’s new label, Naturipe Snacks, will introduce a new produce line in 2018 that will include fresh berry fruit cups, as well as grapes and apples.
“With Florida strawberries coming in strong, we are excited to include the crop in the Naturipe Snacks products,” said Jaqueline Padilla, marketing coordinator for Naturipe Farms LLC, Estero, Fla.
Padilla said there are several opportunities to promote with the Florida crop through the season.
“With Florida and Mexico both lining up with good production and no sign of extra wet or cold weather in California, this winter is shaping up to be the most abundant strawberry year ever, with plenty of opportunity for promotions, beginning with the holidays and continuing through the new year,” she said.
That will allow retailers to promote larger pack sizes earlier than ever, she said.
 
Red Blossom holds steady with acreage levels
Craig Casca, sales manager for Los Olivos, Calif.-based Red Blossom, said the company will hold its Florida acreage steady this year, with a total of 230 acres of radiance and Florida beauty varieties.
The firm plans to ship until April 1, he said.
Casca said the company expects to pack 1-pound, 2-pound, 4-pound and 4-pound packs with stems, depending on customer requests.
 
Well-Pict Berries adds Florida beauty volume
The radiance strawberry is the leading variety in Florida for Well-Pict Berries, said Dan Crowley, vice president of sales for Watsonville, Calif.-based company.
The company is adding volume of Florida beauty, he said.
Well-Pict expects to pack as long as there is a market this season, Crowley said. Weather, labor and quality are the top challenges for Florida growers.
 
Wish Farms launches consumer campaign
Plant City, Fla.-based Wish Farm has a new consumer-driven campaign just in time for Florida strawberry season.
With the campaign, consumers who have signed up for the Wish Farms e-mail newsletter, Berry Lovers, are entered to win $100 every week.
The weekly offer is for existing and new Berry Lovers, according to the company.
Being a part of the Berry Lover program gives consumers what’s-in-season updates, contests and exclusive recipes.