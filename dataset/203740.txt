Advanced Transportation Services
Visalia, Calif.-based Advanced Transportations Services Inc. continues to add driver teams for its fleet, said Marshall Kipp, the company's CEO.
"We're running about 1,400 refrigerated trucks and, out of those, about 75% was teams, compared to, probably, 40% a year ago, so we're constantly working at it," he said.
The company most recently added 79 teams Aug. 1, 2016.
All were already employed with ATS, Kipp said.
 
American Trucking Associations
The Agricultural and Food Transporters Conference of the Arlington, Va.-based American Truckers Associations has been working with the United Fresh Produce Association, Produce Marketing Association and Western Growers to educate members about the Food Safety Modernization Act's Final Rule on Sanitary Transportation of Human and Animal Food, which will go into effect in May 2017, said Jon Samson, executive director of the Agricultural and Food Transporters Conference.
"This is not a huge change in what you're currently doing, but you're documenting how you're cleaning out your trailers and handling produce," Samson said.
The push is designed to help shippers and truckers know what is expected of them, Samson said.
"It makes everything go smoother," he said.
 
C.H. Robinson
Third-party logistics provider C.H. Robinson Worldwide, based in Eden Prairie, Minn., has released Navisphere Carrier, a new mobile app designed to support contract motor carriers and their business.
The new app is expected to "immediately improve the access" of the current users of the company's former app, CHRWTrucks, and provide access to the tens of thousands of loads C.H. Robinson books daily, the company said in a news release.
Navisphere Carrier, "built with contract carriers for contract carriers," was developed to support the more than 50,000 small carrier businesses C.H. Robinson works with, according the release. The technology is available on Apple and Android mobile devices. C.H. Robinson said it would lead to "increased connectivity to their drivers and C.H. Robinson, expanded planning capabilities and expedited payment processes."
The app gives greater functionality of C.H. Robinson's single global technology platform, Navisphere, to contract carriers, the company said.
The free Navisphere Carrier app is available in the App Store and Google Play. For more information on the technology, visit www.chrobinson.com.
 
Crowley Maritime
In an effort to "better serve the diverse equipment needs of ocean cargo and logistics customers," Jacksonville, Fla.-based Crowley Maritime Corp. has placed $32.7 million worth of new cargo-carrying equipment into service recently, said David DeCamp, corporate and marketing communications manager.
The additions include 440 generator sets and 400 40-foot-high cube refrigerated containers, which Crowley received at the end of 2015.
DeCamp said the company, since 2003, has invested over $250 million in new cargo equipment and today operates more than 52,000 pieces of owned and leased intermodal equipment, including more than 22,015 chassis; 21,297 dry containers; and more than 3,916 refrigerated containers, all of which come in a variety of sizes and are strategically located throughout the United States, Central America and the Caribbean.
"We at Crowley and Customized Brokers have seen a rise in Peruvian perishables coming into the U.S., perhaps as a result of some of the cold-chain initiatives bringing produce in through South Florida," said Nelly Yunta, vice president of Customized Brokers, a Crowley subsidiary.
Yunta said Mexico has seen a 28% year-over-year increase; Jamaica, 46%; Uruguay, 22%; and Honduras and El Salvador, 5% percent each, Yunta said, noting that Chile and Argentina also had increases.
"The company is also in the final discussion stages with government agencies to obtain permitting to allow for perishables shipments moving over ocean from Latin America to connect via air in Miami for forwarding to Europe and Asia allowing for fresher products with reduced seasonality throughout that hemisphere," Yunta said.
Crowley also has added a port call from Jacksonville to Puerto Limon, Costa Rica, which allows for additional options when moving goods to and from the southern zone of Central America, said Nelly Yunta, vice president of Customized Brokers, a Crowley subsidiary.
 
Infinity Transportation Logistics
Infinity Transportation Logistics LLC, Atlanta, is seeing increased interest and usage from produce shippers as a result of improved door-to-door transit times in its expedited temperature controlled domestic intermodal service to and from Washington State and Oregon, the company said in a news release.
ITL's expedited door-to-door domestic intermodal temperature controlled service departs from and returns to intermodal ramps in Spokane, Wash.; Portland, Ore.; and Seattle six days per week.
ITL's door-to-door transit times are now just four to five days to or from the Midwest and only six to seven days to or from various destinations on the East Coast, which is nearly as fast as long-haul trucks.
ITL representatives recently attended the 2015 Produce Marketing Association Fresh Summit in Atlanta and met with dozens of fresh produce shippers who were "very happy" about the continued improvements in ITL's door-to-door transit times in the regions that ITL serves, the company said.
In particular, ITL is able to provide expedited eastbound temperature controlled door-to-door domestic intermodal service from Washington State and Oregon to the following destinations or locations:

Illinois (Greater Chicago & surrounding areas including western Michigan, southern Wisconsin and northern Indiana);
New York (Buffalo area, New York City and its boroughs);
Pennsylvania (Chambersburg, Harrisburg, and Philadelphia areas);
Maryland (greater Baltimore area);
Massachusetts (Boston and Springfield areas);
Georgia (greater Atlanta area); and
Florida (Jacksonville, Orlando, and Tampa Bay areas).

Recently, ITL began providing expedited domestic refrigerated intermodal service from the Washington State/Oregon to Ontario, Canada (i.e., metropolitan Toronto area), and ITL also added staff, expanded services and upgraded systems to keep up with growth in business, the company said.
ITL uses 53-foot refrigerated containers to move fresh apples, potatoes, pears, onions, carrots and other items and other refrigerated products eastbound from Washington State and Oregon to the Midwest and East Coast.
ITL has a service radius of about 160 miles from/to intermodal ramps in Spokane, Portland and Seattle and can pick up/drop off product in many parts of Washington, Oregon and Idaho, including the greater Portland area, the Willamette Valley, Hood River and Bend, Ore.; the Columbia Basin in Washington and Oregon; and the Wenatchee area, Yakima Valley, Skagit Valley, Puget Sound area and greater Spokane in Washington; and northern Idaho, the company said.
 
IHS Markit
Englewood, Colo.-based IHS Markit, which does data gathering and analysis on transportation and compiles an annual report on commercial motor vehicle registrations, is "paying more attention to foodstuffs of all types, not just produce," said Chuck Clowdis, Lexington, Mass.-based managing director of trade and transportation the company's economics division.
"We're paying more attention in identifying the origins and destinations," Clowdis said.
The company's annual Transearch research project helps determine when how much of a particular crop will ship by truck and how much will go by rail, Clowdis said.
 
Allen Lund Co.
The La Canada Flintridge, Calif.-based truck brokerage Allen Lund Co. is celebrating 40 years in business.
The company says it has grown from a handful of employees at its beginning in 1976 to more than 400 now. Lund says it maintains key relationships with "top customers" like Costco, Coca-Cola, PepsiCo and Ahold USA, among others
"Celebrating 40 years is a dream come true to me which has been made possible by the hard-working people in this industry," president and CEO Allen Lund said in a news release.
The company also is expanding the use and functionality of its Transportation Management System, which it designed specifically for the shipment of produce, said Kenny Lund, vice president.
The system provides live updates on location, temperature loads and other management issues, Lund said.
"We unveiled it for the produce industry a couple of years ago, but we are adding so many things to it so fast, it's doing well," he said.
The tool keeps shippers informed of their product's condition at all stages, Lund said.
"The average produce load has six changes. The ability to absorb changes and get that information out to the players is very hard to do, but we're able to do it," he said. Allen Lund Co. anticipates reaching a "run rate" - what their customers spend on transportation - of about $2 billion by the end of 2016, Lund said
 
NationaLease
Downers Grove, Ill.-based truck-leasing organization NationaLease is rolling out a new web portal and customer reporting tool, NationaLease Fleet 20/20, which allows its customers "to have visibility into their fleets' maintenance on a local and national level," said Victoria Kresge, vice president of dedicated services and logistics.
The tool gives NationaLease customers the ability to run customized reports, as well to help its customers manage their fleets, Kresge said.
 
OHL International
Brentwood, Tenn.-based OHL International now is part of France-based logistics firm GEODIS, said Chris Ryan, OHL's key account manager for perishables.
"In November of 2015, Paris-based GEODIS announced the acquisition of OHL to strategically expand its global footprint and build a solid platform to better serve its clients, Ryan said. "Our long-term direction is to work closely with our clients in the fresh produce industry in developing end-to-end logistics and compliance solutions on a more global level."
 
Total Quality Logistics
Cincinnati-based truck brokerage Total Quality Logistics, now in its 20th year, continues to expand, said Kerry Byrne, president.
"TQL continues its aggressive, organic growth trajectory, expanding our footprint across the country and tapping into new markets for the best available talent in the industry," he said. "So far in 2016 we've opened a dozen new offices."
The newest, in Salt Lake City, opened Aug. 8.
 
Union Pacific
Omaha, Neb.-based Union Pacific Railroad announced multiple marketing and sales leadership appointments, effective Sept. 1.
John Kaiser has been named vice president of strategic planning in Union Pacific's executive department. He has been vice president and general manager-Intermodal since 2003. Kaiser joined Union Pacific 14 years ago as vice president and general manager-Automotive.
Jason Hess has been named vice president and general manager-Intermodal, succeeding Kaiser. Hess has been vice president and general manager-Agricultural Products since March 2014. He started his career with Union Pacific as a marketing and sales account representative and has worked in Agricultural Products, Industrial Products and Chemicals.
Brad Thrasher has been named vice president and general manager-Agriculture Products, succeeding Hess. Thrasher has been vice president and general manager-Industrial Products since March 2012. Before that, he was assistant vice president and general manager at two Union Pacific subsidiaries - Union Pacific Distribution Services and Streamline. His 20-year career with Union Pacific includes numerous positions in Automotive, Chemical and Industrial Products.
Kenny Rocker has been named vice president and general manager-Industrial Products, succeeding Thrasher. Rocker currently serves as assistant vice president-Chemicals. He has been with Union Pacific 22 years and held positions in Chemicals, Industrial Products and the Market Development and Sales Center.