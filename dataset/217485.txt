Sponsored Content
Cows produce huge amounts of heat, so cold stress is almost never a problem if you can keep them dry and protected from the wind. Our dairy and beef nutritionist experts explain why winter feeding can be a breeze for dairy, but it’s more complicated for beef producers.
“Cows actually prefer cold weather, so it’s easier to feed dairy cows in the winter,” said Bill Weiss, professor of Animal Science at The Ohio State University.
From a nutrition perspective, corn silage, assuming it’s been well made, has improved with time in the silos and should have become more digestible, which again makes it easier to feed.
“In the winter, dairy cows might eat a little more and we never want them to run out of feed, so if they clean up the feedbunk, you can always give them more, which may require an increase in feed deliveries,” said Weiss. “However, there won’t necessarily be major changes in the composition of the diet. In cold conditions, an increase of intake of 10 percent is not uncommon.”
One key is to ensure water is always available. But overall, Weiss says winter feeding is easy on the dairy.
Winter Feeding Not a Breeze for Beef Producers
In many parts of the country beef herds are calving in March to April, so by December, those herds are nearly to the third trimester of pregnancy. Therefore, in addition to colder weather and precipitation events, there’s an increased need for nutrients from the standpoint of pregnancy and fetal growth.
“There are increased energy requirements with colder weather due to beef cattle being outside, and at the same time, increased nutrient requirements due to the amount of fetal growth that’s occurring,” said Greg Lardy, professor of Animal Science at North Dakota State University. “Data indicates 60 to 70 percent of fetal growth will occur in the last trimester, so there’s a substantial increase in the level of nutrition, energy and protein required.”
The impact of winter feeding means increasing the nutrient supply for the cow, which should be done with better-quality forages and supplementation.
“It’s important to ensure you’re providing adequate protein in the diet and adequate protein for the rumen microorganisms, so they are equipped to do the work of fermentation,” said Lardy. “If there is inadequate protein for the rumen microorganisms, the result is depressed digestibility and microbial protein production, which ultimately means the cow will be short on energy and protein.”
If beef producers are feeding a low-quality hay or forage or if they’re grazing a dormant forage, there are many supplements to choose from. It’s always best to work with your nutritionist to determine the right fit for your herd.
“Another winter feeding strategy from an intake standpoint is to have cows grazing in a crop field consuming corn stalks, corn residue or a dormant forage,” said Lardy. “In addition, producers will include 10 to 15 pounds of higher-quality forage and corn silage or another supplement and feed them out on pasture at the same time, so part of their ration is coming from the pasture and part is coming from a total mixed ration.”
Beef cattle do a good job of acclimating to cold weather by growing more hair. If they’re in good body condition, that will also act as an insulator.
“Problems occur when cattle don’t have a chance to acclimate or they get wet, so rain or wet-snow events immediately followed by cold weather can be pretty dangerous to their health and well-being,” said Lardy. “In the Central and Southern Plains and into the Southeast, it’s not uncommon to have rain that turns to a snow mix or freezing rain, which limits the cow’s ability to maintain her body heat. Once she’s wet, there’s no ability to insulate her anymore.”
Other winter essentials you don’t want to overlook:

Body Condition Score – by December calves have been weaned, so plan to have mature cows at a body condition score of 5 and even a little bit better for the younger cows: heifers or 2-year olds.
Bedding – in northern climates, bedding in a drylot is usually corn stover or crop residue like wheat straw to offer another form of insulation so cows are not lying on cold, frozen ground and to keep them dry. Bedding can also cut down on their energy requirement.
Water – keep water sources open when it gets really cold. If water intake is limited, it can lead to decreased feed intake and poor animal performance. Be careful around automatic watering troughs or a stock pond where holes have been chopped into ice, as cattle can slip and fall on the ice.
Wind – protect cows from the wind in grazing situations, especially when temperatures are cold. Build a windbreak or ensure there is natural protection in pastures.

 

 
Sponsored by Lallemand Animal Nutrition