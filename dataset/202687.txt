A partnership between Produce For Kids and Meijer is bringing the Jump with Jill Live Tour to 25 Midwest elementary schools in a promotion called Meijer Rocks Midwest Schools.
 
"Our partnership with Produce for Kids allows us to better educate our customers about the importance of making healthy food choices," said Melissa Hehmann, healthy living advisor at Meijer, in a news release.
 
Ten Produce For Kids' industry partners donated funds in 2016 to make the Meijer-hosted tour possible: Avocados From Mexico, Dole Salads, Earthbound Farm, Michigan Apples, Pero Family Farms, Sunset (Mastronardi Produce), Highline Mushrooms, Marzetti Salad Dressing, Green Giant Fresh and Lil' Snappers (Stemilt Growers).
 
The Jump with Jill Live Tour will come to four Midwest cities:
• Columbus, Ohio - Feb. 27 to March 3;
• Milwaukee - March 6-10;
• Lansing, Mich. - March 13-16; and
• Detroit - March 20-24.
 
Created by a registered dietitian and musician, Jump with Jill approaches nutrition education with catchy songs and upbeat dance moves to engage kids on healthy eating, according to the release.