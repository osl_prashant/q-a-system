Chicago Mercantile Exchange live cattle futures on Tuesday finished lower as concern about increased supplies ahead yanked the market from early-session highs, traders said.
Investors exercised caution while waiting for the bulk of slaughter-ready, or cash, cattle in the U.S. Plains to change hands this week. 
April ended 0.400 cents per pound lower at 122.825 cents, and June finished down 0.575 cent at 114.775 cents.
Based on recent U.S. government cattle reports, the industry will have plenty of animals to move through the pipeline, Top Third Ag Marketing broker Jeff French said.             
"Beef demand has been good, but it will have to remain robust or else we're going to continue to backlog cattle," he said. 
On Tuesday, a few cattle in the U.S. Plains brought $126 per cwt versus mostly $127 last week. Remaining sellers are holding out for at least $128.
Wednesday's Fed Cattle Exchange auction of 474 animals could set the bar for this week's cash prices.
Market bulls believe higher packer margins and the recent bump in wholesale beef demand bodes well for cash returns.
Bearish investors contend that packers pulling from cattle contracted against the futures market, and increased supplies in the coming weeks, will kept a lid on cash prices.       
Funds in CME's livestock markets that track the Standard & Poor's Goldman Sachs Commodity Index sold, or "rolled," some of their April long positions into the June contract.
The "roll" process was done in advance of similar moves that will officially begin on Wednesday and last five business days. 
Flat-to-weaker cash feeder cattle prices and softer CME live cattle futures pressured the exchange's feeder cattle contracts.
March feeders ended 0.850 cent per pound lower at 144.300 cents.