The United States Department of Agriculture's National Agricultural Statistics Service (USDA, NASS) released their monthlyCattle on Feedreport Friday afternoon (May 20, 2016). The latest numbers in the Cattle on Feed report were quite surprising, particularly with regard to placements. Placements totaled 1.664 million head, an increase of 7.49% from April 2015 and a 1.50% increase from the five-year average from 2011 to 2015. Market analyst expected placements to be down 1.6%, so the reported value came in well above expectations. The highest analyst estimate was expecting less than a one percent increase, so the reported value was much higher than even the highest expectation. The increase in placements was largely driven by the Southern Plains states, with Kansas placements up 17.5%, Oklahoma placements up 64.7%, and Texas placements up 13% year-over-year. Most Northern Plains states saw year-over-year declines in placements with Nebraska placements falling 2.6% and South Dakota placements down 14.3%. This month's numbers continue the trend of increasing heavy placements, with cattle larger than 800 pounds seeing an 11.7% year-over-year increase.Cattle marketed in April totaled 1.658 million head, up 1.16% versus last year and down 6.37% compared to the average from 2011 to 2015. Pre-report expectations called for marketings to be 1.6% higher than the same period last year, so they came in slightly below where analysts anticipated they would be.
When placements and marketings are put together, the total number of cattle in feedlots with 1,000 head or larger capacity totaled 10.783 million head, up 1.34% versus May 1, 2015 and 0.60% lower than the five-year average. Market analysts expected a 0.1% year-over-year increase in cattle inventories, so the reported value came in well above expectations and fell above even the highest analysts' expectations. While April placements typically mark a seasonal low in terms of total cattle numbers, the report is still considered bearish for cattle markets. The larger placement numbers in April combined with higher placements in February and March will mean a larger supply of fed cattle coming out of the feedlot as we enter September and October, which will put downward pressure on the markets this Fall.

<!--

    function delvePlayerCallback(playerId, eventName, data) {
        var id = "limelight_player_351368";
        if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
            DelvePlayer.registerPlayer(id);
        }
        switch (eventName) {
            case 'onPlayerLoad':
                var ad_url = 'http://oasc14008.247realmedia.com/RealMedia/ads/adstream_sx.ads/agweb.com/multimedia/prerolls/agwebradio/@x30';
                var encoded_ad_url = encodeURIComponent(ad_url);
                var encoded_ad_call = 'url='   encoded_ad_url;
                DelvePlayer.doSetAd('preroll', 'Vast', encoded_ad_call);
                break;
        }
    }

//-->


<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->

Want more video news? Watch it on AgDay.
The Markets
The five-area fed steer price ended the week averaging $130.75 for live sales, and $204.07 for dressed; respectively, down $1.81 and $4.32. Nebraska feeder cattle were trading $5.00 to $12.00 higher on the week with 500-600 pound steers averaging $188.87 and 700-800 pound steers averaging $161.67. Corn was $0.01 higher on the week trading at $3.68/bu in Omaha on Friday.