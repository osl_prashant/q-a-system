April 1 U.S. potato stocks were up 2% from year-ago levels but f.o.b. prices are easily above mid-April 2017.
The U.S. Department of Agriculture reported the 13 major potato states held 134 million cwt. of potatoes in storage April 1, up 2% from the same date a year ago.
The USDA said potatoes in storage accounted for 33% of the fall storage states’ 2017 production, 1% more than last year.
Potato stocks in Idaho totaled 50 million cwt. on April 1, down 4% from 52 million cwt. from a year ago. As a percent of production, Idaho potato stocks represented 38% of the state’s 2017 production, up 1% from a year ago.
Shipping point prices for Idaho size 60-count cartons of Idaho russet burbank potatoes were trading at $12.50-$13 per 50-pound carton on April 17, up from $9.50-$10 per carton a year ago.
Shipping point prices for cartons of 10 5-pound bags of Idaho potatoes were $5-$6.50 per carton on April 17, also up from $3.50-$4.50 per carton a year ago.
In Washington, the state with the second-highest potato stocks on April 1, the USDA reported 30 million cwt. of potatoes on hand. That was up 3.4% from 29 million cwt. the same time a year ago. Washington’s stocks accounted for 30% of 2017 production, up from 27% of 2016 production on hand on April 1 last year.
The April potato stocks report said U.S. potato disappearance, at 266 million cwt., was down 3% from a year ago.
Season-to-date shrink and loss, at 19.3 million cwt, was 8% lower than the same time last year, according to the report. Processors in the eight major states used 146 million cwt., up 1% from April 2017.