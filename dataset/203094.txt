Research funded by the Center for Produce Safety is looking at how marketers can control cross-contamination of cantaloupe during field pack and retail handling of cantaloupe.
 
The $217,000 study, which is set to finish by the end of 2017, is directed by Laura Strawn, researcher with Virginia Polytechnic Institute and State University, according to a news release. 
 
Other researchers contributing to the study are Michelle Danyluk of the University of Florida and Ben Chapman of North Carolina State University.
 
With a focus on salmonella and Listeria monocytogenes, the project is looking at potential cross-contamination points for melons, according to the release. 
 
Intervention strategies targeted to reduce the occurrence of pathogen transfer events during the handling of melons at harvest and retail will be developed.
 
A project goal is to develop best management practices to reduce potential pathogen transfer to and from surfaces.
 
"We really want to hone in on the contact surfaces and take it from farm to fork," Strawn said in the release. "There are hardly any farm-to-fork studies that use the same methodology throughout."