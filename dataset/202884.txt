The World Apple and Pear Association (WAPA) released Southern Hemisphere production forecasts for apples and pears that shows mixed trends.
 
The group, convening at the Fruit Logistica produce trade show and conference on Feb. 10, estimates 2017 apple production in the Southern Hemisphere at 5.43 million metric tons, up 9% from the 2016 crop, according to a news release. Dropping slightly, Southern Hemisphere pear production in 2017 is forecast at 1.341 million metric tons, 2% lower than 2016.
 
Exports of Southern Hemisphere apples are forecast stable at 1.665 million metric tons, while pear exports from the Southern Hemisphere are expected to decrease 6% to 656,000 metric tons, according to the release.
 
The estimates from members in Argentina, Australia, Brazil, Chile, New Zealand and South Africa.
 
Also at the meeting, Todd Fryhover, president of the Washington Apple Commission, was elected president of WAPA. The new vice-president of the group is Nicholas Dicey from the South African Apple and Pear Association, according to the release.
 
The group is scheduled to meet in August at the Prognosfruit Conference in Lleida, in the Catalonia region of Spain, to release the Northern Hemisphere 2017-18 crop estimates.