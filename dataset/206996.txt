At some point in secondary school, we all learned about the “Four Basic Flavors” – sweetness, sourness, bitterness — a sensation that goes way beyond the culinary realm — and saltiness.It’s that last taste sensation that’s the subject of today’s rant…er, commentary.
Historically, salt has been one of humanity’s most precious resources. Although it may seem incredulous to us sophisticated post-industrial consumers, salt was once a form of currency. For centuries, salt was a key commodity that fueled East-West trade.
We still refer to people we admire as “the salt of the Earth,” meaning, someone who’s reliable and honest, a person of character and substance.
While our contemporary interest in salt is tied to its role as a seasoning — and to the (alleged) threat that excessive consumption plays in exacerbating hypertension — the reason salt was so important to ancient civilizations is its functionality as a preservative.
Whether fish or meat, the millennia that preceded the invention of refrigeration required vast amounts of salt to keep those dietary staples from spoiling within days of butchering or processing. Wherever salt could be obtained in the ancient world became a population center.
Wherever salt was scare, commercial interest arose to facilitate its acquisition.
Wisdom vs. ScienceBut let’s fast forward to the 21st century, when we’re surrounded by so much salt that it’s become the basest of commodities, due to its virtually universal availability.
Has anyone ever swiped a packet of salt from a restaurant or foodservice vendor, because there was none at home?
Nowadays, the chief concern about salt, other than whether it contributes to heart disease (hint: it doesn’t), is how much to use, and when we’re grilling our favorite meat, when to apply it.
Just the other day, a friend who prides himself as a master barbecue chef, and who owns a top-of-the-line Weber grill (the Genesis II — man, that’s a great grill!), as well as a custom smoker, asserted adamantly that steak should be lightly salted prior to grilling.
His insistence echoed the “wisdom” of British celebrity chef and self-proclaimed animal welfare expert Gordon Ramsay, whose “perfect steak” recipe calls for seasoning the beef with salt and pepper on both sides prior to cooking.
To be fair, Ramsey’s recipe specifies frying the steak in a pan, while adding butter, mushrooms and garlic.
Actually, that sounds pretty darn tasty.
But if we’re talking about grilling, salting the meat before cooking, as both scientists and culinary experts who don’t spend their spare time railing against modern livestock production would advise, is a bad idea.
According to the folks at the Gourmet Meat Club, a UK-based online marketing firm that describes its staff as “experienced butchers with a passion for great food,” one should never season meat before cooking it.
As a story in The Independent newspaper reported, “The idea of seasoning your meat before whacking it on the barbecue is an urban food myth, one that all meat-eaters have probably been guilty of at one point or another.”
Food scientists are quick to note that salting raw meat draws out the moisture and tends to make it drier and tougher when cooked.
Likewise, applying pepper before grilling tends up burp that spice and potentially ruin its sensory contribution to the cooked entrée.
So what’s the alternative?
Try applying a light rub of pure olive oil on the meat before cooking it and then seasoning it to taste after it’s cooked.
By the way, the same theory applies to marinating. Soaking meat for hours in a marinade really doesn’t accomplish what most people believe it does.
Actually, according to the Gourmet Meat Club folks, only a small amount of the marinade ends up being absorbed. What does penetrate into the meat cells? The salt that’s added to virtually all marinades, and once again, that tends to dehydrate the meat and make it tougher after cooking.
The solution (no pun intended)?
Apply the marinade after the meat’s been cooked, the same way professional chefs add a sauce or glaze to that high-end steak for which you’ll end up paying three figures, tax and tip included.
For family or friends, you can accomplish the same results at home.
Minus the expectations of any gratuity.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.