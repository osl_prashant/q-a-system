J ustin Dlomo watches his small herd of emaciated cattle scrounge for bits of dry grass with a growing sense of dread."I don't even know what to do anymore," he says.
Worsening drought in Zimbabwe has dried up water holes, crops and pasture, leaving farmers like 56-year-old Dlomo, who lives about 120 kilometres north of Bulawayo, unable to feed their animals - and unable to sell them for much either.
"We are all selling off our livestock. Better that than watch the cattle die," Dlomo told Thomson Reuters Foundation.
But because so many desperate farmers now have animals on the market, a cow that used to sell for $500 now fetches just $150 - or in some places as little as $50 - from buyers in the cities.
As climate change strengthens, drought is becoming more frequent and severe in southern Africa, and that - combined with this year's El Nino phenomenon - is taking a heavy toll on rural lives and economies, experts say.
"Water sources have dried up and we are drinking from the same reservoirs with our cattle," Dlomo said.
Zimbabwe is one of many countries feeling the strain of El Nino, which has dried up rainfall across southern Africa over the last year, killing crops, disrupting hydropower production and forcing local water authorities to enforce stringent water rationing in some areas.
When facing drought in the past, Dlomo would have moved his cattle to another neighbouring region with more rainfall. But this time the drought is widespread, he said.
"We cannot move our cattle anymore. There is no grass everywhere," Dlomo said.
Thousands dying
Livestock experts say parched pastures are causing the deaths of thousands of cattle across the country. Last year, the agriculture ministry's livestock department estimated that the national cattle herd stood at 5.3 million animals, down from over 6 million in 2014.
In one district in Masvingo province last year, more than a thousand cattle died because of drought, according to the Ministry of Agriculture.
Last week, the meteorological services department announced that the country should not expect any rain in the coming month, putting a further strain on livestock farmers.
According to the agriculture ministry, Zimbabwe is experiencing its worst drought since one in 1991-1992 that killed more than one million cattle.
However, while the World Food Programme said last month that millions of Zimbabweans will require food assistance this year, Dlomo said there is no assistance in sight for his dying livestock.
"We have not seen anyone here coming to offer solutions to our plight. It's like a punishment from God," Dlomo said.
Simangaliphi Ngwabi, the agriculture ministry's chief livestock specialist in Matebeleland South province, said there was little water or pasture for cattle in the region, as dry conditions continue across the country.
The livestock department estimates that more than 350,000 cattle may face death due to drought in Matebeleland South.
Last month, the U.N.'s Food and Agriculture Organization said it would provide subsidised livestock feed to small-scale farmers in four districts of hard-hit Matebeleland South province as hundreds of cattle succumbed to El Nino-worsened drought.
Planning ahead?
Last year, amid another season of poor rainfall in the country's southwest, Paddy Zhanda, the deputy agriculture minister in charge of livestock told farmers to sell their livestock to avoid losses.
By cutting herds, farmers would earn something from their cattle instead of losing them to drought, he said.
However, Naboth Chuma, a livestock researcher at Chinhoyi University of Technology, said farmers facing more frequent drought need to make plans to prepare for it - including storing cattle feed - before animals start dying.
"When a drought occurs it is never a sudden thing," Chuma said.
"What is required is for government and aid agencies to work with these usually resource-challenged farmers to help them stock enough feed for their animals. The unfortunate part is that there are competing needs for both humans and livestock and it is cattle that suffer," he said.
Deputy minister Zhanda has told farmers that they must find solutions to the livestock crisis and not wait for the cash-strapped government to intervene.
"We are still praying for rain. It is the only thing that will save us," Dlomo said.