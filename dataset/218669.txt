Regional Stockmanship and Stewardship events from the National Cattlemen’s Beef Association will be held in five states in 2018, expanding an educational program that gives cattle and dairy producers expanded knowledge on successful animal handling strategies. The two-day sessions will be held in South Carolina, Colorado, Washington, Texas and California, giving producers from several parts of the country an opportunity to access cattle handling suggestions and education that can help them improve their bottom lines.

The sessions include demonstrations and hands-on learning experiences that are entertaining, lively and informative. They feature lessons on horseback cattle handling, chute-side cattle handling, BQA training and preventative herd health programs. The program is made possible by sponsorships from Boehringer Ingelheim and the Beef Quality Assurance (BQA) Program, which is funded by the beef checkoff.

Scheduled events are:
Clemson, S.C., Aug. 24-25, 2018
Montrose, Colo., Sept. 21-22, 2018
Pasco, Wash., Oct. 12-13, 2018

Events with dates to be announced will be in Stephenville, Texas, and Tulare, Calif. The Tulare event will have a dairy focus.

The Stockmanship and Stewardship events will be led by clinicians Curt Pate and Ron Gill, Ph.D., who have hosted educational events at past NCBA trade shows and Stockmanship and Stewardship events. Pate has been conducting demonstrations and clinics for more than a decade, entertaining and educating audiences with his personal stories and innovative strategies. Gill is a renowned stockman and animal scientist for Texas Agrilife Extension who captivates cattlemen with his credibility as a rancher and ability to relate to his audience.

“The Stockmanship and Stewardship events serve as a location to learn from the best in the cattle industry, while providing networking opportunities for attendees with fellow producers who are applying these innovative strategies on their own operations,” according to Chase DeCoite, director of NCBA Beef Quality Assurance. “Every year it’s becoming more of a ‘must attend’ event.”

“At BI, we believe putting cattle first naturally leads to doing the right thing,” according to Steve Boren, vice president and head of the U.S. cattle business for BI. “These events provide producers with new tools and ideas for improving cattle handling, health and well-being. We’re very pleased to support this significant program, which adds value to the operations of cattle producers.”