BC-APFN-Business News Digest
BC-APFN-Business News Digest

The Associated Press



Here are AP Business News' latest coverage plans, top stories and promotable content. All times EDT.
TOP STORIES:
FACEBOOK-BABY STEPS — Facebook is taking baby steps for now to address the latest privacy scandal because something stronger might harm the core of its business: making money off the data it has on you. By Barbara Ortutay and Anick Jesdanun. SENT: 950 words. UPCOMING: 900 words with new approach by 4 p.m., photos.
With:
DELETING FACEBOOK — Fed up with Facebook? You're not alone. A growing number of people are deleting it, or at least wrestling with whether they should, in light of its latest privacy debacle — allegations that a Trump-linked data-mining firm stole information on tens of millions of users to influence elections. By Barbara Ortutay. SENT: 820 words, photos.
And:
TRUMP-CAMBRIDGE-ANALYTICA — Special counsel Robert Mueller is scrutinizing the connections between President Donald Trump's campaign and the data mining firm Cambridge Analytica. By Jonathan Lemire. SENT: 1,090 words, photos.
OF MUTUAL INTEREST-LURE OF BEATING THE MARKET — After years of falling short of index funds, stock-picking managers of mutual funds had a big pickup in performance last year, and most acquitted themselves well during the market's sell-off last month. But the majority still haven't been able to keep up with index funds over the long term. Experts say investors' primary goal should be to invest in funds with low expenses, before even considering whether they track indexes or are trying to beat them. By Stan Choe. SENT: 930 words, photos.
HARVEY-SILENT SPILLS- — The toxic onslaught from the nation's petrochemical hub was largely overshadowed by the record-shattering deluge of Hurricane Harvey as residents and first responders struggled to save lives and property. But a half-year after floodwaters swamped America's fourth-largest city, the extent of the environmental assault is beginning to surface, though questions about the long-term consequences for human health remain unanswered. By Frank Bajak of AP and Lise Olsen of the Houston Chronicle. SENT: 3,700 words, with photos, video, and abridged version of 900 words, as well as a sidebar slugged Harvey-Silent Spills-Arkema.
REVEAL-JP MORGAN CHASE-REDLINING — America's biggest bank is benefiting from a loophole in the Community Reinvestment Act, a landmark law designed to fight racial discrimination in lending. An analysis by Reveal from The Center for Investigative Reporting found that JP Morgan Chase rarely lent to African Americans or Latinos in the Washington area, where a majority of residents are people of color. By Aaron Glantz and Emmanuel Martinez of Reveal. SENT: 1,280 words, photos.
FEDERAL RESERVE-POWELL'S PERFORMANCE — In his first news conference as head of the world's leading central bank, Jerome Powell avoided any professorial lectures. If anyone was wondering how the new chairman of the Federal Reserve would differ from his two immediate predecessors, his exchange with reporters offered some clues. By Josh Boak. SENT: 740 words, photos. Story moved after midnight.
MARKETS & ECONOMY:
FINANCIAL MARKETS —Stocks are falling sharply and bond prices are climbing as investors fear that trade tensions will spike between the U.S. and China. By Marley Jay. SENT: 730 words. UPCOMING: Will be updated through 5 p.m.
MORTGAGE RATES — Long-term U.S. mortgage rates are ticking up slightly this week, the 10th increase in the past 11 weeks. By Marcy Gordon. SENT: 220 words, photos.
INDUSTRY:
TOYS R US-BID EFFORT — Toy company executive Isaac Larian and other investors have pledged a total of $200 million and hope to raise four times that amount in crowdfunding in a bid to save potentially more than half of the 735 Toys R Us stores that will go dark in bankruptcy proceedings. By Anne D'Innocenzio. SENT: 730 words, photos.
GLOBAL TOURISM — The travel and tourism sector is set for a modest slowdown in 2018 as a result of higher oil prices and airfares, a year after it experienced its best year on record, according to a leading global industry body. By Pan Pylas. SENT: 370 words, photos.
WYNN-STOCK SALE — Former Wynn Resorts CEO Steve Wynn is selling about 4 million shares that he owns in the casino-operating company. SENT: 120 words, photos.
STARBUCKS-CRYSTAL BALL FRAPPUCINO — Starbucks, thirsty for some social media magic, is releasing its latest sugary concoction: the fortune-telling Crystal Ball Frappucino. SENT: 180 words, photos.
RIGHT TO TRY-Q&A — The idea is a political crowd-pleaser with a catchy slogan: giving desperately ill patients the "right to try" experimental medicines. Lawmakers in the U.S. House of Representatives on Wednesday became the latest group of politicians to back the effort, sending a bill to the Senate which President Donald Trump has pledged to sign into law. By Matthew Perrone. SENT: 940 words, photos.
TECHNOLOGY & MEDIA:
AT&T-ANTITRUST TRIAL — The Trump administration is facing off against AT&T to block the telephone giant from absorbing Time Warner, in a case that could shape how consumers get — and how much they pay for — streaming TV and movies. SENT: 130 words, photos. UPCOMING: Will be updated from court proceedings.
SELF-DRIVING VEHICLE-FATALITY — Two experts say video of a deadly crash involving a self-driving Uber vehicle shows the sport utility vehicle's laser and radar sensors should have spotted a pedestrian, and computers should have braked to avoid the crash. By Tom Krisher and Jacques Billeaud. SENT: 940 words photos, video.
YOUTUBE-FIREARM VIDEOS — YouTube has tightened its restrictions on firearms videos. The video-serving network owned by Google is banning videos that provide instructions on how to make a firearm, ammunition, high-capacity magazines, and accessories such as bump stocks and silencers. SENT: 110 words.
TURKEY-MEDIA-SALE — Turkey's largest media group said Thursday it is in talks with a business group close to President Recep Tayyip Erdogan for the sale of its outlets, a development that further curtails independent journalism in the country that has taken an increasingly authoritarian turn under his leadership. By Susan Fraser. SENT: 370 words, photos.
ISRAEL-GARBAGE TO GOLD — Israeli start-up UBQ says its innovative method of converting garbage into plastics, five years in the making, will revolutionize waste management worldwide and make landfills obsolete. SENT: 930 words, photos.
WASHINGTON:
BUDGET BATTLE — The House easily approved a bipartisan $1.3 trillion spending bill Thursday that pours huge sums into Pentagon programs and domestic initiatives ranging from building roads to combatting the nation's opioid abuse crisis, but left Congress in stalemate over shielding young Dreamer immigrants from deportation and curbing surging health insurance premiums. SENT: By Andrew Taylor and Lisa Mascaro. 1,080 words photos.
With:
BUDGET BATTLE-EMAIL SEARCHES — The budget bill before Congress includes an update to federal law that makes clear that authorities with a warrant can obtain emails and other data held by American technology companies but stored on servers overseas. SENT: 320 words.
TRUMP-TRADE — Farmers, electronics retailers and other U.S. businesses are bracing for a backlash as President Donald Trump targets China for stealing American technology or pressuring U.S. companies to hand it over. By Paul Wiseman and Ken Thomas. SENT: 840 words, photos.
TRUMP-TRADE-STEEL — The European Union, Australia, Argentina, Brazil and South Korea are among the nations that will get an initial exemption from looming steel and aluminum tariffs from the Trump administration, U.S. Trade Representative Robert Lighthizer said. By Kevin Freking. SENT: 460 words, photos.
TRUMP-TRADE-IMPORTS FROM CHINA — President Donald Trump's decision to impose tariffs equaling as much as $60 billion on Chinese imports could mean higher prices for some of the most popular consumer goods Americans buy, from toys to furniture, computers and cellphones. UPCOMING: 800 words by 4 p.m. By Christopher Rugaber.
CHINA-TRUMP- TRADE — The Chinese government vowed to take "all necessary measures" to defend the country's interests if President Donald Trump targets it for allegedly stealing American technology or pressuring U.S. companies to hand it over. By Gillian Wong. SENT: 460 words, photos.
PERSONAL FINANCE:
NERDWALLET-HOME VIEWS— A house with a fabulous view can be hard for a homebuyer to resist. But seeing the mountains, water or city lights from the comfort of home comes at a price. The hazy part is figuring out what that added cost is — and whether it's worth it. By NerdWallet columnist Marilyn Lewis. SENT: 840 words, photos.
INTERNATIONAL:
GERMANY-ECONOMY— German business confidence has fallen amid growing concerns about international risks like the possibility of a trade war with the United States. SENT: 150 words.
BRITAIN-ECONOMY — The Bank of England kept its interest rates unchanged Thursday but appeared to issue another broad hint that they may rise again in May to get inflation back to target. By Pan Pylas. SENT: 580 words.
FRANCE-STRIKES — Trains, planes, schools and other public services across France were seriously disrupted Thursday as unions set up dozens of street protests across the country. By Sylvie Corbet. SENT: 360 words, photos..
MONEY & MARKETS SUMMARY:
SPOTLIGHT
Heavy metal
Caterpillar shares fell on concerns that Trump's trade restrictions and tariffs will hurt the construction equipment maker and other industrial companies.
CENTERPIECE
Trade tracks
The amount of freight that railroads carry might signal how President Donald Trump's new steel and aluminum tariffs are affecting international trade, because much of what the railroads deliver is imported.
Business News Supervisor Richard Jacobsen (800-845-8450, ext. 1680). For photos (ext. 1900.) For graphics/interactives (ext. 7636.) For access to AP Newsroom and technical issues: customersupport@ap.org, or 877-836-9477. Questions about transmission of financial market listings, call 800-3AP-STOX.
The full digest for AP's Money & Markets service can be found at markets.ap.org. For questions about M&M content, contact Greg Keller at (212) 621-7958.