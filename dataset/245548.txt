AP-IL--Illinois News Coverage Advisory 8:30am, IL
AP-IL--Illinois News Coverage Advisory 8:30am, IL

The Associated Press



Here's a look at how AP's general news coverage is shaping up in Illinois at 8:30 a.m. Questions about coverage plans are welcome and should be directed to the AP-Chicago bureau at 312-781-0500 or chifax@ap.org. Caryn Rousseau is on the desk, followed by Herbert McCann. A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
UPCOMING TODAY:
ILLINOIS-LEGIONNAIRES' DISEASE
SPRINGFIELD, Ill. — Officials in Gov. Bruce Rauner's administration will again face lawmakers' questions over the response to the Legionnaires' disease outbreak at the Quincy veterans' home. The Illinois House and Senate Veterans' Committees will question officials from the Illinois Departments of Veterans' Affairs and Public Health and the Capital Development Board Monday in Chicago. SENT: 130 words. Will be updated. Developing from 9 a.m. CT hearing.
LEAVING YEMEN BEHIND
DEARBORN, Mich. — People from Yemen have recently begun to see long-term futures in the U.S. and are making their culture part of their businesses. One such business is a cafe in the Detroit suburb of Dearborn that shares Yemen's centuries-old coffee tradition and uses coffee from a family farm in Yemen. The deeper roots that Yemeni are putting down is partly a result of the normal socio-economic evolution among first- and second-generation immigrants. But it's also due to war back home. By Jeff Karoub. SENT: 810 words, photo, video. Illinois editors note Chicago interest.
INDUSTRIAL FARMING IMPACT
WILLARD, N.C. — The Tar Heel State is home to 10.2 million humans and 9.3 million hogs — which produce three times the waste of their two-legged counterparts. All that waste is stored in open-air lagoons and sprayed onto fields as fertilizer. Rural residents say some of that waste gets onto and into their homes. Civil trials begin next month against a subsidiary of the world's largest pork producer, and people are watching to see whether things will change in the country's No. 2 hog state. By Allen G. Breed and Michael Biesecker. SENT: 2,010 words, photos, video.
IN BRIEF:
—ILLINOIS BICENTENNIAL STAMP: The U.S. Postal Service is set to issue a stamp honoring Illinois' 200th anniversary of statehood.
—AURORA POLICE-LAWSUIT: A federal judge has given the green light to a lawsuit by seven police officers against the Chicago suburb of Aurora after an official sent their addresses, phone numbers and other personnel records to a purported street-gang leader they helped imprison to serve an 88-year sentence.
—OBAMA FOUNDATION: The Obama Foundation and the University of Chicago have teamed up to offer a master's degree program for the next generation of community leaders.
SPORTS:
BKN--CELTICS-BULLS
CHICAGO — The Chicago Bulls host the Boston Celtics on Monday. UPCOMING: 600 words, photos. Game starts at 7 p.m. CT.
___
If you have stories of regional or statewide interest, please email them to chifax@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.