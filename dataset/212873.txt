Most California summer fruit commodities will start shipping a bit later than last year’s unusually early kickoff date, but water was plentiful this season following a multi-year drought, so quality should be good, grower-shippers say.
Here’s how supplies of some of the state’s most popular fruits were shaping up.
 
Avocados
This year’s California avocado crop is projected to be around 200 million pounds, compared to last year’s crop of about 400 million pounds, said Jan DeLyser, vice president of marketing for the Irvine-based California Avocado Commission.
“Harvest has increased steadily through April,” she said, “with peak volume expected from mid-April through mid-July.
Eating quality is “outstanding,” she added.
“We’ve kicked off the California avocado season-opener media activities, and our targeted ‘Made of California’ advertising campaign is underway,” she said in mid-April.
 
Citrus
California growers should be shipping navel oranges into June this year, said Bob Blakely vice president of Exeter-based California Citrus Mutual.
That’s not quite as late as the past couple of years because this year’s crop is much smaller, he said.
Valencia oranges currently are being exported, and some will be available domestically when the navel crop winds down.
Meantime, Santa Paula-based Limoneira Co. has moved its lemon harvests from the company’s San Joaquin Valley ranches to the Central Coast, said John Chamberlain, director of marketing.
“Although rains were heavy and delayed getting fruit off trees, there have been some upsides, such as experiencing good size structure with larger fruit,” he said.
 
Cherries
California cherries should have good sizing this season with at least an average-size crop, grower-shippers said.
Stemilt Growers LLC, Wenatchee, Wash., which grows some cherries in California, should have decent volume by the first week of May, said Roger Pepperl, marketing director.
“It most likely won’t be a record, but a very good crop,” he said.
Primavera Marketing Inc., Linden, Calif., expected to start picking cherries in late April, a week or so later than last year, which had an all-time record early start, said sales manager Rich Sambado.
 
Grapes
The Fresno-based California Table Grape Commission expects this year’s table grape crop to at least be equal to last year’s 110 million box units, and commission president Kathleen Nave said volume may well be higher than that.
At Anthony Vineyards, Bakersfield, Calif., co-owner Bob Bianco expected to start grape shipments out of the Coachella Valley the week of May 8, five or six days later than last year.
The company will transition to its vineyards in the San Joaquin Valley around the Fourth of July.
“It looks like it will be a good start to the (California) season,” he said.
 
Melons
Five Crowns Marketing, Brawley, Calif.-based, expected to start picking watermelons and mini watermelons in Mendota and Tracy by the end of June, said Daren Van Dyke, director of sales and marketing.
The company planned to kick off its cantaloupe program May 1 in Brawley then move up to Mendota and Patterson.
Varietal melons and honeydews should be ready for harvest about 10 days after the cantaloupes start.
“Right now the crop looks outstanding,” he said in mid-April.
 
Stone fruit
Apricots, peaches, plums and nectarines from Simonian Fruit Co., Fowler, Calif., will start seven to 10 days later this year than last, said Jeff Simonian, sales manager.
The company will kick off the season with apricots May 8.
“We have a real light set on apricots this year,” Simonian said.
Yellow nectarines will start the week of May 15, followed by yellow peaches the following week and plums the week of May 29.
“We’re expecting a good-quality season,” he said.
 
Strawberries
Strawberries, which already are shipping, were a bit problematic for growers early this season. They were in and out of fields picking, then stripping and cleaning up after numerous rainstorms.
Watsonville-based Well-Pict Inc. started picking in Watsonville in early April, a bit earlier than had been anticipated, said Jim Grabowski, merchandising manager.
He expected shipments to accelerate by early May.
Picking also was “cranking up” in Santa Maria, and Oxnard was peaking in mid-April.
Volume out of Oxnard was not as great as it should have been for that time year, he said, “but they’ll get there.”