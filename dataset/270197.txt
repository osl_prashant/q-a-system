Grains mostly higher and livestock mixed
Grains mostly higher and livestock mixed

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mostly higher Thursday in early trading on the Chicago Board of Trade.
Wheat for May delivery rose 4 cents at $4.7640 a bushel; May corn was up 1 cent at $3.8240 a bushel; May oats gained 2.60 cents at $2.34 a bushel while May soybeans fell 4.60 cents at $10.37 a bushel.
Beef was lower and pork was higher on the Chicago Mercantile Exchange.
April live cattle fell .07 cent at $1.1855 a pound; Apr feeder cattle lost .60 cent at $1.3790 a pound; April lean hogs was up .78 cent at .6988 a pound.