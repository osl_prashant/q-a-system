Man barred from farm where pregnant goat was shot
Man barred from farm where pregnant goat was shot

The Associated Press

SCARBOROUGH, Maine




SCARBOROUGH, Maine (AP) — A man eyed as a possible suspect in the death of a pregnant goat that was shot by a crossbow at a Maine farm has been barred from the property.
The Portland Press Herald reports that a judge issued a protective order for the owner of Smiling Hill Farm against 40-year-old Daniel Arnold.
Police investigating the goat's death arrested Arnold last month on suspicion of possessing a crossbow in violation of a previous probation order, but have not charged him with killing the animal.
Investigators say he was seen near the farm, which straddles Westbrook and Scarborough, on Feb. 17, the night before the goat was found dead. Police say he denied shooting the animal and said he did not have his crossbow with him that night.