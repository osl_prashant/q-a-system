Model year 2014-2018 John Deere 4-Series Sprayers can be outfitted with ExactApply retrofit kits. ExactApply was first introduced as a factory-installed option in 2016 for model year 2018 sprayers. The system improves coverage and control for spray applications with:

industry-exclusive Pulse Width Modulation (30 hertz pulsing)
automatic A/B nozzle switching from the sprayer cab
turn compensation
individual nozzle on/off control and full variable-rate functionality at the nozzle level
smart diagnostics to improve, monitor and document sprayer applications at the nozzle
LED lighting on each individual nozzle

“Now operators of model year 2014 and newer R4030, R4038 and R4045 machines have the same ability to precisely manage droplet size and coverage of products being applied, enhancing the accuracy and efficacy of the applied products while optimizing input costs,” says Doug Felter, product marketing manager for application equipment at John Deere.
Kits include new ExactApply nozzle bodies, new electrical boom harnessing to power and control the nozzles, and a higher capacity alternator for the machine to provide for the added electrical power demands.