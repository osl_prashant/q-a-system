The value of U.S. potato exports grew 4% in the 12 months ending in June, and volumes increased 6%.
Potatoes exported in the 12-month period were valued at $1.69 billion, the second-highest total ever over a 12-month period, according to Denver-based Potatoes USA.
The volume of exports in the period was 3.2 million metric tons, or 71 million cwt.
Fresh potato exports increased 8%, to 441,990 metric tons, and were valued at $183 million. Fresh export numbers cited by Potatoes USA included table-stock, chip-stock and fresh destined for frozen processing.
The top four export destinations for fresh potatoes all saw increases: Canada (up 10%), Mexico (13%), Japan (108%) and Taiwan (18%). Shipments to the fifth-biggest market, South Korean, were down 38%.
Potatoes USA cited the conclusion of West Coast port delays, better market access and Potatoes USA promotional programs as reasons for the rise in exports.
Prospects for future growth are good due to rising worldwide demand for potatoes, though a strong dollar and increased competition could restrain growth, according to Potatoes USA.