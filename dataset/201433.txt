ALERT Aug. 1, 2017 -- Book 70% of 2017 harvest farm diesel needs.


Farm diesel prices showed strong signs of a price recovery in this week's price survey.
No state posted a lower price week-on-week.
Crude oil futures have been on the rise since early June, adding support to farm diesel.
Our heating oil/farm diesel spread indicates near-term upside risk for farm diesel is building.
Our appetite for risk on 100% of harvest farm diesel needs is exhausted.