F.o.b. prices were less than stellar, but quality was good and supplies plentiful as the west Mexico squash deal got underway. Many distributors got off to an earlier start than usual.
Nogales, Ariz.-based Bernardi & Associates Inc. started receiving squash the first week of October, said salesman Manny Gerardo. Squash ususally starts at the end of October.
Growers had an incentive to kick off their squash programs early this year, he said.
“The market on zucchini was really good out of California,” he said. “They saw what the prices were in California, so they hoped to get a good market.”
Ciruli Bros. LLC, Rio Rico, Ariz., will ship Italian green, yellow and Mexican gray squash until April, said Chris Ciruli, partner.
“Right now, the markets are singe-digits and very promotable, so it’s good for the consumer,” Ciruli said.
Rio Rico-based Fresh Farms has been shipping soft squash, such as zucchini, yellow and gray varieties, since late September, said Jerry Havel, director of sales and marketing.
It will add acorn, butternut, spaghetti and kabocha squash in December.
“The market could be better, but the quality has been good,” he said. “I think we’ll have a good Christmas.”
He attributed some of the stepped-up demand to hurricanes in the East this fall.
“There will be less supply from the East and more supply from Mexico,” he said.
Fresh Farms plans to ship squash into May.
Thomas Produce Sales Inc. in Rio Rico shipped some spaghetti squash last year and this year will have spaghetti squash and butternut squash, said Chuck Thomas, owner and president.
“Hard squash seems to be something people are eating more and more of — especially butternut,” he said.