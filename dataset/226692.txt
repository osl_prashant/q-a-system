Two of the better-known indicators of the farm economy, Creighton University’s Rural Mainstreet Index and The Purdue/CME Group Ag Economy Barometer, have both turned higher in January, showing improved moods.

Before soybeans began their rally of an 11-month high, moods at the National Farm Machinery Show in Louisville, Kentucky were relatively optimistic.

According to Bob Utterback of Utterback Marketing, free cash flow is as low as it’s been in more than a decade, and farmers are hoping a dry weather event could be a couple year correction.

“Guys are optimistic but are getting more concerned [about if] this could last,” he said on AgDay.

Another factor weighing on farmers’ minds is the looming threat of rising interest rates. At the Federal Reserve meeting in March, interest rates are expected to rise again.

As a result of the three hikes over the last year, Utterback is seeing more farmers shift from short-term financing to longer-term. He urges farmers if they’re going to make the switch, make it sooner than later.

“You’re probably looking at a point and a half increase in the prime over the next year and a half—that’s going to affect spreads and overall it will start impacting equity markets,” he said.

Hear his full comments on AgDay above.