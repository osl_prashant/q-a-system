Agriculture Secretary Sonny Perdue appointed 13 people to serve on the National Watermelon Promotion Board Dec. 18. 
Newly appointed members to serve three-year terms are:

   Ernesto Chamberlain, Nogales, Ariz.
   Christian Murillo, Nogales,
   Dan O’Connor, Pompano Beach, Fla.
   Chadelle Robinson, Las Cruces, N.M.
   Gerardo A. Lozano, Edinburg, Texas
   Ward Thomas, McAllen, Texas

Re-appointed members are:

   Mathew Singletary, Alva, Fla.
   James Shepherd, Fort Myers, Fla.
   Lee Wroten, Lakeland, Fla.
   Alan Guzi, Pompano Beach, Fla.
   Rolando Rosales, Pompano Beach, Fla.
   Katelyn Kelley Miller, Punta Gorda, Fla.
   Jeff Fawcett, Edinburg, Texas

The 37-member board consists of 14 producers, 14 handlers, eight importers and one public member. The Agricultural Marketing Service provides oversight, paid for by industry assessments, which helps ensure fiscal responsibility, program efficiency and fair treatment of participating stakeholders.