For trucks, trains and ships carrying fresh produce, the mission is simple, according to logistics companies.
"Keeping the product moving from the moment it is picked to delivery," said Victoria Kresge, vice president of dedicated services with Downers Grove, Ill.-based truck-leasing company NationaLease Inc. "Freshness is the key, and any delay en route can result in spoilage."
It helps to anticipate any potential problems, Kresge said.
"It's critical as a carrier to be thorough with route planning, as well as have a contingency plan in case something goes wrong," she said.
Done well, a logistics program will be successful, said Roger Saldivar, division manager with Los Angeles-based Giumarra Trucking, a subsidiary of grower-shipper Giumarra Cos.
"Because we sell a perishable product, maintaining the cold chain is top priority with regard to quality," he said.
San Diego-based Organics Unlimited Inc. harvests the fruit it grows mostly in central Mexico when an order comes in, said Mayra Valazquez de Leon, CEO.
"It is then washed, packed and sent to the U.S., which results in less wasted product spoiling on shelves," she said.
It takes about four days from harvest of organic bananas in central Mexico to reach a warehouse in San Diego, she said.
"This logistics method provides an absolutely fresh product for the customer, and it has a much lower carbon footprint than bananas that are packed in containers and transported via ocean freighters," she said.
Cincinnati-based truck brokerage Total Quality Logistics works closely with its shipping customers to schedule accurately to send the appropriate equipment to keep on-time delivery, said Kerry Byrne, president.
There are no excuses, Byrne said.
"Whether it be product not harvested, delays at the border or product not yet cooled to shipping temperature, we are still expected to get the product to market in a timely manner," he said.


Drivers key

Finding good drivers who know their cargo's needs is the most crucial transportation issue, said Chuck Clowdis, managing director for trade and transportation with Englewood, Colo.-based IHS Markit, which provides analysis and gathers data on transportation.
"You want to find a driver who, first, wants to drive a truck and, secondly, has the wherewithal - the mental capacity and drive - to monitor that, because he has perishable product in his trailer," he said.
Finding drivers of any ability level is probably the most daunting challenge the trucking industry faces now, said Jon Samson, executive director of the Agricultural and Food Transporters Conference of American Trucking Associations, Arlington, Va.
"Getting people behind the wheel is probably one of the biggest concerns we've had," Samson said.
A current shortage of 40,000 to 50,000 drivers could reach 200,000 in 20 years, Samson said.
"Our economist sees some pretty significant driver shortage issues," he said.
If there is a shortfall of drivers, it doesn't affect everybody, and likely barely touches the produce business, said Kenny Lund, vice president of La CaÃ±ada Flintridge, Calif.-based transportation brokerage Allen Lund Co.
"I think the driver shortage is something that's pushed by the larger truck lines. An owner-operator by definition doesn't have a driver shortage," he said.
Large truck lines have "huge turnover," but owner-operators don't, Lund said.
"Produce transportation is dominated by the smaller truckers," he said. "(A driver shortage) is an issue. We'd love to see more drivers and more people go into it."
Regulatory changes - perhaps most notably electronic logs, which record hours of service for all drivers automatically - complicate matters, Samson said.
"We've always continually fought with the different hours of service changes they keep throwing at us, both daily and hourly," he said.
All pieces of the logistics puzzle have to fit in order to ship produce with optimum efficiency, said Vicky Gable, coordinator with Glendale, Ariz.-based Bigelow Truck Brokers.
"It depends on the time of year, but truck availability, marketing prices - it all has to come together," she said. "Often it does, and often it does not."
Technological advances have helped keep produce moving in a timely fashion, said Cole Stoiber, who works in operations with the eastern produce division of L&M Transportation Services in Raleigh, N.C.
"Having stuff in the cooler quicker and planning out the day better has helped get product to the receiver fresher and at the right temperature," he said.
Brentwood, Tenn.-based OHL International works with clients to ensure all required compliance documents are filed with government agencies quickly, said Chris Ryan, key account manager for perishables.
"For us, the most crucial transportation issue is ensuring that fresh produce imports and exports are moved efficiently through port terminals and border crossings," he said.