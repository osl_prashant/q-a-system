Another big drop in spring wheat condition ratings and building dryness sent HRS wheat futures soaring to new contract highs this week. This helped winter wheat to hold near steady on the week at a time when harvest pressure typically weighs on prices. Price action was quite the opposite in corn and soybeans, as the arrival of mild temps and timely rains gave funds incentive to rebuild short positions in these markets. With the weather shaping up nicely as the corn crop’s key pollination period nears, money flow is likely to favor market bears.
News Editor Meghan Vick has highlights:



China accepted its first shipment of U.S. beef in 14 years on June 23 and recent Cold Storage data signaled stronger-than-expected beef demand, but traders were more focused on the big drop in the cash cattle market and the possibility of more declines. Lean hog futures rebounded early in the week, but signals the cash hog market may be working on a top weighed heavily on the market at week’s end.
Click here for this week's newsletter.