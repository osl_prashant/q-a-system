If you have the idea that some invisible hand of the market place is somehow guiding consumers and retailers to where they need to be, recent news out of the United Kingdom is a little unsettling. 
 
The Times story is headlined "Make us sell healthy food, supermarkets implore May"
 
According to the story, British retailers were disappointed that Prime Minister Theresa May over "her  failure to impose laws restricting sugar in food."
 
Retailers were responding to a UK plan to fight childhood obesity
 
The plan, while promising to impose a tax on sugary drinks,  calls for voluntary measures by food manufacturers to limit sugar in foods.
 
According to the plan, all sectors of the food and drinks industry will be challenged to reduce overall sugar across a range of products that contribute to children's sugar intakes by at least 20% by 2020, including a 5% reduction in year one.
 
Being "challenged" and being mandated to cut sugar are two different things.
 
The UK government's obesity plan said the strategy strikes a balance.
 
"We are confident that our approach will reduce childhood obesity while respecting consumer choice, economic realities and, ultimately, our need to eat," the report said.
 
Others say that is nonsense. 
 
Jamie Oliver, the UK celebrity chef and food activist, said the obesity plan falls way short.
 
In a Facebook post, Oliver said
 
"I'm in shock. The long-awaited Childhood Obesity Strategy from Theresa May's new Government is far from robust, and I don't know why was it shared during recess. It contains a few nice ideas, but so much is missing. It was set to be one of the most important health initiatives of our time, but look at the words used – 'should, might, we encourage' – too much of it is voluntary, suggestive, where are the mandatory points? Where are the actions on the irresponsible advertising targeted at our children, and the restrictions on junk food promotions? The sugary drinks tax seems to be the only clear part of this strategy, and with funds going directly to schools that's great, but in isolation it's not enough. This strategy was Britain's opportunity to lead the way and to implement real, meaningful environmental change, to start removing the crippling financial burden from our NHS and reversing the tide of diet-related disease. With this disappointing, and frankly, underwhelming strategy the health of our future generations remains at stake. I sincerely hope the Government's promise to 'take further action where it is needed' is true…
 
But The Times story points out that voluntary measures are a non-starter. Calls for voluntary reductions leave companies that make the first move to cut sugar vulnerable to eroding market share. Previous calls for companies to voluntarily reduce sodium damaged sales of companies that tried to comply first, the story said.
 
The British Retail Consortium, the story reports, believes the only way to achieve the sugar reduction targets enumerated in the obesity plan is by law.
 
That's an eye opener. When industry leaders say "the market" or consumer choice alone is insufficient for transformative change, it is time to listen.
 
The market won't always move consumers in the right direction, or at least in the path that leads to less obesity and health problems.
 
For the U.S., perhaps it is time for produce industry lobbyists to consider more serious measures to reverse childhood obesity. Of course, greater consumption of fruits and vegetables will play a role, but the produce industry also needs to lend its support to a tax on sugary drinks. Proceeds of the tax could fund nutrition education and greater fruit and vegetable access for school children.
 
While the UK childhood obesity plan is dismissed as underwhelming by critics in Britain, it would be a welcome improvement over the current state of obesity policy in the U.S.