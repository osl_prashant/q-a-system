Stemilt Growers LLC is promoting its Pinata apples for Cinco de Mayo.
 
Pinata apples are harvested in mid-October each year and available until June for consumers, according to a news release.
 
"The quality and condition of this year's Pinata apple crop is fantastic," said Roger Pepperl, marketing director at Stemilt, in the release. "Beautiful color, high sugars and balanced acids. This apple gets better every year."
 
Last season Pinata apples were featured in over 7,000 supermarkets in the U.S. and also distributed in Canada. This season, Pinata apples have also sold to most areas of Mexico, according to the release.
 
Stemilt offers retail support with signage, photo and recipe assets, and with social media, important for promoting product around a holiday, according to the release.
 
Stemilt encourages a mix of bulk and bagged Pinatas leading up to and during Cinco De Mayo.