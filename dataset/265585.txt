BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Mon. Mar 26, 2018          USDA Market NewsDaily California EggsPrices are 29 cents higher for Jumbo, 37 cents higher for Extra Large, 38cents higher for Large and 22 cents higher for Medium and Small. Tradesentiment is steady. Demand is moderate to fairly good. Supplies andofferings are light to moderate. Market activity is moderate. Smallbenchmark price $1.95.Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                301EXTRA LARGE          337LARGE                335MEDIUM               215Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 26-Mar-18 12:03 PM E MP