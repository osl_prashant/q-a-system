Connecticut ag liaison dies
Rick Macsuga, a longtime marketing representative for the Connecticut Department of Agriculture, died Oct. 25.
Macsuga, a 33-year employee of the department, had worked with growers and organized farmers markets across Connecticut. He handled administration of the organic cross-share reimbursement program and worked with the Farmer’s Market Nutrition Program and helped to start the Veteran’s Voucher Program.
“Macsuga left the agency for medical reasons early this year,” said Linda Piotrowicz, the department’s bureau director for agriculture development/resource preservation/regional market.
 
Heart of the Harvest looks to expand
Fresh-cut specialist Heart of the Harvest is looking to expand at the Connecticut Regional Market in Hartford, said Bill Driscoll Sr., president.
But finding the extra room isn’t easy, he said.
“We’re struggling with the state of Connecticut to get more space,” he said. “It’s been about three years, myself, looking to get more space. The state was always saying they needed to make templates on the leases.”
Heart of the Harvest filed its final application documents Oct. 30, and the company now is waiting for a response from the state-run market, which has about 70-80 units.
Heart of the Harvest could use another 25,000 square feet, he estimated.