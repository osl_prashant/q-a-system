The United States Department of Agriculture reported this afternoon that 2,914,100 dairy cull cows were slaughtered through Federally inspected plants in 2015, an increase of 98,500 head over 2014. That's an increase of 3.5%.
December 2015 culling was actually down 2,000 head compared to December 2014.
On the hand, December over November culling was up 11%. But there were two more business days in December versus November, so in reality, the number of cattle slaughtered per day was only slight higher in December (67 head).
You can read the full report here.
This article was originally posted at http://www.agweb.com/livestock/dairy/article/dairy-cow-culling-up-almost-100000-head-in-2015-naa-jim-dickrell/.