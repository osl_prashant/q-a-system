CHICAGO — The Packer named Fred Hess, president of Hess Bros. Fruit Co., Lancaster, Pa., as its 2017 Apple Man of the Year.I presented Hess the award at the U.S. Apple Association’s annual Apple Crop Outlook and Marketing Conference Aug. 24 in Chicago.
Hess is clearly loved and respected by the Eastern apple industry, as well as growers across the country.
“To all the young people in this room, I want to say this is a tremendous industry,” Hess said in his acceptance.
His peers described him as a good mentor, a natural teacher, a leader in food safety, a visionary in the managed variety trend, having remarkable personal integrity and unfailingly kind and helpful to others. 
He was even called an apple man to the core.
Hess’s father and uncle started a vegetable farm business in 1969 and began to sell other products such as apples to local grocery stores and restaurants. He began working for the company as a child as its success grew.
By the mid-1990s Hess and his brother took over the business. In 2012, his brother left the business, and Hess brought in 3 new partners from the next generation, who help him run the business today in Lancaster.
Today, Hess Bros. works with more than 70 growers in Pennsylvania, New York, Virginia and Maryland.  The company packs and distributes more than 20 varieties of Eastern apples including its own propriety variety, named Sweet Cheeks by our winner’s wife, Jean.
Hess was also noted for his leadership on many apple organizations including the U.S. Apple Association, Pennsylvania Apple Marketing Board, and the U.S. Apple Export Council.
Sales manager Andy Figart said he remains actively involved with the business and comes to work every day with a tireless energy and enthusiasm.
The U.S. Apple meeting runs Aug. 24-25.