Kansas City, Mo.-based Fairbanks Scales, Inc. has introduced a new generation of the FB6000 weighing instrument.
 
The equipment features a new web interface that allows rapid calibration and custom ticket formatting, according to a news release.
 
The FB6000 can meet the needs for all mid-range single truck scale applications including in/out, gross/tare/net, or basic in/out weighing, according to the release. 
 
The equipment's web interface reduces time spent on calibration and custom ticket formatting, according to the release.