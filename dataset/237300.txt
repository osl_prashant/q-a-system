Tyson fined $2 M for causing Missouri fish kill
Tyson fined $2 M for causing Missouri fish kill

The Associated Press

SPRINGFIELD, Mo.




SPRINGFIELD, Mo. (AP) — Tyson Foods Inc.'s poultry subsidiary has been ordered to pay a $2 million fine for discharges from a southwest Missouri plant that caused a fish kill.
The U.S. Justice Department announced Tuesday that Tyson also must pay $500,000 for damages and serve two years of probation after pleading guilty in September to two counts of violating the federal Clean Water Act. The discharge of an animal feed ingredient into the wastewater treatment system in the town of Monett killed more than 100,000 fish in a nearby stream.
The Springdale, Arkansas-based company said previously in a statement that it was taking full responsibility for the "an unfortunate mistake."
Under the sentence, Tyson also will hire an independent auditor to examine environmental compliance at its poultry facilities, conduct training and improve procedures.