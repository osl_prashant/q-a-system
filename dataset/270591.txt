Mann Packing Co., Salinas, Calif., has added two Fresh Veggie Pasta flavors to its Nourish Bowls line: Basil Pesto and Tomato Bolognese.
They bowls are the first warm vegetable meals to feature plant-based protein, according to a news release. The pesto bowl includes vegan Chick’n and the bolognese option contains vegan sausage, according to the release.
Both bowls use kohlrabi noodles that cook and taste like traditional pasta, according to the release. 
The basil pesto bowl also uses shredded kale, with a pesto sauce and grated parmesan cheese, and has 15 grams of plant-based protein, according to the release.
The tomato bolognese flavor uses marinara sauce, shredded carrots and parmesan and has 14 grams of protein.
Mann Packing plans to show the new bowls at booth No. 1219 the Canadian Produce Marketing Association’s convention, April 24-25 in Vancouver.