California fall fruits could start on the early side and with high sugar content after the state experienced some of the same 100-degree-plus summer weather felt elsewhere in the U.S.
However, results vary by crop. As in spring when strawberry growers started slowly because of wet and cool El NiÃ±o conditions, Bakersfield, Calif.-based Slayman Marketing began pomegranates later than last year. It wasn't by much, though, and the industry expected promotable pomegranate supplies by mid-August.
Navel oranges, on the other hand, could appear earlier in October than retailers are accustomed to for a California fruit that traditionally starts in November.
"The heat is impacting the timing of commodities in different ways," said Ray England, vice president of marketing for Pismo Beach, Calif.-based DJ Forry. "Some are early and some are late."
Most of the state's table grapes are sold after Sept. 1, and that fruit is near its all-time high for volume. Strawberries rewarded buyers' patience in the spring with some high summer yields.
 

Citrus

Navel oranges were giving mixed signals in early August, but the crop could start as early as the first half of October according to Chris Stambach, director of industry relations at California Citrus Mutual, Exeter.
The San Joaquin Valley had recently experienced a 13-day run of temperatures 100 degrees and higher.
"There has been a lot of heat," Stambach said. "That may make the fruit really sweet, but we might have some smaller fruit too."
Pasadena, Calif.-based Sun Pacific expects to start clementines under its Cuties label in late October or early November, said Bob DiPiazza, president. Easy-peel citrus may not match last fall's exceptional crop, which was up 15% to 20% over 2014. However, Sun Pacific anticipates sufficient volume to back fall promotions and to benefit from an increase of 10% to 15% in its murcott and tango varieties that carry Cuties forward after the winter holidays.
 

Grapes

The Fresno-based California Table Grape Commission is estimating the 2016 crop at nearly 117.1 million 19-bound box equivalents. The benchmark of 116.3 million was set in 2013.
"The crop is out there, it's just a question of whether it will finish off and will it all get boxed," said Jon Zaninovich, president of Jasmine Vineyards, Delano, Calif.
More than 60% of California table grapes ship after Sept. 1. Fall varieties include autumn kings, scarlet royals, autumn royals and red globes.
Nick Dulcich, co-owner and president of Sunlight International Sales, Delano, which markets for J.P. Dulcich & Sons, forecast high brix and good flavor.
"Some extreme heat has brought on a lot of sugar, which I believe will help move this crop in an efficient and strategic manner," Dulcich said in early August. "Customers are going to realize that the grapes are just loaded with sugar this year and taste really good."
 

Kiwifruit

California kiwifruit was on pace to surpass last year, when hail and wind damage limited volumes in the Sacramento Valley.
"Last year's crop was down probably 10% to 15%," said Chris Kragie, deciduous sales manager at Madera, Calif.-based Western Fresh Marketing. "With the increases at Sun Pacific and the crop looking normal, I think we'll be around 8.5 million trays."
Western Fresh Marketing hoped to start as early as the third week of September.
Venida Packing, Exeter, expects to ship about 1.4 million trays of kiwifruit between October and February or March.
"In Tulare County, we're seeing similar piece count and better sizing than last year," said Chris Tantau, operations manager.
Sun Pacific is the state's largest kiwifruit grower and sales of its Mighties label have been growing about 30% annually, said Bob DiPiazza, president. Mighties is likely to start the last week of October or the first week of November.
 

Pomegranates

California growers anticipated promotable supplies of pomegranates from Aug. 15 onward, although 80% or more of the crop is in the wonderful variety, which starts around Oct. 1.
"The industry is expecting somewhat lower than normal total volume because there are still growers swapping out pomegranates for nut varieties that they feel will give them higher returns," said Tom Tjerandsen, manager at the Sonoma, Calif.-based Pomegranate Council. "So acreage is declining somewhat, but sizes are going to be up."
Bakersfield, Calif.-based Slayman Marketing, an early variety specialist, kicked off the pomegranate season Aug. 2 with granadas, about two weeks later than last year.
Fowler, Calif.-based Simonian Fruit Co. expected good volumes and sizing.
"Crop size is going to be the same as last year if not slightly larger," vice president of sales and marketing Jeff Simonian said. "The fruit looks like it will be bigger this year due to the more prolonged winter and more water."
Simonian Fruit planned to start its urbanekgranate variety in the second week of September and then proceed to other varieties. Harvest ends in the first or second week of November.
 

Persimmons

Fuyu persimmons - the variety that can be eaten out of hand - will start shipping around the third week of September.
"Volume is similar to last year, maybe off 5% or so," said DJ Forry's England. The company also offers hachiyas, the acorn-shaped persimmons used in baking and drying.
Western Fresh Marketing, which ships fuyus through the season, planned to start persimmons early this year, in the last week of August, with very limited production of a specialty item, chocolate persimmons.
 

Apples 

Fuji volumes are likely to rise about 10% over last year at Primavera Marketing, the leading producer of California apples.
The Stockton-based marketer started fujis close to Aug. 15, with granny smiths following a week later. The green variety will sell through December.
Fujis will finish close to mid-October, according to sales manager Rich Sambado. Around the same time, Primavera will begin offering Pink Lady apples. The company will move about 1.2 million boxes.
Galas started and finished early, not the ideal scenario for California growers contending with old crop Washington or lingering imports. Washington was likely to be early too, which would impact the back end of California's deal.
 

Strawberries

Fresh strawberry volume in July exceeded year-ago numbers, according to the California Strawberry Commission, Watsonville.
For the week ending July 30, 5.4 million trays shipped compared to 4.3 million in 2015. That pattern held throughout the month. The yearly total still lagged, 129.1 million to 143.5 million, but retailers were enjoying an opportunity to make up for sales declines in the spring.
In Watsonville and Salinas, volumes were expected to decrease as transitions to other growing regions approach. Many growers will begin focusing on Oxnard, Mexico and/or Florida starting in October or November.