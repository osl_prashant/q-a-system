LePage says he'll respond in person to 'scandalous' claims
LePage says he'll respond in person to 'scandalous' claims

The Associated Press

AUGUSTA, Maine




AUGUSTA, Maine (AP) — Republican Maine Gov. Paul LePage says he'll respond in person to "totally false and unfounded allegations" leveled by a legislative panel.
The Legislature's Agricultural, Conservation and Forestry committee is set Tuesday to take up an inquiry into whether the LePage administration diverted timber harvested on public lands and whether such timber has been sold to Canada.
The committee asked the LePage administration a list of questions, including whether the governor was involved in decisions to divert wood from public lands. Republican Sen. Paul Davis and Democratic Rep. Michelle Dunphy told LePage the allegations are "disheartening and disturbing" if true.
The Associated Press obtained LePage's response in which he said lawmakers are leveling "scandalous allegations" and accusing his administration of breaking the law.