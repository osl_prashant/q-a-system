In a move that promises to set up a Bronx local food hub less than a mile from the Hunts Point Terminal Produce Market, the state of New York has announced plans to build a $20 million food hub in the Bronx.
The state is also launching the New York State Grown & Certified program, to promote produce and other agricultural products.
That program will identify and promote New York producers who adhere to New York's food safety and environmental sustainability programs, New York Gov. Andrew Cuomo said in a news release. He said the program will help assure consumers that the food they are buying is local and produced with high standards.
The food hub in the Bronx will significantly expand distribution capacity, provide new markets for farmers, and create 95 permanent jobs and 150 construction jobs in Hunts Point, according to the release.

Cuomo said in the release that New York is investing $15 million in the construction of a new $20 million Greenmarket Regional Food Hub in the Bronx. The facility will support farms that are New York State Grown & Certified by promoting their products to restaurants and institutional buyers, according to the release. The food hub will include a wholesale farmers' market, a cold storage facility for growers, a food-processing center and other infrastructure to support local food businesses, according to the release.

The plan by the state to build a local food hub in the Bronx wasn't known by Hunts Point Terminal Produce Market operators, said market co-chairmen Joel Fierman, president of Fierman Produce Exchange Inc. "It's not a plan they have discussed with us, it is a plan they are doing with the upstate growers," he said.
Fierman said he wasn't aware of where state wants to build the food hub.
"I just hope they have to meet FSMA, Primus and all the other safety standards that the market as to meet," he said. "That's our only concern."
Fierman said the local food hub could be a destination for produce buyers.
"We welcome the buyers coming to both markets, that's not a problem," he said. "The problem is our primary concern is food safety and we just hope we follow the same guidelines this market has to follow."

The state's move to build a local food hub raises questions about what comes next for the Hunts Point Terminal Produce Market.

Earlier this year, New York's development agency Empire State Development provided $250,000 for the Hunts Point Terminal Produce Cooperative Association to conduct a feasibility study on the best way to upgrade the facilities at the market. Officials said that study was expected to be completed by September.
Richard Ball, commissioner of the New York State Department of Agriculture and Markets, told The Packer Aug. 15 that the state still supports long sought modernization of the Hunt Point produce market.
"The lion's share of New York product that winds up in the urban areas goes through that market," he said. "That needs to continue."
Ball said he would like to see more progress on discussions between the city and the market on updating the market and ideas on how the state could play a role.
However he said the food hub project targets different audiences.
"I'm sure there is some overlap, but the overall positive effect is that there be more New York grown product getting to more people," he said.
Ball said the state looks forward to working with the produce market to sell more New York grown produce.
"Some of the same customers will utilize both but I don't see (the planned food hub) as an alternative at all," Ball said.
The planned food hub will be a 120,000-square-foot facility in the Bronx, less than a mile away from the Hunts Point Terminal Produce Market, Ball said. By way of comparison, the Hunts Point produce market has more than 1 million square feet of interior space, according to the market's website.
The food hub facility, planned for currently vacant land, will be located between the produce terminal market and the Hunts Point fish market.
Ball said part of the agricultural department's mission is to increase food access in areas of the city that are food deserts.
"We think it raises the tide level for an awful lot of boats," he said.
The project, still requiring approval, funding from New York state and USDA and environmental studies, may take a year to a year and a half to complete, Hall said.
Modernization of the Hunts Point Produce Market and the construction of the food hub would be mutually beneficial, Ball said. "As a grower who shipped to Hunts Point, I have long been frustrated with their challenges and I know the quickest and simplest way to move more product to the metropolitan area is to have the arteries unclogged to have adequate refrigeration and loading dock space (at the Hunts Point produce market)," he said. "It is long overdue that it gets the attention it deserves."
Grower value
Hall said the New York State Grown & Certified program has received enthusiastic responses from grower and retail groups. Ball said the program will ramp up this fall. "We will roll this out, get the seal approved, get banners and signage made, but we will start elevate (the program) through the fall," Ball said.
When consumers see the New York Grown & Certified label, Ball said they will know the produce is from New York, they will know farmers have a food safety plan and Good Agricultural Practices audit and they will know that farmers have an environmental strategy on their farms.
The program answers the demand for transparency about food safety and environmental stewardship on the farm, Allen said. "It is meeting the consumers' needs and connecting the dots in a good way for our growers."
One vegetable marketer, speaking on condition of anonymity, said one question he had about the program is how its food safety and environmental requirements might challenge small growers.
If the label is just for marketing through to supermarkets or wholesalers, the label would be appropriate. But those growers already have to comply with Good Agricultural Practices, the grower said.