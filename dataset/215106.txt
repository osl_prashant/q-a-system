Warm weather and good growing conditions prompted an early start and plentiful supplies of winter produce items out of west Mexico this season.
Ciruli Bros. LLC, Rio Rico, Ariz., started its specialty eggplant program in early November and expected to start shipping green beans by the middle of the month, said Chris Ciruli, partner.
“Beans will be promotable for the month of December,” he said.
He expects to see an abundance of product arrive in the post-Thanksgiving period.
“By the first week of December, we’ll be rolling with everything from green bell peppers to squash,” he said.
About the only item the company will lack early on will be tomatoes, which should start arriving Dec. 15-25.
Early markets were better than usual on squash, cucumbers and bell peppers, he said, and some distributors already were seeing a strong tomato market, which should continue through Christmas.
Nogales, Ariz.-based Big Chuy Distributing Co. Inc. started shipping squash the first week of November, said Jesus “Chuy” Lopez Jr., sales manager.
The company has acorn, spaghetti and butternut squash and is in its fourth season of organic squash.
Sizing has been good, as well, with most of the squash arriving in medium to large sizes, he said.
Rio Rico-based Thomas Produce Sales Inc. launched a spring cucumber deal last year and has added a fall-winter program this season, said Chuck Thomas, owner and president.
The program started out of Sonora in late October and will switch to Sinaloa in mid-December.
The bell pepper deal continues to expand, said Jose Luis Obregon, president of IPR Fresh, Rio Rico.
IPR Fresh will have hothouse-grown red, yellow and orange bell peppers and shade house-grown green bells.
Hothouse peppers are available from IPR Fresh from Sept. 1 through May, and green bell peppers start in mid-November and ship through April.
Rio Rico-based Grower Alliance LLC expects to kick off its roma and beefsteak tomato programs from Culiacan in mid-December, said partner Jorge Quintero Jr.
The company already was importing tomatoes through McAllen, Texas, from Michoacan and Jalisco.
“Quality has been great, and the market has been good,” Quintero said. “It’s definitely a good way to start the Nogales season.”
Ciruli said he was surprised by a strong eggplant deal.
“We have a very high eggplant market, and it seems like it’s going to stay that way through the holidays,” he said in early November.
The market opened higher than usual, possibly because of tight supplies from the East Coast resulting from the fall hurricanes.
Items from west Mexico could experience higher demand because of planting delays or lost crops in the East, he said.
“People are going to need product out of Nogales.”
In the meantime, while the process of moving fresh produce through the Nogales-Mariposa Port of Entry continues to improve, the facility still could use some additional personnel — especially customs agents — said Chuck Thomas, owner and president of Thomas Produce Sales Inc., Rio Rico.
Some military veterans have been hired, he said, and crossing is a lot better than it used to be.
Bringing a load of produce across the border used to involve wait times of six to eight hours, he said. Now the wait is down to a half hour to two hours at most.