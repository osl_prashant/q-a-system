MONTEREY, Calif. – Most people know what the produce and foodservice trends are, but businesses will be successful when they can implement them.The education sessions at the annual Produce Marketing Association Foodservice conference and expo July 29 had a more news-you-can-use feel this year, including the urge to capitalize on trends from David Nour of the Nour Group.
“We don’t need new ideas,” he said. “We need ways to implement them.”
Nour said food strategies need to more simple and straightforward.
“If you have to explain your strategy, it’s too complicated,” he said.
Fellow presenter Joe Pawlak from Technomic, said taste and transparency matter the most to consumers, especially the millennial generation.
He posed a question to the audience, what one term best describes produce as a menu item that’s healthy and tasty?
It’s “fresh.” The audience knew it easily.
He said some trends are surprisingly easy to implement, such as using more exotic spices and sauces traditionally used for meat but on produce. For instance, he said vegetables with buffalo sauce are getting great consumer response in restaurants.
On the subject of home delivery meal kits, the thing consumers value most is ease of preparation, he said.
“81% of consumers say cooking at home is a good way to live,” Pawlak said, but so many consumers don’t know how. Meal kits are a solution.
Third party food delivery is growing, but a college student consumer panel of three showed there are significant problems. Two of the three said they never used them, and one said she only used it once. The problem is the cost.
Speakers throughout the day said modern consumers want to hear the story associated with their food, and produce companies all have a good story to share.
Ron DeSantis, owner of CulinaryNXT, and a former culinary teacher said one trend needs very little explanation.
“When I hear ‘farm to table,’ I say, ‘where the heck to do people think it comes from?” he said.