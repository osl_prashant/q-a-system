With an eye on sustainability, as well as potentially offering consumers price breaks on aesthetically "imperfect" produce, SunFed is launching its Almost Famous program in stores nationwide.
"Sustainability has a changing definition for consumers, and one of those shifting aspects is the desire to find programs that seek to decrease food waste," Brett Burdsal, vice president of marketing for Rio Rico, Ariz.-based SunFed, said in a news release.
"We have always used this imperfect produce outlet and have continued to look for ways to maximize output from the farm in terms of production and find homes for the fruits and vegetables."
The Almost Famous program brings cosmetically imperfect produce items that have the same freshness and flavor as the company's No. 1-graded branded items, according to the release.
Burdsal said consumers likely will catch a price break on the items.
"Obviously, we can't control what retailers do, but we definitely think so, that's the idea," Burdsal said.
"This is stuff that doesn't make the cut under the current retail specs, but it's still extremely good, so this should be sold at a lower price than a regular bell pepper or regular eggplant."
The company saw an opportunity to start something new, as it "noticed people were responding to reducing food waste," he said.
SunFed offers items ranging from eggplant, green bell peppers and colored bell peppers to zucchini, cucumbers and yellow squash, depending on the time of year and product availability.
Almost Famous vegetables are packed in bags.
With the value proposition of a grab-and-go item, this program addresses the challenge for retailers of selling the items in bulk and also ensures the produce is rung up correctly at the register, according to the release.