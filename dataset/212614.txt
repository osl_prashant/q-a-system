There may be no commodity that benefits more from the fresh-cut segment than melons, marketers say.The fresh-cut segment continues to gain popularity due to the busy lifestyles of consumers today, said Josh Knox, melons category manager with Eden Prairie, Minn.-based Robinson Fresh.
He trotted out some numbers to make the case.
“Melons represent 43% of fresh cut fruit sales, ranking the No. 1 fresh-cut fruit item,” he said. “Last year, fresh-cut melons generated over $1 billion in retail sales, accounting for 39% of total retail melon sales.”
Additionally, he said, about half of fresh-cut watermelon sales occur during fall and winter months.
“I believe the other driving factor is cut gives the consumer an opportunity to see the finished good, whereas whole does not,” Knox said.
Melon grower-shippers look to fresh-cut as a dependable outlet for their product, marketers say.
Some retailers are even returning to cutting the melons themselves, rather than buying it through processors.
“Years ago, they cut it in the back room and it didn’t amount to much. Then the food safety came in and third-party cutters processed it and delivered it to the store,” said Barry Zwillinger, partner with Dos Palos, Calif.-based Legend Produce LLC, which is one of two companies licensed to market the proprietary Origami cantaloupe.
“Now, we’re coming full circle. Some of our largest retailers, during certain times of year, their consumption is 50% to 90% cut melon and they cut it in the store. The Albertson’s-Safeway group and Publix cut internally and their consumption is greater than the whole fruit when they’re not on ad.”
The fresh-cut market is “very huge” to Brawley, Calif.-based Five Crowns Marketing, said Daren Van Dyke, sales and marketing director.
Five Crowns is the other company that can market the Origami.
“(Origami) has a nice bright color. It has virtually no seed cavity, no breakdown,” Van Dyke said. “We’ve had a lot of processors come to us. You don’t get a lot of waste.”
Autryville, N.C.-based Jackson Farming Co. sends watermelons, cantaloupes and honeydews to processors in different states, said Matt Solana, vice president of operations/supply chain.
“This market has the greatest growth potential at retail, with more time-strapped consumers reaching for the fresh-cut section in the produce department of their supermarket,” he said.
Price might be an obstacle in the fresh-cut market, however, said Jeff Fawcett, salesman with Edinburg, Texas-based Bagley Produce Co.
“The cut melon program is a tough one,” he said. “As a consumer, I think it’s very expensive.”
A whole melon, on the other hand, has longer shelf life, so it doesn’t have to be consumed immediately, he said.
A lot of consumers feel it’s worth a bit more expenditure for the convenience of ready-to-eat melon chunks, said Ramon Murilla, president of Nogales, Ariz.-based Cactus Melon Corp.
“You’ll pay a little more, but at least you know you’re going to have a good-colored and sweet watermelon,” he said. “Cutters will look for color, consistency and sugar content.”
Fresh-cut sales will plunge during the spring and summer, though, Murilla said.
“Spring and summer, that’s different. Everybody wants watermelon, and they’ll take the whole melon,” he said.