Missouri lawmakers have given final approval to a bill increasing a fine for illegally using herbicides that damage other farmers' crops.
The legislation passed the House on Thursday by a 139-18 vote after passing the Senate on Wednesday. It now goes to Gov. Eric Greitens. If it is signed, the legislation will go into effect immediately.
The bill gives the Department of Agriculture more power to fine people who damage other farmers' crops, land, or property by using herbicides not marked for the used purpose.
The farmers can be fined up to $10,000 for each instance of damage and $25,000 for repeat offenders.
Supporters of the bill hope it will prevent the illegal spraying of herbicides such as when crops were damaged in southeast Missouri in 2015 and 2016.