AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:15 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
UPCOMING TOMORROW:
DEATH PENALTY-MENNONITE JAILED
DENVER — A defense investigator in a Colorado death penalty case is behind bars for refusing to testify for prosecutors, saying that helping their fight to execute the defendant would violate her religious beliefs.
TOP STORIES TODAY:
COLORADO LEGISLATURE-SEXUAL HARASSMENT
DENVER — A Democratic lawmaker accused of sexually harassing a fellow legislator and other women should be expelled, fellow party members who control Colorado's state House said Tuesday. A private, third-party investigation found credible allegations against Rep. Steve Lebsock, House Majority Leader KC Becker told lawmakers. By James Anderson. SENT: 410 words, photos.
COLORADO IMMIGRATION LAWSUIT
DENVER — The Colorado American Civil Liberties Union on Tuesday filed a lawsuit contending a sheriff's department is improperly holding people in the country illegally on behalf of federal authorities in what it says is the first legal challenge to a Trump administration effort to work around court rulings limiting how it can work with local jails to enforce immigration laws. The complaint in Colorado District Court alleges that El Paso County Sheriff Bill Elder "holds prisoners in custody for days, weeks and even months after state law requires their release." By Nicholas Riccardi. SENT: 360 words.
SCHOOL SHOOTING-FLORIDA-ADVICE
PARKLAND, Fla. — After school shootings like the massacre at Marjory Stoneman Douglas High, administrators reach out to former Columbine High principal Frank DeAngelis for advice, since there is no book to teach what he learned after gunmen killed 12 of his students and a teacher in 1999. There should be no balloons at Stoneman Douglas' welcome-back ceremony, he told the school's administrator. The reason: Some balloons popped at Columbine's reopening, sending students diving for cover. Have substitutes on hand in case teachers need time to compose themselves. Change the sound of the fire alarm, which got pulled at both Columbine and Stoneman Douglas during the shootings, or it will cause some to panic. By Terry Spencer. SENT: 800 words, photos.
SCHOOL SHOOTINGS-VIEWS FROM COLUMBINE
DENVER — Patrick Neville was outside, sneaking off to smoke with friends, and avoided the outburst of gunfire at Columbine High School nearly two decades ago, but he did not dodge the heartbreak. A close friend died, and the anguish in his father's eyes is seared in Neville's memory. Samantha Haviland was fundraising in the cafeteria and froze, uncomprehending, at the sound of screams just outside the window. Trance-like, she and others fled the room, then pressed against a wall of lockers, windows shot out down the hall. She, too, lost a close friend. By P. Solomon Banda and James Anderson. SENT: 900 words, photos and video.
OF COLORADO INTEREST:
YOUTH SPORTS-CHILD ABUSE
SEATTLE — With Olympic prodigies having just dazzled audiences worldwide, parents in the U.S. are reconciling the thrill of the gold with their fears from recent sexual abuse scandals in elite youth sports. Shannon Stabbert said her 6-year-old daughter wants to be a gymnast, but the Seattle mother decided to put her in a martial-arts program instead. By Sally Ho. SENT: 980 words, photos.
IN BRIEF:
— GREEN RIVER-PIPELINE PROPOSAL — A Colorado man has brought back a disputed proposal to pipe water from the Green River watershed several miles to Colorado's Front Range.
— FORT COLLINS HOMICIDE-SENTENCING — A man convicted of killing a Fort Collins mother, whose body was found in a lake in June, has been sentenced to life in prison without the possibility of parole.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for March rose 3.75 cents at 4.6325 a bushel; March corn was up 2 cents at 3.7050 a bushel; March oats fell 2.50 cents at $2.60 a bushel; while March soybeans gained 3.75 cents at $10.38 a bushel.
SPORTS:
CLIPPERS-NUGGETS
DENVER — The Denver Nuggets host the Clippers. (Game starts at 8:30 p.m. MT)
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.