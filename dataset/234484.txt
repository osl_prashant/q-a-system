It wouldn’t be a Southern Exposure without a little fun and games.
This year’s event, with its Elvis theme, features booth decorating and costume contests for exhibitors and attendees, and an opportunity to find the Elvis-themed Golden Gators to win some Southeast Produce Council swag.
The hunt for the Golden Gators works like this:

Find the golden gator;
Take a selfie;
Post to your preferred social media with the hashtag #SEPCSouthernExposure; and
Turn the gator in at the registration desk to redeem a prize.

The event also will feature an expo of charitable organizations including the Paul Anderson Youth Home, Society of St. Andrew, Arnold Palmer Children’s Hospital and Children’s Healthcare of Atlanta, as an opportunity for exhibitors and attendees to make connections and build community.
SEPC also partnered with Produce for Pencils through DonorsChoose.org, to help provide tools and experiences for students in selected communities.
SEPC encourages attendees to donate at the registration desk.