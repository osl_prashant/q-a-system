It comes as no surprise to people with common sense, but it’s refreshing to see research reviews that show physical inactivity as the real cause of chronic disease.
A new review of more than 500 studies examines the environmental and physiological causes of physical inactivity and the role it plays in the development of chronic disease, according to the American Physiological Society. The article was recently published in Physiological Reviews.
“Physical inactivity is an actual cause of over 35 chronic diseases [and] conditions, with evidence from studies that physically inactive groups have increased prevalence,” wrote the research team that conducted the literature review. According to the article, the researchers were from the University of Missouri, University of Kansas Medical Center and VA Greater Los Angeles Healthcare System.
The World Health Organization reports that the combination of sedentary behavior and poor diet is the second leading cause of death in the U.S., the article states.
Additionally, 86% of people in the U.S. fail to meet recommended guidelines for daily exercise—a phenomenon that has been on an upward curve since the advent of “power-driven machines and motorized transportation,” the researchers noted.
While diet certainly plays an important role, the culprits are calorie- and sugar-dense snack foods and processed foods, not the valuable protein found in meat and dairy products.
People who are not active may be up to 50% more likely to develop conditions that are considered major causes of death, such as heart disease, type 2 diabetes and Alzheimer’s disease.
In addition, prolonged inactivity affects numerous bodily functions, such as:

cardiovascular fitness
bone density
nervous system function
muscle strength
cognition

Among other findings, the reviewed studies showed that inactivity and activity operate on different molecular pathways. Understanding these differences, the researchers explained in the article, may help pave the way for better, more personalized prevention tools and treatment options—including individualized exercise prescriptions, medications and targeted gene therapy.
“It is important that the public understand that physical inactivity itself is underappreciated in the toll it takes on the health, quality of life and health care costs of U.S. citizens and individuals around the world,” the research team wrote in the article. “By increasing physical activity levels in children and adults, we can ameliorate the physical, emotional and economic burden that occurs among inactive people in our society.”
Read the full article, “Role of inactivity in chronic diseases: evolutionary insight and pathophysiological mechanisms,” published in the October issue of Physiological Reviews. Share it with your friends the next time they want to place the blame for chronic health problems on livestock producers.