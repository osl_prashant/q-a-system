Capitalizing on the trend of mixing chopped mushrooms with ground meat — known as “blending” — Monterey Mushrooms plans to introduce a new product line called Let’s Blend at the Produce Marketing Association’s Fresh Summit.
The 8-ounce packages of finely diced mushrooms come in three flavors: classic, Mexican and Italian.
“People want to eat better without giving up flavor and texture,” Monterey Mushrooms marketing specialist Lindsey Roberts said in a news release. “Let’s Blend works well with ground beef, chicken, lamb, pork or turkey.
“It makes meat juicier without adding fat or cholesterol.”
Let’s Blend products have an extended shelf life, come in recyclable packaging and include cooking instructions on the packages, according to the release.
The company will also have other products on display, including vitamin D and clean-and-ready mushrooms, during the expo, Oct. 21-22, in New Orleans, at booth No. 1906.