Photo courtesy Wada Farms Marketing Group
Wilcox Fresh remodels facility
Wilcox Fresh has finished remodeling its Rexburg, Idaho, packing facility. The update includes a new grading and sizing system, new palletizer and stacking robots.
The equipment is planned to be up and running the week of Oct. 23.
“These updates will allow us to be less dependent on labor and improves our sizing and grading to be much more accurate,” said Derek Peterson, vice president of sales and marketing for Wilcox Fresh. “This is the biggest change in this building since 1998.”
 
Wada expands organics, runs cross-promo
Wada Farms Marketing Group, Idaho Falls, Idaho, is expanding its organic program to include value-added items such as its Easy Baker microwave individually wrapped russet potato.
“We also have a co-branded promotion running with Mrs. Smith pumpkin pies during the holiday months at select retailers,” said Kevin Stanger, president. “All bags of potatoes will have a coupon to take advantage of instant savings.”