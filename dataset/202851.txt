Slowly but surely, the west Mexico deal has expanded over the years.
Not that long ago, the season got underway around Thanksgiving and wound down at the end of March or maybe stretched to mid-April, said Chuck Thomas, owner and president of Thomas Produce Sales Inc., Rio Rico, Ariz.
Now, the watermelon deal alone is stretching the limits of the season.
"It's unheard of, how many watermelons are crossing in late spring and early summer," he said. "It's rivaling the big tomato push of the old days."
Watermelons now ship until the end of June, and the grape deal along with mangoes and some leafy items go well into summer, he said.
The peak of the winter deal, volumewise, used to be January or February, said Chris Ciruli, partner at Ciruli Bros. LLC in Rio Rico.
Now it's more like March and April, he said.
Some growers are going farther south into Mexico and setting up greenhouses, Ciruli said. "They're bringing (product) in here a lot longer."
They try to bring as many crops as they can as long as they can "to bridge into the U.S. crops in May and June," he said.
Nogales, Ariz.-based Calavo Growers Inc. is taking advantage of crops grown in shade houses or greenhouses in Jalisco that are disease resistant and offer other growing advantages, said Brian Bernauer, director of operations and sales.
"Those things are a big factor in how the seasons are extended," he said.
Just as some product from west Mexico now is imported through McAllen, Texas, during the fall and winter to offer a freight advantage for the Midwest and Eastern markets, some product from Jalisco now comes through Nogales during the spring and summer to offer freight advantages to West Coast buyers, he said.
Also, Calavo and other tomato growers now ship from Mexico year-round by sourcing from new growing areas during what traditionally has been the off season, Bernauer said.
Growers are expanding into different areas, agreed Jimmy Munguia, sales manager for Del Campo Supreme Inc., Nogales.
In the past, most of the product the company shipped came out of the state of Sinaloa, he said.
"Now it seems like the Sonora season has expanded," Munguia said. "You've got a lot of romas and vine-ripes grown in the city of Obregon now that didn't exist years ago."
Volume is increasing significantly, he added.
"Sonora is big," said Mikee Suarez, salesman for MAS Melons & Grapes LLC, Rio Rico. "That's where the expansion is."
Almost all of Mexico's grapes come from the Caborca and Hermosillo regions of Sonora, he said.
And volume constantly is increasing as a result of improved growing practices and new grape varieties that are more efficient, sprout out more buds for more volume and use less water, enabling growers to add acreage, he said.
"It seems to me that Nogales starts earlier every year, and we seem to go longer," said Jerry Havel, director of sales and marketing for Fresh Farms in Rio Rico.
"We go all the way into July with grapes," he said, and some distributors go even further with some items.
"The trend here with the bigger shippers is looking for opportunities to go year-round," he said.
It makes sense for Nogales distributors to ship as long as they can, Thomas said.
"It was senseless to have multimillion dollar facilities down here and have a workforce that only worked five, six or seven months out of the year," he said.
Workers would be collecting unemployment checks and buildings would be sitting idle.
Now, some distributors ship 80% or 90% of the time, he said.
"It needed to be done, and they figured out how to do it," Thomas said.