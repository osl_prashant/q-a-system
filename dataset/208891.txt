During challenging financial times, strategic direction and business planning are vital—albeit intimidating. As the leader of your operation, you need to provide the direction for your team. Do so by empowering your employees, being an agile decision maker and thinking broadly about what can throw you off course.At ONE: The Alltech Ideas Conference, an event dedicated to inspiring innovation, speakers touched on many ways to think about the future. Here are a few tactical lessons from speakers you can apply on your own operation.
1. Focus on Your Business’ Direction and Ideal Destination.
Oftentimes, business leaders spend too much time focusing on where they’ve been, says Aidan Connolly, Alltech chief innovation officer and vice president of corporate accounts.
The traditional strategic model answers some key questions in the following order:
1: Where did we start?
2: Where are we now?
3: How we will get there?
4: Where do we want to go?
Instead, Connolly suggests, swap the order to: 2, 1, 4, 3
“Spend more time focusing on the destination,” he says.
 
 
2. Kill Stupid Rules.
We live in a complex, always-on world. This is a major hurdle for business leaders, says Lisa Bodell, CEO of futurethink, a firm that trains businesses how to become innovators. “Complexity is the enemy of meaningful work,” she says. “It holds us back. People incorrectly think complicated systems are more meaningful and valuable.”
As you develop your strategic and business plans, enact one of Bodell’s tips: Kill a stupid rule.
“When I do this with leaders all over the world, I’m astounded at how many things they come up with,” she says. “Most of the things are not rules, simply annoyances like meetings, reports, emails, etc.”
Ask your team: What would you love to stop doing? What are your biggest time wasters? What tasks have little ROI? Identify which tasks can easily go.
“People love it when you give them permission to stop doing things,” she says.
 
Read three more tips from Top Producer