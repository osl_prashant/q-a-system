USDA Weekly Export Sales Report
			Week Ended Oct. 12, 2017





Corn



Actual Sales (in MT)

Combined: 1,270,800
			2017-18: 1,254,900
			2018-19: 15,900 



Trade Expectations (MT)
2017-18: 800,000-1,100,000


Weekly Sales Details
Net sales of 1,254,900 MT for 2017-18 were down 21% from the previous week, but up 58% from the prior 4-week average.  Increases were reported for Mexico (510,100 MT), Japan (233,800 MT), Honduras (132,200 MT, including 132,100 MT switched from unknown destinations), Peru (117,200 MT), and Colombia (98,100 MT, including 75,000 MT switched from unknown destinations and decreases of 6,000 MT).  Reductions were reported for unknown destinations (16,900 MT) and Canada (14,400 MT).  For 2018-19, net sales of 15,000 MT were reported for Peru. 


Weekly Export Details
Exports of 339,300 MT were primarily to Mexico (166,100 MT), Colombia (80,100 MT), Peru (39,200 MT), Japan (28,600 MT), and Canada (12,600 MT).


Comments and Performance Indicators
Sales were stronger than expected. Export commitments at the start of the 2017-18 marketing year are running 34% behind year-ago compared with 36% behind last week. USDA projects exports in 2017-18 at 1.850 billion bu., down 19.3% from the previous marketing year.




Wheat



Actual Sales (in MT)

2017-18: 615,400



Trade Expectations (MT)
2017-18: 250,000-450,000


Weekly Sales Details
Net sales of 615,400 metric tons for delivery in marketing year 2017-18 were up noticeably from the previous week and 75% from the prior 4-week average.   Increases were for Mexico (180,000 MT), China (120,000 MT), unknown destinations (94,000 MT), Japan (65,100 MT), and Iraq (50,000 MT, switched from Turkey).  Reductions were reported for Turkey (50,000 MT).  


Weekly Export Details
Exports of 315,000 MT were down 3% from the previous week and 34% from the prior 4-week average.  The primary destinations were Indonesia (74,200 MT), Colombia (45,500 MT), Israel (45,100 MT), Mexico (38,300 MT), and Japan (34,200 MT).


Comments and Performance Indicators
Sales were stronger than expected. Export commitments for 2017-18 are running 4% behind year-ago versus 5% behind the week prior. USDA projects exports in 2017-18 at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual Sales (in MT)

2017-18: 1,275,200



Trade Expectations (MT)
2017-18: 1,300,000-1,700,000 


Weekly Sales Details
Net sales of 1,275,200 MT for 2017-18 were down 27% from the previous week and 36% from the prior 4-week average.  Increases were reported for China (1,174,800 MT, including 523,000 MT switched from unknown destinations and decreases of 68,500 MT), Pakistan (70,800 MT, including 65,000 MT switched from unknown destinations), Germany (66,600 MT, previously reported as the Netherlands), Turkey (65,700 MT, switched from unknown destinations), and the Netherlands (52,700 MT, including 66,000 MT switched from unknown destinations and decreases of 13,300 MT).  Reductions were reported for unknown destinations (317,400 MT) and Costa Rica (41,500 MT). 


Weekly Export Details
Exports of 1,850,000 MT were primarily to China (1,371,000 MT), Spain (70,600 MT), Pakistan (69,300 MT), Germany (66,600 MT), and Turkey (65,700 MT).


Comments and Performance Indicators
Sales were a bit lighter than expected. Export commitments are running 17% behind year-ago compared with 16% behind last week. USDA projects exports in 2017-18 at 2.250 billion bu., up 3.5% from year-ago.




Soymeal



Actual Sales (in MT)
2017-18: 296,000 


Trade Expectations (MT)

2017-18: 100,000-250,000



Weekly Sales Details
Net sales of 296,000 MT for 2017-18 were reported for the Dominican Republic (106,100 MT), Thailand (50,000 MT, switched from unknown destinations), Colombia (35,000 MT), Honduras (31,000 MT), Nicaragua (24,300 MT), and Panama (23,600 MT).  Reductions were reported for unknown destinations (37,400 MT) and Japan (1,600 MT).  


Weekly Export Details
Exports of 154,700 MT were reported to the Philippines (77,900 MT), Canada (15,100 MT), Mexico (13,100 MT), Morocco (12,000 MT), Peru (10,000 MT), Guatemala (8,700 MT), and Israel (5,100 MT).


Comments and Performance Indicators
Sales topped expectations. Export commitments for 2017-18 are running 8% ahead of year-ago versus 13% ahead of year-ago to kick off the marketing year last week. USDA projects exports in 2017-18 to be up 3.5% from the previous marketing year.




Soyoil



Actual Sales (in MT)

2017-18: 27,400 



Trade Expectations (MT)
2017-18: 5,000-22,000


Weekly Sales Details
Net sales of 27,400 MT for 2017-18 were reported for Guatemala (11,000 MT), Mexico (7,900 MT), and the Dominican Republic (5,600 MT), were partially offset by reductions for Canada (100 MT).  


Weekly Export Details
Exports of 6,500 MT were reported to the Dominican Republic (3,200 MT), Mexico (2,800 MT), and Canada (400 MT).








Comments and Performance Indicators
Sales topped expectations. Export commitments at the start of the 2017-18 marketing year are running 59% behind year-ago compared with 69% behind year-ago to start the 2017-18 marketing year last week. USDA projects exports in 2017-18 to be down 17.6% from the previous year.




Cotton



Actual Sales (in RB)
Combined: 262,000
			2017-18: 253,200
			2018-19: 8,800 


Weekly Sales Details
Net sales of 253,200 running bales for 2017-18 were up 64% from the previous week and 39% from the prior 4-week average.   Increases were reported for Vietnam (61,600 RB), Bangladesh (61,500 RB), Turkey (33,900 RB), China (21,300 RB), and Thailand (18,800 RB).  For 2018-19, net sales of 8,800 RB were reported for China. 


Weekly Export Details
Exports of 86,100 RB--a marketing low--were down 27% from the previous week and 36% from the prior 4-week average.  Exports were reported primarily to Mexico (19,600 RB), Vietnam (17,100 RB), China (14,600 RB), Indonesia (8,000 RB), and South Korea (7,500 RB). 


Comments and Performance Indicators
Export commitments for 2017-18 are running 36% ahead of year-ago compared to 40% ahead last week. USDA projects exports in 2017-18 will decline 2.8% from the previous year to 14.5 million bales.




Beef



Actual Sales (in MT)

Combined: 14,400
			2017: 13,600 
2018: 800



Weekly Sales Details
Net sales of 13,600 MT reported for 2017 were up 5% from the previous week, but down 13% from the prior 4-week average.  Increases were reported for Japan (4,600 MT), Hong Kong (2,800 MT), South Korea (2,100 MT), Mexico (1,500 MT), and Taiwan (1,300 MT).  Reductions were reported for Indonesia (100 MT).  For 2018, net sales of 800 MT were reported for Japan (400 MT), Hong Kong (300 MT), and Mexico (100 MT). 


Weekly Export Details
Exports of 15,900 MT were unchanged from the previous week, but down 1% from the prior 4-week average.  The primary destinations were Japan (4,600 MT), South Korea (4,000 MT), Hong Kong (2,400 MT), Mexico (2,000 MT), and Taiwan (900 MT). 


Comments and Performance Indicators
Last week, USDA reported export sales totaling 14,500 MT for 2017 and 2018, combined. USDA projects exports in 2017 to be up 10.9% from last year's total. 




Pork



Actual Sales (in MT)

Combined: 14,200
			2017: 11,400 
2018: 2,800 



Weekly Sales Details
Net sales of 11,400 MT reported for 2017 were down 28% from the previous week and 51% from the prior 4-week average.  Increases were reported for Mexico (3,400 MT), Japan (2,600 MT), Colombia (1,200 MT)South Korea (1,000 MT), and Canada (900 MT).   For 2018, net sales of 2,800 MT were reported for Chile (1,100 MT), Colombia (1,000 MT), and Mexico (700 MT).


Weekly Export Details
Exports of 21,100 MT were down 1% from the previous week, but unchanged from the prior 4-week average.  The destinations were primarily Mexico (8,100 MT), Japan (3,900 MT), South Korea (3,200 MT), Canada (1,800 MT), and Hong Kong (1,200 MT).


Comments and Performance Indicators

Last week, USDA reported pork export sales totaling 15,900 MT. USDA projects exports in 2017 to be 9.0% above last year's total.