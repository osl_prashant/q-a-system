Farm diesel fell 2 cents per gallon this week to a regional average of $1.96.





Propane firmed a penny per gallon to $1.32 on strength in Indiana.





Our farm diesel/heating oil spread suggests near-term upside risk for diesel prices is growing.