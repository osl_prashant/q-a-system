Recent volatility in the cattle market suggests a defensive position is best for now, says Bryan Doherty, Stewart-Peterson. He recommends puts as a tool that can help producers protect the downside."We're up against some resistance points on the chart," Doherty tells "AgDay" host Clinton Griffiths on the Agribusiness Update segment. "How do I defend this? ‚... We would suggest moving in there and buying puts against April cattle. That's going to give you protection until you move your cattle. Or buying a put, selling a call, collecting that premium. You have to be willing to meet margin call on that scenario, or be willing to be hedged higher, or both. But it's a win-win. The key is to do something."
The alternative to defensive action could be witnessing a "train wreck," he cautions.
"If you try to catch either the falling knife or bouncing ball, it's a hard thing to do," Doherty says.
Even though it's smart to lock in prices before they retreat, Doherty says he's not exactly bearish on the cattle market overall.
"Lower prices in the retail sector, lower fuel prices should help generate good demand, so I'm not a real bear from that perspective," Doherty says. "But I still go back to the idea that I think that cattle, the hog, the poultry industries have all been expanding. They've maybe slowed a little or paused a little bit, but the cold storage report this week showed record inventories. There's expansion. You've got downward momentum in a lot of our commodities, so I think play defensive for now."
Click the play button below to watch the complete interview with Doherty on "AgDay."   

<!--

    function delvePlayerCallback(playerId, eventName, data) {
        var id = "limelight_player_351368";
        if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
            DelvePlayer.registerPlayer(id);
        }

        switch (eventName) {
            case 'onPlayerLoad':
                var ad_url = 'http://oasc14008.247realmedia.com/RealMedia/ads/adstream_sx.ads/agweb.com/multimedia/prerolls/agwebradio/@x30';
                var encoded_ad_url = encodeURIComponent(ad_url);
                var encoded_ad_call = 'url='   encoded_ad_url;
                DelvePlayer.doSetAd('preroll', 'Vast', encoded_ad_call);
                break;
        }
    }

//-->


<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->

Want more video news? Watch it on AgDay.