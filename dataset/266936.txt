Asians stocks fall on US-China trade tensions, tech scrutiny
Asians stocks fall on US-China trade tensions, tech scrutiny

By JOE McDONALDAP Business Writer
The Associated Press

BEIJING




BEIJING (AP) — Asian stocks fell for a second day Tuesday amid jitters about U.S.-Chinese trade tensions and mounting public scrutiny of technology companies.
Markets in China, Japan and South Korea all declined. The dollar declined against the euro and yen but rebounded later against the Japanese currency.
The Shanghai Composite Index lost 0.9 percent to 3,134.88 and Japan's Nikkei 225 shed 0.5 percent to 21,279.77. Hong Kong's Hang Seng lost 0.6 percent to 29,913.68.
Investors worry China's decision to raise tariffs on $3 billion of U.S. goods including pork, apples and steel pipe increases the risk of a broader conflict that might depress global trade.
The amount of goods affected is a small share of China's $150 billion annual imports of U.S. goods. But investors see a bigger fight looming over President Donald Trump's approval of possible higher U.S. duties on $50 billion of Chinese goods in response to complaints Beijing steals or pressures foreign companies to hand over technology.
"The risk of a downward spiral to tit-for-tat trade measures has appreciably increased," said Weiliang Chang of Mizuho Bank in a report.
China's foreign minister tried to reassure companies and investors that Beijing, the No. 1 trading partner for all of its Asian neighbors, wasn't closing its markets.
"Despite the rise of protectionism in the world, China will remain committed to openness (and) will open wider to the rest of the world," said Wang Yi at a news conference.
Elsewhere, Australia's S&P-ASX 200 gained 3 points to 5,762.00. South Korea's Kospi retreated 0.6 percent to 2,430.01 while India's Sensex edged down 13 points to 33,246.02. Benchmarks in Taiwan and Southeast Asia also declined.
The dollar dipped in early trading but rebounded to 105.96 yen from Monday's 105.89 yen. The euro gained to $1.2315 from $1.2302.
Benchmark U.S. crude gained 13 cents to $63.14 per barrel in electronic trading on the New York Mercantile Exchange. The contract plunged $1.93 on Monday to close at $63.01.
Brent crude, used to price international oils, rose 17 cents to $67.81 in London. It tumbled $1.70 to $67.64 on Monday.
On Wall Street, stocks sank as worry about trade tensions compounded by heightened public scrutiny of tech companies in the United States and Europe.
That deflated some previously high-fliers including Amazon, Microsoft and Facebook.
The Dow Jones industrial average fell 1.9 percent to 23,644.19. The Standard & Poor's 500 index gave up 2.2 percent, to 2,581.88. The Nasdaq composite slumped 2.7 percent to 6,870.12.
Amazon fell 5.2 percent following broadsides from Trump on Twitter. Facebook tumbled as a widening privacy scandal weighed on its stock.
The threat of tighter regulation in Europe and the United States prompted investors to pull money out of Netflix, Microsoft and Google parent Alphabet.