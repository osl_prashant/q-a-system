The Vidalia Onion Committee plans to introduce a new logo and discuss its 2018 marketing campaign at its Fresh Summit booth Oct. 20-21.
The committee will also feature cooking demonstrations by Eli Kirshtein, a former Top Chef contestant and a Georgia native, according to a news release.
The group is partnering with restaurants in New Orleans, the site of the expo, to feature dishes with Vidalia onions Oct. 15-21, Vidalia Onion Week.
Participating establishments include Dickie Brennan’s Bourbon House, Dickie Brennan’s Tableau, Galatoires Restaurant and New Orleans Creole Cookery and Briquette.
The new logo will be promoted the Vidalia Onion Committee on the expo floor at booth No. 4116.