BC-Merc Early
BC-Merc Early

The Associated Press

CHICAGO




CHICAGO (AP) — Early trading on the Chicago Mercantile Exchange Fri:
Open  High  Low  Last   Chg.  CATTLE                                  40,000 lbs.; cents per lb.                Apr      118.20 118.20 117.15 117.55   —.25Jun      103.50 103.55 101.80 102.10   —.90Aug      103.37 103.42 101.72 102.07  —1.05Oct      107.82 107.87 106.32 106.60  —1.05Dec      112.22 112.30 110.85 111.15   —.95Feb      113.87 113.90 112.42 112.77   —.88Apr      114.60 114.60 113.57 113.57  —1.00Jun      108.22 108.22 107.50 107.50   —.87Aug      107.00 107.00 106.80 106.80   —.45Est. sales 15,639.  Thu.'s sales 65,805 Thu.'s open int 346,023,  up 1,374     FEEDER CATTLE                        50,000 lbs.; cents per lb.                Apr      137.27 137.70 136.47 136.60   —.62May      138.82 139.02 137.15 137.70   —.72Aug      144.07 144.20 141.55 142.07  —1.63Sep      145.50 145.52 142.97 143.47  —1.53Oct      145.70 146.22 143.67 144.17  —1.53Nov      145.77 145.77 143.50 144.05  —1.27Jan      141.00 141.05 140.50 140.50  —1.30Mar      138.75 138.75 138.75 138.75  —1.25Est. sales 6,926.  Thu.'s sales 15,581  Thu.'s open int 49,977,  up 227        HOGS,LEAN                                     40,000 lbs.; cents per lb.                May       70.15  70.40  69.90  70.27   +.12Jun       78.00  78.50  77.32  77.72   —.35Jul       80.12  80.97  79.90  80.15   —.55Aug       80.02  80.40  79.32  79.75   —.27Oct       67.72  68.22  67.35  67.65   —.57Dec       62.00  62.25  61.55  61.90   —.60Feb       65.42  65.92  65.42  65.72   —.43Apr       69.25  69.45  69.00  69.25   —.35Est. sales 11,948.  Thu.'s sales 42,570 Thu.'s open int 236,703                PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.