Leamington, Ontario-based Ontario Greenhouse Vegetable Growers Association has named Joe Sbrocchi as general manager. 
Sbrocchi will assume the general manager position Sept. 18.
 
He previously worked in management positions for Sobeys and Wal-Mart. Since 2010, he has been vice president of business development and strategy at Mastronardi Produce Ltd., Kingsville, Ontario.
 
“I believe my lifetime in produce and in particular the past eight years in the greenhouse sector have prepared me well for this role,” Sbrocchi said in the release. “I am looking forward to representing Ontario greenhouse growers to the very best of my abilities.”
 
Sbrocchi succeeds Rich Seguin, who was general manager from September 2015 until his retirement in April, according to LinkedIn. Paul Glenney is acting general manager until Sbrocchi joins the group in mid-September.