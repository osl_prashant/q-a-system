BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Thursday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMay                              116.25  Down  .90May      114.70  115.20  113.35  114.25  Down  .65Jul                              118.40  Down  .90Jul      116.95  117.45  115.70  116.25  Down  .90Sep      119.30  119.65  117.90  118.40  Down  .90Dec      122.80  123.10  121.40  121.90  Down  .90Mar      126.35  126.60  125.00  125.45  Down  .90May      128.75  128.95  127.35  127.80  Down  .90Jul      130.85  130.85  129.65  130.05  Down  .80Sep      132.75  132.75  131.70  132.00  Down  .75Dec      135.50  135.50  134.60  134.85  Down  .65Mar      138.20  138.20  137.65  137.65  Down  .60May                              139.50  Down  .60Jul                              141.15  Down  .60Sep                              142.75  Down  .60Dec      145.80  145.80  145.20  145.20  Down  .60Mar                              147.35  Down  .60