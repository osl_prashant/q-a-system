It’s the season of giving, but for many in agriculture, they’ve been giving for months.

People from the Wisconsin dairy industry have stepped up to help victims of the Hurricanes Harvey and Irma that struck Texas and Florida respectively.

The Wisconsin Milk Marketing Board is highlighting some of those who are responsible for bringing comfort food to those who need comforting.

Watch the story on AgDay above.