The Cochin International Airport in India is making the most of its space.

The airport is offering passengers and employees the chance to buy organic vegetables grown on the airport’s grounds.

The airport built rows of solar panels to meet national energy guidelines and are putting the energy to good use.

Watch the full story on AgDay above.