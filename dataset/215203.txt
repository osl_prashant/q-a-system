Photo courtesy A M King.
Henry Avocado Corp., Escondido, Calif., is boosting its East Coast presence with a cold storage distribution center in Charlotte, N.C.
The facility, in renovated leased warehouse space, has 25,000 square feet of forced air rooms, cooler storage space for fresh produce and loading docks. The refrigeration system allows for expansion and energy-saving devices curb energy use, according to a news release.
A M King, Charlotte, built the facility.
“There was a high level of coordination needed to understand and properly implement the client’s custom ripening process,” A M King’s project manager JD Boone said in the release. “
The facility has the capacity to distribute 300,000 ripened avocados each day, with storage to hold another million avocados, according to the release.