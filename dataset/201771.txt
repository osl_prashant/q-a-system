Free flowing water is just as import-ant in the winter as it is in the summer, says Mindy Hubert, South Dakota State University. Feeding hay and salt blocks increases water intake.There are a variety of options to help keep water from freezing. All watering systems will benefit by placement out of the wind. Here are the most common types:
Automatic watering units
While more expensive, starting at $400, automatic units use a floating ball or door easily opened by livestock. There are energy-free models as well as ones with a built-in heating component.
Electrical tank heaters and deicers
Available in various sizes and models (ring, tube, cage, drain-plug, etc.), these waters are the least expensive ($20+ depending on size). A water-safe electrical heating unit is placed inside of water tank to either float or be submerged. Submersible heaters are less likely to be bothered by animals but ensure cords are labeled chew-safe. Most heaters are safe for metal tanks, but check product details for use in plastic or rubber tanks.
Heated buckets
Costing $30 to $65, these waterers have a similar concept as tank heaters, but the electrical unit is built into a plastic bucket.
For the next two devices, it is best to keep the heating unit and attachments out of reach of livestock. A protective housing is recommended or placing the units on the other side of a fence.
Propane stock tank heaters
Larger portable units cost $420 or more and connect to a 100-lb. or larger propane cylinder. It's good for heating large tanks where no electrical source is available. It is thermo-stat regulated and uses a pilot light to ignite the heater.
Water circulators
Instead of heating water, these units circulate the water to keep it open. They don't require electricity or fuel, rather a battery-operated unit circulates water to prevent from freezing. Available for tank sizes 400 gal. to 1,500 gal., it works best for tanks with an automatic fill (such as a float), as the unit sits on tank ledge and needs contact with water.
With several options, there is little reason to break ice and cattle will benefit from consistently open water.

Note: This story appears in the January 2017 issue of Drovers.