“Ich mochte ein Apfel kaufen, bitte,” I told the produce vendor tentatively, hoping my rusty German wouldn’t cause me to say something absurd.
He nodded in understanding. “Do you like more sweet or sour?” he asked in English that was admittedly much better than my German.
It was market day in Eisenach, Germany, a city famous for associations to Martin Luther and J.S. Bach, among other notables, and our tour group was taking a stroll through the town square before heading up to the famous Wartburg Castle. 
As soon as we’d parked on a little side street off the square I’d headed for the shop across the street selling Obst and Gemuse — fruits and vegetables. Painted on the window was a giant smiling figure that looked like a pickle and the words “Gherkin Lust,” which, as near as I can make out, means roughly “desire for cucumbers.” 
I still feel like something must have been lost in translation on that one.
The displays out front were pretty basic — offerings still in the cardboard shipper boxes — but the muscat grapes, Spanish clementines and huge abate fetel pears were beautiful. We debated buying some of the clementines to share with the rest of our group, but decided to go explore the marketplace stalls instead. 
There were several produce vendors there hawking everything from huge heads of brilliant green Romanesco and pointy cabbages to persimmons and cherimoyas grown in Spain. 
Amid the crates of still earthy-looking white or yellow potatoes were boxes of fingerlings and sweet potatoes, too, something I don’t recall seeing at similar markets a decade ago. 
And then there were the apples. 
Common American supermarket varieties like fuji and gala (on the “more sweet” side as indicated by the vendor’s signs) shared the table with elstars, Cox’s orange pippins, gigantic russeted boskoops and others. 
In trying to translate the subsequent conversation with the vendor as we exchanged money I soon forgot what varieties I’d actually purchased, but, as we piled back into our van and headed up the winding road to the Wartburg, I bit into the first apple. It was crisp and sweet and tart, just how a perfect autumn apple should be. 
I thought I’d save the other apple for a snack the next day, but a couple of hours later after hikes up to, around and back down from the castle I was feeling rather peckish, so I devoured it, too.
We have a staggering number of great apple varieties available to us here in the U.S., but it’s always fun to try something new (to me) when away from home. 
I wouldn’t have been able to bring those apples back with me, but I still have the memory of their taste and crunch on that road to and from the castle.
Amelia Freidline is The Packer’s copy chief. E-mail her at afreidline@farmjournal.com.
What's your take? Leave a comment and tell us your opinion.