Chicago Mercantile Exchange lean hogs climbed for the second straight session on Wednesday, buoyed by short-covering and rising cash hog prices, traders and analysts said.
Live cattle and feeder cattle futures each were little-changed in relatively light-volume trading in livestock futures ahead of a U.S. Department of Agriculture monthly grain supply and demand report due on Thursday.
Traders continued to close out positions in October hog futures. The contract settled up 1.075 cents at 61.250 cents per pound, gaining on most-active December hogs, which finished 0.950 cent higher at 62.500 cents.
Hogs in the top cash market of Iowa and southern Minnesota were 70 cents higher to $56.10 per cwt, according to U.S. Department of Agriculture, suggesting pork packers needed hogs despite abundant U.S. supplies.
CME December live cattle settled 0.275 cent lower at 118.525 cents per pound, easing slightly after earlier hitting a more than two-month high of 119.175 cents.
CME November feeder cattle finished 0.475 cent lower at 155.675 cents per pound, weakening after failing to surpass Wednesday's multiweek high of 156.975.
Analysts polled by Reuters expected the USDA on Thursday to raise its estimates of U.S. corn yields and production.
Corn futures dropped to a one-week low ahead of the report and the December contract was hovering just above its Aug. 31 contract low of $3.44-1/4 per bushel.
Cattle futures typically rise when prices for corn feed decline but traders were awaiting the fresh USDA data before taking new long cattle positions, said CHS Hedging analyst Steve Wagner.
"(Traders) are kind of holding their breath before the report," Wagner said.
No cattle sales were reported at the online Feeder Cattle Exchange, where 1,444 animals were offered in the weekly auction, according to its website.