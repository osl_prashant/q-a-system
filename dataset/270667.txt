Whoa! Nevada horse giveaway on hold near Reno; no takers
Whoa! Nevada horse giveaway on hold near Reno; no takers

The Associated Press

RENO, Nev.




RENO, Nev. (AP) — The state of Nevada is reconsidering a proposal to find private owners for nearly 3,000 free-roaming horses under state jurisdiction after no one offered to assume ownership of the mustangs that are at the center of a legal battle.
Opponents of the proposed ownership transfer fear the animals ultimately would end up being sold for slaughter.
State Agriculture Director Jim Barbee says the Board of Agriculture will consider new ideas for the horses after receiving no formal response for proposals. The state had hoped to begin giving away the Virginia Range herd south and east of Reno to a private owner as early as May.
Lawyers for the California-based American Wild Horse Campaign and Cynthia Ashe of Silver Springs filed a lawsuit in state court in Carson City last month seeking an injunction to block what they say would be "a giveaway of a valuable and cherished Nevada asset."
The lawsuit accuses Nevada's Department of Agriculture of breaching a contract that called for the wild horse group to manage 2,500 to 3,000 mustangs in the Virginia Range through 2020 in a humane manner under a joint agreement emphasizing fertility control.
That agreement, initiated in 2013 and updated in 2015, was administered under state law that dictates state ownership of stray or feral horses is not entitled to U.S. protections on neighboring federal land.
Campaign director Suzanne Roy said Thursday she's hopeful the board will resume negotiations.
"We are glad the state's terrible idea of giving away the beloved Virginia Range horses is finally dead," Roy said.
The lack of bidders was a setback for people hoping the state would rid itself of any responsibility for the animals.
"We are still under the existing directive from the Board of Agriculture to manage the horses for public safety only," Barbee said. "The next step is to go back to the board for further direction, and we will continue to manage the Virginia Range feral/stray horses for public safety."
Barbee, who helped spearhead the search for private owners, announced this week he's leaving his post to become the Churchill County manager.
"The only organization that could take something like this on is one that will take them off the range and kill them," said Deniz Bolbol, the American Wild Horse Campaign's programs director.
Bolbol said the state required her group take to out a $1 million insurance policy to cover volunteers who had been working to dart the horses with a fertility control drug. She said she contacted six companies that insure livestock and none was willing to even consider issuing full liability coverage.
___
Eds: This story corrects an earlier version that indicated the lawsuit was filed Monday. It was filed last month.