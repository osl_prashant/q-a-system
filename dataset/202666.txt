The Wisconsin Potato & Vegetable Growers Association elected Eric Schroeder, Schroeder Bros. Farms Inc., Antigo, as its president for 2017.
Schroeder succeeds Mark Finnessy of Plover-based Okray Family Farms.
The association confirmed three other elected board officials:
Josh Mattek, J.W. Mattek & Sons Inc, Deerbrook, vice president;
Gary Wysocki, RPE Inc., Bancroft, re-elected secretary;
Wes Meddaugh, Heartland Farms Inc., Hancock, treasurer.