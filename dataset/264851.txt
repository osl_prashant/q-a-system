BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Wednesday:
Open  High  Low  Settle   Chg.ORANGE JUICE                          15,000 lbs.; cents per lb.                May     137.60 138.55 137.00 137.15   —.25Jun                          137.70   —.15Jul     137.90 138.05 137.55 137.70   —.15Aug                          138.25   —.10Sep     138.20 138.60 138.20 138.25   —.10Nov     139.00 139.45 139.00 139.00   —.05Jan                          139.50   —.05Feb                          140.05   +.05Mar                          140.05   +.05May                          140.50   +.05Jul                          140.60   +.05Sep                          140.70   +.05Nov                          140.80   +.05Jan                          140.90   +.05Feb                          141.00   +.05Mar                          141.00   +.05May                          141.10   +.05Jul                          141.20   +.05Sep                          141.30   +.05Nov                          141.40   +.05Jan                          141.50   +.05Est. sales 438.  Tue.'s sales 457       Tue.'s open int 12,591,  up 57