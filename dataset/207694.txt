Opinions over what qualifies as locally sourced produce vary widely, but the Newark, Del.-based Produce Marketing Association reports that the largest percentage of consumers, almost 40%, believes local produce should be raised within a certain mile radius, based on the Food Marketing Institute’s 2016 Power of Produce survey.However, the next largest group, 28% of shoppers polled, say produce grown in the state qualifies as local.
Despite any controversy or confusion surrounding the definition of local produce, state efforts often lead the way in marketing fresh fruits and vegetables.
 
Florida
Earlier in 2017, the state’s Fresh from Florida program partnered with the Florida Fruit and Vegetable Association with a Specialty Crop Block Grant to promote Florida peaches, said Kinley Tuten, communications coordinator for the Florida Department of Agriculture and Consumer Services.
 
Kentucky
Kentucky Proud launched Buy Local in April, an incentive program to encourage restaurants and other foodservice businesses to purchase locally produced food products.
“This program will help food services meet consumer demand for local foods while providing opportunities for Kentucky family farms and small businesses,” said Sean Southard, director of communications for the Kentucky Department of Agriculture.
On the school side, the state’s annual junior chef competition is scheduled for the state fair, which runs Aug. 17-27.
 
Maryland
Maryland has several consumer programs to support and increase demand for the state’s local growers, said Mark Powell, chief of marketing and agribusiness development.
One of the longest-standing programs is the Buy Local Challenge, which was created in 2006.
Powell said the state promoted the Buy Local Challenge, which encouraged Marylanders to eat local produce and agricultural products daily, from July 22-30.
To promote the challenge, Maryland Gov. Larry Hogan and first lady Yumi Hogan hosted the 10th annual Buy Local Cookout on July 20.
The cookout featured 22 winning dishes made with local items, with all submitted recipes published in an online cookbook.
 
New Jersey
The Jersey Fresh program also has several consumer-focused programs in place to increase local demand.
“We are continuing to build out our social media presence in an effort to create even stronger connections with consumers,” said Jeff Wolfe, public information officer for the New Jersey Department of Agriculture.
It regularly shares recipe videos on the Jersey Fresh Facebook page to highlight Jersey Fresh products in meal ideas. The department also promotes the Jersey “Freshcipes” contest and the #JerseyFreshLove photo contest on Instagram, with weekly winners to keep interaction high.
“We are also highlighting retailers that are promoting our brand with photos of Jersey Fresh displays and scheduling Jersey Fresh tour stops at supermarkets,” Wolfe said.