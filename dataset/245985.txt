Grain mostly higher and livestock higher
Grain mostly higher and livestock higher

The Associated Press



Wheat for March advanced 10.25 cents at 5.0225 a bushel; March corn was up 1 cent at 3.7825 a bushel; March oats was off .25 cent at $2.6475 a bushel; while March soybeans rose 6 cents at $10.6675 a bushel.
Beef and pork were higher the Chicago Mercantile Exchange April live cattle was up 1.05 cents at $1.2322 a pound; March feeder cattle rose 1.48 cents at $1.4515 a pound; while April lean hogs gained 1.23 cents at $.6880 a pound.