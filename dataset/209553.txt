Corn futures prices have remained stubbornly low for the past three months despite heavy rain and cold weather complicating planting and early crop development for many U.S. Midwestern farmers.But the tide may be shifting as the summer is beginning under a warmer and drier regime.
Driven partially by mounting U.S. weather concerns, July corn on Wednesday broke above the $3.60-to-$3.80 per bushel range it had been trading in for three months and new-crop December corn topped $4.00 a bushel for the first time since March 6.
So far this year, the corn market seems particularly unwilling to latch on to weather-related U.S. crop worries. Planting weather was cold and wet in many parts of the Corn Belt and the current crop conditions are not as good as they have been in previous years.
But corn futures stuck within the range anyway as traders are often unstirred over planting hardships so long as the weekly progress numbers align with averages, which they did this spring.
Long-range weather stories are going to be an even tougher sell, and the hesitation is understandable considering the botched forecasts from many weather vendors last summer calling for potential drought.
Instead, plentiful rains arrived in the key months of July and August and U.S. corn ended up breaking the previous yield record by more than 3 bushels per acre in 2016.
Yet some weather analysts have been pounding the drum for some time now about July and August 2017 possibly being hot and dry, just as corn will enter its grain filling stage.
There are various meteorological and statistical reasons offered for the outlook, but the market seems to have tuned that all out for now.
It might take a while longer for traders to buy into any hazardous grain-fill forecasts and for forecasters to regain the traders’ trust, but it seems as if the market is perking up at over weather at least in the near-term.
The summer’s first expansive atmospheric ridge – sometimes known as a “ridge of death” when it persists – will grip the Corn Belt this weekend, ushering in unseasonably hot and dry weather to places that do not need it.
 

However, weather models as of mid-day Wednesday were calling for much more rain across the Midwest into mid-month than the previous day’s models. But if that forecast does not hold, corn futures could be heading even higher in the coming days.
Wet Spring = Yield Loss?
Ignoring the summer weather and putting the meteorology aside, the statistics point to a greater chance of below-trend yields when spring planting is much wetter than usual.
In the top 20 wettest April-to-May periods over the last 50 years, some 13 of those years ended with corn yields more than 1 percent below the long-term trend. Yields were within 1 percent of average in three of the years, and the remaining four years recorded above-trend yields.

April and May of 2017 ranked as the fifth wettest in the last 50 years across the U.S. Corn Belt. Only one of the top 10 wettest springs – the tenth in line – gave way to above-trend corn yields later in the summer.
To be fair, yield performance in two of the 20 wet spring years – 2004 and 1982 – was some of the best to date, though rainfall was notably greater during the 2017 spring.
And some of the worst-yielding years are also mixed in, including 1995, 1991, and 1983, all of which fell inside the top 10 wettest April-to-May periods.
Whether the yields that faltered in those 20 years did so over a hot and dry late summer or something else is an entirely different discussion. But the statistics make it clear that corn has a steeper hill to climb when the springtime is as rainy as it was in 2017, meaning there may be less room than previous years for weather woes later in the summer.
Problem Areas
Areas of concern already exist in various corners of the Corn Belt, and conditions may worsen over the next week given the drier, warmer forecast.
Crop ratings in the Eastern Corn Belt are considerably below both average levels and last year. As of June 4, the amount of corn rated in good or excellent condition in Illinois, Indiana, and Ohio was 59 percent, 46 percent, and 49 percent, respectively.
The overly wet spring in parts of these states has led to a fair amount of replanting, disease and weaker plants. Historically, low ratings at this point in the season do not guarantee below-trend yields to the Eastern belt, but they apply more pressure to have near-perfect weather going forward.
Despite the rainy spring, many areas in the Eastern Corn Belt are now in need of rain again, but the forecast is fairly devoid for the next week. Together, Illinois, Indiana, and Ohio produce one-quarter of the nation’s corn.
In the Northern Plains, drought has been creeping into the picture. According to USDA’s weekly progress report, the amount of topsoil classified as short or very short of moisture grew by 50 percent in the Dakotas during the week ended June 4. Some 54 percent of topsoil in both states now carries this rating.
North Dakota is best known as the top spring wheat-producing state, but corn production has become more prevalent both there and in South Dakota in recent years. The two states together produce about 9 percent of the U.S. corn crop.
If weather models continue to trend wetter into the middle of the month, the market’s concern will likely be sidelined for the time being until the next unfavorable pattern starts to surface in the forecasts.