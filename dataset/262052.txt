Rapping teacher connects with students, helps charity
Rapping teacher connects with students, helps charity

By HOWARD DUKESSouth Bend Tribune
The Associated Press

LAPORTE, Ind.




LAPORTE, Ind. (AP) — It wasn't easy to make a mix tape when 33-year-old Joe Ruiz was a kid.
You had to wait until the song came on the radio and then you had to hit "play" and "record" on your cassette tape deck and hope that the on-air personality didn't talk over the song's introduction — which of course they always managed to do when the song you wanted to record came on.
The generation born in the age of Napster and raised with YouTube, Spotify and iTunes cannot even relate to young Ruiz listening to Chicago rap stations from his parents' Michigan City home with his cassette ready to anthologize the best of 1990s era hip-hop.
Ruiz had compiled a stash of tapes filled with rap songs by the time he graduated from Michigan City High School in 2003. But, he had the foresight to also store the original rap songs he made on a computer.
Fast forward to current day, and it didn't take long for Ruiz's Generation Z students at LaPorte High School to go into the Cloud and retrieve their teacher's musical creations.
Once that happened, the teens convinced him to resume his music-making dreams.
"I'll create an album for my students if they can raise the money," he told himself, "but I'll also try to make that music serve some good."
Enter the PAX center, a local food bank operated by State Street Community, Ruiz's church.
"They are trying to combat food poverty, and so people come in for community meals every week and eat and fellowship," he said. "It's not a place where they make you hear a sermon. It's a place where people from all walks of life can come in and be well-fed."
Ruiz, a married father and high school business teacher, said the church has even bigger plans for the PAX center and the community. It was given some land, and leaders want to create a green space inside the neighborhood for an urban garden that would be open to all.
The Rev. Nate Loucks, pastor of State Street Community Church, said that the church plans to invest about $500,000 into creating a green space where a dairy factory once stood. It'll have multiple gardens as well as a community center that will be used for educational programs.
Ruiz wants to help because he feels strongly about feeding the hungry, But, he originally didn't know how to do that.
"I'm not a handy man," he said, "I'm just a school teacher, and I wondered what I can do to be of some service."
That's where his music comes in.
After one of his students found his SoundCloud files, he came in with a surprise for Ruiz.
"He rapped one of my verses word for word back to me."
Ruiz discovered that his musical background made him more relatable to students.
But his students were already thinking bigger.
"They began to ask me, 'when are you going to put out an album? When are you going to put out a mix tape?'"
They convinced Ruiz to start a crowdfunding campaign to raise the $600 dollars that he needed to rent studio space.
"I didn't expect anybody to donate to it, but they did and so I started pricing studios," he said. He eventually found one in Crown Point, Ind., that charges $50 an hour. He's currently reached half his goal.
If his students' dream for him — and once his own for himself — becomes a reality, he says he'll donate half of the proceeds from his music to the PAX center.
"I just want to be a good influence," he said, "and be known as a good person who uses the gifts that God gave him for something good."
Students who have been taught by Ruiz say that his gifts extend into the classroom, as well as the studio.
"He's one of my favorite teachers from high school," said senior Daniel Sobecki. "He's a really good guy and a really good teacher, and I like what he's doing for his music project and what he's doing for the community."
Tiffany Lin, another student, said that she is impressed by the quality of Ruiz's music, along with his commitment to the community and his students. And, she said that while he usually focuses on rap, he "actually (also) has a good singing voice."
___
Source: South Bend Tribune
___
Information from: South Bend Tribune, http://www.southbendtribune.com


This is an Indiana Exchange story shared by the South Bend Tribune.