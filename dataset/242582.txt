Milk co-op mailing highlights suicide risk for dairy farmers
Milk co-op mailing highlights suicide risk for dairy farmers

By LISA RATHKEAssociated Press
The Associated Press

MONTPELIER, Vt.




MONTPELIER, Vt. (AP) — When some Northeast dairy farmers received their milk checks last month, their dairy cooperative included a list of mental health services and a suicide prevention hotline.
It was an alarming sign of the financial stress small dairy farms are enduring.
The Agri-Mark Inc. dairy cooperative says it felt obligated to get the resources out to its 1,000 farmers in New England and New York following the suicide of a member farmer in January.
Agri-Mark economist Bob Wellington says farmers are facing their fourth year of payments well below their cost of production, due in part to a national and global oversupply of milk.
Minnesota has set up a free 24-hour confidential helpline for farmers, and calls have been pouring into FarmNet, a help line in New York.