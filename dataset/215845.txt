Chicago Mercantile Exchange live cattle futures closed lower on Wednesday after softer wholesale beef demand and this week's expectations for steady-to-lower cash prices erased some of Tuesday's rally, said traders.
Technical selling further weighed on CME's live cattle market, they said.
December live cattle finished down 0.500 cent per pound to 115.650 cents. February ended 0.775 cent lower at 118.375, and below its 100-day moving average of 119.335 cents.
A scant number of animals at Wednesday's Fed Cattle Exchange brought $116 per cwt. No sales were reported there last week.
Slaughter-ready, or cash, cattle bids in the southern U.S. Plains stood at $114 per cwt. There was no response from feedlots that a week ago sold cattle in the U.S. Plains from $115 to $118.
The afternoon's choice wholesale beef price was $1.58 per cwt lower at $202.48 from Tuesday. Select cuts dropped 90 cents to $185.02, the U.S. Department of Agriculture (USDA) said.
Despite fewer cattle for sale than last week, packers are not expected to pay more for them while looking to shutter plants during the Christmas and New Year's holidays, said analysts and traders.
Grocers have plenty of beef, pork and chicken at their disposal and are not expected to aggressively restock meat cases until a few weeks into the new year, they said.
Live cattle future's pullback, and this week's steady to $5 per cwt lower cash feeder cattle prices, dragged down CME feeder cattle contracts.
January feeder cattle closed 1.450 cents per pound lower at 145.650 cents. 
Hog Futures Rise
Short-covering and technical buying pulled up CME lean hog futures in the face of slumping cash and wholesale pork prices, said traders.
December hogs, which will expire on Thursday, finished up 0.200 cent per pound at 63.950 cents. Most actively-traded February closed 0.275 cent higher at 66.800.
Wednesday afternoon's cash hog price in Iowa/Minnesota averaged $57.75 per cwt, 93 cents lower than on Tuesday, the USDA said.
USDA data showed the average wholesale pork price tumbled $4.61 per cwt to $76.51 from Tuesday, pressured by $13.81 lower pork bellies.
Belly prices suffered after three weeks of record-high pork production, and pork belly prices reached a point where "freezer demand has shut down," said independent market analyst Bob Brown.
Week-over-week hog weight slippage suggests farmers are actively sending pigs to market before plants close over the upcoming holidays, said analysts and traders.