Recent rain in California has made harvesting and planting more difficult than usual and may lead to some shortages.
Between Jan. 1 and Jan. 10, Santa Maria received 2.97 inches of rain, Salinas received 3.29 inches, and Watsonville received 7.25 inches, according to Roland Clark, senior consultant for Lake Isabella, Calif.-based Weather Advisory Service.
Ande Manos, marketing manager for Santa Maria, Calif.-based BabÃ© Farms, said excessive rain and mud will limit or prohibit tractors and crews getting into fields for harvest, and the combination of moisture and warmth could cause mildew in baby head lettuces and specialty greens.
As for longer-term effects of the rain, interrupted planting will lead to gaps in availability on all items, Manos said.
"Fortunately, many of our root veggies are grown in sandy ground where there is better drainage and we have the capability to harvest once the rains let up," Manos said. "Recent cold and freezing temperatures have severely impacted baby head lettuces and specialty greens, thus limiting supplies for the next 2-4 weeks."
"The market is active on all items due to short supply," Manos said. "Order fulfillment is being impacted. We're anticipating higher prices due to strong demand and limited production."
Dave Johnson, salesman for Santa Maria, Calif.-based Gold Coast Packing, said the rain would have a minimal effect on Gold Coast's overall operations because it does most of its growing in Yuma, Ariz., at this time of year, but the broccoli and cauliflower it is growing in California could show some evidence of the rain.
The moisture could lead to some quality issues in coming weeks, like pin rot in broccoli and discoloration in cauliflower.
A delayed effect of the rain could be shortages in about three months since plantings have been interrupted, Johnson said.
 Photo by Babe Farms
Henry Dill, sales manager for Salinas, Calif.-based Pacific International Marketing, said the company's plantings in Salinas and Santa Maria would be delayed four to seven days because the ground was saturated, making it impossible to use tractors in the fields. Because of that, Dill said, there could be some supply gaps for leaf lettuce in early April, for head lettuce in mid-April and for broccoli and cauliflower in late April.
Those are the items, along with celery, spinach and other bunched greens, that Pacific International Marketing is in the process of planting. If the rain continues, future plantings could also be affected. The company hasn't had to pump water out of fields yet, Dill said, but it may need to depending on how much more rain falls.
In Santa Maria, the company is harvesting mainly cauliflower and broccoli. The rain is making it difficult, and the company is taking care with how far it ships the product since extra moisture could lead to reduced shelf life.
"We're trying to be as selective as we can," Dill said.
Loree Dowse, foodservice marketing manager for Salinas-based Mann Packing Co. Inc., said the only item the company is currently growing in Salinas is broccolini. She reported some of the same issues others mentioned.
"It's definitely tougher to harvest with the ground being so saturated - think getting equipment through the mud, etc.," Dowse said.
"We have to get through the rest of the rain in order to take a longer-term look at how it will affect planting for spring, as it all depends on timing. Right now it's day-by-day."
 Photo by Babe Farms
Dowse said it was too soon to tell what effect the rain could have on pricing and overall business.
Art Barrientos, vice president of harvesting for Castroville, Calif.-based Ocean Mist Farms, said the rain shouldn't have a significant bearing on the business but colder temperatures might.
"Continued rain and colder temps could have impact on overall volume availability, which could result in higher pricing," Barrientos said.
Barrientos said cold temperatures have prompted some blistering in romaine and mix leaves in Coachella, Calif., but other commodities the company is growing in the state weren't showing quality issues.
Cindy Jewell, vice president of marketing for Watsonville, Calif.-based California Giant Berry Farms, said in an e-mail the company has seen interruptions in harvesting because rain but those have been short, allowing workers to then clean up fields, remove damaged fruit and get back into production within a few days.
FreshPoint, a division of Houston-based distributor Sysco, issued a notice on its website Jan. 10 that rain in the Central Valley region was expected to interrupt harvesting and packing of oranges, mandarins and some lemons.
"Suppliers are doing their best to pack ahead of time," FreshPoint said in its post. "They can't go into the fields when it is raining and also can't pick fruit while it is wet - it is not good for the fruit quality, and it is not safe for the harvesting crews.
"Suppliers are forecasting loading delays and shortages next week," the post continued. "To fill orders, suppliers may sub sizes and/or grades."
 Photo by Babe Farms
Kansas City, Mo.-based distributor C&C Produce gave a similar take on availability of oranges in its Jan. 9 market update.
"Shippers have been picking heavy leading up to this event, but with little to no pick during this rain, expect bin inventory to drop quickly and (make) supplies tight toward the end of the week and beginning of next," C&C said in the update.
Scott Owens, vice president of sales at Delano, Calif.-based Wonderful Citrus, indicated in an e-mail that the company wasn't overly inconvenienced by the weather.
"We welcome the rain," Owens said. "Aside from a brief delay on a few harvesting days, it has had minimal impact on our operations and we continue to enjoy a very high quality, robust crop."
Though the rain may be a bit of a hassle for some in the short term, it is an overall positive for the state, said Jim Bogart, president of the Grower-Shipper Association of Central California.
"It's definitely good news and most welcome," Bogart said.
"The better news is that these storms are coming right after the other, but we're not being deluged with constant rain for weeks on end," Bogart said. "We've got some dry spells between these storms, which is even better."
Christina Barnard, director of marketing at Salinas-based Taylor Farms, also pointed to the big-picture upside of the rain.
"Hopefully the rain fills our aquifers and drives a market so we can all continue to farm and thrive for seasons to come," Barnard said in an e-mail. "The drought conditions we have been faced with over the last couple of years have forced us to make difficult (decisions) as to where we source our core items. The Salinas Valley is dependent on deep well water and that water is finite and has to be replenished via our reservoir systems and rain shed."
Taylor Farms is planting lettuce, romaine, cabbage, broccoli and cauliflower in Salinas currently. Barnard said the positive effects from rain could include better quality and weight.
"Negatives would be reduced yield due to stand issues or germination concerns due to anaerobic conditions as well as other bacterial concerns that cause early decay and pinking," Barnard said.
Supply gaps could result from interrupted plantings, but Taylor Farms doesn't see that as too much of an issue just yet.
"The planting windows are large, and our ability still to plant in the desert for the same windows preclude us from the concerns as of now," Barnard said. "As we continue in to the season that risk increases."