Developing new technology isn’t as simple as coming up with an idea. It takes years of work, access to funding and collaboration.
The Sprint Accelerator has served as an avenue for startup technology companies to connect with other entrepreneurs since 2014. This year, Dairy Farmers of America (DFA) joined Sprint in sponsoring a 90-day agriculture technology incubator. Four companies participated in the 2017 Sprint Accelerator, see below for details.

The Program's Benefits
The program exceeded expectations of DFA, says Kevin Strathman, DFA senior vice president of finance. “What we brought were connections.” Through the process My Dairy Dashboard formed a partnership with DFA to build an enhanced version of myDFA, a website where DFA members can access information on their farms.
The portal enables users to access milk production and weather data from the cooperative, all being displayed through charts and graphs. DFA members who want to upgrade the experience to include on-farm data from their herd or feed management would be able to subscribe themselves and combine it through myDFA.
“They’ll able to pull that data together to give them a richer environment to see trends and make decisions,” says Mitch Norby, CEO of My Dairy Dashboard.
What The Future Holds
In the future Norby hopes to work with other companies to help get more data into producers’ hands, similar to what is being done with DFA to increase the automation of data collection. Having data collection companies like HerdDogg and AgVoice participate in the Sprint Accelerator opened the door to building those types of connections.
“We’ve bounced ideas off each other and there are opportunities as we move forward to work with them,” Norby says.
HerdDogg works as a “Facebook for cows” says Melissa Brandao, CEO of HerdDogg. The tags gather information from cattle that walk within 30' of a Bluetooth “dog bone” tag reader or a smartphone.
Through the program HerdDogg is conducting research with DFA, Sprint and Colorado State University. One goal is raising conception rates by improving heat detection.
“This program in all earnestness has been a game changer for HerdDogg,” Brandao says.
AgVoice made its first venture into dairy, having previously been focused in fruit and vegetable markets. Now, AgVoice has found uses in dairy such as breeding cows, pregnancy checks and herd health inspections without having to write anything down.
“It is interesting that all of us complement each other, we don’t compete with each other,” says Aaron Gobin, co-founder of AgVoice.
The Sprint Accelerator will continue next year with an agriculture focus, along with a food component. Sprint and DFA will return as sponsors with CoBank as a supporting partner.
 
Note: This article appears in the October 2017 magazine issue of Dairy Herd Management.