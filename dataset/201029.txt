Crop calls
Corn: 3 to 4 cents lower
Soybeans: 5 to 6 cents lower
Winter wheat: 1 to 3 cents lower
Spring wheat: 10 to 12 cents lower

Corn and soybean futures faced followthrough from yesterday's late-session decline to do some short-term technical chart damage Traders are reacting to recent widespread rains across the central Corn Belt and more in the forecast through the weekend. Spring wheat futures posted sharp losses overnight, but still remain in the boundaries of the steep uptrend. Increasing harvest activity is triggering profit-taking in the winter wheat markets. This morning's weekly export sales data showed corn, soybean and wheat sales within expectations -- not strong enough to stop the overnight decline in prices.
 
Livestock calls
Cattle: Lower
Hogs: Mixed
Cattle futures are vulnerable to followthrough selling after yesterday's sharp to limit lower performance due to weakness in the cash market. Cash trade has deteriorated as the week progresses, ranging $3 to $5 lower so far compared to last week. The daily trading limit for live cattle is expanded to $4.50 today. Meanwhile, packer demand for hogs is strong thanks to profitable margins and declining weights. The cash market is expected to be $1 to $2 higher today, which should limit spillover pressure from cattle futures.