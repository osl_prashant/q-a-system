Chicago Mercantile Exchange live cattle futures tumbled to the lowest level in eight months, led by fund liquidation and weaker-than-expected prices for cattle ready for processing, traders said.August ended 2.475 cents per pound lower at 110.000 cents, and October closed 2.550 cents lower at 108.050 cents.
Packers in the U.S. Plains on Wednesday paid $115 per cwt for slaughter-ready, or cash, cattle that brought $116 to $119 a week earlier.
That followed a small number of animals that sold at Wednesday morning's Fed Cattle Exchange for $114 to $115.50 per cwt. Animals there last week brought $116.
"There's a lot of beef that has to moved right now. Now we have cooler weather across the cattle belt and feed costs remain cheap, which should promote weight gains," U.S. Commodities President Don Roose said regarding this week's lower cash returns.
This week's cash price setback and Wednesday's mixed wholesale beef values could help grow packer profits, a trader said.
Despite futures' abrupt retreat on Wednesday, the market remains bullishly undervalued compared to cash prices, which may attract bargain hunters on Thursday, he said.
Technical selling and sharply lower live cattle futures dropped CME feeder cattle by their 4.500-cents per pound daily price limit. That limit will increase to 6.750 cents on Thursday. 
August feeders ended limit down at 141.525 cents. 
Higher Hog Market Close
CME lean hogs benefited from their price discounts compared to the exchange's hog index for Aug. 7 at 85.76 cents, traders said.
Some investors bought lean hog futures and simultaneously sold live cattle contracts because of their more bearish fundamental implications.
Nonetheless, both markets are under a cloud of uncertainty amid heightened tensions between the United States and North Korea - a key Chinese ally, said traders and analysts.
Asia, including South Korea, Japan and China, is a major market for U.S. pork and beef, especially with cattle and hog supplies on the rise in the United States, said Roose.
"When you have a big supply, you want to make sure that the demand is solid," he said.
August, which expires on Aug. 14, closed 0.600 cent per pound higher at 84.125 cents. Most actively traded October ended 0.500 cent higher at 68.250 cents.