BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Monday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMay                              118.95  Up   1.25May      116.30  117.10  116.30  117.00  Up   1.25Jul                              121.00  Up   1.20Jul      118.10  119.25  118.05  118.95  Up   1.25Sep      120.15  121.30  120.15  121.00  Up   1.20Dec      123.70  124.80  123.70  124.50  Up   1.20Mar      127.30  128.25  127.20  128.00  Up   1.20May      129.50  130.50  129.50  130.30  Up   1.15Jul      132.05  132.70  132.05  132.50  Up   1.15Sep      134.00  134.45  134.00  134.40  Up   1.10Dec      137.10  137.30  136.95  137.20  Up   1.05Mar      139.85  140.20  139.70  140.00  Up   1.05May      141.60  142.15  141.45  141.85  Up   1.05Jul      143.20  143.85  143.05  143.50  Up   1.05Sep      144.70  145.35  144.55  145.10  Up   1.05Dec      147.10  147.80  146.95  147.55  Up   1.05Mar                              149.70  Up   1.05