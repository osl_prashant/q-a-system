After a consumer reported a fishy taste in a Taylor Farms pasta salad, the company recalled some of the salads because they contained anchovies, which weren’t listed on the label.
Taylor Farms issued the recall Oct. 28, and the notice was posted in the U.S. Department of Agriculture’s Food Safety and Inspection Service website — and not the Food and Drug Administration’s website — because the salad contains chicken.
Anchovies are an allergen and must be on the product label. No one has become ill in connection with the recall, which affected just 732 pounds of the salad.
The Taylor Farms American Style Pasta Salads in the recall are 9.75-ounces and in plastic bowls.
 Identifying information includes:

Use by dates of 11/01/17 and case code of TFFLD 295 L5 EA; and
Use by dates of 11/02/17 and case code of TFFLD 296 L5 JP.

A consumer contacted Taylor Farms on Oct. 26 to complain about the taste of the dressing, which shold have been Bacon Ranch, but instead was Caesar dressing, which contains anchovies.