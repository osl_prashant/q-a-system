Lawrence, Kan.-based ChloroFields has recalled 20 cases of its Asian Mix microgreens that may be contaminated with salmonella.
The items subject to the recall were distributed to retailers in Colorado, Kansas and Missouri and have a sell-by date of March 26, according to a recall notice on the FDA website.
The Universal Product Code number on the product, which comes in 1.5-ounce clamshells, is 853763007096.
A routine examination by the FDA found finished product that contained the bacteria, and ChloroFields has stopped distributing the product as it and the FDA investigate what caused the problem.
No illnesses have been reported in connection with this recall to date, according to the notice.