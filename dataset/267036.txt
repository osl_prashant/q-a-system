US proposes tariffs on $50 billion in Chinese imports
US proposes tariffs on $50 billion in Chinese imports

By PAUL WISEMANAP Economics Writer
The Associated Press

WASHINGTON




WASHINGTON (AP) — The Trump administration on Tuesday escalated its aggressive approach to trade by proposing 25 percent tariffs on $50 billion in Chinese imports to protest Beijing's alleged theft of American technology.
The Office of the U.S. Trade Representative issued a list targeting 1,300 Chinese products, including industrial robots and telecommunications equipment. The suggested tariffs wouldn't take effect right way: A public comment period ends May 11, and a hearing on the tariffs is scheduled for May 15. Companies and consumers will have the opportunity to lobby to have some products taken off the list or have others added.
The move risks heightening trade tensions with China, which on Monday slapped taxes on $3 billion in U.S. products in response to earlier U.S. tariffs on steel and aluminum imports.
On Tuesday night, the Chinese embassy in Washington issued a statement saying it "strongly condemns" the move: "It serves neither China's interest, nor the U.S. interest, even less the interest of the global economy."
China is likely to retaliate against the new tariffs, which target the technology and advanced manufacturing industries that Beijing is nurturing. The sanctions are designed to punish China for using strong-arm tactics in its drive to become a global technology power.
These include pressuring American companies to share technology in exchange for access to the Chinese market, forcing U.S. firms to license their technology in China on unfavorable terms and even hacking into U.S. companies' computers to steal trade secrets.
The administration sought to draw up the list in a way that limits the impact of the tariffs — a tax on imports — on American consumers while hitting Chinese imports that benefit from Beijing's sharp-elbowed tech policies. But some critics that American will end up being hurt.
"If you're hitting $50 billion in trade, you're inevitably going to hurt somebody, and somebody is going to complain," said Rod Hunter, a former economic official at the National Security Council and now a partner at Baker & McKenzie LLP.
Even representatives of the tech industry, which has complained for years that China has pilfered U.S. technology and discriminated against U.S. companies, were critical of the administration's latest action.
"Unilateral tariffs may do more harm than good and do little to address the problems in China's (intellectual property) and tech transfer policies," said John Frisbie, president of the U.S.-China Business Council.
And the Internet Association, which represents such companies such as Google, Facebook and Amazon, expressed concerns, too.
"There's no doubt the U.S. government can and should address China's trade practices," Melika Carroll, the association's senior vice president of global government affairs. "But consumers and American job creators should not be caught in the crossfire. . . . These tariffs will leave our customers worse off, stifle growth and make it harder for the digital economy to succeed."
At the same time, the United States has become increasingly frustrated with China's aggressive efforts to overtake American technological supremacy. And many have argued that Washington needed to respond aggressively.
"The Chinese are bad trading partners because they steal intellectual property," said Derek Scissors, a China specialist at the conservative American Enterprise Institute.
In January, a federal court in Wisconsin convicted a Chinese manufacturer of wind turbines, Sinovel Wind Group, of stealing trade secrets from the American company AMSC and nearly putting it out of business. And in 2014, a Pennsylvania grand jury indicted five officers in the Chinese People's Liberation Army on charges of hacking into the computers of Westinghouse, US Steel and other major American companies to steal information that would benefit their Chinese competitors.
To target China, Trump dusted off a Cold War weapon for trade disputes: Section 301 of the U.S. Trade Act of 1974, which lets the president unilaterally impose tariffs. It was meant for a world in which much of global commerce was not covered by trade agreements. With the arrival in 1995 of the Geneva-based World Trade Organization, Section 301 largely faded from use.
Dean Pinkert, a partner at the law firm of Hughes Hubbard & Reed, found it reassuring that the administration didn't completely bypass the WTO and act only unilaterally: As part of its complaint, the U.S. is bringing a WTO case against Chinese licensing policies that put U.S. companies at a disadvantage.
China has been urging the United States to seek a diplomatic solution and warning that it would retaliate against any trade sanctions. Beijing could counterpunch by targeting American businesses with a big exposure to the Chinese market: Aircraft manufacturer Boeing, for instance, or American soybean farmers who send nearly 60 percent of their exports to China.
In fact, rural America is especially worried about the risk of a trade war. Farmers make tempting targets in trade spats because they depend heavily on foreign sales and because they are spread across congressional districts and are quick to complain to their representatives when government policy threatens their livelihoods.
"The next couple of weeks will be very interesting," says Kristin Duncanson, a soybean, corn and hog farmer in Mapleton, Minnesota.
____
On Twitter follow Wiseman at https://twitter.com/paulwisemanAP