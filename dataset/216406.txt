Taking advantage of corn stalks after harvest and during the start of winter is advantageous for producers and can be beneficial for crop farmers as well. Here's four resources to get every gain from the grain.
 
1. A series of studies by University of Nebraska researchers measured the quality of corn residue from the fall through spring grazing periods. The results showed the effect of grazing on the crop rotation and subsequent corn-residue quality. Read this study and more on residue management in the 2017 Beef Cattle Report from the University of Nebraska.
2. To help landowners determine appropriate stocking rates and grazing intervals on harvested corn fields, the University of Nebraska offers a Corn Stalk Grazing Calculator.
3. Iowa State University publishes an annual Cash Rental Rates for Iowa Survey, which includes rates for residue grazing leases.  For 2017 shows seasonal lease rates for corn-stalk grazing ranging from $8.00 to $12.00 per acre in different regions across Iowa. 
4. ISU also offers an online Corn Stover Pricer spreadsheet decision tool, which allows the user to enter variables such as hay prices, costs of baling and the value of baled stover in determining a fair price for a grazing lease. Click for more information and a link to the Excel decision tool.
 

Read more in this article series: 
Grazing Gold: A Win-Win
Grazing Gold: Effects on Crop Yield
Grazing Gold: Long-Term Effects
Grazing Gold: 4 Corn Residue Resources