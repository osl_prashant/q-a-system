U.S. soybean futures declined on Friday, retreating further from a more than one-year high set in the previous session, with drier weather in Argentina seen aiding harvest progress but crop losses from recent flooding still widely anticipated.Corn futures inched higher on an expected dip in production due to dry weather in Brazil, while wheat prices edged down.
The most-active soybean futures on the Chicago Board of Trade were 0.3 percent lower at $10.24-1/4 a bushel at 1041 GMT. The contract hit a peak of $10.46-1/4 on Thursday, the highest since January 2015.
"Much of the crop is reportedly suffering from lower yields and reduced quality," said Tobin Gorey, director of agricultural strategy at Commonwealth Bank of Australia.
"A dry weather pattern is expected to take place over the next week though, which should at least help to get harvest progress back on track."
A third of Argentina's soy farms remain swamped after early April storms. Analysts estimated crop losses at 5 million tonnes as harvesting starts in areas dry enough to support the 30-tonne combines used to bring in the beans.
The most-active corn futures rose 0.3 percent to $3.92-1/2 a bushel.
Dealers said corn was supported by strong demand for U.S. supplies and a diminished crop outlook in Brazil.
Dry weather in Brazil may reduce the country's winter corn crop by 5 million to 10 million tonnes, resulting in a similar drop in exports from the world's No. 2 supplier of the grain, Bunge CEO Soren Schroder said on Thursday.
"Brazil's second (Safrinha) crop still requires another round of rain or two, so it is not yet in the clear. Meteorologists though aren't optimistic that they will find what they are looking for in the weather models," Gorey said.
The U.S. Department of Agriculture on Thursday said weekly corn export sales topped 2.6 million tonnes, the highest combined old-crop and new-crop total in four years.
Wheat futures declined slightly as the market continued to be weighed by plentiful supplies.
The International Grains Council on Thursday raised its forecast for the 2016/17 global wheat crop, citing improving outlooks in the European Union and Russia.
"Wheat fundamentals have, if anything, become even more bearish over the week as improving conditions push up production estimates," UK merchant Gleadell said in a market update.
Chicago July wheat futures fell 0.2 percent to $4.84-3/4 a bushel while May wheat in Paris dipped 0.5 percent to 152.25 euros a tonne.