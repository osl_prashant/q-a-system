Keasbey, N.J.-based Wakefern Food Corp. has launched a private label for organics —Wholesome Pantry — for all 270 of its ShopRite store locations in New Jersey, New York, Pennsylvania, Connecticut, Delaware and Maryland. 
The new brand, marketed as Wholesome Pantry and Wholesome Pantry Organic, is designed “as an accessible alternative for customers seeking cleaner ingredients and simpler labeling,” according to a news release.
 
The brand includes products found in nearly all departments, including produce, although the release does not specify what items. More than 100 Wholesome Pantry items are available now, with that number expected to triple in the coming months, according to the company. The products also are available online.
 
“We know there’s a growing demand for simple foods that are free from artificial ingredients, and we also know that our customers want these foods to be accessible and convenient,” Chris Lane, Wakefern’s executive vice president, said in the release. “With Wholesome Pantry on our shelves, we can offer products that embrace the free-from and organic food trends and deliver the exceptional quality our customers trust and expect from ShopRite.”
 
Wholesome Pantry’s “free-from” line includes products void of 110 ingredients and contains no artificial additives, flavors or preservatives.
 
Stores will be working with Wakefern’s staff in-store dietitians to host demonstrations and provide product suggestions and recipes featuring Wholesome Pantry items, the company said.