The Texas A&M AgriLife Extension Service and Bexar County Agriculture/Natural Resources Committee will present the 2017 Beef Cattle Short Course in San Antonio during two consecutive Thursdays in January.
The course will be Jan. 12 and Jan. 19 in Room 101 of the Performing Arts building of Palo Alto College, 1400 Villaret Blvd.
Registration for both days will begin at 6 p.m. The programs will start at 6:30 p.m. and conclude by 9 p.m.
"This short course will address many of the health issues in beef cattle as well as important land management issues and techniques," said Sam Womble, AgriLife Extension agent for agriculture and natural resources for Bexar County. "There will be refreshments, door prizes and a vendor area for attendees to see some of the products and services available to them."
Womble said Capital Farm Credit is the main corporate sponsor for the program.
Topics and presenters on Jan. 12 will be:
Beef Cattle Herd Health and the Veterinary Feed Directive ‚àí What Producers Need to Know, Dr. Joe Paschal, AgriLife Extension livestock specialist, Corpus Christi.
Pesticide Laws and Regulations, Vick Alexander, Texas Department of Agriculture inspector, San Antonio.
One Texas Department of Agriculture continuing education unit - 0.5 in laws and regulations and 0.5 in general ‚Äî will be offered.
Topics and presenters on Jan.19 will be:
What's New in Range Weed and Brush Herbicides and How to Calibrate your Spray Equipment, Dr. Bob Lyons, AgriLife Extension range specialist, Uvalde.
Pesticide Laws and Regulations, Alexander.
Two Texas Department of Agriculture continuing education units - 1.5 integrated pest management and 0.5 laws and regulations.
Registration is $15 for both days and attendees must RSVP by Jan. 6 to Angel Torres at the Agrilife Extension office for Bexar County at 210-631-0400.