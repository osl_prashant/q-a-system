This market update is a Pork Network weekly column reporting trends in weaner pig prices and swine facility availability. All information contained in this update is for the week ended Jan. 8, 2016.Looking at hog sales in July 2016 using July 2016 futures, the weaner breakeven was $58.54, down $4.12 for the week. Feed costs were down $0.04 per head. July futures decreased $1.00 compared to last week's July futures used for the crush and historical basis is less favorable compared to last week by $1.03. Breakeven prices are based on closing futures prices on January 8, 2016. The breakeven price is the estimated maximum you could pay for a weaner pig andbreakeven when selling the finished hog.

For the Week Ended


1/8/2016


Weekly Change


Weaner pig breakeven


$58.54


($4.12)


Margin over variable costs


$80.37


($4.12)


Pig purchase month


Jan, 2016





Live hog sale month


July, 2016





Lean hog futures


$76.85


($1.00)


Lean hog basis/cwt


($1.47)


($1.03)


Weighted average sbm futures


$270.82


$2.45


Weighted average sbm basis


$0.14


$0.00


Weighted average corn futures


$3.63


($0.02)


Weighted average corn basis


($0.26)


$0.00


Nursery cost/space/yr


$35.00


$0.00


Finisher cost/space/yr


$40.00


$0.00


Feed costs per head


$70.32


($0.04)


Assumed carcass weight


205


0

Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations don't take into account market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash traded weaner pig volume was above average this week with 50,152 head being reported which is 159 percent of the 52-week average. Cash prices were $57.54 up $7.27 from a week ago. The low to high range was $39.00 to $65.00. Formula priced weaners were up $0.60 this week at $42.38.
Cash traded feeder pig reported volume was below average with 7,900 head reported. Cash feeder pig reported prices were $64.62 up $7.05 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through January 8th.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential decreased $11.38 this week to $1.00 per head.

Casey Westphalen is general manager of NutriQuest Business Solutions, a division of NutriQuest. NutriQuest Business Solutions is a team of leading business and financial experts that bring years of unparalleled experience in the livestock, grain, poultry and financial industries. At NutriQuest our success comes from helping producers realize improved profitability and sustainability through innovation driven by a relentless focus on helping producers succeed. For more information please visit our website at www.nutriquest.com or email casey@nutriquest.com.