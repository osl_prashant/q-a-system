The following commentary does not necessarily reflect the views of AgPro or Farm Journal Media. The opinions expressed below are the author's own.
RSS By: Revolutionizing Conservation Delivery -- Precision Conservation is focused on soil and water conservation in agriculture. It challenges the current thinking about soil and water conservation and provides a different perspective. With my blog, I hope to stimulate an exchange of ideas to uncover new and existing technologies and research in soil and water conservation.
Smack dab in the middle of Iowa, you will find Casey's General Store headquarters. For those of you who have stopped at a Casey's General Store, you realize this is one of the great businesses in the Midwest. Whether you are looking for gas, a slice of pizza, clean restrooms, or just a great cup of coffee, Casey's is your place.

After starting with one location, Casey's General Store now has over 1,900 stores across a 14 state area. So why is Casey's so successful? It is because Casey's has everything a traveler needs, whether it is for a cross country trip or a trip across town. Instead of stopping at BP for gas, McDonalds for coffee, Walgreens for candy, Pizza Hut for pizza, or Hy-Vee for milk, you can get it all at Casey's General Store. One might think Casey's lacks focus, but I maintain Casey's has incredible focus‚... they are focused on travelers.
In the busy world of agriculture, should ag retailers focus more on their clients and offer conservation services? These are my Top 5 reasons why I think ag retailers should lead the way‚...
1. Farmers are under pressure to implement more conservation
Look at almost any publication dealing with agriculture or the environment and you will find evidence that farmers simply need to do more. America's water quality problems stretch from the Chesapeake Bay to the Central Valley and Lake Erie to the Gulf of Mexico. I read a report in my local conservation newsletter that water samples collected between May 2015 and January 2016 froma stream in our countyhad nitrate levels that were 2.5 to 5 times higher than the safe standard set for drinking water.
2. Farmers need more help
There simply is not enough technical assistance to help farmers with their conservation needs. And now when farmers need help the most, conservation agencies are facing a declining workforce and it looks to get worse. I have often repeated, and have never been corrected, that conservation agencies assist only about 5% of farmers each year. Sure 5% of farmers may seem like a lot, but if an agency plans to see or assist each and every farmer, it would take 20 years. Can you imagine, as a retail agronomist, only making a sales call to a customer once every 20 years?


USDA's SCS/NRCS staff has been on the decline over the last 30 years.

3. You are a trusted advisor
A2012 surveyof 5,000 Midwestern corn producers reported their most trusted advisor, when making decisions about agricultural practices and strategies, was their chemical and seed dealer. To paraphrase whatMatt Carstens, Senior Vice President of Land O'Lakes, Inc.has said, there are only so many advisors that are invited to a farmer's inner circle. Usually that inner circle is limited to 5 or 6 advisors. Almost 100% of the time, the farmer's ag retailer is one of those advisors. It only makes sense that the farmer would want that ag retailer to provide conservation services along with agronomic services.


Chemical dealers top the list of ag's most trusted advisors.

4. Farmers want their ag retailer to engage in conservation
In 2015,Dr. J. Arbuckle, Iowa State University Rural Sociologist, analyzedsurveysof 1,128 Iowa farmers. A majority of farmers (60%) surveyed agreed that their fertilizer or ag chemical dealer "should do more to help farmers address nutrient losses into waterways." Only 9% of the farmers reported they did not think their ag retailer should provide conservation services.

5. Client loyalty
The Rockefeller Corporation studied why customers leave one company for a competitor. They found 68% of clients leave a business because they believe the business doesn't care about them. According to Lee Resource Inc., attracting new customers will cost your company 5 times more than keeping an existing customer. I would think these statistics also apply to ag retailers. Recently, a large ag retail executive informed me that their customer turnover was as high as 22% annually. I was surprisedby that number. Obviously a commitment to customer retention should focus on the needs of a customer while at the same time maintaining the competitive advantage.