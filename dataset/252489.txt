The milk production forecast for 2018 is raised from last month on more rapid growth in milk per cow in the first half of the year.
The 2018 imports on a fat and skim-solids basis are reduced on slower sales of a number of processed dairy products.
Exports on fat basis are raised on increased cheese sales and exports on a skim-solids basis are raised on stronger sales of both cheese and whey products.
The supply and use estimates are adjusted to reflect revisions to 2016 and 2017 milk production and 2017 storage data.
Annual product price forecasts for cheese and butter are raised from the previous month as recent prices have increased. However, continued large supplies of nonfat dry milk (NDM) are expected to pressure NDM prices, and the forecast is reduced. No change is made to the annual whey price forecast.
The Class III price is raised on the cheese price projection, while the Class IV price is down, as the lower NDM price more than offsets a higher butter price forecast. The all milk price is forecast at $15.75 to $16.35 per cwt, unchanged at the midpoint.
The full March 2018 World Agricultural Supply and Demand Estimates from USDA can be read here.