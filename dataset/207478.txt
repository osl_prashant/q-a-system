At the FrigoAmazonas slaughter house inside the world's largest rainforest, the owner doesn't mince his words - much of the cattle processed here comes from illegally deforested land."It's impossible to buy cows from land that isn't deforested," Felipe Oliveira told the Thomson Reuters Foundation in his tatty office at the abattoir in Brazil's Amazonas State.
"Everyone here deforests... if they don't, it's impossible for a family to live," the slaughterhouse boss said, sitting beneath exposed electrical wires hanging from the ceiling.
If the Amazon forest, described as the lungs of the planet for its role sucking climate-changing carbon dioxide out of the atmosphere, is to be saved, then addressing the impact of livestock is the most pressing priority, environmentalists say.
Deforestation Rises
The clearance of land for cattle pasture is responsible for 80 percent of the forest destruction in the Amazon, according to data from Yale University.
The rate of deforestation in the Amazon increased by 29 percent last year, according to government figures.
Larger ranchers, truckers and traders have set up elaborate schemes to "launder" cattle raised on illegally deforested land on the legitimate market, said analysts and officials.
Brazilian authorities also link illegal deforestation to other crimes in the Amazon, such as forced labor on farms or "grilagem" - land grabs - by ranchers who fraudulently register properties occupied by small farmers to produce cattle.
Powerful rural businessmen often bribe government officials or land registry agents known as "cartorios" to obtain property title deeds, according to Brazilian prosecutors. These illegally registered plots are often hotbeds of deforestation.
As the world's largest exporter of beef and chicken, the importance of Brazil's struggle to contain illegal deforestation for livestock extends far beyond rural Amazonian settlements.
Bovine Battles
In Boca do Acre, a poor municipality of wooden stilted homes with 28,000 residents, the cattle industry accounts for more than 70 percent of the economy, officials said.
It's a reality replicated in towns throughout the Amazon rainforest, often making it difficult for officials to take a hard line enforcing laws on deforestation.
"It's impossible for this city to live without cattle," Josimar Fidelquino, a local government official responsible for environmental monitoring told the Thomson Reuters Foundation. The municipality is the largest cattle producer in Amazonas, Brazil's biggest state, officials said.
The FrigoAmazonas slaughterhouse employs more than 100 staff. In muddy rubber boots, ripped jeans and cowboy hats, they arrive at the plant in open-air trucks to process 1,200 head of cattle per month. The slaughterhouse produces meat exclusively for the local market in Amazonas State.
"We know illegal cows are being killed by people in town," said Fidelquino, adding that authorities are working on schemes to allow more cattle to be raised on smaller chunks of land.
Typically, farmers simply cut down trees on a plot and allow cattle to graze freely, eating plants growing on the scrub land. The same number of cows could be produced on far smaller plots with supplies of hay or other feed, officials said.
The president of the Boca do Acre Ranchers Union, Paulo Castillo, refused interview requests about slaughterhouses and their adherence to deforestation or land registration rules.
Under Brazilian law, Amazon land owners must maintain 80 percent of the forest cover on their properties.
Some local farmers and slaughterhouse owners say that expectation is unrealistic; environmentalists say it's essential.
Regardless, laws on forest preservation and land governance are not well enforced in remote jungle regions, analysts said.
"The Amazon is like the 'Wild West' was in America," said Jose Puppim de Oliveira, a professor at Brazil's Getulio Vargas Foundation university, who studies land politics.
"Forest land is considered unproductive, as are the people living there," he told the Thomson Reuters Foundation.
‘Constant Problem’
While small slaughterhouses like FrigoAmazonas producing for the local market are known to flout environmental laws, they account for a comparatively small part of Amazon deforestation, said University of Wisconsin professor Holly Gibbs.
About 90 percent of Brazil's cattle is processed in slaughterhouses that can export nationally or internationally, Gibbs said, and they are a driver of deforestation.
Large cattle companies say they are taking the problem seriously by improving their monitoring of suppliers and working with the government and environmentalists to try to keep cattle produced on illegally cleared land out of their supply chains.
Brazil-based JBS, the world's largest meatpacker, has pledged not to buy cattle from deforested land in the Amazon, said an official at the company which is in the midst of a separate political corruption scandal.
"Building a supply chain free from deforestation is a constant challenge for the entire industry," a JBS official told the Thomson Reuters Foundation in an email.
The company has been successful in stopping its direct suppliers from deforesting land, the official said, but monitoring indirect suppliers remains a "major challenge".
Following The Cows
Brazilian cows are supposed to have two tracking numbers so their origins can be traced by authorities.
The first is a health registration document known as a GTA - which shows cattle have received the right vaccinations along with information about the animal's transportation history.
The second is certification from Brazil's environment ministry known as a CAR showing that they were raised on legally registered land adhering to forest protection rules.
GTA documents could be the "holy grail for ending deforestation", said Gibbs, by acting as a passport for cattle.
They could allow cattle to be tracked from birth until death and as they move between farms to trace legality. But the information is not publicly available.
Companies agree. JBS wants authorities to launch a new "Green GTA" to allow for better tracking of the origins of cattle. Government officials in Boca do Acre said they had no information about possible improvements to the GTA system.
Without clear tracking data, ranchers can move cattle from illegal land to legally registered properties just before selling them to slaughterhouses, Gibbs said.
The border post in Amazonas State, where cattle documents are supposed to be inspected before bovines travel into neighboring Rondonia State, has been shuttered for months, said a cook who works at locked, dusty building. The lack of checks makes it harder for companies to monitor their supply chains.
Back at FriggoAmazonas, slaughterhouse boss Oliveira says more paperwork under an expanded GTA system would make life even more difficult for small farmers.
"Most of the small farmers here don't have titles or land registration," Oliveira said. "How could they get these papers? Some can't even sign their names."
Travel support for this reporting was provided by the Society of Environmental Journalists (SEJ).