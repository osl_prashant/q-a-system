Maine's scallop season nearing end with grounds still open
Maine's scallop season nearing end with grounds still open

The Associated Press

LUBEC, Maine




LUBEC, Maine (AP) — Maine's scallop fishing season is nearing its final weeks with many scallop fishing grounds still open for harvesting.
The Maine scallop season begins in December and runs until April 10 this year. The state's regulators use targeted closures to protect scallops from succumbing to overharvesting.
The Maine Department of Marine Resources says no emergency actions or closures are currently pending. The department says it's continuing to monitor the health of scallops through surveys and industry updates.
Maine fishermen harvested nearly 800,000 pounds of scallops last year, the most since 1997. The value of Maine scallops for fishermen and the price to consumers have also been high in recent years.