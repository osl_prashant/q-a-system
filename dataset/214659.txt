Avocados from Mexico is sponsoring a Nov. 14 event by Politico that will bring together agriculture media and public policy leaders in Washington, D.C.
Politico reporters Helena Bottemiller Evich and Jenny Hopkinson will discuss the future of food in a global economy with a panel of agriculture, trade and supply chain experts, according to a news release.
The event will be at the Mexican Cultural Institute, at 2829 16th Street NW, Washington, D.C., at 5:30 p.m. on Nov. 14, and registration is available online.
Agriculture leaders at the event, according to the release, include:

Kathleen Merrigan, executive director of sustainability at George Washington University and former U.S. Department of Agriculture deputy secretary;
Francisco Javier Trujillo, the general director of plant health for the Mexican National Service of Agrifood Health, Safety and Quality;
Darci Vetter, diplomat in residence at the University of Nebraska-Lincoln and former U.S. chief agricultural negotiator; and 
Karil Kochenderfer, principal at the global supply chain consultancy Linkages.