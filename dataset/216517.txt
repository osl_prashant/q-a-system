U.S. Customs and Border Protection in Pharr, Texas, busted a shipment of avocados hiding more than 1,000 pounds of marijuana.
The seizure took place Dec. 16, when border officers at the Pharr International Bridge cargo facility stopped a 2005 Freightliner tractor trailer hauling a commercial shipment of avocados, according to a border protection release..
Officers found and seized 440 packages of marijuana totaling 1,055 pounds. 
That amount is worth about $210,000. The case remains under investigation by Homeland Security Investigations special agents.