St. Louis retailer Schnuck Markets has partnered with Instacart to offer its customers the option to shop online and have their groceries delivered.
Groceries will come from 29 Schnucks locations in the St. Louis metro area, with deliveries beginning Feb. 16, said company spokesman Paul Simon.
Shoppers can order their groceries through the website schnucksdelivers.com.
For a single order of $35 or more, two-hour delivery costs $5.99 and one-hour delivery costs $9.99.
A single order between $10 and $34.99 can be delivered within two hours for $7.99.
Shoppers also have the option to pay $149 per year or $14.99 per month for unlimited two-hour delivery on orders of $35 or more. The monthly payment option does not require a yearlong commitment.