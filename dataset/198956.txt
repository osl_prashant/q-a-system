In Episode 8 of Silage Talk, the Dairy Herd Management team gets helpful advice on corn silage from Dr. Bob Charley, Forage Products Manager with Lallemand Animal Nutrition.

Dr. Charley addresses the following questions:

What do producers need to keep in mind as they start to bring their silage in?
We have heard a lot about mycotoxins: might they be an issue in the 2016 corn silage crop?
How long before new silage can be fed?

Sponsored by Lallemand Animal Nutrition.