Market conditions for California vegetables bounced higher in late February in response to cold weather in desert growing regions.
Markets may stay elevated through part of March and perhaps longer as desert growing districts in California and Arizona seasonally wind down and production moves north to Huron and then the Salinas regions in California.
Doug Classen, vice president of sales for The Nunes Co. Inc., Salinas, said growers will be finishing many crops 7-14 days ahead of schedule in desert growing regions.
“We were originally set to start items ahead of schedule in Salinas and Huron, but cooler weather pushed things back a little bit and as a result, there is some disruption in the market on the supply side,” he said.
Classen said the industry may be looking at reduced vegetable volume for several weeks, with suppliers looking at tighter volumes from perhaps early March to mid-April.
 
Rising f.o.b.s
California Imperial Valley cauliflower prices rose from $6-8.95 per carton on Feb. 17 to $25-30 per carton on Feb. 28, according to the U.S. Department of Agriculture.
Imperial Valley cartons of 24 heads romaine lettuce sold as low as $5-6 from early January to mid-February. But as of Feb. 28, the USDA reported romaine lettuce was trading at $14-19 per carton.
Broccoli prices from the region rose from $6-8.25 per carton on Feb. 17 to $11-14 per carton at the end of the month.
Growing conditions for desert vegetables were ideal until late February, pushing crops well ahead of normal harvest schedule, Andy Piña, sales manager for Salinas, Calif.-based broker Produce Center. Then cold weather and some rain created a supply crunch and higher prices.
“This winter was a perfect storm for product growth,” he said. “We haven’t had any weather issues and produce is two weeks ahead at least.”
For most of the winter, the desert had an oversupply of product and poor markets.
The spike in vegetable prices comes at a time when growers in the desert are seeing their volume drop. Salinas and Santa Maria cauliflower and broccoli production are just starting up in a light way.
Cold weather caused California desert lettuce to show blister and peel issues.
Pina said vegetable production in the desert typically goes through the end of March, but growers are expected to finish earlier than that.
While Central Coast regions were also running ahead of schedule, Pina said cold and rain has slowed progress.
Lettuce production will transition to the Huron district by the middle to the end of March, with volume from the Salinas Valley expected the first couple weeks of April.
 
Monterey County outlook
Recent cold weather and rain in the Salinas Valley has slowed crop progress, said Richard Smith, California extension vegetable crop farm advisor in Monterey County. However, through the end of February, growers had not experienced any planting delays because of rain.
“In spite of cold weather (harvest) could be ahead of schedule,” he said. Lettuce harvest in the Salinas Valley typically starts the end of March or early April.
Russ Widerburg, sales manager for Boskovich Farms Inc., Oxnard, Calif., said weather in the Central Coast region is expected to be cooler than normal in the next couple of weeks, with off and on rain possible. That should slow down crop progress and keep markets up during the next few weeks, he said.