List includes three key USDA nominees 



President Trump announced more than three dozen White House appointments late Friday night. He tapped Oklahoma Rep. Jim Bridenstine to run NASA and Pennsylvania Rep. Tom Marino to become the next drug czar. Marino previously turned down the position citing a family illness. These nominations will mean special elections in Oklahoma and Pennsylvania. Both seats are solidly conservative. 
The president also announced long-awaited nominees for key USDA posts. Trump announced Gregory Ibach as Undersecretary for Marketing and Regulatory Programs (MRP), Bill Northey as Undersecretary for Farm Production and Conservation (FPAC), and Stephen Vaden as USDA’s General Counsel.
The Undersecretary for MRP oversees three USDA agencies: the Animal and Plant Health Inspection Service; the Agricultural Marketing Service; and the Grain Inspection, Packers, and Stockyards Administration. 
The Undersecretary for FPAC oversees three USDA agencies: the Farm Service Agency, Natural Resources Conservation Service, and the Risk Management Agency.
Perdue comments. “I look forward to the confirmations of Greg Ibach, Bill Northey, and Stephen Vaden, and urge the Senate to take up their nominations as quickly as possible,” USDA Secretary Sonny Perdue said. “This is especially important given the challenges USDA will face in helping Texans and Louisianans recover from the devastation of Hurricane Harvey.”
Regarding the individual selections, Perdue issued the following statements:
On Greg Ibach:
“Greg Ibach will bring the experience and vision necessary to serve as a first rate Under Secretary for MRP at USDA. His exemplary tenure as Nebraska’s Director of Agriculture places him squarely in tune with the needs of American agriculture, particularly the cattle industry. His proven track record of leadership will make him a great asset to USDA’s customers, the hard working, taxpaying people of U.S. agriculture.”
On Bill Northey:
“Bill Northey will continue his honorable record of public service in leading FPAC. Having served the people of Iowa for the last ten years as their Secretary of Agriculture, and as a fourth generation corn and soybean farmer, Bill has a unique understanding of issues facing farmers across the nation. He will be an invaluable member of the team.”
On Stephen Vaden:
“Stephen Vaden has a keen legal mind, as we have already experienced through his work since he joined USDA as part of the beachhead team on day one. He has a firm grasp of the legal issues facing American agriculture, and very importantly, understands the breadth and complexity of the regulatory burdens placed on our producers. Our farmers, ranchers, foresters, and producers will be well served by his counsel.”