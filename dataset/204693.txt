The first Florida citrus estimate of the season offers a gloomy forecast: a 14 percent drop in orange production from the previous season and an 11 percent decrease in grapefruits.
The U.S. Department of Agriculture reported last week that Florida was only expected to produce 70 million boxes of oranges during the 2016-2017 growing season. Last season, Florida produced 81.5 million boxes.
The estimate puts Florida's grapefruit forecast at 9.6 million boxes.
Florida Commissioner of Agriculture Adam Putnam says in a statement that the forecast is disheartening and further proof of the difficult times facing Florida's citrus industry.
Florida's citrus industry has been decimated in the past decade by the citrus greening disease.