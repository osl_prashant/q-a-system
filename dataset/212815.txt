Bringing together two large tree fruit marketers, Chelan Fresh of Chelan, Wash., and Borton Fruit of Yakima, Wash., plan to merge. 
Effective Sept. 1, the combined marketing company will retain the Chelan Fresh name and operate from Chelan and Yakima, according to a news release. Terms of the merger were not disclosed.
 
The merger combines five generations of growers, according to the release.
 
“Customers want a one-stop-buying experience and shoppers want a wide selection of high-quality apples, pears and cherries all year long,” Tom Riggan, CEO of Chelan Fresh, said in the release. “With the increase in volume and varieties, we can support any size program twelve months out of the year.”
 
Bill Borton, president of Borton Fruit, said in the release that the combined company will offer buyers valued fruit varieties from multiple growing regions in the Northwest.
 
“The next generation of our family is determined to be positioned as industry leaders in the marketplace for the future in the fruit business,” John Borton, CEO of Borton Fruit, said in the release. “That means servicing retail accounts from a large volume base, from modern orchards, state-of-the art packing lines, and with exciting new varieties year-round.”
 
 
Combining strengths
 
The addition of Borton Fruit’s volume brings an estimated 47% expansion to Chelan Fresh’s sales and marketing offerings, according to the release, including organic offerings, early-season cherries and new proprietary apple varieties.
 
Borton Fruit joins the Chelan Fresh supplier base that includes Gebbers Farms, Crane & Crane and Chelan Fruit Cooperative in the marketing group.
 
In total, Borton Fruit and Chelan Fresh have recently invested more than $235 million dollars in packing facilities and technologies, new storage facilities, and production efficiencies, according to the release. The firms have more than 13,000 acres planted in young, high-density orchards with modern trellis systems, including industry-leading volume of Honeycrisp apples, according to the release.
 
The apple, pears and cherries from the combined company will be mostly marketed in the Trout label. The additional volume creates the label’s largest brand expansion since the Trout label was first marketed in 1923. Chelan Fresh also markets the Cascade Crest Organic and the Chelan Fresh labels.