BC-SD--South Dakota News Digest 6 pm, SD
BC-SD--South Dakota News Digest 6 pm, SD

The Associated Press



Here's a look at AP's general news coverage in South Dakota. Questions about coverage plans go to News Editor Doug Glass at 612-332-2727 or dglass@ap.org. Jeff Baenen is on the desk, to be followed by Gretchen Ehlke at 6 a.m.
This information is not for publication or broadcast, and these plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
AROUND THE STATE:
CROP REPORT
DES MOINES, Iowa — Corn has been dethroned as the king of crops as farmers report they intend to plant more soybeans than corn for the first time in 35 years. The U.S. Department of Agriculture says in its annual prospective planting report released Thursday that farmers intend to plant 89 million acres (36 million hectares) in soybeans and 88 million acres (35.6 million hectares) in corn. By David Pitt. SENT: 590 words, photos.
With:
PROSPECTIVE PLANTINGS
GIRL DROWNS-SIOUX FALLS
SIOUX FALLS, S.D. — A 2-year-old review of Falls Park that Sioux Falls officials used to defend safety protocol at the park where a 5-year-old girl drowned on March 18 was actually a training exercise. Documents released to the Argus Leader newspaper show that what the city cited as an "independent review" and "safety audit" of the park was a training exercise for employees of the South Dakota Public Assurance Alliance. City Emergency Manager Regan Smith says he erred in calling the review an audit. SENT: 340 words.
PIERRE-WATERTOWN FLIGHTS
PIERRE, S.D. — The suspension of flying by Great Lakes Airlines is causing scheduling headaches for Pierre-area residents. Partner Aerodynamics Inc. is still operating Great Lakes Jet Express flights between Denver and Pierre and Watertown, but the booking is a little more complicated. SENT: 270 words.
IN BRIEF:
DOWNTOWN SHOOTING-RAPID CITY, STATE CUSTODY DEATH-LAWSUIT, JAIL ESCAPE-NEBRASKA PANHANDLE, ESCAPEE CAUGHT
SPORTS:
TWINS-ORIOLES
BALTIMORE — Center fielder Adam Jones plays in his 11th consecutive opening day for the Baltimore Orioles, and right-handed starter Jake Odorizzi makes his debut with the Minnesota Twins when the teams launch their 2018 season at Camden Yards. By David Ginsburg. UPCOMING: 650 words, photos. Game starts at 2:05 p.m. CT.
STARS-WILD
ST. PAUL, Minn. — The Minnesota Wild try to move closer to a spot in the Western Conference playoffs, while the Dallas Stars are barely hanging on in the race. The two Central Division rivals meet for the first of two games in three days. By Dave Campbell.
MINNESOTA-MOTZKO
MINNEAPOLIS — Bob Motzko is introduced as the new men's hockey coach at Minnesota, where he's been tasked with restoring the program's place among the sport's elite programs. By Dave Campbell. UPCOMING: 500 words, photos.
IN BRIEF:
VIKINGS-SHERELS
___
If you have stories of regional or statewide interest, please email them to apsiouxfalls@ap.org. If you have photos of regional or statewide interest, please send them to the AP in New York via FTP or email (statephotos@ap.org). Be sure to call to follow up on emailed photos: 800-845-8450, ext. 1900. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.