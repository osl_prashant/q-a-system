LITTLE ROCK, Ark. (AP) — Arkansas lawmakers have approved banning a herbicide that farmers say has drifted onto crops where it wasn't applied and caused damage, but the prohibition still faces a legal challenge from a maker of the weed killer.
The Legislative Council on Friday without discussion approved the Plant Board's plan to ban dicamba from April 16 through Oct. 31. A subcommittee earlier this week recommended the council, the Legislature's main governing body when lawmakers aren't in session, approve the proposal.
The board proposed the ban after receiving nearly 1,000 complaints last year about dicamba. Monsanto, a maker of dicamba, has asked a state judge to prevent the restriction from taking effect.
Arkansas is one of several states where farmers have complained about dicamba drifting.
 
Copyright 2018 The Associated Press