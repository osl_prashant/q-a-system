California cherries will peak sooner and in bigger volumes than last year, spurring retailers to offer Mother’s Day promotions.“We are projecting 7 (million) to 7.5 million cartons in California, a significantly better crop than last year,” Brianna Shales, communications manager for Wenatchee, Wash.-based Stemilt Growers LLC, said March 29.
“Our early California cherries are hanging a very good crop, the best in the last four years,” said Keith Wilson, owner of Dinuba, Calif.-based King Fresh Produce LLC. He expects peak volumes to start close to May 2.
The Flavor Tree Fruit Co., an early shipper based in Hanford, Calif., anticipates company in the first weeks of the deal.
“We’re always an early house, but this year a lot of the other players will have more volume early on,” said Maurice Cameron, sales manager.
Much of Flavor Tree’s fruit will move April 21 to May 15, he said. Shipments will continue through June 5 with later varieties such as bing, regina, skeena and rainier. The company expects to move about 700,000 cartons.
“I’ve been getting customers geared up to change their thoughts on when to market California cherries,” Mike Jameson, director of sales and marketing for Stockton, Calif.-based Morada Produce, said April 6.
“The crop is pushed forward about 10 days. The industry hasn’t seen the kind of volume come off in the first part of May that we’ll see this year.”
“We haven’t been able to get chains to promote this early before, just because the timing and volume are so different,” said Mark Cotton, sales manager for Stockton, Calif.-based Grower Direct Marketing LLC.
 
Promotable May volumes
Stemilt Growers anticipated harvests would start in the third week of April in the Patterson and Arvin-Bakersfield districts. Royal varieties — such as Royal Hazel and Royal Lynn — will be the first to come off, with corals soon to follow, Shales said.
“The crop for both the royals and corals looks awesome,” she said.
“The first three weeks of May are the promotable volumes for shipping out of California, which makes Mother’s Day a huge potential for retailers to pull new dollars through their cherry category.”
The volume is credited to an early bloom and the increasing prominence of low-chill varieties such as corals. Statewide about 2 million boxes of corals are expected. Other estimates include 1.5 million for brooks and about 1 million for tulares.
But to the north, rain damage during bloom in Stockton, Linden, Lodi, Gilroy and Hollister is expected to make for a lighter bing cherry crop than the region sometimes sees.
“The late district in the north had two different sets,” Shales said.
“One bloomed really well and set average or better, but the last half was not so great. You had about three quarters of a crop there.”
Even so, Morada Produce expects sufficient volumes for Memorial Day promotions as the state’s districts and varieties begin to overlap.
“Corals, brooks and tulares will overlap with the bings,” Jameson said. “You’ll have mostly corals, some late tulares and bings for the Memorial Day push, but the main variety will be bings.”
“There will be enough cherries to promote both holidays,” he said. “But the focus of the industry is on trying to get domestic and international retailers to push their promotional periods up.”
California’s quick start raises the prospect of some gap in production before Washington cherries are in buyers’ hands.
“Based on the temperatures we’re seeing, we think we’ll have a bit of a gap in late May or early June,” Shales said. “Washington won’t have the May start that it did last year. Early June is more normal.”
 

Export challenges

The recent strength of the U.S. dollar in currency markets could create problems for exporters of California cherries, but they seem up to the challenge.
“A few years ago we didn’t really see a significant impact to our business when the dollar got strong,” Jameson said.
“But I’m anticipating the dollar will be stronger this year in most of the markets we deal with. That will create some issues to work around, but I don’t think it will limit the amount of business we do in those markets.”
Morada Produce does about 40% of its cherry business internationally, making it one of the state’s larger exporters. The cherries go worldwide but have an Asia focus in South Korea, Japan, China and Taiwan.
“A strong dollar is a hindrance, but we still feel the Asian market will be viable this year,” Wilson said.
“Canada has been a challenge, but we’re seeing the exchange rate improve there,” Shales said.
“But we expect the early timing of the crop to really help us in Asian markets because we’re going to be able to line up for certain holidays there that we can occasionally be in short supply for.”