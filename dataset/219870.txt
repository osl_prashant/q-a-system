Tracey Erickson, South Dakota State University Extension
As the American farm continues to grow and evolve, the need for extra labor within these operations is also exhibiting the same parallel trend. Unfortunately, many of us as agriculture producers did not sign on to become human resource managers, when we entered into the occupation of agricultural production. In addition, agricultural producers, as a group, tend to be very independent, do it ourselves types, and the transition into being people managers instead of maybe “livestock or crop managers” is not the easiest. Thus, if we are going to be successful in the future and our success is dependent upon hired labor, we need that managers must develop the skills to manage people.
Recruitment & Hiring
Once an operation has determined they need to hire someone either part-time or full time, they will need to put together a plan for recruiting and hiring outstanding staff. No simple or even complex recipe guarantees hiring success. Luck is not the answer. Agriculture cannot meet its goals by hiring at the bottom of the barrel. We need to attract good people who are willing to work for others. The employer who seems lucky in always finding high quality people rarely is in fact lucky. Instead, such producers are depending on carefully made plans and a reputation as an excellent employer that has been patiently built. The answer to hiring a good employee lies in developing a plan for filling positions.
Checklist
The following eight steps suggest a checklist that managers can utilize in helping them to succeed in hiring a quality employee.

Determine the labor and management needs of the farm business that the new employee is expected to address.
Develop a current job description based on the needs.
Build a pool of applicants. (Ask yourself if you would work for you. Become known as the place to work, instead of the place not to go to work for. This will help build your pool of applicants.)
Review applications and select those to be interviewed.
Interview
Check references / background checks
Make a selection
Hire

Values
Another factor that needs to be considered is personal values. Ask yourself what is important to you as a person. Are you courageous, respectful, a collaborator, driven by performance, have a positive attitude, are you driven by success, honorable, trustworthy, these are among many, many values that you may have as a person. Take a true look at your values and try to hire people with similar values as you. You will be more apt to connect with that person or group as they work in your operation towards your overall purpose, vision and mission of your operation.
The Bottom Line
Farm managers face a major challenge in finding and keeping quality employees. The recipe for farm success is complex. Animals, equipment, financing, land and buildings matter a great deal. People also matter. To a great extent, managers reach their goals through people. Getting things done through people requires competent employees. Mediocrity in filling positions can make a huge difference over time. To have competent employees, people who have the potential of being competent need to be hired. The question is: Do I maximize my chances of hiring the “right” people or do I leave my success to chance? Each farm employer answers this question directly or indirectly and then lives with the answer. Lastly, remember to “hire for attitude and train for aptitude”.

* Adapted from “Recruiting and Hiring Outstanding Staff”; Bernard L. Erven, Department of Ag, Environmental & Development Economics, Ohio State University.