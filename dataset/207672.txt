The Produce Marketing Association reports local food sales totaled about $12 billion in 2014, up from just $5 billion in 2008, citing information from the U.S. Department of Agriculture.“Retailers and foodservice companies continue to participate in the locally grown initiative and support all of our small family farmers,” said Brian Knott, president of Louisville, Ky.-based Grow Farms.
The company markets for growers spread across Kentucky and Tennessee who produce tomatoes, bell peppers, cucumbers, eggplant, squash, zucchini and green beans, he said.
USDA predicts the market value for locally produced food could hit $20 billion by 2019.
Even large chain stores are participating in the local trend.
PMA reports Wal-Mart Stores Inc. is the largest purchaser of locally sourced and sold produce in America. It cites data from “The big business behind the local food,” a 2015 article by Laurie Tarkan published in Fortune magazine, which states Wal-Mart sold $749.6 million of locally grown produce annually at that time.
 
Making connections
One of the biggest hurdles to increasing promotion for local produce is connecting small and mid-size producers with retailers, restaurants and distributors.
The Maryland Department of Agriculture works hard to meet that challenge. Its Buyer-Grower Expo has been successful in bridging that gap, said Mark Powell, chief of marketing and agribusiness development.
It is the state’s key effort to bring together Maryland growers with buyers from chain stores, chefs and distributors.
“We had about 300 attendees this past year. MDA started the Buyer-Grower 14 years ago with just a handful of farmers and produce buyers. It has really taken off from there,” Powell said.
Connections are also needed between growers and schools or other institutions.
Florida sponsored a booth at the School Nutrition Association’s Annual National Conference expo to highlight Florida agriculture producers and promoted the Fresh From Florida brand.
“Florida Classic Growers, Pero Family Farms and R.C. Hatton showcased their products and networked with school nutrition professionals from across the country as a part of their co-sponsorship of the booth and participation in the Florida Farm to School initiative,” said Kinley Tuten, communications coordinator for the Florida Department of Agriculture and Consumer Services.
To help smaller growing operations connect with schools, Florida also developed the Good Agricultural Practices/Good Handling Practices Certification Program to assist Florida producers with the cost of a food safety audit with the intent to sell produce to Florida schools.
Knott said those audits can sometimes be a hurdle but that “state programs have been a help with our growers on a one-on-one basis in assisting with GAP audits.”