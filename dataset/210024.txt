The American Farm Bureau Federation is adding to the push for farm labor reform.
In an Oct. 10 news release and video headlined “Farmers Squashed by Labor Shortage,” Farm Bureau refers to fresh produce rotting in the fields as many growers lack access to an adequate and stable workforce. 
From the release:

“Farmers and ranchers across the country are calling for long-overdue reform to the current guest worker visa program that would create flexibility and provide stability in the agricultural workforce.
 
As Washington state farmers Burr and Rosella Mosby explain in a new video from the American Farm Bureau Federation, the farm workforce is dwindling, and even with higher wages, it’s hard to find enough workers for harvest. The Mosbys were forced to abandon a field of zucchini squash on their farm just south of Seattle when their workforce came up 25% short this season.
 
“I think we need more options,” Rosella Mosby said in talking about the guest worker visa program. She said there is an availability of foreign workers ready to come work in agriculture, but the current system does not give farmers or workers the flexibility needed to fill farm jobs. 
 
“It’s supposed to be that you work hard and produce something, and you’re getting paid at the end of the day,” Burr Mosby said as he watched the 20-acre field being plowed under. “Here we produced something. We grew it, and I don’t have enough hands to pick it, put it in boxes, and sell it to the grocery store. That’s what hurts.” The Mosbys estimate that their workforce shortage this year will cost them $100,000 in lost profits and productivity.
TK: It is one thing to refer to the concept of “unidentified produce rotting in the field.” It is another thing to show a real farm family that has had to plow under acres of zucchini because of an inadequate supply of legal labor. If anything will reach the hearts and minds of lawmakers and consumers and convince them for the need of farm labor reform, it is the depiction of this grim reality.