BC-Wheat KX
BC-Wheat KX

The Associated Press

CHICAGO




CHICAGO (AP) — Winter Wheat futures on the Chicago Board of Trade Wed:
OpenHighLowSettleChg.WHEAT5,000 bu minimum; cents per bushelMay480½491¼479¼488¾+8¼Jul499½510¼498¼507¾+8¼Sep519¼530¼518¾527¼+7¾Dec546556½545554¼+7¾Mar564½572¾564½572¼+8May576583576582+7½Jul581587¼581586¼+5¾Sep594½+6½Dec609¼+6Mar614¾+6May614¾+6Jul608+6Est. sales 52,250.  Tue.'s sales 56,225Tue.'s open int 269,351