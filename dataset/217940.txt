Gary Davis, former president and owner of Russ Davis Wholesale in Wadena, Minn., has died.
Davis died Jan. 3 after a long battle with cancer. He was 81.
Last year marked the 60th anniversary of Davis’ start in the industry, selling fresh produce with his father, according to a news release from the company.
Davis was a respected expert in operations, sales procurement, products and executive leadership, according to the release.
His father founded Russ Davis Wholesale in 1955, with only two trucks, and Gary Davis started working there two years later, according to his obituary. He worked there until his death.
“His passion for the business and life in general was inspiring,” according to the release. “People who knew him will recall his spirit displayed in a short temper, but an even quicker smile. He was a generous leader and made his employees owners of the company when he was ready to retire.”
Friends and family are invited to gather from 5 p.m. to 8 p.m. Jan. 12 at the Schuller Family Funeral Home’s Johnson-Schuller Chapel in Wadena, resuming at the Wadena Alliance Church at 9:30 a.m. until the start of the memorial service at 11 a.m.
Survivors include his wife, Benda, Ottertail, Minn.; and three daughters.