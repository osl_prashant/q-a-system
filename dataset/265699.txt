Gallagher, Price lead Canadiens to 4-2 win over Red Wings
Gallagher, Price lead Canadiens to 4-2 win over Red Wings

The Associated Press

MONTREAL




MONTREAL (AP) — Even with the Montreal Canadiens eliminated from postseason contention, Brendan Gallagher keeps competing hard every single night.
Gallagher scored twice, including his 30th goal of the season, and Carey Price snapped a seven-game losing streak as Montreal defeated the skidding Detroit Red Wings 4-2 on Monday.
"Those 30 goals are well deserved," Canadiens coach Claude Julien said about Gallagher. "It's an example of what hard work and perseverance and commitment and dedication is all about. He never complains about anything. He goes about and does his job. That's what you expect from your leaders."
With the two goals, Gallagher pushed his total to 49 points this season. His previous career high was 47.
"It's nice," the feisty winger said about reaching the 30-goal plateau. "You want to contribute and you want to score. It's also nice to get it out of the way and stop talking about myself and get back to finishing hard and finishing the right way."
Paul Byron and Alex Galchenyuk also scored for Montreal in a matchup of Original Six franchises who will miss the playoffs. Price made 26 saves for his first win since Feb. 4. He had been 0-5-2 since then.
"We have five games remaining and we want to finish the right way game after game and really push each other to carry this over into next year," Gallagher said.
Gustav Nyquist and Tyler Bertuzzi scored for Detroit, and Jared Coreau stopped 27 shots in his fifth game of the season. He is 0-4-0.
Both the Canadiens and Red Wings will take part in the draft lottery this spring. Montreal is 26th in the overall NHL standings, three points ahead of 27th-place Detroit.
The Red Wings have won just once in their last 14 games (1-12-1).
"We've been real good through the stretch," Detroit coach Jeff Blashill said. "We've played really good hockey. We've deserved better results. No chance is that good enough tonight. We have to be way better.
"Our attention to detail, not good enough. Our intensity, not good enough. Winning puck battles, doing all the little things it takes, just too many guys not going."
Gallagher scored his first of the game and team-leading 29th of the season at 4:27 of the second period to give the Canadiens a 2-1 lead. The forward threw the puck on net from the corner of the ice and it bounced off Red Wings defenseman Danny DeKeyser's skate and in.
The Edmonton native, who has scored in three consecutive games, got his 30th at 9:32 when he tipped a shot by Mike Reilly on net. The puck went off Coreau's skate and landed in the crease before the goalie accidentally knocked it in himself.
"Those are the ones I'm used to scoring," said Gallagher, who also had two blocked shots. "It's always good when the guys don't really know that you scored the goals. But any way you can score is obviously nice."
The 5-foot-9 forward came within inches of his first career hat trick. With 4:26 remaining, Jeff Petry's shot from the blue line trickled past Coreau. Gallagher came close to knocking it in, but Galchenyuk got his stick on it first to make it 4-2.
"I don't think there's a guy who works harder in this locker room," Petry said about Gallagher. "He's one of the guys who's consistently going to those tough areas and he's getting rewarded for it."
Bertuzzi got one back for Detroit at 13:41 of the second when he squeezed a loose puck between Price's pads.
The teams traded goals in the first period.
Nyquist got the first at 4:41 with a hard one-timer before Byron responded, on the power play, with a backhand off the post and in at 18:11 for his 19th.
NOTES: Detroit recalled 21-year-old defenseman Joe Hicketts from Grand Rapids of the American Hockey League. ... Several Canadian Olympians were honored in a pregame ceremony, including medalists Kim Boutin, Charles Hamelin, Justine Dufour-Lapointe and Mikael Kingsbury.
UP NEXT
Red Wings: Host the Pittsburgh Penguins on Tuesday night.
Canadiens: Play at Pittsburgh on Saturday night.
___
More NHL hockey: https://apnews.com/tag/NHLhockey