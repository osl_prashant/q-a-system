Bryan Howe of the Allen Lund Co.'s Charleston, S.C. office was promoted to business development specialist.
Howe has been with Allen Lund since June, 2015 as a transportation broker-in-training, according to a news release.
Don Roberts, Howe's manager at the Charleston office, praised his ability to adapt and learn quickly.
"He has the skill set and backing to successfully sell all the products ALC has to offer," said Roberts in the release.
Howe graduated cum laude from the College of Charleston in May of 2015. He earned a bachelor's degree in business administration, with a minor in economics.
"I look forward to helping grow an already phenomenal company and bringing another persistent work ethic to our sales team," Howe said in the release.