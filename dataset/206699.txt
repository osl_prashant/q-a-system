Soybeans are in the green, and part of the reason is because of the U.S. Department of Agriculture’s crop conditions report released Monday.
The quality of the crop decreased two percentage points from the week prior and prices continued to rally. Much of the crop was planted in poor conditions, then didn’t receive rain for weeks after it was put in the ground. Andy Shissler of S&W Trading thinks it will be difficult to have another year of record-breaking soybean yields.
“We just don’t have the population,” Shissler told AgDay host Clinton Griffiths. “To supersede yields like the last couple of years, I say good luck on that.”
He believes that by the time harvest rolls around, there will be close to a 200 million bushel carryout.
“Everybody is really bearish beans, and I think people are committed to be short early and low in the market,” said Shissler. “It takes a long time for that cycle to develop.”
Hear Shissler’s full comments on the soybean crop on AgDay above.