Thanks to what is called an "Internet mapping glitch," Joyce Taylor and the folks who rent her Kansas farm have been treated like criminals for a decade. They've been accused of being identity thieves, spammers and fraudsters. The FBI has visited. So have federal marshals, IRS collectors and ambulances searching for suicidal veterans.The trouble began in 2002 when a digital mapping company started providing "IP intelligence." To say that's not an exact science is an understatement. When the company knows only that an IP address exists "somewhere" in the U.S., they point the user to the center of the country. Yep, the Taylor place happens to sit near the center of the U.S., thus a swarm of folks looking for scammers and thieves have been directed to visit the farm 82-year-old Joyce Taylor was raised.
Earls backtracks
Earls, the Canadian restaurant chain with 66 locations, says it was a mistake to switch to a U.S. supplier for "natural" beef.
Last week the company said it would buy all its meat produced without hormones or antibiotics from Creekstone Farms, a Kansas packing company. Canadians - consumers and producers - cried foul.
"Good luck keeping your restaurants viable in this province," one Alberta resident posted on Earls' Facebook page. Other social media comments were similar, prompting Earls to do an about-face. "Earls' fans, we're listening to you. We made a mistake, and we're sorry. It was wrong to move away from Canadian Beef, and we want to make it right. Earls will get Canadian Beef back on the menu."
Friday funnies
Fun stories we've stumbled upon while composing this week's GTN.
The Top 10 home repairs seldom experienced inside the city limits
KFC Launches Edible Chicken Flavor Nail Polish
Colorado Sheriff's Office Chronicles Epic Saga of Llama on the Loose
Man stuffs Stolen Snake in Pants
Dog meat farms close as South Korea's younger generation says no thanks
North Dakota farmer pays homage to Prince with in-field symbol
Meat of the Matter: Activism Made Easy
According to the 'modern' approach to advocacy, it's no longer necessary to convince people of the morality of your cause. Just make changing one's lifestyle so easy it takes no effort at all. Drovers' columnist Dan Murphy has the scoop.