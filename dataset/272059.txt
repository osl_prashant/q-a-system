Grains higher and livestock lower
Grains higher and livestock lower

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were higher Wednesday in early trading on the Chicago Board of Trade.
Wheat for May delivery advanced 20.60 cents at $4.7860 a bushel; May corn was up 6.60 cents at $3.8560 a bushel; May oats rose 6.60 cents at $2.2560 a bushel while May soybeans was gained 11 cents at $10.33 a bushel.
Beef and pork were lower on the Chicago Mercantile Exchange.
April live cattle fell .13 cent at $1.2185 a pound; Apr feeder cattle lost .22 cent at $1.3940 a pound; April lean hogs was off .80 cent at .6730 a pound.