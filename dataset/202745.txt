The U.S. Department of Agriculture is seeking nominations to fill positions on the National Watermelon Promotion Board.
 
The board is seeking nominees for two producer member seats and two handler member seats in District One in Florida, according to a news release.
 
The USDA is also seeking nominees for one public member seat and eight importer member seats. Members serve three-year terms, according to the release.
 
The deadline for submitting nominations is June 30, according to the the release.
 
Nomination forms are available from the board's director of operations and industry affairs Rebekah Dossett or industry affairs manager Andrea Smith at 407-657-0261.