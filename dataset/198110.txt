Crop calls
Corn: 1 to 2 cents lower
Soybeans: 1 to 2 cents higher
Winter wheat: 2 to 4 cents lower
Spring wheat: Mixed
The crop markets generally faced profit-taking overnight, but came off session lows and soybeans ended the session firmer. Traders are concerned about hot temps stressing the crops this weekend, but there are still chances of showers across the Northern Plains beginning on Monday and across the Corn Belt by next Wednesday. Price action could be choppy this morning as traders move to the sidelines ahead of the 11:00 a.m. CT USDA reports. Traders expect just minor adjustments to old- and new-crop balance sheets as well as to the winter wheat crop. Soybeans should also be supported by USDA's announcement an unknown buyer has purchased 201,000 MT of old-crop U.S. soybeans.
 
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Only light cash cattle trade has been reported so far across the Plains at levels near week-ago. Due to overall strength in the beef market this week that has lifted packer margins, remaining feedlots are holding out for higher prices. As traders wait on active cash trade to begin, futures are expected to be mixed as traders even positions. Hog futures are also expected to see a mixed tone this morning as traders even positions ahead of the weekend. But pressure on futures should be limited by an expected firmer tone in the cash hog market.