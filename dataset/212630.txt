Georgia green bean and sweet corn buyers can expect to see changing volumes throughout the season, say growers-shippers around the state.In Bainbridge, Ga., Jon Browder, sales manager for Belle Glade, Fla.-based Pioneer Growers, reports that the mid-March freeze affected beans and corn to some degree.
Pioneer grows more than 2,000 acres of sweet corn and 300 acres of beans in southwest Georgia.
“Beans started early, around May 5,” Browder said. “We expect them to be light at the start, then become normal through mid-July.”
Green bean harvest typically starts around May 10, he said.
He said sweet corn should start May 15. As with beans, he expects corn to start light, but then increase to heavy volume from June through July Fourth.

Prices

Browder said that bean prices should be very good, in the $18-20 range.
Early May prices for bushel cartons of machine-picked green beans from Central and South Florida were $24.35-24.85, according to the U.S. Department of Agriculture.
Last year, late May f.o.b.s for green beans from south Georgia in bushel cartons/crates were $10.35-12.85, according to the USDA.
Browder expected corn to be in the $10-12 range.
Wire-bound crates of sweet corn from Florida were shipping for $11.25 as of May 5.
A year ago, late May f.o.b.s for Georgia sweet corn were $9.95-10.95, according to the USDA.
Calvert Cullen, president of Northampton Growers, Moultrie, Ga., is reporting a “bumper crop on everything.”
Growing conditions have been ideal, he said, and he expects there to be ample supply of beans to last well into June when the shift to North Carolina takes place.
Northampton Growers has maintained production this year, with a slight cutback on hot peppers as it manages variety to demand.
Cullen said that he would like to see some improvement in the cabbage market, which has been low the past few years.
Joey Johnson, president and co-owner of J&S Produce Inc., Mount Vernon, Ga., expects to start beans on May 12.
Johnson expects good quality on light volumes at the start of the deal.
J&S Produce should run beans for 6 weeks, with a 3-4 day “skip” because of rain that delayed planting. The company should return to good volume through to the end of the season around July 1.
“The markets have been tough for beans the past few years,” Johnson said.
“We’ve actually cut back a little this year due to lower bean prices. Still, we expect good quality.”
J&S represents growers with 300 acres in spring green beans and another 300 acres planted for a fall harvest.