BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Thursday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeApr        11.68   11.81   11.63   11.75  Up   .01May                                11.95  Up   .04Jun        11.82   11.99   11.81   11.95  Up   .04Jul                                12.19  Up   .05Sep        12.07   12.23   12.05   12.19  Up   .05Dec                                13.38  Up   .03Feb        13.27   13.42   13.27   13.38  Up   .03Apr        13.49   13.60   13.45   13.55  Up   .02Jun        13.56   13.69   13.56   13.65  Up   .01Sep        13.80   13.90   13.80   13.87  Up   .01Dec                                14.47  Down .01Feb        14.47   14.51   14.42   14.47  Down .01Apr        14.47   14.53   14.47   14.50  Down .04Jun        14.53   14.58   14.53   14.55  Down .06Sep        14.76   14.76   14.71   14.74  Down .07Dec                                15.09  Down .07Feb        15.09   15.09   15.09   15.09  Down .07