The latest round of talks to modernize the North American Free Trade Agreement doesn’t hold immediate promise for a breakthrough, industry leaders say.
Through Jan. 25, the Montreal round of NAFTA talks — set to conclude Jan. 29 — have been fairly quiet, said Hunt Shipman, principal and director for Cornerstone Government Affairs, Washington, D.C.
Shipman lobbies for the Produce Marketing Association, among other clients.
Administration officials had been hoping for a conclusion of the talks by March, but that seems unlikely.
“From what I’ve seen, dairy seems to be the agricultural topic of the moment,” he said. On the other hand, there has not been much discussion on the U.S. proposal on seasonal trade protection for perishable crops, Shipman said. 
Agriculture leaders have repeatedly expressed concern that President Trump would pull out of NAFTA, and Trump himself has recently left the door open to that decision.
“My personal sense of it is that there are those that recognize the benefits of NAFTA and the benefits to NAFTA to agriculture as a whole,” he said. 
But perhaps the greatest leverage the Trump administration has to win a better deal, Shipman said, is to keep the threat to pull out of NAFTA alive.
“I don’t think it helps our negotiators to get the best deal that they can across all the areas of discussion if (the threat to pull out) is suddenly taken off the table,” Shipman said.
Dennis Nuxoll, vice president of federal government affairs with Irvine, Calif.-based Western Growers, said statements by U.S. Trade Representative Robert Lighthizer and Commerce Secretary Wilbur Ross raise concerns that the U.S. may want to split NAFTA into separate agreements with Canada and Mexico.
“Those are very aggressive statements,” he said. He noted that Tom Nassif, CEO of Western Growers, has said its members have generally benefited from NAFTA and would be very concerned if NAFTA would be broken into two pieces.
“It’s not entirely clear how you would knit those things back together,” Nuxoll said,