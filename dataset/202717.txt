The comment period for the U.S. Department of Agriculture's Jan. 18 proposed rule on the organic promotion order has been extended from March 20 to April 19. 
 
More than 2,300 comments have been received through Feb. 27 at the www.regulations.gov website on the organic research and promotion order.
 
The proposed program would be financed by an assessment on domestic producers, handlers and importers of organic products and would be administered by a board of industry members, according to a summary of the proposal.
 
The proposed initial assessment rate would be one tenth of 1% of net organic sales for producers and handlers, and one tenth of 1% of the transaction value of organic products imported into the U.S. for importers.