In the AgPro Podcast, voices from the industry asked: How is technology helping to identify greater efficiencies and better connectivity in ag retail?
Ben Craker
Duluth, Ga.
Product Manager, Global Fuse, AGCO Corporation
A: “No one company is capable of all things to all farmers when it comes to technology. Farmers should have a choice on equipment and technologies, but being able to connect all of those different technologies and have them talk to each other is key. We partnered with AgGateway to come together and use the same standards to bring data together. Fuse Technologies, AGCO’s approach to precision ag, has a strategy to get technology pieces to work together. Connectivity for farmers and their data is a must in today’s world of precision agriculture.”