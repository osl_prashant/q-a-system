Cincinnati-based Total Quality Logistics recently launched TQL Carrier Dashboard, a web portal and free mobile app for carriers and dispatchers.
Carrier Dashboard, designed by TQL developers, replaces the company’s Carrier Web Portal and has extra features to simplify how carriers search for loads, according to a news release.
The dashboard allows carriers and dispatchers to manage their freight directly through the web or mobile app. They can also submit quotes on loads, send invoices and upload other documents, and provide location updates, according to the release.
“Logistics is a fast-paced industry that is constantly evolving to keep up with economic demands,” Kerry Byrne, president, said in the release.  
“TQL continually looks for ways to create more efficiency for carriers which is key to our ability to provide the best service possible to customers,” he said.
Logins for the TQL Carrier Dashboard can be requested at www.tql.com.