On the news of the Chinese tariff on soybeans, markets have fallen significantly, but should you lock-in feed? Analysts agree probably some of it, but maybe not all of it.
Soybeans are 80 cents lower now than they were on Wednesday. While some analysts say to wait and watch beans go lower before locking in feed, Matt Roberts an analyst with Fremantle says to take a tip from the Chinese and use this sale as a chance to secure a good price.
“If I'm on the buy side of my dairy from a feeder use this as a pricing opportunity,” he says adding that if the tariffs are put into place prices could go lower.  
Andy Shissler of S&W trading says there’s no reason to panic and lock-in everything just yet.
“We have a whole month for these two to yell at each other before this gets worked out or put into place,” he says. “I would not get too emotional about it.”
According to Shissler, the best opportunity for a price dive would be in May when the deadline to implement the tariffs inches closer.
“The dollar lower could come as these things get a little sideways in May if this continues,” he says.
Still, Roberts says that while dairy farmers struggle with margins, President Trump’s latest trade debacle is providing a nice clearance sale on soybeans.
“We're seeing China using this to purchase,” he says. “There's no reason you shouldn't do the same.”