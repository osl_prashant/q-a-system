Phosphorus (P) is an important macronutrient for field corn. Adequate amounts of the nutrient are associated with improved corn root development, increased stalk strength, resistance to disease, kernel (seed) production and earlier crop maturity. In some scenarios, the right amount of P can boost corn yields by up to 40 bu. per acre.
At the same time, P is under increased scrutiny for its impact on the environment, particularly with regard to water quality. That’s certainly been the case for farmers in the past decade or so in areas such as the western Lake Erie Watershed and Chesapeake Bay Watershed.
Tom Bruulsema, phosphorus program director at the International Plant Nutrition Institute, and Ken Sechler, senior agronomist with Southern States Cooperative, addressed the topic of phosphorus management and stewardship during a recent Crop Fertility Quarterly webinar sponsored by AgPro. During the hour-long session, they provided participants with five practical steps to improve their phosphorus management skills.
Soil test on a consistent basis. You can’t manage phosphorus if you don’t know the level of P in the field.
“It sounds simple, but too many retailers and farmers don’t pull good soil samples and that throws off everything,” Sechler says. “Pulling a good sample at a depth of 4” to 6” is important.”
Sechler encourages farmers to soil test their fields at least every other year. Over time the information can help farmers and their retailers establish a good nutrient baseline.
Bruulsema adds that phosphorus needs are best determined by evaluating soil tests against the phosphorus balance in the field. “If your soil test is too high, a deficit is appropriate,” Bruulsema says. “If it’s too low, you need to be putting on more than the crop removes.”
Sechler tells farmers to also evaluate the laboratory they use to test soil samples.
 “We tend to work with private laboratories rather than state university labs, which aren’t always as well-funded,” he says.
Calculate the farm and individual phosphorus balances. “Look at what you’re removing with the crop compared to the sources of P you’re applying,” Bruulsema recommends. “Consider both fertilizer and manure. Biosolids and compost need to be accounted for as well.”
Placement is very important. Whenever possible, place phosphorus in the soil not on the soil. Along with that, Sechler says the closer farmers can get P placed to corn seedlings, the more effective the uptake. Ultimately, less P will be needed which can reduce nutrient costs. “In our geography, most corn planters now run a popup fertilizer system. We frequently run with 3- to 5-gal of pop-up fertilizers near the corn,” he says. “We still have some 2” x 2” placements that are popular as well.”
For crops like perennials and forages, application timing is important to consider.  “It’s better to apply P after the second cut of forage for instance,” Bruulsema says. ”It’s a lot less risky for runoff then than in early spring or late fall.”
Tissue tests are valuable tools in-season.  Sechler says tissue tests give you a snapshot of how a crop is using nutrients at a given time, and you can often adjust your fertility program based on what you learn and potentially improve yield outcome.
Farmers benefit from working with a professional. Bruulsema says retailers who have a “certified level” of knowledge have the experience and expertise to provide sound technical advice that’s tied to the 4R’s of sustainability— the right fertilizer source, at the right rate, the right time, and in the right place. “They benefit everyone in the agricultural community,” he notes.
Sechler adds that precision ag technologies are helping farmers become better stewards.
“We use GPS soil sampling maps and overlay that with in-season NDVI and yield maps.” Sechler explains. “And our equipment is set up to variable rate apply up to three products in a single pass to accurately apply the nutrients needed.”




Phosphorus considerations
© IPNI