As fruit and vegetable grower-shippers build more sustainable business models, they are turning increasingly toward the sun for help.Solar panels have been popping up across the produce landscape for several years.
“One hundred fifty acres of our largest ranch are dedicated to a solar farm, operated by Pacific Gas & Electric, which supplies all the electricity to the entire ranch,” said Samantha Cabaluna, vice president of brand marketing & communications with Salinas, Calif.-based Tanimura & Antle Inc.
“Conserving energy and improving energy efficiency not only decreases our environmental footprint, it’s smart farming, too.”
San Diego-based Organics Unlimited installed its first solar panels in late June, said Mayra Velazquez de Leon, president.
“This is new to us but part of our sustainability program,” she said.
Mike Williams, project developer with Pickett Solar, a division of Fresno, Calif.-based Don Pickett & Associates Inc., said solar power is a practical solution to the soaring costs of energy.
“With the cost of energy constantly on the rise, going solar gives you the ability to control rising energy costs and creates positive PR from choosing to go green at your operation,” he said.
Solar arrays can be customized to suit operational needs, Williams said.
“It all depends on energy consumption and when you are using your power,” he said.
“Here in California’s Central Valley, we see cold storage (facilities) and packinghouses using a majority of their power during peak times in the summer months, which is the most expensive time to use power.”
Solar panels are useful to any crop or type of operation, Williams said.
“The crop only plays a factor as one operation may need more solar than the other due to energy consumption and time of use,” he said.
The number of solar panels needed to power, say, a 200-acre operation could vary, Williams said.
“No two operations are exactly alike,” he said. “We would need to dive into their historical energy consumption to determine a system size. Operation A with 200 acres may need a megawatt of solar while Operation B with 200 acres may only need 400 kilowatts of solar.”
One megawatt generally involves 3,000 plus panels, depending on a panel’s wattage, Williams said.
Costs are variable, depending on needs, such as a ground-mount tracker, fixed system, rooftop, solar structure or whether utility upgrades are required, Williams said.
Installation times can vary, as well, he said.
“Depending on the size of the system and whether utility upgrades are required would determine the timeline to install a solar array,” he said. “The construction portion is on average three months for a megawatt-sized project and much shorter for smaller systems.”
There are variables involved in calculating any savings solar power would provide, Williams said.
“For example, in PG&E territory, we see an average of $250,000 annual savings per megawatt,” he said.
Payback averages 2 1/2 to four years, Williams said.
Climate is a factor in the efficacy of solar power, but “having a utility that is solar-friendly is the major factor,” Williams said.
“For example, New Jersey has been a leader in solar from early on,” he said. “For farming operations, Net Energy Metering Aggregation changed the game, allowing you to install one solar array that can offset multiple meters if they are on contiguous land.”
Demand for solar installations is particularly brisk in the area of cold storage and packinghouses, Williams said.
“For larger operations, the typical size is 1 megawatt,” he said.
How many produce operations will go solar in some way this year?
“Here in the Central Valley, we are seeing a steady demand for new systems and return clients looking to grow their existing systems,” Williams said.