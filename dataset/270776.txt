Potato marketers long have said their product has been saddled with a bad health rap, but new research seems to be changing that scenario, according to a recent report from the Alliance for Potato Research and Education. 
The group reported that two studies published in the American Journal of Clinical Nutrition failed to find an association between potatoes and cardiovascular disease. The studies said white potatoes can be part of a healthy diet.
 
The first study, which looked at data from nearly 70,000 Swedish men and women, concluded that there were no significant trends between the consumption of boiled potatoes, fried potatoes or french fries and the risk of any cardiovascular outcomes. 
 
The study was supported by research grants from the Swedish Stroke Association, the Swedish Research Council/Council for Research Infrastructures and Karolinska Institute.
 
Then, a systematic review of 13 observational studies, supported and conducted by the University of Copenhagen, concluded there “is no convincing evidence to suggest an association between intake of potatoes and risk of obesity, type 2 diabetes or cardiovascular disease,” the alliance said. 
 
The alliance also noted the authors suggested french fries may be associated with increased risks of obesity and type 2 diabetes, although they cautioned other factors, including overall diet and other lifestyle issues, may have affected the conclusions. The alliance said that because all of the longer-term studies cited were observational, the researchers pointed out the need for more long-term randomized control studies.
 
Potato marketers say they promote their product as heart-healthy.
 
“In February, we used a campaign called ‘Spice Up Your Spud Life,’ and it got a double tie-in with heart health,” said Keith Groven, fresh sales manager with Grand Forks, N.D.-based Black Gold Farms.