A mid-March freeze knocked out more than blueberries in Georgia this year.The freeze, which came as a knockout punch after a milder-than-normal winter, will cost the Peach State about 70% of its peach crop this year, said Gary Black, the state’s agriculture commissioner.
“It’s a tough year for our peach producers in mid-Georgia,” he said, noting the middle region is the heart of Georgia’s peach production.
Growers in the northern part of the state didn’t fare as poorly, Black said.
“A couple of them are in the driver’s seat,” he said.
One of them, apparently, is Judah Echols, partner at Jaemor Farms, who said his operation has about 70% of its normal crop available this year.
Black said most Georgia grower-shippers likely will be winding down their shipments earlier than normal, although he declined to specify when that would occur.
“Typically, we’d have peaches into August and September, but we’re not going to see that this year,” Black said. “Packingsheds that are open will be closing, shall we say, much earlier, this year.
As of June 25, 81% of Georgia’s peach crop had been harvested, compared to 56% a year earlier and a five-year average of 55%, according to the U.S. Department of Agriculture.
Echols said he would have fruit into September, although he also said it would end earlier than normal.
“In a normal year, we’d have peaches until the end of September, but this year, it will go to about Sept. 10,” he said. “The late winter was so mild in March and April, they came in earlier.”
If weather has posed any problem for his peach crop, it has been rain, which has been falling at an average rate of 9 inches per month, Echols said.
All told, however, the peach crop looks good, Echols said.
“We’re still in good shape on the peach crop; the rains haven’t messed it up by any means, but it makes it more difficult.”
Musella, Ga.-based Dickey Farms suffered a 65-70% loss from a one-two punch in late winter, said Lee Dickey, partner.
“I’d really say here in Georgia, the primary damage came from low chill hours, the lack of dormancy,” he said.
Lack of chill hours was the chief culprit, responsible for as much as 60% of the 70% total loss, Dickey said.
The fruit that survived was good, Dickey said.
“Interesting enough that quality and packout has been overall pretty good and size has been fine — I think maybe a hair smaller on some early stuff, which is not atypical,” he said.
A lot of the lost fruit would have been waste, anyway, Dickey said.
“A lot of fruit that was in that 70% loss would be buttons or abandoned fruit that didn’t grow to maturity; of the ones that did, sizing is larger than usual,” he said.
Prices are up commensurately, Black said.
“There’s no doubt, when demand is up and supply is short, the price responds accordingly; we’re seeing that in the wholesale and retail, as well,” he said.
According to the USDA, as of June 26, 25-pound cartons of loose yellow-flesh peaches from California’s San Joaquin Valley were priced at $22.95-24.95, for size 60-64; $18.95-20.95, 70-72; and $15.95-16.95, 80-84.
A year earlier, the same product was $13.95-16.95, for size 54-56; $12.95-14.95, 60-64; and $10.95-12.95, 7-72.
Dickey said he likely will have “a limited amount of peaches” through August.
“But we’ll be done packing wholesale in the next two weeks, compared to mid-August, which is when we like to finish,” he said.
California, meanwhile, is a month into its peach season and looking to fill any gaps, said Ian LeMay, director of member relations with the Fresno-based California Fresh Fruit Association, which represents about 90-95% of the grower-packer-shippers of California stone fruit.
“I would say that California growers are definitely being contacted by retailers to inquire upon availability and, to the best of California’s ability, they’re filling those orders,” LeMay said.
California’s season generally lasts into late August or early September