We all love the podcast “Stuff you should know” and other like-minded productions such as “How Stuff Works” and “Stuff you missed in history class” and so on.
So I asked members of the Fresh Produce Industry Discussion Group to imagine a podcast called “Stuff you should know about produce.” What topics should be covered in the first few shows if the decision was up to them?
Folks are fired up about this topic - 17 responses so far.
 
Here are a few responses so far:
D.J. I am biased but i would be interested in the following: The operational process of produce from Field to Supermarket; Maximizing value add through branding rather than commodity pricing; How to achieve cost efficiency in the pack house; Business challenges including labor costs and Brexit.
V.M. Let The Color & variety of the seasons fruits be your guide in merchandising your sets. Break with color and merchandise with flair. Weather is the great Equalizer in the produce industry it can go boom or it can go bloom. I prefer bloom.
R.P. I would broadcast our issue in America on consumption. Can it be moved by better merchandising, better products being embraced, continue growing better products and selling higher quality rather than focusing on price. There is a misconception on produce value. I believe our industry can change things.

A.A. As a grower packer and importer its amazing how automation (sort and pack facilities in Mexico) has compressed the time it takes to get our fruit into retailers hands.
K.M.  Growers and family farms in the US fruit and veg industry. We all know that many consumers are more interested in where and who their produce comes from. But I’d bet that people would be surprised at the relatively small size of the family farms in the specialty crop industry especially when compared to the average acreage in a farm growing for corn commercially for example.
E.D. Are organic foods healthier than conventional foods?
D.V. Transportation is vital for our perishable products. The trans world is changing and many times produce spends more time in a refrigerated trailer then anywhere else in the supply chain. Soon transportation may be the largest cost in the supply chain and the consumer will have to pay. It is time to pay more attention to your transportation process.
M.O. Educate the public on how to best handle/store produce. Fruits and vegetables are unique to the species and should be treated as such. Shelf life matters!
 
TK: My input was to focus on the consumer with a “stuff you should know” podcast about how produce gets to from the farm to the fork.
What would you want to hear about in the (as yet) fictional podcast “Stuff you should know about produce”? Let's hear it!
Speaking of “stuff you should know,” check out this quote from a smartly written and a fact-filled book called “Best Before: the evolution and future of processed food,” by Nicola Temple. I’m about halfway through the book.
 
In her chapter about fresh produce, she writes: 
“My greater concern lies in the various methods that are used to slow the decay of fresh-cut produce. With the best intention of increasing the shelf life of a product, an opportunity to deceive consumers has been created. In pre-packaged produce this could be resolved by having a “cut on” date as well as a “best before” date.”
Later she writes...
“Sometimes, I feel as though just because we can extend the shelf life of produce, doesn’t mean we should. Surely there are other ways that we can reduce the amount of food that we waste.”
To the extent consumers share her wariness about the technology used to extend shelf life, Temple makes some points worth pondering. I recommend her book.