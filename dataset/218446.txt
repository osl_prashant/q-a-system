St. Patrick’s Day is an excellent time multi-variety apple ads for retailers.
Noting that St. Patrick was known for planting apple trees in Ireland, the holiday is a natural time to expand promotions, Brianna Shales, communications manager for Stemilt, said in a news release.
“Retailers should schedule a multi-variety ad right now and plan for visually stimulating displays with lots of color that feature many popular apples starting the week of March 12,” she said in the release.
According to legend, St. Patrick planted apple trees at an ancient settlement east of Armagh in Ireland. Today County Armagh is actually known as the “Orchard County” in Ireland, according to the release.
Retailers can create their own “Orchard County” in their produce departments to push sales.
Bagged apple promotion opportunities also abound, Shales said.
Promoting the green granny smith apple is a natural for St. Patrick’s Day, and the release said Stemilt has both conventional and organic granny smith apples.
“Creating a vibrant display with smaller sized organic granny smith apples in bags is a great way to feature the green color in that multi-variety ad,” Shales said in the release.