Crawford and Vernon Counties make up the Driftless Wisconsin Area, sitting between the Mississippi River on the west and the Kickapoo Valley to the east. 

This section of the state is home to seven creameries and seven estate wineries that are opening their doors as part of the Drifless Wisconsin Wine and Cheese Trail.

Watch the story from the Dairy Farmers of Wisconsin, formerly known as the Wisconsin Milk Marketing Board, on AgDay above.