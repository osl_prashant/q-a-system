Fertilizer prices were mostly lower with only UAN32% posting gains this week. 
Our Nutrient Composite Index softened 4.16 points to 491.92 compared to last year's 498.37. 

Nitrogen

Anhydrous and UAN28% led declines in the nitrogen segment this week, each falling roughly $10 per short ton.
UAN32% took cues from last week's $8.26 rise in UAN28% pricing, gaining $8.78 of its own this week.
Urea fell for the first time in several weeks after creeping higher since mid-August.
Our nitrogen margins are wide and variable, which in the past has signaled near-term nitrogen price strength.

Phosphate

Phosphate prices retreated this week with declines in MAP out-pacing declines in DAP by roughly two-to-one.
The DAP/MAP spread narrowed slightly to 11.03, which is more narrow than our expected margin.
Wholesale DAP and MAP prices were higher at all reported terminals during the week ended October 20.

Potash 

Potash declined this week on softness in Missouri.
Wholesale potash prices ran sideways last week suggesting retail prices may be slightly overdone.
As rainy weather continues to dictate harvest progress and add delays around the Midwest, it is likely growers are still making decisions regarding fall-applied P&K.

Corn Futures 

December 2018 corn futures closed Friday, October 20 at $3.91 putting expected new-crop revenue (eNCR) at $620.00 per acre -- lower $11.36 per acre on the week.
With our Nutrient Composite Index (NCI) at 491.92 this week, the eNCR/NCI spread narrowed 7.70 points and now stands at -128.08.





Fertilizer


10/9/17


10/16/17


Change


Current Week

Fertilizer



Anhydrous


$421.11

$416.60

-$10.94

$405.66
Anhydrous



DAP


$442.51

$445.63

-$1.35

$444.28
DAP



MAP


$456.02

$459.00

-$3.68

$455.31
MAP



Potash


$331.14

$328.47


-$3.08


$325.39
Potash



UAN28


$220.81

$229.07

-$9.24

$219.83
UAN28



UAN32


$244.21

$243.50

+$8.78

$252.29
UAN32



Urea


$336.87

$341.22

-$3.12

$338.10
Urea



Composite


493.01

496.08

-4.16

491.92
Composite