West Virginia's elk herd could top 100 by summer
West Virginia's elk herd could top 100 by summer

The Associated Press

CHARLESTON, W.Va.




CHARLESTON, W.Va. (AP) — West Virginia's elk herd could rise to more than 100 by early summer, after this year's calves are born, according to the state's elk project leader.
The Charleston Gazette-Mail reports that so far this year, the Division of Natural Resources has imported 51 elk from Arizona and 15 from Kentucky's Land Between the Lakes Elk and Bison Prairie. One of the Arizona animals has since died.
The Kentucky elk were released shortly after they arrived. The Arizona elk are being held in quarantine at the Tomblin Wildlife Management Area in Logan and Mingo counties. When they're released, West Virginia's fledgling elk herd will total 87 animals.
Randy Kelley, the DNR's elk project leader, said 53 of the 87 are females, and many of them should deliver calves sometime around the first of June.
The state's elk population should top 100 after that, he said.
Just how much more has yet to be determined, he said. Three of the four adult cows imported from Land Between the Lakes tested positive for pregnancy, but results from the Arizona cows have not yet been confirmed.
"We'll want to get that information before we release those animals, but until we turn them loose there's no real concern," Kelley said.
DNR officials have plenty of time to get the pregnancy-test results. Under orders from the U.S. Department of Agriculture, the Arizona elk can't be released for quite some time yet.
"The agreement we have with the USDA is that we can't release the animals until 120 days after the last one went into the pen in Arizona," Kelley explained. "That puts us into late May."
Kelley said he and other DNR personnel tending to the animals still in quarantine at Tomblin are doing their best to make sure they remain healthy.
"We expanded the existing (3-acre) holding pen by about 1½ acres so the elk would have more room," he said. "The grass inside the pen is greening up, and the animals are foraging on it. That's not enough to sustain them, so we're supplementing it with an alfalfa-grass mixture and some commercial pellet feed. They're gaining weight."
People quite naturally want to see the Arizona elk, but Kelley said DNR officials are restricting access to the holding-pen area.
"We're not allowing anyone in who isn't taking care of the animals," he said. "The quieter we can keep things, the better off those animals will be."
___
Information from: The Charleston Gazette-Mail, http://wvgazettemail.com.