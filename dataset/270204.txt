BC-USDA-Midwest Regional Eggs
BC-USDA-Midwest Regional Eggs

The Associated Press



NW—PY018Des Moines, IA          Thu. Apr 19, 2018          USDA Market NewsDaily Midwest Regional EggsProducer prices are steady. Midwest delivered asking prices are steady. Theundertone is steady. Demand is light to moderate. Offerings and suppliesare moderate. Market activity is moderate. Breaking stock prices aresteady. The undertone remains steady. Demand is light to mostly moderate.Offerings are mostly moderate. Supplies are moderate. Schedules are fulltime. Market activity is moderate.PRICES TO RETAILERS, SALES TO VOLUME BUYERS, USDA GRADE A AND GRADE A,WHITE EGGS IN CARTONS, CENTS PER DOZEN.PRICES DELIVERED TO WAREHOUSE:RANGE           MOSTLYEXTRA LARGE          123-132         126-129LARGE                122-131         124-127MEDIUM               84-93           86-89