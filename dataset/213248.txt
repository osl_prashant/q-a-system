Organic bananas continue to account for a large chunk of banana category sales.
According to The Packer’s Fresh Trends 2017, 9% of banana buyers said they always purchased organic bananas, while 28% said they bought organic fruit at least some of the time.
“Organic sales are skyrocketing,” said Nicole Vitello, president of Oke USA Fruit Co., West Bridgewater, Mass., the importing arm of the Equal Exchange brand.
It used to be that 1 in 20 containers of bananas were organic, she said. Now that number is up to 1 in 10.
“Organics is a growing business,” she said.
Dole Food Co., Westlake Village, Calif., started its organic program more than 20 years ago, and the company now is the largest grower and distributor of premium organic bananas in the U.S., said Bil Goldfield, director of corporate communications.
“Our organic production and diverse sourcing network has allowed us to keep up with the increasing demand for organic bananas in recent years,” he said.
All of the bananas sold under the Organics Unlimited brand are certified organic, said Mayra Velazquez de Leon, president and CEO of Organics Unlimited Inc., San Diego.
“The organic produce trend has taken off quite a bit in the past few years as more and more consumers focus on healthy eating and wish to support companies that care for both the environment, workers and the community,” she said.
At Miami-based Del Monte Fresh Produce, the organic component of the banana category has seen “continuous positive growth,” said Dennis Christou, vice president of marketing.
The company has produced and sold organic bananas for more than 10 years.
“As demand for organic produce continues to increase, sales have been significantly higher over the past five years,” he said. “We expect to see this trend continue.”
The company’s organic banana products are certified organic by Quality Certification Services and also are Control Union certified, he said.
“Organics is a big piece of our business,” said Rick Feighery, vice president of sales for Philadelphia-based Procacci Bros. Sales Corp.
He estimated that 10% of the bananas the company distributes are organically grown.
“It’s been a pretty steady market for some time,” he said.
Procacci Bros. has offered organic bananas since 1999, he said, adding that just about every supermarket sells some organic bananas.
Sales of organic bananas are up 10% over last year at Earl’s Organic Produce in San Francisco, said Patrick Stewart, director of operations.
The reason Earl’s customers buy organic bananas goes beyond their desire to consume what they consider to be more healthful food, he said.
“It’s a much bigger picture than that,” Stewart said.
They are concerned for the workers who grow, pick and pack the product and about the soil and the fields where the fruit is grown, he said.
“For our customers, it’s a way of life. It’s a conscious decision,” he said.
“I believe there are more and more people who are making that conscious decision, and they understand the 360 view of importance from soil to mouth.”
Similarly, Vitello of Oke USA Fruit Co. attributes increased sales to a growing number of consumers learning about “the toxic ways in which bananas are grown.”
The cavendish, by far the most common variety, is a “genetic clone,” she said, that lacks resistance to many diseases.
“They’re just spraying more and more noxious chemicals to hold back the fungal and bacterial diseases that are rampant in bananas,” she said.