Crop calls 
Corn: 6 to 9 cents lower
Soybeans: 18 to 20 cents lower
Wheat: 7 to 10 cents lower
Focus remains on weather, with weekend rains weighing on corn and soybean futures overnight. Over the weekend, rains fell across much of the Midwest. Rains were most significant from eastern Iowa to northern Indiana and in central and southeastern Ohio. More rains are possible for the Midwest midweek and temps are expected to moderate. While southern and western Iowa, eastern Nebraska, northern Missouri and far southern Illinois mostly missed the weekend rains, traders deem weather as favorable for crop development and negative for prices to open the week. Wheat futures will face spillover from selling in the other markets and ideas the market has factored in enough spring wheat crop losses for now. 
 
Livestock calls
Cattle: Lower
Hogs: Lower
Cattle futures are called lower to start the week. Last Friday's Cattle on Feed Report was negative compared to pre-report expectations. Feedlot inventories and June placements topped expectations, while June marketing fell short of what traders anticipated. In addition, the Cattle Inventory Report showed the U.S. cattle herd as of July 1 was 4.5% bigger than two years ago. While there weren't pre-report estimates because there was no midyear report last year, the U.S. cattle herd was bigger than generally expected. Hog futures are called lower this morning on a combination of followthrough selling after a poor close last Friday and spillover from the cattle market. The big discount hog futures hold to the cash index may somewhat limit selling.