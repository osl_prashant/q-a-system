Cal/OSHA has cited six employers $241,950 for workplace safety and health violations after reports that workers contracted Valley Fever on a solar project construction site in Monterey County.
According to a news release, the employers at the California Flats Solar Project in Cholame Hills were cited for serious violations that included failure to control employee exposure to contaminated dust at the worksite, and failure to provide and ensure use of appropriate respiratory protection. 
The release said Valley Fever is caused by a microscopic fungus known as Coccidioides immitis, which lives in the top two to 12 inches of soil in many parts of the state.
“Employers who work in areas endemic to Valley Fever must take preventative measures to protect workers who may be exposed,” Juliann Sum, Chief of Cal/OSHA, said in the release.
Fungal spores can become airborne when soil is driven by high winds or when soil is being moved, according to the release.
Employers cited include:

McCarthy Building Companies, Inc., $46,540;
Papich Construction Co., Inc., $68,900;
Granite Construction Co., Inc., $46,590;
Sachs Electric Company, $46,400;
Dudek, $23,620; and
Althouse and Meade, Inc., $9,900.