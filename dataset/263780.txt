Medical marijuana growers can apply for North Dakota system
Medical marijuana growers can apply for North Dakota system

By BLAKE NICHOLSONAssociated Press
The Associated Press

BISMARCK, N.D.




BISMARCK, N.D. (AP) — North Dakota's Health Department on Friday began accepting applications from potential manufacturers of medical marijuana, though it's still unclear whether growers will need to comply with the state's anti-corporate farming law.
The application period that ends April 19 is the latest step by state officials who have been developing the medical marijuana system since legislators crafted a law a year ago. That followed voters' approval of the drug in November 2016.
The state will register two manufacturers. Last summer, nearly 100 groups and businesses submitted nonbinding letters of intent showing interest in producing or dispensing medical marijuana. Potential growing operations that follow through with a formal application will have to pay a non-refundable $5,000 fee.
North Dakota for nearly a century has barred corporations from owning or operating farms to protect the state's family farming heritage. The law does include exceptions for small family farm corporations.
The Health Department has requested an attorney general's opinion on whether medical marijuana growers will be exempt from the law, according to Jason Wahl, director of the Health Department's Medical Marijuana Division.
The request was questioned by some lawmakers at a Monday hearing during which the Legislature's Administrative Rules Committee signed off on rules covering such things as medical marijuana testing, security and transportation requirements.
During the 2017 legislative session, "we added additional language within the bill to make it clear that these were not considered agricultural operations," said Rep. Robin Weisz.
Wahl said there is not a "specific" exemption for medical marijuana operations in either the medical marijuana or corporate farming statutes, requiring legal clarification.
The North Dakota Compassionate Care Act allows the use of the drug for 17 medical conditions, along with terminal illnesses. The Health Department hopes to have the drug available by late this year.
The agency last month began accepting proposals from laboratory companies that want to test medical marijuana, and selected a Florida-based company to implement a system to monitor the drug program.
BioTrackTHC submitted a five-year, $600,000 proposal. Negotiations concluded this week, with the state agreeing to pay $560,000 over five years, Wahl said. The state can end the contract at any time after the first two years.
The Health Department will announce application periods for potential dispensaries, patients and caregivers in the coming months.
___
Follow Blake Nicholson on Twitter at: http://twitter.com/NicholsonBlake