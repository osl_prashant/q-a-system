As the population of international cultures increases throughout the U.S., so does the knowledge and appreciation of their unique produce.
“It’s growing, every single item is growing,” said Mary Ostlund, director of marketing for Homestead, Fla.-based Brooks Tropicals Inc.
“These fruits and veggies may have been introduced by others with native cusisines from afar, but sales are growing and growing beyond ethnic demographics.”
Ricardo Echeverri, vice president of tropicals for Coral Gables, Fla.-based Fyffes North America, also sees sales of tropical produce increasing along with the population.
“I believe ethnic tropicals will continue to grow as the latest data shows Hispanic and Asian populations grew by 2% and 3%, respectively, from 2015-16,” he said.
Jose Rossignoli, director of sourcing for Eden Prairie, Minn.-based Robinson Fresh, said his company continues to see a growing demand for its papayas and mangoes.
“Our papaya and mango sales are higher year-over-year.”
Other items that are trending with consumers right now are star fruit, red guava, Thai guava, dragon fruit, lychees and longans, Ostlund said. Brooks Tropicals grows these items in southern Miami-Dade County.
“Star fruit has grown beyond top garnish status,” she said. “Its slices are starring in salads both leafy and fruity.”
She added that certain specialties have devoted followers, causing curious consumers to try something new.
Millennials and social media also help to shed light on these items trying to break out of their niche.
“It’s easier than ever to look up recipes online for exotic products,” said Andrea Dubak, marketing specialist for Calgary, Alberta-based Thomas Fresh.
“Many millennials subscribe to food-related accounts on their social media, which may inspire them to be more adventurous with the ingredients they purchase.”
Facebook, Instagram and Pinterest are platforms for sharing new and different fruits and vegetables with a wider audience, Ostlund said, adding that Twitter can be used as a discussion board to ask questions.
“Brooks Tropicals has social media posts on specialties that can be (adapted) for retail, wholesale and foodservice use,” she said.
 
Winter promos
If there is one thing that most marketers can agree on it’s that during the cold months of winter, tropical, international and specialty items shine and bring a little warmth to the produce department.
“Many of our Asian and ethnic items are available year-round, but we see some trends towards specific items in the fall and winter months,” Dubak said.
Dubak said persimmons and cactus pears sell well when the temperatures start to drop.
“Persimmons are very popular within the ethnic communities, especially during the Christmas season, as growing becomes localized.”
“Cactus pears are also a great Christmas and winter fruit,” Dubak said.
“These specialty pears are used to make delicious jams in some communities.”
Echeverri agreed that around the holidays is a popular time for consumers buying international produce.
“Winter is the time of the year with the most demand due to people wanting to cook more and have (a) family get-together,” Echeverri said.
“All ethnic (tropicals) sell better during the winter months, including yuca, chayote, calabaza, malanga and eddoes,” he said.
Although consumers are already headed for the produce department to buy specialty items in the winter, it is important that retailers promote these items to further drive sales, while also encouraging other interested shoppers to buy something new and different.
“Think of your produce aisle as destination travel for the taste buds,” Ostlund said.
“The gorgeous colors and textures of many specialty fruits deliver the exotic locales that are the opposite of the weather outside.”
Rossignoli thinks the demand for fruit in the winter decreases, but showcasing tropical produce can help to increase sales.
“With winter months being colder, we encourage retailers to bring a bit of summer warmth into their offerings with tropical produce,” Rossignoli said.
Ostlund said “funny-looking fruit draws attention.” She suggests retailers use signs to their advantage to drive sales.
“Signage showing these fruits’ beautiful but unusual insides draws the curious to reading more,” Ostlund said.
Good signs draw the shoppers in, but offering samples of the produce can complete the sale.
“Encouraging sampling of the product in-store is also a way to introduce ethnic tropicals to new consumers,” Echeverri said.