Stay sought for order allowing 6 farmers to use dicamba
Stay sought for order allowing 6 farmers to use dicamba

The Associated Press

LITTLE ROCK, Ark.




LITTLE ROCK, Ark. (AP) — Arkansas is asking the state's top court to halt a judge's order allowing six farmers to use an herbicide that was banned by state regulators following complaints that it drifted onto crops and caused damage.
Attorney General Leslie Rutledge's office on Thursday asked the state Supreme Court to stay Pulaski County Circuit Judge Tim Fox's ruling exempting the farmers from the state Plant Board's rule banning dicamba's use. The panel has banned dicamba's use from April 16 through October 31 this year. Rutledge on Wednesday filed notice she was appealing Fox's ruling.
Fox last week dismissed the farmers' lawsuit over the ban, citing the state's sovereign immunity. But he voided the rule for the six farmers, ruling their due process rights and right to appeal the ban had been curtailed.