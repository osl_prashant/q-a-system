BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Tue.Mon.No. 2 Soft wheat4.66¼4.62½No. 1 Yellow soybeans10.2110.17No. 2 Yellow Corn3.62½e3.65½eNo. 2 Yellow Corn3.80½p3.82½p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted