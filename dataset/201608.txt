After a six year period of dormancy, Ag Secretary Tom Vilsack has sent a draft of rules changes to the Grain Inspection, Packers and Stockyards Administration (GIPSA) act to the White House for final review. That suggests USDA is hoping to release them to the public before the Obama Administration leaves office in about 90 days.In a letter to meat industry groups last week, USDA said the GIPSA rules include an interim final rule on competitive injury and two proposed rules to address undue preference and the poultry grower ranking system. Vilsack said USDA is considering leaving out several provisions that were contained in the GIPSA rule proposed in 2010 that many in the beef and pork industries found objectionable.
Still, several meat industry groups object to USDA's latest GIPSA proposal. Barry Carpenter, president and CEO of the North American Meat Institute, says the interim final rule will "open a floodgate of litigation, upend the established system for marketing cattle, pork and poultry in the U.S., and add costs at every step along the process."
The chairman of the Senate Ag Committee, Kansas Senator Pat Roberts, has voiced his opposition to the GIPSA proposal. He says, "if the proposal is in any way similar to the 2010 GIPSA proposal he believes the U.S. livestock and meat sectors will be tremendously burdened during already difficult times."
Iowa Senator Chuck Grassley says he supports the proposed GIPSA rule. In a conference call with reporters on Tuesday, Grassley said he, "believes the rules will enable family farmers to have more confidence in the market and help ensure that they get a fair price for the efforts."


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.