Cranberry marketers say they have room to build sales of their product two ways: time and place.
Fresh cranberry sales peak around the Thanksgiving-Christmas holiday period. Now, marketers are looking to promote the berry year-round.
The holidays drive fresh cranberry sales, but food trends do as well, said Brian Bocock, vice president of product management for Salinas, Calif.-based Naturipe Farms LLC.
"Many of today's consumers are looking for more traditional and healthy alternatives to the typical processed or canned cranberry option," he said.
"Consumers are making increasingly more informed decisions when it comes to buying fresh, buying local and incorporating healthy fruits and vegetables into their diets," said Michelle Hogan, executive director of the Wareham, Mass.-based Cranberry Marketing Committee.
Those decisions transcend seasonality, she said.
"We see cranberries' versatility and myriad health benefits as being the top drivers of sales," Hogan said.
Kellyanne Dignan, senior manager of corporate communications with Lakeville-Middleboro, Mass.-based Ocean Spray Cranberries, offered a statistic in agreement.
"We know over 60% of fresh consumers buy three bags or more and a majority of them freeze for later usage," Dignan said, noting that fresh cranberries are available only from October to December.
Holiday meals remain the peak sales season, though, Dignan said.
With that in mind, it's important to provide plenty of use ideas, she said.
"Recipes are always a key strategy in selling more fruit," she said. "The cranberry's vibrant color and bold, tart taste makes it a truly versatile ingredient."
Agritourism has become a marketing tool for some organizations, as visitors get up-close looks at working bogs.
"Like many growers in our area, we have in the past decade been expanding our agritourism efforts," said Linda Burke, marketing director for A.D. Makepeace Co. in Wareham, Mass.
Makepeace hosts bus tours throughout the harvest, as well individual visitors on some dates, Burke said.
Makepeace also co-sponsors an annual Cranberry Harvest Celebration, which draws an estimated 30,000 visitors each Columbus Day weekend. There also is Redbrook HarvestFest, a lower-key event, Burke said.
A new event this year, which Ocean Spray is co-sponsoring, is a Bog to Table Dinner, to showcase cranberries in "gourmet dining and unexpected cocktails," in a flooded cranberry bog setting, Burke said.
"Combined, these events help us raise understanding of this iconic American industry and capitalize on the growing interest in local and healthy foods," she said.
Marketers are busy promoting cranberries whenever and wherever possible, as a way to restore balance to the supply-demand equation, said Brian Wick, executive director of the Cape Cod Cranberry Growers Association in Carver, Mass.
"In terms of marketing, I think most of the industry feels that with the amount of berries out there, we're going to need to have more markets developed, more demand and more opportunities," he said.
Marketers also have to think beyond traditional revenue streams, said Mary Brown, owner of Glacial Lake Cranberries in Wisconsin Rapids, Wis.
Brown launched a separate company, Honestly Cranberry, two years ago to offer dried, unsweetened berries to retailers.
"I think the world is changing; it's no longer grocery stores or even following the brand anymore," she said.
Ocean Spray got into the category with its Craisins in 1993.
"We are very customer-centric and focused against programs, packaging and pricing that meet the needs of each of our retailers," Dignan said.
Of course, there are traditional holiday marketing pushes on the fresh side, said Doug Perkins, CEO of Sheridan, Ore.-based HBF International LLC.
"The holidays, with the timing of the crop our cranberries, are perfect for Thanksgiving and Christmas promotions," he said.
As part of its traditional promotional efforts, the cranberry industry is targeting young adults, said Bob Wilson, managing member of Wisconsin Rapids, Wis.-based The Cranberry Network LLC, which markets fruit grown by Tomah, Wis.-based Habelman Bros. Co.
"What the cranberry industry is attempting to do is reach this year, and beyond ... a new category of consumer - the millennials - who may be less prone to cooking big dinners or making Grandma's recipes," Wilson said.
This year's cranberry crop, particularly in the East, has dealt with weather challenges, notably drought conditions, said Brenden Moquin, controller with Morse Bros. Inc., an Easton, Mass.-based cranberry growing operation with bogs totaling 385 acres in Massachusetts and 390 in Quebec, Canada.
"Cranberry bogs have significant need for water," he said.
Moquin said the season should produce ample volumes of berries, from early September through about early November.
"It's a very short deal," said Bob Von Rohr, marketing and customer relations manager with Glassboro, N.J.-based Sunny Valley International Inc. "We did outstanding last year - the bagged product was a real success."
The various marketing approaches appear to be paying dividends, said Tom Lochner, executive director of the Wisconsin Rapids-based Wisconsin Cranberry Growers Association.
"We are getting increases in sales and also seeing inventories starting to stabilize or plateau."
Wisconsin has about 250 growers, 275 farms and 21,000 acres, and the state produces more than 60% of the U.S. crop, Lochner said.