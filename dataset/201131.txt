Pro Farmer Crop Consultant Dr. Michael Cordonnier left his estimates of the U.S. corn and soybean crop unchanged from last week, but he has changed his bias from "neutral to negative" to "neutral." He notes while there are still dry pockets across the Corn Belt, the forecast for later this week is calling for generous rains, which if realized, would "go a long way toward eliminating some of the dry concerns in the central and eastern Corn Belt."
"Dryness is still a major concern in the western and northwestern Corn Belt and that area is not forecasted to receive much moisture," notes Cordonnier. "The temperatures have been very cool recently, which helps to extend the moisture supply. I worry that the cool temperatures may be masking the drying conditions in the western and northwestern Corn Belt. If that area of the Corn Belt turns excessively hot, moisture stress could quickly surface once again."
Based on yesterday's USDA crop condition ratings  which held steady with the previous week, Cordonnier says the top five rated corn states are Tennessee, Pennsylvania, Colorado, Kentucky and then it's a tie between Iowa and North Carolina. The five lowest rated corn states are South Dakota, Indiana, North Dakota, Ohio and Illinois.
For soybeans, USDA lowered the amount of crop rated "good" to "excellent" by one point from the previous week. Cordonnier points out that improvements in condition ratings in the central, southern and eastern Corn Belt were offset by declines in the western and northwestern Corn Belt. He says the top five rated soybean states are Tennessee, Minnesota, North Carolina, Kentucky and then it's a tie between Louisiana and Wisconsin. The five states with the lowest rated soybeans are South Dakota, North Dakota, Indiana, Ohio and Nebraska.



Cordonnier 2017 Production

Harvested
			acreage 


Yield


Production




million acres


bu. per acre


billion bu.



Corn

81.6


167.0


13.63



Soybeans

89.1


48.0


4.27




Specifically, Cordonnier outlines the following areas of concern for the U.S. crops:

"The major area of concern now are South Dakota (63% short to very short on soil moisture), North Dakota (53% short), and Nebraska (56% short).
Pockets of relatively light rainfall over the past 30 days (less than 2 inches) in an area starting in southeastern South Dakota and stretching from northwestern Iowa to southeastern Iowa and extending into west-central Illinois and central Illinois. Approximately from Sioux Falls, South Dakota to Des Moines, Iowa to Springfield, Illinois.
There continues to be pockets of concerns in the eastern Corn Belt, but the region has slowly improved.