The proposed 2018 U.S. Department of Agriculture budget suggests the elimination of funding for the Market Access Program and the Specialty Crop Block Grant Program.Those programs, along with others, are listed in the proposal under the heading “Duplicative, Ineffective, or Lower Priority Programs.”
Robert Guenther, senior vice president of public policy for the United Fresh Produce Association, and Kevin Moffitt, president and CEO of Pear Bureau Northwest, said they were disappointed with the proposal but confident the programs would continue.
“Agriculture in general is disappointed in this budget,” Guenther said. “Fortunately, it is just a document that Congress can look at. They don’t have to agree with it.”
Pear Bureau Northwest is particularly opposed to eliminating MAP funds, which the bureau has used since the 1980s.
“This is a program ... shown to give a very good return on the money spent,” Moffitt said.
The bureau receives about $3 million per year from MAP, and it matches that amount.
Though Moffitt expected the programs will survive, it was a bit concerning to see them targeted, he said.
“Everybody has to work hard up on Capitol Hill,” Moffitt said. “That’s where the bottom line is.”
House Agriculture Committee Ranking Member Collin Peterson, D-Minn., released a statement that acknowledged the budget is a proposal, but it “is still a statement of priorities and should be of concern to all rural Americans. Going down this path all but guarantees there will be no new farm bill ... I’m hopeful we can get past this ideology and make reasonable reforms as part of future farm bill debate, but this is not a good place to start.”
MAP funding, supplemented by money from industry groups, helps promote dozens of fresh fruits and vegetables in export markets. This year it allocates $173 million to fresh produce and other agricultural products, administered by the Foreign Agricultural Service. The Specialty Crop Block Grant Program, which announced in March that more than $60 million would be available to the industry, is under the purview of the Agricultural Marketing Service.
The Specialty Crop Farm Bill Alliance issued a statement decrying the proposed changes.
“We are very disappointed to see the president’s budget, released yesterday, call for draconian cuts, or even total elimination of programs that are important to the specialty crop sector,” the alliance wrote. “Eliminating programs that are critical to developing domestic and international markets for specialty crops ... seems to indicate a fundamental misunderstanding of what policies are needed to help specialty crop providers create their own success.”
The alliance argues that the programs are sound investments and says it plans to “work vigorously with our industry partners to make sure the administration understands the challenges our industry faces so that we may work together to develop policies that help, not hurt, America’s specialty crop producers,” according to the statement.
The elimination of MAP funding is projected to save $1.8 billion over 10 years, and the elimination of the Specialty Crop Block Grant Program is projected to save $771 million over 10 years.
Awards through the specialty crop grant program last year included more than $22 million to the California Department of Food and Agriculture for 73 projects. The Florida Department of Agriculture and Consumer Services received more than $3.8 million for 33 projects.
MAP commitments this year include:
$4.86 million to the Washington Apple Commission
$4.83 million to the National Potato Promotion Board
$3.12 million to the California Table Grape Commission
$2.89 million to Pear Bureau Northwest
$1.9 million to the Cranberry Marketing Committee
$1.76 million to the Washington State Fruit Commission
$1.14 million to Sunkist Growers
Other produce groups receiving funding include the U.S. Apple Export Council, California Cherry Marketing and Research Board, the California Cling Peach Growers Advisory Board, the California Pear Advisory Board, the California Fresh Fruit Association, the California Strawberry Commission, the Cherry Marketing Institute, the Florida Tomato Committee, the U.S. Highbush Blueberry Council and the American Sweet Potato Marketing Institute.
 
SNAP
The proposed budget also suggests changes to the Supplemental Nutrition Assistance Program, including implementing a cost-sharing requirement for states, introducing fees for retailers applying or recertifying to accept SNAP benefits, and “tightening certain eligibility and benefit calculation standards.”
The budget proposal includes $193 million in cuts to the program, according to a news release from Congressman Jimmy Panetta, D-Calif.
“While it is important to have a strong defense and national security budget, it cannot come at the expense of cutting other programs on which Americans depend,” Panetta said in a statement. “I will not support these proposed cuts, and I encourage my colleagues on both sides of the aisle to reject this harmful budget. We must instead work together to advance a responsible budget that helps our constituents and strengthens our communities.”