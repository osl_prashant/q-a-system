Soybean acres to exceed corn for the first time in 35 years
Soybean acres to exceed corn for the first time in 35 years

By DAVID PITTAssociated Press
The Associated Press

DES MOINES, Iowa




DES MOINES, Iowa (AP) — Corn has been dethroned as the king of crops as farmers report they intend to plant more soybeans than corn for the first time in 35 years.
The U.S. Department of Agriculture says in its annual prospective planting report released Thursday that farmers intend to plant 89 million acres in soybeans and 88 million acres in corn.
The primary reason is profitability. Corn costs much more to plant because of required demands for pest and disease control and fertilizer. When the profitability of both crops is close, farmers bet on soybeans for a better return.
The only year that soybean acres beat corn in recent memory was 1983, when the government pushed farmers to plant fewer acres to boost prices in the midst of the nation's worst farm crisis.