BC-USDA-SD Direct Fdr Catl
BC-USDA-SD Direct Fdr Catl

The Associated Press



SF—LS160Sioux Falls, SD    Fri Apr 13, 2018    USDA-SD Dept of Ag Market NewsSouth and North Dakota Direct Feeder CattleFeeder Cattle Weighted Average Report for 04/13/2018Receipts: 292          Last Week: 0              Last Year 243Compared to last week:  Feeder steers and heifers not tested lastweek.  Severe winter storms are sweeping across the state bringingblizzard conditions and high winds.  Supply includes 76 percent over600 lbs and 24 percent heifers.  All sales FOB North and South Dakotawith a 2-3 percent shrink or equivalent and a 4-8 cent slide onyearlings and an 8-12 cent slide on calves from base weights.Feeder Steers Medium and Large 1Head   Wt Range   Avg Wt    Price Range   Avg Price  Comments26     635        635       163.31         163.31 Current FOB195     825        825       135.00         135.00 Current FOBFeeder Heifers Medium and Large 1Head   Wt Range   Avg Wt    Price Range   Avg Price  Comments71     600        600       153.56         153.56 Current FOBSource:  USDA Market News Oklahoma City, OKJoe Massey 405-232-542524 Hour Market Report 1-405-636-2691www.ams.usda.gov/mnreports/sf—ls160.txt