Roland Lafont knows firsthand the daily challenge growers face to feed the world, his theme as new board president for the Quebec Produce Marketing Association.
In the 1980s, he was working for the provincial government in Montreal when his father bought an abandoned apple orchard in Saint Paul d'Abbotsford.Lafont fell in love with the land, and with absolutely no farming experience decided he could "do something with it."
He and his wife Sylvie launched Vergers Saint-Paul Inc. in 1989 and never looked back.
Today the family owns nearly 200 acres and manages about 500 acres with grower-partners.
In 1997, Lafont became president of the Association of Quebec Apple Packers, where for eight years he worked to raise quality standards and improve the marketing of Quebec apples.
He joined QPMA the same year to meet more industry people.
"The QPMA was, and still is, the place to be," he said.
Lafont became a director in 2006 and will become the association's next president on Aug. 20 in Montreal.
"It's been a long journey, and my wife and I worked very hard to be here today, but I am proud of what we have accomplished," he said.
He's also proud that his 31-year-old son Robert has joined the business.
"It takes a lot to grow, pack, ship and distribute high-quality produce so consumers can enjoy a variety of fresh fruits and vegetables," Lafont said.
"Most of the produce we grow is still hand-picked, and we invest a lot of money to improve the way we grow and distribute our products with a fair price in mind. That's a challenge for everybody," he said.
He believes the produce industry needs to do more to educate consumers and encourage them to eat more fruit and vegetables.
"It's the best investment we can make for a long, healthy life," he said.