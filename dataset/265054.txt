BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Thu.      Wed.No. 2 Soft wheat              4.46¾     4.44½No. 1 Yellow soybeans        10.04¾    10.04¾No. 2 Yellow Corn             3.56  e   3.55  eNo. 2 Yellow Corn             3.74  p   3.73  p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted