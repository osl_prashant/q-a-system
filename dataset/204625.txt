Herbicide resistance is spreading at alarming rate. Hear weed science experts from the University of Illinois, Iowa State University and the Ohio State University talk resistance trends and management tips*. Watch now.Bob Hartzler
Iowa State University
Bob Hartzler stresses the need for diversity. He says, "It is important to use multiple sites of action. And to make sure the rates are effective. You need to make sure all herbicides used are at their full rates."
Aaron Hagar
University of Illinois
Aaron Hagar talks about using diversity to achieve as close to zero weed seed return to the seed soil bank as you can. Especially on tough-to-control weeds like multiple-resistant waterhemp.
Mark Loux
The Ohio State University
Mark Loux mentions adding cover crops as a tool in the mix for the fight against herbicide resistance. He mentions using cereal rye to help control marestail.

*Given as part of a fall 2016 CropLife webinar