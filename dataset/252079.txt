BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Thursday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMar         2529    2529    2491    2506  Up    50May                                 2518  Up    50May         2435    2522    2427    2493  Up    50Jul         2458    2540    2452    2518  Up    50Sep         2476    2553    2472    2535  Up    49Dec         2474    2541    2474    2528  Up    47Mar         2465    2528    2465    2512  Up    43May         2478    2533    2471    2516  Up    42Jul         2490    2527    2490    2527  Up    42Sep         2502    2539    2502    2539  Up    43Dec         2517    2555    2517    2555  Up    45