Is it ever acceptable to deliberately kill an animal — when it’s food for another animal, that is, rather than for one of us?
That issue’s up for debate (again), following an alleged after-school stunt by an Idaho junior high school teacher that has both animal welfare groups and ordinary citizens up in arms.
We’re not including livestock in this discussion, because activists always demand that all farm animals be set free, or sent to sanctuaries to die of old age, or whatever other equally ridiculous recommendations they propose as the means to eliminate animal agriculture.
No, this debate is over an incident — unconfirmed by the authorities, it should be mentioned — in which one animal ate another animal; commonplace in the wild, most would acknowledge, but horrific in a classroom, according to majority opinion.
An additional complication in sorting out where any moral outrage should be directed is the fact that this incident involved an invasive species that was illegal for the teacher in question to own as a pet.
Here’s a brief recap of what happened, on the odd chance you haven’t already soaked up the gory details on social media.
Invasive, and Nasty
A Preston, Idaho, junior high school science teacher named Robert Crosland — a well-liked teacher, it should be noted — in front of some students, allegedly fed his pet snapping turtle a live but sick puppy.
Which was pretty much the exact characterization directed at Mr. Crosland when complaints about his actions began pouring into school district offices and to animal welfare organizations in the area.
The Idaho Statesman classified the protests as a “tidal wave of public outrage,” including phone calls and emails from around the country. Although no charges have yet been filed, school district officials, as well as the sheriff’s office in Franklin County (located in southeast Idaho bordering Utah and Wyoming) are investigating the incident.
Meanwhile, the Idaho Department of Fish and Game has apparently confiscated his snapping turtle, considered an invasive species in Idaho.
Probably for good reason. Native to the Eastern U.S. and brought to the West as pets, the snapping turtle is characterized as a “robust, aquatic turtle” that can weigh up to 90 pounds and is a “highly adaptable omnivore at the top of the food chain in its habitat,” according to the Fish and Game website.
Charlie Justus, regional conservation officer for Idaho Fish and Game, explained that snapping turtles are considered invasive because “They compete with our natural wildlife,” he told the newspaper.
Although snapping turtles can be found up and down the Boise River, Justus said a permit is required to keep one. A person found to have one without a permit can be charged with a misdemeanor.
Of Mice and Men — and Puppies
Now, if the turtle in question were living in the wild — east of the Mississippi, let’s stipulate — there would be no issue with its consumption of some unlucky prey, although a puppy wouldn’t likely be on its regular menu.
Thus, the fact that the incident took place in Idaho clouds the already muddied waters, and not just in whatever tank the turtle’s living.
But now we come to the more pressing question: Is it okay to feed predator animals with live prey? Animal welfare advocates say absolutely not. No way, no how, never ever.
“People are really upset, [and] we share the concern and the outrage,” Jeff Rosenthal, executive director of the Idaho Humane Society, told the Idaho Statesman. Rosenthal suggested that feeding a live puppy to a turtle would be a clear violation of the state’s animal cruelty statutes.
But what about feeding a live mouse to a pet snake? Is that animal cruelty? Or feeding a live worm or an insect to a chicken? And what about all those cute, adorable pet kitties, who annually kill millions of birds, rodents and even equally cute baby bunnies? They’re not wild animals hunting to survive. Is that considered “cruelty” on the part of the cats’ owners?
It seems the problem here isn’t so much that some predator ate another animal for dinner, but that the victim was a puppy, and that the killing took place in front of impressionable kids.
At the end of the day, what (allegedly) happened was reckless and ill-advised — at best — on the part of the teacher. He’s not exactly serving as an admirable role model by keeping a snapping turtle illegally, and then allowing it to kill and eat a puppy in front of young people.
But wait: there’s yet an additionally troubling coda to this already ugly story.
According to the newspaper’s reporting, the Preston woman who reported the incident to the police said “she’s now a pariah” in that small town of 5,200 people.
“I’ve been threatened,” said Jill Parrish. “This is a cut-and-dried case of animal cruelty. But they’re saying, ‘The teacher is a good teacher.’ Everybody loves him.”
In fact, more than 2,600 people have apparently signed an online petition in support of the accused teacher and are demanding that he retain his job.
Could the answer to the question of whether it’s okay to kill an animal get any murkier? Tell us what you think.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.