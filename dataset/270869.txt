Sunkist Growers Inc. is now shipping gold nugget mandarins.Valencia, Calif.-based Sunkist kicked off its deal the week of Feb. 8, said Joan Wickham, the company’s manager of advertising and public relations.
“This year’s crop is very strong and has incredible flavor,” Wickham said.
Gold nuggets, a late-season California mandarin variety, are known for their sweetness and shiny, bumpy rind. Sunkist plans to ship them through May, Wickham said.
Sunkist offers retailers customizable point-of-sale materials to promote gold nuggets.