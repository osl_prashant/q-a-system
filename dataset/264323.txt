Last week, AgDay host Clinton Griffiths was part of a delegation from the Missouri Farm Bureau (MOFB).

Each year, MOFB brings agricultural leaders from across the state to Washington D.C. for an in depth discussion with policy analysts, advocates and legislators. It’s a diverse cross-section of the industry to focus on the issues at hand.

If you missed any of the coverage on AgDay, watch on the following links:
Perdue Asks CME, ICE For Advice on RINs Transparency

Perdue: Farmers “Dutifully Anxious” About Agricultural Situation

Pruitt’s EPA Not Putting Up Fences, WOTUS Reformation

Farmer Mac: Cautious Optimism on the Farm Economy

An Immigration Fix For Ag Still Possible

Clinton Griffiths’ One-On-One With Sec. Perdue

Farm Bill Status: Sit-Down with Sen. Pat Roberts
Conaway, Roberts Discuss Farm Bill Delays
Farm Sense: Marveling Washington D.C.