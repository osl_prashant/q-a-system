When USDA's weekly crop condition ratings are plugged into Pro Farmer's weighted (by production) Crop Condition Index (CCI; 0 to 500 point scale, with 500 representing perfect), the corn crop dropped 1.93 points and is 29.82 points lower than year-ago. Meanwhile, the soybean crop improved by 1.31 points, but is 25.44 points lower than year-ago.
The corn and soybean crops declined slightly in Illinois and Iowa, while the Indiana crops improved slightly.




 
Pro Farmer Crop Condition Index





Corn




This week



Last week



Year-ago


 


Soybeans




This week



Last week



Year-ago





Colorado (1.03%*)


3.55


3.68


3.90


Arkansas *(3.78%)


14.32


14.36


14.47




Illinois (15.40%)


54.37


55.91


62.85


Illinois (13.85%)


49.73


50.56


54.86




Indiana (6.64%)


22.64


22.44


26.88


Indiana (7.41%)


25.72


25.35


28.91




Iowa (17.72%)


64.32


64.86


68.03


Iowa
			(13.35%)


47.13


47.27


52.66




Kansas (4.29%)


14.81


14.99


14.66


Kansas (3.96%)


13.71


13.67


13.49




Kentucky (1.57%)


6.20


6.15


6.42


Kentucky (2.14%)


8.04


8.09


8.63




Michigan (2.35%)


8.59


8.76


8.59


Louisiana (1.59%)


6.24


6.22


6.50




Minnesota (9.66%)


37.77


37.87


37.84


Michigan (2.38%)


8.53


8.65


8.46




Missouri (3.81%)


13.90


13.79


13.95


Minnesota (8.82%)


33.62


33.53


33.71




Nebraska (11.62%)


41.15


41.50


45.62


Mississippi (2.38%)


9.29


9.22


9.14




N. Carolina (0.71%)


2.79


2.80


2.69


Missouri (5.86%)


21.27


21.38


21.63




N. Dakota (2.70%)


8.36


8.30


9.71


Nebraska (7.46%)


26.20


26.20


29.29




Ohio (3.80%)


13.69


13.65


13.93


N. Carolina (1.50%)


5.69


5.53


5.87




Pennsylvania (0.98%)


4.31


4.21


3.93


N. Dakota (5.24%)


16.14


15.88


17.76




S. Dakota (5.62%)


15.89


15.44


19.91


Ohio
			(6.14%)


21.26


20.76


21.45




Tennessee (0.89%)


3.69


3.64


3.48


S. Dakota (5.93%)


17.62


16.67


20.43




Texas (2.06%)


7.86


7.80


7.00


Tennessee (1.86%)


1.00


1.00


1.00




Wisconsin (3.61%)


13.78


13.75


14.56


Wisconsin (2.29%)


8.84


8.75


8.78




Corn total


357.49


359.42


387.72


Soybean total


348.36


347.05


373.80





* denotes percentage of total national corn crop production.
Iowa: All of Iowa experienced cooler than normal temperatures and most of the State received below normal precipitation during the week ending August 6, 2017, according to the USDA, National Agricultural Statistics Service. Statewide there were 5.9 days suitable for fieldwork. Activities for the week included applying fungicides and insecticides, hauling grain, and haying. Topsoil moisture levels declined to 24 percent very short, 32 percent short, 44 percent adequate and 0 percent surplus. According to the August 1, 2017 U.S. Drought Monitor, Iowas region of severe drought expanded to include 16 counties in south central and southeast Iowa. Subsoil moisture levels rated 19 percent very short, 32 percent short, 48 percent adequate and 1 percent surplus.
Ninety-five percent of Iowas corn crop has reached the silking stage, 5 days ahead of the five-year average. Forty-two percent of the corn crop has reached the dough stage, 4 days behind last year. Corn condition declined to 2 percent very poor, 8 percent poor, 26 percent fair, 53 percent good and 11 percent excellent. Soybeans blooming reached 89 percent, 1 week behind last year and 3 days behind average. Two-thirds of soybeans were setting pods, 5 days behind last year but equal to average. Soybean condition rated 3 percent very poor, 9 percent poor, 29 percent fair, 50 percent good and 9 percent excellent.
Illinois: There were 5.8 days suitable for fieldwork during the week ending August 6. Statewide, the average temperature was 70.5 degrees, 3.3 degrees below normal. Precipitation averaged 0.46 inches, 0.42 inches below normal. Topsoil moisture supply was rated at 9 percent very short, 25 percent short, 64 percent adequate, and 2 percent surplus. Subsoil moisture supply was rated at 5 percent very short, 25 percent short, 68 percent adequate, and 2 percent surplus.
Corn silking reached 97 percent, compared to 98 percent for the 5-year average. Corn dough was at 57 percent, unchanged from this time last year. Corn dented reached 9 percent, compared to 14 percent for the 5-year average. Corn condition was rated at 4 percent very poor, 8 percent poor, 30 percent fair, 47 percent good, and 11 percent excellent. Soybeans blooming reached 94 percent, compared with 90 percent last year. Soybeans setting pods was at 70 percent, compared to 66 percent last year. Soybean condition was rated 4 percent very poor, 8 percent poor, 24 percent fair, 53 percent good, and 11 percent excellent.
Indiana: Favorable weather conditions last week enabled farmers to make good progress in field work, according to Greg Matli, Indiana State Statistician for the USDAs National Agricultural Statistics Service. Dryer weather in the beginning of the week enabled farmers to make strides in haying activities. Some farmers continued to irrigate fields due to the lack of rain until the latter part of the week. The statewide average temperature was 69.9 degrees, 3.1 degrees below normal. Statewide precipitation was 0.67 inches, below average by 0.24 inches. There were 5.4 days available for fieldwork for the week ending August 6 up 0.4 days from the previous week.
Regionally, corn was 94% silked in the North, 93% in Central, and 95% in the South. Corn was 35% in dough in the North, 61% in Central, and 56% in the South. Corn rated in good to excellent condition was 56% in the North, 48% in Central, and 53% in the South. Soybeans were 92% blooming in the North, 90% in Central, and 86% in the South. Soybeans were 61% setting pods in the North, 69% in Central, and 66% in the South. Soybeans rated in good to excellent condition were 60% in the North, 50% in Central, and 52% in the South. In the northwestern portion of the state, some fields were hit with nickel sized hail. There was some lightening and minimal damage. Crops are in varying stages due to replanting. Farmers are continually walking their fields for diseases. There have been several reports of weeds, and common and southern rust. Aerial application of fungicide has taken place across the state. Farmers continued with mint harvest.
Minnesota: Mild weather during the week ending August 6, 2017, allowed for 5.4 days suitable for fieldwork according to USDAs National Agricultural Statistics Service. Rains quenched the dry areas through the middle of the state, but some southern and northern counties were left hoping for more. Activities for the week included small grains harvesting and spraying of pesticides. Topsoil moisture supplies rated 5 percent very short, 20 percent short, 72 percent adequate and 3 percent surplus. Subsoil moisture supplies rated 4 percent very short, 20 percent short, 73 percent adequate and 3 percent surplus.
Ninety-three percent of the corn crop was silking, with 36 percent of the crop at the dough stage. Corn crop condition rated 80 percent good to excellent, down 1 percentage point from the previous week. Nearly all soybeans were blooming this week at 95 percent blooming. Seventy-one percent of the crop was setting pods, one day ahead of the five year average. Soybean condition rated 74 percent good to excellent, up 1 percentage point from the previous week.