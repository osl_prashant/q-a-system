The solar eclipse on Monday, Aug. 21, has inspired three dairies to open their gates and invite people onto their farm for the once in a lifetime experience. It also will be a great opportunity to hopefully cash in on the event by either selling tickets or dairy products for those in attendance.Moving from west to east the eclipse will start in Oregon, but the first dairy to host an eclipse viewing party (that we know of) will be near the middle of the country outside Osborn, Mo.
Shatto Milk Company is 50 miles north of Kansas City and sells milk throughout the metro area in stores and via a milk delivery business. The farm sold 350 tickets for parking and an estimated 1,500-2,000 people are expected to attend.   
“We never appreciated how big of deal this is,” says Matt Shatto, vice president for the company founded by his parents who own the farm.
There has been interest from people across the country to attend, and there was also a family from Denmark that contemplated watching the eclipse on the 300 cow dairy.
Prior to the eclipse happening Shatto Milk has been offering specialty eclipse T-shirts for sale online and has produced 6,500 eclipse milk bottles. The limited edition eclipse milk is a black milk containing a double dose of cookies with cream. Stores in the KC-metro that want the milk will either receive it with their Thursday or Friday delivery, just in time for the eclipse.

“Earlier in the week we released a few hundred bottles at our country store and those sold out in four hours,” Shatto says.
A few bottles are being saved to sell at the event, but they likely won’t last long with all the interest in everything tied to the eclipse.
“I think for us it is just a great opportunity to let people be part of a working dairy farm for the large part of a day,” Shatto says.
The solar eclipse will reach its “point of greatest eclipse” near Hopkinsville, Kentucky. Just down the road 60 miles east a viewing party will be hosted at the Chaney’s Dairy Barn, the second stop on our tour.

Chaney’s Dairy Barn was started in 2003 as an ice cream parlor and restaurant that is adjacent to the dairy farm still owned and operated by Carl and Debra Chaney. Their family farm dates back to 1888 and has been milking registered Jerseys for 77 years. Today, the Chaneys milk their cows through a robotic unit installed this fall and it is visible to visitors who purchase a tour ticket.
The viewing event at Chaney’s is expected to bring in 500 vehicles. While there are no tickets being sold specifically for the solar eclipse, people who visit the farm can purchase tickets to see the cows and robotic milker in action. Special solar eclipse glasses are being sold by Chaney’s Dairy Barn and there will be plenty of ice cream and food for sale when the eclipse isn’t happening.
A final stop along the solar eclipse dairy tour path ends at Sweetwater Valley Farm near Sweetwater, Tenn. and 75 miles north of Chattanooga.
Sweetwater Valley Farm milks 1,250 cows and has an on-farm cheese processing plant with a store, so the Harrison family is familiar with managing a crowd. However, the amount of people likely to be on the farm for the eclipse will be more than they’ve experienced in the past.
“It has been absolutely crazy. We had no idea the interest there would be,” says Mary Lyndal Harrison. She handles the marketing efforts for her family’s business and is excited about the opportunity to have so many people visit the farm and try their dairy products.  
During the pre-sale there were 900 tickets sold with varying levels based on a private tours of the farm or memorabilia. Additional calls were made for tickets so Harrison is expecting around 1,500 people on the farm.

Fields have been mowed and cattle have been moved from the pastures to help accommodate the people who are in attendance. Some local food trucks will be selling meals to attendees and a few have even inquired about using Sweetwater Valley Farm cheese in the future.
The cheese plant will be running for people to watch from the in-store viewing glass and 90 people will get a private tour of the farm.
“I’m most excited that we’ll be getting people onto the farm. A lot of them have never been here before,” Harrison says.
It should be a boon to the local area and Harrison worked with the Loudon County Visitor's Bureau on promotional gift basket. “Hopefully they’ll come back,” Harrison says of the eclipse farm visitors.
Do you know of any other eclipse viewing events taking place on dairy farms? Feel free to share them with us in the comments.

For more eclipse coverage read the following stories:
10 Things to Know About the August 21 Eclipse
Oregon, Idaho Dairy Industries Preparing for the Eclipse
Note: Wyatt Bechtel will be attending the eclipse viewing event at Shatto Milk Company and will have coverage later online. Let us know where you plan to view the eclipse.