This market update is a www.porkbusiness.com weekly column reporting trends in weaner pig prices and swine facility availability.  All information contained in this update is for the week ended November 10, 2017.

Looking at hog sales in May 2018 using May 2018 futures the weaner breakeven was $67.29, down $0.15 for the week. Feed costs were down $0.56 per head. May futures decreased $0.35 compared to last week’s May futures used for the crush and historical basis is unchanged from last week. Breakeven prices are based on closing futures prices on November 10, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was below average this week with 20,845 head being reported. This is 57% of the 52-week average. Cash prices were $42.64, up $0.69 from a week ago. The low to high range was $25.00 - $50.00. Formula-priced weaners were up $0.99 this week at $41.83.
Cash-traded feeder pig reported volume was below average with 14,705 head reported. Cash feeder pig reported prices were $55.12, up $1.34 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through November 10, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential decreased $0.84 this week to a projected gain of $24.65 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.