A recent national survey by Cargill of some 840 U.S. adults revealed that two-thirds consume dairy products on a regular basis. Of course, that would suggest that the other one-third do not.
Indeed, the survey data showed that 13% of the respondents were “dairy avoiders,” as a result of such factors as lactose intolerance, allergies to dairy proteins or other issues, such as wanting to avoid foods produced with growth hormones or limiting the consumption of saturated fat.
Presumably, a fair number of that 13% segment are purchasing plant-based alternatives to conventional dairy products, with vegan yogurt leading the list of alternatives that consumers are encouraged by both nutritional authorities and animal activists to feel good about when purchasing and consuming.
It’s another variation on the recurring theme that plant-based foods are, say it with me, “Better for you, better for the animals and better for the planet.”
I beg to differ.
Beyond the Marketing Hype
A number of the more popular non-dairy yogurt brands are manufactured using soy milk as the major substrate; others use a formulated almond milk that is artificially flavored and enhanced with gums and stabilizers as the primary ingredient.
But many of the more popular brands rely on cashews and especially coconut as key ingredients, both of which, according to the marketers of such products, are well-suited to produce the “smooth, creamy texture” that yogurt lovers desire.
Such vegan brands as CoYo, So Delicious, Coconut Grove and Forager’s utilize combinations of coconut milk, cashews and various thickeners in their products. For example, Forager’s Creamy Dairy-Free Cashewgurt is made from ground cashews, cassava root and locust bean gum.
What’s wrong with that? The short answer is, the same problems that are associated with bananas, for starters.
The phrase, “banana republic,” is a reference to the domination of the economies of various countries in Latin America to serve the interests of the multinational corporations that appropriated and then decimated vast swaths of rainforests to cultivate the banana plantations responsible for the fruit’s global popularity.
Guess what? Coconut and cashew production have followed similar models, with nearly identically negative results.
Here are the details.
› Coconut. These versatile fruits require a tropical habitat for cultivation. As a result, the leading producers are Indonesia, The Philippines and India, where massive plantations have displaced native rainforests and indigenous tropical vegetation.
It also means that any food product made with coconuts that is consumed in Europe or North America represents an enormous number of “food miles” for that product to reach its intended market, and additional packaging to maintain freshness during transportation, which adds to its carbon footprint.
As for its cultivation, not only does it require rainforest destruction, but the massive monoculture coconut plantations are dependent on heavy use of chemical fertilizers to maintain productivity. That negatively impacts the health of the soil and the workers.
› Cashews. These nuts are also grown solely in tropical climates, with the production leaders being India, Cote d’Ivoire and Vietnam. The same issues of habitat destruction and carbon footprint associated with coconut production also apply to cashews — only there’s an additional serious issue.
During the deshelling process (mostly done by hand), cashews produce a caustic liquid that can burn the skin. In some factories in India, the mostly female work force must wear their own rubber gloves for protection, and according to a lengthy report in the British newspaper The Guardian, not everyone can afford them, and thus they suffer from serious burns and even disfigurement.
The volume of cashews processed in India has grown significantly in the past decade, as African farmers have increased production of the nut to satisfy demand in Europe and North America. So once again, choosing vegan alternatives to dairy products fosters low-wage work among impoverished populations in developing countries, while destroying what’s left of tropical rainforests and the habitats on which so many wildlife species depend.
So, let’s summarize. In becoming a conscientious, enlightened vegan shopper who cares oh-so deeply about the well-being of animals — not so much about people, but at least they’re on the list — you are:

Funding the destruction of tropical rainforests
Responsible for seriously reducing native biodiversity
Ensuring the decimation of endangered wildlife habitat
Accelerating climate change by purchasing products with massive carbon footprints
Supporting the replacement of subsistence agriculture with industrial, plantation-style farming
Enriching global corporations driven only by bottom-line profits
Hurting the viability of American farmers and dairy producers

And did I mention that by patronizing manufacturers of coconut- and cashew-based products you’re perpetuating the virtual enslavement of millions of mostly female workers who earn — at best — poverty -level wages to produce the ingredients for products that you tell yourself are wonderful for you and just terrific for the planet?
Congratulations, veganistas.
You’re not only ignorant, uninformed and utterly hypocritical about your dietary choices, you’re actively supporting the deaths and ultimately the extinction of millions of animals about which you (allegedly) care so much.
Enjoy your non-dairy concoctions as you violate every principle you claim to embrace.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.