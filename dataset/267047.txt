Plowing a path: State to buy farms to help new farmers
Plowing a path: State to buy farms to help new farmers

By JENNIFER McDERMOTTAssociated Press
The Associated Press

PROVIDENCE, R.I.




PROVIDENCE, R.I. (AP) — Rhode Island is launching a program to buy farms and sell them to new farmers for dirt cheap.
A farm bought for $500,000, for example, could then be sold for $100,000. It is an unconventional approach to ensure that farming remains viable.
The National Farmers Union knows of no other state that buys farmland to sell to farmers at less than market price. Other states give tax credits and loans to beginning farmers.
Though some critics say this is not the role of state government, Rhode Island sees it as a way to keep young entrepreneurs from moving to other states, where land may be cheaper. It also could attract other farmers to the state, though retaining farmers who already are here is the main goal and the selection process favors Rhode Island farmers.
"We want these minds, their energy, this entrepreneurial spirit to stay here in Rhode Island," said Ken Ayars, chief of the Rhode Island Department of Environmental Management's agriculture division.
The state does not know exactly how many people are leaving, but officials often hear from the agriculture community that the high price of land is forcing some people out, while others are leasing land because they cannot afford to buy it, Ayars said.
"When we think about the future of agriculture in Rhode Island, this is a weak link," he said.
Sarah Turkus, 30, started the Sidewalk Ends Farm with two other young women in a vacant lot in Providence in 2011. When they wanted to expand three years later, they could not find available, affordable land in Rhode Island. They now lease 2 acres (0.81 hectares) in Seekonk, Massachusetts, to grow vegetables, herbs and flowers.
"This program specifically, or a program like it, would probably be the only way I'd ever be able to own farmland in our area because of the cost of the land," she said.
Under the program, the state will buy a farm at the full appraised value, which takes into account the land's worth if it was developed. The state will then resell the farm at the agricultural appraised value, which is its worth solely as a farm. That is typically 20 percent of the full value, Ayars said. The condition is that it must remain a farm, which the state broadly defines as anything related to the production of agricultural crops or raising livestock.
Rhode Island plans to spend $3 million from the most recent environmental bond approved by voters to buy farmland and development rights.
The value of all land and buildings on farms nationwide averaged $3,080 per acre for 2017, according to the United States Department of Agriculture. The farm real estate value in Rhode Island was the highest in the nation at $13,800 per acre.
Rhode Island plans to solicit applications for its new program within 45 days. The National Farmers Union, which represents farmers, fishers and ranchers with divisions in 33 states, is typically wary of anyone other than a family farmer owning farmland, said Thomas Driscoll, director of conservation policy.
But, Driscoll said, property values may be getting so high along both coasts that unconventional solutions are needed to ensure food production.
"We want to see how this operates in practice and how it affects farmers who are farming there already, but ultimately access to land is a major problem for farmers and we're glad that people are thinking creatively about it," he said.
Some Republican state lawmakers and established farmers in Rhode Island have said that buying and selling farmland is not the state's role.
Glen Cottrell owns Cottrell Homestead, a 118-year-old dairy farm that is going out of business. He fears this is a roundabout way to seize property and divide it.
"That's what the communists did," he said.
Ayars said the state will only own the land long enough to transfer it from one farmer to another, and a farmer's decision to participate is completely voluntary.