The Northwest fresh pear crop estimate for 2016 has been bumped up because of expected strong quality and sizes.
 
The Northwest pear industry is expecting the fresh pear crop to total 18.89 million standard boxes, down 6% from the five-year average but 3% above last year, said Kevin Moffitt, president and CEO of the Pear Bureau Northwest. The August crop estimate went up about 1% compared with an earlier season forecast, he said.
 
The revised fresh forecast, Moffitt said in an Aug. 12 e-mail, is likely due to larger fruit size this season and an expected higher fresh packout for bartletts.
 
The Northwest winter pear crop - consisting of mainly of anjou, bosc, red anjou, comice, seckel, concord, forelle - is forecast to total 14.04 million cartons, unchanged from last year and 9% below the 2015 winter pear crop.
 
Meanwhile, summer/fall pear varieties - green bartlett, red bartlett and starkrimson - are expected to total 4.85 million cartons, up 11% from 2015 and 1% above the five-year average.
 
The early August fresh crop estimate showed anjou production at 9.62 million cartons, unchanged from last year but down 11% from the five-year average. Green bartlett output was rated at 4.45 million cartons, up 11% from 2015 and up 1% from the five-year average.
 
Bosc output was rated at 3.01 million cartons, down 1% from last year and 2% below the five-year average. 
 
Red anjou production is pegged at 1.02 million cartons, up 6% from last year and unchanged from the five-year average.