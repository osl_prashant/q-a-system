Texas oilman and philanthropist T. Boone Pickens has listed his Mesa Vista Ranch for sale. The price tag $250 million.
Mesa Vista Ranch is 64,809 acres and located in the Texas Panhandle near Pampa and 80 miles northeast of Amarillo. At the current listing the property would sell for approximately $3,857.
Pickens acquired the original property in 1971, starting with just 2,926 acres. As he continued to find success in the oil industry and through investments he added more surrounding property.
“Selling the ranch is the prudent thing for an 89-year-old man to do,” Pickens shares in the brochure announcing the sale on Nov. 29. “There are many reasons why the time is right to sell the ranch now, not the least of them ensuring that what I truly believe is one of the most magnificent properties in the world winds up with an individual or entity that shares my conservation beliefs.”
Mesa Vista Ranch was developed by Pickens to provide habitat for wildlife, primarily quail. Other wildlife found on the ranch include whitetail deer, mule deer, aoudad, turkey and dove.  
Livestock are part of the current ranch with Pickens running between 400-500 cows. There are some areas of the ranch that have not been grazed in 20 years or more. 
There are 14 center pivots on the ranch that can be farmed, too. 
Today, the ranch includes an airport with a hangar, 40 pen dog kennel featuring a veterinary lab, a two hole golf course, a chapel and a pub.
The main home on the property called “The Lodge” is more than 25,000 square feet in size, complete with a wine cellar, 30 seat theater and 10,000 square feet in porches and patios. At least three other homes are listed ranging in size from 2,300 square feet to 11,500 square feet.



Nothing compares to seeing it in person, but here’s some great drone footage of 65,000 acres of rolling hills, extensive waterways, incredible architecture, and the best quail hunting in the world. #MesaVistaRanch https://t.co/ma5kL5hJzf pic.twitter.com/uzkpDM6j7J
— T. Boone Pickens (@boonepickens) December 4, 2017



Pickens is selling Mesa Vista Ranch nearly “turnkey” with trucks, farming equipment, bird dogs and furnishings being included in the sale. Exclusions would be personal items. Pickens would be willing to sell livestock separately along with much of his art collection.
The sale of the ranch will go towards funding the T. Boone Pickens Foundation which supports a number of organizations like American Red Cross, Big Brothers Big Sisters, Park Cities Quail, USO Dallas/Fort Worth and the Institute for Economic Empowerment of Women.
In 2013, Pickens’ net worth was listed at $1.2 billion by Forbes. Since then his wealth has decreased to $500 million largely because he has donated it to others.
“I see this sale as a new beginning — for the Mesa Vista's new owners and for the recipients of my charitable giving,” Pickens says.
An interactive map of the property can be viewed here.
For a look at the ranch in 2012 watch the following video: