The Corn Belt is currently experiencing a historically cold April, which is delaying planting and will likely impact yields. Luckily, once April leaves, meteorologists expect the remainder of the growing season to be under favorable weather conditions.
“At the moment our general thinking is that we’re going to be pretty close to normal from a rainfall and temperature perspective but there’s always going to be a yield drag because of the late start we’re getting now,” Mike Tenurra of T-Storm Weather explained to Chip Flory on AgriTalk After The Bell. “I don’t think there’s any real strong signals that we have a problematic growing season coming in.”
His prediction of favorable weather rides on two things: La Nina ending and historical data.
While the year started in a La Nina weather pattern, it is quickly ending and turning to an El Nino Southern Oscillation (ENSO) neutral weather pattern, which will mean the majority of the growing season won’t be influenced by La Nina or El Nino.
“When we start in La Nina and end up in a neutral event, we’ve only had one or maybe two bad corn and soybean crops,” he explains. “Historically, it’s unlikely anything unusual is about to happen.”
In addition, if you look at historically cold Aprils, the remainder of the growing season has not experienced drought or lack of rainfall.
“When [extremely cold Aprils] happened in the past, even though corn yields have been compromised, the weather didn’t really point in the direction of hot and dry conditions,” he explained. 

The Short-Term Forecast
Tenurra expects temperatures to start warming up in 10 to 15 days. At this point his biggest cause for concern is a storm system expected to develop around April 25 which will likely result in rain the last 3-5 days of the month.
“That will probably take on more importance as it gets closer because we need it to stay dry and we need to stay warm if we’re going to get all this corn planted in a timely manner,” Tenurra explained.
The good news is that historically cold Aprils often lead to mild Mays, he said.
“But when you look at those 20 real cold Aprils they were followed by very warm Mays,” he explained. “The only time it’s not true, is when you look at the five coldest.”
For that reason he’s cautious to forecast really warm weather next month. 
“I want to be telling everyone the statistics point to a really warm May, but it’s been so cold now that the statistics start to break down a little bit,” he said.