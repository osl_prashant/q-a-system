As sweet onions start to ship out of Walla Walla, Wash., the season appears to have returned to normal conditions and timing, compared to 2016.Dan Borer, general manager in the Walla Walla office of Greencastle, Pa.-based Keystone Fruit Marketing Inc. said Walla Walla fresh onions should ship through the end of August.
“We’re looking at a fairly normal season,” Borer said. “Weather was cooler than normal — the typical June high is 80 degrees. Through mid-June, the high has been more in the mid-70s. There’s also been more precipitation than normal but it hasn’t been detrimental yet. It will actually help the onions bulk up.”
Last year, Walla Walla sweet onions had an early start and finish to the season. However, this season will have more normal timing.
Pricing should be normal in the high teens to $20 for a 40-pound carton, Borer said.
“Demand for sweet onions is trending upward as the No. 1 mover in the onion category and availability 365 days a year,” he said.
Michael Locati, president of Mike Locati Farms Inc., Walla Walla, and co-owner of Pacific Agra Farms LLC, said volumes of the company’s sweet onions look to be comparable to previous years and should begin shipping mid-June.
Locati said Pacific Agra Farms and sister company Walla Walla River Packing & Storage work closely with Keystone Fruit Marketing to develop promotional strategies.
Market conditions are average due to large sweet onion crops in Georgia and California, he said. Also, carryover stocks from a large 2016 year have suppressed overall onion prices.
Washington has experienced large increases in labor costs, Locati said. A major factor contributing to this is the state increasing its minimum wage to $11 an hour starting Jan. 1.
“Not to mention the trouble finding labor, but the increase in average minimum wage has put pressure on producer margins,” Locati said.
Harvest is coming later than last year but this is more normal timing for the season.
“Last year came on early, had excellent yields and quality. We expect similar yields but a later start,” he said.
Brooks, Ore.-based Curry & Co. president Matt Curry said the company is finishing the previous season with an abundance of storage onions as they head into the 2017 harvest.
“We continue to receive demand on sweet onions,” he said. “A somewhat wet start to the Vidalia season slowed shipments at first, but we have been full speed ahead since, and are now getting requests for Walla Walla sweets also.”
The onion market continues to be volume exceeding demand, Curry said.
“There are a lot of onions available, and new crop is right around the corner. Pricing is aggressive, and we are trying hard to create movement to make room for new crop when the season begins.”
Locati said retailers should focus on the uniqueness of Walla Walla sweet onions in their promotions, and also the heirloom background and seasonality of the product that makes it special to the region.