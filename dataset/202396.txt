Alliances between container shipping lines appear to be under scrutiny by the antitrust division of the U.S. Department of Justice, with media reports indicating that several top executives from the industry have been subpoenaed to testify.
 
Companies receiving subpoenas from the Department of Justice include Denmark's A.P. Moller-Maersk and Hong Kong-based Orient Overseas Container Line, according to Reuters.
 
The Journal of Commerce reported U.S. investigators delivered the subpoenas to CEOs of major container lines at an industry meeting in San Francisco.
 
The move comes several months after the DOJ raised concerns about new proposed shipping alliances in a Nov. 22 letter to the Federal Maritime Commission. It is not certain if the subpoenas related to the shipping alliances; the department declined comment to several media organizations. 
The November letter from the Department of Justice said the proposed THE Alliance Agreement - involving Hapag-Lloyd, Yang Ming, Hanjin, Mitsui OSK Lines, NYK Line, "K" Line and United Arab Shipping - raises significant competitive concerns, particularly in view of the recently approved OCEAN Alliance. The OCEAN Alliance includes CMA-CGM, Cosco/China Shipping, Evergreen and OOCL.
 
"The creation of these two new alliances will result in a significant increase in concentration in the industry as the existing four major shipping alliances are replaced by only three," Renata Hesse of the DOJ said in the letter. Hesse at the time was acting assistant attorney general.
 
"This increase in concentration and reduction in the number of shipping alliances will likely facilitate coordination in an industry that is already prone to collusion," according to the letter.
 
She stated in the letter that four companies - three of which are set to join THE Alliance - have pleaded guilty in connection with a global conspiracy involving price fixing, bid-rigging, and market allocation among providers roll-on, roll-off shipping.
 
The letter said that over the last several years, 16 of the top 20 global liner carriers combined into four alliances that serve the North American trade lanes. Ocean carriers now seek to realign into three alliances comprised of 13 carriers beginning in April, the letter said.
 
Following the proposed alliance realignment, the letter said the 2M Alliance (Maersk, Mediterranean Shipping and Hyundai Merchant Marine) will control about 30% of worldwide container capacity, with the OCEAN Alliance with approximately 25% and THE Alliance 20%.