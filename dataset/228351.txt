BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Thursday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeFeb        13.40   13.74   13.40   13.71  Up   .33Mar                                13.58  Up   .31Apr        13.28   13.63   13.28   13.58  Up   .31May                                13.67  Up   .24Jun        13.43   13.71   13.42   13.67  Up   .24Sep        13.82   14.04   13.80   14.00  Up   .17Dec                                14.76  Up   .13Feb        14.60   14.81   14.60   14.76  Up   .13Apr        14.69   14.82   14.65   14.77  Up   .11Jun        14.74   14.81   14.67   14.79  Up   .10Sep        14.96   15.01   14.92   15.00  Up   .11Dec                                15.42  Up   .11Feb        15.42   15.44   15.41   15.42  Up   .11Apr        15.36   15.37   15.35   15.35  Up   .09Jun        15.33   15.34   15.32   15.32  Up   .09Sep        15.47   15.49   15.46   15.47  Up   .11