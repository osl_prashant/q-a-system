Many North Dakota, Montana, and South Dakota livestock producers are buying hay and feed from outside the region because drought resulted in pasture, hay and other feed shortages in the upper Great Plains.The North Dakota State University Extension Service and North Dakota Stockmen’s Association remind producers to ask questions and use sound business practices to protect themselves and their livestock as they make purchases.
“Hay, stover and other feeds are coming from around the country, and the quality of those feeds can vary considerably,” NDSU Extension beef cattle specialist Carl Dahlen says.
Here are some questions producers should ask so they have a clear understanding of what they are buying:
What year was the product harvested?
Are bales round or square?
What is the average bale weight?
What grass/plant species are included in the bales and at what approximate percentages?
Are noxious weeds in the area where the bales originated? What are the approximate amounts of the weeds in the bales? At what stage of growth were the weeds cut?
Were the bales from the first, second, third or subsequent cutting?
How is the hay bound (plastic twine, sisal twine, net wrap, etc.)?
How and where was the hay stored?
Is any mold visible on the hay?
What is the nutrient content of the feed? Knowing the nutrient profiles of hay, stover, grains and byproducts helps producers make informed decisions.
Are ergot or other antiquity factors present in the grains or byproducts? If so, the feed needs to be tested to determine if or how it can be fed to cattle.
“If feasible, producers may want to look at the hay before making a purchase,” says Julie Ellingson, executive vice president of the North Dakota Stockmen’s Association.
Tim Petry, NDSU Extension livestock economist, suggests livestock feed buyers, sellers and haulers have written contracts.
Contracts should include details such as the names of the parties, the price of the commodity, the terms of the agreement, transportation details and signatures.
He also urges producers to beware of payment scams in which buyers are being asked to pay by direct deposit only and sellers are not available to answer questions about the feed.
“Be cautious of anything in your communication with potential sellers that seems out of the ordinary,” Petry says.
Transportation can be a significant component of the hay cost. Producers should know if the transportation is included in the price of the hay.
Here are other transportation questions producers should ask:
Is the cost per mile roundtrip or only per loaded mile?
What equipment will I need to unload the truck?
Will the trucking be paid separately from the hay?
“People are inherently good, but there may be opportunists who seek to take advantage of people in desperate or serious situations,” Ellingson says. “Miscommunication also can lead to problems, so making sure agreements and arrangements are qualified in writing is important.”
Dahlen adds: “Drought conditions have brought about serious management and financial considerations to the table, and we hope that everyone making feed purchases receives the products they thought they were buying.”