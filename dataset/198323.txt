Editor's note: The following article appeared in the January 2016 issue of Dairy Herd Management.Starting in 2015, farmers have an Affordable Care Act reporting requirement if they have 50 or more employees or have less than 50 employees and a self-funded healthcare plan.
As an applicable large employer (ALE), you are required to file the appropriate forms by Jan. 31, 2016, to employees, and to the IRS by Feb. 29, 2016, if you paper file or March 31, 2016, if you e-file, even if you don't offer insurance.
Businesses are considered an ALE if they have 50 or more fulltime equivalent employees. What does that mean? Adam Kantrovich with Michigan State University Extension said fulltime equivalent employees include not only those people who work an average of 30 or more hours each week, but also the total hours your parttime employees work divided by 30. For example, if you have three part-time employees who each work 15 hours a week, that is 1.5 full-time equivalent employees.
"The Mid-Size Transition Relief basically says farmers will not owe any penalties if they meet certain requirements like having 50-99 FTEs without reducing the size of their workforce after Feb 9, 2014, unless it's for a bona fide business reason," said Tonya Rule, senior tax manager with accountancy Eide Bailey.
While farmers meeting these standards may get out of penalties, they don't get out of filing the appropriate forms.
"There is a huge misconception out there that employers meeting this relief do not have to file the forms," she said. "The forms are still required - the relief just gets those employers out of the 'pay or play' penalties."
It's important to note you must provide each fulltime employee with a copy of the 1095-C by Jan. 31, 2016. Rule said this helps the IRS to determine which employees are ineligible for a premium tax credit on their individual return.
The penalty for not filing the appropriate forms could add up.
"The penalties range from $50-$250 per 1094-C and 1095-C (and 1094-B and 1095-B)," Rule said. "The penalties are capped at $3 million for businesses with average annual receipts of greater than $5 million. For everyone else, the penalties are capped at $1 million."
While penalties will be waved for employers acting in good faith this first year, Rule said filing the forms late exempts you from that waiver.
"Don't forget penalties are not deductible," she said.

Talk to your accountant
Rule recommends farmers ask the following questions of their accountant:
‚Ä¢ Am I considered to be an ALE?
 If I am, does the Transitional Relief for determining this apply to me?
‚Ä¢ If I still am an ALE, does the Mid-Size Employer Transition Relief apply to me?
‚Ä¢ If I'm close to 50 FTEs, what should I do different for 2016? Note: the ALE calculation is always done on the previous year's hours so employers have December 2015 to make adjustments for their 2016 reporting.

‚Ä¢ If I have a reporting requirement, will you prepare the forms? If not, who does?