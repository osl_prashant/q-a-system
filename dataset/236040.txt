US judge blocks weed-killer warning label in California
US judge blocks weed-killer warning label in California

By SUDHIN THANAWALAAssociated Press
The Associated Press

SAN FRANCISCO




SAN FRANCISCO (AP) — A U.S. judge blocked California from requiring that the popular weed-killer Roundup carry a label stating that it is known to cause cancer, saying the warning is misleading because almost all regulators have concluded there is no evidence that the product's main ingredient is a carcinogen.
U.S. District Judge William Shubb in Sacramento issued a preliminary injunction on Monday in a lawsuit challenging the state's decision last year to list glyphosate as a chemical known to cause cancer.
The listing triggered the warning label requirement for Roundup that was set to go into effect in July.
Glyphosate is not restricted by the U.S. Environmental Protection Agency and has been widely used since 1974 to kill weeds while leaving crops and other plants alive.
The International Agency for Research on Cancer, based in Lyon, France, has classified the chemical as a "probable human carcinogen." That prompted the California Office of Environmental Health Hazard Assessment to add glyphosate to its cancer list.
Shubb said a "reasonable consumer would not understand that a substance is 'known to cause cancer' where only one health organization had found that the substance in question causes cancer."
"On the evidence before the court, the required warning for glyphosate does not appear to be factually accurate and uncontroversial because it conveys the message that glyphosate's carcinogenicity is an undisputed fact," he said.
Sam Delson, a spokesman for the state's office of environmental health hazard assessment, noted that the judge did not block the state from putting glyphosate on its cancer list, but only from requiring the warning label.
"We are pleased that the listing of glyphosate remains in effect, and we believe our actions were lawful," he said.
He said the office had not decided yet whether to appeal the ruling on Monday.
The ruling came in a lawsuit filed by the national wheat and corn growers associations, state agriculture and business organizations in Iowa, Missouri, North Dakota and South Dakota, and a regional group representing herbicide sellers in California, Arizona and Hawaii. The plaintiffs also include St. Louis-based Monsanto Co., which makes Roundup.
"Every regulatory body in the world that has reviewed glyphosate has found it safe for use, and no available product matches glyphosate with a comparable health and environmental safety profile," Chandler Goule, CEO of the National Association of Wheat Growers, said in a statement.
The lawsuit will continue with Shubb's injunction in place.