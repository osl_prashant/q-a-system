BC-USDA-Natl Sheep
BC-USDA-Natl Sheep

The Associated Press



SA—LS850San Angelo, TX    Wed Apr 11, 2018    USDA Market NewsNational Sheep Summary for Wednesday, April 11, 2018Compared to last week at Sioux Falls, SD no trends due to the lightreceipts last week.Slaughter Lambs:   Choice and Prime 2-3 90-150 lbs:Sioux Falls:    shorn and wooled 121 lbs 158.00; 135-175 lbs 148.00-156.00.Virginia:       no test.Slaughter Lambs:  Choice and Prime 1-2:Sioux Falls:    59 lbs 230.00; 64 lbs 205.00; 80 lbs 215.00.Virginia:       no test.Slaughter Ewes:Sioux Falls:    Good 3-4 (very fleshy) 49.00-55.00; Good 2-3 (fleshy)50.00-58.00; Utility 1-2 (thin) 45.00-56.00; Cull 132.50-39.00.Virginia:       Good 2-4 no test.Feeder Lambs:   Medium and Large 1-2:Sioux Falls:    50-60 lbs 242.50-245.00; 60-70 lbs 240.00-250.00; 97lbs 220.00; 100 lbs 187.50; 134 lbs 177.50.Virginia:       no test.Replacement Ewes: Medium and Large 1-2:Sioux Falls:    no test.Sheep and lamb slaughter under federal inspection for the week to datetotaled 23,000 compared with 22,000 last week and 31,000 last year.Source:  USDA Market News Service, San Angelo, TXRebecca Sauder 325-653-1778www.ams.usda.gov/mnreports/SA—LS850.txtwww.ams.usda.gov/LSMarketNews1600  rs