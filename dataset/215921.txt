Robert M. Thompson, a partner with the Kansas City office of the Bryan Cave law firm, was elected chairman of the Agricultural Business Council of Kansas City at the group’s annual meeting held December 7.  Greg Krissek, CEO of the Kansas Corn Growers Association and Kansas Corn Commission, was elected vice chairman of the Council. 
Thompson serves as co-leader of Bryan Cave’s Food and Agribusiness Industry Group.  He served as managing partner of the firm’s Kansas City office from 2006-2012.  He is a native of Nevada, Missouri, where he and his family have an active livestock and crop business.  He holds a law degree from the University of Missouri.  He has been active in numerous civic organizations, including the American Royal.
Krissek, a native of Kansas City, Kansas, has over 24 years’ experience working with agriculture and ethanol sectors.  Prior to being named CEO of Kansas Corn in 2014, his previous stints included executive positions with Kansas Department of Agriculture, ICM, Inc., and Kennedy and Coe. He earned his law degree and MBA from the University of Denver.
Thompson extended his thanks to several outgoing leaders who have played an influential role in the Council’s activities through the years.  Those include Cliff Becker, outgoing Council Chairman and vice president, Farm Journal Media, as well as to outgoing long-time board members Jim Gray, Amber Spafford, and Steve Taylor.
Bob Petersen, Council Executive Director, noted that Becker, VP of Farm Journal’s Livestock Division which includes Drovers, Dairy Herd Management, MILK and PORK, has played a key role as chairman over the last two years.  “Cliff Becker brought his enthusiasm for agriculture to this role and made all the rest of us better at what we did.  He is a great leader, a wonderful advocate for the agricultural sector, and a ‘doer’ – he makes things happen and we are very appreciative of his efforts.”
The Council’s Board of Directors is composed of 20 persons who serve staggered two-year terms.  Those elected to the Board for a two-year term commencing January 1, 2018 were:

Lee Blank, GFG Ag Services
Ben Breazeale, Cargill
Brad Garrison, Alpha Gamma Rho
Garrett Hawkins, Missouri Dept. of Agriculture
Terry Holdren, Kansas Farm Bureau
Dustin Johansen, Osborn Barr
JJ Jones, Roots & Legacies Consulting
Jackie McClaskey, Kansas Dept. of Agriculture
Kristen Parman, Livestock Marketing Association
Lynn Parman, American Royal Association