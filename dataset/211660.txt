Crook Bros. of Beckley, W.V., has acquired Charleston, W.V.-based Corey Bros.The terms of the deal, effective Aug. 31, were not released.
“The transaction will immediately give Crook Bros. a greater ability to service West Virginia and its surrounding five-state region where national contracts can be better served,” Kenneth Crook, president of Crook Bros., said in a news release.
Corey Bros. will operate as a standalone Crook Bros. subsidiary, according to the release, with business continuing as usual and with management continuing as before the acquisition.

All Corey Bros. procedures, routes, personnel and billing procedures will continue as before, according to the release.
Bob Corey, previous owner of Corey Bros., will remain during the transition and lend counsel to both companies.
Crook Bros., led by Kenneth and Keith Crook, will lead efforts with Corey Bros. management to plan future growth of the combined companies, according to the release.
Corey Bros.’ produce roots go back to 1895 and recognitions include being a Produce for Better Health Champion Company.
Crook Bros. was founded in 1989, with the family’s involvement in the produce industry beginning decades earlier, the release said.