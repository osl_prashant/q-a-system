Recent editorials published in Nebraska newspapers
Recent editorials published in Nebraska newspapers

By The Associated Press
The Associated Press



Omaha World Herald. March 30, 2018
Input from all sides helps NU as it decides its free-speech policies
The University of Nebraska is in the middle of stepped-up discussions about free speech on campus. The issue is a fundamental one for university life, deserving robust examination and input from all sides.
The issue, not surprisingly, is generating complications that NU administrators, faculty and students need to think through and address.
What parts of campus, for instance, should be spaces where people are permitted to hold demonstrations on issues of importance to them? Campuses can't be entirely open in that regard; classrooms and laboratories, for example, aren't areas for mass protests. Sensible regulations are appropriate.
Some faculty members and two organizations — the Academic Freedom Coalition of Nebraska and the American Civil Liberties Union of Nebraska — say some draft policies for NU buildings and grounds are too restrictive, failing to designate locations such as sidewalks and greenspaces as public spaces.
Such criticism serves a positive purpose. NU needs to take the concerns into consideration. And such input is, in fact, what NU leaders are encouraging. The discussion is still in the early stage, and input on the public-space issue is welcome and valuable.
Wide-ranging input will be needed, too, for another complication: how to promote free expression and debate while keeping exchanges from devolving into insults or harassment?
Striking that balance can be difficult because of our country's political culture. It too often encourages people to demand free expression for one's political camp — but to be intolerant when faced with an opposing view.
For proof, look at social media or watch cable-TV political chatter, and see how rudeness and a thin-skinned attitude about disagreement are constantly reinforced.
Express a conservative viewpoint and be labeled a Nazi. Voice a liberal viewpoint and be branded as un-American.
In such an environment, debate with one's political opposites is considered beside the point because it's claimed that either a) they're morally deficient people, undeserving of respect, or b) all the facts line up totally, all the time, for one's own point of view and never for the other side. Then there's the rudeness factor: Hurling nasty, supposedly clever insults at one's political opposites routinely wins cheers and encouragement.
Our college campuses need to take a stand against these negatives and instead cultivate a responsible approach toward political discussion. They should encourage vigorous debate and safeguard the right of students and faculty to express their beliefs grounded in values they hold dear.
At the same time, political expression needs to be responsible, with no disparagement or harassment of individuals due to their political beliefs. Ideological tolerance on campus is imperative. Otherwise, students and faculty will feel pressure to remain silent about their own political expression even as others feel at liberty to express theirs. A public university should guard against creating such an unjust campus atmosphere.
Can NU campuses achieve that proper balance? That's what the current discussions are all about. Input from all sides is vital as the state's public university decides the best ways to move forward on the issue.
_______
The Grand Island Independent. March 27, 2018.
Public transit boost should be a priority
Public transportation has been a topic of discussion for the Grand Island City Council since the city took over operation of what used to be Hall County Public Transportation in July 2016.
Currently, the city has a fleet of vehicles that go out to pick people up as they call for a ride and take them where they need to go.
But the question has been whether this type of public transportation is adequate for Hall County, with a population of more than 22,000 households, including 1,370 households that have no vehicles to provide their own transportation.
Last week the City Council was updated on the results of a transit study that has been completed by Olsson Associates to assess the public transportation need in the area and the feasibility of the city meeting that need.
The final report shows that Olsson Associates was very aware of the city's budgetary constraints as it drafted recommendations based on all the data and public input it had collected. Its recommendation for the next five years is a "Fiscally Constrained Plan" that calls for maintaining the status quo with the current demand response service and increasing marketing efforts to build ridership, but adding van pool services in coordination with Enterprise and a ride share program. It also recommends a planning study for intercity bus service to and from Kearney and Hastings.
The intercity bus service is included in this plan because it could be almost totally funded through federal and state grant funds. The main costs to the city would be 20 percent of the signage and shelter expenses and 10 percent of the vehicle cost.
That is a very economical proposal worth investigating to see if there would be enough demand to warrant it. The study presented last week indicates there would be interest in intercity transportation.
The City Council needs to remember the reason this study was conducted, at a cost of more than $150,000, and recognize the importance of following through with the recommendations. There should be a concrete priority of increasing the public transportation available to their constituents.
The report estimated an annual transit need of 863,100 rides in Hall County and said the current system is only meeting 4 percent of that need. Six other cities, including Casper, Wyo., Enid, Okla., and North Platte, were compared, with Grand Island coming in last. North Platte's public transportation also falls short in comparison to some of them, but it is meeting 13 percent of its need.
Four percent is not where Hall County should be. There's no disputing that. But the only way the percentage will increase is for the City Council to prioritize it and carry through.
The study includes an implementation plan, with a service-contract bid set to be released for bids sometime this year, a branding document to be finished this spring and a request for proposal (RFP) for the ride share app and van pool services set to be designed later this year or early next year. A RFP for intercity bus service is being designed and should be ready within a few months.
The recommendation for branding and other marketing efforts to get more people using what's available seems to be a first step and should be followed through on right away. This is not a place for the city to count pennies. Marketing is an important early step and as more people use the current service, the city needs to be ready to move forward with increased service during the next couple of years.
___
Lincoln Journal Star. March 27, 2018
Speed limit compromise aids rural highways
As debate accelerated regarding the merits of increasing Nebraska's interstate speed limit to 80 mph, the arguably more important provisions of the bill sat parked away from the spotlight.
The measure required a state study to determine if Nebraska's interstate infrastructure could adequately handle 80 mph interstate speeds, a key means of why the Journal Star editorial board favored the original legislation. To secure the bill's advancement, however, an amendment axed its most buzzworthy and controversial piece.
Even with that provision gone, though, another element we support lives on - improved transit on rural state highways.
The compromise struck by senators creates a new classification for these roads called "super-two" highways. These will be differentiated from standard state highways by their intermittent passing lanes in both directions, which increase the ability of motorists to pass slow-moving vehicles.
Few of these exist in Nebraska, with short stretches of Nebraska 92 between Wahoo and David City and Nebraska 50 near Weeping Water representing potential models to expand upon. But neighboring states, including Iowa, use passing lanes extensively on major, non-interstate highways.
Nebraska drivers depend on the state's network of highways to attend work and school. Those same routes move large numbers of agricultural implements, 18-wheelers and other large trucks, vehicles that power the state's economy - but tend to travel far more slowly than a passenger car.
Opening up major routes with these sporadic passing lanes would help provide the rest of Nebraska with the transportation advantages enjoyed by those with interstate access.
Nebraska contains nearly 500 miles of interstate highways - but that pales in comparison to its roughly 10,000 miles of other highways.
Of the 16 Nebraska cities with more than 10,000 residents, six have no direct interstate access in their counties. Only five of the 15 communities with populations between 5,000 and 10,000 sit along Interstate 80. The rest of the state - including the vast majority of its land area - must rely almost exclusively on two-lane highways for transit, travel and commerce.
This bill still streamlines the ability of local authorities and the state to increase the speed limit on a variety of state highways.
These highways can vary in speed, seemingly without rhyme or reason to drivers, because state law currently requires a study to increase highway speeds from 60 mph to 65 mph. As Nebraska Department of Transportation Director Kyle Schneweis said recently on the Journal Star's Fresh Pressed podcast, this helps promote more uniformity on speed limits.
So, Nebraska drivers may soon get their chance to speed up - just not on I-80. But the remainder of the bill offers significant upgrades to far more miles of highways in the state.
______
Kearney Hub.  March 30, 2018
Korean trade pact a dose of sanity
A rose to ... trade negotiators, who announced this week they have forged a new agreement with South Korea. The results will mean a welcome infusion of stability for Nebraska agriculture. Recently, our state's farmers and ranchers have been anxious about the potential negative consequences of imposing tariffs on imported steel and aluminum. Agricultural products often are the pawns of international trade wars. In that regard, Nebraska farmers have a lot to lose if the steel and aluminum tariffs backfire and foreign trade partners cut off their dealings in crops and livestock from our state.
Thankfully, the United States-Republic of Korea Free Trade Agreement (KORUS) will continue into the future, even though Trump had withdrawn the United States from the agreement.
That's terrific financial news for Nebraska's farmers and ranchers. The KORUS agreement was worth roughly $340 million to our state in terms of total ag exports in 2016, according to the Nebraska Farm Bureau Federation. To put the numbers into a more personal perspective, the KORUS agreement is worth $34.35 cents per-head to Nebraska beef producers and $11.52 cents per-head for the state's pork producers.
A raspberry to ... government red tape. Rules and regulations really can ruin a person's day, but that's no longer the case for Nebraskans who provide equine massages. Spurred on by the Platte Institute, Nebraska lawmakers are pressing forward with LB596, a measure that will remove the requirement for equine massage therapists to register with the state. Lawmakers enthusiastically expanded the no-registration amendment to include massages for dogs and cats.
Receiving a massage can be so relaxing, regardless if you're walking on two legs or four. And now, with registration regulations going out the door, massage therapists can relax a bit as well.
A raspberry to ... discourteous drivers, especially those who have never discovered the turn signal lever on their steering column. If they were to reach forward with their left hand, they might find the lever and put it to use. Think of the joy they'll create for others by signaling their intentions. They'll no longer cause other motorists to wait needlessly because they approach corners and then suddenly turn without signaling.
If you're not using yours, watch how courteous drivers use them and join the club.
___