This market update is a PorkNetwork weekly column reporting trends in weaner pig prices and swine facility availability. All information contained in this update is for the week ended January 15, 2016.Looking at hog sales in July 2016 using July 2016 futures, the weaner breakeven was $60.51, up $1.97 for the week. Feed costs were up $0.98 per head. July futures increased $1.45 compared to last week's July futures used for the crush. Breakeven prices are based on closing futures prices on January 15, 2016. The breakeven price is the estimated maximum you could pay for a weaner pig andbreakeven when selling the finished hog.

For the Week Ended


1/15/2016


Weekly Change


Weaner pig breakeven


$60.51


$1.97


Margin over variable costs


$82.34


$1.97


Pig purchase month


Jan, 2016





Live hog sale month


July, 2016





Lean hog futures


$78.30


$1.45


Lean hog basis/cwt


($1.47)


$0.00


Weighted average sbm futures


$274.22


$3.39


Weighted average sbm basis


$0.96


$0.82


Weighted average corn futures


$3.70


$0.07


Weighted average corn basis


($0.25)


$0.00


Nursery cost/space/yr


$35.00


$0.00


Finisher cost/space/yr


$40.00


$0.00


Feed costs per head


$71.30


$0.98


Assumed carcass weight


205


0

Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations don't take into account market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash traded weaner pig volume was above average this week with 39,972 head being reported which is 126 percent of the 52-week average. Cash prices were $59.42 up $1.88 from a week ago. The low to high range was $41.00 to $65.00. Formula priced weaners were up $1.35 this week at $43.73.
Cash traded feeder pig reported volume was below average with 8,995 head reported. Cash feeder pig reported prices were $68.57 up $3.95 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through January 15th.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential increased $0.09 this week to $0.09 per head.

Casey Westphalen is general manager of NutriQuest Business Solutions, a division of NutriQuest. NutriQuest Business Solutions is a team of leading business and financial experts that bring years of unparalleled experience in the livestock, grain, poultry and financial industries. At NutriQuest our success comes from helping producers realize improved profitability and sustainability through innovation driven by a relentless focus on helping producers succeed. For more information please visit our website at www.nutriquest.com or email casey@nutriquest.com.