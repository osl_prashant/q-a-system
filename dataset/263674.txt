Germany said on Thursday any escalation of U.S. President Donald Trump’s tariffs on metal imports into a full-blown trade war could cause tangible damage to the global recovery, although the tariffs themselves should have only a limited effect.
Trump last week ordered the imposition of duties on incoming steel and aluminum and threatened to levy a tax on European cars if the European Union did not remove “horrific” tariffs and trade barriers on a range of goods.
“The German economic upswing is continuing at the beginning of 2018. The global economic environment is still favourable,” the Economy Ministry said in its monthly report. But it said U.S. trade policies were creating a sense of uncertainty.
The tariffs on steel and aluminum will affect trade flows in some regions, but their overall implications for the global economy are likely to be manageable, it said.
“But a possible escalation into a trade war and rising uncertainty among market participants could cause tangible damage,” it added.
European Council President Donald Tusk on Wednesday urged the United States to revive trade talks rather than escalate a dispute over tariffs on metals and cars.
Germany’s transatlantic coordinator Juergen Hardt also pushed for a quick resumption of negotiations between Brussels and Washington on a free trade agreement, known as TTIP.
“It was a big mistake not advancing this,” Hardt told lawmakers in the Bundestag lower house of parliament.
Germany favors the abolition of all tariffs and other forms of trade barriers between the United States and the EU, Hardt said, adding: “Our offer is TTIP and (U.S. Trade Representative Robert) Lighthizer is invited to come back to Brussels soon.”
German Finance Minister and vice-chancellor Olaf Scholz is sworn-in by Parliament President Wolfgang Schaeuble in Germany's lower house of parliament Bundestag in Berlin, Germany, March 14, 2018. REUTERS/Kai Pfaffenbach
“At a Crossroads”
Germany’s new economy minister, Peter Altmaier, said Trump was challenging the multilateral trade system as defined by the rules of the World Trade Organisation (WTO).
“We are at a very important crossroads,” Altmaier said, warning of a scenario in which countries could start a spiral of tit-for-tat trade restrictions.
“This is a really huge challenge with implications for all of us,” Altmaier added. He said consumers in all countries would end up footing the bill because tariffs would push up prices for many kinds of products.
The threat of a full-blown trade war will also be on the agenda of the G20 meeting in Argentina, where finance ministers and central bank governors from the world’s 20 biggest economies meet from March 17 to 20.
Germany’s new finance minister, Olaf Scholz, will meet his U.S. counterpart Steven Mnuchin on Sunday or Monday on the sidelines of the meeting to discuss trade, banking regulation and other issues, senior German officials said on Thursday.
“The minister will have bilateral meetings with all G7 counterparts,” one of the officials said, on condition of anonymity, adding that multilateral trade would “certainly be a big topic” at the G20 meeting.
The taxation of profits from digital business and regulation of crypto currencies will also be in focus, the official added.
Swiss National Bank Chairman Thomas Jordan said on Thursday U.S. protectionism could be a threat to the export-dependent Swiss economy and trigger safe-haven flows that would drive up the value of the Swiss currency.
Editor’s Note: Reporting by Michael Nienaber, Gernot Heller and Markus Wacket; Editing by Kevin Liffey.