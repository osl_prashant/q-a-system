BC-MI--Michigan Weekend Exchange Digest, MI
BC-MI--Michigan Weekend Exchange Digest, MI

The Associated Press



AP-Michigan stories for the weekend of March 25 and March 26. Members using Exchange stories should retain the bylines and newspaper credit lines. If you have questions, please contact the desk at 313-259-0650.
For use Sunday, March 25, and thereafter:
EXCHANGE-JUST GREGORY
LANSING, Mich. —A Michigan restaurant owner says he doesn't make money on his business, but that he keeps it because it's home to a lot of people. Seventy-eight-year-old Gregory Eaton owns Gregory's Soul Food in Lansing. Eaton is one of the state's first black multi-client lobbyist, the former owner of The Garage and a partner of the national dealership, Metro Cars. By Vickki Dozier, Lansing State Journal. SENT IN ADVANCE: 2,912 words.
EXCHANGE-CANCER CONCERNS
OTSEGO, Mich. —A cancer survivor from a rural Michigan area is seeking to find if the number of illnesses in her hometown is tied to something dumped in the environment. Mary Zack of Otsego says she's now healthy, but she has questions about the area she grew up in. Her investigation has captured the attention of federal and state agencies. By Brad Devereaux, Kalamazoo Gazette. SENT IN ADVANCE: 1,285 words.
For use Monday, March 26, and thereafter:
EXCHANGE-CHICKEN RULE CHANGES
PORT HURON, Mich. —A Michigan10-year-old girl has inspired change in her township. Avery Baker wrote a letter to Clay Township last year because she wanted to raise a few chickens, but her family's Harsens Island property was half an acre shy of the minimum allowed. The township is now tweaking its rules for recreational animals. An ordinance change was approved by the planning commission in February. By Jackie Smith, The Times Herald. SENT IN ADVANCE: 747 words.
EXCHANGE-HIVE HEAVEN
DETROIT —A growing number of people in Detroit and around the globe are cultivating urban beehives as part of social missions and small businesses. It's a trend that has prompted cities to lift beekeeping limitations and inspired entrepreneurs to sell beekeeping starter kits and the bees' honey.. By Frank Witsil, Detroit Free Press. SENT IN ADVANCE: 1,609 words.