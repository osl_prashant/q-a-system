10 wolves killed in northern Idaho to boost elk numbers
10 wolves killed in northern Idaho to boost elk numbers

By KEITH RIDLERAssociated Press
The Associated Press

BOISE, Idaho




BOISE, Idaho (AP) — Federal officials have killed 10 wolves in northern Idaho at the request of the Idaho Department of Fish and Game to boost elk numbers, and state officials say more might be killed this winter.
The U.S. Department of Agriculture's Wildlife Services said Wednesday that workers used a helicopter in the Clearwater National Forest in late February and early March to kill the wolves.
"At the request of Idaho, we did remove wolves in that region," said agency spokeswoman Tanya Espinosa.
Idaho officials say the area's elk population in what's called the Lolo zone has plummeted in the last 25 years from about 16,000 to about 2,000, and that wolves are to blame along with black bears, mountain lions and a habitat transition to more forests.
Fish and Game has liberal harvest rules for bears and mountain lions, but wolves are more challenging to hunt. So in six of the last seven years, Fish and Game has sought to kill wolves to boost elk. Elk are a prominent big game species in Idaho and hunters have decried a scarcity of elk in the region. Elk are also a source of revenue through hunting license sales for Fish and Game.
"We've made an obligation to try to manage this elk herd at levels at maybe not peak levels, but at least bring it back to levels that we've seen in the past that were adequate for hunting," said Jim Hayden, a biologist with Fish and Game.
Officials say Fish and Game license dollars paid for the federal agency to kill the wolves. State and federal officials didn't have the cost immediately available.
Environmental groups blasted the killing of the wolves, focusing on the operation being made public only after it happened.
"Now more than ever, Wildlife Services and the Idaho Department of Fish and Game need to be up front with the public about their plans to kill wolves," said Andrea Santarsiere, an attorney with the Center for Biological Diversity. "Idaho stopped monitoring wolves last year and stopped releasing annual reports revealing how many wolves remain in Idaho. It's troubling to see this ever-increasing veil of secrecy fall over the management of Idaho's wolves."
The last intensive wolf count in Idaho was in 2015 when officials said the state had an estimated 786 wolves at the end of the year. That's also the last year Fish and Game was required to do that type of count after wolves were removed from the Endangered Species List.
But Fish and Game has continued to monitor wolf populations. Hayden said that based on DNA samples from more than 700 wolf droppings, nearly 150 remote cameras and other information, at least 11 packs are in the Lolo zone. Hayden said the agency manages populations and doesn't count individuals. But he said an Idaho wolf pack typically has six to nine wolves. That means there are roughly 65 to 100 wolves in the Lolo zone.
Fish and Game estimates that statewide there are more than 90 packs, Hayden said, far above the state's minimum requirement of 15 packs. The federal government could take back management of Idaho wolves if the population gets too low.
Hayden said the state and federal agencies do not announce wolf-kill operations out of concern for the safety of the helicopter crew as well as the last-minute nature of the operations. He said a snowy day must be followed by clear flying weather, and there's a chance that if those conditions occur again this winter federal workers will try to kill more wolves in the Lolo zone.
"After you go after the first one, the wolves are scattering, so it's not common to take a whole pack," he said.