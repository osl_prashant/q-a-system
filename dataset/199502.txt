From the May 2016 issue of Drovers. In October 2013, several heavy bred Hereford cows were stolen from a northeast Oklahoma pasture. Two weeks later the rustlers returned, stealing three black Angus embryo recipient cows, two registered Hereford yearling bulls and eight weaning-age registered Hereford heifer and bull calves.Decades of breeding genetics valued at more than $100,000 were gone in just two nights.
The heat was on, says Bart Perrier, special ranger for the Texas and Southwestern Cattle Raisers Association (TSCRA). Law enforcement called in media to broadcast the story. The cattle were identifiable with an upside-down horseshoe branded on the left hip and the calves were tattooed in the left ear.
Five days later, a call came from the owner of the Durant, Okla., auction market. He had seen reports of the missing cattle and recognized them from the auction ring days before. Fortunately, the cattle wererecovered. "Because this ranch had good management practices, they were able recover their cattle," Perrier says. Here are six tips for ranchers to thwart rustlers:
1. Permanent identification. "The only guaranteed way to identify livestock is through a registered brand," Perrier says. Tattoos are the next best option.
2. Lock gates. "That isn't going to stop a serious thief, but it proves someone who definitely should not have been on your property was there. It also gives you a timeline on when it could have happened."
3. Count your cattle on a regular basis. "The sooner you realize something is missing, the more likely there will be leads to find," he says.
4. Don't feed in pens.
5. Don't build pens right off the roadways. 
6. Don't keep a regular routine. 
Click here to read "I'm A Drover: Cowboy cop" featuring TSCRA special ranger Bart Perrier.