Nearly 90 U.S. agricultural groups have sent a letter to President Trump asking him to build strong trade relationships with Asian countries.
The Feb. 6 letter said the Asia Pacific region is the world largest market for food and will grow rapidly in the years ahead.

"We hope your administration will create such opportunities for our sector by deepening U.S. economic engagement in this critical region while responding to the Asia-only regional trade agreements being negotiated by our foreign competitors," the letter said.
The U.S. Apple Association, the American Farm Bureau Federation, the American Potato Trade Alliance, the California Cherry Export Association, the National Potato Council, and the Northwest Horticultural Council were among the groups that signed the letter.