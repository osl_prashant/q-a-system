If there is a "Holy Grail" of produce safety, it is a kill step that would remove any threat of pathogens on fresh produce.
 
Right behind that, perfect compliance with the Produce Traceability Initiative is another lofty industry goal.
 
Both ambitions still need work.
 
Researchers will continue to look for a silver bullet in the years ahead, but that is no guarantee they will find it, said Bob Brackett, director of the Food and Drug Administration's Center for Food Safety and Applied
Nutrition at the time of the 2006 spinach outbreak, now vice president and director of the Institute for Food Safety and Health at the Illinois Institute of Technology.
 
Brackett said evolving technology will likely result in the use of new analytics to look and find microorganisms in products sooner than was possible a decade ago. 
 
"Hopefully technology will also come up with a better way to reduce the amount of contaminants on the leaf," he said. 
 
The Holy Grail would be to find some sort of sanitizer or solution that would eliminate pathogens on produce, Brackett said.
 
There could be other as yet undiscovered technologies that could also provide answers, he said.
 
The stratification of leafy greens production is also possible, he said, with the rising influence of urban agriculture and hydroponics. In theory, those production systems are safer because they don't have exposure to the outside elements and animals as in a farm field.
 
Growers have more control over the product but growing methods are much more expensive, he said.
 
Brackett said the Leafy Greens Marketing Agreement and other similar groups are paving the way for what the food safety expectations are for growers, producers and processors.
 
"I don't think it is any secret that farmers don't like having FDA out on the farm with them, but I think the need to have that presence on the farm will decrease as the industry adopts best practices and implements new technologies."
 
Testing ahead
 
The future may include more testing for pathogens in the supply chain, said Jamie Strachan, CEO of Salinas, Calif.-based Growers Express.
 
While testing may be imperfect, it is one way to try to validate if produce safety practices are working.
 
"It is challenging to validate the practices we have put in place based on sound science, and whether they are yielding the results we want them to," he said. 
 
"As there is more testing and more validation, and more research being done, we can update our food safety systems to meet future challenges."
 
Will Daniels, owner of California-based Fresh Integrity Group, said much of the testing of fresh-cut produce is customer-driven. 
 
Though testing of finished product is not widely done, many of the major fresh-cut players are testing for pathogens and holding their product at the request of key customers.
 
Work needed on PTI
 
The industry still struggles with synchronizing buyers' requirements around food safety, said Tim York, CEO of Salinas, Calif.-based Markon Cooperative. 
 
"There are several buyers that are specifying additional or specific data not called for in the original PTI," he said. 
 
Suppliers are reporting different requests for traceability data, he said, and leaders of the Produce Traceability Initiative are working on resolving those expectations and understanding buyers' motivation for asking for what they want in traceability data.
 
York said harmonization of PTI demands is still possible. 
 
"I think it is very reconcilable," he said.
 
The traceability initiative, which first took form in late 2007 and early 2008, is still gaining steam, said Ed Treacy, vice president of supply chain efficiencies for the Newark, Del.-based Produce Marketing Association.
 
Publix and Wal-Mart previously made PTI part of their supplier requirements, and in the past 12 months Whole Foods has made PTI labeling of cases a part of thits Responsibly Grown program.
 
The Foodservice GS1 US Standards Initiative is working on traceability and some foodservice companies are stepping up, Treacy said. 
 
"Subway Foods made it a requirement for all food coming to the stores and they partnered with FoodLogiQ and they scan every carton going into the store," he said.  "They are gleaning supply chain transparency from that."
 
Some of the benefits of its approach, he said, is the ability to monitor expiration dates and to analyze if its distribution centers may have misrotated product. 
 
Other operators are reportedly using PTI labels to calculate their environmental effect by measuring distance from the farm to the store.
 
Among retailers, Wal-Mart uses pack date information to rotate its produce, Treacy said. The use of the pack date in the PTI label may eventually be the standard for all PTI labels, Treacy said.
 
Item-level coming?
 
A long-term goal for some is item-level traceability, but that is not likely for bulk items.
 
Reggie Griffin, principal with Reggie Griffin Strategies, Hilton Head, S.C., said he always viewed case-level PTI as an interim step toward the end goal of item-level traceability. 
 
Griffin thinks technology available today may provide a solution for item-level traceability.
 
However, Treacy said item-level traceability is difficult because of the challenges of maintaining label integrity for items in bulk displays.
 
The FDA has not yet issued record-keeping regulations for high-risk foods, Treacy said, but that regulation - when it is issued - won't derail PTI. While high-risk food regulations will include language that speaks to traceability, that won't negate the work on PTI, he said.
 
"Whatever comes out of PTI will be over and above their requirements," he said.  "By all indications, PTI would more than satisfy their requirements."
 
Making PTI mandatory isn't the way to go, Treacy said.  "I'm not in favor of any more regulations required," he said.  
 
"Our industry is responsible in doing what we are doing. We've gotten close to 60%, by our estimations, of implementation of PTI today and there is far more available if their customers required it." 
 
Treacy said many shippers don't label everything until their customers ask for it.
 
In another 10 years, Treacy thinks that PTI will be a way of life, and it will be hard to find cases that are not labeled. 
 
"It will be the way we do business," he said. 
 
"There is so much efficiency around advance ship notices and bar codes on cases," he said.