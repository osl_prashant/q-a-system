BC-Cash Prices, 1st Ld-Writethru,0342
BC-Cash Prices, 1st Ld-Writethru,0342

The Associated Press

NEW YORK




NEW YORK (AP) — Wholesale cash prices Wednesday
Wed.        Tue.
F
Foods

Broilers national comp wtd av            .9904       .9904
Eggs large white NY Doz.                  2.66        2.57
Flour hard winter KC cwt                 15.00       14.95
Cheddar Cheese Chi. 40 block per lb.    2.1650      2.1800
Coffee parana ex-dock NY per lb.        1.1911      1.1799
Coffee medlin ex-dock NY per lb.        1.3868      1.3803
Cocoa beans Ivory Coast $ metric ton      2752        2752
Cocoa butter African styl $ met ton       7426        7426
Hogs Iowa/Minn barrows & gilts wtd av    55.13       55.75
Feeder cattle 500-550 lb Okl av cwt     171.00      171.00
Pork loins 13-19 lb FOB Omaha av cwt     88.59       92.11
Grains
Corn No. 2 yellow Chi processor bid     3.54½       3.54½
Soybeans No. 1 yellow                  10.03¼      10.03¼
Soybean Meal Cen Ill 48pct protein-ton 366.20      362.10
Wheat No. 2  Chi soft                   4.44         4.44
Wheat N. 1 dk  14pc-pro Mpls.           7.29¼       7.33¼
Oats No. 2 heavy or Better              2.46½       2.50½
Fats & Oils
Corn oil crude wet/dry mill Chi. lb.    .31           .31
Soybean oil crude Decatur lb.           .30           .30½
Metals
Aluminum per lb LME                    0.9903        0.9391
Antimony in warehouse per ton            8700          8700
Copper Cathode full plate              3.0771        3.0804
Gold Handy & Harman                    1321.35      1311.00
Silver Handy & Harman                   16.483       16.179
Lead per metric ton LME                2345.50      2360.00
Molybdenum per metric ton LME           15,500       15,500
Platinum per troy oz. Handy & Harman    947.00       945.00
Platinum Merc spot per troy oz.         950.60       945.00
Zinc (HG) delivered per lb.             1.4709        1.4795
Textiles, Fibers and Miscellaneous
Cotton 1-1-16 in. strict low middling    79.39       79.88
Raw Products
Coal Central Appalachia $ per short ton   63.00       63.00
Natural Gas  Henry Hub, $ per mmbtu       2.66        2.63
b-bid a-asked
n-Nominal
r-revised
n.q.-not quoted<29
n.a.-not available