After hitting a low price of $97.75 per cwt in mid-October, cash fed cattle prices rallied $13 per cwt by the end of November, a 12% increase. Prices reached a seasonal peak above $114 per cwt the first week in December before retreating modestly.Overall, the market recovered from October's unexpected decline which took prices below the $100 mark for the first time since December 2010. Feedyards posted modest profits on cattle delivered in late November and early December.
Our group of cattle industry stakeholders and experts who make up the Monday Market Sentiment varied significantly in their estimates of weekly market prices throughout the month. However, their average estimate for the week of Nov. 14, was only $0.41 off the five-area fed cattle price of $103.92.
Their largest gap in estimates occurred a week later, with an underestimation of $3.06 per cwt the week of Nov. 21, when prices rallied to $108.04 per cwt.
Boxed beef cutout prices rallied to match higher fed cattle prices. By mid-December the Choice cutout was averaging $189.55, about $9.50 higher than the low in mid-October.
The rally was driven by higher prices for ribs, loins, briskets and short-plate primals. Rib prices gained 30% from their October lows. The Choice beef rally also widened the Choice-Select spread to $16.74 by mid-December, $5.48 per cwt better than a year ago and $1.69 better than the five-year average.
As the calendar turns to 2017, analysts expect beef values to soften and the Choice-Select spread to narrow. Fed cattle and beef supplies will remain above year-ago levels. Packers continue to operate with good margins, which will encourage them to remain fairly aggressive buyers and maintain high harvest levels.
Increases in red meat and poultry supplies will remain a price limiting factor for all proteins throughout the first quarter of the new year.

Feed cattle? Like predicting market trends? Have use for a $100 gift card from Cabela's? Each week join our Monday Market Sentiment panel of cattle industry experts in an online contest where participants are asked to pick the weekly market price projection for fed cattle. The entry closest to USDA's five-area weighted reported fed-cattle price wins a $100 Cabela's gift card.
November Weekly Winners:
Nov. 7: Ryan Loseke, Loseke Feedyards
Nov. 14: Tom Stuckey, Denker Feedlots
Nov. 21: Shelby Jones, Ranger Feeders
For more information, visit www.cattletradercenter.com.

Note: This story appears in the January 2017 issue of Drovers.