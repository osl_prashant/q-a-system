It was a tough year to make quality hay. On some Michigan farms, 2017 was an impossible year and the hay never got made, even though plant growth was excellent. Saturated clay soils through much of the summer prevented hay harvest altogether in some areas of the eastern Upper Peninsula. The result was relatively thick stands of over-mature timothy/trefoil hay. No profitable local market exists for this very low quality hay.Some fields were mowed and windrowed, then after repeated rain events ruined the hay, windrows were burned. The damp windrows failed to burn very well. If left unmanaged, unharvested, standing forage can result in a dense mat on the soil surface over winter and interfere with spring growth and hay harvest next year.
The question is, “What to do with it?” There are two practical options to consider: Burn off the fields, or chop the standing hay and return it to the field surface.
Chopping and returning the hay to the field has advantages. Whatever fertilizer value is in the hay material (N, P2O5, K2O, S, etc.) is recycled back into the soil. The disadvantages to chopping are the machinery cost, time invested, potential compaction on wet soils and the possibility of the chopper depositing the material in a heavy swath, which may smother some of the plants underneath. In the short term, this may decrease yields, but in the long term, seeds in the swath will create new plants.
The negatives associated with burning include loss of nutrients, drifting smoke and personal safety hazard. Manitoba Agriculture estimates the loss of nutrients from burning small grain stubble at 90 percent. Estimates of nutrients lost through straw burning from Mosaic Crop Nutrition are more conservative, including 98-100 percent loss of nitrogen and 70-90 percent loss of sulfur, but only 20-40 percent loss of phosphorus and potassium.
Using feed analysis reports from a Chippewa County timothy/trefoil field harvested in early August 2015 (crude protein: 7.37 percent, phosphorus: 0.13 percent, potassium: 0.93 percent), fertilizer value per ton of hay can be estimated.  Nitrogen (N), phosphorus (P) and potassium (K) values are based on 2017 fertilizer price estimates. 
Total pounds of N, P2O5, estimated value ($ per pound) and K2O in old hay
Nitrogen: 20 pounds per ton; $0.33 per pound
P2O5: 5 lbs. per ton; $0.34 per pound
K2O: 19 pounds per ton; $0.27 per pound
$ Value of plant nutrients in old hay
Nitrogen: $1.65 (25 percent of total calculated N per ton)
P2O5: $1.73
K2O: $5.12
Total: $8.50 fertilizer value per ton of hay (at 85 percent dry matter) returned to field
If the hay is burned and the losses are calculated using the estimate from Mosaic Crop Nutrition:
$4.77 fertilizer value per ton of hay burned on the field
Michigan State University Extension estimates the cost of chopping hay at $7.50 to $12 per acre, so we will use $9.50. The following is a rough estimate of the cost/benefit of chopping versus burning.
Chopping 2 tons per acre hay:
$17 per acre estimated fertilizer value of 2 tons per acre hay – $9.50 per acre chopping = $7.50 per acre
Burning 2 tons per acre hay (using optimistic ash nutrient values from Mosaic):
$9.54 per acre estimated fertilizer value of ash from 2 tons per acre hay - $1 per acre burning cost = $.854 per acre
Based on this estimate of nutrient return value, there is not a compelling reason to choose one method over the other. However, chopping avoids the potential problems associated with burning.
A third option is to graze the fields. Trampling is more effective than chopping at returning organic matter to the soil because it doesn’t create swaths and the manure is more available to plants. Obviously, this requires access to livestock and a fenced field.