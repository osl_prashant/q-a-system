Markets Right Now: US stocks open slightly lower
Markets Right Now: US stocks open slightly lower

The Associated Press

NEW YORK




NEW YORK (AP) — The latest on developments in financial markets (all times local):
9:35 a.m.
Stocks are opening slightly lower, extending the market's losses, after several companies reported weak results or warned of higher costs.
Goodyear Tire & Rubber fell 2.9 percent early Wednesday after warning of higher expenses for raw materials.
Entergy fell 1.9 percent after reporting results that missed forecasts.
The S&P 500 index lost 11 points, or 0.4 percent, to 2,623.
The Dow Jones industrial average fell 97, or 0.4 percent, to 23,928. The Nasdaq gave up 25 points, or 0.4 percent, to 6,979.
Bond prices fell. The yield on the 10-year Treasury rose to 3.02 percent, the highest level since January 2014.