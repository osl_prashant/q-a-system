A New Jersey-based retailer believes in New Jersey produce.Local produce — conventionally and organically grown — is growing in popularity in New Jersey and New York.
To meet that demand and expand its partnerships with the local farming community, Mahwah, N.J.-based Inserra Supermarkets is bringing locally sourced produce to all of its 22 ShopRite stores this spring and summer.
“Consumers can taste the robust flavors and freshness of produce that was picked that same day or the day before,” said Eric Beelitz, director of produce for Inserra Supermarkets, which is connected to Elizabeth, N.J.-based Wakefern Food Corp., which has more than 300 stores under 45 owners in its network.
“That taste is important, and of course, really makes a difference.”
In New Jersey, the family-owned chain’s 17 ShopRites are located in Bergen, Hudson and Passaic Counties. In New York, the company has five stores in Rockland County.
The company has turned New Jersey produce into one of its signature departments, Beelitz said.
“It’s becoming a bigger and bigger part of our business, and ShopRite has a heavy emphasis on making sure it’s right,” Beelitz said.
The stores also make an effort to showcase the locally grown products, he said.
“We try to segregate most of the Jersey product for a big massive display,” he said.
Inserra Supermarkets has a longtime partnership with Abma’s Farm in Wyckoff, N.J.
“We want to let our customers know we’ll have produce from Abma’s,” Beelitz said.
Dealing with Abma’s enables the stores to get product quickly, Beelitz said.
Take sweet corn, for example.
“Abma’s Farm will pick the corn at 4 a.m. and deliver it to our stores the same morning,” Beelitz said.
“We did this last year with Abma’s, and it resonates with our customers. They couldn’t get enough of it.”
Inserra Supermarkets sources local organic produce and herbs from Zone 7, a distributor that works with more than 120 regional sustainable farms — mostly in New Jersey and Pennsylvania — year-round.
“We have forecasted out our needs with all of our growers and are in constant contact with them,” Beelitz said.
Marketers say the approach that Inserra and other retailers take is invaluable.
“People want locally produced, and they want to know where their food comes from and how it’s produced, and that’s in our favor,” said Pegi Adam, marketing and promotion consultant with the Glassboro, N.J.-based New Jersey Peach Promotion Council.
“They want to know who their growers are.”
Adam credited Inserra for its merchandising approach to local produce.
“Last year, ShopRite had a farm wagon with all Jersey produce on it,” she said.
Retailers’ approaches sometimes vary, but the “buy local” message is standard, and effective, said Bob Von Rohr, marketing and customer relations manager for Glassboro, N.J.-based distributor Sunny Valley International Inc.
“They’re always at the forefront of the departments,” he said of displays. “That’s usually the best way to sell them.”