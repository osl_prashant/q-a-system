Los Alamitos, Calif.-based Frieda's Specialty Produce is bringing back specialty citrus supplies.
Peak season citrus including Tahitian pummelos, kumquats, limequats, mandarinquats, pink lemons, proprietary varieties of mandarin orange and organic mango oranges are available to ship, according to a news release.
"We have had an incredible specialty citrus season so far," said Alex Jackson, senior account manager at Frieda's, in the release. "These varieties really bring excitement to the citrus display."
Jackson said Frieda's wants citrus supplies to hit big with retailers and foodservice as well.
"Winter doesn't mean just apples and oranges anymore. A big, beautiful display of bulk and packaged specialty citrus will draw attention," according to the release.