<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


WASHINGTON, D.C. - A new U.S. president will be determined in the November election, but many key issues remain the same for hundreds of fresh produce industry advocates who took part in the Sept. 13 United Fresh Produce Association's March on Capitol Hill.
Priority issues of immigration reform, free and fair trade, school lunches and the Fresh Fruit and Vegetable Program were highlighted by industry leaders, said Tom Stenzel, president and CEO of Washington, D.C.-based United Fresh.
Stenzel said the United Fresh Washington Conference organized more than 300 meetings with members of Congress Sept. 13.
Among trade issues, Stenzel said the industry was lobbying lawmakers for passage of the Trans Pacific Partnership this year.
Farm labor remains a big concern for fruit and vegetable growers, he said.
"Immigration reform is something that is absolutely critical, we have a labor crisis," he said. "We are not going to solve that this year, but we have to lay the groundwork for the next presidential administration no matter who it is."
Stenzel said child nutrition programs, including locking in gains to the school lunch program and preserving the Fresh Fruit and Vegetable Program, are also key priorities for United Fresh.