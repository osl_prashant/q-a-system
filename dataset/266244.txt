BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Wed.Tue.No. 2 Soft wheat4.36½4.40No. 1 Yellow soybeans9.939.94½No. 2 Yellow Corn3.53½e3.54eNo. 2 Yellow Corn3.71½p3.72p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted