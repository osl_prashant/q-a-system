The Pork Checkoff has selected 12 college students to represent the #RealPigFarming Student Social Forces team this year. Candidates were selected based on their involvement in the pork industry and their strong communication skills. The team will be active from July until December.  “Social media is ingrained in young people’s lives,” said Claire Masker, public relations director for the Pork Checkoff. “It’s easy for them to share their thoughts about an industry that they are proud to be a part of through the various social media channels available to them.” 

Name


Institution


City, State


Tori Abner


Texas A&M University


Howe, Texas 


Hannah Rehder


North Dakota State University


Moorhead, Minnesota 


Brooke Sieren


Iowa State University


Keota, Iowa 


Edan Lambert


Iowa State University


Orange City, Iowa 


Katelyn Lowery


North Carolina State University


Clayton, North Carolina 


Taylor Homann


University of Minnesota


Pipestone, Minnesota 


Amy Lund


Iowa State University


Polk City, Iowa 


Kristin Liepold


University of Minnesota


Okabena, Minnesota 


Hunter Everett


North Carolina State University


Mebane, North Carolina 


Jenna Chance


Kansas State University


Lebanon, Indiana 


Megan Anderson


The Pennsylvania State University 


Schellsburg, Pennsylvania 


Julia Hay


Texas A&M University


Somerset, Pennsylvania

 Consumers continue to have questions about how pigs are raised, and no one knows the answers better than pork producers. The Pork Checkoff’s social media outreach program is helping real farmers share real stories with consumers through #RealPigFarming. The hashtag (#) before Real Pig Farming helps people search social media posts with the same phrase, making it easier for them to follow conversations. "I am excited to have this opportunity to share my Real Pig Farming story with consumers searching for answers about where their pork comes from,” said Edan Lambert, one of the newly selected members of the #RealPigFarming Student Social Forces and a student at Iowa State University. “The social forces team will be encouraged to use #RealPigFarming as advocates for the pork industry,” Masker said. “Through social forces, the students will be able to improve their communications skills and expand their professional network within the industry.”