BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Thu. Feb 22, 2018          USDA Market NewsDaily California EggsPrices are unchanged. Trade sentiment continues sharply higher. Demand ismixed, usually good. Supplies and offerings are light to moderate. Marketactivity is active.Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                225EXTRA LARGE          203LARGE                194MEDIUM               165Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 22-Feb-18 11:42 AM E MP