Third-party logistics and supply chain management company C.H. Robinson is adopting the use of Omnitracs Virtual Load View to track loads.
The Virtual Load View service allows brokers, shippers and carriers to track and manage loads from a single portal, according to a news release from Omnitracs.
“C.H. Robinson us strengthening its commitment to providing in-depth insights as well as visibility into load locations and delivery times,” Omnitracs vice president of product management Jeff Champa said in a news release.
C.H. Robinson has increased the number of loads tracked by the system since January.
“The software integrates seamlessly into our system, while still allowing our contract carrier network to use their favorite C.H. Robinson app: Navisphere Carrier or Driver,” Tim Kurtzal, director of C.H. Robinson process and standards, aid in the release. “Overall, (Virtual Load View) provides us with essential freight visibility, which increases our customers’ satisfaction.”