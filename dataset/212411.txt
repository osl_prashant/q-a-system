(UPDATED, June 2)  Testing the waters of its newly opened market, Argentina lemon exporters plan to send a small air shipment of several pallets of fruit to the U.S. in June, according to a report from an Argentina news outlet. 
Roberto Sanchez Loria, president of the Tucuman Citrus Association, told www.eltucumano.com in Argentina that the industry expected to send a symbolic lemon shipment of five or six pallets — each with about 70 boxes — to the U.S. in June. No date for the shipment was disclosed, according to the website.
 
Embassy of Argentina agricultural attache Jose Molina said June 1 the shipment is expected sometime between June and the end of August, when the shipping season for lemons ends.
 
Argentina lemon exporters have been absent from the U.S. market for more than 15 years, and California citrus leaders have filed a lawsuit seeking to reverse the U.S. Department of Agriculture’s decision in May to allow imports of Argentina lemons, beginning May 26. The lawsuit, filed by the U.S. Citrus Science Council, argues that proper procedures were not followed to develop the rule, which was issued Dec. 23, subjected to a 60-day stay Jan. 22 and to another 60-day stay March 17.
 
Molina said there is concern in Argentina that the lawsuit will create uncertainty for exporters this year and next year. The last time California filed a lawsuit against the USDA for a rule allowing Argentina lemon access, in 2000, it took over  a year for the judge to remand the issue to the USDA.
 
“That’s clearly a concern on the commercial side,” he said. “We are hopeful USDA will prevail,” he said, noting 16 years of science has gone into the rule giving Argentina access.
 
Joel Nelsen, president of California Citrus Mutual, said June 2 he had no knowledge of the Argentina lemon shipment or how the USDA would handle the air shipment.  
 
Sánchez Loria of the Tucuman Citrus Association told eltucumano.com that substantial shipments of Argentina lemons, shipped by ocean container, are not expected until April of next year.