Just because food companies will soon be able to market their products as certified transitional organic doesn't mean the produce industry will adopt it.
 
That's a wise move.
 
The U.S. Department of Agriculture's new transitional organic program is designed to help growers make the transition from conventional to organically grown, and there will be no official seal or label.
 
This is good. 
 
It also appears the biggest beneficiaries will be for growers of animal feed products like corn and soybeans, where the difference between conventional and organic prices is most extreme.
 
Fresh produce companies mostly say there's little value to the concept. It's either certified organic or it's not.
 
They have no plans to market product as transitional organic.
 
The fear is that food marketed as certified transitional would steal demand away from organic, and there could be quality issues that would erode the value of organic.
 
USDA says it has safeguards, such as a limit to the number of times the same piece of land can get into the program. 
 
A year ago The Packer Editorial Board called the proposal more risk than reward, and we still believe that, but the risk is much lower if fruit and vegetable growers, and their buyers, agree there's little value in the designation.
 
Did The Packer get it right? Leave a comment and tell us your opinion.