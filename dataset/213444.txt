Veggie Noodle Co. founder Mason Arnold holds up two of the company's newest items at Fresh Summit.

NEW ORLEANS — Veggie Noodle Co. will launch riced cauliflower and riced broccoli products in December.
Adding those products to the portfolio was a decision driven by customer demand, founder Mason Arnold said during Fresh Summit.
The company has also been developing seasonal varieties of vegetable noodles, including white sweet potato, golden beet, purple sweet potato and watermelon radish.
Veggie Noodle Co. is finishing up with the white sweet potato noodles and watermelon radish noodles currently and will have golden beet noodles and purple sweet potato noodles ready to ship in November, Arnold said. The first pair did well in its trial run at retail.
Veggie Noodle Co. is developing more items for the spring line now.
The company recently celebrated the opening of a new facility, which is now fully operational. Veggie Noodle Co. is based in Austin, Texas.