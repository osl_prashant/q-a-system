Top EU court: Poland broke law by logging in pristine forest
Top EU court: Poland broke law by logging in pristine forest

The Associated Press

WARSAW, Poland




WARSAW, Poland (AP) — The European Union's top court has ruled that Poland violated environmental laws with its massive logging of trees in one of Europe's last pristine forests.
The ruling Tuesday by the European Court of Justice said that, in increasing logging in the Bialowieza Forest, Poland failed to fulfil its obligations to protect natural sites of special importance.
Poland had argued that felling the trees was necessary to fight the spread of bark beetle infestation.
Environmentalists say the large-scale felling of trees in Bialowieza was destroying rare animal habitats and plants, in violation of regulations. They held protests and brought the case before the court last year.
Poland has since replaced its environment minister and stopped the logging. Warsaw has said it will respect the EU court's ruling.