Situated in close proximity to major local growing regions, wholesalers on the Atlanta State Farmers Market in Forest Park source and distribute high volumes of local and regional produce.
As the capitol city of South, the metropolitan area is within short drives to growing regions in south Georgia, South Carolina, North Carolina, Alabama, Tennessee and Florida.
Those states grow a variety of vegetables, tomatoes, berries, melons and peaches.
"Local produce is a big draw," said Andrew Scott, director of marketing and business development for the Nickey Gregory Co. LLC, Forest Park. "Interest in local is strong, but not only at retail. It's starting to grow on the foodservice level as well."
As evidence of increased foodservice interest in local produce, Scott pointed to billboards Subway has erected in the Atlanta area that promote the Georgia Grown program and the bell peppers, cucumbers and tomatoes the chain places on its sandwiches.
East Coast growers are growing more vegetables traditionally grown on the West Coast, he said.
Asparagus, broccoli, brussels sprouts, leafy greens and strawberries are among some of those items, Scott said.
"I see local continuing to increase," he said. "If they (the growers) can get everything dialed-in in terms of food safety, they will do just fine."
Bryan Thornton, general manager of Coosemans Atlanta Inc., Forest Park, also noted Subway's promotion of local through billboards.
 

Georgia Grown

He said the Georgia Department of Agriculture's Georgia Grown program has also been successful in raising public awareness about local produce.
"That local push has been amazing," Thornton said. "It's that brand recognition. The local economy is good for restaurants. The commissioner of agriculture, Gary Black, has definitely sparked an interest in local produce across the state."
Black has done well promoting Georgia produce in the state's schools, said Matt Jardina, vice president of general business operations for J.J. Jardina Co. Inc., Forest Park.
"Local has run past organic in terms of what people are looking for in the marketplace," he said. "Local is where everything is now. It's what everyone wants. There will always be a demographic which likes and wants organic but the demographic that wants local has kind of run past that organic demographic in terms of size."
 

Fresh, pretty and local

In late August, Sunbelt Produce Distributors Inc. in Forest Park received a trailer load of vegetables from Ohio and Cliff Sherman, owner, said local production was moving to Michigan and Ohio.
The distributor was pulling peaches from South Carolina and Sherman said he waits for Georgia vegetable production to begin in the fall.
"Demand is still strong for local produce," he said. "I see demand continuing to increase. Customers are requesting more local product. As long as it's fresh and pretty, they're jumping on it."
Forest Park-based Phoenix Wholesale Foodservice Inc., and its sister retail distributing company, Collins Bros. Corp., e-mails a weekly newsletter to customers discussing local produce, trends and where product is originating during different times of the year.
"Local demand is growing," said David Collins III, Phoenix's president. "Production is growing and interest is growing very well. You may see local growing faster than organics."
More customers visiting the market are requesting local product, said Paul Thompson, the market's manager.
The Georgia Grown program remains critical in promoting local, he said.
"That program is building momentum," Thompson said. "You're seeing more companies using that logo. The demand for knowing where your food comes from, the local flavor here on the market and around Atlanta, it's popular. We see that continuing to grow."
Demand remains solid, said Diana Earwood, vice president of Sutherland's Foodservice Inc., and general manager of Destiny Organics LLC in Forest Park.
"Local is something to be proud of and something consumers support, thus they are pushing it through the supply chain," she said. "Our company supports our surrounding southeastern states (production)."
Robert Poole, director of sales for Athena Farms in Forest Park, said many customers that state they want to purchase local throughout the year find it's impossible.
"I run into people who say they have to have local, but there are actually very few people that are strictly local," he said. "Local is one of the buzzwords. A lot depends on the definition. I think there are more people getting into it."