Jim Gorny is leaving the Produce Marketing Association to join the Food and Drug Administration, one of several moves from the agency in recent weeks to focus more on food safety.
Gorny, PMA’s vice president of food safety and technology, is the senior science advisor for produce safety at the FDA, a new position focused on Food Safety Modernization Act regulations. Gorny will develop and implement strategies for the FSMA’s produce safety standards, according to a news release.
He starts at the FDA Feb. 5.
The Jan. 31 announcement came a day after the FDA and U.S. Department of Agriculture heads signed an agreement promoting collaboration between the agencies on food safety matters.
“Gorny will facilitate, coordinate and advise senior leadership regarding policies, programs and high-priority scientific matters affecting the safety of fresh produce,” according to the release.
His role will oversee food safety on all points of the supply chain, including fields, packing, transportation, processing, wholesale and retail.
“My job is going to be about building bridges,” Gorny said in the release. “Between the states and the FDA, for example, because of the huge role states have in inspecting farms.”
Educating industry on FSMA rules is critical, but finding practical ways of meeting those rules is also important, he said.
“I’ve heard loud and clear in my role at PMA that produce businesses have a strong desire to get implementation right,” Gorny said in the release.”