School offers medicinal gardening class
School offers medicinal gardening class

By HARMONY BIRCHThe Brattleboro Reformer
The Associated Press

NEWFANE, Vt.




NEWFANE, Vt. (AP) — Herbalism is about autonomy, according to Juliette Carr.
"It's promoting autonomy and self-empowerment to prioritize reclaiming lost wisdom," she said.
Carr, who is a founding member of The Women's Action Team, wants to bring back home medicine traditions. She wants people to reclaim that lost knowledge so they can advocate for themselves and their family's health. That's why she opened up Old Ways Herbal School.
During warm weather months, the school offers several classes and training programs. This year, Carr announced a medicinal gardening class so students could learn to grow their own herbs.
Herbal medicine isn't about making "major changes" to the body, "it's not heroic medicine," she said. It's more about promoting general health and well being. "We're literally talking about tea," she said.
Carr, who is a firm believer in modern medicine and a registered nurse, doesn't think herbs can solve all health problems. But she can solve a simple headache. She also makes honey and tinctures that help people focus, or relax, or deal with depression or anxiety.
"It works well with modern medicine," she said.
The act of infusing honey or making a cup of tea, she said, is a loving one. "You think of your mom or your grandma making you a cup of tea," she said. Herbalism wasn't a skill that belonged to just women, Carr said, but women were seen as the caregivers and herbalism lent itself to that.
"I don't mean that to say that women should stay home," Carr said. "It's the traditional role of women in the community and women in families. It's health promotion, healing and the act of making tea."
Lately, though, she said people have become completely reliant on pharmacies and doctors. At one point, she said, "promoting health was part of our birthright."
The traditions of family-based healing or what Carr calls "rational medicines were taken from our cultural consciousness." And those who offered home-remedies — your aunt, your mother, your grandmother — lost their power. Carr aims to give her students independence. She wants them to go to the doctor and have the confidence to ask for second opinions or to know for themselves when something's wrong.
"For women, we're taught to be agreeable, to take 'no' for an answer," she said.
For her, herbalism is a way to give back to marginalized communities. Modern medicine, for instance, has a history of experimenting on people of color. The story of Henrietta Lacks, a woman whose cancer cells were used and experimented on without her permission, or the Guatemalan syphilis experiments, where the U.S. infected Guatemalans with syphilis without their consent, serve as of examples of this. Herbalism, Carr said, is a way for people to take back their health. That's something she talks a lot about with students in her classes. She also works to make her classes accessible to everyone by using a sliding payment scale, instituting a work-trade program, and often using barter to pay for students' classes. Though, she added, the ability to take time out of your day to do a course implies a certain level of privilege that not everyone has.
The addition of the gardening class only helps Carr's mission to teach independence. It gives people the tools to set up their own ecosystem within their gardens, she said. She'll be teaching students how medical plants can be used as companion plants in gardens to keep away predators. She'll be teaching, "how to grow these fussy plants," she said. Herbs, she said, can be hard to grow and temperamental. She'll also be teaching the correct way to harvest, soil rotation, and microclimates. There won't be any actual making of medicine, but Carr encourages her students to make medicine at home.
"Herbs are pretty safe," she said.
___
Online: https://bit.ly/2GJhVFu
___
Information from: Brattleboro Reformer, http://www.reformer.com/