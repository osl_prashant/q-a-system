Nor'easter grounds flights, halts trains along East Coast
Nor'easter grounds flights, halts trains along East Coast

By SARAH BETANCOURTAssociated Press
The Associated Press

BOSTON




BOSTON (AP) — A nor'easter pounded the Atlantic coast with hurricane-force winds and sideways rain and snow Friday, flooding streets, grounding flights, stopping trains and leaving 1.6 million customers without power from North Carolina to Maine. At least five people were killed by falling trees or branches.
The storm submerged cars and toppled tractor-trailers, sent waves higher than a two-story house crashing into the Massachusetts coast, forced schools and businesses to close early and caused a rough ride for passengers aboard a flight that landed at Dulles Airport outside Washington.
"Pretty much everyone on the plane threw up," a pilot wrote in a report to the National Weather Service.
The Eastern Seaboard was hammered by gusts exceeding 50 mph, with winds of 80 to 90 mph on Cape Cod. Ohio and upstate New York got a foot or more of snow. Boston and Rhode Island were expected to get 2 to 5 inches.
The storm killed at least five people, including a 77-year-old woman struck by a branch outside her home near Baltimore. Fallen trees also killed a man and a 6-year-old boy in different parts of Virginia, an 11-year-old boy in New York state and a man in Newport, Rhode Island.
Floodwaters in Quincy, Massachusetts, submerged cars, and police rescued people trapped in their vehicles. High waves battered nearby Scituate, making roads impassable and turning parking lots into small ponds. More than 1,800 people alerted Scituate officials they had evacuated, The Boston Globe reported.
Massachusetts Gov. Charlie Baker activated 200 National Guard members to help victims.
Airlines canceled more than 2,800 flights, mostly in the Northeast. LaGuardia and Kennedy airports in New York City were brought to a near standstill.
President Donald Trump, who traveled to North Carolina for the funeral for the Rev. Billy Graham, was forced to fly out of Dulles instead of Joint Base Andrews in Maryland, where Air Force One is housed, because of high winds.
Amtrak suspended service along the Northeast Corridor, from Washington to Boston. In New Jersey, a tree hit overhead wires, forcing the suspension of some New Jersey Transit commuter service.
Winds toppled a truck on Rhode Island's Newport Pell Bridge and prompted officials to close several bridges in the state to commercial vehicles. A tractor-trailer also tipped over on New York's Tappan Zee Bridge, snarling traffic for hours.
The federal government closed all offices in the Washington area for the day. Smithsonian museums also shut their doors.
In the western New York town of Hornell, 30-year-old Anna Stewart was milking the 130 cows of her dairy farm in a barn powered by a generator hooked up to a tractor. Stewart lost power Thursday night. Hornell got more than 14 inches of snow.
"The snow is pretty wet and heavy. It's taken down a lot of lines," Stewart said. "There's more snow than I've seen in quite a few years."
On the tip of Cape Cod, Provincetown resident Andy Towle took video of a 50-foot fishing boat breaking free from its mooring and drifting dangerously toward the rocks.
"I've never seen anything like that," the 50-year-old resident said. "The harbormaster was down there with police, and they didn't know what to do."
___
This story corrects that several bridges in Rhode Island were closed, not all major bridges.
___
Associated Press writers Michael Hill in Albany, New York, Michelle R. Smith in Providence, Rhode Island, Anisha Frizzell in Baltimore, and Susan Haigh in Hartford, Connecticut, contributed to this report.