July 2017 natural gas opened today at $3.28 -- up 1 cent from last Friday.
Farm Diesel is 4 cents lower on the week at an average of $1.98 per gallon.
July 2017 WTI crude oil opened the day at $48.75 -- $1.36 higher on the week.
June 2017 Heating oil futures opened the day at $1.54 -- up 7 cents from our last report.
Propane is a penny lower at an average of $1.24 per gallon regionally.

Farm Diesel -- No state posted a higher farm diesel price this week. Michigan led declines falling 16 cents per gallon as Iowa and Minnesota each softened 7 cents. Six states were unchanged.
This week, front month WTI futures rejected the move above $50 per gallon and fell sharply on news that OPEC and other non-OPEC producers have agreed to extend the production cuts enacted on January 1 of this year. That was not what traders and investors wanted to hear. You can read more detailed thoughts of mine in this week's article titled "OPEC Agrees to Nine-month Extension of Production Cuts". The basic idea is that traders were hoping OPEC would be more aggressive with production cuts, which, thus far have really only served to bring more U.S. production capacity online.
It is possible that OPEC is playing the headline game in hopes of a massive selloff, squeezing U.S. producers who need prices to stay relatively high in order to maintain margins. But U.S. producers have proven their agility in the past several months and in the case that high cost producers have to shut wells down, once prices rebound on that news, the cycle will start again.
There is an outside chance that increasing U.S. crude oil stockpiles could actually encourage domestic refining activity and pressure fuels prices including farm diesel. It seems counter intuitive, and fuel prices do generally follow crude oil prices. The fact is, this week's OPEC announcement does very little to clear the waters, and our outlook remains very uncertain.
That being the case, we will keep an eye on heating oil futures, which have fallen 8 cents since peaking earlier this week. That narrows our farm diesel/heating oil spread and indicates near-term price pressure for farm diesel.


Distillate inventories reported by EIA fell 0.5 million barrels to 146.3 mmbbl. Stocks are currently 4.5 mmbbl below the same time last year.
The regionwide low currently lies at $1.80 in Michigan. The Midwest high is at $2.18 in Illinois.





Farm Diesel 5/26/17


Three Weeks Ago


Previous Week


Change


Current Week

 



Iowa


$1.97


$1.97


-7 cents


$1.90

Iowa



Illinois


$2.16


$2.18


Unchanged


$2.18

Illinois



Indiana


$2.07


$2.10


Unchanged


$2.10

Indiana



Ohio


$1.99


$1.99


-6 cents


$1.93

Ohio



Michigan


$1.95


$1.96


-16 cents


$1.80

Michigan



Wisconsin


$1.92


$1.92


Unchanged


$1.92

Wisconsin



Minnesota


$2.12


$2.12


-7 cents


$2.05

Minnesota



North Dakota


$2.08


$2.08


-6 cents


$2.02

North Dakota



South Dakota


$2.05


$2.05


Unchanged


$2.05

South Dakota



Nebraska


$1.98


$1.98


Unchanged


$1.98

Nebraska



Kansas


$1.95


$1.95


-3 cents


$1.92

Kansas



Missouri


$1.94


$1.94


Unchanged


$1.94

Missouri



Midwest Average


$2.02


$2.02


+-4 cents


$1.98

Midwest Average



Propane -- Only three of the twelve states in our survey moved this week, leaving 9 states unchanged. North Dakota was our only decliner, falling 10 cents as Michigan firmed 4 cents and Missouri added 3 cents per gallon.
National propane stocks are beginning to rebuild. The cooler and wetter than expected end to winter heating season interrupted propane's initial downtrend in early April and LP has been waffling between $1.24 and $1.25 per gallon for a few weeks now. The chart at right indicates that a flat spot in the downtrend is not unheard of and that even with a slight holdup ahead of summertime, prices can still end up around $1.00 per gallon by midsummer.
Export activity has backed off slightly, but there is risk that summer propane prices will be above those of last year. This week's supply build reported by EIA is encouraging and we are in no hurry to book fall/winter propane.


According to EIA, last week, national propane inventories firmed 1.478 million barrels -- now 30.443 million barrels below the same time last year at 43.686 million barrels.
The regionwide low is at $1.05 per gallon in Nebraska, and the regionwide high is in Michigan at $1.46.





LP 5/26/17


Three Weeks Ago


Previous Week


Change


Current Week

 



Iowa


$1.11


$1.11


Unchanged


$1.11

Iowa



Illinois


$1.38


$1.39


Unchanged


$1.39

Illinois



Indiana


$1.35


$1.37


Unchanged


$1.37

Indiana



Ohio


$1.20


$1.20


Unchanged


$1.20

Ohio



Michigan


$1.42


$1.42


+4 cents


$1.46

Michigan



Wisconsin


$1.20


$1.20


Unchanged


$1.20

Wisconsin



Minnesota


$1.17


$1.17


Unchanged


$1.17

Minnesota



North Dakota


$1.19


$1.19


-10 cents


$1.09

North Dakota



South Dakota


$1.09


$1.09


Unchanged


$1.09

South Dakota



Nebraska


$1.05


$1.05


Unchanged


$1.05

Nebraska



Kansas


$1.34


$1.34


Unchanged


$1.34

Kansas



Missouri


$1.42


$1.42


+3 cents


$1.45

Missouri



Midwest Average


$1.24


$1.25


-1 cent


$1.24

Midwest Average