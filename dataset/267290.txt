As promised, I said would look at the newly updated per capita availability numbers with an eye toward the under-performing fresh commodities compared with their peers. I previously highlighted veggies making per capita gains in 2017.
The USDA's updated per capita availability charts for vegetables reveal head lettuce and celery didn’t have great years in 2017. In fact, the last decade or so hasn’t been a picnic for those commodities.
The USDA reports head lettuce per capita availability for 2017 was 13.27 pounds, down 7% from 14.30 pounds in 2016 and off 17% from 15.93 pounds in 2010.
When you look up iceberg lettuce on Google Trends, some of the fastest rising related searches in the last year include:

romaine lettuce vs iceberg
iceberg lettuce vs romaine lettuce
leaf lettuce iceberg
can guinea pigs eat iceberg lettuce
butter lettuce

Meanwhile, the USDA reported celery per capita availability was 4.63 pounds in 2017, down 7% from 4.98 pounds in 2016 and off 24% from 6.13 pounds in 2010.
Fastest rising Google Trends searches for celery in the past year were;

gordon ramsay pickled celery
can cats eat celery
substitute for cream of celery soup
can rats eat celery
how many calories does celery have

Fresh carrot per capita availability was 7.33 pounds in 2017, off 6% from 7.80 pounds in 2016 and down 5% from 7.76 pounds in 2010.
Fastest rising searches for carrots in the past 12 months, according to Google Trends, were:

coding for carrots
three carrots fountain square
carrots on keto
keto carrots
instant pot carrots

Fresh broccoli availability was 7.11 pounds, off 5% from 7.45 pounds in 2016 but up 19% from 5.95 pounds in 2010.
Google Trends reports fastest rising searches for broccoli in the past year were:

broccoli city
broccoli city festival 2018
keto broccoli cheddar soup
keto broccoli cheese soup
instant pot broccoli

 
Leaf/romaine lettuce per capita availability was estimated by the USDA at 12.54 pounds, off 1% from 12.67 pounds in 2016 but up 4% from 12.01 pounds in 2010.
Fastest rising search terms for romaine lettuce, according to Google Trends, were not a marketer’s dream. They were:r

romaine recall
romaine e coli
lettuce recall
e coli
e coli romaine lettuce

 
Fresh potato per capita availability in 2017 was 33.38 pounds in 2017, off 1% from 33.62 pounds in 2016 but off 9% from 36.81 pounds in 2010.
Finally, Google Trends reports fastest rising search terms for potatoes in the past year were:

potatoes in socks
mashed potatoes in instant pot
instant pot mashed potatoes
instant pot red potatoes
air fryer potatoes

Fresh tomatoes showed no change in per capita availability in 2017 compared with 2016, according to the USDA. Per capita availability of 20.21 pounds was off 1% from 2010’s level of 20.55 pounds.
Google Trends fastest rising search terms for tomatoes offered no insight on food trends. They were:

rotten tomatoes last jedi
rotten tomatoes lady bird
shape of water rotten tomatoes
the last jedi
last jedi

 
TK: It takes more than one year to make any conclusions about per capita trends, but the latest update shows there is much work to do by fresh vegetable marketers. Perhaps promoting anything with a keto diet tie-in that also can be cooked in an insta-pot would be a good place to start. And, of course, avoid produce recalls.