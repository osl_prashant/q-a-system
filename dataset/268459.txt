Just as a retailer wouldn’t rely on just an apple variety or two for its category, mango leaders say the category should show lots of variety.
“We’d like to see mangoes be an entire category like apples with multiple varieties and colors,” said Greg Golden, partner in Vineland, N.J.-based Amazon Produce Network.
“That would be my dream. There’s a mango for every mood.”
Golden said he stresses variety information at the retail point of sale.
In the mango variety world, there are generally about half a dozen that U.S. consumers are familiar with, with two standing out.
“Tommy atkins and ataulfo/honey continue to be the most abundant varieties,” said Manuel Michel, executive director of the National Mango Board, Orlando, Fla.
“We haven’t heard of any variety challenges thus far.”
He said this season ought to have good supplies of six main mango varieties: tommy atkins, ataulfo/honey, haden, keitt, kent and madame francis. 
Valda Coryat, director of marketing of the National Mango Board, said it’s important for retailer to have a larger variety mix to allow consumers to experiment as they become more comfortable with the category.
“As consumers become more familiar with mangoes, they are willing to try different varieties and experiment (with) the different flavors of the many varieties available in the U.S.,” she said.
“Retailers are encouraged to offer multiple varieties, sizes and prices to present consumers with more options.”
James Watson, mango commodity manager for Robinson Fresh, Eden Prairie, Minn., said the red mango varieties like tommy atkins and haden continue to represent the greater portion of the variety mix, but he’s seeing yearly increasing popularity in the honey — aka ataulfo — variety.
Golden said he’s also seeing the honey/ataulfo variety gain acceptance in the market.
Golden said mangoes with less fiber, such as the kent and keitt, seem to be more popular at retail also.
“But we have to be careful as an industry not to disparage a variety,” he said. “We try to educate them that there are different varieties — put it on a PLU sticker, that varietial education.”
Chris Ciruli, partner in Ciruli Bros., Rio Rico, Ariz., also sees a shift in variety preferences.
“Each season we see that we are moving more Champagne (ataulfo) and keitt mangoes than prior years with a decline in tommy atkins,” he said.
Robert Schueller, director of public relations for Los Angeles-based World Variety Produce, which markets the Melissa’s brand, said the tommy atkins variety has a good place in the market with its year-round supply compared to the honey/ataulfo, which isn’t available year round yet.
At retail, Schueller also stresses the “more varieties are better” approach.
He said the industry has to educate retailers in addition to consumers about varietal differences, so they don’t get caught up in “a mango is a mango” mentality.
Too often retailers “just like mangoes for their coloration,” Schueller said. “Most don’t see the difference, but there are significant differences from one variety to another.”
As far as the lesser known varieties, the mango board’s Michel said to keep an eye on limited supplies of manila, erwin, and nom doc mai.