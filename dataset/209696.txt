While waiting for more details, some food industry leaders and farm groups were guardedly optimistic about tax reforms proposed in late September by the White House.

In remarks Sept. 27, President Donald Trump said the tax reform plan would be a “once-in-a-generation opportunity.”
Trump said the White House has been working for several months with members of Congress on the framework for tax reform.
To protect small businesses and farmers, Trump said the tax reform plan would eliminate the estate tax, commonly referred to as the death tax.
That won support from the American Farm Bureau Federation’s president Zippy Duvall.
“Farm Bureau is encouraged to see that this framework includes important principles such as lower tax rates for individuals who own businesses, elimination of the death tax and some business interest deductibility,” he said in a statement. 
Duvall said the group wants to be sure the reform package lowers effective tax rates for farmers and includes permanent tax provisions such the continuation of cash accounting and like-kind exchanges, unlimited stepped-up basis and lower capital gains taxes.
Trump said the plan would cut corporate tax rates to no higher than 20%, down from the current corporate tax rate of 35%.
“This is a revolutionary change, and the biggest winners will be the everyday American workers as jobs start pouring into our country, as companies start competing for American labor, and as wages start going up at levels that you haven’t seen in many years,” Trump said. Lower U.S. tax rates would also bring back trillions of dollars that U.S. corporations have banked overseas, he said.
Additionally, the tax code would give businesses more reason to invest by allowing companies to fully write off the cost of equipment in the year they buy it.
Industry wait and see
The step toward tax reform drew support from the Food Marketing Institute and the Grocery Manufacturers Association.
Tom Stenzel, president of United Fresh Produce Association, said Sept. 28 that the group was waiting for more details before commenting. Likewise, Dennis Nuxoll, vice president of federal government affairs for Western Growers, said the group was looking forward to seeing more details of the plan and had no immediate comment.
“It is hard for us to predict, without more information, how this will impact individual companies because they all have different ways they are organized,” said Bob Whitaker, chief science and technology officer for the Produce Marketing Association. He said PMA members will be looking at the plan closely to understand what it will mean to their businesses.
Details
In other features, Trump said the plan is based on cutting taxes for middle income Americans, with the first $12,000 of income earned by a single individual tax free, with no taxes for a married couple on their first $24,000 of income. 
Taxable income will be subject to three tax rates of 12%, 25% and 35%. The plan offers an expanded child tax credit for children under the age of 17.
Trump said the plan would eliminate most itemized deductions that primarily benefit the wealthiest taxpayers. 
“Our framework will make the tax code simple, fair and easy to understand, finally,” he said.