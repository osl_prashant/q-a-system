The Cattlemen's Update will provide Nevada's cattle producers and ranchers with the most current information on topics important for their businesses Jan. 9 ‚Äî 13, 2017, at various locations across Nevada. The annual event is being presented by the
 
University of Nevada, Reno's College of Agriculture, Biotechnology and Natural Resources
 
and University of Nevada Cooperative Extension, with support from the Nevada Department of Agriculture, the U.S. Department of Agriculture Risk Management Agency and other local sponsors.
 
"This is a great opportunity for livestock producers in Nevada to interact with University faculty to learn about ongoing research and extension education programs and to hear updates from the Nevada Department of Agriculture," said Dean Bill Payne from the College.
The Cattlemen's Update provides current research-based information about important management practices and issues that may affect the efficiency, productivity, profitability and sustainability of cattle production businesses. Each day, the program is held at a different location in the state, where experts discuss pertinent topics with participants. Sessions are approximately three to four hours, and the cost is $20 per ranch per location attended, which includes lunch or dinner and the "Red Book" recordkeeping guide for cattlemen.
Experts from the University and Nevada, Reno; Nevada Department of Agriculture; and Nevada Cattlemen's Association will discuss the following topics:

College Update
New Faculty Introduction in Livestock Production
2017 Climate Update and Status of Drought Monitor
Rangeland Monitoring App Update
Flexible Grazing Management for Riparian Functions and Recovery
Current and Emerging Livestock Disease and New Feed Directives
Efficiency Relative to Cow Size
Update on Paternity Study and Cheatgrass Project
Update from Nevada Cattlemen's Association
Veterinarian Update
Sponsor Updates

Here is this year's schedule. Times given are the registration times, and the programs begin 30 minutes after registration.
Jan. 9, 10 a.m., Reno, lunch provided
Washoe County Cooperative Extension Office, 4955 Energy Way
This session only will also be offered via interactive video at University of Nevada Cooperative Extension offices in Caliente, Logandale, Lovelock and Eureka.
Jan. 10, 10 a.m., Wellington, lunch provided
Smith Valley Community Hall, 2783 State Route 208
Jan. 10, 5:30 p.m., Fallon, dinner provided
Fallon Convention Center, 100 Campus Way
Jan. 11, 5:30 p.m., Ely, dinner provided
Elks Lodge, 694 Campton St.
Jan. 12, 12:30 p.m., Elko, dinner provided
Great Basin College Solarium, 1500 College Parkway
Jan. 13, 10 a.m., Winnemucca, lunch provided
Humboldt County Cooperative Extension, 1085 Fairgrounds Road
Jan. 9, 5:30 p.m., Sierra Valley, Calif., dinner provided
Sierra Valley Grange #466, 92203 Highway 70
For more information, contact Mineral County Extension Educator Staci Emm at
 
emms@unce.unr.edu
 
or 775-475-4227. Persons in need of special accommodations or assistance should call or notify Emm at least three days prior to the scheduled event.