Ham Produce adding melons
Ham Produce Co. Inc., Snow Hill, N.C., plans to diversify its offerings in 2017, marketing director J.D. Wyborny said.
"We are planting 250 acres of watermelon this year with the goal of having a year-round program," he said.
 
N.C. commission gets new look
The North Carolina Sweet Potato Commission, Benson, is updating its brand, including its logo and website.
"The brand refreshment will renew the commission's look, feel and value, making it the most approachable and refreshed in consumer mindsets," said Kelly McIver, executive director.
 
Southern Produce marks milestone
Southern Produce Distributors Inc., Faison, N.C., is celebrating its 75th year in business.
"We've been around a long time," vice president Kelly Precythe said, who added that the company will be touting the milestone this year at its trade show booths. "We've had a lot of good partnerships with retailers. It's not easy to stay sustainable in agriculture that long."
Precythe said the company also is planning to have a significant increase in summer vegetable acreage, starting in May with zucchini and yellow squash. Southern also will have increased acreage for butternut, acorn, spaghetti and buttercup squash, starting in June.
"We used to be huge in mixed vegetables," Precythe said. "With the growth of sweet potatoes, we got away from it somewhat. We're going after it in a big way this year."
 
Council to court food writers
Representatives of the U.S. Sweet Potato Council will be talking about sweet potatoes with dozens of writers during the Editors Showcase March 21 in New York, executive director Kay Rentzel said.
"It's an opportunity to meet with food editors from consumer publications and blogs and sell them on sweet potatoes," Rentzel said. "It' something new for us, and we're going to give it a shot."
The aim will be to persuade food writers to publish feature articles and/or recipes about sweet potatoes, Rentzel said. The council will have a booth that will feature product samples, dietary and health information and tips for storage and handling.
 
Vick introduces steamer bag
Vick Family Farms, Wilson, N.C., has launched a new line of microwaveable bags. Carolina Steamables is a 24-ounce steamer bag which takes about eight minutes to microwave.
"The demand for this item seems to be growing as it attracts today's consumer that has a busy schedule and not much time for preparing a meal," partner Charlotte Vick said. "Anything that saves time and is healthy will be an attractive item today."
 
Wayne E. Bailey hires salesman
Jay White has joined Wayne E. Bailey Produce Co., Chadbourn, N.C., as salesman for the Midwest region. President George Wooten said White is new to the produce industry but has a background in sales and marketing.
White, who joined the company in December, previously spent three years as a digital marketing specialist for Ford Motor Co.