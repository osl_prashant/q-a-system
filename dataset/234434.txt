BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Monday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMar                              121.90  Up    .90Mar      119.50  121.05  119.50  120.50  Up   1.05May                              124.05  Up    .90May      121.55  122.70  121.25  121.90  Up    .90Jul      123.70  124.80  123.35  124.05  Up    .90Sep      126.00  127.05  125.60  126.30  Up    .95Dec      129.35  130.35  128.95  129.70  Up   1.05Mar      132.65  133.70  132.30  133.05  Up   1.10May      135.20  135.70  134.30  135.05  Up   1.10Jul      136.75  137.45  136.20  136.85  Up   1.10Sep      138.35  139.05  138.35  138.50  Up   1.10Dec      140.85  141.05  140.85  141.05  Up   1.15Mar      143.45  143.55  143.20  143.55  Up   1.10May                              145.30  Up   1.10Jul                              147.00  Up   1.10Sep      148.10  148.60  148.10  148.60  Up   1.10Dec                              151.00  Up   1.20