## Q&A System
### Summary
* Given `n documents` and a query, find the correct output for the query and the whole paragraph where it exist.
* TFIDF and Hugging Face (distillBEET )BERT Model have been used for finding the paragraph and outputing the correct results respectively.
* Basic preprocessing is done like stemming, removal of stop words, n_grams, etc.
* We can choose `k_top_outputs` for the result if required, by default `k = 1`.

![architecture](Q%26A%20system%20Architecture.svg)  

The executable code is in **Q&A_runner.py**.  
Use this [Colab Link](https://colab.research.google.com/drive/1uBvDlWobdNHr6PncToyKUfpEfrR8NjGm) to run the program online.  
The dataset that used to train is also available [here](https://drive.google.com/drive/folders/1Q5z1u3AugB8IJqh0AMchXZLUEuFPKW2q?usp=sharing).  

NOTE: This code can be used for any other dataset, the file ** Demo_HuggingFace_Q&A.ipynb** can be used to train on the new dataset and then use the required changes in runner code.

