Bryan Thornton, general manager of Coosemans Atlanta Inc. has some simple advice where the specialty produce business in Atlanta is concerned: “Adapt or die.”
Coosemans has built a business on specialty produce over the years, and its Atlanta branch has followed form, Thornton said.
“You have to adapt to your environment and people in your area, and we’ve been able to do that,” he said.
Product focuses can change by the season, and new items come in every year, it seems, Thornton said. “You’re going to see a lot of heirloom varieties of tomatoes or lettuces that are hot items,” he said. “They will move out seasonally and as it cools off, it goes toward turnips and beets for warm salads.”
The key is to stay on top of the changes, he said.
It’s not specific to Atlanta, said Jake Simpson, chief operating officer at Forest Park, Ga.-based Athena Farms.
“Specialty produce has continued to grow nationwide, and Atlanta is no different,” he said. “Atlanta has a lot of high-end restaurants and they have to promote the specialty side.”
It’s also important to be cognizant of the specialty category’s limitations, said David Collins III, president of Phoenix Wholesale Foodservice Inc. in Forest Park.
“Because of this specialty nature, it can be limited at times. On the retail side, sometimes it’s harder for them to embrace items that may be not as common.”
Collins said foodservice operators are more likely to want items such as micro-sprouts or bean shoots that make for a nice presentation on the plate, while retailers would be more likely to experiement with newer apple and melon varieties.
Local growers will embrace new varieties, too, Collins said.
“The farmers like new boutique type items because margins they make on those are higher than on regular red delicious apples,” he said. “Maybe your braeburns or mutsus or little varietals that you might see from time to time.”