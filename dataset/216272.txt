Several holiday traditions involve farmers—chopping the tree and horse-drawn sleigh rides, for example. But the residents of Centralia, Mo., have an entire parade dedicate to farmers and the rural community they live in.
The 11th annual Lighted Tractor Parade, sponsored by the local Young Farmers group, brought more than 45-minutes of machines, floats and fun for the thousands of people that lined the cold streets of downtown. 
Thousands of local residents, as well as visitors from St. Louis and Kansas City traveled to line the streets of town.
Favorites, such as the Thomas the Train tractor and the wind-up forage harvester toy, were met with two new favorites.
"Blue Christmas" with New Holland tractor and a forage mixer brought Santa and Elvis, singing his famous song. (see photo above)

And whether you drive green or red, we all need a little tug to get us in the holiday spirit once in a while. 
Want to watch the full lineup? Check out this drone footage. The equipment is lined up at the local MFA and wind through downtown.