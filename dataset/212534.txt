The California Giant Foundation, through Tour De Fresh pledges, is setting a goal of at least $156,000 to privately fund 50 salad bars in schools across the country.This year’s ride includes tracking devices — sponsored by Locus Traxx/Emerson — for each rider can be tracked in real-time during the Tour de Fresh.
The ride begins on July 25 at Pismo Beach, Calif., into Lake Nacimiento on July 26 and ending in Monterey on July 27, prior to the Produce Marketing Association’s Foodservice Conference and Expo, according to a news release.
“We’re excited to give our riders a new experience through Northern California this year, while also allowing friends and family to watch in real-time from their phone or computer,” Cindy Jewell, vice president of marketing for California Giant Berry Farms and The California Giant Foundation, said in the release.
Here are some ways to encourage fundraising over the next two months:
Friends and family: ask personally for support and post on social media using this link: http://www.tourdefresh.com/riders.php.
Company incentives: offer raffles, prizes, team lunches, time off, for those who donate.
Industry partnerships: ask for support from buyers, suppliers, etc.
Business partnerships: ask local businesses to donate tax-free.
For more information about the Tour de Fresh, visit www.tourdefresh.com.