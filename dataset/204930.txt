Cressleaf groundsel is on the rise in fields across Ohio dueto a mild winter that's allowed many weeds to get an aggressive start this growing season, said a weed scientist in the College of Food, Agricultural, and Environmental Sciencesat Ohio State University.And while cressleaf groundsel isn't a new problem, the weed, typically found in no-till corn and soybean fields that have not yetbeen treated with burndown herbicides, is being reported in fairly high numbers in hay and wheat fields, saidMark Loux, an Ohio State University Extensionweed specialist.
OSU Extension is the statewide outreach arm of the college.
Cressleaf groundsel is a winter annual that emerges in late summer or fall and infests late-summer seedings of forages and hay. It can be a problem for livestock producers because it is poisonous to cattle, horses, goats and sheep, Loux said.
The weed is also poisonous to humans, he said.
Focus on management
"This year so far has definitely been a big year for cressleaf groundsel, as the mild winter allowed more of the weed to survive," Loux said. "It's caught many hay producers by surprise.
"However, applying herbicides to hay fields now likely won't reduce the risk of toxicity in animals. And it's too late for wheat growers to apply any herbicide to their wheat crops."
Cressleaf groundsel has a hollow and grooved stem, is purplish in color and has yellow sunflower-like flowers. The winter annual is best controlled with herbicides in the fall or early spring when the plants are smaller and more susceptible, Loux said.
"But at this point, the plant is flowering and there are many more of them," he said. "So growers are going to have focus on managing the weeds to prevent them from going to seed and work to avoid letting the weed get into the supply chain for straw when baling.
"Producers have to be careful to ensure that it doesn't get into the food chain to poison animals."
Timely mowing
That means taking steps to mow infestations in time to prevent seed production, and to know what the risk of toxicity in hay or straw based on the level of cressleaf groundsel infestation, Loux said.
"Drying or ensiling the plants during the hay or straw making process doesn't reduce the toxicity of cressleaf groundsel," he said.
Producers should also avoid harvesting areas of the field that have high concentrations of cressleaf groundsel, he said. Or, instead of leaving the plant residue in the field, they could instead bale and discard hay or straw from those areas, Loux said.
More information on cressleaf groundsel, including how to identify it and manage it, can be found on Ohio State's weed science website.A factsheet can be found on the "Weeds" pull-down menu on the site under "Other weeds."