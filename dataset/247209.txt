General Mills, Annie's Mac & Cheese tap South Dakota farm
General Mills, Annie's Mac & Cheese tap South Dakota farm

By STEVE KARNOWSKIAssociated Press
The Associated Press

MINNEAPOLIS




MINNEAPOLIS (AP) — General Mills announced a deal Tuesday to create South Dakota's largest organic crop farm as the company works to secure enough organic ingredients to meet growing consumer demand worldwide.
Gunsmoke Farms will convert 34,000 acres — more than 53 square miles — near Pierre to organic by 2020, where it will grow organic wheat for General Mills' popular Annie's Macaroni & Cheese line.
General Mills, which is guaranteeing a market for the wheat, is working with Madison, Wisconsin-based Midwestern BioAg to develop the crop rotation and soil-building program needed for such a large farm to go organic.
"We're kind of obsessed with soil," Carla Vernon, president of General Mills' Annie's unit in Berkeley, California, told The Associated Press ahead of the announcement. "And that's because we know the power of soil is big."
Golden Valley, Minnesota-based General Mills, like many other food companies, has ambitious environmental goals, and like other big industry players it has bought smaller brands and tweaked its own products to appeal to consumers who want more organic and natural products. It wants to double its organic acreage by 2020 and to cut greenhouse gas emissions 28 percent by 2025 throughout its supply chain all the way down to consumers, because it believes climate change will be bad for business. The company's chief sustainability officer, Jerry Lynch, said it's on pace to meet its organic acreage goal well ahead of schedule.
Lynch said the project is one of several sites where General Mills is pilot-testing the same regenerative practices. The company will measure results in sequestering carbon in the soil, increasing biodiversity on the landscape and bringing socio-economic benefits to local communities.
Gunsmoke Farms will also carve out around 3,000 acres of pollinator habitat in cooperation with the Portland, Oregon-based Xerces Society. General Mills and Xerces announced a partnership in 2016 to add more than 100,000 acres of bee and butterfly habitat on or near existing crop lands.
General Mills bought Annie's — a brand known for its rabbit logo and bunny-shaped snacks — in 2014 for $820 million. While Gunsmoke Farms will become a huge supplier, Vernon pointed out that Annie's also works with small farms. It's partnering now with two farmers in Montana who use regenerative practices, and it will roll out single-source, limited-edition organic macaroni and cheese and bunny graham crackers this month.
South Dakota doesn't have much organic agriculture now — just 86 certified farms with 115,780 total acres during the 2016 growing season, according to U.S. Department of Agriculture statistics. And a little more than half that is pasture or rangeland rather than crop acres.
Gunsmoke Farms is owned by San Francisco-based TPG, a private global investment company with an interest in sustainability. TPG bought the farm recently from Fargo, North Dakota-based R.D. Offutt Co, best known as a potato company, which used it primarily to grow conventional wheat, corn, soybeans and sunflowers. Midwestern BioAg will work with local managers on the three-year process of converting the land to organic.
Gary Zimmer, founder of Midwestern BioAg, said it's his biggest project yet in 30 years of converting land to organic. He said the land at Gunsmoke Farms needs natural waterways re-established, as well as cover crops, no-till practices and the addition of lots of trace minerals.
Since the area is fairly dry, he said, it needs deeply rooted plants to trap rainwater and to build up organic matter in the soil. The crop rotation will include legumes such as peas, clover and alfalfa, which add nitrogen to fertilize the soil.
"I think everybody's going to be watching it, so we have to make sure we do a lot of things right," he said.