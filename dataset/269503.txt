Report: Kansas winter wheat struggling amid lack of moisture
Report: Kansas winter wheat struggling amid lack of moisture

The Associated Press

WICHITA, Kan.




WICHITA, Kan. (AP) — The latest government snapshot shows the Kansas winter wheat crop continues to struggle amid the lack of soil moisture.
The National Agricultural Statistics Service reported Monday in its weekly update that 46 percent of the wheat in Kansas is rated as poor or very poor. Another 42 percent is in fair shape, while 11 percent is in good and 1 percent is in excellent condition.
About 22 percent of the state's wheat has now jointed, well behind the 51 percent that is average for this time of year.
Corn planting in Kansas is just 6 percent complete.
The agency also is reporting that topsoil moisture is 72 percent short to very short across Kansas.