When USDA's weekly crop condition ratings are plugged into Pro Farmer's weighted Crop Condition Index (CCI; 0 to 500 point scale, with 500 representing perfect), it shows the HRW crop declined by 19.1 points since it went into dormancy in late November. This was led by an 8.6-point drop in the Kansas crop, with much lesser deterioration noted in Oklahoma and Texas.
Meanwhile, the SRW crop improved marginally from late November, but is rated 4 points below year-ago levels. Crop ratings showed little net change across all states listed below.




 
Pro Farmer Crop Condition Index





HRW




This week



Nov. 27, 2016



Year-ago


 


SRW




This week



Nov. 27, 2016



Year-ago





Kansas *(39.07%)

125.42
134.02

135.74


Missouri *(9.94%)

36.57
35.08

35.37




Oklahoma (10.67%)

34.36
36.82

39.84


Illinois (9.79%)

35.94
36.82

36.91




Texas (9.94%)

32.32
32.62

36.12


Ohio
			(10.12%)

40.08
41.70

35.05




Colorado (10.39%)

32.73
34.81

31.21


Arkansas (3.84%)

14.37
13.07

19.72




Nebraska (7.08%)

24.01
24.30

24.83


Indiana (5.68%)

21.47
21.30

21.35




S. Dakota (6.26%)

21.71
21.65

20.68


N. Carolina (7.72%)

28.17
26.70

31.30




Montana (10.87%)

39.47
43.82

40.61


Michigan (10.70%)

38.84
40.37

32.90




HRW total

328.79
347.88

355.24


SRW total

372.79
372.04

376.81





* denotes percentage of total national HRW/SRW crop production.
Following are details from USDA's National Ag Statistics Service (NASS) crop and weather reports for key HRW wheat states:
Kansas: Will update when available.
Oklahoma: Multiple scattered showers and storms last week brought needed drought relief across most of Oklahoma, even though South Central and East Central rainfall totals were an inch below normal. According to the OCS Mesonet, the drought situation significantly improved, with rainfall averaging 2.28 inches statewide. Temperatures were relatively cooler, averaging in the mid 50s. Drought conditions were rated 78% moderate, down 3 points from last week and 36% severe, down 10 from last week. Topsoil and subsoil moisture conditions were rated mostly adequate to short. There were 3.7 days suitable for fieldwork.
Winter wheat jointing reached 66%, up 6 points from normal.
Texas: East Texas, Central Texas, the Edward Plateau, and the Plains received between 1 and 2 inches of rain, with isolated areas recording upwards 5 inches of precipitation. Hail and strong winds accompanied the rains. High winds in the Blacklands caused damage to the crops. The weather only allowed for 4.8 days suitable for fieldwork.
Winter wheat condition was rated 79 percent fair to good. Producers were optimistic for improved conditions with the widespread precipitation. Wheat began to head out in the High Plains and was entering the ripening stage in South Texas.
Following are details from USDA's NASS crop and weather reports for key SRW wheat states:
Ohio: There was 1 day suitable for fieldwork in Ohio during the week ending April 2, according to Cheryl Turner, Ohio State Statistician with the USDAs NASS. Soil conditions have been too wet for planting and field work in most areas, but some spring tillage, burndown, anhydrous applications, and topdressing of wheat was possible on lighter soils. Soil temperatures are higher than normal due to the mild winter.
Wheat is doing well despite a few reports of winter damage. It is too early to tell if recent frost will have a negative effect on the fruit crop.
Michigan: There were 1.1 days suitable for fieldwork in Michigan during the week ending April 2, 2017, according to Marlo Johnson, Director of the Great Lakes Regional Office of the NASS. Spring showers have kept the ground too waterlogged for planting in most areas. The cold, damp weather was also tough on the newborn livestock. Some farmers managed to accomplish a minute amount of fieldwork. While planting was out of the question, many kept busy hauling manure, cleaning barns/brush, pruning trees, prepping equipment, and performing necessary repair work. In certain areas of the state, sugarbeet planting had begun.
The winter wheat continued to green up, but remained soggy; some fields were top dressed with fertilizer.
Missouri: Rain throughout the region delayed planting of corn. There were 1.1 days suitable for fieldwork for the week ending April 2. Temperatures averaged 52.7 degrees, 2.7 degrees above normal. Precipitation averaged 1.43 inches statewide, 0.39 inches above normal. Topsoil moisture supply was rated 4% very short, 17% short, 55% adequate, and 24% surplus. Subsoil moisture supply was rated 7% very short, 26% short, 60% adequate, and 7% surplus.
Corn planting was 1% complete, 2 percentage points behind the previous year and the 5-year average. Winter wheat condition was rated 2% poor, 35% fair, 56% good, and 7% excellent.