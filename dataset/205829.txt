Farm Diesel

Farm diesel is 6 cents higher on the week at $2.00.
With this week's price strength, our appetite for risk is exhausted and we advise subscribers fill remaining harvest farm diesel needs.
Distillate supplies were unchanged last week, now 4.8 million barrels below the year-ago peg.
Our farm diesel/heating oil futures spread is at 0.38 which is a strong indication of pending near-term farm diesel price support.
Click here to view our official Inputs Monitor Farm Diesel advice.

 
Propane

Propane is 2 cents higher at $1.18 per gallon this week.
Only half of the states in our survey were unchanged this week after last week when all were unchanged.
Propane supplies firmed 2.934 million barrels on the week but remain 23.962 million barrels below year-ago.
Prepare to book harvest and winter propane needs soon.

 
 
 
 




Fuel


8/7/17


8/14/17


Week-over Change


Current Week

Fuel



Farm Diesel


$1.94


$1.94


Unchanged


$2.00

Farm Diesel



LP


$1.16


$1.16


+2 cents


$1.18

LP