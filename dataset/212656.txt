Urban Organics, an aquaponics grower in St. Paul, Minn., is adding nine blends of greens to its retail line.“We’re proud to expand our greens offerings — thanks to the tremendous positive response to our first urban farm launch three years ago,” Kristen Haider, co-founder, said in a news release. “By farming vibrant produce and sustainably-raised fish in the heart of the city, our aquaponics operations create a new way to connect urban consumers with nutritious, great-tasting food.”
The five-ounce clamshells will be available at Hy-Vees in Eagan, and Savage, Minn. 
The available blends are:
Twin Kales — green and red kale;
Umami Blend — arugula, bok choy and green kale;
(Finally) Spring Mix — arugula, bok choy, green leaf, romaine, kale, red leaf and swiss chard;
Just Romaine — green romaine;
Classic Duo — green romaine and red romaine;
Patio Mix — green leaf, red leaf, arugula and swiss chard;
Rosé Blend — red kale, red leaf and red romaine;
River City Mix — green romaine, red romaine and swiss chard; and
Just Arugula — arugula.
Urban Organic has an 87,000-square-foot greenhouse in Schmidt, Minn., which grows 475,000 pounds of produce yearly.