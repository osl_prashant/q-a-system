Alfalfa may start growing in the spring while some freezing nights are still occurring.  This has caused many management questions among farmers, writes Dan Undersander, professor at the University of Wisconsin.
Alfalfa is tolerant of cold temperatures.  To make the best management decisions, we must understand the growth and biology of alfalfa.  A few alfalfa growth principles to keep in mind:

Temperatures in the 25°F to 30°F range may cause some leaf deformation for those leaves in early development stages, but earlier and later leaves will not be affected.
Night time temperatures must fall to 24°F or lower for four or more hours to freeze the alfalfa meristem at the top of the plant stem. This means that temperatures at freezing or just below (28°F to 32°F) will not damage the alfalfa.  In fact, we can actually have snow with no damage to growing alfalfa.
The only way to tell if alfalfa is damaged from a cold night is to wait two to four days to determine if the leaves are wilted or blackened. Unless this damage is present, there is no frost injury. Damage will occur mainly to the top of the growth since that is most exposed to the cold temperatures.


If leaf edges only are blackened or show signs of "burn," damage is minimal with little to no yield loss and nothing should be done. Wait to harvest at normal time.
If only a few entire leaves are damaged but not the bud, yield loss will be minimal and nothing should be done. Wait to harvest until normal time.
If the entire stem top (some leaves and bud) is wilted and turns brown, then the growing point (bud) has been killed by frost and that stem will not grow any taller. However, axillary shoots may develop at leaf junctures on the stem.  In addition, new growth will occur from developing crown buds.  When entire tops are frosted significant yield loss will occur.
	
If the growth of frozen stems is too short to justify harvest, do nothing and new shoots will develop from crown and axillary buds. Yield will be reduced and harvest will be delayed while the new shoots develop.
If the growth of frozen stems is sufficiently tall to be economic to harvest (20" or more), do so. There is no toxin in the frozen topgrowth, and it will provide good high-quality forage.  Mow immediately and harvest as normal.  Regrowth will be slow and some total season yield loss will occur.  After harvest ensure that soil fertility is adequate for good growth.  Letting the next cutting grow to first flower will improve stand condition.


To learn more about growing alfalfa click here.