PHILLIPS STATION, Calif. (AP) - California water officials tromped through long-awaited fresh snowdrifts in the Sierra Nevada mountains Monday, but a welcome late-winter storm still left the state with less than half the usual snow for this late point in the state's important rain and snow season.
Runoff from snow in the mountains historically provides Californians with nearly a third of their water for the whole year. Monday's snow surveys in the mountains by state water officials, with news crews in tow, is one of several closely watched gauges of how much water California cities and farms will have.
Plunging a rod into a snow drift, snow-survey chief Frank Gehrke measured 41.1 inches (104.4 centimeters) of snow Monday, almost all of it laid down by a heavy winter storm that rolled in Wednesday.
On Monday, the Phillips Station measuring location was up to 39 percent of the historical average for the date, compared to just 7 percent of its usual snow before the storm dropped up to 8 feet 2.4 meters) of snow, Gehrke said. Across the Sierra, the state was at 37 percent of normal snowfall as of Monday.
"Of course we don't know what the rest of the month is going to bring," Gehrke said. "But it is a much rosier, happier picture than it was a week ago."
California had accumulated less than a quarter of its normal snowpack for the year before last week's storm. By February, most of Southern California was back in drought, owing to a dud of a rain and snow season so far this year.
It would take six more storms to bring the state up to its normal winter precipitation by April. The odds of that happening are about one-in-50, the National Weather Service cautioned.
March is typically the last month of the rain and snow season in the state.
California emerged only last year from a historic five-year drought that forced mandatory water conservation for cities and towns, dried wells, and caused massive die-offs of trees and many other native species.
The Los Angeles-based Metropolitan Water District, the country's largest urban supplier of water, plans to vote in April on increased funding for conservation programs, spokeswoman Rebecca Kimitch said.
"One storm isn't going to ... make up for what has been a very dry few months," Kimitch said.
California's rainy season is often this kind of a cliffhanger, Daniel Swain, a climate scientist at the University of California, Los Angeles, said last month.
The state is dependent on a handful of significant storms for its water, so things can turn around quickly, he said.
California's reservoirs are at 106 percent of their historical average for this point in the year thanks to last year's rains, said Chris Orrock, a spokesman for the state Department of Water Resources.
While the heavy snows in the Sierra Nevada are the main gift from the latest storm, it helps that arid Southern California got doused as well, Orrock said.
Rain in Southern California rain means reservoirs get filled and vital below-ground natural reservoirs depleted during the drought are replenished.
 
Copyright 2018, The Associated Press