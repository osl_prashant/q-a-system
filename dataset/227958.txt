Planters in parts of Texas are already rolling, but planting seems far away for parts of the Corn Belt covered in snow, ice or flood waters.

The USDA has planting on its mind, and during Thursday’s Agricultural Outlook Forum in Arlington, Va., the agency released its acreage estimates for 2018.

According to the USDA’s acreage estimates, corn and soybeans will be split down the middle with 90 million acres going to corn and 90 million acres going to soybeans.

Wheat acres are believed to increase to 46.5 million acres. Chip Nellinger of Blue Reef Agri-Marketing, Inc., thinks that $7 spring wheat could be one reason for secured acres in the north.

Cotton is also estimated to increase one million acres from the USDA’s 2017 estimate of 12.6 million acres. This year, estimated cotton acreage is at 13.3 million. Nellinger says it isn’t as simple as soybean acres stealing from corn.

“It’s not just beans that is competing for corn right now,” he said on AgDay. “It’s some other crops as well.”

There are some analysts who believe soybean acres could topple King Corn’s reign, and up to 91.5 million acres could be planted.

“If you get outside of heavy livestock areas or areas where there’s a lot of ethanol plants, producers are actually raising good enough soybean yields of the kind of prices we’ve seen,” said Matt Bennett, owner of Bennett Consulting.

Soybean prices have rallied off the South American weather scare; however, the story in exports haven’t been favorable toward soybeans. Nellinger thinks prices will drag if more than 1 million acres are planted.

“Even though demand’s down a little bit, it’s still very large,” he said. “The unknown here is how short is the crop.”

Hear Nellinger’s full thoughts on AgDay above.