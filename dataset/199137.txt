The WVC Women's Veterinary Summit (WVS) will be held July 17 and 18 at the Oquendo Center in Las Vegas. The two-day event is expected to draw over 150 participants who will have the chance to hear from inspirational speakers, learn skills outside the lab and design a road map for personal and professional success."We put a lot of energy into this curriculum," said David Little, CEO of WVC, "and consulted with partners such as the Women's Veterinary Leadership Development Initiative (WVLDI) to ensure topics covered were of the utmost relevance to the audience. We couldn't be more excited to present this event to the veterinary community."
According to the Association of American Veterinary Medical Colleges, currently 78 percent of veterinary students are women. The WVS was developed to speak to this rising demographic, across topics of relevance to both young and seasoned professionals.
WVC has rounded up exceptional leaders in their fields‚Äîincluding keynote speakers Carey Lohrenz, the first female F-14 Tomcat Fighter pilot in the U.S. Navy, and Sharon Stevenson, co-founder and managing director of Okapi Venture Capital‚Äîto speak on a wealth of topics, such as communication skills, time management, financial security and legal considerations for practice owners and partners. Other speakers include:
¬? Kathleen Ruby, PhD, Washington State University
¬? Karen Bradley, DVM, Owner Onion River Animal Hospital
¬? Carla Gartrell, DVM, JD, DACVIM (SAIM), Associate Dean Midwestern University
¬? S. Dru Forrester, DVM, MS, DACVIM (SAIM), Hill's Pet Nutrition
¬? Ellen Lowery, DVM, PhD, MBA, Hill's Pet Nutrition
¬? Valerie Ragan, DVM, Virginia-Maryland College of Veterinary Medicine
WVC would like to thank sponsor Hill's Pet Nutrition that makes events like the WVS possible.
For a full schedule of events or to register for WVS, visit the WVC website.