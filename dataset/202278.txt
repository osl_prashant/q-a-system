Hog farms across Eastern North Carolina are continuing to assess the damage caused by Hurricane Matthew and the extensive flooding brought by the storm. There are more than 2,100 permitted hog farms in North Carolina and the vast majority of them faced tremendous challenges caused by the storm. Fortunately, the damage caused by the storm up to this point has been relatively minimal.North Carolina hog farms are surviving Hurricane Matthew. Some farms are flooded, but no hog lagoon breaches reported. There remains a serious threat to life and animals caused by additional flooding, and the pork industry continues to work tirelessly to protect hog farms, animals and the environment.
The Status of Hog LagoonsThrough 2 p.m. Thursday, there have been no reported breaches of hog lagoons. It is estimated that six farms across Eastern North Carolina have lagoons that have been inundated with flood waters. This is a situation where the lagoons did not overflow or breach, but the lagoon is underwater as a result of flood waters coming onto the property.
There has been one reported incident involving a failed flush tank pipe. This issue was found immediately once the power was restored and reported to the Department of Environmental Quality. The farm reported a discharge of approximately 500 gallons. It did not reach the waters of the state and the wastewater was pumped back into the lagoon.
The Waterkeeper/Riverkeeper Alliance continues to stoke fears about potential breaches and is deliberately exaggerating the environmental impact from hog lagoons. For example, one reported breach photographed by the Riverkeeper occurred on a farm in Hookerton that hasn't been in operation for five years. The reality is that hog lagoons across North Carolina have survived the historic flooding with minimal incidents to this point.
Understanding the Difference Between Breaches vs. InundationWhen a hog lagoon is breached - the lagoon walls give way and can no longer hold back the wastewater - the contents of the lagoon are typically emptied into surrounding fields.
By contrast, a lagoon that is underwater still remains intact. The floodwater runs over the lagoon and carries away only a small portion of the wastewater. Most of the wastewater remains in the lagoon and the environmental impact is greatly minimized.
Providing Context: The Impact of Hog Lagoons vs. Other Sources of PollutionWhile an extremely small number of North Carolina's 2,100 hog farms have been flooded, more than a dozen municipalities in the region have spilled untreated human waste into the waters. This includes a 6.2 million gallon spill in Dunn, a 1 million gallon spill in Elizabeth City, a 1 million gallon spill in Washington, and numerous others.
In addition, flooding results in water pollution from a host of other sources, including contaminants and chemicals from area residents and industries.
Donald van der Vaart, Secretary of the NC Department of Environmental Quality, said of potential environmental issues related to flooding: "The good side of this is there's a tremendous amount of water flowing through this area, so any exchange is going to be vastly diluted."
Update on Animal MortalityThrough 2pm Thursday, it appears that fewer than 3,000 swine were killed during Hurricane Matthew. Only one farm lost any swine due to flooding.
Significant Improvements Since Hurricane Floyd The pork industry has worked closely with the State of North Carolina to mitigate the risk of pollution during extreme weather events such as Hurricane Matthew.
More than 100 hog lagoons located within the 100-year floodplain in Eastern North Carolina have been closed since Hurricane Floyd. Conservation easements were acquired that prevent the land being used as feedlots, lagoons or as sprayfields for liquid fertilizer. The easements were acquired through grants made by the Clean Water Management Trust Fund.
An additional 231 out-of-service lagoons in Eastern North Carolina were closed using Environmental Enhancement Grant money donated to the NC Foundation for Soil and Water Conservation by Smithfield Foods.
These steps have successfully reduced the risk to water quality from flood events.
Floyd vs. Matthew: Comparing the DamageThe flooding caused by Hurricane Matthew rivals - and in some cases exceeds - the damage by Hurricane Floyd. The impact on hog farms from this storm has been much less significant.












Impact on Hog Lagoons






Hurricane Floyd





50 Lagoons Flooded






6 Lagoon Breaches









Hurricane Matthew





6 Lagoons Flooded (Estimate)






0 Lagoon Breaches Reported


Animal Mortality






Hurricane Floyd





21,474 Swine Deaths









Hurricane Matthew





Fewer Than 3,000 Swine Deaths (Estimate)