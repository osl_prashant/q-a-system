One of the reasons the produce industry generally supported the election of President Trump was his business experience and positions, including reducing the regulatory burdens.
Late in the year, the U.S. Department of Transportation granted produce and other agriculture product shippers a temporary waiver from the Electronic Logging Device rule.
Produce companies saw other small changes through the year, and by the end, its two largest associations, United Fresh and PMA, had made formal comments requesting the loosening of laws regarding listeria regulations, water provisions and off-farm packinghouses and cooler provisions in the Food Safety Modernization Act.
 
Dec. 11 – Produce groups to submit comments on regulatory burdens
By Tom Karst
Not wasting a chance to seek relief and clarity in federal rules, produce industry groups are preparing to respond to an executive order from President Trump seeking ways to reduce the burden of regulations.
Earlier this year, Trump issued Executive Order 13771, requiring government agencies to search for ways to reduce regulatory burdens.
For its part, the Food and Drug Administration published a notice in September inviting public comments on food safety regulations. The due date for public comments is Feb. 5.
Jim Gorny, vice president of food safety and technology for the Produce Marketing Association, and Jennifer McEntire, vice president of food safety and technology for the United Fresh Produce Association, have taken lead roles in composing industry comments centered around:

FDA’s Listeria monocytogenes regulatory policy for raw, ready-to-eat agricultural commodities;
Ag water provisions of the Produce Safety Rule; and
Coverage of the Preventive Controls Rule versus the Produce Safety Rule for off-farm packinghouses and coolers.

Gorny said Dec. 4 that comments were being finalized with a range of produce groups and grower associations.
 
Dec. 4 – Electronic logging device mandate delayed for truckers
By Tom Karst
Truckers carrying fresh produce and other agricultural commodities have been given a 90-day temporary waiver from the Electronic Logging Device rule mandate that will be implemented Dec. 18.
In addition to the 90-day waiver, the U.S. Department of Transportation’s Federal Motor Carrier Safety Administration said in late November it will soon issue guidance relating to the existing hours-of-service exemption for the agricultural industry.
Smaller and independent trucking companies are pushing for a two-year delay in the electronic logging device mandate while the American Trucking Association is still pushing for quick implementation.
The three-month waiver is welcome, trucking leaders say, and eases near-term concerns about disruptions in trucking rates and delivery delays.
Longer term, many say there is an underlying need to make hours of service regulations for truckers more flexible.
 
Sept. 11 – FDA issues compliance guide to help small businesses implement produce safety rule
By Tom Karst
The Food and Drug Administration has issued a compliance guide for small companies subject to provisions of the Food Safety Modernization Act’s produce safety rule.
The document, available at the FDA’s website, can help farmers determine whether they are eligible for a qualified exemption from the regulations, according to a news release.  
A qualified exemption would modify the requirements small growers are subject to under the produce safety rule.
The document also can help small growers understand the modified requirements under the produce safety rule, according to the release.
The release said the main compliance dates for small businesses and very small businesses under the Produce Safety Rule are Jan. 28, 2019, and Jan. 27, 2020, respectively.
 
May 8 – Perdue pledges USDA focus on the needs of farmers
By Tom Karst
KANSAS CITY, Mo. — After being sworn in April 25, Agriculture Secretary Sonny Perdue made his first official trip outside the Beltway to Kansas City and pledged the agency would expand trade opportunities and have a customer-first focus on the needs of farmers.
Speaking April 28 to a crowd of nearly 500 at Hale Arena at the American Royal, Perdue talked for about 25 minutes and then answered questions for 15 minutes from a group of farmers and agriculture leaders from Missouri and Kansas.
Perdue said President Trump’s April 26 farm roundtable event at the White House brought together a diverse group of farmers from across the country.
President Trump told the group, which included a few produce growers, that he is most concerned about the criminal element of illegal immigrants in the U.S., Perdue said, not about longtime immigrants working on U.S. farms and ranches.
Trump also told members of the farmer roundtable that the administration would reduce the burden of unnecessary regulations on farmers.