Trying to keep pace with modern food safety requirements and buyers' demands, management at the Atlanta State Farmers Market works to maintain the facilities.
The facility in Forest Park was opened in 1959.
A roofing project is underway on the wholesale side of the market, and the facility is undergoing other upgrades.
To upgrade operations, the market is in the planning stages of constructing a refrigerated building on the public side, the west side of the market, where the public open-air sheds reside.
No construction timetable has been set, said Paul Thompson, the market's manager.
Some of the open-air sheds would be demolished to make room for the new building.
"This building would hopefully start a trend of putting up much-needed refrigerated buildings on the market," he said. "There is certainly a demand for more refrigerated cold storage space here. This will be the biggest project in the market for quite some time."
That new 80,000-square-foot building is to be occupied by Phoenix Wholesale Foodservice Inc. and its sister retail distributing company, Collins Bros. Corp.
David Collins III, Phoenix's president, said the market's administration is adept at keeping up with trends and marketplace requirements.
"They (the market leaders) are spending more money to develop new buildings and to try to transform the market into a newer institution to stay current," he said. "As we go on, we will see the market continue with redevelopment."
The market's wholesalers are fortunate to be led by Thompson and Gary Black, the state's agriculture commissioner, Collins said.
The agency invests a lot of time and energy into promoting Georgia agriculture and developing its Georgia Grown local program, he said.
"They're definitely putting money into the market," said Matt Jardina, vice president of general business operations for J.J. Jardina Co. Inc. "They're pumping money into the infrastructure on the wholesale side and making changes in the lower half of the market where the sheds are. They will be erecting some new facilities over time and there are some plans to beautify the place. If the economy stays solid, we will see the new structures take off in terms of the church you can create there with people and commerce."
The market is doing well, said Andrew Scott, director of marketing and business development for the Nickey Gregory Co. LLC.
"It's still jam-packed and it's almost like there are no vacancy signs," he said. "This place is full. There's so much dock business. It goes to show you that people don't want to move off the market because there's so much business that comes to the market.
The market's improvements, which include roofing, plumbing and electrical upgrades, help keep the facilities up to date, said Bryan Thornton, general manager of Coosemans Atlanta Inc.
"This market is antiquated and a little old," he said. "For them to make these improvements, it looks better for us. The buildings and how they function, that looks better for everyone. They're (the market) keeping it up, which is great."
Cliff Sherman, owner of Sunbelt Produce Distributors Inc., agrees the facility is well maintained.
"I think they do a good job maintaining the market," he said. "As big as it is and as many things they have to worry about, no one's complaining about it."