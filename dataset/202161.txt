This market update is a PorkNetwork weekly column reporting trends in weaner pig prices and swine facility availability. All information contained in this update is for the week ended August 19, 2016.Looking at hog sales in February 2017 using April 2017 futures, the weaner breakeven was $34.74, up $8.11 for the week. Feed costs were up $1.43 per head. April futures increased $8.10 compared to last week's February futures used for the crush and basis was less favorable by $3.43 compared to last week. Breakeven prices are based on closing futures prices on August 19, 2016. The breakeven price is the estimated maximum you could pay for a weaner pig andbreakeven when selling the finished hog.

For the Week Ended


8/19/2016


Weekly Change


Weaner pig breakeven


$34.74


$8.11


Margin over variable costs


$54.01


$8.11


Pig purchase month


Aug, 2016





Live hog sale month


Feb, 2017





Lean hog futures


$67.10


$8.10


Lean hog basis/cwt


($5.42)


($3.43)


Weighted average sbm futures


$324.48


($4.14)


Weighted average sbm basis


($3.00)


$0.75


Weighted average corn futures


$3.49


$0.15


Weighted average corn basis


($0.24)


$0.00


Nursery cost/space/yr


$35.00


$0.00


Finisher cost/space/yr


$40.00


$0.00


Feed costs per head


$68.74


$1.43


Assumed carcass weight


205


0

Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations don't take into account market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash traded weaner pig volume was above average this week with 39,280 head being reported which is 131 percent of the 52-week average. Cash prices were $20.76, up $0.04 from a week ago. The low to high range was $13.00 to $25.00. Formula priced weaners were up $0.60 this week at $33.94.
Cash traded feeder pig reported volume was below average with 5,745 head reported. Cash feeder pig reported prices were $27.73, down $1.97 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through August 19, 2016.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential increased $8.07 this week to $13.98 per head.

Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest. NutriQuest Business Solutions is a team of leading business and financial experts that bring years of unparalleled experience in the livestock, row-crop and financial industries. At NutriQuest our success comes from helping producers realize improved profitability and sustainability through innovation driven by a relentless focus on helping producers succeed. For more information, please visit our website at www.nutriquest.com or email casey@nutriquest.com.