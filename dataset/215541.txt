Q. I"m going to try Shredlage® this year; is there anything I should take into consideration when ensiling this forage? My corn silage doesn"t normally have any heating issues. What do you recommend?
A. You are correct that there are additional considerations when ensiling mechanically disrupted silage, commonly referred to as Shredlage®, which is a trademark for the material produced by a specific processor developed for Claas forage harvesters.
One concern that has been expressed is that the mechanical disruption process may create silage that is lighter and less dense, therefore potentially allowing for more air to be trapped in the ensiled material. This excess air can lead to a slower initial fermentation, and could also lead to air reentering the silage at feedout - therefore affecting aerobic stability.
To prevent issues with air during both ensiling and feed out, I recommend you use a proven combination inoculant that includes both a fast, efficient lactic acid bacteria and a spoilage inhibitor such as L. buchneri 40788. This will combat the dual challenge presented with the lighter, less dense silage.
Since Shredlage® requires an additional investment, it is particularly important to combat dry matter losses and spoilage to protect that investment and ensure you get the maximum benefits from Shredlage®.
I hope this information helps.
Sincerely,
The Silage Dr.