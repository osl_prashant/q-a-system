According to Deke Arndt, chief of NOAA's Climate Monitoring Branch, there are three distinct lenses with which to observe drought, and it's important to distinguish among them. But first, a bit of good news.
"For the first time since March 2011, there was no D4 'exceptional drought' anywhere in the United States, as analyzed by the U.S. Drought Monitor, he writes in NOAA's "Beyond the Data" blog. "The bottom line is that the nation is in better shape drought-wise than it has been for most of this decade."
That said, Arndt says learning about these three types of data helps bring nuance and valuable insights to the drought discussion.
1. Meteorological drought measures observed precipitation against expected precipitation.
2. Agricultural drought measures soil moisture and its ability to support plant and crop health.
3. Hydrological drought measures change on a longer timescale (a year or longer).
Arndt points to recent California rain and snowfall as a reason why understanding each of these types of droughts is important.
"The latest rains literally have not had time to filter down and replenish aquifers," he says. "And even when they do, they will be but a blip against the big picture. It's a point worth dwelling on: weeks of rain will relieve wildfire and agricultural impacts, but we need seasons to years of rain to restore groundwater."
Droughts come and go, and Arndt says climate scientists will continue to study them.
"In the drought world, we often divide up history into 'green times' (non-drought) and 'brown times' (drought)," he notes. "What we learn during brown times - and especially how we act on that during green times - prepares us for the next episode."