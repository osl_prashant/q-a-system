This market update is a PorkNetwork weekly column reporting trends in weaner pig prices and swine facility availability.  All information contained in this update is for the week ended March 31, 2017.

Looking at hog sales in September 2017 using October 2017 futures the weaner breakeven was $39.30, down $5.33 for the week. Feed costs were up $0.50 per head. October futures decreased $2.35 compared to last week’s October futures used for the crush and historical basis was unchanged. Breakeven prices are based on closing futures prices on March 31, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations don’t take into account market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash traded weaner pig volume was above average this week with 47,427 head being reported which is 133 percent of the 52-week average. Cash prices were $38.37, down $2.93 from a week ago. The low to high range was $24.00 - $51.00. Formula priced weaners were up $0.85 this week at $39.81.
Cash traded feeder pig reported volume was below average with 9,025 head reported. Cash feeder pig reported prices were $67.20, down $9.10 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through March 31, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential decreased $2.40 this week to a projected gain of $0.93 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.