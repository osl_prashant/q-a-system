Conroe, Texas-based Country Fresh has recalled more than 2,500 cases of mushroom and fruit products that contain Sargento-branded cheeses because they have the potential to be contaminated with listeria.
The products being recalled were shipped to retailers in Alabama, Florida, Georgia, Kentucky, Louisiana, Maryland, Mississippi, North Carolina, South Carolina, Tennessee, Texas and Virginia, according to a recall notice on the FDA website.
Product labels included in the recall notice suggest H-E-B, Harris Teeter and Wal-Mart are among the retailers to which the product was shipped.
No illnesses have been connected to the recall by public health officials so far.
Country Fresh has recalled:
Country Fresh Tuscan Style Portabella Mushrooms with use-by dates of Feb. 14-Feb. 17 and a UPC code of 74641-07211
Country Fresh Stuffed Mushrooms with use-by dates of Feb. 14-Feb. 17 and a UPC code of 74641-07207
Southwestern Stuffed Mushrooms with use-by dates of Feb. 14-17 and a UPC code of 72036-88471
Marketside Garlic & Four Cheese Stuffed Baby Bellas with use-by dates of Feb. 15-Feb. 17 and a UPC code of 681131-14821
Ready Fresh Go Fresh Fruit and Hatch Pepper Cheese with use-by dates of Jan. 19-Feb. 16 and a UPC code of 41220-03680.
Sargento's recall of certain cheese products also spurred a secondary recall by Salinas, Calif.-based Taylor Farms.