While beef producers are raising cattle that become steaks, a restaurant in Houston has been raising the stakes on selling beef to customers for 40 years.
Edd Hendee started the steakhouse Taste of Texas with his wife, Nina, on Nov. 19, 1977.
“I didn’t think we could make it 40 days when we started,” Hendee says jokingly. “Now, here we are 40 years later.”
Through the years the couple has built a business which serves 1,000 customers per day. In the past two years Taste of Texas has generated $17 million in total revenue annually. Hendee says the restaurant ranks number one in Texas by sales for an independently owned restaurant. Nationally, Taste of Texas would rank number 33 by the same measurement.
Hendee attributes the success of Taste of Texas to building his brand over the long haul.
During those early years Hendee identified a branded beef program he wanted to feature in Taste of Texas. It was in 1984 that the restaurant became the first in Texas to serve Certified Angus Beef and it has ever since.
“In the restaurant business taking a brand like Certified Angus Beef and sticking with it for 34 years is work, but it is worth it and it pays off,” Hendee says.
Taste of Texas has sold more than six million lb. of Certified Angus Beef since 1984. It would equate to 9.5 million lb. in packer equivalent weight and 95,200 head of live cattle.

Having a branded beef program like Certified Angus Beef has offered consistency and a premium dinning option that would have been difficult to regularly source otherwise.
For Hendee, he is only interested in about 110 lb. from the carcass: the middle meats or loin. “I want the product that goes on a plate at a great steakhouse.”
Beef is sourced from Buckhead Beef and National Beef Packers by Taste of Texas. A contract is set annually using formula pricing. Hendee pays attention to the cattle markets to help gauge what pricing could be like down the line.
Steaks go into a specialized ageing line specific to Taste of Texas. Boneless steaks are aged 42-45 days, while bone-in steaks are aged 32-35 days.
A butcher shop in the restaurant allows customers to pick out the steak they will be eating. “When the customer picks a steak, before they have even eaten it they have enjoyed it in their mind,” Hendee says of the unique strategy. “You eat with your eyes first.”
Steak Schools are offered at Taste of Texas with Hendee’s daughter, a trained chef, leading the course four times a year. Hendee says the goal of Steak School is to teach customers how to cook and select great steaks.
Attendees learn where various cuts of beef come from and they also get a pasture to plate presentation describing what it took for their steak dinner to make it on the dinner table.
Not only are their cooking classes, to celebrate the 40th anniversary of being in business Taste of Texas is selling a cook book called “Perfectly Aged.”

Some might question why a restaurant would have a cook book or offer cooking classes. Prevailing wisdom would be that a customer wouldn’t need to come back if they already know the secrets to cooking a good steak.
“I want them when they are cooking at home to talk about my place. I want them to tell their friends that they learned all of this by coming to my cooking school. I want them to identify excellence, even if it is at home, with our restaurant,” Hendee says.
The cook book and Steak Schools come full circle thanks to an online store where consumers can buy the same steaks they would eat at the steakhouse. Ribeyes, filets, T-bones and Porterhouses are just a few of the cuts for sale along with other items like steak seasoning.
“We’re in the business of making excellence, whether it is eating at the restaurant, teaching our customers or sending across the country the products that we sell,” Hendee says.