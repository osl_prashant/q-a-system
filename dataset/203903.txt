Mushrooms are seeing increased consumer interest across the board, but varieties beyond basic white mushrooms are growing at even faster rates.
Brian Kiniry, vice president of Kennett Square, Pa.-based Oakshire Mushrooms, said the industry saw positive growth in white and brown mushrooms during the second quarter of the year.
"The white mushroom pounds are increasing around 2% while the brown mushrooms are growing closer to 5%, (year over year), mostly due to increased crimini sales," Kiniry said.
Kathleen Preis, marketing manager for the San Jose, Calif.-based Mushroom Council, said for the 52-week period ending May 15, crimini mushrooms grew 8.6% while portabella sales were up 3.5%.
Specialty mushrooms also continue to grow with 10% growth for the same 52-week period, Preis said.
Pete Wilder, marketing director for To-Jo Mushrooms, Avondale, Pa., is seeing growth in specialty categories such as sliced shiitakes and its exotic blend.
"Consumers and chefs are looking at wild or specialty products as an opportunity to add diversity and flavor to a menu or their personal shopping list," Wilder said.
To-Jo's Foraged Fresh program gives customers the opportunity to pre-order wild seasonal products like chanterelles, morels, fiddle heads, ramps or stinging nettles, depending on the season, and has garnered a strong following, Wilder said.
Kiniry believes chefs are one important driving force behind this trend.
"The shiitake mushroom is becoming more available in menu selections at restaurants, so the consumer is more familiar with shiitake and other specialty mushrooms like oyster. With this new-found knowledge, the consumer is starting to be adventurous and look for specialty mushrooms in their local retail markets," he said.
Customers of The Mushroom Hub are proving that point.
Since opening on May 21, the Windsor, Ontario-based fungi-focused deli has sold the least amount of white mushrooms, according to Denis Vidmar, company spokesman.
Bill St. John, sales director for Kitchen Pride Mushroom Farms Inc., Gonzales, Texas, has seen the trend affect the company's business and production.
"Ten years ago, white button mushrooms were 90% of our business. Today, white mushrooms are about 70%," he said.
St. John also reported that demand for portabellas has stayed fairly stable while crimini demand has increased tremendously.
Other more exotic options are also gaining ground.
"Oyster and shiitake mushrooms are a very small percentage of the business but demand for those has tripled in during the past few years," St. John said.
"Conversion of growing area from white mushrooms to browns reduces overall production because browns do not produce as many pounds per square foot as whites. It is surprising that that there isn't a larger spread between prices of white mushrooms and those for crimini, or even portabellas," St. John said.


Wild mushrooms

Mike O'Brien, vice president of sales and marketing for Monterey Mushrooms, Watsonville, Calif., said there are more than 2,000 varieties of edible mushrooms, which gives consumers a lot of options, especially since many can now be cultivated.
"So-called 'wild mushrooms' are popular on restaurant menus. Since most of us can't spend our time hunting mushrooms in the woods, and while truly wild mushrooms such as chanterelles, matsutakes and porcinis are scarce and expensive, there are other richly flavored mushrooms in your local supermarket that can fill your flavorful needs."
When it comes to foodservice, wild mushrooms are certainly a trend.
However, many chefs use the term "wild" for many brown varieties such as maitakes, enokis and shiitakes, even though they are able to be cultivated.
"I think for chefs and restaurants 'wild' is sometimes used as a marketing term. They consider brown, and shiitake, and others like them as wild. Most restaurants sell cultivated mushrooms. There is a food safety risk to selling real wild mushrooms," O'Brien said.