Six exemplary educators were voted by participants to receive the honored distinction during the 89th Annual Conference held March 5-9, 2017 in Las Vegas. The annual award acknowledges the significant role of instructors who deliver innovative and effective methodologies that enhance animal health.Accomplished in relevant and practical expertise within their specialty, the six winners are:
¬? Avian & Exotics: Douglas Mader, MS, DVM, DAVBP (C/F, R/A), DECZM (Herpetology).
¬? Equine: Anne Wooldridge, DVM, MS, PhD, DACVIM (LAIM)
¬? Food Animal: Robert Sager, DVM, MS, PhD, DABVP (Beef Cattle)
¬? Practice Management: Karen Felsted, CPA, MS, DVM, CVPM, CVA
¬? Small Animal: Etienne Cote, DVM, DACVIM (SAIM & Cardiology)
¬? Veterinary Technician: Heather Prendergast, RVT, CVPM
"We are grateful to all of our instructors who contribute their time to sharing knowledge at WVC, and congratulate these educators who have been recognized by participants and who are held in high esteem by their colleagues," said WVC CEO David Little. "The Annual Conference attracts committed instructors whose dedication to education positively impacts our participants and veterinary medicine overall."