Mushroom sales are on the rise.
"For the 52-week period ending May 15, total U.S. dollar sales for fresh mushrooms at retail grew 6.3%," said Kathleen Preis, marketing manager for the San Jose, Calif.-based Mushroom Council.
Kevin Donovan, national sales manager for Kennett Square, Pa.-based Phillips Mushroom Farms, said growth is across the board.
"You've got your traditional white mushroom consumption still going up, but people are also moving toward the brown mushrooms," Donovan said.
He lists baby portabella mushrooms and shiitake as two varieties seeing a lot of growth.
Mike O'Brien, vice president of sales and marketing, Monterey Mushrooms, Watsonville, Calif., said brown mushrooms are driving category growth.
"The conversion from white button mushrooms to brown mushrooms continues," he said.
However, white mushrooms are still doing well.
O'Brien said the Mushroom Council numbers show white mushrooms were up 11% in June.
He credits that growth to "blend" promotions featuring chopped mushrooms and ground meat, as well as well as other summer mushroom recipes like kabobs.
When it comes to products, Donovan also said fresh and dried, as well as frozen, are seeing increased interest.
"Many restaurant chains use frozen mushrooms, but we're seeing more and more frozen mushroom products going into retail, too," he said.
Growers are ramping up production to keep up with this growth.
For the most part, supply is keeping up with the growth of the category, St. John said.
"Demand is still increasing and production is being added by several growers. Mushroom supply is typically tight at the fall holiday peaks, but that is expected of most any agricultural commodity," he said.
Donovan said the past few months were tight, but supplies have evened out for the summer.
"I see supplies tightening up again come fall," he said.
However, in some cases, mushroom growers are having a hard time keeping up.
"We have seen shortages of white and crimini mushrooms at times during the year that the industry has not seen in years past. I see prices starting to rise as supply tightens and higher growing costs continue to affect the mushroom farms," said Brian Kiniry, vice president of Kennett Square-based Oakshire Mushrooms.
Organic mushrooms, in particular, could be in short supply if demand continues to rapidly increase.
"The awareness of organics has also spiked a dire need for more production, seeing major industry leaders paving the way for more organic production," said Denis Vidmar, spokesman for The Mushroom Hub, a mushroom deli in Windsor, Ontario.
Kiniry agreed organic mushrooms are a topic to discuss in the coming years.
"Retailers and suppliers need to work together to ensure good supply with the proper pricing for the consumer and the mushroom grower. As an industry, we need to be cognizant of trends like organic in the market, but be smart about not reacting without a well-thought-out plan in order to drive consumer satisfaction," Kiniry said.