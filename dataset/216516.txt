Over the past few Christmases agriculture toys have been making headlines at AgWeb. Here is a look at some of the stories we’ve covered:
Elf on a Ranch Spreads Christmas Cheer
A ranch in Kansas needed a little help heading into the holidays, so they hired an elf. But he doesn't spend much time on the shelf.
While working on the ranch throughout the day, Adam and Jeanne Jones have been taking photos of the Elf helping out. Then they share the photos in a blog post on their ranch website calling the page “Elf on a Ranch.”
Grass-Fed Toy Cows?
If you've got a farm kid who needs a stocking stuffer, these are some toy cows that might have you scratching your head.
At the Toys 'R' Us in Columbia, Mo., there’s a cardboard bin filled with plastic toy cows for $7.99. Just above the pricing information on the Holstein and Hereford replicas was an interesting label reading “Grass-Fed Only Cows.”
Spreading the Christmas Spirit with Farm Toys for Tots
An animal rights activist tried to be the Grinch, but it backfired with the help of some kindhearted people who share rural roots.
At Christmas in 2015, children across the country will be unwrapping farm toys, thanks to several agriculture advocates.
Politically Correct Farm Toys for Christmas
Political correctness seems to be all the rage nowadays and it may have made its way to a toy store near you.
It won’t be long before Christmas presents are being unwrapped around the country by boys and girls who made it on the Nice List. Unfortunately, some people who probably deserve to be on the Naughty List are trying to make farm toys “politically correct.”
Even Pigs Prefer New Toys
We can’t help but be tempted by new things. We see it in a child’s eyes when she opens a new toy, and feel it every time a new version of the iPhone is released. It turns out our preference for shiny, new things is pretty universal throughout the animal kingdom. Yes, even piglets prefer new toys.