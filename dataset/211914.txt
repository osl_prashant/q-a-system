The University of California-Davis has its 21st annual fresh-cut workshop Sept. 26-28.The workshop will be at the Buehler Alumni & Visitors Center on the university’s campus and is open to anyone in the fresh-cut industry, from foodservice operators, university researchers and anyone processing fresh fruits and vegetables, according to the event website.
Through interactive sessions and demonstrations, attendees will learn about fresh-cut production, processing, packaging, distribution and quality assurance.
Topics include:
Marketing and consumer issues;
Product biology;
Product quality;
Product preparation;
Temperature management;
Microbiology and sanitation; and
Modified-atmospheres and packaging.
An industry expert panel is new at the 2017 workshop.
Industry professionals interested in attending can register online. The cost to attend is $1,275.