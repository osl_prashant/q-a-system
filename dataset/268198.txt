War of words over buffer strip law heats up at Capitol
War of words over buffer strip law heats up at Capitol

The Associated Press

ST. PAUL, Minn.




ST. PAUL, Minn. (AP) — The Republican chairmen of agriculture committees at the Minnesota Legislature are accusing the Dayton administration of overreach as it implements a state law requiring farmers to plant buffer strips between fields and waterways to trap pollutants.
The Board of Water and Soil Resources is considering a draft proposal to give local governments a new option for imposing higher administrative penalties on the few landowners who don't comply.
The chairmen call the proposal "ludicrous ," ''absurd" and "heavy-handed ." Committee hearings on it are set for Wednesday and Thursday.
The board on Monday apologized for what calls a "communication misunderstanding." It says the draft is meant only as an option for local governments to achieve compliance. And it says the board probably won't adopt it without broad support from landowners or local governments.