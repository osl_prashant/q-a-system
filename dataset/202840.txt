Woodridge, Ill.-based, Produce Pro Software launched a redesigned website at www.producepro.com.
The site features a simplified look, enhanced visual content, search functionality and mobile optimization, according to a news release. Tse features allow visitors to connect with Produce Pro Software from desktop and mobile devices.
"Being a technology company and living in a digital age, having an up-to-date website was a top priority for us," said Kristen Santangelo, marketing manager, in the release. "We needed a digital platform we could manage ourselves; ensuring visitors receive an informative experience each time they visit our site as well as a clear way to contact us."
Produce Pro Software offers integrated enterprise resource planning software solutions, warehouse management systems, mobile apps and industry consulting, according to the release.
Website visitors can access features including:
Videos about products and service
Customer case studies and testimonials
Recent news and upcoming education webinars and workshops