Struggling with negative margins of less than $100 per head for several weeks, feedyards witnessed a massive increase in losses last week as cash prices dipped below $98 per cwt. The near $4 per cwt. decline in USDA's 5-area reported cash price left feedyard closeouts showing per head losses of $248, according to the Sterling Beef Profit Tracker. Negotiated cash cattle prices dipped below the $100 mark for the first time since December 2010.The losses were magnified by the fact the price of feeder cattle calculated against last week's marketings were $61 per head higher than the previous week, and the cost of feed was $42 per head higher, according to Sterling Marketing, Inc., Vale, Ore.
Cash prices traded at $97.98, about $10 per cwt lower than a month ago. The total cost of finishing cattle last week was $1,613, compared to $1,507 the previous week and $2,167 last year, according to Sterling Marketing.
Beef packer margins increased $6 per head to $195. Packers earned an average of $50 per head during the same period a year ago.
A month ago cattle feeders were losing $89 per head, while a year ago losses were calculated at $301 per head. Feeder cattle represent 72% of the cost of finishing a steer, compared to 78% last year.
Farrow-to-finish pork producers lost $45 per hog last week, about $3 more than the week before. A month ago farrow-to-finish pork producers lost about $15 per head.
Pork packers saw their margins hold about steady at $38 per head. Negotiated prices for lean hogs were $49 per cwt. last week, also steady with the previous week. Cash prices for fed cattle are $36 per cwt. lower than last year and prices for lean hogs are $24 per cwt. lower.
Sterling Marketing president John Nalivka projects average cash profit margins for cow-calf producers at $154 per cow this year. In 2017, Nalivka projects cow-calf profits of $21. Last year's estimated average cow-calf margins were $432 per cow.


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.