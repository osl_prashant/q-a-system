There's something special about the agricultural community. It's been seen time and time again - communities rally around 4-Hers dealing with illness or tragedy, cheering them to victory.Now, another phenomenal community has responded to a family's need.
Kathryn Colvin is the doting mother of 11-year-old Hunter. Hunter is, by all accounts, an average 4-Her with one exception: he has cerebral palsy, a congenital condition that leads to movement disorder.
Hunter hasn't let cerebral palsy stop him from being active in the showring though. With the help of other 4-Hers, he has shown pigs at the Chautauqua County(N.Y.) Fair since 2014.
This year, however, will be different.
This July, as the hogs enter the ring, Hunter's family hopes he will be able to show pigs in a new, special track wheelchair - an all-terrain machine that will give Hunter independence in - and out - of the show ring.
The crowdfunding page has brought in roughly $5,000 in just 8 days, or approximately one-third of the funds needed. Kathryn told PORK Network one of the largest donations came from the young man seen helping Hunter in the photo above, who is now serving in the military.
"To say we are overwhelmed is a complete understatement!" she said.
Many of the people who've donated have left their encouragement for the young man:
"Your possibilities are endless kiddo, let nothing hold you back."
"Wish I could do more! That chair will open up endless possibilities for Hunter and I KNOW he will LOVE it!"
"I do believe I will go to the Chautauqua Co Fair this July to watch Hunter accomplish on his own!"
"Go Hunter!!! Do what you do best and light up the world with your amazing personality and we will do whatever we can to help you succeed!!"
Click here to learn more about Hunter and the fundraiser. 
The generosity doesn't stop at monetary donations. Pigs for both Hunter and his old brother, Brendan, have been donated for this year's fair. They will be picking up the pigs later this month.