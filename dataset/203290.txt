Agriculture Secretary Tom Vilsack appointed 13 members to the National Watermelon Promotion Board. While 12 of the members began three-year terms starting Jan. 1, one new member (John Prukop) will complete a two-year term beginning immediately.
New members are:
Cody Smith, Bruceville, Ind., District 4;
John Prukop, Bishop, Texas, District 6; and
Jeremy Holden, Canutillo, Texas, District 7.
Reappointed members are:
Autumn Freeman, Oaktown, Ind., District 4;
Sara Frey-Tally, Keenes, Ill., District 4;
Ed McClellan, Ann Arbor, Mich., District 4;
Kevin Antongiovanni, Bakersfield, Calif., District 5;
Arthur Saikhon, El Centro, Calif., District 5;
Josh Knox, El Dorado Hills, Calif., District 5;
Joseph Paul Gomes, Manteca, Calif., District 5;
Chuck Botsford, Lake Oswego, Ore., District 7;
John Morrow, Deming, N.M., District 7; and
David Mayhue, Fayetteville, Ark., District 7.
The USDA's Agricultural Marketing Service oversees the National Watermelon Promotion Board.