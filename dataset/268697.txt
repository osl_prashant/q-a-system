The Latest: Lawmakers say Trump will look into trade talks
The Latest: Lawmakers say Trump will look into trade talks

The Associated Press

WASHINGTON




WASHINGTON (AP) — The Latest on the U.S. trade dispute with China (all times local):
12:50 p.m.
Farm state lawmakers say President Donald Trump has asked top administration officials to look into rejoining talks on the Trans-Pacific Partnership, from which the U.S. withdrew last year.
Nebraska Sen. Ben Sasse says Trump said during a White House meeting with Midwest governors and lawmakers that he had "deputized" U.S. trade representative Robert Lighthizer and economic adviser Larry Kudlow to look into the U.S. rejoining the TPP. It would be aimed at opening U.S. farmers to more overseas markets.
Kansas Sen. Pat Roberts says Trump wants "to see if we couldn't take another look at TPP." Roberts says it would be "good news all throughout farm country."
Eleven other Pacific Rim countries signed a sweeping trade agreement last month that came together after the U.S. pulled out.
___
11:30 a.m.
President Donald Trump is telling a group of farm state governors and lawmakers that he's pressing China to treat the American agriculture industry fairly.
Trump is seeking to reassure the lawmakers on proposed China tariffs. He's meeting at the White House with the governors of Iowa, Nebraska and North Dakota and several Midwest senators and House members.
The president says China has consistently treated U.S. agriculture unfairly and his administration is "changing things with respect to trade."
He is telling the group, "Everything is going to be better."
Trump was discussing the Chinese threat to slap tariffs on soybeans and other crops grown in rural America. The move could hurt Midwestern farmers, many of whom are strong supporters of the president.
____
11:19 a.m.
An array of business executives are expressing alarm to federal lawmakers Thursday about the impact that tariffs will have on their business.
Kevin Kennedy, president of a steel fabrication business in Texas, says tariffs on steel and aluminum imports have led U.S. steel producers to raise their prices by 40 percent.  He says that's shifting work to competitors outside the U.S.
Kennedy says what was presented as a tariff on foreign steel has effectively become a tax on U.S. manufactures such as his company.
Kennedy is appearing before the House Ways and Means Committee, which is examining the impact of tariffs.
Lawmakers later Thursday are scheduled to meet with President Donald Trump to talk about trade.
China has countered by announcing its own set of tariffs on U.S. products.