Odell, Ore.-based Diamond Fruit Growers received the 2017 Oregon Governor’s Occupational Safety and Health Conference Safety Committee Award.Diamond Fruit Growers is the pear producer for Wenatchee, Wash.-based Oneonta Starr Ranch Growers.
“This was a great effort by the crew down at Diamond, and we’re very proud of the team for receiving the State of Oregon’s 2017 Govenor’s Occupational Safety and Health Conference Safety Committees Award for 2017,” Scott Marboe, director of marketing for Oneonta Starr Ranch Growers, said in a news release.
The company received the award at the Oregon OSHA conference for a few of its efforts including:
Using a work order system to address issues;
Implementing a weekly safety talk; and
Upgrading its warehouse and packing line for safety.