World food prices rose in February from the month before, as rising prices for staple grains and dairy products more than balanced out lower values for vegetable oils, the United Nations food agency said on Thursday.
The Food and Agriculture Organization’s (FAO) food price index, which measures monthly changes for a basket of cereals, oilseeds, dairy products, meat and sugar, averaged 170.8 points, up 1.1 percent from January.
Food prices on international markets were 2.7 percent lower than last February, FAO said.
Agricultural commodities have emerged from a highly volatile period and FAO has said it expects them to remain stable over the next decade.
FAO raised its estimate for global cereals output in 2017 by 2 million tonnes to 2.642 billion tonnes, a record high.
In its first forecast for 2018 wheat production, the agency predicted output of 744 million tonnes, saying this would be an above average yield, but the second consecutive annual dip.