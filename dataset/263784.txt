2 bison slaughter protesters arrested in Yellowstone park
2 bison slaughter protesters arrested in Yellowstone park

The Associated Press

BOZEMAN, Mont.




BOZEMAN, Mont. (AP) — Two people were arrested after chaining themselves to cement-filled barrels in an effort to prevent trucks from hauling Yellowstone National Park bison to slaughterhouses.
The Bozeman Daily Chronicle reports two members of the group Wild Buffalo Defense were arrested Friday morning in the park.
A clerk at the federal court in Mammoth, Wyoming, confirmed the arrests. Their names were not released.
Wild Buffalo Defense spokesman Monty Slate said the protest delayed a shipment of bison for a few hours before a path was cleared around the protesters.
Bison are shipped to slaughter each year as part of a population management plan for the park.
Three Wild Buffalo Defense protesters who were arrested in the park on March 6 were banned from the park for five years and ordered to pay more than $1,000 in fines.
___
Information from: Bozeman Daily Chronicle, http://www.bozemandailychronicle.com