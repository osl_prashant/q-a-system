After falling conspicuously last week along with urea, UAN32% shot $5.85 higher this week to lead overall gains in the fertilizer space.
The general trend is higher on the week, supporting a 3.16 point rise in our Nutrient Composite Index.

Nitrogen

UAN28% was our only fertilizer to post a price decline, falling 84 cents by the short ton regionally. UAN28% remains priced above anhydrous ammonia on an indexed basis, but only by a few points. Historically, when UAN28% crosses below NH3, the tendency is for nitrogen prices to firm.
If UAN28% violates support from anhydrous ammonia next week, we will consider it a suggestion that nitrogen will continue to firm aggressively.

Phosphate

Phosphates are higher this week, but did not repeat last week's sharp move to the upside.
Industry watchers around the world now believe the bottom is in for DAP and MAP although near-term retail price action is likely to be fairly docile.

Potash

Potash gained a little ground toward the upside as well but is still the lowest-priced fertilizer in our weekly survey on an indexed basis.

Corn Futures

December 2017 corn futures closed Friday, March 17 at $3.79 putting expected new-crop revenue (eNCR) at $599.67 per acre -- softer $16.94 per acre on the week.
With our Nutrient Composite Index (NCI) at 539.22 this week, the eNCR/NCI spread narrowed 20.10 points and now stands at -60.45. This means one acre of expected new-crop revenue is priced at a 60.45 premium to our Nutrient Composite Index.





Fertilizer


3/13/17


3/20/17


Week-over Change


Current Week

Fertilizer



Anhydrous


$515.16


$516.36


+$3.58


$519.94

Anhydrous



DAP


$449.65


$456.61


+$2.28


$458.89

DAP



MAP


$453.53


$456.87


+74 cents


$457.62

MAP



Potash


$329.99


$331.24


+$1.53


$332.77

Potash



UAN28


$248.58


$249.74


-84 cents


$248.90

UAN28



UAN32


$281.07


$280.83


+$5.85


$286.68

UAN32



Urea


$363.38


$362.21


+$2.10


$364.31

Urea



Composite


533.75


536.06


+3.16


539.22

Composite