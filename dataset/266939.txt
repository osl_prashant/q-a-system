Kennedy's speech following King assassination is remembered
Kennedy's speech following King assassination is remembered

By ROBERT KINGThe Indianapolis Star
The Associated Press

INDIANAPOLIS




INDIANAPOLIS (AP) — Fifty years ago this Wednesday, Robert Kennedy ascended a platform near 17th Street and Broadway to deliver a poignant plea for peace and racial harmony in the wake of word that Dr. Martin Luther King Jr. had been murdered in Memphis.
The neighborhood, like so many Kennedy had visited in his brief presidential run, was another ring of urban decay afflicted by poverty, business closures and rising crime. Over subsequent decades, the population declined by more than 70 percent. Homes deteriorated. Many were erased by demolition, leaving rows of vacant lots that became a pocket of prairie in the city.
Yet as the neighborhood now known as Kennedy-King marks the golden anniversary of its moment in history, it is in the early stages of a revival that is breeding hope, if also some notes of caution.
New homes with modern designs and eye-popping colors are sprouting like spring flowers in the vacant lots of the neighborhood, which sits on both sides of College Avenue between 16th and 22nd streets. Rising along the edges are rows of town homes, some with rooftop views of Downtown and price tags of up to $500,000.
In what were once a pair of corner markets, the harbingers of coming hipsters have arrived — a craft brewery and an artisan whiskey distillery. Aside from neighborhood patrons, they hope to draw business from the Monon Trail, a vein previously untapped in this part of town.
"I'm really excited about what's taking place," said Cynthia Hooks, who grew up in the neighborhood. Now 50, she has inherited her grandmother's house and is president of the neighborhood association. "I've watched it change from the time I was a little girl until now. So it's exciting."
The revival has been a long time coming.
Old-timers describe the mid-20th century neighborhood as one of tree-lined streets, densely packed with large, occupied houses and businesses, from food markets and dry good stores to bottling plants and midsize companies that did the laundry for hospitals and other larger institutions.
But that began to ebb in the 1960s as people, especially white residents, fled to the suburbs. By the time Kennedy arrived on that fateful day of April 4, 1968, the mostly black population was poor, and there was a growing crime problem.
The moment was a turbulent time in history. King had led a movement that brought about advances in civil rights, and he had paid the ultimate price. Kennedy, himself from a wealthy family, had raised the poor, from Indian reservations to urban ghettos. But within two months he, too, would be dead.
Amid the welling anger and grief from King's death, Kennedy expressed hope — during his speech that night in Indianapolis — that one day black people and white would want to live together in America and improve the quality of life. At the time, and in the decades that followed, his wish could have been dismissed as the words of a dreamer.
Hooks, who is black, was born just a few months before Kennedy's visit. As a girl in the 1970s, she spent summers at her grandmother's house on Bellefontaine Street. She remembers being restricted on how far she could venture — no further than two blocks to the Frankovitz Market.
"It got to be quite the scary place for me," Hooks said.
It remained scary for many people until only in the past few years. Hooks took ownership of her grandmother's place in 2010, formed a crime watch and began picking up trash off the streets. By then, she said, it was evident the popularity of Downtown living was going to spread further out, even to Kennedy-King. In the past few years, that has come to fruition.
Today, the census tract that encompasses Kennedy-King — and some surrounding areas from the Old Northside and Herron-Morton — shows a population that is slightly more white than black, with a growing Hispanic population. According to the GeoLytics National Change Database, the area is also much wealthier than it was in 1970, adjusting for inflation.
The demographic changes are evident even at the neighborhood's soul food landmark, Kountry Kitchen, where people come hungry to devour hefty plates of fried chicken wings, collard greens, mac and cheese, and corn bread.
A neighborhood institution, the restaurant has always been a draw for local politicians, sports figures and the occasional celebrity. But co-owner Cynthia Wilson says she increasingly sees white families join her traditionally black client base. To a certain degree, she sees it as a realization of the harmony Kennedy and King lived and died for.
"We're not where we need to be," Wilson said, "but we're trying to still get there continuously."
Among the new arrivals is West Fork Whiskey Co., which in October moved from the northwest side into what used to be the Frankovitz Market that Hooks visited as a girl. When West Fork arrived, there were leaks in the roof, dirt on the floors, missing doors and damage from where a truck had crashed into the corner. Today, the facility has new floors, modern furnishings around its whiskey bar and stacks of oaken barrels filled with newly distilled whiskey.
Co-founder Blake Jones, 28, who is white and originally from Southern Indiana, said his partners liked the location because it was an area that had been down but was coming back.
"We wanted something more urban, something in an area that was re-gentrifying and close to Downtown," he said. "We looked around and talked to a couple of other businesses in the area and thought it was a great fit for us."
Adjacent to the distillery, a screen-print shop opened two years ago after leaving Fountain Square. Rivet Press co-owner Dan Wagoner, 36, who is white, said he didn't know much about Kennedy-King's history before coming to the neighborhood. But he has been learning about it from longtime residents, and he appreciates the diversity of the population.
Roughly 100 new homes are being built or planned in and around Kennedy-King. And the prices being asked and paid have prompted the few pangs of hesitation in people such as Wilson, the soul food restaurant co-owner, who is black. She worries that rising property values could lift taxes to the point poorer residents, and older ones on fixed incomes, can't afford to stay.
Among those who share her concern is Jullian Walker, a black retiree who has been in the neighborhood since 1965. Since the 1980s he has lived in his single-story three-bedroom home on Carrollton Avenue, raising a family and now, at 70, living in retirement.
In the past year, a handful of homes featuring striking, angular designs and brilliant colors have been erected on previously vacant lots. Some of them are selling for upward of $300,000.
Developers have offered Walker $80,000 for his home so they can demolish it and replace with something along the lines of the newer crop of homes. But Walker doesn't like where that would leave him. "The money they are offering you ain't no money anymore," he said. "I'm 70 years old. Money ain't what I need. I need peace."
Walker's fears coming tax bills amid higher property values. "I hope they don't try to put me in the same bracket with the other people," he said. "I'm on fixed income and all that stuff. If the taxes go up it would be hard for me to maintain."
Walker is not alone in seeing such redevelopment as a mixed blessing.
Elle Roberts, a black writer and artist who lives just north of the neighborhood, said recent improvements to the park at the center of the neighborhood and upgrades to the nearby Kroger (of $1.3 million) are welcome. But she knows rising property values will ultimately mean higher property taxes, and she worries what that means, particularly for renters such as herself.
"I think the word I would use is concerned but hopeful," Roberts said.
Similarly, wherever redevelopment occurs in the city, there needs to be more front-end conversations about how that is going to benefit longtime residents, said Imhotep Adisa, executive director of the nonprofit Kheprw Institute, which has conducted workshops on gentrification in Indianapolis.
Adisa said the vacant lots being redeveloped became empty, in many cases, because of past decisions made to pull investment out of the area and sometimes the decisions were based on structural racism. Too many redevelopment decisions, Adisa said, are made to benefit investors, often based out of state or out of the country.
"Developers need to consider how to increase opportunities for those who have been left out of the story," Adisa said.
Some efforts are being made to avoid displacing current residents, said Evan Tester, deputy director of King Park Development, a community development corporation in the neighborhood. Assistance is available for low-income residents who need money to repair their homes.
Additionally, he said rising property values will create more home equity for residents. "We want them to stay here," said Tester, who is white. "We want them to enjoy the same amenities as all the new residents."
King Park has 60 homes under construction in the neighborhood. To ensure they don't just go to the wealthier newcomers, the developer is offering below-market-rate loans to people with low-to-moderate incomes who want to live there.
Despite his concerns about taxes, Walker likes that his neighborhood is making a comeback, even if the houses look strange to him. "They're crazy," he said. "I never saw houses like it."
Whether the old and the new can co-exist will be a key question for Kennedy-King in the years ahead. And it will also test whether Robert Kennedy's plaintive hope, in his speech from 50 years past, can come to fruition, with people of different races living together, improving one another's quality of life.
Hooks, the neighborhood association president, is hopeful.
"I certainly think we're doing the best we can," Hooks said, "to live up to what they stood for."
___
Source: The Indianapolis Star, https://indy.st/2Gs6UMB
___
Information from: The Indianapolis Star, http://www.indystar.com


This is an AP-Indiana Exchange story offered by The Indianapolis Star.