Following are final Farm Journal Midwest Crop Tour results from Ohio:




Ohio Corn




2017 District


Ear Count in 60 ft of Row


Grain Length
			(inches) 


Kernel Rows Around


Row Spacing
			(inches) 


Yield
			(per bu.)


Samples



OH1

92.18


6.49


15.73


30.30


155.10


33



OH2

95.00


6.61


16.61


28.50


182.32


12



OH4

93.36


6.85


15.50


29.79


167.45


47



OH5

95.38


6.38


15.51


29.17


161.94


24



OH7

84.50


7.62


16.18


30.00


172.90


4



Ohio Average

93.31


6.66


15.70


29.68


164.62


120 




3-year avg. by district


Ear Count in 60 ft of Row


Grain Length
			(inches) 


Kernel Rows Around


Row Spacing
			(inches) 


Yield
			(per bu.)


Samples



OH 1

99.41


5.63


15.41


29.70


147.46


28



OH 2

97.34


6.04


15.56


29.19


158.84


10



OH 4

97.86


6.36


15.50


29.79


164.24


40



OH 5

99.64


6.15


15.53


29.23


163.91


19



OH 7

100.59


6.56


15.90


30.00


174.05


6



OH Average

98.67


6.11


15.52


29.61


159.81


102




 




Ohio Soybeans




2017 District

Pod Count in
			3 feet
Soil Moisture
Growth Stage
Row Spacing
			(inches) 
Pod Count in
			3 X 3 Square
Samples



OH1


323.28


2.58


4.55


13.61


920.52


33




OH2


385.62


3.00


4.92


13.33


1179.68


12




OH4


512.93


2.62


4.72


16.66


1211.70


47




OH5


463.12


3.25


4.71


14.79


1164.64


24




OH7


434.85


2.00


4.75


20.00


851.57


4




Ohio Average


435.48


2.75


4.69


15.23


1107.01


120 




3-year avg. by district


Pod Count in
			3 feet


Soil Moisture


Growth Stage


Row Spacing
			(inches) 


Pod Count in
			3 X 3 Square


Samples



OH 1

400.87


2.99


4.71


13.09


1215.17


28



OH 2

445.05


3.45


4.61


14.53


1150.61


10



OH 4

483.95


3.14


4.67


16.55


1127.36


39



OH 5

440.94


3.24


4.61


15.19


1152.77


19



OH 7

580.89


3.58


5.00


14.72


1387.15


6



OH Average

454.54


3.17


4.69


15.04


1174.24


102