<!--
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
p
	{mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";
	mso-fareast-theme-font:minor-fareast;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
-->







USDA Weekly Grain Export Inspections
			Week Ended Nov. 23, 2017 




Corn




Actual (MT)


638,711 




Expectations (MT)


550,000-750,000




Comments:


Inspections slipped 21,050 MT from the previous week and were within expectations. Inspections are running 42.3% behind year-ago versus 43.9% behind the week prior. USDA's 2017-18 export forecast of 1.925 billion bu. is down 16.0% from the previous marketing year.




Wheat




Actual (MT)


344,721




Expectations (MT)


200,000-400,000 




Comments:


Inspections were up 84,417 MT from the previous week, and the tally was within expectations. Inspections are running 5.9% behind year-ago versus 6.8% behind year-ago last week. USDA's export forecast for 2017-18 is at 1.0 billion bu., down 5.2% from the previous marketing year.




Soybeans




Actual (MT)


1,578,592




Expectations (MMT)


1,500,000-1,800,000  




Comments:


Export inspections fell 696,920 MT from last week, and shipments were near the low end of expectations. Inspections for 2017-18 are running 13.5% behind year-ago versus 12.5% ahead of year-ago last week. USDA's 2017-18 export forecast is at 2.250 billion bu., up 3.5% from year-ago.