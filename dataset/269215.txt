Ag experts troubled by GOP Senate candidates' tariff talk
Ag experts troubled by GOP Senate candidates' tariff talk

By BRIAN SLODYSKOAssociated Press
The Associated Press

INDIANAPOLIS




INDIANAPOLIS (AP) — Indiana's three Republican Senate candidates continue to voice support for President Donald Trump's trade brinkmanship with China.
But economists and agriculture experts warn that a trade war would drive farmers into bankruptcy while hurting the state economically.
Indiana is a major producer of pork, soybeans and corn. All three would be targeted by tariffs the Chinese have proposed in response to Trump's call for tariffs on aluminum and steel.
With few major policy differences to distinguish themselves, Reps. Todd Rokita and Luke Messer, and former state Rep. Mike Braun have each clamored to portray themselves as the biggest Trump supporter.
But farmers and agriculture experts say that is shortsighted. They say the state would be hit hard by the tariffs.