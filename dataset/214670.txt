U.S. live cattle futures were mostly lower on Monday, pressured by weaker beef prices and expectations that cash cattle at Plains feedlot markets would trade at lower prices this week, traders said.
Chicago Mercantile Exchange December live cattle ended steady at 120.575 cents, while actively traded February futures fell for a sixth straight session, shedding 0.350 cent lower to settle at 126.400 cents.
“We saw boxed beef slip today and that’s not something people want to see,” said Rich Nelson, chief strategist with Allendale Inc.
The choice boxed beef cutout was $1.04 per hundredweight (cwt) lower on Monday while select cuts fell by 68 cents per cwt.
Packers will be buying cattle for a smaller slaughter next week as packing plants will close for at least a day for the U.S. Thanksgiving Day holiday.
But market pressure from that lighter demand may be partly offset by a smaller number of cattle available this week. Showlists in the Plains were down about 7,700 head this week, Nelson said.
Cash cattle in the Plains traded early last week at $124 per cwt or less.
Most feeder cattle contracts moved lower with the live cattle market. November futures were the sole gainer, adding 0.225 cent to 158.700 cents per lb, while actively traded January fell 0.200 cent to 156.975 cents.
Hogs Mixed
Lean hog futures ended on both sides of the previous close, with the nearby December contract pressured by weak spot demand for hogs heading into the Thanksgiving holiday.
Packer demand for hogs will be disrupted by downtime next week for the holiday, but the following week is typically the largest hog slaughter week of the year.
Pork packer margins are profitable at about $38.15 per head on Monday, up from $29.15 a week earlier, according to livestock marketing advisory service HedgersEdge.
CME December lean hog futures settled 0.175 cent lower at 62.300 cents per pound, the ninth straight daily decline and the lowest in four weeks. February futures rising 0.050 cent to 70.300.