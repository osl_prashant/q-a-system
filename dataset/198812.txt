The Supreme Court's split decision on immigration last week is only ratcheting up commentary this election cycle. It is also creating unease for those operations that rely on immigrant labor.
We've all heard the views about immigration from politicians.
"One of the reasons why America is such a diverse and inclusive nation is because we're a nation of immigrants," says President Barack Obama.
Meanwhile, Republican presidential Candidate Donald Trump says:"We are going to build a wall, and Mexico is going to pay for the wall. They will pay for it!"

But the immigration issue affects more than just people in politics.
"The challenge is finding people and maintaining people," says Jeff Mulligan, a dairy farmer in Avon, N.Y.
"In 2012, we hired 1,630 domestic workers and three stayed more than three days," says Bill Brim of Lewis Taylor Farms in Tifton, Ga.
Without local workers, many farms rely on the H2A visa program. That's just for seasonal work, which isnot ideal for dairies.

"We're going to continue to try to employ as many folks from the local community as we can, but there's just not enough in that labor pool to supply all of the farms in our area. A guest worker program that would suit for dairy would be the best fix we could possibly have to try to find some employees to do the work on the farms," says Kendra Lamb, a dairy farmer in Oakfield, N.Y.
A year-round solutionalso a need for Brims of Lewis Taylor Farms. They use hundreds of workers,and red tape is slowing down the process of employing them.

"Well, we're still doing the snail mail with them, mailing stuff back and forth. It's slow and we're trying to have them go to a computer system so they can file on a computer," says Brim.
While some pushfor a more effective guest-worker program, others wantmore comprehensive immigration reform.

"Weneed immigration reform in Washington. It needs to come from them,"says Sarah Noble-Moag of NobleHurst Farms in Pavilion, N.Y. "Aguest-worker program would work. We have no access to a H2A dairy in New York. We have no options to bring a foreign person in unless they have the documents that come from our federal system."

While dairies try to hire locally, farmers say those workers don't typically stay on the job.
"We need to have a legal way--especially for our Hispanics to come in and work for us, to make the funds they need to send home to their families in Mexico," says Noble-Moag.
The National Milk Producers Federation says one-third of U.S. dairy farms employ foreign-born workers. Those farms produce nearly 80%of the nation's milk.

"If you were to round up everyone who is illegal in agriculture, it would basically shut down our food system. That's about as bad or worst case scenario as you can imagine. It would be a disaster for dairy farming as well as the fruit and vegetable industry and the meat packing industry,"says Chris Galen,National Milk Producers Federation Senior Vice President of Communications. "I think that's where we really have to be careful about making certain that we don't end up with the reality that matches some of this campaign rhetoric we're heard so far."

For others, limiting immigration is about safety. Trump "isn't politically correct about it, but he wants to control the invasion of people and drugs into the United States," says Arizona cattle rancherJohn Ladd.
No matter the view, what seems sure, immigration and labor will continue to be a topic no matter the next administration.
Farmers Mixed on Immigration

Want more video news? Watch it on MyFarmTV.