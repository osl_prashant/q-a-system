Farmers test out-of-the-box techniques to add soybean bushels and profits
Add some vinegar and sugar—think you’re getting directions for salad dressing? Guess again. Ron Small takes advantage of vinegar, sugar and a few other unorthodox tactics to average 90-plus bushels per acre on all 2,500 of his soybean acres, with around 400 of those acres at more than 100 bu. each.

The former mechanical engineer likes to experiment with yield-boosting strategies on his southern Indiana farm. While he has achieved more than 300-bu. per acre corn yields, soybeans are his first love.

“I find soybeans like to be fed and treated like they’re special,” Small says. “I give them foliar feed, applied at least four times during the season.”

He still applies some of what you’d expect, such as nitrogen, phosphorus and pesticides. But he also isn’t afraid to try the unusual to increase bushels and ultimately, profit. Taking a tip from watermelon farmers,
Small applies a watermelon nutrient supplement as well as an oceanic compound, vinegar and sugar to boost yields—without losing money.

Small uses WinField’s Max-In Vine and Vegetable supplement, which includes trace elements, such as manganese, and is created to help melons hold blooms. Because soybeans abort blooms easily, applying this product is one way to regain yield potential. “Vine and Veg” costs Small about $7 per acre per application and boosts yields by about 20 bu. per acre. “I apply it during the full moon to get it in the plant quicker since sunlight is the main factor in plant production and moonlight is reflected sunlight,” he adds.

For the past two years, Small has been working with a Texas company to experiment with an oceanic compound that adds a minimum of 10 bu. per acre with each application. The ocean solution is $3.50 per acre per application.

“The solution combines 90 elements needed not only by the human body, but we’re finding plants need them, too,” Small says. 

When the solution hits his doorstop it looks just like water, but when it’s applied at 1 quart per acre at the right time it produces stellar results, Small says. He applies it at 28 days, when soybeans start to place blooms; at 56 days to hit another cycle; and he plans to try a third application this year. 
Small says he sees a 5 bu. per acre increase when he applies vinegar twice each season. Vinegar helps the plant hold blooms and seeds and keeps the plant shorter, which is beneficial 
at harvest.

“Vinegar helps drive the auxins down and shortens internodes,” says Kip Cullers, a prize-winning corn and soybean farmer in southwest Missouri who uses the practice. “You need to buy the 10% vinegar at about 1 pint to 1 quart per acre.”

Sugar helps provide glucose to plants to increase energy and ultimately, yield. Soybeans need 119 lb. of glucose to make 1 bu. and corn takes 78 lb., according to Cullers.

“We put sugar in furrow and apply 4 oz. to 6 oz. anytime we’re spraying,” Cullers says about his own sugar experience. “You can pick up a 2% to 10% yield increase.”

Consider ways to boost yields this season, but watch your bottom line.