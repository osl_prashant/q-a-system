Scientists collect roadkill for eagle feeding experiment
Scientists collect roadkill for eagle feeding experiment

The Associated Press

FLAGSTAFF, Ariz.




FLAGSTAFF, Ariz. (AP) — A team of Flagstaff biologists are using roadkill to determine if supplementing the food supply could boost egg production among breeding pairs of golden eagles during the winter.
For three months this past winter, the team collected nearly 70 deer and elk that had been killed on area highways and delivered the carcasses to nine breeding areas scattered across the Babbitt Ranches in northern Arizona, the Arizona Daily Sun reported this week.
The team is now collecting and analyzing photos and data from the feeding experiment, which is scheduled to last four years. The results so far are showing encouraging signs, said Tom Koronkiewicz, an avian ecologist with the environmental consulting firm SWCA.
Images from wildlife cameras appear to show that the resident female eagles that took the meat were likely to remain in their established nesting areas throughout the winter, Koronkiewicz said.
The team will then compare the eagles' egg-laying at these locations with control sites. The biologists will also examine the 50,000 photographs taken during the winter to better determine which eagles took the delivered meat, Koronkiewicz said.
If the study demonstrates a boost in eagles' reproduction, a supplemental feeding program could provide officials with another conservation strategy for the federally protected eagles.
Reproduction among eagles in northern Arizona has been lower than what biologists want for the species, said Greg Beatty, a U.S. Fish and Wildlife Service biologist.
"They haven't had many nests be successful," Beatty said.
An eagle may use a nesting site for its entire life. The nests are often perched on cliff sides or wedged into large trees.
Factors that could be contributing to the species' low reproduction include parasitic insects that feed on young eagle nestlings and climate change, Koronkiewicz said.
With warmer and drier conditions in the area, less vegetation grows. Lacking vegetation hurts rabbit and prairie dog populations, which are main food sources for the eagles. When food becomes scarce, the eagles are less likely to breed, Koronkiewicz said.
Surveys have located about a dozen breeding pairs of eagles across the 700,000 acres of the Babbitt Ranches, Koronkiewicz said.
___
Information from: Arizona Daily Sun, http://www.azdailysun.com/