Del Monte Fresh Produce NA Inc. has inked an agreement to keep its importing operations at Port Manatee for up to 20 more years.
Del Monte Fresh Produce, a subsidiary of Coral Gables, Fla.-based Fresh Del Monte Produce Inc., signed a lease extension with the Palmetto, Fla., port through August 2021.
On Aug. 18, members of the Manatee County Port Authority approved the new agreement for Del Monte's distribution facility.
The lease includes options for three additional extensions of five years each, according to a news release.
If Del Monte uses all options, the grower-shipper and importer could be importing fruit at the central Florida port until at least 2036.
Del Monte, which has imported fruit at the port since 1989, handles weekly refrigerated vessels containing containers and pallets of Central American bananas and pineapples.
For exports, Del Monte ships linerboard used in packaging and also handles other third-party containers and project cargos.
"We are very pleased to continue our relationship with Port Manatee," Brian Giuliani, Del Monte's Port Manatee-based port manager, said in the release. "The cooperation with Port Manatee is exceptional and has been vital to the growth of our business at Port Manatee."
Since 1989, Del Monte has moved 8.7 million short tons of cargo through the port.
"Extension of Port Manatee's long-term partnership with Del Monte demonstrates the mutual commitment on the part of our port and a most-valued tenant," Betsy Benac, the port authority's chairwoman, said in the release.
Del Monte's Southeast distribution center at the port has become the company's second-largest U.S. facility, according to the release.