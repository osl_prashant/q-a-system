Milk production in the United States during August totaled 18.1 billion pounds, up 2.0 percent from August 2016.Production per cow in the United States averaged 1,919 pounds for August, 24 pounds above August 2016.
The number of milk cows on farms in the United States was 9.41 million head, 71,000 head more than August 2016, but unchanged from July 2017.
23 Major Dairy States Up 2.1% in Milk Production

Milk production in the 23 major States during August totaled 17.0 billion pounds, up 2.1 percent from August 2016. July revised production, at 17.2 billion pounds, was up 2.1 percent from July 2016. The July revision represented an increase of 31 million pounds or 0.2 percent from last month's preliminary production estimate.
Production per cow in the 23 major States averaged 1,948 pounds for August, 26 pounds above August 2016. This is the highest production per cow for the month of August since the 23 State series began in 2003.
The number of milk cows on farms in the 23 major States was 8.73 million head, 66,000 head more than August 2016, but unchanged from July 2017.