USDA Weekly Grain Export Inspections
			Week Ended July 20, 2017




Corn



Actual (MT)
935,262


Expectations (MT)

750,000-1,175,000



Comments:

Inspections fell 187,590 MT from the previous week and the tally was within expectations. Inspections for 2016-17 are up 32.6% from year-ago, compared to 34.7% ahead the previous week. USDA's 2016-17 export forecast of 2.225 billion bu. is up 17.0% above the previous marketing year.




Wheat



Actual (MT)
451,665


Expectations (MT)
400,000-650,000 


Comments:

Inspections dipped 141,663 MT from the week prior and the figure was near the low end of expectations. Inspections are running 18.8% ahead of year-ago early in the marketing year versus 26.3% ahead last week. USDA's export forecast for 2017-18 is at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual (MT)
596,920


Expectations (MMT)
200,000-450,000 


Comments:
Export inspections were up 298,848 MT from the previous week and the figure topped expectations. Inspections for 2016-17 are running 16.8% ahead of year-ago, compared to 17.3% ahead the previous week. USDA's 2016-17 export forecast is at 2.100 billion bu., up 8.1% from year-ago.