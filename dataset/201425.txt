Below we plug USDA's weekly crop condition ratings into our weighted (by production) Pro Farmer Crop Condition Index (CCI; 0 to 500 point scale). It shows the condition of the spring wheat crop declined by 6.46 points from last week to 277.43, which is 89.05 points lower than year-ago.
USDA reports harvest is 9% complete, which is on track with the average pace. Harvest is 46% complete in South Dakota (31%), while harvest is just getting underway in other states. Minnesota has just 3% harvested, compared to 14% on average.





Pro Farmer Crop Condition Index






Spring wheat




This Week




Last Week




Year-ago





Idaho (5.74%)*


19.45


20.94


22.90




Minnesota (12.99%)


53.00


52.48


46.67




Montana (14.95%)


33.19


34.09


61.77




North Dakota (50.80%)


135.63


140.71


179.68




South Dakota (10.36%)


19.99


19.68


34.49




Washington (4.22%)


13.54


13.29


17.23




Spring wheat total


277.43


283.88


366.48




* denotes percentage of total national spring wheat crop production.
Following are details from USDA's National Agricultural Statistics Service (NASS) state crop and weather reports:
North Dakota: For the week ending July 30, 2017, drought conditions continued to add stress to crops and livestock, according to the USDAs National Agricultural Statistics Service. Some areas in the western part of the State received over an inch of rain late in the week, however drought conditions persisted. Some livestock producers have sold cattle because of poor pasture conditions and a lack of hay supplies. Temperatures across the State averaged two to six degrees above normal. There were 6.8 days suitable for fieldwork. Topsoil moisture supplies rated 36 percent very short, 37 short, 26 adequate, and 1 surplus. Subsoil moisture supplies rated 29 percent very short, 37 short, 33 adequate, and 1 surplus.
Spring wheat condition rated 22 percent very poor, 22 poor, 27 fair, 25 good, and 4 excellent. Spring wheat coloring was 78 percent, behind 84 last year, but ahead of 67 average. Harvested was 5 percent, near 3 last year and 7 average.
Montana: Hot and dry conditions with limited precipitation occurred for a majority of the state, according to the Mountain Regional Field Office of the National Agricultural Statistics Service, USDA. High temperatures ranged from the upper 80s to 102 degrees. Low temperatures ranged from 37 to the upper 50s. The highest amount of precipitation was recorded in Culbertson with 0.92 of an inch of moisture while other stations recorded between zero and 0. 58 of an inch of moisture, with the vast majority of stations receiving less than 0.25 of an inch. Crop conditions continue to deteriorate due to the hot, dry weather. Soil moisture conditions continue to decline with 96 percent of topsoil rated very short to short and 90 percent of subsoil rated very short to short, compared with 35 percent of topsoil last year rated very short to short and 37 percent of subsoil last year rated very short to short. Reporters were noting low yields on grain and pulse crops, with some areas being zeroed out by crop insurance.
Minnesota: Warm and sunny days had Minnesota farmers out cutting hay and beginning small grain harvest during the 5.9 days suitable for fieldwork during the week ending in July 30, 2017, according to USDAs National Agricultural Statistics Service. The warm and dry conditions were beneficial for row crop development, though some areas could use some rainfall. Activities for the week included cutting hay, harvesting sweet corn, and isolated aerial spraying of pesticides. Topsoil moisture supplies rated 3 percent very short, 23 percent short, 72 percent adequate and 2 percent surplus. Subsoil moisture supplies rated 2 percent very short, 18 percent short, 77 percent adequate and 3 percent surplus.
Eighty-six percent of the spring wheat crop was coloring, 5 days ahead of average. There were scattered reports of spring wheat being harvested. Spring wheat condition rated 87 percent good to excellent.