Exemption for frac company hits snag in Wisconsin Senate
Exemption for frac company hits snag in Wisconsin Senate

By SCOTT BAUERAssociated Press
The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — Environmental exemptions that would clear the way for a Georgia company to build a $70 million frac sand processing plant in western Wisconsin that environmentalists oppose hit a snag Wednesday in the state Senate.
The Republican chairman of the Senate Natural Resources Committee said he had no intention of taking up the amendment with the exemptions added to a largely technical bill in a late-night vote by the state Assembly last week. While state Sen. Rob Cowles said it wasn't his plan to consider it, "there's always forces above us that can override this guy."
The bill before the Senate committee does not yet include the provision meant to benefit Meteor Timber. Even if Cowles doesn't pass it, the full state Senate could decide to take up the Assembly version of the bill when it meets on its last planned session day March 20.
The activity in the Senate comes as an administrative law judge in Tomah was hearing testimony this week in opposition to the state Department of Natural Resources decision to grant a permit for the project in May.
Construction is on hold pending the judge's decision on whether the permit was properly granted by DNR.
But the state Assembly last week, in a late-night move on its last planned day in session, approved an amendment to exempt Meteor from additional permitting requirements that could be ordered by the judge to protect nearly 16 acres of wetlands that the company plans to fill.
Essentially, the company could move ahead with filling the wetlands even as the appeal to the DNR permit was pending. The bill would have to pass the Senate and be signed by Gov. Scott Walker to take effect.
Cowles said the Assembly amendment "obliterates" the process. Republican Sen. Van Wanggaard, co-sponsor of the underlying bill in the Senate, said he hadn't read the amendments that were adopted in the Assembly and didn't know what they did.
Democratic Sen. Mark Miller urged Wanggaard to kill the bill, calling the exemptions for Meteor Timber "absolutely outrageous."
Meteor Timber is an Atlanta-based company that's planning a sand-processing plant to serve the frac sand industry near Interstate 94 in Monroe County. Environmental groups and others have been speaking out against the project, saying its plans to destroy a 16-acre wetland that includes a 13-acre rare white pine-red maple swamp would cause irreparable harm.
To make up for destroying those wetlands, the company's plans include restoring more than 630 acres of other land, including wetlands, near the 750-acre project site in Millston.
Opponents to the project allege that Meteor is gaming the system with the Assembly amendment to get around any adverse ruling. Company leaders have said environmentalists are trying to bully them and are being obstructionists.
Nathan Conrad, executive director of the pro-business advocacy group the Natural Resource Development Association, said the loss of roughly 16 acres of wetlands will be more than made up for with the restoration of the other 630 acres. Conrad has also defended the project, saying it will create 300 construction jobs and about 100 people will work at the facility that will process and ship industrial sand to oil exploration companies in Texas.
The Ho-Chunk Nation and Clean Wisconsin are challenging the DNR's issuance of the permit. They argue that Meteor's wetland mitigation plan won't make up for the loss of the rare pine-red maple swamp.
They and other opponents spoke out against it at an administrative hearing this week in Tomah. Former DNR employees testified that they were pressured to approve the project and that the wetlands slated to be filled are irreplaceable.
Wetlands act as natural flood control and water filters. They support a wide range of wildlife, are a key component of the ecosystem and in some cases can take hundreds or even thousands of years to develop.
___
Follow Scott Bauer on Twitter: https://twitter.com/sbauerAP