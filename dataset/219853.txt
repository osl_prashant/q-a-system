Producers throughout farm country are relying more and more on loans to stay in business, as the economic picture remains bleak. But, what do you do if your bank turns you down? Welcome to the world of alternative financing. This kind of lending isn’t new. Companies like Upstart and LendingClub have been providing non-traditional lending for years.
However, this category is newer to the ag industry. Companies providing alternative financing to farmers include Ag Resource Management, Conterra Asset Management, Farmland Partners, Farmers Business Network, AgAmerica Lending, John Deere Financial and CHS.
Non-traditional lending has risks, but it provides additional options for farmers, according to Peter Martin, principal and finance consultant at K•Coe Isom. In challenging times, these lenders will be less stringent with borrower requirements. “They will lend on what we think is going to happen going forward, instead of what has happened in the past,” Martin says.
Non-traditional lenders should be used like a physical therapist for your business, says Curt Covington, senior vice president of agricultural finance for Farmer Mac. “The point of going to one of those lenders is to get your business rehabilitated so you become a viable borrower for a traditional bank again,” he says. “You just can’t go to a non-traditional lender with the hopes that everything will be OK somehow. You’ve got to go there with the mindset that this is a three-tofour- year plan [to restructure].”
What To Know. Covington and Martin agree distressed borrowers are more likely to agree to financial terms they wouldn’t normally take. “We get to the point where we’re so desperate that we forget to ask the hard and important questions,” Covington says. “One of the things you have to do as a distressed borrower is get good advice outside of the alternative lender. Make sure you’re getting good competent advice from an independent third source.”
Martin cautions that not everyone working in this space has your best interest in mind. Think twice about anyone charging you an upfront fee for this kind of service. Have a lawyer review any deal you do with a non-traditional lender because they are not federally regulated. Ask smart questions of new lending partners.
Covington recommends asking: How long have you been doing this? What’s your philosophy on lending money to someone like me? How do you structure these deals? Aim to select a reputable nontraditional lender who understands the risks associated with farming.
UNDERSTAND THE RISKS AND REWARDS
PROS:

Flexible. Typically, non-traditional lenders don’t have any regulatory burdens. This can affect everything from how much they lend you to repayment terms.
Quick. These types of lenders can often expedite lending you money, compared to a traditional bank.
Experienced. These lenders have an abundance of practice working with distressed borrowers.
Interest-Only Payments. Some of these lenders have interest-only models, where you only pay interest expense and don’t have to worry about repaying principal for the next couple of years.

CONS:

Term Length. Most loans made through these lenders are short term. But it might require a longer commitment from your non-traditional lender to get your business back on track, Covington says.
Easy. Don’t plan to stay with this type of lending long term, even though they make borrowing money easy, Martin say. Have an exit strategy in place.
Red Tape. Funds provided through these lenders will be thoroughly monitored.
Cost. These types of loans are expensive due to higher rates and fees.