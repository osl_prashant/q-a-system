BC-Cash Prices, 1st Ld-Writethru,0342
BC-Cash Prices, 1st Ld-Writethru,0342

The Associated Press

NEW YORK




NEW YORK (AP) — Wholesale cash prices Monday
                                         Mon.       Fri.
F
Foods

 Broilers national comp wtd av             .9142      .9100
 Eggs large white NY Doz.                   1.91       1.86
 Flour hard winter KC cwt                  16.30      16.60
 Cheddar Cheese Chi. 40 block per lb.     2.1150     2.1150
 Coffee parana ex-dock NY per lb.         1.2280     1.2076
 Coffee medlin ex-dock NY per lb.         1.4278     1.4131
 Cocoa beans Ivory Coast $ metric ton       2462       2462
 Cocoa butter African styl $ met ton        6596       6596
 Hogs Iowa/Minn barrows & gilts wtd av     62.29      62.11
 Feeder cattle 500-550 lb Okl av cwt      182.00     182.00
 Pork loins 13-19 lb FOB Omaha av cwt      93.05      92.04
Grains
 Corn No. 2 yellow Chi processor bid      3.75¼       3.70¼
 Soybeans No. 1 yellow                   10.52½      10.46 
 Soybean Meal Cen Ill 48pct protein-ton  393.40      397.70
 Wheat No. 2  Chi soft                    5.19¼       5.10 
 Wheat N. 1 dk  14pc-pro Mpls.            7.54¼       7.40¼
 Oats No. 2 heavy or Better               2.80        2.79½
Fats & Oils
 Corn oil crude wet/dry mill Chi. lb.     .30          .28 
 Soybean oil crude Decatur lb.            .30¾         .30¾
Metals
 Aluminum per lb LME                     0.9724      0.9290
 Antimony in warehouse per ton             8600        8600
 Copper Cathode full plate               3.1219      3.1079
 Gold Handy & Harman                     1320.40    1322.30
 Silver Handy & Harman                    16.429     16.320
 Lead per metric ton LME                 2454.50    2458.00
 Molybdenum per metric ton LME            15,500     15,500
 Platinum per troy oz. Handy & Harman     957.00     968.00
 Platinum Merc spot per troy oz.          962.10     965.10
 Zinc (HG) delivered per lb.              1.5405      1.5444
Textiles, Fibers and Miscellaneous
 Cotton 1-1-16 in. strict low middling     82.16      78.92
Raw Products
 Coal Central Appalachia $ per short ton   63.10      63.10
 Natural Gas  Henry Hub, $ per mmbtu        2.63       2.61
b-bid a-asked
n-Nominal
r-revised
n.q.-not quoted<29
n.a.-not available