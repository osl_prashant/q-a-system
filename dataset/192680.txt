Some Republican leaders start to fret






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






 
The self-inflicted wounds of President Donald Trump may turn into more than just pinpricks.  
Rep. Jason Chaffetz (R-Utah), the Republican chairman of the House Oversight Committee, has requested the memo in which former FBI Director James Comey reportedly recorded a request from President Trump to stop investigating Michael Flynn. Chaffetz said he was considering subpoenaing the memo, if it was not made available. From his letter to the FBI: "If true, these memoranda raise questions as to whether the President attempted to influence or impede the FBI's investigation as it relates to former National Security Adviser Lt. Gen. Flynn." The move was supported by House Speaker Paul Ryan (R-Wis.).
In a statement issued Tuesday evening, the White House denied the account. Meanwhile, Trump's campaign sent supporters a fundraising email yesterday with the subject line "SABOTAGE": "You already knew the media was out to get us. But sadly it's not just the fake news… There are people within our own unelected bureaucracy that want to sabotage President Trump and our entire America First movement."
Meanwhile, Sen. Lindsey Graham (R-S.C.) has invited Comey to testify in public and also said he wants to see the memo. Some memo if true could lead to allegations of obstruction of justice against the president. According to the New York Times, Trump urged Comey to shut down the federal investigation into Trump's former national security adviser, Mike Flynn. "I hope you can let this go," Trump said, according to a memo Comey wrote after their meeting.
"I am worried, concerned, that continual political drama will drain the energy away from real accomplishments," said Rep. Mac Thornberry (R-Texas), chairman of the House Armed Services Committee, in an interview with the Wall Street Journal. But Chaffetz told NBC Tuesday evening that, “The government is always full of crisis — some of this is very self-inflicted by the White House itself — but we still have to be able to pass meaningful legislation and get it to the president’s desk.”
Sen. John McCain (R-Ariz.) told CBS announcer Bob Schieffer at an award dinner last evening that the Trump scandals have reached a "Watergate size and scale."
Market impacts: With some Republicans now joining most Democrats in calling for further investigations, the growing controversies have threatened Trump's legislative agenda on Capitol Hill, with potential hearings and widening investigations siphoning time and energy from plans to overhaul the tax code, among other initiatives. Gold rose to a two-week high and the yen, a safe haven currency, is rising and the dollar is at the lowest level since November.
Meanwhile, Washington work is continuing. USDA Secretary Sonny Perdue appears today before the House Agriculture Committee. The always worried farm-state lawmakers will throw out more than a few topics for comments from Perdue. These include concerns about rural development losing the Undersecretary position under the recently announced USDA reorganization plan. Cotton and dairy interests will likely ask for any updates on what USDA and the administration can do for aiding those sectors. The coming Trump budget plan for USDA, to be unveiled May 23, will also be a ripe topic for both political and substantive questions.


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.