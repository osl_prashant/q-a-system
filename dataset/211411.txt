Chicago Mercantile Exchange live cattle nearby contracts on Wednesday finished down their 3-cents-per-pound daily price limit, hit by fund liquidation and lower preliminary cash prices, said traders.June and August closed limit down at 124.500 cents and 117.875 cents, respectively. Both contracts ended below their respective 40-day moving averages of 124.545 and 120.100 cents.
CME live cattle's trading limit will expand to 4.500 cents on Thursday following Wednesday's limit-down settlement.
So far this week, a small number of market-ready, or cash, cattle in the U.S. Plains moved at $130 to $134 per cwt, down as much as $7 from a week ago in parts of the region, said feedlot sources.
Remaining cattle bids are at $128 per cwt with no response from sellers. Last week, cash cattle in the Plains traded at mostly $136 to $137.
Computer problems delayed Fed Cattle Exchange price reporting until next Wednesday.
Processors last week may have purchased enough cattle in advance for this week's production, said KIS Futures vice president Lane Broadbent.
Traders and analysts said more than 40,000 animals for sale last week, and wholesale beef prices close to topping out seasonally, contributed to this week's bearish attitudes toward cash returns.
Sell stops and live cattle futures liquidation upended CME feeder cattle contracts.
August feeders ended 3.825 cents per pound lower at 146.125 cents. 
Firmer Hog Futures
Sharply higher cash prices and modest wholesale pork price gains lifted nearby CME lean hogs, said traders.
They said investors bought bullishly viewed hog futures and simultaneously sold cattle contracts because of its bearish market tone.
June, which will expired at noon CDT (1700 GMT), closed up 0.225 cent per pound to 83.000 cents. Most actively traded July finished 0.300 cent higher at 82.575 cents.
Dwindling seasonal supplies could get even tighter as hot summer weather in the Midwest takes hold, which tends to slow animal weight gains, an analyst said.