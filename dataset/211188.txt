Chicago Mercantile Exchange live cattle contracts generally landed in bearish trading territory on profit-taking and traders that sold deferred months and bought August futures, said traders. August was also supported by its discount to prices paid by packers for cash cattle this week at $116 to $119 per cwt. Last week, cattle brought $117 to $118. 
August ended up 0.225 cent per pound at 115.450 cents per pound, and above the 100-day moving average of 115.407 cents. October finished 0.725 cent lower at 114.100 cents, and December 0.725 cent lower at 115.175 cents. 
Some cash prices benefited from tight supplies in parts of the U.S. Plains, said traders and analysts. 
But, they said, seasonally lackluster wholesale beef demand and increased cattle numbers in the months ahead discouraged other packers from bidding up for animals. 
Still, bullish investors believe cash and wholesale beef values have bottomed out seasonally, with grocers featuring more beef that is now competitively priced to pork.
Lean Hog Futures Turn Higher on Cash Discounts
CME lean hogs settled higher on Friday, with buyers attracted to futures that remained undervalued compared to prices paid by packers for market-ready, or cash, hogs, traders said. 
CME's hog index, a barometer for cash prices, for Aug. 2 was 87.11 per cwt. 
Bargain buying after recent losses and purchases by funds contributed to futures advances. 
August , which expires on Aug. 14, closed up 1.250 cents per pound to 83.225 cents. Most actively traded October ended 1.275 cents higher at 66.775 cents, and above the 10-day moving average of 66.330 cents. 
Lean hog contracts made headway despite Friday morning's softer cash and wholesale pork prices as hog numbers start to build seasonally. 
The U.S. Department of Agriculture projected this week's hog slaughter at 2.250 million head, up 11,000 from last week and 50,000 more than a year ago. 
"I think we're living on borrowed time with these bigger supplies around us, but the board (futures) doesn't seem to think so," a Midwest hog merchant said. 
Some packing plants will be closed on Monday for a floater holiday, said traders and analysts. Packers in August typically give employees time off in exchange for work during winter holidays.