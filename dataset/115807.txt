The average value of Canadian farmland rose 7.9% in 2016, according to Farm Credit Canada (FCC). While a gain, the rise marks a slowing in farmland values as prices rose 10.1% in 2015 and 14.3% in 2014. Overall, FCC notes the average national values have continued to increase since 1993.

FCC annually updates its system of benchmark farm properties to monitor variations in cultivated land values across Canada. FCC appraisers estimate market value using recent comparable sales. These sales must be arm’s-length transactions.

Farmland values increased in nearly all provinces. The exception was in Newfoundland and Labrador, where there were not enough publicly reported transactions to accurately assess farmland values. Prince Edward Island experienced the highest average increase at 13.4%, followed by Alberta at 9.5%, Nova Scotia at 9.1%, British Columbia at 8.2% and Manitoba at 8.1%.

The average increase in Quebec was 7.7%. Saskatchewan was up 7.5%, followed by Ontario at 4.4% and New Brunswick at 1.9%.