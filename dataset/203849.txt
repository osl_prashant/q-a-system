Columbia Marketing adds to Daisy Girl line
The Daisy Girl product line of Wenatchee, Wash.-based Columbia Marketing International has expanded to include co-branded Ambrosia, Kiku, Kanzi and RosaLynn apples, said Steve Lutz, vice president of marketing.
The additions illustrate the great progress the brand has made, Lutz said.
"Since the introduction of the Daisy Girl organics brand, sales have taken off," he said.
CMI says Daisy Girl is the No. 1-selling packaged organic apple brand in the U.S.
"That's a significantly accomplishment for CMI and shows the power of strong branding, even with organic consumers, who are often perceived as not being receptive to supplier brands," Lutz said.
 
Deardorff bumps up organic celery, leaf
Deardorff Family Farms, Oxnard, Calif., said it saw about a 20% increase in its organic production in the last year, said Tom Deardorff, president.
"Celery, leaf lettuce - I'd say all the wet vegetable items" are where most of the increase occurred, he said.
Deardorff Family Farms, which grows in Oxnard and Salinas, Calif., as well as in Mexico - in San Quintin, Baja California - has a conventional/organic production balance of about 65% to 35%, Deardorff said.
 
Dole augments organic salad offerings
Westlake Village, Calif.-based Dole Food Co is extending its organic product lines, such as berries, vegetables and salads, said Bil Goldfield, director or corporate communications.
"The Dole Organic Salad line features organic versions of our most popular conventional salad mixes, available in resealable clamshells in various sizes," he said.
This year, Dole will add a kale mix, Super Spinach, baby spinach and arugula to its organic line of salad mixes, Goldfield said.
 
Earl's Organics now pre-ripens avocados
San Francisco wholesaler Earl's Organics now has a preconditioning program for organic avocados, said Earl Herrick, owner.
"We're continuing to see some real specific growth in specific categories. One is the avocado," he said.
It's an essential service, he said.
"We have seen great growth because not everybody has the bandwidth or space to do that themselves," he said. "If you want to buy an avocado, you want one that's either ready to eat or will be soon. If you do, you're going to buy more. We're seeing our sales grow 25% in a year."
 
Giumarra wholesales certified vegetables
Los Angeles-based The Giumarra Cos. has announced its Los Angeles wholesale operation, Giumarra Bros. Fruit Co., recently added certified organic vegetables to its product line. The new organic line includes lettuce, cabbage, broccoli, celery, cauliflower, beets, chard, and kale, along with a variety of specialty vegetables, greens and herbs.
"The demand for organic produce is rising globally," Chuck Anunciation, division manager for Giumarra Bros. Fruit Co., said in a news release. "We feel it is important to continue serving our customers' growing needs with a broad organic offering."
Anunciation and his staff bring more than 25 years of organic buying expertise to Giumarra Bros. Fruit Co. The new category is certified organic, California-grown and will be available year round.
"We have seen double digit growth in our wholesale organic category," says Kellee Harris, western region business manager. "Our new vegetable offering will nicely complement our organic fruit offering, as well as our conventional produce."
Giumarra recently revamped its warehouse facility to accommodate the growth resulting from the new category.
The company will be showcasing its organic vegetables this summer at the Los Angeles Wholesale Market during planned demos designed to promote the vegetables at both retail and foodservice levels.
 
Homegrown Organic packs blueberries
Porterville, Calif.-based Homegrown Organic Farms has opened a blueberry packing shed near Salem, Oregon, this year, said Scott Mabs, CEO.
"We're packing fruit for northwestern blueberries," he said.
The facility opened in June, Mabs said.
"We forged a relationship with packinghouses there," he said,
The company invested in an existing shed, he said. Mabs declined to provide more details about the deal.
 
Kern Ridge Growers steps up acreage
Bakersfield, Calif.-based Kern Ridge Growers LLC has expanded its acreage a bit, said Chris Smotherman, salesman.
"Our numbers the last three years, it looks like demand has exceeded what we've had available to market, but we've been cautious," he said.
The caution is a respond to capricious consumer buying habits, which fluctuate with prevailing economic conditions, Smotherman said.
"When consumers have less money, they're not willing to part with extra money for organic product," he said.
However, the company expects to increase volume by about 15% this year, Smotherman said.
The expansion is about 40 acres, he said.
 
Lakeside Organic ready to build plant
Watsonville, Calif.-based Lakeside Organic Gardens is building a new base of operations, said Katie Bassman, marketing and communications representative.
"We're getting all new facilities - going from about a 10,000-square-foot cooling space to 50,000," she said.
All told, the company is increasing its square footage - including office space - from about 17,000 to 65,000 square feet, Bassman said.
The new cooler will have solar power as well as a new water-recycling system, Bassman said.
A groundbreaking is scheduled for late September, she said.
Meanwhile, Lakeside is operating in temporary structures on its grounds, Bassman said.
Lakeside and its owner, Dick Peixoto, also are investing in the community, Bassman said.
"Last December, our owner put $2 million into a fund with a local nonprofit to build an organic agriculture and sustainable learning center," she said. "People can learn how to get started farming organically and sustainably or people can just learn about what we do."
Lakeside also is marking its 20th anniversary this year, Bassman said.
 
National Sustainable Agriculture Coalition
In May, the National Sustainable Agriculture Coalition announced publication of its Organic Farmers' Guide to the Conservation Reserve Program Field Border Buffer Initiative.
The guide is intended to be a free resource, one of many free guides NSAC regularly produces for farmers and farm groups, for organic farmers interested in accessing the U.S. Department of Agriculture's new Organic Buffer Initiative.
"Organic farming already provides many environmental benefits, including improved soil health and water quality," Greg Fogel, senior policy specialist with the coalition, said in a news release. "The Organic Buffer Initiative is a great new tool that will help organic farmers looking to take their conservation efforts to the next level, and we hope that our farmers' guide will help them seamlessly access and utilize the program."
NSAC's guide includes eligibility and application information, program basics, detailed descriptions of key conservation practices and associated payments, as well as two producer profiles and resources for additional information.
The buffer initiative, which is administered by the Farm Service Agency as part of the Conservation Reserve Program's Continuous Sign-up, aims to establish up to 20,000 acres of new conservation buffers. Conservation buffers can come in many varieties, but are generally described as a small areas or strips of land in permanent vegetation that are designed to slow runoff, provide shelter for wildlife, and prevent erosion along riverbank areas.
Through the Organic Buffer Initiative FSA provides farmers with rental payments, cost-share payments, and in many cases incentive payments for land that is set-aside for conservation buffers for a period of 10-15 years. The initiative helps organic producers limit the impacts of pesticide drift, enhance their conservation systems, and meet National Organic Program certification requirements for natural resource and biodiversity conservation.
Unlike the general sign-ups that occur under other CRP programs, CCRP producers may enroll at any time throughout the year. The program also has no bidding and ranking system, and the land is enrolled automatically if it meets the eligibility criteria.
CCRP-eligible practices include: riparian buffers, wildlife habitat buffers, wetland buffers, filter strips, wetland restoration, grass waterways, shelterbelts, windbreaks, living snow fences, contour grass strips, salt tolerant vegetation, and shallow water areas for wildlife.
Under the new organic initiative, farmers are free to use whichever CCRP-eligible practice or suite of practices that best suits their particular needs. However, in most cases, we believe farmers will be most interested in the following practices.
The guide is available at http://sustainableagriculture.net/wp-content/uploads/2016/05/2016_5-CRP-....
 
Oneonta transitions more groves
Wenatchee, Wash.-based Oneonta Starr Ranch Growers is adding to its organic program, said Bruce Turner, national salesman.
"We added significant acreage into our transitional program. Over 20% of our production will be organic during the 2018 harvest and we have plans to continue that growth," he said. "Thirty percent of our stone fruit is organic and in the best varieties that offer flavor and color. The quality of our organic stone fruit pack is simply amazing and well received by retailers."
Oneonta Starr Ranch is adding organic pears in 2017, Turner said.
"By 2020, over 25% of our apples will be organically produced in all the best varieties - Honeycrisp, gala, fuji, Pink Lady, granny smith and our proprietary Juici and Koru," he said.
 
Pacific Organic gets into peas, beans, squash
San Francisco-based Pacific Organic Produce, which long has focused on fruit, debuted an organic vegetable line this year, said John Stair, domestic commodity manager.
"The company was founded on the marketing of organic fruit and has had a strong organic fruit focus throughout our history," he said.
However, after speaking with some of its retail/distributor partners, the company decided it was time to develop an org vegetable program to complement its fruit lineup, Stair said.
"The program is primarily focused on growing product in Mexico and hitting windows where the like commodities are less plentiful," he said.
The program started by including onions, cucumbers, zucchini, snow peas, snap beans and yellow squash, but there are plans to develop the program further, Stair said.
"I feel like we had a good first season," he said, noting that onion shipments were continuing.
Pacific Organic Produce also started shipping organic jicama out of Mexico, Stair said.
 
Rainier Fruit ships more organic apples
The organic apple program at Selah, Wash.-based Rainier Fruit Co. has grown about 15% in the past year, thanks, in large part, to increased shipments of Honeycrisp, gala and fuji varieties, said Blake Belknap, organic sales manager.
"We believe if we're stuck trying to push old varieties, that doesn't make a lot of sense," he said.
The company plans to increase its organic tonnage by 20% in the next year, Belknap said.
"That has become a really big part of our business," he said.
 
Stemilt promotes Organic Lil Snappers
Wenatchee, Wash.-based Stemilt Growers LLC has a new line of Organic Lil Snapper apples and pears, said Roger Pepperl, marketing director.
"This will be a big program. We hope to have distribution across all of North America," he said.
Stemilt has converted additional apple orchard blocks and will continue to transition crop from conventional to organic each year for the next five years, Pepperl said.
Stemilt now also has a new Fresh Blender 4-pound pouch bag for consumers of smoothies and juice, Pepperl said.
"This is a fun program that include recipes and usages," he said. "We feel it will ignite the purchase of produce for drinks by offering a main ingredient for the recipe."
 
Viva Tierra updates website
Mount Vernon, Wash.-based Viva Tierra Organic Inc. is working on redeveloping its website, www.vivatierra.com, said Addie Pobst, organic integrity and logistics coordinator.
That project is scheduled to be completed by the late summer or fall, Pobst said.
The company also has a new sales assistant, Courtney Devries, who joined June 1, Pobst said.
It's Devries' first job in the produce industry, Pobst said.
"She's helping sales staff compiling reports on what customers need and what products are available, getting info together," Pobst said of her new colleague.
Viva Tierra moved to Mount Vernon from nearby Sedro-Woolley, Wash., in July 2015, Pobst said.
 
World Variety delivery sales increase
Los Angeles-based Word Variety Produce, which markets the Melissa's label, grew sales by more than 10%, said Robert Schueller, director of public relations.
"Our online organic offerings have increased significantly - kind of like (community-supported agriculture) boxes delivered to customer homes," Schueller said. "Only, we deliver by FedEx."
Word Variety Produce's online delivery program is about five years old, Schueller said.
The program diverges from CSA programs in that it focuses on products customers want, rather than what might be available at a certain time, Schueller said.