North Carolina sweet potato growers say they'll be in the fields early this year and the harvest should bring the best yields in years.
Faison, N.C.-based Southern Produce Distributors Inc. expects to begin digging somewhere between Aug. 20 and Aug. 25, said Stewart Precythe, the company's president and CEO.
"We're a little earlier this year. We normally don't like to dig until Labor Day."
An early start was the only blemish on the season as of Aug. 8, Precythe said.
"We had an excellent planting season, it's not been real hot, we've had good rains. The survival rates are some of the best I've seen in 47 years."
North Carolina sweet potatoes saw their share of very hot temperatures this season, but timely rains helped prevent any negative heat-related effects, said George Wooten, owner of Wayne E. Bailey Produce Co. Chadbourn, N.C.
Wayne E. Bailey expects to begin digging the week of Aug. 15, about three weeks earlier than usual, Wooten said. Last year's storage potatoes would likely clean up in September, with new-crop cured kicking off in the second half of the month.
Nashville, N.C.-based Nash Produce expects to kick off its new-crop deal in late September, right on time, said Laura Hearn, marketing and business development director.

"Everything looks good. We've had good weather, ample rain. The crop looks healthy."
North Carolina's official acreage estimate isn't out yet, but Precythe is predicting a slight increase in the state, up to 5%. It likely would have been up even more, he said - low prices for other crops caused growers to look at pulling them and adding sweet potatoes - but plants were hard to come by.
Some early plants were hit by cold and some late by heat, which will affect yields, but potatoes planted between about May 10 and June 20 should yield well and have "superior" quality, Precythe said.
Southern Produce will ship cured-to-cured this year, but other shippers will likely have to ship some uncured because of dwindling 2015-16 supplies as of early August, Precythe said.
"The market's firm now, and the new-crop market should be firm. It should hold through August, September and October."
Markets were stable in early August, slightly lower than both the year before at the same time and three months earlier, Wooten said. He said they should stay were they were or slightly increase after the new crop begins shipping.
On Aug. 9, the U.S. Department of Agriculture reported prices of $16-18 for 40-pound cartons of U.S. No. 1 sweet potatoes from North Carolina, down from $16-19 last year at the same time.Nash Produce also will ship cured-to-cured, Hearn said.
Nash Produce is increasing production of its purple-skinned/white flesh and white-skinned/white flesh sweet potatoes this year, after growing the varieties on a trial basis in the past.
The company also is test-marketing a couple of new products this year, Hearn said.