Add Baldor Specialty Foods to the growing list of produce companies marching toward zero food waste.
The New York-based company announced in December it is diverting 100% of the waste generated from its fresh-cut operation from landfill through a creative mix of partnerships.
Baldor's SparCs (that's scraps spelled backwards) program provides produce for a New York cooking school and cafe, a juice company and a farm that uses the waste for animal feed.
"We process over 1 million pounds of produce a week," said Thomas McQuillan, Baldor's director of sales and sustainability.
"One hundred and fifty thousand pounds of SparCs are generated as a result of this production. All of this food product is diverted from landfill."
McQuillan said juice customers pay 30-70 cents a pound for the company's excess product. A pig farm also pays for the scraps, with the price fluctuating based on volume.
"This food product is nutritious, and its value was simply overlooked," McQuillan said.
"There are many food products that we discard in our culture that should be utilized as edible food. Our SparCs initiative looks to find opportunities to utilize this food first for human consumption and second for animal consumption. Discarding this food is no longer an option."

Lemons
Companies looking to reduce their waste have plenty of options. The Limoneira Co., Santa Paula, Calif., introduced Misfits in 2015, finding a new use for wind-scarred lemons.
Director of marketing John Chamberlain said the effort has allowed the company to increase its fresh lemon volume 10% rather than sending the imperfect product to juicing.
"We expect this to grow as more consumers and the supply chain support this effort," he said.
"Limoneira Lemon Misfits tap into the growing trend for using the whole fruit and eliminating waste. Consumers, especially millennial shoppers, embrace sustainability. They dislike food waste, desire the authentic, and messages about the importance of farming resonate with them."
Misfits also offer a consumer-friendly price point, he said.

Potatoes
Potato grower-shipper Black Gold Farms, Grand Forks, N.D., also has tried to "work the ugly food angle," said CEO Eric Halverson.
"I think this has some promise, but this channel will only be able to move a modest amount of product," he said.
"We certainly try to move our unmarketable product by sending it to livestock operators when possible."
Halverson, however, actually has bigger aspirations on the food waste front.
"I believe the key is to prevent those products from becoming waste in the first place," he said.
"In our business most of the waste comes in the form of unharvested crop due to quality concerns. This is usually a result of too much rain or heat. These potatoes don't even make it to the shed due to spoilage. From a sustainability standpoint, when this happens that means all of the inputs we put into the crop goes to waste, effectively increasing our environmental footprint per pound of marketed crop. Our efforts have been around preventing this type of spoilage."
Halverson said those steps involve better coordination and planning with customers as well as improved land and variety selection.
"It would be easy to ignore unharvested acres in a sustainability discussion," he said.
"The story is not nearly as interesting as some of the other activities that are being promoted. If we truly want to reduce our environmental impact, it starts in the field."

Compost, recycling
Stemilt Growers has been working on waste reduction for four decades, said director of marketing Roger Pepperl. He said that although the Wenatchee, Wash.-based grower-shipper provides some waste to animal feed programs, the zero-waste company's focus in this area is its 23-acre compost farm on Stemilt Hill.
"Last year, we fed 1,400 acres of cherries with our composted waste," he said.
"We also compost shrimp and oyster shells from a Seattle fishery, manure from a local equestrian center and use our lime and some of our competitors' waste also. We are also beginning to take food waste from our local school district's cafeterias and composting this waste also."
It doesn't stop there. Stemilt also takes lawn waste from local landscapers, and it shreds its old wooden pallets for compost.
Recycling also is an emphasis. Pepperl said Stemilt's plastic banding is shredded and recycled. Cardboard, meanwhile, is converted into fruit trays.
The Oppenheimer Group, Vancouver, British Columbia, has found even more uses for produce that's not fit for retail. Marketing and communications manager Karin Gardner said that slightly distressed culls from the company's Calgary operation are routinely donated to local food banks, but when fruit is not usable by humans, the company sends it to area animal sanctuaries.
The company's Vancouver location also prioritizes donations to food banks, but all culled product from that warehouse that is not fit for human consumption is composted by Enterra, a company that specializes in the development and manufacture of ecological insect protein used in the production of fertilizer and animal food.
Gardner said Enterra uses items such as peppers, kiwifruit, apples, squash and melons to feed soldier fly larvae.
The larvae's development is halted at a specific stage, and the insects are dried and used for feed. The droppings the larvae generate while feeding are filtered and used as fertilizer, which is certified for organic crop production.
Enterra's products, Gardner said, offer a sustainable alternative to more costly and resource-intensive feed ingredients.