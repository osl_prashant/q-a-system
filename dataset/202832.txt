Two more companies have pulled out of the California asparagus deal.
A.M. Farms, Stockton, Calif., no longer grows asparagus, said owner Marc Marchini.
"It was a good run while we were at it," he said.
The company had been growing asparagus in Stockton since the 1930s.
"I fully expect big box stores and people like that are going to try to get (asparagus) from the cheapest place they can," he said.
"That's going to be Mexico or Peru."
The firm continues to grow a number of crops, including grapes, tomatoes, nuts and vegetables, and will concentrate on items that can be mechanically harvested, he said.


Water, inputs

Monterey, Calif.-based Dole Fresh Vegetables no longer will market California asparagus, said Tyler Mollring, product manager for asparagus.
For the past four years, Dole has marketed asparagus from Harris Ranch, Coalinga, Calif., but faced with water shortages and the rising costs of growing the crop, Harris Ranch "decided to get out," Mollring said.
"I see the California asparagus deal becoming more and more challenging," he said.
Dole will continue to market asparagus from Caborca, Mexico, from the second half of January through April, then source from central Mexico into October or November.
Peru will provide the company with asparagus in December and January.