You have your farm or ranch financial information entered into Quicken (Deluxe) and categorized each transaction by the proper type of income or expense. With this information, you can provide an up-to-date cash flow or tax summary report to your banker or accountant at a moment’s notice.  
Getting this far with your financial documents is a monumental accomplishment, but how do we take the next step in farm or ranch financial analysis? The answer is "tags."
 
Do you have multiple enterprises within your operation? Have you ever wondered which crop is more profitable? How much do you spend on family living? Do you need to have a better system for tracking equipment repairs? Tags can help you glean this information from your Quicken file without complicating your category list.
 
Many Quicken users are not aware of the tag feature. This article discusses how to get started with tags, and how to use them to enhance your financial records.
Tags vs. Categories
Tags are similar to categories, allowing you to easily sort transactions. Unlike categories, tags are not associated with a tax line item. Transactions can be given both categories and tags. In fact, each transaction can have multiple tags and can even be split by tags.
 
Reports filtered by tag are more specific and narrower than reports filtered by category. 
 
For example, you want to track your repairs by each piece of equipment. When you enter a transaction, the category will be “Farm Repairs” and the tag will be for the piece of equipment. 
• Category: Farm Repairs 
• Tag: Tractor 1, Tractor 2, Tractor 3…
 
By tagging each piece of equipment, we can now run reports for all “Farm Repairs” (category report) or for each individual tractor (tag report). 
Tags vs. Sub-categories
You could do something similar to the example above by using sub-categories. However, subcategories may not work for every situation. This time, let’s think about tagging transactions by crop or enterprise. 
• Category: Chemicals, Grain Sales, Fertilizer, Farm Insurance
• Tag: Corn, Soybeans
 
Now we have multiple combinations of categories and tags. We could create a subcategory under each main category for corn and soybeans. However, it would be easier to simply tag the transaction by the appropriate commodity. 
Getting Started with Tags
When starting to use tags, it is best to start as simple as possible with only a few tags.
 
In most cases, when entering transactions, the tag column is not visible without changing your register columns preferences. To change these preferences click on the gear icon on the top right just below the search bar. Check the box next to “Tag”. 
 
To create a tag, simply type your tag word into the tag column. Once you press tab or enter, a dialog box will appear, click save. Another way to add tags is to view the Tag List. Go to Tools>Tag List and click the “New Tag” button on the bottom left of the dialog box. 
Running Reports by Tag
By using tags you can create customized reports. To do this, create the desired report using the reports menu. Once a report is open tap the “Customize” gear on the top right hand of your report. Then select the “Tags” tab and uncheck the desired tag (you must have created at least one tag for the tag tab to appear). This will include or exclude the desired information regardless of the category or account. 
 
In some reports you may also be able to change the column to tag, or subtotal by tag using the dropdown menus across the top of the report. 
Splitting Transactions 
If your operation is like many others, occasionally family expenses make their way onto the business checkbook or credit card. One useful tag is “Family Living”. By using a “Family Living tag”, you can filter out family expenses from your business financial reports.
 
If the total amount of a transaction is a family living expense, you can simply tag the entire transaction as “Family Living”. 
 
However, if only a portion of the transaction is a family living expense, you will need to split the transaction. When entering data press Ctrl+S, the “Split Transition” dialog box will appear. Now you can assign each portion of the amount to different tags. 
 
You enter a transaction from your local farm store for a total of $81.50. On the receipt for this transaction, you notice that a candy bar was purchased for $1.50. You can split the transaction, tagging the expense of the candy bar as “Family Living”. You can categorize the remaining $80.00 of the transaction to the appropriate business expense category. Now, when you run a report the $1.50 will not be included as a business expense. 
 
These are just a few simple ideas do get you started with tags. If you need more help with Quicken for your farm or ranch check out Oklahoma State University’s Farm/Ranch Quicken website http://agecon.okstate.edu/quicken/.