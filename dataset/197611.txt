USDA Weekly Grain Export Inspections
			Week Ended May 18, 2017




Corn



Actual (MT)
1,144,187


Expectations (MT)

850,000-1,250,000



Comments:
Inspections fell 276,082 MT from the previous week and the tally matched expectations. Inspections for 2016-17 are up 51.8% from year-ago, compared to 53.9% ahead the previous week. USDA's 2016-17 export forecast of 2.225 billion bu. is up 17.2% above the previous marketing year.



Wheat



Actual (MT)
674,559


Expectations (MT)
425,000-700,000 


Comments:

Inspections slipped 20,457 MT from the week prior and the tally was near the upper end of expectations. Inspections for 2016-17 are running 35.0% ahead of year-ago compared to 33.6% ahead last week. USDA's export forecast for 2016-17 is at 1.035 billion bu., up 33.5% from the previous marketing year.




Soybeans



Actual (MT)
348,535


Expectations (MMT)
250,000-400,000 


Comments:
Export inspections climbed 64,060 MT from the previous week, and the tally was within expectations. Inspections for 2016-17 are running 16.3% ahead of year-ago, compared to 15.8% ahead the previous week. USDA's 2016-17 export forecast is at 2.050 billion bu., up 5.9% from year-ago.