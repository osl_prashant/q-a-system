Superior Fruit has selected Joseph Rumley as its chief financial officer.
Rumley comes to the company from Santa Paula, Calif.-based citrus grower-shipper Limoneira Co., where he had been CFO for about eight years.
Prior to joining the produce industry, Rumley was an audit partner at accounting firms McGladrey & Pullen and Grant Thornton.
“We’re really excited to have him as part of the team, with his expertise and knowledge base,” said vice president of business development Roger Privett III.
Superior Fruit recently expanded and now markets California strawberries grown on about 2,000 acres in Oxnard, Santa Maria and Salinas.
The name of the company’s farming operation is West Coast Berry Farms.