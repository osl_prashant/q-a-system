The Port of Oakland, (Calif.), said containerized export volume increased 9% in January, compared to the same month a year ago. It was the seventh straight month of rising exports as the port, and the 12th increase in 13 months.
Agriculture commodities continue to drive export increases, thanks to strong harvests and aggressive marketing, according to a news release from the port.
"We've been cautiously optimistic about 2017 cargo volumes and this is a solid start to the year," said Maritime Director John Driscoll, in the release. "In particular, we're gratified by the continued success in our export business."
The increase in exports is notable because the strong U.S. dollar often dampens exports, as U.S. commodities become more expensive overseas, according to the release.
In honor of the Port of Oakland's 90th anniversary, Mayor Libby Schaaf declared Feb. 12 as Port of Oakland Day.
"What a moment to celebrate," said Schaaf, in a news release. "What an incredible impact this organization has on the region, the nation and the world."
Schaaf was previously public affairs director at the port.
"We pioneered trans-Pacific air travel and we brought container shipping to the West Coast," Chris Lyte, executive director, said in a news release. "And we're far from done - we're transforming to upgrade our status as a global gateway."