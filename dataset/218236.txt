Ranchers in western Texas want local culinary students to know more than how to cook a good steak.

They took them to a ranch to learn what goes into producing quality beef.

In this report from the Texas Farm Bureau on AgDay, Ed Wolf shows the class where students are immersing themselves in all things beef.