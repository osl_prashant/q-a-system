C&S Wholesale Grocers is providing support to victims of Hurricane Maria in Puerto Rico and to those affected by the California wildfires, according to a news release.
The company made hurricane relief contributions to:

 Banco de Alimentos de Puerto Rico (Food Bank of Puerto Rico);
 Hispanic Federation; and
 United Way de Puerto Rico.

C&S partnered with Feeding America and Associated Supermarket Group to donate food and water to Puerto Ricans, the release said, as well as other items needed.
To help the families and victims of the deadly wildfires throughout California, the company provided snacks to the American Red Cross, according to the release.