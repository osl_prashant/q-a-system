Irene Moyo, a cook at Phangani Vocational Training Centre, no longer wakes up at dawn to prepare breakfast for the students.Until the institution installed a manure-fed biogas digester three years ago, the mother of five spent two hours a day collecting firewood in the nearby mountains to meet the school's heating and cooking needs.
"It was very exhausting ... because of the distance to the mountain, chopping firewood and pushing the wheelbarrow," said 43 year-old Moyo, who works in Matabeleland South province in southwestern Zimbabwe.
"It became much tougher when it was wet, as firewood produced a lot of smoke which affected my eyes," she said.
The biogas stove lights instantly and is quicker at boiling sugar beans, eggs, sorghum porridge and tea for the 200 students. She no longer worries about making the lunch hour and dinner schedule.
There is also no shortage of student and staff volunteers to feed cow dung into the digester, as the payoff is warm bathing water on chilly mornings.
Biogas - the process of producing energy by fermenting waste - is gaining in popularity around the world as a way of reducing reliance on fossil fuels, cutting deforestation and providing cheap, reliable and easy-to-produce local energy.
It has proved an attraction in rural areas, beyond national power grids, where animal dung is plentiful but firewood is becoming harder to access.
But a $3 million project to roll out biogas across Zimbabwe, set up by the government in partnership with development organizations SNV and Hivos International, has had a low take-up in most rural areas.
Cost is a problem, in a country where 20 percent of the population live on less than $1.90 a day.
Over the last five years, about 40 digesters have been built at farms and institutions, and some 250 home units - well short of the project's aim of 7,400 home digesters by 2017.
"Considering the high costs associated with constructing biogas digesters for many rural households, the uptake has been good," said Blessing Jonga, biogas expert at the Ministry of Energy and Power Development.
He said home units can cost $800 and institutional units $2,000 or more, with cement the biggest expense.
People also have to buy stoves, lamps and refrigerators designed to run on biogas.
Despite the challenges, Zimbabwe is pushing to scale up biogas use, said Dumisani Nyoni, Matabelelend North Provincial officer for the Agriculture Ministry.
"We realize that with shortages of electricity in Zimbabwe and Africa in general, there is need to think (of) renewable energy and promote the use of biogas and solar systems," Nyoni said.
Cutting Costs
One way to cut costs and expand biogas use is to train local technicians to make biogas stoves, lamps and other equipment, said Kuda Mudokwani, a researcher specializing in climate change and disaster risk reduction at the Fambidzanai Permaculture Centre, a non-governmental organization in Matabeleland South.
"Right now biogas products, even refrigerators, come from China but they can be modelled and produced here ... This will also create jobs for our people," said Mudokwani.
People in rural areas can mold their own bricks and crush quarry stone which will also reduce the costs of building a digester, said Mudokwani.
"This way, we will beat the cost associated with setting up digesters and roll out the programme more widely in Zimbabwe," Mudokwani added.
In the meantime, the government has made imported solar and biogas products duty free, to help cut costs, said Jonga.
Agriculture and mechanisation officer with the Agriculture Ministry, George Chinyama, said households can get around the high set-up costs by saving as communities.
"That way they can also approach the Ministry of Energy to assist them," said Chinyama.
Some district councils have set up revolving funds which can be lent to communities to build a digester and paid back after they have sold their crops, said Sandra Gobvu, a spokeswoman for Environment Africa, an NGO that has also been backing the use of biogas in Zimbabwe.
They can also earn money from selling the nutrient-rich manure which is a by-product of the digesters, she said.
The $3 million push, which is due to end this year, aims to help Zimbabweans run everything from hospitals, clinics and schools to commercial dairy farms on biogas power.
The national Rural Electrification Agency and its partners will continue to fund and promote biogas after the project ends, Nyoni said.
Stella Nyanhete, a farmer who has installed a biogas digester for her home in Mashonaland Central province, said it had proved a good source of energy for cooking and lighting. The manure from six cattle is enough to run her digester.
"It's a resource that never runs out as long as we have cattle that produce dung," she said.