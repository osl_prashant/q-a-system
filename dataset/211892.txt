As the trend for fruit and vegetables offered in stand-up pouches continues, Sev-Rend Corp. is releasing its own version of the popular packaging. 
Calling it a “strategic addition” to its portfolio in a news release, Sev-Rend, Collinsville, Ill., offers customized package items, according to a news release.
 
“Sev-Rend’s vertical integrated manufacturing process assists to this addition of such a packaging type without hindering lead times or quality,” according to the release. “Pouches have become a preferred packaging type for many commodities. This drive for demand of a local source for quicker lead times has allowed Sev-Rend to transition into an ever-growing market.”
 
The company also has a research and development division to focus on unique packaging. The company launched its Clear-View Pouch at the Produce Marketing Association’s Fresh Summit in October. The pouches protect against damaging UV light for items such as potatoes, which are subject to greening.