North Dakota’s drought-stressed pastures, especially pastures stressed during the fall of 2017, should receive special care this spring to help them recover from the drought, North Dakota State University Extension Service grazing experts advise.
“It is critical that these pastures are given adequate time to recover,” says Miranda Meehan, livestock environmental stewardship specialist. “Grazing too early in the spring can result in decreased total forage production for the entire grazing season.”
Decreased forage production is a concern because North Dakota experienced widespread drought in 2017. More than 99 percent of the state was subjected to some type of drought conditions during the growing season.
This was the worst drought the state had seen since 2006, according to Adnan Akyuz, state climatologist and professor of climatological practices at NDSU. In some areas, conditions were worse than those experienced during the drought in 1988, which was the beginning of a particularly dry five-year period in North Dakota.
The dry conditions are persisting. Approximately 97 percent of the state is drier than normal, and current conditions indicate that the drought could extend into 2018, Akyuz says.
As a result of the decreased forage production during last year’s drought, many of the state’s pastures received excess grazing pressure. Many pastures in drought-impacted areas also experienced some level of overgrazing, NDSU Extension agents reported in weekly surveys.
“Overgrazing affects the entire rangeland plant community, leading to a loss of plant species diversity and biomass, soil erosion and weed growth, and a reduction in the soil’s ability to hold water,” warns Kevin Sedivec, Extension rangeland management specialist. “These issues have the potential to result in long-term detrimental effects on overgrazed pastures, but a few key management steps this grazing season can help pastures recover.”
Grazing before grass plants reach the appropriate stage of growth for grazing readiness causes a reduction in herbage production, which can reduce the recommended stocking rate and/or animal performance, the specialists add. Grazing readiness for most domesticated pasture is at the three-leaf stage, whereas grazing readiness for most native range grasses is the 3 1/2-leaf stage.
In North Dakota, the recommended time to begin grazing native range is mid to late May, which coincides with grazing readiness in most cool-season native range grasses while optimizing the use of the invaded cool-season grasses such as Kentucky bluegrass and smooth brome. Domesticated grass pastures, such as crested wheatgrass and smooth brome, reach grazing readiness two to four weeks earlier than native range, permitting grazing in late April to early May.
However, a pilot project NDSU Extension conducted in 2017 found the exact dates varied widely across the state, reinforcing the importance of making decisions based on monitoring data and not calendar dates.
Extension agents in 21 counties will be monitoring grazing readiness this spring.
“This year, we may see a delay in grazing readiness in areas impacted by the 2017 drought, especially in pastures that were overgrazed and did not receive adequate time to recover,” Meehan says.
Strategies to avoid grazing native range prior to grazing readiness include:

Grazing domesticated grass pastures in May
Providing supplemental forage to livestock on domesticated pasture or hay land
Using winter annuals that were established last fall for early spring grazing or hay
Continuing dry lot feeding in May

“It is important to allow adequate recovery to native pastures,” Sedivec says. “For our native grasses, grazing before grass has reached the 3 1/2-leaf stage can result in a loss of 45 to 60 percent of the potential forage production. This ultimately leads to a reduction in the recommended stocking rate and lowered animal performance.”
Producers should have a grazing management plan in place. The plan should include the possibility of drought continuing into the 2018 growing season.
Implementing the plan in a timely manner is important because 80 percent of the grass growth on rangeland in this region is dictated by May and June precipitation, and drought conditions during that time will reduce the amount of grass available on pasture and rangeland for the duration of the grazing season, as well as hay land, the specialists say.
“Actively managing your grazing resources to prevent overgrazing will reduce the length of time it takes to recover from drought and improve the long-term sustainability of your operations,” Meehan notes.
For more information on determining grazing readiness and managing drought, contact your county office of the NDSU Extension Service or check out the following NDSU Extension publications:

“Ranchers Guide to Grassland Management IV” at http://tinyurl.com/grassmgmt
“Strategies for Managing Drought in the Northern Plains” at http://tinyurl.com/DroughtManagementStrategies