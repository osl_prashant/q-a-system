Despite a challenging period for fresh avocados, Santa Paula, Calif.-based Calavo Growers Inc. reported double-digit income growth for the quarter ending Jan. 31.
 
Boosted by strong sales of fresh-cut products from its Renaissance Food Group LLC business segment, the company reported revenue grew 11% over year-ago levels. At $226.6 million, first quarter revenues set a record and compare with $204.6 for the same quarter a year ago, according to a news release.
 
Renaissance Food Group revenues rose 29%, according to the release, and gross margin increased 60% compared with a year ago. First-quarter results include nearly $1.2 million in nonrecurring operating expenses related to recent senior management staff transitions, according to the release. Excluding those expenses, adjusted net income in the first quarter of fiscal 2017 totaled $6 million, compared with net income of $6.3 million for the same quarter a year ago.
 
"Calavo's double-digit revenue growth and solid profitability during the fiscal 2017 first quarter were paced by a sharply higher top line and notable gross margin expansion in our RFG business segment," chairman, president and CEO Lee Cole said in a new release.
 
Fresh outlook
 
Cole said in the release that the first month of the quarter - November - was hampered by inconsistent avocado industry supply. However, he said recent trends give the company confidence about prospects for the remainder of the fiscal year.
 
"Calavo's multi-platform business model remains one of the key elements of our consistent growth in annual revenue and earnings," Cole said in the release.
 
Fresh business segment sales equaled $112.1 million in the fiscal 2017 first quarter, about unchanged from $113.1 million a year ago.
 
Fresh units packed and sold in the most recent quarter totaled 3.6 million units compared with 4.7 million in the first period last year, according to the release.
 
First-quarter sales in the RFG business segment surged 29% to $97.7 million, reflecting the addition of customers, penetration into newer markets, and the broadening of product capabilities, according to the release.
 
"I continue to be confident that Calavo has begun the fiscal year on a path to post double-digit increases in revenues and gross margin dollars, leading to record earnings per share in fiscal 2017," Cole said in the release.
 
Despite a challenging first quarter for fresh avocados, Cole said the company sees promising indicators for volume and profitability of the avocado business.