Weed control isn’t a one year plan—it spans over multiple seasons and involves strategy to make sure you and farmers stay ahead of resistant weeds. This time of the year you might find yourself considering herbicides and other inputs for next season. Before farmers make final decisions or pre-pay for weed control in 2018, make sure you have a plan for proactive weed control.
“What you do in the short run can really hurt you in the long run if you don’t use a multiple year strategy and be proactive to prevent resistance from occurring [more frequently],” says David Shaw, chair of the Weed Science Society of America (WSSA) herbicide resistance education committee. “Understand if you don’t use technology appropriately, it may be cheaper this year, but you may not have that option in three years.”
When effective herbicides against problem weeds are in short supply, it’s essential to plan ahead to ensure you switch modes of action. Take simple steps and time to plan a multiyear approach and increase your success.

At a minimum, have a weed management plan for this season and two years out.
If you’re going to cut herbicide costs, cut in your post, not pre, application.
Keep detailed notes. List the weed type, herbicide used, height and location of the weed. Use these notes while planning herbicides for next year.
If you don’t rotate, consider rotation to open up herbicide options and to keep the weeds guessing.
Consider some kind of tillage. This cuts down $20 to $30 of burn down chemical costs and provides another method of weed control.

Start the 2016 season with ahead of weeds by strategizing your best control options.