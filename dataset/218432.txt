Large areas of key wheat-producing areas are experiencing not only a cold winter—but a dry one.
This has stimulated a lot of questions regarding nitrogen application timing.
David Marburger at Oklahoma State University has provided this insights and tips:

In areas where it is dry and dry deeper than the majority of the rooting zone (> 6”), “don’t worry about filling up the nitrogen tank as long as the water tank is empty. As it stands currently, the best option is to hold off for now and wait to apply topdress N right in front of a real chance of rain,” he says.  

He says the good news is that the key time to get nitrogen applied is before jointing, which occurs in late February in southern Oklahoma and mid-March in northern Oklahoma. The reason is the number of potential grain sites are determined just prior to jointing,
“Jointing also marks the beginning of rapid nitrogen uptake by the plant which is used to build new leaves, stem, and the developing grain head. The nitrogen stored in these plant parts will be used to fill the grain later in the season, and the plant is dependent on this stored nitrogen to complete grain fill. And while it does seem like it right now, we still have the potential to make a decent crop if we can get rain before we break winter dormancy,” he says.

What can and should be done right now is applying N-rich strips to help demonstrate the response in yield to the application rates. The N-rich strip can be as simple as using a small lawn fertilizer spreader with a bag of urea. Marburger offs that local county extension educators can provide more information on N-rich strips and they have access to lending small fertilizer spreaders.


For conventional-tilled fields that have limited to no residue, applying UAN through streamer nozzles is an option as UAN has a very high percentage of soil-fertilizer contact.

“This immediately improves the efficiency compared to urea. In fields with crop residue, flat fan nozzles are not recommended right now as the likelihood up of N tie-up is too high,” he says.

“For no-till fields, the two big concerns are ammonia (NH3) volatilization with dry urea and tie-up on the residue with liquid UAN. Picking the best option in this scenario is a much tougher decision with not a real good conclusion,” he says.  “If there is tall standing stubble with dry soil below, the dry urea gets the edge. Why? If the stubble is not in a mat, the urea prill can work its way down towards the soil surface. If it can get there, it is out of the high winds, and it will remain there until we get a rain, heavy dew, or increase in humidity. Is there still a chance for loss due to volatilization? Absolutely. Again, it goes back to whether there is any chance that you can wait to apply?”


Regarding urease inhibitors used with a broadcast application of urea, Marburger does caution their use until there’s a good chance of rain in the 10-day forecast. “Typically, these products do not have the life span to hold off urease (i.e., the enzyme that breaks urea into NH3) for more than 10-12 days,” he says.


Marburger also encourages considering the practice of topdressing urea with a grain drill.

Research results from last year and a calibration guide are available here.

More information about nitrogen applications that are “thinking outside the box” can be found by clicking here.