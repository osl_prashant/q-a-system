China lists $50B of US goods it might hit with 25 pct tariff
China lists $50B of US goods it might hit with 25 pct tariff

By JOE McDONALDAP Business Writer
The Associated Press

BEIJING




BEIJING (AP) — China on Wednesday issued a $50 billion list of U.S. goods including soybeans and small aircraft for possible tariff hikes in an escalating and potentially damaging technology dispute with Washington.
The country's tax agency gave no date for the 25 percent increase to take effect and said it will depend on what President Donald Trump does about U.S. plans to raise duties on a similar amount of Chinese goods.
Beijing's list of 106 products included the biggest U.S. exports to China, reflecting its intense sensitivity to the dispute over American complaints that it pressures foreign companies to hand over technology.
The clash reflects the tension between Trump's promises to narrow a U.S. trade deficit with China that stood at $375.2 billion in goods last year and the ruling Communist Party's development ambitions. Regulators use access to China's vast market as leverage to press foreign automakers and other companies to help create or improve industries and technology.
President Donald Trump says the U.S. lost a trade war with China "years ago."
In a tweet Wednesday after China's announcement, Trump said: "We are not in a trade war with China, that war was lost many years ago by the foolish, or incompetent, people who represented the U.S."
A list the U.S. issued Tuesday of products subject to tariff hikes included aerospace, telecoms and machinery, striking at high-tech industries seen by China's leaders as the key to its economic future.
China said it would immediately challenge the U.S. move in the World Trade Organization.
"It must be said, we have been forced into taking this action," a deputy commerce minister, Wang Shouwen, said at a news conference. "Our action is restrained."
A deputy finance minister, Zhu Guangyao, appealed to Washington to "work in a constructive manner" and avoid hurting both countries.
Zhu warned against expecting Beijing to back down.
"Pressure from the outside will only urge and encourage the Chinese people to work even harder," said Zhu at the news conference.
Companies and economists have expressed concern improved global economic activity might sputter if other governments are prompted to raise their own import barriers.
But Commerce Secretary Wilbur Ross is brushing off concern over trade war with China. In an interview with CNBC Wednesday morning, Ross said that tariffs imposed by China amount to 0.3 percent of U.S. GDP and that some action on tariffs has been "coming for a while."
"What we're talking about on both sides is a fraction of 1 percent of both economies," he said.
The larger concern, Ross said, is the protection of U.S. intellectual property.
Still, U.S. stock futures slumped over concerns that the back-and-forth tariff actions will stunt trade and growth. Ross said he would not comment on the stock market's reaction, but then said he thinks "it's being out of proportion."
China announced tariffs worth $50 billion on a series of U.S. products including soybeans, whiskey and cars.
Chinese officials said they were obliged to act after the U.S. announced plans for retaliatory tariffs in an escalating dispute over China's technology program and other trade issues.
The dispute "may compel countries to pick sides," said Weiliang Chang of Mizuho Bank in a report.
"U.S. companies at this point would like to see robust communication between the US government and the Chinese government and serious negotiation on both sides, hopefully to avoid a trade war," said the chairman of the American Chamber of Commerce in China, William Zarit.
"I can only hope that we solve our differences as soon as possible to avoid damage to the U.S. economy, Chinese economy and to U.S. companies."
American companies have long chafed under Chinese regulations that require them to operate through local partners and share technology with potential competitors in exchange for market access. Business groups say companies feel increasingly unwelcome in China's state-dominated economy and are being squeezed out of promising industries.
Chinese policies "coerce American companies into transferring their technology" to Chinese enterprises, said a USTR statement.
Foreign companies are increasingly alarmed by initiatives such as Beijing's long-range industry development plan, dubbed "Made in China 2025," which calls for creating global leaders in electric cars, robots and other fields. Companies complain that might block access to those industries.
Wang, the commerce official, defended "Made in China 2025." He said it was "transparent, open and non-discriminatory" and foreign companies could participate.
Wang said the plan, which sets specific targets for domestic brands' share of some markets, should be seen as a guide rather than mandatory.
A report released Tuesday by the USTR also cited complaints Beijing uses cyber spying to steal foreign business secrets. It was unclear whether the latest tariff hike was a direct response to that.
The Chinese list Wednesday included soybeans, the biggest U.S. export to China, and aircraft up to 45 tons in weight. That excludes high-end Boeing Co. jetliners such as the 747 and 777, leaving Beijing high-profile targets for possible future conflicts.
Also on the list were American beef, whisky, passenger vehicles and industrial chemicals.
Zhu, the deputy finance minister, expressed thanks to American soybean farmers who he said had lobbied the Trump administration to "safeguard hard-won economic relations between the United States and China."
To minimize the cost to China, regulators picked products for which replacements are available, such as soybeans from Australia or Brazil, said Tu Xinquan, director of WTO studies at the University of International Business and Economics in Beijing.
"China has made meticulous efforts in deciding the list of the products to make sure the impact on China's economy is controllable," said Tu.
"If the U.S. decides to increase intensity, China will surely follow suit," said Tu. "In the event of all-out trade war, both may lose all sense of reason, but I do hope it will never happen."
The Global Times newspaper, published by the ruling party and known for its nationalistic tone, suggested further retaliatory action might target service industries in which the United States runs a trade surplus. Regulators have wide discretion to withhold licenses or take other action to disrupt logistics and other service businesses.
"What China needs to do now is to make the United States pay the same price" so Americans "understand anew the Chinese-U.S. strength relationship," the newspaper said.
In a separate dispute, Beijing raised tariffs Monday on a $3 billion list of U.S. goods including pork, apples and steel pipe in response to increased duties on imports of steel and aluminum that took effect March 23.
The United States buys little Chinese steel or aluminum, but analysts said Beijing would feel compelled to react, partly as a "warning shot" ahead of the technology dispute.
In another warning move, Chinese regulators launched an anti-dumping investigation of U.S. sorghum last month as rhetoric between Beijing and Washington heated up.
China has accused Trump of damaging the global system of trade regulation by taking action under U.S. law instead of the through the WTO.
Previously, Trump approved higher import duties on Chinese-made washing machines and solar modules to offset what Washington said were improper subsidies.
The technology investigation was launched under a little-used Cold War era law, Section 301 of the U.S. Trade Act of 1974.
However, as part of its response, the USTR also lodged a WTO case last month challenging Chinese policies it said unfairly limit foreign companies' control over their technology.
U.S. authorities say Beijing denies foreign companies the right to block use of technology by a Chinese entity once a licensing period ends. And they say it imposes contract terms that are less favorable than for local technology.
___
AP Writers Gillian Wong and Christopher Bodeen and AP researchers Shanshan Wang and Yu Bing contributed.
___
Chinese Ministry of Commerce (in Chinese): www.mofcom.gov.cn