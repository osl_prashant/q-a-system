BC-US--Cotton, US
BC-US--Cotton, US

The Associated Press

New York




New York (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Wednesday:
(50,000 lbs.; cents per lb.)
COTTON NO.2Open    High    Low    Settle     Chg.May       83.17   84.76   82.89   84.76  Up   2.50Jul       81.52   84.54   81.52   83.94  Up   2.43Aug                               78.99  Up   1.40Oct                               80.89  Up   1.75Oct                               78.99  Up   1.40Dec       77.84   79.06   77.59   78.99  Up   1.40Dec                               78.94  Up   1.23Mar       77.78   78.97   77.78   78.94  Up   1.23May       78.48   78.90   78.25   78.88  Up   1.00Jul       78.52   78.82   78.52   78.72  Up    .91Aug                               73.79  Up    .24Oct                               75.84  Up    .55Oct                               73.79  Up    .24Dec       73.95   73.95   73.79   73.79  Up    .24Dec                               73.84  Up    .24Mar                               73.84  Up    .24May                               74.26  Up    .24Jul                               74.20  Up    .24Aug                               72.31  Up    .24Oct                               73.56  Up    .24Oct                               72.31  Up    .24Dec                               72.31  Up    .24Dec                               72.35  Up    .24Mar                               72.35  Up    .24