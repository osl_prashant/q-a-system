Crop calls
Corn: 3 to 4 cents lower
Soybeans: 3 to 6 cents lower
Wheat: 2 to 6 cents lower

Following yesterday's gains in the corn and soybean market, futures faced profit-taking overnight to maintain this week's back-and-forth price action. While rains are moving across the eastern Corn Belt this morning, drier weather is allowing producers in the western Belt to return to fields and the extended outlook calls for below-normal precip for the bulk of the Belt. Wheat faced additional profit-taking overnight after the second day of the HRW wheat tour found yields lower than year-ago, but above the five-year average.
 
Livestock calls
Cattle: Higher
Hogs: Higher
Following yesterday's sharp to limit up performance, limits are expanded in live cattle to $4.50 and $6.75 in feeder cattle futures. While cattle futures have pushed into severely overbought territory, nearbys still hold a sizable discount to this week's cash cattle trade, which has ranged $144 to $147. Lean hog futures are expected to benefit from spillover from cattle, as well as signs the pork market has posted a near-term low and will work higher seasonally. But buying enthusiasm could be limited as May hogs already hold around an $8 premium to the cash index.