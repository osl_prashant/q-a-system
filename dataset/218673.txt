A “normal” Thanksgiving-Christmas sales period now behind them, U.S. mushroom marketers say supplies look good for the winter.
“Overall, the outlook for the first quarter of 2018 is positive,” said Lori Harrison, communications manager with the Avondale, Pa.-based American Mushroom Institute.
That’s an improvement for some areas, she said.
“Production in Texas and Florida has recovered from hurricane damage last fall,” Harrison said.
Mushrooms are grown indoors, but production still depends on the quality of compost, which is grown outside, Harrison said.
“A fairly mild fall has given way to improved conditions, so growers are confident looking forward — as long as they don’t have to deal with frozen compost,” she said.
Storms and cold are one issue, but there is, perhaps, a larger concern looming over the industry, Harrison said. It’s labor.
“Maintaining a stable labor force is the primary concern for all sectors of the mushroom industry, like everyone in agriculture,” Harrison said.
Mushrooms are a labor-intense crop, and since they are produced year-round, growers are not eligible for the federal H-2A guest worker program, Harrison said.
Labor always affects supplies, said Michael Basciani, chief operations officer with Avondale-based Basciani Mushroom Farms.
“Automation is coming, but we haven’t found any automation that gives us the quality the consumer demands,” he said.
Fletcher Street, marketing and sales director with Olympia, Wash.-based Ostrom Mushroom Farms, said automation isn’t realistic, given the current technology.
“The only automated harvesting we know of is done in Holland, and that’s for canning,” she said.
Overall, the supply outlook is positive for early 2018, but occasional shortages of certain varieties can be expected, said Greg Sagan, executive vice president of sales and marketing with Temple, Pa.-based Giorgio Fresh Co.
“Periodic shortages of certain type mushrooms during heavy promotion periods will occur throughout the first quarter of 2018,” he said.
The market continues to respond to in-store promotions, with “consumers hearing consistent reports of mushrooms being a hot trend” for the coming year, Sagan said.
Early winter brought an arctic blast and below-zero temperatures to much of the U.S. The East Coast got a foot or more of snow in some cities, with an inch reported as far south as Florida.
That’s all to the good, ultimately, for mushroom production, said Brian Kiniry, president of Kennett Square, Pa.-based Oakshire Mushroom Farm, which markets product under the Dole label.
“The crop should improve in the first quarter of 2018 for Pennsylvania growers, as the yields continue to increase due to the change in weather and improved compost,” he said.
Watsonville, Calif.-based Monterey Mushrooms Inc. anticipates an outstanding crop for early 2018, said Mike O’Brien, vice president of sales and marketing.
“Since we have 10 farms strategically located around the United States and Mexico and we make our own compost, we are prepared and committed to supply our valuable customers in 2018,” he said.  
Prices are steady from a year earlier, according to the U.S. Department of Agriculture.
As of Jan. 5, the weighted average price of 8-ounce packages of white mushrooms at 3,240 stores across the U.S. was $1.82, USDA reported. Organics averaged $2.48. A year earlier, the same items were $1.82 and $2.51, respectively.
White mushrooms still constitute most mushroom sales, but brown mushrooms continue to gain, said Bill St. John, sales director for Gonzales, Texas-based Kitchen Pride Mushroom Farms Inc.
“Ten years or so ago, white mushrooms represented over 90% of sales. That share is closer to 70%, as baby bellas are still increasing in popularity,” he said, noting that portabellas have been fairly stable, at 6% to 7% of total pounds sold.
Specialty mushrooms, especially shiitake and oyster, also are gaining market share as consumers are more willing to try them, St. John said.