USDA Press Release
(Mifflintown, PA, January 24, 2018) – U.S. Secretary of Agriculture Sonny Perdue today announced the U.S. Department of Agriculture’s Farm Bill and Legislative Principles for 2018 during a town hall at Reinford Farms in Mifflintown, Pennsylvania. 
“Since my first day as the Secretary of Agriculture, I’ve traveled to 30 states, listening to the people of American agriculture about what is working and what is not. The conversations we had and the people we came across helped us craft USDA’s Farm Bill and Legislative Principles for 2018,” said Secretary Perdue. “These principles will be used as a road map – they are our way of letting Congress know what we’ve heard from the hard-working men and women of American agriculture. While we understand it’s the legislature’s job to write the Farm Bill, USDA will be right there providing whatever counsel Congress may request or require.”
Download USDA’s 2018 Farm Bill and Legislative Principles
 
USDA’s 2018 Farm Bill and Legislative Principles:
FARM PRODUCTION & CONSERVATION

Provide a farm safety net that helps American farmers weather times of economic stress without distorting markets or increasing shallow loss payments. 
Promote a variety of innovative crop insurance products and changes, enabling farmers to make sound production decisions and to manage operational risk. 
Encourage entry into farming through increased access to land and capital for young, beginning, veteran and underrepresented farmers. 
Ensure that voluntary conservation programs balance farm productivity with conservation benefits so the most fertile and productive lands remain in production while land retired for conservation purposes favors more environmentally sensitive acres.
Support conservation programs that ensure cost-effective financial assistance for improved soil health, water and air quality and other natural resource benefits.

TRADE & FOREIGN AGRICULTURAL AFFAIRS

Improve U.S. market competitiveness by expanding investments, strengthening accountability of export promotion programs, and incentivizing stronger financial partnerships.
Ensure the Farm Bill is consistent with U.S. international trade laws and obligations.
Open foreign markets by increasing USDA expertise in scientific and technical areas to more effectively monitor foreign practices that impede U.S. agricultural exports and engage with foreign partners to address them.

FOOD, NUTRITION, AND CONSUMER SERVICES

Harness America’s agricultural abundance to support nutrition assistance for those truly in need.
Support work as the pathway to self-sufficiency, well-being, and economic mobility for individuals and families receiving supplemental nutrition assistance.  
Strengthen the integrity and efficiency of food and nutrition programs to better serve our participants and protect American taxpayers by reducing waste, fraud and abuse through shared data, innovation, and technology modernization.
Encourage state and local innovations in training, case management, and program design that promote self-sufficiency and achieve long-term, stability in employment.   
Assure the scientific integrity of the Dietary Guidelines for Americans process through greater transparency and reliance on the most robust body of scientific evidence.
Support nutrition policies and programs that are science based and data driven with clear and measurable outcomes for policies and programs.

MARKETING & REGULATORY PROGRAMS 

Enhance our partnerships and the scientific tools necessary to prevent, mitigate, and where appropriate, eradicate harmful plant and animal pests and diseases impacting agriculture. 
Safeguard our domestic food supply and protect animal health through modernization of the tools necessary to bolster biosecurity, prevention, surveillance, emergency response, and border security. 
Protect the integrity of the USDA organic certified seal and deliver efficient, effective oversight of organic production practices to ensure organic products meet consistent standards for all producers, domestic and foreign.
Ensure USDA is positioned appropriately to review production technologies if scientifically required to ensure safety, while reducing regulatory burdens.
Foster market and growth opportunities for specialty crop growers while reducing regulatory burdens that limit their ability to be successful.

FOOD SAFETY & INSPECTION SERVICES

Protect public health and prevent foodborne illness by committing the necessary resources to ensure the highest standards of inspection, with the most modern tools and scientific methods available.
Support and enhance FSIS programs to ensure efficient regulation and the safety of meat, poultry and processed egg products, including improved coordination and clarity on execution of food safety responsibilities.
Continue to focus USDA resources on products and processes that pose the greatest public health risk.

RESEARCH, EDUCATION & ECONOMICS

Commit to a public research agenda that places the United States at the forefront of food and agriculture scientific development.
Develop an impact evaluation approach, including the use of industry panels, to align research priorities to invest in high priority innovation, technology, and education networks.  
Empower public-private partnerships to leverage federal dollars, increase capacity, and investments in infrastructure for modern food and agricultural science.  
Prioritize investments in education, training and the development of human capital to ensure a workforce capable of meeting the growing demands of food and agriculture science.  
Develop and apply integrated advancement in technology needed to feed a growing and hungry world.

RURAL DEVELOPMENT               

Create consistency and flexibility in programs that will foster collaboration and assist communities in creating a quality of life that attracts and retains the next generation.
Expand and enhance the effectiveness of tools available to further connect rural American communities, homes, farms, businesses, first responders, educational facilities, and healthcare facilities to reliable and affordable high-speed internet services.
Partner with states and local communities to invest in infrastructure to support rural prosperity, innovation and entrepreneurial activity.
Provide the resources and tools that foster greater integration among programs, partners and the rural development customer.

NATURAL RESOURCES & ENVIRONMENT

Make America’s forests work again through proactive cost-effective management based on data and sound science. 
Expand Good Neighbor Authority and increase coordination with states to promote job creation and improve forest health through shared stewardship and stakeholder input. 
Reduce litigative risk and regulatory impediments to timely environmental review, sound harvesting, fire management and habitat protection to improve forest health while providing jobs and prosperity to rural communities. 
Offer the tools and resources that incentivize private stewardship and retention of forest land. 

MANAGEMENT

Provide a fiscally responsible Farm Bill that reflects the Administration’s budget goals.
Enhance customer service and compliance by reducing regulatory burdens on USDA customers.
Modernize internal and external IT solutions to support the delivery of efficient, effective service to USDA customers.
Provide USDA full authority to responsibly manage properties and facilities under its jurisdiction.
Increase the effectiveness of tools and resources necessary to attract and retain a strong USDA workforce that reflects the citizens we serve. 
Recognize the unique labor needs of agriculture and leverage USDA’s expertise to allow the Department to play an integral role in developing workforce policy to ensure farmers have access to a legal and stable workforce.
Grow and intensify program availability to increase opportunities for new, beginning, veteran, and underrepresented producers.

###