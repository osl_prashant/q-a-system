Sweet corn promotions generally start picking up the second week in April, with major peaks on the big four summer holidays — Memorial Day, Father’s Day, Fourth of July and Labor Day.
So far, grower-shippers say the supply outlook is good as they prepare for the big rush.
Brett Bergmann, partner in South Bay, Fla.-based Branch: A Family of Farms, said he expects sweet corn supplies to pick up right after Easter, which falls on April 1 this year.
“Branch should have steady supplies throughout April and May,” he said.
He said he expects Sweet Emotion bicolor corn to start harvesting in late April through July 4.
Daren Van Dyke, director of sales and marketing for Five Crowns Marketing, Tracy, Calif., which markets GloriAnn Farms sweet corn, said an unusually late freeze in the Imperial Valley is reducing supplies out West for April.
“It’s a difficult transition,” he said. “We’re going to have to deal with a little higher f.o.b. with a lighter crop due to the weather.”
Van Dyke said March 20 he expects supplies to recover for May promotions, but California growers may experience some difficulties later in the season due to rain and cooler weather during planting.
Bert Barnes, salesman and corn commodity manager for Duda Farm Fresh Foods, Ovideo, Fla., said a cold snap reduced volumes through Easter, but they’ll bounce back quickly.
“Supply will return to normal in April and stay consistent through transition to Georgia around Memorial Day,” he said. 
He expects good quality and promotable volumes after Easter.
 
Fourth of July pull
Over the past five years, the Fourth of July pull has been the highest-promoted holiday for sweet corn, according to the U.S. Department of Agriculture’s Retail Report.
In 2017, sweet corn was promoted in an average of 4,600 stores per week. The peak weeks, however, showed significantly more promotions, with sweet corn on ad in 18,500 stores the week ending June 30; 17,200 stores the week ending May 26; and 16,400 stores the week ending Sept. 1.
While most consumers are familiar with the traditional sweet corn and butter pairing, Van Dyke said there are a lot of different ways to prepare it, and flavor profiles are expanding, especially in foodservice outlets.
“We’re definitely seeing a trend in foodservice toward more of a fresh application,” he said. “It’s used in creative applications like smoked corn and jalapeño chowder.”
That creativity translates well to retail, and incremental promotions, he said.
“If I was a retailer, I would shoot for some cross promotions,” he said.
Corn grilled with pesto, or street corn with cheese and chili lime seasoning and even infused butter are up and coming flavors.
“And those items want to be in the produce section,” he said. “Corn lends itself to some creative marketing.”