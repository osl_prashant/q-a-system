Dragon fruit from Ecuador has been granted access to the U.S. market. 
The U.S. Department of Agriculture said that dragon fruit, also known as pitahaya fruit, can be safely imported from Ecuador.
 
Under the program, the fruit shipped to the U.S. must meet requirements that include fruit fly trapping, pre-harvest inspections and USDA approval of production sites. The rule takes effect July 20, according to the USDA.
 
Want more to the story? USDA proposes access for Turkish pomegranates