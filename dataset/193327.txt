The National Weather Service's (NWS) forecast for June through August calls for above-normal temps across a large portion of the U.S., including the Southern Plains and eastern Corn Belt. But there are equal chances for normal, below- or above-normal temps across the Northern and Central Plains, as well as the western Corn Belt. The forecast for warmer temps raises weather risks for the crop in the eastern Corn Belt.
Meanwhile, the extended outlook calls for above-normal precip across most of the Plains and well as the western third of Iowa. But for the bulk of the Corn Belt, chances are equal for normal, below- or above-normal precip.
The NWS says as the forecast progresses toward the cold season, a slight tilt toward El Nino is indicated by the latest guidance. "This has a small impact on the temperature and precipitation forecasts for the seasons spanning SON (Sept.-November) 2017 through FMA (Feb.-April) 2018," it states.
NWS outlooks for June through August:




 




 




For June, the NWS says chances are equal for normal, below- or above-normal temps and precip across most of the Corn Belt. Meanwhile, the East Coast, West Coast and South are expected to see above-normal temps.
NWS outlooks for June:




 




 




Below, we compare the Seasonal Drought Outlook to the current Drought Monitor:




 




 





Currently, only 16.6% of the contiguous U.S. is covered by some form of drought -- an impressive reduction from 50.8% at the start of the calendar year. The seasonal drought outlook signals more improvement is ahead, as scattered drought is expected to remain in place across the Southern Plains, with the prominent area of drought located in the Southeast.
"In the last four weeks, drought was alleviated in substantial parts of the Northeast, Middle Atlantic States and south-central Plains. However, latent long-term moisture shortages remain across the Northeast, and drought could redevelop quickly should any substantial period of abnormal heat and dryness occur," it states. "Currently, drought covers much of Florida, Georgia and eastern Alabama, with extreme drought affecting the central Florida Peninsula and portions of both southern and northern Georgia."