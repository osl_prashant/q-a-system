Veterinary technicians work in a growing field, assisting veterinarians with critical medical tests for large and small animals.

Students studying to be vet techs at Columbia State Community College are teaming up with the University of Tennessee Ag Research for some hands-on instruction, including chores on a working farm.

Charles Denney from the University of Tennessee Extension has the story on AgDay above.