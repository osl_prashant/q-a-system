Ethics complaint filed against North Carolina House speaker
Ethics complaint filed against North Carolina House speaker

By GARY D. ROBERTSONAssociated Press
The Associated Press

RALEIGH, N.C.




RALEIGH, N.C. (AP) — A Washington-based group asked North Carolina ethics officials Monday to investigate some of state House Speaker Tim Moore's business interests and interactions with state environmental regulators over the site of an old poultry plant.
The nonprofit Campaign for Accountability said it filed a complaint with the state ethics board based on public records requests to what is now the Division of Environmental Quality. The group contends Moore worked to delay penalties against his company while it tried to sell the Chatham County property, contributing to monetary gain.
"This deal doesn't pass the smell test," Campaign for Accountability Executive Director Daniel Stevens said in a release.
Moore, a Cleveland County Republican in his second term as speaker, called the allegations a "meritless election-year political ploy."
The complaint accuses Moore of improperly intervening with DEQ so a limited liability company he co-owned could avoid fines related to underground fuel storage tanks where the former Townsend poultry plant stood.
The complaint says that in August 2014 a department inspector issued a violation notice to Southeast Land Holdings LLC because the tanks hadn't been registered properly and needed to be closed.
In October 2014, DEQ granted the company a short extension to resolve the problems, the complaint says, but they had not been fixed by the next April. According to emails the group says it received from DEQ, the company's sale of the property had been delayed, so Moore requested another 90-day extension, which the inspector wrote was "unlikely."
But DEQ approved the speaker's request, the emails say, apparently after Moore gave more information to the department. DEQ could not provide that information to the Campaign for Accountability, according to the complaint.
Southeast sold the property to Mountaire Farms in September 2016 for $550,000, compared with Southeast's $85,000 purchase price, the complaint says, citing Chatham County property records.
"North Carolina residents deserve to know whether Speaker Moore abused his government position for personal financial gain," Stevens said in a release.
The activity occurred while GOP Gov. Pat McCrory's administration ran DEQ. Several department heads were aware of the situation, according to the complaint.
Moore said his case wasn't treated any differently. An email cited in the complaint from then-Assistant Secretary Tom Reeder told the Division of Waste Management "to handle this case just like it would any other one with similar circumstances."
"As the public records demonstrate, the Siler City project was a private property redevelopment handled properly by a state agency," Moore said in a release. "DEQ and their underground storage tank division dealt with the project as they would any other business and we complied."
State law says that if the state ethics board finds probable cause to continue the investigation, the case must then be referred to the Legislative Ethics Committee, which would investigate it further. The committee is composed of equal numbers of Democratic and Republican lawmakers.
The complaint also questions links between grants from the state Rural Infrastructure Authority — of which Moore appoints five of the 15 members — and efforts to reopen the plant and expand local wastewater service.