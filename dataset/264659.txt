BC-USDA-SD Direct Fdr Catl
BC-USDA-SD Direct Fdr Catl

The Associated Press



SF—LS160Sioux Falls, SD    Fri Jun 6, 2003     USDA-SD Dept Ag Market NewsSD Direct Feeder Cattle -  Sioux Falls, SDThis week: 380       Last week: 503     Year ago: 381Not enough direct sales confirmed to establish an accurate markettrend. All sales FOB North and South Dakota with a 2-3 percent shrinkor equivalent with a 5-10 cent slide on calves and a 5-6 cent slideon yearlings from base weights. Steers made up 63 percent of the run;heifers 37 percent.Feeder Steer Medium and Large 1Head    Wt. Range    Avg. Wt    Price Range   Avg. Price240      875          875        83.00         83.00   Current FOBFeeder Heifers Medium and Large 1Head    Wt. Range    Avg. Wt    Price Range   Ave. Price140      725          725        85.00         85.00   Current FOBSource:    USDA-SD Dept Ag Market News, Sioux Falls, SDTom Sandau, Market Reporter, 605-338-4061www.ams.usda.gov/mnreports/SF—LS160.txt1010c   cts.