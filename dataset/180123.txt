Crop calls
Corn: 1 to 2 cents higher
Soybeans: Fractionally to 1 cent higher
Wheat: 1 to 2 cents higher
Following yesterday's losses, grain and soybean futures ended the overnight session firmer amid short-covering. Key is whether or not those gains build during early daytime hours as traders even positions ahead of the weekend. Following this week's planting delays, there's a window of opportunity for fieldwork to resume in less-saturated areas of the eastern and southern Corn Belt early next week, as well as across much of the western Corn Belt where activity has once again picked up. But another system is expected to return by midweek. This morning's Stats Canada stocks data showed wheat stocks up 15.5% from year-ago, but 1.7 MMT lower than traders feared.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
After an active week of trade, livestock futures are expected to be mixed as traders even positions ahead of the weekend. Cash cattle trade has averaged around $145, which should limit pressure on nearby futures as they still hold a sharp discount to this price. Feedlot managers are planning to price next week's supplies even higher due to this week's higher beef prices and stressful feedlot conditions. Meanwhile, hog futures are vulnerable to profit-taking following this week's strong gains -- especially with futures at a stiff premium to the cash index. But bulls clearly hold the near-term advantage and the cash market has gained upward momentum.