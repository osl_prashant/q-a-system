Route 66 is a nostalgic road for many. The 2,448-mile stretch of highway spans six states, beginning in Chicago, Illinois and ends in Santa Monica, California.

The highway was decommissioned in 1985, and parts of the road aren’t as drivable as it once was, but there are parts that still draw tourists and memories.

Andrew McCrea takes a trip down memory lane this weekend on American Countryside.

Watch American Countryside weekends on U.S. Farm Report.