Last year was an interesting crossroads for the livestock industry. Supply, demand and prices were all up in 2017.
This is a rare occurrence, says Scott Brown, an economist at the University of Missouri. He and other economists with Food and Agricultural Policy Research Institute (FAPRI) at the University of Missouri, shared their economic projections in Columbia, Mo., last week.
All major meat sectors are posting continued growth. (Here’s an interesting infographic we did about meat consumption in Top Producer.)
“The high-quality and flavorful products were especially successful,” Brown says. “There has been a real switch in what meat cuts consumers want.”
He shared this chart, which shows the percent change (2017 versus 2010) in retail meat prices:

Source: Scott Brown, University of Missouri
Look how both cuts of chicken declined, while prices for every beef cut are charging ahead. Bacon prices also leaped.
What does bacon, steak and ground beef have in common? Taste.
Sure, you can make a case that a nice sirloin steak has more flavor than ground beef. But, you see high-end (and really tasty) burgers on nearly every menu. Plus, those burgers aren’t cheap. Looks like the rising beef tide is lifting all boats.
Though demand has been strong for many meat products recently, higher-quality and more flavorful dining experiences have led consumer trends, the FAPRI economists note in their 2018 U.S. Baseline Outlook. This has favored beef relative to other meat sectors.

Source: FAPRI
Bacon prices have been very strong, but pork loin and chicken breast prices have struggled, FAPRI notes. Turkey demand has also been very weak, leading to wholesale to wholesale turkey price projections for 2018 that are the lowest since 2010.
While it’s not shown on the chart above, another clue to this focus on flavor is chicken wings. They have seen substantial price growth—to the tune of a nearly 50% increase—for this time period, Brown explains.
“Taste matters for all meats,” Brown says.
Demand is the key to long-term growth for the livestock industry. These current consumer trends are supporting meat prices, but what happens when consumers shift their meat preferences? That’s Brown’s biggest worry.
For now, stay optimistic. And be sure to brag how your meaty main dish is full of flavor and incredibly tasty.
 
Curious what FAPRI economists predict for grain prices? Modestly Higher Grain Prices Ahead