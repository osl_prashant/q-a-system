Yakima, Wash.-based Sage Fruit Co., in partnership with New Zealand-based Freshco, is introducing Freshco’s proprietary apple variety, the Cheekie, to the U.S. this summer. 
The variety, a cross between the granny smith and Splendour varieties, is available now.
 
The companies describe the Cheekie as a “crisp and juicy apple with a dark red blush” and distinctive flavor.
 
It is described as being “slightly tropical, with a hint of passion fruit” that is accompanied by a slight tartness.
 
The fruit’s peak sizing ranges from a 64 to 88.