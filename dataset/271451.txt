BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Mon. Apr 23, 2018          USDA Market NewsDaily California EggsBenchmark prices are unchanged for Jumbo, Extra Large and Large and 2 centshigher for Medium and Small. Trade sentiment is mostly steady. Offeringsare moderate. Demand is moderate. Supplies are light to moderate. Marketactivity is slow to moderate. Small benchmark price $1.29.Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                212EXTRA LARGE          202LARGE                196MEDIUM               149Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 23-Apr-18 12:04 PM E MP