Frigid temperatures mean more than just bundling up. For many farmers, it could mean their wheat crop has taken a hit. While there’s been some respite between cold snaps, they still could have caused damage.
“As far as hard red winter wheat, [damage] is a concern in the Plains,” said Kyle Tapley, senior agricultural meteorologist with Radiant Solutions. “We think about a quarter of the crop saw some damage or at least temperatures were cold enough to cause some damage across about a quarter for the Belt, mainly in Kansas but also across parts of eastern Colorado and far northwestern Oklahoma.”
If temperatures stay below zero for days at a time, survivability drops, says Phil Needham, Farm Journal high-yield wheat expert.
“Wait until wheat comes out of dormancy to check for survival, but if you’re getting impatient you can go out now with a hammer or a pick and excavate a handful of wheat with roots and soil,” Needham explains. “Put it in the house for a week or two and see if it greens back up.”
Even if you try the inside test it might not be a true representation of your fields as various zones endure cold in different ways—make sure you scout to ground truth.
Be extra mindful of the possibility of winterkill in the following conditions, Needham says:

Little to no snow cover during the cold temperatures. Remember, taller residue often helps retain more snow cover.
Some varieties perform better than others in cold weather as a result of breeding. Check the winter hardiness or lack thereof in your varieties.
In late-planted fields, the plant won’t reach a sustainable level of growth before going dormant, which makes it more susceptible to the cold.
Fields or areas with greater levels of surface residue are more exposed to the cold. Because seeds are often more shallow or caught in residue, roots don’t reach “safe” depths.
Shallow-planted fields will have more kill because the cold can reach root crowns faster and easier.
Dry fields have less insulation.
If you didn’t apply phosphorus at planting, wheat is at greater risk of death as the nutrient helps improve  root mass, plant health and hardiness.
When the seed furrow didn’t completely close (because it was too dry or wet at planting), the exposed row will be more likely to be damaged, especially without snow cover.

Winterkill likely won’t be uniform across fields, so it’s important to scout in several areas—especially where you know there could be differences in how the seeds were planted.
“To make a good assessment of a field, take stand counts,” Needham says. “If there are many areas where the stand has been wiped out, decide if you want to tear up the field or not.”
In some cases you can spot plant, such as 5 acres with no stand in a 100-acre field. However, if a considerable portion of your field drops below 100 plants per yard it might be beneficial to rip up the stand, especially if there are pockets throughout the field. Needham suggests using a drone, if you have one, to check for dead areas, which will appear brown.
Areas with snowfall will be in better luck because the snow insulates the crop. Kansas and parts of Colorado and Oklahoma, which received little to no snowfall, need to be especially diligent when scouting after greenup.
“From mid-December to mid-February, Kansas wheat is at its maximum tolerance for cold temperatures,” says Marsha Boswell, director of communications, Kansas Wheat Commission. “However, winter injury is affected by several things. Time will tell in the coming months.”