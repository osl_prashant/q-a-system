According to the USDA's latest Crop Progress report, 4% of corn has been planted as of April 10, compared to 1% last year and the five-year average of 4%.Eight of the top 18 corn-producing states are reporting progress in corn planting. See Figure 1 below for a closer look at state-specific progress.
Figure 1: Percent of corn planted as of April 10, 2016

State


This Week


Last Year


Five-Year Average


Illinois


2


--


6


Kansas


17


12


7


Kentucky


6


1


10


Missouri


24


3


10


North Carolina


21


18


19


Ohio


2


--


1


Tennessee


17


4


16


Texas


46


43


52

Click here for the full Crop Progress reportor watch a recent AgDay TV segment on the latest progress: