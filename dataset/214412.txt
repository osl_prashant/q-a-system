When USDA's weekly crop condition ratings are plugged into Pro Farmer's weighted Crop Condition Index (CCI; 0 to 500 point scale, with 500 representing perfect), it reflects a nearly seven-point gain in the condition of the HRW wheat crop from last week, while the SRW rating declined by 2.3 points. HRW condition ratings improved in Kansas, Texas, Nebraska, South Dakota and Montana, but declined in Oklahoma and Colorado. The HRW crop rating is nearly eight points lower than year-ago.
Meanwhile, SRW ratings slipped in Missouri, Ohio and Michigan, improved in Illinois, Indiana and North Carolina, but remained unchanged in Arkansas. The SRW crop is rated 5.3 points above year-ago levels.





Pro Farmer Crop Condition Index






HRW




This week




Last week




Year-ago





Kansas *(42.17%)

148.44
145.91

137.53




Oklahoma (12.54%)

41.88
43.14

37.14




Texas (9.92%)

34.24
32.85

32.32




Colorado (10.25%)

38.35
38.76

35.43




Nebraska (6.15%)

22.06
21.51

24.58




S. Dakota (4.78%)

12.00
10.95

22.03




Montana (9.88%)

33.21
30.44

43.71




HRW total

345.02
338.10

352.88









SRW




This week




Last week




Year-ago





Missouri *(10.93%)

40.02
40.24
35.57



Illinois (10.47%)

37.16
36.84

36.72




Ohio (10.95%)

45.24
45.57

41.19




Arkansas (2.62%)

8.74
8.74

12.87




Indiana (5.83%)

22.46
22.28

21.81




N. Carolina (6.56%)

25.33
25.27

25.54




Michigan (12.32%)

46.21
47.56

41.19




SRW total

377.16
379.42

371.88




 
* denotes percentage of total national HRW/SRW crop production.
Following are details from USDA's National Ag Statistics Service (NASS) crop and weather reports for key HRW wheat states:
Kansas: For the week ending Nov. 5, 2017, below normal temperatures were recorded across the majority of the State, according to the USDA's National Agricultural Statistics Service (NASS). Light snow and rain were reported in some areas, but had little impact on harvest progress. There were 6.5 days suitable for fieldwork. Topsoil moisture rated 2% very short, 20 short, 76 adequate, and 2 surplus. Subsoil moisture rated 3% very short, 21 short, 75 adequate, and 1 surplus.
Winter wheat condition rated 3% very poor, 8 poor, 30 fair, 52 good, and 7 excellent. Winter wheat planted was 93%, near 95 last year and 97 for the five-year average. Emerged was 73%, behind 83 last year and 85 average.
Oklahoma: The state continued to receive little rainfall with the highest precipitation total recorded in the Southeast district. According to the Mesonet, true fall hasn't settled in yet as the state experienced a roller coaster of warming and cooling temperatures. As of Oct. 31, drought conditions were rated 3% moderate drought to exceptional, unchanged from the previous week and down 33 points from the previous year. Statewide temperatures averaged in the mid 50s. Topsoil and subsoil moisture conditions were rated mostly adequate to short. There were 6.5 days suitable for fieldwork.
Winter wheat planted reached 90%, down 3 points from the previous year and down 5 points from normal. Winter wheat emerged reached 78%, down 4 points from the previous year and down 6 points from normal.
Colorado: Cool and drier weather continued to accelerate harvest of several crops last week, according to the Mountain Regional Field Office of the National Agricultural Statistics Service, USDA. In northeastern counties, reporters noted harvest picked up for several crops, but winter wheat emergence slowed due to cooler temperatures and short days. There is some concern wheat fields might blow more this winter due to short cover. A reporter noted that lack of moisture continues to be a problem for rangeland conditions in these areas. Southwestern counties last week were again noted to be abnormally dry for this time of year, which is concerning. In southeastern counties, harvest slowed slightly during cool and damp days, but reportedly progressed well where conditions allowed.
 
Following are details from USDA's NASS crop and weather reports for key SRW wheat states:
Michigan: There were 2.9 days suitable for fieldwork in Michigan during the week ending November 5, 2017 according to Marlo Johnson, director of the Great Lakes Regional Office of NASS. Heavy rainfall throughout the week slowed harvest of many crops. Some producers in northern Michigan received a few inches of snow.
Winter wheat planting was wrapping up for the season. The ground was too wet this past week for much fieldwork activities.
Ohio: Field conditions are extremely wet following storm activity on Sunday, according to Cheryl Turner, Ohio State Statistician for the USDA's NASS. There were 2.9 days available for field work for the week ending November 5, 2017. Rain and cold weather throughout the week slowed harvest progress, but fields dried out sufficiently by Thursday to allow for some activities to take place before Sunday's rain arrived, including fall tillage, manure spreading, and cover crop seeding.
Severe storms on Sunday put many crop fields and pastures under water. Wind damage from tornadoes was also reported in isolated areas. Crop damage was still being assessed, but there were reports of flooding in wheat fields and corn stands impacted by water and high winds.
Missouri: Cool temperatures and little precipitation in the week allowed harvest to progress. Statewide, the temperature averaged 48.0 degrees, 2.7 degrees below normal. Precipitation averaged 0.26 inches statewide, 0.92 inches below normal. There were 5.2 days suitable for fieldwork for the week ending November 5. Topsoil moisture supply was rated 5 percent very short, 17 percent short, 72 percent adequate, and 6 percent surplus. Subsoil moisture supply was rated 6 percent very short, 19 percent short, 73 percent adequate, and 2 percent surplus.
Winter wheat planted reached 69 percent, compared to 75 percent for the 5-year average. Winter wheat emerged reached 47 percent. Winter wheat condition was rated 5 percent poor, 31 percent fair, 57 percent good, and 7 percent excellent.