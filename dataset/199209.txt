A new website from Purdue University'sCenter for Commercial Agricultureand theIndiana Soybean Allianceis available to help farm operators manage risk.Farm Risk Resources (www.farmriskresources.com) is designed to help farmers understand, identify, evaluate and manage risk, which is an inherent part of agriculture. The goal is to offer basic risk management concepts and principles critical to thinking through the decisions farmers make on a daily basis.
"Managing risk is pivotal to a farm's success," saidJim Mintert, director of the Center for Commercial Agriculture. "As production agriculture has become more complex, so, too, have the risks. They expand beyond price and require an integrated management approach."
The site includes a 15-question assessment for farmers to identify which types of risks apply to their specific operations - business, financial or strategic. Once identified, the site provides links to more information about managing the type of risk specific to each farm.
Also available are case studies farmers can use to walk through the risk evaluation and management process, as well as a list of risk management workshops, tools and videos. Information is provided for a variety of operation types and sources of risk, including specialty crops, new market expansion, food safety, labor, input suppliers, production issues, farmland values and crop outlooks.
"The site is full of expert-driven, research-based advice and operational tools," Mintert said. "It's all about helping farmers take a holistic approach to risk management."
Some of those tools include audits, spreadsheets and checklists.
In addition to the website, the Indiana Soybean Alliance and Indiana Corn Growers Association will offer a series of three free workshops titled "Managing Risk in a Challenging Farm Economy." Presenters include Mintert and Purdue Extension agricultural economistsMichael LangemeierandMichael Boehlje.
Dates and locations:
Dec. 7: Beck Agricultural Center, West Lafayette.
Jan. 11: Indiana Grand, Shelbyville.
Jan. 14: Fort Wayne Farm Show, Fort Wayne.
For more information and to register, visitwww.indianasoybean.com/forum.