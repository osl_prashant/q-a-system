The Texas Valley Citrus Committee is seeking producer and handler nominees.  
The committee has scheduled a nomination and budget meeting for noon Central on June 8, at the TexaSweet Building in Mission, according to a news release.
 
“The TVCC plays an important role in ensuring producers and handlers get the best price for their products. We are always looking for new growers to sit on the committee to create diversity and bring in some new energy,” Ted Prukop, manager of the citrus committee, said in the release.
 
For questions about eligibility requirements for serving on the committee, call (956) 581-2190 for more information.