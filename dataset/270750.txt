Samantha Cabaluna, Salinas, Calif.-based Tanimura & Antle’s vice president of brand marketing and communications, is an ambassador for authenticity in marketing.But when she started her career, she wanted to be a sports writer.
“I dreamed about writing the Sports Illustrated cover story someday, at a time when women did not do sports writing,” Cabaluna said.
An established sports journalist told her she was crazy to try to be a sports writer as a woman.
“After that, I realized if I decided to pursue that, I would spend the next 10-15 years of my life fighting that battle,” she said.
Beginning her career in a recession, Cabaluna couldn’t afford to fight the glass ceiling.
“I had to get a job and go to work,” she said.
Cabaluna took a job as a copywriter, writing for an ad agency right after college. It wasn’t long until she was looking for something else.
“Creative people tend to get bored, we want something new to play with,” she said.
When Myra Goodman, founder of San Juan Bautista, Calif.-based Earthbound Farm, told her, “You’ll never be bored at Earthbound,” Cabaluna knew it was the right place for her.
She joined the company in 2003 as a copywriter. In 2006, Earthbound Farm was at its lowest point.
“One of the things that put Samantha on the map was the E. coli outbreak in 2006,” said Tonya Antle, who was with Earthbound from 1998 until she retired in 2010, and is now co-founder and executive vice president of the Organic Produce Network.
Cabaluna become a spokeswoman for the company, handling all press inquiries and media communications regarding the crisis, Antle said.
“That’s a real measure of leadership,” she said. “It’s easy to lead in the good times.”
“She’s become that person I look to as a professional and moral compass,” said Jessica Harris, senior customer marketing manager for Earthbound Farm.
“She has such great instincts about the business. She has a great way of looking at things beyond just the business reasons, but also the personal reasons people do things. She was a really great mentor.”
In 2014, WhiteWave Foods bought Earthbound, and the marketing base shifted to Colorado.
“I wasn’t interested in moving to Colorado. I was doing a lot of traveling — you don’t know whether you’re coming or going. For my personal life it was time to make a change,” Cabaluna said.
“I definitely still have a fondness for Earthbound.”
“If you’re going to leave Earthbound Farm, Tanimura & Antle is a phenomenal place to move,” she said.
With three months under her belt at Tanimura & Antle, Cabaluna is hitting her stride.
The company recently announced an employee stock ownership plan, and Cabaluna’s initial focus was building a communication plan around that.
She’s also redesigned packaging and launched a website.
Cabaluna is enthusiastic about marketing produce, particularly organics.
“I think fresh produce is the most exciting place to be in food — it’s all healthy!” she said.
“It’s so great to work with something both delicious and healthy just by its very nature. That’s a great platform to start from.”
Cabaluna realizes the consumer base is always on the move, and it’s her job to stay on top of those shifting interests.
“We have to understand how the food landscape is evolving so fast these days. In years past, you could read consumer research and have a good idea what consumers would want for the next few years. That’s not the case anymore,” she said.
“People’s attitudes change so fast.”
One thing she’s seen consumers interested in is local produce.
“There’s certainly a fascination with and love of all things local,” she said.
“I think it’s great for seasonal varieties, but I think the drive for local comes from a real desire to know how your food is produced and who is producing it.”
Cabaluna takes the strategy she applied during the E.coli crisis and her years of experience and applies it here: The key is transparency and authenticity.
“I think even a bigger company can make it accessible and localized for people by engaging in regular conversations and being very clear about what you stand for and how you produce.”