Farmers are in skin cancer’s deadly bullseye. Period. Full-stop.
Skin cancer is the single most common cancer in the United States and the rising number of incidents is staggering: 5.4 cases in 3.3 million people in 2012, according to The Skin Cancer Foundation. More people are diagnosed with skin cancer each year than all other cancers combined.
Farmers, livestock producers and agriculture industry personnel are part of core skin cancer statistics related to outdoor work, consistently ranking highest in overall sun exposure. “Farmers are definitely in the most high-risk skin cancer category. They deal with a daily grind in direct sun often for seven days per week at the sunniest times of the year,” says Dr. David Roy, a dermatologist and spokesperson for The Skin Cancer Foundation.
The “Lucky” One
The three most typical skin cancers are basal cell carcinoma, squamous cell carcinoma and melanoma. Of the three, basal cell is the most common and often found on the face, neck, ears, scalp, nose, and shoulders. Caught early, the cure rate is almost 100%. “Basal rarely spreads and the risk of metastasis is less than .5%,” notes Roy.
Despite basal cell’s status as the mildest form of skin cancer, Steve Cornett can testify to its impact. He runs a cattle operation and grows wheat and alfalfa south of Clarendon, Texas, on the southeast side of the Panhandle. Dawn to daylight, Cornett spent his youth under a pounding sun, chasing farm work down bare turnrows or bouncing along on a bald tractor beside his father. The cost of repeated sun exposure patiently hid on golden-brown skin, waiting to manifest until Cornett reached his late 30s.





Basal cell carcinoma is the most common skin cancer, but when caught early, the cure rate is almost 100%.
© Chris Bennett




With a portion of his life spent directly in the sun and with too many sunburns to count, Cornett, 73, has fought skin problems for 40 years. He first began noticing scaly skin patches—actinic keratosis spots—and had them removed with liquid nitrogen treatments. At 45, a specialist noted a particular spot on Cornett’s nose—basal cell carcinoma. For several years, Cornett went through a series of minor surgeries and more nitrogen treatments: “I was lucky to get non-melanoma skin cancer and to catch it early, compared to some guys who won’t go in to the doctor. I go in twice yearly for regular check-ups and at least I’m not disfigured.”
See Until It Happened To Me to read Steve Cornett’s personal account of skin cancer treatment.
Underestimated SCC Tumor
Squamous cell carcinoma (SCC) is the second most common type of skin cancer, and although relatively simple to cure when caught early, can be highly aggressive if unchecked. Over 1 million SCC cases are diagnosed each year in the U.S., resulting in approximately 15,000 deaths. “This can almost look like psoriasis and is an underestimated tumor,” Roy says. “It can get into lymph nodes or other organs and can kill you. Certainly it can be more aggressive than commonly thought. If you’ve got dark pigment skin, this may be the skin cancer you get. Darker skin doesn’t mean you’re protected.”
Rarest and Deadliest
Melanoma ranks as the rarest, but most serious form of skin cancer, causing 9,000 deaths per year. It is typically highly aggressive and can pop up anywhere on the body, even on areas with no sun exposure: palms, soles, genitalia, eyes, navel, or inside the mouth.
The majority of melanomas are diagnosed in white men over 55. However, prior to 50, the majority of melanomas occur in white women. Overall, 1 in 27 white men will get melanoma; 1 in 42 white women will get melanoma.




 

"We know people who work outdoors are most likely to have skin cancer and that puts farmers right at the top of the list," says Dr. David Roy.
© Chris Bennett




As with any cancer, early detection increases survival rate. “The earlier you catch melanoma, the shallower it will be. Generally, a thin melanoma kills 2% to 5% of people, but the numbers go to 80% for a deep melanoma. There are other factors at play as well, but those are the basics,” Roy says.
What does melanoma skin cancer typically look like? Roy describes an asymmetrical dark spot, usually larger than 6 mm diameter with jagged borders, color variation, and changes in appearance. A five-letter (A-E) framework provides a general melanoma description. A = asymmetry; B = border irregularity; C = color variation; D = diameter over 6mm; and E = evolution or change.
“Keep in mind, there are melanoma that don’t fit that pattern. You can have spots that don’t correspond to the chart and still be melanoma. If you’ve got a new mole that’s changing and catches your eye, just get it checked,” Roy says.
Prevention
For farm workers of all stripes, sunshine is a basic element of daily work. Roy recommends limited exposure whenever possible, supplemented with long sleeves, hats, sunglasses and sunscreen protection of at least 30 SPF (water-resistant and broad spectrum to block UVA and UVB, two common types of radiation).
Roy often encounters farm-related patients that undergo treatment for precancerous spots, yet return to daily sun exposure without protection. Although short-term damage is fixed, skin cancer preventive measures are key because long-term damage can be mitigated. “Even if you’ve had treatment, it’s never too late for prevention. After about a year of consistent sun protection, some of the damage starts to decrease and that involves no treatment at all,” Roy explains. “Certainly it means damage when you’ve got 100,000 miles on your skin, but things can be turned around.”





There were 5.4 million cases of skin cancer in 2012 and roughly 6 million cases expected in 2018.
© Chris Bennett




Myths
Common beliefs about skin cancer are fraught with inaccuracies, and Roy points toward four main misnomers.
1. Skin cancer cannot be fatal: “This could not be more wrong. Even neglected basal cell carcinoma can be fatal in time. Remember, there is no good kind of cancer and all of it can get out of control. Get it treated, now.”
2. Dark skin tones are safe: “Nope. Skin cancer is not just for fair-skinned people. It can hit any race, tone, background or ethnicity.”
3. Consistent appearance: “Skin cancer doesn’t read textbooks and doesn’t always look the same. Get it checked out and let us look.”
4. Surgery is a cure-all: “Surgery doesn’t equate to a cure. After treatment, check in and make sure there is no reoccurrence.”
Protect The Kids
Stark U.S. statistics from The Skin Cancer Foundation tell an alarming story.
* More people are diagnosed with skin cancer each year than all other cancers combined.
* 5.4 million cases of skin cancer in 2012 and roughly 6 million cases expected in 2018.
* One in five Americans will develop skin cancer by the age of 70.
* The diagnosis and treatment of non-melanoma skin cancers increased by 77 percent between 1994 and 2014.
* Roughly 178,560 cases of melanoma will be diagnosed in 2018.
* Approximately 9,320 people will die of melanoma in 2018 (5,990 men and 3,330 women)
Despite the sobering numbers, Roy says skin cancer is typically easy to treat when caught early: “If you are in the sun a lot, I recommend a medical exam once a year, and a self-exam once a month. We know people who work outdoors are most likely to have skin cancer and that puts farmers right at the top of the list.”





The diagnosis and treatment of non-melanoma skin cancers increased by 77 percent between 1994 and 2014.
© Chris Bennett




Cornett echoes Roy’s concerns and extends skin cancer prevention advice to the protection of children: “Even if a grower won’t personally follow prevention and protection, at least keep your kids covered. Hats and sunscreen should be kept on them because so much sun damage can be done in childhood and I’m living proof.”
See Until It Happened To Me to read Steve Cornett’s personal account of skin cancer treatment.