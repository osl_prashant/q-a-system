WHO says tainted food outbreak threatens 16 African nations
WHO says tainted food outbreak threatens 16 African nations

The Associated Press

JOHANNESBURG




JOHANNESBURG (AP) — A deadly outbreak linked to tainted food in South Africa is now threatening other African nations, with neighboring Namibia reporting a confirmed case that might be connected, the World Health Organization said Tuesday.
In a statement, WHO said it has reached out to 16 countries to help with preparedness and response to the listeriosis outbreak that has killed nearly 200 people since January 2017. South Africa's health minister has said there have been 950 cases in all.
Contaminated meat products may have been exported to two West African countries and a dozen southern African ones, the U.N. health agency said. The countries include Nigeria, the continent's most populous nation.
A South African factory has been identified as the outbreak's source.
Despite an international recall of the products, further cases are likely because of listeriosis' potentially long incubation period, WHO said.
"This outbreak is a wake-up call for countries in the region to strengthen their national food safety and disease surveillance systems," said Matshidiso Moeti, WHO's regional director for Africa.
The 16 countries are Angola, Botswana, Congo, Ghana, Lesotho, Madagascar, Malawi, Mauritius, Mozambique, Namibia, Nigeria, Swaziland, Tanzania, Uganda, Zambia and Zimbabwe.