The saying “big crops get bigger” holds true in the latest crop production report released by the USDA Jan. 12. In the U.S., the average record yield for corn increased 2.0 bushels per acre from 2016, making the 2017 crop of 176.6 bushels per acre the largest corn yield.

The average soybean yield fell slightly from 2016 levels. Soybeans averaged 49.1 bushels per acre, a decrease of 2.9 bushels compared to the record crop in 2016.

Typically, the January report tends to be a market mover, however, the corn and soybean markets have been relatively unchanged after the report initially was released.

Brian Basting, commodity research analyst with Advance Trading, Inc., said the high level of ending stocks, particularly with soybeans, has “muted” market action.

“Pushing 500 million bushels on beans, 2.5 billion bushels on corn, and still pushing 1 billion bushels of wheat domestically, so all those things continue to underscore the ample supply we have domestically,” he told U.S. Farm Report host Tyne Morgan.

With the numbers, he isn’t surprised the market seemed to be unphased with the market, and he thinks weather in South America will be a bigger deal than this report.

Ahead of the report, farmers and traders alike were preparing for the worst news and expecting prices to fall.

You know it's a big USDA report day when you kind feel like you could puke 2 minutes before release
— Angie Setzer (@GoddessofGrain) January 12, 2018


Kevin Duling of KD Investors says this report didn’t catch anyone off guard, which helps.

“If they’re not caught off guard, they’re not going to be on the wrong side of the boat,” he said. “Going forward, we’re back to South American weather. We’re back to the dollar index. We’re back waiting for exports.”

While he wouldn’t call this report a “non-event,” Duling says this report is a signifier that the markets will stay in the current range.

Watch Duling discuss the negative reaction from the report regarding wheat and what Basting is watching moving forward on the video above.