Cashmere, Wash.-based Crunch Pak is sponsoring the first official app of the Washington State Apple Blossom Festival.
The app, to be released Jan. 15, gives users information on official events, parking and traffic, contests and frequently asked questions, according to a news release.
It also lets users cast their vote for most original speech in the Royalty Selection Pageant, Feb. 10, according to the release.
“The Official Apple Blossom Festival App brought to you by Crunch Pak is a great way to support our community in a tangible way,” Megan Wade, Crunch Pak marketing and product manager, said in the release.
Crunch Pak plans to participate in several festival activities and will sponsor the app through 2020, according to the release.