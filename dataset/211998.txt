When U.S. customers asked Serres Lefort in Ste-Clotilde, Quebec, if they’d consider growing organic greenhouse tomatoes to add to their mix of colored peppers and mini and English cucumbers, the answer was, “Why not?”One year, millions of dollars and many sleepless nights later, Serres Lefort is selling beefsteaks and tomatoes-on-the-vine under its Vog label.
The tomatoes were planted in January in a new 2-acre greenhouse, part of a 9-hectare, $27 million expansion that makes Serres Lefort one of the biggest organic greenhouses in one location in Canada with a total of 12 hectares of vegetable production.
Tomato harvesting began at the end of April, said Julie Lefort, vice president of innovation and development, with plans to continue until December.
Though the project is going well, growing a new crop has been a huge challenge, Lefort said.
Since all her organic crops are growing more quickly under diffused light panels in the new greenhouses, they require constant adjustments in cultivation practices.
Choosing the right labels and tomato packaging materials to satisfy customers while trying to stay green has also been a challenge, she said.
“Next year we’ll have a better idea of what’s most popular,” Lefort said, who’s packing some on-the-vine tomatoes in bags while selling beefsteaks in clamshells and bulk.
Her mini cukes are now packed in an over-wrapped compostable tray, and Lefort peppers are sold in a flow pack of two or shrink-wrapped individually to meet customer requirements.
With U.S. demand for organic vegetables growing every year, especially for greenhouse quality, she said the company hasn’t been able to keep up.
“We were always short,” she said, “which led to the expansion.”
Her next step is to learn to manage the growth and spend more time training new employees who are currently learning on the job.
“Last year we had 120 people,” she said, “and now we have 200.” After that?
“Sleep,” she said. “Everyone’s exhausted.”