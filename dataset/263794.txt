BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Friday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeApr        12.73   12.74   12.60   12.65  Down .09May                                12.85  Down .10Jun        12.96   12.96   12.82   12.85  Down .10Sep        13.33   13.33   13.21   13.23  Down .10Dec                                14.17  Down .08Feb        14.26   14.26   14.13   14.17  Down .08Apr        14.35   14.37   14.27   14.31  Down .09Jun        14.47   14.49   14.40   14.42  Down .09Sep        14.74   14.76   14.66   14.68  Down .09Dec                                15.16  Down .09Feb        15.23   15.26   15.16   15.16  Down .09Apr        15.25   15.32   15.18   15.18  Down .09Jun        15.36   15.45   15.27   15.27  Down .09Sep        15.72   15.76   15.52   15.52  Down .10