The big got a lot bigger in the Treasure Valley onion domain this winter when Ontario, Ore.-based Murakami Produce Co. LLC merged with Baker Packing Co.
The new venture — Baker & Murakami Produce Co. LLLP — aims to provide better service to its customers, said CEO Grant Kitamura.
“The customer base has consolidated, too, and they have become larger,” he said.
Chris Woo, partner in Baker & Murakami Produce and sales manager for Potandon Produce LLC, said the merger “puts a lot of volume under one roof being sold by one organization, so it’s not fragmented in the marketing spectrum of things.”
Labor — or the lack of it — factored into the merger decision, Woo said.
“It’s pretty tough getting people to work on the ranch let alone the packing shed facilities,” he said.
The labor situation led the new company to install a fully automated packing line in its expanded 68,000-square-foot facility.
Even with the move to more automation, concerns about labor still exist at Baker & Murakami.
“Our biggest deal is we store onions, and that takes labor,” Kitamura said. “It’s more difficult to schedule and coordinate.”
Despite the labor issue, the company caught a big break when it decided to automate and expand in the summer of 2016, several months before the merger was announced.
That’s because an abundant snowfall collapsed many onion facilities in Treasure Valley, leaving other companies to have to rush order new equipment and rebuild on the fly.
“We were the first,” Woo said. “We’re a little bit ahead of everybody because we ordered (equipment) early and installed early, so we got a head start.”
None of Baker & Murakami’s buildings collapsed from the snow.
Economics drove the merger.
“With the packing line, we hope to pack more in a shorter period of time and provide better returns back to our growers,” Woo said. “The future is here. If we don’t do this, we’ll be less efficient and can’t keep up with our competition.”
“The main thing we have to maintain is our quality and food safety, and I don’t see a problem there,” Kitamura said.