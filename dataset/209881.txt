A tax reform framework supported by both the White House and Congress was released last week. While the plan includes priorities for agriculture such as repeal of the estate tax, there are a number of unanswered questions according to the American Farm Bureau Federation's tax expert Patricia Wolff. She outlined those concerns on the AgriTalk Radio Show this week. They include:

While the plan would eliminate the estate tax, it does not mention stepped up basis.
The plan raises questions about how farm income is separated between personal and business.
The tax framework may require farms to switch to accrual accounting rather than cash accounting. That could have a negative impact on cash flow for some operat
Will the tax plan eliminate section 1031 like-kind exchanges that allow deferral of taxes on asset transfers?
It is unclear if farms will be able to deduct interest costs.
Deduction for local and state property taxes may be eliminated. 

Wolff said legislation to enact the tax reform framework is expected in a couple of weeks. She urged farmers and ranchers to express concerns for farm priorities to members of Congress now while the legislation is being written up.