President Donald Trump is addressing the opioid epidemic as a national emergency. It’s an issue bleeding throughout the country, in both rural and urban communities. “Nobody is safe from this epidemic that threatens young and old, rich and poor, urban and rural communities,” said President Trump. “Everybody is threatened.”
U.S. Attorney General Jeff Sessions is vocal about the popularity of opioid drug use. 
“According to the Authoritative New England Journal of Medicine, we’re seeing more availability, higher purity and lower price,” said Sessions. “Those are three bad trends.” 
According to the Centers for Disease Control and Prevention (CDC), opioids killed more than 33,000 people in 2015. 
Sessions said the use is wide-spread throughout large and small communities and Ohio is a ground zero location. 
“On average, one person in Columbus, Ohio dies from a drug overdose every day,” said Sessions. “It’s not too different from Alabama and other places around the country.” 
The use is such a problem for rural communities, former U.S. Agriculture Secretary Tom Vilsack, who now is the president and CEO of the U.S. Dairy Export Council (USDEC), led the push to curb opioid use while he was in office. 
“The other compounding fact in rural areas is the lack of treatment. There are not that many treatment facilities in rural areas,” said Vilsack in an interview with AgDay October 2016. “It’s sometimes difficult for a small county hospital to have the kind of personnel.”
Injuries from strenuous work on the farm and pain medicine can potentially lead up to an addiction.
“Those who work on farms have a tendency to hurt themselves and strain muscles,” said Ken Yeager, professor at the Ohio State Wexner Medical Center “We need to be very aware and very cautious of the impact of opioid medications and the addictive nature of opioid pain medications.”
“I’m confident by working with our healthcare and law enforcement experts, we will fight this deadly epidemic and the United States will win,” said President Trump. 
According to the CDC, 91 Americans die every day from an opioid overdose that includes prescription opioids and heroin.