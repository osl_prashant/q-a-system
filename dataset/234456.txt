A lot happened last week in agriculture: the USDA held its Agricultural Outlook Forum and released its acreage estimates, soybean markets enjoyed their trip to the top off rumors of South American dryness, and wild weather tore through many ag states.

For Shawn Hackett of Hackett Financial Advisers, the grain markets could be getting tired since weather in Argentina and the winter wheat regions in the Midwest have “played itself out for now.”

“We think it’s a period of potential for the market to have a setback, ad we’re concerned about that,” he said on AgDay.

For the week ahead, Paul Georgy of Allendale, Inc. is watching first notice day for the March contract.

“That’s going to impact some of the traders, and maybe cause some selling ahead of that time frame,” he said.

Hear why Hackett thinks there could be a short-term inflection point that could soon make itself visible on AgDay above.