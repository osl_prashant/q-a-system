News bulletin: We still haven’t stopped wasting food. 
The Natural Resources Defense Council recently released a 58-page report titled “Wasted: How America Is Losing up to 40 Percent of Its Food from Farm to Fork to Landfill.” The first report of the same name was issued in 2012 and has been widely cited in discussions about sustainability, imperfect produce and feeding more people with less resources.
 
The NRDC noted that after the 2012 report, greater awareness of food waste was reflected in the 2015 move by the U.S. Department of Agriculture and the Environmental Protection Agency to target a 50% food waste reduction by 2030.
 
While gains in reducing food waste are hard to measure, the report does observe progress and, of course, suggests solutions.
 
The report lays out the problem with grim statistics, including an alarming stat that every American throws out 400 pounds of food every year.
 
We have met the food waste enemy, and he is us.
 
The USDA estimates produce alone accounts for $15.4 billion in losses annually. 
 
One graphic in the report shows that households account for a whopping 43% of food waste, followed by restaurants with 18%, growers with 16%, grocery and distribution 13%, institutional and foodservice 8% and manufacturers 2%.
 
By food category, the report says 19.1% of food loss comes from dairy, 19% comes from vegetables, 13.9% comes from fruits, 13.9% comes from grain products and 11.5% from meat, poultry and fish.
 
So fresh produce suppliers and consumers are front and center in the food waste discussion. What does NRDC say should be done? 
 
Among other next steps, NRDC says businesses should:
 
conduct food waste audits to understand the scope and opportunities within their operations;
set reduction goals and publicly report progress; and
broaden cosmetic standards for fresh fruits and vegetables.
 
Besides marketing imperfect produce, the report said growers should practice farm-level recovery for food donations, package produce by the pound and promote regional and local food distribution.
 
For retailers, the report suggests that grocers streamline inventory and stock fewer items.
 
These suggestions to the trade may help reduce food waste, but more studies should be done on the effect of these actions on profitability and consumption. Is an empty display bin and an unhappy consumer a good trade-off for less waste at retail?
 
And with more than 40% of food waste at the doorstep (and fridge crisper drawer) of the consumer, the biggest focus should not be an orange packinghouse in the San Joaquin Valley or a retail cherry display in a Seattle supermarket. 
 
We have met the food waste enemy, and he is us.
 
Tom Karst is The Packer’s national editor. E-mail him at tkarst@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.