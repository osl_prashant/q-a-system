This afternoon’s Cattle on Feed Report showed placements higher than expected at 110% of year-ago levels. Marketings also came in slightly above their average pre-report trade guesses, with the number of cattle on feed basically in line with expectations. Key on Monday will be whether traders view the negative report as “baked” into prices after futures posted weekly losses.




Cattle on Feed


USDA


Avg. Trade Estimate


Range of Estimates




On Feed


106


105.7


105.0-107.0




Placements


110


107.7


103.6-113.1




Marketings


106


105.4


105.0-106.1




* Figures are a percent of year-ago levels.
The report showed an increase in every weight category entering feedlots. Calves under 600 pounds rose by 10.7% from year-ago, with 600-pounders up 12.4%, 700-pounders up 8.3%, 800-pounders up 6.7%, 900-pounders up 10.3% and 1,000-plus-pounders up 20.0% from year-ago levels.