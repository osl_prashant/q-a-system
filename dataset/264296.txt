Public hearing to be held over South Dakota turkey barn
Public hearing to be held over South Dakota turkey barn

The Associated Press

IPSWICH, S.D.




IPSWICH, S.D. (AP) — A public hearing will be held following South Dakota residents' opposition to the site of a proposed 5,000-hen turkey barn.
The Edmunds County Commission approved permits for Farm Holdings S.D. 12's three turkey barns in February. County Commissioner Tim Thomas says only the barn located 2 miles (3 kilometers) southeast of Ipswich has been met with strong opposition.
County official Annette Jones tells Aberdeen American News that 61 residents have signed a petition calling for county commissioners to retract Farm Holdings' building permit. Farm Holdings S.D. 12 is a limited liability company owned by Hendrix Genetics.
Some opposing residents worry about odor and reduced property values.
Thomas hopes residents and the company can reach a compromise during a Monday public hearing.
A Hendrix Genetics spokesman says the barns will benefit the local economy through more business and employment opportunities.
___
Information from: Aberdeen American News, http://www.aberdeennews.com