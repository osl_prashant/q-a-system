Grains mostly lower and livestock higher
Grains mostly lower and livestock higher

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mostly lower Monday in early trading on the Chicago Board of Trade.
Wheat for May delivery fell 5 cents at $4.6440 a bushel; May corn was down 5.2 cents at $3.8300 a bushel; May oats fell 2 cents at $2.3040 a bushel while May soybeans gained 26.2 cents at $10.4960 a bushel.
Beef and pork were higher on the Chicago Mercantile Exchange.
April live cattle rose 4.92 cents at $1.1815 a pound; Apr feeder cattle was up 5.1 cents at $1.4070 a pound; April lean hogs gained .165 cents at .6888 a pound.