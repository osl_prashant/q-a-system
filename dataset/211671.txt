A couple of independent retailers based in the Grand Rapids, Mich., area, have found ways to tailor their fresh produce offerings to the needs and wants of their clienteles. 
Ken’s Fruit Market, a three-store chain based in Grand Rapids, has moved stores into neighborhoods and watched business grow — even through setbacks that include a devastating fire.
 
Vince & Joe’s Gourmet Market, which has units in Shelby and Clinton townships, has catered to the sometimes-exotic tastes of its customers.
 
Ken’s Fruit Market has set up its stores — ranging in size from 12,000 to 18,000 square feet, in the heart of densely populated neighborhoods, said Ken Courts, who had been in retail long before he started his stores in 1981.
 
“By not being in a high-end area, we’re more a meat-and-potatoes type store,” he said.
 
That includes locally grown, where possible, Courts said.
 
“Here in Grand Rapids, we have a lot of stuff being grown, and it gives us a chance to say, ‘This is from right here,’” he said.
 

There’s an acceptance that not everything is available all the time, he said.
 
“There may have been a day or two where you don’t get corn in, due to a rain, but that’s normal,” he said.
 
Produce displays often feature freshly harvested sweet corn, peaches, squash, celery, greens, radishes, onions, and, as summer gives way to fall, apples, in bulk, Courts said.
 
“Michigan stuff is big right now for us, which is normal,” he said.
 
There have been setbacks as well as victories, especially recently. 
 
In the past couple of years, Ken’s Fruit Market has opened a new store and rebuilt another after a fire.
 
The new store, an 18,000-square-foot unit, opened in March 2016 in a building vacated in August 2015 by Duthler’s Family Foods. Duthler’s had moved to another location.
 
The new store is a sign of the company’s growth, Courts said.
 
“The new store is doing real good,” he said. 
 
Ken’s also rebuilt its Plainfield, Mich., store, which sustained damage in an early-morning fire Sept. 8, 2016.
 
The store reopened April 8, and customers have flocked back, Courts said.
 
“Sales are up about 50% there,” he said.

 
Vince & Joe’s has followed a bit of a different path, said Vinny Sciarrino, general manager.
 
Customers support new and specialty items, he said.
 
“I seem to see an interest in items like turmeric, herbs and breadfruit,” he said.
 
“I see more people asking for more tropical things. Fresh figs are becoming kind of mainstream.”
 
He credits food-oriented television networks for bringing exotic items to shoppers’ attention.
 
“That’s made people aware of a lot of things you never saw in the past,” he said.
 
In contrast to the bulk staple items that drive Courts’ business, Sciarrino said Vince & Joe’s has drawn customers with its line of value-added products.
 
“We do a lot of pre-cut fruits, salads, saving steps for the consumers,” he said.
 
“Carrots we now sell peeled, shredded, sliced. Radishes we sell sliced. A lot of squash that’s already cleaned and cubed. You see a lot of celery sticks. Watermelon, cantaloupes, honeydews — people buy enough to serve for one meal.”
 
Vince & Joe’s also deals heavily in Michigan-grown produce during its peak seasons, Sciarrino said.
 
“Michigan products this time of year, everything is available — cabbage, corn, tomatoes, peppers, watermelons, honeyrocks — a Michigan cantaloupe,” he said. 
 
“Apples are just beginning. Every week, there’s an added variety coming up. Temperatures drop and more varieties become available.”