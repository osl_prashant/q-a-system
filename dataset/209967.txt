Crop calls
Corn: Fractionally lower
Soybeans: 1 to 2 cents lower
Wheat: 1 to 2 cents lower
Corn and soybean futures started the overnight session firmer, but faded as buying interest dried up. Widespread rains over the weekend have delayed harvest and there's more rain in the forecast this week. Meanwhile, it's estimated that Brazilian soybean planting is just 5.6% complete, which is about half the normal pace. A weaker tone in the U.S. dollar index limited pressure on grain markets, although it remains in its uptrend from the September low. Due to Columbus Day, government offices are closed today.
 
 
Livestock calls
Cattle: Higher
Hogs: Mixed
Cash cattle trade picked up Friday afternoon at $109 in the Plains, up $1 from the previous week. This is encouraging to market bulls that priced in cash market gains. Traders will be monitoring the beef market early this week for cash clues. Meanwhile, lean hog futures are expected to see an uneven tone this morning amid spreading. After the cash hog market softened late last week, there is a little more uncertainty about cash direction this morning.