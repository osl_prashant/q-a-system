There are a lot of hot topics in agriculture right now: tariff concerns with China, NAFTA renegotiations, planting delays, and the possibility of creating and passing a farm bill come to mind.

One thing that could cause those in agriculture some concern is the growing livestock herds. In the March 23 Cattle On Feed report, there are 11.7 million head of cattle, 9 percent higher than March 1, 2017 levels.

That same growth is being mirrored in the hog market. In the USDA’s quarterly Hogs and Pigs report released March 29, there were 3 percent more hogs and pigs than March 1, 2017.

Domestic demand will have a lot of impact on meat prices in 2018, according to Scott Brown, ag economist of the University of Missouri. He isn’t sure if there will be a repeat of prices this year as there was last year.

If situations align, Brown isn’t sure what a black swan event could be to see higher livestock prices, but there are factors at play that could cause some improvement, including strong domestic demand and an increase in wage rates.

“It’s hard to offset what those growing supplies have been the last few years,” he said during Markets Now on U.S. Farm Report.

One factor that could change the cattle picture would be the drought situation in the southern Plains. According to the latest U.S. Drought Monitor, nearly all of Kansas is impacted by drought, and both panhandles of Oklahoma and Texas are experiencing D3, or extreme drought, or D4, exceptional drought.

Brown believes this drought could impact cattle and could drive prices lower in the short term, but prices could move higher in the long term.

Grains are also experiencing heavy supplies. According to Pat Westhoff, director of the University of Missouri’s Food and Agricultural Policy Research Institute, the world has seen five consecutive years of grains and oilseeds above the long-term trend.

Corn, wheat, soybeans and other crops have seen an increase of stocks, but weather and lower yields could help that picture.

“If we had a bit smaller crop this year because of more normal weather, it could help us work our way through some of those excess supplies and see a fresh recovery,” said Westhoff.

There is a possibility of seeing some upside potential if there is a weather hiccup this summer, preferably not in the U.S. to move prices higher. However, if corn is impacted, Westhoff says producers can switch to feeding wheat.

Another concern on farmers’ minds this spring is dicamba. With a new label, Kevin Bradley, weed specialist at the University of Missouri, isn’t optimistic about the changes and how it will make things “increasingly difficult” this season.
Hear more of what Bradley has to say on farmers adopting the dicamba technology on Markets Now on U.S. Farm Reportabove.