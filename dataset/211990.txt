I signed up for Instacart today. I wanted to check out the service that I had just heard has entered into a pilot with Aldi in Los Angeles, Dallas and Atlanta. I live in a suburb of Kansas City that’s not served by Instacart so I faked it and said I was from Dallas. Well, my sister does live in Texas so I’m merely stretching the truth by about 500 miles. 
My email greeting from Instacart was robust:
 
Welcome, Tom!
Instacart is on a mission to simplify a weekly task for you: grocery shopping. Partnering with local stores, we’ll make short work of your grocery list and bring your order right to your door. Save time and money, skip the stress of parking and lines, and reorder your favorites with ease.
We’re Instacart, and you’re gonna love us! 
To welcome you we’re giving you a FREE delivery when you order in the next 7 days.
 
 
I am curious about the Aldi pilot with Instacart. To use one of those trite business buzzwords, it’s a “game-changer.” It helps both companies, I believe, in big ways. It brings the value-oriented shopper squarely into the path of Instacart, making the idea of using a “personal shopper” mainstream.
 
And for Aldi shoppers, it provides an opportunity to save money associated with a trip to Aldi but without the indignities of paying 25 cents for a shopping cart, hauling around milk, sugar and a bag of apples in a flimsy cardboard box and waiting in a longish line.
 
If you tell me that a “personal shopper” can pick out very nice produce for me to boot, well, then, that is worth the fee of $5.99 for delivery.
 
We love Aldi for its low prices and limited assortment, but it is not a “shareable experience” on social media.
 
Aldi’s corporate site covers the news here and Ashley Nickle talked to several analysts in The Packer’s coverage here.
 
From the Aldi news release:
 
ALDI, one of America’s favorite grocery retailers,* announced a partnership today with Instacart, the technology-driven, nationwide on-demand grocery delivery service. The pilot program from ALDI, already a leader in convenient shopping, will give people even more access to high-quality groceries at the low prices ALDI is known for.
The new partnership allows customers to conveniently complete their grocery shopping by ordering award-winning ALDI products for delivery in as little as one hour. Starting later this month, ALDI will launch the service in Atlanta, Dallas and Los Angeles, with potential for future expansion.
 
TK: “Potential for future expansion”? Yeah, I’m counting on it.
 
More from the Aldi release: 
 
“Our partnership with Instacart is another example of ALDI expanding our commitment to customer convenience and value,” said Jason Hart, CEO of ALDI. “We know customers are looking for new ways to save time and money. Instacart provides easy access to our low prices at the click of a button.”
The Instacart experience is as easy to shop as an ALDI store. Customers fill their virtual carts by visiting instacart.com or downloading the Instacart App. At checkout, customers can choose a delivery window that works best with their schedule, anywhere from an hour or up to a week later. Instacart’s personal shoppers do the rest.
 
TK: TGTBT? Yes it seems “too good to be true,” doesn’t it? More from the release:
 
“From their unique assortment of goods to their low price commitment and high standards, the ALDI and Instacart partnership aligns on delivering excellent value and convenience to consumers,” said Apoorva Mehta, CEO of Instacart. “There’s a clear demand for quality grocery delivery, and ALDI and Instacart are working together to meet it.” 
 
 
TK: From Ashley’s coverage, I found this passage spot on:
 
Analysts expect that increased awareness of Aldi prices, through their listing on the Instacart app, will likely be beneficial for the retailer.
 
“In many cases, this will be a big surprise to (consumers) and probably prompt trial,” said Bill Bishop, chief architect at retail consulting firm Brick Meets Click. “Given the importance of digital communications, particularly with younger customers, this could be a big boost to Aldi sales.
 
“My guess is that it could easily be worth a 3%-5% bump in sales over the first month or so, which is a pretty big deal,” Bishop said.
 
 
TK: Yes, if you can buy a bag of tortilla chips at Aldi for $1.48 and a bag at XYZ Supermarket at $2.99, it will make you sit up and take notice. Aldi didn’t explicitly say that the Instacart app prices will be the same as in-store prices. It would squash shopper enthusiasm if the prices were divergent.
 
This pilot between Aldi and Instacart creates even more pressure for grocers to appeal to shoppers in new ways - local produce, grocerants, produce butchers. 
 
But many people always care about their dollars, and getting the benefit of low prices and home delivery seems like cheating.
 
Now it is not a question whether supermarkets should strategically compete on the basis of price or, alternatively, offer their shoppers perks like home delivery. 
 
If the Aldi-Instagram pilot expands and the economic model is sustainable, retailers must do both.
 
Coverage in the Financial Times quoted analyst Bryan Roberts who said speculated online grocery delivery may be less appealing to American shoppers than Europeans.
 
Roberts’ quote went something like, Why bother with home delivery when it is “so easy” just to go to the supermarket?
 
Uh, I don’t think Bryan Roberts ever had a cardboard box filled with Aldi groceries collapse in the middle of an aisle before.