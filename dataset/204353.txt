China has estimated the country's corn planting area will fall by more than a million hectares this year, the first drop in 13 years, as global grain markets brace for the fallout from Beijing's biggest agricultural reforms in nearly a decade.China said in March that it would end its corn stockpiling program, which had driven up domestic prices and spurred imports of cheaper substitutes like sorghum and distillers' grains.
The scheme, under which large volumes were bought at fixed prices, was designed to support rural incomes, but it has left the state with a bulging stockpile to sell.
China is sitting on close to 250 million tonnes of corn, enough to fill Beijing's Bird's Nest stadium around 34 times, and how it plans to dispose of this is being carefully watched by markets.
"During this reform process, and as the corn market changes, we must ensure that farmers remain positive about growing, and at the same time encourage them to change to other crops in non-optimal areas," Vice-Agriculture Minister Yu Xinrong said on Thursday.
Yu said China expected to reduce its corn planting area by 1.33 million hectares (3.29 million acres) this year.
China's support for corn saw the area under cultivation hit 37 million hectares last year, up from 23 million hectares in 2001, according to U.S. Department of Agriculture data.
After the government decided it would no longer buy corn at artificially high prices, farmers in China were expected to switch to other crops, including soybeans.
However, the ministry predicted last month that soybean imports would reach 82.28 million tonnes in 2016, around the same as last year, even though it expects the growing areas to rise by 400,000 hectares this year.
Exports or Imports
The policy changes have raised fears on overseas markets that China would export surplus corn.
"We are looking at a big possibility of corn exports from China as domestic prices are likely to fall to the level of the international market," said a Singapore-based trader.
However, some Chinese observers have been concerned that cutting domestic production could eventually mean the country increased its dependence on imports.
Another agriculture ministry official, Zeng Jinde, said corn was not likely to go the same way as soybeans, where more than 80 percent of demand is met through imports, with domestic corn supply security still a priority.
China's corn production had risen by more than 108 million tonnes in the last 12 years, he said, and the country continued to impose a 7.2 million-tonne annual import quota.
A recent forecast by the Ministry of Agriculture said 2016 corn output was expected to drop 4.2 percent to 215.17 million tonnes. It said production would also be hit by bad weather in growing areas in the northeast, as well as floods in the south.
Though production is set to fall further to 205.67 million tonnes by the end of 2020, consumption is forecast at 221.92 million tonnes, with the gap to be met through inventories, rather than through imports.
The agriculture ministry also said that it would work to rectify "structural contradictions" in the pork market, which has seen prices soar recently as a result of shortages.
New environmental constraints have made it difficult to increase supplies, but China aims to scale up and relocate pig farms to more suitable regions, the ministry said.