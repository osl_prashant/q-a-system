A new culinary competition is coming to California, focused on berries and hosted by Watsonville-based California Giant Berry Farms.
Foodservice companies can submit chef candidates and the recipes they create, and finalists will participate in the Chef Invitational event July 8-11.
Dishes must include at least two of the four berries California Giant offers — strawberries, blueberries, raspberries and blackberries.
The finalists will present their berry-laden creations for judging and also go on field tours and spend time with California Giant farmers, according to a news release.
Prizes for the winner include a paid trip to Monterey, Calif., and paid registration to attend the PMA Foodservice Conference with California Giant.
The chosen recipe will also be featured at the company booth July 29.
“We are very excited about this new event and the opportunity for California Giant to collaborate with our foodservice partners and their chefs,” Tom Smith, director of foodservice, said in the release. “Any time we can bring individuals together from every walk of life throughout the supply chain, we are going to learn from one another and build upon it.”
Chef nominees and their recipes must be submitted to California Giant by May 18, and finalists will be announced June 1.