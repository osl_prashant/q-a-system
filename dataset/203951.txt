Avocado importers expect to receive ample supplies of good-quality fruit out of Mexico as the summer flora loca crops winds down and the new aventajada crop gets up to speed.
For the first time this fall, Mexico's volume is expected to be supplemented with product from the state of Jalisco, which the U.S. Department of Agriculture in May cleared to export avocados to the U.S.
Berkeley, Calif.-based Healthy Avocado Inc. sources its avocados exclusively from Mexico year-round, said president Paul Weismann, who recently returned from a trip there.
After some high f.o.b.s this summer, prices were "dropping tremendously" by early August, he said.
That was due in part to fruit size that was smaller than what most retail buyers want. Growers were producing size 60s and 70s, when most retailers much prefer 48s and some 60s.
This year's crop from Mexico is expected to equal last year's 980,000 tons, Weismann said, adding that he does not anticipate quality problems.
Mexico's main crop that typically runs from late September through June wound down earlier than usual, and when the flora loca crop got off to a late start, it helped create a gap that caused the spike in prices, said Rob Wedin, vice president of sales and marketing for Calavo Growers Inc., Santa Paula, Calif.
He expects the new crop from Mexico to be at least 10% larger than last year's as a result of more groves being certified.
Mexico is "a strong supplier year-round," said Gary Caloroso, marketing director for Giumarra Agricom International, Escondido, Calif.
"Mexico will start picking up in September and then really get going in October," he said.
West Pak Avocado Inc., Murrieta, Calif., has a high-tech packing facility in Michoacan, Mexico, that will be in full swing as the new avocado supply comes on, said Doug Meyer, vice president of sales and marketing.
Meantime, importers are waiting to see when the first avocados grown in Jalisco will be sent to the U.S.
Until now, only avocados from Michoacan were cleared for import to the U.S., but in May, USDA announced that Jalisco now has been certified, as well. Other Mexican states that meet phytosanitary criteria also may ship avocados to the U.S. eventually.
Officials in Mexico were putting together a work plan that defines the process for certifying orchards, growers and packinghouses, said Emiliano Escobedo, executive director of the Hass Avocado Board, Mission Viejo, Calif.
"Once that plan is approved, they will be able to start shipping fruit to the U.S.," he said.
About 5% of the 3.4 billion pounds of avocados produced in Mexico are grown in Jalisco, Escobedo said.
USDA estimated Jalisco growers would export about 100 million to 110 million pounds to the U.S. this season.
The number could be twice that in future seasons, Wedin said.
Calavo plans to begin operating its new, 70,000-square-foot packinghouse in Jalisco when avocados start shipping from there to the U.S., Wedin said.
West Pak's Meyer said the Jalisco deal should not have a significant impact on volume of avocados exported to the U.S. this season.
"We see that as a ramp up for the following years," he said. "It will be nice to have another region in Mexico that is able to help support the growth in the U.S."
No date for shipments to begin out of Jalisco had been set as of early August.