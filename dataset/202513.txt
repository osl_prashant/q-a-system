Most consumers still prefer to select their avocados from a bulk display at the supermarket, but an increasing number of shoppers are opting for the convenience of bagged fruit.
The volume of bags that Fallbrook, Calif.-based Del Rey Avocado Co. Inc. produces has doubled over the past five years from 5% to 10%, said partner Bob Lucy.
"It seems to really be working," he said. "We're bagging more and more every day."
Big box stores and even mainline supermarkets often have displays of two different sizes of avocados along with a third display of bagged product, he said.
"It really works very well," he said. "It's a great added value for avocados."
Giumarra Agricom International LLC, Escondido, Calif., continues to include health and nutrition messages on its avocado bags, said Gary Caloroso, director of marketing for avocados and asparagus.
"We were the first to do that in aggressive way," he said, adding that the program has been "very successful" so far.
The number of bags the company packs still is a small portion of its overall volume, but the number continues to rise, he said.
Henry Avocado Corp., Escondido, Calif., also ships "quite a few" bags, said president Phil Henry.
"The demand for bags continues to be strong, and we don't anticipate any change in that trend," he said.
Rankin McDaniel, owner and president of McDaniel Fruit Co., Fallbrook, Calif., said he is seeing growth in consumer packs at his company and throughout the industry.
"Multiple offerings versus individual bulk avocados have increased substantially over the past several years," he said.
Retail customers now make a wide range of stock-keeping units available in their stores, he said.
Index Fresh Inc., Riverside, Calif., packs about 35% of its avocados in bags, said Giovanni Cavaletto, vice president of sourcing.
Five years ago, the rate was 15% or 20%, he said.
Bags have proven popular for retailers because they present another way to move volume, he said.
Usually, suppliers pack smaller-sized avocados, like 70s and 84s, in bags, Lucy said.
But some retailers ask for larger fruit, like 40s or 48s, and some even request three-count bags of size 36 avocados.
Del Rey will bag whatever sizes the customer requests, he said.
Similarly, Giumarra typically packs three to six avocados of various sizes in its bags, depending on customer requests, Caloroso said.
The company packs bags for conventional and organic avocados.
McDaniel Fruit Co. has a number of bagging machines that can pack avocados in whatever format a particular customer wants, McDaniel said.
The amount of bags the company packs "seems to be growing every year," McDaniel said.
It's not a hassle for McDaniel to pack in bags, he said.
"We foresaw the growth in the industry, and our facility is set up to grow with the demand without losing any efficiencies."
Index Fresh packs premium size 36 avocados in bags for some customers, and 2-pound bags of its smallest fruit for others, Cavaletto said.
"(Bags are) attractive for high-end customers as well as for bargain shoppers," he said.
Last fall, Cavaletto said he visited a local mainstream supermarket and was ecstatic to see five displays of avocados showcasing large, small, ripe, organic and bagged fruit.
"The fact that they were giving us that much real estate and that many different options for their consumers tells us that the avocados had been a winner for their produce department," he said.
"It was one of the most invigorating store checks I had ever done."