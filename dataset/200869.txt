The U.S. Department of Agriculture's (USDA) National Institute of Food and Agriculture (NIFA) today announced 12 awards in 10 states totaling $2.3 million to help relieve shortages of veterinary services through education, extension, training and support for new or existing veterinary practices in designated rural shortage areas. These fiscal year 2016 competitive grants are funded through the new Veterinary Services Grant Program (VSGP), authorized by the 2014 Farm Bill."The new Veterinary Services Grant Program will enable training and retention initiatives to support veterinarians and veterinary technicians so they can continue to provide quality services in rural areas," said NIFA Director Sonny Ramaswamy. "It also supports the expansion of existing veterinary educational programs and facilities, including mobile services."
VSGP grants fund work by universities, veterinary associations, and state, local or Tribal agencies to help relieve veterinary workforce shortages in the U.S. food and agriculture sector. Funds may also be used to support the establishment or expansion of veterinary services in eligible rural areas.
Fiscal year 2016 grants include:
¬? American Association of Bovine Practitioners, Opelika, Ala., $224,136
¬? Colorado State University, Fort Collins, Colo., $238,251
¬? University of Georgia, Athens, Ga., $236,243
¬? Kansas State University, Manhattan, Kan., $239,656
¬? University of Minnesota, St. Paul, Minn., $238,346
¬? Betsy the Vet, Inc., Hardin, Mont., $124,462
¬? Lewistown Veterinary Service, Lewistown, Mont., $116,036
¬? Town and Country Veterinary Clinic, Auburn, Neb., $124,760
¬? Utah State University, Logan, Utah, $236,619
¬? University of Wisconsin, Madison, Wisc., $237,32
¬? Wisconsin Veterinary Medical Association, Madison, Wisc., $238,429
¬? Squared Circle Veterinary, Evanston, Wyo., $104,000
Among the funded projects, Kansas State University will use its grant to training and networking opportunities for rural production animal veterinarians. Town and Country Veterinary Clinic in Nebraska will expand its mobile veterinary practice service radius an additional 20-40 miles with the help of its grant. More information on these and other projects is available on the NIFA website.
Since 2009, NIFA has invested in and advanced innovative and transformative initiatives to solve societal challenges and ensure the long-term viability of agriculture. NIFA's integrated research, education and extension programs support the best and brightest scientists and extension personnel whose work results in user-inspired, groundbreaking discoveries that combat childhood obesity, improve and sustain rural economic growth, address water availability issues, increase food production, find new sources of energy, mitigate climate variability and ensure food safety.
To learn more about NIFA's impact on agricultural science, visit www.nifa.usda.gov/impacts, sign up for email updates or follow us on Twitter @usda_NIFA, #NIFAimpacts.