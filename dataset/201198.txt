Pro Farmer Crop Condition Index






Spring wheat




This Week




Last Week




Year-Ago





Idaho (5.74%)*


22.66


18.42


23.08




Minnesota (12.99%)


53.65


53.26


46.67




Montana (14.95%)


36.03


43.06


61.44




N. Dakota (50.80%)


154.42


155.44


185.07




S. Dakota (10.36%)


22.27


22.78


36.93




Washington (4.22%)


14.05


15.44


17.19




Spring wheat total


305.99


311.35


374.19




* denotes percentage of total national spring wheat crop production.
Following are details from USDA's National Agricultural Statistics Service (NASS) state crop and weather reports:
North Dakota: For the week ending July 2, 2017, dry conditions persisted over much of the State, especially in the western and southern parts, according to the USDA's National Agricultural Statistics Service. Much needed moisture was received in central and eastern North Dakota, with rainfall amounts ranging from one to two inches. There were some reports of producers in the western part of the state baling wheat because of drought conditions. Temperatures for the week averaged two to eight degrees below normal. There were 6.3 days suitable for fieldwork. Topsoil moisture supplies rated 24% very short, 30 short, 44 adequate, and 2 surplus. Subsoil moisture supplies rated 17% very short, 28 short, 52 adequate, and 3 surplus.
Spring wheat condition rated 11% very poor, 19 poor, 29 fair, 37 good, and 4 excellent. Spring wheat jointed was 94%, near 97 last year, but ahead of 84 average. Headed was 57%, well behind 78 last year, but ahead of 52 average. Coloring was 4%.
Montana: Another week of hot dry weather with limited precipitation occurred for a majority of the state, according to the Mountain Regional Field Office of NASS. High temperatures ranged from the lower 80s to upper 90 degrees and low temperatures ranged from 28 degrees in West Yellowstone to the lower 50s. The highest amount of precipitation was recorded in Hysham with 0.99 of an inch of moisture while other stations recorded between zero and 0.84 of an inch of moisture.
Crop conditions continue to deteriorate due to the hot dry weather Soil moisture conditions continue to decline with 80% of topsoil rated very short to short and 77% of subsoil rated very short to short compared with 35% of topsoil last year rated of very short to short and 38% of subsoil last year rated very short to short.
Winter wheat condition is rated 73% fair to good compared with 63% last year rated good to excellent.
Minnesota: Despite the overall unseasonably cool, damp weather throughout Minnesota, there were 4.8 days suitable for field work during the week ending July 2, 2017, according to USDA's NASS. Other field activities included spraying herbicides and cutting hay. Topsoil moisture supplies rated 1% very short, 8% short, 87% adequate and 4% surplus. Subsoil moisture supplies rated 0% very short, 6% short, 88% adequate and 6% surplus.
Most Minnesota small grains were at or beyond the jointing stage by this week. Spring wheat crop was 98% jointed or beyond with 72% headed. Spring wheat crop condition rated 86% good to excellent.