Good morning!
No overnight trade; abbreviated session today... Markets were closed overnight due to the Thanksgiving holiday. Grain and livestock markets will open at 8:30 a.m. CT for an abbreviated session. Grain markets close at 12:05 p.m. CT, while livestock markets halt trading at 12:15 p.m. CT.
Opening calls... An uninspiring Weekly Export Sales Report could lead to some profit-taking in the grain markets amid thin trading volume today. We expect these markets to hold steady or soften slightly to start the session. Cattle and hog futures are likely to see followthrough buying today after delivering strong performances on Wednesday. The cash hog market has stabilized in recent days and December lean hog futures are below the cash hog index.
Ho-hum export sales for the week ending Nov. 16:



Commodity
Actual (MT)

Expectations (MT)



Corn

2017-18: 1,080,900
			2018-19: 25,500


2017-18: 900,000-1,300,000
			2018-19: 0-100,000



Wheat

2017-18: 199,800
			2018-19: 189,600


2017-18: 350,000-550,000
			2018-19: 0



Soybeans

2017-18: 869,100
			2018-19: 34,500


2017-18: 1,000,000-1,400,000
			2018-19: 0-100,000



Soymeal

2017-18: 379,800


2017-18: 100,000-300,000



Soyoil
2017-18: 4,200
2017-18: 0-22,000


Cotton
2017-18: 357,000
			2018-19: 43,100
NA


Beef
2017: 9,300
			2018: 1,100
NA


Pork
2017: 16,300
			2018: 7,500
NA



EPA rejects requests to change point of obligation for RFS... Requests to change the point of obligation away from refiners to show compliance with the Renewable Fuel Standard (RFS) have been rejected, the Environmental Protection Agency (EPA) announced late Wednesday. EPA said the petitioners requesting the change were unanimous in shifting the point of obligation away from refiners, but "they differed somewhat in their suggestions for alternatives." Even if there were "some marginal net benefits to changing the point of obligation, we believe that the disruptive effects of a change at this time would still warrant denial," EPA said. The agency also noted there would be "no beneficial impact on the RFS program or renewable fuel volumes and would decrease competition among parties that buy and sell transportation fuels at the rack, potentially increasing fuel prices for consumers and profit margins for refiners, especially those not involved in fuel marketing." EPA Administrator Scott Pruitt previously told farm-state lawmakers in an Oct. 19 letter that EPA found "the record demonstrates that granting that petition would not be appropriate." He further stated he directed staff to "finalize this decision within 30 days." As for the RFS levels for biofuels for 2018 and 2019 for biodiesel, EPA has pledged to meet the statutory deadline of Nov. 30.
Early look at next week in Washington... The full Senate is expected to vote on its tax reform plan after a "vote-a-rama," including many amendments offered by Democrats and Republicans. If it passes, differences with the House bill will need to be reconciled before any final legislation can be sent to President Donald Trump's desk. Tuesday morning brings the confirmation hearing for Federal Reserve chair nominee Jerome Powell, who favors gradual interest-rate increases and wants to ease financial regulations. Also on Tuesday, President Donald Trump will meet with Democratic and Republican congressional leaders to discuss a federal spending plan to prevent a partial shutdown and keep the government open after current funding expires Dec. 8. USDA on Tuesday will release some information from its USDA long-term agricultural projections (to 2027). On Wednesday morning, Federal Reserve Chairwoman Janet Yellen testifies before the congressional Joint Economic Committee.
Groups weigh in on Brazilian crop prospects... Brazil will likely export between 66 MMT and 67 MMT of soybeans in 2017, estimates the cereal exporters association Anec. Earlier this month, it projected exports at 66 MMT. Looking ahead to 2018, Anec expects the country to export 68 MMT of soybeans. The group also upped its corn export forecast for 2017 by 3 MMT to 33 MMT. In 2018, Anec expects the country to export just 30 MMT of corn. Meanwhile, the consultancy Agroconsult expects Brazilian farmers to harvest 111 MMT of soybeans in 2017-18, though it says good weather could lift the crop at 115 MMT. The consultancy also trimmed its first crop corn peg by 1.3 MMT to 26.1 MMT.
Argentine ag ministry issues first wheat crop estimate of the season... In its first estimate of the season, Argentina's ag ministry estimates the country will produce an 18 MMT wheat crop in 2017-18. The ministry also said it expects corn to be planted to 8.8 million hectares, with soybean planting likely to come in at 16.8 million hectares.
Big drop from year-ago for U.S. soybean shipments to China in October... China imported more than 5.856 MMT of soybeans during October, a 12.35% jump from year-ago, with Brazil supplying 3.376 MMT of that total and the U.S. shipping it 1.325 MMT of soybeans. Of note, this represented a 45.47% drop from year-ago shipments for the U.S., as crop troubles led to a drop-off in Brazilian shipments last year. With just two months remaining in the year, China has imported nearly 77.307 MMT of soybeans, a gain of 15.25% from this point last year.
China to boost standards for pesticide and veterinary drug residues... China's agriculture ministry aims to have 10,000 standards for pesticide and veterinary drug residues on the books by 2020, according to an official's speech at an annual government food safety meeting. Exporters to China will also need to conform to these tolerances and they may not correspond to standards used in their own country. Reports note that new labs staffed with inexperienced technicians using questionable procedures may produce anomalous results.
China to subsidize grain transportation, storage facilities... Projects to upgrade or build facilities to load and receive grains along major railways and waterways will be subsidized by the Chinese government, according to a document released by the National Development and Research Commission (NDRC). "Setting up special funds... will help reduce the cost of grain distribution and improve efficiency," the document said. Grain facilities that offer storage, processing, trading and quality inspection services will also be subsidized, with the NDRC listing specifications for the facilities including that they have more than 100,000 tonnes of storage capacity. The subsidies could be up to 100 million yuan ($15 million).
IGC hikes global corn crop estimate... The International Grains Council (IGC) hiked its global corn crop estimate by 6 MMT to 1.040 billion MT, which is still down 39 MMT MMT from 2016-17. It also raised its consumption and export forecast slightly for the grain, resulting in just a 3 MMT climb in corn carryover stocks to 206 MMT. It also raised its global wheat crop estimate by 1 MMT to 749 MMT, but a like increase in consumption kept carryover stocks steady. While IGC made no change to its global soybean crop estimate, it did trim its trade and consumption pegs, resulting In a 2 MMT increase in its carryover estimate to 41 MMT.
A growing number of young Americans are trading desk jobs for farming... For only the second time in the last century, the number of farmers under 35 years old is increasing as a movement of highly educated, ex-urban, first-time farmers takes root, the Washington Post reports. More likely to grow organically and get involved in their local food systems, this new generation could help preserve the place of mid-size farms in the rural landscape, the article concludes.
Biggest shopping weekend of the year is here... Some 69% of Americans — an estimated 164 million people — are planning to shop or are considering shopping during the holiday, according to the National Retail Federation. As for the stock market, recent history suggests this is a very good time for retail stocks. Since 2007, the week before Black Friday to a week after, the sector usually gains 5%.
U.S. crude back at two-year highs... Futures are up just over 1% to $58.65/barrel, following the partial shutdown of Keystone XL and ahead of a likely output cut extension next week by OPEC and a group of other producers.
Mexico revokes permit to market GMO soybeans in seven states... Monsanto Co.'s permit to commercialize genetically modified soybeans in seven states has been revoked by Mexico's ag sanitation authority, the company reported Thursday. Monsanto says the move was made on unwarranted legal and technical grounds. According to the Mexican newspaper Reforma, the permit was revoked due to the detection of transgenic Monsanto soybeans in areas it was not authorized, but Monsanto rejects that claim, saying authorities had not done any analysis on how the soy in question was planted.
Cold Storage Report signals solid beef demand... Cattle futures were bolstered by more signs demand continues to chew through supplies via the Cold Storage Report and better than expected cash action on Wednesday. But futures did back off their highs by the close, however, which signals profit-taking is a possibility today.
Cash market stabilizes ahead of Thanksgiving... Cash hog bids stabilized the past two trading days, possibly hinting that packers are in need of supplies for post-holiday kills where they will look to make up for Thanksgiving downtime. Bids climbed 6 cents on a national average basis on Wednesday, following similar gains on Tuesday.
Overnight demand news... Tunisia purchased around 100,000 MT of soft milling wheat and 50,000 MT of feed barley from optional origins.
Today's reports:

7:30 a.m., Weekly Export Sales -- FAS
11:00 a.m., Cotton Ginnings -- NASS
1:00 p.m., Weekly Red Meat Production Report -- AMS