Ranchers placed 11.0 percent more cattle in feedlots in April than a year earlier, the most for the month in 14 years, the U.S. Department of Agriculture reported on Friday, beating average forecasts.Higher cattle prices paid by packers last month enhanced profits for feedlots, allowing them to buy more less-costly calves for fattening. The demand prompted ranchers to drive cattle into feedlots ahead of schedule.
Cattle that entered commercial feeding pens in April will start arriving at packing plants around late summer, which could help keep a lid on beef prices at that time, said analysts.
On Friday Chicago Mercantile Exchange live cattle futures slumped more than 2 percent, partly because of the larger-than-expected April placement, said analysts and traders.
The USDA report showed placements at 1.848 million head, up from 1.664 million in April 2016 and above the average forecast of 1.777 million. It was the most for the month since 1.870 million in 2003.
The government put the feedlot cattle supply as of May 1 at 10.998 million head, up 2.0 percent from 10.783 million a year ago. Analysts, on average, forecast a 0.8 percent gain.
USDA said the number of cattle sold to packers, or marketings, were up 3.0 percent in April from a year ago, to 1.703 million head.
Analysts had projected a 1.8 percent increase from 1.658 million last year.
"This report highlights the idea that we're going to have a few extra cattle waiting for us," said Allendale Inc chief strategist Rich Nelson.
The highest prices for cattle in more than a year, in some parts of the U.S. Plains, have returned bullish enthusiasm to feedlot operators, he said.
Hopes of U.S. beef returning to China, and fallout over the Brazilian beef scandal, contributed to that enthusiasm, said Nelson.
Jim Robb, director of the Livestock Marketing Information Center (LMIC), said feedlot profitability and the difference between futures and cattle prices, or basis, combined to pull more cattle into feedlots than some had anticipated.
LMIC calculated that feedlots in April, on average, made a profit of $190 per steer sold to meat companies. Last year in April cattle feeders barely broke even, said Robb.
"This is a dramatic turnaround in terms of feedlots ability to buy animals to go into their feedyards," he said.