The 2018 Winter Olympics will begin February 9 in Pyeongchang, South Korea and athletes are preparing to be on the global stage.

Pork producers are preparing too, but in a different way. According to Steve Georgy, vice president of Allendale, Inc., demand to South Korea has increased 21 percent compared to where the picture was in 2017, but it’s not solely because of the Olympic Games.

On AgDay, he said there’s bird flu issues that are increasing demand. In one week, more than 800,000 birds were culled. Russia is having some of the same problems.

“This may have more of a bigger impact than we know right now, so be careful of that with these principles,” said Georgy.

Georgy thinks the hog market is showing signs of inflation, and it’s something that can’t be sustained throughout 2018. He suggests that producers should be protected moving forward and leave the upside open.

“We do need to be careful right now,” he said.

Watch his full comments on AgDay above.