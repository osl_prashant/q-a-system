Common wisdom suggests that cropland’s being wasted growing feed, not food, and that the level of livestock rations cannot be sustained. But a new UN report says: Wrong on both counts.If there’s a bad boy that industry critics love to demonize — other than any person on Earth who consumes animal foods, that is — it’s the world’s farmers.
Why? Because they’re stupidly growing feed crops to be eaten by livestock, not people, that’s why. All that corn they harvest every year shouldn’t be mixed into poultry or pig rations; it should be ground up and cooked into the ever-popular cornmeal mush, which is a wonderful centerpiece for any meal.
Just ask the more than one billion people alive today who can’t afford to eat anything else.
Of course, the ultimate solution to both global hunger and food security, those same critics insist, is for everyone to simply stop eating meat and dairy altogether. Replacing those lost calories for the six billion people who currently consume such foods is to be taken care of by shoveling all that corn and soy onto dinner plates, as opposed to feed bins.
As a new analysis from the UN’s Food and Agriculture Organization reported, however, punctured some of those cherished activist truisms, however. For example:
86% of global grain production is not edible by humans
The world’s livestock also require less feed per kilogram of meat yield than previously estimated closer to 3 kilograms of feed per kilogram of meat
A combination of genetics, better veterinary care and improved diets can increase yields and reduce the amount of feed required to produce meat, poultry and dairy foods
The researchers, who were led by Anne Mottet, an FAO agronomist, said that their findings could guide policymakers aiming to make food systems more efficient as the world’s population approaches nine billion people by mid-century, noting that meat is “a high-calorie, high-protein food that also provides key nutrients such as iron, vitamin A and vitamin B12, which are difficult to obtain from a totally plant-based diet.”
The truth about crops
In a summary of the FAO study on the website UnDark.com, the challenge of increasing food production sustainably is put into perspective. “In the past, the article noted, “the primary solution to food shortages has been to bring more land into agriculture and to exploit new fish stocks.”
We know what has happened with the world’s fisheries: They’re seriously depleted from over-harvesting.
But agriculture has done a far better job of expanding sustainably. “Over the past five decades, while grain production has more than doubled,” the report stated, “the amount of land devoted to arable agriculture globally has increased by only 9%.”
Re-read that stat: Since 1960, the world’s farmers — primarily those in the developed world — have produced twice the amount of grain on only 9% more acreage.
And they’re the bad guys?
The FAO study also noted another key statistic on feed versus food debate typically ignored by industry critics.
That debate rarely accounts for how much the typical livestock diet consists of hay, straw, crop residues and other forage sources that humans don’t eat. By using a new computer model that simulates livestock production on a new database on global feed rations for cattle, buffalo, sheep, goats, pigs and chickens, plus the utilization of pastures and grasslands, the researchers drastically increased the estimated efficiency of feed conversion by livestock.
Equally important, the researchers wrote in the September issue of Global Food Security, “Critics typically assume that all meat is produced on intensive farms, such as feedlot cattle, without considering smaller or backyard farms with domesticated pigs or chickens and farms that let animals graze.”
That’s the other piece of the plants vs. animals debate that veggie activists conveniently ignore: The agricultural, energy and land-use impact of global livestock production cannot be assessed solely on the basis of inflated estimates of how much grain is consumed by feedlot cattle or on confinement hog farms.
If demographic projections are accurate, the world’s population is going to need significantly more food in the next five decades. More livestock will be required, and that’s going to require more feed to be grown and more pasture to be put into production.
But sustainably increasing production appears far more achievable, given a clearer analysis of the statistics over the previous five decades.
And going forward, I would assert that there’s one suggestion that won’t be embraced as the key to meeting the challenge of food security.
Two words: Cornmeal mush. 
 
The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator