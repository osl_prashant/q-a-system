Frederic Coville and Elizabeth White began working with blueberries in 1910. As a result of their work, by 1916 blueberries were a valuable crop in the Northeastern U.S.  
Since that time, American farmers, American universities, and American nurseries have invested millions of man hours and dollars developing better varieties of blueberries and perfecting the protocol to successfully grow them.
 
American produce companies who have profited marketing American-grown blueberries to American customers are investing those profits in Mexican farms. 
 
The Mexican farms are planted with blueberry varieties that are products of American breeding programs. The American blueberry farmer has invested his money, the productivity of his land, and the sweat off of his back, proving the best of these varieties and developing the protocols of how to grow them.
 
When an American company starts large farms in Mexico using the best of American blueberry varieties, planted with growing protocols proven in America, and they couple this technology with cheap land, cheap unregulated labor, and minimal environmental regulations, they can produce large volumes of fruit at a cost far below the cost to farm in America. 
 
If the trade is not beneficial to both trading countries, then do not trade for that specific item.
 
The U.S. consumes over 80% of all blueberries grown in the world. The blueberry farms planted in Mexico by American produce marketing companies were planted for the sole purpose of shipping low-cost fruit into the American market and making a high profit. These profits are made at the expense of the American blueberry farmer, and under the disguise of free trade. There is a big difference between “free trade” and “fair trade.”
 
Trade started in the Stone Age. It is based upon the premise that if I have something you want or need and you have something I want or need we can negotiate a trade and both of us will benefit. Early trade was barter. Goods traded for goods. You selected what you traded for and how you traded. Trade was "managed.” 
 
Later, money was introduced. Money separated the buying from the selling. From the marketer’s view point it is all trade. Money is made on the transition of goods back and forth. 
 
From the grower’s view what is traded, the volumes traded, and when it is traded is critical. The American consumer is entitled to blueberries 365 days a year. American blueberry farmers cannot deliver blueberries 365 days a year. We have a need for imported blueberries. If blueberry trade is managed in terms of timing and volume, then all parties benefit. This is a good definition of fair trade.
 
A fair trade agreement needs to be managed. It needs to be crop specific and it does need to manage the volume and timing of what is traded. If the trade is not beneficial to both trading countries, then do not trade for that specific item. It makes no sense to allow a product developed in America to be grown or produced in Mexico and sold in America at prices and volumes that destroy the American farmer who helped develop it. 
 
It is time we put “America First.”
 
Ken Murray is a Georgia blueberry grower.
 
What's your take? Leave a comment and tell us your opinion.