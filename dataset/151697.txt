Corn futures gave back all of their earlier gains late this week as forecasts for next week are drier than previously feared. While cold, soggy soils are limiting planting and emergence, delays haven’t been severe enough yet to trigger sustained buyer interest. Weather supported wheat futures this week as cold temps threatened portions of the winter wheat crop and delayed spring planting. Soybeans were pressured by prospects for acreage to at least match March planting intentions given delays in corn and spring wheat plantings.
Pro Farmer Editor Brian Grete highlights this week's Pro Farmer newsletter below: 



Cattle futures surged to new highs amid a flurry of chart-based buying and strengthening cash cattle prices. Hog futures made a strong case for a short-term low with a “V” bottom on the daily charts. Traders built more premium into summer-month hog futures despite continued declines in the cash index. All markets are keenly watching President Donald Trump as he continues to talk tough with U.S. trading partners.
Click here for this week's newsletter.