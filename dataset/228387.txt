Farm Service Agency employee accused in crop insurance scam
Farm Service Agency employee accused in crop insurance scam

The Associated Press

MONTGOMERY, Ala.




MONTGOMERY, Ala. (AP) — An Alabama woman who works for Dothan's Farm Service Agency has been arrested on multiple fraud charges.
Federal authorities say 36-year-old Anna Marie Knowles, of Headland, was arrested Thursday and faces charges of wire fraud, theft of government property and using a false document in a matter under the jurisdiction of the federal government.
The Dothan Eagle reports prosecutors believe Knowles knowingly carried out a crop insurance scheme that illegally netted her $116,500 from the Noninsured Crop Disaster Assistance Program.
According to a release from U.S. Attorney Louis Franklin's office, if convicted Knowles faces as much as 20 years in prison along with monetary penalties and restitution.
___
Information from: The Dothan Eagle, http://www.dothaneagle.com