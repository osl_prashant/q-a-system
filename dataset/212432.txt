The U.S. avocado market is ripe for the start of the Peruvian season, as Mexico’s crop winds down and California volumes are about half of what they were in 2016.Peru is expecting to ship about 150 million pounds of avocados to the U.S. for its June-August season — about twice its total of about 70 million pounds last year.
It’s shaping up to be Peru’s largest avocado crop to date, and it comes at a time when short supplies and growing demand are combining to boost prices to record levels, marketers said.
“Our primary goal this season is to supply the U.S. with top-quality fruit during the peak summer season, and to educate the public about the wonderful health benefits of Peruvian avocados,” said Xavier Equihua, CEO and president of the Washington, D.C.-based Peruvian Avocado Commission.
An off-bearing year in California and less volume in Mexico than in recent years, combined with continued growth in demand across the U.S., have propelled prices to about double what they were a year ago, marketers said.
“It’s a reflection of the amount of fruit that’s available,” said Robb Bertels, vice president of marketing with Oxnard, Calif.-based Mission Produce Inc., which owns three avocado ranches in Peru. “With the smaller California crop and lessening volume from Mexico, the market can stay comfortable with more than 50 million pounds of avocados a week.
 
Volume
Lately, however, Bertels says overall volume in the market from Mexico and California has been down to around 40 million pounds a week.
“Demand has really exceeded supply in the last couple of months. We really don’t see that changing much until Mexico’s new crop gets going later in the summer. It feels as though prices will remain higher throughout the summer.”
The volume from Peru should continue to grow, with an expected increase of 30% to 40% over last season, Bertels said.
“With the amount of fruit that’s planned to come into the North American market, it should be a great benefit for the summer months. The California crop is a lot smaller — about half the size it was last year. It creates a pretty good window for fruit from Peru.”
Mission was scheduled to receive its first Peruvian shipments the week of May 22, Bertels said.
“There may have been a little bit arriving earlier than that,” Bertels said, who visited all three of the company’s ranches in early May.
“All the fruit I saw looked really good,” he said.
Prices
As of May 19, according to the U.S. Department of Agriculture, two-layer cartons of hass avocados from Mexico were priced at $54 for size 32s; $60-62, 36s; $58-62, 48s; and $52-54, 60s.
A year earlier, the same product was $34 for size 32s; $40-42, 36s; $42-45, 48s; and $32, 60s.
Heavy rains in March pushed the Peruvian harvest back by a few weeks, due to damaged inflicted on roads and bridges, but things improved quickly, Bertels said.
“Fruit is moving from the packing houses to the ports. Originally, we expected to have early arrivals here by the middle of May, but it’s been pushed to the first week of June.”
The crop emerged from the spate of heavy rain relatively unscathed, said Fortunato Martinez, general manager and vice president of Naturipe Avocado Farms, a branch of Salinas, Calif.-based Naturipe Farms LLC.
“Despite the heavy rains, the crop is similar to last year’s,” he said. “Strong volumes are beginning to reach North America in the weeks to come.”
The rains left no issues with the crop, said Rankin McDaniel, president of Fallbrook, Calif.-based McDaniel Fruit Co.
“Quality is very good, even with all the rains they’ve had to endure,” he said. “It appears the orchards and the fruit itself weathered the situation pretty good.”
Even with any delays, the Peruvian fruit will be timely — and welcome — in North American markets, said Doug Meyer, senior vice president of sales and marketing with Murrieta, Calif.-based West Pak Avocado Inc.
“Peruvian supply is needed in the U.S. this season,” he said. “We’re in the middle of a significantly lower California crop, and there is continued uncertainty as to the consistent weekly volume that Mexico can supply into the market with their current crop, which ends, for the crop year, by the end of June.”