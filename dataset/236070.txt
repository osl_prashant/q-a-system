Grain mostly higher and livestock mostly higher
Grain mostly higher and livestock mostly higher

The Associated Press



Wheat for March rose 3.75 cents at 4.6325 a bushel; March corn was up 2 cents at 3.7050 a bushel; March oats fell 2.50 cents at $2.60 a bushel; while March soybeans gained 3.75 cents at $10.38 a bushel.
Beef higher and pork unchanged on the Chicago Mercantile Exchange February live cattle was up .62 cent at $1.2802 a pound; March feeder cattle rose 1 cent at $1.4655 a pound; while April lean hogs was unchanged at $.6995 a pound.