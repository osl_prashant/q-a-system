<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


MONTEREY, Calif. - Seed companies naturally focus their attention on growers, but it's important to hear from the rest of the supply chain, Candace Wilson, strategic account director for Seminis, said at the Markon Chef Summit.
"The farmer is our direct customer," Wilson said at the second annual Markon Cooperative event in Monterey, where she spoke at an education session for chefs. "It's our job to set them up for success. But as we've learned, there are a whole lot of opinions and different needs that happen further down the chain. If we're not keyed in to those needs, then typically we're missing the mark on our breeding resources as well."
While Seminis varieties don't typically take the 15 years or so that some produce items need for commercial development, the time investment is significant.
Development of annual crops - like melons or eggplant - take four to seven years at Seminis, Wilson said.
"If you're working on onions, broccoli or some of the other biennial crops it can take closer to 12 years to bring a new variety to the market."
Wilson outlined flavor profiles and the development trajectory for Seminis melons such as Melorange and the Golden Dewlicious white fleshed honeydew; and for the company's tomato and Frescada lettuce varieties.
Wilson cited Seminis research that found 81% of consumers preferred its Melorange to regular cantaloupe.