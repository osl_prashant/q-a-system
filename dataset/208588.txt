Scours can be a difficult and chronic issue within a farrowing barn. Not only does it affect the health and growth of the pig, but it also can be detrimental to people who work in the farms daily. It’s important to know which pathogens can cause scours and the potential areas within your operation that can help you prevent or control scours in the farrowing barn.
Important pathogensA pathogen is described as a bacterium, virus or other microorganism that can cause disease (see the box to the left).

Each of these pathogens act differently and cause scours at different ages in the pigs. For example, E. coli, Rotavirus, and Clostridium perfringens tend to be more of an early lactation scours, whereas Coccidia causes scours later in lactation. We’ll focus on these first and discuss TGE and PEDv later.
Overall, scours tend to be a complex multifactorial issue, with causes that fit into five categories: lack of immunity, improper movements, unfavorable environment, high pathogen load and biosecurity. 

1. Lack of immunityThe biggest component to the immunity category is colostrum, which is high in energy, protein and antibodies. Piglets absorb colostrum through the gut for up to 24 hours, and it is the main immune defense the piglet has until it is able to develop its own immunity. Creating high quality colostrum via feedback or vaccines weeks prior to farrowing will help create the antibodies specific to scours. With certain pathogens such as Coccidia, it is important to note feedback can amplify your scours situation if not done properly. Make sure you work with your veterinarian to identify and develop a protocol for your operation.

The next step is to ensure piglets are ingesting the colostrum. When piglets are cold and wet, they might not be actively searching for sustenance. But when you dry off piglets so they are warm and dry, they are more likely to search for the sow’s teat and start to suckle.
 
Another strategy for the improvement of colostrum intake includes split suckling. The concept of split suckling is to separate littermates for a certain time, usually less than an hour, to allow all littermates the opportunity for colostrum intake. This process can be repeated within a litter multiple times to ensure colostrum intake by all littermates.

2. Improper movementsThere are two main classifications of improper movements: piglets and people. For piglets, notice how often piglets are moved between litters during the fostering process. There are two primary fostering strategies: unlimited movements (“peas in a pod”) and restricted movement protocols. Each strategy has its own benefit, but if controlling or preventing scours is your goal, then restricted movement using a form of block sizing is suggested. Additionally, restricted use of fostering or processing carts can help reduce the transfer of pathogens. 


Regarding people, look at ways they can directly transfer scours between litters, including foot traffic and order of events. For foot traffic, when stepping into the farrowing stalls, use equipment such as a fish net or piglet grabber to avoid stepping into the stall. If possible, work in pairs to allow a person to be on each side of the stall to easily collect piglets.
 
In the case of improper “people traffic,” look at the order of events during the treatment or processing of piglets. Handle the healthy piglets before the scouring piglets if at all possible during treatment and processing.
 
 
3. Unfavorable environment
Management of the environment in a farrowing room can be difficult because sows and piglets have different requirements. Sows like a cooler environment to be comfortable, and piglets need a warmer environment to conserve body heat. If there’s a draft or piglets become chilled, they’ll experience stress, which can lead to scours. Heat lamps, mats or heat pads help create the ideal microenvironment for piglets.

Look at the pigs to ensure equipment is working properly. For example, a heat lamp might be set too low if the piglets are laying farther away from the lamp. This is a daily chore and will need to be adjusted as piglets get older.
 
Ventilation is another important aspect of the environment in farrowing rooms. Fans, controllers, heaters and inlets work together to help create this environment. General maintenance is needed to ensure the rooms are ventilated properly. Also, daily observations of the humidity, ammonia levels and air speed are important.
 
 
4. High pathogen load
Another way of describing this category is to say how much “bug” there is in your environment. Although exposure to certain bugs might help develop the gut, I am referring to bugs that create a disease challenge. The main goal is to reduce the pathogen load through cleaning and disinfecting, thereby reducing the chance of passing it to the next generation of piglets. 
 
Wash the farrowing stalls, hallways and load outs after each wean. The use of detergents and hot water will help make the cleaning process easier and allow for a higher likelihood that the organic material has been removed. Once washed, the next critical step is to disinfect the rooms and allow enough time for the rooms to completely dry so the disinfectant can work properly to minimize pathogens of concern.
 
It is also important to wash the equipment used for the pigs daily, including carts used for processing, fostering and mortality removal. Also, don’t forget the instruments and totes used during processing and treatment.
 
 
 
5. Biosecurity
Porcine Epidemic Diarrhea virus and transmissible gastroenteritis in the farrowing barn can be devastating, and I truly hope they are not long-term issues for you. Biosecurity is the most important aspect in trying to prevent these pathogens from entering your operation. If your herd has the unfortune of being exposed, you will need to use extreme measures to help control or eliminate these pathogens from your farm. 
 
Scours can be a complex multifactorial issue that can affect the piglets and people working with the pigs. Consider these opportunities to improve your operation in immunity, movements, environment and pathogen load.
 
I encourage you work with your veterinarian to develop a plan to control, prevent, and in certain cases, eliminate scours within your operation. 
 
This article was featured in the July/August issue of Farm Journal's PORK. Click here to read more.