Crop calls
Corn: Fractionally to 1 cent higher
Soybeans: Fractionally to 1 cent higher
Wheat: 4 to 6 cents lower

Corn and soybean futures enjoyed light short-covering overnight amid a weaker tone in the U.S. dollar index, while wheat futures faced profit-taking pressure. Still, spring wheat futures remain in their steep uptrending channel, so key this morning will be if traders view overnight weakness as a buying opportunity or continue to take profits. Buying in corn and soybean futures will be limited as traders view near-term weather as non-threatening.
 
Livestock calls
Cattle: Mixed
Hogs: Higher
Cattle traders are expected to take a step back and reevaluate positions given the slightly oversold condition of the market. But yesterday's $2.47 drop in Choice beef values and light cash cattle trade beginning in Nebraska yesterday around $123 have traders looking for similar trade to pick up during this morning's online cash cattle auction. Meanwhile, hog futures should see a boost after pork cutout values firmed $2.66 yesterday. This keeps packer demand strong and should result in another day of $1 higher cash hog bids across the Midwest.