Former rodeo clown promotes reusing industry wastewater
Former rodeo clown promotes reusing industry wastewater

By HEATHER RICHARDSCasper Star-Tribune
The Associated Press

CASPER, Wyo.




CASPER, Wyo. (AP) — Marvin Nash once worked as a rodeo clown, galvanizing crowds in ill-fitting overalls and hiding in a barrel when the bulls charged. The performer with the Texas drawl spent his youth mucking stalls on ranches and years later cutting operations costs for oil and gas firms.
And now he'll need everything he learned from inside the barrel, the oil patch and the ranch to woo the Wyoming crowd. His next act is turning oilfield wastewater into something ranchers can use to irrigate their crops.
Wyoming's oil and gas companies produced 3.6 billion barrels of water from 2015 to 2016. That water, laced with contaminants from production, was disposed of in myriad ways from injecting it back into the ground to filling disposal ponds.
Water is the lifeblood of the arid West and is often rendered useless after passing through the oil fields. Nash wants to change that.
Nash and his wife Darlene recently poured their life savings into building Encore Green, a mom and pop agriculture business that wants to play matchmaker between ranchers who need water and the oil and gas industry that produces quite a bit of it. Pending permits, the company will take 5,000 barrels of produced water in Laramie County, clean it, test it and use it to water a wheat field on state lands.
In the last year, Nash has pitched his idea before lawmakers and regulators, major oil and gas firms and local ranchers. He's garnered a loose knit group of curious onlookers and a few partners.
But what he's found is that technology isn't the hurdle for repurposing Wyoming's produced water, the Casper Star-Tribune reported . It's the culture that stands in the way, he said, the culture of operators who already have a system of dealing with water, farmers that want to protect their crops and various regulators that each have skin in the game.
Industry has noted that economics and technology are probably where they need to be for re-using water from the oil fields. It's regulations, some say, that need to catch up.
Nash's experiment is a testament to that hurdle. The pilot project required state and private permission as well as permits from four state agencies. And it hasn't proved easy to get them.
Tom Kropatsch, deputy oil and gas supervisor at the Wyoming Oil and Gas Conservation Commission, said the regulations in place today are up to the task if a venture like Nash's takes hold. And there is a permit waiting for Encore Green once the company details which oil and gas operator is going to provide them with the water, he said.
Still the decision made by the oil and gas industry for how they dispose of water is a complicated one, Kropatsch said.
"Liability, economics, regulatory, and other factors all overlap when an operator is considering how to properly manage their produced water," he said.
If produced water is being cleaned up for agriculture, who's responsible for it from one site to another?
"It just takes some agreements to figure that out," said Patrick Tyrrell, the state engineer for Wyoming.
Nash said he learned early on that this issue had to be viewed in light of industry's liability.
So, he's trying to change the way operators think about risk. He's had meetings with a number of players in the state, because cleaning water and handing it back to a rancher for irrigation may be less of a liability than the way companies are disposing of water today, he said.
If industry has a choice to clean water for re-use in agriculture, cheaper than trucking it to a disposal site or building an injection well to put water into an aquifer, everybody wins, Nash said.
Industry and government aren't the only ones that Nash needs on board for his company. He needs agriculture.
Owen Gertz has worked a piece of land outside of Cheyenne since he was a boy when the ranch belonged to his parents. The 67-year-old's three children run cattle on the ranch now. Gertz runs his own cattle and grows wheat on a patch of state leased land.
He's allowing Encore Green to do its pilot program on the wheat land.
Gertz said he doesn't see why produced water can't serve agriculture.
Jim Magagna, vice president of the Wyoming Stock Growers Association, said Nash's pitch has potential.
Many ranchers and farmers don't realize that this could be feasible, that industry water could be an asset to them, Magagna said.
The focus on produced water in Wyoming has been negative for some time, particularly after the coal bed methane boom in the Powder River Basin that posed a threat to ranchers.
___
Information from: Casper (Wyo.) Star-Tribune, http://www.trib.com