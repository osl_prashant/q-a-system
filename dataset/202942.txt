If you need to burn the lunch hour surfing the web, check out Google Trends for some gee whiz insights.
 
 
For example, when I typed "avocados" in the search box today on the Google Trends homepage (set for the past 90 days), it revealed the rising searches for avocados in recent weeks.
 
Google reported "related queries" included:
 
1. how to ripen avocados in the oven +300%
2. how to ripen avocados in oven +250%
3. avocados from Mexico +190%
4. are avocados good for dogs +180%
5. how to grow avocados from the seed +160%
6. how to tell if avocados are ripe +150%
7. how to keep avocados from turning brown +140%
8. where are avocados grown +140%
9. do avocados grow on trees +140%
10. fastest way to ripen avocados +140%
 
 
Google said "related topics" included:
 
1. Border- topic +350%
2. Chipotle Mexican Grill - restaurant company +300%
3. Boiled egg- egg +180%
4. Import - topic +180%
5. Shrimp and prawn as food- food +180%
6. Gut-topic +170%
7. Paper bag - topic +140%
8. Mexico - country +140%
9. Kale- vegetable +120%
10. Berry- fruit +120%
 
 
And yes, the web is really telling consumers they can ripen avocados in ten minutes.
 
 
And the nearly 3 million have searched for and viewed the Super Bowl avocado commerical.
 
 
A nifty feature of Google Trends is the ability to compare search terms. For example, you can compare organic produce with non-GMO and see the relative trend lines. 
 
 
So you see, the web doesn't merely sap productivity and waste time. In the hands of a professional, Google Trends can also be insightful market research.