New Mexico regulators OK massive wind farms near Texas
New Mexico regulators OK massive wind farms near Texas

By SUSAN MONTOYA BRYANAssociated Press
The Associated Press

ALBUQUERQUE, N.M.




ALBUQUERQUE, N.M. (AP) — New Mexico regulators on Wednesday approved a $1.6 billion plan that calls for building two massive wind farms along the Texas-New Mexico border.
The unanimous vote by the New Mexico Public Regulation Commission marks one of the key steps needed for Xcel Energy to move forward with its plans. Texas regulators are expected to act on the proposal in the coming weeks.
Xcel Energy says the proposed wind farms would take advantage of what has become the least expensive generating resource in the region to reduce fuel costs and ultimately save customers money on their monthly bills.
The project is part of major investment the company is making in wind energy across its entire service area. Xcel Energy plans to grow its wind portfolio by more than 50 percent by the end of 2021 by adding a dozen new wind farms in seven states. That would add nearly 3,700 megawatts of new wind capacity to its system.
David Hudson, president of the company's operations in New Mexico and Texas, called the vote in New Mexico historic.
"These wind facilities will power the regional economy with energy from our abundant, fuel-free wind resource and save customers hundreds of millions of dollars in energy costs for decades to come," he said in a statement.
Average monthly fuel savings could total about $2 for a typical residential customer beginning in 2021 once the turbines are spinning.
Wind turbines already dot the plains from central New Mexico to the Texas Panhandle, and Texas leads the nation by far when it comes to installed wind-power capacity.
Analysts with the U.S. Energy Information Administration predicted earlier this year that wind power is expected to surpass hydroelectric power as the largest renewable electricity generation source. That's based on weather patterns as well as the expectation of new wind farms coming online in 2018 and 2019.
Xcel's plans call for adding wind farms near Portales, New Mexico, and in Hale County, Texas, and purchasing more wind power from facilities owned by NextEra Energy Resources. Xcel estimates all three efforts would result in enough electricity to serve more than 440,000 homes.
The two wind farms also are expected to be a boon for state government finances. Aside from roughly 600 construction jobs and a few dozen permanent positions, Xcel Energy estimates the two farms could generate $154 million in additional tax revenue for state and local governments.