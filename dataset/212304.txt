Melon growers on the west side of California’s San Joaquin Valley will start harvesting a bit later this season than in 2016, but they anticipate their usual high quality with volume similar to last year.
In 2016, California cantaloupe growers shipped nearly 22.3 million 40-pound cartons, according to the Dinuba-based California Cantaloupe Advisory Board. That was up about 2 million cartons from the previous year.
Westside growers accounted for about 18 million cartons in 2016.
The state’s honeydew growers shipped about 9 million 30-pound cartons in 2016, up from 8.4 million cartons in 2015, according to the Dinuba-based California Melon Research Board.
Volume of traditional honeydew melons industrywide may be down this year as growers cut back on plantings because of poor prices the past two years, said Barry Zwillinger, partner in Legend Produce LLC, Dos Palos, Calif.
That could change in future seasons as growers plant more of the golden honeydew variety, which he said has a better flavor profile than existing varieties.
“The golden dew eats fantastic,” he said.
This year’s Westside crop will be “a little off” in terms of the timing growers became accustomed to during the recently ended drought, said Garrett Patricio, chief operating officer for Westside Produce Inc., Firebaugh, Calif.
During the drought, the region experienced warm weather during the spring, and the heat units were much higher than this year, he said.
This year, the Westside season probably will return to normal, he said, starting around the Fourth of July. During the past three years, start dates were as early as June 20.
“That will hopefully bode well for the end of the desert season,” Patricio said. “That will give them an opportunity to finish up and give us an opportunity to get started.”
Once the season gets started, supplies should be “ample,” he said.
“We’re hoping that there will be ample demand.”
Turlock Fruit Co. Inc., Turlock, Calif., should kick off its cantaloupe and honeydew programs July 1, with mixed melons starting between July 5 and 10, said Steve Smith, co-owner.
Growing conditions were excellent this spring, he said.
“It was a little cool in April, but May made up for it,” Smith said.
Planting conditions were ideal this spring, with “good rain moisture in the soil,” said Brian Wright, sales manager for Del Mar Farms, Westley, Calif.
He was hoping for a better market this year than last.
“Last year was definitely a humbling experience,” he said.
A supply glut drove down prices on cantaloupes and honeydews in 2016.
“We saw f.o.b. prices definitely less than the previous two seasons,” he said.
Competition from other parts of the country isn’t helping the California deal.
“With more and more local deals going on in the East, it seems like there are not as many people handling the (California) melon deal as there have been in the past,” Wright said.
John Meiser, field operations manager for Los Angeles-based Pacific Trellis Fruit/Dulcinea Farms, said in early June that the quality of the company’s new crop coming out of Yuma, Ariz., was delivering “average to above average yields, sizing is above average and quality is excellent.”
“Our (Westside) plantings are on schedule, and early plantings look to be ready to harvest in early July,” he said.
The firm’s Westside season will run through mid- to late September.
Patricio said it’s hard to beat melons from California’s Westside, where the dry, arid climate, warm days and slightly cooler nights make for “fantastic-tasting fruit in July, August and September.”
Consistent quality and supplies are typical of the deal, he said.
It was too early to predict sizing of this year’s fruit in early June, but Patricio said growers aim for size 9 cantaloupes and size 5 or 6 honeydews, since those are the sizes retailers prefer.