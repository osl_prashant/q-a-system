If you've ever wonderedwhat a cowboy would do if he won the lottery, Neal Wanless is living proof. Seven years ago the 23-year-old was mostly broke, living on a small acreage outside Winner, S.D. That's when he won South Dakota's $232 million Powerball.


He opted for a one-timecash payoff of $118 million, leaving him about $88 millionafter taxes. A few months later he bought 23 square miles of ranch land in a sparsely populated area of Butte County in western South Dakota.
Since then he's built an indoor riding arena, a stable and equipment shed. Oh, and a 2,572 square-foot home with a 1,768 square-foot garage.
Now, federal officials say the 41,000-acreWanless Ranch will become the home of 1,000 wild horses, relocated from the former Triple U buffalo ranch near Fort Pierre, now owned by Ted Turner. The horses are part of a federal program that cares for wild horses removed from western ranges.