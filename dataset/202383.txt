Hydroponic fresh produce growers who label their products as certified organic continue to operate in an anxious existence, and that’s a shame.
 
The National Organic Standards Board last fall tabled a decision on whether to let growers who don’t use soil remain certified as organic. The board wants to ban produce from organic certification if it’s grown using hydroponics, aeroponics or aquaponics.
 
The board said it would vote on a proposal at its April meeting and then take that recommendation to the U.S. Department of Agriculture.
 
But now it will be rewritten as a discussion document to allow for more time to study the issues. 
 
It should study them and find that hydroponics are suitable for organic certifications.
 
The board is caught up in soil and land issues that were the organic pioneers’ original motivation. 
 
But the board’s argument against hydroponic as organic in its discussion document reads like a wish-list of growers afraid of modern agriculture.
 
The organic consumer market has changed.
 
Consumers demand high-quality organic fresh fruits and vegetables in larger numbers every year.
 
A standards board that wants to keep organic production methods from evolving does growers, retailers and consumers a disservice.
 
We argued last fall that the standards board needs to grow up to deal with modern agriculture, and we’re optimistic that delays such as this will give it time for more realistic discussions and assessing whether the board’s makeup reflects the modern reality.
 
Did The Packer get it right? Leave a comment and tell us your opinion.