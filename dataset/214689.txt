A few days of dry weather in Iowa is helping farmers in the top corn growing state make up for lost time.  According to USDA's weekly crop progress report 85% of the state’s corn and 97% of soybeans are now out of the field. 
"The corn harvest is catching up, and is now only one week behind the five year average,” said Mike Naig, Iowa Deputy Secretary of Agriculture.
Overall 83% of the nation's corn crop is harvested, eight points behind the five year average. Two weeks ago corn harvest was 21 points behind. The upper Midwest continues to show the biggest delays. Seventy-nine percent of Minnesota's corn crop is harvested, 15 points off the average pace. Wisconsin is 20 points behind with just 56% out of the field.
Soybean harvest is close to wrapping up with 93% harvested, two points behind the 5 year average. Any delays in the Corn Belt can be measured in single digits.



Winter wheat planting is nearly complete with 95% seeded, and 84% emerged, one point ahead of average. Fifty-four percent of the winter wheat crop is rated good to excellent, 35% is fair.
Wes Mills contributed to this report.