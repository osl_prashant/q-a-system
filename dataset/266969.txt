The U.S. Department of Agriculture’s stingy forecast for domestic corn plantings could be one of the first signposts toward an increasingly bullish corn environment over the next year or so.
Industry analysts had predicted that 2018 U.S. corn plantings would fall slightly from year-ago levels to 89.42 million acres, so USDA’s peg of 88.026 million last Thursday offered a surprising jolt to the Chicago futures market.
At the same time, government forecasts for U.S. corn supply on Aug. 31 – when the 2017-18 marketing year concludes – have been falling. USDA’s latest estimate of 2.127 billion bushels would be down 7 percent from the previous year.
Playing around with these numbers a little bit, it does not take long to realize that a considerably price-friendly scenario could potentially start unfolding for the corn market in 2018.
First, Some Math
Given USDA’s latest 2017-18 corn ending stocks figure and the preliminary 2018-19 projections that the agency printed in February, inserting the new acreage number without changing anything else would cut 2018-19 U.S. corn ending stocks to 1.733 billion bushels.
This number is basically identical to the carryouts in both 2014-15 and 2015-16, but significantly smaller than last year’s 2.293 billion bushels.
In February, USDA forecasted 2018-19 year-end domestic corn supply at 2.272 billion bushels under the assumption of 90 million planted acres this spring and a more modest export program for the current marketing year.
But in March, USDA drastically increased 2017-18 corn use by 225 million bushels, which was mostly rooted in exports.
It is possible that given the more aggressive demand profile for the current year, USDA may have also been more aggressive with new-crop demand as well had that forecast been made in March.
This means 2018-19 ending stocks may drop below 1.733 billion bushels, and potentially as low as 1.5 billion, if new-crop demand were to increase by the same amount as old-crop demand did in March.
Production Nuances
The current weather pattern pretty much ensures that U.S. corn farmers in the core Midwestern production states will not get an early start to planting this year and in fact, they may start late relative to the last couple of years.
Although the calendar has flipped to spring, winter is still bearing down on the Corn Belt and will generally continue to do so through at least the first half of April. On average over the last three years, reported corn planting progress stood at 39 percent by the end of the month.
Growers have technology on their side, however, as it will allow them to plant quickly and efficiently as soon as the soil temperatures moderate and the weather turns more favorable. But if the cold weather hangs on too long, both corn plantings and ultimately year-end supply could shrink.
Even if U.S. corn farmers plant every last acre of USDA’s March target, yield could significantly swing the pendulum to favor either bears or bulls. But yield does not have to fall by much in order for the bull scenario to prevail.
Returning to the potential 2018-19 corn carryout of 1.5 billion bushels, simply replacing USDA’s yield of 174 bushels per acre with 172 bpa would lower that number to 1.35 billion bushels, some 40 percent smaller than in 2016-17.
A national yield of 172 bpa would be the third-largest on record and a year-end supply of 1.35 billion bushels would be the smallest since 2013/14 (1.232 billion bushels).
But this works in the reverse, too, and that has been the trend more often than not in recent years. If yield were to match last year’s high of 176.6 bpa, new-crop carryout jumps to 1.72 billion bushels from the potential 1.5 billion.
And 180 bpa – a very realistic possibility under stellar weather conditions – brings that number right back around 2 billion bushels.
Again, this is a highly simplified computation that has not yet accounted for any likely shift in demand based on eventual production. But this exercise demonstrates just how big a difference a few bushels in the yield can make on overall supply.
Road Blocks
There is a chance that corn plantings could increase from March intentions, however.
USDA’s early-March producer survey found that both corn and soybeans would lose ground in 2018 to crops such as spring wheat, cotton, and other small grains, based on higher relative prices. A material increase in corn prices in the immediate term could revert some growers back to corn, but this would also rely heavily on the weather improving by late April into May.
Current corn supply could also weaken the bulls’ case for the upcoming year. USDA surprised the market on Thursday by placing March 1, 2018, U.S. corn inventory at an all-time high of 8.888 billion bushels, some 2 percent higher than expectations.
This could mean that current-year carryout is in danger of rising in the coming months, and the next chance will come on April 10 when USDA updates its 2017-18 supply and demand forecasts.
That did not happen last year, though. March 1, 2017 corn stocks came in at an all-time record – higher than analysts had predicted – but USDA’s April carryout projection for 2016-17 was unchanged from March.
(The opinions expressed here are those of the author, a market analyst for Reuters)