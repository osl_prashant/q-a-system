Lawmakers OK 5 University of Tennessee board nominees
Lawmakers OK 5 University of Tennessee board nominees

The Associated Press

NASHVILLE, Tenn.




NASHVILLE, Tenn. (AP) — State lawmakers have approved five of Gov. Bill Haslam's nominees to serve on a newly configured University of Tennessee board of trustees.
The Senate voted Tuesday to agree with the House on the confirmation of former PepsiCo President John Compton; former Lady Vol and ESPN analyst Kara Lawson; River City Co. President and CEO Kim White; AutoZone CEO William Rhodes III; and former Tyson Foods CEO Donnie Smith.
Earlier this month, a Senate committee rejected four other Haslam nominees to the board, and one more withdrew.
Haslam had pushed for the law that reconfigures the board and shrinks it from 27 to 11 voting members.