<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


Three brothers are keeping a Texas tradition alive, planting and harvesting Pecos cantaloupes on their west Texas Farm.
 
Mandujano Brothers Produce, near Coyanosa, Texas, are harvesting the popular cantaloupe variety on more than 300 acres.