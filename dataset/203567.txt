Tropicals are hot, and the fall season is looking good in terms of volumes and a smooth transition between growing areas.
 

Mangoes
Mexico has had a strange season, said Greg Golden, partner and sales manager for Mullica, N.J.-based Amazon Produce Network, yet he has imported more Mexican mangoes than ever before and they have been good quality.

"The season started early with indications of good volume," Golden said. "Then we had a brutal gap in late March and April, followed by record volumes through the peak of the deal."
Volumes on keitt mangoes from northern Sinaloa are decreasing as the season winds down, and Amazon should be finished Sept. 15, with Golden looking forward to a smooth transition to Brazil without the gaps and shortages of recent years.
Boats of Brazilian fruit began arriving at U.S. ports in August and should continue until November, with the peak in mid- to late October.
The shift increases costs significantly, said Isabel Freeland, vice president of Coast Citrus Distributors Inc., San Diego.
Though her first two shipments of tommy atkins mangoes from Brazil had problems, she said the season will be strong.
"Brazilian fruit looks gorgeous. It's a perfect red and attractive to consumers," she said.
Golden said he's flying in a small amount of Brazilian ataulfo mangoes to ensure they ripen properly and arrive in good condition.
Gary Clevenger, a co-founder and managing member of Freska Produce International LLC, Oxnard, Calif., said he's shipping more of the bright yellow, thin-skinned mangoes every year from Mexico and Ecuador, which is expected to start in mid-September.
Golden said his main concern for the fall is having enough promotions in place.
"We forsee record volumes for a few weeks in late October or November," he said, "with Ecuador looking like it has a heavy crop and possibly piling up with the end of the Brazil season."
 

Pineapples

Pineapples from Costa Rica face a normal production gap from mid-August to mid-September, said Carlos Rodriguez, director of tropical sales for Procacci Bros. Sales Corp., Philadelphia.
"They have plenty of seven and eight count, sizes great for retail, but they can barely bring in enough big sizes, five- or six-count," Rodriguez said. "They're just picking whatever they can grab to fill orders."
Jose Rossignoli, general manager of the tropical category for Eden Prairie, Minn.-based Robinson Fresh, buys pineapples from Costa Rica and Mexico. He expects the market to begin improving, but says overall it's been a flat year for Costa Rica imports because of poor weather in 2015. The good news, he said, is that favourable weather this year should translate into a healthier 2017 in terms of volume and quality.
 

Papayas

Charlie Eagle, vice president business development for Southern Specialties Inc., Pompano Beach, Fla., expects volumes of golden papaya out of Brazil to pick up the second week of September.
"We'll have increased sizing with good production between sizes seven and eight," Eagle said.
Southern Specialties is also shipping large formosa papayas out of Guatemala, which he says are sweet and low in fiber with excellent flavor.
Homero Levy de Barros, president of HLB Specialties, a leading papaya importer based in Pompano Beach, Fla., said his large conventional and organic papayas are doing well but small golden or solo papayas from Brazil have suffered in the past year with too little rain and too much heat. 
The conventional papayas are from Guatemala, but the organic papayas are from Mexico.
"At the beginning of the year, a grower who used to send 100 pallets on their worst week sent three," said de Barros, who only got a third of the fruit he was hoping to promote during the Summer Olympics in Rio de Janeiro.
A 25% devaluation of Brazil's currency also affects growers, he said.
 

Limes

It's been an excellent year for Mexican limes from Veracruz, said Freeland, though when it rains growers can't harvest wet fruit so shippers have to look to Ecuador and Colombia.
Eagle said the main thrust of Southern Specialties' lime program is high-juice limes out of Guatemala and limes shipped directly from Mexico.
"It's been growing for us every year," he said.
Rossignoli said heat, rain, high humidity and tropical storms challenged Mexico's lime groves this summer, yet volumes have increased up to 4% compared to this time last year. After good demand all summer, with slightly higher prices than last season, he said he expected the lime market to return to normal after Labor Day.
"We anticipate a consistent, good quality crop in the fall," he said, adding that demand is increasing for organic limes and for 1-pound bags of limes.