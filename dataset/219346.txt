The following content is commentary and opinions expressed are solely those of the author.

The new tax reform bill has many implications for farmers and ranchers. Recently, U.S. Farm Report commentator John Phipps addressed ways in which tax reform could change charitable giving.

This week on Customer Support, viewer Roger Ousnamer of Holt, Michigan responds and shares his philosophy.

Watch Customer Support every weekend on U.S. Farm Report.