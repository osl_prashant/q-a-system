BC-US--Cotton, US
BC-US--Cotton, US

The Associated Press

New York




New York (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Thursday:
(50,000 lbs.; cents per lb.)
COTTON NO.2Open    High    Low    Settle     Chg.May       80.74   82.20   80.68   81.46  Up    .72Jul       81.18   82.51   81.08   81.80  Up    .62Aug                               77.73  Up    .74Oct                               79.23  Down  .08Oct                               77.73  Up    .74Dec       76.95   77.78   76.90   77.73  Up    .74Dec                               77.82  Up    .59Mar       77.22   77.93   77.13   77.82  Up    .59May       77.43   78.03   77.43   77.93  Up    .53Jul       77.41   77.93   77.41   77.89  Up    .48Aug                               72.96  Down  .05Oct                               74.92  Up    .14Oct                               72.96  Down  .05Dec       73.00   73.10   72.93   72.96  Down  .05Dec                               73.06  Down  .04Mar                               73.06  Down  .04May                               73.61  Down  .04Jul                               73.69  Down  .04Aug                               72.09  Down  .04Oct                               73.26  Down  .04Oct                               72.09  Down  .04Dec                               72.09  Down  .04