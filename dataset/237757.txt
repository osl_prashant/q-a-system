AP-KS--Kansas News Digest 5 pm, KS
AP-KS--Kansas News Digest 5 pm, KS

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Kansas.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
NEW & UPDATED:
New APNN — POLICE SHOOTING-WAMEGO, DEPUTY CHARGED
AROUND THE STATE:
COLLEGE PAPER CUTS
WICHITA, Kan. — The editor of the student newspaper at Wichita State University fears that it's funding is at risk of being cut in half next year because of aggressive coverage. Chance Swaim, editor-in-chief of The Sunflower, raised concerns after a student fees committee recommended slashing the student paper's budget Friday from $105,000 to $55,000. He said it's unlikely the paper would be able to make up such a large cut with advertising revenue and would have to trim positions. SENT: 390 words.
MIDTERMS-PELOSI
MT. LEBANON, Pa. — The Republican ads have been unsparing: Conor Lamb would be just one more congressional sheep — pun intended — behind that San Francisco liberal, Nancy Pelosi.  Now Lamb, the 33-year-old former Marine trying to pull an upset in a southwest Pennsylvania special election, is fighting back. Ahead of his March 13 matchup with Republican state lawmaker Rick Saccone, Lamb is disavowing Pelosi and making clear he'll go his own way. At least one other Democratic congressional candidate, Paul Davis in the 2nd Congressional District of Kansas, has disavowed Pelosi. Davis, like Lamb, is trying to persuade the eastern Kansas district to shift from its GOP leanings after Republican Rep. Lynn Jenkins opted against a re-election bid. By Bill Barrow. SENT: 982 words. AP Photos NYSB231, NYSB232.
IN BRIEF:
— POLICE SHOOTING-WAMEGO — Authorities have identified a 25-year-old man who was killed during gun battle with law enforcement officers in Wamego.
— DEPUTY CHARGED — A former western Kansas sheriff's detective is charged with three felonies after an investigation into the theft of department funds.
— RAILROAD BRIDGE DEATH — Authorities say a train has struck and killed one person on a railroad bridge near Topeka and that a woman was injured when she jumped to get out of the way.
— CAR STRUCK-DRIVER KILLED — Reno County authorities say a man died when he was hit by a pickup after his car got stuck in a ditch.
— HUMAN TRAFFICKING-CHARGES DROPPED  — Charges have been dropped against a Lawrence man who was jailed for 1.5 years in a sex crimes case.
— RAILROAD BRIDGE DEATH — Authorities say a train has struck and killed one person and critically injured another on a northeast Kansas railroad bridge.
SPORTS:
Nothing scheduled at this hour.
___If you have stories of regional or statewide interest, please email them to apkansascity@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Missouri and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.