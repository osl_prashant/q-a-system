Do you get frustrated with activists who go undercover on farms, ranches and at packing plants to "expose" what they deem to be unwarranted and abusive behavior on the part of the workers there?You're not alone.
In Australia, Deputy Prime Minister Barnaby Joyce and the New South Wales state government are trying to revoke the charitable status of animal rights groups whose members trespass on farms in attempts to expose animal mistreatment, according to a lengthy report in The Guardian-Australia.
Both federal and NSW government officials have been meeting with representatives of the animal agriculture industry to discuss the "farm trespass issue" and what steps can be taken to counter such activity.
Documents obtained by Greens Party Member of Parliament Mehreen Faruqi under NSW's freedom of information regulations revealed what the newspaper called "hostility to these animal rights groups and the federal and state government's apparent support of farming groups to fight them."
So far, I like what I'm reading.
That's because the "hostility" arises from the deception and subterfuge that activists use to paint an unrealistic portrait of animal handling. Whenever an abusive incident is captured on video, it's showcased to media outlets that are generally clueless about animal husbandry and livestock handling, and presented as representative of all of animal agriculture.
Faruqi told the media that the documents showed that NSW state and federal government officials were "hell bent on gagging any debate on animal welfare."
Good. As the story related, the documents "suggested that the joint governmental working group sees little legitimacy in the role of animal welfare and protest groups who obtain footage of wrongdoing and mistreatment of animals on farms."
And that's the key word: legitimacy. As has been exposed dozens of times through interviews, books, news conferences and speeches to their followers, the animal rights activists committed to such tactics as fraudulently getting hired for the purpose of capturing exculpatory video footage have zero interest in reforming livestock production. Their goal is public demonization to the extent that individual companies and eventually large swaths of the industry are forced out of the business.
That's not to suggest that there aren't some operators out there who are indeed substandard" in terms of their adherence to best practices for animal well-being. But they are a distinct minority, and there are far better ways to circumscribe their businesses.
A counterattack game plan
Here are some of the tactics that have been discussed for "cracking down" on animal rights activists:
¬? Revoking tax benefits for animal welfare groups incorporated as tax-exempt charities if they support activists engaged in trespassing.
¬? Reviewing "the adequacy of penalties" for trespassing, including increasing the statute of limitations for such offenses.
¬? Encouraging farmers to initiate in legal action against intentional trespassing, and providing information to launch civil lawsuits.
¬? Making it easier to prosecute animal rights activists by altering how evidence can be gathered under the laws regulating covert listening devices.
¬? Having police train farmers in best-practice surveillance methods to monitor activists.
Although MP Faruqi said these proposed initiatives represent "a shocking attack on groups that have "helped expose horrific animal abuse in the live export trade, greyhound racing and other commercial industries," government officials are standing strong.
"Animal cruelty is against the law and the Coalition [federal] government does not condone animal cruelty," she told The Guardian. "Evidence of acts of animal cruelty should be provided to the relevant enforcement authority for proper investigation.
"Animal welfare organizations that claim charitable tax status should be able to demonstrate the funds donated to them go to support bona fide charitable activities."
Deputy PM Joyce added that "groups who willfully break the law should be prosecuted," according to the minutes of the governmental working group meetings obtained by the newspaper.
"As minister," he said in a statement, "I am concerned to ensure that the voices of producers and industry are heard in the court of public opinion, to balance the 'moral high ground' so often captured unfairly by the activists in some sections of the community if industry remains silent."
I like to think I'd have phrased it a bit more elegantly, but I couldn't agree more with the PM's sentiments.
Just wish he was in office here in The States. 
The opinions expressed in this commentary are those of Dan Murphy, a veteran journalist and columnist.