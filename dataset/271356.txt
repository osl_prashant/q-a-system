AP-WA--Washington News Advisory, WA
AP-WA--Washington News Advisory, WA

The Associated Press



Our news coverage plans. If you have questions or suggestions about the report, please call the Seattle bureau at 206-682-1812 or 1-800-552-7694.
Washington at 2:40 p.m.
SHELTER DOG TURNED K-9
KENNEWICK, Wash. - Criminals in the Tri-Cities will have a harder time trying to hide their drugs with Sawyer on duty. Sawyer is the Benton County Sheriff's Office's new drug dog, and he's already proven to be effective. A golden lab/Shar-Pei mix, Sawyer is an 18-month-old rescue dog from a shelter in Oregon. An AP Member Exchange by Annie Fowler, Tri-City Herald. 600 words.
DOMESTIC SHEEP
WALLA WALLA, Wash. - Wild bighorn sheep and their domestic cousins have an unfortunate relationship. Both can be infected by a bacteria that can trigger pneumonia. But while the pathogen leads to mild sickness in the domestic animals, it can be lethal to bighorn sheep. According to conservationists, the bacteria are one of the biggest threats to the wild sheep, which can come into contact with domestic flocks as both graze on rural lands. But a pilot project at Washington State Penitentiary hopes to help efforts to protect bighorns from the pathogen. An AP Member Exchange by Andy Porter, Walla Walla Union-Bulletin. 530 words.
SPORTS
BBC-COLLEGE BASEBALL OHTANIS
The emergence of Los Angeles Angels sensation Shohei Ohtani has Louisville coach Dan McDonnell saying the time has come for collegians excelling as pitchers and hitters to get more opportunities to do both in professional baseball. McDonnell coached Brendan McKay. The Tampa Bay Rays are developing the rookie as a two-way player this season. McDonnell says two-way players who came along before McKay deserved the same chance. By Eric Olson. SENT: 1135 words. With AP photos.
BBA-MARINERS-RANGERS
ARLINGTON, Texas — Nelson Cruz hit his 100th homer in Texas, Robinson Cano added a tiebreaking shot and the Seattle Mariners beat the Rangers 9-7. Cano's two-run homer in the seventh was part of a four-run outburst off Alex Claudio, who didn't record an out. Seattle's comeback Saturday night spoiled 44-year-old Bartolo Colon's bid to get his first win for the Rangers. The start of the game was delayed 1 hour and 25 minutes because of rain. By Stephen Hawkins. SENT: 792 words.
FBN-DRAFT-SEAHAWKS CAPSULE
Seattle Seahawks draft capsule. SENT: 270 words.
IN BRIEF
FATAL SHOOTING: Officers investigating deadly Seattle neighborhood shooting
GUN BALLOT INITIAITVE: Ballot measure would raise age to buy semi-automatic weapons