Well, I'll admit it. I'm now a 4-H parent. My oldest turning nine this year means we are officially a 4-H family. It's not new to me, I come from a long line of 4-Hers.
As part of our first official year, we took two pigs to the county fair which means this self-described cattle guy is now learning the intricacies of swine.
Regardless of the ribbon count, as a parent, I can't help but feel like we won big. Our 9-year-old is learning to care for something other than himself.
He's finding out what responsibility means, that animals need to be fed even when you're tired or it's dark and nothing good can happen without a little hard work. I see these small but noticeable traits starting to percolate to the surface.
As a parent I know it's my job to teach my kids the value of hard work, respect, and responsibility.
A couple of recent studies confirm the impact of those lessons. One from the University of Minnesota found that the best predictor of a young adult's success in their mid-20's was whether they participated in chores around the home at an early age. A Harvard study found pretty much the same thing.
I'm just thankful the tools my parent and grandparents used to teach these life skills are still available.
With so many options for kids these days, it's easy to see something like 4-H get dropped, buried or pushed to a lower rung on the ladder of importance.
I challenge you, no matter what it is, that you find a way to teach responsibility, hard work and find time in your schedule for them to grow in character.
You might just learn something about yourself along the way...even if it's learning to love pigs.
 


Click here to learn more about the Million Dollar Wildfire Relief Challenge, and see how you can help in the rebuilding effort.