Georgia blueberries suffered some damage as temperatures dipped below freezing the week of March 5.
Mario Flores, director of blueberry product management for Salinas, Calif.-based Naturipe, said growers faced temperatures as low as 28 degrees during the week of March 5. The cold persisted four to five hours on some nights.
“There was damage to the Georgia production,” Flores said. “However, a sizable portion of our highbush blueberry Georgia growers with frost protection had minimal to no damage. Those growers with no frost protection are still assessing the situation.”
Frost protection measures for blueberries include permanent overhead field irrigation as well as hoop tunnels, Flores said.
Renee Allen, area blueberry agent for University of Georgia Extension, said the research farm in Bacon County took frost protection measures the nights of March 7-9 and on the night of March 12.
Will Lovett, the extension coordinator for Bacon County, said highbush blueberries without frost protection suffered damage. Rabbiteye blueberries in bloom might also be affected.
It is hard to determine at this point how much damage to blooms will translate into actual crop loss, Lovett said.
Watsonville-based California Giant Berry Farms reported that its growers in Georgia remain in good shape. The cold could push the start date of the crop a couple of days, but nothing significant.
Lovett said that growers with whom he spoke told him that it is unusual to have back-to-back years with late-season freezes.
A freeze cost Georgia much of its 2017 blueberry crop, after temperatures were as low as 21 degrees for three straight nights.
Despite the recent cold, Naturipe looks forward to a good crop for 2018.
“Overall we have a positive outlook for volume and quality this season,” Flores said. “Though we will see some reductions in the Georgia blueberry crop, we were starting out with a forecasted record volume that may now be down to what would have been considered normal volumes a few years ago.”
Additionally, production in Georgia will be supplemented by fruit from Florida, North Carolina and California.