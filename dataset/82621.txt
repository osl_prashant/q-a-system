A March 30 auction saw 770 acres in Dawson Co., Neb., sell for $1,300 an acre. The property, located 15 miles southwest of Oconto, was offered in two tracts. Each tract sold for $1,300 an acre.
 
Tract 1: 371 acres with 261 acres in hardland grass pasture; 54 acres irrigated; 44 acres dryland cropland; older home and building site.
Tract 2: 399 acres in hardland grass pasture with three dams and two watering tanks.