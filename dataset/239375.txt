Most producers are very well aware that distillers grains are a very economical, concentrated source of protein and energy. Distillers grains are also a good source of phosphorus and can often times eliminate the need for supplemental phosphorus in the mineral. 

While meeting the phosphorus demands of cattle is important, it is also important to make sure the calcium:phosphorus ratio (Ca:P) is correct. The recommended ratio of Ca:P is typically 1.5:1 to 2:1 with no less than a 1:1 ratio. If this ratio becomes inverted, cattle can experience urinary calculi which most cattle producers refer to as “water belly”. A blockage develops in the urinary tract preventing the animal from voiding urine.

When phosphorus is overfed, the risk of cattle developing this condition increases. Therefore, removing additional phosphorus from the mineral package and/or adding calcium may be necessary when distillers grains are fed as a protein or energy source.

Secondary to animal health is that adding phosphorus in the mineral package increases the cost. If the requirement is being met by the distillers grains then the cost margin is being increased unnecessarily.

A late gestation 1200 lb cow requires .26% Ca and .16% P on a dry matter basis in her diet. If this cow is receiving about 26 lb of low quality hay and 1 lb of dried distillers grains supplement on an as fed basis, not only would her protein and energy needs be met but Ca and P would be supplied at .26% and .2% of the diet respectively. This would supply a 1.3:1 ratio of Ca:P so supplemental phosphorus would not be needed in the mineral.

A 600 lb growing steer gaining 1.5-2 lb/d would require .4% Ca and .2% P on a dry matter basis. If the steer was eating 9 lb of poor quality hay, 20 lb of corn silage, and 2.5 lb of dried distillers on an as fed basis, then the calf would have .29% Ca and .36% P in the diet on a dry matter basis. This ratio is not quite 1:1 and so a producer might want to purchase a mineral with no added phosphorus but that did have added calcium.

Distillers grains are also high in sulfur. Sulfur can inhibit copper absorption when fed in high concentrations. Therefore producers feeding distillers as an energy source (for example greater than 30% of the dry matter) should consider feeding a mineral with increased copper to enhance copper availability.

Many mineral companies have mineral packages designed to be fed with distillers grains based diets which have added calcium, low or no added phosphorus, and increased copper availability.

Providing the right type of mineral with diets containing distillers can alleviate potential health problems and often times be more cost effective as well. University of Nebraska extension personnel are happy to help producers evaluate rations.