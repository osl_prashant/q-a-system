Silage bunkers and piles create a host of hazards that could lead to injury or death.
When you are filling the bunker or pile, no one else should be walking where equipment is operating. There should be signs and barriers to warn people not to be in the vicinity.
"When you're delivering forage in the truck, don't drive on the forage that's been recently placed in the storage for fear of tipping the truck over, especially if it's a dump truck," said Brian Holmes, emeritus professor at the University of Wisconsin. "Leave the truck on a hard surface, where it's more stable."
The slope should be kept at less than a 3-1 ratio, 3 horizontal to 1 vertical, so tractors aren't as likely to tip over, either sideways or backward. An experienced operator should be pushing and packing. At the top of the wall, the slope should be extremely low, so as not to be sliding towards the wall or tipping over and falling over the wall. Tractors should have rollover protection; and don't forget to wear your seat belt.
Covering Bunkers and Piles
Once the silo is full, avoid working too close to the wall, or at least face the wall when working in that vicinity, so you know where the wall is at all times.
"The sides shouldn't be so steep that people are going to be sliding off the plastic. Lots of times we see people covering piles or even bunkers with tires strung together with string. They do that because it's so steep that the tires won't stay in place," he said. "If that's the case, then people are going to have a hard time staying in place as well."
Emptying Bunkers and Piles
When it comes to removing silage, there are lots of hazards.

Do not approach the unloading equipment without the operator knowing where you are.
If using a defacer with rotating teeth, steer clear of the teeth.
As with any piece of equipment, stop the engine before exiting the cab.
Stay at least three times the height of the bunker, or pile away from the face, when working on the floor.
Design the face to be no higher than the height the unloading equipment can reach.
Maintain a vertical face all the time.
Don't overfill storage or create overhangs.
Add a bar screen over the tractor windows, so that, if there is an avalanche, it won't come crashing through the window and trap the operator inside the cab.

Sampling Silage Safely
"In the past, samples have been obtain[ed] by going up to the silo face and removing silage or picking up silage that has been knocked onto the floor," he said. "That's a prime hazard zone for an avalanche to crush you. Remove the silage and pick it up with a front end loader and back the loader far enough away the avalanche fall zone (three times the height). Turn off the equipment and take your sample from the bucket."
If a producer is on top of the silage bunker or pile, they run the risk of riding that avalanche down some distance, which can cause serious injury.
"If you're on the ground and it falls on top of you, there's the risk of being asphyxiated by having this weighted material on top of you," he said. "These scenarios can result in death, so be careful not to work in the avalanche zone."