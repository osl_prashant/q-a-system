Gregg Doud has won Senate approval almost nine months after being nominated by President Trump to serve as the Chief Agricultural Negotiator.
Doud’s confirmation was placed on hold by Sen. Jeff Flake, R-Ariz., in response to a Trump administration proposal in North American Free Trade Agreement negotiations. That proposal, supported by Southeast U.S. growers and opposed by western U.S. growers, would make it easier for growers of seasonal crops to make claims of dumping of imported products in the U.S.
Flake removed the hold on Doud’s confirmation Feb. 1 after receiving commitments on trade priorities from the U.S. Trade Representative’s Office.
The U.S. Senate on March 1 approved Doud to serve as the Chief Agricultural Negotiator in the Office of the United States Trade Representative.
Senator Pat Roberts, R-Kan., said he is pleased with Doud’s nomination.
“Our hard-working farmers, ranchers, end-users, and folks in rural America have waited too long to be represented at the trade negotiating table,” Roberts said.
Agriculture Secretary Sonny Perdue also voiced support following the nomination in a tweet.
“Farmers, ranchers, foresters and producers need strong representation at the negotiating table and he understands what trade means to ag,” Perdue wrote.
Doud was nominated to the post by President Trump on June 16, 2017, and a hearing before the Senate Finance Committee was Oct. 5. Following that meeting, Flake placed the hold on his nomination.
Doud is a native of Kansas and grew up on a farm. Since 2013, he has served as president for the Commodity Markets Council. Prior to this he worked on Senator Roberts’ staff for two years on the Senate Agriculture Committee.