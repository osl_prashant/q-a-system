Avocados from Mexico announced it will help mark Canada’s sesquicentennial as a nation with a Cinco de Mayo celebration, in collaboration with the Mexican embassy May 5-7 at the Horticulture Building in Ottawa. 
The Mexican embassy, the Mexican Tourism Board and ProMexico are developing a three-day “Experience Mexico” event to celebrate the two nations’ relationship. 
 
The event, open to public, will be designed to emphasize Mexico tourism, education/culture, products/services, gastronomy, music and folklore. 
 
Avocados From Mexico will share avocado cooking tips and recipes with attendees.  The organization also will celebrate Cinco de Mayo with a co-branded promotion featured in Metro supermarkets. By buying any of the four participating brands, while using their Metro loyalty card, consumers will enter for a chance to win a trip for two to Mexico, Avocados From Mexico said in a news release. 
 
In-store tastings and social media communications will support and promote the contest, according to the release.