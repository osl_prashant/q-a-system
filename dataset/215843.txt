The first commercial season of New York- and Washington-grown Koru apples is off to a good start. 
Jim Allen, vice president of marketing for Glenmont-based New York Apple Sales, which handles New York-grown Korus, said in a news release that the state’s early harvest allowed apples to enter the market in October, minimizing gaps with the New Zealand deal. 
Wenatchee, Wash.-based Oneonta Starr Ranch Growers and Chelan, Wash.-based Chelan Fresh, which market Washington Korus, started harvesting in early November, according to the release. 
Bruce Turner, Oneonta national marketing representative, said the fruit is peaking at size 80-88 and is available in 27-pound euro cartons and 2-pound pouch bags, according to the release.
Consumer feedback so far has been positive, according to the companies.
During New Zealand’s May through September season, more than 11,000 stores in the U.S. and Canada carried Koru, a natural New Zealand cross of fuji and braeburn, according to the release. 
Increased production in the U.S. and New Zealand should ensure year-round availability in 2019.
This is the first year Chelan Fresh has marketed the variety, director of marketing Mac Riggan said in the release.
“We’re excited to be part of Koru family and to work with these other two great companies,” he said.