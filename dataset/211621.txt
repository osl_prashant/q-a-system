Japan is lifting it 11-year old ban on Idaho fresh chipping potatoes. 
Access was revoked in 2006 soon after the Japanese market opened for the first time for U.S. potatoes when the pale potato cyst nematode was found in Idaho.
 
Japantoday.com reported Sept. 13 that the government of Japan opened the doors for fresh imports from Idaho for the first time since 2006, with Japanese officials citing reduced pest risks associated with the pale potato cyst nematode.
 
Japan suspended all U.S. potato imports after the pale potato cyst nematode was detected in Idaho. Later, Japan reopened the market to U.S. potatoes except those grown in Idaho.
 
“It is great news. This is something we have been working on for 11 years,” said Frank Muir, president and CEO of the Idaho Potato Commission. 
 
When potato cyst nematode was found in Idaho, several countries shut their borders to Idaho potato exports, Muir said. With the reopening of the Japanese market, he said the state’s aggressive testing and eradication process has reopened all markets to date except Mexico, which requires testing for the pest before export. 
 
“We believe that can be resolved as well, so that will be the last PCN issue that we have to deal with in terms of our international partners,” Muir said.
Muir said Idaho has extensive testing for PCN and only two counties — Bonneville and Bingham — still have the pest. “We have tested 400,000 to 500,000 samples so we know where PCN is not,” he said.
 
In a news release, Muir also thanked the Idaho Congressional delegation, notably Sen. Jim Risch, R-Idaho. Muir said in the release that Risch met with the Japanese ambassador several times about the issue. 
 
The Idaho Potato Commission appreciates the work of the U.S. Department of Agriculture’s Animal Plant Health and Inspection Service, other industry organizations and the Idaho State Department of Agriculture, who assisted in reopening the market, Muir said.
 
Consumer demand for chips has been expanding among Japanese consumers, Japantoday.com reported, and there have been shortages of Japanese chipping potatoes.
 
The USDA said U.S. fresh potato exports to Japan in 2016 totaled more than 41,000 metric tons, or about 8% of total U.S. fresh potato exports. At 41,000 metric tons, U.S. potato shipments to Japan in 2016 are up from 17,000 metric tons in 2013 and 1,500 metric tons in 2007.