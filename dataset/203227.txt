<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


LONG BEACH, Calif. - Anaheim, Calif.-based Northgate Gonzales Markets and Chilean Blueberries are teaming up for the next few weeks to teach kids how to eat healthy with blueberries.
A Jan. 14 event at a Long Beach Northgate location was the first of 40 free kids' cooking classes hosted by Northgate's Miguelito's Cocina Club that will feature Chilean blueberries, Dannon yogurt and General Mills cereal, said Jhosimar Rivera, Northgate's customer marketing manager.
Different classes will be held throughout the year with a number of partners, rotating through the chain's 41 locations.
Youngsters who join the club receive a free apron, passport, button pin and certificate.
If they attend six classes this year, they also will get a free gift card.
Participants in the blueberry class also received a kids' blueberry activity game and recipe sheets, said Steve Hattendorf, Western region merchandiser for the Santa Cruz, Calif.-based Chilean Fresh Fruit Association.
"Blueberries from Chile tie in great with Northgate's initiative because they are a heathy snack for kids, loaded with vitamin C and fiber, and they're fat-free and go great with kid-friendly recipes, like today's Blueberry Swirl," Hattendorf said.
The purpose of the club and the classes is to give kids a chance to have fun on a Saturday or Sunday morning, while they learn to make traditional or good-for-you recipes, like the Blueberry Swirl, Rivera said.
The chain hopes to tie in with partners promoting seasonal items throughout the year in a program from which suppliers, Northgate and the kids can benefit, Rivera said.
The classes provide produce suppliers and other partners with an opportunity to showcase their products and demonstrate new ways to use them, he added.
"It's a fun experience overall," he said.