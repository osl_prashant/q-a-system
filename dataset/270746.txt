Aronia growers and marketers note not the taste, but the “sensation,” of their berry, is what counts.They note a “puckering sensation in your mouth” that a berry creates when eaten fresh off the bush. It’s particularly noticeable with aronias that aren’t fully ripe.
Indeed, it also has been called the “chokeberry.”
The sensation, brought on by chemicals known as tannins, is a major selling point with most consumers and often is compared to the taste of a dry wine, according to Roberta Barham, president of the Atlantic, Iowa-based Midwest Aronia Association.
The pucker-up sensation from the purple-black, pea-sized berry, also serves as a reminder of a concentrated presence of antioxidants, she said.
“I think the greatest draw is its health and nutritional value,” Barham said. “The taste can be an acquired one or downright repugnant to some people, but when they learn that it has the highest antioxidant level of any fruit, many are swayed to use it, especially if a recipe is provided to assist with the acquisition — such as smoothies — milk offsets the tannins and astringency of the berry.”
Tom Paul, grower of aronia berries and owner of Cedar Gardens LLC, Clinton, Iowa, said aronias have the highest antioxidant value of all fruits and vegetables.
 

Rising interest

According to the Omaha, Neb.-based North American Aronia Cooperative, which in 2015 had 59 grower-members across the Midwest and Canada, increased public awareness of the nutritional weight of the berry has driven up consumer interest.
The co-op cites U.S. Department of Agriculture as saying laboratory tests have shown that “aronia berries are higher in antioxidants than blackberries, blueberries, cherries, cranberries, raspberries” and all other commercially grown fruits in the U.S.
“It’s easy to grow, has a high profit margin and is extremely healthy for human consumption,” Paul said.
Barham said her association provides information to retailers to facilitate aronia sales.
Point-of-sale materials include “tri-fold brochures with recipes to customers at retail stores, farmers markets and farm sales,” she said.
The association also visits stores, restaurants and other businesses to discuss the potential of selling aronia berries at their establishments, or developing products that include berries, Barham said.
“The Midwest Aronia Association has a website (midwestaronia.org) and Facebook page with aronia information regarding planting, harvesting, health and nutrition, etc.,” Barham said.
The association also is developing a video, as well, and is hunting for grant money to promote some marketing aspects of the fruit, Barham said.
Annual production figures are difficult to pin down, Barham said, but USA Today has quoted the cooperative as estimating a production capability of 20 million pounds, with an economic impact of $85 million.
Aronia berries often are an ingredient in hundreds of products, including wines, juices, even baby food.
 

Fresh potential

There is some fresh-market appeal, too, although how much not known, Barham said.
“This is a hard one to gauge, as the market has not grown enough, and I don’t know of any marketing studies — we need to get one — that have been done to analyze the potential, but I would say because of this fact — potential is huge. We just haven’t proven it yet,” she said.
There is work on developing aronias’ potential, said Jonathan Smith, president of Simply Incredible Foods, Menasha, Wis. He said his company is working with aronia growers in Iowa to develop markets for the berry.