AP-PA--Pennsylvania News Digest, PA
AP-PA--Pennsylvania News Digest, PA

The Associated Press



Good afternoon! Here's a look at AP's general news coverage today in Pennsylvania. For questions about the state report, contact the Philadelphia bureau at 215-561-1133. Ron Todt is on the desk. Editor Larry Rosenthal can be reached at 215-446-6631 or lrosenthal@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with updates.
TOP STORIES:
PENNSYLVANIA ELECTION
MT. LEBANON — A razor's edge separated Democrat Conor Lamb and Republican Rick Saccone early Wednesday in their closely watched special election in Pennsylvania, where a surprisingly strong bid by first-time candidate Lamb severely tested Donald Trump's sway in a GOP stronghold. By Bill Barrow, Marc Levy and Steve Peoples. SENT: About 1140 words.
With:
— PENNSYLVANIA ELECTION-MIDTERM FALLOUT — House Speaker Paul Ryan is privately warning Republicans of a "bit of a wake-up call" as Democrat Conor Lamb edged past the Republican in the Pennsylvania special election. By Lisa Mascaro.
— PENNSYLVANIA ELECTION-THE LATEST.
MED--GLOWING TUMORS
PHILADELPHIA — Surgery has long been the best way to cure cancer, and if the disease comes back, it's usually because stray tumor cells were left behind or other tumors lurked undetected. Now researchers are testing fluorescent dyes to make these hidden cells glow, exposing them to surgeons who can cut them out and give patients a better shot at survival. By Marilyn Marchione. SENT: About 1040 words, photos, video.
STUDENT WALKOUT-GUN VIOLENCE-SCENES
Students hoisted "Stand United" signs. They chanted "''Hey, hey, ho, ho - the NRA has got to go" outside the White House. Others read the names of the Marjory Stoneman Douglas High victims aloud in a somber tribute.
With:
— STUDENT WALKOUT-GUN VIOLENCE.
NORTHEAST STORM
BOSTON — Storm cleanup and the restoration of power to the tens of thousands of residents and businesses without electricity were the focus Wednesday, a day after a fierce nor'easter lashed the Northeast with hurricane-force winds and heavy snow. By Mark Pratt and Sarah Betancourt. SENT: About 470 words.
With:
— NORTHEAST STORM-THE LATEST.
BUSINESS:
SMALLBIZ-SMALL TALK-SLOWING HOME SALES
NEW YORK — A slowdown in home sales is creating opportunities for small businesses like home renovators and appliance dealers. Business is up at Brad Beldon's home exterior renovation company, which does roofing, windows, siding and other work in 15 markets across the country. Many of Beldon's customers are homeowners who aren't selling because they can't find other houses they like and can afford. By Joyce M. Rosenberg. SENT: About 1060 words.
EXCHANGE:
EXCHANGE-MUSEUM-LINDBERGH LETTER
BETHLEHEM — Lucky Lindy had more than luck when he made the first nonstop flight across the Atlantic Ocean a little more than 90 years ago. Famed aviator Charles Lindbergh had the force of Bethlehem Steel behind him — well, actually, in front of him.The single engine that powered Lindbergh's "Spirit of St. Louis" through 3,600 miles from New York to Paris included parts made at the company's hometown plant, a longtime source of Bethlehem pride. "If that engine failed, he was a dead man," said Robert van der Linden, curator of the Spirit of St. Louis exhibit at the Smithsonian National Air and Space Museum. "He bet his life on that engine." One of the Smithsonian's affiliates, the National Museum of Industrial History in Bethlehem, has now acquired a forgotten memento of Lindbergh's historic accomplishment — an original telegram congratulating Bethlehem Steel for its part in the making of that landmark flight. Nicole Radzievich, The (Allentown) Morning Call.
IN BRIEF:
CHURCH SHOOTING — A state appeals court has refused to throw out the 10- to 20-year jail term imposed on a man who shot and killed another churchgoer during a Sunday service in a Philadelphia suburb.
CONGRESSIONAL RACE-CORRUPTION — Federal authorities say a former political consultant to U.S. Rep. Bob Brady of Pennsylvania was the target of a murder-for-hire plot in connection with a corruption investigation in Arkansas and Missouri.
TEENS KILLED — Police say they have arrested the father of a teen killed outside of his home in Philadelphia for threatening a friend of his son's alleged killer.
PROFESSOR REMARKS-BLACK STUDENTS — University of Pennsylvania officials have barred a law school professor who said she has never seen a black student graduate in the top quarter of the class from teaching mandatory courses.
MAN CRUSHED BY CAR — Authorities say a man working on a vehicle at an auto shop was killed when it apparently fell on top of him.
ETHNIC INTIMIDATION-SENTENCING — A Pennsylvania man who yelled "go back to your own country" before punching a man of Middle Eastern descent and breaking his jaw has been sentenced to probation.
MARYLAND-PIPELINE PROTEST — Clean-water activists in Maryland say drinking water could be polluted by a planned pipeline that would carry natural gas under the Potomac River and C & O Canal.
SPORTS:
HKN--PENGUINS-RANGERS
NEW YORK— Mats Zuccarello and the New York Rangers look for their fifth win in eight games as they host the second-place Pittsburgh Penguins. New York is 4-2-1 in its last seven. Pittsburgh, winner of 10 of its last 14, trails Washington by one point for the Metropolitan Division lead.
___
If you have stories of regional or statewide interest, please email them to phillyap@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.