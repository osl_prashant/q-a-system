Grain mostly lower and livestock mixed
Grain mostly lower and livestock mixed

The Associated Press



Wheat for May rose 1.50 cents at 4.7675 a bushel; May corn was off 1 cent at 3.82 a bushel; May oats fell 1.50 cents at $2.3450 a bushel; while May soybeans lost 4.50 cents at $10.3725 a bushel.
Beef was lower and pork wase higher on the Chicago Mercantile Exchange. April live cattle was off 1.17 cents at $1.1780 a pound; April feeder cattle fell 1.53 cent at 1.3722 a pound; while May lean hogs rose .30 cent at $.7015 a pound.