Chicago Mercantile Exchange live cattle futures firmed on Tuesday, bouncing back from Monday’s declines.
Traders said good profit margins at packers supported the gains.
April live cattle closed 0.90 cent per pound higher at 118.05 cents. The contract hit its highest since March 21. June ended up 0.725 cent to 104.900 cents.
CME feeder cattle were mixed, with nearby contracts weakening while deferred offerings rose.
April dropped 0.50 cent per pound to 138.725 cents.
Hog futures followed a similar pattern as the feeder cattle.
May hogs closed down 0.275 cent per pound at 67.900 cents. Most actively traded June ended 0.050 cent lower at 76.750 cents while contracts from August onward rose.