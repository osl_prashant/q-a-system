One of my colleagues at The Packer, Ashley Nickle, had a fascinating story about potential "price wars" as Wal-Mart Stores Inc. doubles down on its "Every Day Low Price" promise, under pressure from discounters like Batavia, Ill.-based Aldi, and soon-to-be German cousin Lidl, this summer.

 

Wal-Mart is doing price comparisons in the Midwest and Southeast, reports say. For good reason, I think. Aldi and Lidl reportedly own 12% of the United Kingdom market share, and that number is growing. Aldi also hopes to have 2,000 stores by the end of next year.
 


 

Analysts even have Whole Foods under pressure from Aldi and Lidl, which have expanded their selections of natural and organic products over the past several years.

 

Who isn't a threat from these guys, then?
 


 

When I was a kid, my mom had an encyclopedic memory of prices from every store we frequented, which in central Missouri included Wal-Mart Supercenter, Aldi, Consumers and the local independent, Carl's Market. She could ask my dad how much money he had in his pocket, and come within a couple of dollars, cash.
 


 

I, however, don't have that good of a memory, so I called upon a friend in suburban St. Louis to go price check Aldi for me. We both went on Monday, March 7.
 


 

I took her information to my neighborhood Wal-Mart. I know there's a little bit of a regional difference here, like cilantro is never more than 39 cents in central Texas, and we'd die before paying $1.29 for a small avocado, but a lot of these items don't vary much from market to market.

 

Check out the chart for a sample of what we found, for comparable items, and visit produceretailer.com for the full comparison.

 



<!--

if("undefined"==typeof window.datawrapper)window.datawrapper={};window.datawrapper["5MfTb"]={},window.datawrapper["5MfTb"].embedDeltas={"100":774,"200":654,"300":640,"400":600,"500":600,"600":600,"700":586,"800":586,"900":586,"1000":586},window.datawrapper["5MfTb"].iframe=document.getElementById("datawrapper-chart-5MfTb"),window.datawrapper["5MfTb"].iframe.style.height=window.datawrapper["5MfTb"].embedDeltas[Math.min(1e3,Math.max(100*Math.floor(window.datawrapper["5MfTb"].iframe.offsetWidth/100),100))]+"px",window.addEventListener("message",function(a){if("undefined"!=typeof a.data["datawrapper-height"])for(var b in a.data["datawrapper-height"])if("5MfTb"==b)window.datawrapper["5MfTb"].iframe.style.height=a.data["datawrapper-height"][b]+"px"});

//-->


'Cool' Wal-Mart?
Analysts took a field trip to the reimagined Wal-Mart Stores Inc. Supercenters recently, and dubbed the new features (scan and go, touchscreens, improved lighting and three types of shopping carts)â€¦ "cool."

 

Can Wal-Mart, the epitome of un-cool (the article I'm reference even called it the "Oxyest of Oxymorons"), ever be "cool"?

 

Wal-Mart has a blog post explaining the new features, and they're promising.

 

I've noticed changes in produce lately.
 


 

For example, when I price checked versus Aldi, I saw a surprising amount of organic produce on the shelves.
 


 

There were things I didn't expect, like bell peppers and numerous varieties of apples. Previously, you'd see one, maybe two.
 


 

There were multiple organic options in carrots, too.

 

What was un-cool?

 

Someone at the Supercenter near my house found the pineapple corer again.
 


 


It wasn't hacked apart in random pieces this time, but the sizes are way inconsistent, and each is marked as being weighed and sold for $1.98/lb.
 


 

If you recall, I was pretty harsh about this store's efforts to do in-house cored and chunked pineapple.

 


 

It was a hot mess.

 

I'm happy to report that it looks a lot better than before, but I'm still doubtful that this is a good idea for a retailer like Wal-Mart to do in-store.

 

While the tubs had a price sticker across one side to appear sealed, they were still fantastically inconsistent.

 

They were sold for "$1.98/lb" with every tub marked as a pound, as if it was actually weighed and stickered in-store.
 


 

It was set up to be the same price as whole fruit, which was merchandised alongside the cored fruit for $1.98 each.

 

Every single tub I weighed was more than a pound, thankfully, and one was even a pound and a half.

 

Not cool.

 

Pamela Riemenschneider is editor of Produce Retailer magazine. E-mail her at pamelar@farmjournal.com.

 

What's your take? Leave a comment and tell us your opinion.