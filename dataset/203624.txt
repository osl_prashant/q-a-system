Increased use of pouches is among the packaging trends in the Michigan apple industry.
BelleHarvest Sales Inc., Belding, Mich., will make another big push this year on its secondary retail displays of apple pouches, said Chris Sandwick, the company's vice president of sales and marketing.
"We use pouches a little differently, emphasizing the secondary displays," Sandwick said.
BelleHarvest works with its retail partners to place its pedestal displays of 3-pound bags of galas and fujis and 2-pound bags of Honeycrisps in snack food aisles, at checkout and at other places in the store besides the produce department.
Displays feature the tag line, "A Better Crunch with Lunch." BelleHarvest wants its fruit to be in the same conversation with packaged foods, Sandwick said.
"Frito-Lay and M&M/Mars are our primary competition," he said.
In 2015, its first season doing the secondary display campaign, BelleHarvest was able to sign up a couple of its key retail partners, but Sandwick admits it can be a tough sell, with different retail department managers fighting for the same limited space.
"We had a good rollout last year. We had to really work with produce department managers to go to battle for (the displays). In those inter-departmental battles, no one wants to give up their floor space."
Damon Glei, president and owner of Glei's Inc., Hillsdale, Mich., said his company is taking the plunge on tote bag packaging this year.
"We've stayed out of tote bags, maybe for too long. We'll go into it cautiously this year."
The company plans to pack mcintosh, galas and fujis in white paper bags that will likely weigh about 4.5 pounds, Glei said. The company's logo and name will be printed on one side of the bags.
The tote, Glei said, is in some ways a compromise between two other common forms of packaging for Michigan apples.
"Michigan has always been heavy to poly, but we've also done a lot more trays in recent years. Totes are kind of somewhere in between. We'll see how it goes."
Don Armock, president of Riveridge Produce Marketing Inc., Sparta, Mich., said his company expects its optimum mix of packaged and bulk apples this season, with about 40% of product going into bags, the rest to bulk.
The variety of packaging for Michigan apples keeps increasing, Armock said.
"Poly, pouch, mesh - it's just constantly changing," he said.
Even master cases, he said, are filled with a proliferating variety of pack styles.
Reacting quickly to customer demand is of course important, Armock said, but so is anticipation of trends.
"It's important to be flexible, to try to stay ahead of what the customer needs."