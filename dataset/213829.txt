Giro Pack features its equipment in a video filmed at Fowler Packing.
Giro Pack Inc. has a new box filling machine that automates the last step in the packing process, filling boxes with net bags.
Another machine, the Exact-100, can weigh items at high speeds, streamlining the process with the capability to feed three baggers at once with the same footprint as most layouts with two baggers, according to a news release.
The box filler, the GBF-100 is versatile and accommodates an array of packs and weights for different commodities, according to the release.
The machines were featured at the Produce Marketing Association’s Fresh Summit expo on Oct. 21-22 in New Orleans, with a demonstrations of the box filler, and video of the Exact-100 in use at Fowler Packing.
Specifications of the Exact-100 include:

Capacity of up to 400 pounds of fruits per minute;
Output of up to 90 bags a minute, depending on bag size and commodity; and
Five grams per dump more accurate the company’s previous weigher.

The GBF-100 specifications include:

Up to 90 bags a minute, depending on bag size and commodity;
Rejection system for overweight and underweight boxes;
Includes a vibrating/compacting device to position bags in the box; and
Can pack in plastic or cartons.