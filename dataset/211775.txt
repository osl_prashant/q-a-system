MILWAUKIE, Ore. — Pear Bureau Northwest has its eyes on meal kits and virtual aisles as it looks to keep pears visible amid changing shopping and meal prep practices.The online format is finding favor with a growing number of consumers, particularly younger shoppers.
“We’re working with plans to ensure that we’re on those websites,” said Kevin Moffitt, Pear Bureau Northwest CEO.
Being present is only part of the battle, however, for a fruit that is often an impulse purchase.
“Consumers are not browsing online,” Moffitt said. “They’re taking their list from the last time, or they developed the list the first time and that’s the list they work from, and then they might look at what’s being offered as a special or a recipe section.”
Figuring out how to make pears a part of that equation is something the bureau is working on.
It sees meal kits, also gaining traction, as another possible avenue to reaching consumers.
“We not only want to work with the meal kit companies that are out there, but more retailers are realizing those (companies) are pulling away from their sales, and they’re putting together their own meal kits,” Moffitt said. “Several retailers have their own in-house meal kit programs, and I think that’s an area that’s going to really continue to move forward.
“Retailers have an edge in a lot of ways,” Moffitt said. “Several of the big ones are going that direction.”
Along with targeting meal kits and consumers shopping online, the bureau aims to work with stores to increase pear exposure from all angles, including retail dietitian programs and digital marketing.
“Because those departments aren’t always working in concert, we feel like we can bridge some of those gaps and make sure that we’re making a consistent promotion across all platforms and in stores.” said Julia Smith, marketing communications manager for the bureau.
“The idea is that we can bring all of that together so that it’s an experience for the consumer where they see it on an ad, they read about it on the website or on social media, then they see associated signs in the store or a demo,” Smith said.
The bureau has done a significant amount of pear sampling over the years, but having ripe fruit to offer remains key.
“It can be big when the fruit is ripe,” Moffitt said. “We have a problem controlling that all the way through. It’s the most effective way to increase sales, but it’s the hardest for us on a national scale with big retail chains to control and get some really good ripe fruit in there into that consumer’s hand.”
The bureau has been pushing retailers to offer ripe fruit for more than a decade. Moffitt said the organization has worked with about 45 retailers that do, but adoption is slowing.
Each year the bureau targets 8-10 more, but typically only one or two convert.
Moffitt stresses that in an instant-gratification culture, shoppers are looking to be able to have their pears soon after purchase.
According to consumer research by the bureau, 70% of people want to eat their fruit within one to three days of buying it.