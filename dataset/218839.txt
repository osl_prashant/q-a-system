HOLLYWOOD, Fla. — Organic produce isn’t a segment of the business with which to merely flirt.
It often takes a big commitment.
Most speakers, attendees, exhibitors and buyers at The Packer’s inaugural Global Organic Produce Expo Jan. 25-27 said they were there to go deeper into the organic produce market. And likewise, The Packer wants to go deeper into providing the information and opportunities for buyers and sellers to grow in the organic market. 
Our event partner, the Organic Trade Association, helped provide the broader perspective on organic in the fresh produce industry. 
Maybe even more than the conventional produce industry, getting to know business partners is critical.
“You have to develop the relationships,” said Monique Marez, director of international trade for OTA. “You’re not going to pick up a good deal online through a trader and know that you’re actually getting what you paid for.” 
Among the more than 150 buyer attendees were the largest of retailers in Walmart and Kroger; to strong regional chains like Publix and Sendiks; to natural food chains in
Whole Foods and Fresh Thyme; to grocery suppliers like Associated Wholesale Grocers, C&S Wholesale Grocers, Peirone Produce and Topco Associates; to many smaller and independent chains, not to mention foodservice operators and wholesalers. 
“Our exhibitors raved about the many substantive conversations they had with buyers,” said vice president of produce Shannon Shuman. 
“Face-to-face meetings are so important in the produce industry and maybe even more in organics with the growth we’re seeing.”
Shuman said there were 60 exhibitors and more than 600 attendees at the first-year event, and it’s sure to grow for next year’s GOPEX, scheduled for Jan. 31 to Feb. 2, again in the Diplomat Beach Resort in Hollywood. 
Greg Johnson is The Packer’s editor. E-mail him at gjohnson@farmjournal.com