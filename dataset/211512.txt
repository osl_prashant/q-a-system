Thomas Lundgren, former owner of Spud City Sales, Stevens Point, Wis., has been charged with multiple counts of business theft in Wisconsin’s Portage County. 
Wisconsin Circuit Court case documents report Lundgren faces eight counts of business theft over $10,000 and two counts of theft over $2,500 each.
 
After charges were filed in August, Lundgren was released on a $25,000 signature bond and he is scheduled to be back in court Oct. 9 for a preliminary hearing, according to the court documents.
 
In December 2012, Spud City Sales LLC, Stevens Point, Wis., and Lundgren were named in a case in federal court in Wisconsin filed by owners of Michael Farms Inc., Urbana, Ohio. That lawsuit alleged that the Urbana company was owed more than $123,000 under the Perishable Agricultural Commodities Act for potatoes provided to Spud City from Jan. 4 through Oct. 8 that year.
 
A spokesperson for Michael Farms couldn’t immediately be reached for comment on Sept. 22.
 
The Stevens Point Journal reported that Lundgren has been accused of stealing more than $705,000 from numerous growers and trucking companies in central Wisconsin. The criminal complaint said Portage County detective began investigating his business, Spud City Sales, in October 2012, according to the Journal report.
 
Lundgren also faced similar charges in Portage County in 2013, but the case was dismissed, according to the Journal report.