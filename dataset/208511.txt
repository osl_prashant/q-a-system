Chicago Mercantile Exchange live cattle futures climbed higher on Wednesday, driven by short-covering and futures' discounts to early-week cash prices, traders said.Buy stops and fund buying enhanced market advances, they said.
August ended 1.725 cent per pound higher at 114.525 cents per pound, and October 1.800 cents higher at 114.675 cents.
Both contracts topped their respective 10-day moving average of 114.018 and 114.053 cents.
On Wednesday some market-ready, or cash, cattle in Kansas brought $116 to $117 per cwt, steady to down $1 from last week. But a few cattle in Nebraska fetched $118, up $1 versus a week ago.
Those trades followed Wednesday morning's Fed Cattle Exchange sale of $116 per cwt, down from $117.50 to $118.25 a week earlier.
Profitable packer margins and tight supplies in parts of the Plains emboldened sellers to hold out for at least $119 for remaining cattle, feedlot sources said.
However, some processors are reluctant to bid up for supplies given seasonally sluggish wholesale beef demand and anticipation of building supplies in the coming months.
CME feeder cattle drew support from technical buying and higher live cattle futures. August feeders ended up 1.075 cents per pound to 150.250 cents. 
Hog Futures Rally
Bargain buying and CME lean hog futures' discounts to the exchange's hog index for July 31 at 87.70 cents pared some of the market's recent losses, said traders.
August, which expires on Aug. 14, closed 2.150 cents per pound higher at 81.950 cents, and above the 20-day moving average of 81.538 cents. Most actively traded October ended 1.825 cents higher at 66.225 cents.
Packers on Wednesday bought hogs for less money as supplies began their seasonal increase, said traders and analysts.
"More hogs is an issue of the time of the year, and there are even more numbers in front of us," said Sterling Marketing Inc president John Nalivka.
The U.S. Department of Agriculture estimated that from Monday to Wednesday packers processed 1.313 million hogs, 13,000 more than last week and 87,000 more than a year earlier.
Grocers paid more for pork ahead of some plants shutting down for periodic seasonal floater holidays, traders and analysts said.
At this time of the year, packers give employees floater holidays, or time off in exchange for work during winter holidays.