The California pomegranate deal looks to be on the small side again this year, after last year brought a near 15% reduction.
Retail pomegranate volumes were down 13.5% compared to the previous year, for the 52-week period ending on July 29, according to data from Nielsen Perishables Group.
Ray England, vice president of marketing for DJ Forry Co. Inc., Pismo Beach, Calif., said expectations vary, with some growers expecting a reduction, and others an increase.
“We see the crop as a bit better than last year, maybe 10% better, but we were down 15% to 20% last year,” he said.
As far as timing goes, DJ Forry began with Rubilee harvest during the third week of August, which was about a week later than last year.
“It looks like all varieties may be about 10 days later than last year,” he said.
Crown Jewels Produce, Fresno, Calif., began harvesting the week of Sept. 4 in a very light way, said Jeff Kramm, salesman.
“We will start with the early varieties through the month of September and start wonderfuls in October. Our fresh season should run from September through Christmas,” Kramm said.
Levon Ganajian, director of retail relations with Fresno, Calif.-based Trinity Fruit Sales, said the company’s harvest is about a week later than last year.
“Supplies on our early varieties are up while our wonderful variety looks steady and similar to last year,” he said.
Many expect volumes to be down from last year, however, when California harvested 5.6 million boxes.
Tom Tjerandsen, manager of the Sonoma, Calif.-based Pomegranate Council, expects just over 5 million boxes for the fresh market this year, but Jeff Simonian, sales manager for Fowler, Calif.-based Simonian Fruit Co., isn’t sure they’ll hit that mark.
“We’ve talked to other growers and marketers, and while there are some with a full crop, we believe the overall crop will be down 15% to 20%,” Simonian said.
However, the fruit should be larger, redder and sweeter than in recent years.
“Mother Nature has been kind to growers after five years of drought. We finally got the rain we needed for the trees to recover and we’re expecting to see the benefits of that this year,” Tjerandsen said.
Kramm said color and quality looked very good on the trees.
“This season looks to have less scarring than what we had last season,” he said.
 
Import update
The imported pomegranate deal looked normal this summer, despite potential changes with the addition of Peru as a country of origin.
“I don’t believe that the Peruvian deal had a huge impact on the summer import season,” England said.
Tjerandsen said initial conversations reported that as many as 5 million boxes were expected to arrive from Peru, but that very little volume actually entered the U.S.
Numbers from Chile were down a bit as well and left a gap before the start of the earlier California volumes, Simonian said.
“We’re not involved in the import deal, but it does help remind consumers that pomegranates are there. But because of the gaps, they are still pretty seasonal,” he said.
Fresh pomegranates from Turkey are in consideration to be allowed into the U.S, but would overlap with California’s deal rather than running during the summer.