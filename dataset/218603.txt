North Carolina sweet potato leader George Rufus Massengill, 68, died Jan. 25.
Massengill, the owner of Hill Top Farm Service Center, was on the board of directors of the North Carolina Sweet Potato Commission as secretary/treasurer.
According to an obituary on Legacy.com, Massengill served as a volunteer leader on many agricultural boards and civic organizations.
A funeral service was at Bethel Free Will Baptist Church, Four Oaks, N.C., Jan. 28. 
Condolences may be sent to the family at www.roseandgraham.com.