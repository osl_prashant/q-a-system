There’s not a snowflake in sight in the Midwest and temperatures are starting to rise, kicking off the unofficial start of planting season for many farmers.

The slow start is being reflected in Monday’s crop progress report, and soybeans are making their first appearance.

Nationally, 2 percent of the soybean crop is in the ground, on par with the five-year average.

Winter wheat conditions are holding strong amid dry conditions in the Plains, and 31 percent of the crop is rated good to excellent. Spring wheat planting is rolling, and 3 percent of the crop is planted, lagging behind the five-year average of 25 percent.

Corn planting is also falling behind the five-year average of 15 percent planted to 5 percent planted in Nebraska, Illinois and Indiana. Usually the Iowa crop is at 11 percent planted, but there hasn’t been a seed planted yet.

There are still questions on if this cold, late start to the season will have an impact on yields.

“The detrended yield for 2018 is about 171 bushels per acre—most of those years had yields between 166 and 170,” said Mike Tannura, T-Storm Weather.

The researchers say assuming the three I-States are representative of the entire Corn Belt, 14 days is also the estimate of field days needed to plant the entire U.S. corn crop.