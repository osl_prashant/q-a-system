BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Tue.Mon.No. 2 Soft wheat4.57½4.46½No. 1 Yellow soybeans10.1310.10½No. 2 Yellow Corn3.71½e3.67¼eNo. 2 Yellow Corn3.87½p3.86¼p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted