Howard Marguleas, principal founder of Bakersfield, Calif.-based Sun World International, has died.Marguleas, 82, died June 1 in Rancho Mirage, Calif. following complications from cancer. Funeral services were June 5.
Marguleas built a reputation as a leader in U.S. agribusiness and served under California governors Edmund G. (Pat) Brown and Ronald Reagan as a member of the California State Board of Agriculture.
The Packer honored him as its 1988 “Man for All Seasons” and later named him one of the most influential produce industry leaders of the past 100 years. He was recognized with the Distinguished California Agriculturalist Award in 1990, granted the Mentor’s Award by the California Fresh Fruit Association and named the Riverside County Farm Bureau’s Man of the Year in 1999.
Matt McInerney, executive vice president at Newport Beach, Calif.-based Western Growers, described Marguleas as an “iconic figure” who left an indelible mark on the produce industry.
“Always the visionary and focused on proprietary produce commodity offerings, he leaves a legacy of innovation that others in the industry continue to emulate to this day,” McInerney said. “His leadership will be missed.”
Marguleas is credited with bringing then-unfamiliar fruits and vegetables to U.S. consumers, starting in the 1960s with the first Hawaiian pineapples sold on the mainland, then branded fruit and nuts under the Sun Giant label. He also supplied red flame seedless grapes, vine-ripened tomatoes, colored sweet peppers, seedless watermelon and assorted new grapes and plums under the Sun World brand, in addition to Californian mangoes.
“He was an extraordinary man who touched many people and left lasting impressions throughout a lifetime filled with accomplishment, optimism and passion,” the Marguleas family said in an e-mail announcing his death.
Karen Caplan, CEO of Los Alamitos, Calif.-based Frieda’s Inc., said she lost a good friend in Howard Marguleas.
 
Caplan said her mother, Frieda, has a particularly fond memory of Marguleas.
 
“From the time Howard caught Frieda at an (early 1970s) produce convention, and grabbed over The Packer photographer and said, ‘Take our picture together — she’s the small leader of specialty produce, and I’m the big leader of specialty produce,’ Howard was always Frieda’s go-to mentor,” Caplan said. “Frieda said she knew she was on the industry radar when Howard and Frieda were both invited to present at an important program at Jewel Foods Co. in Chicago.”
Marguleas founded Sun World International in the mid-1970s, and sold it 20 years later.
Earlier in his career, along with his father, Joseph, and partner, Frank Heggblade, Marguleas worked for produce marketing company Heggblade-Marguleas Co., which they sold in 1970 to Tenneco Inc., according to his obituary. Marguleas was president and CEO of Heggblade-Marguleas-Tenneco in Bakersfield, which had the oil conglomerate’s California agriculture and real estate holdings.
Marguleas earned a bachelor of arts in agriculture economics at the University of California-Berkeley.
He was a long-time philanthropist and champion for health care, education and community organizations, serving on numerous boards of directors and trustees in academia.
Marguleas was chairman of the board of the California State Chamber of Commerce and served on various corporate boards, including The Irvine Co., Sun World, Ready Pac Foods, Summit Health and Blue Shield of California, according to the obituary.
Survivors include his wife of 59 years, Ardith; and children, David, Dianna, Anthony and Brian.
Memorial contributions may be made to Eisenhower Medical Center, City of Hope or the American Cancer Society .
Interment was at Salem Memorial Park in Colma, Calif.
A Celebration of Life is scheduled for 1 p.m. June 11 at the Annenberg Center for Health Sciences at the Eisenhower Medical Center in Rancho Mirage.
More information is at http://tinyurl.com/Marguleas.