If you could do one thing to live longer, what would it be?
 
A study published in the Journal of American Medical Association explored mortality factors related to diet.
 
The researchers asked: 
 
What is the estimated mortality due to heart disease, stroke, or type 2 diabetes (cardiometabolic deaths) associated with suboptimal intakes of 10 dietary factors in the United States?
 
The study found that, in 2012, "suboptimal" intake of dietary factors was associated with an estimated 318,656 cardiometabolic deaths, or 45.4% of cardiometabolic deaths. The highest proportions of cardiometabolic deaths were estimated to be related to excess sodium intake, insufficient intake of nuts/seeds, high intake of processed meats, and low intake of seafood omega-3 fats.
 
Bring on the nuts and the fish and take the salt shaker!
 
But you are right if you think that fruits and vegetables also are a part of this story.
 
The ten dietary factors considered were: fruits, vegetables, nuts/seeds, whole grains, unprocessed red meats, processed meats, sugar-sweetened beverages (SSBs), polyunsaturated fats, seafood omega-3 fats, and sodium.
 
Researchers found that the largest numbers of estimated diet-related cardiometabolic deaths were related to:
 high sodium (66,508 deaths in 2012; 9.5% of all cardiometabolic deaths);
 low nuts/seeds (59,374; 8.5%);
 high processed meats (57,766; 8.2%);
low seafood omega-3 fats (54,626; 7.8%);
low vegetables (53 410; 7.6%);
low fruits (52 547; 7.5%); and
high sugar sweetened beverages (51,694; 7.4%). 
 
If you lump fruits and vegetables together, insufficient consumption causes 15.1% of cardiometabolic death, higher than any other single category.
 
So bring on the nuts and take away the salt shaker. But, mainly, (speaking to myself) eat more fruits and vegetables.