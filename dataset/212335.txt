This year’s West Coast Produce Expo saw more exhibitors, buyers and attendees than ever before.The third annual event was held May 19 through the 21st in Palm Desert, Calif.
Attendees participated in a golf scramble and socialized at the Arabian Nights themed reception.
On Saturday, attendees participated in the Fresh Trends Quiz Show, and listened to keynote speaker Kareem Abdul-Jabbar talk about his career and leadership lessons.
The expo itself had over 150 exhibitors, over 250 buyers and over 1000 attendees.
On Sunday, more than 20 buyers visited packinghouse and field tours at Primetime Inc., North Shore Herbs and Sun World.
Did you miss the West Coast Produce Expo? Never fear, the Midwest Produce Expo is just around the corner.
Sign up at midwestproduceexpo.com and join us in Kansas City August 14 through 16 for a look an in depth look at millennial shoppers.