Photo courtesy Tanimura & Antle
Tanimura & Antle has been recognized for commitment to its farmworkers, earning multiple awards for initiatives that include the Spreckels Crossing employee housing complex.
The Salinas, Calif., grower-shipper received the 2017 Ben Heller Award for Leadership Leadership & Courage Oct. 29, which honors companies that improve housing and healthy conditions in the Salinas and Pajaro Valleys.
The award was given at the Annual Center for Community Advocacy’s Dia De Los Muertos Tardeada & Ben Heller Awards Banquet.
Spreckels Crossing, which opened in April 2016, consists of 100 2-bedroom furnished units that can hold up to 800 employees, according to a news release.
“We know our employees are our greatest asset and that their hard work and dedication have helped up to become the vibrant, innovative company that we are today,” CEO and President Rick Antle said in his acceptance speech, according to a news release.
The company also was named the 2017 Hero Award recipient for participation in a pilot program to increase worker safety in the fields. The Commissioner’s Farmworker Advisory Committee program is supported by Tanimura & Antle and four other companies that brought the program about, according to the release.
The other companies, which also receive Hero award are:

SeaMist Farmers,
Costa Family Farms,
Bayview Farms, and
Scheid Vineyards.