AP-AR--Arkansas News Digest 1:30 pm, AR
AP-AR--Arkansas News Digest 1:30 pm, AR

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Arkansas. Questions about coverage plans are welcome and should be directed to the AP-Little Rock bureau at pebbles@ap.org or 800-715-7291.
Arkansas Supervisory Correspondent Kelly P. Kissel can be reached at kkissel@ap.org or 501-681-1269.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
................
POLITICS & GOVERNMENT:
XGR--ARKANSAS LEGISLATURE-SPECIAL SESSION
LITTLE ROCK, Ark. — Arkansas lawmakers are meeting at the Capitol this week to take up pharmacy reimbursement rates, highway funding and several other issues. The House and Senate convened Tuesday for a special session, the day after the Legislature formally adjourned this year's fiscal session. By Andew DeMillo. 400 words.
With:
XGR--ARKANSAS LEGISLATURE-SPECIAL SESSION-THE LATEST
XGR--ARKANSAS-HOG FARM
LITTLE ROCK, Ark. — Arkansas lawmakers meeting in a special session are considering changes in how the state issues liquid waste permits for farmers, worrying environmentalists who fear the new way could somehow make it tougher to fight a farm authorized to house 6,500 hogs near the nation's first scenic river. By Kelly P. Kissel. 430 words, with photos.
FROM AP MEMBERS:
POLICE DISCRIMINATION LAWSUIT
LITTLE ROCK, Ark. — Some black Little Rock police officers are suing the city, alleging discrimination by the department chief, who is also black. 250 words.
IN BRIEF:
— MEDICAL MARIJUANA-ARKANSAS-CHALLENGES — Arkansas regulators are receiving a flurry of challenges to the licensing of the state's first medical marijuana growers.
— ARKANSAS CASINOS — Arkansas Attorney General Leslie Rutledge has again rejected a proposed state constitutional amendment that would allow four casinos in Arkansas.
IN SPORTS:
BKC--NCAA--ONE-BID WONDERS
South Dakota State forward Mike Daum already has experienced the NCAA Tournament twice before. Now he wants to discover what it feels like to win on college basketball's biggest stage. Daum, one of the nation's most prolific scorers, heads a list of several notable players from one-bid leagues relishing their chance at the spotlight this week. South Dakota State (28-6) is the No. 12 seed in the West Region and faces Ohio State (24-8) on Thursday in Boise, Idaho. It's one game to watch going into the NCAA tournament. By Steve Megargee. SENT: 1,000 words, with photos.
___
If you have stories of regional or statewide interest, please email them to pebbles@ap.org and follow up with a phone call to 800-715-7291.
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867.
For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
The AP-Little Rock