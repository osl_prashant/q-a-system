Fall is a good time to start thinking ahead to your farm or ranch taxes. If you have your farm/ranch financial records entered in Quicken (Deluxe), there are some key reports to make your tax planning easier. 

Below are some Quicken tips to help you prepare for you for pre-tax planning with your accountant.

Step 1: Double Check Your Taxable Categories

First, check your categories to ensure the taxable categories are assigned to a schedule. To find your category list use the tools menu. 
Tools —> Category List 

Once the Category List dialog box is open, you can see which of your categories have been designated to a tax line item, by reviewing the tax line item column. There should be a checkmark and the tax schedule listed.

If your categories are not tied to a tax line item, those entries will not show up on the “Tax Schedule” or “Tax Summary” report. Spend the time to double check your categories that should be associated with a tax schedule.

To assign a category to a tax line item, select the category you want to assign a tax line item to and right click on the category. Click on “Edit”. A box will appear called “Set Up Category”. Click on the back tab labeled “Tax Reporting”. Check the box for “Tax Related Category” and then click the dropdown menu (the arrow) on the “Tax line item for this category”. Scroll down to Schedule F and choose the correct tax line item for your farm or ranch business. (Note: the Schedule F is near the bottom of the list… keep scrolling down.) Click OK to save your changes.

Quicken provides descriptions of each tax line item to assist you in selected the proper tax line item for the given income or expense category. If you cannot decide which tax line item is correct item for the given category, you do not have to select a specific tax line item. Just make sure the box for “Tax Related Category” is checked, and leave the tax line item blank. 

A list of farm/ranch categories already assigned to Schedule F tax line items can be found at Oklahoma State’s website at: http://www.agecon.okstate.edu/quicken/samplefiles.asp . Search under “Download Sample Files” for the Farm Categories list. The videos on how to download these categories to your Quicken program is under “Videos and Instructions”- look for the Importing, Adding, and Editing Categories video.

Like other features in Quicken, tax line items are easily edited! Do not be afraid to assign a category to a tax line item- it can always be changed if you or your accountant prefer for it to be assigned to a different tax line item. 

You can assign a tax line item to each applicable category, not just those associated with your farm or ranch business. Check out the other schedules and tax line item descriptions to see what other incomes or expenses should be reported to the IRS. 
Step 2: Summarize Tax Reports to Share with Your Accountant During the Pre-tax Planning

Once your categories are tied to a tax line item, you can run a summary of your taxable income and expenses so that you can easily view your potential taxable income before the end of the year.
There are two reports that can help you with your tax planning: 1) Tax Schedule (sorted by tax schedule, W-2, etc), and 2) Tax Summary (sorted by Taxable Income and Expense line items). These reports can be saved as PDF files and shared/emailed to your tax accountant.

For the Tax Schedule go to:
Reports —> Tax —> Tax Schedule
Change the “Date Range” to reflect the year you are working with.

For the Tax Summary go to:
Reports —> Tax —> Tax Summary
Change the “Date Range” to reflect the year you are working with.

The tax reports can also be customized. Examples include customizing a report using tags. Tags can be used for enterprises (such as overhead, corn, soybeans, cow-calf, and family living.) If you use tags for enterprises (and tag every entry), you can select and sort by specific tags. Other customizations include payees, accounts, and categories.

So gather your records, run tax reports, and meet this fall with your tax preparer. A few hours invested this fall could save you big money on taxes filed next year!