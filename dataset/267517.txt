Pesticide found on plastic eggs from Mohave Valley event
Pesticide found on plastic eggs from Mohave Valley event

The Associated Press

BULLHEAD CITY, Ariz.




BULLHEAD CITY, Ariz. (AP) — Health officials are advising people throw out candy-filled plastic eggs distributed at an event at a Mohave Valley park after testing showed traces of pesticide on an egg and candy.
According to the test results, traces of a substance called dimethoate were found on the plastic egg and candy from the March 24 event at Mohave Valley Community Park.
The Mohave Daily News reports the Mohave County Department of Public Health says that no one has reported any signs of illness related to pesticide exposure.
Patty Mead, the department's director, said in a statement sent out Wednesday that the Arizona Poison and Drug Information Center has confirmed there is no delayed onset of symptoms and no long-term effects.
___
Information from: Mohave Valley Daily News, http://www.mohavedailynews.com