Dairy farm solar project fails to win officials' support
Dairy farm solar project fails to win officials' support

The Associated Press

RUTLAND, Vt.




RUTLAND, Vt. (AP) — A project to install solar panel arrays on a Vermont dairy farm has failed to gain the approval of town officials, who are worried the panels would be visible from a nearby highway.
The Rutland Herald reports that the Rutland Town Select Board refused on Tuesday to sign a letter in support of the project at the Thomas Dairy farm.
A representative for the project says he will meet with the board to discuss their concerns.
He says the solar arrays would be well away from the road and would be screened from view by the farm's buildings. He says the solar project would only cover a small portion of the farm, which would still be mostly used for agricultural purposes.