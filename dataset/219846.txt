Executives at Supervalu continued to express confidence about the direction of the company Feb. 7 as investor Blackwells Capital ratcheted up its calls for drastic change.
In a Feb. 6 letter from Blackwells managing partner Jason Aintabi, the firm announced its intention to submit three nominees for the Supervalu board of directors.
Aintabi wrote in the letter that the “lackadaisical, misguided and value-destructive complacency” of Supervalu leadership prompted the push for seats on the board.
The firm pointed to the plummeting stock price — $15.42 at the end of business Feb. 7, down from $29.12 in May — as evidence that dramatic moves are needed.
In a 79-page presentation titled “Save Supervalu,” Blackwells recommended the company separate its wholesale and retail businesses and explore a sale of the former.
The firm highlighted opportunities for synergy with companies like SpartanNash, United Natural Foods and C&S Wholesale Grocers.
Blackwells has asserted that its numerous proposals can drive the Supervalu stock price to $45.05, roughly triple its current value.
Supervalu responded to the letter and presentation with a news release that highlighted recent positive developments and rebuked Blackwells.
“Over the past few months, members of Supervalu’s board and management team have had several discussions and meetings with representatives of Blackwells,” the company stated in the release. “Discussions encompassed a variety of topics pertaining to the business and the company’s ongoing initiatives as well as our board refreshment efforts — initiatives that have been underway substantially since before Blackwells became a stockholder.
“Despite our efforts to reach a constructive path forward and to discuss overlapping objectives, Blackwells has decided to threaten an unnecessary and counterproductive proxy contest,” Supervalu stated in the release.
The company pointed to its recent reinvention as an organization focused on wholesale, with that segment accounting for 75% of annual sales, up from 44% two years ago.
Some of those gains were the result of the acquisitions last year of Unified Grocers and Associated Grocers of Florida.
In its presentation, Blackwells acknowledged those developments as positive but argued that the retail business is a distraction to management.
Supervalu noted in its release that the date of its annual meeting has yet to be announced and that shareholders do not need to take any action at this time.
Last year, activist investor Jana Partners pressured Whole Foods Market until the organics-focused retailer eventually sold to Amazon.
Jana Partners sold its stake in the company soon after the acquisition was announced and made roughly $300 million for its trouble.