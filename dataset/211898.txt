Orange Cove, Calif.-based Mulholland Citrus is joining Sunkist Growers.
 
Mandarins make up most of what Mulholland produces, according to a news release.
 
“In the four generations of our family’s farming history, Mulholland Citrus has enjoyed a longstanding relationship with Sunkist and we are pleased to now be joining the cooperative,” Heather Mulholland said in the release. “We are aligned with Sunkist’s sales and marketing approach, as well as the organization’s values — which, as a family business, are very important to us.”
 
With projections to pack 8 million 5-pound carton equivalents of mandarins years, Mulholland Citrus will begin packing under the Sunkist brand on Nov. 1 to kick off the 2017-18 citrus season, according to the release.
 
“Mulholland Citrus has a rich history of growing and shipping exceptional quality citrus,” Sunkist president and CEO Russ Hanlin said in the release. “We are honored to welcome this industry leader to
Sunkist and are looking forward to working together to deliver premium fruit to our customers.”
 
The Sunkist cooperative offers more than 40 fresh citrus varieties, according to the release.