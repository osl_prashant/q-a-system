I was marking my calendar when a Hawaiian-shirted Russ T. Blade pushed his way out of my top drawer. “Rusty” is the imaginary miniature produce manager who lives in my desk and pops up occasionally to talk shop. 
 
Rusty: Counting down the days to vacation time, bub?
 
Me: You got it. A few days of summertime R&R, as they say. Looks like you’re dressed for the same?
 
Rusty: Yeah, I know the produce stand is at its busiest period now. But even this old timer needs a few days in the sun to recharge the ol’ batteries.
 
Me: So when you leave do you need to do a lot of prepping with the produce crew and your assistant? You know, leave lots of notes on what to do while you’re gone?
 
Rusty: If I had a green crew, maybe. Or I might postpone the vacation for another time. No, my second-in-command and crew do fine on their own. I might leave a few directions — reminders, I call them, but not much beyond.
 
Me: I like that. Having a seasoned crew makes a difference.
 
Rusty: Few crews starts out seasoned, that’s for sure. In every store I’ve managed the crew always had some range of dysfunction: attendance issues, suspect work habits, lack of training or teamwork, low sense of urgency. All of that has to be worked out before a produce manager can hope to take time off.
 
Me: Yeah, it seems just about every crew I’ve managed started out with some level of drama. What did you do to get everything back on track?
 
Rusty: I know this hurts, but for the first month or six weeks in a new store, I’d dig right in. I came in early, got things organized, pushed hard to get displays built, orders written, and, most of all — training.
 
Me: That’s hard to do, especially in that first month.
 
Rusty: A produce manager must set the tempo. For that whole time, I’d also stay late. I worked side by side with each of my crew. Especially the ones with less produce experience. I’d show them exactly what and how I wanted everything done.
 
Me: That’s a ton of work. But I see how it can pay off. 
 
Rusty: That meant every person, and every shift. I even closed up shop with each of my part-timers. After I worked with everyone, my message was clear. This is what I expected from them: Hustle, no shortcuts, do a good job, and always take care of the customer.
 
Me: And once they follow through, you resume a normal schedule?
 
Rusty: Exactly. Knowing they’ll take care good care of the produce stand — even when I’m on vacation.
 
Armand Lobato works for the Idaho Potato Commission. His 40 years’ experience in the produce business span a range of foodservice and retail positions. E-mail armandlobato@comcast.net.
 
What's your take? Leave a comment and tell us your opinion.