As the weather turns warm across the U.S., sales of melons heat up for retailers, said retail merchandising specialist Dick Spezzano, owner of Monrovia, Calif.-based Spezzano Consulting Service.“It’s a seasonal business,” he said.
Mexico soon will give way to California’s Coachella Valley, which, in turn, will turn production over to California’s Westside.
Then, regional growers across the U.S. will enter the fray.
Melon season is a busy time for retailers, because it’s summer and consumers never have had so many choices, Spezzano said.
“We’ve seen an explosion in value-added, with cantaloupe and watermelon,” he said. “Watermelon is No. 1, when it comes to cut fruit. Cantaloupe is right behind.”
Shoppers are looking for bigger cantaloupes now than in earlier eras, Spezzano said.
“What we’re seeing, it used to be the predominant size at retail for 15s. Now, we’re seeing 9s and jumbo 9s,” he said. “It’s a much denser melon than it’s ever been. They eat more like an apple — much firmer than it used to be.”
On watermelon, the ideal size is a 15-pound seedless, and, for mini-watermelons, it’s a six-count, about a 5-pounder, Spezzano said.
Retailers generally have plenty to promote in the melon category, Spezzano said.
“You get into spring-summer, you see bigger displays and more promotions, too, and we find when you put cantaloupe, honeydew or watermelon on the ad, you pick up 400% to 700% increases,” he said. “You see a big discount in pricing.”
Some shoppers will take advantage of aggressive pricing and buy extra melons, Spezzano said.
“As an example, cantaloupe (normally) at 69 cents a pound and (you) see it on ad for 4 pounds for a dollar,” he said. “You can buy and they’ll stay under refrigeration fresh for 10 days to two weeks. People will stock up to eat it now and eat it later.”
Melons often are an impulse buy, and there are ways retailers can help sales and convert those impulse buys to planned purchases, said Josh Knox, melons category manager for sourcing with Eden Prairie, Minn.-based Robinson Fresh.
Among Knox’s suggestions:
Ensure bins are in-stock and build two displays during peak season, one outside the store and one inside at the front of the department;
Use high-graphic bins, which can increase sales by 68%, according to the National Watermelon Promotion Board;
Place a half-cut melon on display to show the quality;
highlight and sample new varieties;
tie in with other seasonal items;
offer a variety of value-added options to promote impulse buys; and
use point-of-sale materials to educate consumers on product selection, nutrition and usage ideas.
“I think it’s about merchandising, getting the right display,” said Daren Van Dyke, sales and marketing director with Brawley, Calif.-based Five Crowns Marketing.
Timing also can be an advantage, Van Dyke said.
“We feel our competitive advantage is we’re the first to market with domestic fruit and can come to the table with promotable volume,” he said.
It’s also crucial to deliver to the retail customer on every promise, said Matt Solana, vice president of operations/supply chain with Autryville, N.C.-based Jackson Farming Co.
“Price, service and relationship — doing what you say you will do,” he said.
Having consistent quality and proper merchandising also are important for success at retail, said Jeff Fawcett, salesman with Edinburg, Texas.-based watermelon grower-shipper Bagley Produce Co.
“You have to get them off the floor, put them at eye level and keep a good rotation and keep them fresh and tasty,” he said.
Product sampling also works, Fawcett said.
Promotions are important, too, said Ramon Murilla, president of Nogales, Ariz.-based Cactus Melon Corp.
“The best time to sell whole melons is this time of year, the spring,” he said. “It takes you through late summer and that’s when you promote more whole watermelon.”