Hogs v. humans: Neighbors fight back against swine waste
Hogs v. humans: Neighbors fight back against swine waste

By ALLEN G. BREED and MICHAEL BIESECKERAssociated Press
The Associated Press

WILLARD, N.C.




WILLARD, N.C. (AP) — Terry "Pap" Adams says he was out in the backyard, tinkering on one of his car projects, when another cloud of noxious pinkish-brown mist drifted overhead. The droplets hit his wife's black car, leaving blotches with greasy little dots in the center.
"You can feel it on your clothes," he said as he stood outside his home in rural Willard, about 70 miles (113 km) southeast of Fayetteville. "You could feel it, like a misting rain. But it wasn't misting rain. It was that stuff."
The nose-curling mist was wastewater — pumped from a pond where his neighbor stores the urine and feces from thousands of hogs, and sprayed onto the fields just across a narrow ditch from Adam's property line.
"When the wind is right, you get it — ain't nothing you can do about it," the 60-year-old man said, twisting his mouth in an expression of disgust.
For decades, people like Adams say they have felt powerless against the economic and political influence of the commercial hog farming industry. In early December, a federal judge scheduled the first of what could be dozens of trials in a group of nuisance lawsuits filed against Murphy-Brown LLC, a subsidiary of Smithfield Foods — the world's largest pork processor and hog producer.
The first two test cases are set to be tried in April.
Iowa is by far the nation's leading hog producer, but recent events in No. 2 North Carolina have triggered concern at the industry's trade group in Washington, according to lobbying disclosures.
After a series of major spills from hog lagoons caused fish kills and contaminated rivers, pork producers here shifted two decades ago to disposing of their swine waste by spraying it across farm fields. But there are no easy solutions to the challenges caused by the massive amounts of animal waste that are byproducts of the world's demand for pork and other protein. In this corner of rural North Carolina, the response to the problem of hog waste spilling into rivers has spawned a new kind of environmental concern.
Residents' lawyers plan to present DNA testing evidence at the upcoming trials they say shows that tiny particles of hog feces are settling over their clients' properties.
A former environmental engineer for the Environmental Protection Agency, Shane Rogers, swabbed the outsides of homes near several hog farms in southeast North Carolina. He said 14 of the 17 homes — including those belonging to two of Adams' neighbors — tested positive for pig2bac — a genetic marker linked to the presence of hog feces.
When Rogers visited the Rose Hill home of 80-year-old Mary Tatum in November 2016, he was "met at the road in front of her house with exceptionally malodorous air," he wrote in his report. The hog lagoon is less than 500 feet from the Tatum's house, separated from the spray field by a narrow buffer of trees.
Swabs tested positive for pig2bac inside Tatum's home — from a stovetop, a range hood and the top of her refrigerator, where she keeps her bread and her cereal, he testified in a February 2017 deposition.
In a response filed with the court, an expert hired by the pork company said Rogers' methods were shoddy and violated fundamental pillars of sample-collection reliability. Jennifer Clancy, a microbiologist who has also consulted with the EPA, said the study of pig2bac is far from conclusive, and that Rogers "turns a blind eye to the scientific caveats in testing."
Meanwhile, the state Department of Environmental Quality is negotiating with three watchdog groups to resolve complaints of environmental racism in the administration of North Carolina's swine-waste permitting. In a letter to the state agency last January, EPA's civil rights office said it had "deep concern" that African-American, Latino and American Indian communities have been subjected to discrimination in the location of operations that produce a lot of waste.
Lobbying reports show the National Pork Producers Council has been meeting with EPA officials and Congress recently to discuss federal regulations for reporting air emissions from livestock farms and the environmental justice petition in North Carolina.
The industry has also backed a House bill, co-sponsored by two North Carolina Republicans, that seeks to limit the ability of neighbors to sue hog farmers who are "diligently seeking compliance" with state and federal regulations.
A spokesman for the trade group said its farmers have been interested and concerned about environmental justice. "If neighbors have concerns with odor or manure management, hog farmers in North Carolina or anywhere will work and have worked to address and mitigate them," said Dave Warner, the council's communications director.
Virginia-based Smithfield — which was purchased in 2013 by China-based WH Group — disputed that race played any role where it locates its company-owned or contract operations. The company has called the lawsuits a "cash grab" and maintains that the current permitting system works well.
"It protects the environment and the neighbors of farms," said Joyce Fitzpatrick, Smithfield's spokeswoman. "More than 80 percent of hog farms are family farms. These farmers care deeply about their communities. They wouldn't do anything to hurt the places where they and their families live."
Last spring, the state's Republican-controlled legislature passed a law to protect hog and other agricultural operations against lawsuits like those pending in federal court. The new law limited a producer's liability to the lost property value plaintiffs can prove as a result of nuisance, a move critics said was the latest legislative gift to the hog industry at the expense of the poor.
Pork is a $2.3 billion industry in the Tar Heel State, home to roughly 9.3 million hogs. The state's human population is around 10.2 million.
It is the intersection of those two populations that has generated much of the friction.
In North Carolina, as in all the leading pork-producing states, most hogs are raised in airplane hangar-like barns, some holding hundreds of animals each. The difference is how the waste is stored and disposed.
Among the top hog states, North Carolina and, to a lesser extent, Illinois are alone in allowing the widespread use of open-air waste-storage lagoons and fertilizer spray fields. Here, spraying often uses large sprinkler guns that can aerosolize waste into tiny droplets.
Farmers in Iowa — with more than 20 million head and over $7.5 billion in annual sales — and the other large Midwestern pork states store the manure and urine in deep concrete pits beneath the hog houses. The resulting "slurry" is then pumped or trucked to the fields, where it is injected or disked directly into the soil as fertilizer.
An analysis by the Environmental Working Group and Waterkeeper Alliance, two environmental groups, in 2016 estimated that North Carolina's hog operations generate more than 9.5 billion gallons (35.96 billion liters) of fecal waste a year.
Hog farms here tend to be much smaller than their Midwest counterparts. But they're clustered in low-lying, flood-prone counties with sandy soils and shallow aquifers.
There are strict rules about not spraying when rain or high winds are forecast, and to stop before the water begins ponding. But the state has just 14 inspectors to make annually required visits to the roughly 2,100 hog operations.
Plaintiff Elsie Herring, who lives about 7 miles (11 km) south of Tatum in the town of Wallace, said she feels like a prisoner in her own home.
"We don't open our windows or doors," said Herring, who is black. "We don't sit out. We don't cook out. We don't do anything."
Her simple frame home is one of several on 15 acres that has been in the family since the 1890s. Her grandfather, Immanuel Stallings, an ex-slave, bought the property from his former mistress.
"My mother had all 15 of us on this land," said Herring, a community organizer with the North Carolina Environmental Justice Network, an environmental group.
Growing up, Herring said there was always farming going on all around. Her family even kept a few hogs.
But, she said: "These mega-farms are something that's new."
Dr. James A. Merchant, former dean of the University of Iowa College of Public Health, said people like Herring have a "reasonable basis to be fearful and concerned" for their health. He cited conclusions of scientific studies he said clearly establish public health risks from commercial hog farms. One study, which he co-authored, found that a 2,000-head operation can produce as much waste as a small city.
Smithfield has fought unsuccessfully to have Merchant's report and testimony blocked.
During its civil rights investigation, initiated under the Obama administration following a 2014 complaint, the EPA said it heard descriptions of health impacts, but also "retaliation, threats, intimidation and harassment by swine facility operators and pork industry representatives."
Even on a 90-degree day, the smell from two hog houses and a waste lagoon was almost negligible on Everette Murphrey's 172-acre operation in the town of Farmville. Inside the houses, huge fans kept the temperature a good 10 degrees cooler than outside.
"It's not bad on the hogs," Murphrey said as pigs skittered away before him. "It's not bad on the humans that are working in there."
Murphrey uses a center pivot system to irrigate crops of hay, feed corn and soybeans. Resembling the skeleton of some metal dinosaur, the machine can be swung into place over different parts of the field as dangling nozzles spray the wastewater downward — minimizing wind drift.
"If the wind's blowing in the direction of where some houses are, that day we won't pump," he said. "I've never had a complaint from a neighbor. Never."
Fitzpatrick, the Smithfield spokesperson, said the company has no qualms about severing ties with scofflaws.
Just before Labor Day, state officials responded to a spill at a Smithfield contract farm near Croatan National Forest, not far from the Bogue Banks. Inspectors found pinkish waste flowing from a hydrant and into a ditch leading to the Trent River.
"The flow was so good, it looked like there could have been a pipe there," one of the inspectors reported.
An investigation later showed nearly 1 million gallons of wastewater from the lagoon were illegally drained into a nearby wooded area. The state levied a $64,072 fine against Lanier Farms TCB Inc., citing, in part, violations dating back to 2010.
Smithfield said it placed the grower on probation in August. Fitzpatrick said all animals have been removed, and the farm has been empty since December.
Under a 2000 agreement with the state, Smithfield has spent $15 million to research alternatives to lagoons and spray fields. Fitzpatrick told AP that, "No alternative technology has been identified that is both economically feasible and that is as environmentally effective as the current system."
Despite incentives, only two North Carolina swine operations have adopted the "environmentally superior technologies" that might produce renewable energy, while reducing odors. Using numbers from North Carolina State University, the North Carolina Pork Council has estimated it would cost $5.4 billion over 10 years to retrofit all of the state's hog farms.
Dwight Strickland, who owns the farm beside Elsie Herring's property, told AP that all his spraying is done according to state regulations, and that he strives to be a good neighbor.
Some plaintiffs said things have improved since the lawsuit was filed. Herring noticed that Strickland doesn't use the spray field closest to her property line as often.
If the legal challenges fail, Herring worries things will go right back to the way they were.  She says she owes it to her ancestors to keep fighting.
"This is my connection to them," she said. "This is my birthright. Absolutely sacred."
___
Biesecker reported from Washington.
__
Follow Biesecker at http://twitter.com/mbieseck


Part of an occasional series by The Associated Press examining the environmental impacts from commercial farming