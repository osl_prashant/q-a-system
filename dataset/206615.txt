Sponsored Content
Abundant rains may have delayed double-crop soybean herbicide applications or even planting for some.
Post-application considerations include glufosinate for Liberty Link soybeans or an approved dicamba like Engenia for RR2Extend soybeans or other PPO-inhibiting herbicides. At this late stage, consideration must be given to plant-back restrictions or carryover concerns for next year’s crop, especially for cover crops, corn and sorghum. Plan accordingly and always refer to label instructions.
More resources on double-crop soybean weed control:
K-State Extension Agronomy eUpdate: Pigweed control in double crop soybeans 
UTcrops News Blog: Weed Control Options in Double-Cropped Soybeans Video
Sponsored by Nufarm