In the fall and winter, Georgia growers harvest hot peppers, carrots and sweet potatoes.


Specialty peppers

Georgia harvests hot peppers from early October through first freeze, in a season that runs parallel to bell pepper harvesting.
Nate Branch, a salesman with Southern Valley Fruit and Vegetable Inc., Norman Park, Ga., said he expects smaller Georgia acreage.
"We will have the same amount, but I'm seeing less and less variety peppers being planted over the last few years," he said. "I don't think people are getting out of it but are realizing it's a specialty item. Still, there's good demand."
Demand has increased, said Brandi Hobby, a saleswoman for South Georgia Produce Inc., Lake Park.
"We expect good supplies and quality," she said in late August.


Carrots

Generation Farms in Lake Park plans to begin harvesting carrots in mid-December, as usual.
Production should increase to promotable volumes by the first of the year with harvest typically running through late May.
"We should ramp-up wide open on January first," said Jamie Brannen, general manager of produce. "We are looking forward to a good season. We are preparing for good volume and quality."


Sweet potatoes

On Aug. 15, Generation Farms began harvesting sweet potatoes.
The company plans to harvest through mid-December.
The company will sell the sweet potatoes after curing them in computer-controlled rooms for 21 days.
Volume should run through late January, Brannen said.
The summer weather has been favorable for the crop, and Brannen said Generation Farms doesn't expect to harvest as many smaller potatoes as in the past.
"The quality is exceptional," Brannen said in late August. "The size looks good. It's hard to tell if it will be a jumbo or medium-sized crop, but sizings are definitely on the larger side."