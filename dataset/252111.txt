Forest Service chief retires following harassment reports
Forest Service chief retires following harassment reports

By MATTHEW BROWNAssociated Press
The Associated Press

BILLINGS, Mont.




BILLINGS, Mont. (AP) — The chief of the U.S. Forest Service has stepped down just days after revelations that he's been under investigation for alleged sexual misconduct and amid reports of rampant misbehavior, including rape, within the agency's ranks.
A Forest Service spokesman on Thursday confirmed agency chief Tony Tooke's sudden retirement, effective immediately and less than seven months after he was named to the post by Agriculture Secretary Sonny Perdue.
His departure was first reported by the Missoulian . It comes less than a week after PBS NewsHour reported Tooke was under investigation following relationships with subordinates before he became chief.
In a Wednesday night email to Forest Service employees, Tooke said the agency "deserves a leader who can maintain the proper moral authority" as it addresses instances of harassment, bullying and retaliation.
He did not directly deny the allegations against him, saying he "cannot combat every inaccuracy that is reported."
"In some of these news reports you may have seen references to my own behavior in the past," Tooke wrote. "This naturally raised questions about my record and prompted an investigation, which I requested and fully support and with which I have cooperated."
Lawmakers from both parties in Congress expressed outrage over events at the Forest Service, and called for a hearing and investigation.
"I plan to use every tool to ensure all bad actors are held accountable," said Republican U.S. Sen. Steve Daines of Montana, who chairs the Senate agriculture subcommittee that oversees the Forest Service. He said he'll hold a hearing on sexual harassment in the agency.
Rep. Jackie Speier of California, a Democrat and leading voice in Congress against sex harassment, said a broad investigation was needed into the Forest Service's "toxic culture" by the Inspector General of the U.S. Department of Agriculture.
Forest Service spokesman Byron James declined to say if the investigation into Tooke would continue. A replacement was not immediately announced.
The Forest Service has about 35,000 employees and manages more than 300,000 square miles (777,000 square kilometers) of forests and grasslands in 43 states and Puerto Rico.
PBS and other outlets, including The Associated Press, have previously reported on a culture of harassment and retaliation at the Forest Service. Many of the problems mirror misconduct within the nation's other major public lands agency, the Interior Department.
Lawmakers in Congress held hearings on sex harassment at the agencies in 2016. Senior officials have repeatedly vowed to address the problem, both during the administration of former President Barack Obama and more recently under President Donald Trump.
Perdue's office did not immediately respond to telephone and email messages from The Associated Press seeking comment.
Tooke, a native of Alabama who joined the Forest Service at the age of 18, had worked in Florida, Alabama, Georgia and Mississippi. Prior to becoming chief he served as regional forester for the southern U.S.
In announcing his appointment in August, Perdue cited Tooke's knowledge of forestry and his dedication to the "noble cause" of being a steward of public forests.
"Tony has been preparing for this role his whole life," Perdue said at the time. "His transition into leadership will be seamless."
__
This story has been corrected to fix the spelling of Agriculture Secretary Sonny Perdue.