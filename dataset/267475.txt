Maine petting zoo is site of first reindeer birth in years
Maine petting zoo is site of first reindeer birth in years

The Associated Press

WINSLOW, Maine




WINSLOW, Maine (AP) — A traveling petting zoo in Maine says its reindeer has unexpectedly given birth to the first baby reindeer in the state in more than 20 years.
Pony X-Press co-owner Ed Papsis says the reindeer is a dark brown female calf, and it was born at a farm on Easter Sunday. The petting zoo says it has named the reindeer Mistletoe.
Papsis tells the Morning Sentinel the calf's mother's name is Cocoa.
There are no wild reindeer left in Maine, and Pony X-Press has the only domesticated ones in the state. It once had the only reindeer in Maine, an 18-year-old female named Freeway that died in 2016. It now has one male and four females.
Attempts to reintroduce reindeer to the wild in Maine in the past haven't succeeded.