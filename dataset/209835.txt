Anhydrous is $87.70 below year-ago pricing -- lower $27.59/st this week at $411.44.
Urea is $14.08 above the same time last year -- higher $4.16/st this week to $327.48.
UAN28% is $8.56 below year-ago -- lower $2.37/st this week to $221.20.
UAN32% is priced $15.78 below last year -- higher $1.29/st this week at $243.04.

Anhydrous ammonia was solidly our downside leader across the fertilizer segment led by Illinois, which fell $46.79 as Indiana and Iowa each fell about $32. Two states were unchanged and no state posted a higher anhydrous price this week.
UAN28% was lower as well led by Illinois, which fell $28.19 as South Dakota dropped $19.36. Two states were unchanged as Wisconsin gained $12.35 and Michigan gained $7.50.
Urea led gains in the nitrogen segment led by Wisconsin which added $13.75 as Kansas gained $12.55 and Nebraska firmed $11.14. No state was unchanged as Iowa fell $19.90 and Illinois softened $2.12.
UAN32% was higher thanks to Missouri which added $27.78 and Wisconsin firmed $16.65. Four states were unchanged as Indiana fell $41, Iowa softened $10.40 and Illinois dropped $8.47.
We have been watching the spread between anhydrous ammonia and urea widen with great interest. In the five years we have been collecting data, this divergent pattern is wholly unique. We have seen the spread between anhydrous and urea wider than it is now. But it has always been while the two shared the same trajectory. However wide the spread has become in the last five years, urea and NH3 have always shared the same direction.
We believe this to be a significant development. If you have been reading our posts through the past few weeks, you know we believe nitrogen prices are in a transition of leadership. For as long as we have been collecting data, urea has been a reliable leader within the nitrogen segment. But as the fundamentals of the domestic nitrogen industry change, urea is losing its grip.
Since we have already booked a portion of of fall and spring nitrogen, we advise a hand-to-mouth approach for the time being. The next time we issue advice it will likely be for spring urea. We are targeting a move to the $340 area in our regional average to make a move, but given recent strength in wholesale and retail markets, that time may not be far off.
December 2018 corn closed at $3.99 on Friday, September 29. That places expected new-crop revenue (eNCR) per acre based on Dec '18 futures at $633.56 with the eNCR17/NH3 spread at -222.12 with anhydrous ammonia priced at a discount to expected new-crop revenue. The spread widened 32.68 points on the week.





Nitrogen pricing by pound of N 10/4/17


Anhydrous $N/lb


Urea $N/lb


UAN28 $N/lb


UAN32 $N/lb



Midwest Average

$0.25


$0.36 3/4


$0.39 1/4


$0.38 1/4



Year-ago

$0.30


$0.34 1/4


$0.41


$0.39 3/4





The Margins by lb/N -- UAN32% is at a 3 1/4 cent premium to NH3. Urea is 6 3/4 cents above anhydrous ammonia; UAN28% solution is priced 2 1/4 cent above NH3.





Nitrogen


Expected Margin


Current Price by the Pound of N


Actual Margin This Week


Outstanding Spread




Anhydrous Ammonia (NH3)


0


25 cents


0


0




Urea


NH3 +5 cents


36 3/4 cents


+11 3/4 cents


+6 3/4 cents




UAN28%


NH3 +12 cents


39 1/4 cents


+14 1/4 cents


+2 1/4 cent




UAN32%


NH3 +10 cents


38 1/4 cents


+13 1/4 cents


+3 1/4 cent