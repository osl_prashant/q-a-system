Patrick Brecht, president of PEB Commodities, addresses a well-attended session at Fresh Summit.

NEW ORLEANS — Produce shippers need to make compliance with the Food Safety Modernization Act sanitary transportation rule a priority, and they need to collaborate with supply chain partners in the process, speakers said during the Fresh Summit session on “Avoiding a Produce Transportation Meltdown.”
Patrick Brecht, president of Petaluma, Calif.-based PEB Commodities, said during the Oct. 19 session that the Food and Drug Administration constructed the rule to be vague and is currently focusing on education rather than enforcement, so now is the time for companies to get their ducks in a row.
Most will have to deal with the regulations at some point.
“You may be exempt, but you’re probably not,” said Stephen Neel, senior technical director for the Alexandria, Va.-based Global Cold Chain Alliance. “It covers virtually everybody.”
Even moving produce from one facility to another right down the road, within the same company, would fall under the jurisdiction of the rule, which applies to both interstate and intrastate transportation, Neel said.
Broadly, the rule addresses four main topics: vehicles and transportation equipment, transportation operations, training and record-keeping.
Vehicles have to be designed and maintained in such a way that they don’t cause food to become unsafe, and they need to be able to be cleaned in a way that won’t cause food to become unsafe. They also have to be able to maintain temperatures necessary for the safe transport of food.
During the transportation process, there must be temperature control, and there must be procedures to prevent contamination by food or non-food items or by allergens.
Additionally, carrier personnel need to be trained to transport fresh food specifically, and the training must be documented.
Last but not least, companies have to maintain records of procedures, agreements with supply chain partners and training of personnel.
The bulk of the compliance burden is on the shipper, as the party commissioning the transportation is responsible for developing written standards for the handling of the product throughout the supply chain.
Shippers should work with their carriers to create those expectations, Brecht said. Carriers will have more knowledge, for example, regarding what the design of a refrigerated container allows in terms of temperature management and in terms of cleaning.
As comprehensive as the sanitary transportation rule is, Neel said it is an achievable standard.
“Best practices in the industry are sufficient to do everything the government is asking you to do,” Neel said.
Drew McDonald, vice president of quality and food safety for Salinas, Calif.-based Taylor Fresh Foods, noted that commodity-specific standards are desirable given how much handling practices can vary from one product to the next.
“I think we have some work to do as far as really establishing this,” McDonald said.