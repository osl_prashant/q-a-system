BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Fri.Thu.No. 2 Soft wheat4.63¼4.76¾No. 1 Yellow soybeans10.13¾10.22¼No. 2 Yellow Corn3.58½e3.64eNo. 2 Yellow Corn3.76½p3.82p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted