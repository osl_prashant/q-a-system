Cape Cod group plans events
The Carver, Mass.-based Cape Cod Cranberry Growers Association is celebrating the 200th anniversary of the commercial cranberry industry this year, said Brian Wick, executive director.
The organization has scheduled a harvest tour Sept. 26 at a site in Carver to help state and local officials and industry representatives better understand the cranberry industry, Wick said.
The first cranberry bog was started in Dennis, Mass., on Cape Cod, Wick said. Berries are still produced on the same property, he said.
The association also will host its 13th annual Harvest Day Celebration Oct. 8-9, which is Columbus Day weekend, in Wareham, Mass.
"That's a huge celebration where we get people from all over the world to attend," Wick said.
 
Cranberry committee targets millennials
The Cranberry Marketing Committee, Wareham, Mass., is getting ready for the second annual Friendsgiving Photo Contest, said Michelle Hogan, executive director.
"We are leveraging social media to take advantage of this millennial-driven holiday and have consumers show the ways in which they incorporate cranberries into their celebrations," Hogan said.
This year, the contest will focus more on consumers' fresh fruit purchases, including the idea of buying one bag for now and freezing two for later, Hogan said.
Photos from the first contest will be on display at the Academy of Nutrition and Dietetics Food and Nutrition Conference and Expo in October, she said.
It's an effective program in getting the attention of millennial consumers, said Bob Wilson, managing member of Wisconsin Rapids, Wis.-based The Cranberry Network LLC, which markets fruit grown by Tomah, Wis.-based Habelman Bros. Co.
"There's a lot of tweeting, Facebooking and blogging and this contest is designed to capture this fresh new buyer," he said.
"We want the industry to know we want to invest in the fresh industry. This investment is creating new demand and awareness and drawing attention to health benefits and drawing attention to new recipes, and targeting the younger consumer is a very good step for our industry."
 
Decas expands staff, retail offerings
Michael McManama is wrapping up his first year as president CEO of Carver, Mass.-based Decas Cranberry Products Inc.
McManama succeeded Chuck Dillon, who retired last September.
McManama, who earned a B.S. at Boston College and an MBA from Suffolk University, has been in the food business throughout his 37-year career, including senior management positions from 1985-99 at Lakeville-Middleboro, Mass.-based Ocean Spray Cranberries.
Building staff and expanding retail offerings has dominated McManama's first year, he said.
"New hires and also get the organization to prepare for the future by basically expanding sales into other channels," McManama said, noting that the 80-plus-year-old company has expanded into retail items from its traditional focus on "ingredients."
McManama mentioned Decas' Paradise Meadow sweet and dried cranberry line, which includes reduced-sugar LeanCrans and OmegaCrans, which contain Omega fatty acids.
"It started four or five years ago, but we've increased the desire to get into retail," he said.
Brigitte Crowley joined the company in April as vice president of marketing and business development.
Crowley brought more than 15 years in retail and consumer marketing with various food companies to Decas, which is her first foray into produce.
Crowley, a native of Boston, earned a bachelor of arts degree in international affairs at George Washington University and an MBA at the University of North Carolina.
 
HBF International adds bigger bag
Sheridan, Ore.-based HBF International Inc. is offering a 2-pound bag alongside its usual 12-ounce polybag this season, said Doug Perkins, CEO.
The company is ramping up its marketing efforts on social media, as well, Hurst said.
"We have been working closely with bloggers as well to spread the word and have received some great recipes that we will promote as the season gets started," he said.
Point-of-sale materials are available to retailers, too, Hurst said.
"If a retailer is interested in POS material, we are always happy to talk through that request and provide support where needed," he said.
 
Naturipe Farms rolls out pouch bag
Salinas, Calif.-based Naturipe Farms LLC is introducing a 16-ounce bag to its line of Selections Fresh Grab n' Go cranberry packages for retail during the 2016 fresh berry season. The company already offered a 32-ounce package.
It's part of a company effort to "start a fresh tradition," said Brian Bocock, vice president of product management.
"Retailers will find the new-this-year 2-pound Selections Cranberry Grab n' Go bag easy to merchandise, and consumers will be eager to try it out during the holiday season," Bocock said.
The stand-up pouch features a handle and reseal strip.
 
Sunny Valley boosts acreage
Glassboro, N.J.-based shipper Sunny Valley International Inc. says one of its growing operations has more than doubled its cranberry acreage for this year.
"He has about 194 acres total and probably has about 50-75 for fresh," said Bob Von Rohr, marketing and customer relations manager for Sunny Valley.
The grower, whom Von Rohr did not identify, is in Shamong, N.J., in the southern part of the state.
"A lot of bogs are in South Jersey," Von Rohr said.
 
Wisconsin group plans research center
The Wisconsin Rapids-based Wisconsin Cranberry Growers Association, in conjunction with the Wisconsin Cranberry Research and Education Foundation, is working through the process of securing federal funding to build a research center for cranberries, said Tom Lochner, executive director.
"Hopefully by end of (the) federal fiscal year, we'll have funding in place to allow us to acquire property to establish a research station for genetics, pest management, nutrition and water management," Lochner said.