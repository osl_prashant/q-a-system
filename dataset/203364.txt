Apple marketers are capitalizing on changing technology habits to promote a familiar product.
The messages evolve with the technology - there are video ads, demos, price promotions and nutritional messages, along with information on the newest apple varieties, marketers say.
The Michigan Apple Committee in Lansing now is in its second year of funding from the U.S. Department of Agriculture for an "intensive social media campaign" designed to increase the committee's online exposure to consumers, said Diane Smith, executive director.
"Our focus is on our target audience and what they want to know about - identifying Michigan apples in the marketplace, recipes, usage, cooking inspiration," she said.
Advertising at the Fishers-based New York Apple Association relies predominantly on social media today, said Molly Zingler, marketing director.
The message - buy local - hasn't changed much, she said, even if the delivery mechanism has, Zingler said.
"How we reach consumers has changed significantly," she said. "Our advertising programs now predominantly employ new media advertising channels - online ads, Facebook and the like. That has allowed us to shift to interactive ads, to allow consumers to engage with us - and they are"
Selah, Wash.-based grower-shipper Rainier Fruit Co. is in the second year of carrying the banner as "official apple of the Boston Marathon," and the company has launched a few consumer initiatives that tie into the race, said Andy Tudor, Rainier's business development director.
One is the #runwithrainier campaign.
"That engages consumers in promoting a healthy, active lifestyle," Tudor said. The company has hired Dave McGillivray, the race's director, to provide tips on training and how to deal with injuries.
Online engagement is a powerful tool to connect with consumers, said David Nelley, executive category director for apples and pears with Vancouver, British Columbia-based The Oppenheimer Group.
"We're especially proud of the Envy website, which originated here and is now being repurposed for the apple's global markets," Nelley said.
"It amazes us how many Envy lovers direct one another to where to find the apple in their communities using our Envy locator tool on the site."
The interactive aspect of the web is helpful, as well, said Steve Lutz, vice president of marketing with Wentachee, Wash.-based CMI Orchards.
"Consumers are using the information they get on packaging to let us know how much they like our products, and, if we occasionally get it wrong, they let us know about that, too," he said.
Tony Freytag, CEO of Cashmere, Wash.-based grower-shipper Crunch Pak, said his company has worked to develop an "authentic online relationship" with consumers.
"In addition to using social media channels directly (Facebook, Twitter, Instagram and Pinterest), we established relationships with (bloggers) who helped spread the word about our products and the convenience they bring to the home cook," Freytag said. Crunch Pak also has revamped its website to make it more user-friendly, Freytag said.
Yakima, Wash.-based Sage Fruit Co. recently launched a more interactive company website, said Chuck Sinks, president of sales and marketing.
"With that, we also have started to incorporate a weekly blog for consumers," he said.
Websites seem to be more functional than ever, said John Rice, consultant with Rice Fruit Co. in Gardners, Pa.
"Almost all of them invite interaction with consumers - for us, it has been kind of fun," he said.
Wenatchee, Wash.-based Stemilt Growers LLC makes use of a "devoted blogger network," as well as its own Kitchen Council to promote Stemilt's products, their use and availability, said Roger Pepperl, marketing director.
"We think influencers are important to get consumers to try new items and usage, and their reach is large," he said, noting the company also maintains a full range of social media outlets and has a blog within its own website.
"Digital tools are so important in today's marketplace," Pepperl said.