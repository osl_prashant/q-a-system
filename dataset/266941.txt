Spring calving in North Dakota slightly ahead of average
Spring calving in North Dakota slightly ahead of average

The Associated Press

FARGO, N.D.




FARGO, N.D. (AP) — Spring calving in North Dakota is slightly ahead of the long-term average, at 39 percent complete.
The federal Agriculture Department says in its weekly crop report that cattle and calf conditions are rated 76 percent good to excellent. Cattle and calf death loss is mostly average to light, despite recent heavy, wet snows.
Eighty-six percent of the state's winter wheat crop is rated fair, good or excellent.
About half of the subsoil and topsoil moisture supplies statewide are rated adequate to surplus.