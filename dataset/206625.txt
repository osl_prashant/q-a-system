The Precision Impact Award recognizes retailers who are using precision ag to build their business and help their farmer customers improve their bottom line and environmental footprint.Enter here—Deadline is August 31 
The winner will receive:
·         Two complimentary registrations to the ARA Conference and Expo.
·         Two round-trip airline tickets to the site of the conference, November 28-30, 2017 in Phoenix, AZ.
·         (One) two-night accommodation at the Arizona Biltmore Hotel in Phoenix, AZ.
·         Feature article special section recognition in AgPro magazine highlighting the winning retailer operation with emphasis on precision technology.
·         Award trophy with presentation at the ARA Conference and Expo.
·         Highlight of winner in video during award presentation and on AgPro website.
·         Assistance with news release about accomplishment to local and regional media.
 
Here are more details about the 2016 winners.
Precision Impact Award Winner: Data Deliver Product Insights
Missouri Retailer Says Principles of Precision Pay
Three Retailers Named Recipients of the 2016 Precision Impact Award
 
And you can enter here.