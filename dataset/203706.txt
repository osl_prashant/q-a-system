Grower-shipper Uesugi Farms expects its northern California pepper production to be up this year.
Gilroy, Calif.-based Uesugi, which ships green, red and yellow bells, miniature sweet and hot peppers from the region, began shipping in early July, according to a news release.
Acreage increases are the reason for the expected higher production this season, according to the company.
Green bells and hot peppers started on time this year out of northern California, but mini sweets and red and yellow bells started about a week late.
"Even though the cooler weather last week slowed us down a bit, we're expecting pepper production to speed up thanks to a warmer forecast this week," Pete Aiello, Uesugi's general manager, said Aug. 22.
In northern California Uesugi sources from Gilroy, Morgan Hill, Hollister, Brentwood and Lodi.
Pepper production in the region should run through October or early November, after which Uesugi will begin sourcing from California's Coachella Valley.