SAN DIEGO — Giving kids access to fresh fruits and vegetables in schools through the government’s Fresh Fruit and Vegetable Program is just a step toward a greater goal.
Progress will be seen when obesity rates begin dropping and those kids — and their families — begin buying and eating fresh produce on a regular basis.
Several speakers at the United Fresh Produce Association’s Fresh Start Annual Conference Jan. 16-17 in San Diego told of some early success stories. 
Punam Ohri-Vachaspati, a professor at Arizona State University, said a small study in Phoenix showed significant produce consumption changes in kids who were exposed to many different items through the Fresh Fruit and Vegetable Program.
A study of three schools in the program and three non-participating schools showed kids in the program requested vegetables more than twice as much as the other students and 50% more for fruit. 
“Kids who are exposed to fruits and vegetables nagged their parents for more when they shop,” she said. 
In areas where the program is in place, she said local retailers should coordinate with them to have those items available in stores.
Two retailers on the panel gave examples of community outreach to children.
Mike Orf, assistant vice president of produce operations at Des Moines, Iowa-based Hyvee, said the regional chain has many programs to engage younger consumers, such as store tours, community gardens and free fruit for kids to eat while shopping in the stores. 
“We’ve seen that teens love juices and smoothies,” Orf said.
Jeff Cady, director of produce and floral for Tops Friendly Markets, Buffalo, N.Y., said his regional chain also engages young consumers through field trips and family cooking schools in stores. 
In the schools, Gary Petill, director of food and nutrition services for the San Diego Unified School District, said his district has received 58 salad bars through United’s Salad Bars for Schools program. 
He said kids really like the salad bars for the convenience they bring.
For example, kids won’t peel an orange but they will take and eat sliced orange wedges from the salad bar. 
At the restaurant level, Ype Von Hengst, co-founder and executive chef of Silver Diner, a 15-restaurant chain based in Rockville, Md., said he changed the default side dish on the kids menu from french fries to strawberries and saw strawberries go from 29% to 63% on kids’ orders. 
But he learned he couldn’t just put fruits and vegetables on the menu unless kids tested and approved them because in restaurants, most kids want to order their own food.
He said the entire food system has to work harder to give kids access to healthier food like fruits and vegetables.
“We have a moral obligation to pay it forward for this generation of kids,” Von Hengst said.