<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


KANSAS CITY, Mo. - Liberty Fruit Co. completed its rain-delayed charity golf tournament, which raised $691,200 for local Kansas City charities.
 
It was originally scheduled for May 23 but was postponed by rain and lightning.
 
Company CEO Allen Caviar said the golf course, The National Golf Club, allowed Liberty a make-up date of Aug. 8.
 
Caviar said the Kansas City, Kan.-based produce company's golf tournament has raised $4 million dollars over the years for charity. 
 
Next year's event is scheduled for Sept. 17, and it will be Liberty's 17th annual tournament.