<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


LENEXA, Kan. - Tim Arrington recently joined The Packer as a staff writer. He is reporting on a wide range of fresh produce industry news for the Lenexa company Farm Journal Media acquired from Vance Publishing in December.
Arrington will write special section stories and post them to thepacker.com and will write obituaries and report on personnel moves in the industry.
Arrington graduated with an English degree from Missouri S&T in Rolla, Mo. in 2014. He worked as a graduate teaching assistant during the 2014-15 school year at Pittsburg (Kan.) State University, where he studied poetry.
He left the university after a year to teach and advise high school students in Shanghai, China, and upon his return, moved to Lawrence, Kan. where he lives with his fiancee.
Arrington served as editor-in-chief for The Missouri Miner, S&T's campus newspaper, during his final year as an undergraduate and is working on his third book. In addition to reading and writing, he enjoys researching psychology, philosophy, and Christianity.