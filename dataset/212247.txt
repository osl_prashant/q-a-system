Wenatchee, Wash.-based Stemilt Growers LLC is crediting ideal weather conditions for a good start and big volumes on its Washington cherry crop.
Spokeswoman Brianna Shales said Stemilt’s harvest has started strong and is continuing to build momentum. The Chelan variety is averaging 10-row sizing with “great firmness, green stems and a sweet flavor.” Ten-row is “kind of in the middle” for sizing, Shales said.
Stemilt is scheduled to transition into bings during the week of June 19, Shales said.
“Bings are averaging at an agreeable 9-row with high sugar levels and fantastic firmness,” she said.
The company has started its Early Robin Rainier variety, with an average of 9½-row — a size bigger than 10 — and 21 brix, Shales said. A brix of 21 “is a good sweetness for a dark cherry,” Shales said.
“All varieties have high luster and are shining from the trees until picked in early morning hours,” she said.
Stemilt wrapped up its California crop by mid-June, Shales said.
In all, between California and Washington production, Stemilt’s season is “looking very promising” as it moves into Skylar Rae variety and organic cherries, Shales noted.