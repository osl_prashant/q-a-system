Higher menu prices helps boost Chipotle key sales figure
Higher menu prices helps boost Chipotle key sales figure

The Associated Press

NEW YORK




NEW YORK (AP) — Chipotle, which has been working to revive its business after past food safety scares, says a key sales figure rose as it increased prices in most of its restaurants.
After the results were released, shares of Chipotle Mexican Grill Inc. soared nearly 8 percent in extended trading Wednesday.
The burrito chain says sales rose 2.2 percent at existing locations during the first three months of the year. Analysts expected the figure to rise 1.3 percent, according to FactSet.
Chipotle also reported first-quarter earnings and revenue that beat Wall Street expectations.