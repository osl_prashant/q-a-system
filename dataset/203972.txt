United Food and Commercial Workers reached a tentative contract agreement with Albertsons and Ralphs, averting a possible strike of retail food and meat workers, the union announced Aug. 4.
The deal affects about 50,000 workers in Central and Southern California, who will vote Aug. 8 on whether to ratify it. Their prior contract expired about five months earlier.
Ralphs is a subsidiary of The Kroger Co. Albertsons is owned by Cerberus Capital Management.
Terms were not disclosed. In June union members voted to authorize a strike if a deal wasn't reached by Aug. 8, according to media reports.