Four new applications are compatible with the John Deere 4640 Universal Display and with Gen 4 4600 CommandCenter displays. These four applications provide new levels of machine automation and data sharing.

AutoTrac Turn Automation
AutoTrac Implement Guidance
AutoTrac Vision for Tractors
In-Field Data Sharing 

“These new applications are machine-specific bundled activations with the 4600 CommandCenter and provide late-model John Deere machine owners with outstanding technology value,” says John Misher, precision agriculture product marketing manager with John Deere. For owners of machines equipped with a 4640 Display, the applications are offered as bundled one- or five-year subscriptions.
Activations and subscriptions are available for ordering. Delivery will take place beginning in February 2018.

AutoTrac Turn Automation makes end turns smooth, consistent and comfortable for operators during tillage, planting, seeding or other pre-emerge applications when using straight-track guidance modes. When AutoTrac Turn Automation is activated, the machine functions previously required at the end of the field, when operating drawn implements, no longer require user input. The new application for tractors provides automation across the field rather than just between headlands. 

AutoTrac Implement Guidance (passive) enables the tractor to move off the intended path or guidance line in order to achieve expected accuracy of the implement. AutoTrac Implement Guidance is ideal for first-pass tillage, planting, seeding, strip till or other applications with drawn implements when using straight- or curve-tracking modes and when operating on hillsides. Differential-correction signals can be shared between the receiver on the tractor and the implement. 
AutoTrac Vision Guidance was previously released for John Deere 30-Series and newer sprayers. Now, Deere is expanding the application to include 7X30 large-frame, 8X30 and 8X30T, 7R and 8R/8RT tractors. AutoTrac Vision can be used in post-season crop applications to detect the crop row and provide input to the machine’s AutoTrac system to keep the tractor’s wheels or tracks between the crop rows. This level of precision can be beneficial when side-dressing fertilizer, post-emerge spraying and cultivating.

In-Field Data Sharing makes it easier for producers to coordinate multiple machines working in the same field. Operators can use the application to share coverage, application, yield and moisture maps along with straight tracks and circle tracks with up to six other machines. The application helps machines to work together more efficiently, reducing skips, overlap, fuel and input costs for producers.