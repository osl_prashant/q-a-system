Bayer Animal Health today announced that Brittany Martabano, a senior from the University of Florida College of Veterinary Medicine, is the national winner of the Bayer Excellence in Communication Award (BECA). Brittany was selected from entries representing 27 veterinary schools, which were awarded a total of $70,000 in scholarship funds through the 2016 competition. Over the last four years, Bayer has provided a total of $232,500 in scholarship awards through the BECA communication program.Incorporating effective communication skills in veterinary practice, as well as other medical professions, has long been recognized as one of the main tasks in delivering comprehensive medical care to patients.1 BECA, established to recognize effective communication in the veterinary profession, rewards veterinary students who are executing this critical core skill that needs to be taught and learned to the same degree as other clinical skills.
The competition challenged students to submit a filmed interview between themselves and a veterinary client in a clinical setting. A panel of faculty judges at each participating school selected a winner using a scorecard developed by nationally-renowned veterinary faculty specializing in communication. Each school-level winner received a $2,500 scholarship. Twenty-seven schools submitted a video of their winner to compete for the additional $2,500 national scholarship award, which was selected by an independent judging panel.
National winner says BECA represents core of veterinary medicineIn the submission that won her the national award, Brittany exhibited exemplary communication skills from the moment she opened the exam room door. She was empathetic, clarified her understanding of the pet's health problems and included the pet owner in the development of a plan to handle the issue.
While filming her BECA video, Brittany reinforced her belief that client communication is both the most important and most challenging part of being a veterinarian. Practicing medicine can be straightforward, she says, while talking pet owners through medical explanations and difficult situations always requires patience and adaptability.
"I was excited to win the BECA award, because my interest in communications is one of the reasons I chose to go into veterinary medicine," Brittany said. "Pet owners need to understand what their veterinarian is doing in the exam room, as well as what steps they need to take at home to keep their pets healthy. If I can't communicate that information to them, then I'm setting them up for failure as pet owners."
After graduation, Brittany will implement her skills during a rotating small animal internship at the North Carolina State University Veterinary Hospital. From there, she hopes to complete an ophthalmology residency.
Dr. Amy Stone, one of Brittany's professors at the University of Florida College of Veterinary Medicine, explains that veterinarians use communication more than any other skill. The BECA award and corresponding Bayer Communication Project training modules have helped Dr. Stone teach this critical skill to students. "Our participation in BECA helps illustrate that we are making communication a priority in our curriculum," says Dr. Stone, Clinical Assistant Professor in the Department of Small Animal Clinic Sciences. "Over the course of their careers, veterinarians engage in thousands of conversations with pet owners. Since our patients can't speak for themselves, we must be able to effectively speak with our patients' owners to get the information needed to provide the best care."
Bayer shares this belief in the importance of communication and is proud to support to veterinary schools. "Future veterinarians like Brittany Martabano and the other BECA award participants are completely committed to the health of animals, both pets and livestock," said Joyce Lee, president, Bayer Animal Health, North America. "We share that dedication and it guides everything we do at Bayer, from the products we make to the array of resources we provide for veterinarians. Supporting education about effective client communication is a top priority, because it's vital to furthering veterinary medicine and keeping animals healthy."
A long-standing commitment to strengthening veterinary communication The Bayer Excellence in Communication Award (BECA) is one facet of a larger initiative aimed at advancing the communication skills of the next generation of veterinarians. In 2002, Bayer partnered with the Institute for Healthcare Communication to establish the Bayer Communication Project. This collaborative partnership resulted in communication skills training modules offered to colleges of veterinary medicine for incorporation into their curriculum. Selected faculty from all U.S. veterinary colleges have been trained via the Bayer Communication Project "Train the Trainer" program.
The University of Florida College of Veterinary Medicine will receive complimentary tuition valued at $5,500 for one faculty member to attend an upcoming Bayer Communication Project "Train the Trainer" course developed and taught by the Institute for Healthcare Communication. Bayer has provided over $20,000 in complimentary tuition to BECA award winners' schools over the past four years.
The school-level veterinary student winners in the 2016 Bayer Excellence in Communication Award program are:
¬? Tommy Poole, Auburn University
¬? Dan Foster, University of California-Davis
¬? Kayle Austin, Colorado State University
¬? Melanie Friedman, Cornell University
¬? Brittany Martabano, University of Florida (national winner)
¬? Jennifer Munhofen, University of Georgia
¬? Chelsea Bohner, University of Illinois
¬? Jeni Nezerka, Kansas State University
¬? Jenna Terry, Louisiana State University
¬? Dana Goddard, Michigan State University
¬? Hannah Able, University of Minnesota
¬? Meggan Hood, Mississippi State University
¬? Brandon Thornberry, University of Missouri
¬? Annie Chavent, North Carolina State University
¬? Lauren Jaworski, The Ohio State University
¬? Patricia Baker, Oklahoma State University
¬? Kimberly Allsopp, Oregon State University
¬? Christopher Robinette, Purdue University
¬? Jenna Lanagan, Ross University
¬? Theresa Blakely, University of Tennessee
¬? Paul Belnap, Texas A&M University
¬? Caroline McKinney, Tufts University
¬? Sharon Weatherspoon, Tuskegee University
¬? Kathryn Lacy, Virginia Tech
¬? Daniel Harmon, Washington State University
¬? Alina Amaral, Western University
¬? Allison Clarke, University of Wisconsin