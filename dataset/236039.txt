The 23rd annual Commodity Classic began this week in Anaheim, Calif., bringing producers from across the country to the U.S.’s largest farmer-led trade show.

Before the convention opened, Bayer hosted its 2018 AgVocacy Forum, inviting industry leaders to discuss the challenges to agriculture.

At the forum, Bayer shared the results of a consumer survey that identifies conflicts and agreements between farmers and consumers.

The survey showed 95 percent think advancements will help grow more food, but less than half support developing plants that produce a higher yield.

“We’re not in a very good place to be brutally honest,” said Rob Schrick of Bayer North America Corn & Soybean Business. “The general public, the consumers, they’re really afraid of the technology that growers use to grow these crops.”

Panelists at the event included Jayson Lusk, Farm Economist, Professor and Head of Department of Agricultural Economics at Purdue University; Cherryh Cansler, Editor at Fast Casual; and Dr. Tim Anglea of The Coca-Cola Company.