The stock market has been ticking along, but there are signs it’s starting to slow.

According to Jim McCormick, senior broker and market commentator of Allendale, Inc., that growth is starting to slow. Some of it is due to the Federal Reserve raising interest rates, which he says is a red flag for the stocks.

“The stock market is definitely a little bit jittery about trade tariffs, trade wars—that’s never good for companies,” he said on AgDay.

This weakness in the stock market, he hopes, will bring some money back into commodities. 

“As we get a little bit of this inflationary push into the market, that should hopefully help us either support various prices or propel the corn and wheat market higher,” said McCormick. 

Hear McCormick explain why the weaker stocks means more interest in commodities on AgDay above.