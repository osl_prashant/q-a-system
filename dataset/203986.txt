When USDA's weekly crop condition ratings are plugged into Pro Farmer's weighted (by production) Crop Condition Index (CCI; 0 to 500 point scale, with 500 representing perfect), the corn and soybean crop shifted marginally lower to 359.95 points and 349.14 points, respectively.





Pro Farmer Crop Condition Index






Corn




This week



Last week



Year-ago





Colorado (1.03%*)

4.00

3.89


3.85




Illinois (15.40%)

53.60

55.45


63.16




Indiana (6.64%)

22.71

23.04


26.81




Iowa (17.72%)

63.08

62.91


68.20




Kansas (4.29%)

15.16

14.90


14.70




Kentucky (1.57%)

6.09

6.15


6.35




Michigan (2.35%)

8.31

8.29


8.69




Minnesota (9.66%)

38.16

37.96


38.31




Missouri (3.81%)

13.71

13.90


13.99




Nebraska (11.62%)

42.43

42.20


45.38




N. Carolina (0.71%)

2.75

2.75


2.74




N. Dakota (2.70%)

8.95

8.76


9.71




Ohio (3.80%)

13.84

13.65


13.68




Pennsylvania (0.98%)

4.27

4.27


3.78




S. Dakota (5.62%)

17.30

16.51


19.40




Tennessee (0.89%)

3.74

3.69


3.51




Texas (2.06%)

8.19

8.13


6.96




Wisconsin (3.61%)

13.75

13.78


14.59




Corn total


359.95


360.17


387.56









Pro Farmer Crop Condition Index






Soybeans




This week



Last week



Year-ago





Arkansas *(3.78%)

14.13

14.28


14.35




Illinois (13.85%)

48.90

49.87


55.14




Indiana (7.41%)

25.49

25.87


28.91




Iowa (13.35%)

46.60

45.93


52.66




Kansas (3.96%)

13.82

13.67


13.75




Kentucky (2.14%)

8.15

8.17


8.68




Louisiana (1.59%)

6.06

6.19


6.08




Michigan (2.38%)

8.41

8.27


8.65




Minnesota (8.82%)

33.62

33.62


33.97




Mississippi (2.38%)

9.24

9.17


9.38




Missouri (5.86%)

21.03

21.27


21.91




Nebraska (7.46%)

26.72

26.57


29.13




N. Carolina (1.50%)

5.63

5.66


5.82




N. Dakota (5.24%)

17.19

16.88


17.80




Ohio (6.14%)

21.50

21.50


21.83




S. Dakota (5.93%)

18.81

18.33


20.43




Tennessee (1.86%)

1.00

1.00


1.00




Wisconsin (2.29%)

8.77

8.87


8.72




Soybean total


349.14


349.16


375.04




* denotes percentage of total national corn crop production.
Iowa: Topsoil moisture levels improved to 19% very short, 31% short, 49% adequate and 1% surplus. Topsoil moisture levels in south central and southeast Iowa remained more than 90% short to very short. Subsoil moisture levels rated 22% very short, 34% short, 44% adequate and 0% surplus.
Seventy-eight percent of the corn crop was in or beyond the dough stage, one week behind last year. Twenty-one percent of the corn crop has reached the dent stage, one week behind last year and five days behind the five-year average. Sixty-one percent of the corn crop was rated in good to excellent condition. Eighty-eight percent of soybeans were setting pods, four days behind last year but equal to average. Soybean condition improved slightly to 58% good to excellent.
Illinois: Drier weather prevailed across most of the state last week. Topsoil moisture supply was rated at 9% very short, 41% short, 49% adequate and 1% surplus. Subsoil moisture supply was rated at 8% very short, 38% short, 53% and 1% surplus. Corn dented was at 41% compared to 46% for the five-year average. Corn condition was rated 4% very poor, 10% poor, 32% fair, 42% good and 12% excellent. Soybean coloring was at 4%. Soybean condition was rated 3% very poor, 9% poor, 28% fair, 52% good and 8% excellent.
Indiana: Corn was 78% in dough in the North, 81% in Central and 86% in the South. Corn s 41% dented in the North, 37% in Central, and 43% in the South. Con rated in good to excellent condition was 56% in the North, 50% in Central, and 51% in the South. Soybeans were 87% setting pods in the North, 88% in Central, and 83% in the South. Soybeans rated in good to excellent condition were 58% in the North, 51% in Central, and 51% in the South.
Minnesota: Topsoil moisture supplies rated 3% very short, 12% short, 70% adequate and 15% surplus. Subsoil moisture supplies rated 3% very short, 13% short, 75% adequate and 9% surplus.
Seventy-two percent of the corn crop was at or beyond the dough stage, eight days behind last year. Thirteen percent of the corn reached the dent stage. Corn crop condition rated 82% good to excellent. Ninety-one percent of the soybean crop was setting pods, one day ahead of normal. Soybean condition remained at 74% good to excellent. Forty-two percent of the spring wheat crop was harvested, eight days behind average.