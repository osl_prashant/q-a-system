The United Fresh Produce Association aims to provide attendees of its rebranded conference a more customized experience this year.The event, split into FreshMKT and FreshTEC, includes parallel programming for two large segments of the industry, marketing and technology.
“I’d say the biggest advantage is it really lets people know that there’s something there specifically for them and their own operation,” said Tom Stenzel, president and CEO of Washington, D.C.-based United Fresh.
“It really helps people understand where they fit in, so we’ve been real pleased with the response to that.”
The action kicks off June 13 with keynote conferences for each expo — Taylor Farms CEO Bruce Taylor for FreshTEC, Ready Pac Foods CEO Tony Sarsam for FreshMKT.
 

“Both are really outstanding leaders in our industry overall, but in the two specific areas (also),” Stenzel said.
“For Ready Pac, Tony has really driven that company to be an innovator in fresh foods, so a lot of their products now go far beyond produce — their salad bowls, their different types of meal solutions, bringing together proteins with produce. It’s really kind of the new age of our industry, growing into being a fresh foods company, not just a fresh produce company.
“On the other side, Bruce Taylor, who’s done much of that same stuff, his company has embraced new technology probably faster than any other company in our business, whether it’s at the field level in automated harvesting or (in the) packing, processing facilities,” Stenzel said.
“So Bruce is just very much out on the cutting edge when it comes to technology.”
On the FreshMKT side, following the keynote will be sessions on consumer behavior, which entities influence food trends and how to identify those trends early.
The programming will end with what the United Fresh website described as its “crystal ball session” featuring a panel of millennial and Generation Z consumers.
Those panelists will talk about what they look for in food, where and how they want to buy it, and what kind of engagement they want from the companies that produce it.
On the FreshTEC side, Taylor’s presentation will precede sessions on automation, advances in testing for pathogens, controlled-environment growing and on how produce companies can partner with technology companies to develop solutions for their needs.
The FreshTEC programming will also include a university showcase, where representatives of those institutions will share their projects in agricultural technology.
United Fresh’s emphasis on the operations side of the industry will be a distinguishing feature of the show, Stenzel said.
“There’s nothing else quite like it,” Stenzel said. “There are a lot of produce shows around, but in this case, this will certainly be the biggest technology footprint of anything in the entire industry ... It really is going to be a pretty cool thing for folks in the industry to see.”
A joint general session, which is open to attendees of United Fresh as well as the co-located Global Cold Chain Expo and International Floriculture Expo, will take place June 14 — just before the show floor opens — and will feature Doug Rauch, former president of Trader Joe’s.
 

On the floor

One feature of the FreshTEC expo will be the FutureTEC Zone, where more than 40 startup companies will exhibit their products and services for the produce industry.

“This is something for anybody who’s looking to adopt technology, whether it’s in their warehouse, in their harvesting, in their growing operations, transportation, food safety,” said Mary Coppola, senior director of marketing communications for United Fresh.
“This is where you’re going to find those new technology solutions.”
The FreshTEC expo will also have a new food safety pavilion, which companies can visit if they are looking to ramp up their safety standards or if they have questions about Food Safety Modernization Act compliance.
Debuting on the FreshMKT part of the expo floor will be the organic showcase, which highlights products from participating companies so that buyers looking for organic items can browse a large selection and then target companies accordingly for further conversation.
“We know that buyers’ time on the floor is precious time,” Coppola said.
“We can make it easier for them to find exactly what they’re looking for instead of having them walk up and down every aisle or explore a show guide looking for an organic product.”
Coppola said the showcase has an upside for exhibitors, too.
“It’s a way for the exhibitor to be able to have a presence in multiple places at one time, and to display their organic products, which, as we all know, is such a growing segment of the industry right now,” Coppola said.
Other highlights at the event will be learning exchanges near the show floor, where industry members will facilitate discussions on different topics of interest.
“The idea kind of grows out of (the fact that) today’s millennials really do interact with each other in nontraditional ways, the idea that old-fashioned workshops don’t appeal to everybody,” Stenzel said.
“People want to talk instead of just sitting and listening. We really want to drive engagement by all of the participants.”
Learning centers on the show floor will provide educational opportunities as well.
 

Meet and greet

Networking events will include the opening night party June 13 at the Shedd Aquarium, which will be open to United Fresh and Global Cold Chain Expo attendees.
“We’re hoping to increase the opportunities for those two communities to start networking,” Coppola said. “It’s a great event.”
The new Hygienic Design Summit, June 14, will combine education and networking.
Facility and equipment designers, food safety experts and produce packers and processors are expected to attend to hear a presentation by Joe Stout, founder of Commercial Food Sanitation, that will be followed by a reception.
That event is jointly hosted by United Fresh, Global Cold Chain Expo and International Association for Cold Storage Construction.
Closing the overall event June 15 will be the Chairman’s Reception and the Retail-Foodservice Celebration Dinner, the latter of which honors selected retail produce managers as well as chefs.