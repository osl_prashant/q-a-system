AP-Deep South News Digest
AP-Deep South News Digest

The Associated Press



Good evening! Here's a look at how AP's news coverage is shaping up today in the Deep South. Questions about today's coverage plans are welcome and should be directed to:
The Atlanta AP Bureau at 404-522-8971 or apatlanta@ap.org
The Montgomery AP Bureau at 334-262-5947 or apalabama@ap.org
The New Orleans AP Bureau at 504-523-3931 or nrle@ap.org
The Jackson AP Bureau at 601-948-5897 or jkme@ap.org
Deep South Editor Jim Van Anglen can be reached at 1-800-821-3737 or jvananglen@ap.org. Administrative Correspondent Rebecca Santana can be reached at 504-523-3931 or rsantana@ap.org. A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
GEORGIA (All times Eastern):
TOP STORIES:
TRAVEL-MLK50
MEMPHIS, Tenn. — Fifty years ago, the Rev. Martin Luther King Jr. was killed while standing on the balcony of the Lorraine Motel in Memphis, Tennessee. The civil rights leader's shocking murder on April 4, 1968, marked one of the most significant moments in U.S. history. The city's role in the civil rights movement and King's death has long made it a destination for anyone interested in King's legacy. Museums, churches and even iconic Beale Street tell the story of King's final days here. By Adrian Sainz. SENT: 987 words.
AP Photos NYLS221-0119091520, NYLS222-0328180703, NYLS225-0326171022, NYLS229-0328180703, NYLS226-0308171522, NYLS224-0328180703, NYLS228-0122181143, NYLS220-1106030703, NYLS227-0116171759, NYLS223-1103141614.
CDC DIRECTOR
NEW YORK — The new director of the top U.S. public health agency on Thursday pledged to work to bring the nation's opioid epidemic "to its knees" and said he believes the AIDS epidemic could be ended in three to seven years. Dr. Robert Redfield Jr. made the comments at a staff meeting of the Centers for Disease Control and Prevention in Atlanta. By Mike Stobbe. SENT: 683 words.
AP Photos NY438-0918120000.
CHINESE DRYWALL
NEW ORLEANS — Nine years after a judge began supervising a multitude of federal lawsuits that claim homes were damaged by Chinese drywall, he says it's time to return thousands of remaining cases for trial in the courts where they were filed. The panel that put Judge Eldon Fallon in charge in 2009 said the first 1,700 would return to Florida's federal courts if no objections are filed by Thursday. At least three objection notices were filed Thursday by attorneys for about 750 clients. Objectors' briefs must be filed by April 12, with responses are due May 3, according to the U.S. Judicial Panel on Multidistrict Litigation's electronic docket. By Janet McConnaughey. SENT: 787 words.
IN BRIEF:
— ACCOUNTANT-FRAUD — Prosecutors say an Atlanta accountant falsified payroll records to embezzle more than $1.5 million from her employer for six years.
— SOLAR JOBS-FLORIDA —The Solar Foundation says solar jobs are on the rise in the Sunshine State.
— AMTRAK DERAILMENT-NORTH CAROLINA — An Amtrak train carrying automobiles and passengers derailed in North Carolina, but officials say there were no injuries.
— RENASANT ACQUISITION — A Mississippi bank is buying a Georgia bank for $453 million in cash and stock.
— SEVERE WEATHER — Authorities say a tornado touched down in Texas, damaging homes but causing no injuries on Wednesday, and that severe weather continues Thursday to pose a threat of dangerous wind gusts, lightning and flooding as the storms move eastward.
— FOOTBALL COACH-SEXUAL ASSAULT — A former metro Atlanta high school teacher and football coach is accused of having sexual contact with two students.
— DOGFIGHTING RING — Authorities say 63 pit bulls have been rescued from a dogfighting ring in Georgia.
SPORTS:
BBN--PHILLIES-BRAVES
ATLANTA — The new-look Philadelphia Phillies send Aaron Nola to the mound on opening day against Julio Teheran of the Atlanta Braves, who are biding time until the arrival of top prospect Ronald Acuna in a couple of weeks. By Paul Newberry. UPCOMING: 700 words, photos. Game starts at 4:10 p.m. EDT.
BBN--PHILLIES-ARRIETA
ATLANTA — Philadelphia pitcher Jake Arrieta has been assigned to Class A Clearwater while working into shape for his Phillies debut. Since the 2015 NL Cy Young Award winner got a late start in spring training, finalizing a $75 million, three-year deal March 12, he will stay in Florida to pitch simulated games before making his first start for Philadelphia on April 8. SENT: 137 words.
AP Photos FLJR102-0327181308, FLJR101-0327181306.
FBC--HALL OF FAME-PEACH BOWL
ATLANTA — Peach Bowl president Gary Stokan still sees the College Football Hall of Fame as the key to Atlanta's goal of becoming the "capital of college football," and his bowl has invested an additional $8 million toward that vision. The Peach Bowl on Thursday extended its partnership with the Hall of Fame for 10 years with the new investment. The $8 million follows the bowl's original $5 million commitment when the hall opened in 2014. By Charles Odum. SENT: 417 words.
AP Photos NY901-0823141006.
GLF--MASTERS
This Masters is as much about a red shirt as a green jacket. Tiger Woods is back for only the second time in the last five years, and what makes the sight of him at Augusta National even more tantalizing is that Woods is starting to look like the player who dominated golf for nearly 15 years. By Doug Ferguson. SENT: 1,082 words.
AP Photos NYPS302-0331100800, NY175-0410051909, NYPS301-0318181754, NY174-0410051833, NY177-0311182048, NY176-0309181609.
ALABAMA (All Times Central)
TOP STORIES:
XGR-SESSION END
MONTGOMERY, Ala. — Alabama lawmakers had vowed to steer clear of controversy in an election year session. They found it anyway. They ended the 2018 legislative session Thursday amid last-minute disputes over ethics legislation, the demise of a racial profiling bill and some sniping between the House of Representatives and Senate over the pace of votes. By Kim Chandler and Mallory Moench. SENT: 520 words.
— With: XGR-SESSION END-GLANCE
XGR-SESSION END-RACIAL PROFILING
MONTGOMERY, Ala. — A racial profiling bill - a top priority of African-American lawmakers this year - died on the last day of the Alabama legislative session Thursday when it did not get a vote in the House. The bill - approved unanimously by the Alabama Senate - stalled in the House due to Republican opposition. The Legislature adjourned Thursday without voting on the bill, effectively killing a bill that would have required police to compile data on the reasons for traffic stops and the races of the motorist who was stopped. The purpose was to determine whether police are unfairly targeting black motorists when they decide to pull vehicles over. By Kim Chandler and Mallory Moench. SENT: 404 words.
XGR-SHROUD AWARD
MONTGOMERY, Ala. — Alabama Rep. Ritchie Whorton's proposal to turn on headlights from sunrise to sunset — a half-hour earlier than current law — won the House's annual "Shroud Award" for the session's deadest bill Thursday. The award is a House tradition dating back 40 years. On the final day each year, the legislative body gives the winner a black suit mounted on cardboard and a bottle of embalming fluid. The resolution bestowing the award pokes fun — from light-hearted to biting — at the failed bills. By Mallory Moench. SENT: 290 words.
XGR--ETHICS BILL
MONTGOMERY, Ala. — Alabama lawmakers voted Thursday to exempt economic developers from the state law governing lobbyists, an exemption that critics painted as creating a wide loophole in the state ethics law. The measure had narrowly cleared the Alabama Senate on a 15-14 vote. The House of Representatives on Thursday gave final approval to the bill, voting 52-22 to accept Senate changes. By Kim Chandler. SENT: 290 words.
CHINESE DRYWALL
NEW ORLEANS — Nine years after a judge began supervising a multitude of federal lawsuits that claim homes were damaged by Chinese drywall, he says it's time to return thousands of remaining cases for trial in the courts where they were filed. The panel that put Judge Eldon Fallon in charge in 2009 said the first 1,700 would return to Florida's federal courts if no objections are filed by Thursday. At least three objection notices were filed Thursday by attorneys for about 750 clients. Objectors' briefs must be filed by April 12, with responses are due May 3, according to the U.S. Judicial Panel on Multidistrict Litigation's electronic docket. By Janet McConnaughey. SENT: 767 words.
WATER PARK FATALITY-CHARGES
TOPEKA, Kan. — Ken Martin remembers a Travel Channel episode about the world's tallest waterslide. The Richmond, Virginia-based amusement-park safety expert said he was horrified that test sandbags flew off the ride and that nylon straps with Velcro restrained multiple riders on rafts dropping 17 stories at up to 70 miles per hour. "I am sitting on my couch and I am thinking, 'Oh my God. What are these people thinking?'" Martin recalled in an interview. "I started praying for anyone who got on that ride." By John Hanna. SENT: 911 words.
AP Photos NYHK104-0709141236, CER204-0111171450, MOKAS501-0327181039.
TRAVEL-MLK50
MEMPHIS, Tenn. — Fifty years ago, the Rev. Martin Luther King Jr. was killed while standing on the balcony of the Lorraine Motel in Memphis, Tennessee. The civil rights leader's shocking murder on April 4, 1968, marked one of the most significant moments in U.S. history. The city's role in the civil rights movement and King's death has long made it a destination for anyone interested in King's legacy. Museums, churches and even iconic Beale Street tell the story of King's final days here. By Adrian Sainz. SENT: 987 words.
AP Photos NYLS221-0119091520, NYLS222-0328180703, NYLS225-0326171022, NYLS229-0328180703, NYLS226-0308171522, NYLS224-0328180703, NYLS228-0122181143, NYLS220-1106030703, NYLS227-0116171759, NYLS223-1103141614.
IN BRIEF:
— AUTISTIC CHILD-RAPE — An Alabama man and his girlfriend have been convicted on multiple charges after prosecutors say he directed the woman to rape his 11-year-old autistic son because he thought the child was gay.
— XGR-IGNITION INTERLOCK PASSAGE — Stricter regulations will be imposed on drunk drivers in Alabama — a bill to close a loophole in state law passed a final vote in the House on the last legislative day.
— SCHOOL THREATENED — A Florida teenager is accused of threatening a school shooting in Alabama.
— USS MANCHESTER — A U.S. Navy combat ship named for New Hampshire's largest city will be commissioned on May 26.
LOUISIANA (All Times Central)
TOP STORIES:
SEXUAL MISCONDUCT-LOUISIANA
BATON ROUGE, La. — Louisiana is paying $85,000 to settle claims that a former top aide to Gov. John Bel Edwards sexually harassed a woman when they worked together in the governor's office. The governor's office provided a copy of the taxpayer-financed settlement ending the legal allegations against Johnny Anderson by a former state employee to The Associated Press on Thursday. The document was signed and completed Tuesday. By Melinda Deslatte. SENT: 627 words.
POLICE SHOOTING-LOUISIANA
BATON ROUGE, La. — The investigation of a deadly police shooting that inflamed racial tensions in Louisiana's capital city has ended without criminal charges against two white officers who confronted a black man outside a convenience store two summers ago. But experts in police tactics think the bloodshed could have been avoided if the Baton Rouge officers had done more to defuse the encounter with Alton Sterling. They say poor police tactics and techniques may have aggravated the volatile confrontation, which lasted less than 90 seconds. By Michael Kunzelman. SENT: 887 words.
AP Photos NY107-0503170224, NY108-0706161912
XGR-PAY ISSUES
BATON ROUGE, La. — Louisiana's minimum wage will remain at the lowest amount allowed by federal law, lawmakers decided Thursday. They also refused to enact a requirement that state contractors pay men and women equal wages. There was little debate before the demise of the bills that would have implemented a $15 an hour minimum wage and mandated companies with state contracts to pay male and female employees doing similar jobs the same amount of money. By Anthony Izaguirre. SENT: 448 words.
CHINESE DRYWALL
NEW ORLEANS — Nine years after a judge began supervising a multitude of federal lawsuits that claim homes were damaged by Chinese drywall, he says it's time to return thousands of remaining cases for trial in the courts where they were filed. The panel that put Judge Eldon Fallon in charge in 2009 said the first 1,700 would return to Florida's federal courts if no objections are filed by Thursday. At least three objection notices were filed Thursday by attorneys for about 750 clients. Objectors' briefs must be filed by April 12, with responses are due May 3, according to the U.S. Judicial Panel on Multidistrict Litigation's electronic docket. By Janet McConnaughey. SENT: 787 words.
XGR-LEGISLATURE-NO CUTS
BATON ROUGE, La. — Staring down a budget shortfall that hits in July, Louisiana's legislative leaders aren't recommending cuts to their agencies but rather a nearly $12 million increase to their bottom line. The budget proposal for legislative agencies, filed Thursday by House Speaker Taylor Barras, would spend $96.4 million in the 2018-19 financial year that begins July 1. That would be bumped up from the $84.8 million allocated this year for the House, Senate, legislative auditor and other offices that work for lawmakers. By Melinda Deslatte. SENT: 486 words.
WATER PARK FATALITY-CHARGES
TOPEKA, Kan. — Ken Martin remembers a Travel Channel episode about the world's tallest waterslide. The Richmond, Virginia-based amusement-park safety expert said he was horrified that test sandbags flew off the ride and that nylon straps with Velcro restrained multiple riders on rafts dropping 17 stories at up to 70 miles per hour. "I am sitting on my couch and I am thinking, 'Oh my God. What are these people thinking?'" Martin recalled in an interview. "I started praying for anyone who got on that ride." By John Hanna. SENT: 911 words.
AP Photos NYHK104-0709141236, CER204-0111171450, MOKAS501-0327181039.
XGR-LOUISIANA GAMBLING
BATON ROUGE, La. — The Louisiana House agreed Thursday to a 30-year contract extension for Harrah's to operate the land-based casino in New Orleans. Lawmakers voted 78-12 for the measure keeping Harrah's in place at the casino until 2054. The sponsor is Republican House Speaker Taylor Barras. SENT: 181 words.
IN BRIEF:
— PLATFORM EXPLOSION-GUILTY PLEA — A contract worker has pleaded guilty to violating the federal Clean Water Act in connection with a deadly oil platform explosion off Louisiana's coast.
— PRISON STABBING — A man has been convicted of attempted second-degree murder for stabbing two Louisiana prison employees back in 2017.
— SEVERE WEATHER —Authorities say a tornado touched down in Texas, damaging homes but causing no injuries on Wednesday, and that severe weather continues Thursday to pose a threat of dangerous wind gusts, lightning and flooding as the storms move eastward.
— LOUISIANA FATAL WRECK — A 14-year-old driving a car down a highway in the middle of the night is under arrest after police said he collided with a motorcycle, killing both of its riders.
— OFFICER-DOMESTIC ABUSE — Authorities in Louisiana are crediting a 3-year-old girl with trying to save her mother from being choked by her boyfriend, who happens to be a police officer for the city of Monroe.
— WEST MONROE MAYOR — A longtime Louisiana mayor was hospitalized this week, just before losing an election.
— POLICE CRISIS TRAINING —The New Orleans Police Department is marking the graduation of its latest group of officers trained to recognize and deal with people who have mental health disorders.
SPORTS:
BKN--LEBRON-PASSING JORDAN
CLEVELAND — A generation of kids wanted to be like Michael Jordan. They bought his red-and-black Nikes and sported his No. 23 Bulls jersey. They mimicked Jordan's spin move and fadeaway jumper and even wagged their tongues the way he did on a flight to the rim. While millions worshipped Jordan, only a handful entered his rarefied air. By Tom Withers. SENT: 840 words.
AP Photos NCCB111-0328180808, NCCB108-0328180727, NCCB116-0328180808, NCCB101-0328180652.
MISSISSIPPI (All Times Central)
TOP STORIES:
MISSISSIPPI AGRICULTURE COMMISSIONER
JACKSON, Miss. — Mississippi Gov. Phil Bryant on Thursday appointed a third-term state lawmaker to be the state's new agriculture commissioner. Rep. Andy Gipson will succeed fellow Republican Cindy Hyde-Smith once she moves to the U.S. Senate. He will serve the rest of the current commissioner's term, which ends in January 2020. By Sarah Mearhoff. SENT: 520 words.
AP Photos MSRS108-0329181409, MSRS105-0329181437, MSRS106-0329180000, MSRS101-0329181435.
MISSISSIPPI LEGISLATURE-ANALYSIS
JACKSON, Miss. — Leaders of the Republican-dominated Mississippi Legislature put only a few big items on their to-do list for 2018, and two of the biggest — education and transportation— remained unresolved when the nearly three-month session was gaveled to a close Wednesday. The House and Senate completed two must-do items: They wrote a $6.1 billion state budget, and agreed on a plan to keep the Medicaid program in operation once the new budget year begins July 1. By Emily Wagster Pettus. SENT: 672 words.
AP Photos MSRS117-0328181502, MSRS111-0328181122
CHINESE DRYWALL
NEW ORLEANS — Nine years after a judge began supervising a multitude of federal lawsuits that claim homes were damaged by Chinese drywall, he says it's time to return thousands of remaining cases for trial in the courts where they were filed. The panel that put Judge Eldon Fallon in charge in 2009 said the first 1,700 would return to Florida's federal courts if no objections are filed by Thursday. At least three objection notices were filed Thursday by attorneys for about 750 clients. Objectors' briefs must be filed by April 12, with responses are due May 3, according to the U.S. Judicial Panel on Multidistrict Litigation's electronic docket. By Janet McConnaughey. SENT: 787 words.
CROP REPORT
DES MOINES, Iowa — Corn has been dethroned as the king of crops as farmers reported Thursday they intend to plant more soybeans than corn for the first time in 35 years, the U.S. Department of Agriculture said in its annual prospective planting report. Profitability is the primary reason farmers indicate they intend to plant 89 million acres in soybeans and 88 million acres in corn. By David Pitt. SENT: 585 words.
AP Photos NYSP101-0412171855, KSWIE501-0913170434.
TRAVEL-MLK50
MEMPHIS, Tenn. — Fifty years ago, the Rev. Martin Luther King Jr. was killed while standing on the balcony of the Lorraine Motel in Memphis, Tennessee. The civil rights leader's shocking murder on April 4, 1968, marked one of the most significant moments in U.S. history. The city's role in the civil rights movement and King's death has long made it a destination for anyone interested in King's legacy. Museums, churches and even iconic Beale Street tell the story of King's final days here. By Adrian Sainz. SENT: 987 words.
AP Photos NYLS221-0119091520, NYLS222-0328180703, NYLS225-0326171022, NYLS229-0328180703, NYLS226-0308171522, NYLS224-0328180703, NYLS228-0122181143, NYLS220-1106030703, NYLS227-0116171759, NYLS223-1103141614.
WATER PARK FATALITY-CHARGES
TOPEKA, Kan. — Ken Martin remembers a Travel Channel episode about the world's tallest waterslide. The Richmond, Virginia-based amusement-park safety expert said he was horrified that test sandbags flew off the ride and that nylon straps with Velcro restrained multiple riders on rafts dropping 17 stories at up to 70 miles per hour. "I am sitting on my couch and I am thinking, 'Oh my God. What are these people thinking?'" Martin recalled in an interview. "I started praying for anyone who got on that ride." By John Hanna. SENT: 911 words.
AP Photos NYHK104-0709141236, CER204-0111171450, MOKAS501-0327181039.
XGR-LOUISIANA GAMBLING
BATON ROUGE, La. — The Louisiana House agreed Thursday to a 30-year contract extension for Harrah's to operate the land-based casino in New Orleans. Lawmakers voted 78-12 for the measure keeping Harrah's in place at the casino until 2054. The sponsor is Republican House Speaker Taylor Barras. SENT: 181 words.
IN BRIEF:
— PLATFORM EXPLOSION-GUILTY PLEA — A contract worker has pleaded guilty to violating the federal Clean Water Act in connection with a deadly oil platform explosion off Louisiana's coast.
— METHODIST CHURCH LEAVES — A Mississippi church has left the United Methodist Church amid clashing views on homosexuality, abortion and the teaching of Islam.
— SCHOOL BUS — Police in Vicksburg say a driver who failed to stop for a school bus has been arrested for running into a 9-year-old girl.
— RENASANT ACQUISITION — A Mississippi bank is buying a Georgia bank for $453 million in cash and stock.
— JAIL ESCAPEE-GUARDS CHARGED — A Mississippi Delta man who escaped from a county jail has been arrested in Texas after three months on the run.
— SENATE-MISSISSIPPI-HORSE LAWSUIT — An incoming U.S. senator from Mississippi and her husband are facing a lawsuit that claims one of their horses wandered onto a highway and caused a wreck.
— NATCHEZ WATER — A Mississippi city is seeking a $1.9 million loan from the state Department of Health to help improve the city's water system.
— SHOTS FIRED-HIGH SCHOOL — A 21-year-old man is accused of firing a gun at a north Mississippi high school.
— SCHOOL BOND VOTE — Residents of a central Mississippi school district are approving plans to raise taxes to borrow money for improving schools.
— MISSISSIPPI SHOOTING —Three men have been arrested in a shooting at an apartment in Mississippi.
— SEVERE WEATHER — Authorities say a tornado touched down in Texas, damaging homes but causing no injuries on Wednesday, and that severe weather continues Thursday to pose a threat of dangerous wind gusts, lightning and flooding as the storms move eastward.
SPORTS:
BKW--FINAL FOUR-MISSISSIPPI STATE-LOUISVILLE
COLUMBUS, Ohio — Mississippi State wants to write some more history in the women's college basketball world. For now at least, the Bulldogs are best remembered as the team that ended UConn's 111-game winning streak in last year's national semifinal, knocking off the mighty Huskies in overtime on a buzzer-beater by guard Morgan William. It was the shot heard around world, overshadowing Mississippi State's loss in the final to South Carolina. By Mitch Stacy. SENT: 761 words.
AP Photos OHDC122-0329181135, OHDC121-0329181132, OHDC124-0329181133, OHDC125-0329181107, OHDC126-0329181257, OHDC123-0329181143, OHDC115-0329180900, OHDC116-0329181127, OHDC114-0329180904, OHDC112-0329180911, OHDC111-0329180904, OHDC110-0329180904, OHDC109-0329180903, OHDC107-0329180857, OHDC126-0329181257.
___
If you have stories of regional or statewide interest, please email them to
The Atlanta AP Bureau: apatlanta@ap.org
The Montgomery AP Bureau: apalabama@ap.org
The New Orleans AP Bureau: nrle@ap.org
The Jackson AP Bureau: jkme@ap.org
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.