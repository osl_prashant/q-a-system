Endophyte-free fescue was released 30 years ago. Unfortunately, the forage did not hold up as well to grazing pressure and many stands that were planted did not last.
To help combat fescue toxicity, while maintaining the hardiness of toxic varieties, plant breeders released the first novel endophyte fescue in 2000. When fescue has a novel endophyte, the plant produces little or no ergot alkaloid.
“Basically it is a fungus you put back in the plant and the plant thinks it is vaccinated,” says Craig Roberts, University of Missouri Extension forage specialist.
A number of research studies have been performed comparing toxic fescue with novel endophyte varieties. Looking at multiple studies on grazing stockers, Gene Schmitz, regional livestock specialist with University of Missouri Extension, surmises switching to a novel endophyte can net an improvement of nearly 1 lb. per day in gains during the spring and 0.4 lb. per day in the fall.

“We can look at these gains on a pounds per acre value, too,” Schmitz says. Per acre novel endophyte pastures yield 100 lb. more in weight gain in the spring and 40 lb. in the fall.
Another consideration for performance and economics, Schmitz says, are possible deductions placed on calves at marketing time. Calves grazing fescue sometimes do not shed hair causing them to be devalued.
“It can take five to six weeks for those calves to get straight at the feedyard. They gain the same, but they’ll never catch up,” Schmitz says.
Establishing New Forage
Before planting or purchasing any seed, producers should consider their current grazing situation.
Moisture, soil type and land topography can all play a factor in decision making, says Stacey Hamilton, dairy specialist with University of Missouri Extension..
Start with a soil test to give you an idea of the current state of soil fertility in your pastures.
Even if a producer doesn’t decide to convert to a novel endophyte, having current soil test information will help make management decisions.
Pastures with Kentucky 31 should be tested for the amount of endophyte that is currently present.
The Alliance of Grassland Renewal recommends planning a renovation program six to nine months in advance of planting. If deciding to plant a novel endophyte fescue, producers should consult their Extension specialist or agronomist to ensure they have selected an optimal planting date.
After a soil test has been performed, soil amendments, such as adding more phosphorus or potassium fertilizer, can be made. When planting in an established pasture of Kentucky 31, do not allow seed to drop prior to the establishment of a novel endophyte fescue.
A spray-smother-spray program can help eliminate the risk of keeping back toxic fescue before it germinates, Hamilton says.
Approximately six months after making the necessary soil amendments, the pasture will be sprayed with herbicide to kill off the existing toxic fescue.
A smother crop should be planted one week later and grown for 60 to 90 days. Smother crop options might include winter annuals such as rye or wheat and summer annuals such as sorghum sudan or millet.
Depending on the season of the year, producers decide to make renovations, they will have to adjust herbicide applications and the best smother crop choice.
The smother crop can be used as a forage source during its growth stage then killed with another herbicide treatment after grazing or harvest.
Hamilton says after the second spray period producers need to evaluate the “burn down” of the plants. If any germinating toxic fescue appears they should repeat the spray-smother-spray program.
When the pasture is clear of toxic fescue, a new stand of novel endophyte fescue can be planted. Hamilton recommends producers apply 30 lb. of nitrogen per acre at planting to establish the stand.
No-till planting has a good mix of cost efficiency and planting success. Conventional planting following ground work, such as disking, is the most effective practice, but is three times as costly due to the amount of time spent in the field. Broadcast spreading seed is the least successful, however it is the cheapest method.
Part 1 of this story about problems with toxic endophyte fescue will ran on February 20. 
 
Note: This story appears in the February 2018 magazine issue of Drovers.