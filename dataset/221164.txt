Drought has been driving the wheat rally, and according to the latest U.S. Drought Monitor, this is the biggest drought footprint in four years, with parts of the Texas and Oklahoma Panhandles experiencing D3 or extreme drought.

Because of the drought, the wheat market is in a crop scare market, according to Joe Valavik, president of Standard Grain, and farmers could have some of the best opportunities to market wheat this calendar year.

There are two ways to take advantage of the rally, he says. The first is to sell cash contracts or futures against guaranteed bushels. The second is to have some options strategies with a floor place without committing cash bushels.

“I made it sound easy, but it’s not easy,” said Vaclavik on AgDay. “You’re never going to time it right. You’re never going to get the price right. These are some of your best chances to do some marketing.”

For farmers having to make some of these decisions, Vaclavik doesn’t deny this is a difficult call to make. He suggests talking to your crop insurance agent to make sure you sell a safe amount of bushels.

“The fundamental picture, according to USDA, was still pretty bearish, so we’ve got a sharp rally in what’s really still kind of a bearish fundamental supply and demand market,” he said.

Hear Vaclavik’s full comments on AgDay above.