The U.S. Department of Agriculture has announced its preliminary decision to approve the genetically engineered Arctic Fuji apple.
 
The USDA said Aug. 10 it would publish a ruling soon in the Federal Register and allow a 30-day comment period.
 
The apple, developed by Okanagan Specialty Fruits Inc., Summerland, British Columbia, is engineered to resist browning. The USDA in 2015 reviewed and deregulated the genetic engineering trait in the company's Arctic Golden and Arctic Granny varieties.
 
"We are pleased to see the Arctic Fuji reach the next stage of deregulation." Okanagan Specialty Fruits founder and president Neal Carter said in a news release. Interest in the Arctic Golden and Arctic Granny apples is strong, he said in the release, and he predicted the Arctic Fuji and other nonbrowning varieties to come will also find strong demand.
 
"Over a decade of real-world field trial experience has assured us that Arctic trees have the same growing needs as conventional trees, and that Arctic apples are compositionally and nutritionally comparable to conventional apples," Carter said in the release. "It's not until an Arctic apple is bruised, bitten or cut that the nonbrowning benefit becomes obvious."
 
Gene silencing is used in Arctic apples, according to the release, to suppress the apple's expression of polyphenol oxidase, an enzyme involved in browning when the fruit is bruised, bitten or cut. Arctic apples will still show discoloration from bacterial or fungal infections and will rot like other non-genetically engineered apples, according to the release.
 
The company expects commercial introduction of Arctic apple varieties in fall 2017.