USDA designates 61 Arkansas counties as disaster areas
USDA designates 61 Arkansas counties as disaster areas

The Associated Press

LITTLE ROCK, Ark.




LITTLE ROCK, Ark. (AP) — The U.S. Department of Agriculture has designated 61 drought-stricken Arkansas counties as disaster areas, making farmers and ranchers in the counties eligible for natural disaster assistance.
Agricultural officials said Wednesday the designation makes farmers and ranchers in the counties eligible for the Farm Service Agency's emergency loans as well as other assistance programs. Farmers have eight months to apply for loans to help cover part of their actual losses.
The National Weather Service says much of northern, northwest and west-central Arkansas is abnormally dry and that a small portion of northern Arkansas is experiencing moderate drought.
Forecasters say heavy rainfall in February which led to widespread flooding erased drought conditions in other parts of the state. Some reporting stations indicated many areas have received above average rainfall since Jan. 1.