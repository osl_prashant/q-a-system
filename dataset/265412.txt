Indian court jails powerful politician for embezzling funds
Indian court jails powerful politician for embezzling funds

By INDRAJIT SINGHAssociated Press
The Associated Press

PATNA, India




PATNA, India (AP) — A powerful Indian politician was sentenced Saturday to 14 years in prison for embezzling 37 million rupees ($570,000) from a state government's treasury while he was the state's top elected official.
Lalu Prasad Yadav was convicted of embezzling the money to buy fictitious medicines and cattle fodder while he was chief minister of Bihar state from 1990 to 1997.
Eighteen former Bihar state officials, contractors and suppliers were sentenced in the case to jail terms ranging from 3 1/2 to five years.
Yadav, who already has been convicted three times in related cases, was also fined 6 million rupees ($92,307).
Yadav, 69, who served as India's railways minister from 2004 to 2009, is barred from contesting elections. Earlier this month, he was shifted to a hospital from a prison after he complained of chest pains in Ranchi, the capital of Jharkhand state.
Corruption is endemic in Indian politics. Judges are now expediting trials of lawmakers accused of crimes including murder, fraud and extortion following a Supreme Court order to reach verdicts within one year in such cases. Indian lawmakers are now barred from running in elections if they are found guilty of offenses carrying a jail term of at least two years.
Yadav has turned his political party, Rashtriya Janata Dal, or the National People's Party, into a family enterprise, with his wife, two sons and a daughter running it. The daughter is a member of India's Parliament and the two sons have become lawmakers in Bihar state.
Around 15 percent of Indian lawmakers are facing court trials on criminal charges such as rioting, murder and extortion, according to the Association for Democratic Reforms, an activist group.