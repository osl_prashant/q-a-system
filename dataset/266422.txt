AP-NE--Nebraska News Digest 1 pm, NE
AP-NE--Nebraska News Digest 1 pm, NE

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Nebraska. Questions about coverage plans are welcome, and should be directed to the Omaha Bureau at 402-391-0031 or omahane@ap.org. Nebraska News Editor Scott McFetridge can also be reached at 515-243-3281 or smcfetridge@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
TOP STORIES:
CROP REPORT
DES MOINES, Iowa — Corn has been dethroned as the king of crops as farmers report they intend to plant more soybeans than corn for the first time in 35 years. By David Pitt. SENT: 130 words. UPCOMING: 450 words by 3 p.m.
AROUND THE STATE:
STATE CUSTODY-AILING GIRL — Nebraska authorities have stopped efforts to take custody of a 15-year-old girl whose mother had been delaying follow-up treatments after the teen underwent two surgeries to remove a brain tumor. SENT: 130 words. UPCOMING: 300 words.
IN BRIEF:
EMPLOYEE THEFT-BROKEN BOW — A woman accused of stealing from her central Nebraska employer has been sentenced.
PEDESTRIAN KILLED — A driver has been jailed for running into and killing an 86-year-old pedestrian in Holdrege while reading a text message.
JAIL ESCAPE-NEBRASKA PANHANDLE — Officials say two men are still at large after overpowering a jailer and escaping from the Sheridan County Jail in the Nebraska Panhandle.
BANK SLAYINGS-APPEAL — One of four men convicted in a deadly Norfolk bank shooting wants a federal court to overturn his death sentence.
___
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.