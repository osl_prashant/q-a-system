Hog farm bill poised for passage at Arkansas Legislature
Hog farm bill poised for passage at Arkansas Legislature

By KELLY P. KISSELAssociated Press
The Associated Press

LITTLE ROCK, Ark.




LITTLE ROCK, Ark. (AP) — The Arkansas House and Senate approved identical bills Wednesday that would make it harder to perpetuate complaints against farms that need permits to retain then dispose of liquid animal waste.
Under the proposals, third parties wouldn't be able to raise complaints following a comment period if the farm remains in "good standing" with state regulators. Opponents fear an inability to remedy potential problems and a state senator said it could establish a precedent of regulators ending public input on other matters.
"We don't want to do anything that will result in an unintended consequence," said Sen. David Sanders, R-Little Rock.
The House approved the bill on a 73-6 vote and the Senate approved its version 21-7. Only one version needs to pass for the matter to go to Gov. Asa Hutchinson. Final votes are expected Thursday as lawmakers try to wrap up a special session.
The bill's House sponsor, Rep. Jeff Wardlaw, has routinely prefaced his remarks by saying the proposal didn't pertain to the C&H Hog Farm, which since 2012 has been authorized to hold 6,503 pigs near a tributary of the Buffalo National River.
Environmentalists concerned about potential manure runoff fear a new law might undermine their fight against C&H. Wardlaw said making a law out of current Arkansas Department of Environmental Quality practice would also benefit bankers concerned about ongoing challenges.
"We are not changing the public comment period. This outlines that, once this takes place, they are not subject to more lawsuits for new reasons," Wardlaw said.
Michael McAlister, the managing attorney for ADEQ, said the proposal doesn't weaken any environmental standards. "Depending on the circumstances and what you propose, you would have to meet the necessary requirements," he said.
State regulators rejected a C&H permit application in January as the farm sought to change from one type of permit to another. The farm continues to operate while it appeals the rejection.
ADEQ said Wednesday that 159 farms hold liquid waste permits like that sought by C&H. The Arkansas Pork Producers Association said the law would help its members in case there are future complaints.
"It gives some clarification to our current producers that someone cannot come in and sue them for what they have already been in business for," said Jerry Masters, the association's executive director.
In its application to the state, C&H estimated its lagoons would hold about 2.337 million gallons of waste — the equivalent of about 5.4 feet (1.6 meters) of waste atop a standard American football field.
___
Associated Press writer Andrew DeMillo contributed to this report.