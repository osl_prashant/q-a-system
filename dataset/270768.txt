Sales and production figures indicate it’s a good time to be in the mushroom business.Domestic mushroom production and value reached all-time highs in 2014, according to data from the U.S. Department of Agriculture released in August 2015.
The value of domestic mushroom production jumped by 10% from the year-earlier level, with production increasing by 6%. The 953 million-pound crop in 2014-15 broke the previous year’s record value by $112 million. Sales in 2014-15 totaled $1.15 billion, the USDA reported.
“This confirms that mushroom demand is growing at record levels,” Bart Minor, president of the San Jose, Calif.-based Mushroom Council, said in a news release.
Pennsylvania accounted for 63% of the total volume of sales, with second-ranked California contributing 11%, according to USDA.
Brown mushrooms, including portabella and crimini varieties, accounted for 161 million pounds, up 5% from a year earlier. Brown mushrooms accounted for 17% of total mushroom volume sold and 21% of the total crop value. Ninety percent of the crop went into the fresh market — up 9% from the previous year, USDA said.
Production of specialty mushrooms, including varieties such as shiitake and oyster mushrooms, grew by 21% to 22.5 million pounds, with a sales value of $75 million. Organics accounted for 4% of mushroom sales, at 60.7 million pounds of production, but that was up by 40% from a year earlier.
“Shipments could be significantly higher if we just had more mushrooms,” Minor said.
Minor said it’s highly possible that demand could outpace supply in 2016.
Rising demand for mushrooms seems timely, from a grower standpoint, said Patty Foss-Bennie, president of the Avondale, Pa.-based American Mushroom Institute.
“Increased demand will lead to further increases in the price for mushrooms,” she said.
Avondale-based To-Jo Mushrooms hopes to at least match the industry’s 3% or 4% growth as 2016 unfolds, said Pete Wilder, marketing director.
“Production at the farm level is currently keeping pace with this increased demand and should be adequate to service our existing customer base,” he said.
Expanded production would be in keeping with tradition at Temple, Pa.-based Giorgio Fresh Co., said Bill Litvin, senior vice president of sales and national account manager.
“Giorgio has expanded our production every year for many years,” he said.
Grower-shippers are facing higher production costs, including raw materials, wages and health insurance for workers, Foss-Bennie said.
“With tight supply, prices should be moving up to meet demand,” she said.
 
More sophisticated
Brown mushrooms are becoming big sales drivers, said Mike O’Brien, vice president of sales and marketing for Watsonville, Calif.-based Monterey Mushrooms Inc.
“The mushroom consumer is becoming more sophisticated and is moving from the traditional white mushrooms to brown mushrooms, and exotics such as oyster and shiitake,” O’Brien said.
The conversion from white button mushrooms to brown mushrooms continues, O’Brien said.
“If you love mushroom pizzas, a portabella makes your pizza even better,” he said. “Also, the baby bella continues to grow and in some areas outsells white mushrooms.”
Olympia, Wash.-based Ostrom’s Mushroom Farms increased production by 20% in 2015, said Fletcher Street, sales and marketing director.
“For 2016, our goal is consistent production on a weekly basis — no changes in pounds grown,” Street said.
Ostrum anticipates continued strong growth in value-added products, such as retail packs of sliced white and sliced criminis, as well as whole and sliced portabellas, Street said.
“We are also anticipating small but steady growth in shiitake and oyster mushrooms,” she said. “In foodservice, sliced bulk mushrooms continue to grow as a category, criminis are slowly increasing and portabellas remain strong.”
 
Whole segment growing
At Kennett Square, Pa.-based Phillips Mushroom Farms, brown baby bellas continued to increase their share of the market, said Kevin Donovan, national sales director.
But that doesn’t tell the whole story, he said.
“I still see the whole mushroom segment increasing. Demand seems strong, consumer trends seem strong also,” he said. “Looks like it’s going to be a strong demand this year.”
Specialty mushrooms should see healthy growth numbers at retail and foodservice in 2016, said Joe Salvo, president of Ponderosa Mushrooms and Specialty Foods, Port Coquitlam, British Columbia.
“We’re always gaining new customers and markets to distribute our specialty mushrooms,” he said.
Ponderosa has a particular emphasis on morels harvested in the wild, Salvo said.
“For 2016, we look forward to a really exciting crop of morel mushrooms with all the forest fires that happened in the last year,” he said.
Forest fires generally contribute to big morel crops, but there is more to it, he said.
“One of the challenges in 2014 was a big fire year, but very little snow, record dry levels of no snow in the mountains and a warm spring equated to no mushrooms,” he said. “This winter, we’ve already seen record snowfalls, and that’s the second part.”