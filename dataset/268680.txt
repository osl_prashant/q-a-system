BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Thu. Apr 12, 2018          USDA Market NewsDaily California EggsPrices are unchanged. Trade sentiment is sharply lower. Offerings aremoderate to heavy. Supplies are light to moderate. Demand is moderate.Market activity is moderate.Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                283EXTRA LARGE          277LARGE                275MEDIUM               184Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 12-Apr-18 11:14 AM E MP