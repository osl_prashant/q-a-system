This week, 56 outstanding veterinary students from around the world received scholarships from Merck Animal Health, in partnership with the American Veterinary Medical Foundation (AVMF). Through the Merck Animal Health Veterinary Student Scholarship Program, the selected second-and third-year students pursuing careers in companion animal or large animal medicine will each receive a $5,000 scholarship (totaling $280,000) to support their educational endeavors.
“We know that the cost of veterinary education can be a challenge but want to encourage talented students to pursue their dreams,” said Scott Bormann, Vice President, North America, Merck Animal Health. “We’re honored to support these recipients, as the work they do throughout their careers will have an important impact on the animal health industry, helping to advance the well-being of both companion and large animals.”
The AVMF, the charitable arm of the American Veterinary Medical Association, has supported veterinary students for more than five decades.
“The AVMF helps ensure the future of veterinary medicine by recognizing and supporting outstanding students,” said Jan K. Strother, D.V.M., Chair, AVMF Board of Directors. “We are honored to partner with Merck Animal Health in providing these scholarship opportunities.”
Award recipients from U.S. and international veterinary schools accredited through the AVMA were selected based on academic excellence, financial need, leadership and area of interest within the profession. The 2018 scholarship recipients are:

Tristan Darrel Agustin, University of the Philippines Los Banos
Trisha Alina, University of the Philippines Los Banos
Mehedi Hasan Ashiq, Bangladesh Agricultural University
Kelley Black, Western University – California
Alicia Bonke, Michigan State University
Jordan Briggs, North Carolina State University
Jessica Buchy, University of Florida
Robert Buntz, Colorado State University
Rheannon Burris, Michigan State University
Michael Caplan, University of Montreal
Crystal Cardona, Iowa State University
Katherine Crocco, Midwestern University
Melissa Dalton, Colorado State University
Abbey Earle, University of Pennsylvania
Samantha Eder, Auburn University
Lacey Ellingson, University of Minnesota
Maria Estefania-Colon, Tuskegee University
Julienne Fe Butic, University of the Philippines Los Banos
Holly Grams-Johnson, Colorado State University
Rachel Griffin, Michigan State University
Alexi Haack, University of California – Davis
Md. Mehedi Hasan, Bangladesh Agricultural University
Muhammad Sujon Hasan, Bangladesh Agricultural University
Rakib Hasan, Bangladesh Agricultural University
Homaira Pervin Heema, Chittagong Veterinary and Animal Sciences University
Kristine Hill, Ross University
Amy Kraus, University of Pennsylvania
Kyndel Lann, Midwestern University
Jessica Levine, Tufts University
Carl Magnusson, University of Wisconsin
Harrah Grace Magsino, University of the Philippines Los Banos
Emily Mangan, Oregon State University
Alex McFarland, Colorado State University
Michael McKinney, The Ohio State University
Md. Manik Mia, Bangladesh Agricultural University
Ori Nagasaka, University of California – Davis 
Jannatul Nyema Nikita, Sylhet Agricultural University 
Jessica Ruiz-Gonzalez, Auburn University 
Renee D. Saxton-Baumann, Oregon State University 
Trinity Scanlon, University of Pennsylvania 
Sahar Sheikh-Ahmad, North Carolina State University 
Joel Steckelberg, Iowa State University 
Nigar Sultana, Chittagong Veterinary and Animal Sciences University 
Gabriel Tumamac, University of the Philippines Los Banos 
Saif Udden, Bangladesh Agricultural University 
Ashley Ulmer, Virginia – Maryland College of Veterinary Medicine 
Brittney Vaughn, Colorado State University 
Emil Walleser, University of Wisconsin 
Hailey Watlington, Lincoln Memorial University 
Jessica Weirich, University of Illinois 
Taylor Wickware, The Ohio State University 
Rebecca Wilson, University of Guelph 
Jessi Wixtrom, Michigan State University 
Alexis Wohl, Western University – California 
Greg Wojciechowski, Colorado State University 
Michelle Yamashita, Washington State University