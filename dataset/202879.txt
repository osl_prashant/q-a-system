Calavo doubles tomato volume
Calavo Growers Inc., Nogales, Ariz., is doubling its tomato volume in Jalisco, Mexico, starting in May, and will ship round and roma varieties to retail, foodservice and wholesale customers through McAllen, Texas, said Brian Bernauer, director of operations and sales.
"This will give us the ability to seamlessly go from what we're doing here to shipping out of Texas in volume," he said.
Calavo will be able to ship tomatoes through December and into January, depending on markets, he said.
"It's a really good value when you can come up with steady, good supplies of tomatoes from May all the way through January," Bernauer said.
The tomatoes, including its proprietary Intense variety, are grown in high-tech greenhouses, he said.
 
Del Campo offers blocky peppers
Del Campo Supreme Inc., Nogales, Ariz., is bringing back blocky bell peppers after an absence of several seasons, said Jimmy Munguia, sales manager.
Del Campo Supreme Inc., Nogales, Ariz., once again will ship blocky bell peppers this season, says Jimmy Munguia, sales manager.
The company has launched a pilot program that includes organic and conventional red, yellow and orange blocky bell peppers packed in 11-pound boxes.
"Quality has been outstanding," Munguia said.
Del Campo brought back the peppers to "have a more diversified portfolio," he said.
They should be available until May.
 
Farmer's Best adds acreage
Rio Rico, Ariz.-based Farmer's Best is increasing field sizes, updating its technology and adding another product for year-round availability this season, according to a news release.
The company is expanding facilities in San Luis Potosí, where it farmed about 74 acres last year. In 2017, the company will increase production to about 500 acres, making room for more roma tomatoes, cucumbers and green bell peppers.
Net houses cover 72% of the additional acreage. The rest is open field.

"We are very excited about our expansion in San Luis Potosí," Leonardo Tarriba, Farmer's Best general manager, said in the release. "It allows us to provide the same fresh, quality produce we grow in Sinaloa and Sonora, year-round."
The company has added green bell peppers to its year-round lineup that also includes roma tomatoes and cucumbers.
Finally, an updated bell pepper sorting machine has been installed and has increased sorting efficiency by 28%, the release said.