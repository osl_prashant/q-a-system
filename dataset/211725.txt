Southeastern Grocers, which owns the Winn-Dixie, Bi-Lo, Harveys and Fresco y Más retail banners, has recalled four fresh-cut produce products due to potential listeria contamination.According to reports from news outlets in the South, the products subject to recall are:
SEG Tri Pepper Dice, 6-ounce package — universal product code 3825911565;
SEG Stir Fry Vegetable, 10-ounce package — UPC 3825911781;
SEG Fajita Blend, 12-ounce package — UPC 3825911785; and
SEG Vegetable Kabob, 23-ounce package — UPC 3825911592.
Neither Southeastern Grocers nor its individual retailers had posted the recall notice online, and nothing had been posted to the Food and Drug Administration site. Southeastern Grocers did not immediately respond to requests for comment.
According to reports, the affected products were sold under the Country Fresh label and had sell-by dates between Aug. 12 and Aug. 20.