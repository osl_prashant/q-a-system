As the mainstream economy has continuously seen improvement, the rural ag economy hasn’t been as fortunate. Commodity prices are starting to see a little life and land values are starting to stabilize as possible signs that the agriculture is starting to claw its way out of the recession.

According to Jim Mintert, director of the center for commercial agriculture, there’s two things that need to be examined: tightness of supplies relative to demand and strong usage.

“If we see trend line yields this year, we’re going to see those stocks-to-use ratios tighten even further, so that’s a different environment than we were facing from about 2014 up through ’16, ’17,” he said on AgDay.

With trend line yields, Mintert expects to see higher prices, and prices could be more positive if a weather event happens this summer. While he’s hopeful this could be the end of a long struggle for ag producers across the board, he says “it’s not guaranteed.”

“It’s a different environment than what we were experiencing in 2017 through 2017,” he said. “What that means is from a management standpoint to think about the possibility of that and whether or not you’re positioned to potentially capture that.”

Hear his full comments on AgDay above.