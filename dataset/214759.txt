Just in time for the biggest cooking holiday of the year, Amazon and Whole Foods announced fresh price reductions.
Whole Foods’ new reductions are a part of the ongoing integration with Amazon, said CEO John Mackey, “and we’re just getting started.”
“In the few months we’ve been working together, our partnership has proven to be a great fit,” Mackey said in a news release. “We’ll continue to work closely together to ensure we’re consistently surprising and delighting our customers while moving toward our goal of reaching more people with Whole Foods Market’s high-quality, natural and organic food.”
The announcement included a “sneak preview” of “special savings” and in-store benefits for Amazon Prime members, expected when Prime becomes “the official rewards program of Whole Foods Market.”
Prime members get a 50-cent-per-pound discount on turkeys over the already-reduced price.
Other reductions include:

Organic broccoli;
Organic salad mix;
Organic russet potatoes; and
Organic sweet potatoes.

Whole Foods announced big price reductions on items like avocados, tomatoes, organic apples and salad mix the day its acquisition by Seattle-based Amazon was complete, and it was widely criticized after analysis showed overall basket prices were the same, or higher.
The company also has been called into question for going quiet on promoting local sourcing, a potential boon for competitors like Fresh Thyme Farmers Market, Sprouts Farmers Market and Lucky’s Market, as well as national giants like Kroger and Safeway Albertsons.