Just coming off binging on Super Bowl food that was part healthy and part not, it is time for me to get in the gym and start eating better. It’s a thought, anyway.
After checking my inbox this morning, I think I know where to start.
Recent research publicized by the California Table Grape Commission suggests that grapes can serve as a “gateway fruit” to healthier eating.
According to the release, offering fresh grapes as part of the school lunch menu helped improve the school lunch eating behaviors of children in a pilot study conducted through Texas A&M University.
The study examined plate waste when fresh grapes were offered compared to when they were not offered. 
From the release:
The study was conducted in two elementary schools and two middle schools from one school district in Texas. Grapes were made available on “grape days” as an offered fruit choice. Non-grape days were defined as days when grapes were not offered.
Study results included:

 When offered as a fruit choice, grapes were minimally wasted.
 On grape days, lost dollars attributed to vegetable plate waste was significantly less than on non-grape days.
 Intakes of effective calories, fat, sodium, protein, and fiber per serving of entrees, vegetables and fruits on grape days were higher than on non-grape days.
 On grape days, the children consumed more of the school lunch, which is an overall goal of school lunch.


“Our study shows that offering grapes in school lunches is a smart strategy that goes beyond grapes’ status as a favorite fruit to grapes having a beneficial impact on the degree to which students make healthy choices, and on their consumption of the school lunch overall,” says Dr. Peter S. Murano, co-author of the study.

TK: Wouldn’t it be better for all of us if every day was a “grape day?” We need a “gateway food” to healthier eating, especially after an over-indulgent Super Sunday.
---
Speaking of healthy eating and schools, there is pushback from nutrition advocates on the USDA’s interim final rule covering changes to school meal nutrition standards to give operators flexibility on whole grains and sodium levels. The comment period on the rule closed at the end of January. Though the interim rule doesn’t touch the fruit and vegetable requirements, the dissent from the nutrition community and others is worth a close look.
From a teacher: 
I am a school teacher in rural Virginia. It’s important to me that our children receive nutritious meals at school....meals that promote children’s health, not damage it.
I ask the USDA to maintain strong nutrition standards for meals served in schools. Providing flexibility by allowing schools to serve grains that are not whole-grain-rich and flavored milk with 1% fat would constitute a direct contradiction of the current dietary guidelines and result in a step back on the progress already made in promoting a healthier lifestyle,and a healthy weight.
Students need whole-grains, lots of fresh fruits and veggies.....low fat, low salt diets with minimum amount of oils....non-processed foods as much as possible. 
Unfortunately, for many children in the USA , school is their only chance to get a well-balanced meal. 
Thats why I ask you to please keep the bar high when it comes to serving nutritious food in schools across America and not reduce the nutritional quality of school food.
Jennifer Horn
Chesterfield, Va.
 
From the Robert Wood Johnson Foundation’s comments, by CEO Rich Besser:
We urge USDA to support students’ health and well-being by fully implementing the school nutrition standards that took effect in 2012.
The 2012 updates to school nutrition standards—the first such updates in more than 15 years—were transformative, reflecting sound science and supporting children’s health. The standards have led to meals with more fruits, vegetables, and whole grains, with far less sodium, saturated fats, and added sugars. Sugary drinks, unhealthy snack foods, and trans fats have been removed as well. These changes are consistent with the 2015-2020 Dietary Guidelines for Americans (DGA) and the National Academy of Medicine (formerly, Institute of Medicine) 2009 report School Meals: Building Blocks for Healthy Children.
Recent demographic changes in the United States illustrate the importance of healthy school meals. According to recent data from the National Center for Education Statistics, a majority of students in our nation’s public schools—51 percent—now qualify for free and reduced-price school meals. In addition, with low-income, Black and Latino students at highest risk for obesity, ensuring that schools serve nutritious food is a core component of efforts to provide a fair and just opportunity for all children to achieve a healthy weight and mitigate the negative health effects of poverty.
 
TK: Check out the more than 7,800 comments on the interim rule here.