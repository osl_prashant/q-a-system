AP-FL--Florida News Digest 130 pm, FL
AP-FL--Florida News Digest 130 pm, FL

The Associated Press



Good afternoon! Here's a look at how AP's general news coverage is shaping up in Florida. Questions about coverage plans are welcome and should be directed to the AP-Miami bureau at 305-594-5825 or miami@ap.org. Ian Mader is the news editor and can be reached at imader@ap.org. Jason Dearen is on the desk and can be reached at jdearen@ap.org. A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Eastern. Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
TOP STORIES:
SCHOOL SHOOTING-FLORIDA-LEGISLATURE
TALLAHASSEE — In a rare move, the Florida Senate is going into session on a Saturday in order to move along a bill addressing school safety and gun regulations in the aftermath of the Parkland school shootings. The Senate will take up the wide-ranging bill for member questions before getting ready for a final vote on Monday. By Brendan Farrington. UPCOMING: 500 words after session end at 4 p.m. ET.
XGR-WEEK AHEAD
TALLAHASSEE — Florida legislators are nearing the finish line of their 60-day annual session, but they have a long line of unfinished business to take care of in the final week. The session is scheduled to end on March 9 but they have not yet voted on a new budget or sweeping gun and education bills. By Gary Fineout. SENT: 542 words.
NRA-CORPORATE MORAL VOICE
NEW YORK — Some U.S. companies are getting out ahead of the government when it comes to guns. Such actions have been rare in the history of corporate involvement in politics. The decisions usually come under much public pressure and with an eye toward minimizing the impact to the bottom line. By Alexandra Olson. SENT: 1,064 words. With AP Photos.
INVASIVE PYTHONS-DEER
NAPLES — Researchers in Florida say they have found an 11-foot-long invasive Burmese python that had consumed a deer that weighed more than the snake. Wildlife biologists tracking the slithery creatures in southwest Florida found one of them had eaten a white-tailed deer fawn. UPCOMING: 235 words.
POLICE OUTREACH OFFICERS
COLUMBUS, Ohio — A Muslim officer has been appointed in Ohio's capital to bridge gaps between police and the city's growing immigrant population. Egyptian-born Khaled Bahgat is the New American Diversity and Inclusion Officer for Columbus Police. Other officers around the country have been appointed to achieve similar inclusion goals. Bahgat has begun his job working with Somali and Bhutanese-Nepali refugees. Police Chief Kim Jacobs said her goal in appointing Bahgat is breaking down communication barriers. By Andrew Welsh-Huggins. SENT: 701 words. With AP Photos.
LUMBER PLANT
TROY, Ala. — A sawmill company will build a $110 million lumber manufacturing facility in southern Alabama. Al.com reports the Florida-based Rex Lumber Co. will build the state-of-the-art facility and create more than 110 jobs. Gov. Kay Ivey says the plant called "Project Red Fox" will produce a minimum of 240 million board feet per year. SENT: 239 words.
EXCHANGE-HOMELESS WOMEN
FORT MEYERS — John Johnson put down his tools to rest on the back stoop of a house in the re-making, taking in the view of banana fronds brushed with gold afternoon light. With a few other homeless men, the landscaper who specializes in tree work has been helping nonprofit St Martin de Porres rescue the building in a rural corner of Fort Myers for the past seven months; a building he believes others need more than he does. By Patricia Borns. SENT: 1,109 words.
EXCHANGE-CIRCUS CARS
WILLISTON — When Ringling Bros. and Barnum & Bailey Circus closed last year, much of the focus was on the future of the many animals used in the show and whether they would be sent to places at which they could comfortably spend the rest of their lives. But it took a lot of train cars to transport those animals and the human performers from town to town, and trains have dedicated enthusiasts just as animals do. By Cindy Swirko. SENT: 632 words.
IN BRIEF:
FATAL CRASH — A teenager died and five others received life-threatening injuries after their car veered off a highway and hit a tree in northeast Florida.
XGR-PTSD-FIRST RESPONDERS — Firefighters, police officers and other first responders could get workers' compensation benefits for post-traumatic stress disorder under a bill passed by the Florida Senate.
DEPUTY FIRED-ASSAULT — A Florida sheriff's deputy was fired after authorities say he threatened a woman riding along with him into exposing her breasts.
UNIVERSITY POWDER STUNT — Officials say a University of Central Florida fraternity has been suspended after a pledge threw white power in a classroom as part of a LeBron James-themed stunt.
MOTHER KILLING — A Florida man has been sentenced to life in prison for murdering his elderly mother.
IN SPORTS:
HKN--FLYERS-LIGHTNING
TAMPA, Fla. — Steven Stamkos and the Tampa Bay Lightning return home to face the Philadelphia Flyers in NHL action. By Erick Erlendsson. UPCOMING: 600 words, photos. Game starts at 1 p.m. ET.
______
If you have stories of regional or statewide interest, please email them to miami@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477. MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Florida and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.
_____
The AP, Miami