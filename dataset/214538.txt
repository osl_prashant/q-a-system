Market reaction

Ahead of USDA's reports, corn futures were trading steady to 3/4 cent lower, soybeans were 5 to 6 cents higher, wheat was 1 to 3 cents higher and cotton was 35 to 55 points higher.
After the report, corn is trading 5 cents lower, soybeans are 9 to 11 cents lower, winter wheat futures are mixed with a downside bias, while spring wheat is up 1 to 4 cents. Cotton is 10 to 55 points lower.
 
Crop production
Corn: 14.578 billion bu.; trade expected 14.333 billion bu.
-- compares to 14.280 billion bu. in October; 15.148 billion bu. in 2016
Beans: 4.425 billion bu.; trade expected 4.408 billion bu.
-- compares to 4.431 billion bu. in October; 4.307 billion bu. in 2016
Cotton: 21.377 million bales; trade expected 20.9 million bales
-- compares to 21.12 million bales in October; 17.2 million bales in 2016
Well... this is kind of shocking. After a less-than-ideal planting season, a corn crop that showed stress after pollination in key areas of the central and western Corn Belt, a dry (but cool) August and early September and we're looking at a record national average corn yield of 175.4 bu. per acre. That's up 3.6 bu. from the October estimate and is 0.8 bu. above the previous record yield set just last year. It's amazing what a lack of late-season stress, a shot of rain in late August and "extra time" on the end of the growing season can do for the corn crop. If this yield estimate of 175.4 bu. per acre stands, this will mark the first time the national average yield topped the "all sample" yield collected on the Farm Journal Midwest Crop Tour. Yep... 2017 was a weird year.
Looking at the state-by-state yield trends for corn, there are some significant month-to-month increases. In the major states, yield cuts occurred in just two states: Michigan, down 1 bu. per acre to 167 bu.; and Nebraska, down 2 bu. per acre to 179 bu. per acre. The corn yield estimate in Ohio was steady with October at 173 bu. per acre. Yield increases were estimated in Illinois (up 6 bu., to 198 bu. per acre), Indiana (up 8 bu., to 181 bu.), Iowa (up 6 bu., to 197 bu.), Kansas (up 2 bu., to 136 bu.), Minnesota (up 6 bu., to 190 bu.), Missouri (up 3 bu., to 175 bu.), North Dakota (up 8 bu., to 134 bu.), South Dakota (up 3 bu., to 150 bu.) and Wisconsin (up 4 bu., to 168 bu. per acre).
Corn production this year at 14.578 billion bu. is up 298 million bu. from last month and is 245 million bu. above the average pre-report trade estimate. The market started this year with expectations the corn crop would be down about a billion bu. from last year -- it's now just 430 million bu. smaller than last year's record.
Compared to the production report for corn, the bean update is fairly uneventful. The national average bean yield is unchanged from last month at 49.5 bu. per acre and the crop estimate of 4.425 billion bu. is 17 million bu. bigger than trade expectations, but still down 6 million bu. from last month.
Looking at the state-by-state estimates, yields increased in Illinois (up 1 bu., to 58 bu. per acre) and Nebraska (up 2 bu., to 58 bu. per acre). Yields were steady with month-ago in Indiana (55 bu.), Iowa (56 bu.), Minnesota (46 bu.), Missouri (49 bu.) and South Dakota (45 bu. per acre). Yields fell from last month in Arkansas (down 1 bu., to 50 bu.), Kansas (down 1 bu., to 40 bu.), Michigan (down 4 bu., to 45 bu.), North Dakota (down 1 bu., to 35 bu.), Ohio (down 1 bu., to 51 bu.) and Wisconsin (down 1 bu., to 46 bu. per acre).
The 2017 cotton crop estimate is 477,000 bales bigger than the average pre-report trade estimate and is up 257,000 bales from last month. The national average cotton yield is now estimated at 900 lbs. per acre, up 11 lbs. from last month. Georgia's cotton yield is estimated at 900 lbs. per acre, unchanged from last month, and Texas' yield is up 9 lbs. per acre from last month at 754 lbs. per acre.
 
U.S. carryover
Corn: 2.487 billion bu. for 2017-18; up from 2.340 billion bu. in October
Beans: 425 million bu. for 2017-18; down from 430 million bu. in October
Wheat: 935 million bu. for 2017-18; down from 960 million bu. in October
Cotton: 6.10 million bales for 2017-18; up from 5.8 million bales in October
Corn carryover is up 147 million bu. from last month and is 121 million bu. above the average pre-report trade estimate. Total supplies were increased 297 million bu. from last month on the bigger crop estimate. On the demand side, USDA increased estimated feed & residual use by 75 million bu. (to 5.575 billion bu.) and increased estimated exports by 75 million bushels (to 1.925 billion bu.). The end result was the 147-million-bu. increase in estimated carryover. USDA puts the national average on-farm cash price at $2.80 to $3.60, unchanged from last month.
Soybean carryover is down 5 million bu. from last month, but is 5 million bu. above the average pre-report trade estimate. Total supplies were cut 5 million bu. on the smaller crop. No changes were made on the demand side of the balance sheet. USDA now puts the national average on-farm cash bean price at $8.45 to $10.15, up a dime on both ends of the range from last month.
Wheat carryover is down 25 million bu. from last month and is 22 million bu. below the average pre-report trade estimate. USDA made no change on the supply-side of the balance sheet. On the demand-side, USDA added 25 million bu. to expected exports, pushing the tally to 1 billion bushels. USDA left the national average on-farm cash wheat price unchanged from last month at $4.40 to $4.80.
Cotton carryover is up 300,000 bales from last month and is 500,000 bales above the average pre-report trade estimate. USDA added 260,000 bales to the supply side of the balance sheet on the bigger crop peg. On the demand-side, USDA cut estimated unaccounted use by 40,000 bales. USDA puts the national average on-farm cash cotton price at 60 cents to 66 cents, up a nickel on the bottom and up a penny on the top end of the range from last month.
 
Global carryover
Corn: 226.58 MMT for 2016-17; down from 226.99 MMT in October
-- 203.86 MMT for 2017-18; up from 200.96 MMT in October
Beans: 96.28 MMT for 2016-17; up from 94.86 MMT in October
-- 97.90 MMT for 2017-18; up from 96.05 MMT in October
Wheat: 255.61 MMT for 2016-17; down from 256.58 MMT in October
-- 267.53 MMT for 2017-18; down from 268.13 MMT in October
Cotton: 88.67 million bales for 2016-17; down from 89.57 million bales in October
-- 90.88 mil. bales for 2017-18; down from 92.38 mil. bales in October
 
Global production highlights
Argentina beans: 57.8 MMT for 2016-17; compares to 57.8 MMT in October
-- 57.0 MMT for 2017-18; compares to 57.0 MMT in October
Brazil beans: 114.1 MMT for 2016-17; compares to 114.1 MMT in October
-- 108.0 MMT for 2017-18; compares to 107.0 MMT in October
Argentina wheat: 18.4 MMT for 2016-17; compares to 18.4 MMT in October
-- 17.5 MMT for 2017-18; compares to 17.5 MMT in October
Australia wheat: 33.5 MMT for 2016-17; compares to 33.5 MMT in October
-- 21.5 MMT for 2017-18; compares to 21.5 MMT in October
China wheat: 128.85 MMT for 2016-17; compares to 128.85 MMT in October
-- 130.0 MMT for 2017-18; compares to 130.0 MMT in October
Canada wheat: 31.7 MMT for 2016-17; compares to 31.7 MMT in October
-- 27.0 MMT for 2017-18; compares to 27.0 MMT in October
EU wheat: 145.47 MMT for 2016-17; compares to 145.47 MMT in October
-- 151.49 MMT for 2017-18; compares to 151.04 MMT in October
FSU-12 wheat: 130.48 MMT for 2016-17; compares to 130.74 MMT in October
-- 139.27 MMT for 2017-18; compares to 138.27 MMT in October
Russia wheat: 72.53 MMT for 2016-17; compares to 72.53 MMT in October
-- 83.0 MMT for 2017-18; compares to 82.0 MMT in October
China corn: 219.55 MMT for 2016-17; compares to 219.55 MMT in October
-- 215.0 MMT for 2017-18; compares to 215.0 MMT in October
Argentina corn: 41.0 MMT for 2016-17; compares to 41.0 MMT in October
-- 42.0 MMT for 2017-18; compares to 42.0 MMT in October
South Africa corn: 17.48 MMT for 2016-17; compares to 17.48 MMT in October
-- 12.5 MMT for 2017-18; compares to 12.5 MMT in October
Brazil corn: 98.5 MMT for 2016-17; compares to 98.5 MMT in October
-- 95.0 MMT for 2017-18; compares to 95.0 MMT in October
China cotton: 22.75 mil. bales for 2016-17; compares to 22.75 mil. bales in October
-- 25.0 MMT for 2017-18; compares to 24.5 MMT in October