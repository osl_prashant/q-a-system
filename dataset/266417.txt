Barbecue pioneer, Little Pigs BBQ founder Joe Swicegood dies
Barbecue pioneer, Little Pigs BBQ founder Joe Swicegood dies

By MACKENSY LUNSFORDAsheville Citizen-Times
The Associated Press

ASHEVILLE, N.C.




ASHEVILLE, N.C. (AP) — The scene at Little Pigs BBQ on Friday was about the same as it ever was: hushpuppies dancing in hot peanut oil; barbecue sandwiches flying over the counter; platters of the restaurant's famous broasted chicken getting devoured with crisp sides of slaw.
The lines were long, but they moved quickly. If, like some of the construction workers laboring over fat plates of ribs, you didn't look up from your meal, you might not notice the friendly-but-sad eyes behind the register. You might not discern that some diners ate quietly.
These were the people who knew Joe Swicegood, the founder of Little Pig's BBQ, who died Thursday at 91.
The marquee in front of the restaurant, opened in what was once a service station in 1963, paid characteristically low-key homage to the man behind it all: "We'll miss you Joe," it simply said.
Little Pigs began as a franchise of a small barbecue chain. The Swicegoods' restaurant outlasted the chain, but kept the name and made it synonymous with local barbecue well before names like 12 Bones and Luella's entered the local lexicon.
"People thought my father was crazy to locate there rather than uptown," said his son Carr Swicegood, who has worked in the family business for nearly 30 years, in a 1999 Citizen Times interview.
Any venture into the restaurant industry is a bold move, but Swicegood was no fool. The restaurant still serves an average of 250-300 people daily, according to manager Matt Thomas, who stepped from behind the counter during a post-lunch lull.
"We're busy. We used to have lulls in the winter, but here about the last four to five years, it's just wide open all the time."
That's a feat as Asheville's restaurant scene explodes with competition, including celebrated barbecue restaurants with James Beard-nominated pitmasters behind them.
But Swicegood was one of Asheville's first pitmasters, cooking his barbecue over nothing but wood before gas-assisted smokers became widely used.
"He had to stand there with a water hose to keep the flames down," Thomas chuckled in a 2013 interview. "The health code was a lot more lenient back then."
On Friday, Thomas said the restaurant's secret was, of course, good food. But he also noted how Swicegood always drove home the importance of friendly service. "One-on-one customer service was something he was very big on."
But even those friendly smiles seemed a bit strained on Friday. "It's a big blow," Thomas said. "He was the patriarch. He started it all ... and it's been a staple in the community ever since."
So too was Swicegood himself who, despite "retiring" long ago, maintained a solid presence in the restaurant until just a year ago.
"He was always here, hands-on, making sure everything was right, the way he wanted it, up to his standards. He was tremendous," Thomas said, before dining back behind the counter as the next rush began.
Emily Thomas worked at Little Pigs for the latter half of the '90s as a cashier before joining the Asheville Citizen Times. She recalled Swicegood as an honorable man who started from little and added restaurants and real estate to his portfolio with little more than hard work.
"When I worked there he had already 'retired' but still was in there almost every day," she said.
Swicegood, she said, was a "wonderful man," who sometimes fed people for free who were down on their luck. "He would talk to everybody, and if they were down on their luck, sometimes he would help them out with food ... he always made everyone feel like they were his best customer."
Craig Greene dined in a booth near the front door on Friday, a plate of broasted chicken glistening with hot sauce in front of him.
"Joe was a wonderful man. Every time, he wanted to know if you had enough to eat, that everything was going exceptionally well," he said.
Greene, 48, said he'd been coming here on his lunch breaks for his past decade working with WeighSouth, a local lab, industrial and educational scale company.
But his history with the restaurant stretches back to his childhood. His grandfather was "Mean" Joe Greene, the professional middleweight boxer whose picture hangs on the wall. The elder Greene was a regular customer, his grandson said, gesturing toward the picture.
"That's how well connected we are. I've known Joe for a while, and he was a very good man, and very good to us."
Like a grandfather, Greene said Swicegood would often try to feed him extra servings of chicken, even when he protested that he was full.
That sort of familial hospitality has kept Greene coming back his whole life, extra servings of food or no. "I like coming here. It's easy and convenient, the food is fresh, it's just a good place. It's home."
___
Information from: The Asheville Citizen-Times, http://www.citizen-times.com