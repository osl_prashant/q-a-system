Chris Wada has joined Palm Desert, Calif.-based North Shore Sales and Marketing as the company’s marketing director. 
The Southern California company, a hydroponic greenhouse grower of fresh living herbs and microgreens, plans to launch a new Organic Living line of products soon, and complete a $6 million greenhouse expansion in Thermal, Calif., according to a news release.
 
“I am excited to start a new challenge within North Shore and look forward to working with the team to further develop their already extensive product lines, business-to-business support and business-to-consumer engagement,” Wada said in the release. “I am fortunate to be joining such a respected company that prides itself on delivering the freshest living produce on the planet to many of the top retailers in the country.”
 
For nine years, Wada had worked at Wada Farms Marketing Group LLC, according to the release. Wada also had volunteer industry leadership posts at Potatoes USA and most recently was business development manager for BiologiQ, a sustainable packaging company. 
 
Suzette Overgaag, co-founder and CFO of North Shore, said that Wada brings valuable experience and knowledge to the growing company.
 
“Our new innovations and the increasing demand from our customers led us to look for an addition to our team who will fit in with our ethos of innovation and exceptional service, and it is very fortunate that we were able to find someone of his caliber to fulfill this role,” Overgaag said in the release. “I’m confident that Chris will play a key role in providing and implementing high quality solutions for our clients.”