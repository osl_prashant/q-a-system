Photo courtesy Fox Solutions
McAllen, Texas-based Fox Solutions has added a “soft fill” capability to its Pouch Bagger machine so that it can better handle apples, pears, peaches and plums.
The machine, which works with a wide variety of produce items, can hold 300 empty bags and fill up to 30 bags per minute, according to a news release.
“Our team is happy to have come up with such a valuable solution for our tree fruit customers,” Aaron Fox, president of Fox Solutions, said in the release. “Since introducing our Pouch Bagger last year, we have had a phenomenal response from customers who have wanted an efficient solution to meet the growing demand for pouch-bagged produce.
“The majority of baggers on the market can pack fruit of all kind, but few are fresh produce-specific like ours,” Fox said. “There simply isn’t a comparable piece of equipment that handles the fruit in a gentle enough way to prevent unnecessary bruising.”
The company will be showcasing the Pouch Bagger, its Wicketed Bagger and its Newtec weighers at Fresh Summit booth No. 1845.
The expo is Oct. 20-21 in New Orleans.