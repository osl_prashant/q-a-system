Quality beef heifers showed price strength in the Show-Me-Select Replacement Heifer sale, Friday, May 20. The 291 bred heifers averaged $2,236 at Joplin Regional Stockyards.The sale high was $3,200 per head.
"There's never been a larger spread in prices paid for heifers carrying AI (artificial insemination) sired calves compared to heifers with bull-sired pregnancies," said Dave Patterson, University of Missouri Extension beef specialist. AI pregnancies brought $496 more.
The price spread was greater, $679, for Tier Two heifers. Those are daughters of superior AI sires, bred to proven Show-Me-Select-approved AI sires.
"In declining cattle markets, it's clear buyers choose to pay more for high-quality heifers," Patterson said.
The high average paid to a farm was $3,028 per head for Shiloh Land & Cattle Co., Darrel and Anita Franson, Mount Vernon.
"They did everything right," Patterson said. The Angus-Simmental crossbred heifers were all Tier Two, all AI bred and genomically tested. In feedlot tests over nine years, Shiloh calves graded 69 percent USDA choice or better.
The 18 head were the last to come from Shiloh. The Fransons sold their cows last year as they prepare to retire. "It was sad to load the calves," Darrel said. However, picking up the sale check he said, "The melancholy is over."
Second-high average of $2,523 went to John Wheeler, Marionville, for 57 head of black whiteface heifers. This was Wheeler's 24th SMS sale. "Many of my buyers were repeats," Wheeler said. "They know what they are getting."
Wheeler sold the sale-high lot. The five heifers went to Michael Scarlett of Billings.
"I've never had so much pre-sale interest in replacement heifers," Wheeler said.
Kathy Wheeler, wife of John, took fifth-high average of $2,420 on her 20 black, mostly Angus heifers.
Third-high average at $2,507 was Gilmore Farms, Aurora, on 15 Angus-Simmental heifers.
Fourth-high average of $2,443 went to Don Hounschell, Stark City. His 24 black whiteface and black heifers included 11 Tier Two. He added a new level of quality with Show-Me Plus heifers with genomic-enhanced EPDs (expected progeny differences). The DNA test results go with heifers sold.
A sale-day catalog gives data on all heifers. Also, it tells about the MU educational program. All consignors enroll in the yearlong management and breeding program. That includes genetics, reproduction, pre-breeding exams and at least three veterinary exams. SMS also covers health, nutrition and body condition of heifers.
All are sold with expected calving dates. Those save labor at calving time. Heifers with timed AI have more precise dates.
"Our sale is an educational event," said Eldon Cole, MU Extension livestock specialist, Mount Vernon. "After the sale, two consignors said, 'I know what I must do next year.'"
Bidders educate producers by paying more for quality. SMS sales allow beef producers to earn more with proven genetics and heifer management. Many who are not consignors or buyers come to see.
Standards continue to rise with new technology. The latest upgrade is genomic-enhanced EPDs. Breeding is more precise with DNA information.
The program was started to reduce death loss at calving time when genetics for calving ease became available. That saves newborn calves and heifers.
"While other states have attempted to copy the program, many struggle to gain the success of Missouri," Patterson said. "Supervision by local MU specialists keeps integrity strong. It is not about sales but developing quality heifers."
For quality control, all heifers are checked on arrival at the sale by graders from the Missouri Department of Agriculture. Unfit heifers are sent home.
Producers enroll for the long term. Results build the farm herds before there are surplus heifers to sell. Herd owners can sign up through local MU Extension centers.
The last spring sale is 6 p.m. on June 4 at F&T Livestock Market, Palmyra. Find details athttp://agebb.missouri.edu/select.