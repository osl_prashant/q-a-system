The December Hogs and Pigs Report from USDA confirmed what analysts expected: U.S. producers continue to produce more pigs, with no decline in productivity. In fact, the report showed a record high in pigs produced per sow, at 10.74, compared to 10.63 a year earlier. The good news is that even with all this extra pork on the market, U.S. producers should have a profitable year. There are, however, a few important caveats.
First, here are highlights of the December report:

The U.S. inventory of all hogs and pigs on December 1, 2017 was 73.2 million head (up 2% from December 1, 2016, but down slightly from September 1, 2017).
Breeding inventory, at 6.18 million head, was up 1% from last year, and up 1% from the previous quarter.
Market hog inventory, at 67.1 million head, was up 2% from last year, but down slightly from last quarter.
The September-November 2017 pig crop, at 33.4 million head, was up 3% from 2016. Sows farrowing during this period totaled 3.11 million head, up 2% from 2016.
The sows farrowed during this quarter represented 51% of the breeding herd.
The average pigs saved per litter was a record high of 10.74 for the September-November period, compared to 10.63 last year.
Pigs saved per litter by size of operation ranged from 7.90 for operations with 1-99 hogs and pigs to 10.80 for operations with more than 5,000 hogs and pigs.
U.S. hog producers intend to have 3.07 million sows farrow during the December-February 2018 quarter, up 3% from the actual farrowings during the same period in 2017, and up 5% from 2016.
Intended farrowings for March-May 2018, at 3.08 million sows, are up 2% from 2017, and up 4% from 2016.

In a nutshell, the report showed a record high pig crop, record high pigs per litter, and the highest number of market hogs ever in 2017.
Stronger Prices Than Expected
With so many pigs on the market, one would expect prices to be in the tank, but that hasn’t been the case, for a number of reasons.
“We have a growth industry and that is very positive,” says Chris Hurt, professor of agricultural economics at Purdue University. He, along with Jim Robb, director of the Livestock Information Center; and Bob Brown, a market consultant from Edmond, Okla.; participated in a teleconference sponsored by the National Pork Board following the report in late December.
“When you think about growth orientation, it’s not just the domestic side with population growth of near 1%, but it’s also the export market,” Hurt says. “Exports add to our ability to expand the herd and expand production at about 2% a year or somewhat higher,” he adds.
“Packer margins recovered in November, largely due to strong pork product prices,” writes Mildred Haley, in the Dec. 18 Livestock, Dairy and Poultry Outlook from USDA’s Economic Research Service. She notes that October pork exports were 495 million lbs., 9.5% higher than a year ago and that fourth-quarter commercial pork production is “expected to be record-high at almost 6.8 billion pounds, about 2 percent higher than a year ago.”
Consumer Outlook is Better
“More income, not more people, means more pork demand,” says Dennis DiPietre, economist with Knowledge Ventures, LLC. “U.S. producers are a proven, dependable supplier of large quantities of high quality pork, provisioned to final user demand and supplied at reasonable cost. Almost 25% of the carcass now travels to another country for consumption, and that number has the potential to grow because global income is expected to rise in 2018.
“Domestic demand is quite possibly stronger than it has ever been, largely fueled by the never-ending uses of the primal belly,” DiPietre adds.
Along with that demand is a U.S. economy that’s doing very well, which means consumers have more income to spend on food. The economy is expected to do even better in coming months, in terms of some increase in growth rates and lower unemployment rates, Hurt says.
“We have the highest employment since the year 2000,” he says. “As we go into 2018, we’re expecting bidding up of wage rates, so we’ll have more people working at higher wages.”
Keep in mind however, consumers will have lots of meat and poultry from which to choose, and it’s a “headwind” to growth. In 2018, Robb says the U.S. will produce in excess of 2 billion pounds of meat.
Poultry will continue to top the global meat market in terms of production, but pork is making gains on white meat, according to a recent report released by the Food and Agriculture Organization (FAO).
“Per capita, we’re going to ask consumers in 2018 to eat as much red meat and poultry as they did in 2007,” Robb says, “and that’s much larger than in recent years. All the more reason to make sure export markets remain open.

Global Markets Growing
In global markets, “we’ve been doing great,” Hurt says. “USDA is projecting a 6% increase in exports in 2018. That’s up almost 13% from 2016. Demand is helping us market more pork.”
Dermot Hayes, professor of economics at Iowa State University and a recognized authority on exports, says pork industry expansion is about 3% to 3.5% per year and “you don’t get Americans to buy 3.5% more pork – maybe 1% if you’re doing well.
“The opportunity is there to export an additional 2.5% of our production every year,” Hayes adds. “That happened this year and it could happen next year: It’s ours to choose.”
He notes that trade agreements are critical to the U.S. pork industry’s leadership position in the export arena.
“In early November, the U.S. embassy in Tokyo released a report about the free trade agreement between Japan and the EU. It’s pessimistic for us. In the EU free trade agreement, Japan gives them exactly the deal they had offered the U.S. under the Trans Pacific Partnership. That deal was a good one, and now Europe has that access.”
In addition, the U.S. exports huge quantities of seasoned ground pork to Japan, Hayes says.
“Duties will fall very quickly to zero for EU pork but not to us,” Hayes says. “We’re compromised in our No. 1 value market already because we pulled out of TPP. Europe worked aggressively to make sure it had the same access in Japan as we did under TPP, so it pushed through a free trade agreement. The fact that we pulled out is to Europe’s advantage.”
The same is true with the North American Free Trade Agreement, Hayes believes. 
“I am very concerned that we will withdraw from NAFTA and give Mexico six months to come to our terms,” Hayes says. “I think Mexico is willing to put up with economic dislocation because they don’t want to be bullied, or pay for a wall.”
Any disruption in trade could be critical to future profitability, because, as Hayes says, “The industry would have to downsize if things go wrong with exports. That’s miserable, because the only way to get an industry smaller is to induce losses and that’s a major concern right now.”
Low Grain Prices
If the U.S. has good corn and soybean crops in 2018, Brown predicts futures prices in the high $70s, low $80s. Reasonable prices certainly helped producers remain profitable in 2017.
“We are in a very stable period of record or near-record global coarse grain/feed ingredient production, which will keep feed costs low and reasonable for many months ahead,” DiPietre says. 
“As an example, USDA forecasts for 2017/18 U.S. corn output keep growing on the back of record-high yields,” he says. “In addition, the feed industry worldwide is consolidating and recently exceeded an unprecedented production of 1 billion metric tons on 7 percent fewer mills, according to the 2017 Alltech Global Feed survey. 
While this is feed of all types, DiPietre notes the trend is toward creating better quality feed at lower cost from larger, more technologically driven mills.
“Droughts happen and will no doubt happen again, but right now, low feed costs will drive high carcass weights and exceptional profitability into 2018 for U.S. producers.”

Outlook Optimistic, But…
While the outlook is very good for producers in 2018, with both producers and processors in the black, the industry needs to keep economics in mind.
“We can grow in the range of 2.5% if we maintain a strong U.S. economy and continue to do well in the export market,” says Hurt. “With much lower feed prices, we can produce more and still do it profitably. But as we get back to 220 lbs. per capita, we have to be cautious across the animal industries of expanding too rapidly. Going forward, we have to match supply increases with what the world is going to eat.”
The new pork plants have been a boon to the industry, and will become even more beneficial as they ramp up to full capacity. The new Prestige plant in Iowa is expected to be operational in November 2018. The roof is on the facility, which will facilitate construction through the cold Iowa winter.
“Just the sheer existence of that capacity is encouraging optimism,” Hayes says. “Producers are willing to add production because they know there will be somewhere to kill those pigs. Potentially people who add production will get packers to compete for contracts or for open market hogs.
Provided export markets stay open, grain prices remain low, demand (especially for bellies) remains high, and consumers – both here and abroad – are anxious to eat more pork, 2018 should shape up to be a good year for U.S. pork producers.