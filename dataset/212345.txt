The U.S. Department of Agriculture is requesting recommendations to fill a vacancy on the National Organic Standards Board, a federal advisory committee that provides advice and recommendations to the USDA on organic food regulations.The nominee must have expertise in environmental protection and resource conservation, and be able to serve from January 2018 to January 2023.
The USDA is also creating a pool of candidates to fill future unexpected vacancies in any of the seven position categories.
Committee member duties include:
attending committee meetings (travel paid by USDA);
participating in bi-monthly subcommittee conference calls;
reviewing materials and/or recommending changes to the National List of Allowed and Prohibited Substances; and
advising Agriculture Secretary Sonny Perdue on other aspects of the USDA organic regulations.
Applications must be submitted before Aug. 7. For more information, visit the USDA’s nomination process website.