Businesses balance Facebook privacy concerns, ad needs
Businesses balance Facebook privacy concerns, ad needs

By JOYCE M. ROSENBERGAP Business Writer
The Associated Press

NEW YORK




NEW YORK (AP) — Some small businesses that use Facebook ads to promote themselves and attract new customers are wrestling with whether they need to change strategy after the company's data-misuse scandal.
The revelations that Cambridge Analytica gathered personal information from 87 million Facebook users made some people and small businesses jittery. Small business owners don't want customers to draw any connection between their ads and Cambridge Analytica, or to be unnerved by how their data is used.
But even wary owners, especially those trying to reach a wide audience on a small advertising budget, say they need to go where their customers are — and for many, that's Facebook.
"While I considered deleting Facebook, I understand the importance and reach Facebook has to keep an open channel of communication with our customers," says Mike Seper, owner of Eco Adventure Ziplines. The company, based in New Florence, Missouri, operates adventure rides for people to glide along cables suspended 50 or 250 feet above the ground.
Seper is concerned that Facebook users, seeing his ads and then viewing other users' political posts or ads, might mistakenly assume they're connected. "We just don't want that association," says Seper, who had been doing most of his advertising on Facebook. He'll still post videos on his company's Facebook page, but will advertise instead on Google and use more traditional methods like postcards, brochures and print ads.
Small business owners also need to keep in mind that many people are newly aware of data issues or more cautious about their online behavior.
Breakout, which operates 44 "escape room" game locations, is reducing its use of reminders that Facebook sends to people who start but don't complete a booking. The strategy does persuade many people to book another time slot — but it might also make them feel like they're being tracked, says digital marketing director Drew Roberts.
"People who aren't familiar with how it works may be a little turned off," Roberts says.
Breakout has also cut its Facebook advertising budget by 50 percent in response to the Cambridge Analytica scandal, but it's not going to abandon an effective method of reaching out to customers.
"It excels for us when we're opening new locations," Drew says. Like other advertisers, Breakout can take advantage of the data Facebook has available to target specific demographic groups and geographic areas, ensuring that it will reach people most likely to be interested in what they're selling.
Facebook has apologized to those whose data was misused, and says it's restricting app developers' access to people's information. The company has 10 million small businesses with Facebook pages in the U.S. and 6 million advertisers, most of them small companies; CEO Mark Zuckerberg was quoted earlier this month as saying he doesn't believe the company has seen a meaningful impact on ad spending from the scandal.
Facebook spokesman Joe Osborne acknowledges the concerns of some owners, including those that target ads to specific groups, but said Facebook doesn't share their information or that of their customers with anyone.
"Protecting people's information is a top priority," Osborne said Wednesday. "We've said before that if we can't, then we don't deserve the right to their information."
Elisa Vazquez, who helps companies run their social media marketing and ad campaigns, doesn't want to put her clients' customers and potential customers at any risk of being misused. She's placing fewer ads and aiming at broader audiences, which means targeting fewer Facebook users based on demographic data.
"For how many years did they say, no one's data has been leaked?" says Vazquez, social media director for Elevate My Brand, an online marketing company based in Los Angeles. "I want to be cautious."
Facebook's explanation of what it's done to protect users' data has reassured Ekiria Collins, owner of Yorkies of Houston. The breeder of Yorkshire terriers does about three-quarters of her advertising on Facebook and Instagram, which is owned by Facebook, and estimates that about 80 percent of her customers find and contact her business through Facebook.
Collins says she's concerned about her customers' privacy, but "I am confident that Facebook is researching the privacy matter and has implemented programs to ensure this doesn't happen again."
Others aren't convinced. Oliver Coen, a watch company based in London, gets more than 80 percent of its sales in the U.S., and puts half its advertising dollars into Facebook. Founder David Murphy has cut the company's Facebook budget by 5 percent because he's still concerned about users' personal information being passed on to third parties.
"I'd be surprised if it was one-off," he says of the Cambridge Analytica situation.
Murphy considered leaving Facebook, but also doesn't see an advertising alternative that would have the same reach.
But the company that runs the Alameda County Fair, not far from California's Silicon Valley, doesn't see data privacy as an issue. And it says the number of followers on its Facebook page is up 5 percent from a year ago.
"We're not worried about it — maybe because we live in a place that's so data-driven here, in the Silicon Valley," says Angel Moore, vice president for business development of the Alameda County Fair Association. "No matter what we do, data is being collected."
_____
Follow Joyce Rosenberg at www.twitter.com/JoyceMRosenberg . Her work can be found here: https://apnews.com/search/joyce%20rosenberg