The Kansas Veterinary Medical Association will launch the 2017 KVMA Convention this February 3-5, 2017, held at the Hilton Garden Inn Manhattan. A series of sessions, taught by nationally recognized speakers, will cover both small animal and food animal medicine. Designed to keep veterinary professionals up-to-date on cutting edge science and strategies, the convention will focus on providing the best care in animal health and welfare."The KVMA is pleased to provide a convention for members and guests packed with a diversity of speakers and topics that are of high value to today's veterinarians," said Dr. Gregg Hanzlicek, Committee chair. "Our committee put in extra efforts to ensure we are covering some of the most current topics to help our members and attendees stay in-tuned to current issues in animal health."
The three-day event kicks off February 3 at 11:30 am. A first-ever highlights will be the KVMA Poster Session and Career Fair designed to connect veterinarians in the field with future professionals. The Poster Session will feature an exhibition of veterinary medical research by students, faculty and industry partners as well as invited presentations from selected speakers.
Among the convention's hot presentations:
¬? Small Animal Dermatology: Demodicosis, Dermatitis and Otitis Externa
¬? Immunology: Maximizing Herd Health
¬? Ophthalmology: Ocular Diseases, Surgery and More
¬? Cow-calf and Feedlot Lameness
¬? Neurology
¬? Bovine Vaccinology and Immunology
¬? Small Animal Behavior
¬? Food Animal Trace Mineral Deficiencies, Urea and Monensin Toxicities and More
Speaker Tad Coles, DVM, is providing the luncheon address on Saturday, February 4, with his focus on personal wellbeing and creating your own resilience. Dr. Coles is a passion fatigue coach, wellbeing consultant, and impairment and prevention specialist. His mission is to decrease the impact of compassion fatigue, burnout and substance use disorder in healthcare professionals.
"Veterinarians from around the state will converge on Manhattan for more than the outstanding continuing education," said Dr. Aaron White, KVMA President. "It's also about relaxing with life-long friends, networking with colleagues, and taking home tips and tricks to make our practice the best it can be."
To learn more about the 2017 KVMA Convention, visit www.ksvma.org or contact the KVMA office at 785-234-0461. The convention is sponsored in part by Kansas State Veterinary Diagnostic Laboratory, Hill's Pet Nutrition, ThermoFischer and Zoetis.