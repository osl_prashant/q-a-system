While it is difficult to envision a healthier environment for nursing calves than a green summer pasture, bovine respiratory disease (BRD) remains a common problem in pre-weaned calves.During the recent Academy of Veterinary Consultants (AVC) meeting, South Dakota State University veterinarian Russ Daly said pre-weaning BRD affects about 20 percent of cow-calf operations each year and is the leading cause of mortality in calves from three weeks to weaning.
Daly notes that the risk factors for BRD in feedyards and stocker operations are well-known and extensively studied, but we know less about the disease in beef calves. The incidence of calf BRD varies widely between herds and from year-to-year within herds. Weather plays a role, but does not appear to account for all the variability in BRD incidence.
Daly outlined several studies intended to identify herd-level risk factors for pre-weaning BRD.
Across several studies, three common herd-level factors emerged as being associated with the incidence of pre-weaning BRD. Those are:
¬? Herd size: Larger herds appear to have a higher incidence than smaller herds.
¬? Genetics: Some breeding lines appear more susceptible than others to calf BRD.
¬? Estrus synchronization: Processing cows and heifers for artificial insemination could provide opportunities for pathogens to spread within the herd.
Other calf-level factors appear to be associated with a higher BRD incidence, based on somewhat limited data. These include:
¬? Male calves appear more susceptible than female calves.
¬? Calves from younger dams - two years of age or younger.
¬? Inadequate passive transfer of immunity from the dam.
¬? Dystocia.
¬? Twins.
Calves born without high serum titers for bovine herpes virus (BHV) or bovine viral diarrhea virus (BVDV).
Researchers at the U.S. Meat Animal Research Center in Clay Center, Nebraska collected several years of data on 110,000calves, and noted two seasonal peaks in the incidence of BRD. One peak occurs from birth through around 20 days of age, and anther takes place when calves reach 70 to 100 days of age. Other studies have shown a similar pattern, Daly says. Researchers believe the early spike relates to a failure of passive transfer of immunity from the dam, and the next peak at 3 to 5 months occurs as maternal antibodies decline.
The mix of pathogens involved in pre-weaning BRD is similar to that found in older calves. Daly says the SDSU diagnostics lab finds Mannheimia haemolytica, Pasteurella multocida, Histophilus somni, mycoplasma and corona virus, usually in association with at least one other pathogen.
Timely vaccination for cows and calves plays a key role in preventing pre-weaning BRD, but good management practices, including cow nutrition during gestation and ensuring the calf receives adequate quantity and quality of colostrum, are critical in supporting calf response to a vaccination program.
In another AVC presentation, North Dakota State University Extension veterinarian Gerald Stokka suggested these vaccination plans for cows and calves:
For cows
- Reproductive virus vaccination, two doses prior to first breeding
¬? IBR, BVDV
¬? BRSV and PI3 are included, but are not considered reproductive antigens
- Reproductive bacterial vaccination, depending on herd history
¬? Leptospirosis
¬? Vibriosis (Campylobacter)

For calves
- Respiratory virus vaccination, two doses prior to weaning.
‚Ä¢ IBR, BRSV, PI3, BVDV (MLV)
‚Ä¢ Coronavirus?
- Respiratory bacterial vaccination, depending on environment and herd history.
‚Ä¢ Mannheimia hemolytica, Pasteurella multocida, Histophilus somni, Mycoplasma bovis. This group requires careful consideration as 3 of these are gram negative vaccines and questions remain as to efficacy.
- Clostridial vaccination, two doses prior to weaning.
‚Ä¢ Clostridium chauvoei, Cl. septicum, Cl. novyi, Cl. sordellii, andCl. Perfringens types C and D