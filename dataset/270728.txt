I was out to dinner the other night with a group of friends.  
One by one each of us ordered, and I couldn’t help but notice that not one person ordered an entree exactly as it appeared on the menu. 
 
Everyone crafted his or her meal to their personal liking — opting for sides from other dishes, switching out one cheese for another (or not at all) and choosing gluten-free substitutes. 
 
The dinner party reminded me of a topic of discussion at a recent Food Foresight meeting: in a world of abundance, personalization has no bounds. 
 
Want hand-crafted vegan creations delivered to your desk at work? No problem. Grass-fed beef sent overnight from a farm you know by name? 
 
Yep, easy. 
 
How about a personalized meal based on your own DNA, body metrics and vitamin levels? While not so commonplace, it can — and is — being done by Campbell’s Soup and others.  
 
“From GMO-free to gluten-free to plant-based and vegan, people are picking and choosing their food choices to a level of specificity that most would have considered ‘elitist’ a decade ago,” according to the 2017 Food Foresight report, a trends intelligence report by Nuffer, Smith, Tucker, and the California Institute of Food and Agricultural Research at the University of California-Davis.
 
The report also cites a myriad of retail channels and models of food delivery as contributing to the trend, along with people’s propensity to choose foods that align with their personal values and — increasingly — their genetic makeup. 
 
Companies offering genetic testing span the gamut from highly scientific tests administered by health professionals to a number of companies that promote themselves online. 
Those who can provide tailored products and services will benefit from increased loyalty and purchases over time. 
For a mere $199 consumers can examine not only their ancestry, but also their genetic makeup. Proponents of genetic testing say it enables individuals to avoid potential health and wellness roadblocks before they emerge. 
 
In foodservice, personalization is driven by a variety of taste and preferential factors. Personalization first caught consumers’ attention with popular burrito and pizza chains — perhaps because of the ease of customization. 
 
Today, personalization is happening in white table-cloth establishments, fast-casual segments and anything in between — much to the chagrin of many chefs who take pride in their carefully crafted menus. 
 
While some restaurants — like hip Milktooth in Indianapolis and Bone Garden Cantina in Atlanta — are bucking the trend with menu postings such as, “Modifications politely declined (we will accommodate allergies IF possible),” and “NO SPECIAL ORDERS (SERIOUSLY, NOT EVEN FOR YOU),” the majority of operators we interact with have accepted personalization as the new reality. 
 
One study conducted by Mintel Research indicates 30% of millennials say the ability to customize their meals is important when considering which restaurants to visit.
 
Targeting a customer of one is no easy feat — no matter where on the supply chain you sit. 
 
Growers and shippers are accustomed to a set menu of products. 
 
Foodservice operators are used to a set menu — particularly in institutional foodservice. Retail operators only have so much shelf space and thus make product choices that appeal to the largest number of buyers. 
 
Home cooks prepare family meals, not ones made specifically for each family member.
 
To all parties, personalization can add time, expense and challenges — but it can also provide opportunities. Consumers and customers increasingly crave customization.
 
For those still taking a “one-size-fits-all” approach, can you move along the continuum of customization to better meet the needs of your customers and their consumers? 
 
Those who can provide tailored products and services will benefit from increased loyalty and purchases over time. 
 
Tim York is CEO of Salinas, Calif.-based Markon Cooperative. Centerplate is a monthly column on “what’s now and next” for foodservice and the implications for produce. E-mail timy@markon.com.
 
What's your take? Leave a comment and tell us your opinion.