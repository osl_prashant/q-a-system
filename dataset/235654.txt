The U.S. Department of Agriculture has appointed 16 new grower members to the National Potato Promotion Board.
Ten of the growers are from Idaho, others are from Colorado, Michigan, N.C., N.J., Ohio and Washington.
The USDA also reappointed 13 growers and a member of the public to the board, according to a news release. They’ll all serve three-year terms starting March 1.
The new members are:

Jared Smith, Alamosa, Colo.
Wes Pahl, Aberdeen, Idaho;
Tyson Ruff, Aberdeen;
Kasey Poulson, American Falls, Idaho;
Eric Searle, Burley, Idaho;
Mike Larsen, Declo, Idaho;
Ryan Christensen, Grace, Idaho;
Jeff Blanksma, Hammett, Idaho;
Steve Elfering, Idaho Falls;
P.J. Stevens, Idaho Falls;
Blake Matthews, Oakley, Idaho;
Blake Thorlund, Greenville, Mich.;
Jeffrey Jennings, Camden, N.C.;
John Coombs Jr., Elmer, N.J.;
Kyle Michael, Urbana, Ohio; and
Adam Weber, Moses Lake, Wash.

Growers reappointed to the board are:

Dan Moss, Declo;
Jeff Harper, Mountain Home, Idaho;
Kent Bitter, Shelley, Idaho;
Kyle Lennard, Sturgis, Mich.;
Jeff  Edling, Becker, Minn.;
Gary Gray, Clear Lake, Minn.;
Chris Hansen, Bliss, N.Y.;
Jeff Van Ray, Pingree, N.D.;
Tyler Young, Little Compton, R.I.;
E. Phillip Hickman, Horntown, Va.;
Randi Renee Hammer, Pasco, Wash.;
Molly Connors, Richland, Wash.; and
Heidi Alsum-Randall, Cambria, Wis.

The USDA reappointed Marilyn Freeman Dolan, Atwater, Calif., as the public member.
Producers, importers and the public member are represented on the board, which helps develop new markets and conduct research and promotions, according to the release. The research and promotion program operates as the U.S. Potato Board, which has been rebranded as Potato USA.