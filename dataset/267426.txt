Digital dairy app helps milk data at Waynesboro farm
Digital dairy app helps milk data at Waynesboro farm

By DAMON CLINEThe Augusta Chronicle
The Associated Press

SHELL BLUFF, Ga.




SHELL BLUFF, Ga. (AP) — No. 14433 doesn't stand out from the herd at Seven Oaks Dairy. But dairy owner Richard Watson knows everything about the brown Jersey heifer without even looking at her.
Watson keeps tabs on the cow, and dozens like her at his 3,000-acre operation near Waynesboro, through sensor-laden collars that wirelessly transmit health data to his computer and smartphone.
The product, an app called Ida, lets him know when the cow is standing or resting. It lets him know when she eats and drinks, and how much. And most important, it lets him know whether she is healthily producing the milk his farm depends on for its survival.
Sick cows are bad for business, and Ida is his early-warning device.
"If it fixes a 10 percent problem with a herd of 100 cows, we're talking tens of thousands of dollars," said Watson, whose 2,500 cows are spread across three farms. "For us, a 10 percent problem is hundreds of thousands of dollars. So if we can extract an extra 10 percent efficiency out of the system, then it's a five or six times return on the investment. So it makes complete sense to do it."
Watson's Burke County dairies have been testing Ida for a little more than a month. The "intelligent dairy app" was released last year by Netherlands-based Connecterra, which runs herd data through Google's TensorFlow open-source artificial intelligence software to ensure the bovines are behaving as they should.
To produce prodigious amounts of milk, dairy cows spend roughly 90 percent of their time resting, eating and ruminating — a process known as "chewing the cud," in which previously consumed feed is regurgitated and masticated a second time. Illness, bad weather and other factors can disrupt the feeding routine and lower a cow's milk production.
"The number of hours a cow is ruminating is what you want to know," Niels Molenaar, Connecterra's customer success manager, said during a visit to Watson's farm. "You want to know which of your cows are healthy and which are not. We point out the cows that are unhealthy and help the farmer treat them to make sure they are as happy and healthy as they can be."
Molenaar said Ida is being used on more than 2,000 cows in seven countries. Subscriptions range from $3 to $9 per month per cow depending on the service level, according to prices listed on Connecterra's website. Watson's dairy is the first in Georgia to use the technology.
The product has been called a "Fitbit for cows," but unlike the popular activity-tracking wristwatch, Ida's data is crunched by Google's TensorFlow machine-learning system, which not only monitors activity and biometrics but also can predict fertility cycles and identify looming health problems, such as a digestive disorder.
"It's not only telling you what the cow is doing, but it's taking those actions and synthesizing that data to figure out what it means," said Justin Burr, a spokesman for Google's artificial intelligence division. "Without that, you would just be observing these cows visually and trying to figure what's going on with them, which is essentially impossible and very time consuming."
Watson, a native New Zealander, started his free-range dairy 10 years ago after retiring from the University of Georgia's agricultural science faculty. His farms operate under the Hart Agriculture brand in rural Burke County, which the U.S. Department of Agriculture says is Georgia's second-biggest milk-producing county.
Nationally, dairy farms are declining in number but growing larger in size. The average dairy in 1970 had just 19 cows, according to the USDA. Today, most dairies have between 1,000 to 5,000 cows.
Watson said herd-management technology is becoming an indispensable part of his operation.
"I've got 500 cows just on this farm," he said. "As a farmer-owner-operator, I can't possibly get around to all 500 of those cows in a given day and make sure their welfare and well-being is being looked after. We needed some remote-sensing technology — something like this to help us manage our cows."