The U.S. Department of Agriculture again raised U.S. corn and soybean yields on Tuesday to the dismay of many doubting analysts.But any remaining skeptics might want to re-examine their narrative as the yields are mostly justifiable and may rise even further in the months ahead.
USDA increased U.S. corn yield to 169.9 bushels per acre from its August estimate of 169.5 bpa, and it also raised soybean yield to 49.9 bpa from last month’s 49.4 bpa. The average analyst estimates heading into the report were 168.2 and 48.8 bpa, respectively.
Even after the report, many market bulls still proclaim that the true (lower) yield will be realized “once the combines roll.”
But overall August weather – the period that most heavily influenced USDA’s recent adjustments – shows that temperatures and precipitation last month were actually supportive of a yield increase for both crops.
Looking into USDA’s state-by-state figures also reveals reasonable assumptions, especially in the minor states, and comparing the national numbers with years past suggests that nothing excessive is being predicted.
Moving ahead, the trend in USDA’s monthly yield estimates is on the bears’ side, and analysts still holding out for sizable production cuts are only doing themselves a disservice given the supporting evidence.
The precise yield is most important to the soybean balance sheet as a 1-bpa yield change is worth about 89 million bushels – roughly 20 percent of the projected ending stocks for the current marketing year.
But the impact on corn is more of a psychological one, especially if that number breaks the 170-bpa mark. About 84 million bushels of corn are at stake with a 1-bpa yield move, just 4 percent of USDA’s domestic carryout prediction for 2017/18.
State-by-State
Corn yields in 2017 are expected to surpass those of 2016 in 20 of the 32 states for which USDA provided estimates on Tuesday. Only three of them – Nebraska, Ohio and Missouri – are among the top 10 corn-producing states, but the combination of all 20 accounts for about one-third of national production.
Of these 20 states, only seven are predicted to set new outright yield records, and the margins are nothing outrageous. On average, these states will top previous bests by 4.5 bpa, with Alabama’s 8 bpa being the largest.
Observed weather justifies the 2017 predictions. Many of the states in which yields are scheduled to rise on the year are in the South, where the warmer climate typically limits yields. In 2016, temperatures in the fringe states were much warmer than normal, but this year they were average to cooler, which is supportive of bigger yields.

The rainfall pattern was also more reliable in most places outside the core Corn Belt. While parts of top producers Iowa and Illinois notably struggled with dryness this summer, the majority of the other states received timely and adequate rainfall during key periods. 
Favorable weather has also boosted soybean yields. Seventeen of the 30 states for which USDA provided 2017 estimates are set to top last year’s yields, including two of the top 10 producers – Missouri and Arkansas. These 17 states account for just less than one-quarter of national production.
Nine states are expected to set new soybean yield records, and the average margin is also small at less than 2 bpa. Further, yields in Illinois and Ohio are slated to fall less than 2 percent from year-ago, and the two states combine for one-fifth of total production.

Summing It Up
At first glance, the state-level data might not seem supportive of USDA’s national corn and soybean yields, especially since most of the year-on-year improvements reside in the minor states.
But when considering the general upward trend of yields over time, the lower national-level assumptions relative to year-ago are statistically significant, particularly for soybeans.
USDA’s 49.9 bpa soybean yield is 4.2 percent down from last year’s record 52.1 bpa. Larger year-on-year reductions have occurred only three times in the past 15 years: in 2003, 2008 and 2012.
And this bucks the recent trend as soybean yields rose by an average of 7 percent per year between 2013 and 2016.
The corn peg of 169.9 bpa is 2.7 percent lower than last year’s all-time best of 174.6 bpa. Larger year-on-year drops were observed in five of the last 15 years – 2002, 2005, 2010, 2011 and 2012 – and at least four of those years had significantly more trying weather conditions than this year.
Viewed in this light, USDA’s predictions for the third-best corn and second-best soybean yield might seem more reasonable.
Forward Trends
It might be wise to mentally prepare for corn and soybean yields to hit 170 and 50 bpa, respectively, if the trend in USDA’s past adjustments is any indication.
For six years in a row now, the final soybean yield that is published in January has been higher than USDA’s September estimate.
In the last 15 years, USDA has lowered soybean yield in October only four times, and the reduction was less than 1 percent in two of those cases. The October yield has been larger than September for four years straight now – but this excludes 2013 as an October estimate was not published due to a U.S. government shutdown.
Trends suggest the corn trajectory is likely upward, too. Whenever September yield rebounded from an August number below the long-term trend set by USDA’s World Agricultural Outlook Board, the final corn yield was actually higher than the September figure.
And similar to soybeans, corn yield tends to increase again in October if it increased in September.
Into October and especially November, USDA’s corn yields have actually gotten a little too aggressive in recent years. Since 2010, the agency’s November number increased to the final January yield only twice, in 2011 and 2012.
So if USDA’s corn yield increases in both October and November, then analysts might be justified in calling foul on the estimates.