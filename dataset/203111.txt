The Twitter account of President Donald Trump, @realDonaldTrump, promised this on the evening of Jan. 24:  "Big day planned on NATIONAL SECURITY tomorrow. Among many other things, we will build the wall!"
 
Unlike normal politicians who have their staff tweet for them, President Trump's tweets seem to come directly from his fingertips, complete with occasional use of the CAPS LOCK mode and never wavering (!) end of tweet exclamations.
 
Trump's determination to act on national security and immigration has been on display both before and after his election to the office in November. 
 
Reuters reported that Trump will sign executive orders relating to a ban refuges and suspend visas for citizens from certain Middle Eastern and African countries until more careful checking of their backgrounds can be put in place.
 
This morning, @realDonaldTrump unveiled news in these bursts:
 
 I will be making my Supreme Court pick on Thursday of next week. Thank you!
 I will be asking for a major investigation into VOTER FRAUD, including those registered to vote in two states, those who are illegal and  even, those registered to vote who are dead (and many for a long time). Depending on results, we will strengthen up voting procedures!
 
Trump's get it done or get out of the way style is in stark contrast to most politicians in Washington, D.C.  
 
There is a quote I like attributed to Johann Wolfgang von Goethe that says "Don't  give us your doubts, gives us your certainties, for we have doubts enough of our own."
 
The refreshing but ultimately alarming characteristic of President Trump so far is that he has nothing but certainties.