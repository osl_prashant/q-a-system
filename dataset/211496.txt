After a lively summer, buyers of granny smith apples and hass avocados still have a tiger by the tail.
 
U.S. Department of Agriculture shipping point price reports show that Washington 2017 crop granny smith apples on Sept. 25 traded from $47-51 per carton for size 88 apples,  compared with $13-18 per carton for 2016 size 88 red delicious fruit and $25-27 per carton for size 88 2017 crop galas.
 
There just has been too much demand for the limited supply of the tart apple this summer - fresh cut slicers depend on the variety -  and so far new crop inventories are apparently still too slim for pricing to relax any.
 
Meanwhile, the USDA reports that Dallas terminal market pricing for avocados on Sept. 25 was an astounding $95 per carton for Mexican hass 40 count fruit. Shipping point prices for Mexican avocados crossing through south Texas were $78-81 per carton for 48s. Since mid-June, hass avocado prices in south Texas have increased from a little more than $40 to almost double that now. 
High prices solve high prices; relief should be coming eventually to buyers of both hass avocados and granny smith apples, but that relief is already long overdue.
 
---
 
The news cycle keeps turning and we forget about the last disaster when something else grabs our attention. But the recovery effort from hurricane Harvey and Irma will be a long process. At the same time, I heard from one grower who reminded me that there are plenty of farms in good shape. Carolyn Byrd, with Traders Hill Farm near Jacksonville, Fla. wrote Sept. 19:
 
I just read your article on the devastation to the agriculture industry in Florida left in the wake of Irma. I would like to share some Good News with you! Here at Traders Hill Farm, we were up and running with 36 hours of being hit by Irma. I will say we feel incredibly blessed to have not sustained major damage to any part of the farm and we are still in full production, and able to deliver on schedule to our customers and distributor this week as promised.
In all the devastation that was Irma, I believe your readers would love to hear that there was also some good news. 
 
 
Carolyn brings an important reminder, and I thank her for pointing out that plenty of Florida’s great produce suppliers are still meeting the needs of the marketplace, despite the considerable damage wrought by Irma.