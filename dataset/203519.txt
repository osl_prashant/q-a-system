The U.S. isn't alone in looking to Peru for onion supplies; other markets, near and far, are lining up for Peruvian product, as well, marketers say.
"Europe seems to be growing annually with their imports of Peruvian onions," said Marty Kamer, president of Greencastle, Pa.-based Keystone Fruit Marketing Inc.
Markets outside the U.S. often prefer smaller-sizes, such as prepacks and mediums - which works out well for grower-shippers of Peruvian onions, said Ralph Diaz, export and import sales manager with Sunbury, Pa.-based Karpinski Trucking & Produce.
"There's not a strong market in the U.S. for them, so they'll ship them to Pamana and Colombia," Diaz said, noting that Japan and Europe also are showing interest in smaller onions.
Spain, as well as Europe in general, has been a reliable receiver of Peruvian onions in recent years, said Barry Rogers, president of Grant, Fla.-based Sweet Onions Trading Corp.
"The local Latin American market has been pretty good, around Central America, but it's more of a No. 2 market, Rogers said.
South America is gaining some prominence in Peru's export deal, said Delbert Bland, owner of Glennville, Ga.-based Bland Farms LLC.
"Colombia is pulling the most volume, but they're not puling as many as we ship to America," Bland said, noting that Chile also is taking some Peruvian product.
"We're selling some to each of those South American countries, but the majority by far is coming to America," he said.
One Latin American country that is not a major customer is Mexico, some shippers said.
"Mexico is traditionally a white onion market; they want a spicy onion," Rogers said. "Probably if they were short, they'd look to the north for a round yellow, but if there isn't a huge price difference and they need it, they're not averse to it."
Mexico does buy red onions from Peru, said John Williams, sales director for Lyons, Ga.-based L.G. Herndon Jr. Farms Inc.
"They go to several neighboring countries, I'm sure," he said, noting a desire to "get the most bang for the buck."
Williams noted, though, that U.S. remains Peru's "best market."
Diaz said sweet onions aren't brisk sellers in Peru's own markets.
"They have a big domestic market on the reds; that's their No. 1 onion, which they use domestically," Diaz said.
Indeed, some of those red onions are heading to the U.S. in increasing numbers, Diaz noted.
The sweet onion is gaining popularity across Peru, though, Bland noted.
"It wasn't several years ago, but now, it's really growing faster per-capita than it is in America," Bland said.
Onions bound for export are the best available, Bland said.
"Domestically, they get what's not quality enough to ship export," he said. "Normally, their domestic market buys No. 2s."
Often, the domestic market fills an early-market window in Peru, Williams said.
"When those onions get ready to go in July, they'll look for more domestic options or to Europe or other countries until we're ready to start," he said.