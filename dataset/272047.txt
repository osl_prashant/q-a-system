BC-Merc Early
BC-Merc Early

The Associated Press

CHICAGO




CHICAGO (AP) — Early trading on the Chicago Mercantile Exchange Wed:
Open  High  Low  Last   Chg.  CATTLE                                  40,000 lbs.; cents per lb.                Apr      121.30 121.85 121.30 121.72   +.62Jun      105.02 105.42 104.62 105.37   +.32Aug      104.70 105.17 104.45 105.17   +.25Oct      108.50 108.80 108.25 108.77   +.12Dec      113.00 113.12 112.62 113.07   —.15Feb      114.75 114.77 114.30 114.77   —.20Apr      115.27 115.50 114.87 115.50   —.20Jun      109.00 109.07 108.50 109.07   +.12Aug      107.75 108.22 107.75 108.22   +.22Est. sales 15,452.  Tue.'s sales 59,095 Tue.'s open int 346,558,  up 994       FEEDER CATTLE                        50,000 lbs.; cents per lb.                Apr      139.20 139.60 139.20 139.45   +.35May      139.90 140.92 139.70 140.92   +.77Aug      145.97 146.62 145.42 146.52   +.05Sep      146.72 147.37 146.20 147.30   +.15Oct      147.15 147.70 146.60 147.70   +.05Nov      146.60 147.17 146.37 147.15   —.12Jan      142.55 143.05 142.42 142.95   —.27Mar      140.45 140.85 140.22 140.85   —.55Est. sales 3,750.  Tue.'s sales 14,488  Tue.'s open int 48,473,  up 149        HOGS,LEAN                                     40,000 lbs.; cents per lb.                May       67.05  67.57  66.70  67.30   —.15Jun       74.72  74.82  73.95  74.35   —.47Jul       78.00  78.00  77.35  77.62   —.38Aug       78.17  78.30  77.72  77.97   —.20Oct       66.27  66.32  65.77  66.17   +.02Dec       60.92  60.92  60.50  60.60   —.32Feb       64.90  64.90  64.40  64.47   —.43Apr       68.40  68.40  68.02  68.10   —.30Est. sales 14,143.  Tue.'s sales 46,421 Tue.'s open int 245,235,  up 2,587      PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.