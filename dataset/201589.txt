Brazil's Agriculture Ministry on Tuesday said it is in talks with Japanese authorities to increase its exports of fresh beef and fruit to the Asian nation.
The talks are aimed at recognizing and harmonizing sanitary and other food safety regulations, and if successful Brazil will be able to increase its share of Japan's beef and fruit market sometime in the middle of 2017, the ministry said in a statement.

Brazil's agriculture minister, Blairo Maggi, whose family is among the world's biggest soybean producers, is in Japan accompanying President Michel Temer on an official visit through Wednesday.

Currently the import of fresh Brazilian beef to Japan is largely blocked. Brazil exported only 1.4 tons of beef to Japan this year through September compared with 1 million tons exported worldwide. The figures are for fresh and processed beef and their sub products.

Japan is a major buyer of Brazilian soybeans, corn and cotton, the ministry said.
Brazil and Japan in talks over beef exports


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.