Click here for related charts.

CORN COMMENTS
The national average corn basis improved 3/4 cent from last week to 12 1/4 cents below July futures. The national average cash corn price softened 6 1/2 cents to $3.64 3/4.
Basis is weaker than the three-year average, which is 2 cents under futures for this week.
 
SOYBEAN COMMENTS
The national average soybean basis softened 1 cent from last week to stand 16 1/2 cents below July futures. The national average cash price is steady with last week at $9.15 1/4.
Basis is softer than the three-year average, which is 5 cents above futures for this week.
 
WHEAT COMMENTS
The national average soft red winter (SRW) wheat basis softened 1 cent from last week to 2 1/2 cent below July futures. The average cash price firmed 2 1/4 cents from last week to $4.45 1/2. The national average hard red winter (HRW) wheat basis firmed 1 3/4 cents from last week to 66 cents below July futures. The average cash price improved 13 3/4 cents from last week to $3.91 1/2.
SRW basis is firmer than the three-year average of 15 cents under futures. HRW basis is much weaker than the three-year average, which is 38 cents under futures for this week.
 
CATTLE COMMENTS
Cash cattle trade averaged $136.07 last week, down 20 cents from the previous week. Cash cattle have traded $3 to $5 lower so far this week. Last year at this time, the cash price was $127.52.
Choice boxed beef prices firmed 32 cents from last week at $250.71. Last year at this time, Choice boxed beef prices were $226.42.
 
HOG COMMENTS
The average lean hog carcass bid firmed $3.30 over the past week to $82.10. Last year's lean carcass price on this date was $81.62.
The pork cutout value improved $4.66 from last week to $94.59. Last year at this time, the average cutout price stood at $86.45.