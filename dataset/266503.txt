AP-WI--Wisconsin News Digest 6 pm, WI
AP-WI--Wisconsin News Digest 6 pm, WI

The Associated Press



Here's a look at AP's general news coverage in Wisconsin. Questions about coverage plans go to News Editor Doug Glass at 612-332-2727 or dglass@ap.org. Jeff Baenen is on the desk, to be followed by Gretchen Ehlke at 6 a.m.
This information is not for publication or broadcast, and these plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
COMING TOMORROW:
WISCONSIN-SUPREME COURT
MADISON, Wis. — Wisconsin Supreme Court candidates Rebecca Dallet and Michael Screnock were scheduled to debate for a final time Friday, just days before the April 3 election.
TOP STORIES:
WALKER-SPECIAL ELECTION
MADISON, Wis. — Wisconsin Gov. Scott Walker reluctantly issued an executive order Thursday scheduling special elections to fill two vacant legislative seats, as Senate Republicans abandoned their efforts to block the contests amid Democratic criticism that the GOP is afraid of losing more seats. By Todd Richmond. SENT: 540 words, photos.
With:
WALKER-SPECIAL ELECTIONS-THE LATEST, WISCONSIN-SPECIAL ELECTIONS
SEX ASSAULT-TV AD
MILWAUKEE — The family of two sexual assault victims says a television ad that alludes to their case to attack a Wisconsin Supreme Court candidate is hurtful and should be removed from the air. By Ivan Moreno. SENT: 510 words.
HOUSE SPEAKER-FUTURE
WASHINGTON — Paul Ryan's future as House speaker has been such a topic of speculation that even the simple question of whether he will seek re-election to his Wisconsin seat remains secret. Officially, Ryan says he's still deciding. But a person familiar with Ryan's thinking told The Associated Press this week the speaker plans to file campaign paperwork and intends to win his seat. By Lisa Mascaro. SENT: 1,020 words, photos.
AROUND THE STATE:
CROP REPORT
DES MOINES, Iowa — Corn has been dethroned as the king of crops as farmers report they intend to plant more soybeans than corn for the first time in 35 years. The U.S. Department of Agriculture says in its annual prospective planting report released Thursday that farmers intend to plant 89 million acres (36 million hectares) in soybeans and 88 million acres (35.6 million hectares) in corn. By David Pitt. SENT: 590 words, photos.
TEENAGE DRINKING
MADISON, Wis. — A survey of Wisconsin high school students shows underage drinking is down, reflecting a national trend. SENT: 330 words.
IN BRIEF:
WISCONSIN SUPREME COURT, SENATE-WISCONSIN-DEBATE, FATAL STABBING-DUNN COUNTY, DEMOCRATIC CONVENTION-MILWAUKEE, BLUFF CLIMBING DEATH
SPORTS:
BREWERS-PADRES
SAN DIEGO — Chase Anderson and the Milwaukee Brewers face left-hander Clayton Richard and the rebuilding San Diego Padres on opening day. By Bernie Wilson. UPCOMING: 600 words, photos. Game starts 3:10 p.m. CT.
___
If you have stories of regional or statewide interest, please email them to apmlw@ap.org. If you have photos of regional or statewide interest, please send them to the AP in New York via FTP or email (statephotos@ap.org). Be sure to call to follow up on emailed photos: 800-845-8450, ext. 1900. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.