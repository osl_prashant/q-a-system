Rabobank has released its annual survey of the world’s largest dairy companies and there have been a number of major moves.The top three remained unchanged with Nestlé, Lactalis and Danone falling into their respective spots.
Chinese company Yili moved up two positions to #8 and German company Müller saw the largest rise going from #20 to #15. The largest negative mover was French company Savencia.
“First of all we see the effects of currency volatility, particularly the strengthening of the U.S. dollar against other currencies,” says Kevin Bellamy, global dairy strategist for Rabobank.
The collective turnover between the top 20 dairy companies was $194 billion in 2015, down 13% from the previous year. However, it was up 4% in euros at €174 billion.
“That’s more of a comment about the strength of the dollar versus the euro than performance of the companies on the list, but it has had an effect,” Bellamy adds.
Four U.S. companies are in the top 20 with Dairy Farmers of America going up a spot in the rankings to #4, jumping ahead of New Zealand’s Fonterra. Dean Foods fell one spot to #10, Kraft/Heinz climbed three spots to #13 and Schreiber Foods remained at #18. Land O’ Lakes fell out of the top 20 after ranking #19 last year.
“Second main takeaway from the list is the effect of those low commodity prices which have affected dairy throughout 2015,” Bellamy says.
In 2014, whole milk powder sold for a high of $5,400/metric ton and an average of $3,764/ton. Last year whole milk powder averaged just $1,450/ton and the high was only $2,462/ton.
“And third takeaway would be the continued intense level of mergers and acquisition activity,” Bellamy says.
Some of these recent acquisitions include Danone’s purchase of WhiteWave Foods earlier in the month to get into the organic dairy market and non-dairy milks. Müller purchased Dairy Crest in 2014 and it helped move the company further in the rankings. Canada’s Agropur was the only new addition to the list coming in at #20 thanks to an acquisition of Davisco in 2014.
With China’s economy slowing down Africa could be the next global market for expansion. In 2014 there were three dairy mergers and acquisitions in Africa. The following year it jumped to 14 deals and so far in 2016 there have been four business agreements made in Africa. 
For more on the Global Dairy Top 20 read Rabobank's report or watch the following video from AgDay:
Top 20 Dairy Companies Ranked by Rabobank


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.