Ideal growing conditions in February bode well for a timely and bountiful tomato season in Florida, growers say.
“I’d say everything is on track — probably a little ahead of schedule with the warm weather in Florida,” said Tony DiMare, vice president of DiMare Co., Homestead, Fla., in late February.
DiMare and other growers said daytime temperatures across the region hit the mid- to upper 80s through much of February, though early March saw cooler temperatures with lows in the 40s.
The Florida tomato season starts in October and runs into June with spring production in the Homestead and Immokalee, Fla., regions generally cranking up to full volume by mid-March.
Volume from late fall plantings started to peter out in February but accelerated again in March, DiMare said.
Quality remained consistently very good though, DiMare said.
“Volume is fairly light right now,” he said Feb. 23, “but overall, quality is good and sizing has been very good. We’re honestly getting April weather – mid-80s to upper 80s.”
DiMare Co. grows all varieties of tomatoes, plus avocados, from early summer to December, DiMare said.
 
Pricing
As of Feb. 23, 25-pound cartons of U.S. 1 or better loose mature-green tomatoes from central and South Florida were $8.95-9.95 in all sizes, according to the U.S. Department of Agriculture. A year earlier, the same product was $6.95.
Weather problems earlier in the winter led to a downturn in Florida supplies by late February, and Mexico was still the dominant tomato source then, said Chuck Weisinger, president and CEO of Weis-Buy Farms Inc., a Fort Myers, Fla.-based brokerage.
“We had some serious, serious wind problems back in January, and we’re suffering from that right now,” he said.
By March 6, prices for 25-pound cartons of U.S. 1 or better mature-green tomatoes had jumped to $13.95 for size 5x6; $11.95 for size 6x6; and $9.95 for size 6x7, according to USDA numbers.
Growers need stronger markets, Weisinger said.
“To grow one acre of tomatoes costs $11,000,” he said.
New federal rules requiring truckers to keep electronic logs are adding to the costs, Weisinger said.
“It’s critical they do something about the trucking situation,” he said.
“I want truckers to live a good life, but prices have skyrocketed in the last couple of months. If they don’t do something pretty soon, they may have to leave crops in the field.”
According to figures from the USDA, which reports from March to May, Florida accounts for 55% of grape and cherry tomatoes in the U.S. market, and 41% of the round tomatoes.
Fresh tomato sales out of Florida averaged $18.6 million in sales from 2014-16, USDA reported.
The spring tomato crop was looking good at Immokalee-based Oakes Farms Inc., said Steve Veneziano, director of operations.
“For the spring, we’re going to have real strong volume,” he said. “We’ve had a lot of bumps with Hurricane Irma (in September) and its after-effects, but that’s in the past.”