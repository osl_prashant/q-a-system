Carlisle, Pa.-based Giant Food Stores and Martin’s Food Markets raised $655,274 during the Children’s Miracle Network balloon campaign.
The annual fundraiser benefits CMN Hospitals, a nonprofit organization, through in-store customer and employee donations, according to a news release.
Since 1996, the company has donated more than $40 million to local children’s hospitals.
“For more than 20 years we have been honored to support our local CMN Hospitals, who provide critical treatments and healthcare services that help kids live healthy lives,” Tom Lenkevich, Giant/Martin’s president, said in the release.
Giant/Martin’s employees also volunteer at radio and television events in support of Children’s Miracle Network hospitals, according to the release.
The company operates 200 grocery stores throughout Pennsylvania, Maryland, Virginia and West Virginia, and seven hospitals across those states received the balloon campaign funds.