BC-Merc Table
BC-Merc Table

The Associated Press

CHICAGO




CHICAGO (AP) — Futures trading on the Chicago Mercantile Exchange Mon:
Open  High  Low  Settle   Chg.CATTLE                                  40,000 lbs.; cents per lb.                Apr      117.72 118.40 117.10 117.15   +.60Jun      104.82 105.95 103.92 104.17   +.52Aug      105.12 105.87 104.00 104.20   —.07Oct      109.35 109.97 108.37 108.60   —.12Dec      113.55 114.02 112.40 112.80   —.15Feb      115.10 115.70 114.47 114.80        Apr      115.92 116.17 115.30 115.40   —.05Jun      109.40 109.60 108.65 108.77   —.03Aug      108.00 108.00 107.60 107.60   —.25Est. sales 49,248.  Fri.'s sales 40,115  Fri.'s open int 345,443                 FEEDER CATTLE                        50,000 lbs.; cents per lb.                Apr      139.90 141.30 139.00 139.22   —.15May      141.40 142.65 140.07 140.37        Aug      146.00 146.77 144.35 144.77   —.33Sep      147.10 148.00 145.82 146.27   —.30Oct      147.52 148.37 146.22 146.70   —.20Nov      147.00 147.75 145.87 146.35   +.10Jan      143.40 143.72 141.85 142.35   +.25Mar      141.00 141.00 140.75 140.75  +1.38Est. sales 13,363.  Fri.'s sales 16,197  Fri.'s open int 50,941                  HOGS,LEAN                                     40,000 lbs.; cents per lb.                May       69.20  69.50  68.00  68.17  —1.53Jun       77.40  78.07  76.40  76.80   —.85Jul       79.30  79.80  78.25  78.67   —.90Aug       79.22  79.50  78.02  78.37  —1.05Oct       67.95  68.20  67.12  67.40   —.95Dec       61.82  61.87  61.17  61.42   —.70Feb       65.52  65.57  65.10  65.25   —.65Apr       69.12  69.15  68.67  68.70   —.82May                            74.00   —.25Jun                            77.30   —.25Jul                            76.90   —.25Aug                            76.50   —.25Est. sales 32,849.  Fri.'s sales 41,195  Fri.'s open int 246,040,  up 1,975      PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.