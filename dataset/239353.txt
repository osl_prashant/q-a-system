BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Thursday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMar                              123.95  Up   1.95Mar      121.35  122.45  121.35  122.45  Up   1.40May                              126.00  Up   1.90May      122.05  124.05  121.50  123.95  Up   1.95Jul      124.20  126.10  123.65  126.00  Up   1.90Sep      126.35  128.15  125.85  128.10  Up   1.90Dec      129.60  131.35  129.15  131.30  Up   1.80Mar      132.55  134.55  132.45  134.55  Up   1.75May      134.95  136.50  134.95  136.50  Up   1.75Jul      136.70  138.20  136.70  138.20  Up   1.70Sep      138.30  139.70  138.30  139.70  Up   1.55Dec      140.80  142.20  140.80  142.20  Up   1.55Mar      144.40  144.70  144.15  144.70  Up   1.50May                              146.45  Up   1.50Jul                              148.15  Up   1.50Sep                              149.75  Up   1.50Dec                              152.15  Up   1.50