How well do you personally model integrity in your day-to-day interactions?




People are always watching us, appraising whether we keep our promises and “do what we say we will do.” Colleagues watch us. Neighbors watch us. Family members watch us. Even strangers watch us!
If we live with integrity, then those watchers see that we "do what we say we will do." They learn they can trust our word because we back it up with aligned actions.
If we don’t live with integrity, then those watchers see us break our promises all the time. They learn they can’t trust our word, so they don’t.
Sometimes that lack of integrity is difficult to observe. It’s not obvious. Broken promises are made subtly, over time. Other times, a lack of integrity is bold and clear. In today’s Culture Leadership Charge video episode, I share an example I experienced on a golf course many years ago.
Our integrity is fragile. It’s only as good as every kept promise. Do what you say you will do, every day -- with colleagues, neighbors, family, and strangers.


S. Chris Edmonds
April 25, 2017
Leadership
Leadership and Management