Farmer's Best general manager Leonardo Tarriba (left), and vice president of sales and marketing Steve Yubeta will be at the Farmer's Best Produce Marketing Association's Fresh Summit booth in New Orleans. Courtesy Farmer's Best.
 With acquisitions of land in in San Luis Potosi, Mexico, Farmer’s Best is expanding shipments of cucumbers, roma tomatoes and green bell peppers.
The 51-year-old Farmer’s Best can ship those three vegetables during the traditional winter growing season and now through the summer, according to a news release. Bell pepper production uses upgraded technology, including a new sorting machine.
The company plans to exhibit at the Produce Marketing Association’s Fresh Summit, Oct. 20-22 in New Orleans, at booth No. 1057.
“We are certainly very excited about what’s been taking place lately here at Farmer’s Best,”
 Leonardo Tarriba, Farmer’s Best general manager, said in a news release. “PMA Fresh Summit is one of the most important industry events that we attend during the year.”
Farmer’s Best also plans to highlight the company’s food safety, social responsibility and sustainability programs at Fresh Summit, according to the release.