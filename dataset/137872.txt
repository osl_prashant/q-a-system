USDA Weekly Grain Export Inspections
			Week Ended April 20, 2017




Corn



Actual (MT)
1,453,506


Expectations (MT)

1,000,000-1,300,000



Comments:
Inspections climbed 123,437 MT from the previous week and the tally topped expectations. Inspections for 2016-17 are up 62.2% from year-ago, compared to 64.1% ahead the previous week. USDA's 2016-17 export forecast of 2.225 billion bu. is up 17.2% above the previous marketing year.



Wheat



Actual (MT)
612,536


Expectations (MT)
400,000-600,000 


Comments:

Inspections are down 72,778 MT from the previous week and the tally just topped expectations. Inspections for 2016-17 are running 31.8% ahead of year-ago compared to 31.0% ahead last week. USDA's export forecast for 2016-17 is at 1.025 billion bu., up 31.0% from the previous marketing year.




Soybeans



Actual (MT)
634,877


Expectations (MMT)
400,000-600,000 


Comments:
Export inspections climbed 189,179 MT from the previous week, and the tally was stronger than expected. Inspections for 2016-17 are running 14.5% ahead of year-ago, compared to 13.7% ahead the previous week. USDA's 2016-17 export forecast is at 2.025 billion bu., up 4.6% from year-ago.