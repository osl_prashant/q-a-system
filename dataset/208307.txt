Chicago Mercantile Exchange live cattle futures settled moderately higher on Wednesday, with help from short-covering and improved wholesale beef values, said traders.December received added support from fund buying after the contract topped the 10-day moving average of 109.648 cents.
October live cattle finished up 0.275 cent per pound at 104.700 cents, and December closed 0.425 cent higher at 109.800 cents.
Some grocers bought beef to avoid shortages after plants closed on Monday for the U.S. Labor Day holiday, traders and analysts said.
A trader, however, said beef demand tends to struggle after Labor Day - the last grilling holiday of the summer - and as consumers focus more on buying items to get their children settled into classrooms.
"The market may want to see if cash prices can stabilize before making any further moves on the futures end," said Allendale Inc. chief strategist Rich Nelson.
Market participants awaited this week's sale of market-ready, or cash, cattle in the U.S. Plains that last week brought $103 to $105 per cwt.
Bullish traders believe packers will pay at least $105 per cwt for supplies given better wholesale beef prices and extremely profitable packer margins.
Contrarians point to ample supplies of slaughter cattle at heavier weight, which tends to pump more meat into the retail sector.
Technical buying and live cattle futures advances pulled up CME feeder cattle contracts.
September closed 1.250 cents per pound higher at 143.300 cents. 
Mostly Firmer Hog Futures
The bulk of CME lean hog contracts finished in slightly bullish territory.
In a trading strategy known as bear spreading, investors bought deferred months and simultaneously sold October futures following lower cash and wholesale pork prices.
October ended down 0.200 cent per pound to 63.550 cents. December finished up 0.100 cent to 60.275 cents, and February closed up 0.175 cent to 64.725 cents.
Hog numbers remain seasonally abundant and the end of the summer bacon-lettuce-tomato sandwich season continues to drag down wholesale pork prices, a trader said.
Bullish investors were also concerned about the U.S. government's weekly data that showed increased hog weights, which makes more product available to retailers, said Nelson.
However, he said pork demand could get a boost from grocers looking to feature more product during National Pork Month in October.