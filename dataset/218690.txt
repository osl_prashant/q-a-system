In his annual State of the Port speech, Port of Oakland executive director Chris Lytle said cargo volumes could hit new records in 2018, following up a year high traffic in 2017.
“This is our time,” Lytle said in a news release on the annual forecast on Jan. 30. “We’ve spent much of this decade working with business partners to build out the cargo volume delivery platform our customers want and in 2018 we’re putting it to work.”
Milestones for 2018, Lytle said, include:

A project to heighten four ship-to-shore cranes for megaship operations;
Opening of Cool Port Oakland, a 283,000-square-foot refrigerated distribution center for containerized perishables; and
Expansion of TraPac marine terminal to double its Oakland footprint.

The port set a cargo record last year, handling 2.42 million 20-foot-equivalent units. With completion of Cool Port and other facilities at the port, officials are projecting growth through 2022. Cool Port is expected to handle 30,000 containers a year, and the Seaport Logistics Complex, opening in 2019, will also increase volume, according to the release.