Like so many other farmers, consultants and Extension educators, Nathan Mueller rarely heads to the field to scout without bringing along his smartphone.Mueller, an Extension cropping systems educator with the University of Nebraska-Lincoln, has tried dozens of apps and mobile-friendly tools and picked up a few favorites along the way.
For starters, Mueller suggests farmers try an app called "Field GPS." Your phone's GPS is accurate to within a few feet‚Äîgood enough for most scouting purposes. Field GPS helps users geotag a problem spot in a field, take notes and photos, and email data as aCSV file for easy transfer into precision ag software.
That's just scratching the surface of what your smartphone can do around the farm. Here are some ideas and advice to raise your mobile device's IQ. 
Before You Head to the Field
Make sure your team is using the same apps for efficiency and consistency.
Know the pros and cons of each app. Does it adequately perform four or five functions, or does it perform one function really well?
Learn which apps do and don't need GPS functionality or an Internet connection, especially before heading to remote areas.
Phone (or Tweet) a Friend
Your trusted advisers have always been a phone call away; now, technology allows for new interesting ways to reach them‚Äînamely, using a video chat app such as AgriSync, FaceTime or Skype.
AgriSync was designed with farmers in mind. Most farmers tap a small team of retailers, consultants or other trusted advisers for agronomic or technical support. But when does a delay waiting for a service call turn from a hassle to a problem?
AgriSync helps connect farmers to their adviser network through video chat. Could a quick video chat save a two- or four-hour service call? Not always, but it doesn't hurt to check first, says Casey Niemann, AgriSync president.
"Most tech breakdowns don't need a wrench, they need an expert set of eyes," he says.
AgriSync is free for farmers to use (advisers pay a monthly or yearly subscription fee to join).
Mueller says farmers and crop consultants are starting to use another on-the-go resource‚Äîeach other. Twitter, a 140-character-or-less social media network, is one of the best platforms to talk about farming conditions, he says. "I have a group of farmers sending me direct messages all the time."
Swiss Army Smartphone
Various apps can help transform a smartphone into any number of tools. Check the Apple Store or Google Play to add these virtual tools.
Flashlight
Bubble Level
Field Guides: examples include ScoutPro Corn, USDA's Vector Spray, University of Missouri's ID Weeds and Virtual Farm Manager.
Click Counter: "When you're counting soybeans, for example, use this app to store your counts instead of trying to keep track in a notebook as you go," Mueller says.