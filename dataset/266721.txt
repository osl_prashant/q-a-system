NC commercial fishing group sues over new license policy
NC commercial fishing group sues over new license policy

The Associated Press

WILMINGTON, N.C.




WILMINGTON, N.C. (AP) — The organization that represents commercial fishermen in the state says in a lawsuit that the North Carolina Marine Fisheries Commission violated open meetings laws when it wrote a proposal to redefine eligibility for a commercial fishing license.
The StarNews of Wilmington reports the North Carolina Fisheries Association is asking the courts to overturn the new definition, which the commission narrowly approved in February. Legislators also must approve the new definition.
Among the changes in the policy is a requirement that commercial fishermen record at least 1,000 pounds of fish caught or 15 trips in any two of a running five-year period.
The lawsuit was filed last week in Carteret County Superior Court.
In an interview last week, commission chairman Sammy Corbett described his concerns about how the vote was reached.
___
Information from: The StarNews, http://starnewsonline.com