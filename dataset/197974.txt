USDA Weekly Export Sales Report
			Week Ended May 18, 2017





Corn



Actual Sales (in MT)

Combined: 457,700
			2016-17: 457,200
			2017-18: 500



Trade Expectations (MT)
600,000-1,100,000


Weekly Sales Details
Net sales of 457,200 MT for 2016-17 were down 35% from the previous week and 33% from the prior four-week average. Increases were reported for Japan (227,400 MT, including 156,300 MT switched from unknown destinations and decreases of 5,500 MT), Mexico (68,600 MT, including decreases of 2,600 MT), Taiwan (67,000 MT, including 65,000 MT switched from unknown destinations), South Korea (62,800 MT), and Bangladesh (53,400 MT, including 50,000 MT switched from Nigeria). Reductions were reported for unknown destinations (120,000 MT), Nigeria (50,000 MT), and the Dominican Republic (3,200 MT). For 2017-18, net sales of 500 MT were reported for unknown destinations. 


Weekly Export Details
Exports of 1,052,400 MT were down 32% from the previous week and 14% from the prior four-week average. The primary destinations were Japan (464,900 MT), Mexico (348,600 MT), Taiwan (70,300 MT), Bangladesh (52,900 MT), and Peru (42,500 MT).


Comments and Performance Indicators
Sales fell short of expectations. Export commitments for 2016-17 are running 27% ahead of year-ago compared to 31% ahead the week prior. USDA projects exports in 2016-17 at 2.225 billion bu., up 17.2% from the previous marketing year.




Wheat



Actual Sales (in MT)

Combined: 544,800
			2016-17: 201,900
			2017-18: 342,900



Trade Expectations (MT)
250,000-650,000 


Weekly Sales Details
Net sales of 201,900 metric tons for delivery in marketing year 2016-17 were down 19% from the previous week, but up 49% from the prior four-week average. Increases were reported for Mexico (119,500 MT, including decreases of 400 MT), Venezuela (60,000 MT, switched from unknown destinations), Japan (56,300 MT, including 28,600 MT switched from unknown destinations), and Taiwan (52,700 MT, including 50,400 MT switched from unknown destinations). Reductions were reported for unknown destinations (174,000 MT), Vietnam (50,000 MT), and Nigeria (17,500 MT). For 2017-18, net sales of 342,900 MT were reported primarily for Mexico (231,700 MT), Guatemala (37,700 MT), unknown destinations (28,400 MT), and South Korea (16,000 MT). 


Weekly Export Details
Exports of 733,600 MT were up 9% from the previous week and 17% and from the prior four-week average. The destinations were primarily Algeria (105,200 MT), Mexico (95,700 MT), Japan (92,800 MT), South Korea (79,400 MT), and Venezuela (60,000 MT).


Comments and Performance Indicators
Sales were within expectations. Export commitments for 2016-17 are running 37% ahead of year-ago versus 36% ahead the week prior. USDA projects exports in 2016-17 at 1.035 billion bu., up 33.5% from the previous marketing year.




Soybeans



Actual Sales (in MT)

Combined: 478,700
			2016-17: 472,700
			2017-18: 6,000



Trade Expectations (MT)
200,000-550,000 


Weekly Sales Details
Net sales of 472,700 MT for 2016-17 were up 33% from the previous week and 9% from the prior four-week average. Increases were reported for unknown destinations (148,500 MT), China (130,000 MT, including 66,000 MT switched from unknown destinations), Pakistan (65,000 MT), Canada (31,400 MT, including decreases of 100 MT), and Indonesia (31,100 MT). Reductions were reported for Cuba (200 MT). For 2017-18, net sales of 6,000 MT were reported for Taiwan (4,000 MT) and Indonesia (2,000 MT). 


Weekly Export Details
Exports of 334,700 MT were down 3% from the previous week and 32% from the prior four-week average. The destinations were primarily China (142,000 MT), Mexico (68,900 MT), Japan (32,900 MT), Canada (29,800 MT), and Indonesia (18,800 MT). 


Comments and Performance Indicators
Sales met expectations. Export commitments for 2016-17 are running 22% ahead of year-ago, compared to 22% ahead the previous week. USDA projects exports in 2016-17 at 2.050 billion bu., up 5.9% from year-ago.




Soymeal



Actual Sales (in MT)
Combined: 159,900
			2016-17: 125,000
			2017-18: 34,900


Trade Expectations (MT)

50,000-250,000 



Weekly Sales Details
Net sales of 125,000 MT for 2016-17 were up 10% from the previous week and 7% from the prior four-week average. Increases were reported for the Philippines (30,300 MT), Mexico (22,400 MT), Colombia (22,000 MT), Panama (20,500 MT, including 8,100 MT switched from unknown destinations), and Canada (9,000 MT). Reductions of 300 MT were reported for the Dominican Republic. For 2017-18, net sales of 34,900 MT were reported primarily for Honduras (19,200 MT) and Guatemala (15,300 MT). 


Weekly Export Details
Exports of 149,200 MT were down 11% from the previous week and 14% from the prior four-week average. The destinations were primarily the Philippines (31,600 MT), Thailand (30,600 MT), Mexico (27,800 MT), Venezuela (23,100 MT), and Canada (11,400 MT).


Comments and Performance Indicators
Sales matched expectations. Export commitments for 2016-17 are running 1% ahead of year-ago compared with 2% ahead of year-ago the week prior. USDA projects exports in 2016-17 to be up 1.1% from the previous marketing year.




Soyoil



Actual Sales (in MT)

2016-17: 10,000



Trade Expectations (MT)
5,000-42,000


Weekly Sales Details
Net sales of 10,000 MT for 2016-17 were down 33% from the previous week and 24% from the prior four-week average. Increases were reported for Mexico (3,300 MT), Colombia (3,000 MT), El Salvador (1,900 MT), Nicaragua (1,200 MT), and the Dominican Republic (1,000 MT). Reductions were reported for South Korea (700 MT) and the United Kingdom (300 MT). 


Weekly Export Details
Exports of 4,600 MT--a marketing-year low--were down 85% from the previous week and 83% from the prior four-week average. The destinations were primarily Mexico (3,700 MT), Canada (700 MT), and Honduras (100 MT). 


Comments and Performance Indicators
Sales were within expectations. Export commitments for the 2016-17 marketing year are running steady with year-ago, compared to 2% ahead last week. USDA projects exports in 2016-17 to be up 2.7% from the previous year.




Cotton



Actual Sales (in RB)
Combined: 252,400
			2016-17: 16,200
			2017-18: 236,200


Weekly Sales Details
Net upland sales of 16,200 RB for 2016-17--a marketing-year low--were down 87% from the previous week and 88% from the prior four-week average. Increases were reported for Turkey (10,800 RB), Brazil (8,800 RB), Vietnam (8,500 RB, including 4,400 RB switched from China and 400 RB switched from Indonesia), Malaysia (3,400 RB, including 500 RB switched from Indonesia), and China (3,100 RB). Reductions were reported for India (11,500 RB) and Bangladesh (4,300 RB). For 2017-18, net sales of 236,200 RB reported primarily for Vietnam (125,000 RB), Indonesia (42,000 RB), China (26,400 RB), were partially offset by reductions for Mexico (3,300 RB). 


Weekly Export Details
Exports of 332,800 RB were down 14% from the previous week and 9% from the prior four-week average. The primary destinations were Turkey (76,700 RB), Vietnam (61,000 RB), India (47,100 RB), Mexico (22,400 RB), and Pakistan (14,800 RB). 


Comments and Performance Indicators
Export commitments for 2016-17 are 68% ahead of year-ago, compared to 71% ahead the previous week. USDA projects exports to be up 58.5% from the previous year at 14.500 million bales.




Beef



Actual Sales (in MT)

2017: 7,100



Weekly Sales Details
Net sales of 7,100 MT reported for 2017 were down 6% from the previous week and 49% from the prior four-week average. Increases were reported for Mexico (1,700 MT, including decreases of 100 MT), Canada (1,600 MT, including decreases of 100 MT), Japan (1,400 MT, including decreases of 2,200 MT), South Korea (700 MT, including decreases of 200 MT), and Taiwan (700 MT, including decreases of 100 MT). Reductions were reported for Indonesia (100 MT). 


Weekly Export Details
Exports of 13,600 MT were down 3% from the previous week, but up 2% from the prior four-week average. The primary destinations were Japan (4,700 MT), South Korea (3,000 MT), Mexico (1,600 MT), Hong Kong (1,500 MT), and Taiwan (1,100 MT). 


Comments and Performance Indicators
Weekly export sales compare to 7,900 MT the week prior. USDA projects exports in 2017 to be up 10.0% from last year's total.




Pork



Actual Sales (in MT)
2017: 17,600


Weekly Sales Details
Net sales of 17,600 MT reported for 2017 were up 13% from the previous week and 1% from the prior four-week average. Increases were reported for Mexico (7,100 MT), Japan (4,200 MT), the Dominican Republic (1,400 MT), Australia (1,200 MT), and Canada (1,100 MT). 


Weekly Export Details
Exports of 21,900 MT were down 2% from the previous week and 3% from the prior four-week average. The destinations were primarily Mexico (7,000 MT), Japan (3,600 MT), China (2,400 MT), South Korea (2,200 MT), and Hong Kong (1,400 MT). 


Comments and Performance Indicators

Export sales compare to a total of 15,600 MT the prior week. USDA projects exports in 2017 to be 9.8% above last year's total.