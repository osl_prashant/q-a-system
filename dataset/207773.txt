Markesan,Wis.-based Trembling Prairie Farms Inc. is offering Midwest celery until early October. 
Supplies of the firm’s celery were increasing in late July, the beginning of the 12-week harvest, according to a news release.
 
An Alsum Farms & Produce grower partner, the company grows celery in the muck soils of Green Lake County, according to the release.
 
Trembling Prairie Farms, owned and operated by John and Connie Bobek, is in its sixth year of growing and packing celery for retail and foodservice markets.
 
The farm, a commercial grower of potatoes and onions, started with just 3 acres of celery in 2012. Acreage has grown to about 35 acres this year, according to the release. Farm Manager Doug Lee said in the release that the company features a Midwestern celery variety that brings a mild flavor and a big crunch to the local market.
 
Packs offered include:
24 count naked or sleeved;
30 count naked or sleeved; 
36 count naked or sleeved; and 
18 count celery hearts.