Grains mixed and livestock higher
Grains mixed and livestock higher

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mixed Monday in early trading on the Chicago Board of Trade.
Wheat for May delivery fell 9.20 cents at $4.6020 a bushel; May corn was off 11 cents at $3.7720 a bushel; May oats was up .80 cent at $2.3320 a bushel while May soybeans rose 1.20 cents at $10.2460 a bushel.
Beef and pork were higher on the Chicago Mercantile Exchange.
April live cattle rose 6.75 cents at $1.1998 a pound; Apr feeder cattle was up 3.70 cents at $1.3930 a pound; April lean hogs advanced 16.07 cents at .6845 a pound.