Bentonville, Ark.-based Wal-Mart has begun offering Signature Sidekicks by Robert Irvine, a line of four fresh-cut vegetable products for use in cooking and meal preparation.
They’re supplied by Robert Irvine Foods, Tampa, Fla. Celebrity chef Robert Irvine appears in videos for the products posted Oct. 11, in which he suggests using them with chicken, salmon, shrimp or beef.
Preparation instructions are on the back. Ingredients are separately bagged within the pack. Preparation time is 10 minutes or less, according to Robert Irvine Foods.
Signature Sidekicks products include:
Garlic Balsamic Broccoli with broccoli, red onions, garlic balsamic oil, golden raisins and toasted pine nuts.
Sesame Ginger Stir Fry with bok choy, red onions, carrots, snow peas, Asian sesame ginger sauce, garlic and grapeseed oil.
Braised Collard Greens with collard greens, cider vinegar, mustard barbecue sauce, red onions, applewood smoked bacon and grapeseed oil.
Provencal Ratatouille with red onions, zucchini, yellow squash, red swiss chard, Kalamata olives, sundried tomato mixes, garlic and grapeseed oil.