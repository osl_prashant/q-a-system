Greg Peterson is great at finding trends in the agricultural equipment market.

This week, Machinery Pete is continuing to see sellers get strong prices for good condition used equipment six weeks into 2018.

Watch Machinery Pete every Monday morning on AgDay.