Consumer confidence in the safety of food at grocery stores continues to rise, though many consumers want retailers to impose tough food safety standards on their suppliers, according to the Food Marketing Institute’s U.S. Grocery Shopping Trends 2017. 
The survey found that 87% of consumers said they were confident that food in the grocery store is safe in 2017, up from 86% in 2016 and 84% in 2015.
 
The report said shoppers look for cues to determine the level of trust they may instill in a particular store.
 
“If a store is seen to neglect proper inventory management of perishable goods, shoppers often say they just won’t buy products from those departments,” the report said. “Once lost, consumer confidence can be difficult for a retailer to regain.”
 
When asked what store behaviors make shoppers more likely to shop at a particular store, 77% of consumers said that they would shop at stores who “impose strict food safety standards on suppliers.”
 
Also rated higher — at 76% who approve — are stores who are “proactive/prompt communicating recalls.”
 
That’s higher than 69% who said they would favor stores who have a “reputation for selling high-quality goods” and 69% who said they would favor stores who make it “easy to find out sourcing of fresh produce.”
 
The survey said 95% of U.S. shoppers “trust their grocery store to ensure that the food they purchase is safe,” up from 94% in 2016.
 
“Shoppers may trust the food from the grocery store, but this is due (in part) to the large amount of trust they put in government organizations, such as the (Food and Drug Administration) and (U.S.
Department of Agriculture), which monitor and regulate to ensure food safety across the food chain,” the FMI report said.
 
The survey indicated that 49% of consumers believe food safety problems most likely are to occur at food processing or manufacturing plants, compared with 5% who believe food safety problems are most likely to occur on farms and 4% who believe food safety problems are likely to occur at grocery stores. Other percentages were warehouses (9%), transport (5%), restaurant (10%) and home (7%).
 
For one in six shoppers, recalls prompt them to stop purchasing the product altogether, according to the survey. Food conditions shoppers believe pose at least some health risk:
Contamination by bacteria or germs, 74%;
Residues, such as pesticides and herbicides, 68%;
Product tampering, 62%;
Terrorists tampering with the food supply, 57%;
Antibiotics and hormones used on poultry or livestock, 56%;
Food from China, 51%;
Foods produced by biotechnology or GMOs, 45%;
Irradiated foods, 44%;
Food handling in supermarkets, 41%;
Eating food past the ‘USE BY’ date, 39%;
Eating food past the ‘BEST BY’ date, 34%; and 
Eating food past the ‘SELL BY’ date, 32%.
Source: Food Marketing Institute’s U.S. Grocery Shopping Trends 2017