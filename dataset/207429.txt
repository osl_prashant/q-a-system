India's Supreme Court suspended on Tuesday a government ban on the trade of cattle for slaughter, a boost for the multi-billion dollar beef and leather industries mostly run by members of the Muslim minority.Prime Minister Narendra Modi's Hindu nationalist government in May decreed that markets could only trade cattle for agricultural purposes, such as plowing and dairy production, on the grounds of stopping cruelty to animals.
The slaughter of cows, considered holy in Hinduism, was already banned in most parts of India, but Hindu hardliners and cow vigilante groups have been increasingly asserting themselves since Modi's government came to power in 2014.
Muslims, who make up 14 percent of India's 1.3 billion people, said the May government decree against the beef and leather industry employing millions of workers, was aimed at marginalizing them.
The Supreme Court, in issuing its decision, stressed the hardship that the ban on the trade of cattle for slaughter had imposed.
"The livelihood of people should not be affected by this," Supreme Court Chief Justice Jagdish Singh Khehar told a court packed with lawyers and representatives of the beef industry.
India's meat and leather industries are worth more than $16 billion in annual sales.
After the decision, the government told the court it would modify and reissue its May order, Additional Solicitor General P.S. Narasimha said.
The government could exclude buffalo from the ban - buffalo are not considered sacred - and buffalo meat constitutes the bulk of India's "beef" exports.
The crackdown on the beef industry has become highly emotive with a wave of attacks on Muslims suspected of either storing meat or transporting cattle for slaughter. An estimated 28 people have been killed in cow-related violence since 2010.
Late last month, after months of silence on the violence, Modi condemned lynchings.
Media has reported at least two cases of attacks on Muslims since Modi spoke out.
‘Huge Relief’
Abdul Faheem Qureshi, the head of the Muslim All India Jamiatul Quresh Action Committee that supports meat sellers, welcomed the court decision.
"We have to now restore the confidence of cattle traders that they can resume their business. It' a victory for us," said Faheem Qureshi, who had lodged a petition with the Supreme Court against the government ban.
India exported 1.33 million tons of buffalo meat in the 2016/17 fiscal year to March 31, worth about $3.9 billion. The previous year, it exported 1.31 million tons.
The Supreme Court order was also a boost for the leather industry which supplies brands such as Inditex-owned Zara and Clarks.
"The order has brought huge relief," said Puran Dawar, chairman of Agra-based shoe exporter Dawar Footwear Industries.
Apart from Muslims, the leather industry also employs lower-caste Hindus, mostly in menial jobs in tanneries.
But a cow protection group said the government should have been more forceful in making its case in court.
"The government should have stood its ground by fighting for the implementation of the ban," said Pawan Pandit, chairman of the India Cow Protection Group.
"Modi must respect the sentiments of millions of Hindus, who have supported his government," Pandit said.