Rolling along: White House Easter eggs come from Maine
Rolling along: White House Easter eggs come from Maine

The Associated Press

NEW VINEYARD, Maine




NEW VINEYARD, Maine (AP) — A Maine company is helping to get things rolling at the White House Easter Egg Roll.
Maine Wood Concepts made thousands of colorful, wooden eggs for the annual event.
The company's mill in New Vineyard makes hundreds of products from wood, but its president, Doug Fletcher, said the Easter eggs are an especially "cool item." The keepsakes will be given to children at the Easter egg roll Monday. They're also sold by the White House Historical Association.
All told, the company made 110,000 eggs.
They are made of birch and painted in shades of pink, yellow, green and blue selected by first lady Melania Trump. Golden eggs bear the presidential seal.