For bananas, uneasy lies the head that wears a crown. The long-time leader in produce department sales and U.S. consumption has had to deal with some hits lately, like Shakespeare’s King Henry IV, from whence that quote came.
The greatest indignity: Millennials don’t think bananas are cool.
Produce Retailer Editor Pamela Riemenschneider touched on this in an October story and an education session at The Packer’s Midwest Expo in August.
Among the reasons for bananas’ lack of popularity with this largest generation: they’re not trendy like kale, mangoes or premium apples; millennials aren’t into buying practical food; and bananas are often wasted.
Yep, millennials don’t like food waste, and, like most people, they don’t eat them all before they turn brown on the counter.
Our 2017 Fresh Trends data showed indeed, the 18-39 age group was more than 10 percentage points less likely to buy bananas than any other age group. In the 2018 edition, which comes out in about a month, we will reveal that this gap has increased to 17 percentage points to the next closest age group to the 18-39 group.
On top of this, consumer news recently revived the “killer fungus will wipe out the cavendish variety” stories. Should we believe them now after years of overplaying this hoax?
Sounds like bananas will soon be dead and millennials won’t even care.
Only that’s not reality.
Two sets of numbers show bananas are still tops and retailers should remain bullish on banana sales.
According to Amazon, bananas were the most ordered item through AmazonFresh last year. Meanwhile, rebate app company Ibotta shared some purchase data with us that showed bananas dominate produce purchases.
Consumers using Ibotta bought about 10 million produce items a month in 2017, so 30 million a quarter. In Q1, bananas accounted for 3.25 million purchases, with carrots a distant second at 680,000. In Q2, same thing with bananas at 3.5 million over cucumbers at No. 2 with 696,000. And bananas again in Q3 with 3.07 million over No. 2 cukes at 642,000.
What about millennials? Ibotta broke out 25- to 34-year-old app users, and for the year, the most purchased produce item was bananas at 4.1 million followed by red onions, 905,000; cucumbers, 814,000; green peppers, 773,000; and tomatoes, 713,000.
People looking for reasons to write off the banana miss even more factors for its continued success.
Bananas remain inexpensive, consistent in quality and ripening, omnipresent and easy to feed to kids. They’re also about the easiest produce item to fill online orders, which will only increase in 2018.
The crown will stay on a while longer. 
Greg Johnson is The Packer’s editor. E-mail him at gjohnson@farmjournal.com.