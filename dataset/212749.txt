Capesan merges all operations
Capesan North America, Gloucester, N.J., consolidated all of its operations, including value-added, under one roof at the South Jersey Port District’s Broadway Terminal Pier 5 in Camden, N.J., said Chuck Yow, director of U.S. sales and business development.
The facility is operated by Southport Distribution, a subsidiary company of HOLT Logistics, which operates Gloucester Refrigerated Warehouse, Yow said.
Value-added services will be provided on-site by Fresh PAC, a private contractor with whom Capespan North America has entered into a long-term, strategic relationship with for value-adding services, Yow said.
The arrangement will facilitate accuracy and ease of truck loading and dispatch of orders to Capespan North America’s customers.
 
Greenyard builds Northeast facility
Vero Beach, Fla.-based Seald Sweet International’s sister company, Greenyard Logistics, Swedesboro, N.J., is nearing completion of a 152,200-square-foot packing and cold storage facility in the Northeast which will quadruple its current capacity, said Kimberly Flores, marketing director.
This is the first expansion of the fresh division of Greenyard in the U.S. market. The facility will be opening early this summer.
Summer citrus will be the first product to run on the new lines, Flores said.
The expansion adds to the company’s services in areas such as value-added packing, consolidation, controlled atmosphere storage and more, she said.
Seald Sweet will also relocate its import production operations and Northeast sales offices to the new facility.
 
Seven Seas hires director
Seven Seas Global Produce Network, Iselin, N.J., hired Miles Fraser-Jones in January as director of global business development for Seven Seas’ New Jersey office. He started January 9.
Fraser brings 18 years of food industry experience to Seven Seas, most recently as senior vice president of AMC North America.