Soybean markets have been rallying off South American weather scares, and corn has been flirting with $4. Several farmers are taking this opportunity to sell their grain after a long stretch of low volatility, and the recent market moves have given a spark of life into commodities.

Paul Georgy, chairman of the board at Allendale, Inc., says is analyzing the current market to see if it fits into one of three categories: supply, demand or other.

“At this point, I would say other—it’s [a] money-flow driven rally that have right now,” he told U.S. Farm Report host Tyne Morgan. “It’s an opportunity to market grain.”

Small or low volatility gives farmers the chance to prepare for a rally or break, he said.

“He can’t sit with deer in the headlight attitude—not selling anything or selling it all because he’s panicked,” said Georgy. “He has to take action and do it wisely.”

Shawn Hackett, president of Hackett Financial Advisors, says if farmers are going go aggressively sell, he advises them to not sell too much.

“If you do, make sure you have some protection to the upside,” he said.

In his area, he said producers have been selling more grain, but Hackett thinks producers will get caught making a mistake by selling too early.

“We think they should be more light on the selling because we think we’re entering a different phase,” said Hackett.

Hear why Hackett is excited about corn in the long run and why Georgy thinks near-term wheat potential could help corn producers on Markets Now above.