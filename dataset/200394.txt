Cold stress starts when temperatures drop below 60 degrees Fahrenheit in dairy calves who are less than 21 days-of-age and when below 42 degrees (Litherland, 2013) in calves greater than 42 days of age.Winter can present challenges for dairy producers and heifer growers as they try to keep calves alive and growing adequately in frigid temperatures.
Cold Stress Management Tips
Some tips to care for un-weaned calves are as follows:
Get them off to a good startin the first 48 hours of life - following proper protocols for newborn calves, especially drying them off as quickly as possible after birth and making sure they are consuming 4 quarts of high quality colostrum within the first 12 hours of life, as well as giving proper vaccinations, and dipping their navels.
Consider using a calf warmerto quickly dry newborn calves and help increase body temperature.
Deep & dry bedding.Provide a proper nest; when they are laying down a guide is that one should not be able to see the feet & legs of the calf.
Calf blankets.Even though they add extra expense, they are reusable and provide an extra layer of protection for calves. Make sure to adequately clean and dry blankets between uses.
Proper ventilationis important but you must also prevent direct drafts from hitting young calves. Fresh air helps reduce the presence airborne pathogens and ammonia that is produced by urine & manure.
Additional feedings per day.Increasing feedings per day which will provide 1.5 pounds to 2.25 pounds of milk replacer powder per day with 20% fat are necessary to provide adequate energy intake daily.
Fat supplement.When it is extremely cold feed 0.25 pounds per day of additional 60% fat supplement in the milk during the first 14 days of life. Many commercial products are available. Taper off feeding the supplement slowly as they start to consume starter
Be consistent in feeding times, making sure milk is warm but yet does not scald the calves' mouth.
Warm water.Other options of increasing caloric intake. Provide warm water above 102 degrees F about 30 minutes after feeding. Water is essential to start and keep the digestive track of the calf working properly, along with promoting dry matter intake. Again, remember, be careful not to get it too hot though and scald the calves' mouth.
Provide a high-quality, fresh starter daily(free of mold and fines), to encourage daily intake.
Lastly, producers/employees should take care of themselves so they can be vigilant and observant as they care for the animals.

Reference: Litherland, N. (2013). Feeding and Management tips to combat cold stress in nursery calves. University of Minnesota.