BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Wed. Apr 18, 2018          USDA Market NewsDaily California EggsPrices are steady. Trade sentiment is steady. Offerings are moderate.Demand is mostly moderate into all channnels. Supplies are light to usuallymoderate. Market activity is moderate.Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                212EXTRA LARGE          202LARGE                196MEDIUM               147Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 18-Apr-18 12:14 PM E MP