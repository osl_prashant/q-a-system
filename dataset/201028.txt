USDA Weekly Export Sales Report
			Week Ended June 8, 2017





Corn



Actual Sales (in MT)

Combined: 614,200
			2016-17: 600,700
			2017-18: 13,500



Trade Expectations (MT)
600,000-1,00,000


Weekly Sales Details
Net sales of 600,700 MT for 2016-17 were up 72% from the previous week and 25% from the prior 4-week average. Increases were reported for Mexico (200,400 MT, including decreases of 1,600 MT), Colombia (109,700 MT, including decreases of 1,100 MT), China (60,800 MT, switched from unknown destinations), Peru (57,800 MT, including 45,500 MT switched from unknown destinations), and Nigeria (39,100 MT, including 40,000 MT switched from unknown destinations and decreases of 900 MT). Reductions were reported for unknown destinations (103,000 MT) and Taiwan (61,000 MT). For 2017-18, net sales of 13,500 MT were reported for Panama (16,500 MT), the French West Indies (8,200 MT), and Nicaragua (1,500 MT), were partially offset by reductions for unknown destinations (12,700 MT). 


Weekly Export Details
Exports of 992,300 MT were down 18% from the previous week and 23% from the prior 4-week average. The primary destinations were Mexico (276,900 MT), Japan (195,800 MT), Taiwan (74,500 MT), South Korea (64,500 MT), and China (62,100 MT).


Comments and Performance Indicators
Sales were at the lower end of expectations. Export commitments for 2016-17 are running 20% ahead of year-ago compared to 21% ahead the week prior. USDA projects exports in 2016-17 at 2.225 billion bu., up 17.2% from the previous marketing year.




Wheat



Actual Sales (in MT)

2017-18: 373,400



Trade Expectations (MT)
350,000-550,000 


Weekly Sales Details
Net sales of 373,400 metric tons were reported for delivery in marketing year 2017/2018. The primary destinations were Mexico (121,400 MT), Nigeria (56,600 MT, including 7,000 MT switched from Algeria), Taiwan (53,900 MT, including 52,500 MT switched from unknown destinations and decreases of 300 MT), Colombia (47,500 MT, including 42,300 MT switched from unknown destinations and decreases of 200 MT), Peru (27,200 MT), Costa Rica (16,500 MT, including 9,700 MT switched from unknown destinations), and unknown destinations (11,600 MT). Reductions were reported for Algeria (6,000 MT) and Jordan (1,200 MT).


Weekly Export Details
Exports of 624,400 MT were reported to Yemen (122,300 MT), Nigeria (113,600 MT), South Korea (60,500 MT), Taiwan (54,000 MT), Jordan (48,800 MT), and Guatemala (39,300 MT).


Comments and Performance Indicators
Sales were within expectations. Export commitments for 2017-18 are running 7% ahead of year-ago versus 14% ahead the week prior.USDA projects exports in 2017-18 at 1.0 billion bu., down 3.4% from the previous marketing year.




Soybeans



Actual Sales (in MT)

Combined: 654,200
			2016-17: 340,200
			2017-18: 314,000



Trade Expectations (MT)
350,000-750,000 


Weekly Sales Details
Net sales of 340,200 MT for 2016-17 were up noticeably from the previous week, but down 15% from the prior 4-week average. Increases were reported for the Netherlands (85,000 MT, including 80,000 MT switched from unknown destinations), China (61,500 MT), Bangladesh (61,000 MT, switched from unknown destinations), Indonesia (59,900 MT, including 54,500 MT switched from unknown destinations and decreases of 5,300 MT), and Japan (52,600 MT, including 46,000 MT switched from unknown destinations). Reductions were reported for unknown destinations (45,200 MT). For 2017-18, net sales of 314,000 MT were reported for unknown destinations (186,000 MT), China (60,000 MT), and Pakistan (57,000 MT). 


Weekly Export Details
Exports of 489,000 MT were up 53% from the previous week and 45% from the prior 4-week average. The destinations were primarily China (145,000 MT), the Netherlands (85,000 MT), Indonesia (65,900 MT), Japan (60,300 MT), and Bangladesh (54,300 MT). 


Comments and Performance Indicators
Sales met expectations. Export commitments for 2016-17 are running 20% ahead of year-ago, compared to 21% ahead the previous week. USDA projects exports in 2016-17 at 2.050 billion bu., up 5.9% from year-ago.




Soymeal



Actual Sales (in MT)
Combined: 277,200
			2016-17: 166,800
			2017-18: 110,400


Trade Expectations (MT)

50,000-250,000 



Weekly Sales Details
Net sales of 166,800 MT for 2016-17 were up 97% from the previous week and 49% from the prior 4-week average. Increases were reported for Mexico (60,600 MT), Ecuador (32,400 MT, including decreases of 15,000 MT), Venezuela (20,700 MT, including 12,000 MT switched from Panama), Colombia (12,700 MT, including 10,000 MT switched from unknown destinations), and the Philippines (10,200 MT). Reductions were reported for Panama (11,000 MT) and unknown destinations (10,400 MT). For 2017-18, net sales of 110,400 MT were reported primarily for Ecuador (60,000 MT), Mexico (29,800 MT), and Peru (9,000 MT).


Weekly Export Details
Exports of 215,700 MT were up 74% from the previous week and 28% from the prior 4-week average. The destinations were primarily the Philippines (90,700 MT), Ecuador (32,400 MT), Venezuela (14,700 MT), Canada (13,300 MT), Mexico (11,600 MT), and Colombia (9,500 MT). 


Comments and Performance Indicators
Sales were stronger than expected. Export commitments for 2016-17 are running 3% ahead of year-ago compared with 2% ahead of year-ago the week prior. USDA projects exports in 2016-17 to be up 0.3% from the previous marketing year.




Soyoil



Actual Sales (in MT)

2016-17: 30,300



Trade Expectations (MT)
5,000-35,000


Weekly Sales Details
Net sales of 30,300 MT for 2016-17 were up 72% from the previous week and 86% from the prior 4-week average. Increases were reported for unknown destinations (15,000 MT), South Korea (7,000 MT), the Dominican Republic (5,800 MT), and Mexico (2,300 MT).


Weekly Export Details
Exports of 16,800 MT were down 18% from the previous week, but up 3% from the prior 4-week average. The destinations were primarily the Dominican Republic (9,500 MT), Nicaragua (3,100 MT), and Mexico (1,900 MT). 


Comments and Performance Indicators
Sales were within expectations. Export commitments for the 2016-17 marketing year are running steady with year-ago, compared to 2% behind year-ago last week. USDA projects exports in 2016-17 to be up 2.7% from the previous year.




Cotton



Actual Sales (in RB)
Combined: 267,000
			2016-17: 69,400
			2017-18: 197,600


Weekly Sales Details
Net upland sales of 69,400 RB for 2016-17 were down 16% from the previous week and from the prior 4-week average. Increases were reported for India (21,000 RB), Vietnam (15,500 RB, including 1,300 RB switched from Taiwan and 700 RB switched from Japan), China (12,000 RB, including 1,100 RB switched from Indonesia and 600 RB switched from Macau), and Turkey (11,100 RB). Reductions were reported for Macau (800 RB) and Japan (100 RB). For 2017-18, net sales of 197,600 RB  reported primarily for Bangladesh (74,900 RB), Vietnam (64,000 RB), and Indonesia (11,600 RB). 


Weekly Export Details
Exports of 233,300 RB were down 26% from the previous week and 34% from the prior 4-week average. The primary destinations were Turkey (39,500 RB), Vietnam (35,000 RB), India (33,200 RB), China (28,500 RB), and Indonesia (21,500 RB). 


Comments and Performance Indicators
Export commitments for 2016-17 are 65% ahead of year-ago, compared to 66% ahead the previous week. USDA projects exports to be up 58.5% from the previous year at 14.500 million bales.




Beef



Actual Sales (in MT)

2017: 8,500



Weekly Sales Details
Net sales of 8,500 MT reported for 2017 were down 38% from the previous week and 16% from the prior 4-week average. Increases were reported for Mexico (2,100 MT, including decreases of 100 MT), Hong Kong (1,300 MT, including decreases of 100 MT), Japan (1,300 MT), Taiwan (1,100 MT), and Canada (1,100 MT, including decreases of 100 MT). 


Weekly Export Details
Exports of 14,500 MT were up 17% from the previous week and 6% from the prior 4-week average. The primary destinations were Japan (5,000 MT), South Korea (3,200 MT), Mexico (1,900 MT), Hong Kong (1,400 MT), and Taiwan (1,100 MT). 


Comments and Performance Indicators
Weekly export sales compare to 14,000 MT the week prior. USDA projects exports in 2017 to be up 10.0% from last year's total.




Pork



Actual Sales (in MT)
2017: 12,300


Weekly Sales Details
Net sales of 12,300 MT reported for 2017--a marketing-year low--were down 46% from the previous week and 29% from the prior 4-week average. Increases were reported for Mexico (5,700 MT), Japan (3,400 MT) Colombia (2,000 MT), Hong Kong (1,900 MT), and Chile (1,000 MT). Reductions were reported for China (3,900 MT). 


Weekly Export Details
Exports of 21,000 MT were up 6% from the previous week, but down 5% from the prior 4-week average. The destinations were primarily Mexico (7,600 MT), Japan (3,900 MT), South Korea (2,100 MT), China (1,500 MT), and Canada (1,400 MT). 


Comments and Performance Indicators

Export sales compare to a total of 22,600 MT the prior week. USDA projects exports in 2017 to be 9.8% above last year's total.