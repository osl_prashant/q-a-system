Following is a snapshot of key data from this morning's USDA Crop Production and Supply & Demand Reports:




 


2016-17


2017-18






Commodity


USDA


Avg.


Range


USDA
			June


USDA


Avg.


Range


USDA
			June




in billion bushels




Corn


2.370


2.321


2.235-2.375


2.295


2.325


2.181


1.921-2.398


2.110




Soybeans


0.410


0.430


0.400-0.465


0.450


0.460


0.473


0.374-0.513


0.495




Wheat


*1.184


NA


NA


1.161


0.938


0.876


0.757-0.957


0.924




Cotton (mil. bales)


3.20


NA


NA


3.20


5.30


5.10


4.60-5.50


5.50




*2016-17 wheat carryover was set by the June 30 Grain Stocks Report.




2017 Wheat Production


USDA


Avg.


Range


USDA
			June




in billion bushels




All wheat


1.760


1.748


1.634-1.834


1.824




All winter


1.279


1.261


1.237-1.280


1.250




HRW


0.758


0.745


0.693-0.775


0.743




SRW


0.306


0.303


0.296-0.332


0.298




White winter


0.216


0.210


0.199-0.218


0.209




Other spring


0.4229


0.416


0.305-0.470


NA




Durum


0.0575


0.078


0.065-0.090


NA




 




Global Carryover


2017-18
July


2017-18
June


2016-17
July


2016-17
June




Million Metric Tons
 





Corn

200.81


194.33


227.51


224.59



Wheat 

260.60


261.19


258.05


256.43



Soybeans 

93.53


92.22


94.78


93.21



Meal 

13.27


12.51


13.82


13.27



Soyoil 

3.71


3.55


3.75


3.63



Cotton (mil. bales)

88.73


87.71


90.27


89.34




 




Global crop in MMT


2017-18
July


2017-18
June


2016-17
July


2016-17
May




Wheat



U.S.

47.89


49.64


62.86


62.86



Australia

23.5


25.0


35.11


35.00



Canada

28.35


28.35


31.70


31.70



EU

150.0


150.75


145.47


145.47



China

130.0


131.0


128.85


128.85



FSU-12

125.17


123.01


130.54


130.24




Corn



U.S.

362.09


357.27


384.78


384.78



Argentina

40.0


40.0


41.0


40.0



Brazil

95.0


95.0


97.0


97.0



EU

61.6


62.0


60.71


60.71



Mexico

25.0


25.0


27.4


27.0



China

215.0


215.0


219.55


219.55



FSU-12

48.80


48.85


47.36


47.43




Soybeans



U.S.

115.94


115.80


117.21


117.21



Argentina

57.0


57.0


57.8


57.8



Brazil

107.0


107.0


114.0


114.0



China

14.0


13.8


12.9


12.9




Cotton (mil. bales)



U.S.

19.0


19.20


17.17


17.17



Brazil

7.0


7.0


6.80


6.80



India

29.0


28.0


27.00


26.50



China

24.0


24.0


22.75


22.75



Pakistan

9.15


9.3


7.7


7.7