Corn




The national average corn basis firmed 2 3/4 cents from last week to 8 3/4 cents below July futures. The national average cash corn price improved 1/2 cent from last week to $3.62 3/4.
Basis is weaker than the three-year average, which is steady with futures for this week.












Soybeans





The national average soybean basis firmed 3 cents from last week to stand 17 3/4 cents below July futures. The national average cash price improved 8 1/2 cents from last week at $9.58.
Basis is softer than the three-year average, which is 3 3/4 cents above futures for this week.












Wheat





The national average soft red winter (SRW) wheat basis firmed 1/4 cents from last week to 9 1/2 cents below July futures. But the average cash price dropped 4 1/2 cents from last week to $4.17 1/2. The national average hard red winter (HRW) wheat basis firmed 3 1/4 cents from last week to 77 cents below July futures. The average cash price dropped 9 3/4 cents from last week to $3.49 1/2.








SRW basis is firmer than the three-year average of 22 cents under futures. HRW basis is much weaker than the three-year average, which is 41 cents under futures for this week.











Cattle




 




Cash cattle trade averaged $137.28 last week, down $7.32 from the previous week. Cash trade began today at around $3 below week-ago. Last year at this time, the cash price was $132.56.
Choice boxed beef prices firmed $3.59 from last week at $248.17.  Last year at this time, Choice boxed beef prices were $227.76.











Hogs





 




The average lean hog carcass bid firmed $6.50 over the past week to $74.14. Last year's lean carcass price on this date was $77.79.
The pork cutout value improved $5.67 from last week to $85.67. Last year at this time, the average cutout price stood at $82.87.