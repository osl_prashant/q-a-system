USDA Weekly Export Sales Report
			Week Ended Oct. 5, 2017





Corn



Actual Sales (in MT)

Combined: 1,608,400
			2017-18: 1,593,200
			2018-19: 15,200 



Trade Expectations (MT)
2017-18: 800,000-1,100,000


Weekly Sales Details
Net sales of 1,593,200 MT for 2017-18 were reported for Mexico (1,014,000 MT, including 30,000 MT switched from unknown destinations and decreases of 35,100 MT), Japan (166,200 MT, including 24,200 MT switched from unknown destinations), Colombia (87,100 MT, including 80,000 MT switched from unknown destinations), Peru (76,500 MT), South Korea (69,600 MT), and unknown destinations (47,500 MT).  Reductions were reported for Panama (10,500 MT) and Barbados (4,500 MT).  For 2018-19, net sales of 15,200 MT were reported for Mexico.  


Weekly Export Details
Exports of 669,600 MT were primarily to Mexico (348,900 MT), Colombia (84,700 MT), South Korea (69,700 MT), Panama (44,100 MT), and Guatemala (41,300 MT).


Comments and Performance Indicators
Sales were much stronger than expected. Export commitments at the start of the 2017-18 marketing year are running 36% behind year-ago compared with 41% behind last week. USDA projects exports in 2017-18 at 1.850 billion bu., down 19.3% from the previous marketing year.




Wheat



Actual Sales (in MT)

2017-18: 175,000



Trade Expectations (MT)
2017-18: 300,000-500,000


Weekly Sales Details
Net sales of 175,000 metric tons for delivery in marketing year 2017-18 were down 65% from the previous week and 55% from the prior 4-week average.   Increases were for Japan (88,000 MT, including 50,300 MT switched from unknown destinations), the Philippines (52,000 MT), Honduras (31,200 MT), Indonesia (30,900 MT), and Nigeria (25,000 MT).  Reductions were reported for unknown destinations (99,300 MT) and Colombia (3,300 MT).  


Weekly Export Details
Exports of 325,500 MT were down 55% from the previous week and 35% from the prior 4-week average.  The primary destinations were Japan (94,800 MT), the Philippines (47,000 MT), Indonesia (36,900 MT), Nigeria (36,000 MT), and Mexico (32,200 MT).


Comments and Performance Indicators
Sales were well below expectations. Export commitments for 2017-18 are running 5% behind year-ago versus 3% behind the week prior. USDA projects exports in 2017-18 at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual Sales (in MT)

2017-18: 1,747,300



Trade Expectations (MT)
2017-18: 900,000-1,200,000 


Weekly Sales Details
Net sales of 1,747,300 MT for 2017-18 were reported for China (1,020,400 MT, including 331,000 MT switched from unknown destinations and decreases of 61,900 MT), unknown destinations (351,500 MT), the Netherlands (104,000 MT, including 94,000 MT switched from unknown destinations), Indonesia (71,000 MT, including 64,200 MT switched from unknown destinations), and Vietnam (68,800 MT).  Reductions were reported for Thailand (41,200 MT).


Weekly Export Details
Exports of 1,190,200 MT were primarily to China (700,700 MT), Mexico (116,600 MT), the Netherlands (104,000 MT), Indonesia (83,300 MT), and Pakistan (69,800 MT).   


Comments and Performance Indicators
Sales were much stronger than expected. Export commitments are running 16% behind year-ago compared with 18% behind last week. USDA projects exports in 2017-18 at 2.250 billion bu., up 3.5% from year-ago.




Soymeal



Actual Sales (in MT)
Combined: 00
			2016-17: 00
			2017-18: 106,000 


Trade Expectations (MT)

2016-17: -200,000-0
			2017-18: 100,000-300,000



Weekly Sales Details

Net sales of 106,000 MT for the 2017-18 marketing year (which began October 1) were primarily for Thailand (53,000 MT), the Dominican Republic (25,000 MT), Venezuela (11,000 MT), and Mexico (9,900 MT).  Reductions were reported for unknown destinations (10,500 MT) and Panama (6,500 MT).  Net sales reductions of 19,500 MT for September 29-30 resulted as increases for Mexico (7,600 MT), Japan (3,200 MT, including 2,900 MT switched from unknown destinations), and Canada (1,400 MT), were more than offset by decreases for Ecuador (30,000 MT), unknown destinations (2,900 MT), and Sri Lanka (400 MT).  
A total of 256,500 MT in sales were outstanding on September 30 (the end of the 2016-17 marketing year) and carried over to the 2017-18 marketing year.  



Weekly Export Details

Exports for October 1-5 of 34,300 MT were reported for Mexico (10,700 MT), Canada (9,600 MT), and Guatemala (9,400 MT).  
Exports of 133,300 MT were reported for September 30.  The primary destinations were Mexico (41,800 MT), Morocco (30,900 MT), the Philippines (21,800 MT), and Panama (16,600 MT).  
Accumulated exports for the 2017-18 marketing year were 10,164,800 MT, down 1% from the 10,243,400 MT reported in 2016-17.    



Comments and Performance Indicators
Old- and new-crop sales met expectations. Export commitments for 2017-18 are running 13% ahead of year-ago. USDA projects exports in 2017-18 to be up 3.5% from the previous marketing year.




Soyoil



Actual Sales (in MT)

Combined: 12,600
			2016-17: 1,600
			2017-18: 11,000 



Trade Expectations (MT)
2016-17: -30,000-0
			2017-18: 5,000-22,000


Weekly Sales Details

Net sales of 11,000 MT for the 2017-18 marketing year (which began October 1) were primarily for the Dominican Republic (9,500 MT), Mexico (600 MT), and Canada (400 MT).  Exports for October 1-5 of 8,500 MT were reported for the Dominican Republic (7,500 MT), Mexico (900 MT), and Canada (100 MT).  Net sales of 1,600 MT for September 29-30 were primarily for Mexico. 
A total of 22,900 MT in sales were outstanding on September 30 (the end of the 2016-17 marketing year) and carried over to the 2017-18 marketing year.



Weekly Export Details

Exports for October 1-5 of 8,500 MT were reported for the Dominican Republic (7,500 MT), Mexico (900 MT), and Canada (100 MT).  Net sales of 1,600 MT for September 29-30 were primarily for Mexico.  
Accumulated exports for the 2017-18 marketing year were 1,120,400 MT, up 6% from the 1,052,400 MT reported in 2016-17. 









Comments and Performance Indicators
Old-crop sales topped expectations, while new-crop sales matched expectations. Export commitments at the start of the 2017-18 marketing year are running 69% behind year-ago. USDA projects exports in 2017-18 to be down 17.6% from the previous year.




Cotton



Actual Sales (in RB)
Combined: 00
			2017-18: 154,400
			2018-19: 9,300 


Weekly Sales Details
Net sales of 154,400 RB for 2017-18 were down 4% from the previous week and from the prior 4-week average.   Increases were reported for Indonesia (32,600 RB, including 1,900 RB switched from Japan), Vietnam (25,600 RB, including 400 RB switched from Japan), China (22,600 RB), Turkey (18,600 RB), Bangladesh (13,600 RB), and Taiwan (9,500 RB).  Reductions were reported for Japan (2,000 RB) and Honduras (100 RB).  For 2018-19, net sales of 9,300 RB were reported for China (8,800 RB) and Japan (500 RB).  


Weekly Export Details
Exports of 118,000 RB were up 3% from the previous week, but down 11% from the prior 4-week average. Exports were reported primarily to Vietnam (37,800 RB), China (16,800 RB), Indonesia (15,200 RB), Mexico (10,700 RB), and South Korea (8,800 RB).


Comments and Performance Indicators
Export commitments for 2017-18 are running 40% ahead of year-ago compared to 43% ahead last week. USDA projects exports in 2017-18 will decline 2.8% from the previous year to 14.5 million bales.




Beef



Actual Sales (in MT)

Combined: 14,500
			2017: 12,900 
2018: 1,600



Weekly Sales Details
Net sales of 12,900 MT reported for 2017 were down 38% from the previous week and 19% from the prior 4-week average.  Increases were reported for Japan (5,200 MT), South Korea (2,000 MT), Canada (1,700 MT), Hong Kong (1,300 MT), and Mexico (1,200 MT).  For 2018, net sales of 1,600 MT were reported for Hong Kong (1,100 MT), Japan (300 MT), and Taiwan (200 MT).  


Weekly Export Details
Exports of 16,000 MT were unchanged from the previous week, but up 5% from the prior 4-week average.  The primary destinations were Japan (4,900 MT), South Korea (4,300 MT), Hong Kong (1,700 MT), Mexico (1,600 MT), and Canada (1,300 MT). 


Comments and Performance Indicators
Last week, USDA reported export sales totaling 22,700 MT for 2017 and 2018, combined. USDA projects exports in 2017 to be up 10.9% from last year's total. 




Pork



Actual Sales (in MT)

2017: 15,900 



Weekly Sales Details
Net sales of 15,900 MT reported for 2017 were down 25% from the previous week and 32% from the prior 4-week average.  Increases were reported for Japan (4,900 MT), Mexico (3,500 MT), Canada (2,700 MT), China (1,000 MT), and South Korea (1,000 MT).   


Weekly Export Details
Exports of 21,300 MT were up 6% from the previous week and from the prior 4-week average.  The destinations were primarily Mexico (8,000 MT), Japan (3,900 MT), South Korea (3,200 MT), Canada (2,000 MT), and Hong Kong (1,000 MT). 


Comments and Performance Indicators

Last week, USDA reported pork export sales totaling 21,200 MT. USDA projects exports in 2017 to be 9.0% above last year's total.