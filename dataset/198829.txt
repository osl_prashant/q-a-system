What makes calves happy, and can the way we manage them make a difference in their level of happiness - and thus their stress level, health and productivity?Researchers from Aarhus University in Denmark and the University of British Columbia in Vancouver, BC recently collaborated to determine whether pair housing, enhanced feeding, or both impacted the social well-being and growth of preweaned calves.
The trial involved 48 male and female Holstein calves, using the following structure:
¬? Calves were housed either individually or in pairs in straw-bedded pens (3.0 m X 4.5 m).
¬? Half of the calves in each housing treatment were fed a "standard" milk allowance of 5 L per day from 3 to 42 days. The other half received an enhanced milk allowance of 9 L per day from 3 to 28 days, then were cut back to 5 L per day from days 29 to 42.
¬? All calves were weaned abruptly at 42 days of age.
¬? Play behavior was observed in all calves for 48 hours beginning on days 15, 29 and 43.
The results included:
(1) Play behavior on day 15 was greater in enhanced-fed calves compared to those on the standard ration. This behavior decreased when the enhanced-fed group was cut back on day 29.
(2) Calves on the enhanced-milk diet consumed less starter grain, but the enhanced-fed calves in pair housing consumed more grain than those in individual pens.
(3) Among the calves fed enhanced milk, pair-housed calves had greater body weight than individually housed calves. There was no difference in weight gain for standard-fed calves in pair versus individual housing.
(4) For calves on the standard milk ration, there was no significant difference in weight gain between the two housing configurations.
The researchers concluded that greater duration of play behavior in enhanced-fed calves suggests an animal welfare benefit to this feeding method. Based on the weight-gain and social outcomes, they recommended using a higher level of nutrition in conjunction with pair (or group) housing.