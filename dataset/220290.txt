That special, once-a-year, mark-your-calendar event is right around the corner.
Valentine’s Day is a holiday marked by an annual ritual that takes place between 5 and 8 p.m. the evening before: a long line of men standing in line at the local supermarket or drugstore holding the last of the boxes of Whitman’s Samplers, a random bouquet of flowers and some sappy greeting card covered in cutesy red hearts and lots of glitter.
Guys — we can do better.
They say the way to a man’s heart is through his stomach? Well, the way to a woman’s heart is through the kitchen range, when a sumptuous dinner is all prepared, waiting for one’s better half to arrive home, freed from the entire burden of menu planning, shopping, cooking and, of course, cleaning up.
That, my fellow U.S. males, will produce far better results than any “romantic” gifts purchased just minutes before the evening begins.
Even better: Flowers and candy sent to the office during the day, so your beloved’s co-workers get to admire your thoughtfulness — then a scrumptious dinner upon her arrival home. If romance were a sport (and who says it isn’t?), that’s the equivalent of not just an interception, but a pick-six romp all the way to the end zone.
Touchdown dance afterwards is optional.
A Simple, Yet Savory Approach
Question is, what to prepare for that special dinner to be savored à deux?
We can rule out vegetarian options right off the bat. For proof, consider this “special Valentine’s meal” recommended by a prominent vegan chef: Cream of mushroom soup with shiitake chips; sweet potato and mushroom cannelloni; carrot and rye berry salad; spinach and hominy enchiladas; topped off with chilled avocado and lime juice “ice creme.”
If that’s what someone told me was being prepared for dinner, my question would be, “So what’s the main course?”
Here’s a way better idea, as described in The New York Times article titled, “Meat and Potatoes Made Magical.”
“Instead of going to a crowded restaurant, you can easily pull off the simple luxury of steak and potatoes at home,” the article began, “and use the money you save by splurging on an expensive, top-quality cut like ribeye, tenderloin or strip steak.”
You can tell right there that the article was written by a guy, although what the writer didn’t mention is although you’re being thrifty with the “let’s-stay-at-home” approach, that is not how you present the idea to your mate.
Ever.
Back to the meal.
The recommendation was for what was called “the most succulent steak, richly marbled and very tasty: ribeye” (or in French, côte de boeuf, if that’s a turn on for that someone special).
Now, some preparation specifics.
Purchase one large, two-inch thick steak at the butcher shop or service counter, rather than two separate pieces. The larger piece will make for a juicier, more flavorful steak.
Next, cover the meat with “generous dashes” of salt and pepper, some sliced garlic and chopped rosemary, allowing time for the beef to absorb the seasonings (the garlic and rosemary are removed prior to cooking).
Brown both sides of the steak in a “scorching hot cast-iron pan” on the stovetop. Then place the pan in a 400-degree oven to roast until the interior reaches 120 degrees F, which the article stated will take about 8 to 10 minutes.
Let the steak rest for 10 minutes or so before slicing into wide strips, across the grain.
That takes care of the steak. And for the ultimate complement: baby potatoes. As the article explained, just boil the potatoes in heavily salted water (about 10 minutes), then toss them with butter, garlic, parsley and lemon zest.
Simple as that, yet sure to accomplish the desired effect.
Oh, yes: Don’t forget to open a nice bottle of wine.
In fact, for the prefect evening, why not start with the wine?
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator. For those of you who only eat pork, watch for JoAnn Alumbaugh’s commentary this week for a pork alternative that’s just as delicious as this one.