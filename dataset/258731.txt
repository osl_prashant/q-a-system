BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Monday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMar         2573    2579    2565    2565  Up    87May                                 2569  Up    77May         2472    2569    2453    2546  Up    81Jul         2492    2592    2479    2569  Up    77Sep         2512    2600    2492    2580  Up    73Dec         2500    2585    2487    2571  Up    71Mar         2482    2565    2471    2550  Up    68May         2497    2572    2477    2556  Up    69Jul         2488    2579    2488    2568  Up    69Sep         2499    2580    2499    2579  Up    68Dec         2569    2597    2567    2595  Up    68