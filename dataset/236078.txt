White House: Talks to continue on renewable fuel standard
White House: Talks to continue on renewable fuel standard

By KEN THOMASAssociated Press
The Associated Press

WASHINGTON




WASHINGTON (AP) — The White House says talks will continue on a dispute over the future of the renewable fuel standard, which has pitted lawmakers from corn-producing states against those representing refineries.
White House spokeswoman Sarah Huckabee Sanders says President Donald Trump had a productive meeting Tuesday with four Republican senators: Ted Cruz of Texas, Pat Toomey of Pennsylvania and Charles Grassley and Joni Ernst of Iowa.
The White House says Trump has made clear his commitment to the RFS and support for farmers and energy workers.
The standard requires biofuels from corn and soybeans to be blended into gasoline and diesel. Midwest states have sought to maintain the standards while oil companies have pushed to ease the mandates.
Grassley says in a conference call with reporters that no deal was reached Tuesday.