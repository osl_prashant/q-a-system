Crop calls
Corn: Fractionally to 1 cent lower
Soybeans: 3 to 5 cents lower
Wheat: 1 to 2 cents higher
 
Corn and soybean futures saw two-side trade overnight, but softened late in the session despite a weaker tone in the dollar index and rains in the forecast. Meanwhile, wheat futures saw light short-covering following yesterday's decline. Yesterday's crop condition ratings showed a one-point gain, as expected, in the percent of the winter wheat crop rated "good" to "excellent," which limited overnight buying.
 
Livestock calls
Cattle: Higher
Hogs: Lower
Cattle futures are expected to benefit from followthrough technical buying, especially following improvement in beef prices yesterday. April live cattle ended yesterday at a $1.50 discount to the average of last week's cash cattle trade, putting them more in line. Meanwhile, lean hog futures are called weaker on followthrough selling as well as steady to weaker cash hog bids. Pork cutout values softened 2 cents yesterday on light movement of 173.97 loads, raising concern about packer demand.