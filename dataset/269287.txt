A roundup of recent Michigan newspaper editorials
A roundup of recent Michigan newspaper editorials

By the Associated Press
The Associated Press



The Detroit News.  April 11, 2018
Detroit students lag country, again
Detroit schools are once again dead last for how their students performed on a national test. And not by just a little — by a lot. This is the fifth test since 2009 the state's largest district has landed in that position, and it should serve as a serious wake-up call for change.
The National Assessment of Educational Progress is given every two years to a sample of students in all states and measures important subjects like reading and math. Students took this test in 2017.
No standardized test is a perfect measure, but the consistency of how poorly Detroit kids do on these exams points to a failed system that is resistant to reform. It also highlights the challenges of education in high-poverty, minority districts.
As on the other tests, students at Detroit Public Schools Community District scored in single digits for proficiency in fourth- and eighth-grade reading and math — something no other urban district did.
Among all grades and subjects, Detroit posted the lowest scores. In the benchmark of fourth-grade reading, only 5 percent of students scored proficient or higher (meaning they passed the test). The average for 27 large districts was 28 percent, with the top two districts scoring at 38 and 42.
Detroit's eighth-grade reading scores were barely better, with 7 percent meeting proficiency. Math scores were even lower, with only 4 percent of fourth-graders proficient.
Those numbers mean that Detroit students are years behind their peers. Catching up will be a monumental challenge.
Since these students took the standardized test, the district has undergone some major changes, including hiring Superintendent Nikolai Vitti a year ago. He is very aware of the problems facing DPSCD and is working to ensure classrooms are using updated curriculum and are tackling reading in a systematic way.
It's worth noting that two of the large city districts that made the most progress and scored highest on the NAEP are Miami and Duval County — two Florida districts where Vitti previously worked. Prior to getting the Detroit job, Vitti was superintendent in Duval County. So he's seen what works.
While Detroit's students are the farthest behind, Michigan as a whole remained fairly stagnant in its performance. Although students made slight gains, Michigan's scores are below national averages. Nationwide, scores aren't great, either. Only about a third of eighth-graders and 40 percent of fourth-graders are proficient in reading and math.
This latest round of scores reinforces what we already know about the need for a new approach to education in Michigan.
Amber Arellano, executive director of the Education Trust-Midwest, says Michigan's student performance is "well below top 10 education states."
As Michigan seeks to attract new companies and boost economic development, the quality of K-12 schools matters. Businesses take note of these scores and other academic measures in making decisions of where to locate, and it's one of the reasons Amazon passed on choosing Detroit for its second headquarters.
Other states showing gains have followed innovative reform plans and stuck with them for more than a decade. That kind of focus and consistency has been lacking in Michigan, where reforms come and go at a dizzying come and go.
Many different groups, including education and business, have put out plans recently for how to improve the state's schools, and given it's an election year, state politicians are starting to release their ideas, too.
This is an essential priority, and one that deserves an urgent action plan.
___
Lansing State Journal. April 12, 2018
Break down barriers to equal pay for all women
The Time's Up movement got its start in the wake of the Harvey Weinstein scandal, and has come to stand for the women's movement itself.
The time is definitely up. Harassment and discrimination have no place in the modern workplace, and that applies to equal pay as well. Women make up over half the world's population, it's time to make everyone an equal partner in the community.
Equal Pay Day, which was April 10, is a perfect reminder of what's at stake: The day symbolizes how far into 2018 women had to work to catch up to what men made in 2017.
The National Partnership for Women & Families released rankings on Tuesday to commemorate the day. Michigan ranks 20th for the gender wage gap - women across the state make an average 78 cents per dollar paid to a man, compared to the national average of 80 cents per dollar.
That translates to Michigan women taking home $11,044 less each year - on average. The state's total gender wage gap is equates to $24.5 billion a year.
Minority women in Michigan fare far worse: Black women average 63 cents per dollar paid to a man, and Latina women only 58 cents per dollar.
According to the Institute for Women's Policy Research, if current laws remain unchanged in Michigan the gender wage gap is not projected to close until 2086. That's 68 years from now. And that's not acceptable.
It's time to stop evading the issues that allow the wage gap to persist. Workplaces must be more friendly to women, including paid sick days to care for dependents, paid maternity leave, zero tolerance for harassment and bullying and specific policies that prohibit pay discrimination.
Some employers already provide these benefits and adhere to fairness policies. They should be role models to the ones that don't. Our workplaces are strongest when they are diverse and engaged. Equal pay regardless of gender, race and ethnicity makes business sense.
The time is up for women to receive equal pay. Legislators, employers and workers must work together to end the discrimination.
___
Times Herald (Port Huron). April 12, 2018
Trade threats sow unease on the farm
It has never been easy to be a farmer. In recent years, it only seems to get more difficult.
Crop prices are down 40 percent since 2013 and farm income has fallen by half. Margins are slim, and every bump in the agricultural economy pushes more farms over the edge.
"It's tough right now," Roger Headley, a western Michigan dairy farmer dairy farmer, said at an auction where another farmer liquidated his herd. "You don't make enough to pay the bills."
Nate Elzinga, 32, said his family wasn't making enough money to keep up with maintenance costs of their cows. "We just aren't getting paid enough for our milk," Elzinga said. "It's supply and demand."
The news out of Washington isn't helping. Whatever that news is. President Donald Trump, his political allies and opponents and their counterparts around the world aren't helping. The president threatened China with broad import tariffs. The Chinese threatened back with tariffs on U.S. exports, specifically targeting agricultural goods. Trump's economic advisers say there are back-channel negotiations underway with China. China says it is too late for negotiations. As one of his first acts as president, Trump pulled out of the Trans-Pacific Partnership, an 11-nation free trade agreement among some of the largest economies in the world. On Thursday, he said he was directing advisers to look into rejoining the alliance.
It should make your head swim.
Farm state leaders in Congress welcomed the idea.
In 2017, China imported 1.4 billion bushels of U.S. soybeans, 62 percent of total U.S. exports and nearly one-third of U.S. farmers' annual soybean production. Soybeans are St. Clair County's largest crop and biggest export. China said it would slap a 25 percent tariff on U.S. soybeans if Trump carried through on his threat.
If that happened, U.S. soybean exports to China could drop dramatically as it shopped for lower priced soybeans elsewhere in the world.
"Retaliation by China against U.S. tariffs would undercut prices received by soybean producers and further hurt the already depressed farm economy," John Heisdorffer, president of the American Soybean Association, told the House Agriculture Committee this week.
The farm economy is not being helped by all the uproar, threat and counter-threat. Soybean futures have whipsawed up and down since the trade war of words began, adding uncertainty and anxiety to farming's financial headache. It is spring in the United States and farmers need to know now if their soybeans have a future buyer.
Talk may be cheap, but it has expensive ramifications for St. Clair County farmers.
___
The Mining Journal.  April 13, 2018
Proposed watercraft fee won't have desired impact
We sincerely hope the state drops its interest in setting up a new user fee system to cover paddle boats, canoes and kayaks.
The Michigan State Waterways Commission, the public entity that oversees such matters, voted in February in favor of registering "all rigid-hulled kayaks and canoes at a rate not to exceed $10 per year," the Associated Press reported.
Although the concept is rooted in concerns about boater safety — the U.S. Coast Guard reports that Michigan had the fourth-highest number of boating fatalities in the U.S. in 2016 with 38 — we can't see how this fee will do much to improve that number, given the number of people recreating on Michigan waterways each year and the almost limitless number of rivers, lakes and streams where said recreation takes place.
State Rep. Beau LaFave, a Republican from Iron Mountain, has come out against this proposal, noting, "This is one of the silliest things I've heard in my time as representative. What's next? Will we have to register our water skis, our boogie boards, our wetsuits?"
Point taken.
A superficial case can always be made that more money will solve the problem. And sometimes that case is stronger than others. In this particular case, though, we just don't see it. LaFave cites high costs for some families, rental companies and summer camps as among groups which would be most adversely impacted by the new set up. We have to agree.
The resolution and proposal are next expected to be discussed on April 25. We hope lawmakers make short work of it and move on to other more substantive matters.
___