BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Wednesday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMay                              119.80  Down 1.20May      119.15  119.20  117.35  117.75  Down 1.20Jul      121.30  121.30  119.50  119.80  Down 1.20Sep      123.50  123.50  121.70  122.00  Down 1.20Dec      126.80  126.80  125.15  125.45  Down 1.15Mar      130.50  130.50  128.65  128.95  Down 1.15May      132.50  132.50  131.15  131.30  Down 1.15Jul      134.00  134.25  133.40  133.40  Down 1.15Sep      135.90  136.10  135.25  135.25  Down 1.15Dec      138.65  138.85  138.00  138.00  Down 1.10Mar      141.45  141.45  140.65  140.65  Down 1.10May      142.45  142.45  142.45  142.45  Down 1.10Jul      144.05  144.20  144.05  144.20  Down 1.10Sep      145.45  145.80  145.45  145.80  Down 1.05Dec                              148.20  Down 1.05