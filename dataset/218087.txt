Cheese curds are a popular treat in the upper Midwest. Sometimes the cheesy treat is served as a side at fast food restaurants such as Culver’s and A&W.

The Wisconsin Milk Marketing Board is sharing the history of the dairy product on AgDay above.