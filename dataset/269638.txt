On Wednesday, the House Agriculture Committee is set to debate the 2018 farm bill, and conservation could benefit from that title of the legislation.

The Conservation Reserve Program (CRP) is likely to see a 5 million acre increase of up to 29 million acres, but the big concern is how will it be paid for.

Rep. Collin Peterson (D-Minn.), minority leader on the House Ag Committee, is proposing a 80 percent cap of the rental value in a county.

“We will pay farmers less—considerably less,” he said. “We have been overpaying for the CRP. The reason I believe this has happened is that [CRP] has been hijacked by environmentalists. It drove the price up.”

This sentiment has also been echoed by Dale Moore, executive director of public policy with the American Farm Bureau Federation. He said it’s a complaint that’s been heard from young farmers and ranchers.

“The ability to take on a new land or the ability to get started in agriculture, they’re competing against the CREP program,” said Moore. “If you can get those rental rates lined up a little more fairly in terms of cahs rental rates that might help open that door a little bit.”

Ahead of the bill’s release, Moore says AFBF wasn’t in favor of increasing CRP acres to nearly 30 million, and preferred to keep the current program at 24 million acres.