Mucci Farms is coming to the U.S. to put down roots. 
Kingsville, Ontario-based Mucci Farms has broken ground on a multi-year investment in Ohio that will eventually total 60 acres of greenhouse capacity.
 
The Huron, Ohio facility will be completed in three phases over several years, according to a news release.
 
Mucci Farms has been exploring a number of U.S. expansion opportunities over the past 18 months and company officials said the Huron site was chosen because of its easy access to a number of current and potentially new retailer distribution facilities. Another plus for Huron is its proximity to Mucci Farms headquarters in Kingsville, the company said. Huron is less than three hours by car from Kingsville, Ontario.
 
Company officials said expanding Mucci Farms operations to include a U.S. growing facility was strategically done to meet U.S. customer demands for locally grown, year-round fresh produce.
 
“We are extremely pleased to be a part of this next step for Mucci Farms,” Abbey Bemis, executive director of the Erie County Economic Development Corporation, said in the release. “The company’s advanced practices and reputation is well suited for our region and business community.” 
 
The facility will be equipped with grow lights which can extend the greenhouse growing season year-round. When complete, the facility will include a 272,000-square-foot distribution warehouse to support the company’s growing U.S. customer base, according to the release.
 
The first phase, consisting of 24 acres, is expected to be shipping produce as soon as March 2018, according to the release. When the first phase of the building is complete, the company will employ up to 100 full-time positions.