Porcine Epidemic Diarrhea virus (PEDv) isn't solely an American pork industry issue. As Dr. Paul Yeske, a veterinarian with the Swine Vet Center in St. Peter, Minn., reported at the 2015 Swine Day the virus has also been found in countries around the world, including Canada.Canada, the world's third-largest pork shipper, initially identified PEDv in January 2014.
Like their American counterparts, Canadian pork producers immediately attempted to figure out how the disease crossed into Canada.
One Alberta-based veterinarian pointed to dried porcine plasma as the source of PEDv, disagreeing with an announcement made by the World Organization for Animal Health (OIE) in October 2014 that dismissed plasma products as the source of PEDv.
Though the source of PEDv was not clear, the industry had no time to waste. As seen in American barns, Canadian producers raced to implement strong biosecurity efforts to keep the disease away from their animals.
These efforts paid off - by 2015, many of the remaining cases of PEDv in Canada were linked to what Greg Douglas, Ontario's chief veterinarian, called "lapses in biosecurity."
See, "More biosecurity today pays dividends tomorrow"
Today, Canada's pork industry is still facing the threat of PEDv. However, Julia Keenliside, a veterinary epidemiologist with Alberta Agriculture and Rural Development, points to the United States as the biggest threat of exposing Canada's pork operations to PEDv.
Keenliside explained the situation to Bruce Cochrane in a recent Farmscape broadcast.
"In the U.S.A. which is the main source of infection, PED is established or endemic so we have to remember that it's going to be there for quite a long time and still provide a risk for Canada," she said.
Keenliside added "We'll know that for sure as the season wears on but the U.S.A. will still be considered a risk for Canada."
Click here for the full broadcast.