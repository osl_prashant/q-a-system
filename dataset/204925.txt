The growing likelihood of La Niña could be good news for coffee producers in drought-hit Colombia where the weather pattern is associated with heavy rains and the sugar sector in India where it may trigger a stronger-than-normal monsoon.Traders and analysts said, however, much would depend on timing with strong rains at the wrong stage of crop development potentially damaging coffee production in Vietnam and Indonesia.
A U.S. government weather forecaster last week said there was an increasing chance of La Niña taking place in the second half of the year.
Typically less damaging than El Niño, La Niña is characterized by unusually cold ocean temperatures in the equatorial Pacific Ocean and tends to occur unpredictably every two to seven years.
"La Niña usually brings more rainfall to India. This is good news for the sugar industry as farmers can increase cane plantings for the 2017/18 crushing season," said Sanjeev Babar, managing director of Maharashtra State Co-operative Sugar Factories Federation.
India is the world's number two sugar producer.
In Brazil, the top producer of both sugar and coffee, any impact from La Niña appears likely to be mild.
"El Niño and La Niña effects on the main coffee areas, i.e. Minas Gerais, are weak at best," said Carlos Mera, senior commodity analyst at Rabobank.
Mera said La Niña can cause increased rainfall in Colombia which would be beneficial in view of the present water deficit.
The weather pattern is also associated with heavy rainfall across south-east Asia.
"Strong rain can bring bad impact to trees after a period of extremely dry conditions," said Le Ngoc Bau, director of the Western Highlands Agriculture and Forestry Institute based in Daklak, Vietnam's leading coffee growing province.
Harvesting of Vietnam's 2016/2017 coffee crop will start in October, while heavy rain around the start of the process could cause cherries to drop, reducing output, traders said.
Vietnam is the world's second largest coffee producer.
The weather pattern could also pose a threat to coffee production in Indonesia where any incidence of heavy rains in September could hamper flowering.
"Ideally (in the flowering season) it rains during the night, and is dry during the day. But if it rains non-stop, then it will cause production to drop," said Irfan Anwar, chairman of the Association of Indonesian Coffee Exporters and Industries.
In No. 2 sugar exporter Thailand, the impact of any La Niña was hard to predict as drought had damaged yields.
"First we have to see how much cane survives and how many cane plantations will still be left in the wake of this ongoing drought," said Boonthin Kotsiri, production director at Thailand's Office of Cane and Sugar Board.