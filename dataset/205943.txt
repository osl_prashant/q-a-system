Market reaction

Ahead of USDA's reports, corn futures were trading 3 to 5 cents lower, soybeans were steady to a penny lower, wheat futures narrowly mixed and cotton was 100 to 180 points lower.
In post-report trade, corn is 9 to 10 cents lower, soybeans are around 14 cents lower, winter wheat is 2 to 3 cents lower, spring wheat is 5 to 10 cents lower and cotton is trading sharply lower to down the 300-point limit.
Pro Farmer Editor Brian Grete reacts to the report:



 
Crop production
Corn: 14.184 billion bu.; trade expected 14.035 billion bu.
-- compares to 14.153 billion bu. in August; 15.148 billion bu. in 2016
Beans: 4.431 billion bu.; trade expected 4.328 billion bu.
-- compares to 4.381 billion bu. in August; 4.307 billion bu. in 2016
Cotton: 21.758 million bales; trade expected 20.59 million bales
-- compares to 20.55 million bales in August; 17.2 million bales in 2016
USDA's national average corn yield estimate of 169.9 bushels per acre is up 0.4 bu. from August and is 1.7 bu. above the average pre-report trade estimate. The corn crop estimate is 149 million bu. above the average pre-report trade estimate and is up 31 million bushels from last month. Harvested acres were unchanged from last month.
State yield estimates are a mixed bag from last month. USDA expects yield increases from August in Illinois (up 1 bu. to 189 bu. per acre), Missouri (up 2 bu. to 164 bu.), North Dakota (up 3 bu. to 124 bu.), Ohio (up 2 bu. to 173 bu.), South Dakota (up 5 bu. to 145 bu.) and Texas (up 8 bu. to 140 bu. per acre).
Corn yield declines from last month are estimated in Indiana (down 2 bu. to 171 bu. per acre), Iowa (down 1 bu. to 187 bu.), Michigan (down 1 bu. to 169 bu.), Minnesota (down 1 bu. to 182 bu.) and Nebraska (down 2 bu. to 181 bu. per acre).
Yields are steady with month ago in Kansas (133 bu.) and Wisconsin (162 bu. per acre).
USDA national average soybean yield estimate of 49.9 bu. per acre is up 0.5 bu. from August and is 1.1 bu. above the average pre-report trade estimate. The bean crop estimate is 103 million bu. bigger than the pre-report trade estimate and is up 50 million bushels from last month. Harvested acres were unchanged from August.
State yield estimates for soybeans are also a mixed bag, but most are either steady to higher from last month. Yield increases are estimated in Arkansas (up 2 bu. to 51 bu. per acre), Indiana (up 1 bu. to 56 bu.), Iowa (up 1 bu. to 57 bu.), Kansas (up 2 bu. to 43 bu.), North Dakota (up 2 bu. to 35 bu.), Ohio (up 1 bu. to 54 bu.) and South Dakota (up 4 bu. to 45 bu. per acre).
Yield declines are estimated in Michigan (down 1 bu. to 48 bu.), Minnesota (down 2 bu. to 47 bu.) and Nebraska (down 2 bu. to 56 bu. per acre).
Soybean yields are steady with month ago in Illinois (58 bu.), Missouri (49 bu.) and Wisconsin (48 bu. per acre).
The cotton crop estimate came in 1.168 million bales above trade expectations and is up 1.208 million bales from last month. The national average cotton yield is now estimated at 908 lbs. per acre, up 16 lbs. from last month. Yields in Georgia are estimated down 26 lbs. from August at 1,013 lbs. per acre and the Texas cotton yield is estimated at 757 lbs., up 15 lbs. from August. USDA increased estimated harvested cotton acres by 452,000 acres.
 
U.S. carryover
Corn: 2.350 billion bu. for 2016-17; down from 2.370 billion bu. in August
-- 2.335 billion bu. for 2017-18; up from 2.273 billion bu. in August
Beans: 345 million bu. for 2016-17; down from 370 million bu. in August
-- 475 million bu. for 2017-18; unch. from 475 million bu. in August
Wheat: 933 million bu. for 2017-18; unch. from 933 million bu. in August
Cotton: 2.75 million bales for 2016-17; down from 2.8 million bales in August
-- 6.0 million bales for 2017-18; up from 5.8 million bales in August
On old-crop corn, USDA raised estimated exports by 70 million bu. to 2.295 billion bushels. That was partially offset by a 50-million-bu. reduction in estimated food, seed & industrial use (with ethanol accounting for 15 million bu. of that decline). USDA puts the national average on-farm cash corn price for 2016-17 at $3.35, unchanged from August.
On new-crop corn, USDA increased total supplies by 12 million bu. (the result of lower beginning stocks, but higher production). On the demand side, USDA cut FSI use by 75 million bu. (corn-for-ethanol was cut 25 million bu.) and raised estimated feed & residual use by 25 million bushels. The end result is a 62-million-bu. increase in estimated new-crop carryover from last month. USDA puts the national average on-farm cash corn price for 2017-18 at $2.80 to $3.60, down a dime on both ends of the range from last month.
On old-crop soybeans, USDA cut estimated carryover by 25 million bushels. Estimated exports were increased 20 million bu. (to 2.170 billion bu.) and crush was increased 5 million bu. (to 1.895 billion bushels). USDA puts the national average on-farm cash bean price for 2016-17 at $9.50, unchanged from last month.
On new-crop beans, USDA increased total supplies by 24 million bu., the result of a bigger crop estimate, but smaller beginning stocks. On the demand side, USDA increased estimated exports by 25 million bushels to result in a steady carryover estimate. USDA puts the national average on-farm cash bean price for 2017-18 at $8.35 to $10.05, down a dime on both ends of the range from last month.
On new-crop wheat, USDA made no changes to the supply or demand side of the balance sheet with the Annual Small Grains Summary coming up at the end of the month. USDA puts the national average on-farm cash wheat price for 2017-18 at $4.30 to $4.90, down a dime on the bottom of the range and down 30 cents on the top end of the range from last month.
On old-crop cotton, USDA cut estimated carryover by 50,000 bales, the result of a 50,000-bale-increase in unaccounted use. It put the national average on-farm cash cotton price for 2016-17 at 68 cents, unchanged from last month.
On new-crop cotton, USDA increased total supplies by 1.16 million bales, the result of the bigger crop estimate. On the demand side, USDA increased estimated cotton exports by 700,000 bales (to 14.9 million bales) and unaccounted use by 260,000 bales. The result is a 200,000-bale increase in estimated carryover from last month. USDA puts the national average on-farm cash cotton price at 54 cents to 66 cents, down a penny on both ends of the range from last month.
Global carryover
Corn: 226.96 for 2016-17; down from 228.61 MMT in August
-- 202.47 MMT for 2017-18; up from 200.87 MMT in August
Beans: 95.96 MMT for 2016-17; down from 96.98 MMT in August
-- 97.53 MMT for 2017-18; down from 97.78 MMT in August
Wheat: 255.83 MMT for 2016-17; down from 258.56 MMT in August
-- 263.14 MMT for 2017-18; down from 264.69 MMT in August
Cotton: 89.57 million bales for 2016-17; down from 89.99 million bales in August
-- 92.54 mil. bales for 2017-18; up from 90.09 mil. bales in August


Global production highlights

Argentina beans: 57.8 MMT for 2016-17; compares to 57.8 MMT in August
-- 57.0 MMT for 2017-18; compares to 57.0 MMT in August
Brazil beans: 114.0 MMT for 2016-17; compares to 114.0 MMT in August
-- 107.0 MMT for 2017-18; compares to 107.0 MMT in August
Argentina wheat: 17.5 MMT for 2016-17; compares to 17.5 MMT in August
-- 17.5 MMT for 2017-18; compares to 17.5 MMT in August
Australia wheat: 33.5 MMT for 2016-17; compares to 35.11 MMT in August
-- 22.5 MMT for 2017-18; compares to 23.5 MMT in August
China wheat: 128.85 MMT for 2016-17; compares to 128.85 MMT in August
-- 130.0 MMT for 2017-18; compares to 130.0 MMT in August
Canada wheat: 31.7 MMT for 2016-17; compares to 31.7 MMT in August
-- 26.5 MMT for 2017-18; compares to 26.5 MMT in August
EU wheat: 145.43 MMT for 2016-17; compares to 145.7 MMT in August
-- 148.87 MMT for 2017-18; compares to 149.56 MMT in August
FSU-12 wheat: 130.74 MMT for 2016-17; compares to 130.54 MMT in August
-- 137.27 MMT for 2017-18; compares to 133.77 MMT in August
Russia wheat: 72.53 MMT for 2016-17; compares to 72.53 MMT in August
-- 81.0 MMT for 2017-18; compares to 77.5 MMT in August
China corn: 219.55 MMT for 2016-17; compares to 219.55 MMT in August
-- 215.0 MMT for 2017-18; compares to 215.0 MMT in August
Argentina corn: 41.0 MMT for 2016-17; compares to 41.0 MMT in August
-- 42.0 MMT for 2017-18; compares to 40.0 MMT in August
South Africa corn: 17.15 MMT for 2016-17; compares to 16.7 MMT in August
-- 12.5 MMT for 2017-18; compares to 12.5 MMT in August
Brazil corn: 98.5 MMT for 2016-17; compares to 98.5 MMT in August
-- 95.0 MMT for 2017-18; compares to 95.0 MMT in August
China cotton: 22.75 mil. bales for 2016-17; compares to 22.75 mil. bales in August
-- 24.5 MMT for 2017-18; compares to 24.5 MMT in August