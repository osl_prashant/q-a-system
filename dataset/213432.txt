The sinking of the R.M.S. Titanic is one of the most famous events in maritime history — history, period — thanks in large part to some 17 major motion pictures that have been made about the iconic ocean liner.
That list includes three silent films released in 1912, just months after the ship sank; several “family-friendly” animated movies re-telling the disaster in which more than 1,500 people drowned; a 1964 musical starring Debbie Reynolds, “The Unsinkable Molly Brown;” and a 1953 Twentieth-Century Fox blockbuster (for the time) about an ex-pat American who obtains a last-minute steerage ticket for Titanic’s maiden voyage, discovers that his wife (Barbara Stanwyck), who’s on the ship, is taking his 18-year-old daughter to America — only the daughter falls in love with a dashing university student, played by the young Robert Wagner decades before he became the eye patch-wearing Number 2 in the Austin Powers series. But of course, after the ship hits the iceberg, he dies, she survives, their love goes on … sound familiar?
Even with the $200-million production budget that director James Cameron lavished on the 1997 multiple Academy Award winner “Titanic” that likely set the bar so high no future disaster epic will try to top it, the special effects don’t fully capture the size, scale and splendor of the actual vessel itself.
Hard to believe that the Titanic was built in just three years for what is estimated to be about $400 million in today’s dollars. Not when the cost of one new football stadium for the LA Rams is expected to top $2.6 billion — and the Titanic was as long as three football fields put together.
Just a brief summary of the ship’s amenities is staggering: Parlor suites with private 50-foot promenades; first-class staterooms designed in Italian Renaissance, Queen Anne, Louis Quatorze and Georgian décor; a heated swimming pool (the first ever on a ship); a 10,000-square-foot dining salon (First Class only, please); a Veranda Cafe with live palm trees; squash courts, Turkish baths, electric elevators and, of course, elegantly appointed smoking rooms.
The ship’s dimensions were as titanic as its nameplate: 47,000 tons when loaded, holding 3,547 passengers and crew, powered by 76,000-horsepower steam engines fired by 29 boilers that consumed more than 850 tons of coal a day.
Those classic scenes of dozens of soot-stained men in the bowels of the ship relentlessly shoveling coal into roaring fireboxes? They weren’t too far off the mark.
Strange Menu Choices
Along with all the high-tech navigation and telecom systems incorporated on modern cruise ships, the foodservice options have evolved just as dramatically in the last century.
Although the Titanic was provisioned with 75,000 pounds of meat, 40,000 pounds of poultry and game, 15,000 pounds of fresh and salted fish, 40,000 eggs and more than five tons of bacon, ham and sausage — also included (ouch!) were 7,000 head of iceberg lettuce — suffice to say that the “exotic” food choices available to the rich and haughty occupying its first-class accommodations would not only fail to connect with 21st century palates, most contemporary cruise line passengers would look at the 1912 menu with outright puzzlement.

For example: Consider the Titanic’s “Luncheon Menu” of April 14, 1912 (the day before the ship sank):
Consommè Fermier
Cockie Leekie
Fillets of Brill
Eggs a l’Argenteuil
Chicken a la Maryland
If you recognize any of those choices, you’re way ahead of me.
For the record, the consommé was a clear vegetable broth; Cockie Leekie is chicken soup with leeks; Fillets of Brill is some sort of fish entrée; Eggs a l’Argenteuil is similar to Eggs Benedict, with the eggs parboiled, simmered in butter then served topped with cream and croutons; and Chicken a la Maryland (apparently) was a fancy name for fried chicken.
And those were appetizers.
Entrées included Grilled Mutton Chops, Salmon Mayonnaise, Norwegian Anchovies (are there any other kind?), Roast Beef, Round of Spiced Beef, Veal & Ham Pie, Virginia and Cumberland Ham, Bologna Sausage, Galantine of Chicken (the 1912 Turducken) and Corned Ox Tongue.
With the exception of “roast beef,” would any of those items get traction on a contemporary restaurant menu? Probably a lot of questions for the wait staff, but not many takers.
By contrast, the “Supper Menu” for Titanic’s third-class passengers consisted of “Gruel, Cabin Biscuits and Cheese.”
Charles Dickens would be proud.
With ocean voyages, we like to measure our progress over the last century by the advance of technology, with GPS navigation, smartphone connectivity, digital entertainment and a host of leisure activities unknown in 1912. Truthfully, the “lifestyle” aboard a modern cruise ship bears little resemblance to the experience of the passengers on the Titanic — even the ones occupying those first-class suites.
Not to mention that nowadays, we don’t set sail to get back home, we go on cruises to get away from where we live.
But the biggest change of all between then and now is captured in a single sheet of paper: The Titanic’s menu.
Most of that ill-fated ship’s cuisine has disappeared as surely as the vessel itself.
The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.