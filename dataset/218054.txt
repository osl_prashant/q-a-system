For all its utility as an incredibly convenient means of connecting with friends and family, social media has become a double-edged sword for numerous celebrities, professional athletes and public officials.
Too many to count, to be honest.
As quickly as Twitter messages or Facebook posts turn people into targets of serious online shade, the offenders try to delete the posts (doesn’t work), or walk back the memes that have gotten them in trouble (ineffective).
From Blake Shelton’s social media messages that painted him as both racist and sexist, to Cher’s tweeting sympathy for victims of an airport attack in Turkey with a bomb emoji, to the classic Instagram post of rapper Bow Wow claiming he was heading to New York City on a luxurious private jet with a stock photo used by a Florida transportation service, while a passenger photographed him boarding a commercial flight, celebrities never seem to learn that the social media universe will pounce on anything untoward or ill-advised and turn it into an online shamefest.
Even big-name corporations have spent time in social media Blundertown, including:
· Adidas. After the 2017 Boston Marathon, the shoe company tweeted out, “Congrats, you survived the Boston Marathon!” which recalled horrifying images of the 2013 Boston Marathon bombing.
· Dove. The cosmetics manufacturer went viral with its ill-advised launch of shampoo bottles shaped to reflect different women’s body types. Guess who weren’t thrilled to purchase the “plump” shaped bottle?
· United Airlines. In one of the most celebrated social media fails, CEO Oscar Munoz followed up the viral video of flight attendants dragging a combative passenger through the aisles with an “apology” blaming the victim and praising employees for “following proper procedures.”
A Beef with the Minister
Despite the obvious social media pitfalls politicians should have learned to avoid, we can now add a provincial minister in Alberta to the list of epic online failures.
According to a story in The Globe and Mail, Shannon Phillips, Alberta’s Minister of Environment and Parks and the Minister Responsible for the Climate Change Office, tweeted earlier this month to encourage citizens of that Canadian province to “eat less meat.”
Phillips, a member of Canada’s NDP (the New Democratic Party, or in Quebec, Nouveau Parti Démocratique), denied that she sent the tweet, claiming that the message, posted on her account Jan. 2, was actually created by a staffer when Phillips said she was “away from her Twitter account,” to quote her explanation to the media.
Seriously?
She added that the tweet was “regrettable,” and that she’s been assured “It won't happen again.”
The tweet in question suggested that anyone needing a New Year’s resolution should consider taking the Green Challenge, a campaign developed by Alberta-based Environment Lethbridge. The month-long challenge involved five eco-sensitive actions people should embrace:
· Reuse shopping bags (good idea)
· Eliminate vehicle idling (even better idea)
· Unplug electronics devices (underrated way to save energy)
· Take shorter showers (good luck with that one)
· Eat less meat
That last item probably seemed like an obvious choice for some clueless staffer enamored with the “meat-is-killing-the-planet” mantra activists have been flogging for close to a decade now, but it was a horrible suggestion for Albertans, given the huge economic impact livestock production contributes to the province.
Alberta raises nearly five million cattle, with 149 feedlots of 1,000 head or more totaling 1.6 million head, representing more than two-thirds of Canada’s fed beef, according to data from the Alberta Cattle Feeders Association.
That prompted Jason Kenney of the opposition United Conservative Party to tweet that people are free to eat what they want, and it should stay that way.
“As someone who supports Alberta’s farmers and ranchers, and enjoys a good steak from time to time, I will not be taking the NDP’s advice,” Kenney tweeted.
As everyone does when caught red-handed in an embarrassing social media post, Phillips quickly tried to backpedal from the offending tweet.
She sent out a series of tweets last week noting her support for Alberta’s meat sector. She said she comes from “a family of beef producers,” adding that her grandfather came to Canada in 1919 and his cow-calf operation became one of Alberta’s most successful family farm operations.
“My tweet was about a local enviro challenge, and was in no way meant to offend the industries that are vital to our province’s culture and economy,” Phillips said on Twitter. “We know that the rhetoric from Jason Kenney and the UCP is often overheated.”
Methinks what’s “overheated” is more likely to be the seat in which the unfortunate staff person in Phillips’ office is currently sitting.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.