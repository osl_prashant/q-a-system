You negotiate every day. From input costs to employee salaries to partnership opportunities, you have a lot on the line. Are you leaving dollars on the table?
"Negotiation is one of the most valuable activities in which you'll ever participate," says Mark Faust, author of "High Growth Levers" and columnist for AgPro, a sister publication of Top Producer. "You make more money per hour when you prepare and execute negotiations ina strategic manner."
Start by understanding the basics, says Jack Williams, a professional negotiator and consultant for Corporative Visions, a marketing and sales training company.
"A negotiation is simply two parties trying to reach an agreement over a scarce resource such as land, money, water, labor or equipment," Williams points out.
When people get in the heat of the moment during a negotiation, they lose their wits and make mistakes, Williams explains. "You need to be able to identify what mistakes you make and break those habits," he says. Here are some common missteps to avoid:
1. Failing to prepare in advance. "Too many people think a negotiation is about the face-to-face phase," Williams says. "Most negotiations are over by the time you hit that phase." Make a list of positive and negative consequences, musts and wants, and build rapport, Faust says.
2. Opting to present alone. Maximize team talent in high-stakes negotiations. "Only idiots negotiate major deals alone," Faust says.
3. Talking too much. "If you are doing all the talking, then you aren't doing enough listening," Williams says. Ask questions to learn more about the other party's needs.
4. Splitting the difference. Create multiple offers that let you negotiate value and variances without giving in on price or other high-priority needs or wants, Faust advises.
5. Giving in too fast. Yielding to the other party early in the process can set an unwanted precedent.
6. Fearing conflict. You'll face differences of opinions in nearly all negotiations. Don't let those potential disagreements intimidate you.
7. Trying to control what you can't. Hurdles to an agreement can include timing, market consolidation and insufficient funds. "When you look at all the challenges, they have something in common‚Äîyou can't control them," Williams says. "Good negotiators don't worry about these things they can't control."
5 Core Emotions to Understand
Emotions can run a negotiation off the tracks. But instead of burying emotions, lead negotiations with your emotions and those of the other person, says Daniel Shapiro, founder and director of the Harvard International Negotiation Program, and author of "Negotiating the Nonnegotiable."
Focus on five emotions that matter to everyone, Shapiro advises. "Your ability to deal with these core concerns will give you power," he says.
Appreciation: The desire to feel heard and valued is universal. Aim to understand the other side's viewpoint and communicate understanding.
Autonomy: The freedom to make decisions without imposition from others. Follow the "ACBD" model‚Äîalways consult before deciding.
Affiliation: This is emotional connection between you and the other person. If you feel excluded, you are no longer focused on the content, you are focused on the emotion.
Status: Who is up and who is down? You need to respect everybody's status rather than competing over it.
Role: Instead of automatically playing the adversary, fill a meaningful and effective role. Craft roles that make you all work more effectively. You have great power to structure the role you and others play.