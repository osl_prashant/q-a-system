Many acres of fescue pastures are fertilized each spring. Determining whether to apply fertilizer and if so, how much depends on many factors. These would include: whether the field is hayed or grazed; how much, if any Nitrogen (N) was applied in the fall; the price of fertilizer, the value of hay or forage produced; and the growing conditions since last fall.
Hay 
Normal N fertilization rates for established fescue and brome hay fields are 50 to 100 pounds of actual N per acre, or about 60 pounds of actual N per ton of expected yield. A recent summary of fescue and brome N response data shows that across nearly 100 experiments, the average yields for unfertilized plots was 1.35 tons of hay per acre, while maximum yields averaged 3.15 tons of hay with 140 pounds of N.




N Rate 


Hay Yield 

 

Hay


Fertilizer 


Total  




(lbs. N/acre) 


(tons DM/acre) 

 

Value


Cost


Return




0


1.35

 

$81


$0


$81




20


1.8

 

$108


$8


$100




40


2.2

 

$132


$16


$116




60


2.52

 

$151


$24


$127




80


2.78

 

$167


$32


$135




100


2.97

 

$178


$40


$138




120


3.1

 

$186


$48


$138




140


3.15

 

$189


$56


$133




Using a hay value of $60 per ton and an applied N cost of $.40/pound the chart above confirms that the normal rates of nitrogen application (50 – 100#/acre) are appropriate to maximize profit in most years. Obviously hay value and nitrogen costs vary from year to year so adjust rates accordingly.   
One issue these calculations don’t consider is hay quality. There is potential for harvesting hay with a higher protein level as long as the hay is harvested in the boot stage.  Of course that advantage is diminished or goes away if grass is allowed to mature when grazed or harvested for hay. In that case, extra fertilizer just results in more low quality hay. Hay harvested in the boot stage has the potential to meet all of the protein and energy needs of beef cows in all stages of production. Forage test available through our offices provide insight into the true value of the hay you produce or purchase.
Pasture 
Under normal conditions, fescue and brome pastures that are grazed in both spring and fall should receive about 100 pounds total N per acre, with 60 percent applied in the winter or early spring and 40 percent in late August or early September, along with any needed Phosphorus(P) and Potassium(K). So producers should plan on applying 60 to 70 lbs. N per acre in winter or early spring.
To get a good response from nitrogen applications, P and or K must be applied if needed. Best results from phosphorus and potash applications occur when they are applied prior to the forage coming out of summer dormancy along with a small amount of nitrogen.  Early fall applied P will help the grass develop a good root system for the winter, and develop buds for new tillers the next spring. P and K applied in winter or early spring won’t provide the same benefits. If no P or K was applied last fall, it can be applied along with the early spring application of nitrogen.
Other considerations 
One additional nutrient producer should consider monitoring in fescue and brome pastures or hayfields is sulfur (S). The only way to determine if sulfur along with phosphorous, potassium or lime are needed is through soil testing. Contact your local extension office for the procedures needed to submit a soil sample for analysis. An ideal time to sample is 30 days prior to fertilizer application. Samples for a P and K soil test should be taken to a 6-inch depth. A profile N test to a depth of 24 inches should be used to evaluate S needs.