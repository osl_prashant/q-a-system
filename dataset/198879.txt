Drought put the Finger Lakes region of New York State on the map this summer. And it has dairy managers scrambling to mitigate the extreme weather's grim consequences in the face of continued low milk prices and oversupply.Records from 18 DTN weather stations across the state show the lowest rainfall in Trumansburg, which lies in the center of the lakes: 3.2 inches between May 1 and July 19. Blazing sun and temperatures camped out in the 90s only intensified the drying process. Wells sputtered, ponds dried up.
Some old timers say the last brutal drought here was in the 70s, others recall a bad year in '56. "The difference today is that businesses are bigger," says Mark Ochs, an independent crop consultant from Trumansburg. "Cost structures have elevated to the point that a crop loss today is a much bigger deal. You try to source forage for 500 or 1000 cows‚Äîit's crazy, because your neighbor is in the same boat!"


Severe drought that scorched central New York all summer spread west in August, putting thousands more dairy cattle in harm's way.
While first cutting haylage was average or better, second cutting was off 25-50%, he says. "Third cutting was virtually unharvestable. A lot of guys mowed it just because they were sick of looking at it." Corn fields are uneven, but generally short, and in certain areas the plants didn't even pollinate.
"We have a pretty good carryover of feed," says Jason Burroughs, of Aurora Ridge Dairy in Aurora, N.Y., where they milk 2200 registered Holsteins. "But when you do the math, we're going to burn through that pretty quickly. What are we gonna' feed all these animals?"
A question that haunts and motivates
Aurora Ridge is now buying hay from north of the drought area, and has located some fermented corn silage to purchase. Jonathan Lamb, of Lamb Farms, Inc., in Oakfield, N.Y., is altering rations (adding straw, soy hulls) to extend haylage inventory. The Lambs milk a total of 6000 cows in three facilities and learned from other dry years to build up forage inventories when possible. "We understand that we accept risk every time we put seed in the ground," he says. "Still, years like this can be pretty stressful. It's definitely going to be a financial hardship." Severe drought spread from central New York to his location in the western region in early August.
Walnut Ridge Dairy, Lansing, N.Y., is feeding wet brewers grains for the first time in many years, and actively looking for standing corn to chop. "The showers were so spotty," says one of the dairy's partners, Steve Palladino, "it's a matter of finding the areas outside our region that had enough rain to produce better crops."
His farm's field crew soil tested alfalfa fields after third cutting and applied potash to boost fertility in case late-summer showers generate a decent fourth, and in some fields, fifth cutting. "That's $6000 we didn't want to spend this summer," he adds. Ochs says even with a really good fourth cutting, Finger Lakes dairies will end up with only a half crop of alfalfa haylage.
Some dairies will sell cows or heifers to reduce the number of mouths to feed. This amounts to another financial zinger since livestock prices are trending down in.
Who ya gonna' call?
Advisors of all stripes are pulling out their "A" games to assist dairy clients through this challenge.
Clayton Wood, who supervises 30 nutritionists across New York and New England for Cargill Animal Nutrition, says he is helping dairy managers: 1) tally actual on-farm feed inventories, demand needs & how much of a deficit that they may have, 2) address concerns about quality of this year's forage, and 3) help to source potential alternative sources of fiber (such as corn gluten, soy hulls, beet pulp) and starch (bakery, chocolate). "The 55-60% high forage diets that have been setting production records in New York State are a thing of the past for now," he says.
Jim Peck, of ConsulAg in Newark, N.Y., says you can reduce the danger of high nitrates in drought-stressed corn silage if you avoid chopping right after a rain event when plants are pulling nutrients up into the stalk. Beware nitrate gas in upright silos, or at the face of bunk silage on a quiet morning where you might see a yellow haze.

Third cutting was an exercise in futility, virtually unharvestable on many dairies.
One bright spot in the otherwise dire situation is lower prices for corn, soybeans, and commodities due to record yields in other regions.
The Northwest New York Dairy, Livestock and Field Crops Team has created a fact sheet about drought conditions that is available at the NWNY website, along with a number of other tools and spreadsheets to estimate forage needs. Cornell University's PRO-DAIRY experts also offer Resources for Forage Management in a Drought Situation.
And since some corn crops in this bone-dry region may be almost worthless to harvest for grain, Ochs proposes his cash grain clients consider selling their corn to dairy farmers as grass forage‚Äîeven if they have crop insurance.
"It still has feed value," he explains. "By avoiding harvest costs, drying costs, and shrink, the cash grain guy may offset his loss by selling to a neighboring dairyman to help him bridge this forage gap. He may break even or come out ahead."
Crop insurance adjusters will do field evaluations and estimate yields to help farmers decide whether to harvest or make a claim. But Ochs says there are unique factors brought on by the drought that concern him. "The fields are uneven. The stalks are poor quality because they've been so beat up by the weather; they may not dry down. The really small ears may go right through the stripper plates."
So Ochs will do his own yield estimates and forage analysis before making recommendations to his clients. "I've known these folks for 35 years. I hope we can work together."

The incredible shrinking pond. Water worries stressed producers as the drought intensified.
The rain will come
At press time a new weather system had begun to move into the region, blessing the parched hay and scorched pastures with more frequent moisture. But to most farmers, it feels too little, too late. "Two inches of rain (at the end of July) could have saved the corn crop," says Ochs. For plants that pollinated and eared, August rain will increase kernel size, fill out the ear, and increase the energy component of the stubby plants. "NEL of corn silage without ears might range between 58-62," he says. "With ears, we're talking low 70s." Much is still unknown until chopping gets underway.
It will be a while before financial experts measure how deeply this drought gouged the industry. But there will be survivors. Leaner, wiser, perhaps grayer survivors. "There's not a darn thing you can do about the weather," concludes Burroughs. "What you can do is strategize, be realistic, and make smart decisions."
Dry Summer Puts Pressure on New York Dairies


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.