This year, hog farmers dodged lower prices when they expanded sow herds and grew the second-largest pork supply since 2008.
Prices stayed above expectation, say University of Missouri Extension economists. Growing exports and consumers’ love for bacon helped demand for growing supply.
However, the economists caution, “At this rate, supply can outstrip demand.”
Good times are ahead. But Scott Brown and Daniel Madison see price drops in 2018 and 2019. Feed and nonfeed expenses continue to be uncertain with financial losses possible.
The economists tell all in an “Update for Livestock and Dairy Markets.” The report is part of an MU Food and Agricultural Policy Research Institute (FAPRI) midyear outlook.
After success, hog farmers will test their luck in 2018. “Higher sow numbers with more production per sow boosts expected production more than 3 percent,” Brown says.
“Pork exports grew more than 12 percent in the first half of this year. More export sales are needed to take added pork, keeping prices from sharp declines,” he says.
There’s a plus for producers this fall. “Added processing capacity coming on line boosts demand for live hogs.”
So far, producers win. “Farrow-finish farms have their third most profitable year since 2006,” Brown says. “That’s amazing, given that pork production grew 13 percent since 2014.”
There’s more than supply and demand in play. A shrinking dollar helps export buyers. Free-trade deals must remain in place to move pork abroad.
U.S. consumers love their bacon, but they won’t take all of the growing supply, Brown says.
In MU Extension livestock outlook meetings across the state, Brown urges farmers to do risk management. Taking higher prices is easy. Heading into declines takes protection.
Brown and Madison are agricultural economists in the Division of Applied Social Sciences. That’s part of the MU College of Agriculture, Food and Natural Resources.
The FAPRI report, which includes crops and biofuel, can be seen at fapri.missouri.edu.