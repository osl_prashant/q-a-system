With the Russian market closed to U.S. pork and beef, U.S. Meat Export Federation (USMEF) Manager Yuri Barutkin, who is based in St. Petersburg, is now focused on market development efforts in Europe and in the region surrounding Russia.Barutkin offers his observations on the state of Russian pork and beef production and red meat trade. He notes that the Russian government has increased its support for domestic meat production, and this has raised Russia’s level of self-sufficiency. Barutkin adds, however, that the higher level of self-sufficiency is also due to lower per-capita consumption.   
Many of Russia’s traditional red meat suppliers – such as the European Union, the U.S., Canada and Australia – are locked out of the Russian market due to economic countersanctions related to the ongoing conflict in Ukraine, so only a few major exporting countries remain eligible.
Russia’s primary red meat supplier is now Brazil, although Russia also imports beef from Paraguay and pork from Chile. But with economic conditions in Russia having a negative impact on consumers’ disposable income, even these remaining suppliers often struggle with sluggish demand.


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->