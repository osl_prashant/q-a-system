April 17 could be a historic day for wildfires across the High Plains and Southwest cautions officials with the Oklahoma Forestry Service (OFS) and the National Weather Service (NWS).
A red flag fire warning has been issued by NWS for a wide swath of counties throughout Texas, Oklahoma, Kansas and New Mexico. Colorado and Arizona also have a high fire risk in several counties today.
Wind speeds in the High Plains could average 30-40 mph and gusts could reach 65 mph, according to NWS. The peak fire timing is expected from 10 am April 17 to 2 am April 18.
“These areas are already drought-stricken; thus, combining dry fuels with very low relative humidity with gusty winds, this will give way to dangerous, life-threatening fire weather conditions. By Wednesday, the fire weather conditions will lessen but still be critical in parts of the southern and central Plains,” reads a report from NWS.
Click the National Weather Service map to visit the interactive version

The pink counties on the map are at a red flag fire warning for April 17.
 
In Oklahoma, temperatures are expected to reach upper 80°F and low 90°F, with clear skies and relative humidity values of 5-15%. OFS projects this will deliver fine-dead fuel moisture values as low as 2% with widespread observations of 3% expected.
“Fire weather is predicted to be historic. Meaning we haven’t seen conditions this extreme in over 10 years,” says Mark Goeller, fire management chief of OFS. “These conditions have the potential to produce very large devastating fires. Oklahoman’s needs to be extremely careful while doing anything outdoors.”
Oklahoma has been battling several fires since April 12 that have burned more than 400,000 acres. Firefighting has been complicated by the amount of eastern red cedar trees that have been catching on fire and large amounts of grass carried over from last year that are now dry from drought.
“We have an extremely heavy fuel load out in the grass and shrub,” says Jamie Atkins, fire behavior analyst for OFS. “The probability of ignition is 100%.”
A burn ban in Oklahoma has been expanded by Lt. Gov. Todd Lamb while Gov. Mary Fallin is out of the state. The ban extends into 36 counties in western Oklahoma.
“We have seen unprecedented fire conditions develop over the last week that created the dangerous wildfires that have burned over 400,000 acres so far,” Lamb says. “An expanded burn ban is called for to reduce the risk of preventable wildfires and to protect lives and property.”
Campfires, bonfires, and setting fire to any forest, grass, woods, wildlands or marshes, as well as igniting fireworks, burning trash or other materials outdoors are unlawful in the region.