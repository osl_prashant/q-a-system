Douglas Ackerman, the former head of the Florida Department of Citrus, will now lead a dairy organization.
The Lakeland, Fla.-based Ackerman has been hired as general manager of the Southeast United Dairy Industry Association Inc.
The Atlanta-based organization is a nonprofit dairy promotion organization that promotes dairy foods through schools, health professionals, retailers, dairy processors and the public.
Ackerman is scheduled to take his position in January after he succeeds Cheryl Hayn, who plans to retire after more than 28 years working for the dairy organization, according to a news release.
Ackerman, a former marketing manager with the Lakeland-based Publix Super Markets Inc., joined the Bartow-based citrus promotion agency in 2011.
A 22-year military veteran, he also worked for several marketing and advertising firms, worked in marketing for Pizza Inn and for Metromedia Restaurant Group, which operates as Bennigan's.