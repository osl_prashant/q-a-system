Pasture rent is an expense most cattle producers negotiate on a yearly basis and it can fluctuate based on competition from other grazers. This year cash rent pasture rates averaged $12.50/acre nationally, down $0.50/acre from last year and $1.50/acre lower than 2015. Rental rates in 2013-14 stayed at $12/acre and in 2012 were down to $11.50/acre, according to U.S. Department of Agriculture data.
Still, it begs the question where can a cattle producer or livestock grazer find the best deal on grazing? And where is land in the highest demand for grazing?
The most expensive county to rent pasture in 2017 is Snohomish County, Washington where reported cash rents were $128/acre. The cheapest county to rent pasture in this year is Culberson County, Texas where reported cash rents were $0.60/acre.
The only other county with $100/acre or higher rent was Chester County, Pennsylvania at $113/acre. Of the 50 highest priced pasture rent counties in the country, Iowa has 39% of the counties in the top 50.
Forty-nine of the 50 lowest pasture cash rent counties are located in western states with rocky or desert terrain like Texas, New Mexico, Colorado, Arizona, Wyoming and Utah, with prices being $3/acre or lower. Lewis County, West Virginia was the only county east of the Mississippi River to rank in the top 50 lowest, tied with Glasscock County, Texas and Weston County, Wyoming.
Below is an interactive map of the U.S. showing pasture rents by county and a run down on the lowest and highest states to rent pasture.
Pasture Cash Rents, by County
.embed-container {position: relative; padding-bottom: 67%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}


Click a county to see the cash rent for 2017 in that location. (Christopher Walljasper/Farm Journal Media)
10 States with Highest Pasture Rent Price

State      Pasture Cash Rent ($/acre)
Iowa                     $54
Pennsylvania       $44
Maryland             $40
Wisconsin            $40
Indiana                 $39
Illinois                  $38
New Jersey         $36
Missouri               $31
Minnesota           $30
Georgia                $29

10 States with Lowest Pasture Rent Price
State      Pasture Cash Rent ($/acre)

Arizona                $2.3
New Mexico       $3.2
Utah                    $4.8
Wyoming             $4.8
Colorado             $5.6
Montana             $6.3
Texas                    $6.6
Washington        $8
Oregon                $11
California            $12 (tie)
Hawaii                  $12 (tie)
Idaho                    $12 (tie)

Both cropland and pasture cash rents are surveyed by USDA’s National Agricultural Statistics Service. For more information on the program go to the USDA’s website and to see how pasture rent compares to cropland rent read the following story from AgWeb.