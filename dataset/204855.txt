Click here for the Ag Pro podcast
Jean Payne, Bloomington, Illinois
President of Illinois Fertilizer and Chemical Association (IFCA), and MAGIE Show
A: "There's a downturn in equipment sales no matter whether it's tractors, sprayers or toolbars. And you think, oh boy, maybe MAGIE will be a little off
this year, but we had a re-cord number of exhibitors. Talking to the exhibitors, even in a downturn, there are still opportunities. Maybe their customers won't buy something this year, but all know we bounce out of these things. IFCA prides itself in being proactive for our industry: influencing policies, promoting stewardship and sponsoring events like the MAGIE show."

Tony Orlando, Boone, Iowa
President of Firestone Ag Tires
A: "Certainly it's a very challenging marketplace. We are in year three of an industry downturn‚Äî expected to be in year four‚Äîprimarily driven by low crop prices. There's been a real compression in large equipment sales. We continue to need to look at our cost structure and make sure we're efficient in spending. But we will continue to have innovative products coming out‚Äîit's the lifeblood of Firestone Ag. We are certainly looking at other ways to go to market, making sure we're really connected with the dealers and the OEMs, and then making sure we meet the needs of the American farmer."

Craig Miller, Jackson, Minnesota
AGCO Product Marketing Specialist, Application Equipment
A: "We would love to see the industry go back up. But retailers still understand there's a job to be done. There's still somebody asking them to put down that fertilizer, to spray that chemical. So, yes, they are still making that commitment. Now by the same token, some in the industry have pulled back. Instead of rolling their machines every year, maybe they're now rolling every two years. And that's where some of that downturn comes from, but at the same time, that's also where stability comes from in the industry. That well will not completely dry up."

This article appeared in the October issue of Ag Pro magazine
Click here to subscribe to Ag Pro.