This is a classic “good news-bad news” story.The good news is that according to a recent study conducted at Kansas State University, the use of black pepper “helps limit the formation of cancerous compounds in grilled meats.”
Obviously, I’m happy that black pepper, which I use liberally on grilled meats, has a positive impact on health, as well as taste.
But did the lead of the article in the Topeka Capital-Journal have to mention the c-word?
Both the Health and Human Services Department’s National Toxicology Program and the UN’s World Health Organization recognize that open-flame cooking can cause formation of heterocyclic amines (HCAs), which an even larger scientific consensus agrees are carcinogenic.
That’s the bad news.
As a result, both industry associations and consumer groups have urged people to turn down the heat on their barbecue grills and avoid letting dripping grease cause fiery flare-ups. Some health authorities have even gone so far as to advise consumers to switch to other methods of cooking their meats.
That ain’t gonna happen on the Murphy ranch.
But now there appears to be a way to mitigate the potential damage caused by HCAs.
The Spice is RightJ. Scott Smith, KSU professor of Animal Science and chairman of the university’s Food Science Graduate Program, found that black pepper nearly eliminates the formation of HCAs on the surface of cooked meat.
Smith mixed 1 gram of finely ground black pepper with 100 grams of ground beef. That formulation worked well at inhibiting HCA formation, he said in a statement, but the pepper flavor was “too strong to be pleasant.”
I wish the good professor had let me be the judge of what’s “pleasant.”
Instead, he blended the pepper with other spices, such as garlic and oregano, to make the cooked product more palatable yet equally protected against HCA formation.
“Blending pepper with antioxidant-rich spices works so well in ground beef patties and on steaks that the spice formulation eliminates nearly 100% of HCAs,” Smith said. “In these cases, the spices are added at a level that is quite practical, so the result is flavorful and healthy.”
The article noted that Smith’s research determined that marinades and other herbs also effectively limit HCAs. Even store-bought marinades can reduce HCAs to nearly zero, Smith said, although marinating meats too long can negate the positive effect.
“Some people might think that … a lot of marinating time would do a lot of good,” Smith said, “but marinating too long can cause the antioxidants in the sauce to decompose. Just a couple of hours is an ideal time.”
Much of Smith’s research focuses on the effect of antioxidant-rich spices, including many from the mint family, such as rosemary, thyme, oregano, basil, sage and marjoram, and from the myrtle family, which includes cloves and allspice.
Personally, I’ll stick with a salt-and-pepper formulation, although a little garlic doesn’t hurt, either.
In the end, though, there will be plenty of “official” advice coming from all quarters this summer about lowering the temperature on one’s outdoor grill. If the meat doesn’t get burned or blackened, there is less formation of HCAs, and thus less danger from eating grilled items.
So how hot is too hot?
Smith said that HCAs start forming at about 300 degrees F and increasingly develop at temperatures above 350 F. Problem is, keeping the flame or the heat too low doesn’t develop the crisp, caramelized “crunch” that makes grilled meats taste so darn good.
There is a compromise: Quickly sear the meat or poultry at high heat on both sides, then slowly raise the internal temperature with lower temperature cooking. That’s the way high-priced steakhouses do it: Sear the steak on both sides over a hot flame, then oven-finish to the desired doneness.
Which, by the way, should never exceed medium rare.
In summary: Barbecue at a high enough heat to promote the formation of surface flavor compounds, but not to the point of burning and charring that can trigger the build-up of carcinogenic HCAs.
That way, you earn the right to wear that personalized “The Man | The Meat | The Legend” chef’s apron you might be fortunate enough to receive on Father’s Day.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.