New Zealand dairy group Fonterra said on Tuesday that its domestic milk output rose 3 percent in October from the year before as wet weather subsided towards the end of the month.
The surprise jump in production in the world’s top dairy exporter could drag on international dairy prices that have already been under downward pressure in recent months.
“Weather conditions improved towards the latter part of the month (pushing up production),” Fonterra said in a statement.
The start of October had seen heavy rain in many major dairy regions, leading observers to expect tighter milk production.
The increase in output could dampen prices at the Global Dairy Trade (GDT) auction due early on Wednesday.
“My expectation would be that it would have an effect on prices, we’ve got a GDT auction overnight so I would expect whole milk powder to come back (down) on that,” said Amy Castleton, an analyst at New Zealand’s AgriHQ.
However, derivatives markets were still pointing to a rise of around 1 percent in whole milk powder prices at Wednesday’s sale, according to some analysts.
Global dairy prices rose in the second quarter, but have since had a bumpy ride due to volatile international appetite, falling to a seven-month low at a recent auction.
Fonterra also said in its monthly update to the stock exchange that shipments to China continued to be strong, rising 34 percent in September from the same period last year.
Milk production at Fonterra’s smaller Australian operations in September fell 1 percent due to poor weather and reduced herd size.
Fonterra said total New Zealand dairy exports fell 7 percent in September, while Australian dairy shipments slipped 3 percent.
Shares of the dairy giant were trading up 0.5 percent at 0054 GMT, compared with a 0.5-percent decline in the broader market.