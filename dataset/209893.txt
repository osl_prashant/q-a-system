Twelve veterinary students receive 2017 AABP Foundation – Zoetis Veterinary Student Scholarships at the 50th Annual AABP Conference in Omaha, Nebraska. Front row (L-R): Dr. Roger Saltman (Zoetis), Lauren Thompson, Sarah Cook, Delaine Quaresma, Lora Gurley, Michelle Mitchell, Jonathan Lowe (Zoetis). Back row (L-R): Kevin Gavin, Max Beal, Cade Luckett, Ben Bennett, Joseph Hammes, Ethan McEnroe, Brian Schnell. Photo by Sam Albers.
Twelve fourth-year veterinary students from across the country were presented scholarships on Friday, Sept. 15 during the 50th Annual Conference of the American Association of Bovine Practitioners (AABP) in Omaha, Nebraska. Made possible by the annual AABP Foundation – Zoetis Veterinary Student Scholarship program, each student received a $5,000 scholarship and a travel stipend to the conference. Since 2009, this scholarship program, a joint effort between the AABP Foundation and the Zoetis Commitment to Veterinarians™ initiative, has helped encourage and support careers in large-animal veterinary medicine.
2017 AABP Foundation – Zoetis Veterinary Student Scholarship applicants entering their senior year of veterinary school were evaluated on academic performance, essay submission and involvement in bovine medicine and related extracurricular activities. The recipients are:

Maxwell Beal, Kansas State University
Ben Bennett, Kansas State University
Sarah Cook, Louisiana State University
Kevin Gavin, Washington State University
Lora Gurley, Michigan State University
Joseph Hammes, University of Minnesota
Cade Luckett, Texas A&M University
Ethan McEnroe, University of California, Davis
Michelle Mitchell, Purdue University
Delaine Quaresma, University of California, Davis
Brian Schnell, University of Wisconsin-Madison
Lauren Thompson, Texas A&M University

For Kevin Gavin, scholarship recipient and veterinary student at Washington State University, the scholarship means more than helping relieve financial pressure from costs of veterinary education.
“This scholarship will reduce the financial burden of my final year of veterinary school significantly, which will allow me to spend more time gaining as many valuable educational experiences as I can before graduation,” he said. “It is very important for students to see how veterinarians take scientific concepts that are researched and taught in universities and apply these concepts to real-world situations. The only way to do this is to spend time with veterinarians and on farms, allowing students to understand the concepts firsthand.”
Travel assistance to the annual AABP conference also is an important component of the scholarship program.
"We appreciate the collaboration between the AABP Foundation and Zoetis to provide significant scholarships to these future colleagues," said K. Fred Gingrich II, DVM, executive vice president of AABP. "In addition, travel stipends that are provided to offset the cost of attending the annual conference provide these students with the opportunity to network with AABP members and take part in our high-quality continuing education offerings."
Roger Saltman, DVM, MBA, group director of Cattle and Equine Technical Services at Zoetis, who has helped present the scholarships during the conference for the last seven years, said he sees this program continue to give back and strengthen the cattle industry.
“By investing in these students who are the future of veterinary medicine, these scholarships will help us realize the important goal of providing the best care possible for cattle,” Dr. Saltman said. “Support, not only through scholarships, but through research and development, philanthropy, mentorships and continuing education, helps to enhance educational experiences and, in turn, aids in ensuring a well-trained and economically viable veterinary profession.”
For more information about additional support for the veterinary industry through Commitment to Veterinarians, visit www.zoetisUS.com/c2v.