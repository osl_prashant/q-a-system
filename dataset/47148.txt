Crop calls
Corn: Fractionally to 1 cent lower
Soybeans: 2 to 4 cents lower
Wheat: Mixed
Corn and soybean futures were weaker overnight as the dollar returned above the 100-point level. Traders are largely focused on evening positions ahead of tomorrow's key USDA reports. Traders expect USDA to signal producers plan to increase soybean acres at the expense of corn acres and for quarterly stocks data to remind of ample supplies. Meanwhile, weekly soybean export sales topped expectations, while corn sales were a disappointment and wheat sales were at the top end of expectations. Also this morning, USDA announced China has purchased 165,000 MT of new-crop soybeans.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Cattle futures are called mixed amid position squaring following yesterday's firmer tone. This week's cash cattle trade is shaping up to be around $1 to $2 lower than last week, but futures have even more cash deterioration priced into the market. Meanwhile, price action in lean hog futures is expected to be choppy as traders even positions ahead of this afternoon's Hogs & Pigs Report, which traders expect to reflect expansion. The cash hog market is called steady to $1 lower.