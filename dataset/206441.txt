The bank of evidence supporting the virtues of bovine colostrum has grown even more with a new study performed by Chinese researchers.
Jim Quigley, calf researcher with Provimi North America, says colostrum’s virtue as the best way to start a young calf’s life and provide essential immunoglobulins has been supported by research for more than 100 years. Still, researchers continue to learn more about the mechanisms of colostrum, and why it is so valuable.
He points to research done at China’s Agricultural University where the researchers divided 24 calves into three treatment groups:
First-milk maternal colostrum – average IgG 70 g/liter (L);
Transition milk (collected on days 2 and 3 post-calving) – average IgG 39 g/L; and
Whole milk – average IgG 1 g/L.
All calves were fed 4 L of their treatment immediately after birth, and then 2 L at 8 hours after birth. On the second day, calves were fed 3.5 L of their respective treatments at 8 a.m. and again at 4 p.m. After that, all calves were managed normally and similarly. Among the results:
The calves in the colostrum and transition-milk groups gained weight in the first eight days of life, while the whole milk group had lost an average of 400 g (.88 lb.) of bodyweight per head by Day 8.
Five of the eight whole-milk-fed calves developed diarrhea in the first week of life.
Serum IgG levels at 24 hours were 24.56, 15.66 and 0.09 g/L for the colostrum, transition-milk and whole-milk groups, respectively.
Four calves in each group also were sacrificed at Day 8 to examine their intestinal tissue development. Calves fed colostrum had the greatest villus length and width and crypt depth. The villi are the finger-like projections in the intestinal wall that increase intestinal surface area and promote nutrient absorption in the digestive process.
“These findings indicate that colostrum contained more bioactive components to promote intestinal tissue growth,” Quigley says. “Theoretically, this would lead to greater ability of the calf to absorb nutrients from ingested feed.” 
Calves fed transitional milk also showed intestinal development, although not to the degree of the colostrum-fed calves. Whole-milk-fed calves actually showed degeneration of intestinal tissue.
“This study shows that the delivery of high-quality colostrum affects many different aspects of the calf’s physiology, including development of the digestive tract,” Quigley says. 
 
Note: This story appears in the May 2017 issue of Dairy Herd Management.