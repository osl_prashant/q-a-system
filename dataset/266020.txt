New Mexico state rep shaped by her farming, ranching roots
New Mexico state rep shaped by her farming, ranching roots

By TIMOTHY P. HOWSARERoswell Daily Record
The Associated Press

ROSWELL, N.M.




ROSWELL, N.M. (AP) — When someone asks Candy Spence-Ezzell, who represents District 58 in the New Mexico Legislature, where she grew up, her answer is typically "west of L.A."
If your first thought after reading that makes you think of Los Angeles and the Pacific Ocean, that somewhat-large body of water west of the sprawling metropolis, then you got the wrong interpretation.
By "west of L.A." Spence-Ezzell means "west of Lake Arthur," which is that little town in Chaves County not too far from Artesia.
Spence-Ezzell was born in Artesia and grew up in rural Chaves County in a farm community called Cottonwood.
She attended Cottonwood School, which taught pre-first grade through sixth grade. In seventh grade, she was bussed to Artesia Park Junior High School and graduated from Artesia High School.
Cottonwood School closed sometime in the late 1960s, she said.
"The school was the center of all activities," she said. "The only thing left now is the gymnasium."
Spence-Ezzell grew up on a farm, where she picked cotton and cut and baled hay.
She said the town kids loved to visit with their classmates on the farms, where they could see all the baby animals and do fun stuff like swim in the irrigation tanks.
On the other hand, she said, the farm kids envied the town kids because they could ride their bikes on paved streets and sidewalks instead of the rough dirt roads out in the country.
Decades later, Spence-Ezzell is still very much a farm girl and owns a 17,920-acre ranch in west Chaves County with her husband, T. Calder Ezzell, who serves on the Chaves County Council and is a law partner with the Hinkle-Shanor firm in Roswell. His specialty is oil and gas law.
T. Calder grew up in Paducah, Kentucky, and moved to Roswell in 1976.
The couple met in 1993 when they both participated in the pig show at the Eastern New Mexico State Fair.
"He was the preppy and I was the redneck," she said, emphasizing that she's proud to be a redneck.
Spence-Ezzell said she had broken her ankle a week before the fair and was on crutches.
She needed about six or seven men who could help her with the pig boards, which are pieces of plywood used to guide the pigs into their stalls.
"Calder came up and asked if he could help," she said.
The couple married in 1995. Spence-Ezzell has a son and daughter from a previous marriage and Calder has no children of his own.
Both Spence-Ezzell and Calder are active with the Eastern New Mexico State Fair and activities that involve youth with agriculture. Spence-Ezzell has taught gymnastics and coached Little League.
Both of her kids helped pay for their college educations with rodeo scholarships.
Spence-Ezzell and her three brothers are partners with their family farm near Artesia that was passed down from their parents. Her middle brother, Steve, manages the farm.
The Ezzell's ranch, called The Two C Slash Ranch, is a cow/calf operation and has no oil and gas leases to supplement the income from raising livestock.
In a bad year, cattle ranchers often have to sell off stock to stay afloat.
"We don't need to go to Las Vegas to gamble," she said. "We gamble every day. It's scary right now with no rain, the winds and the threat of wildfires."
Despite the challenges, Spence-Ezzell said she would never trade being a rancher for a cushy office job.
"My office is walking out the door and looking across the open land," she said.
The Ezzells own several horses that they keep on a horse farm owned by veterinarian Leonard Blach. If that name rings a bell, it's because Blach is a co-owner of Mine That Bird, the gelding who won the 2009 Kentucky Derby despite given 50-to-1 odds. The other owner is Mark Allen, who lives across the street from Blach.
The horses are being raised for breeding and two of the mares now have fillies that were born earlier this month. They also own three horses that are being trained at the Sunland Park racetrack near Las Cruces.
Spence-Ezzell was first elected to the New Mexico Legislature in 2004. She said she became interested in political office soon after someone in the local Republican Party asked her husband if he wanted to run.
Calder turned down the offer, Spence-Ezzell said, because he had a lot going on at that time with his job and other activities. After talking it over with her family, Spence-Ezell decided she would run.
Spence-Ezzell believes she brings a "common sense/real-world approach" perspective to the state capital, where elected officials from the big cities like Albuquerque, Santa Fe and Las Cruces have little or no understanding about agriculture.
"They think food comes from a grocery store," she said, meaning that most people don't consider where all that food is grown or raised before it gets to the grocery store.
While "book smarts" are important, Spence-Ezzell said, a well-rounded person also needs to have "street smarts."
Known for speaking her mind, at a recent service club meeting in Roswell the state representative/rancher said she always brings her dogs to live with her in a rented house during the legislative sessions.
"Sometimes they are the only ones who are glad to see me," she quipped.
Spence-Ezzell will run unopposed in both the primary and general elections.
Recalling the days of her youth, Spence-Ezzell questions whether we have progressed as a society.
"We didn't have money and grew up in a two-bedroom house, but it was a great experience," she said. "It was a simple way of life. We didn't have to go school and worry about getting shot at. Is it really a good thing when we don't know who our next door neighbors are?"
___
Information from: Roswell Daily Record, http://www.roswell-record.com