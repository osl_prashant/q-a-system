Dear Editor: 
Our letter is in response to the Produce Coalition for NAFTA’s opposition to perishable and seasonal dumping rules.
 
It is important for your readers to know that the perishable and seasonal dumping rule would not be operative unless it is determined by a federal agency that the crop is perishable, is seasonal, was dumped, and the U.S. seasonal crop industry was injured by the Mexican imported crop. 
 
The coalition’s concern about the Trump Administration “leveling the playing field” is an admission that they have or intend to dump their Mexican crops in the U.S.
 
Produce growers in California, Florida, Georgia, Michigan and Pennsylvania together with the American Farm Bureau Federation have sent letters to the Trump Administration recommending the modernization of perishable and seasonal unfair actions. 
 
It is wrong to penalize one geographical perishable and seasonal crop for dumping when another geographical area was responsible for the dumping and injury.
 
The proposed rule would limit the increased duty to a period of a few weeks instead of a year and longer.
 
Absent correcting the problem, U.S. produce growers will be restricted to growing in geographical areas that are not injured by dumped Mexican perishable and seasonal crops. 
 
It is important to recognize not all produce would qualify as perishable. 
 
The coalition’s members should devote their influence to determining what crops are perishable.
 
We agree strawberries, peaches and tomatoes are perishable, but are potatoes, onions or even apples perishable since there is atmospheric storage for these crops? 
 
For example, should Mexico dump sweet onions in the U.S. market, Washington Walla Walla onions could be held in storage until the Mexican onions are off the market. 
 
Whereas, if Mexican patented strawberries are dumped in the U.S. market, U.S. strawberry growers do not have the ability to hold the strawberries in cold storage. 
 
In essence, some crops have short marketing periods, such as tomatoes, while other crops have long marketing periods, such as apples and avocados.
 
In closing, we continue to hear that Mexico will retaliate should the U.S. succeed in modernizing NAFTA’s perishable and seasonal dumping and countervailing programs.
 
These very same organizations are lobbying to amend NAFTA for their own benefits without the fear of retaliation by Mexico. 
 
It is obvious the Produce Coalition for NAFTA is representing Mexico’s concerns and not those of U.S. perishable and seasonal growers.
 
Sincerely,
Blaine Carian
Chairman, Growing Coachella Valley