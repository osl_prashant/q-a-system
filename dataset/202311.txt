Billionaire printing magnate and Minnesota Timberwolves owner Glen Taylor is becoming involved in the food industry.
Taylor and a group of investors are spending millions of dollars to convert a former beef plant into a hog processing facility,Minnesota Public Radio reported. They plan to open Prime Pork in Windom, Minn. by January.
The plant will be a medium-sized operation that will process more than 6,000 hogs daily. The group hopes to tap into people's growing desire to know where and how their food is produced.
"The customer can know which farms the hogs came from," Taylor said. "How they're raised, what they're fed, how they're treated."
Plant Manager Wayne Kies said the operation won't accept hogs treated with a growth stimulant that's banned by China, a fast-growing pork market.
"We want to produce a quality, consistent product," Kies said.
The Food Marketing Institute report found that 43 percent of millennials distrust large food companies, more than double the rate of the rest of the population.
"I think that there is a demand and an expectation this day and age that I should be able to find out any information about anything that I want," said David Fikes with the Food Marketing Institute.