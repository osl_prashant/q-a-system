A very wet winter, weather-disrupted plantings and reduced acreage have put the head lettuce market in a fairly solid position, grower-shippers say.
“In general, the market this year has been a lot stronger than last year,” said Anthony Mazzuca, senior director of commodity management for Salinas, Calif.-based Tanimura & Antle.
Mark McBride, salesman for Salinas-based Coastline Family Farms, agreed with Mazzuca.
“From a standpoint of comparing prices to previous years, we’ve seen an improvement,” he said.
Henry Dill, sales manager for the Salinas, Calif.-based Pacific International Marketing Inc., said the market for head lettuce has been OK.
“Everything got off a little bit slow because the plantings were a little bit fragmented at the beginning,” he said.
“Because of that, the markets for lettuce items and leaf items have been active.”       
Mazzuca said weather in the northern part of the Salinas Valley had been cool and consistent, making for excellent quality.
“We’re about on our weekly production plan for this time of year, and we have not seen too many spikes,” he said.
Mazzuca wouldn’t characterize the markets as stellar, but he isn’t complaining.
“We saw lettuce top out at about $17.50,” he said.
“It has retreated a little bit, but it has sustained ‘in the black’ type levels, so everybody should be happy to maintain price levels near or around these levels year-round.”
On July 13, Dill said head lettuce was priced at $14-15 on iceberg, about $3-4 higher than a week ago at this time.
“Part of that is because we had warmer weather, and we had some stuff bunch up for a couple weeks,” he said.
“And now it created a lighter week in here as far as lettuce production.”
Dill said he suspected some growers did not get all of their plantings in because they couldn’t turn their ground around in Salinas.
“But now we’re facing pretty stiff competition from the Canadian producers and the Northeast producers,” he said.
“So now we’re pretty much relegated to shipping to the Western part of the U.S. and a little to the East Coast.”
McBride said the last couple of summers had been “pretty darn tough” as far as f.o.b.s.
“The biggest change in f.o.b.s has been in iceberg lettuce, and we’ve been fortunate enough in the last week or so to be in the mid- to high teens, and we didn’t come close to that last year at this time,” he said on July 13.      
McBride attributed the higher prices to reduced acreage, even with homegrown product factored in.
The U.S. Department of Agriculture reported July 21 prices for cartons of film-wrapped iceberg lettuce 24s from Salinas-Watsonville at $10-14.55, with 30s $9.45-12.55. Year-ago prices for cartons of film-wrapped 24s were $9-11.56.
 
Leaf lettuce
As could be expected, the wet weather earlier in the year also wreaked havoc with leaf lettuce.
“The plantings were so chopped up because of the rains we had during the critical planting time and farming time,” Dill said.
“It made for kind of a historic start for pricing on a lot of items as we got into the swing of the Salinas deal.”
“Because leaf lettuce is pretty commonplace these days, we haven’t seen the upsurge in prices as we have with head lettuce,” Dill said.
“Leaf lettuce is probably $7-8 for romaine and leaf. There are some people in the romaine heart deal ... and there are probably more romaine hearts planted right now than the industry can use, so it’s pretty (bad) as the price goes.”
The romaine market has been kind of on the depressed side, Mazzuca said.
“Markets have been $6.50-7.50 on the low end,” he said. “Stronger shippers out there, your top tier, are seeing $1-2 over the top end of that call.”
Prices for cartons of romaine lettuce 24s from Salinas-Watsonville July 21 were $5.95-9.55, according to USDA numbers. Cartons of 12 three-count packs of romaine hearts were $9.50-13.55.
“Green leaf and red leaf have stayed pretty stagnant,” Mazzuca said.
“The main reason is local plantings, and Tanimura & Antle purposely reduced their acreage this time of year to account for that.”