The busy holiday season is approaching.  You have so much to do beyond work that you may be wondering if you can just take a little “hiring holiday” over the next couple of months.
Do you really need to continue recruiting during November and December? YES! Your competitors are actively recruiting, and candidates are still searching. If you search for “holiday recruitment,” your results are likely to return with many seasonal and holiday staffing opportunities. People have the misconception that the only job searching and hiring taking place now is in the retail sector, but that’s not the case. 
Consider people’s hectic schedules of holiday parties, gatherings, school events, and preparations for the season. This eventful stretch provides an opportunity to evaluate candidates’ ability to juggle multiple responsibilities. Candidates that make time to search and apply during this season are likely to be dedicated, ready-to-work potential employees. AgCareers.com processed more than 7,000 applications in December 2016.     
What kind of jobs are applicants looking for?
Traditionally, in November and December, candidates’ most popular search terms on AgCareers.com:

“Internship” or “Intern”
“Sales”
“Agronomy” or “Agronomist”
“Cattle”
“Marketing”

Now is an ideal time to finalize student and intern hiring, and recruit salespeople to kick off revenue production for 2018.
Who’s hiring in agriculture now?
All organizations want to start off the New Year prepared with the right staff. Although the agriculture and food industry has seasonal staffing needs based on harvest and production, we still require talented people year-round.
Agricultural employers are continuing to search for the best talent. Competitors in most industry sectors were recruiting last December. Businesses in food products, equipment, animal and crop production, and commodities were top hirers. A recruitment vacation right now may mean foregoing top talent.
The top 5 career types for job openings during December were:

Farm & Ranch Operations/Herdsperson/On Farm
Sales/Retail
Accounting/Finance/Asset Management
Maintenance/Repair
Operations

This season, at the very least, update and simplify your job descriptions (see When Help Wanted Ads Need Help for tips on writing a good on-line job description). An effective job description is clear and concise. Formatting matters in a job posting and description, so make good use of bullets, paragraphs, and bolding. When candidates are already experiencing the joy and frenzy of the season, the simpler, more concise job postings are more likely to catch their attention.
Also, be considerate of holidays when you are following up with candidates. Response times and scheduling interviews may be a bit challenging during the season, but it will be worth your time to keep recruitment efforts on target to start 2018 off on the right foot.   
Editor’s Note: Bonnie Johnson is a marketing associate with AgCareers.com.. Continue your hiring over the holidays at www.AgCareers.com.