Is chronic wasting disease (CWD) a potential time bomb for the agriculture industry? A silent killer stalking deer and elk, CWD continues to move quietly across the U.S., with 22 states currently reporting CWD presence in free-ranging cervids.
CWD is a neural malady with devastating final-stage symptoms akin to mad cow disease. There is no direct proof of transmission via contaminated grain or feedstuffs, but researchers say accumulating evidence warrants a closer look at the possibility for potential disruption of multiple facets of the agriculture industry.
CWD is a guarantee of protracted death. Without exception, host deer slowly enter a zombie-like state characterized by extreme thirst, lack of mobility, loss of balance and near-total disorientation. CWD plays by a unique set of disease rules and does not spread by the common modes of infection (bacteria or virus), but is transmitted by a corrupted prion, essentially a misfolded protein. The corrupted prions multiply until the infection reaches the brain, destroying cells and attacking the central nervous system. Brain cells die in response to the presence of prions, creating holes in the brain.
“It’s a bad way to die. The animals are emaciated and waste away, and ultimately die from aspiration pneumonia because the swallowing reflex is affected and saliva gets into the lungs, or they freeze to death because they have no more body fat,” says Tracy Nichols, a molecular biologist and staff officer with USDA’s Veterinary Services Cervid Health Program in Fort Collins, Co.
On average whitetail deer have an approximate two-year CWD incubation period from infection to death. Although deer appear and act healthy for the vast majority of the disease duration, they can transmit CWD during nearly the entire span. (It is detectable in bodily fluids from infected animals as early as three months.) Deer directly contract CWD through infectious saliva, feces and urine, or indirectly via soil ingestion, foraging or a water source contaminated with prion proteins.
After initial detection in 1967 at a Colorado captive mule deer facility, CWD spread with a slow, but sure pace. Fifty years later, CWD has infected cervid populations in 181 counties across 22 states, according to the Centers for Disease Control and Prevention (CDC).
The geographic footprint of CWD continues to swell, likely through two different functions. One, slow diffusion from deer to deer in the wild. Two, faster movement through human-assisted spread by captive cervid farms or hunter-transported carcasses.






Deer appear and act healthy for the vast majority of CWD duration, but can transmit the disease during nearly the entire span.
© HERBERT LANGE, WDNR






There are no known cases of natural transmission of CWD to domestic livestock. However, in laboratory conditions, CWD has been reproduced experimentally in cattle and swine, and research studies continue to examine crossover possibilities. There is no evidence of human crossover of CWD, although a 2017 studydocumented oral transmission of CWD to macaque monkeys. (Conducted in Canada, the study awaits peer review and publication.) Since 1997, the World Health Organization (WHO) has called for the exclusion of all prion disease material from the human and animal food chains.
“Mad cow disease crossed the species barrier to humans, but we haven’t seen evidence of CWD crossover. We can’t say crossover won’t happen in time, and as a precautionary measure WHO doesn’t want infected prions in the human or animal food chains,” says Bryan Richards, chronic wasting disease project leader for the U.S. Geological Survey (USGS) National Wildlife Health Center in Madison, Wis.
The possibility of CWD transmission beyond cervid populations remains an unanswered question, partly because different strains of CWD behave distinctly. “There is so much about CWD transmission we don’t know and so much yet to be studied. There are so many CWD unknowns related to brain matter, infectivity, prion stage, lymph nodes and more,” says David Clausen, a retired Wisconsin veterinarian, farmer, and former chair of the Wisconsin Natural Resources Board. Clausen presently serves as board president of Midwest Environmental Advocates.
The infectivity range of CWD is another significant unknown. When one species door is closed, another may serve as a Trojan horse. Hamsters were originally thought to be immune to CWD, while ferrets were easily infected. “Take deer CWD and infect the ferret. Take the ferret CWD and infect the hamster. Boom. The hamster can’t be infected by the deer, but is susceptible to the ferret. These prions are constantly evolving and changing,” Clausen explains.
“The question is sitting there: If CWD is able to get into livestock or swine, could it be transmitted in feedstuffs?” he adds.
Echoing Clausen’s query, is CWD passed along in agricultural plant matter exposed to urine, feces or carcass material? Prions form a chemical bond with plant surfaces and also bind to some soil particles, according to Richards. “After dipping plant leaves in prion proteins, researchers haven’t been able to wash off the prions. There isn’t enough science yet, but it’s been shown that plants can pick up proteins in the soil matrix through their roots and deposit those proteins in shoots and leaves, likely in flowers as well,” he explains. “Is the infectivity still there and is the concentration enough to transmit disease?”






CWD has infected cervid populations in 181 counties across 22 states, according to the Centers for Disease Control and Prevention.
© USGS






Uptake in a laboratory isn’t real-world confirmation, but Richards finds the implications disturbing. In addition, CWD expansion across the U.S. compounds the infectivity issue. “With the geographic footprint of CWD greatly expanded, we have to ask if plant material really moves infectious material. We have to contemplate the question because agriculture would be such a huge mechanism for disease movement,” he says.
In theory, take a county with a 40% CWD rate in a whitetail population. If 20 whitetail typically feed in-and-out of a given wheat field, eight of those deer are CWD-positive. An adult deer defecates roughly 12 times per day and urinates at a higher frequency. After harvest, if the wheat straw is rolled, statistical probability places a good deal of fecal material in the bales. “It’s not a far-fetched idea for infectious material to move in agricultural commodities,” Richards says. “This is not really on the ag industry radar, but it comes up regularly in conversations among scientists. We don’t know if transmission happens, but it must be kept on the radar screen.”
“There was once a time when people said mad cow disease could never cross the species barrier. Almost 300 human deaths and an industry disaster later, it’s pretty simple to see why we keep on conducting the science and relying strictly on what the evidence shows,” he adds.
Nichols cites several studies where plant roots grown in hydroponic or spiked soil solutions were exposed to CWD material. The stem and leaf tissue subsequently tested positive for infected prions, despite no contact with the CWD material. However, high levels of laboratory exposure don’t necessarily equate with field environments. “The results are disconcerting, but what really matters is what happens in the real world. The next level of inquiry is to find out if there is any threat via agriculture crops,” Nichols says.
“There is no scientific conclusion, but the questions raised could impact international trade. Right now, we don’t know what, if any, threat exists,” she continues.
Clausen takes a dim view of CWD containment and believes the disease will continue to creep into additional states: “I’m a country boy. Basic epidemiology of disease control is to contain and quarantine. Deer farm or deer carcass, we have to prevent the movement of all CWD material, dead or alive.”






“With the geographic footprint of CWD greatly expanded, we have to ask if plant material really moves infectious material. We have to contemplate the question because agriculture would be such a huge mechanism for disease movement,” says Bryan Richards, USGS.
© HERBERT LANGE, WDNR






It is incumbent on the agriculture industry to consider the ramifications of CWD spread through agricultural commodities, according to Clausen. “Sometimes we just seem to bounce from crisis to crisis without enough preparation or foresight. Corn or alfalfa, CWD uptake has been demonstrated in plants. Whether the uptake is infectious, that research hasn’t been done.”
“Everyone at least needs to wake up to the potential of CWD to move in crops because it would shake up agriculture as we know it,” Clausen says. “We’re all responsible to take a precautionary approach and consider what could happen.”