Kern County may soon top the California agricultural production leaderboard.The value of crops there was nearly $7.2 billion in 2016, a 6% increase from 2015, according the annual report the county recently released.
Fresno County has logged a total value of nearly $6.2 billion, a 7% decrease from the previous year.
The total for Tulare County has not been released, but in 2015 it was nearly $7 billion, down 13% from the previous year due to the falling value of milk, its top commodity.
For Kern County, grapes, almonds, citrus, pistachios and milk were the top five crops. Eight of its most valuable 10 are produce.
Pistachios vaulted from seventh on the list to fourth, with the value skyrocketing from about $245 million in 2015 to about $769 million in 2016.
Cherries also rose in prominence, from the 19th spot at about $42 million to the ninth spot at about $106 million.
The value of the blueberry crop nearly doubled, from $17 million in 2015 to $33 million in 2016.
The top 10 crops for Kern County are:
grapes, $1.66 billion, up from $1.64 billion
almonds, including byproducts, $1.3 billion, down from $1.48 billion
citrus, fresh and processed, $824 million, down from $928 million
pistachios, $769 million, up from $245 million
milk, marketing and manufacturing, $580 million, down from $595 million
carrots, fresh and processed, $439 million, up from $299 million
cattle and calves, $309 million, down from $356 million
potatoes, fresh and processed, $110 million, up from $82 million
cherries, $106 million, up from $42 million
pomegranates, fresh and processed, $103 million, down from $191 million