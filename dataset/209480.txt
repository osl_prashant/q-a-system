Dairy farmers in various parts of Wisconsin experienced severe alfalfa winterkill and injury this past winter. Losses were the greatest on heavy soil types with poorer drainage. 
“It has been a long time since we’ve seen this degree of alfalfa loss in northeast Wisconsin,” said Mike Ballweg, University of Wisconsin-Extension Fond du Lac County crops and soils agent. “Many farms are reporting losses as high as 70 percent.”
 
Because of these losses, there may be an interest in selling hay from counties with surplus forage inventories to areas of the state in need of forages. The Farmer to Farmer website http://farmertofarmer.uwex.edu can facilitate that process.
 
Ballweg said, “The Farmer to Farmer Forage and Corn Website is probably best thought of as an electronic neighborhood bulletin board which allows local farmers to get in touch with one another.”
 
UW-Extension developed and supports the website that facilitates the local marketing of feed commodities where livestock producers in need of hay, haylage, straw, high moisture corn, corn silage or corn grain can easily make contact with sellers. All transactions and negotiations are handled directly between buyers and sellers.
 
The Farmer to Farmer Forage and Corn list is free of charge for both buyers and sellers. Users can search for, or list for sale, hay, haylage, straw, high moisture corn, corn silage or corn grain. Buyers can search for farmers in just one Wisconsin county or in any number of counties at once.
 
“This site has been an excellent way for buyers and sellers to get in-touch locally,” Ballweg said. “Neighbors often within short distances have been able to buy and sell as a result of the website.”
 
People who wish to use this service but do not have access to the Internet can get access and assistance at their local county UW-Extension office.