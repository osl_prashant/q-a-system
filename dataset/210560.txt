It’s still summer, but now is the time to be thinking about stockpiling forage for the winter, said Dirk Philipp, associate professor-animal science, for the University of Arkansas System Division of Agriculture. Stockpiling is the practice of accumulating forage growth for later use, he said.
“Stockpiling is done later in the season to take advantage of cooler days to reduce fiber accumulation and promote leaf growth,” Philipp said. ”Stockpiling can also save money from not having to harvest hay but let cattle graze standing forage instead.”
Philipp said the first step is to select a field for stockpiling and remove the existing stubble height to 3-4 inches.
“This can be done with either grazing or haying at the appropriate times so that you can start out with that canopy height,” he said.
However, “if you experienced a very hot summer, it is possible that the fescue is brown and dormant,” he said. “In this case, it is possible to bush hog the stand to open up the canopy for growth and cut down on dead leaf material, although grazing or haying is preferred.” 
Philipp recommends fertilizing with 60 pounds of nitrogen per acre on or near Sept. 1.
“Don’t wait for rain,” he said. “Once fertilized defer grazing until November mid-November.”
He also recommends soil sampling on a regular basis to get a handle on what nutrients are missing or in only limited quantities in the soil. 
Grazing management
Once November rolls around and the stockpile is ready to be grazed, Philipp recommends using strip grazing to help stretch the forage to February.
“Run a poly wire across the stockpiled field so that animals have access to just two to three days of forage at a time,” he said. “This way you make the most efficient use of the stockpiled fescue without wasting it.”
For more information about livestock and forage management, contact your county extension agent or visit www.uaex.edu.