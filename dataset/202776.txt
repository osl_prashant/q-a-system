R채 Nanoshoots, sprouts from R채 Foods LLC, are now available in 120 Vons stores.
The sprouts are already available at Stater Brothers, Save Mart and Lucky stores in the area, according to a news release from the Sacramento, Calif., company.

The R채 shoots are grown in their own packaging through a patent pending process, which the company says eliminates handling which causes spoilage. The sprouts are untouched until consumers open the package.

"It is a real food-safety breakthrough," Dan Sholl, R채 Foods general manager, said in a news release," and satisfies pent-up demand that has existed since traditional sprouts began to disappear from the shelves."
The product was launched in November, and is in clover-alfalfa, spicy blend, 100% clover and clover-broccoli.