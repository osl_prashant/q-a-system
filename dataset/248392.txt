AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:15 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
UPCOMING TOMORROW:
COLORADO CAUCUSES
DENVER — Colorado's Democratic Party releases results of a preference poll for its candidates in the 2018 governor's race from Tuesday night caucuses. The results may assign delegates leading up to the party's state assembly in April but are not binding. State Republicans won't release a poll.
IN BRIEF TODAY:
— CEREMONIAL FIRES — The city of Boulder reached an agreement with members of the local Native American community to allow for ceremonial fires to be lit and exempted from city enforcement.
— COLORADO INMATES ESCAPE — One of two inmates who escaped from a jail in southern Colorado has been found.
— DNA TESTING-DOGS — A Colorado town is considering DNA testing more than 50 canines living in town-owned apartments in an effort to solve a longstanding problem of pet owners either failing or refusing to clean up after their dogs.
— COLORADO CAUCUSES — Colorado voters are taking the first step to decide which major party candidates get to the June 26 primaries. Republicans and Democrats gather at caucuses across the state Tuesday night.
— MOTHER KILLED — Prosecutors allege that a Colorado man being re-tried for killing his wife in 2001 shot her in the head while she slept and put her body in a dumpster at work.
— DRILLING RIG-EXPLOSION-OKLAHOMA — A wrongful death lawsuit has been filed over a natural gas rig explosion in southeastern Oklahoma that killed five men — three from Oklahoma, one from Texas and one from Colorado.
— POLICE SHOOTING-FARMINGTON — New Mexico authorities say a police officer fatally shot a knife-wielding Colorado man when he charged officers in a Farmington motel room following a standoff.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for March fell .25 cent at 5.02 a bushel; March corn was up 1.50 cents at 3.7975 a bushel; March oats was off 2.75 cents at $2.62 a bushel; while March soybeans lost 2.25 cents at $10.6450 a bushel.
SPORTS:
AVALANCHE-BLACKHAWKS
CHICAGO — The Colorado Avalanche play the Blackhawks in Chicago. (Game starts at 6:30 p.m. MT)
NUGGETS-MAVERICKS
DALLAS — The Denver Nuggets play the Mavericks in Dallas. (Game starts at 6:30 p.m. MT)
PAC-12 TOURNAMENT
LAS VEGAS — Arizona's season started under a dark cloud following the arrest of assistant coach Emanuel Richardson as part of a federal probe into shady recruiting practices. Wildcats coach Sean Miller found himself in the crosshairs late in the season when an ESPN report alleged he was caught on an FBI wiretap discussing a $100,000 payment to lure top recruit Deandre Ayton to the school. By John Marshall. SENT: 790 words, photos.
BKC-ALL PAC-12
Arizona freshman Deandre Ayton has been named player and newcomer of the year on the All-Pac-12 Conference men's basketball team announced Tuesday by The Associated Press. Utah coach Larry Krystkowiak was named coach of the year. Ayton was one of the nation's top incoming recruits when he arrived in Tucson and lived up to expectations during a stellar freshman season with the 15th-ranked Wildcats. By John Marshall. SENT: 500 words, photos.
SPORTS IN BRIEF:
— SWM-ALEXANDROV-DOPING — Mike Alexandrov, a former Bulgarian Olympian and U.S. national team member, has accepted a one-year doping ban.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.