Ranchers placed 16.1 percent more cattle in U.S. feedlots last month than in June 2016, making it the most for the month in more than a decade, the U.S. Department of Agriculture reported on Friday.Good profits for feedlots in June allowed them to buy more calves for fattening for sale to packing plants, said analysts.
They said an increasing number of cattle in parts of the northern Plains were entering feedyards as drought grips the region.
On Monday, Chicago Mercantile Exchange live cattle futures deferred contracts may open lower - largely based on Friday's report on placement outcome, the analysts said.
USDA's report showed June placements at 1.770 million head, up from 1.525 million a year earlier and above the average forecast of 1.618 million. It was the most for the month since 1.95 million in 2006.
The government put the feedlot cattle supply as of July 1 at 10.821 million head, up 4.5 percent from 10.356 million a year ago. Analysts, on average, forecast a 2.9 percent gain.
USDA said the number of cattle sold to packers, or marketings, were up 4.0 percent in June from a year ago, to 1.989 million head.
Analysts had projected an increase of 4.7 percent from 1.912 million last year.
Worsening drought in the upper Plains resulted in more cattle taken off grazing land, contributing to larger-than-anticipated June placements, said U.S. Commodities analyst Don Roose.
He said ramped up beef demand was needed to compensate for increased cattle supplies ahead.
Livestock Marketing Information Center (LMIC) director Jim Robb attributed June's cattle placement surge to feedlot profitability, which encouraged them to pull in more animals from a broad region of the country - including areas stricken by drought.
LMIC calculated that feedlots in June, on average, made a profit of $210 per steer sold to meat companies versus a roughly $80 loss a year earlier.
On Friday the government simultaneously issued the semi-annual cattle inventory report. It showed the July 1 U.S. cattle herd at 102.6 million head, up from 98.2 million two years ago.
USDA did not publish the July 2016 cattle inventory data due to budgetary issues.
The report suggests the herd continues to expand, said Robb. He added that the second half of 2017 would be more critical than the first half, in terms of the rate of expansion, because of the drought and more heifers entering feedlots.