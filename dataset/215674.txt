Legion Logistics will be able to provide customers real-time information about their shipments thanks to new geo-location service LegionTrack.
The service uses the cell phones of drivers to locate loads. It is free for drivers to use, does not require them to download an app, and stops tracking them once a load has been delivered.
LegionTrack is available for drivers now, and customer service portal LegionEMS will launch in January.
“This innovation allows us to provide customer service on par with much larger companies,” CEO Antony Coutsoftides said in a news release. “Now we can locate our drivers at any time without them having to take their eyes off the road or interrupting their sleep.
“We understand that technology, tracking and freight visibility are the future of logistics,” Coutsoftides said. “Therefore, we have focused intently on developing, launching and upgrading our technology from day one.”
Both the portal and the tracking system were created in-house.