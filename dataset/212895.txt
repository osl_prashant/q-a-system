Abundant rain in California’s Salinas Valley and a heat wave in the Arizona desert have drastically cut supplies, resulting in record prices in the iceberg lettuce market, and prices are expected to be high into June.Grower-shippers are reporting record prices after a winter of heavy rains in California wreaked havoc on planting schedules, while warmer-than-usual temperatures pushed the lettuce deals in Yuma, Ariz., and Huron, Calif., to early finishes. A resulting supply gap has prices rising, grower-shippers said.
“There’s a whole generation out there that haven’t seen rains like we’ve had this winter,” said Mark McBride, salesman with Coastline Family Farms in Salinas, Calif.
The price spike is evident in the latest market report from the U.S. Department of Agriculture. As of April 25, cartons of iceberg lettuce from California’s Watsonville-Salinas District were $48.50-$52.55 for film-lined 24s; $49.50-53.55 for 24s film-wrapped; and $44.50-48.56 for 30s film-wrapped, according to the USDA.
A year earlier, the same product was $10.50-13.55 for film-lined 24s; $11.50-14.55 for 24s film-wrapped; and $9.45-12.55 for 30s film-wrapped.
Robbie Coyle, another salesman at Coastline, said this is a new experience for him.
“I’ve been doing this 30 years and never saw $50 lettuce,” Coyle said.
McBride said the phenomenon is the latest fallout of an unusual combination of weather events in the winter and early-spring growing regions.
“Where it started, at least, was the heat spell we had in the desert in late February and early March that helped bring the tail end to that deal to a rapid close,” McBride said. “You combine that with 193% of normal rainfall California had, plus we hadn’t had a winter like that in 15 years.”
The rain disrupted planting schedules and growth cycles across the Salinas Valley, McBride said. From December and into the spring, a lot of fields were not “farmed normally,” he said, leading to smaller sizes and lower yields.
“It’s history-making that we’ve had as wide a range of commodities that have all had supply issues, and have had some incredibly high markets for a long time.”
Cauliflower, broccoli and romaine lettuce also have dealt with weather problems, and now, it’s iceberg’s turn, McBride said.
“Our production people are pretty knowledgeable, and they see issues into June,” McBride said. “There’s gonna be some highs and lows.”
Salinas-based D’Arrigo Bros. Co. of California transitioned to Salinas production 10-14 days earlier than some grower-shippers, and isn’t facing the extreme supply dearth some growers are now dealing with, said Grag Heinz, salesman.
But D’Arrigo still is feeling pressure.
“Sounds like the main challenge, for a lot of guys as they moved over from Huron, was a lot of stuff was not ready to go, and the value-added guys are trying to source from the commodity guys, too,” Heinz said.
Labor shortages have compounded problems, Heinz said.
“A lot of guys are 24-30% down on needed labor,” he said.
The market should resemble “a kind of rollercoaster,” perhaps into June, Heinz said.
“You see strong demand through Mother’s Day,” he said.
Joe Ange, purchasing director with Salinas-based Markon Cooperative Inc., said prices on iceberg continue to inch upward.
“Fifty-dollar iceberg lettuce is something we’ve never seen before,” Ange said. 
Supplies shortage should ease in “the next couple of weeks,” Ange said April 25. However, he expects an erratic market all the way through the end of the Salinas deal at the end of October or early November.
Summer crops across the country will help ease pressure on the iceberg market, but some challenges will persist, Ange said.
“We’ll definitely see the market come down from where it is now, but we don’t think we’re going to see a flat, steady market,” he said. “We’ll see undulations in the market throughout the Salinas season.”
Huron was able to provide needed supplies, but only temporarily, Ange said.
Now, that’s gone, he said.
“Now that Huron is finished, all of that iceberg demand is solely on one area — the Salinas Valley,” Ange said.
Fields in Salinas are being harvested slightly early, so yields and weight are lower than normal, said Martin Jefferson, production manager in Salinas for Oviedo, Fla.-based Duda Farm Fresh Foods.
“Iceberg supplies still scattered and light, as processors are now pulling from available volumes,” he said. “With light lettuce, the processors need more acres to make up the volume, and this creates pressure between the fresh market and processors. Pricing continues to remain very strong for now and volume will be light for the next two weeks.”