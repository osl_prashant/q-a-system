BC-Wheat KX
BC-Wheat KX

The Associated Press

CHICAGO




CHICAGO (AP) — Winter Wheat futures on the Chicago Board of Trade Mon:
OpenHighLowSettleChg.WHEAT5,000 bu minimum; cents per bushelMar506512¾506509+1May521525513522¼+1¾Jul537¼541¼529¾539+1¾Sep553¾558547556+1¾Dec572577566¼575+1¾Mar579585¾576½585+2½May584½588¼579¼586½+2½Jul578½583¼576¼582½+4¼Sep583¾590¾583¾590¾+5¾Dec598¾603½598¾603½+3¾Mar608+3¾May608+3¾Jul590+3¾Est. sales 34,081.  Fri.'s sales 58,571Fri.'s open int 293,138