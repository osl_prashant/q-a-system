South Dakota ranchers say drought aid data is inaccurate
South Dakota ranchers say drought aid data is inaccurate

The Associated Press

RAPID CITY, S.D.




RAPID CITY, S.D. (AP) — Some ranchers in western South Dakota say data that determines their eligibility for federal drought aid is inaccurate.
The Rapid City Journal reports that local ranchers raised the issue with Republican U.S. Sen. John Thune of South Dakota during a Wednesday discussion over a federal farm bill draft.
Rancher Linda Gilbert says her insurance benefits during a drought year depend partly on conditions more than 50 miles away at weather stations operated by the National Oceanic and Atmospheric Administration. She says the federal Pasture, Rangeland, Forage insurance program doesn't accept data from weather stations closer to her.
Thune says the situation is an example of federal bureaucracies operating "multiple, duplicative and redundant systems."
He says he'll find a more comprehensive approach.
___
Information from: Rapid City Journal, http://www.rapidcityjournal.com