As long as antibiotic use continues in humans or animals, pathogens will develop resistance. But as public pressures to fight the emergence of antibiotic-resistant pathogens and preserve the efficacy of existing products, animal agriculture will need to deploy alternatives, particularly for growth promotion and disease prevention.Alltech scientist Richard Murphy, PhD, made those points and others in a recent webinar titled "The path of least resistance."
Murphy notes that according to a study from Princeton University, global use of antibiotics is poised to increase by 60% by 2030, with much of that increase occurring in developing countries. Disease attributed to antibiotic resistance could reduce global gross domestic product (GDP) production by $100 trillion by 2050.
The "golden age" of antibiotic research and development occurred during the 1950s and 1960s, Murphy says. Today, research into new antibiotics has declined as funding focuses on high-profile health challenges such as heart disease and cancer. The prevalence of antibiotic-resistant pathogens meanwhile, continues to grow.
Murphy says antibiotic resistance originates and persists through several processes including:
Spontaneous mutation.
Genetic exchange.
Selection pressure.
Genetic linkage.
Cross resistance.
The most contentious use of antibiotics in agriculture is that for performance or growth promotion. The mode of action for antimicrobial growth promotants (AGP) is not well understood, but probably involves a combination of several effects, Murphy says. Those include:
Disease-control effect.
Nutrient-sparing effect.
Metabolic effect.
Government regulations and market pressures likely will shut down the use of medically important antibiotics for growth promotion, and their uses for disease prevention also are likely to decline. Use of antibiotics for treatment of disease will continue for economic and animal-welfare reasons, but the industry needs to develop alternative strategies for enhancing animal performance and preventing disease. These could include:
Direct-fed microbials.
Enzymes.
Prebiotics.
Plant extracts.
Nutrient management programs.
These strategies generally focus on building the animal's ability to grow and to resist disease by enhancing rumen health and encouraging a favorable microbiome, or population of microbes, in the animal's digestive system.
We probably cannot stop antibiotic resistance entirely, but Murphy believes the industry needs to pursue alternatives for enhancing performance and preventing disease while preserving the use of existing products for treatment of disease.
The archived webinar is now available for viewing from Alltech.