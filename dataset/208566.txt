This market update is a PorkNetwork weekly column reporting trends in weaner pig prices and swine facility availability.  All information contained in this update is for the week ended July 21, 2017.
Looking at hog sales in January 2018 using February 2018 futures the weaner breakeven was $33.88, up $5.53 for the week. Feed costs were up $1.00 per head. February futures increased $0.80 compared to last week’s February futures used for the crush and historical basis is improved from last week by $2.38 per cwt. Breakeven prices are based on closing futures prices on July 21, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was below average last week with 20,060 head being reported which is 54% of the 52-week average. Cash prices were $24.98, down $2.53 from a week ago. The low to high range was $16.00 - $30.00. Formula priced weaners were up $1.36 this week at $37.26.
Cash-traded feeder pig reported volume was below average with 14,450 head reported. Cash feeder pig reported prices were $52.51, down $0.98 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through July 21, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential increased $8.06 this week to a projected gain of $8.90 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.