In Episode 5 of Silage Talk, the Dairy Herd Management team talks silage with Tony Hall, who works in Dairy Technical Services with Lallemand Animal Nutrition North America. The discussion covers mycotoxins and what we can do to increase yield and quality. 

Sponsored by Lallemand Animal Nutrition.