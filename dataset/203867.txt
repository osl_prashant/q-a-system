Oceanside Pole brand tomato volumes are peaking in August.
That sets up perfectly for late-summer vine-ripe and roma promotions, according to a news release from Vancouver, British Columbia-based The Oppenheimer Group, marketer of Oceanside Pole tomatoes, which are grown by Oceanside, Calif.-based West Coast Tomato Growers.
This year the Singh family celebrates 75 years of growing vine-ripes in Oceanside.
Quality this season is outstanding, said Mark Smith, Oppy's senior sales manager.
"This season has brought some of the best tomatoes we've seen."
Oceanside Pole tomatoes, so named because they're grown on poles, are perfect for locally-grown promotions, Smith said, but they're also popular throughout the U.S.
"Retailers seeking flavorful, meaty, clean-cutting tomatoes that are naturally ripened instead of gassed-green will appreciate the difference without doubt."
Oppy expects to market Oceanside Pole tomatoes into November.