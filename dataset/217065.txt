Apple marketers have plenty of fruit to sell in 2018, although U.S. production won’t approach record volume, according to a report from the Vienna, Va.-based U.S. Apple Association.
The U.S. Department of Agriculture projected a national apple volume of 248.7 million bushels in 2017, which would fall short of the record take of 281.3 million in 2014 and 268.4 million in 2016, which was the fourth-largest all-time.
When the USDA’s estimated 2017 crop of 248.7 million bushels is included, the five-year rolling average of annual apple production stands at 257 million bushels, which is more than 2 million bushels greater than the previous record 5-year average of 255 million bushels in the period from 1994-98, Mark Seetin, U.S. Apple’s director of regulatory affairs, said in a December report.
Washington led U.S. production with an estimate of 142.3 million bushels designated for the fresh market, according to USDA. New York was next, at 28 million, and Michigan, third, at 20.3 million. 
A year ago, Michigan was No. 2-ranked in production, behind Washington, with 28 million boxes.
“Overall, it ended up really good,” said Cynthia Haskins, president and CEO of the Fishers-based New York Apple Association.
“We did have a few pockets of hail that we were concerned with overall, but overall the crop sized up beautifully.”
Honeycrisp, one of the most sought-after varieties across the U.S., was in ample supply through the end of 2017, said Mark Russell, grower, vice chairman of the board and chairman of the marketing committee with Crunch Time Apple Growers, a Wolcott, N.Y.-based cooperative.
“For one reason or another, there were maybe 4 million more bushels of Honeycrisps coming out of Washington than maybe were expected, so, for this year, it was a great rubbing of the hands sound from the retailers for a great many Honeycrisps that needed to be moved.”
A delay in Washington’s harvest created a temporary bottleneck in supplies, but that situation didn’t last long, said Dave Williams, vice president of sales and marketing with Wolcott-based Fowler Bros. Inc.
“Usually, Washington is a little ahead of us, so we had to contend with that,” he said. “Overall, a good growing year.”
Selah, Wash.-based Rainier Fruit Co. reported a “real solid year” in 2017, although it encountered temporary supply gaps lasting a few weeks at midyear, said Andy Tudor, business development director.
No gaps are anticipated in 2018, Tudor said.
 
Sales
According to AC Nielsen, U.S. apple sales for the 52-week period ending Sept. 30 were $2.9 billion, down 1.4% from year ago; weekly sales per store averaged $3,095, which were down 2.1% year-on-year; and the average retail price was $1.63, down 2.5% from a year earlier.
“The later start to apple harvest in Washington state followed last year’s record early start to create a lag in apple category performance year-over-year at retail,” said Brianna Shales, spokeswoman with Wenatchee, Wash.-based Stemilt Growers LLC. 
Fruit size is generally smaller this year, which can be advantageous, Shales said.
“Apples are down a size on average, which means an increase in bag supplies and a tight market for bulk,” she said.