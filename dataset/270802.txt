Huanglongbing (HLB) has been confirmed in a residential citrus tree in Southern California, and officials are expanding the area under quarantine for the disease, said Bob Blakely, vice president of the Exeter-based California Citrus Mutual.HLB was confirmed April 11 in a single “backyard” citrus tree in the Orange County city of La Habra, Calif., Blakley said, noting that the new find creates a new HLB quarantine area that now connects already- existing quarantines in portions of Los Angeles and Orange counties, Blakely said.
“When they finalize the quarantine around this, it will make one large contiguous quarantine,” he said.
The infected tree, which has been removed, was about the 55th discovered in that region since 2009, Blakely said.
“It’s 2017 now, so we feel fortunate to have held it to a very small number of backyard trees,” he said.
He noted that all HLB-infected trees have been found in residential trees in that same region.
“It was the same area as earlier finds — all in that area around San Gabriel,” he said.
Officials from the industry and the California Department of Food and Agriculture are intensifying survey efforts, Blakely said.
“It’s concerning that we’re finding these trees, but they’re all backyard trees,” he said. “We have not found any HLB in any commercial citrus groves in California, nor have we found any infected psyllids. They’re continually surveying those areas. I think it’s a testament to the aggressive approach that the industry and CDFA are taking to prevent this disease.”
Asian citrus psyllids (ACP) are the vector for the disease, which already has ravaged Florida’s citrus industry and threatens California’s citrus production, which, by Blakely’s estimate, is worth nearly $4 billion per year.
In addition to having established quarantines around the affected areas, agriculture officials have mandated tarping of all bulk citrus being transported anywhere in the state, to prevent psyllids from “hopping off” any loads, Blakely said.
Around the same time of the La Habra find, two ACP samples taken in Anaheim, Calif., tested positive for the HLB-causing bacteria, Blakely confirmed, noting that CDFA is treating residential plants in an area around the area where the infected pests were found.
Blakely said no trees had tested positive for HLB there.
“We have not found any infected trees in that area, but because we found the psyllids, CDFA has intensified their survey and is surveying every house plant in a very tight radius around where those psyllids were round,” Blakely said.
The surveying includes testing trees in sections and sampling leaves from each.
Commercial citrus growers are advised to cooperate with their area programs and treat their groves to control psyllid populations, Blakely said.
The best approach is to coordinate treatments with neighboring growers, Blakely noted.
“You can control psyllids in a broad area that way,” he said. “The broader area you can cover with one treatment, the better you’re going to control and actually eradicate psyllids in a wide area.”
Growers also are urged to coordinate psyllid-control programs as part of a regular pest-control regimen, Blakely said.
“If you incorporate materials in your regular program, you can control incidental low-level populations, you can prevent them from becoming established,” he said.