Saving the bison: Polish bison off to help Spanish herd grow
Saving the bison: Polish bison off to help Spanish herd grow

By MONIKA SCISLOWSKAAssociated Press
The Associated Press

WARSAW, Poland




WARSAW, Poland (AP) — Seven young female bison from Poland are being shipped off to help boost a herd in Spain and expand the population of Europe's largest animal, which is on the endangered species list.
Michal Krzysiak, head of the Bialowieza National Park in eastern Poland, said the bison — aged between 1 and 4 years old — have been put on trucks in special wooden cases and were on their way to two breeding farms north of Madrid. He expected them to arrive there mid-week.
Wild bison were extinct in Europe before World War I. The new bison are the offspring of more than a dozen selected captive animals. There are some 1,900 bison in Poland now, including 690 in the Bialowieza reserve.
Krzysiak said the reserve's goals are to help develop new herds, preserve the species and protect the population from the spread of any diseases. The program is financed by Poland's forestry authorities.
Wanda Olech of Warsaw's University of Life Sciences said the animals were going to two private farms near Segovia: La Perla and Posada, where they will have a large fenced-in terrain.
"The bison is Europe's largest land animal. It had very dramatic history in the 20th century — was almost extinct — so we are doing everything to develop and preserve the population," Olech told The Associated Press.
Olech said bison from Poland have brought new energy to a village in northern Spain, San Cebrian de Muda, whose predominantly elderly population now take regular walks to visit the small herd brought there eight years ago.
Bison from Poland have also been sent to some other farms across Europe in recent years.