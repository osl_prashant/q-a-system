Carolina produce shippers don’t have to go far, for the most part, to find prized export markets.
Most who deal in offshore markets go to Canada and Mexico first.
“Most of our sweet potato business is export, so we want to continue with our current customers as well as building new business,” said Kim Kornegay-LeQuire, vice president with Princeton, N.C.-based watermelon, sweet potato and asparagus grower-shipper Kornegay Family Produce.
Kornegay-LeQuire pointed out that North Carolina not only supplies about half the country’s sweet potatoes but also is the top-ranked state by sweet potato exports, due to its supply and proximity to ports.
Ridge Spring, S.C.-based broccoli, peach, pepper and vegetable grower-shipper Titan Farms LLC ships product into Canada and Mexico, said Chalmers Carr, owner and CEO.
The company was a “pioneer” in opening the Mexican market, Carr said.
“Because of a couple of insects on the East Coast, we’ve had to go through postharvest sanitation,” he said. “We only had fumigation as a tool. Now we have irradiation, which is much better for fruit quality. It actually improves shelf life.”
The company irradiates via a third party, Carr said.
North Carolina potatoes often end up in Canadian markets, said Tommy Fleetwood, marketing supervisor with the North Carolina Department of Agriculture and executive director of the North Carolina Potato Association.
“Depending on the Canadian crop and how their crop goes into storage and how it holds in storage, Canada is a real good opportunity for North Carolina potatoes,” he said. “When the Canadian crop runs short, I guess, if their storage is depleted by the first of June or so, there’s a big opportunity for North Carolina potatoes to be shipped into retail and into there for chips.”
This year could be a big year for export into Canada, Fleetwood said.
There are opportunities for potatoes elsewhere, as well, for Swan Quarter, N.C.-based potato grower-shipper Pamlico Shores Produce, said Hunter Gibbs, partner.
“I sent some stuff to Puerto Rico, and I’m looking to expand that business,” he said. Northern Europe and Latin America generate “a lot of sales” for Faison, N.C.-based Farm Fresh Produce Inc., which supplies vegetables, greens, peppers and squash, as well as sweet potatoes, said Bethany Malcolm, vice president.
North Carolina’s sweet potato industry has been working on expanding its export business for years, said Kelly McIver, executive director of the Benson-based North Carolina Sweet Potato Commission.
“Europe is by far still expanding and growing, and (especially) in Scandinavia,” she said. “There’s still quite a few new territories for us to try to conquer.”
Export is such a big part of the business at Lucama, N.C.-based sweet potato grower-shipper, the company has an office in England to handle the growing European trade, said Jeff Thomas, marketing director.
Raleigh, N.C.-based L&M Cos. Inc. is entering the sweet potato export business this year for the first time, said Derek Ennis, sales manager for potatoes and onions.