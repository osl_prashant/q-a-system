BC-TX--Texas News Digest 5 pm, TX
BC-TX--Texas News Digest 5 pm, TX

The Associated Press



Good afternoon! Here's a look at AP's general news coverage in Texas at this hour. Questions about coverage plans are welcome and should be directed to the Dallas AP at 972-991-2100, or, in Texas, 800-442-7189. Email: aptexas@ap.org. Terry Wallace is at the desk.
A reminder: This information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
— ADDS Southwest Airlines-Emergency Landing-Pilot.
— UPDATES Shooting-Casino Company Picnic.
— ADDS Army Futures Command.
— ADDS Armstrong-Lawsuit.
— ADDS Armstrong-Lawsuit-The Latest.
— UPDATES US-Syria-Missing Journalist.
— ADDS Obit-Popovich's Wife.
— ADDS Obit-Popovich's Wife-The Latest.
— ADDS AP Was There-Waco Standoff Ends.
— ADDS Car Crash-Home Explosion.
— ADDS Shooting-Austin Judge.
— ADDS California Oil Spill.
.....................
TOP STORIES:
SOUTHWEST AIRLINES-EMERGENCY LANDING
DALLAS — Southwest Airlines sought more time last year to inspect fan blades like the one that snapped off during one of its flights this week in an engine failure that left a passenger dead. Dallas-based Southwest opposed a recommendation by the engine manufacturer to require ultrasonic inspections of certain fan blades within 12 months, saying it needed more time to conduct the work. On Tuesday, an engine on a Southwest jet exploded over Pennsylvania during the New York to Dallas flight and the plane made an emergency landing in Philadelphia. By David Koenig and Claudia Lauer. SENT: 750 words, photos, video.
With:
— SOUTHWEST AIRLINES-EMERGENCY LANDING-THE LATEST
SOUTHWEST AIRLINES-EMERGENCY LANDING-PILOT
BOERNE, Texas — Tammie Jo Shults was determined to "break into the club" of male military aviators. One of the first female fighter pilots in the U.S. Navy, Shults flew training missions as an enemy pilot during Operation Desert Storm, while working with other women to see a rule excluding them from combat flights repealed. Twenty-five years later, Shults was at the controls of the Dallas-bound Southwest Airlines Flight 1380 Tuesday when it made an emergency landing in Philadelphia after one of the engines on the Boeing 737 exploded while the plane was traveling 500 mph at 30,000 feet with 149 people on board. "Everybody is talking about Tammie Jo and how cool and calm she was in a crisis, and that's just Tammie Jo," said Rachel Russo, a friend from Shults' church in Boerne, Texas, about 30 miles northwest of San Antonio. "That's how she's wired." By Emily Schmall and Jim Vertuno. SENT: 750 words, photos.
SHOOTING-CASINO COMPANY PICNIC
LAS VEGAS — A disgruntled casino employee accused of shooting two executives at a company picnic in Las Vegas planned an escape and spent days on the run before a sheriff's deputy happened upon him sleeping in a car Thursday near a small Texas Panhandle town, authorities said. A deputy on routine patrol checked the license plate of a car parked at a highway rest stop not far from the New Mexico state line and found it was stolen from Utah. When confronted by Oldham County deputies, Anthony J. Wrobel initially reached for a 9mm handgun but then surrendered without incident, authorities said. By Ken Ritter. SENT: 420 words, photos.
With:
— SHOOTING-CASINO COMPANY PICNIC-THE LATEST
BARBARA BUSH-AUTHORS
NEW YORK — Sandra Brown knew she had a friend in Barbara Bush from the moment she heard the story about election night, 2000, and how the former first lady was reading one of her books to calm her nerves. Brown, Mary Higgins Clark and Brad Meltzer were among those sharing memories of Bush, a longtime advocate of literacy who became close to numerous writers. Bush died Tuesday in Houston at age 92. By AP National Writer Hillel Italie. SENT: 660 words, photos.
TEXAS GOVERNMENT & POLITICS:
TEXAS AGENCY-BUNGLED CONTRACTS
AUSTIN, Texas — Texas lawmakers have rebuked a state agency for botching millions of dollars in contracts. The Austin American-Statesman reports that the Texas Health and Human Services Commission discovered earlier this month that staff members incorrectly scored five managed care contracts worth an estimated $600 million. The State Auditor's Office released a report this week highlighting a series of problems with the agency's procurement processes. SENT: 300 words.
Also:
— ARMY FUTURES COMMAND — The Army says it's considering 15 cities for the headquarters of its planned Futures Command, which will keep track of emerging technology and innovations that could be used in warfighting. The contenders are Atlanta; Austin, Texas; Boston; Chicago; Dallas; Denver; Houston; Los Angeles; Minneapolis; New York; Philadelphia; Raleigh, North Carolina; San Diego; San Francisco; and Seattle. SENT: 120 words.
— CHRISTIE PORTRAIT — Former New Jersey Gov. Chris Christie, a fan of the Dallas Cowboys, will have an official portrait costing $85,000, which is more than taxpayers shelled out for paintings of his three predecessors combined. SENT: 130 words, photos.
AROUND THE STATE & NATION:
ARMSTRONG-LAWSUIT
AUSTIN, Texas — Lance Armstrong reached a $5 million settlement with the federal government in a whistleblower lawsuit that could have sought $100 million in damages from the cyclist who was stripped of his record seven Tour de France victories after admitting he used performance-enhancing drugs throughout much of his career. The deal announced Thursday came as the two sides prepared for a trial that was scheduled to start May 7 in Washington. Armstrong's former U.S. Postal Service teammate Floyd Landis filed the original lawsuit in 2010 and is eligible for up to 25 percent of the settlement along with attorney fees paid by Armstrong. By Jim Vertuno. SENT: 1000 words, photos. Moved on general and sports news services.
With:
— ARMSTRONG-LAWSUIT-THE LATEST
US-SYRIA-MISSING JOURNALIST
WASHINGTON — Federal authorities for the first time are offering a reward for information leading to an American journalist who has been missing in Syria for more than five years. The reward is up to $1 million. Austin Tice, of Houston, Texas, disappeared in August 2012 while covering Syria's civil war. A video released a month later showed him blindfolded and held by armed men saying "Oh, Jesus." He has not been heard from since. By Sadie Gurman and Jamie Stengle. SENT: 320 words, photos.
AP WAS HERE-WACO STANDOFF ENDS
WACO, Texas — Around 6 a.m. on April 19, 1993, FBI agents moved in to end a 51-day standoff with the Branch Davidian religious sect near Waco, Texas, ramming holes in the group's compound with armored vehicles and spraying tear gas inside. About six hours later, smoke poured from the compound, which soon was consumed by a massive fire. Here are story and a timeline published by The Associated Press the day the standoff ended. SENT: 1900 words, photos. Also moved Wednesday.
MS-13 LEADERSHIP-PROSECUTION
NEW YORK — The alleged East Coast crime boss of the notorious street gang MS-13 was ordered held without bail on Thursday at his first court appearance on Long Island, where prosecutors accuse him of having a hand in a wave of violence. Miguel Angel Corea Diaz had been jailed in Maryland in a separate drug case. An indictment alleges Corea Diaz ordered beatings and killings and directed the gang's drug operations in New York, New Jersey, Maryland, Texas and elsewhere. By Tom Hays. SENT: 210 words, photo.
SPACE-HUBBLE
CAPE CANAVERAL, Fla. — NASA is marking the 28th anniversary of the Hubble Space Telescope's launch with a peek into a wild stellar nursery. Scientists released the picture Thursday in advance of next week's milestone. Mission Control is at Johnson Space Center in Houston. By Marcia Dunn. SENT: 120 words, may be updated.
OBIT-POPOVICH'S WIFE
SAN ANTONIO — LeBron James fought back tears. Former President Bill Clinton offered condolences. Craig Sager's daughter tweeted that she was "just sick" over the news. Such is the regard that so many, from so many walks of life, have for San Antonio Spurs coach Gregg Popovich, whose 67-year-old wife, Erin, died Wednesday after a long battle with a respiratory problem. SENT: 800 words, photos. Moved on general and sports news services.
With:
— OBIT-POPOVICH'S WIFE-THE LATEST
EXCHANGE-LEASING CONVICTS-CEMETERY
SUGAR LAND, Texas — The 31 marked graves inside Old Imperial Farm Cemetery are rusted and crumbling, markers of a time that Reginald Moore believes Sugar Land hopes to forget. The Houston Chronicle reports the 58-year-old has spent nearly two decades telling anyone who will listen about the old cemetery in Fort Bend County and how nearby areas may contain graves of people who were part of the convict leasing system in Sugar Land. The statewide program, initiated shortly after slavery was outlawed more than 150 years ago, allowed prisoners, primarily African-Americans, to be contracted out for labor. By Brooke A. Lewis, Houston Chronicle. SENT: 1150 words, photos.
IN BRIEF:
— CAR CRASH-HOME EXPLOSION — Police have released dashcam video showing a North Texas house exploding just as a police officer steps into the yard while responding after an SUV hit the residence. SENT: 120 words, video.
— SHOOTING-AUSTIN JUDGE — A judge shot during an assassination attempt outside her Austin, Texas, home has told jurors that there were times she wanted to die following the 2015 attack. SENT: 130 words.
— RED SNAPPER — Federal authorities have set the red snapper season for charter boats in the Gulf of Mexico, giving them from June 1 through July 21 to catch the popular sport and table fish. SENT: 130 words.
— KILLER NURSE — A Texas prosecutor says an imprisoned former nurse who authorities believe could be responsible for the deaths of up to 60 children has confessed in the past to killing youngsters in her care. SENT: 130 words, photos.
— APARTMENT KILLING — A Vermont man who was arrested in Texas has admitted to fatally shooting a University of Vermont student in a dispute over money.
— GAS PRICES — Texas and nationwide retail gasoline prices have jumped 7 cents per gallon this week. SENT: 120 words.
BUSINESS:
CALIFORNIA OIL SPILL
LOS ANGELES — It was the worst California coastal spill in 25 years, spreading a shimmering sheen out to sea that eventually deposited tar balls on beaches more than 100 miles away. But were they looking at a crime scene? Jurors being selected Thursday in Santa Barbara County Superior Court will determine if the company that operated the pipeline that ruptured, spilling 142,000 gallons of crude May 19, 2015, broke any laws. Plains All American Pipeline, based in Houston, is charged with three felonies for spilling oil on land and in state and federal waters, along with a dozen misdemeanors, including violations of fish and game laws for killing sea lions, pelicans, a loon and a dolphin. By Brian Melley. SENT: 600 words, photos Moved on general, financial and small business news services.
SPORTS REFER:
BKN--WARRIORS-SPURS
SAN ANTONIO — The Spurs, trailing defending champ Golden State 2-0, play their first home game in the opening-round series a day after the death of San Antonio coach Gregg Popovich's wife, Erin, following a long illness. Ettore Messina, a Spurs assistant since 2014, will coach the team. By Raul Dominguez. UPCOMING: 700 words, photos. Game starts 8:30 p.m.
____
If you have stories of regional or statewide interest, please email them to aptexas@ap.org
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867.
For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
The AP.