House panel moves to curb food stamps, renew farm subsidies
House panel moves to curb food stamps, renew farm subsidies

By ANDREW TAYLORAssociated Press
The Associated Press

WASHINGTON




WASHINGTON (AP) — A bitterly-divided House panel Wednesday approved new work and job training requirements for food stamps as part of a five-year renewal of federal farm and nutrition policy.
The GOP-run Agriculture Committee approved the measure strictly along party lines after a contentious, five-hour hearing in which Democrats blasted the legislation, charging it would toss up to 2 million people off of food stamps and warning that it will never pass Congress.
The hard-fought food stamp provisions would tighten existing work requirements and expand funding for state training programs, though not by enough to cover everybody subject to the new work and training requirements.
Agriculture panel chair Michael Conaway said the provisions would offer food stamp beneficiaries "the hope of a job and a skill and a better future for themselves and their families."
At issue is the Supplemental Nutrition Assistance Program, or SNAP, which provides food aid for more than 40 million people, with benefits averaging about $450 a month for a family of four.
The food stamp cuts are part of a "workforce development" agenda promised by GOP leaders such as Speaker Paul Ryan, R-Wis., though other elements of the agenda have been slow to develop.
"The timing is just perfect, given the fact that we have more than five million jobs that are open and available," said  Rep. Glenn Thompson, R-Pa., who said the GOP provisions would cement "a pathway to opportunity" for the poor and "give them better access to skills-based education."
But Democrats said the provisions would drive up to two million people off of the program, force food stamp recipients to keep up with extensive record keeping rules, and create bulky state bureaucracies to keep track of it all — while not providing enough money to provide job training to all those who would require it.
"This legislation would create giant, untested bureaucracies at the state level. It cuts more than $9 billion in benefits and rolls those savings into state slush funds where they can use the money to operate other aspects of SNAP," said Rep. Collin Peterson of Minnesota, top Democrat on the panel. "Let me be clear: this bill, as currently written, kicks people off the SNAP program."
Currently, adults 18-59 are required to work part-time or agree to accept a job if they're offered one. Stricter rules apply to able-bodied adults without dependents between the ages of 18 and 49, who are subject to a three-month limit of benefits unless they meet a work requirement of 80 hours per month.
Under the new bill, that requirement would be expanded to apply to all work-capable adults, mandating that they either work or participate in work training for 20 hours per week with the exception of seniors, pregnant women, caretakers of children under the age of six, or people with disabilities.
In addition to food stamps, the measure would renew farm safety-net programs such as subsidies for crop insurance, farm credit, and land conservation. Those subsidies for farm country traditionally form the backbone of support for the measure among Republicans, while urban Democrats support food aid for the poor.
The legislation has traditionally been bipartisan, blending support from urban Democrats supporting nutrition programs with farm state lawmakers supporting farm programs.
The measure mostly tinkers with those programs, adding provisions aimed at helping rural America obtain high-speed internet access, assist beginning farmers, and ease regulations on producers.
"When you step away from the social nutrition policy much of this is a refinement of the 2014 farm bill. So we're not reinventing the wheel. That makes it dramatically simpler," said Rep. Frank Lucas, R-Okla., a former chairman of the committee. "Most folks are generally satisfied with the fundamentals of the farm safety net."
That satisfaction has helped fuel speculation that this year's renewal of food and farm programs will fail because just a short-term renewal of current policies would satisfy many lawmakers. The Senate is taking a more traditional bipartisan approach that's sure to avoid big changes to food stamps.
The House measure also would cut funding for land conservation programs long championed by Democrats, prompting criticism from environmental groups. At the same time, it contains a proposal backed by pesticide manufacturers such as the Dow Chemical Company that would streamline the process for approving pesticides by allowing the Environmental Protection Agency to skip reviews required under the Endangered Species Act.