Raising calves is much like being an Olympic athlete. Both require passion and dedication for continuous improvement. The “One team. Gold dreams.” Dairy Calf and Heifer Association (DCHA) annual conference is set for April 10-12 in Milwaukee, Wis.
“We’re excited about our lineup for this year’s conference,” says Brent Caffee, DCHA conference committee co-chair and board of director. “Past attendee feedback and trending industry topics play an integral role in shaping our conference agenda.”
Here are five reasons why you should attend:
1. Learn about hot topics
Breakout sessions will cover a wide range of hot topics including calf immunity, group housing, ventilation systems, managing employees, feed center management, feedlot management and cattle transportation. Learn from the best in the business.
2. Experience farm and industry tours
Tours will focus on several areas of calf and heifer management. Attendees can attend one of two industry tours and will conclude the day by meeting on-farm for a calf management walk-through.
Industry tours (choose one):
STgenetics: A leader in bovine genetics, you will get a glimpse into the research and technology that supports the growing industry. Hear from the experts on the latest in genomic testing and sexing technology.
Milk Products: A behind-the-scenes tour of milk replacer manufacturing. Learn about the research and expertise that goes into calf milk replacer, and milk replacer for a variety of species.
Farm tour:
Vir-Clar Farms: Exceptional calf care protocols and cleanliness are key to calf management success on this 2,000-cow dairy. Hear from the dairy’s calf manager, Katie Grinstead, about their approach to getting calves and heifers off to the best start possible.
3. Get an outside perspective
Learn how to use influence and integrity to inspire and engage others. Regardless of your role on-farm or in the industry, great leadership is the driving force behind your growing business. Learn more about “The Power of Influence” through keynote speaker, Ty Bennett.
4. Network with your peers
Network with other calf and heifer raisers from across the U.S. and world. All aspects of calf and heifer raising will be represented at the conference. Take time to learn from your peers during meals, between sessions or during evening social events.
5. Enjoy a new location
This year’s new conference location in Milwaukee, Wis. makes for easy travel arrangements. All conference activities will be held at the Potawatomi Hotel and Casino, except the industry and farm tours.
“We hope you can join us at this year’s conference and learn how to take your calf and heifer program to the next step on the podium,” says Caffee.
Last year’s conference drew more than 600 dairy calf and heifer raisers, dairy farmers and allied industry professionals from 30 states and 10 countries, representing more than 2 million cattle.
ARPAS Credits
The conference has been pre-approved by the American Registry of Professional Animal Scientists (ARPAS) for up to 11 continuing education units. Participants should request credits for this event at arpas.org.
Make plans to attend
To register and for a full conference schedule with session descriptions, visit calfandheifer.org.
The Dairy Calf and Heifer Association (www.calfandheifer.org) was founded in 1996 based on the mission to help dairy producers, calf managers and those professionally focused on the growth and management of dairy calves and heifers. With a national membership of producers, allied industries and research leaders, DCHA seeks to provide the industry’s standards for profitability, performance and leadership, serving as a catalyst to help members improve the vitality and viability of their individual efforts and that of their business.