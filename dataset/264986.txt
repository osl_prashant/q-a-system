BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Thu. Mar 22, 2018          USDA Market NewsDaily California EggsPrices are unchanged. The undertone is higher. Offerings are light tooccasionally moderate. Demand is fairly good to good. Supplies are light tomoderate. Market activity is moderate to active.Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                272EXTRA LARGE          300LARGE                297MEDIUM               193Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 22-Mar-18 11:10 AM E MP