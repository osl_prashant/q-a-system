For fans of the movie The Princess Bride, we can say with Vizzini there are two classic blunders: First, “never get involved in a land war in Asia” and “never go in against a Sicilian when death is on the line.”
 
Craig Boyan, H-E-B president and CEO, might add a third -  “never speak to industry groups.” He reminded United Fresh attendees that H-E-B — with its $23 billion in annual sales, 380 stores and 96,000 employees — is private and likes to stay that way. 
 
Happily for us, the company made an exception for the United Fresh show in Chicago.
 
Fred Wilkinson nicely summed up Boyan’s presentation in his recent opinion piece but I can’t resist adding a few more observations.
 
Giving an overview of H-E-B, Boyan remarked on the importance of leadership and motivated employees to keep its high market share in Texas and northern Mexico.
 
“The most important thing is great partners and greater leadership in every store,” he said.
 
H-E-B believes in investing in customer by providing the lowest possible prices, invest in employees by giving them good careers and invest in communities so they can be the best they can be.
 
H-E-B has a diversity of retail banners so it can serve all consumers, which he said is very important to the ethos of H-E-B. The company has a 60% market share in key markets, he said.
 
Boyan said the most important group to H-E-B is its employees, or to use his term, partners. 
 
“Our most important group is our partners and if we take care of them and our communities they will take care of our customers and our business,” he said.
 
Shifting gears to talk about macroeconomic challenges, Boyan said that sales growth is a concern. The perception that the retail food industry is “recession proof” is an illusion. 
 
“The industry as a whole is struggling mightily in sales as we have reached a deflationary state,” he said. 
 
Boyan said some of the reasons for sales struggle include deflation and demographic changes in spending patterns. For example, he said Baby Boomers are tending to spend less as they age. After age 46, consumers typically spend less; he observed all boomers are on the downhill side of 46.
 
Even consumers 35 to 64 are spending less, he said. 
 
What’s more, an ever larger share of the consumer’s food dollar is spent at restaurants, he said, with total dollars spent at restaurants now exceeding the share for retail store sales.
 
About 20% of consumers shop online for goods, but only about 3% shop online for groceries, he said. “We are a dawn of that battle, we know that battle is coming,” he said. Making the economics work will be difficult if delivery is free, he said. It may cost $10 to “pick” an online grocery order and $15 to deliver, he said. With the typical retailer earning only a buck or two on a $100 basket of groceries, adding in the cost of serving the online customer will not be easy.
 
But the most important macroeconomic factor, Boyan said, is the decline of real median household income in the U.S.
 
From 1945 to 1975, there was great job and income growth in America.
 
In the mid 1970s that changed, he said, with increasing pressure on U.S. jobs.
 
Since 2008, the U.S. economy has been in a period of negative wage growth. 
 
Starting in the late 1970s, income growth was replaced by household debt growth to help boost the economy. 
 
U.S. debt as a ratio to the Gross Domestic Product hit about 400x in 2008, up from 300x in the Great Depression and similar to the debt levels now being experienced in China.
 
Boyan asked where the growth in retail sales for the next 30 years will come from if household income is down and consumers are trying to de-leverage from debt.
 
“This is the thing that keeps me up and night and the reason I never get invited back to speak at places,” he said. Boyan said it is possible that food retailing is a flat to declining industry for the next 30 years. 
 
“The question for you is how do you run your business differently than if you thought this was just a quarterly blip and we are going to get bailed out by strong growth next year?” 
 
H-E-B is preparing to compete in that kind of economy, he said.
 
Even Texas, often touted as a growth state, is seeing dropping household income, he said. 
 
Boyan said the industry may see consolidation and the fight for share intensifying. In San Antonio, for example, Wal-Mart had 23 stores in 2012 and has doubled its store count to 50 this year, he said. 
 
The H-E-B exec said that the industry can rally around food safety. “When one company makes a mistake we will all pay for it.” “It is incumbent on this group to get better on food safety.
 
Boyan said health and wellness trends can be addressed by the produce industry.
 
The food industry provides 4,000 calories every day to consumers, up from about 3,000 40 years ago. That has added to the obesity problem, he said.
 
Though measures of obesity and produce consumption in Texas are unimpressive, Boyan said H-E-B values the role it can play in helping consumer eat more fruits and vegetable for better health.
 
“We have a lot of opportunity to get better at how we communicate health and wellness, how we teach a healthier lifestyle and incorporate your products in a more fundamental way than we have to date, even though we are doing good things.”
 
Citing exceptions like Costco, Publix, Wegmans(and H-E-B) Boyan said the food industry has under-invested in people and he urged the industry to do better.
 
“How do you show your people that you care about them, that you are investing in them?” he said. “The way out, we think, by investing in our people."
 
Boyan was a great choice as a speaker, even though he laid out some hard realities for all industry players. Competing in a flat to declining market is a grim prospect, but not quite as tough as a land war in Asia.