Customs and Border Protection officers at the Pharr (Texas) International Bridge cargo facility discovered 50.7 pounds of alleged cocaine in a commercial load of tomatoes. 
On July 8, CBP officers referred a 1995 Freightliner tractor-trailer for a secondary inspection, according to a CBP news release.
 
During the inspection, officers used a non-intrusive imagining system along with a canine team to locate 16 packages of alleged cocaine comingled within a shipment of tomatoes.
 
CBP seized the drugs, and the case remains under investigation by Homeland Security agents, the release said.