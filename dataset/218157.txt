There are a lot of skilled people cowboy poet Baxter Black admires. He’s fairly skilled in some areas (he is a veterinarian and rancher), but he lacks in others.

One talent that always alluded him was his horseshoeing skills.

Watch Baxter Black weekends on U.S. Farm Report.