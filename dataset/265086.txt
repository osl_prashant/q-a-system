AP-Washington News Coverage Advisory
AP-Washington News Coverage Advisory

The Associated Press



Washington state at 4:05 p.m.
The Seattle bureau can be reached at (800) 552-7694 or (206) 682-1812. The photo supervisor is at (206) 682-4801 or (800) 552-7694.
For questions on stories from Olympia, call (360) 753-7222. For questions on Spokane-area stories, call Correspondent Nicholas Geranios at (800) 824-4928 or (509) 624-1258.
Please do not give out these phone numbers or email addresses to members of the general public.
AP stories, along with the photos that accompany them, can also be obtained from http://www.apexchange.com. Reruns are also available from the Service Desk (800) 838-4616.
Please submit your best stories via email to apseattle@ap.org. Stories should be in plain text format.
PRISONER-EARLY RELEASE-LAWSUIT
SEATTLE - A man who was released from a Washington state prison a few months early due to a software glitch is suing the Department of Corrections, saying his re-arrest three years later to serve the remaining time was illegal and upended the life he had painstakingly rebuilt. Orlando Wright was nearing the end of his eight-year prison term for armed robbery in 2012 when he was released 76 days early. He was one of more than 3,000 inmates released prematurely due to the software problem, which lasted from 2002 to 2015.  By Gene Johnson.  SENT: 900 words. AP Photos.
ATLANTIC SALMON FARMING-BAN
SEATTLE —Washington state will phase out marine farming of Atlantic salmon and other nonnative fish by 2022 under legislation signed Thursday by Gov. Jay Inslee. By Phuong Le. SENT: 700 words. AP Photos.
HELICOPTER DRUGS
SEATTLE — A Canadian man who spent nearly a decade fighting his extradition to the U.S. for running a helicopter-based drug-smuggling ring has pleaded guilty. By Gene Johnson. SENT: 570 words.
SEA LIONS VERSUS SALMON
NEWPORT, Ore. - Oregon wildlife officials want to kill sea lions that are feasting on fish listed under the Endangered Species Act. Wildlife managers already can trap and euthanize limited numbers of sea lions that journey to the Columbia River and now want approval from the U.S. government to do the same in a different location. The protected marine mammals have been stuffing themselves on wild winter steelhead and spring chinook salmon, which face extinction if nothing changes. By Gillian Flaccus. SENT: 930 words. AP Photos, video.
BUDGET BATTLE-WILDFIRES
WASHINGTON — A spending bill slated for a vote in Congress includes a bipartisan plan to create a wildfire disaster fund to help combat increasingly severe wildfires that have devastated the West in recent years. By Matthew Daly. SENT: 710 words. AP Photos.
FOSTER CARE-WASHINGTON
SEATTLE — An audit report by federal officials says that unannounced visits in 2016 to 20 foster-care group homes in Washington revealed that every one of them failed to meet at least one state licensing requirement. SENT: 140 words. Will be expanded.
SPORTS
BKC--NCAA-FLORIDA ST-GONZAGA
LOS ANGELES — Johnathan Williams and the fourth-seeded Zags (32-4) could move to the brink of their second straight Final Four by winning their 17th consecutive game when they face coach Leonard Hamilton's balanced Seminoles (22-11), who knocked off top-seeded Xavier last weekend. By Greg Beacham. UPCOMING: 700 words, photos. Starts 7:07 p.m. PDT.
FBC--WASHINGTON STATE-AFTER HILINSKI
PULLMAN, Wash. — Spring football arrives at Washington State with a different feel after the death in January of quarterback Tyler Hilinski. Thursday's practice is the first full team event since Hilinski took his life, an impact being felt on the Washington State campus and beyond. By Tim Booth. UPCOMING: 700 words. By 7:30 p.m. PT.
BBA--MARINERS PREVIEW
SEATTLE— Robinson Cano has spent enough time now to understand the angst surrounding the Seattle Mariners. The Mariners have the longest playoff drought in the four major sports, meaning there is an urgency to finally end the skid. By Tim Booth. SENT: 730 words. AP Photos.
BBO--FAST BALLPARK ENTRY
NEW YORK — Three more major league teams are emphasizing life in the fast line. SENT: 270 words. With AP photos.
FBN--SEAHAWKS-DAVIS: The Seattle Seahawks are bringing back running back Mike Davis after a strong conclusion to the 2017 season.
IN BRIEF
—ZINKE OPIOIDS: Interior Secretary Zinke discusses opioids in Wellpinit.
—OFFICER SHOOTOUT-SENTENCING: Man who engaged police shootout gets 37 years in prison.
—BARGE BOAT CRASH: Tugboat and barge reportedly hit boat on Columbia River.
—JUDGES SUE-ELECTRONIC RECORDS: Benton-Franklin County judges sue clerk to keep paper files.
—CARETAKER-RAPE: Caretaker gets 5 years for raping woman, 90, in her home.
—STANDOFF SHOTS FIRED: Police: Man surrenders after firing shots during standoff.