Go as far as you can see, and you will see even farther.
I interviewed with Mike for the specialist position in our chain, and I remember inserting a John Shedd quote to express why I wanted to work for him.
I said I was happy managing my produce stand. A ship in the harbor is safe, but that’s not what ships are made for. Mike promoted me to his staff.
However, this wasn’t our first encounter. Years earlier, Mike recruited me into the produce department while he was a supervisor himself, and I was a teenager. Even though we didn’t work closely the first 10 years, he soon became our produce director and I was fortunate enough to eventually reunite and work closely with Mike at an elevated level.
Mike was more than good to work for, he was among the best.
He had what psychologists now refer to as having high emotional intelligence. In layman’s terms, he not only knew produce, but was able to handle stressful situations, deal with people, and express himself calmly. Like a good captain, riding out a storm.
I learned so much from Mike. When I messed up, he was there to rein me in. When I did well, he let me know about that too. Mike made sure that after I spent countless hours on a remodel I took time off to decompress. How many bosses care or take the time for that? I thought I’d stay with that chain, with Mike, for the long run.
Times change.
Mike was eventually lured away to help lead major produce suppliers. Although I was upset, Mike pointed out that if you’re going to run with the big dogs, sometimes you have to get off the porch.
True. Mike was destined for more challenges and opportunity than what a single chain could provide. His produce knowledge, people skills, attention to detail and great sense of humor were embraced by everyone from the agriculture side, to retailers and foodservice, throughout the U.S. and beyond.
When I departed from that chain myself a few years later, he advised, “Always do what you say you’re going to do, and you’ll be successful.” 
So simple. Yet, in a world riddled with spotty follow-through, not always so easy.
Mike wrote weekly marketing bulletins. With a journalism base, his style was like no other director. His words, often emulated in this space, were direct, with substance and yet brief — the mark of a good communicator. 
He always answered his phone, “Hi, it’s Mike.”
And now, Mike Aiton is retiring. I think of the countless lives he has affected and touched in our produce industry and can only reflect.
I was lucky to be one of those people.
Armand Lobato works for the Idaho Potato Commission. His 40 years’ experience in the produce business span a range of foodservice and retail positions. E-mail lobatoarmand@gmail.com.
What's your take? Leave a comment and tell us your opinion.