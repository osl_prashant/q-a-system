Market reaction

Ahead of USDA's reports, corn futures were trading 2 to 2 1/2 cents lower, beans were 2 cents higher, wheat was 3 to 6 cents lower and cotton was 75 to 80 points lower.
Following USDA's reports, corn futures are trading 6 to 7 cents higher, soybeans 26 to 28 cents higher, winter wheat is 1 to 2 cents higher, spring wheat is 2 to 4 cents lower and cotton futures are about 40 points lower.
Pro Farmer Editor Brian Grete reacts: 




Crop production
 
Corn: 14.280 billion bu.; trade expected 14.204 billion bu.
-- compares to 14.184 billion bu. in September; 15.148 billion bu. in 2016
Beans: 4.431 billion bu.; trade expected 4.447 billion bu.
-- compares to 4.431 billion bu. in September; 4.307 billion bu. in 2016
Cotton: 21.12 million bales; trade expected 21.2 million bales
-- compares to 21.758 million bales in September; 17.2 million bales in 2016
Once again, USDA's national average corn yield estimate of 171.8 bu. per acre is above the highest pre-report trade estimate and is 1.7 bu. above the average pre-report trade guess. The yield is also up 1.9 bu. per acre from last month. That increase in yield was just partially offset by a 377,000-acre drop in estimated harvested area, to 83.119 million acres. The end result is a corn crop that's 76 million bu. bigger than trade expectations.
Looking at the state-by-state yields for corn, yield increases from last month were estimated in Illinois (up 3 bu., to 192 bu. per acre), Indiana (up 2 bu. to 173 bu.), Iowa (up 4 bu. to 191 bu.), Kansas (up 1 bu. to 168 bu.), Minnesota (up 2 bu. to 184 bu.), Missouri (up 8 bu. to 172 bu.), North Dakota (up 2 bu. to 126 bu.), South Dakota (up 2 bu. to 147 bu.) and Wisconsin (up 2 bu. to 164 bu. per acre).
Yields were steady from month ago in Nebraska (181 bu. per acre) and Ohio (173 bu. per acre).
Yields were estimated lower from last month in Michigan (down 1 bu. per acre to 168 bu. per acre).
The national average soybean yield of 49.5 bu. per acre is down 0.4 bu. from last month and is 0.6-bu. below the average pre-report trade estimate. That lower yield, however, was fully offset by a 740,000-acre increase in harvested acres to 89.471 million acres, resulting in a bean crop estimate that's down less than half a load of beans from last month (422 bu.).
Looking at the state-by-state yields for soybeans, yield increases from last month were estimated in Michigan (up 1 bu., to 49 bu. per acre) and North Dakota (up 1 bu., to 36 bu. per acre).
Yields were held steady with month-ago in Arkansas (51 bu. per acre), Missouri (49 bu.), Nebraska (56 bu.) and South Dakota (45 bu. per acre).
Yields are estimated lower than last month in Illinois (down 1 bu. to 57 bu. per acre), Indiana (down 1 bu. to 55 bu.), Iowa (down 1 bu. to 56 bu.), Kansas (down 2 bu. to 41 bu.), Minnesota (down 1 bu. to 46 bu.), Ohio (down 2 bu. to 52 bu.) and Wisconsin (down 1 bu. to 47 bu. per acre).
The national average cotton yield is now estimated at 889 lbs. per acre, down 19 lbs. from last month. Georgia cotton yields are 113 lbs. per acre lower than in September at 900 lbs. and Texas yields are estimated 8 lbs. lower at 745 lbs. per acre. USDA also cut 100,000 acres from estimated harvested area, dropping the tally to 11.405 million acres.
 
U.S. carryover
Corn: 2.295 billion bu. for 2016-17; down from 2.350 billion bu. in September
-- 2.340 billion bu. for 2017-18; up from 2.335 billion bu. in August
Beans: 301 million bu. for 2016-17; down from 345 million bu. in September
-- 430 million bu. for 2017-18; down from 475 million bu. in August
Wheat: 960 million bu. for 2017-18; up from 933 million bu. in September
Cotton: 2.75 million bales for 2016-17; unch. from 2.75 million bales in September
-- 5.8 million bales for 2017-18; down from 6.0 million bales in August
On old-crop corn, USDA made several changes to "fine tune" the usage numbers to line up with the Sept. 1 Quarterly Grain Stocks Report. These changes resulted in an old-crop carryover of 2.295 billion bushels, 55 million bu. lower than last month.
Beginning stocks for 2017-18 corn are 55 million bu. smaller than estimated in September. Production, however, is up 96 million bu. for a 40-million-bu. increase in total supplies. USDA increased estimated 2017-18 corn feed & residual use at 5.5 billion bu., up 25 million bu. from last month, and it increased estimated food, seed & industrial corn use 10 million bu. to 6.935 billion bushels (corn-for-ethanol use was steady at 5.475 billion bu.). USDA puts the national average on-farm cash corn price for 2017-18 at $2.80 to $3.60, unchanged from last month.
On old-crop beans, USDA made several changes on the supply- and demand-side of the balance sheets, resulting in a carryover of 301 million bu., lining up old-crop stocks with the Sept. 1 Quarterly Grain Stocks Report. Old-crop carryover is 44 million bu. below last month's estimate from the World Board.
Beginning stocks for 2017-18 beans are 44 million bu. smaller than estimated in September. The bean crop estimate is basically steady with last month, resulting in a 44-million-bu. drop in total new-crop supplies. USDA made no changes on the demand-side of the balance sheet, resulting in a 45-million-bu. drop in estimated new-crop bean carryover. USDA puts the national average on-farm cash bean price for 2017-18 at $8.35 to $10.05, unchaged from last month.
For 2017-18 wheat, USDA cut total supply by 3 million bushels. On the demand side, USDA cut estimated Feed & Residual use by 30 million bu. (to 120 million bu.), raising carryover by 27 million bu. from last month. USDA puts the national average on-farm cash wheat price for 2017-18 at $4.40 to $4.80, up a dime on the bottom and down a dime on the top of the range from last month.
Total supplies for the 2017-18 cotton marketing year are down 640,000 bales from last month. That was partially offset by a 400,000-bale cut to estimated exports (14.50 million bales) and a 40,000-bale cut to unaccounted use (230,000 bales). Carryover is down 200,000 bales from last month. USDA puts the national average on-farm cash cotton price for 2017-18 at 55 cents to 65 cents, up a penny on the bottom and down a penny on the top end of the range from last month.
 
Global carryover
Corn: 226.99 for 2016-17; up from 226.96 MMT in September
-- 200.96 MMT for 2017-18; down from 202.47 MMT in September
Beans: 94.86 MMT for 2016-17; down from 95.96 MMT in September
-- 96.05 MMT for 2017-18; down from 97.53 MMT in September
Wheat: 256.58 MMT for 2016-17; up from 255.83 MMT in September
-- 268.13 MMT for 2017-18; up from 263.14 MMT in September
Cotton: 89.57 million bales for 2016-17; inch. from 89.57 million bales in September
-- 92.38 mil. bales for 2017-18; down from 92.54 mil. bales in September
 
Global production highlights
Argentina beans: 57.8 MMT for 2016-17; compares to 57.8 MMT in September
-- 57.0 MMT for 2017-18; compares to 57.0 MMT in September
Brazil beans: 114.1 MMT for 2016-17; compares to 114.0 MMT in September
-- 107.0 MMT for 2017-18; compares to 107.0 MMT in September
Argentina wheat: 18.4 MMT for 2016-17; compares to 17.5 MMT in September
-- 17.5 MMT for 2017-18; compares to 17.5 MMT in September
Australia wheat: 33.5 MMT for 2016-17; compares to 33.5 MMT in September
-- 21.5 MMT for 2017-18; compares to 22.5 MMT in September
China wheat: 128.85 MMT for 2016-17; compares to 128.85 MMT in September
-- 130.0 MMT for 2017-18; compares to 130.0 MMT in September
Canada wheat: 31.7 MMT for 2016-17; compares to 31.7 MMT in September
-- 27.0 MMT for 2017-18; compares to 26.5 MMT in September
EU wheat: 145.47 MMT for 2016-17; compares to 145.43 MMT in September
-- 151.04 MMT for 2017-18; compares to 148.87 MMT in September
FSU-12 wheat: 130.74 MMT for 2016-17; compares to 130.74 MMT in September
-- 138.27 MMT for 2017-18; compares to 137.27 MMT in September
Russia wheat: 72.53 MMT for 2016-17; compares to 72.53 MMT in September
-- 82.0 MMT for 2017-18; compares to 81.0 MMT in September
China corn: 219.55 MMT for 2016-17; compares to 219.55 MMT in September
-- 215.0 MMT for 2017-18; compares to 215.0 MMT in September
Argentina corn: 41.0 MMT for 2016-17; compares to 41.0 MMT in September
-- 42.0 MMT for 2017-18; compares to 42.0 MMT in September
South Africa corn: 17.48 MMT for 2016-17; compares to 17.15 MMT in September
-- 12.5 MMT for 2017-18; compares to 12.5 MMT in September
Brazil corn: 98.5 MMT for 2016-17; compares to 98.5 MMT in September
-- 95.0 MMT for 2017-18; compares to 95.0 MMT in September
China cotton: 22.75 mil. bales for 2016-17; compares to 22.75 mil. bales in September
-- 24.5 MMT for 2017-18; compares to 24.5 MMT in September