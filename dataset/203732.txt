With per-capita consumption of sweet potatoes continuing to set records, suppliers are offering retailers plenty of options for the root vegetable.
Kelly McIver, executive director of the North Carolina Sweet Potato Commission Inc., Benson, said as consumers continue to choose sweet potatoes there should be increased demand in retail.
"Sweet potatoes from North Carolina are experiencing quite a bit of demand from all aspects of consumers," she said. "Folks want them on the menu from the local deli to the nearest buffet. Dieticians and nutritionists are doing a great job outlining the benefits."


All segments

Jeff Scramlin, the Raleigh, N.C.-based director of business development-sweet potatoes for Wada Farms Marketing Group LLC, Idaho Falls, Idaho, agreed that consumers are becoming increasingly aware of sweet potatoes' health and nutritional benefits and also are realizing they are readily available and inexpensive.
"Thanks to growth across all domestic segments (retail, foodservice, wholesale and processing), we're seeing per capita consumption at an all-time high," he said. "Combine this with the 30(% to) 40% yearly increases in the export segment, and demand for North Carolina sweet potatoes has never been higher."


Variety of pack options

Norman Brown, director of sales and co-packer relations for sweet potatoes at Wada Farms Marketing Group LLC, Idaho Falls, Idaho, added that the company offers bulk, bags, microwaveable wrapped and easy-steamer bags (microwaveable bags). Wada Farms offers sweet potatoes in 40-pound and 20-pound cartons, count packs and for the export market - 18-, 15- and 6-kilogram cartons.
George Wooten, president of Wayne E. Bailey Produce Co., Chadbourn, N.C., said customers also are interested in convenience, which has led to the popularity of tray packs and steamer bags.
"We started doing a steamer bag five years ago and now all of our competitors do one as well," he said.
In addition, Wayne E. Bailey Produce Co. has trademarked and is selling a new size of sweet potato that it's calling "Petitelings." These sweet potatoes are a half-inch to 1 inch in diameter and 2-4 inches in length. These will ship in 10-, 20- and 40-pound bulk containers or 2-pound, 24-ounce or 36-ounce bags.


White, purple, fingerlings

Charlotte Vick, a partner in Vick Family Farms, Wilson, N.C., said the company offers a 40-pound carton, 3- and 5-pound bags, a 14- and 6-kilogram carton and steamer bags among its other offerings.
"We carry orange-flesh varieties, white and purple," she said. "We also are now offering a fingerling sweet potato in both a 3-pound bag (12/3s) and steamer bags."
Kelley Precythe, Southern Produce Distributors Inc., Faison, N.C., said the company offers traditional sweet potato packaging plus a newer 1.5-pound steamable bag.


Private labels, specialties

Thomas Joyner, general manager of Nash Produce Co., Nashville, N.C., said the company supplies electronically sized sweet potatoes, mesh bags, and microwavable as well as steamable sweet potatoes.
"We also are willing to work with customers with private labels," he said. "We are focusing this year on more specialty packs, including different varieties of sweet potatoes - particularly white- and purple-skin sweet potatoes."
Nash Produce also is developing a package that Joyner said the company hopes to introduce before the Produce Marketing Association's Fresh Summit this year.
Jimmy Burch, a co-owner of Burch Farms, Faison, N.C., said pricing has been flat for retail since last Thanksgiving and this shouldn't change with the new crops.