BC-Merc Table
BC-Merc Table

The Associated Press

CHICAGO




CHICAGO (AP) — Futures trading on the Chicago Mercantile Exchange Tue:
Open  High  Low  Settle   Chg.CATTLE                                  40,000 lbs.; cents per lb.                Apr      115.40 116.57 115.00 115.42   +.25Jun      105.32 106.67 104.90 105.25   —.05Aug      103.65 105.12 103.45 103.97   +.30Oct      107.52 108.55 107.35 107.52   —.05Dec      111.62 112.60 111.47 111.75   +.13Feb      113.45 114.65 113.32 113.85   +.28Apr      114.82 115.10 114.20 114.45   +.13Jun      108.22 109.05 108.00 108.72   +.32Aug      107.47 107.47 107.00 107.37   —.05Est. sales 68,259.  Mon.'s sales 61,892 Mon.'s open int 357,016,  up 1,286     FEEDER CATTLE                        50,000 lbs.; cents per lb.                Mar      135.00 135.80 135.00 135.42   +.50Apr      134.50 136.90 134.50 135.35   +.85May      135.55 137.87 135.42 136.35   +.80Aug      141.40 143.52 141.10 142.20  +1.05Sep      142.45 144.50 142.30 143.20   +.85Oct      142.75 144.77 142.50 143.50   +.93Nov      142.75 144.97 142.75 143.82  +1.02Jan      140.00 141.27 139.67 140.22   +.17Est. sales 13,101.  Mon.'s sales 13,573 Mon.'s open int 54,572,  up 587        HOGS,LEAN                                     40,000 lbs.; cents per lb.                Apr       58.00  58.22  57.52  57.75   —.27May       65.40  66.05  65.15  65.45   +.20Jun       74.15  74.75  73.72  74.02   —.08Jul       75.17  76.25  75.00  75.25   +.05Aug       75.30  76.25  75.10  75.30   +.05Oct       64.87  65.62  64.45  64.57   —.35Dec       60.10  60.62  59.57  59.77   —.25Feb       64.52  64.80  64.00  64.00   —.45Apr       67.72  68.22  67.67  67.70   —.42May       73.90  73.90  73.75  73.75   —.12Jun       76.90  76.90  76.90  76.90   —.10Jul                            77.17   —.45Est. sales 38,239.  Mon.'s sales 36,117 Mon.'s open int 236,954,  up 443       PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.