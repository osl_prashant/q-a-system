As farmers prepare for the first year of over-the-top dicamba application, questions cloud the horizon. With drift concerns at the forefront of many minds, uncertainties about stewardship and expectations bubble to the surface. University of Illinois Extension weed scientist Aaron Hager is in the weed control trenches with farmers. He, along with BASF and Monsanto address questions you might have this year.

What was the outcome of Monsanto releasing Xtend seed prior to the herbicide’s approval?

Aaron Hager: Honestly, I thought it was a bad decision. They claimed the seed was released because it is high yielding, but it was offered in areas with glyphosate-resistant weeds. If you can’t control weeds, the varieties won’t yield to their genetic potential. They oversold the product.

BASF: As this does not involve a BASF technology or product, we cannot make comments regarding another product. However, BASF is firmly committed to promoting the proper and legal use of its portfolio of products.

Monsanto: We’ve been developing our best products and germplasm for the Roundup Ready Xtend Crop System. Even without dicamba, growers were able to maximize their yield potential.

What stands out on the new dicamba labels?

Aaron Hager: There are restrictions for applications near sensitive areas and dicamba-sensitive crops, and sometimes language varies between labels. You don’t know ahead of time where your buffer is going to be—as wind direction changes, so does the buffer. Inversions are something to watch, too. If you would’ve taken a poll of 100 applicators a few years ago and asked if it was better to spray at 15 mph or zero, most would’ve said zero. Now labels indicate otherwise. I suspect many don’t understand inversions or how far they can move particles.

BASF: The supplemental labels have a two-year conditional registration—everyone must follow the label. The labels outline the best management practices for resistance management as well as application requirements. We’ve also provided resources to our field sales reps of buffer examples and categories of sensitive areas.

Monsanto: Monsanto and the industry are continuing to build upon ongoing education efforts on the approved label application requirements. Since December, we have been educating growers, applicators, dealers, retailers and other stakeholders on approved application requirements.

How does safety messaging change when it gets to the field?

Aaron Hager: I’ve heard representatives from both companies say “every herbicide has restrictions” and all that does is gloss over the importance of those restrictions. I’ve heard many company people talking about these labels and using the word “recommendation.” There’s a huge difference between a recommendation and a restriction—you can ignore a recommendation, but not a restriction.

BASF: The details on the label are a requirement. We stress the importance of covering those label requirements.

Monsanto: We have been working extensively to prepare customers, applicators, dealers and retailers for the upcoming season with education and training on the label and application requirements. Always read and follow directions for use on pesticide labeling.

How will you handle complaints?

BASF: BASF will investigate all alleged efficacy complaints and monitor for resistance. We will steward our product by investigating off-target allegations in an advisory capacity. We continue to reinforce the need to read label requirements and educate applicators on proper herbicide application through our On Target Application Academy (OTAA).

Monsanto: We trained 130 field engagement specialists who will be deployed regionally to answer claims called into the toll-free phone number on the label.

Will there be off-label spraying this year, and how will states find who’s to blame?

Aaron Hager: I’m concerned off-label spraying is inevitable. When symptoms of off-target exposure develop, it can be difficult to determine from where drift originated. If you see your neighbor spraying dicamba-tolerant soybeans, and seven days later see your soybeans cupped up, you’ll likely blame the neighbor you saw seven days ago. The problem is, three days before, you didn’t notice your other neighbor spraying the adjacent cornfield, and it was he who drifted.

BASF: It’s important to keep accurate and detailed records of pesticide application. You can investigate weather conditions at the time of application. Records help clarify where spray drift originated.

Monsanto: It’s up to states to determine appropriate measures for those who illegally spray, but we continue to support efforts to deter illegal pesticide applications. It’s our experience most farmers comply with the law and follow stewardship practices for all pesticide applications.

Are there any other key points to consider about this technology?

Aaron Hager: I get a lot of calls from farmers who haven’t read the label—it’s unnerving. Even though companies say it’s “low volatility,” that doesn’t mean no volatility. These formulations still have volatility issues and there will be problems—it’s a chemical process. Also remember this isn’t a stand-alone technology and treating it that way will hasten the evolution of resistance.

BASF: This technology is vital in controlling broad-leaf weeds—that’s why it’s so important to steward it. Engenia must be used as part of a comprehensive system, including other herbicides. We encourage users to learn to properly and effectively apply Engenia at a BASF OTAA session.

Monsanto: Stewardship is important to us, and we support farmers gathering as much information as they can before this growing season. With an approved EPA label, Monsanto and the industry are building upon ongoing education efforts specifically on the label application requirements.