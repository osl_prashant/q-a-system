US Forest Service interim chief confronts misconduct scandal
US Forest Service interim chief confronts misconduct scandal

By MATTHEW BROWN and DAN ELLIOTTAssociated Press
The Associated Press

BILLINGS, Mont.




BILLINGS, Mont. (AP) — After a career spent battling wildland fires across the West, U.S. Forest Service interim Chief Vicki Christiansen must now confront another sort of crisis: the harassment and bullying of women that's persisted in the agency's male-dominated culture despite numerous efforts at reform.
Christiansen's ascension to the top of the 35,000-employee agency follows the ignominious downfall this week of its former chief, Tony Tooke, amid allegations of relationships with subordinates.
Such misconduct appears to run deep in the agency's ranks, according to employee accounts, media reports and testimony during past congressional hearings. That's a reality Christiansen appeared to acknowledge when she told employees in a Friday email that they've had to face "some hard truths" about alleged harassment and retaliation.
With her decades working in fire suppression, a traditionally masculine arena, Christiansen knows firsthand about the barriers women face in the Forest Service, said Oklahoma state forester George Geissler, who met with Christiansen in Washington, D.C., just days before her appointment.
During that meeting, Christiansen openly discussed the challenges women face in the agency and made clear that harassment was not to be tolerated, Geissler said.
"She's willing to confront it and determined to confront it," he said. "She understands it's not just a Forest Service issue but an overarching issue that we as wildland firefighters, we as professionals in forestry, need to recognize."
The institutional problem the Forest Service faces is far from unique among government agencies. Sexual misconduct has long been a problem in the U.S. military and over the past several years emerged as a major issue at the Interior Department.
The Forest Service is part of the U.S. Department of Agriculture.
The agency's struggles date to at least the 1970s, when a class action lawsuit was filed alleging discrimination against women in hiring and promotions. The matter has gained renewed attention as female Forest Service employees recently stepped forward with tales of discrimination, harassment, retaliation and rape.
Lawmakers in Congress responded this week with calls for greater scrutiny of the agency.
Just over a third of the Forest Service's permanent employees are women, a figure that drops during the summer with additional seasonal hires, agency employment data shows. The balance is even more skewed among firefighters: Last summer, just 13 percent of the agency's 19,500 firefighters were women, according to the data.
Gail Kimbell, the first woman to be chief of the Forest Service, from 2007 to 2009, told The Associated Press she'd was heartbroken by accounts from female Forest Service firefighters who appeared on "PBS NewsHour" saying they were raped and groped by co-workers or supervisors and suffered retaliation if they reported it.
Kimbell said she experienced discrimination but wasn't assaulted during her 32-year career as a full-time Forest Service employee. In 1970s, women were a novelty in the workforce, especially in field work, she said. Unlike Christiansen, she did not work as a firefighter.
She said the Forest Service has spent decades trying to improve the workplace for women. By the time Kimbell became chief, training and other programs were well established, she said, and she supported them.
"The Forest Service continues to put effort into it. It's not perfect but it's a lot better than it was in the 1970s," she said.
In announcing Christiansen's appointment, Agriculture Secretary Sonny Perdue tasked her with two goals: improving the agency's response to sexual misconduct while effectively managing more than 300,000 square miles (777,000 square kilometers) of forests and grasslands in 43 states and Puerto Rico.
Tooke's case and other reports of harassment have underscored that there's more work to do. During a 2016 congressional hearing, agency officials said 67 employees had been cited for sexual misconduct over the prior three years, including 21 who were fired and 28 who were suspended. The agency did not respond to requests for more recent figures.
In November, the Forest Service opened a "Harassment Reporting Center" with a toll-free number to make it easier for victims to report mistreatment. And on March 1, it started a one-year trial program in which only outside investigators will be used to look into sexual harassment and misconduct complaints originating in its Pacific Southwest region.
The move came in response to an audit from the inspector general at the Department of Agriculture that said almost half of service employees interviewed in the region had expressed distrust in complaint reporting progress.
The Forest Service and Agriculture Department have refused to say if an investigation into Tooke's actions was being handled by an outside entity or if it would continue upon his retirement.
In her Friday email to Forest Service employees, Christiansen pledged to stick by workers who "stand up for their colleagues and themselves."
"We've had to face some hard truths about allegations of harassment and retaliation in our agency, even as we stare down some of the biggest land-management challenges in our nation's history," she wrote. "I know we are up to the task.
___
Elliott reported from Denver.
___
Follow Matthew Brown on Twitter at www.twitter.com/matthewbrownap and Dan Elliott at www.twitter.com/DanElliottAP.