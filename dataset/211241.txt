Drought in Montana led multiple wildfires in eastern Montana burning more than 400 square miles of pasture and cropland. In all, the state has seen more than 575 square miles burn in the past few weeks.The largest fire is the Lodgepole Complex fire, it has burned 270,200 acres as of Friday, July 28 at 10:00 a.m. MST.  The Lodgepole Complex fire is currently at 80% containment, according to National Interagency Fire Center.
In the past two weeks there have been 23 wildfires listed by the National Interagency Fire Center in Montana totaling 368,127 acres. Besides the Lodgepole Complex fire, the other fires have ranged in size from 44 acres to 28,957 acres. Many of the other wildfires are taking place in national forests like eight in Lolo National Forest, four in Helena-Lewis and Clark National Forest, two in Beaverhead/Deerlodge National Forest, one in Bitterroot National Forest and one in Custer Gallatin National Forest.

The Northern Plains region of the U.S. has an elevated risk of wildfires after dealing with drought for much of the spring and summer.
Montana is experiencing extreme drought in 23.89% of the state, according to the latest Drought Monitor released on Thursday by the National Drought Mitigation Center. Conditions have gotten worse lately with the most severe level of exceptional drought at 11.87%, it has increased more than 10% from the previous week.

Neighboring North Dakota is unusually dry as well with nearly twice the amount of extreme drought at 45.56%. North Dakota has 7.62% of the state classified as being in exceptional drought.
South Dakota is in slightly better shape with just 14.96% of the state in extreme drought and none of the state in exceptional drought.
Donations are being gathered by several local cattlemen’s associations and other agriculture businesses in Montana to help producers impacted by the fires:
Petroleum Co. Stockgrowers
Contact Sig Pugrud, 406-429-7801
 
Montana Stock Growers Foundation
Contact 406-442-3420
Donations can be sent to 420 N California St Helena, MT 59601
 
Garfield County Fire Foundation
Checks can be made to Garfield County Fire Foundation c/o Garfield County Bank or send to Circle c/o Redwater Valley Bank
 
Farmers Union Oil/Cenex in Jordan
Cash and credit card donations are being accepted for fuel
Contact 406-557-2215
 
Fencing Donations
Anne Miller 406-853-3610
Reeverts Fencing LLC 406-487-2363
 
Hay, trucking and cattle fencing
Jana Hance at Redwater Valley Bank 406-485-4782
Bart Meged 406-951-3005
Misty Meged 406-951-4840
 
On Friday, CHS Inc. announced it would be donating $100,000 to ranchers impacted by wildfire in eastern Montana. The contributions will be given to livestock producers through the CHS Payback program that discounts feed purchased at local feed dealers.
"Ranchers across eastern Montana have been working around the clock to defend their land and livestock from the wildfires," says Rod Paulson, vice president, CHS Processing. "Long after the blazes subside, many of these hard-hit ranchers will need assistance to rebuild herds, fence and other infrastructure. Through this contribution, we hope to alleviate some of the recovery costs as these ranchers work to restore their livelihoods."
There will be an opportunity for those around the Helena, Mt. area to donate to the fire relief cause by participating in Pint Night to Aid Fire Relief Efforts in Eastern Montana at the Ten Mile Creek Brewery on Aug. 1. From 5-8 pm, Ten Mile Creek Brewery will donate $1 from every beer sold to aid in rebuilding in the wake of the Lodgepole Complex fire.