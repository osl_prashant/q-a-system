An oversupply faced by conventional Brussels sprouts growers also is affecting the organic market.
"Our Brussels sprouts crop looks the best it has in years," Brian Peixoto, sales manager for Watsonville, Calif.-based Lakeside Organic Gardens, said in an e-mail.
"(But) the category is doing terrible. Prices are the worst we've seen in years."
Still, he said the category is growing and will continue to do so. When asked if organic Brussels sprouts were continuing to trend upward given their "superfood" status, Peixoto said it's "debatable. Foodies love them, but (I) wouldn't say trending upward."
Lakeside Organic grows all of its product in Watsonville, he said.
"We only pack in 10-pound bulk," Peixoto said.
"All our sprouts are harvested and packed in (the) field. It is a very labor-intensive process â€¦ but our harvesters take great pride in their work. We, in turn, are able to put out very high quality sprouts."
Several companies that deal with conventional Brussels sprouts are also involved in organics.
Diana McClean, director of marketing for Castroville, Calif.-based Ocean Mist Farms said in an e-mail that "We believe the organic category is doing very well and have most recently added both Brussels sprouts and radishes to the Ocean Mist Organic line of 26 items."
"Ocean Mist Farms has the land and harvest capabilities to grow organic produce in various regions including Coachella, Calif., and California Baja, Mexico, which supports the consumer demand for a wide variety of organic offerings."
Don Hobson, vice president of sales and marketing for Oxnard-based Boskovich Farms Inc. said there was plenty of organic product in the market.
"It's a night-and-day difference from last year," he said.
Robert Schueller, director of public relations for Los Angeles-based World Variety Produce, which markets under the Melissa's label, said the company has seen an increased demand in the marketplace for organic product, and more growers are coming in as certified organic.
"Now, we're carrying organic on a year-round basis when it was highly seasonal in the past," he said.
"We get half-season from U.S., half-season from Mexico. All are USDA certified organic because the marketplace demands it."