Washington governor to roll out measures to protect orcas
Washington governor to roll out measures to protect orcas

By PHUONG LEAssociated Press
The Associated Press

SEATTLE




SEATTLE (AP) — With the number of endangered Puget Sound orcas at a 30-year low, Washington Gov. Jay Inslee is expected Wednesday to issue an executive order calling for more state actions to protect the struggling whales.
The fish-eating orcas that spend time in Puget Sound have struggled for years because of lack of food, pollution, noise and disturbances from vessels. There are now just 76, down from 98 in 1995.
Inslee intends to announce the executive order at a news conference in Seattle.
It will direct state agencies to take immediate steps while identifying long-term solutions to help the whales and setting up a task force with state agencies, tribal leaders and others to come up with recommendations.
The Legislature passed a supplemental budget Friday that includes money for increased marine patrols to see that boats keep their distance from the orcas and to boost hatchery production of fish that the orcas prefer to eat.
The governor's proposed budget also included funds for orca protections.
Many people have been sounding the alarm about the orcas' plight since the September death of a juvenile dropped the population to 76.
A baby orca has not been born in the past few years. Half of the calves born during a celebrated baby boom several years ago have died. Female orcas are also having pregnancy problems linked to nutritional stress brought on by a low supply of chinook salmon, the whales' preferred food, a recent study found.
Last year, the endangered orcas spent the fewest number of days in the central Salish Sea in four decades, mostly because there wasn't enough salmon to eat, according to the Center for Whale Research, which keeps the whale census for the federal government.
"I applaud anything that helps (the orcas) through the short term, but the long term is what we really have to look at — and that's the restoration of wild salmon stocks throughout Washington state," Ken Balcomb, senior scientist with the Center for Whale Research, said Tuesday.
Balcomb and others say aggressive measures are needed and they have called for the removal of dams on the Snake River to restore those salmon runs.
One House bill set aside $1.5 million to produce 10 million more hatchery chinook salmon — a roughly 20 percent boost —but it didn't get a House floor vote. The recent supplemental budget passed by the Legislature includes $837,000 for increased hatchery production.
Recreational and commercial fishermen groups told lawmakers that increasing salmon supplies would benefit fishermen and orcas.
Orcas use clicks, calls and other sounds to navigate, communicate and forage mainly for salmon. Noise from vessels can interfere with that.