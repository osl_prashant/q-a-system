"This is stupid. [I'm] not even 10% done with soybean harvest."


This is stupid, not even 10% done with soybean #harvest16 pic.twitter.com/Qjq1Uo3to4
‚Äî matt bainbridge (@BainbridgeAG) October 6, 2016
Such was the lament of South Dakota farmer Matt Bainbridge, who shared a photo on Twitter yesterday of his truck lightly dusted with snow.

And Bainbridge is not alone. Combines are rolling, with 24% of corn harvest and 26% of soybean harvest complete for the week ending Oct. 2, according to USDA. But Mother Nature is doing her best to create a disruption or two. Most of the Corn Belt got at least one drink this past week.

Also this week, Hurricane Matthew provides a not-so-subtle look atjust how disruptive weather can be, with more than a million Floridians evacuated and southeastern cotton and tobacco farmers rushingto harvest their crops ahead of the storm.
A recent Pulse poll seemed to confirm: Farmers are more confident dealing with yield limiters they can control, like diseases, insects, weeds and other crop pests. And tothe contrary, you can hope the weather will play nice, but such bargains come with no guarantees.
The poll asked farmers, "What was your biggest yield limiter this crop season?" According to the more than 1,100 respondents, about two-thirds (68%) say weather will take the largest bite out of yields. Diseases accounted for 10% of the responses, weeds 7%, insects 2%, and the "other" category filled out the remaining 12%.
Interested in becoming part of the heartbeat of our new national producer panel that speaks up and speaks out? Participating is as easy as answering two quick poll questions per month via text message from your mobile phone. Visit www.agweb.com/farmjournal/farm-journal-pulse/ to get started.