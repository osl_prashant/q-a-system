Ugly delicacy? Industry touts weird-looking Monkfish
Ugly delicacy? Industry touts weird-looking Monkfish

By PATRICK WHITTLEAssociated Press
The Associated Press

PORTLAND, Maine




PORTLAND, Maine (AP) — Now serving sea monsters.
That's the message from members of the fishing industry, environmentalists and regulators who are trying to persuade U.S. consumers to eat more of a particularly weird-looking creature from the deep — monkfish.
Monkfish have been commercially fished for years, but recent analyses by the federal government show the monster-like bottom dweller can withstand more fishing pressure. However, U.S. fishermen often fall short of their quota for the fish.
A lack of reliable markets for the fish and convoluted fishing regulations make it difficult to catch the full quota, fishermen said. Nevertheless, the U.S. government is upping harvesters' limits for monkfish for the next three years.
Some New England fishermen switched to targeting monkfish in recent decades when traditional species such as cod began to decline, said Jan Margeson, a Chatham, Massachusetts, fisherman who made such a switch himself. He said the availability of monkfish represents an opportunity for the industry.
"It is healthy. We can't even catch the quota," he said. "We had to find an alternative species once groundfish died years ago."
Monkfish, also known as goosefish, are predatory fish that camouflage themselves on the ocean bottom and can grow to be about 5 feet long. With a gaping maw and uneven, jagged teeth, its appearance is the stuff of nightmares.
But proponents often say the taste and texture of its flesh is similar to lobster. And monkfish, which is often sold as a whole fish or as steaks of tail meat, frequently is more affordable than some other kinds of domestic seafood.
Tails typically sell for about $7 per pound at New England fish markets where popular items such as lobsters and flounder sell for $10 per pound or more.
The fish is brought to shore from Maine to North Carolina, with most coming to land in Massachusetts.
Fishermen have caught more than 15 million pounds of the fish every year since 1987. They were allowed to catch 32.5 million pounds of monkfish each year from 2013 to 2015, but typically caught less than two thirds of that amount. The federal government increased that limit to about 33.8 million pounds for the 2017-18 fishing year, and that number will hold until 2020.
The Environmental Defense Fund Seafood Selector and Monterey Bay Aquarium Seafood Watch both give the fishery positive reviews for sustainability. The National Oceanic and Atmospheric Administration also touts the fishery as a "smart seafood choice" that is "sustainably managed" according to federal guidelines, the agency says on its website.
Right now is a good time for fishermen to start exploiting that reputation, said Ben Martens, executive director of the Maine Coast Fishermen's Association.
"When we talk about diversification, monkfish is one of the things," he said. "It's a fishery that has opportunity for fishermen right now."