On Tuesday, parts of western Oklahoma received badly needed rain after the winter wheat crop has been struggling this winter. According to the latest U.S. Drought Monitor, more than 25 counties in the Sooner State are experiencing extreme (D3) and exceptional (D4) drought.

In the Crop Conditions Report for Oklahoma, more than 50 percent of the wheat crop is rated poor to very poor. There is no wheat in the state rated excellent, and only 9 percent is rated good.

While one-third of the state is experiencing dire drought, the impact on Oklahoma cattle producers may be limited.

“The next few weeks will be very critical in terms of whether or not they can move forward with normal production plans or whether they really have to go into drought contingency plans,” said Derrell Peel, livestock marketing specialist with Oklahoma State University.

The next U.S. Drought Monitor will be released on Thursday.