The Latest: US soybean group calls tariffs 'devastating'
The Latest: US soybean group calls tariffs 'devastating'

The Associated Press

WASHINGTON




WASHINGTON (AP) — The Latest on U.S.-China trade tensions (all times local):
___
12:45 p.m.
The American Soybean Association, a lobbying group that says it represents 21,000 U.S. soybean producers, says China's proposed 25-percent tariff on soybeans would be "devastating" to U.S. farmers. China is the largest consumer of U.S. soybeans, buying about one-third of all U.S. soybean production each year, the group says.
Association President John Heisdorffer, an Iowa farmer, is calling on the Trump administration to withdraw its proposed tariffs and meet with soybean farmers to discuss ways to improve competitiveness without resorting to tariffs.
The association says soybean farmers lost an estimated $1.72 billion on Wednesday morning alone as soybean futures tumbled.
"That's real money lost for farmers, and it is entirely preventable," Heisdorffer says in a statement.
___
12:10 p.m.
Ford and General Motors are calling for continued dialogue to resolve escalating trade tensions between the U.S. and China.
Both automakers issued statements Wednesday after China proposed steep retaliatory tariffs on imports of U.S.-made vehicles and other goods.
Ford says it encouraged the governments to work together. GM urges the countries to "engage in constructive dialogue and pursue sustainable trade policies."
GM would be affected by U.S. tariffs because it imports the Buick Envision SUV from China. Ford has plans to import a new version of the Focus compact car from China next year. Both make most of their vehicles in China that are sold there and ship only a small number of vehicles from the U.S. to China.
___
11:45 a.m.
National Economic Council Director Larry Kudlow is suggesting that proposed tariffs on Chinese imports may not ultimately go into effect.
Kudlow is trying to soothe stock market fears over a potential trade war with China. He says the tariffs announced Tuesday are "potentially" a negotiating ploy in an effort to get China to level the playing field for U.S. businesses.
Kudlow tells reporters: "There are carrots and sticks in life," adding that the U.S. is encouraging China to lower trade barriers.
He adds that China should take Trump "seriously" on tariffs, but that ultimate the president is a "free-trader."
Kudlow says the Trump administration is focused on growing the U.S. economy, saying: "Sometimes the path to this kind of growth is a little rocky. That's the way the world works."
____
11:30 a.m.
Boeing shares are sinking after China proposed steep tariffs on U.S. aircraft as trade tension grows between the United States and China.
China is a key market for Boeing, accounting for nearly one-fourth of the Chicago company's deliveries of commercial airplanes last year.
Chinese tariffs on U.S.-made planes are not certain, and the proposal only covers planes within a certain weight range. Analysts say the tariffs would hit older versions of the Boeing 737 that are still in production, but not newer MAX models. Larger wide-body aircraft would not be affected.
Still, the threat that Boeing Co. could be caught in trade war crossfire sent the shares down nearly 6 percent in morning trading. They regained some ground by late morning — down $9.65, or 2.9 percent, to $321.17.
___
10:35 a.m.
Analysts at Sanford C. Bernstein are saying that BMW, Mercedes-Benz and Tesla are the biggest potential losers from higher Chinese tariffs on U.S. auto imports.
Bernstein said in a note Wednesday that BMW sends 89,000 vehicles annually from the U.S. to China while Daimler AG's Mercedes-Benz ships 65,000. Tesla sells about 14,000 but China accounts for around 15 percent of forecast Model S and X sales this year.
If enforced, the measures announced by Beijing earlier would increase tariffs on cars from the United States to 50 percent from the usual 25 percent.
BMW is headquartered in Munich, Germany but claims the title of biggest U.S. car exporter by shipping vehicles including its X5 SUV from its Spartanburg, South Carolina plant. Stuttgart-based Daimler builds Mercedes in Vance, Alabama.
The Bernstein team raised the question of whether the threat of a U.S-China trade war could "nudge the Germans toward localization" — that is, moving production to China.
___
10:00 a.m.
China says it has made a "request for consultations" at the World Trade Organization in response to U.S. tariffs against Chinese products.
The Chinese Ministry of Commerce confirmed the step taken Wednesday at the Geneva-based trade body, which triggers the WTO's dispute settlement mechanism that will begin with consultations between the two countries.
China said the request centers on a proposed product list subject to the additional tariffs under U.S. Section 301 rules.
___
8:15 a.m.
Commerce Secretary Wilbur Ross is brushing off concern over trade war with China. In an interview with CNBC Wednesday morning, Ross said that tariffs imposed by China amount to 0.3 percent of U.S. GDP and that some action on tariffs has been "coming for a while."
"What we're talking about on both sides is a fraction of 1 percent of both economies," he said.
The larger concern, Ross said, is the protection of U.S. intellectual property.
Still, U.S. stock futures slumped over concerns that the back-and-forth tariff actions will stunt trade and growth. Ross said he would not comment on the stock market's reaction, but then said he thinks "it's being out of proportion."
___
All times below this point are local time for Beijing.
___
7:50 p.m.
President Donald Trump says the U.S. lost a trade war with China "years ago."
In a tweet Wednesday after China announced a list of U.S. products that might be subject to a 25 percent tariff, Trump said: "We are not in a trade war with China, that war was lost many years ago by the foolish, or incompetent, people who represented the U.S."
China announced tariffs worth $50 billion on a series of U.S. products including soybeans, whiskey and cars.
Chinese officials said they were obliged to act after the U.S. announced plans for retaliatory tariffs in an escalating dispute over China's technology program and other trade issues.
___
7:00 p.m.
The chairman of the American Chamber of Commerce in China says Beijing's potential retaliation in an escalating dispute with Washington is a concern for every U.S.  company doing business in China.
William Zarit told The Associated Press in an interview Wednesday that proposed tit-for-tat tariff hikes by both sides would first hit global stock markets.
Zarit said he still hopes Washington and Beijing will not follow through on their threats.
Instead they should work toward a "serious negotiation."
U.S. business groups mostly agree that something needs to be done about China's aggressive push in technology. But they worry that China's measures targeting U.S. exports of aircraft, soybeans and other products could help bring on a tit-for-tat trade war of escalating sanctions between the world's two biggest economies.
___
6:40 p.m.
A senior Chinese commerce official has defended the official technology policy at the center of an escalating trade dispute with the United States.
Wang Shouwen, a deputy commerce minister, told reporters Wednesday that the long-range industrial strategy known as "Made in China 2025" is open to foreign companies, including American firms, and to private companies, not just state-owned enterprises.
The policy calls for creating Chinese global leaders in electric cars, robots and other fields.
Wang said the plan, which lays out specific targets for domestic brands' share of the market in some sectors, should be seen as a guide rather than mandatory.
Foreign business groups complain that strategy will limit or outright block access to those industries.
___
6:00 p.m.
A Chinese foreign ministry spokesman has urged the U.S. to come to the table and discuss an escalating trade dispute.
Ministry spokesman Geng Shuang told reporters in Beijing on Wednesday that China's door to dialogue with Washington remains open "but the U.S. has missed the opportunity time and time again."
Geng made the comments after Beijing announced plans to raise tariffs on imports from the U.S.
He said talks between the two countries require "mutual respect and equal treatment, instead of being coerced by one party unilaterally and condescendingly."
Geng added that neither side should be "threatening the other with senseless and unreasonable demands."
___
5:50 p.m.
European stock markets have fallen sharply after China announced a series of tariffs on U.S. goods, stoking fears that the two countries were on the brink of a full-scale trade war.
Among the region's major indexes, Germany's DAX was down 1.2 percent while the FTSE 100 index fell 0.4 percent, in the wake of China's decision to raise tariffs on $50 billion of U.S. goods including soybeans, aircraft and automobiles. China was responding to Washington's equivalent duties on Chinese goods.
Joshua Mahony, market analyst at IG, said it's "no surprise that we are seeing traders flock to safe havens in response, with gold and the yen both sharply higher throughout the morning's trade."
Gold, for example, was up 0.8 percent at $1,347.40 an ounce.
___
5:30 p.m.
A senior Chinese finance official has warned that attempts by foreign countries to pressure China on trade will fail, striking a firm note as Beijing announced proposed tariff hikes on American goods in response to planned U.S. duties.
Zhu Guangyao, China's deputy finance minister, said Wednesday that Beijing hopes both sides can work together in a constructive manner, "instead of acting in a willful way."
He made the comment at a briefing to discuss China's plans to impose a 25 percent tariff hike on U.S. soybeans, autos, aircraft and other goods valued at $50 billion.
Zhu said that China never gives in to outside pressure and that "pressure from the outside will only urge and encourage the Chinese people to work even harder" and to innovate and develop.
___
5:15 p.m.
A senior official with China's Commerce Ministry says Beijing will keep the door to negotiations with the U.S. open, after announcing a plan to impose tariffs on $50 billion worth of U.S. goods in response to proposed U.S. tariffs.
Wang Shouwen, a vice minister of Commerce, said Wednesday at a briefing with reporters that Beijing doesn't want a trade war but will fight one if forced to.
Wang said Beijing was willing to resolve these disputes with the U.S. through dialogue on the basis of "mutual respect and mutual benefit."
Wang said: "If anyone wants to fight, we'll be there with him, if they want to negotiate, the door is open."
___
4:20 p.m.
China has raised tariffs on $50 billion of U.S. goods including soybeans, aircraft and automobiles in response to Washington's increased duties on Chinese goods in a technology dispute.
The Commerce Ministry on Wednesday criticized the U.S. move as a violation of global trade rules and said China was acting to protect its "legitimate rights and interests."
It said a 25 percent tariff would be imposed and the date the charges will take effect would be announced later.
The dispute stems from U.S. complaints that Beijing pressures foreign companies to hand over technology in return for market access.
Companies and investors worry the conflict could dampen worldwide commerce and set back the global economic recovery.