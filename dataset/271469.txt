Proactive planning and attention to detail at planting can help growers achieve maximum yield potential. 
There are four key yield components that growers can positively or negatively impact by their agronomic management decisions, according to Jeremy Hogan, BASF Technical Marketing Agronomist. These four components include:

Ear count – uneven stands can produce lower yields due to inconsistent or reduced ear count, so it’s imperative to ensure uniform germination and emergence.
Maximizing the number of rows per ear – early season stresses during row determination can negatively influence corn row number. 
Maximizing the number of kernels per row– consider what can be done before, during and after pollination to ensure the maximum number of kernels are established and pollinated.
Maximizing seed size and weight – both size and weight can make a dramatic difference in the overall yield potential, so it’s critical that growers protect their crop down the homestretch.

 
Agronomic Tips to Maximize Ear Count
Uniform emergence and spacing is critical when planting seed; therefore, it’s important to plant into a uniform seedbed. A cloddy seedbed due to inconsistent tillage may result in temperature variation, which can cause seeds to germinate and emerge at different times. Ensure planting depth, seed quality and seed furrow closure are all monitored. Winter annual weed pressure as well as early season insect pests must also be managed in order to achieve uniform growth and development.
“Some agronomic factors can be corrected as the planter is rolling through the field, but often we need to remind growers to take time to slow down and get out of the tractor and do some digging behind the planter to see and understand how some of these factors could be impacting overall yield potential,” Hogan says. “Paying close attention to these details can make a big difference at the end of the season.” 
Residue is another factor that if not managed and monitored, could create a favorable microenvironment for fungal pathogens to attack young corn seedlings, resulting in seedling blight. Delays in seedling growth and development could occur, along with increased risk of reduced ear count.
 
Managing Conditions Favoring Seedling Blight
Hogan encourages growers to protect their yield by protecting seedling health. 
“As far as residue management, that can be managed with row cleaners, but we also have some environments with poorly drained soils that favor fungal pathogen development,” Hogan says. “For those areas, BASF does have a spectrum of products to help reduce the risk of seedling blight or other fungal pathogens that may impact crop emergence.”
Xanthion® in-furrow fungicide provides extended disease protection against fungal pathogens and root diseases, including Rhizoctonia and Fusarium spp., and even suppression of Pythium spp. In addition, Xanthion in-furrow fungicide has helped increase root growth and seedling vigor under some environmental conditions, and it has also helped increase cold tolerance. 
“Under cool, wet conditions, an application of Xanthion in-furrow fungicide provides additional Plant Health benefits. For example, it can help increase crop emergence consistency, even in the absence of fungal pathogens,” Hogan says. 
Manticor™ LFR® in-furrow fungicide/insecticide defends corn seedlings from early season corn disease as well as belowground insect pressure in one liquid-fertilizer-ready (LFR) formulation. Manticor LFR in-furrow fungicide/insecticide provides longer protection and a larger zone of seedling protection. 
“Anytime we can provide enhanced root growth and development and seedling vigor right out of the gate, it will result in the opportunity for enhanced root growth as we enter the V2 growth stage. As we transition from seedling roots to nodal roots, it’s imperative that we have a healthy root system from the time the radicle and seminal roots come out,” Hogan says. “This will ensure good nodal root establishment which is important for water and nutrient update throughout the early vegetative growth stages. 

To learn more about the root growth benefits of in-furrow fungicides, click here.
 
Manticor is a Restricted Use Pesticide. 
 
Always read and follow label directions.
Xanthion is registered trademarks of BASF. Manticor is a trademark of BASF. LFR is a registered trademark of FMC Corporation
 
Sponsored by BASF