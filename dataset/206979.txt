Federal police in Brazil sent a formal request to prosecutors to pursue insider trading charges against Wesley and Joesley Batista, owners of JBS SA.
On Sept. 21, police accused both Batista brothers of illegally profiting on trades made prior to a plea bargain deal in mid-May. Police allege the brothers saved $44 million by doing illegal trades prior to the announcement of the plea agreement, according to a report by Reuters.
The plea deal was arranged with federal prosecutors after Wesley and Joesley Batista admitted to bribing 1,900 politicians. The corruption scandal goes all the way to the top of Brazilian politics with President Michel Temer’s alleged involvement.
Joesley Batista was the first to go to jail on Sept. 10 when he turned himself into authorities when it was revealed he may have taken advantage of prosecutors during the plea deal by withholding information. Wesley Batista was then arrested on Sept. 13 for insider trading and a warrant was issued for Joesley’s involvement.
A request to release the brothers from jail was denied on Sept. 22 by the Supreme Court, upholding a decision made by a lower court earlier in the week.
By law, prosecutors in Brazil are left to handle lodging formal charges once a police investigation has concluded.
Following the legal developments JBS has restructured the business by removing Wesley as the CEO and bringing back his father and company founder, José Batista Sobrinho, to run the world's largest meat packer.
Here is a list of some of the latest headlines involving JBS and the Batista family:
JBS Names Founder José Batista Sobrinho as CEO, Two Sons in Jail
Brazilian Authorities Arrest JBS CEO Batista for Insider Trading
Billionaire Co-owner of JBS Gives Himself up to Police in Brazil
Brazil's JBS Sets Up Independent Governance Board for U.S. Unit
JBS Plans to List U.S. Unit as Brazil Presses for CEO's Ouster
JBS Sells Canadian Cattle Feeding Business for $40 Million
Brazilian Meatpacker JBS Unveils $1.8 Billion Divestment Plan

JBS Sells Beef Units In Three Countries


Brazil's JBS says Joesley Batista Resigns as Chairman

Brazil Meat Scandal: 63 Indictments