RJ Deakins of Houweling’s is retiring after nearly four decades in the produce trade. Photo courtesy Houweling’s Group 
 
After 39 years in fresh produce sales management, RJ Deakins of Houweling’s Group is retiring.
A member of Houweling’s sales staff since December 2011, Deakins’ history in the produce industry extends back to September of 1979, with Sunkist.
Over the next nearly four decades, his career saw him spend time at Perricone, Genova & Associates, Eco Farms, Oceanside Produce, Sun Pacific and six years at Houweling’s Group, according to a news release.
Deakins is a body surfer, competing in last year’s Manhattan Beach Gillis Body Surfing Competition, where he finished third in the master’s division.
He swims every day, enjoys golf, can talk Lakers basketball with anyone and is an avid reader and music lover, the release said.
In retirement he plans to continue to pursue his athletic and intellectual passions, while spending more time with his wife Ruthie, and their adult children Casey and Brooke, the release said.