The U.S. Department of Agriculture has filed an administrative action under the Perishable Agricultural Commodities Act against American Fruit & Produce Corp.
 
The Florida company allegedly failed to make payment promptly to 38 produce sellers in the amount of $2.84 million from April 2015 through May 2016, according to a news release.
 
American Fruit & Produce will have an opportunity to request a hearing, according to the release.
 
If USDA finds the company committed repeated and flagrant violations, the firm would be barred from the produce industry for two years and its principals could not be employed by or affiliated with any PACA licensee for one year, according to the release. Even then, principals could be employed only with the posting of a USDA-approved surety bond.
 
Parties interested in the case can contact USDA PACA officials by phone at (202) 720-6873 or by email at PACAInvestigations@ams.usda.gov.