Brooks focuses on its Slimcados
Brooks Tropicals Inc., Homestead, Fla., is featuring its Slimacados green-skin, Florida-grown avocados in its monthly social media promotions, including Facebook and Pinterest, this summer, said Mary Ostlund, director of marketing.
Slimcados and salads are in the spotlight for August, she said. July featured Slimcados in nondairy appetizers with a theme of "Party Till the Cows Come Home."
"We get over 50,000 likes when we feature Slimcados," she said.
The fruit should be available until March, with promotable volume available through the holidays.
 
Calavo opens Florida facility
Santa Paula, Calif.-based Calavo Growers Inc. has opened a value-added facility in Jacksonville, Fla., said Rob Wedin, vice president of sales and marketing.
It's a joint facility for ripening, bagging and distribution that will be operated with Calavo Foods' Renaissance Food Group subsidiary, which is developing an operation there for its fresh-cut fruit and other products, he said.
"That's definitely a fast-growing area, and we feel good about the investment we made there," Wedin added.
 
Commission notes fruitful season
The California Avocado Commission, Irvine, reviewed another successful year as the California season wound down.
"The highlight of the season was the consistently good quality of locally grown California avocados," said Jan DeLyser, vice president of marketing.
Highlights from a marketing standpoint included a new consumer advertising campaign with the theme "California by Nature," outstanding influencer outreach for the fourth year of the commission's June California Avocado Month program, which reached millions of consumers, and the strongest California avocado Fourth of July shipments on record, she said.
 
Giumarra adds cooler capacity
Giumarra Agricom International, Escondido, Calif., has added ripening capability and cooler space at its Escondido facility, said Gary Caloroso, marketing director.
"That's very important for us as we continue to grow," he said.
The company also operates a California packinghouse in Ventura, with other facilities throughout the U.S.
As the California season winds down, the company will rely more on imports from Mexico, Chile and Peru, Caloroso said.
"We bring in Mexico avocados every week of the year through Texas," he said.
 
Health Avocado sees solid growth
Berkeley, Calif.-based Healthy Avocado Inc. expects its business to grow this season as a result of increased sales to major retailers, including club stores, said president Paul Weismann.
Weismann said the best way to grow a business is through in-person contact, so he plans to spend a lot of time on the road cementing relationships with customers and growers.
The company also maintains a strong social media presence, he said, and disseminates comprehensive price lists and publishes blogs that give customers an accurate depiction of market conditions so they can make better-informed buying decisions.
"You have to inform the customer about what's happening," Weismann said. "Market information is more and more important."
 
Henry Avocado adds Houston plant
With the addition of a facility in Houston last fall, Escondido, Calif.-based Henry Avocado Corp. now has 95 ripening rooms, said president Phil Henry.
The company also has two ripening facilities in Escondido and one each in Milpitas, Calif.; Phoenix; and San Antonio.
About 70% of the company's avocados are custom ripened, he said.
 
Index Fresh hires three staffers
Index Fresh Inc., Riverside, Calif., has hired two new sales account managers and a food safety manager, said president Dana Thomas. Bobby Fingerlin has joined the company's sales staff from outside the industry, and Matt Golden is a recent graduate of California State Polytechnic University, Pomona.
Eva Kaudze, most recently with Corona-College Heights Orange and Lemon Association, Riverside, Calif., has been named food safety manager.
Index Fresh also plans to introduce a 12.5-pound standard flat box for the Chinese market. The company does substantial marketing to Hong Kong, Taiwan and Japan and now is entering the Chinese market with a box with graphics designed to appeal to customers in that country, Thomas said.
 
New Limeco updates website
New Limeco LLC, Princeton, Fla., has unveiled a new website, newlimeco.com, said Eddie Caram, general manager. The site includes a wealth of product information, a tropical fruit blog and facts about the company, pointing out, for example, that New Limeco has received Non-GMO Project Verified certification on its avocados and is Primus Labs and hazard analysis and critical control point certified.
The firm also has hired Allan Sanchez, most recently with Miami-based Team Produce International, as sales manager.
 
West Pak adds two salespeople
West Pak Avocado Inc., Murrieta, Calif., has hired two new sales staffers and revamped its website, said Doug Meyer, vice president of sales and marketing.
Susie Rea, most recently with the San Juan Capistrano, Calif., office of Pompano Beach Fla.-based Ayco Farms Inc., and Marlon Abarca, most recently with Vernon, Calif.-based Giumarra International Berry LLC, have joined the company's sales and business development group.
The company also has upgraded its website, westpakavocado.com, which features recipes, a ripening guide and information for growers and consumers. The new site features responsive web design technology, which allows for easy access by mobile devices, desktop or laptop computers or other devices, Meyer said.