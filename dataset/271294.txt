Maybe you’ve heard about it? A woman blogger started a website called “People I Want to Punch in the Throat,” followed by publication of a book by the same title.She’s been described as “an acerbic with a surprising warmth.” Her website and book don’t actually name names; rather, the author/blogger targets entire categories of people whom she feels would benefit from a punch in the throat.
Here’s a couple examples:
People Who Complain They’re Busy But They’re Busy with Stupid Stuff. “I’m sooooo busy, because Eustace and Duncan and Dorset have Tae Kwon Do on Mondays, baseball practice on Tuesdays, violin and cello on Mondays, Wednesdays and Fridays, soccer all day on Saturdays, baseball for the OTHER team they play for Wednesdays, and Kumon on Fridays.”
People Who Treat Their Dogs Like Children. “A dog is not a child. A child is that tiny human being that lives in my house. It walks and talks and poops in a toilet (finally!). It cannot be left alone for the day chained in the yard with a bowl of water and a rawhide bone. It doesn’t sleep in a cage or in my bed. It has never chewed up my shoes or drank out of the toilet.”
Douchey Dads. “I was attending an auction at a chic country club and I was surprised to find the bar full of young, well-dressed (if you can call expensive plaid shorts well dressed), golf playing, thousands of dollars a year for dues paying men sitting around drinking and yukking it up. I wanted to say, ‘Hey ... where do you guys work that you can spend half of a Tuesday golfing at this expensive club?’ ”
I don’t think it’s a stretch to attribute the success of her project to the fact that expressing our innermost, visceral loathing for other people can be a very satisfying pastime –only most of us can’t pursue that hobby quite as cleverly as the Punch in the Throat author.
The Acid Truth
The concept seems to have crossed The Pond, as they say, and taken root in Great Britain, as a British writer for The Spectator recently daydreamed about starting a website called “People I Want to Stab to Death with a Bradawl” or “People I Want to Dissolve in a Vat of Acid.”
Yikes. Talk about visceral.
Here’s what’s interesting: The writer nominated a BBC executive for the bradawl treatment. Why? Because he decided that corporation staff would not be allowed to eat meat on Mondays.
“This facile, overpaid halfwit said in an internal communication that chefs would be preparing wonderful vegetarian alternatives for ‘carnivores,’ ” the article began. “There was then a magnificently condescending and arrogant explanation for this dictatorial idiocy: ‘Why are we doing this? Well, to put it simply, it’s good for the environment.’ ”
According to the story, the BBC executive then went on to note that “skipping meat on the menu just for one day a week can help reduce greenhouse gas emissions, water use and land use.”
Needless to say, the article noted, BBC staff have been tweeting their displeasure at this “high-handed and patronizing diktat.”
Enjoying the needling? Keep reading, because the writer really turned up the Vat of Acid vitriol.
“Listen,…the point of the staff canteen is to provide meals which your employees would like to eat, full stop,” he wrote. “It is not a conduit for your infantile politics or for fascistic corporate virtue-signaling. It is for the individual to decide if he or she wants to eat meat or not. Not you.”
I just hope that particular journalist can learn to get off the fence someday, and tell us what he really feels …
 … about Stabbing Someone to Death with a Bradawl.
Whatever that is.
In between the slicing, dicing and bradawling, the writer made a good point relevant to the Meatless Mondays controversy: Very few people eat meat every single day, for a variety of reasons, so imposing a corporate mandate that prohibits meat-eating on a specific day is arguably unnecessary.
As the article concluded in a message to the BBC, “Your job is not to shove a political agenda down the throats of employees along with stewed mung beans and a lentil bake, any more than it is the job of those BBC employees to shove a political agenda down our throats in lieu of making watchable programs.
“The bradawl is too good for this overreaching imbecile. I think we need the acid bath.”
After I stop chuckling, I have to find out what a bradawl is.
 
The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.