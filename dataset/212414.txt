Researchers at Michigan State have been awarded a $150,000 grant from the Foundation for Food and Agriculture Research to fight spotted wing drosophila (SWD), a significant pest for Michigan’s tart cherry crop. 
In 2016, the pest destroyed an estimated 21% of Michigan’s 2016 cherry crop, according to industry surveys.
 
Michigan State University, the Michigan Cherry Committee, and the Michigan State Horticulture Society are matching the foundation’s grant, according to a news release. Those contributions will combine for a $300,000 total investment in research to combat SWD, according to the release.
 
“Invasive pests pose a serious threat to the livelihood of Michigan’s cherry farmers,” Sen. Debbie Stabenow, D-Mich., and ranking member of the Senate Agriculture Committee, said in the release. “Michigan State University is a national leader in agriculture research with the expertise needed to quickly respond to threats to this important part of Michigan’s diverse agricultural economy.”
 
So far, there has been limited success in providing immediate and long-term relief against the SWD, according to the release.
 
The grant will support research to find integrated pest management strategies that will work not only to reduce pest damage to tart cherries but also can work for other specialty crops including blueberries and raspberries, according to the release.
 
According to the release, co-principal investigators on this project include: 
 
Bill Ravlin, MSU professor and entomology department chair;
Larry Gut, professor at MSU;
Rufus Isaacs, professor at MSU;
Nikki Rothwell, extension specialist at MSU; and 
John Wise, professor at MSU.