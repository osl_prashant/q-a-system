AP-MO--Missouri News Digest 5 pm, MO
AP-MO--Missouri News Digest 5 pm, MO

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Missouri.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
TOP STORY:
MISSOURI SENATE-TRUMP
JEFFERSON CITY — He is one of the Republican Party's most-prized recruits, a young U.S. Senate candidate with an outsider resume and a populist message designed to appeal equally to farmers, suburban moms and the national GOP's moneyed elite. But things get complicated when you ask Josh Hawley about President Donald Trump. By Summer Ballentine and Steve Peoples. SENT: 900 words. AP Photos.
AROUND THE STATE:
MISSOURI AG-TECH INVESTIGATION
JEFFERSON CITY —Missouri Attorney General Josh Hawley demanded information from Facebook on Monday following allegations that the social media giant mishandled user data. SENT: 600 words. By Summer Ballentine.
MISSOURI PRISONS-HARASSMENT
JEFFERSON CITY — The state of Missouri has paid out nearly $600,000 to a female corrections officer to settle another sexual harassment lawsuit. The St. Louis Post-Dispatch on Monday cited a report issued by the Missouri attorney general's office showing that Michelle Findley received a check for $294,500 in February, and her attorney was paid $291,443. SENT: 310 words.
INSURANCE PAYOUT LIMITS
JEFFERSON CITY —A bill on Gov. Eric Greitens' desk would limit how much money insurance companies would have to pay after accidents. SENT: 400 words. By Blake Nelson.
MIDWEST ECONOMY
OMAHA, Neb. — A business conditions index for nine Midwestern and Plains states surged again last month, which a report released Monday said is a sign of continued improvement in regional economic conditions. SENT: 330 words. With BC-NE--Midwest Economy-Glance.
IN BRIEF:
— BASEBALL BAT ATTACK — Two men are hospitalized after being attacked with a metal baseball bat in north St. Louis.
— SPRINGFIELD SINKHOLE — The driver of a trash truck is unhurt after the vehicle fell into a deep sinkhole in Springfield.
— BATHROOM PHOTOS-PORN — A Missouri man is now being accused of possessing child pornography after his arrest last month on allegations he was taking photographs of another person in a library bathroom stall.
— REMAINS FOUND-ST. LOUIS — St. Louis police have identified a person whose remains were found in the basement of a home last month, but the circumstances of his death remain under investigation.
— BROTHER KILLED —A spokeswoman for the St. Louis family of a 7-year-old boy who was shot and killed by his brother says the 5-year-old found their father's gun while looking for candy and didn't know the difference between it and a toy gun.
— SPRINGFIELD HOMICIDE — Authorities say the death of a man whose body was found inside a car in a Springfield neighborhood is being investigated as a homicide.
— COLUMBIA POLICE SHOOTING — Police in Columbia have released more than 280 pages of investigative reports related to the fatal shooting of a suspect by a Columbia officer last year.
— BEEF RECALL — A North Texas company has recalled nearly 4 tons (3.63 metric tons) of raw beef wrongly produced and packaged without federal inspection.
SPORTS:
BBA-ROYALS-TIGERS
DETROIT — Francisco Liriano pitched into the seventh inning in his Detroit debut and Victor Martinez drove in three runs to give the Tigers their first win of the season, 6-1 over the Kansas City Royals on a chilly Monday. SENT: 400 words. Will be updated. By Noah Trister. AP Photos.
BBN-CARDINALS-BREWERS
MILWAUKEE — Pitcher Miles Mikolas hit a home run and got the win in his return from a three-year stint in Japan, and the St. Louis Cardinals beat Milwaukee 8-4 on Monday to spoil the Brewers' home opener. By Genaro C. Armas. SENT: 450 words. Will be updated. AP Photos.
HKN-CAPITALS-BLUES
ST. LOUIS — The Washington Capitals look to continue their push to a division title when they travel to face the St. Louis Blues, who have lost two in a row after winning six straight games. UPCOMING: 650 words. AP Photos. Game starts at 7 p.m. CT.
HKN-BLUES-UPSHALL-KIDNEY LACERATION
ST. LOUIS — St. Louis Blues forward Scottie Upshall is out indefinitely with a lacerated left kidney. SENT: 130 words.
___
If you have stories of regional or statewide interest, please email them to apkansascity@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Missouri and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.