If you think of axe throwing, you might think of lumberjacks. Well, it’s now a popular sport at bars across North America with leagues boasting thousands of members.

Common sense might tell you alcohol and throwing sharp objects shouldn’t be mixed, but one “axe-pert” says you can actually throw better after a couple drinks!

Watch the story from Robert Bumsted of the Associated Press on AgDay above.