A couple of years ago, John Deere CEO Sam Allen memorably said: “Most people don’t realize that one of our 8000 Series tractors has more computing power on it than the first space shuttle.” That statement is awe-inspiring–and maybe a little terrifying. Here are tips for successfully hiring tech-savvy employees.
Click here to download and print this checklist.




Bridge the gap.



The USDA Economic Research Service reports that the average farmworker makes less than $11 per hour.
New ag tech makes its way to market every year, and farmworkers likely will need to master an increasingly tech-savvy skill set in the future.





Identify the right mindset.



In a recent Facebook post, former “Dirty Jobs” host Mike Rowe wrote: “I believe a solid work ethic and a measure of ambition are essential ingredients to success, and readily available to anyone. Obviously, the desire to succeed and the willingness to work hard are not enough to guarantee success, but success without either is impossible.”





Remember the four Cs.



Author and education expert Charles Fadel says employers desire employees who think critically, work creatively, communicate effectively and are able to collaborate.
Fadel says this isn’t a new need, but it has grown in importance.





Mindset can be more important than skill set.



“We look for creative innovators, self-starters who are willing to fail along the way,” says Dwight Koops, vice president of Crop Quest, which employs eight precision ag specialists.
He says it helps if the person has an interest in technology and likes to take things apart and put them back together.





Set up employees for success.



Some ag tech is relatively easy to learn, but other technology requires a much steeper learning curve.
Koops says give employees some room for trial and error. Technology training isn’t easy; it’s a moving target.