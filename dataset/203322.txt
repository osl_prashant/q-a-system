<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->



A review of the Women, Infants and Children program proposes significant increases of the value of vouchers for fresh fruits and vegetables, proposing up to $24 more each month for some participants.
The National Academies of Sciences, Engineering and Medicine's review of WIC suggests revisions to the federal program to be more in line with the Dietary Guidelines for Americans.
The United Fresh Produce Association collaborated with the National WIC Association (NWA) more than 10 years ago to encourage the National Academies' 2005 recommendations to include fruits and vegetables, and produce-specific vouchers were first available through the program in 2009.
The produce industry is optimistic about the recommended changes.
"United Fresh will work with the NWA and the U.S. Department of Agriculture to update the WIC Food Packages to ensure that WIC continues its transformative role in improving healthy eating choices for our nation's a risk mothers, infants and children," said Tom Stenzel, president and CEO of United Fresh, in a news release.
"The proposed revisions provide better adherence to the Dietary Guidelines for Americans, are more consistent with the standards of other nutrition assistance programs, and increase flexibility and choice for participants," said Kathleen Rasmussen, chairwoman of the committee, in a news release.
"These changes build up on the revisions made to the food packages in 2009," she said in the release, "so we anticipate they will not be difficult to implement."
A notable change proposed is a significant increase in vouchers for fruits and vegetables, bumping the allowance of $8 a month per child to $12, and $11 a month for pregnant and postpartum mothers to $15. To encourage breastfeeding, the recommended voucher for an exclusively breastfeeding mother is $35 s month, and a partially breastfeeding mother would receive $25 a month. At the current allowance, participants may purchase less than one cup of fruits or vegetables per day, which is significantly lower than the Dietary Guidelines for Americans recommends.
One of the priorities for the committee was determining how to encourage vegetable consumption, when, according to the report, WIC participants prefer fruits over vegetables. To address this, the committee suggested the increased voucher amounts for produce. The assumption is that if participants are able to adequately meet their fruit requirements, they will use the remainder to purchase vegetables.
State agencies are not allowed to choose the vegetables and fruits participants have access to. If participants wish to purchase produce that costs more than their vouchers allow for, they must pay for it out of pocket.
The report also suggests increased nutrition education to increase vegetable consumption.
State agencies "must authorize fresh and one non-fresh variety each of vegetables and fruits" according to the report. Vendors are now required to stock at least three varieties of vegetables, an increase from the previous requirement of two, to support the incentive of more vegetable consumption, as well as two varieties of fruit.
Another issue the committee found was that vendors faced challenges meeting the 2009 requirements of stocking at least two different fruits and two different vegetables. The committee addressed this by allowing for canned/frozen varieties, if authorized by the state agency in place.
The committee also suggested that future evaluations of the WIC food packages should include how increasing the vouchers affects how vendors stock produce.