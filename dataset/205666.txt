The National Institute of Food and Agriculture (NIFA) isconvening a summitto identify the frontiers and future of data in agriculture and build on existing U.S. government-wide efforts and investments in Big Data.Save the date for an opportunityto help shape the agenda driving innovation in our agricultural enterprise. Join us via livestream as leaders in agriculture and data science fields converge to synthesize the future of data-driven agriculture.ThisOct.10 eventwill include an address from NIFA Director Dr. Sonny Ramaswamy, a set of visionary talks from leaders in the fields of data science and agriculture, and discussion.
This eventwill be streamed live in tandem with the Oct.10-12, National Science Foundation'sMidwest Big Data Hub All-Hands Meeting.

Event Details
Monday, October 10, 2016 - 10 a.m. - 2 p.m.

Add to Calendar
iCalendar
Outlook
Google
Yahoo
Be part of the conversation with@USDA_NIFA#NifaAgData
Stay tuned for more webinar details and information. More information will be available on theevent web page.