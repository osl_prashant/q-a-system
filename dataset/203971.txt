Mann Packing Co. Inc. is recalling organic vegetable trays because of concern over ingredient labeling.
The voluntary recall of 844 cases of the Salinas, Calif.-bases Mann's 18-ounce O Organics-branded organic vegetable trays with creamy ranch dressing dip may have mislabeled eggs, milk and soy, according to a news release.
The ingredients label didn't properly identify ingredients that could pose an allergen risk, according to the release.
The recalled product has a best if used-by date of Aug. 11 and an incorrect UPC barcode: 21130 98428.
To-date, no illnesses have been reported from the product and no other Mann or O Organics products are affected by the recall, according to the release.
The trays were sold in 24 states including the West Coast and Idaho, Utah, Nevada, Arizona, New Mexico, Texas, Louisiana, Arkansas, Pennsylvania, Maryland, Delaware, New Jersey, New York, all New England states as well as Alaska and Hawaii.
The recalled product was sold at the following retailers:
Albertsons;
Acme;
Carrs;
Haggen;
Pavilions;
Pak 'N Save;
Safeway;
Shaw's;
Star Market;
Tom Thumb; and
Vons stores.