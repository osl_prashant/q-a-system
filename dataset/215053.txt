It’s the holidays, which means families everywhere will click open their preferred streaming video provider to play that now-classic Chevy Chase-Randy Quaid movie, “Christmas Vacation.” I mean, who doesn’t love it?
We sympathize with Chase’s character, Clark Griswold, as he endures excess family time, holiday lighting mishaps and, especially, his belated bonus—in the ego-deflating form of a membership in the Jelly of The Month Club. Argh! We almost want to ride with Cousin Eddie as he kidnaps Clark’s thoughtless boss.
Yet as business owners, we can see these seemingly ridiculous circumstances in a different light. We understand budget cuts and the need to maximize employee contributions. We want to create a high-performing team, and we can relate to striving for operational efficiency.
Tip Jars. People seem to want a bonus for just about everything these days. Tip jars are found in bathrooms, coffee shops, gift shops and the car-rental desk. Just when I figured out Uber, drivers started asking for tips.
I’m not writing this to sound cheap. On the contrary, I’m asking you to think about whether the bonuses you hand out are working. Are you seeing an uptick in performance? Or are you seeing more proverbial tip jars placed around your farm?
Merit Rewards. The research firm Aon Hewitt reports there is a trend among companies to shift to bonuses only for high performers. As you think about the end of the year and the year ahead, revisit your compensation structure.
If merit-only bonuses are gaining steam among bigger firms, how are they doing it? Here are some statistics from an Aon Hewitt survey of more than 1,000 organizations reported in The Wall Street Journal:
• Two-thirds surveyed plan to use merit-based pay to prove a point about who’s doing well and who needs to improve.
• 40% of those have cut raises for low performers or are considering it.
• 15% of companies plan to “set more aggressive targets for bonuses and incentive pay.”
Evaluate Now. Performance has never been as important as it is today. Maybe it’s time to review your compensation structure for 2018.
 

Consider These Bonus Structure Ideas
When reworking the bonus structure for your farm business, try using one or more of these tactics to ensure the process runs smoothly.
Phase It. Going cold turkey on eliminating bonuses is a bad idea for you and employees that might count on the money. When you make a change, consider a phased approach over time, and make employees aware of it in advance.
Scale It. One way to demonstrate transparency about who qualifies for a bonus and who doesn’t is to create a scale with percentages based on factors you set. If an employee exceeds a target, the top tier is met. If they miss it entirely, the bonus could be zero.
Make Up For It. Evaluate performance more than once a year. If, at mid-year, an employee is not on target for a bonus, let them know. Consider providing ways for them to make up for that in the latter half of the year.
Create A New Program For New Hires. It’s easier to develop a program for new team members rather than upsetting the apple cart for existing employees. Move longer-term employees toward the new model.
Merit Isn’t Just Sales Or Profit. Employees can earn a bonus for any set of criteria you create. You might decide to give someone a bonus for improving their working relationships with peers, formally mentoring a new person or even developing standard operating procedures for their role.
 
Sarah Beth Aubrey’s mission is to enhance success and profitability in agriculture by building capacity in people. She strives to foster that potential through one-on-one executive coaching, facilitating peer groups and leading boards through change-based planning initiatives. Visit her at SarahBethAubrey.com, and read her blog, The Farm CEO Coach, at AgWeb.com.