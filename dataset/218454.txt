An old television commercial for Chiffon margarine warned it was “not nice to fool Mother Nature.” Jim Harbach hasn’t fooled Mother Nature, he’s done his best to match her.
“Our goal is to mimic Mother Nature,” Harbach says. That’s the best way to explain what he and his family have done with the soil profiles on their dairy, Schrack Farms, near Loganton, Pa. With water infiltration rates that average 8" an hour—average for tilled ground is ½" per hour—Harbach has accomplished his goal.
It’s the family’s innovations and the leadership demonstrated in building sustainable land profiles that helped them win the 2018 Innovative Dairy Farmer of the Year award, co-sponsored by the International Dairy Foods Association and Dairy Herd Management.
Harbach and his wife, Lisa, are partners in Schrack Farm Resources with Lisa’s brother Kevin Schrack. Care of the 1,000-cow dairy herd falls on Kevin while Harbach manages the 2,500 acres of crop ground. Acres include about 1,300 acres of corn, 300 acres of soybeans and the rest in alfalfa, grass and small grains.
Read more about Schrack Farm Resources by clicking the interactive story below:

 
Note: This article appears in the January 2018 magazine issue of Dairy Herd Management.