As the USDA’s World Agricultural Supply and Demand Estimates (WASDE) were released Thursday, U.S. Farm Report roundtable guests Elaine Kub, author of Mastering the Grain Markets, and DuWayne Bosse of Bolt Marketing LLC took the stage of the CHS Ag Industry Day in Grand Forks North Dakota with Tommy Grisafi of Advance Trading, Inc. filling in as host.

One of the biggest parts of the report that was most prominent to Bosse was decrease of Argentina’s soybean crop from 54 million metric tons to 47 million metric tons.

“USDA doesn’t like to make big jumps,” he said. “So a 7 million metric ton drop is a big drop.”

According to Kub, market was waiting for that lower number out of South America, but the reaction after the report was released wasn’t as dynamic as what could have been expected, citing how global edible oils aren’t able to support this soybean rally.

“I don’t know if it’s going to have a lasting ability to build the rally,” she said.

Argentina has been dry for weeks now, and Kub says this production problem is already being priced into the market.

“Your opportunities to sell are here,” she said. “There’s nothing more we can do at this point bullishly from a fundamental standpoint.”

In November, Bosse said farmers were “begging for a profit” in soybeans, and now is an excellent time to sell.

Hear their full reports on the report and spring wheat on U.S. Farm Report above.