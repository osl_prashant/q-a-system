Spring weather has made it difficult for farmers to get in the fields. With delays and replants, many in ag are wondering when the spring rally will hit.
According to Mike North, president of Commodity Risk Management Group, there could be a rally in the next two weeks.
“Nobody’s got excited about any slow pace, but they are starting now to question what the quality of the crop might be,” he said. “If we can break through some chart resistance, we can motivate some funds and move some money in the market, a 20 to 30 cent rally is very much within reach.”
However, North says moving the fund position at this point in the game will be difficult because the real buyers of corn “aren’t motivated to chase it.”
Watch North’s full comments on AgDay above.