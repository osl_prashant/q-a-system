Crop Consultant Dr. Michael Cordonnier looks for production of the South American corn and soybean crops in 2017-18 to be down from 2016-17. He expects the estimates listed below to change as the season progresses, but for now projects South American soybean production at 179.60 MMT, or approximately 9 MMT less than 2016-17. For corn, he looks for production at 135.00 MMT, or approximately 8 MMT less than last year.



Dr. Cordonnier
			Soybean Estimates

2016-17 acreage 


2017-18
			acreage 


2016-17 production 


2017-18
			production




acreage in million hectares; production in MMT



Brazil

33.90


34.74


114.0


109.0



Argentina

18.35


19.00


57.8


55.0



Paraguay

3.38


3.41


10.6


10.0



Bolivia

1.13


1.40


2.10


2.60



Uruguay

1.08


1.08


3.20


3.00



Total

57.84


59.63


188.7


179.6








Dr. Cordonnier
			Corn Estimates


2016-17 acreage 


2017-18
			acreage 


2016-17 production 


2017-18
			production




acreage in million hectares; production in MMT





Brazil

17.59


17.00


97.70


88.00



Argentina

4.90


5.20


41.00


42.00



Paraguay

0.65


0.75


3.30


3.70



Bolivia

0.38


0.37


0.90


0.90



Uruguay

0.05


0.06


0.40


0.40



Total

23.57


23.38


143.3


135.0





Cordonnier soybean planting in Brazil is off to a slower-than-normal start, with just 1% planted compared to the five-year average of 2.4%. He says farmers are waiting on rains to arrive to begin planting, and will reassess the situation after weekend rains arrive.