While ag leaders often celebrate the fact the U.S. has the world’s safest food supply, keeping it safe requires a long list of interagency coordination from farmer to consumer.
Hosted by the Agricultural Business Council of Kansas City, U.S. Senators Claire McCaskill (D-Mo.) and Pat Roberts (R-Kan.), along with William Bryan, Department of Homeland Security acting under secretary for science and technology, participated in a roundtable discussion about the importance of planning for agro-terrorism threats. Moderated by Cliff Becker, vice president of Farm Journal Media, discussion with government, education and private officials centered on the measures taken every day to maintain a safe, secure food supply.
“Food security is national security,” said Roberts, chair of the Senate Agriculture, Nutrition and Forestry Committee. “The devastating ramifications of being ill prepared for a malicious attack or a natural disaster on our food supply would be absolutely overwhelming, we’ve had several exercises to prove that. However those consequences can and should be mitigated through research, prevention and preparedness—we have to do that.”
Hear more from Senators McCaskill and Roberts by clicking the video below. 

 
From past experiences with livestock disease outbreaks, Craig Wallace, CEO of Ceva Animal Health LLC, said building a vaccine bank isn’t enough. There needs to be a strategy on how vaccines are deployed in the event of an animal disease outbreak.
McCaskill, the ranking Democrat on the Senate Homeland Security Committee, agreed. “We’ve got to figure out when they should be used, how they should be deployed and how that actually occurs.”
She was happy to hear USDA’s Animal and Plant Health Inspection Service planned a summit on the issue of deploying vaccines when necessary.
Other issues discussed during the roundtable were coordination of investigation responsibilities between veterinarians and law enforcement during an event, the lack of skilled veterinary students prepared to take on those future roles, the ability to provide quick diagnostic tools for producers and veterinarians, and the need for more planning and coordination in the event that vaccines are dispensed. They also brought up the need for more border patrol officers to investigate incoming products for contamination and other risk factors.
The event comes after the “Securing our Agriculture and Food Act”, cosponsored by Roberts and McCaskill, was signed into law by President Donald Trump in July. The law requires the secretary of the Department of Homeland Security (DHS) to lead the government’s efforts to secure the nation’s food, agriculture and veterinary systems against terrorism and high-risk events. DHS is currently building a new National Bio and Agro-defense Facility in Manhattan, Kan., at a cost of $1.25 billion.
“I never thought I’d work so hard for something in Kansas,” McCaskill told the audience. ”But what’s good for Kansas in this instance is good for our country….”
Each panel member agreed: Coordination is the key to keeping U.S. agriculture safe and secure.
“All disasters are local—they start locally and end locally,” added LTC Robert Payne, Missouri National Guard.
“DHS does have an important role, and it’s a coordination role. Don’t underestimate the importance or value of that coordination role,” Bryan said. “Everyone in their agencies has a role to play, both in their industry as well as federal government, state and local authorities, and the National Guard.”
Also participating in the roundtable discussion were:
Ted Elkin, deputy director for Regulatory Affairs, USDA’s Center for Food Safety and Applied Nutrition, Food and Drug Administration, U.S. Department of Health and Human Services
Jere Dick, associate administrator, USDA’s Animal and Plant Health Inspection Service (APHIS)
Dr. Marty Vanier, director of Partnership Development, National Bio and Agro-Defense Facility, Department of Homeland Security
Jonathan Greene, deputy assistant secretary and director, Human Threats Resilience Division, Office of Health Affairs, Department of Homeland Security
Dr. Tammy Beckham, Dean, College of Veterinary Medicine, Kansas State University
Dr. Carolyn Henry, Interim Dean, College of Veterinary Medicine, University of Missouri
Brandon Depenbusch, vice president of Cattle Operations, Innovative Livestock Services
Gen. Lee Tafenelli, Adjutant General of Kansas