Bazooka Farmstar Expands Their Full Throttle Series Product Line

Bazooka Farmstar announced the extension of their Full Throttle Series trailer line at the National Farm Machinery Show in Louisville, KY this week. The Full Throttle Outlaw is a triple section force feeding boom with optional agitation features that allows for a quick, convenient set-up and easy travel to and from sites.
The Outlaw has retained key features of the Full Throttle booster trailer with a 500-gallon capacity fuel tank for longer runtime, as well as a reinforced front deck with integrated tie-down locations for ATV or parts storage. In addition, a 35’, triple section force feeding boom has been mounted to the rear of the trailer and reaches a maximum depth of 19’.
This trailer is available in gooseneck or bumper style hitches and can accommodate 13.5 L (500 HP – 600 HP) engines with a clutch and drive train. The Outlaw’s engine/main and knife gate hydraulic functions are regulated via Bazooka Farmstar’s Electronic Control System (ECS).
For more information or to find a dealer near you, go to http://www.bazookafarmstar.com/
John Deere Bolsters its Low-Profile Specialty Tractor Lineup



John Deere announced it has expanded its specialty tractor lineup with the addition of the new 5090EL and 5125ML, and additional updates to the 5100ML and 5115ML Low-Profile Tractors – reaffirming its commitment to the specialty tractor market.

The 5090EL features a low stance and is built on the rugged John Deere 5E 4-cylinder tractor platform. With an overall height of only 69 in. and 90 engine hp, it fits easily through small doors like those found in poultry houses or horse barns and has plenty of power to pull a variety of implements.

A lowered ROPS hinge point and factory-installed horizontal side exhaust on the 5090EL help minimize tractor height. Depending on the application, the 5090EL can be equipped with either R4 industrial or R1 agricultural tires.

John Deere also strengthened its premium spec 5 Series low-profile tractor lineup by adding the new 5125ML and adding additional options to the 5100ML and 5115ML. A PowerTech TM engine delivers 125 horsepower to easily handle heavy implements and workloads.

The 5090EL is backed by a John Deere 2-year or 2,000 hour comprehensive factory warranty, and a 5-year or 2,000-hour powertrain warranty. The 5ML tractors are backed by a John Deere 2-year or 2,000 hour comprehensive factory warranty. John Deere dealers are currently taking orders for the 5090EL and will begin taking orders for the 5125ML in late February. Production of the 5090EL will begin in mid to late April and production of the 5125ML will begin later this fall. For detailed information visit JohnDeere.com or see your local dealer.