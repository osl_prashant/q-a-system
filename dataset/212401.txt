The Los Angeles Times has published a thoughtful and well-sourced article about the challenges of finding housing for H-2A workers in California.  
The piece, headlined “Desired for their labor, rejected as neighbors. Farmworkers in California face hostile communities,” opens with a story about suspected arson in 2016 at a “urban farm home” being constructed to house strawberry workers near Nipomo, Calif. 
 
Rick Antle, CEO of Tanimura & Antle, is extensively quoted in the piece about the company’s impressive Spreckels Crossing housing complex for its workers.
 
Well-heeled Californians like strawberries but not the workers that pick them, the story suggests.
 
As the story put its, “That age-old “not in my backyard” reaction threatens growers’ ability to fill the current labor shortage and exacerbates the long-term housing crisis for local farmworkers.”
 
Other reporting the series includes:
 
“To keep crops from rotting in the field, farmers say they need Trump to let in more temporary workers”
 
Key stat: More than 11,000 H-2A workers were approved last year for California agriculture, up fivefold from 2011. And the numbers appear to be heading higher.
 
 
"Wages rise on California farms. Americans still don’t want the job"
 
Key quote: “You don’t need a deep analysis to understand why farm work wouldn’t be attractive to young Americans,” says Philip Martin, an economist at UC Davis.
 
 
 
 
Optimism still
 
It is safe to say that President Trump’s election has created even more certainty for agricultural employers. For example, will Republicans proceed with mandatory E-Verify for all employers?
 
Yet Frank Gasperini, president and CEO the Washington, D.C.-based National Council of Agricultural Employers. said there is still is a lot of optimism among growers about the Trump presidency.
 
“Of course the president and the Secretary of Agriculture in particular have made some promises to ag that everything is going to be all right,” he said. “I think there is a tremendous amount of optimism out in the field that they will make good on those promises.” 
 
Given the worries about the issues surrounding farm labor, Gasperini said it is almost remarkable the level of optimism about what the Trump administration will do.
 
The numbers of H-2A workers in the U.S. are on the rise, with first quarter estimates reporting 36% bigger numbers than a year ago, according to Gasperini. If those numbers hold for the year, 2017 guest workers for ag will total close to 215,000. However, freezes in the Southeast could reduce labor needs and diminish the numbers. On the other hand, Washington state figures to have about 20,000 H-2A workers, Gasperini said, up from 17,000 last year.
 
Growers in Washington state are finding it easier to build farm worker housing than in California, he said. 
 
If California were to solve its housing issues, Gasperini said growers in other states have angst that the Golden State could soak up nearly all available H-2A workers, considering the state’s higher wages compared with other states.
 
Time will tell how all the moving pieces come together for farmers who rely on immigrant labor. For now, it seems, there is still optimism that Trump can deliver on his promises.