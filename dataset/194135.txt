A May 18 auction of 56.45 acres of medium quality farmground currently dedicated to hay production in Wayne County, Iowa brought a total of $191,080.00 per acre for 2 tracts for an average price of $3,400 per acre. These tracts are located within the Corydon, Iowa city limits.
Tract 1: 35.2 acres tillable currently in hay production, CSR2: 51.2 with a lake.
Tract 2: 21.25 acres tillable currently in hay production, CSR2: 53.9.
Both tracts improved with city water, electric and other city amenities. Dale Bolton, Sullivan Auctioneers, Hamilton IL, 217-847-2160.
An April 28 auction of 386.3 acres of Perkins County, Nebraska irrigated farmland brought $930,983.00 per acre for 2 tracts at an average selling price of $2410 per acre.
Tract 1: 154.3 total acres with 122.0 acres pivot irrigated, 28.1 acres dryland and 4.2 acres of road. PLC corn yield of 135 bu. and 40 bu. for wheat. Located 4 1/2 miles south of Brandon, NE.
Tract 2: 232.0 total acres with 160.3 pivot irrigated, 65.0 acres dryland and 6.7 acres road. PLC 135 bu. For corn and 40 bu. For wheat. Located 3 miles north of Brandon, NE.
Mark Reck, Reck Agri Realty & Auction, Sterling, CO, 1-800-748-2589.