“Here you go, kid. Have a banana.”
Or an apple, a tangerine, a sample cup of grapes. The idea of providing a produce treat to the under-12 group shopping with their parents is great. It’s catching on with many grocers.
And for good reason. It’s good for business.
I recall when my kids were in grade school. Their teachers, always looking for a quality field trip, were attracted to a free annual spaghetti lunch offer by a local Italian restaurant owner. Each spring the owner invited a couple of classes, and he’d treat them well, with balloons, music, a couple of face-painters — and a hot plate of his great “sgetti.” Each kid received a coupon for a free kid’s meal to bring them back. 
Of course this meant dragging along their family or friends. It was a great way of bringing in new customers, or diners who hadn’t been into the restaurant for a while. 
The investment to attract business was modest, and seemed to pay dividends.
Same thing goes for building sales in the humble produce department. Providing a fresh produce treat helps win over the mom or dad who’s shopping, too. They’re able to focus on shopping while the little ones are busy munching on something good-tasting that also is good for them.
Can a produce manager duplicate the restaurant owner approach?
It depends. Certainly not if the produce manager never ventures out of his or her store. 
However, if the produce manager calls on his nearest high school, and offers to provide a produce “workshop” to the food arts class, anything is possible.
If you can make the connection, offer to come out, say, once per semester. Ask your produce specialist or supervisor to help offset the cost of a big box or two of samples, both traditional fare and a healthy range of specialty produce too. Once set, arrange a time to go and speak to a class or two.
Culinary or food arts teachers love this. Their food budgets are razor-thin, and if you bring fresh produce, they’ll be more than happy. (It’s also great PR for your chain). 
Once in front of the group, give them the basics: Identification, season, flavors — and plenty of samples. Give them a taste of the familiar, perhaps offer an apple variety they may not have tried, or something that looks a bit unusual such as yellow cherry tomatoes or kumquats. 
You will be amazed at their reactions, and more so at the questions that follow. 
Kids can be loud and playful (or like I was, a bit annoying), but above all they’re a curious bunch who are also fast becoming your customers. Help them get acquainted with fresh produce.
Armand Lobato works for the Idaho Potato Commission. His 40 years’ experience in the produce business span a range of foodservice and retail positions. E-mail him at lobatoarmand@gmail.com.