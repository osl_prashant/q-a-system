Florida peach growers see interest in their fruit increasing and anticipate a strong crop this spring.
The peaches have gotten their recommended chill hours, and quality is looking good.
Flavor of the fruit is always great, but getting those chill hours means yield will be better, said Al Finch, president of Dundee-based Florida Classic Growers.
He expects that some cool weather could delay the start of harvesting by a week or so. Harvesting will begin in a limited way the week of March 26, with the most substantial volumes coming available the last two weeks of April and the first week of May.
The Florida peach deal has growing rapidly in the last decade, filling the void between when Chile exits the market and before Georgia, California and South Carolina enter the market.
Florida Classic Growers, which has about 750 acres of peaches and is in its ninth season of growing the fruit commercially, ships as far north as Canada and targets east of the Mississippi River.
“This is not just a local (or) regional program anymore,” Finch said. “We’re taking it out of the Southeast.”
Florida peaches are unique in that they are smaller than others and tree-ripened, so they are ready to eat rather than needing a couple of days to ripen after purchase.
“Consumer awareness is beginning to really take off on these peaches,” Finch said.
Alise Edison, owner of Deer Park Peaches, St. Cloud, Fla., also said business is growing.
“Every year things have gotten bigger, increased considerably,” Edison said.
She said production looked on track to start about two weeks earlier than in 2017.
“We started about mid-April last year, and I think we’re going to be starting right around April 1,” Edison said. “We won’t be harvesting on Easter, but it’s going to be right at either the very end of March or the very beginning of April.”
 
Diversifying
Kay Rentzel, managing director for the National Peach Council, said part of the reason for the growth of the Florida peach deal is that companies are seeking to diversify given the extreme challenges that citrus greening disease has imposed on the industry.
Ali Sarkhosh, an extension specialist for tree fruit at the University of Florida, will be doing research in a number of areas that may be helpful to Florida growers investing in peach production.
One focus will be on reducing costs, as growers can spend $1,500 to $2,000 per acre or more for thinning and pruning. Another area of interest is a rootstock more tolerant of flooding, particularly in the wake of trees being killed last year because they sat in water for days after Hurricane Irma.
The ceiling for Florida peach production remains to be seen, but less than 10 years ago acreage was about a quarter of what it is now.
“The fact that it’s grown this much in that period of time is certainly encouraging,” Rentzel said.