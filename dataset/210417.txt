It's June Dairy Month, so we want to highlight our 2017 Dairy BQA Award winner!
Tricia Adams, Hoffman Farms from Shinglehouse, Pa., says treating their cows with low-stress and human handling will only have more value once they leave the milk line and become beef.
Watch more as Tricia explains, from the dairy in Pennsylvania.