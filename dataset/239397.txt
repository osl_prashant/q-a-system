A New Zealand politician shows up on a college campus in a t-shirt guaranteed to provoke condemnation — and not just from vegetarians, but pretty much everyone else.
One of the minor political parties in New Zealand is the ACT Party (Association of Consumers and Taxpayers). It is described as a “right-wing New Zealand political party” promoting “individual freedom, personal responsibility, doing the best for our natural environment and for smaller, smarter government in its goals of a prosperous economy, a strong society, and a quality of life that is the envy of the world.”
Aspirational rhetoric aside, the party’s sole Member of the New Zealand Parliament, David Seymour, created a bit of a firestorm this week by wearing a t-shirt that was decidedly non-PC (see photo), in that it depicted the silhouette a woman (with a cow’s head!) reclining provocatively and posing the question: “got meat?”
In a story titled, “David Seymour grilled over 'sexist' meat shirt,” Radio New Zealand reported that after Seymour posted a picture of himself wearing the shirt at a barbecue hosted by the University of Auckland’s Meat Club, all heck broke out on social media.
Plenty of hardcore vegetarians took Seymour to task, as would be expected for any politician who participated in a Meat Club event. However, many others on Facebook and Twitter had a beef not about the meat but about the perceived put-down of women.
Golriz Ghahraman, a Green Party MP, accused Seymour of “contributing to sexism” on campus.
A Lame Defense
In his defense, Seymour came up with what has to go down as one of the dumbest counter-arguments to the charge of belittling women. In an interview, he told Radio NZ’s Morning Report that, “The [Meat] Club idolizes meat, so its members don’t see being compared with meat as demeaning.”
That suggestion is even more objectionable than the imagery on the shirts.
It didn’t get any better during the rest of the interview, as Seymour accused people who objected to the imagery as wanting to prevent “clubs on campus from getting together and wearing t-shirts that portray the things they enjoy.”
When asked if the image on the shirts was deliberately designed to be provocative, Seymour answered that the silhouette was merely that of “an athletic woman,” and that the real message was all about meat.
Nice try, Dave.
On one hand, this controversy could easily be dismissed as yet another frat boy stunt that no one should bother taking seriously. Nobody got hurt; nobody got ripped off; nobody was forced to participate in something they didn’t like.
However, Seymour is serving as a Member of Parliament, not pledging Delta Tau Chi of “Animal House” fame.
More to the point, if it’s all about meat, then stick to what’s on the barbie, mate.
There’s not only no reason — other than a salacious one — to reprise the familiar female figure that graces about two hundred thousand tractor-trailer mud flaps, doing so detracts from the stated purpose of the Meat Club.
Nobody’s talking about one of New Zealand’s primary industries, one of its principal exports or the source of high-quality nutrition for millions of Kiwis.
Instead, thanks to these ill-advised t-shirts, the debate is solely about the objectification of women.
Given the imagery Seymour and his student pals are displaying, that’s the appropriate conversation to be having.
But just because he’s appearing at an event on a college campus, is a professional politician and MP, especially one who espouses personal responsibility, he shouldn’t be acting so sophomoric.
 
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.