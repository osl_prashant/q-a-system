The only thing that should surprise the produce industry about President Donald Trump's first week in office is that he started implementing the policies he ran on quicker than we thought.
 
The industry has had his whole campaign and more than two months after the election to prepare for the withdrawal from Trans-Pacific Partnership trade agreement; re-evaluation of NAFTA; and immigration (legal and illegal) limits including a southern border wall.
 
Considering Trump and Democrats Hillary Clinton and Bernie Sanders all supported withdrawal from TPP, no one can claim this is a surprise.
 
The industry will have to continue to work with the Trump administration to impress on it the importance of imports and exports to the produce industry. 
 
He ran as a business president. Hold him to it.
 
Immigration remains a looming issue, but a recent proposal by two House Representatives to streamline the H-2A program by moving it from the Department of Labor to the U.S. Department of Agriculture seems like a good start.
 
Now is not the time to panic on immigration and labor. There will be plenty of time for that later.
 
Did The Packer get it right? Leave a comment and tell us your opinion.