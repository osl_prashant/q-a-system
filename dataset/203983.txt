Next year, things have to be different, says Forest City, Iowa, farmer Doug Tweeten. This past summer he, and thousands of U.S. farmers, walked into cupped, crinkled soybean fields with suspected dicamba damage. “I don’t think anyone wants to go through this again—in a perfect world the [dicamba] formulation won’t move,” Tweeten says.
  More than 2,200 complaints with dicamba named as the suspect have been filed in the U.S. since the beginning of the 2017 season. 
In perspective, if various Extension experts’ estimate of 3.1 million U.S. acres of damage is accurate, that represents 3.5% of planted soybean acres, as of press time. This past spring, farmers planted Xtend technology, which withstands dicamba herbicide, on 20 million soybean acres and five million cotton acres, according to Monsanto.
“The industry needs to work on this [dicamba damage] issue because we’re running out of tools to manage resistant weeds,” says Ken Ferrie, Farm Journal Field Agronomist. “I’m hoping we can learn from this year so we can protect the use of the technology.”
EPA lists each new dicamba formulation under a two-year conditional label. Poor stewardship could mean labels won’t be renewed after 2018, leaving some farmers backed into a corner with resistance issues.
Understand this year’s dicamba issues to plan for success next season.
“The most important discussions we can have now are what to do moving forward,” says Kevin Bradley, University of Missouri Extension weed scientist. “We can’t get into another situation where a stop sale happens and hamstrings someone’s weed management plan.”
“Growers need to know what they will be dealing with and what they will and will not have available before the season starts,” he adds, citing the decisions by Missouri and Arkansas to temporarily ban dicamba products, the latter is still in place. “If nothing changes [with damage and application requirements] in our area, I expect Xtend soybean acres to go through the roof with guys just wanting to protect themselves.”
 
Dicamba Complaints and Damage, by State
 
.embed-container {position: relative; padding-bottom: 50%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}


 

Click the state to see the number of official complaints filed, as well as estimated acres damaged. Estimated damage provided by Extension weed scientists. (© Kevin Bradley)

 
Monsanto’s chief technology officer Robb Fraley says the company is taking a three-fold approach. “First, we have to work with the states to stop the use of unregistered products [older dicamba formulations]; second, we will continue our successful training programs to reinforce proper application methods; and third, we need to work with farmers and applicators so they can identify the weather conditions that contribute to drift and temperature inversions.” 
As companies look to 2018, manufacturers of new dicamba formulations are boosting training efforts. (BASF manufactures Engenia; Monsanto manufactures XtendiMax with Vapor Grip Technology, which is also licensed to DuPont and sold as FeXapan.) Companies also say they are investigating each complaint to find the cause and determine how to correct it for next year.
Both Monsanto and BASF have expressed concern about illegal applications of older, more volatile dicamba formulations that could have contributed to damaged acres this season. “In a year with corn and wheat acres way down, the use of older products should be reduced and we’re hearing they’re up,” Fraley says. “Because of a lack of record keeping it’s hard to pin down exactly how many generic formulations were sold—it’s something we’re working with regulators on.”
University scientists want more information. “It wouldn’t surprise me if there’s been some generic applications [causing damage], but I work mostly with commercial applicators who keep records and used legal formulations,” Bradley says. “Last season I was told repeatedly it was older formulations [causing issues] and we wouldn’t see this with newer formulations. We can’t blame generics and ignore other problems like volatility and drift of new products.”
There is no one, easy solution to the dicamba dilemma, adds Aaron Hager, University of Illinois Extension weed scientist. “Applying dicamba according to label only covers physical drift; if it’s volatility, that’s something difficult for everyone to deal with since it’s a chemical process.”
 
 
 
"We're not going to buy Xtend beans just to protect ourselves. If I plant them it will be for weed control, not for drift or volatilization."
 
- Doug Tweeten, Iowa Farmer     
 
 
Chad Asmus, BASF technical marketing manager, says good stewardship practices are critical. “Engenia must be used as part of a comprehensive weed-control strategy; it isn’t meant to be a stand-alone [herbicide] solution,” Asmus says. He encourages applicators to get additional training from their local BASF representative and via the company’s online training site growsmartuniversity.com.
Georgia required training before dicamba application on any of their 260,000 (USDA, 2016) soybean acres. To date, the state has no official dicamba complaints. “We had mandatory training to address dicamba and 2,4-D use,” says Tommy Gray, Georgia Department of Agriculture director of the plant industry division.
There is still much left to learn this season as yields are discovered and more information is available. Homer, Ill., corn and soybean farmer Jeremy Wolf says he has dicamba damage on 48% of his acres. “I’m very concerned about yield—there’s an obvious impact where we’ve been looking,” Wolf notes. His beans were hit in the R2 to R3 stage, and he sees damaged pods, fewer pods and smaller seeds.
“Literature suggests that damage after reproductive stages means an increased likelihood of adverse effect on yield,” Hager says. “It might not be the case, but chances are higher.”




  


Jeremy Wolf in some of his dicamba damaged soybeans.


© Matt Garrison


 




Ferrie is hopeful most soybeans were able to rebound. “We’ve dealt with drift issues before that didn’t result in yield loss,” he says. “Beans are really resilient, and that could make a difference this fall.”
Ferrie encourages farmers with suspected damage to reach out for help. “I advise farmers call the 800 number, report it to chemical reps and go through all channels as far as notifying insurance companies.”
Ferrie reminds farmers to work with insurance companies to understand coverage. Most commercial applicators carry liability policy that could potentially provide a payout.
Stewardship is needed to keep dicamba on the market. “Overall I give kudos to applicators,” Ferrie adds, citing stewardship efforts.
Looking ahead Hager encourages farmers and retailers to “follow steps to address physical drift and clean equipment. Hopefully, if nothing else, this year taught everyone more about the sensitivity of soybeans.”
Upcoming editions of Farm Journal will address product volatility, yield results and other issues as dicamba’s story unfolds.

.embed-container {position: relative; padding-bottom: 80%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}