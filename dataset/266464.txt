BC-Cotton
BC-Cotton

The Associated Press

NEW YORK




NEW YORK (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Thursday:
OpenHighLowSettleChg.COTTON 250,000 lbs.; cents per lb.May80.7482.2080.6881.46+.72Jul81.1882.5181.0881.80+.62Sep77.73+.74Oct79.23—.08Nov77.73+.74Dec76.9577.7876.9077.73+.74Jan77.82+.59Mar77.2277.9377.1377.82+.59May77.4378.0377.4377.93+.53Jul77.4177.9377.4177.89+.48Sep72.96—.05Oct74.92+.14Nov72.96—.05Dec73.0073.1072.9372.96—.05Jan73.06—.04Mar73.06—.04May73.61—.04Jul73.69—.04Sep72.09—.04Oct73.26—.04Nov72.09—.04Dec72.09—.04Est. sales 32,025.  Wed.'s sales 31,247Wed.'s open int 279,189,  up 903