Learn how to take your calf and heifer program to the next step on the podium by attending the 2018 Dairy Calf and Heifer Association (DCHA) annual conference. With the theme “One team. Gold dreams.” the conference is set for April 10-12 in Milwaukee, Wis. “The DCHA conference focuses on calves and heifers — our future,” says Elizabeth Quinn, DCHA board of directors. “It is important for me to stay up to date on current herd health practices and protocols presented at the conference. Being current with industry standards helps us finetune our operation.”
Here is an overview of what you’ll see at the 2018 conference:
Topic highlights
Producer panels, presentations and breakout sessions will discuss timely, hot-button topics. The conference will also include on-farm and industry tours, and hands-on, post-conference demonstrations.
Farm and industry tours with a focus on calf and heifer management
Breakout sessions
Calf immunity
Feed center management
Feedlot management
Housing ventilation

Producer panel topics
Cattle transportation
Group housing
The Power of Influence, Ty Bennett
Calf hunger behavior, Dr. Marina von Keyserlingk, University of British Columbia
Colostrum management, Dr. Sandra Godden, University of Minnesota
The fairlife® story, Sue McCloskey, Fair Oaks Farms
General session topics
Closing session
 “The DCHA conference brings together some of the best in the industry,” says Quinn. “It is where I learn new ideas, procedures and products from one year to the next. These learnings come not only from presenters, but also from conference attendees.”
Book your hotel
Call (800) 729-7244 and ask for the DCHA room block to book your room. Or book online at calfandheifer.org under the “Conference” tab.
Sponsorship and trade show opportunities
Want to increase your exposure among top-notch calf and heifer raisers? Sponsorship and trade show opportunities are designed to create greater visibility and access to customers. Email info@calfandheifer.org to get involved at this one-of-a-kind event.
Last year’s conference drew more than 600 dairy calf and heifer raisers, dairy farmers and allied industry professionals from 30 states and 10 countries, representing more than 2 million cattle.
For more information about the 2018 DCHA Conference or to become a member visit calfandheifer.org, call (855) 400-3242 or email info@calfandheifer.org. 
The Dairy Calf and Heifer Association (calfandheifer.org) was founded in 1996 based on the mission to help dairy producers, calf managers and those professionally focused on the growth and management of dairy calves and heifers. With a national membership of producers, allied industries and research leaders, DCHA seeks to provide the industry’s standards for profitability, performance and leadership, serving as a catalyst to help members improve the vitality and viability of their individual efforts and that of their business.