Producers are excited to have a Secretary of Agriculture who has first-hand experience working in agriculture and growing up on a farm.
On April 28, three days after former Georgia Gov. Sonny Perdue was sworn in as Secretary of Agriculture he visited Kansas City, Mo. for his first official trip away from Washington D.C. representing the U.S. Department of Agriculture. Perdue spoke in front of a crowd of more than 450 people made up of farmers, ranchers, allied industry members, students and FFA members at the American Royal.
Several producers asked questions and made comments to Perdue during the town hall hosted at the American Royal with topics ranging from trade, wildfire relief and advocating for agriculture.
The confirmation of Perdue as Secretary of Agriculture is exciting because there are common values shared by the leadership at USDA with farmers and ranchers, says Ramona, Kan. cattle rancher Tracy Brunner. Last year Brunner served as National Cattlemen's Beef Association president.
“As a trained and practicing veterinarian, he brings so much understanding of the livestock industry to the office,” Brunner says.
Brunner asked about the Grain Inspection, Packers and Stockyards Administration (GIPSA) rule and what Secretary Perdue thinks about it.
Perdue understand GIPSA is complicated with producers from poultry, pork and beef having different views. “We’re going to look at it very closely,” Perdue says.
While GIPSA continues to be a debated topic in the livestock sector, opening up beef trade to China looks like it might become a reality.
Perdue told the crowd about his communication with President Trump on getting U.S. beef into China. Trump even asked Perdue to make some remarks for a letter that will be addressed to the Chinese President in an effort to open up beef exports.
He also believes Iowa Gov. Terry Branstad will be a major part in getting beef access to China once Branstad is confirmed as U.S. Ambassador to China.
“Gov. Branstad has done a good job selling Iowa soybeans. We want him to sell some Kansas, Missouri and Heartland beef over there,” Perdue says of trade with China.
Opening up beef trade back with China would be great for cattle ranchers, says Jared Wareham, a cow-calf producer from Osceola, Mo.
“Secretary Perdue understands trade. He knows we need to fix some of these trade deficiencies and open up additional trade lines with countries like China,” Wareham says.
In addition to trade, Wareham believes Perdue brings some boots on the ground knowledge after growing up on a farm, being a veterinarian and grain trader.
“He knows what it is like to roll your selves up and grind it out,” Wareham says.
Ken McCauley, a row crop farmer from White Cloud, Kan. who is a past president of the National Corn Growers Association, says it is refreshing to have a down to earth person like Perdue serving as Agriculture Secretary.
“It should give all farmers a good feeling that we have a guy who has been where we are,” McCauley says.
During the town hall, McCauley told Perdue that he was happy to see the Trump Administration address the deficiencies in trade agreements like the North American Free Trade Agreement (NAFTA).
“I really don’t believe that President Trump or Secretary Perdue are going to throw agriculture under the bus,” McCauley says.
Higginsville, Mo. dairy farmer Paul Heins trusts Perdue will get things done for farmers and ranchers.
“I was impressed that just a few days on the job he was right on top of issues across agriculture,” Heins says.
Heins was particularly pleased to see Perdue talk about current dairy trade issues with Canada.
“Perdue emphasized that President Trump is a negotiator. We’re going to play fair and we expect our trading partners to play fair,” Heins says.
The halt in ultra-filtered milk across the Canadian border impacts producers like Heins because the milk will need to enter new marketing channels.
“That milk has to find a home somewhere. All of the processing plants are at or over capacity,” Heins says.
Heins was encouraged to hear President Trump was upset and willing standup for dairy producers impacted by the trade disputes with Canada.