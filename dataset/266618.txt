Arizona weighs unusual plan for longer egg expiration dates
Arizona weighs unusual plan for longer egg expiration dates

By MELISSA DANIELSAssociated Press
The Associated Press

PHOENIX




PHOENIX (AP) — This year's Easter eggs may already be hard-boiled, dyed and laid in a basket, but Arizona buyers might wind up with a batch that's a little less fresh next year.
State lawmakers are considering an unusual proposal to put a 45-day expiration date on Grade A eggs, a longer window of use than the current 24-day sell-by date that's one of the strictest in the U.S. That means Arizona consumers could be cracking and cooking eggs that left the farm more than six weeks ago — and food safety experts say that's perfectly safe.
"When it comes to use-by, sell-by or best-if-used-by dates, these are there much more for quality purposes, not so much for safety," said Sadhana Ravishankar, a food microbiologist and associate professor at the University of Arizona.
Republican state Rep. Jill Norgaard said Arizona's strict sell-by rules led her to sponsor the proposal. Other states have 30- to 45-day expiration dates, she said, which reduces food waste by extending the shelf life.
"We throw over 2 million eggs away because of expiration dates in Arizona," Norgaard said.
Supporters include the free-market advocates Americans for Prosperity, Walmart and the Arizona Chamber of Commerce and Industry. The state's grocery lobby, the Arizona Food Marketing Alliance, did not officially weigh in, according to state legislative records.
Norgaard said the proposal would drive down egg costs by luring in out-of-state egg producers who have more time to transport their product into Arizona.
"That will increase the supply and hopefully reduce the price point," she said.
The current iteration of the proposal doesn't create new expiration dates for Grade AA eggs. They would continue to be marked with a 24-day sell-by requirement.
The proposal previously garnered more than three-quarters support from the House of Representatives, and was unanimously moved out of the Senate. It's due for one more vote in the House before it could head to Gov. Doug Ducey. Norgaard said she's optimistic that could happen as early as next week.
Republican state Rep. T.J. Shope cast one of the votes against the measure. His family has been in the grocery business since the 1950s, and he said most consumers purchase Grade AA eggs.
"The average consumer is not going to notice a difference," he said.
Arizona shoppers bought nearly 316 million dozens of eggs last year, up from more than 285 million dozens of eggs in 2016, according to state Department of Agriculture data.
Some consumers might balk at the idea of using month-old eggs. Pat Sparks, a professor at the Department of Nutritional Sciences at the University of Arizona, said eggs with a 45-day expiration date will be safe to eat, though they may lose some moisture as they age.
"The yolk will also become more fragile, however that's not a safety issue," she said. "If you're scrambling your eggs, using them in a cake or something like that, it's not going to have any effect at all."
Oscar Garrison, senior vice president of food safety regulatory affairs at industry association United Egg Producers, said in an email that he's not aware of any other state considering such a measure.
"It certainly is true that eggs may remain safe to eat after the date of expiration, however freshness and quality may begin to be impacted beyond that date," he said.