Papayas are having a moment.
"They're not mainstream, like pineapples or avocados," said Jose Rossignoli, general manager tropicals for Eden Prairie, Minn.-based Robinson Fresh, "but they're definitely becoming more popular than other exotic fruits, like guava or kiwi."
In a recent blog post, Rossignoli said papayas rank third in tropical fruit sales around the world, and global production increased by more than 5 million metric tons from 2000 to 2013, to 12.4 million metric tons.
He linked the increase in production to greater consumer demand, better post-harvest techniques and investment in new varieties.
Papaya's evolution from ethnic exotic to mainstream still has a way to go, Rossignoli admits. In the U.S., five cities account for nearly 75% of papaya sales: Los Angeles, Houston, Miami, Dallas and New York City. All have significant Hispanic and Asian populations. And in 2010, total U.S. per-capita consumption stood at less than 2 pounds a year.
He said lack of mass marketing at the retail level may be hindering sales, and consumers unfamiliar with the fruit don't know how to gauge its maturity and ripeness.
Robinson Fresh is importing large maridol and tainung (formosa) papayas from Mexico.
"The season is going well, with adequate supplies," Rossignoli said, "and we continue to work on varieties to improve the taste experience."
Carlos Rodriguez, tropical sales director for Procacci Bros. Sales Corp., Philadelphia, said he only brings in maridol papayas from Mexico.
"I bring in huge sizes 8, 9 and 12, the bigger the better, because my Latin customers love them," Rodriguez said.
Brooks Tropicals Inc., Homestead, Fla., is importing solos from Brazil and Caribbean Reds from Guatemala and the Dominican Republic "with great 12-count fruit coming in," said sales director Peter Leifermann.
"Demand is ever increasing," Leifermann said, though he warned that papayas are "hypersensitive to weather, so Mother Nature can impact volumes."
The consumer's familiarity with the papaya has produced a preference for a sweet aroma and taste, said Brooks' marketing director Mary Ostlund, while the personal-sized Brazilian papayas "are perfect for the consumer who doesn't need the larger size and likes their papaya a bit sweeter."