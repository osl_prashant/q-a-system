AC Foods, owner of citrus brands Suntreat and Sumo Citrus, has hired Daniel Kass as vice president of sales and business development.
Kass is former director of export sales for Paramount Citrus (formerly Wonderful Citrus) and vice president of export sales and marketing for Dole Citrus.
The Dinuba, Calif., company recently finished a construction project at the AC Foods Legacy Packing facility.
“This buildout puts us 20 years ahead of the global industry in terms of food safety, product handling and traceability,” Kass said in a news release.
The company is expanding its “global citrus footprint as a full range, one-stop shop program supply destination,” according to the release.
President Darren Filkins said in the release that Kass has built citrus programs that align with AC Foods’ aggressive growth strategy.
Kass is secretary of the California Citrus Quality Coucil, according to the release.