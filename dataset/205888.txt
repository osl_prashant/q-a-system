Crop calls
Corn: 3 to 4 cents higher
									Soybeans: 8 to 10 cents higher
									Winter wheat: 5 to 10 cents higher; spring wheat: 1 to 2 cents higher
Soybean futures led gains overnight in an extension of last week's corrective buying. Corn and wheat followed soybeans higher. A chilly forecast is providing incentive for traders to cover short positions in the corn and soybean markets. While some northern areas may see temps get cold enough for a light frost later this week, a hard frost is not anticipated. Still, traders are concerned enough about how crops are finishing with the cool weather to cover short positions. A recent pickup in export demand activity is also supportive, as it signals prices got low enough for global end-users to ramp up purchases.
Wheat futures were led higher by SRW contracts overnight, with HRW futures not far behind. Buying was limited in spring wheat futures by ongoing harvest in the Northern Plains. In addition to spillover from soybeans and corn, a weaker U.S. dollar is also supportive.
 
Livestock calls
Cattle: Steady to weaker
									Hogs: Steady to weaker
Cattle futures are expected to open with a weaker tone coming out of the extended holiday weekend. Fundamental focus will be on boxed beef trade, as that will give traders an indication of holiday beef clearance. With the grilling season unofficially over, a demand lull is anticipated once retailers have restocked beef supplies. Until the cash cattle and boxed beef markets signal lows are in place, buying in cattle futures will be limited.
Lean hog futures are also expected to face general price pressure to open the week. Pressure is expected from the cash hog market as market-ready hog supplies are building. The big discount futures hold to the cash market should limit selling, but corrective gains ran out of steam last Friday, suggesting the upside is capped for now.