Two leaders talked about a 100-day trade plan






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






The past week’s summit between U.S. President Donald Trump and Chinese President Xi Jinping yields initial wins for U.S. beef and financial product trade, but no North Korea agreement. 
Trump and Xi indicated progress in their relationship after they completed their 21-hour summit impacted by the US strike on Syria. Trump’s secretaries of State, Treasury and Commerce, speaking after the summit, said they had not reached specific agreements on curbing North Korea’s nuclear program or cutting the US trade deficit with China, but had established strong chemistry over candid conversations.
On trade policy, "only time will tell," said President Trump.
Averting a trade war. China will offer the Trump administration better market access for financial sector investments and U.S. beef exports to help avert a trade war, according to Chinese and U.S. officials involved in talks between the two governments, the Financial Times reported Sunday. The two concessions on finance and beef are relatively easy for Beijing to make, the newspaper said.
Commerce Secretary Wilbur Ross said the two leaders opted to instigate a “100-day plan” on trade between the two countries that he characterized as unusually speedy, and to include “way-stations of accomplishment.” A primary goal for the U.S. would be to increase exports to China and reduce the U.S. trade deficit, Ross said, but a wide range of products were discussed. He said he was struck that China expressed interest in reducing its trade surplus with the U.S. because of the impact it was having on money supply and inflation. That was the first time he had heard Chinese officials say that in a bilateral context, he said.
President Trump, in Twitter posts, said, "It was a great honor to have President Xi Jinping and Madame Peng Liyuan of China as our guests in the United States. Tremendous goodwill and friendship was formed, but only time will tell on trade."
Regarding currency, Steven Mnuchin, the US Treasury Secretary, said no decision on Chinese intervention in currency setting was reached.
Xinhua said President Trump had accepted Xi’s invitation to pay a state visit to China this year and that the two sides had established four mechanisms for dialogue and cooperation in areas including security, the economy, law enforcement and cybersecurity. Tillerson confirmed that Trump had accepted the visit to China and would work with the Chinese on the date.
 “We made very clear that our primary objectives are twofold,” Ross said Sunday on Fox News. “One is to reduce the trade deficit quite noticeably between the US and China, and two, to increase total trade between the two.” Ross said the meeting between the American and Chinese leaders went well, but “words are easy, discussions are easy, endless meetings are easy. What’s hard is tangible results, and if we don’t get some tangible results within the first 100 days, I think we’ll have to re-examine whether it’s worthwhile continuing it.”
Official Chinese accounts did not mention the 100-day action plan on trade, though they also indicated no progress on North Korea. China placed greater emphasis on Trump’s acceptance of Xi’s invitation for him to visit China and for the U.S. to participate in China’s “Belt and Road” initiative to build trade and transport infrastructure between East and West.






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.