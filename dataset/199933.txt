To GMO or not GMO - that is the question on many farmers minds in 2016. The prospect of reduced input costs and potential for premium can be tantalizing, but is the risk of possible lower yields worth it?Maybe the better question is - why not do both?
According to a recent Farm Journal Pulse poll, most farmers are not necessarily taking that approach, at least not yet. More than 1,500 farmers responded to the question, " What percent of your corn acres this year will be planted to non-GMO hybrids?" Four out of five respondents say they are in all the way - or not at all.
Farmers gave the most common response as planting zero non-GMO acres, with 71%. Another 10% say they're planting every acre to non-GMO hybrids.
That leaves 19% of respondents who say they're doing a bit of both. One in 10 respondents say they are setting aside a small amount of their acres (1-25%) to non-GMO varieties. Another 4% are planting up to half, with 3% going non-GMO on 51-75% of their acres and 2% with 76-99%.

Mike Preiner, vice president of product management at Granular, says there are five areas farmers should explore whendeciding whether or not to plant non-GMO crops, and how much.
1. Assess potential higher premiums for non-GMO crops, which can range from $1 to $4 per bushel in soybean and $0.20 to $0.40 per bushel in corn.
"Rising demand is being met by an increased supply, so premiums are likely to stabilize or go down," Preiner notes.
2. Determine cost savings of non-GMO seed, which can range from $10 to $15 per acre in corn and $40 to $60 in soybean.
3. Review operational cost differences - extra time and costs associated with special handling and other procedures can range from $15 to $30 per acre, Preiner says.
4. Also review input cost differences, which can vary quite a bit by location.
5. Last but not least, review yield potential based on local pest pressures, management practices and other environmental factors.
"Will non-GMO crops be more profitable for everyone? Of course not, especially given the recent downward trend of premiums," Preiner concludes. Farmers need a take a comprehensive approach when making these decisions, he says.