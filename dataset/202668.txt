The Wisconsin Potato & Vegetable Growers Association inducted two potato growers into its Hall of Fame at its annual awards banquet Feb. 8 in Stevens Point, Wis.
Dick Pavelski and the late Donald Hamerski were honored by the association for their significant contributions to the development of the Wisconsin potato industry, according to a news release.
Pavelski is the CEO/owner of Heartland Farms Inc., a large chipping potato and vegetable operation.
He has served as president of the WPVGA, as well as serving on the U.S. Potato Board (now Potatoes USA) and the National Potato Council and in the National Fertilizer Solutions Association.
Pavelski is a past president of the Wisconsin Fertilizer and Chemical Association as well as the Wisconsin Agri-Business Council.
He continues to serve on the WPVGA Chip Committee, according to the release.
Hamerski was the owner of Hamerski Farms Inc. until his death Aug. 9, 2016.
A longtime member of the WPVGA, Hamerski also served as the Portage County Drainage Commissioner for 27 years.
Hamerski received the Century Farm Award from The Rural Life Committee of the Diocese of La Crosse in Sept. 2013, marking his farm's 100 years in business.