In Atlanta, the capitol of the Southeast, grocery retailers experience brisk business.
The area remains highly competitive and the wholesalers that supply the stores must remain fortified and capable of adequately serving their buying needs, distributors report.
"This area is a hotbed for retail," said Andrew Scott, director of marketing and business development for the Nickey Gregory Co. LLC.
According to a recent report in The Atlanta Journal-Constitution, supermarket construction has taken off after slowing in 2008.
 

Newcomers

New and established chains have erected 50 stores in the last five years.
Kroger, Publix and Wal-Mart remain the market leaders and continue to add stores, along with wholesale clubs, according to the report.
Ingles, Whole Foods, Trader Joe's, Target Supercenters and even Dollar General Markets also are adding stores in the region, Scott said.
Since 2014, Phoenix-based Sprouts Farmers Markets has opened 10 stores and Batavia, Ill.-based Aldi Inc. has constructed nearly 20 stores since 2011, according to the report.
The Food Depot, Stockbridge, which operates nearly 40 stores in the region, is also a player in Atlanta as well as in Columbus, Macon and Warner Robins, Scott said.
German retailer Lidl recently added a North Carolina distribution center and should soon enter Georgia as well, he said.
"Produce sales are going well with Aldi and Food Depot," Scott said.
"This is a growing region for retail. They're doing a good job here."
 

Higher expectations

The retail segment remains competitive and today expects more from its suppliers, said David Collins III, president of Forest Park-based Phoenix Wholesale Foodservice Inc., and its sister retail distribution company, Collins Bros. Corp.
"Those that really want to compete have to put in a lot of infrastructure to be able to properly compete in today's marketplace," he said.
Sprouts' expansion in the greater Atlanta area shows the importance of the region, Collins said.
Merchandising varies by retailer and by store, he said.
"Some are doing a good job and are executing product well," Collins said. "You go into other stores and you see them just kind of piling product out there."
The chains are investing heavily in their produce departments, said Matt Jardina, vice president of general business operations for J.J. Jardina Co. Inc.
"It's amazing how, when you go into these retailers, immediately what jumps out at you are these big produce sections in their stores," he said. "Produce and seafood are the big magnets that pull people into the stores. They spend a lot of money and time on presentation to get the customers to come in."
Diana Earwood, vice president of Forest Park's Sutherland's Foodservice Inc., and general manager of Destiny Organics LLC, said the stores' competitive nature keeps retail segment thriving.
"Retailers remain strong and innovative, with the new online shopping process," she said. "The competition and consumer demand are what drives their business. Merchandising occurs through aggressive pricing, local media presence like billboards and signage, along with new store layouts."
 

More product offerings

Sprouts and Trader Joe's are expanding in the region, said Bryan Thornton, general manager of Coosemans Atlanta Inc.
"With all the new growth here, new retailers are coming in," he said. "The growth is there. They're doing well merchandising produce. They have to, or they wouldn't be here."
Coosemans' retail business has increased, Thornton said.
Many of the retailers the wholesaler works with are expanding the diversity of the items they merchandise and more are carrying Belgian endive, a Coosemans staple, he said.
Some of the other hardcore specialties, including cherimoya, dragon fruit and kiwano melons, also are seeing higher retail sales, Thornton said.
Though the region's retail landscape remains competitive, the rate of supermarket sales varies, said Cliff Sherman, owner of Sunbelt Produce Distributors Inc., Forest Park.
"Some areas are doing better than others," he said. "We have some stores that use to do a robust business. They've backed off a bit while others in other areas of town are picking up. It depends on where the stores are."