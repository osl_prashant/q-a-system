Industrial-scale pork on trial in federal nuisance lawsuits
Industrial-scale pork on trial in federal nuisance lawsuits

By EMERY P. DALESIOAP Business Writer
The Associated Press

RALEIGH, N.C.




RALEIGH, N.C. (AP) — Drive past clustered hog sheds containing thousands of animals in the country's No. 2 pork-producing state on the wrong day and the reason hundreds of North Carolina neighbors are suing in federal court is clear: it really stinks.
The question facing jurors in a federal lawsuit starting Monday is whether the open-air animal waste pits that proliferated over a generation generate intense smells, clouds of flies and noise.
The trial's outcome could shake the profits and change production methods of pork producers who have enjoyed legislative protection and promotion in one of the nation's food hubs. Supporters see industrial-scale hog complexes as generating jobs and revenues in rural communities. Friendly lawmakers last year sought to retroactively sink the lawsuits by more than 500 neighbors now lined up to go to trial one after another.
Neighbors like Randy Davis contend the industry's long practice of storing liquid waste in what producers call lagoons, then spraying treated excrement onto nearby fields as fertilizer, spells misery for them.
"A lagoon is something that a pretty 15-year-old in the South Pacific swims in," said Davis, a state employee whose Green County home is about is about ¾ mile (1.15 kilometers) from the nearest impoundment. "This is not a lagoon. It's a cesspool. It's a 9-acre feces and urine pit."
The plaintiffs' attorneys also say that information uncovered about company influence campaigns show their "knowledge of the harmful health effects of its hog operations," the inadequacy of regulations to address those health effects, and the availability of feasible alternatives.
Pork producers say they not only care about their rural neighbors, but their operations are inspected annually to make sure they comply with environmental regulations. There are 14 state inspectors checking 2,100 hog operations.
Hog growers are keeping an eye on the litigation that could force them to make changes or part with big payouts.
"Certainly we're watching it. There's some levels of concern in particular as it relates to possible impact to the economy" of the rural communities where the farms operate and pay wages, said Andy Curliss, CEO of the North Carolina Pork Council, a trade group.
The upcoming string of federal lawsuits target Murphy-Brown LLC, the hog production division of Virginia's Smithfield Foods. Smithfield was bought in 2013 by a division of China-based WH Group, the world's largest pork producer. China, with higher feed and labor costs than American pork producers, is one of the fastest-growing markets for U.S. exports and including Hong Kong now makes up about 9 percent, according to the U.S. Agriculture Department.
The cases are being overseen by Judge W. Earl Britt, who was born 86 years ago in a town of about 130 people along the South Carolina border and joined the court in 1980 after Democratic President Jimmy Carter's nomination.
The first case to be tried next week involves 10 neighbors of Kinlaw Farm, a Bladen County hog operation located 10 miles (16 kilometers) east of a massive Smithfield Foods slaughterhouse. The farm and its owners, who raise hogs under contract with Murphy-Brown, are not defendants. Instead, the target is the company that set specific standards of how the farm must operate.
The neighbors' lawyers hand-picked the plaintiffs for this first trial. A second trial starting later this spring will involve parties selected by hog industry lawyers. The two sides will alternate choosing the players for further trials.
Such test cases allow lawyers to measure the strength of their evidence. The resulting verdicts - and any financial penalties - create guiderails for a possible settlement, said Christopher Seeger, a New York attorney who helped negotiate a $4.85 billion settlement over the health risks of arthritis painkiller Vioxx.
Perhaps more important in shaping the ultimate resolution are the precedent-setting decisions judges make to accept some evidence and keep other details from jurors, Seeger said. Hog industry attorneys say the trial should be strictly limited to whether the 10 suing neighbors are suffering with a foul nuisance. They don't want jurors to hear about alternative disposal hog waste methods used elsewhere, industry lobbying, or evidence of Chinese ownership.
"You generate data from these bellwether trials that are very informative," Seeger said. "Where people would get more, where people would get less" based on how evidence persuades jurors.
___
Follow Emery P. Dalesio at http://twitter.com/emerydalesio. His work can be found at http://bigstory.ap.org/content/emery-p-dalesio .
___
Associated Press writer Allen G. Breed contributed to this story.