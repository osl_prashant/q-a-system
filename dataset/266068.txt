BC-IN--Indiana News Digest 1:30 pm, IN
BC-IN--Indiana News Digest 1:30 pm, IN

The Associated Press



Here's a look at AP's Indiana news coverage at 1:30 p.m. Questions about coverage plans are welcome and should be directed to the AP-Indianapolis bureau at 317-639-5501, 800-382-1582 or indy@ap.org. Tom Davies is on the desk, followed by Herbert McCann.
All times are Eastern.
A reminder: This information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Eastern. Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
TOP STORY:
DIESEL SPILL-SETTLEMENT
SOLITUDE — Marathon Petroleum Corp. has agreed to pay $335,000 for spilling nearly 36,000 gallons of diesel fuel into the Wabash River near the Indiana-Illinois border in 2016. The company's settlement with the U.S. Environmental Protection Agency calls for $109,000 to go to Illinois, which pursued civil penalties based on violations of state-level environmental regulations. The settlement comes as about 42,000 gallons of diesel fuel spilled last week into Big Creek near the Posey County community of Solitude. SENT: 130 words. UPCOMING: 300 words.
AROUND THE STATE:
CHINA-US TRADE-INDIANA FARMERS
LAFAYETTE — Some Indiana farmers are worried that if China imposes tariffs on U.S. soybeans it could deal another blow to farmers who've been battered by soybean prices that have dropped by nearly half since 2012. More than half of the soybean produced in Indiana is exported and China is one of the main importers of Indiana soybeans, said Kevin Underwood, a Tippecanoe County councilman and a long-time soybean farmer. UPCOMING: 300 words.
EXCHANGE-DISTRACTED DRIVING
GARY, Ind. — Students at Thea Bowman Leadership Academy are learning more than math, English and computer technology. They also are learning about the dangers of distracted driving. To drive home that reality, AT&T brought a virtual reality simulator to the high school. Students and staff — some of whom will be heading out for spring break travel shortly — experienced firsthand how dangerous it is to take their eyes off the road and glance at a cellphone. By Carmen McCollum. The (Northwest Indiana) Times. SENT: 550 words, photos pursuing.
IN BRIEF:
— INDIANA FIRE: A judge has denied bond for a northwestern Indiana mother who faces neglect charges after her two young children died in an apartment building fire. Thirty-three-year-old Kristen Gober of Gary appeared in court Wednesday after she was charged Tuesday with two counts of child neglect causing death and other charges. With AP Photos.
— POLICE SHOOTING-INDIANA: Authorities say a police officer has shot and critically wounded a man in northern Indiana following a physical confrontation that also injured the officer.
— JEFFBOARD SHUTDOWN-REDEVELOPMENT: A southern Indiana mayor says the impending closure of a barge and towboat manufacturer could open riverfront property to new development in his Ohio River city.
— DELAWARE COUNTY CRASH: Eastern Indiana police say a fourth person has died following a February collision between two cars.
— GEOTHERMAL UTILITY BILL: The city of Evansville has placed a $1.5 million lien on an apartment building it says continues sending water into local sewers without paying.
— OFFICER DRAGGED-APPEALS COURT: An appeals court has upheld a Muncie man's convictions that stemmed from an attempted traffic stop in which he was shot by a police officer.
— CHILD DEATH-POOL: Police say the death of a 3-year-old boy in a pool outside of a home in Indianapolis is being investigated as an accident.
— HOLCOMB ADMINISTRATION: A high-level Republican staffer in the Indiana House of Representatives has been appointed by Gov. Eric Holcomb to lead the state's purchasing and property management agency.
___
If you have stories of regional or statewide interest, please email them to indy@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.