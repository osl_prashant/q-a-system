With the election nearly a month away, how could the markets react?On Market Rally Radio, Glen Ring of Glen Ring Enterprises, LLC. said since March 2015, the dollar "has been on vacation." The dollar could run up by the end of 2016, putting in cycle lows. This could mean good recovery in 2017.
Ring also comments on how in presidential election years, corn sees either a low or a high.
He cites the 2012 drought, the 2008 crash, and the 2004 cycle low.
Ring said he's optimistic long-term, but farmers could see "a lot of ugliness."
Listen to Ring on Market Rally by clicking here.