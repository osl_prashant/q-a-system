Nobody expects Mexico’s grape production to approach last year’s more than 21 million-carton bounty, but this year should fall into the “promotable volume” category, growers and shippers say.
The take in Mexico this year should be closer to 16 million to 18 million boxes, which falls well within the normal range, said Jerry Havel, sales and marketing director with Nogales, Ariz.-based Fresh Farms.
“I think we’re going to have a normal crop of very good quality grapes,” he said.
There were some hurdles growers had to clear during this year’s growing season, Havel said.
“We had a really dry, warm winter and then, right after bloom came out, we had some cold and that affected the grapes,” he said.
The cold snap likely was the only factor that prevented another 20 million-box crop, he said.

Early reports of a super-light crop were overly alarmist, premature and, frankly, wrong, as early reports often are.

Grape volume out of Mexico will be lighter this year, but there will be plenty of product to promote, said Juan Alberto Laborin Gomez, director of Hermosillo, Mexico-based Sonora Spring Grapes.
“We had lower-than-normal chilling hours, a cool January and February and few days close to freezing temperatures,” he said.
It’s tough to expect an equal to 2017 production, though, Gomez said.
“Last year was Mexico’s biggest crop ever,” he said.
Bunch count is lower this year, and production won’t approach 2017, but there will be “plenty of grapes most of the season, especially the last two weeks of May through all June in all varieties,” Gomez said. “For sure, plenty to promote.”
John Pandol, director of special projects with Delano, Calif.-based Pandol Bros. Inc., said he visited orchards in late March and returned with a strong sense of optimism about the crop in Mexico.
“Early reports of a super-light crop were overly alarmist, premature and, frankly, wrong, as early reports often are,” he said. “The first 500,000-box day will be in the week of May 21 and I estimate the first 3 million-box week will be the week of May 28.”
The cold weather created a bit of an erratic crop, said Jared Lane, vice president of marketing for Bakersfield, Calif.-based Stevco Inc.
“Last year, we had a really warm winter, so the plants continued to grow throughout the winter, which caused kind of an erratic bud break, and during that, we had a freeze,” he said.
Damage was minimal, he said.
“This year it’s going to be a little smaller volume than last year, but Mexico can be somewhat cyclical,” Lane said.

This is still a substantial number in a very small window and will need a concerted effort by all to move quickly to the consumer.

The most significant issue facing the crop this year is a lack of chill hours prior to pruning, said Kelly Dietz, vice president of business development with Vero Beach, Fla.-based Seald Sweet International.
“This will result in a lower yield than in 2017 and more in line with the volume seen in 2016,” Dietz said, noting that volume in 2017 was 21 million boxes and, in 2016, was about 16 million.
“The consensus amongst growers is that the 16 million number is a fair estimate for the 2018 season,” Dietz said.
“This is still a substantial number in a very small window and will need a concerted effort by all to move quickly to the consumer.”
Mexico has four areas of production within Sonora, which account for practically all of exported fresh table grapes: Hermosillo, Caborca, Guaymas and, more recently Obregon, Dietz said.
Miguel “Miky” Suarez, owner of Rio Rico, Ariz.-based MAS Melons & Grapes, also recently toured orchards in Mexico and forecast a lighter crop than a year earlier.
“After I personally visited both the Hermosillo and Caborca growing areas, what I can tell you is that the yields and the production in both areas will be definitely lower compared to last year’s production,” he said.
According to the U.S. Department of Agriculture, as of March 30, 18-pound containers of bagged thompson seedless grapes from Chile were priced at $28-30 for extra-large and $26-28 for large.
A year earlier, the same product was $32-34 for extra-large; $28-30, large; and $16-18, medium-large.