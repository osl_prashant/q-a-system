At the Southern Research and Outreach Center (SROC) in Waseca, Minn., we have found it has been more challenging to maintain good calf growth and health in the summer than in the winter months. Factors contributing to calf performance challenges include:High ambient temperature and humidity - This can result in reduced calf starter intake and higher incidence of infections. The Upper Critical Temperature (UCT) for calves has been shown to be 78¬?F or greater. The potential for heat stress occurs when combining temperature with high relative humidity expressed as temperature humidity index (THI). A THI of 72 or greater can cause calves discomfort. In such situations, calf panting and sweating do not compensate for this stress and calf energy needs will increase which must be supported by adequate nutrition. In addition, as calves attempt to maintain body temperature in the summer months increased respiration and sweating results in water losses which have to be replaced. As environmental temperature increases, water intake increases accordingly.
Access to water and effects of feed supply - These are important factors. Research has indicated that the amount of water needed by nursery calves depends not only on the environmental conditions but also on the incidence of scours and the amount of milk/milk replacer and starter intake. Water intake is closely related to starter intake which in turn drives rumen development. However, water intake may increase independent of starter intake when temperature is above the UCT for calves. Research has shown the amount of liquid in milk replacer also affects amount of water consumed. Water temperature can also affect water intake.
Other stressor factors that affect calf performance - These include management of the housing environment (individual and group-fed calves; clean uncontaminated water offered; ensure water buckets and facilities are clean), exposure to direct sunlight, less than adequate ventilation, and fly population control. In addition, calves born from heat-stressed dams will often have light birth weight, depressed intake and compromised immune function with the potential for rapid dehydration.
How do pre-weaned calves respond to milk and starter feed without additional water?
In a study by Kertz et al. (1984), calves were offered ad libitum (free choice) or no water when fed 4.2 pounds of milk replacer (11.4% solids) twice daily for 3 weeks and once daily in week 4 prior to weaning. There were no differences in daily gain for the first 3 weeks but greater gain in week 4, and 38% overall for calves offered ad libitum water. Starter intake was higher for calves fed ad libitum water from week 2 onward with the largest differences in weeks 3 and 4. No effect of water intake on scours incidence was observed. The authors emphasized the importance of ad libitum water to support good starter intake and growth.
 More recently, SROC revisited this work by offering restricted or free choice water in the pre-weaning period during the summer months (Manthey et al., 2011). Calves on the restricted treatment were offered no water for 35 days, then 5 pounds daily from day 36 to weaning at 42 days. Both groups were fed water free choice after weaning. Calves were individually fed a 20:20 milk replacer (12.5% solids) twice daily for 35 days and once daily from day 36 to weaning at 42 days. An 18% texturized calf starter was offered free choice from days 1 to 56.  Figure 1 depicts study results. Water intake and dry matter intake (DMI) increased in both groups from day 35. Contrary to the previous work, there were no differences in DMI and calf performance pre- and post-weaning. There were no differences in scouring days and health costs.
 Information from a recent SROC study from April to July (Strayer, 2014) evaluated water and DMI when calves were fed a 24:20 milk replacer at 0.62 pounds in 4.25 pounds water (A) or 1.56 pounds in 5.3 pounds water (B) twice daily for 35 days and once daily from day 36 to weaning at 42 days. Both milk replacers were fed with 14.7% solids. An 18% texturized calf starter was offered free choice from days 1 to 56. Table 1 summarizes calf performance. Water intake increased as DMI increased, especially from day 35 onward. Calves fed higher milk replacer levels had lower starter intakes. Calves gained well and had good frame growth. Number of scouring days were lower than in previous summer studies.
Take-home messages
Managing calves during the summer requires attention to a number of factors. Water availability is important for calf health and does impact feed intake and growth, especially when milk feeding is reduced or the calves are weaned.
Table 1. Daily feed DM and water intake by calves fed a 24:20 milk replacer at 1.25 (A) or 1.5 lb./d (B) at 14.7% solids with an 18% texturized calf starter. 

Days in the nursery


Total DMI, lb./d


Starter intake, lb./d


Water intake, lb./d


Total DMI, lb./d


Starter intake, lb./d


Water intake, lb./d


Pre-weaning


A


A


A


B


B


B


7


1.27


0.05


2.11


1.55


0.05


2.84


14


1.52


0.30


2.31


1.67


0.17


2.82


21


1.81


0.58


3.15


1.92


0,40


3.48


28


2.21


0.98


3.87


2.23


0.71


4.07


35


2.43


1.50


5.34


2.33


1.19


5.65


42


3.77


2.84


9.53


3.81


2.67


10.08


Post-weaning




















49


4.76


4.76


16.00


4.68


4.68


16.68


56


5.43


5.43


18.50


5.47


5.47


19.32