Chris Boland from Salem, Indiana restored a 1952 John Deere A and then sold it to Vernon Morton of Butner, North Carolina.

After serving its time in the Hoosier state, Morton brought the tractor for retirement in the Tar Heel State, showing it in parades along the East Coast.

Watch the story on Tractor Tales on U.S. Farm Report above.