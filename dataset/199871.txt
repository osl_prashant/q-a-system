Like safe, nutritious foods? At an affordable price?Genetic modification has long played a role to bring that to your dinner table. Every day.
Although it's been highly debated. Often criticized. And never fully appreciated.
It's a well-known food fight. One that CBS News tackled this weekend. Taking a look at GMOs from different viewpoints in its piece,Digging for Seeds of Truth in GMO Debate.
A Hawaiian papaya grower, a plant geneticist, an anti-GMO activist and even the CEO of Monsanto talked with reporters. Scientific studies were presented. Questions were asked. Conversations were had.
The segment showed how biotechnology saved Hawaiian papaya farms from extinction. It offered an insight to Golden Rice, a genetically modified food that could feed starving populations.
It showed a promising technology with endless possibilities. To grow even more drought-tolerant crops. Combat disease and insects with fewer pesticides. And fight hunger.
All are important as we face changes in weather patterns, a growing population and decreasing amount of farmland.
Yet there's still a push to ban GMOs right here in the United States. That would restrict our ability to grow food. Exposing our fields even more to drought and disease. Leading to damaged crops. And potentially more hardship.
So, after all that, is the fear of GMOs justified? I don't think so.
Because countless peer-reviewed studies prove the safety of GMOs. Science supports their use. And we've been genetically modifying food for centuries.
Farmers and ranchers have taken advantage of the technology. Growing more food. On less land. It's efficient and productive. Making American agriculture stronger. Self-sufficient.
And farmers are feeding those foods to their families. That shows their confidence in the safety of GMOs.
It's a confidence I, and Texas agriculture, hope you embrace.