My wife is a first-grade teacher and lately, she has told me that a few kids in her class have begun to talk about mortality.
For example, she is reading Dr. Seuss and one of the boys interjects, “Dr. Seuss is dead!”, a remark followed by murmurs and startled questions of “He is?”
Sally would say, yes, Dr. Seuss, would be a very old man if he was alive today. Indeed, Theodor Geisel, better known to the world as the “Green Eggs and Ham” author Dr. Seuss, was born in 1904. He died in 1991, long before these first graders of 2017 were a gleam in their parents’ eyes.
The point is — which some of these first graders are haltingly coming to terms with — is that folks die.
Just as the very young, we aging baby boomers may find it equally difficult to conceive that our day of reckoning will eventually come. Between managing our investments, enjoying our grandkids and going to the health club, there seems to be no horizon in our world.
However, the Census Bureau puts it in no uncertain terms with a recent article on their website headlined “As Population Ages, U.S. Nears Historic Increase in Deaths”
Here are few “highlights” from the report:

Deaths are projected to reach more than 3.6 million in 2037, 1 million more than in 2015;
The number and percentage of people who die will increase dramatically every year, peaking in 2055 before leveling off gradually;
While the U.S. population as a whole is not expected to experience natural decrease (fewer births than deaths), two states (West Virginia and Maine) and almost a third of counties experienced this demographic threshold in 2015.
With the highest median age of all racial and ethnic groups (43.3), the non-Hispanic white alone population is the only group projected to experience natural decrease in the near future.
Florida had the highest percentage of its population age 65 and over in 2015 (19.4%), followed by Maine (18.8%) and West Virginia (18.2%).
Sumter County, Fla., has a majority of its population (54.8%) age 65 and over. 
Based on data used in the population estimates, nationally, there were 8.2 deaths per thousand people in 2015. That ranged from a high of 11.8 deaths per thousand in West Virginia to a low of 5.2 in Utah. 
 Other states with the highest ratios of deaths to the total population include Alabama, Pennsylvania, Maine and Mississippi. 

With nearly a third of U.S. counties expecting a natural decrease in population because of rising death rates, trouble may be coming for those communities. As the story says “Unless something changes and those increasingly graying counties can attract more migrants, their populations will likely continue to decline.”
Alas, we baby boomers could solve this looming problem - if only we would be alive to take it on. It is just as one of Dr. Seuss later works says, “You’re Only Old Once!: A Book for Obsolete Children.”
For a good read about how the food industry may adjust to an aging population, youngsters can check out this report from Agri-Food Canada, “Market Opportunities for Foods with Added Health Benefits for an Aging Canadian Population”