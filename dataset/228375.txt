Sponsored Content
When spring weather delays planting, Joe Lauer, agronomist at the University of Wisconsin, often gets asked, “How late can corn be planted?” The short answer is Aug. 1. However, your production objectives need to change. 

In order to determine what options are still available, Lauer says to start by figuring out how many Growing Degree Units (GDUs: base=50, max= 86) can still accumulate during the growing season. By back calculating the number of GDUs remaining after a planting date until the average frost date (<32° F), a farmer can determine the best relative maturity for the remaining growing season. For example, the total GDU accumulation between Jan. 1 until the average fall frost date is 2700 GDUs at Arlington and 2500 at Marshfield (Figure 1). Another 200-300 GDUs are required to dry the crop while standing in the field after it matures. 

Figure 1. Average Growing Degree Unit (GDU) accumulation at Marshfield and Arlington, WI. Weather data obtained from Bill Bland (AWON and UW-Soils) and the Midwest Region Climatological Center. The curves represent 30-year averages (1983-2012).


All hybrids require a similar amount of GDUs to complete grain filling (~1000-1200 GDUs). The main difference between hybrids with different maturity ratings is the time required to achieve silking (Table 1). 
Long-season hybrids (110-115 d RM) in Wisconsin require about 1500-1700 GDUs, while shorter-season hybrids (80-85 d RM) require about 800-900 GDUs. Plants respond to these GDU requirement differences by producing fewer leaves, which can be further influenced by photoperiod (latitude).
Table 1. Corn Growing Degree Units (GDUs) required to achieve silking, 50% kernel milk and maturity growth stages.

Grain and Silage
Table 2 presents suggested hybrid maturities for planting dates during the month of June and the remaining GDUs that can be accumulated by the average frost date. For example, on a June 10 planting date at Arlington there are 2060 GDUs remaining for the growing season (Figure 1). An 80-90 d RM hybrid requires about 1700-2200 GDUs to mature while a 95-100 d RM hybrid would be at 50% kernel milk after the same number of GDUs (Table 1). Thus, proper maturity selection depends upon the production objective in June.
Table 2. Remaining GDUs and suggested corn hybrid maturity for planting dates at Arlington and Marshfield.

Table 2 also shows the last planting dates for corn to produce grain for silage. The last dates to produce corn grain are June 1 in the north and June 10 in the south. For silage it is June 20 in the north and July 1 in the south. 
With corn silage we have two forage-quality peaks: one at silking, the other near maturity (see Figure 1 at http://wisccorn.blogspot.com/2012/07/harvesting-barren-and-poorly-pollinated.html). In a normal silage situation, we want to select a maturity that gets us to the second peak. In a biomass production situation, we want to hit the first peak. To do that at Marshfield on July 1 when we only have 1570 GDUs remaining in the growing season (Table 2), we would choose a hybrid that is 110-115 d RM so it silks when a frost occurs. 
For a silage production situation, long-season hybrids are the ones to choose so that silking occurs when a killing frost occurs. Frost kills the plant and drying will need to occur before it can be properly ensiled. The risk in this situation is that many acres could be ready at the same time and be difficult to harvest in a timely fashion.
 
Sponsored by Lallemand Animal Nutrition