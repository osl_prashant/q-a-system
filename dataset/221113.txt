A new value-added product will soon be available from Kennett Square, Pa.-based Gourmet’s Finest Mushroom Co.
Two varieties of Mushroom Crumble are set to debut Feb. 19 — plain white button mushrooms, and a blend of buttons, shittakes, criminis and portabellos.
Mushroom Crumble “takes blendability to the next level,” said Alan Kleinman, sales and business development manager.
The Blend initiative by the Mushroom Council has spurred more companies to create value-added products so consumers can more easily include mushrooms in meat dishes like burgers and meatloaf.
Mushroom Crumble comes in an 8-ounce package for retail, a 32-ounce package for club stores, and a pair of 5-pound tubs or trays for foodservice.
Shelf life on the products is expected to be 10-12 days. Mushroom Crumble is sold in packaging with technology from Atlanta-based Maxwell Chase.
“We have had a lot of interest with potential (retail customers) as well as the foodservice and pizza industry,” Kleinman said.