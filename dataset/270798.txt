(UPDATED April 18) Scorpions generally aren’t life-threatening arachnids, but, for a Maryland couple who found one in a bag of spinach mix, they can be startling. 
The bag of spinach was bought from a Giant store recently in Chevy Chase, Md. When the couple got around to opening the bag three days later, they found the scorpion, they told NBC News.
 
“I saw something inside the bag crawling,” said Sri Sindhusha Boddapati.
 
Initially, she suspected a cricket.
 
She was wrong.
 
Boddapati said she captured the scorpion in a water bottle and sent a cellphone video of her find to her husband, who was at work. 
 
She then carried the bottle to the Giant store.
 
The store pulled the Giant private-labeled spinach, according to media reports, but it did not issue a recall. 
 
Landover-based Giant Food also responded with the following statement:
 
“Customer satisfaction is our highest priority. We regret any inconvenience to our customer. We take the quality of our products very seriously, and we are following up with the supplier to take every step to ensure this isolated incident does not occur in the future.”
 
The chain did not identify the grower or growing region and declined to answer any other questions.
 
According to Jason Strachman Miller, a communications specialist with the Food and Drug Administration, no consumers have contacted the FDA about a scorpion in a salad. The FDA followed up with the Giant store the salad came from and the manager said they "did not hear from the consumer either." The state's health department has not been contacted either.