You can't say the Florida citrus industry is averse to change.
 
Since the arrival of citrus greening or huanglongbing, nearly everything has changed, and after a period of fight and survival in a new normal, growers are looking to reestablish the state's place as a citrus leader.
 
This week is the last of our series by staff writer Ashley Nickle looking at the Florida citrus industry's changing landscape. 
 
The past year or two brought several new ways to fight back against HLB, but there's still no long-term solution.
 
Growers, who used to be tough competitors, report much more cooperation among themselves to find techniques that are cost-effective. 
 
One grower said it took years for them to discover that HLB won't be beaten individually.
 
Florida's battle with HLB also coincided with the rise in popularity of easy-peel varieties, led by consumer brands Halos and Cuties.
 
Florida citrus growers may have a solution here too.
 
About four years ago, they found bingo, a greening-tolerant mandarin variety with an early market window and a deeper orange color inside compared to the 
popular California varieties.
 
Time will tell if this variety can compete in the consumer market and survive HLB long-term, but it's clear there's a new way of thinking in the Florida citrus industry.
 
Did The Packer get it right? Leave a comment and tell us your opinion.