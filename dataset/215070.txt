It’s been 10 years since my editor called me up and asked me if I’d be comfortable shooting video of myself reviewing value-added produce in my kitchen. 
At the time, I’d just moved from Kansas City to a telecommuting position in Austin, Texas, and was a little worried about making sure people remembered my face at the office. 
What my editor didn’t know is that I’ve been doing goofy stuff like this since I was in middle school. I’m a ham, a performer, and have always loved putting myself up on a stage. 
Looking back at that first Pamela’s Kitchen, with its bad lighting, ubiquitous rental unit cabinets and poster board Squeezy Fruit Scale is painful, and hilarious. 
You can see it on ProduceRetailer.com, by the way. 
The first product I reviewed was Mann Packing’s Ready Set Steam, which included broccoli, cauliflower and a cheese sauce, which I affectionately called “goo.” 
For a fun 10th Anniversary Celebration, I hosted a Facebook Live video with my boys where I came full circle and cooked Mann’s Nourish Bowls, which, coincidentally, also include a packet of “goo.” 
I’ve seen some winners and some losers over the past 10 years. Some of my favorite items are some of the most recent, like blendable mushrooms from To-Jo Mushrooms and Monterey Mushrooms. Ocean Mist Farms worked tirelessly to bring steamed artichokes and Brussels sprouts to market that were not only fresh but perfectly cooked. I reviewed Season & Steam back in the early days, and have loved watching the line grow. 
Stinkers were few and far between, and usually left on the cutting room floor. One my son Ike can remember clearly included a seasoning packet to make a kale apple smoothie that did no favors to the kale or apples in the equation. 
Salsa blend-up kits with refrigerated tomatoes went over with a resounding thud, and the early days of microwave steamer bags smelled like burning plastic when they were cooking. 
The changes we’ve seen in packaged produce over the past 10 years are astounding. We’ve seen major leaps in shelf life, convenience, and, most importantly, consumers themselves. 
Consumers are ready to accept flavors and concepts they simply weren’t 10 years ago. Think of it: If a produce company came out with steamed broccoli with cheese sauce today, would a retailer jump to put it on your shelf? Probably not.
What about Spicy Thai Fresh Veggie Noodles? Or Southwest Chipotle Cauliflower? That’s more like it. 
It’s an exciting time to be in produce, celebrating the possibilities of flavor — and convenience.
Pamela Riemenschneider is editor of Produce Retailer magazine. E-mail her at pamelar@farmjournal.com.
What's your take? Leave a comment and tell us your opinion.