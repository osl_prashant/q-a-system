BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Monday:
Open  High  Low  Settle   Chg.ORANGE JUICE                          15,000 lbs.; cents per lb.                May     142.55 146.20 142.05 145.85  +2.60Jun                          145.95  +2.90Jul     142.40 146.40 142.10 145.95  +2.90Aug                          146.15  +2.85Sep     144.10 146.50 143.55 146.15  +2.85Nov     144.60 146.75 144.60 146.70  +2.80Jan     145.20 147.25 145.20 147.20  +2.45Feb                          147.95  +2.35Mar     147.85 147.95 147.85 147.95  +2.35May                          148.80  +2.35Jul                          149.25  +2.35Sep                          149.55  +2.35Nov                          149.90  +2.35Jan                          150.45  +2.35Feb                          150.75  +2.35Mar                          150.75  +2.35May                          151.05  +2.35Jul                          151.40  +2.35Sep                          151.75  +2.35Nov                          152.10  +2.35Jan                          152.40  +2.35Feb                          153.05  +2.35Mar                          153.05  +2.35Est. sales 1,895.  Fri.'s sales 1,539    Fri.'s open int 13,211