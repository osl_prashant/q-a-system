Tuesday’s weekly Crop Progress report from USDA showed corn maturity is still delayed, and that conditions for corn, soybeans and even cotton have improved.Despite extensive damage to the Texas cotton crop from Hurricane Harvey, USDA says the stat’s cotton crop improved by one point, compared to last week. USDA says 59% of the Texas cotton is rate good to excellent, 21% of it have bolls open. The national cotton condition is good at 65% good to excellent. It’s slow maturing like the corn crop though, 25% has open bolls, which is five points behind the five-year average.
This week, USDA says 61% of the nation’s corn crop is rated good to excellent, that’s down one point from a week ago. State by state, conditions are a mixed bag. Corn conditions in Illinois and Iowa improved, but Indiana and Minnesota declined. Slow maturing corn is plaguing the top corn states. Overall, maturity is six points lower than the five year average of 18%. Illinois and Iowa are behind 13 points each, Minnesota is five points behind, and Indiana and Nebraska are both three points behind.
 

<!--
if("undefined"==typeof window.datawrapper)window.datawrapper={};window.datawrapper["PoFff"]={},window.datawrapper["PoFff"].embedDeltas={"100":523,"200":441,"300":441,"400":400,"500":400,"600":400,"700":400,"800":400,"900":400,"1000":400},window.datawrapper["PoFff"].iframe=document.getElementById("datawrapper-chart-PoFff"),window.datawrapper["PoFff"].iframe.style.height=window.datawrapper["PoFff"].embedDeltas[Math.min(1e3,Math.max(100*Math.floor(window.datawrapper["PoFff"].iframe.offsetWidth/100),100))] "px",window.addEventListener("message",function(a){if("undefined"!=typeof a.data["datawrapper-height"])for(var b in a.data["datawrapper-height"])if("PoFff"==b)window.datawrapper["PoFff"].iframe.style.height=a.data["datawrapper-height"][b] "px"});
//-->

 
This week USDA left the national soybean condition alone. Again, the nation’s soybeans are 61% good to excellent. Some states improved, including Illinois, Iowa, and Minnesota which are all one point higher. Soybean defoliation is just beginning, at only 11% nationwide. However, that’s only one point behind the five year average.

<!--
if("undefined"==typeof window.datawrapper)window.datawrapper={};window.datawrapper["I3JmE"]={},window.datawrapper["I3JmE"].embedDeltas={"100":575,"200":467,"300":441,"400":426,"500":400,"600":400,"700":400,"800":400,"900":400,"1000":400},window.datawrapper["I3JmE"].iframe=document.getElementById("datawrapper-chart-I3JmE"),window.datawrapper["I3JmE"].iframe.style.height=window.datawrapper["I3JmE"].embedDeltas[Math.min(1e3,Math.max(100*Math.floor(window.datawrapper["I3JmE"].iframe.offsetWidth/100),100))] "px",window.addEventListener("message",function(a){if("undefined"!=typeof a.data["datawrapper-height"])for(var b in a.data["datawrapper-height"])if("I3JmE"==b)window.datawrapper["I3JmE"].iframe.style.height=a.data["datawrapper-height"][b] "px"});
//-->

 
Wes Mills contributed to this story.