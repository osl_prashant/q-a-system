There’s no shortage of media gushing over the test-tube, alt-meat start-ups that are promoting the eco-benefits of so-called cellular agriculture.
“In 20 years, we want to be producing more than half of the world’s supply of all of the foods we’re getting from animals,” Patrick Brown, the CEO of Impossible Foods, has stated.
Some observers have taken to calling the technology used to fabricate meat-like protein from plant-based sources as “The Second Domestication of Agriculture.” That’s a little over-the-top, given that the production of such alt-meat products is truly in its infancy.
But what about the proverbial Year 2037 that Brown is referencing? Could this new technology revolutionize how the world produces food?
To answer that, it’s important to disaggregate what most media accounts have failed to do: Separate competing technologies at play in this emerging “factory foods” category.
Impossible Foods is marketing a formulated patty (the “Impossible Burger”) using plant proteins and a proprietary mix of oils and binders to replicate the tissue structure of ground beef, then adding heme from animal blood to produce a “burger” that quite honestly, tastes pretty good, looks somewhat similar to actual hamburger and actually “bleeds” like real ground beef.
That technology has arrived, as have their products, which are being sold at Whole Foods and other premium-priced retail outlets.
Likewise, Beyond Meat is marketing the Beyond Burger, which is also a plant-based shamburger patty made with pea protein and a slew of gums, binders, flavorings and extracts. Seeing as how it’s all plant-based, you might think it’s a “healthier” alternative to greasy, fat-filled ground beef, right?
But the Beyond Burger, for all its healthier-then-thou positioning, is 66% fat, based on relative calories-per-nutrient. That compares with about 60% of calories from fat in a typical 80-20 ground beef patty.
Of course, the promoters of products in the Veggie Burger 2.0 category claim that there’s no animal fat in them, but safe to say: if you want the juiciness and mouthfeel of real meat, the fat has to come from somewhere.
A Point of Difference
When pundits and promoters talk about replacing livestock production with test-tube production, however, they’re referring to a different process. Typically described as creating “cultured meat,” or “in-vitro meat,” that process involves using an animal-based substrate to grow meat-like tissue under controlled, laboratory-like conditions.
Albeit on an industrial scale.
Most experts believe commercialization of cultured meat is still years, maybe decades away.
Nevertheless, proponents of the emerging alt-meat category have their eco-talking points burned into every sound bite, embedded into every video clip, and prominently quoted in every fawning piece of media coverage, much of it generated because some of the early investors are prominent members of The Billionaire’s Club (Bill Gates, Richard Branson, Peter Thiel).
These days, nothing carries more clout than those nine (or ten) zeroes in your net worth statement.
And to date, that is the chief barrier to more rapid advancement of cultured meat: money. In 2013, the project to create a cultured meat patty in the lab cost north of $330,000. Four years later, that number has dropped to about $400 a patty.
Which is why it’s going to be awhile before such cultured products are competitive with animal foods.
Not even Bill Gates is in the market for $400-dollar shamburger.
Here’s one other wrinkle to the plants-versus-animals saga playing out in the food labs and PR offices across the country.
As these factory foods manage to acquire market share, they are putting “traditional” animal agriculture into ever sharper contrast.
Do you want to feed your family a shamburger made from formulated ingredients concocted in a lab and processed in sterile tanks inside some converted warehouse?
Or would you prefer “real” meat, poultry and dairy products produced with the power of sunlight, rainfall and the power of Nature?
Even if eco-benefits and vegetarian purity lure a large percentage of consumers into buying formulated and/or cultured analog animal products, there is a marketing advantage to be had for foods from actual farms tended by actual farmers raising actual animals as people have done for millennia.
If nothing else, the appetite for meat among the additional 2 billion people expected to be alive in 2037 will more than make up for any market loss suffered by North American producers if they do have to go head-to-head with those factory-fresh alternatives.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.