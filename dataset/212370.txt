Every business has them — tools. Within each organization exists an array of equipment, gadgets and gizmos which, alongside terminology unfamiliar to outsider ears, is dedicated to its craft. 
The produce aisle is no exception. Here are a few thoughts on some of the things retailers regularly use to keep produce moving from the back dock to the customer’s shopping cart.
 
Electric jack: Produce-laden pallets are heavy. OK, real heavy. Make sure this back-saving piece of equipment is in good running order and fully charged so when the load arrives it is ready too. 
 
Also, make sure outside vendors know who has dibs so when your truck does show up, it’s all yours.
 
U-boats: These large, U-shaped carts are great for carrying heavy loads through narrow aisles. Ensure the produce department has several reserved.
 
Produce carts: Every non-produce department thinks so many of these handy carts “belong” to them. Round up all these stray carts as you’ll need them every shift, every day (no matter who wrote “deli” on a couple using a black marker). 
 
Cooperation is vital in any store, but carts have a way of disappearing, even in the best-run operations. If your location is short, lobby your store manager to spring for extras.
 
Prep table: Keep your tables clutter-free, supplies organized and accessible for all the overwrapping, sorting and cutting you do at this produce workbench each day. Clean and sanitize daily.
 
Knives: All cutting tools need to be kept sharp. Replace worn, broken, or chipped knives/blades immediately. Make sure everyone is issued a sheath, and emphasize safety for every task. 
 
Overwrap station: Not much upkeep needed with an overwrap machine. Most perishable departments use them and produce departments do as well for numerous jobs. Just keep them clean and in good working order and turn off the heat plate when not in use. 
 
Technology: For produce managers this is usually limited to the hand-held ordering device. Make sure that you communicate with other department heads when you need it, for how long, and, along with normal care, store it in a secure place. Keep the batteries charged.
 
Cleaning supplies: Stop me if you’ve heard this — “It only takes five minutes to sweep up a mess, but it takes 40 minutes to find the broom!” Don’t be this produce department. 
 
Strive to have your own dedicated set of cleaning tools, vacuums, brushes, etc. And then, insist on having a designated area to store everything in the same place, all the time.
 
Elementary stuff? To be certain. However, if you keep tabs on your tools and equipment, productivity — and with it your sales and gross profits — will be that much better.
 
Armand Lobato works for the Idaho Potato Commission. His 40 years’ experience in the produce business span a range of foodservice and retail positions. E-mail armandlobato@comcast.net.
 
What's your take? Leave a comment and tell us your opinion.