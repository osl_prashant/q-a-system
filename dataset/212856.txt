Organic produce suppliers say they are happy to work with retailers to further build an organic category that already has gained a strong foothold in stores.Consumers want the product, and retailers appear happy to oblige, said Bil Goldfield, director of corporate communications for Westlake Village, Calif.-based Dole Food Co.
“With the rapid growth in demand for organic, many of our retailers are offering a wider range of Dole organic fruits, vegetables, berries and salads,” he said.
“Within the produce section, Dole supports retailers with specific programs to help drive organic sales, including turnkey promotional programs, as well as the customized components to ensure success, including: expanded recipes and usage ideas, point-of-sale materials, in-store posters, take-one educational materials, and PR, digital and social channel support.”
Expanded retailer accommodations come as no surprise to Jacob Shafer, spokesman with Salinas, Calif.-based Mann Packing Co.
“Shelf space and variety of products are tied directly to consumer demands. If consumers continue to buy organic products, then more and more organics will become available at retail.”
There are no signs of an end to the growth period organic is enjoying in retail stores of all sizes and formats, said Steve Lutz, senior strategist with fruit grower-shipper CMI Orchards in Wenatchee, Wash.
“Right now, we are in a season of growth with no signs of slowing down, so this is the perfect time for retailers to have plans in place to add or reposition organics and capture strong profits,” Lutz said.
It’s a good time for retailers to get aggressive in promoting organic offerings, Lutz said.
“As organic apple and pear production trends upwards, retailers should continue to aggressively merchandise and promote. Two-pound pouch bags offer the perfect foundation for retailers considering introducing an organic selection to trial an organic offering,” he said.
“Consumers love and trust the Daisy Girl Organics brand, and CMI has a loyal fan-base of consumers who know to look for the distinctive yellow dress and blue packaging,” he said.
Organics provide better returns when they’re showcased properly at retail, said Gary Wishnatzki, owner of Plant City, Fla.-based Wish Farms.
“Grocers that give organics more shelf space and merchandise them alongside conventionally grown are moving more organics than those that have them segregated in a corner of the produce department,” he said.
Those retailers are moving “three or four times more” product, Wishnatzki said.
“It’s a pretty dramatic increase in the amount of organic product sold,” he said.
It also gives shoppers a “visual choice” when they see organics side-by-side with conventional, Wishnatzki said.
“There is a direct comparison between price and what they look like, and the consumers decide on the spot and many times they choose on the organics. Retailers do get a higher ring on the organic product,” he said.
Roger Pepperl, marketing director with Wenatchee, Wash.-based conventional and organic fruit grower-shipper Stemilt Growers LLC, said he could visualize a time when organics could own 30% of the produce marketplace — apples, even higher.
“Pure growth will push some items out, and they’ll replace some conventional items,” he said.
Already there are some instances in which a retailer carrying salad mixes will offer only organic in that product, Pepperl said.
“They may carry only organics in a variety of items because the price spread is so narrow and they don’t see the point of carrying two kinds,” he said.
Whether growers can maintain parity in pricing, where it exists now, is questionable, though, said Darrell Beyer, salesman with Oxnard, Calif.-based Boskovich Farms.
“Retailers are carrying more and more because I think they’re driving down the cost, but I don’t think organic growers can stomach the costs for very long,” he said.
“Some prices don’t make sense for growers — they don’t come out in the black.”