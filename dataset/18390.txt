No confirmation yet that Brazil's gov’t has accepted grower request
 






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.










Brazil’s soybean growers association, APROSOJA, has reportedly requested that their government consider filing a WTO case against the U.S. soybean crop insurance program. No confirmation is available as to whether or not the Brazilian government has given the grower group any positive response. 
A recent International Food Policy Research Institute (IFPRI) paper, “Agricultural Insurance and the World Trade Organization,” by former USDA Chief Economist Joseph Glauber, now senior research fellow in the Markets, Trade and Institutions Division of IFPRI, raises concerns about the vulnerability of government-subsidized crop insurance programs to WTO challenges.
According to Glauber, “A landmark achievement of the 1986 Uruguay Round, and specifically of the AoA (Agreement on Agriculture), was the full inclusion of agriculture in a system of multilateral rules and disciplines, including disciplines governing agricultural support.”
The WTO boxes. With the launch of the Uruguay Round, “green box” policies were held to be minimally trade distorting. These policies include nationally sponsored agricultural research programs, extension services, and investments in infrastructure.
Direct payments for program crops, like the U.S. had from 1996 to 2013, were also thought by US stakeholders to fall into the green box, though that was never challenged. They were eliminated in the 2014 Farm Bill as politically unsustainable in period where farmers were receiving record income from the market.
Most developed countries, including the US, have reported crop insurance subsidies as “amber box” – trade distorting and subject to reduction over time.
According to Glauber, “In both the U.S. Upland Cotton case and the four antisubsidy cases in which crop insurance was investigated, the authorities found that US programs conferred a specific subsidy. In no case have US arguments that its crop insurance program does not confer benefits to a specific crop prevailed.”
Bottom line: The possible Brazilian case could test whether or not the subsidized U.S. crop insurance program is immune from WTO challenges.


Comments: I asked a trade policy expert to comment and the source said the following:
“The U.S. is nowhere close to exceeding its allowable limits, so Brazil would have to argue serious prejudice. According to the Congressional Research Service (CRS): In the cotton case, in evaluating that particular claim [serious prejudice], the Dispute Settlement Panel separated U.S. cotton support programs into two groups: those that are directly contingent on market price levels (i.e., loan deficiency payments, marketing loss assistance payments, counter-cyclical payments, and Step 2 payments), and those that are not (i.e., PFC and Direct Payments, and the federal crop insurance program). U.S. domestic support measures that are not contingent on market price levels were not included in this finding of serious prejudice as the panel could not find enough of a connection between the direct payments program and cotton planting decisions to declare the direct payments program a serious factor in price suppression. The bottom line is Brazil has already challenged the U.S. on both direct payments and crop insurance and the Dispute Settlement Panel did not find serious prejudice. Based on this, it would be a reversal by the WTO to rule differently on crop insurance with respect to soybeans. Seems a bit of a stretch.” 
But a farm policy analyst said, “The broader issue is this represents yet another reason the U.S. crop insurance industry is going to have to defend itself ahead.”






 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.




 
"