Milk prices are set to rise in Europe as the continent faces a shortage of milk, cream and butter by Christmas, the head of one of the world’s biggest dairy companies said on Friday.Global milk prices, which are up about 28 percent from 12 months ago after producers cut output, have stabilized in recent months but Peder Tuborgh, CEO of Danish-based dairy co-operative Arla Foods, said world milk stocks were very low.
“There has been a scarcity of milk in the whole world after the very low prices last year,” he told Reuters.
“There is a big lack of fat, cream and butter products everywhere in Europe. It will not at all be possible to meet demand up to Christmas. It is those forces that are dragging up the prices significantly,” Tuborgh said.
Milk prices rallied earlier this year after European producers cut output last year following the scrapping of European Union milk quotas in 2015, which had led to a sharp fall in prices.
Arla, a co-operative owned by 12,500 farmers in Denmark, Sweden, Germany, the UK, Luxembourg, the Netherlands and Belgium, said on Friday it would increase the price it pays for milk from its farmer owners for a third consecutive month in September, by one euro cent per kilo to 38.3 euro cents per kilo, and could raise it again before the end of the year.
“After that we might increase it one more time this year, but that is a bit uncertain,” he said.
The coming spring season in New Zealand will be key to further price moves in the global milk market, analysts have said.
In the first half of this year, Arla increased the price it paid its farmer-owners by 19 percent.
“I think we’ve already had the main part of the price increases we’ll see this year,” Tuborgh said.
He predicted that milk production would catch up next year and grow by 2-3 percent.
Strong Growth in Asia and Africa
Arla, the world’s seventh-largest dairy company in terms of turnover, continues to expand outside of its main North European markets.
Sales from outside Europe grew by 10 percent in the first half of this year to 792 million euros, or 16 percent of total revenue, it said.
Sales to Asia jumped 36 percent from a year earlier while sales to sub-Saharan Africa rose 32 percent.
"I would have been satisfied with growth rates of around 20 percent, and I think that that is probably a more viable level long term," Tuborgh said.