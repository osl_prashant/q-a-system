10 wolves killed in northern Idaho to boost elk numbers
10 wolves killed in northern Idaho to boost elk numbers

By KEITH RIDLERAssociated Press
The Associated Press

BOISE, Idaho




BOISE, Idaho (AP) — Federal officials have killed 10 wolves in northern Idaho at the request of the Idaho Department of Fish and Game to boost elk numbers.
The U.S. Department of Agriculture's Wildlife Services said Wednesday that workers used a helicopter in the Clearwater National Forest in late February and early March to kill the wolves.
Idaho officials say the area's elk population has plummeted in the last 25 years from about 16,000 to about 2,000, and that wolves are partly to blame.
The state agency says hunters and trappers have killed 22 wolves during the current seasons in the remote and rugged region.
Officials say Fish and Game license funding paid for the federal agency to kill the wolves.
State and federal officials didn't have the cost immediately available.