U.S. live cattle futures fell sharply on Thursday as a rally in the corn market sent feeder cattle tumbling and as weak cash cattle prices and rising supplies weighed on sentiment, traders said.
The actively traded Chicago Mercantile Exchange June contract dropped by the 3.000-cent-per-pound daily limit to settle at 102.575 cents, the lowest point for the contract in nearly a year. Nearby April futures ended down 2.250 cents at 113.750 cents.
Futures tumbled despite a sharp jump in corn prices, which would typically be supportive to live cattle because higher feed costs may prompt feedlots to market cattle sooner and at lighter weights. Corn prices jumped more than 3 percent after the U.S. Department of Agriculture projected fewer-than-expected corn acres this spring.
“The market is still dealing with the crippling fact that we are waiting for a spring bulge in supply. It’s going to be ugly and the only question is how ugly it will be,” said Rich Nelson, chief strategist with Allendale Inc.
Cash cattle at U.S. Plains feedlot markets traded earlier this week at around $120 to $121 per cwt, at least $5 per cwt below a week ago, traders said.
Feeder cattle futures also fell sharply, pressured by the surging corn prices.
CME March feeder cattle ended down 0.375 cent at 135.600 cents per pound while actively traded April feeders were down the daily 4.500-cent limit at 133.325 cents.
After the limit-down settlements in both cattle markets, the daily limit in live cattle will expand on Monday to 4.500 cents while the feeder cattle limit will grow to 6.750 cents.
Hogs Mixed
Lean hog futures ended mixed on Thursday ahead of the USDA’s quarterly hogs and pigs report that was released after the close.
Front-month CME April hogs gained 0.325 cent to 57.250 cents per pound while most-active June hogs were down 0.075 cent at 76.550 cents.
The USDA reported the number of hogs on U.S. farms in the December-February quarter was up 3.1 percent from the same period a year ago, a record high for the quarter but largely on par with trade expectations.
Lean hog futures on Monday may be influenced more by near-term market fundamentals than the hog report, said analysts.
CME futures markets will be closed on Friday for the Good Friday holiday.