Minnesota farmers endure 5th straight year of thin profits
Minnesota farmers endure 5th straight year of thin profits

By STEVE KARNOWSKIAssociated Press
The Associated Press

MINNEAPOLIS




MINNEAPOLIS (AP) — Bumper crops and slightly better prices for some commodities weren't enough to save most Minnesota farmers from a fifth straight year of thin profits in 2017, according to a report released Tuesday.
The annual analysis from University of Minnesota Extension and the Minnesota State colleges and universities system found that nearly one-third of Minnesota farmers saw their net worth decrease in 2017. The state's median farm income was about $28,500, down from about $36,000 in 2016.
Dairy farmers faced some of the toughest challenges, Extension economist Dale Nordquist said. Milk prices tanked in the second half of the year, he said and a lot of farmers are now losing $2 on every hundred pounds of milk they produce. Pork producers were the one group that enjoyed higher profits thanks to somewhat better prices after losses the year before, he said. High-to-record yields have helped crop farmers "tread water" and withstand low prices, he said.
"It certainly wasn't the year we needed to turn things around," Nordquist said. "It was a tough year for crop producers and a little better for livestock producers. ... It wasn't really enough to make up the difference."
The report comes at a gloomy time for agriculture across the country. The U.S. Department of Agriculture forecasts that net farm income nationwide will fall 6.7 percent this year, to $59.5 billion, which would be the lowest level since 2006 and far below the record set in 2013 of over $120 billion. Low commodity prices have been the main reason. And the USDA predicts net farm income is likely to remain flat over the next 10 years, falling in real terms after inflation.
Minnesota's dairy farmers enjoyed relatively profitable prices in the first half of 2017 before the bottom fell out of the market and left them squeezed between low milk prices and higher feed costs, Nordquist said. Some are deciding to call it quits because of the financial stresses, he said. Many producers are older with fewer cows, he said. It's tough for farmers in that situation to decide whether to invest more money in aging facilities or do something else, he said.
The findings rang true for Tom Sedgeman, who has about 380 cows on his farm near Sauk Centre. He said he's more concerned about the impact of the downturn on younger farmers than himself.
"In my case, I'm 62. We're able to withstand a few more things than the younger generation," he said.
Sedgeman is particularly concerned for his son-in-law, a fellow farmer who wonders whether there will be opportunities for his young children to farm.
"In today's world it's extremely difficult to start from scratch, the capital investment is just so huge," he said.
While Minnesota corn and soybean farmers harvested a "tremendous crop," Nordquist said, so did farmers across the Corn Belt, keeping prices low. Fortunately, he said, there's been a "little bit of a run-up" in world prices for soybeans and corn in the last few weeks, mostly because of the drought in Argentina, so some producers have taken advantage to get better prices for their 2017 crops and lock in those prices for 2018.
Although Minnesota's crop farmers have struggled with low profits for the past five years, Nordquist said it's not a crisis for most. Balance sheets for most remain "pretty strong" because their strong yields have given them more bushels to sell, he said.
The study looked at data from over 2,100 participants in Minnesota State's farm business management programs and over 100 members of the Southwest Minnesota Farm Business Management Association. The participants represent about 10 percent of the state's commercial farmers.