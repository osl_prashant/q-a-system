California Citrus Research Board researcher Michelle Cilia has been named a recipient of the Presidential Early Career Awards for Scientists and Engineers.
 
The recognition is the highest honor bestowed by U.S. government on science and engineering professionals in the early stages of their independent research careers, according to a news release.
 
"We are very proud of Michelle and her achievements," Richard Bennett, California Citrus Board chairman, said in the release. "California citrus growers are truly getting the "best of the best" scientific researchers, who are working on cutting edge technologies to ensure a sustainable future for the California citrus industry."
 
Cilia's CRB-funded research focuses on huanglongbing (HLB/citrus greening disease), a critical threat to the U.S. citrus industry, according to the release.
 
"Each year the CRB funds in excess of $7 million towards citrus research; we are pleased Dr. Cilia has been chosen for this award because of some of her work for us," Citrus Research Board president Gary Schulz said in the release.
 
HLB is associated with a bacterium spread by the Asian citrus psyllid. Affected citrus trees produce bitter, green fruit and eventually die from infection related causes, according to the release. Research in the Cilia lab focuses on the protein level interactions responsible for the acquisition and transmission of the bacterium.
 
"One of the most exciting and fulfilling parts of my scientific career has been working with the California citrus growers and the California Citrus Research Board," Cilia said in the release. "I hope that my award will draw the public's attention to this serious disease, which is threatening citrus production in this country and around the world."