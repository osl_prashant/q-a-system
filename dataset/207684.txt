Table grape growers in California’s San Joaquin Valley were hoping for cooler weather in early August after enduring an extensive heat wave with temperatures topping 100 degrees.
“The crop started out looking very good, but the heat we’re having is taking a toll on different varieties, depending on where they were in their maturity cycle,” Keith Andrew, sales manager for Columbine Vineyards in Delano said the first week of August.
Volume was down, but not by much, he said.
Hot weather had its biggest effect on red and black grapes.
“The heat has slowed us down on the colored grapes for sure,” said Louie Galvan, managing partner at Fruit Royale Inc. in Delano. “They need cool nights to get color on the grapes.”
President Justin Bedwell experienced the same thing at Bari Produce LLC in Madera.
“We’ve had a little bit of a heat spell going out here, and it has kind of slowed down everything,” he said. “Red grapes are getting color at a slower pace than we would like.”
Although the size of the crop and flavor of the fruit were “still right there,” he said berry size on some of the early-season fruit seemed smaller than past years.
Visalia Produce Sales Inc. in Kingsburg was cutting back hours by stopping the harvest at noon instead of 1 or 2 p.m., said George Matoian, sales and marketing director.
“We’re not harvesting as much as we had hoped to per day,” he said.
He did not anticipate shortages of table grapes, however.
“There’s still plenty of volume to promote,” Matoian said.
The heat was accelerating the maturity of some grapes and holding back others, he said.
Growers still reported excellent quality on the grapes they harvested.
The latest crop estimate from the Fresno-based California Table Grape Commission issued July 27 forecast 111.4 million 19-pound box equivalents.
Last year, the state’s growers produced nearly 109 million boxes.
“The harvest is well underway, with more than 60% of the volume shipping after Sept. 1, making California grapes a major part of the fall fruit scene,” said Jeff Cardinale, vice president of communications.
“Numbers are right on par with what we estimated,” Galvan said.
But he added that by early August, Fruit Royale had lost a week or so of picking because of the heat.
“We just can’t go out there and pack full days at these temperatures,” he said.
High temperatures are to be expected in the San Joaquin Valley during the summer, he said, but there typically is some relief between hot spells.
That wasn’t the case this year.
“It seems like (hot weather) came in May and hasn’t left,” he said. “It’s just that we’re not used to this long a stretch.”
But the weather had not had a negative effect on quality.
“The fruit’s been fantastic,” he said. “It just takes a little longer to mature.”
He described some of the varieties in early August as “huge,” perhaps because they had to hang on the vine longer.
“We’ve got some varieties that are just massive,” he said.
There should be plenty of grapes to promote during the fall, Matoian said.
In late August, the company should be shipping Ivory, princess, thompson and Sweet Globe green seedless varieties, he said.
Autumn kings should start the first or second week of October.
In the red seedless category are flames, scarlet royal, Timco, Magenta and Krissy with crimsons just getting underway in late August.
Summer royal and autumn royal black grapes also should be available, and the company planned to start shipping the Allison red seedless grapes in September.