It’s difficult for any business to remain profitable and successful for nearly 140 years, but it’s especially challenging for a family enterprise. In fact, less than one-third of family-owned businesses survive beyond the founder’s generation, and just 12% make it to a third generation of ownership, according to the Family Business Institute.
But brothers, Kent and Barry Holden, owners of Holden Farms in Northfield, Minn., have strong, underlying beliefs and a respect for people, land and animals that create the foundation for one of the largest pork operations in the country. 
These underpinnings are paving the way for the fifth generation to take leadership roles. Kent’s three grown children, Nick, Nate and Tyler; and two of Barry’s five grown children, Blake and Kyle; are poised to manage the business.  (In the photo above, the Holden Farms management team: (top row, L-R): Barry, Nick, Tyler and Kent; 
(bottom row, L-R) Blake, Nate and Kyle.)

Sixty percent of the firm’s sow farms are company owned, and 40% are contract sow farms, for a total of 55,000 sows. Kent and Barry are in the process of expanding by an additional 7,000 sows, to be completed in January. Holden Farms also operates three turkey farms, is part owner in a turkey processing plant and works with nearly 250 contract partners in the region. They sell pigs to Tyson, JBS and Triumph Foods and are among the top 20 largest pork producers in the nation.
Planning and Preparation
Howard Holden, Barry and Kent’s father, was far ahead of his peers in terms of succession planning, and unlike many of his generation, he wanted his sons to know his intentions. Barry even accompanied him to the meetings. Howard put all of his assets into the corporation formed in 1971, including his house and the three farms he owned at the time. 
In 1973, Howard passed away on Thanksgiving Day, at the age of 58. Kent was just 25 and Barry was 24. There were 500 acres but no cropland—it was all livestock with the bulk of it turkey production. They had a feed mill and had begun building hog barns. At that time, they had 100 sows, sold 150,000 turkeys per year and owned a breeder flock.
The corporation was set up for each of the four sons, John, Kent, Barry, and Craig, to have equal shares of 10% and their mom to have 60% of the business.
Holden Farms grew throughout the 1980s as the four brothers pioneered some of the region’s first contract partner arrangements for hog production.
“We owned the animals and the feed, and we’d find farmers to build barns for us,” Kent says. “We decided we would own the sow farms if we could and we didn’t want to own very many finishing or nursery buildings. We own the research barns and a couple of nurseries (that we bought back), but it is not our goal to own all the facilities. It’s a model that has worked well for us.”
In 1991, John and Craig sold their portions of the company to Kent and Barry, who assumed sole ownership responsibilities of the hog farming operation plus a portion of the turkey production venture.
Barry says his and Kent’s experience of working with their own brothers gave them a different perspective than their children have. The split was difficult, but because there were different business philosophies, they knew it was the right thing to do. 
“We knew we had to be separate but equal, and we knew the importance of having different roles. We were going to maintain the turkey business and grow the hog business,” he says.
Kent took the production side, while Barry took care of the turkeys and the ancillary part of the legal contracts, including banking. They met regularly and decided to grow the business. 
“If we were going to do something, we basically always said yes (you don’t get very far when you say no to things),” Barry says. “Some of those things didn’t work out but at least we knew.”
There are disagreements, as there are in any business—family or otherwise. Communication and mutual respect kept the family moving forward.
 
This article the first of a four-article series, and appeared as the cover story of the November/December issue of Farm Journal's PORK.

Read more:
All in the Family—Holden Farms Has Room to Grow
Communication Keeps Holden Farms A Family
Holden Makes Family the First Priority
Holden Farms: Secure Transfer to the Fifth Generation