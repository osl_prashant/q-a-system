Crop calls
Corn: Fractionally to 1 cent lower
Soybeans: Fractionally to 1 cent firmer
Wheat: 3 to 9 cents lower
Wheat futures faced profit-taking overnight to correct the overbought condition in most contracts. Traders also noted better-than-expected results from the first day of the Wheat Quality Council's HRW wheat tour across Kansas. Scouts found an average yield of 43.0 bu. per acre in northern Kansas, versus last year's 47.1 bu. per acre and the five-year average of 42.7 bu. per acre. HRW wheat faced the brunt of the pressure overnight, which spilled into the corn market. Corn traders also point to the drier NWS outlook for May 8 through 12 as a source of pressure, as it signals planting conditions will improve across the Midwest.
 
Livestock calls
Cattle: Higher
Hogs: Higher
Livestock futures are expected to benefit from followthrough buying as well as strength in the product markets. Choice beef values rose $3.18 yesterday and pork cutout values gained 67 cents. While traders doubt last week's sharp improvement in the cash cattle market will be repeated this week, nearby live cattle are still playing catch up as they hold a sizable discount to last week's cash trade. Meanwhile, traders are encouraged by a more balanced supply and demand situation in the hog market, which has improved the near-term cash market outlook. Also encouraging is the fact average hog weights in the Iowa-southern Minnesota market for the week ended April 29 dropped by 0.4 lbs. from the previous week.