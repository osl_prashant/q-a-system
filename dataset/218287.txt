As a newly minted crop protection rep, I landed a rookie sales territory that was remote to say the least. One day, I pulled into a mom-and-pop shop in your typical wide-spot-in-the-road kind of town. I was ushered into a cluttered office as dusty as the mill floor.
As I assembled my brochures, I noticed an overflowing bookcase. Contemporary and classic literature lined the shelves. As I was going for my order forms, a young employee named Billy knocked on the door.
“Hey, boss, thanks for loaning me this one,” Billy said enthusiastically. To my astonishment, he laid down a weathered copy of “For Whom the Bell Tolls”.
“Hemingway?” I thought.
You could have knocked me over with a feather.
“Yeah, this story was cool, but I still like ol’ F. Scott better,” Billy acknowledged.
Trying not to look stunned at the thought of spray-rig operators reading classics, I waited for an explanation. The owner said when his dad founded the business, there wasn’t much to do in the community, so people gathered at his store. His mother began bringing in books, and people shared them. As a boy, he read with the guys in the afternoons, And when he took over, the habit stuck.
Read More. Perhaps without realizing it, that business owner created an environment that encouraged learning as well as engagement.
Do you demonstrate the value of learning and growing? Any employer knows employees who seem checked out are likely to leave or become less productive. In our business, they could even be unsafe.
It typically costs the equivalent of six to nine months’ wages to find and train a new person, according to a 2016 report by the Society for Human Resource Management. Even for a $12-an-hour hire, that could amount to $20,000 or more.
But we know agriculture competes with other industries on both hourly and salaried employees that find it more tempting to work easier jobs with no seasonal long hours. Keeping productive employees is essential but so is improving average workers.
Other Benefits. You might not be into the classics. Even so, making career-long learning a priority has a host of benefits including heightened safety; improved understanding of the operation; greater crop or livestock knowledge; and technical skill-building.
As a student and leader, you demonstrate the value of learning. But learning must be relevant to the receiver. Meet employees where they are, and provide learning opportunities where you can. Some ideas include:
• CCA training for sprayer operators.
• Grain-marketing classes for your CFO.
• Invite a speaker or sales rep to come speak to everyone on your team—not just to the leaders.
• Attend a one-day conference as a group and discuss it on the way home.
A learning culture is not for fluff, especially when you are paying for it with time off or registration fees.
“Don’t make the mistake of investing money in people, resources and time without a clear understanding of how these investments contribute to your bottom line,” writes Jeff Miller, a recruiter and trainer, at forbes.com. Ways to be effective might include tying training to incentive packages or performance reviews.
Build Your Brand. A learning culture also pays dividends for your operation in the community by encouraging employees to be excellent stewards of your brand when they’re off work. A learning culture is an advocacy and public relations strategy, too. Maybe you’ll inspire someone. Instead of flipping through social media, your truck drivers might start reading Faulkner in the elevator line.
Editor’s Note: Sarah Beth Aubrey’s mission is to enhance success and profitability in agriculture by building capacity in people. She strives to foster that potential through one-on-one executive coaching, facilitating peer groups and leading boards through change-based planning initiatives. Visit her at SarahBethAubrey.com, and read her blog, The Farm CEO Coach, at AgWeb.com.