Court blocks company's attempt to restock Atlantic salmon
Court blocks company's attempt to restock Atlantic salmon

The Associated Press

SEATTLE




SEATTLE (AP) — An Atlantic salmon farming company cannot restock a net-pen farm while it challenges Washington state's termination of its license to operate, a judge has ruled.
The Seattle Times reports that a Thurston County Superior Court Judge on Friday denied Cooke Aquaculture's request for a preliminary injunction while its lawsuit against the Department of Natural Resources plays out.
One of the company's pens collapsed last August at its Cypress Island farm, releasing more than 300,000 Atlantic salmon and causing widespread concern that the fish could spread diseases and compete with native fish.
In December, Public Lands Commissioner Hilary Franz ended the company's 10-year aquatic lands lease, saying the Canadian company had violated the terms of the lease. The company filed a lawsuit in January contending it was not in default of its net-pen lease and there was no basis for terminating its contract.
Judge John Skinder's ruling means the state won't have to allow the company to restock the net-pens.
The company removed the collapsed pen but has two remaining pens at its Cypress Island farm. The company wants to restock with nearly 800,000 juvenile Atlantic salmon.
Joel Richardson, vice president for public relations for Cooke, declined to comment about the court ruling.
"Today's decision is a win for the people and waters of Washington," Franz said in a statement. "I encourage Cooke to drop this baseless lawsuit and work with us to safely and quickly wind up its operations at this site."
The company has also sued over termination of its net-pen leases at its Port Angeles farm. The state is allowing the company to grow its existing stock of fish at that site and harvest them by June 2019, then remove its operation there.
Washington lawmakers last session passed legislation signed by Gov. Jay Inslee that phases out Atlantic salmon net-pen farming, with no new leases allowed after the current leases expire in 2025.
___
Information from: The Seattle Times, http://www.seattletimes.com