Sitting on a couch in his home in the Karen suburb of Nairobi, Samuel Odaga stares at his phone, his fingers occasionally flicking the screen.But he's not checking the weather or the news, or looking at his email.
"I am looking for new cows posted online," he explains.
Finding cattle to buy - or finding buyers for animals on sale - used to be a difficult business, said the livestock trader.
With large swathes of the country hit by recurring periods of drought, Kenyan herders and farmers are increasingly eager to sell animals when conditions turn worse rather than lose them to insufficient water and pasture.
But finding buyers can be a problem, they say, and they often lack good information on what would be a fair price for the animals.
Victor Otieno, an animal science graduate of Kenya's Egerton University, said he "once met a farmer who thought she had made an excellent deal selling a cow for 70,000 Kenyan shillings (about $675)."
"Only we met a neighbor later that day who had just bought the very same cow from the middleman… for 130,000 shillings ($1,250)!" he added.
That information gap prompted Otieno in 2015 to set up Cowsoko, an online marketplace for dairy farmers that derives its name from the Swahili word "soko", or market.
Sellers can upload a picture of their animal, information on its weight, breed and cost, as well as the seller's details so prospective buyers can contact them directly.
They pay an annual fee of 5,000 shillings ($48) to the site, in addition to a fee of 2.5 percent of the cow's selling price.
More Options
Philip Okech, a consultant at SNV, a Dutch development agency, said farmers often need to travel long distances – up to 100km – to buy or sell animals.
Cowsoko encourages sellers to hire a van to deliver sold animals to their buyer within a day of purchase, Otieno said.
Payment on the platform is enabled by M-Pesa, a popular mobile money service in Kenya.
Odaga, who so far has bought six cows using the service, said he frequently used to get "tricked by middlemen who would sell me cows at a much higher price than what they had originally bought them for".
"And I had to spend even more money to feed the cows or buy drugs because they weren't in good health," he added.
In addition to problems locating cows for sale or buyers, Otieno said, farmers face another major challenge: finding reliable information on fodder and drugs for the animals.
"I noticed that even once farmers had bought an animal, they knew very little about diseases that might affect them, what feed they should give or what fodder might be available in the area," he said.
With support from experts including SNV, Otieno now has made information on cattle fodder and drugs, as well as potential suppliers, available free on the cow trading platform.
"I want farmers to be able to find all the information they need in one place," he said.
Tackling Fraud
Not everyone shares Odaga's enthusiasm, however. David Mugambi, a lecturer at Chuka University in Kenya's central Tharaka-Nithi county, warned that fraud is rife on such online trading platforms.
"Buyers on these platforms need to be careful as they rarely get to meet the person they're dealing with – the seller – nor do they see the animal physically before buying it, which weakens their negotiating position," he said.
"They also have no control over the delivery time," said Mugambi. "If the seller delivers the animal later than agreed then there's not much you can do about it."
To curb fraud, Otieno said the platform now only accepts sellers who are registered breeders, "so we can authenticate them".
He also introduced the annual membership fee for sellers to dissuade fraudsters, and to be able to trace any transaction back to the seller in case of a dispute.
Otieno said he would consider offering a full refund on an animal if needed, although none has been requested to date.
He hopes to expand Cowsoko, which currently has about 10,000 users across the country, to neighboring Uganda, Tanzania, Somalia and Ethiopia – and to sell other animals like pigs and poultry.
"We have more potential buyers than the platform can accommodate, so we need to grow it and bring more cow sellers on board," he said.