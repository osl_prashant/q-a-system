Delaware levies $250K fine for poultry plant water pollution
Delaware levies $250K fine for poultry plant water pollution

The Associated Press

HARBESON, Del.




HARBESON, Del. (AP) — Delaware has slapped a poultry producer with nearly $250,000 in penalties and other costs for years of wastewater violations at a chicken processing plant.
The News Journal reports the state Department of Natural Resources and Environmental Control released a secretary's order Wednesday assessing Allen Harim Foods a $241,000 fine for more than 90 violations at its Harbeson plant. The company must also pay the department nearly $8,000 to cover its investigations.
State environmental regulators said in 2016 that millions of gallons of polluted waste were dumped into the Delaware Bay tributary of Beaverdam Creek between 2012 and 2016.
A DNREC employee said during a hearing on Wednesday that the company is appealing the fine to the state Environmental Appeals Board.
The company didn't respond to the newspaper's request for comment.
___
Information from: The News Journal of Wilmington, Del., http://www.delawareonline.com