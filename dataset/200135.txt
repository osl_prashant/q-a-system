This market update is a PORK Network weekly column reporting trends in weaner pig prices and swine facility availability. All information contained in this update is for the week ended April 29, 2016.Looking at hog sales in October 2016 using December 2016 futures, the weaner breakeven was $27.20, down $1.15 for the week. Feed costs were up $2.64 per head. December futures increased $0.75 compared to last week's December futures used for the crush. Breakeven prices are based on closing futures prices on April 29, 2016. The breakeven price is the estimated maximum you could pay for a weaner pig andbreakeven when selling the finished hog.

For the Week Ended


4/29/2016


Weekly Change


Weaner pig breakeven


$27.20


($1.15)


Margin over variable costs


$49.04


($1.15)


Pig purchase month


Apr, 2016





Live hog sale month


Oct, 2016





Lean hog futures


$64.45


$0.75


Lean hog basis/cwt


($1.36)


$0.00


Weighted average sbm futures


$333.19


$19.42


Weighted average sbm basis


($7.63)


$0.00


Weighted average corn futures


$3.93


$0.16


Weighted average corn basis


($0.24)


$0.00


Nursery cost/space/yr


$35.00


$0.00


Finisher cost/space/yr


$40.00


$0.00


Feed costs per head


$76.34


$2.64


Assumed carcass weight


205


0

Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations don't take into account market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash traded weaner pig volume was below average this week with 29,556 head being reported which is 95 percent of the 52-week average. Cash prices were $38.85, down $1.77 from a week ago. The low to high range was $31.00 to $43.00. Formula priced weaners were down $1.64 this week at $37.67.
Cash traded feeder pig reported volume was below average with 8,945 head reported. Cash feeder pig reported prices were $71.96, down $2.19 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through April 29, 2016.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential increased $0.61 this week with a ($11.65) per head loss.

Casey Westphalen is general manager of NutriQuest Business Solutions, a division of NutriQuest. NutriQuest Business Solutions is a team of leading business and financial experts that bring years of unparalleled experience in the livestock, grain, poultry and financial industries. At NutriQuest our success comes from helping producers realize improved profitability and sustainability through innovation driven by a relentless focus on helping producers succeed. For more information, please visit our website at www.nutriquest.com or email casey@nutriquest.com.