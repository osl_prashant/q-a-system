The following content is commentary and opinions expressed are solely those of the author.
The oversupply of agricultural products has been making the headlines.

This week on Customer Support, U.S. Farm Report commentator John Phipps answers a question from viewer Thomas Marsh on what can happen to raise prices and drop the surplus.

Watch his response on U.S. Farm Report above.