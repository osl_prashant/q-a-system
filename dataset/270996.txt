A long winter, above-average snow pack and a cool spring have pushed back crop maturity in Oregon’s Hood River pear region. 
“In 2016, we started picking and packing pears the last week of July,” Duckwall Fruit Co. sales manager Ed Weathers said in a news release. “This year, with bloom being seven to 10 days later than normal, and three weeks behind last year, we won’t start our harvest on bartletts and starkrimson until later August.”
 
Full bloom on the firm’s Hood River pears took place the last week of April, making the crop about three weeks behind the 2016 and about a week to ten days later than normal, according to the release.
 
Opening the harvest season, which runs through October, bartletts and starkrimson are projected to begin by late August, according to the release. Green anjou, red anjou, bosc, comice, forelle and seckel are usually about three weeks behind bartletts and should be in the market by mid-September.
 
Duckwall Fruit is expanding its pouch bag product offerings after a successful launch in 2016. The company, an exclusive packer of pears, will offer variety-specific pouch bags this season, according to the release.