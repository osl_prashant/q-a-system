<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->



At the Florida tomato industry's annual meeting, growers and packers elected new leaders to direct their industry organizations.
During their Aug. 16 organizational meetings, members of the Maitland-based Florida Tomato Committee, Florida Tomato Exchange and Florida Tomato Growers Exchange elected and reelected members to their individual boards.
The officers were formally installed into their respective organizations during the Sept. 6-9 Joint Tomato Conference.
New leaders of the organizations are:
Florida Tomato Committee:
Chairman - Frank Diehl, owner of Tomatoes of Ruskin Inc., Ruskin;
Vice chairman - Patrick Engle, general manager of Big Red Tomato Packers, Fort Pierce.
Florida Tomato Exchange:
President - David Neill, president of Neill's Farm and managing partner of Big Red Tomato Packers; 
Vice president - Toby Purse, chief financial officer and chief accounting officer of Lipman Family Farms, Immokalee.
Florida Tomato Growers Exchange:
President - Tony DiMare, vice president of the DiMare Co., Homestead;
Vice president - John Harllee IV, president of Harllee Packing Inc., Palmetto.
Kern Carpenter, owner of Kern Carpenter Farms Inc., Homestead, and Mike Sullivan, chief financial officer for Gargiulo Inc., Naples, were reelected as secretary and treasurer, respectively, of all organizations.
Dave Wilson, manager of Palmetto, Fla.-based Classie Growers, was elected to the Florida Tomato Exchange's board.
All other committee and exchange members outside of the officers were re-elected.
Members serve two year terms.
This was the 41st yearly meeting of the Joint Tomato Conference and the 30th year the organizations have met at the Ritz-Carlton Hotel.
The tomato industry is one of the longest-running organizations to meet at the resort.