(UPDATED Jan. 11) Belgium-based Greenyard has announced an end to negotiations to buy Dole Food Co.
Responding to speculation by some analysts, Greenyard had said Dec. 19 it was then involved with talks to buy Dole. Those talks ended without a deal, Greenyard said Jan. 5.
"While the acquisition of Dole by Greenyard would have marked a significant milestone for both companies, we are confident that Greenyard has the right strategy and priorities in place to continue generating profitable growth and strengthening our global leadership position in fruit and vegetables,” Hein Deprez, executive chairman of Greenyard, said in a news release. “We pursued all efforts to realize a transaction with financial and strategic merit that would have created value for all stakeholders involved, nevertheless an agreement could not be reached.”
Dole confirmed in an e-mail Jan. 5 that the talks were over.
“Every effort was made on both sides to reach a mutually beneficial agreement that made sense for both businesses moving forward, but at the end of the day we couldn’t come together,” Bil Goldfield, director of corporate communications for Dole, said in a statement.
The company remains “financially strong and stable,” according to Goldfield’s statement.
 
 
No Dole IPO
On Jan. 9, Dole filed documents with the Securities and Exchange Commission stating it has abondoned plans for an initial public offering.
The company “has determined not to pursue the contemplated public offering at this time,” according to the filing, which reversed an April 2017 notice to pursue the IPO plan to offer $100 million in public stock.
Dole controlling owner David Murdock took the company private in November 2013 after four years as a publicly traded company, but that transaction prompted lawsuits from investors that were settled last year.
For fiscal 2016, Dole reported revenue of about $4.51 billion, operating income of about $21.4 million, and a net loss of $23 million. Dole reported in the SEC filing that it has more than $1 billion in debt.
(Note on update: This story has been updated to reflect Dole's decision to rescind the company's IPO intentions.)