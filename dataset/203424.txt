The end of audit fatigue is in sight. 
 
Tomato growers at an annual conference heard the news in early September that a single audit in compliance with food safety regulations is close to reality. 
 
In an effort led by the Washington, D.C.-based United Fresh Produce Association, the produce industry has worked to develop a single, harmonized audit to fulfil buyers' food safety certification requirements for all fresh produce items. 
 
United Fresh's retired senior vice president of food safety David Gombas said at the conference that the Global Food Safety Initiative-benchmarked audit has been approved by GlobalG.A.P. and should be approved by other organizations.
 
Gombas also said growers and packers have access to training materials to help them develop their safety plans.
 
This is a big step in the produce industry's move toward a stronger food safety culture. 
 
Audit fatigue is a legitimate problem, but it was never a good excuse to consumers and their advocates, who questioned whether the produce industry was doing everything it could to ensure a safe food supply.
 
Soon, that concern will be off the table.
 
Audits and standards don't 100% guarantee safety of a perishable product, but they do set benchmarks that can't and shouldn't be ignored - by buyers or sellers.
 
The end of audit fatigue should now produce audit excitement and energy!
 
Did The Packer get it right? Leave a comment and tell us your opinion.