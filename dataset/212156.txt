Ontario asparagus growers are excited about two new varieties now in trials around the globe.Ken Wall, co-owner of Port Burwell, Ontario-based Sandy Shore Farms, said Eclipse and Equinox “look stellar.”
“In almost every single trial we have seen increased yields and improved spear quality,” Wall said.
“We are very excited about the future prospects for our industry in Ontario and throughout North America with the introduction of those two varieties.”
Like current favorite Millennium, Eclipse and Equinox are owned by Ontario growers through their company, Fox Seeds.
Bernie Solymar, executive director of Simcoe-based Asparagus Farmers of Ontario, said this year’s first commercial sales of Eclipse sold out immediately.
“Next year we expect to have double the volume,” said Solymar, who anticipates Equinox will be available commercially by 2020.
Along with traditional green, Solymar is a big fan of Purple Passion asparagus, and hopes to revive production across Ontario in the next few years with the addition of new European varieties.
The firm burgundy stalks, common in Europe, are sweeter than traditional green asparagus and maintain their deep color when roasted or shaved raw in salads. When boiled they turn dark green.
Solymar is planning a local research trial using four varieties imported from Europe. Initial results are expected in 2020.
“I strongly believe there is a market for purple,” he said. “It would make a good specialty crop and chefs love the splash of color it adds.”
Ontario organic asparagus is also increasing with the addition of a third certified grower.
Bill Nightingale, president of La Salette, Ontario-based Nightingale Farms, began harvesting his first 15-acre crop of organic asparagus at the end of April, but stopped at the end of May to give the new plants a rest.
“I think I’ll plant more next year,” said Nightingale, who also grows conventional asparagus.
“We could use way more as demand for organic is growing every year.”
So far, he said the organic crop isn’t more profitable than conventional considering the risks involved and the pressure from weeds and disease.
Wall said the trends toward healthy eating and buying local have increased demand for Ontario asparagus in the past five to 10 years. “When you realize that the vast majority of asparagus consumed in North America comes from Mexico and Peru, there’s a great deal of excitement in May and June when the Canadian and U.S. deal kicks in,” he said.