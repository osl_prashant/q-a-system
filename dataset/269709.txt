BC-US--Cotton, US
BC-US--Cotton, US

The Associated Press

New York




New York (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Tuesday:
(50,000 lbs.; cents per lb.)
COTTON NO.2Open    High    Low    Settle     Chg.May       83.28   83.58   82.06   83.13  Down  .12Jul       83.28   83.49   81.97   82.95  Down  .27Aug                               78.40  Down  .14Oct                               78.95  Down  .27Oct                               78.40  Down  .14Dec       78.50   78.68   77.92   78.40  Down  .14Dec                               78.46  Down  .21Mar       78.61   78.69   78.17   78.46  Down  .21May       78.38   78.54   78.35   78.51  Down  .10Jul       78.06   78.36   78.06   78.31  Up    .05Aug                               73.55  Up    .05Oct                               75.32  Up    .04Oct                               73.55  Up    .05Dec       73.50   73.55   73.50   73.55  Up    .05Dec                               73.84  Up    .28Mar                               73.84  Up    .28May                               74.26  Up    .28Jul                               74.34  Up    .28Aug                               72.68  Up    .28Oct                               73.91  Up    .28Oct                               72.68  Up    .28Dec                               72.68  Up    .28Dec                               72.72  Up    .28Mar                               72.72  Up    .28