Traders have been slow to respond to excessive moisture in the eastern and southern Corn Belt as national corn planting and emergence have stayed close to the average paces. Gains in corn futures and losses in the soybean market late last week, however, suggest traders are starting to factor in a switch of some intended corn acres to soybeans. USDA’s first corn crop condition ratings of the season may give the corn market a boost this week, while additional rains forecast for the Corn Belt threaten to push soybeans lower. Harvest pressure on winter wheat futures was offset last week by crop disease concerns. Spring wheat futures firmed amid expectations demand for high-protein supplies will increase due to the winter wheat quality concerns.
Pro Farmer Editor Brian Grete highlights this week's Pro Farmer newsletter below: 



Cattle futures were choppy as their big discount to the cash market limited pressure from weaker cash cattle prices. Summer-month hogs rallied to new highs in anticipation of more cash market strength, despite hefty market-ready supplies.
Click here for this week's newsletter.