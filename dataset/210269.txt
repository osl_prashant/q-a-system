Bovine respiratory disease (BRD) can have a tremendous negative impact on dairy herds. It continues to be the major cause of death in weaned calves and can lead to poor performance later in a cow’s life.
There are several factors that can contribute to a respiratory disease outbreak.
“When we investigate bovine respiratory disease, the role viruses play in secondary bacterial pneumonia has long been known,” said Dr. Brian Miller, senior professional services veterinarian with Boehringer Ingelheim. "If you add a third factor to the mix besides these viruses and bacteria, there’s usually some type of immune distress that has immunocompromised those animals and contributes to respiratory diseases,” he explained.
Factors that result in immunosuppression include ventilation insufficiencies, new herd additions, calving, transportation, sudden weather changes, overcrowding and more.
“There are always viruses and bacteria lurking in the cow’s environment,” said Dr. Miller. “When exposure overwhelms host defense mechanisms, outbreaks of respiratory disease can occur.”
Monitoring for BRD in your herd
Respiratory disease is often the most difficult infectious disease to diagnose and is frequently misdiagnosed or under diagnosed. It is often the “default diagnosis” by ruling out other infectious diseases that also can cause a fever.
Some clinical signs to monitor include:
·         Fever – early in the course of respiratory disease, a fever is a fairly consistent sign.
·         Nasal discharge – discharge will appear watery in the early stages, and later may become yellow, white or blood tinged.
·         Cough – a cough may or may not be present.
“To arrive at a diagnosis of pneumonia, you need to observe two of these three clinical signs,” Dr. Miller explains. “Another sign to help confirm the diagnosis is the character of the respiration. Check to see if it is more difficult for the cow to exhale. If you stand behind the cow, you can observe the extra abdominal effort needed to expel the air.”
It’s important to work with your herd veterinarian to properly diagnosis and treat animals with pneumonia. They can also help to determine causative or contributing factors so preventive measures can be put into place.
Prevention through management practices and vaccinations
It’s important to help your herd build and maintain a strong, competent immune system to reduce respiratory risk. This is accomplished by implementing preventive management practices such as:
·         A sound nutritional program.
·         Providing adequate bunk and bed space.
·         Minimizing overcrowding.
·         Implementing biosecurity measures for new herd additions.
·         Ensuring adequate ventilation and heat abatement for all seasons.
·         Maximizing cow comfort.
·         Minimizing pen moves.
In addition to these preventive practices, strategic vaccination can also help protect your herd against pneumonia caused by respiratory viruses and bacteria. Appropriate vaccine choices and vaccine placement protocols are best accomplished with the help and advice of your veterinarian.
“Not only does the herd veterinarian understand the respiratory risks within your operation, they also understand the risks in your given geographical area,” said Dr. Miller. Previous experiences, submissions to a diagnostic lab or results of post-mortem examinations, can be drawn upon to design and maintain a preventive respiratory vaccination protocol.