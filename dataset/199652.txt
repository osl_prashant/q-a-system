InterstateLivestock.com, developed by the United States Animal Health Association (USAHA) and the National Institute for Animal Agriculture (NIAA,) is a one-stop resource for finding requirements to move animals across state lines. The site is in its first phase featuring cattle movement regulations for all 50 states, and designed to provide quick results.Best of all, it's free! Anyone involved in cattle movements can easily prepare to meet the necessary requirements. No more reading through regulations, or waiting until normal business hours to call the state - the information is available 24/7 at your fingertips.
Some common applications include:
¬? Veterinarians
¬? Livestock Markets
¬? Seedstock sales
¬? Cattle buyers
¬? Exhibitions and Fairs
¬? Private treaty sales
The mobile and desktop friendly site provides you with a simple Q&A process to narrow down your requirements for your specific situation. Whether it's a health certificate, disease tests or other specifics, this resource provides up-to-date information on what regulations will apply. The site also provides an interactive, map based tagging site finder to help get your stock properly identified when entering commerce.
The site is developed as a partnership of the USAHA and the NIAA, through which the information gap was identified, and this solution was developed through a collaborative effort of states, USDA, members of both organizations and Trace First, the service provider. The site was officially launched late last fall, and continues to grow.
The groups are now developing the second phase of the site, which will include additional species: Equine, Swine, and Sheep and Goats, making it a truly robust resource to meet regulatory requirements for a broad range of individuals involved in transporting livestock.