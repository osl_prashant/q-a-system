In my conversations with vegetarians — and I’ve questioned literally thousands of them over the years — most initially adopt their restrictive diets for ethical reasons: animal abuse, factory farming and the (alleged) horrors of slaughterhouses.
Once people start down that nutritional highway, however, they simply assume that superior health follows without exception.
Give up meat. Live long and prosper.
Allegedly.
I’m often amazed at how blithely confirmed veggies spout off about how a green salad is so much more nutritious if one delicately picks out all the chicken, beef or shrimp and then eat only the lettuce and tomatoes.
Early on in elementary school, children are urged to eat fruits and vegetables, and that’s a good idea — as long as all that produce complements, rather than replaces, meat and dairy sources of protein.
Part of the reason that “health” is equated with veggie diets is that so much of American society is Puritanical at heart. If something tastes good, it must be bad. If it’s satisfying to eat, you should deny yourself the pleasure.
Like snack foods, desserts, cocktails and candy, we’ve been conditioned to consider anything that tastes good to be unhealthy, and that includes any food that contains any appreciable amounts of fat — especially saturated fat.
Of course, moderation is necessary in all things, but this is a report focused on health, not pleasure. And for the 95% of us who consume animal foods, its conclusions are quite pleasing, indeed.
Euro Data Don’t Lie
The study, which all media have quickly labeled “controversial,” suggests that reduced consumption of saturated fat and cholesterol, due to replacing animal foods with fruits and vegetables, may result in a higher risk of cancer, allergies and mental health problems.
The new study, which was conducted by the Medical University of Graz in Austria, contained the following summary:
“In simple terms, a good bit of saturated fat and cholesterol, commonly found in red meat, can actually reduce some ‘health risks’” [italics added.
The researchers used data obtained from the Austrian Health Interview Survey to analyze the dietary and lifestyle differences between meat-eaters and vegetarians.
Those survey data were collected under the auspices of the European Union Health Interview Survey conducted among EU countries to “collect statistical data on a harmonized basis and with a high degree of comparability between the EU Member States,” according to Eurostat.eu. The EU Health Interview Survey includes people 15 years and older living in private households, with the goal of capturing data to better monitor health policies, measure health inequalities and promote healthy aging.
In this case, the researchers analyzed the data from Austria, matching 1,290 people: 330 vegetarians, 330 who ate meat but still consumed significant amounts of fruit and vegetables, 300 “mainstream” consumers who ate reduced amounts of meat, and 330 heavy meat-eaters.
And the takeaway that every source, from CBS News to The Independent newspaper in the UK to the federally funded National Center for Biotechnology Information to a slew of websites, was that despite the reality that vegetarians consume less alcohol per capita and generally have a lower and presumably healthier BMI (Body Mass Index), they were in worse physical and mental condition than their meat-eating counterparts.
Let me repeat that: Veggies were in worse physical and mental condition than their meat-eating counterparts.
People who ate less meat were also more likely to embrace adverse health habits, such as avoiding visits to a physician.
“Our study has shown that Austrian adults who consume a vegetarian diet are less healthy (in terms of cancer, allergies, and mental health disorders), have a lower quality of life, and also require more medical treatment,” the researchers wrote.
Not surprisingly, there was pushback to the study’s conclusions, with critics arguing that the report is simply propaganda to promote the meat industry. Even one of the principal investigators chimed in with the media’s take on the study.
“We have already distanced ourselves from this claim, as it is an incorrect interpretation of our data,” Nathalie Burkert, an epidemiologist and the study coordinator, told The Austrian Times.
Now, that might seem like a flat-out denial of the published summaries of the study. But check out what Burkert also told the newspaper.
“We did find that vegetarians suffer more from certain conditions, like asthma, cancer and mental illnesses, than people that eat meat as well,” she said, “but we cannot say what is the cause and what is the effect.”
Burkert then added the classic researcher’s addendum: “There needs to be further study done before this question can be answered.”
That’s true, but all epidemiological studies require further study, especially ones that attempt to link dietary specifics with disease specifics.
Nevertheless, to read that meat and dairy are part of the solution — not the problem — regarding health and nutrition is as satisfying as digging into a thick, juicy steak or pork chop.
That’s how you ward off cancer, allergies, and most definitely, mental health problems.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.