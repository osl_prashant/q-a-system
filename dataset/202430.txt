The National Weather Service's (NWS) forecast for fall -- September through November -- calls for above-normal temps across the contiguous U.S. and for above-normal precip along the Gulf Coast states into central Missouri. Elsewhere, chances are equal for normal, below- or above-normal precip. The highest probability for above-normal temps are forecast for parts of the Southwest, the Northeast and central and southern Florida. The forecast will alleviate concerns about an early frost, as some forecasters have been sounding the alarm on such an event.
The NWS says the current neutral phase of ENSO is expected to persist at last into early winter, as the chances of El Nino development the next few months have decreased. "At this time, the set of seasonal outlooks is based on the expectations for a borderline neutral/La Nina event," it states. "As the ENSO situation becomes more certain over the next few months, adjustments will be made accordingly to the seasonal outlooks."
NWS outlooks for September through November:




 




 




In its outlook for September, the NWS calls for below-normal temps in southern Nebraska into northern Oklahoma, while above-normal temps are expected west of the Rockies and along the Gulf and East Coasts. Across the remainder of the Plains and Midwest, chances are equal for normal, below- or above-normal temps. Additionally, above-normal precip is expected across the Southern Plains and eastern Corn Belt eastward to the East Coast. Across the West, the remainder of the Plains and western Corn Belt, chances are equal for normal, below- or above-normal precip. If realized, above-normal precip in the eastern Corn Belt during September would provide a boost to filling crops and additional rains in Oklahoma and Texas would improve soil moisture for HRW wheat establishment.
NWS outlooks for September:




 




 




Below, we compare the Seasonal Drought Outlook to the current Drought Monitor:




 




 




The latest National Drought Monitor reflects the introduction of extreme (D3) drought into south-central Iowa and the increase of severe (D2) drought. While rains this week are providing some relief to dry areas of Iowa, much of the state has been in a precip deficit situation for about four months.
The Seasonal Drought Outlook calls for some improvement to drought areas of Iowa, but for drought to linger into the fall. "As climatological precipitation decreases, drought persistence is favored for Montana, North Dakota, western South Dakota and northern Minnesota," it notes. "A wet pattern over the next two weeks, however, favors drought reduction across the central Plains, Iowa and eastern South Dakota. Drought reduction is also anticipated across southern Texas, where September is a wet time of year."