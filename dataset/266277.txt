MPCA urges judge to drop objection to wild rice rule change
MPCA urges judge to drop objection to wild rice rule change

By STEVE KARNOWSKIAssociated Press
The Associated Press

MINNEAPOLIS




MINNEAPOLIS (AP) — Minnesota regulators asked the state's chief administrative law judge Wednesday to reverse her rejection of their attempts to change the state's water quality standard for protecting wild rice.
The Minnesota Pollution Control Agency said in a 286-page filing that it has made technical changes to its proposal that should address the judge's concerns.
The MPCA has been working for several years to replace the current 10 milligram-per-liter flat limit for sulfate discharges into lakes and streams that produce wild rice with a complex mathematical formula tailored to the chemistry of specific waters. As part of that effort, the agency also developed a list of around 1,300 water bodies where the new standard would apply.
But Chief Administrative Law Judge Tammy Pust rejected the equation-based approach and the list in January, saying the proposal would violate federal and state law and put an unfair burden on Native Americans who harvest wild rice for food. The proposal had upset both sides in the debate — environmentalists and tribal groups that support the existing standard on one side, and mining companies and wastewater treatment plant operators on the other that say it's prohibitively expensive to comply with either version.
In its response Wednesday, the MPCA said its revised proposal will adequately protect wild rice, and that similar equation-based standards for protecting water quality have been successfully implemented within Minnesota and the Great Lakes. The MPCA said in a separate statement that it expects the judge will review its response and say whether her concerns about the proposed standard and list of waters have been addressed.
Minnesota adopted the 10 milligram standard in 1973 but it went largely unenforced until recent years when the debate heated up over mining in northern Minnesota. The 2011 Legislature ordered the MPCA to study whether the old standard, based on research from the 1930s and 1940s, needed updating, and to revise it accordingly.
Some lawmakers are now promoting legislation to nullify the existing 10 milligram standard and block the MPCA from adopting its proposed change. The bill already has passed three committees apiece in the House and Senate, but it still has other committee stops to go and its future is uncertain.