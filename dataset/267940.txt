Grains mostly higher and livestock lower
Grains mostly higher and livestock lower

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were higher Monday in early trading on the Chicago Board of Trade.
Wheat for May delivery rose 12 cents at $4.8140 a bushel; May corn was up 2 cents at $3.9020 a bushel; May oats gained 4.20 cents at $2.3660 a bushel while May soybeans was advanced 19.80 cents at $10.4320 a bushel.
Beef and pork were higher on the Chicago Mercantile Exchange.
April live cattle rose .57 cent at $1.1380 a pound; Apr feeder cattle gained 2.03 cents at $1.3763 a pound; April lean hogs was up .12 cent at .5250 a pound.