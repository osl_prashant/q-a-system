The makers of edible coating products that extend the shelf life of fresh-cut fruits and vegetables has a new product for fresh-cut bananas.
Bielefeld, Germany-based Food Freshly's O Plus Ultra-7 is an antioxidant that can add up to 10 days of shelf life to fresh-cut bananas, according to a news release.
Fresh-cut bananas are dipped for less than two minutes in a solution that is free of preservatives and genetically modified organisms.
"Bananas are not to be found in the fresh-cut landscape as of yet, since they turn brown immediately," Benjamin Singh, Food Freshly's director of technical sales, said in the release. "Current antioxidants on the market were not able to handle these sensitive fruits until now."
Founded in 1994, Food Freshly expanded into North America in 2013, opening Food Freshly North America Inc. in Toronto.
The company's coatings are used on a variety of fresh-cut fruits and vegetables.