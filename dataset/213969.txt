Photo courtesy Burch Farms
(Corrected) Despite a significant decline in acreage, North Carolina sweet potato growers are optimistic about the size and quality of their crop.
“We’ve had pretty good weather,” said George Wooten, owner of Wayne Bailey Produce, Chadbourn, N.C. “We’ve had good digging conditions. Quality is really good. We’ll have good supplies for the holidays and the rest of year.”
Wooten said in mid-October that his harvest was about half finished and that his company had been shipping new crop sweet potatoes for a month.
The U.S. Department of Agriculture reported Oct. 17 that 40-pound cartons of U.S. No. 1 sweet potatoes from North Carolina were $13-16.
North Carolina, the nation’s largest sweet potato producer, has 83,000 planted acres this year, down from 98,000 a year ago, according to USDA.
Wooten said the loss of acreage would be offset somewhat by yields that have been roughly 5% higher than a year ago. The new crop, he said, has produced a high percentage of larger-sized sweet potatoes.
“That’s what people want,” he said. “Everyone likes the big ones.”
Hurricane Irma left North Carolina’s crop largely unscathed in mid-September. In fact, the biggest weather concern for growers was a long dry spell in June that turned out to be a non-issue.
“Quality is good,” Kendall Hill, co-owner of Tull Hill Farms Inc., Kinston, N.C., said in late September. “We haven’t had any weather issues. Our crop is decent. It’s not the biggest we’ve ever had.”
Jeff Thomas, director of marketing for Scott Farms International, Lucama, N.C., said the 2017 crop has “great sizing, consistency and quality” and added that the company’s volume should be similar to last year.
Kelley Precythe, president and owner of Southern Produce Distributors Inc., Faison, N.C., echoed those sentiments.
“The quality of my crop is outstanding,” he said. “Weather has been very cooperative. I don’t see a real big crop, but I see a good quality crop.”
Director of marketing and business development Tami Long said in mid-October that Nash Produce LLC, Nashville, N.C., was about half finished with its harvest, which was expected to be finished by Thanksgiving. She said the company expected to meet or surpass its 2016 volume.
Long also said the company increased its organic acreage this season.
“There’s big-time demand,” she said. “Everyone is looking for organic.”
Jimmy Burch, co-owner of Burch Farms, Faison, N.C., said retail sales of bulk sweet potatoes are relatively flat but demand for prepackaged items, such as bags, steamers and microwavable sweet potatoes, is increasing.
Kim Kornegay, owner of Kornegay Family Farms & Produce, Princeton, N.C., said such specialty packs could also help reinforce marketers’ push to expand sweet potatoes beyond their traditional role as a Thanksgiving staple.
“Sweet potato loose packing hasn’t changed much, but it’s exciting to see sweets in steam bags, micro-wrapped and in other value-added packaging,” she said. “Those options have been on the rise in the past few years and helps reinforce the idea that sweets aren’t some hard-to-cook root that your grandma only made at Thanksgiving; they are suitable year-round for all sizes of families and all levels of cooks.”
Burch said that as consumer demand increases, more chefs are taking notice.
“Restaurants are using sweet potatoes in more dishes and as more than just a baked side item,” he said. “Consumption is up, and that is driving foodservice sales up.”
Editor's note: The original version of this story incorrectly identified Kelley Precythe's current title.