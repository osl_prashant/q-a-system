"I like blowing away stereotypes. You can take care of those pigs. You can load those market hogs. You can drive that tractor."That's the message pouring out of a new video campaign from the National Pork Board to celebrate and empower women working in the pork industry. The featured women, bonded by the #RealPigFarming sisterhood, are an amazing group of experts who should never be underestimated.
The National Pork Board pulled together four #RealPigFarming women to speak about their experiences in a short video. These women included:
Cristen Clark, a sixth generation farm girl who farms with her family in central Iowa and still finds time to manage her successful blog
Lexi Marek, who grew up on a pig farm in Southwest Iowa and is currently attending Iowa State University
Erin Brenneman, part of the Brenneman Pork family, one of the USFRA's "Faces of Farming and Ranching" winners, and regular PORK Network contributor
Emily Erickson, who grew up on a pig and crop farm in southwest Minnesota and currently works as the animal well-being and quality assurance manager for New Fashion Pork
The video did more than praise women who workin modern hog barns. As Clark explained, women have always been active in the industry. For example, women were working in the barns 50 years ago.
"A lot of women were farming. They were at the helm of their farm at the time. They set the stage for us to be able to do this through social media and new efforts to get out there and have a voice," she said.
Erickson added, "Our jobs haven't changed over the last 50 years. We're just more vocal about it."
"Your gender doesn't stop you," Marek stressed.
To watch the full "Women in Pig Farming" video, click here or watch the video above.