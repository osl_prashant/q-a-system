Exports continue to play a significant role in the California table grape deal.
About 36% of the state’s table grapes were exported in 2016, and Kathleen Nave, president of the Fresno-based California Table Grape Commission, expects export volume to be significant in 2017, as well.
“Exports are critically important to the overall health of the California table grape industry,” she said.
California growers export to more than 60 countries.
“We pretty much export everywhere,” Nave said. “We have a growing industry and we need all the markets we can get.”
The commission targets 30 of those markets for special promotions, said Susan Day, vice president of international marketing.
Targeted markets account for more than 97% of the total export volume.
They include Asian markets like China, Japan, Hong Kong and The Philippines; and European markets such as Germany, Ireland and The United Kingdom.
Australia and New Zealand are also targeted.
In addition, the commission will promote the state’s grapes in Mexico, South America and several countries in Central America and the Caribbean.
Most of the promotions are scheduled for supermarket chains and public and wholesale markets.
They include:

Point-of-purchase materials featuring a wood-grained background, California’s blue skies and three colors of California grapes. The materials have been are translated into 15 languages.
In-store sampling that gives shoppers a chance to meet a California grape mascot, take home recipes and other information and sometimes receive a gift, like a small lunch box, with each California grape purchase.
Display contests designed to motivate retailers to move California grapes and entice shoppers to buy.
Importer incentive contests designed to motivate marketing of California grapes earlier in the season.

Retailers love to promote California grapes, Day said.
“When we do retailer promotions, we see an uplift in sales,” she said.
Sales increases range from 50% to as much as 300% over previous sales periods, she said.
In-store sampling also has proved to be an effective way to sell California grapes.
“We get a lot of requests from retailers for in-store sampling,” Day said. “They definitely get a lift in sales when their customers are able to try them first.”
Sampling is conducted by uniformed team members who sample multiple varieties and are equipped with talking points touting the attributes of California grapes.
“It’s all done very professionally,” Day said.
A pilot project involving online ads and sales at home delivery companies also is planned for South Korea and China in 2017, she said.