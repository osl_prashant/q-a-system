Nick Wishnatzki and Amber Maloney, members of the marketing staff at Wish Farms, stand by the company's berry display at Fresh Summit.

NEW ORLEANS — Wish Farms highlighted its raspberries at its Fresh Summit booth, celebrating the elevation of its program to a one-stop berry shop.
Customers want to be able to get all their berries from the same place, said Amber Maloney, director of marketing for Wish Farms, so that has been a goal for the Plant City, Fla.-based company.
“It’s a really big deal for us,” Maloney said. “We’ve been year-round in strawberries and blueberries for a while now, we added blackberries last October, and now we wanted to complete the berry patch.
“We’ve done it with raspberries, and we’re really excited,” Maloney said.
Wish Farms was also talking with expo attendees about a new package it has developed, called Take Two, which allows consumers to pick up two pounds of berries at once.
“It’s about helping to move volume,” Maloney said. “Consumers, they eat one pound of strawberries really fast, so how can we sell two at a time, how can we make it convenient for consumers to pick up more berries.”
The packaging, which is fully recyclable, is also intended to provide convenience for retailers. The packaging that holds a pair of one-pound clamshells will have one universal product code, but on the bottom of the clamshell will be a different universal product code, so retailers can convert the two-pound packs to one-pound packs easily if desired.
Manos said responses from booth visitors Oct. 20 were instructive.
“They think it’s creative, they think the marketing opportunities are unique because you have a lot more space for your messaging, your branding, that kind of thing,” Maloney said. “There’s definitely some things we need to work through before it goes to market, but that’s why we’re introducing it at PMA, because we want that feedback.”