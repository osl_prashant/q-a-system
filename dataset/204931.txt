Crop calls
Corn: 1 to 3 cents higher
Soybeans: 6 to 8 cents higher
Wheat: 2 to 6 cents higher
Futures benefited from short-covering overnight, with traders also taking note of day 2 results from the Farm Journal Midwest Crop Tour. Traders said yesterday's data from Nebraska and Indiana didn't meet their expectations. Wheat futures enjoyed spillover from corn and soybeans overnight, as well as a weaker tone in the dollar index. Also this morning, USDA announced old-crop soybean sales cancellations of 640,970 to China, but that an unknown buyer has purchased 284,000 MT for 2017-18 and 11,220 MT for 2016-17.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Livestock futures are called mixed as traders sort out yesterday's Cold Storage Report. The report showed frozen beef stocks higher than expected and frozen pork stocks a little lighter than expected. Pressure on futures should be limited today by the discount they hold to their respective cash markets. A smaller cattle showlist gives feedlots more bargaining power this week, but another drop in pork cutout values will keep the cash market under pressure today.