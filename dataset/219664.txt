Chicago Mercantile Exchange lean hogs on Wednesday finished lower for a second straight day following another round of fund liquidation, along with increased supply expectations, traders said.
February, which will expire on Feb. 14, was pressured by lower market-ready, or cash, hog prices as packers try to stabilize their slipping margins.
February hogs closed 0.900 cent per pound lower at 73.850 cents. Most-active April finished 2.200 cents lower at 69.225 cents, and below the 200-day moving average of 71.329 cents.
"Once you crush the April (futures) like you did a couple of days ago, and it fell below the moving averages, April just fell apart," said Rosenthal Collins broker James Burns.
In a few weeks, packer competition for supplies will subside as more hogs come to market, he said.
Pork demand is crucial for grilling items, such as loins, with spring coming early to the south and southwestern regions of the country, analysts and traders said.
They said Easter ham buying will soon pick up and more pork bellies will be stored for spring and summer use.
On Wednesday the U.S. Department of Agriculture's (USDA) monthly export data showed total U.S. pork exports in December at 514.3 million pounds, down 3.9 percent from November, but up 3.8 percent from a year ago. 
Cattle Settle Weaker
Sell stops and technical selling weighed CME live cattle futures, said traders.
They said Wednesday's firmer wholesale beef values and this week's cash price expectations limited February futures declines.
February live cattle closed down 0.025 cent per pound to 125.600 cents. April ended 0.600 cent lower at 123.975 cents.
Some futures traders believe packers may pay roughly $126 per cwt for supplies after a few animals at Wednesday's Fed Cattle Exchange sold at that price.
So far processors in the U.S. Plains had bid $124 per cwt for cattle versus up to $130 asking prices. Last week cattle in the Plains brought $125 to $126.
Improved beef packer margins and higher wholesale beef values bode well for cash prices, a trader said, but added that futures price direction will be key.
USDA's export data showed December U.S. beef exports totaled 260.8 million pounds, up modestly from November and up 2.4 percent from a year earlier.
Sell stops and softer live cattle futures weakened CME feeder cattle contracts.
March feeders ended down 0.425 cent per pound to 148.300 cents.