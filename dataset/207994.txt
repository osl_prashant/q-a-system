German retailer Lidl’s first foray into the U.S. brings it on par in pricing with fellow German discount retail chain Aldi and below other major chains, including Wal-Mart, a recent study by Deutsche Bank shows.
Deutsche Bank analysts visited three of 10 stores Lidl opened in June — in Greenville, Kinston and Sanford, N.C. — to compare prices there against those at Wal-Mart, Kroger and Aldi.
“We came away impressed with Lidl’s relatively upscale in-store experience, low prices, and unique product assortment,” analysts Paul Trussell and Shane Higgins said in the report.
Lidl, which has more than 10,000 stores in 28 countries, now has 10 stores in Virginia, North Carolina and South Carolina.
The Deutsche Bank analysts said pricing was “about in-line” with Aldi. Lidl prices also came in at 10% below the Wal-Mart Neighborhood Market in the group’s first study and 5.7% below the Wal-Mart Supercenter in its second study — 8.1% below, when excluding national brands.
Lidl was priced 14.6% below the Kroger store that the analysts visited.
“Lidl and Aldi were almost at parity, though Aldi had the slightest edge, with its basket priced 0.7% less,” the analysts said.
“Notably, three items at Aldi were marked down the day of the Lidl opening.”
The analysts said the shopping experience at Lidl had a “premium” feel.
“Lidl uses shipment boxes exactly as Aldi does, but the overall presentation surpasses that of the average Aldi, in our view,” the analysts said.
“We believe Lidl’s aggressive private-label promotion, competitive assortment of national brands — 10% of the total assortment — and non-grocery section ‘surprises,’ such as shoes, cell phone chargers, workout mats, cookware, games and other items provide a unique value proposition and overall shopping experience.”
Deutsche Bank declined to comment further on the report.
Lidl U.S. spokesman Will Harwood did not respond to e-mail and telephone requests for comment.