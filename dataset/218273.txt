The spring semester Agriculture and Life Sciences Career Day at Iowa State University is filled with organizations recruiting students on Jan. 31.
The job fair is open to the public from 10 a.m. to 2 p.m. at Iowa State’s Memorial Union, a smaller location than the one held last fall. There will be 101 employers recruiting compared to the fall semester job fair that attracted 265 organizations.
“This is a great ‘second chance’ event for students still seeking internships or full-time positions,” said Mike Gaul, director of career services for the College of Agriculture and Life Sciences. “It’s also a great opportunity for students to embrace networking opportunities for future employment endeavors.”
The recruiting organizations are listed on the career services website: http://www.career.cals.iastate.edu/cals-career-day
“There is a nice mix of new companies looking to attend which hopefully has a trickle-down effect on our fall career day numbers next October,” Gaul said.
Several employers will stay to conduct on-campus interviews the following day in the Sun Room of the Memorial Union, he added.
The union is located at 2229 Lincoln Way. Parking is available in a ramp next to it.