Crop calls
Corn: 3 to 5 cents higher
Soybeans: 1 to 2 cents higher
Winter wheat: 1 to 2 cents higher
Spring wheat: 2 to 5 cents higher

Corn erased much of yesterday's losses overnight in reaction to USDA's lower-than-expected initial crop condition rating. The rating suggests top-end yield potential has already been removed for the crop. Spring wheat futures also posted solid gains overnight, with USDA ranking just 62% of the crop in "good" to "excellent" shape, which is 17 points lower than year-ago. A two-point drop in winter wheat rated in top shape was also supportive overnight. Soybeans benefited from light short-covering and spillover from corn and wheat. Traders will also be focused on end-of-the-month position squaring.
 
Livestock calls
Cattle: Higher
Hogs: Mixed
Cattle are called higher on followthrough from yesterday's recovery. Traders are also focused on narrowing the discount nearby futures hold to the cash cattle market. June cattle hold around an $8.50 discount to the average of last week's cash trade, which according to USDA was $131.50. Meanwhile, lean hog futures are expected to see a mixed start, with buying in the nearby contracts limited by the premium they hold to the cash index. But traders have a positive bias toward the cash hog market as packer demand is strong given highly profitable margins.