Court blocks company's attempt to restock Atlantic salmon
Court blocks company's attempt to restock Atlantic salmon

The Associated Press

SEATTLE




SEATTLE (AP) — A judge has rejected a request by an Atlantic salmon farming company that wants to restock a net-pen farm while it challenges Washington state's termination of its license to operate.
The Seattle Times reports that a Thurston County Superior Court Judge on Friday denied Cooke Aquaculture's request for a preliminary injunction while its lawsuit against the Department of Natural Resources plays out.
One of the company's pens collapsed last August at its Cypress Island farm, releasing more than 300,000 Atlantic salmon.
The company removed the collapsed pen but has two remaining pens. The company wants to resume business by restocking with nearly 800,000 juvenile Atlantic salmon.
The company declined to comment about the litigation.
___
Information from: The Seattle Times, http://www.seattletimes.com