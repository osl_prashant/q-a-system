Saying goodbye is always difficult, whether it’s to someone you love, a place with fond memories, or something else.

This week on U.S. Farm Report, cowboy poet Baxter Black shares the emotions of leaving the ranch.

Watch Baxter Black weekends on U.S. Farm Report.