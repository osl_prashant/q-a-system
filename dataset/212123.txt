CERRITOS, Calif. — Melissa Hartwig, co-creator of the Whole30 clean-eating program, told members of the Anaheim, Calif.-based Fresh Produce & Floral Council that they might be able to persuade consumers to eat more fresh fruits and vegetables if they practice some of the lessons she has learned over the years.Hartwig, a best-selling author and certified sports nutritionist, said she helped create the Whole30 program in 2009 to help people “change their health, their habits and their relationship with food.”
Whole30, she explained, involves “resetting … your cravings, your metabolism, your digestion and your immune system” by eliminating for 30 days foods that have been shown to be “problematic,” like sugar, alcohol and grains, and replacing them primarily with fruits and vegetables.
One of the biggest challenges she faced in getting people involved in the program is the same one that produce suppliers face: consumers know that vegetables are good for them, but they’re put off by them because their vegetable repertoire is limited to a few, often-boring selections, and they don’t know how to prepare vegetables in ways that make them “tantalizing or appealing.”
At the same time, vegetables are competing with convenience foods laden with sugar, salt and fat.
“People literally cannot appreciate the natural flavor found in fresh food when their taste buds are overstimulated with this hyper-palatable food,” she said.
Hartwig gave up telling people to eat vegetables because they’re nutritious and instead helped promote ways to make them taste good.
She introduced consumers to new produce items, new cooking methods and new preparation techniques.
“What we have done is made vegetables exciting and creative and tantalizing,” Hartwig said.
Social media played an important role and can help produce suppliers “create a brand and a voice,” she said.
She encouraged the produce industry to do what she did: solicit bloggers from the community to develop and share recipes that make fruits and vegetables appealing.
Get materials into the hands of “influencers” — bloggers who talk about food.
“Those are the people instrumental in helping you share your message,” she said.
Answer questions like, “What do I do with persimmons?” “When is asparagus in season?” “What can I do with kale besides steam it or boil it?”
“All of those things are creating value for your social media community,” she said.