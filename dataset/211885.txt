Last year’s ideal weather conditions contributed to a very bountiful crop of onions in the Treasure Valley.
Too bountiful, in fact.
This year’s challenging winter weather, plus an expected decline in acreage and yields, have onion grower-shippers feeling pretty good about their near-future prospects.
“There is much more optimism this year than there was at this time last year,” said Shay Myers, general manager for Nyssa, Ore.-based Owhyee Produce.
“The overall attitude is that we have a pretty manageable crop this year,” Myers said.
Myers anticipates a reduction in yields in the Northwest of 10% to 15% below average.
“Last year the yields were 10% to 15% above average,” he said.
“It’s a little too early to early to make any good projections about how yields will be. The best we can do is speculate from our own observations from walking the fields,” Myers said.
Kay Riley, general manager for Nyssa, Ore.-based Snake River Produce Co., said his company foresees a slight reduction in acreage and lower yields because of the challenges of the growing season.
“I think we should have a very manageable crop, which we hope would lead to at least a good marketing situation,” he said.
“We’re cautiously optimistic, and I think most of the growers are quite optimistic, and that definitely helps.”
John Vlahandreas, Salem, Ore.-based onion sales manager for Idaho Falls, Idaho-based Wada Farms, said he thought the demand for onions would be there.
“Acreage is down a little bit,” he said.
“So, hopefully, all the key factors will come in, and we’ll have some decent movement here.”  
Chris Woo, partner in Ontario, Ore.-based Baker & Murakami Produce Co. LLLP and sales manager for Potandon Produce LLC, adopted a wait-and-see approach.
“It’s a little bit too early to tell,” he said on Aug 9, before the harvest began.
“Once we get started in production, we will test the waters and see what the market will bear. This time of year you see the prices other areas are shipping, and if those prices could hold, then we’d be in good shape.”
Grant Kitamura, CEO for Baker & Murakami Produce, said onions were coming out of New Mexico and California in early August.
“I think we’ll have a good transition since we’re late and allow them to clean out their product,” he said.
“We expect a decent price in the fall.”
Jon Watson, president of Parma, Idaho-based J.C. Watson Co., said his company expects the size profile might be a little down.
“We are looking forward to a manageable crop in the Northwest,” he said.
“Maybe it’s our turn to have a right-sized, manageable crop. We pray for these times, but often they don’t come around.”