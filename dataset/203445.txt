New York's Hunts Point Produce Market is in long-term talks with the city that could eventually result in new warehouses for distributors, according to a report in The New York Post. 
 
The Sept. 5 report quoted Joel Fierman, president of Fierman Produce Exchange Inc. and co-president of the 38-member Hunts Point Market cooperative. 
 
Fierman could not immediately be reached for comment by The Packer on Sept. 8.
 
According to the report in the Post, recent lease negotiations with the city's Economic Development Corp. are considering new buildings that would be constructed in stages. As new buildings are added, old facilities would be removed, Fierman said in the story. Fierman told the Post that the market is in its last five years of a seven year lease option with the city.