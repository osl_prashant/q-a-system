What if you lost 20 percent or more of your herd? Or what if a corn farmer lost 20 percent of a standing cornfield to pests or some other natural disaster? In both cases, it would be fair to say that there would be many questions about why those losses occurred, and solutions would likely be sought regarding how to prevent such losses in the future.
 
Yet similar losses occur every year in silage bunkers and piles across the United States. Losses of 15 percent or more of harvested dry matter commonly occur, representing significant financial costs. Storage losses also affect the feeding value of the remaining silage. Feeding even low levels of spoiled silage results in reduced intake, lowered digestibility and poor performance.
 
Harvest Considerations
Optimizing silage value starts by harvesting at the right moisture content. Harvest should start at no wetter than 70 percent moisture with the goal of being completed before the crop is drier than 58 percent to 60 percent moisture. The length of cut should be between ½" to ¾". Dull knives tend to tear rather than cut cleanly making it more difficult to pack.
 
Relying on how the crop “looks” to determine when to start chopping silage can often be deceiving. Drought-stressed corn is often wetter than it looks from the road. Modern corn hybrids are bred to stay green longer under good conditions, so corn not under stress tends to be drier than one might think just by looking. Producers can use a microwave oven and a gram scale to estimate moisture quickly and be much more precise with harvest decisions. Step-by-step instructions on how to use a microwave for moisture testing are available in Using a Microwave Oven to Determine Moisture Content of Forages.
 
Dry Matter Recovery
Failure to exclude oxygen out of the silage pack leads to several fermentation issues and dry matter losses. When it comes to silage fermentation, the “good” bacteria require anaerobic conditions. These microbes produce lactic acid resulting in a rapid drop in pH and maximum dry matter preservation. If oxygen is present, undesirable microbes such as yeast and molds feed on the most digestible nutrients reducing the amount of recoverable dry matter and the energy content of the silage.
 
Using inoculants can help increase the amount of desirable bacteria, resulting in a faster pH drop and increased dry matter recovery. Inoculants can also improve aerobic stability when the silage is fed. Remember that inoculant strains are living organisms, so avoid exposing them to excessively high temperatures.
 
Packing and Storage
Packing the pile well can be challenging, especially with modern, high-capacity chopping equipment, but it is an essential step to exclude oxygen. Adding additional packing tractors may be necessary for sufficient capacity. Keep the layers uniform and less than 6" in depth. Packing needs to be continuous throughout the filling process.
 
Covering a silage pile is arguably the least desirable job on a farm but is vitally important to preserve feed value. An uncovered pile can lose 60 percent of the dry matter in the original top three feet. An oxygen-barrier film combined with a white-on-black plastic cover results in less loss compared to a layer of plastic alone. Cover the pile as soon as possible after harvest is completed. Waiting as long as 12 to 24 hours after harvest significantly increases storage losses.
 
Farm Safety
Finally, keep safety in mind. Slopes should be no steeper than a ratio of 1-to-3 (1' of rise for 3' of horizontal) to reduce the chances of rollover. Keep the height of the pile no more than the unloading equipment can reach to reduce the risk of catastrophic, even life-threatening, avalanches.
 
Sponsored by Lallemand Animal Nutrition