Within agricultural production a good share of livestock producers perform routine veterinary work themselves. This includes administering vaccinations or treatments for common disease or sickness. A result of performing this type of work there is increased risk for injury do to a needle stick injury. According to the Upper Midwest Agricultural Safety and Health Center (UMASH), needlestick injury research shows that over 80% of farm workers and 73% of swine veterinarians working in animal agriculture have accidentally stuck themselves with a needle.
Needlestick Injuries: Risks & Prevention
Producers need to educate themselves and their employees about the proper protocols when administering injections to livestock to prevent a needle stick from occurring.
First, we need to ask, “Why are needlestick injuries a big deal?” Some of the more common injuries that can occur are the following: skin infections, allergic reactions, and deep tissue wounds that would require surgery. However, some less common but more serious injuries can occur and possibly even cause death include: miscarriages due to hormone products, a serious cardiovascular event, suppression or coma, systemic infections and allergic reactions to antibiotics.
Employers
As livestock and employee managers’ we need to do and provide the following:

Train and retrain employees regularly about safe needle handling, safe injection procedures and the types of medications used.
Provide safe animal handling equipment and ensure proper staffing when handling livestock.
Provide readily accessible sharps container for safe needle disposal. You may need to show and explain what the sharps container is and why it is important.
Provide needle / syringes with protective devices such as retractable needles or hinged syringe caps.
Remind employees to use caution when using products of concern.
Pregnant employees should not inject hormones.
Encourage employees to practice best management practices when handling livestock, staying calm and not rushing.
Encourage employees to report injuries.
If a needlestick has occurerred the employees should contact a healthcare provider.

Employees
Employees and those administering any medication should practice the following:

Slow down – don’t rush injections.
Restrain animals properly using the appropriate livestock handling equipment and technique.
Do not put needle caps in your mouth.
Discard all bent needles and do not reuse them or straighten them.
Do not put needles or syringes in your pockets.
Use an approved and properly labelled sharps container.
Do not remove needles from sharps containers.
Do not recap needles.
Report all needlestick injuries to management.
Contact your health care provider if you have a needlestick injury.

Product Safety
As livestock producers we need to be aware of various products that are high risk and cause the most concern when a needlestick injury occurs. They are the following: Tilmicosin (Micotil®), Sedatives (e.g. Xylazine), Oil-based adjuvants, Brucella abortus Strain RB51 vaccine, Modified live vaccines (e.g. Erysipelas vaccine, Johne’s vaccine), Hormones – especially if handlers may be pregnant, and any Antibiotic which may cause an allergic reaction.
Training Materials
If you would like to access UMASH training materials in English or Spanish regarding needlestick prevention, visit the UMASH Resource Database. Scroll down through the tags (left side of screen), and click on needlestick.