U.S soybean and corn futures fell again on Wednesday following hefty drops on Tuesday as better weather in U.S. grain belts and a new report of good U.S. crop conditions raised expectations of large U.S harvests.Wheat also fell on favorable U.S. harvest progress and good crop conditions.
"There is a bearish market mood today with the focus again on the outlook for ample world supplies, with U.S. crop conditions looking good and U.S. crop weather improving," said Frank Rijkers, agrifood economist at ABN Amro Bank. "The world market is facing large crops meeting large stocks."
Chicago Board of Trade November soybeans were down 1.07 percent at $10.65-3/4 a bushel at 0857 GMT. Soybeans had fallen around 5 percent on Tuesday on forecasts for crop-friendly U.S. weather.
CBOT December corn slipped 1.1 percent to $3.53-3/4 a bushel after falling on Tuesday after welcome rainfall for U.S crops.
CBOT September wheat was down 1.04 percent at $4.29 a bushel after rising on Tuesday on bargain hunting after the front-month contract touched its lowest since September 2006.
"The U.S. crop condition reports late on Tuesday showed that U.S. wheat, corn and soybeans are in overall positive condition and development is on track despite talk in the past couple of weeks about adverse, dry U.S. weather," Rijkers said. "Crop ratings for soybeans were down a tick on the week but better than last year."
The new U.S. crop progress report from the U.S. Department of Agriculture (USDA) on Tuesday said 62 percent of U.S. winter wheat was in a good to excellent condition against 62 percent a week ago and only 40 percent a year ago.
For U.S. corn, 75 percent was in good to excellent condition against 75 percent last week and 69 percent a year ago.
For soybeans, 70 percent was in good to excellent condition, down from 72 percent a week ago but well up from 63 percent a year ago.
The outlook for more rain in key growing areas of the U.S. Midwest during the next two weeks outweighed concerns about dryness in some areas.
"Also bearish for soybeans are indications of slackening demand for U.S. exports," Rijkers said.
U.S. soybean export inspections totalled 191,426 tonnes in the latest week, down from 295,816 a week ago.
"The U.S. winter wheat harvest is also progressing well, the picture is looking good at this stage of the game," Rijkers said.