There’s still a lot of lore and romance left in the Old West: cowboys, cattle drives, dusty trails.
It’s a way of life unique in many ways to the American cowboy, and this year, one of the iconic links to the lifestyle is celebrating is sesquicentennial.
Dave Deken with SUNUP TV shares the story of the 150th anniversary of the Chisholm Trail.