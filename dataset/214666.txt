The U.S. Department of Agriculture has named 12 members of the Hass Avocado Board.
Growers appointed to 3-year terms are:

Susan Pinkerton, Santa Paula, Calif.;
Laurie Luschei, Mission Viejo, Calif.; and
Ben Van Der Kar, Carpinteria, Calif.

Growers appointed to 3-year terms as alternates are:

C J Shade, Ojai, Calif.;
Will Carleton, Carpinteria; and
Jeff Dickinson, Fallbrook, Calif.

Importers with 3-year terms are:

Jorge Hernandez, Pharr, Texas; and
Sergio Chavez, Lake Elsinore, Calif.

Importers appointed to 3-year terms as alternates are:

Jose Antonio Gomez, Plantation, Fla.; and
Nils Goldschmidt, Henderson, Nev.

Grower Bob Schaar, Fallbrook, was appointed to a 2-year term as alternate, and grower Thomas Escalante, Oxnard, Calif., was appointed to a 1-year term as alternate.
“These appointees represent a cross section of the hass avocado producers and I know that the hass avocado industry will be well served by them,” Agriculture Secretary Sonny Perdue said in a news release.
Each Hass Avocado Board member has an alternate who can vote on behalf of an absent member, according to the release. The USDA appoints about one-third of the members each years as terms expire.