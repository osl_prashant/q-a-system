To date, Arkansas has received more than 50 complaints of dicamba drift, damaging thousands upon thousands of acres. That number has room to grow as much of the season remains. The state received just 32 dicamba complaints in 2016.
“Dicamba complaints have been much more widespread and may cover 1,000 acres or more at a time,” writes University of Arkansas Extension weed scientist Tom Barber in a recent blog post. “Our agent in Phillips County has estimated approximately 20,000 acres that have been affected in his county.”
Ten other counties in eastern Arkansas have also seen damage, and last Thursday, more than 7,000 acres of drift were reported in Mississippi County, including an Arkansas Extension research center.



Click here to see examples of dicamba damage.

Missouri and Mississippi are seeing complaints roll in, too.
“It just started this week,” says Kevin Bradley, University of Missouri Extension weed scientist. “We didn’t get any calls until June 22 last year.”
Most of these complaints are coming from the northern part of Missouri—last year, the Bootheel received the brunt of the damage. To date, Bradley estimates thousands of acres are damaged. Some are reported to the Missouri Department of Agriculture, but many cases he deals with stay unreported to the state and are handled between farmers.
It’s not old technology causing the problems this year.
“The drift I’m aware are all legal applications that are being made of the new products,” Bradley says.
This provides further proof that minding wind speeds, boom height and inversions are all critical  practices, even with new formulations.
“We certainly want to hear from growers if they have questions regarding the use of Engenia herbicide and we will continue to reinforce the need to read and follow label requirements,” notes BASF in an emailed statement. “We are here to support our customers and, if requested, can assist in investigating an off-target allegation in an advisory capacity to provide technical support.”
Damage can enter a field in several ways but physical drift seems to be the biggest culprit.
“Physical drift accounts for at least 80% to 90% of all the dicamba injured fields that I have personally walked,” Barber says. “The other 10% to 20% is not that easy to figure out.”
What do I do now?
Although dicamba damage is easy to recognize, it can be difficult to identify the source of drift. It can take 7 to 21 days for dicamba damage to appear in soybeans—and it only shows up on new leaves, not the leaves that were present when the herbicide entered the field. As little as .06% to 1.9% dicamba drift can cause yield loss, according to North Dakota State University.
“We’ve seen in our research, a very low rate (1/30,000x) of dicamba can cause soybean leaves to cup,” Barber says. “No, there will not be any yield lost at this low of a rate, but you will still see injury on the beans.”
Applicators should keep detailed records of when dicamba was applied, where, what the wind speed was and what the temperature was to protect themselves if drift appears in their area.
“Since receiving label approval, we have been building upon ongoing education efforts specifically on the approved label application requirements and are continuing those trainings into the season. If applicators have questions on XtendiMax label compliance, it is vitally important that they contact their state or local pesticide regulatory agency, local extension agent, agronomist or Monsanto representative before spraying,” advises Monsanto in an emailed statement.
If you think you might have dicamba drift, talk to an Extension agent or state agronomy representative who can help investigate the origin and chemical causing drift. Agents or other agronomists can also help identify where and to whom drift should be reported. You could also consider speaking with a lawyer to learn your options for recovering damages.
“The implications of this year are going to help us learn what role this technology will have in the future,” Bradley says. “We’ve got to know what’s going on—whether it’s reporting it to the department of ag or to Extension, don’t keep damage to yourself.”
Farmers and agronomist take to social media with their experiences

Dicamba drift on non extend soybeans is common in Phillips county. Even happening in places with good stewardship pic.twitter.com/KU7qLVbHvt
— Robert Goodson (@agentgoodson) June 8, 2017


Don't expect miracles from Dicamba folks. This weed is clearly off label and you can see the results. Don't skimp on rates #resistance pic.twitter.com/io5YUcXL83
— Jeff Theis (@TheisAg) June 5, 2017


Getting quite a few calls on off target dicamba...be careful out there, do not forget how sensitive soybeans are!
— Bob Scott (@BobScottWeedDr) June 1, 2017


Research plots got zapped by dicamba drift. I'm sure we will see more of this in the future. pic.twitter.com/zsyezP5HLt
— John Orlowski (@OrlowskiJM) May 31, 2017


Arkansas Plant Board to hold weekly meeting on dicamba complaints until Jul 21. Fridays, 10 a.m. https://t.co/lFkH8Hnj6I
— Andrew Thostenson (@Thostenson) June 15, 2017


Seed guy ask why UofA banned Xtendimax n require all this stuff for dicamba apps...#bad information..this is ASPB, federal label #not UofA
— Bob Scott (@BobScottWeedDr) February 23, 2017