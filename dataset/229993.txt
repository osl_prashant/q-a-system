BC-Cash Prices, 1st Ld-Writethru,0342
BC-Cash Prices, 1st Ld-Writethru,0342

The Associated Press

NEW YORK




NEW YORK (AP) — Wholesale cash prices Friday
                                         Fri.       Thu.
F
Foods

 Broilers national comp wtd av             .9124      .9124
 Eggs large white NY Doz.                   1.60       1.56
 Flour hard winter KC cwt                  15.10      14.95
 Cheddar Cheese Chi. 40 block per lb.     2.1150     2.1150
 Coffee parana ex-dock NY per lb.         1.1745     1.1814
 Coffee medlin ex-dock NY per lb.         1.3786     1.3874
 Cocoa beans Ivory Coast $ metric ton       2405       2405
 Cocoa butter African styl $ met ton        6383       6383
 Hogs Iowa/Minn barrows & gilts wtd av     62.67      63.71
 Feeder cattle 500-550 lb Okl av cwt      181.00     181.00
 Pork loins 13-19 lb FOB Omaha av cwt      92.20      91.61
Grains
 Corn No. 2 yellow Chi processor bid      3.54¼       3.54¾
 Soybeans No. 1 yellow                   10.16¼      10.13 
 Soybean Meal Cen Ill 48pct protein-ton  380.40      382.50
 Wheat No. 2  Chi soft                    4.54¼       4.53¼
 Wheat N. 1 dk  14pc-pro Mpls.            7.45¾       7.48 
 Oats No. 2 heavy or Better               2.77        2.74¾
Fats & Oils
 Corn oil crude wet/dry mill Chi. lb.     .31¼         .31¼
 Soybean oil crude Decatur lb.            .30½         .30½
Metals
 Aluminum per lb LME                     0.9951      0.9933
 Antimony in warehouse per ton             8550        8550
 Copper Cathode full plate               3.1895      3.1763
 Gold Handy & Harman                     1327.95    1328.35
 Silver Handy & Harman                    16.540     16.640
 Lead per metric ton LME                 2524.00    2561.00
 Molybdenum per metric ton LME            15,500     15,500
 Platinum per troy oz. Handy & Harman     990.00     990.00
 Platinum Merc spot per troy oz.          998.80     999.40
 Zinc (HG) delivered per lb.              1.5984      1.6163
Textiles, Fibers and Miscellaneous
 Cotton 1-1-16 in. strict low middling     77.81      75.73
Raw Products
 Coal Central Appalachia $ per short ton   60.10      60.10
 Natural Gas  Henry Hub, $ per mmbtu        2.60       2.65
b-bid a-asked
n-Nominal
r-revised
n.q.-not quoted<29
n.a.-not available