Retailers have struggled for various reasons to get the most bang for their buck out of organics in the produce department. Suppliers offered some ideas to help. 
A key factor in building organic sales in supermarkets is in understanding the natural entry points for shoppers, said Steve Lutz, senior strategist for CMI Orchards, Wenatchee, Wash.
 
“We know from the success of our top-selling Daisy Girl Organics brand that when consumers make the shift from conventional to organic, they first look for the common organic version of the apple variety they currently buy,” he said. 
 
“Retailers can best leverage the organic opportunity by focusing sales programs first on the widely popular core varieties like gala and red delicious.” 
 
Lutz said national scan data showed that organic red delicious apples have the lowest retail price point of all major organic apple varieties, even lower than conventional pricing for many niche apples. 
 
“Some retailers are using organic red delicious and galas to create lower-price entry points to entice shoppers to move out of conventional and into organics,” he said. “This has the double benefit of building organic sales for the retailer while reinforcing a value message with consumers.” 
 
Another key for organic success in conventional retail stores is to watch the price spread between organics and the conventional substitute. 
 
“The data shows that as the spread between the same conventional and organic item reaches $1 per pound, consumers show price resistance,” Lutz said. “The result is organic sales falter and the retailer begins seeing shrink skyrocket. The sweet spot appears to be a per-pound price premium of between 50 and 75 cents per pound. Getting the shelf pricing right is key to accelerating organic sales and reducing shrink.” 
 
Mayra Velazquez de Leon, president and CEO of Organics Unlimited Inc., San Diego, said this year’s organic banana season coincides with back-to-school promotions.
 
“This offers retailers countless opportunities to talk about the nutritional benefits of bananas for children as well as the ease of portability in a lunchbox, as a banana does not need to be packaged,” she said.
 
Because shoppers often have questions about what “organic” really means, it’s important that store produce staff have some knowledge about organic standards, or at least where to go to seek out that information, said Samantha Cabaluna, vice president of brand marketing and communications for Tanimura & Antle, Salinas, Calif. 
 
Michael Castagnetto, vice president of sourcing for Eden Prairie, Minn.-based Robinson Fresh, said shoppers want clear choices. 
 
“Creating a destination for organic produce can better highlight the options available and build credibility for the retailer,” he said.
 
“If a destination for organic produce is created, we recommend cross-promoting with other organic items — like organic snacks or organic meat — to provide shopping convenience and to showcase the variety and broad assortment of organic items offered.” 
 
Castagnetto also recommended merchandising a smaller destination for snacking items near the larger organic display to capture additional impulse purchases. 
 
“For example, cross-merchandise berries, grapes, apples or easy-peel citrus with organic nuts, organic crackers or organic fruit snacks,” he said.