The Rio Grande Valley is the only place Texas’ sweetest crop is grown, and harvest is about to wrap up.

Sugarcane harvest lasts nearly half the year ,and growing the plant takes about as long.

Ed Wolff from the Texas Farm Bureau follows the path to sweet success on AgDay above.