New Zealand dairy company A2 Milk said on Thursday it had successfully registered its infant formula, processed by partner firm Synlait, in China.
The company, which is focused on the booming demand for foreign milk powder in China, had applied for registration in May after Chinese authorities ramped up oversight of foreign formula producers.
“We look forward to the continued expansion of our business in China following this announcement,” said Geoffrey Babidge, managing director of A2 Milk, in a statement posted on the stock exchange.
China is requiring all brands of imported infant formula to register with its Food and Drug Administration by Jan. 1.
A2 bought an 8.2-percent share in Synlait in March, with which is has an agreement to process its formula.