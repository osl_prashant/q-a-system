DENVER – There are consumers who love potatoes, and Potatoes USA’s fiscal year 2019 will target them.
That’s not to say potatoes aren’t popular among all consumers. Board leaders showed how potatoes are consumers’ favorite vegetable, and that’s reflected in the board’s mission statement: Strengthen demand of Potatoes USA.
At the Potatoes USA annual meeting March 12-15 in Denver, the board started with a review of its domestic marketing plans.
It approved FY19 domestic marketing investments of $4.86 million for the July to June fiscal year.
In that, $1.53 million goes for consumer marketing (80% on performance consumers, 20% general); $1.035 million for foodservice; $600,000 each for retail and nutrition professionals; $540,000 for research; $430,000 for ingredients; and $125,000 for digital resources.
In addition, members approved special project requests of $200,000 each for reaching nutrition professionals, integration of performance into other programs and a website redesign for potatogoodness.com.
Last year the board began marketing potatoes as a performance food, and that continues this next fiscal year.
John Toaspern, chief marketing officer, said potatoes are on an upward trajectory, which is a nice trend after some flat recent years.
The primary domestic strategies for the board are to inspire potato innovation across all channels, promote potatoes as performance food, cultivate strategic partnerships and advocate for scientific research to show potatoes’ role in enhancing physical and mental performance.
But it’s not all retail. Toaspern said the latest research shows food spending at home vs. outside of home are even.
In foodservice, potatoes saw a 0.5% increase in shipments. Also, potatoes have 82.9% menu penetration, up a percentage point from last year, he said.
Potatoes are the No.1 vegetables in school menus at 98%.
As meal kits gain popularity, especially from retailers, potatoes are a natural fit.
“Grocery store meal kits will grow, and we need to be a part of that,” Toaspern said.
Ross Johnson, global marketing manager — retail, said retailers have been receptive to the board’s “Do you have the right mix?” promotion, which can benefit larger sizes.
One retailer he worked with ran a 3-day promotion with 20-pound potato bags and had so much success it had to order more bags during the promotion.
“You can’t tell me consumers don’t want large bags of potatoes,” Johnson said.
Director of marketing programs Kim Breshears said marketing research shows the top diet trend is low sodium, followed by organic and then non-GMO, so the board has prepared information for retailers and the potato industry to show how potatoes fit into these trends.
She also said consumer website potatogoodness.com showed its best month in February, where it had 180,000 visitors for the month and more than 100,000 recipe searches, up from 40,000 last February.
Sarah Reece, global marketing manager — consumer marketing, said the board plans to continue running native advertising on food sites to drive traffic to potatogoodness.com.
“Americans love potatoes,” Breshears said. “They’re the No.1 favorite and top vegetable sold by volume and the No.1 side dish.”