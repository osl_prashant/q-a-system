Right now there are more questions than answers when it comes to dicamba damage. Among the biggest questions is what role do drift, volatility  and temperature inversions play.
“I’ve heard a lot of farmers say I’ve done this and this and it didn’t affect my neighbor’s soybeans. I applaud that,” says Aaron Hager, University of Illinois Extension weed scientist. While farmers might not see issues in fields next door, there has been little testing to determine if dicamba will volatilize and damage fields one or two miles down the road, he adds.
Farm Journal Field Agronomist Ken Ferrie encountered such a situation this summer that involved a field sprayed with a new dicamba formulation. Cupped soybeans were observed in a field one-half mile to the south of the sprayed field, which meant dicamba travelled over a corn field, as well as another north soybean field.
“It looks like there are some volatility issues because when you research the spray records they applied it according to label,” Ferrie says. 
Hager says the actual number of cases he’s seen with clear physical drift is low. “Instances of contaminated application equipment are fairly high, but higher still is the number of fields I’ve seen with uniform side-to-side damage. That’s not [from] physical drift but is volatility or inversion or a combination of both,” he adds.
Drift happens while the sprayer is still in the field. Volatility occurs after the sprayer has left. If a field is sprayed during a temperature inversion, small droplets can remain suspended and move off-site with wind. 
 




 


Normal atmospheric conditions


© Lindsey Benne







 




 


Conditions in a temperature inversion


© Lindsey Benne







It’s fairly easy to identify drift and sprayer contamination—you’ll either see a gradient of damage from one side of the field to the other from drift or a pattern of sprayer passes that eventually run out of product. It’s harder to track the origin or timing of damage from temperature inversions or movement from volatility because damage is usually even across the field.
“Drift is impacted by the particle size and by the sprayer, nozzles and boom height, and we know from our testing that if label instruction for XtendiMax is followed it can be used safely,” says Robb Fraley, Monsanto chief technology officer.
Drift and sprayer equipment contamination issues are within an applicator’s control. The label is the law. When it comes to volatility, farmers have little control. However, that doesn’t mean they can’t lessen risk. 
Monsanto, creator of XtendiMax with VaporGrip Technology, says their new formulation provides 90% reduction in volatility potential compared with Clarity. By what measure of volatility  is reduced? Other than the comparison to Clarity, there’s no measure and no third-party data with XtendiMax. 
“We conducted a lot of the studies leading up to [EPA approval], but we didn’t find VaporGrip until just a few years ago and used our own data; some third parties tested it, but they were contracted by us,” says Ty Vaughn, Monsanto lead for the global regulatory organization.
BASF says their Engenia product also provides a 90% reduction in volatility compared with Clarity, as it uses an improved dicamba salt called BAPMA. “Even a slight alteration from the label can result in physical drift going farther than expected, which can be confused with volatility,” says Chad Asmus, BASF technical marketing manager. “Engenia is currently available for any researcher who wants to do additional testing.”
Researchers have been performing their own tests to see what farmers can do to manage the new dicamba products, specifically for volatility. 
“Volatility is unpredictable,” says Kevin Bradley, University of Missouri Extension weed scientist. “For the first time, many university weed scientists were able to conduct tests on the volatility of these newly approved products in 2017.  I believe the information we have gathered, and that continues to be summarized and gathered even now, will help us to understand the role volatility plays in off-target movement.” 
The volatility information provided by dicamba manufacturers compares new products to old products such as Clarity or Banvel, Hager says. There is no numerical value or any information about what that means in relation to soybeans. “You have to stand on the science and all companies are doing is speculating,” he adds. 
“There are a lot of things we still need to understand, like what happens if soybeans have multiple exposure events?” Hager says. “We have 50 years of research and none of them shows multiple exposure testing.”
He says universities have divvied up some research projects. For example, while Missouri, Arkansas and Tennessee are testing volatility, Illinois is testing what multiple exposures mean for soybeans.
“The big question that keeps coming up is do we need better chemistry to address volatility concerns?” Ferrie says. “We have to look and compare cases, both where it did and didn’t work. By comparing the two, the answers should shake out.”
Companies are also studying the damage. If it’s from an application issue that’s an easy fix—more education for applicators. If volatility is  the issue that could lead to label restrictions or a change to the chemistry.
 “We have ongoing conversations with EPA ... if any changes are necessary we’ll make them based on the facts and science,” Asmus says.
As the industry works together to find a solution to dicamba, the focus is on next year—the second of the two-year label. In Arkansas, a task force is recommending dicamba  use be limited in the state in 2018 to applications between Jan. 1 and April 15. Farmer access to this technology and how EPA and others handle the technology in the future are riding on the answers to this year’s questions.