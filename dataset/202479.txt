Bugs Bunny must be ecstatic over the number of carrot variations now available on supermarket shelves and at local restaurants.
Besides traditional table carrots, major California suppliers now offer baby carrots, colored carrots and numerous different cuts, like shredded, diced and chips.
"When baby carrots hit the market decades ago, they dynamically changed our business and changed the way the world consumes carrots," said Steve Roodzant, senior vice president of plant operations and marketing for Bakersfield, Calif.-based Grimmway Farms.
"Today it's hard to imagine life without them, especially since they make up almost half of dollar sales in the U.S. carrot category," he said.
Baby bunch carrots are one of the most popular items at Babe Farms, Santa Maria, Calif., said Ande Manos, marketing and business development manager.
The company produces seven varieties - white, yellow, purple, maroon, pink, French (orange) and round, an orange carrot also referred to as Thumbelina or Parisian.
"Our mixed pack is our most popular pack," Manos said.
Consumers like the variety, and retailers like the fact that they don't have to buy separate boxes of different colored carrots.
Baby carrots now account for about 20% of the carrot volume at Kern Ridge Growers LLC, Arvin, Calif., said Chris Smotherman, account manager.
"It has grown slowly, for us," he said.
They've become a particularly popular item for school cafeterias.
"For schools, that category is huge," he said.
In fact, baby carrot sales to school districts have grown tenfold over the past couple of years, he said.
At home, consumers like baby peeled carrots for snacking, but they're mostly used for cooking, Smotherman said.
"They save people time peeling and chopping, and they're easier to handle," he said.
Kern Ridge also has enjoyed success with its matchstick shredded carrots, which the company has offered for about two years, he said.
Sales of baby carrots are up about 20% over last year for Los Angeles-based Coosemans L.A. Shipping Inc., said Tony Ramirez, a salesman and buyer.
The company's rainbow mix also is catching on among consumers and foodservice establishments, he said.
Coosemans offers mixed bags of orange, purple, red and yellow carrots for chefs who desire to create an attractive dish and consumers preparing a special meal, he said.
Grimmway Farms offers traditional and rainbow cut-and-peeled baby carrots, snack carrots, carrot dippers, Carrot Chips, shredded carrots (traditional and colored), Carrot Stixx and crinkle cut coins, Roodzant said.
Foodservice items include bias-cut carrots, carrot sticks, Carroteenies, chunk carrots, coin cut, diced carrots, julienne shredded carrots (traditional and rainbow) and matchstick shredded carrots.
Consumers have their timeless, traditional recipes, he said, but they also find creative, new ways to keep carrots on the table.