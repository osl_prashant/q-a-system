How greater sage-grouse conservation practices have affected ranch economics across six states is being studied by a University of Wyoming research team.The group in the Department of Ecosystem Science and Management will draw input from local ranchers across Wyoming, Idaho, Montana, Nevada, Oregon and Washington, said John Tanaka, professor and associate director of the Wyoming Agricultural Experiment Station.
The team will develop cow-calf ranch enterprise budgets for use in models to estimate the economic impacts of different conservation practices on ranches, said Holly Kirkpatrick, one of the research assistants.
Partnerships between federal and state agencies and private landowners have reduced threats to greater sage-grouse in 90 percent of the species’ breeding habitat, said Tanaka. He said the practices have changed the way livestock are grazed on millions of acres of land across the western United States, especially on public lands.
“Ranchers manage extensive areas of those lands and are critical to help keep the bird from being listed as threatened or endangered in the future,” said Tanaka. “The project will assess how ranchers and the communities in which they operate have been affected.”
The project is part of an initiative through the Sustainable Rangelands Roundtable and funded by the Natural Resources Conservation Service. The SRR is a partnership of agencies, non-governmental organizations, producer groups, scientific societies, environmental groups and multiple land-grant universities.
The UW team of four research assistants and two research scientists will develop four cow-calf ranch enterprise budgets, which document management practices, available resources and technology used, from nine major land resources areas within the six states.
The budgets, once finalized, will be made available to ranchers.
The ranch types are:
Small, private land only.
Small, private and public land.
Large, private land only.
Large, private and public land.
The rancher focus groups will ensure the validity of the representative ranching operations in the budgets, said Kirkpatrick.
“These enterprise budgets will be drafted as representative cow-calf ranching operations, which requires no private information from ranchers,” Kirkpatrick said. “Rather than sharing personal information about their specific operations, focus group participants will be asked to consider the typical procedures and cost estimates for cow-calf operations of a given size throughout their region.”
There can be a win-win for producers and sage-grouse if habitat management recommendations can be designed to also help ranchers be more productive or enhance profitability, said Tanaka.
“There is a saying out there, ‘Good for the bird, good for the herd,’” he said. “This project is the first range-wide effort to see if that is true from an economic perspective.”