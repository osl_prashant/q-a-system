The 2M Alliance, a vessel-sharing agreement between Maersk Line and Mediterranean Shipping Co., is adding a Northern European shipping route with service to Port Everglades, Fla. 
The new TA1/NEUATL1 line’s route is Antwerp, Belgium, to Rotterdam, Holland, to Bremerhaven, Germany, to Norfolk, Va., to Port Everglades to Houston to Norfolk and back to Antwerp.
 
The service will be offered on U.S.-flagged Maersk ships. It began Wednesday calls at Port Everglades on Aug. 16.
 
“This Northern European service further diversifies the fleet and services that call at Port Everglades and generates additional opportunities for international trade to and from South Florida,” Jim Pyburn, director of business development for Port Everglades said in the release.