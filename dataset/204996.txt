A Minnesota cooperative plans to build a $240 million soybean processing plant in North Dakota.
Officials with Minnesota Soybean Processors, or MnSP, say the co-op has selected a site on 150 acres near Spiritwood, in eastern North Dakota. The group says the facility would produce 900,000 tons of soybean meal and 490 million pounds of soybean oil a year. Half of the oil would produce biodiesel.
North Dakota Gov. Doug Burgum says the plant should add up to 60 jobs and support local service companies, vendors and suppliers. It would be the first of its kind in North Dakota.
The group said in a news release that construction would begin "following further due diligence, necessary approvals and a successful engineering study."
MnSP operates a soybean crush facility in Brewster, Minnesota.