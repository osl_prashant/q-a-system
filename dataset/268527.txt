Boom time on central Wisconsin's grasslands
Boom time on central Wisconsin's grasslands

By BARRY ADAMSWisconsin State Journal
The Associated Press

RUDOLPH, Wis.




RUDOLPH, Wis. (AP) — Hoarfrost hung from the trees, fence wire and prairie grass.
A school bus, pronounced by its blinking roof beacon, slowly headed west on Highway M just a few minutes before Carl Flaig could be heard to the northeast calling in his herd of 60 cows in a nearby paddock for their morning milking.
It was just past 6 a.m., the sun was rising behind us and the 10 inches of snow that fell had given Flaig's 350-acre organic farm a February look. The single-digit temperatures made the April morning feel like deep January.
But the reason for a 3 a.m. car ride to central Wisconsin and a 5:20 a.m. hike to one of four blinds set up on Flaig's back 40 had yet to appear.

                The Wisconsin State Journal reports that this is boom time on the grasslands when greater prairie chickens strut, dance, hop and flap in their annual courtship that is unique to this region of the state.
There was anticipation, hope and mounting doubt as the sun continued its climb sans birds, but just after 6:30 a.m. the payoff arrived at the breeding ground, referred to by birders as a "lek." That's when a pair of prairie chickens flew in from the south and gently landed about 100 feet from our blind. Another arrived about five minutes later. Two more came in from the east at 6:41 a.m. By 7:15 a.m., we found ourselves enjoying the company of eight prairie chickens, one of four species of native grouse in Wisconsin.
"It's a whole different experience when it's spring," said Flaig, whose cows and cattle graze his land to ideal conditions for prairie chicken mating dances. "That's why they like to come to my farm, it's a very tight, highly managed grassland."
Flaig's farm is surrounded by the 3,000-acre Paul J. Olson Wildlife Area that is made up of scattered parcels ranging from 40 to 860 acres in western Portage and eastern Wood counties. The habitat, which includes canary, timothy, brome and quack grass, along with willow brush and spirea, provides prime areas for prairie chickens to mate, roost, nest and draw onlookers.
On April 14, the area will be front and center for the second annual Wisconsin Prairie Chicken Festival. The daylong event debuted last year after the Central Wisconsin Prairie Chicken Festival ceased operations. Volunteers with that organization rallied to bring back a festival under a new name but still focused on birds that have been part of the landscape here for hundreds of years.
"They really are magnificent to watch," said Sharon Schwab, one of the festival's organizers. "People go all over the world to see similar displays (by different wildlife species) and we have this right in our own backyard."
Reservations to stake out a spot in a blind are full but spots are still open for a $15 grassland birding bus tour that includes breakfast. The keynote speaker at the festival is Bill Volkert, a retired naturalist and wildlife educator for the state Department of Natural Resources at Horicon Marsh who has traveled the world to study birds. Since 2002, Volkert has worked with ornithologists and environmental educators to develop a national bird education plan for Nicaragua.
Other speakers during the day will make presentations about the early history of the Paul J. Olson Wildlife Area, named after the father of the Dane County Conservation League who helped generate interest and money in the 1970s to purchase land to preserve grassland habitat for prairie chickens in central Wisconsin. The organization owns about 4,000 acres of grassland within the Buena Vista Wildlife Area south of Stevens Point. Other talks will focus on the decline of butterflies and bees and another on reviving ecosystems with "resilient" agricultural practices.
Most festival events are based at the Sigel Town Hall in Wisconsin Rapids, but "Our Birds," a documentary from Wisconsin Public Television, will be shown at 10 a.m. and 2 p.m. at the Fine Arts Center in the McMillian Memorial Library in Wisconsin Rapids. From 11 a.m. to 4 p.m., vendors and exhibitors featuring nonprofits, conservation agencies and nature-based artists will be set up in Lester Hall at Camp Alexander in Wisconsin Rapids.
"I think we're making progress in terms of awareness and maybe appreciation, but these are birds that require a vast open landscape," Schwab said. "Where they succeed is where you have connectivity between open expanses."
The grasslands, scrapes and ponds of the region will provide some of the best viewing opportunities for birders. Species could include sandhill cranes, trumpeter swans, ducks, Canada geese, bobolinks, meadowlarks and snipes. There are short-eared owls, marsh hawks and a resident golden eagle. Last week, a snowy owl was spotted. But the leks will be the focal point for most.
"It's like their playground. That's where they strut," Dan O'Connell, part of a prairie chicken census team since 1999, said of the cocks that have colorful markings. "The birds kind of bounce around the landscape but they're always going to come back to the lek."
Prairie chickens are abundant in western Minnesota, Colorado, South Dakota, Kansas and Nebraska where there are established hunting seasons. But in Wisconsin, there is no hunting as the population is limited. But at one time the birds were prolific.
Logging opened up habitat for prairie chickens in northern Wisconsin and by the early 1900s the birds could be found in every county of the state. Hunting and the loss of habitat saw numbers plummet and in 1955 the state ended hunting for the birds. Research work by Fred and Frances Hamerstrom in the 1950s led to habitat management recommendations in central Wisconsin with the first parcel of land acquired on the Buena Vista Marsh in 1954. Today, about 15,000 acres, owned by the state, Wisconsin Society of Ornithology, Dane County Conservation League and other private land owners, are managed as grassland habitat for prairie chickens and other grassland species.
Landowners have worked to mow select spots to improve habitat and have left other areas to grow taller grasses in an effort to promote prairie chicken reproduction and nesting sites. A decline in farming has also meant fewer hay and alfalfa fields, desirable nesting sites for prairie chickens who can lay around a dozen eggs each spring.
"There's plenty of food for them to eat but food isn't the issue," said O'Connell, who works with the Portage County Land Conservation Department. "Without that cover (for nesting), the population goes down very quickly. We're trying to provide the habitat that the birds desire."
The Paul J. Olson Wildlife Area is home to between 150 and 200 prairie chickens and 12 to 18 leks. But the location of leks can change from year to year based on alterations to habitat. For example, this is the first time in three years a lek has been established on Flaig's farm. His blinds, which he rents out for $25 between March and May, are situated on prime grazing ground for his livestock.
Flaig, 59, was born and raised on the farm that now includes a solar-powered aquaponics farm in a former dairy barn. Run by his son, Holden, the aquaponics operation includes four, 200-gallon tanks of tilapia that provide nutrients for lettuce, kale, kohlrabi and Swiss chard growing on floating rafts under grow lights. Organic milk from the farm is sold to Organic Valley and Carl Flaig's grass-fed beef are also marketed.
And at this time of the year on his land, the prairie chickens dance.
"I'm farming for nature," Flaig said, as we rode in O'Connell's pickup truck during a tour of the wildlife area. "I'm creating an environment that is sustainable."
Barry Adams covers regional news for the Wisconsin State Journal. Send him ideas for On Wisconsin at 608-252-6148 or by email at badams@madison.com.
___
Information from: Wisconsin State Journal, http://www.madison.com/wsj


An AP Member Exchange shared by the Wisconsin State Journal.