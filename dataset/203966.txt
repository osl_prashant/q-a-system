When it comes to kids and fruit, size matters.
DJ Forry introduced California Summer Fruit Heroes - 2-pound bags of kid-sized stone fruit - at the beginning of the summer, and the Reedley, Calif.-based company's peach, plum and nectarine program is expected to continue into September.
"We see Heroes as an opportunity for families to be more efficient with their food dollars," vice president of marketing Ray England said.
"Many supermarket bulk displays are stocked with fruit that can weigh up to 9 ounces on average, and most children just aren't going to eat 9 ounces of fruit."
The average child couldn't eat a 9-ounce steak, England said, so why you offer them a 9-ounce piece of fruit?
"A package that offers a variety of fruit that weighs 4 ounces to 5 ounces on average, per piece, is a much better value when a child can eat the whole piece and eliminate waste," England said.
"That's why we think fruit that's sized right for kids is a natural."
England said feedback from retailers has been extremely positive.
"Any retailer will tell you that a significant percentage of shrink occurs at the cash register, which can make carrying multiple sizes of a specific commodity difficult from a scan standpoint unless one is packaged," said England, who added that DJ Forry offers retailers freestanding displays.
"We believe that retailers who carry a large piece of fruit in their bulk displays would benefit from carrying our Heroes package as it gives them the opportunity to promote peaches, plums and nectarines sized right for kids, which should appeal to a significant portion of their customer base. Plus they have the confidence they will get sales credit at the front end."
England said packaged, small-sized fruit is not only good for retailers and consumers, it also helps growers by providing a fresh market for those small sizes.
"Harvesting any size fruit is always based on market conditions, and if the return for the grower on any certain pack style or size is not positive, then why pack it?" he said.
"We hope to make picking and packing smaller fruit a worthwhile opportunity for growers."


Heroes

Meanwhile, Columbia Marketing International, Wenatchee, Wash., has its own Hero-branded program featuring kid-sized apples that has been around for several years.
"We continue to push our Hero program," said Steve Lutz, vice president of marketing.
"It's been a big winner with retailers that have strong programs focused on targeting moms and kids, and we will hit it hard again this fall."
How hard will depend to some extent on the availability of small sizes.
"The key piece is having the supplies of smaller-sized apples to accommodate the bagged apple program," Lutz said.
"At this point, we're uncertain on quantities until we get a little closer to the harvest. But we'll certainly have product available in the fall."
CMI's Heroes are available in 2-pound bags of gala, fuji, granny smith, red delicious and golden delicious.
The grower-shipper offers retailers a two-box shipper display along with supporting posters and other point-of-sale materials.