This John Deere LA tractor has been in Stanley Hula Jr.’s collection since the early 2010s after a friend bought it from underneath him.

After it was restored, Hula says the tractor is in better shape now than when it was brand new.

Watch his story on Tractor Tales on U.S. Farm Report.