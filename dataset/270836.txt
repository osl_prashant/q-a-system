California traceability and food safety consultant Michael McCartney, 69,  has died. 
He died April 8 in Mill Valley, Calif., according to a memorial website.
 
McCartney, managing principal of QLM Consulting since 1993, worked extensively in food safety, traceability and RFID technology.
 
Since January of 2015, he also has been responsible for business development for Salinas, Calif.-based Intellgistics, according to his LinkedIn profile.
 
McCartney also was chairman of the FreshPackMoves conference seminars for Cal Poly. The 2017 FreshPackMoves conference is set for May 22-24.
 
Prior to his consulting business, McCartney was president of ITEL, a Fortune 500 company specializing in global equipment leasing.
 
McCartney was a visionary for all segments of the fresh produce industry, said Koushik Saha, associate professor at San Luis Obispo-based California Polytechnic State University.
 
“He would enlighten people about factors that play a major role in growing the fresh produce business, from processing to value-added to packaging,” he said. “He had an understanding of the full system and that was very beneficial for academics like us because he could share his experience from so many years.” 
 
Bruce Peterson, president of Arkansas-based Peterson Insights Inc., said he had known McCartney for more than 20 years and worked with him on many projects.
 
“He made people around him better, and my own professional development and my own career was heavily influenced by Michael McCartney,” he said. 
 
McCartney had outstanding clarity of thought and was able to express very complex issues concisely and then act upon them, Peterson said.
 
“Michael was a doer and not a bystander — he was in the middle of it,” Peterson said. McCartney’s closing greeting on every e-mail was “all good things,” Peterson recalled, and that reflected his positive energy and personality. A memorial fund is available online.
 
McCartney is survived by his wife, Francesca McCartney, and two adult children, Zoe McCartney and Zena McCartney.