Grain mixed and livestock lower
Grain mixed and livestock lower

The Associated Press



Wheat for May fell 4.75 cents at 4.4625 a bushel; May corn was down 1.5 cent at 3.8725 a bushel; May oats gained 6.75 cents at $2.3175 a bushel; while May soybeans was dropped 9.25 cents at $10.3550 a bushel.
Beef and pork were lower on the Chicago Mercantile Exchange. April live cattle was off 1.35 cents at $1.1240 a pound; April feeder cattle fell 1.45 cent at 1.3187 a pound; while April lean hogs was off 2.98 cents at $.5427 a pound.