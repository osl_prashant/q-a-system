Dallas-based DMA Solutions has chosen nine students from Texas A&M University to attend the upcoming Viva Fresh Produce Expo in San Antonio.
The students, part of the Aggies for Fresh scholarship program supported by the Texas International Produce Association, were chosen by DMA president and CEO Dan’l Mackey Almy for their desire to pursue a career in the fresh produce industry, according to a news release.
“For the past two years, we have had the joy of mentoring and helping Aggies find their place in the fresh produce industry at the Viva Fresh Expo thanks to the support of TIPA,” Almy said in the release. 
“We believe that these students are the next generation of great minds who could really help shape the future of fresh — we’re hopeful to continue the success of placing students in this growing industry.”
This year’s scholarship winners:

Shelby Counter, bachelor’s in agribusiness;
Jacob Jordan, bachelor’s in agricultural leadership and development;
Maria Alejandra Ramirez Lopez, master’s in agribusiness;
Chase Mayfield, bachelor’s in horticulture;
Bridget Santana, bachelor’s in bioenvironmental science;
Jessica Schaffer, bachelor’s in agricultural leadership and development;
Monica Swei, bachelor’s in business administration;
Cheyenne Upton, bachelor’s in agricultural communications and journalism;
Claire Wilson, bachelor’s in agricultural communications and journalism.

All the winners are interested in internships or full-time positions in fresh produce, according to the release. 
Industry members can talk meet with the students on the show floor or during the Viva Fresh Aggies for Fresh reception, 4:30-5:30 p.m. on April 6, according to the release.