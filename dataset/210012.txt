Chicago Mercantile Exchange live cattle futures climbed to a two-week high on Monday, buoyed by technical buying and ideas that strong consumer demand for beef would continue, traders and analysts said.
Feeder cattle were narrowly lower and lean hog futures nearly unchanged, with little fresh news to alter the momentum for the three livestock futures contracts.
Front-month October live cattle gained for the third straight session, settling up 0.400 cent to 111.425 cents per pound. Most-active December cattle ended unchanged at 116.925 cents.
CME November feeder cattle eased 0.250 cent to 155.500 cents per pound.
"For the moment, we're following the trends from last week," said Steiner Consulting Group analyst Altin Kalo.
Beef packers were paying slightly higher prices for cattle to satisfy robust demand for the meat from retailers.
Average beef packer margins were $141.80 per cattle, up from $132.30 a week ago, according to HedgersEdge LLC. Margins per hog were $35.15 by comparison, down from $45.95 a week ago.
"The packers wouldn't have those margins if demand for beef wasn't there," Kalo said.
The U.S. Department of Agriculture said choice-grade wholesale beef prices were up 91 cents to $198.13 per cwt.
Wholesale pork rose 93 cents to $73.54 per cwt while cash hogs in the top market of Iowa and southern Minnesota climbed 42 cents to $54.47 per cwt.
CME October hogs settled 0.150 cent lower to 59.025 cents per pound and most-active December hogs up 0.025 cent to 60.950 cents.