Equine herpes virus confirmed in horse in South Dakota
Equine herpes virus confirmed in horse in South Dakota

The Associated Press

PIERRE, S.D.




PIERRE, S.D. (AP) — The South Dakota Animal Industry Board says equine herpes virus has been confirmed in a horse in Lincoln County.
The board says the horse became ill and had trouble with coordination and walking. Lab tests confirmed the disease.
According to the board, the horse has traveled extensively in South Dakota for cutting and sorting events in the past few weeks.
State veterinarian Dustin Oedekoven (OH'-dih-koh-ven) says the disease can result in respiratory problems, abortion or neurologic disease among horses. Since the disease is contagious and can be spread between horses through direct contact or through contaminated buckets, brushes or tack, Oedekoven recommends cleaning and disinfecting feed and water buckets, stalls and trailers to prevent the disease from spreading.
The virus is not a threat to humans.