AP-Deep South News Digest
AP-Deep South News Digest

The Associated Press



Good morning! Here's a look at how AP's news coverage is shaping up today in the Deep South. Questions about today's coverage plans are welcome and should be directed to:
The Atlanta AP Bureau at 404-522-8971 or apatlanta@ap.org
The Montgomery AP Bureau at 334-262-5947 or apalabama@ap.org
The New Orleans AP Bureau at 504-523-3931 or nrle@ap.org
The Jackson AP Bureau at 601-948-5897 or jkme@ap.org
Deep South Editor Jim Van Anglen can be reached at 1-800-821-3737 or jvananglen@ap.org. Administrative Correspondent Rebecca Santana can be reached at 504-523-3931 or rsantana@ap.org. A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
GEORGIA (All times Eastern):
TOP STORIES:
XGR-PAPER BALLOTS
ATLANTA — As Georgia lawmakers consider scrapping electronic voting machines for a system that uses paper ballots, a razor-thin margin in a U.S. House race over 500 miles away in Western Pennsylvania has highlighted a crucial distinction between the two systems: the presence of an auditable paper trail. The proposal would move Georgia from its 16-year-old electronic touchscreen voting system with no paper backup, to either a touchscreen system that prints a paper ballot or paper ballots marked by pencil. By Ben Nadler. SENT: 432 words, photo.
ZOO BAMBOO
ATLANTA — Georgians are growing bamboo in their backyards to help feed the pandas as Zoo Atlanta. The zoo's four pandas go through more than 200 pounds of bamboo every day. It's harvested from Georgia yards by the zoo's bamboo team. SENT: 350 words.
AP MEMBER EXCHANGE:
EXCHANGE-HOMELESS VETERAN
ROME, Ga. — David Miller was essentially homeless. In the summer of 2014 he made his way to the Salvation Army looking for a place to stay — a military veteran with no place to go. Today, David turns a key in a door to his own home — a home he worked hard to secure for his family. By Severo Avila. Rome News Tribune.
IN BRIEF:
— MOTORCYCLIST KILLED — Authorities say a Florida man crashed into four motorcycles stopped along a highway, killing one rider and injuring two others
— SMUGGLING ARREST — A 27-year-old Dallas woman is in federal custody on charges she tried to smuggle a 14-year-old boy into the U.S. from Mexico by using an illegal Georgia birth certificate
SPORTS:
BKW--NCAA-DUKE-GEORGIA
ATHENS, Ga. — After taking its first NCAA Tournament win since 2013, and the first for third-year coach Joni Taylor, fourth-seed Georgia will face fifth-seed Duke in the second round on Monday night. By Charles Odum. UPCOMING: 500 words, photos by 3 p.m.
ALABAMA (All Times Central)
TOP STORIES:
SUPPLY PLANT
WOODSTOCK, Ala. — A German auto supplier opened a $46.3 million plant in central Alabama. Al.com reports MöllerTech officials say the company will hire 222 employees at the new supply plant by the end of 2019. The ribbon-cutting ceremony was held this week almost 16 months after the company announced it would build the plant in Bibb County. SENT: 232 words.
AP MEMBER EXCHANGES:
EXCHANGE-ARTIST-BREAST CANCER
BIRMINGHAM, Ala. — This article is part of the Black Magic Project, which is a series of stories focusing on those who do inspiring things in the black community. This year, we created a Facebook group where If you're a Facebook user, you can join our Black Magic Project group, where we will not only share stories from our website, but we will also hold live video discussions and talk to people who are contributing to their communities in big and small ways. Yolonda Carter finds sanctuary by painting poetry nearly every day. By Jonece Starr Dunigan. Al.com.
EXCHANGE-WILD HOG PROBLEMS
MONTGOMERY, Ala. — They are four-footed eating, breeding, rooting machines. Feral hogs are an invasive species present in at least 35 states, according to the United States Department of Agriculture. They cause billions of dollars in crop damage each year, and their rooting causes widespread and lasting environmental damage, according to the USDA. By Marty Roney. Montgomery Advertiser.
IN BRIEF:
— HISTORIC SITES-CLEANUP — An organization that preserves Civil War battlefields is looking for volunteers to clean up historical sites in Alabama.
— WALKING TOURS — Alabama's tourism agency is promoting a series of walking tours that will start next month in cities both large and small.
SPORTS:
BKC--NCAA-CLEMSON-AUBURN. Game starts about 7:10 p.m. ET in San Diego
BKC--NCAA-OUSTED FRESHMAN
The first two rounds of the NCAA Tournament haven't been kind to many of the freshmen regarded as potential NBA lottery picks. Arizona's Deandre Ayton, Missouri's Michael Porter Jr., Alabama's Collin Sexton, Oklahoma's Trae Young and Texas' Mohamed Bamba all have been knocked out already. By Steve Megargee. UPCOMING: 500 words, with photos, expected by 4 p.m.
LOUISIANA (All Times Central)
TOP STORIES:
LOUISIANA SPOTLIGHT-ANALYSIS
BATON ROUGE — Gov. John Bel Edwards has immersed himself in the controversies of Louisiana's financial woes, but he's noticeably avoided two of the other biggest fights shaping up in the regular legislative session, over guns and gambling. The Democratic governor says he's studying the issues and will weigh in eventually. He'll likely get dragged into the dispute whether he wants to be there or not. An AP News Analysis. Melinda Deslatte. UPCOMING: 700 words.
AP MEMBER EXCHANGES:
EXCHANGE-BIONIC HAND
MANDEVILLE, La. — Maintaining a high-and-tight military hairstyle, even after leaving the Army, Perry Pezzarossi has always sported a pretty distinctive look. These days, though, it's his hand and forearm that's drawing the attention. By Robert Rhoden. NOLA/The Times-Picayune.
EXCHANGE-WOMEN'S DAY
DERIDDER, La. — March is Women's History Month and the eighth is globally recognized as International Women's Day. The roots of International Women's Day trace back to a Women's Day Celebration on Feb. 28, 1909 in New York City. The event was organized by the Socialist Party of America and first suggested by Theresa Malkiel. It is not only considered by many to be the precursor to International Women's Day, but also a key point in the women's movement.By Micah Pickering. The Beauregard Daily News.
IN BRIEF:
— EARLY CHILDHOOD GRANTS — Louisiana's education department has divvied up a $1.5 million federal grant aimed at improving early childhood education to eight parishes.
SPORTS:
BKN--CELTICS-PELICANS
NEW ORLEANS — The Boston Celtics visit the increasingly desperate New Orleans Pelicans on Sunday evening. While Boston has weathered recent injuries and remains firmly near the top of the Eastern Conference, the Pelicans have dropped four of five and are in danger of falling out of the Western Conference playoff picture. By Brett Martel. UPCOMING: 700 words, photos. Tip-off 6 p.m. ET
MISSISSIPPI (All Times Central)
TOP STORIES:
UNDER THE CAPITOL DOME-ANALYSIS
JACKSON, Miss. — Mississippi legislators are approaching the final phase of writing a $6 billion budget for the year that begins July 1. Among the many items they will consider is the Department of Child Protection Services' request for millions more dollars. The agency is trying to fulfill a court order to improve the state's long-troubled foster care system. By Emily Wagster Pettus. UPCOMING: 600 words.
AP MEMBER EXCHANGES:
EXCHANGE-HISTORIC BLACK BUSINESS
VICKSBURG, Miss. — Mississippi's oldest black-owned business and oldest registered black funeral home turned 123 years old in December. William H. and Lucy C. Jefferson founded W.H. Jefferson Funeral Home in 1894, and it has remained in family hands since. By John Surratt. The Vicksburg Post.
EXCHANGE-HISTORIC HATTIESBURG HOUSE
HATTIESBURG, Miss. — Catherine Strange knew the moment she saw the big, white house at Sixth and Walnut streets in downtown Hattiesburg — it had to be hers one day. It was nearly 30 years ago that she spied the dwelling — a home that hadn't made it on her real estate agent's list of showings. Catherine Strange's husband, Randall, was still working at his job in Houston, and she had been living on Corinne Street with her 18-month-old son. She couldn't stop thinking about the 4,000-square-foot, one-story house — nearly a century old and vacant for 10 years. By Ellen Ciurczak. The Hattiesburg American.
IN BRIEF:
— CORRECTIONAL OFFICER ARRESTED — Prison officials in Mississippi say a correctional officer has been arrested after synthetic marijuana in her car parked at the facility
— INMATES CAPTURED — Authorities say two inmates have been captured after escaping a correctional facility in Mississippi this week
SPORTS:
BKW--NCAA-OKLANHOMA ST-MISSISSIPPI ST
STARKVILLE, Miss. — Top-seeded Mississippi State is well aware that No. 9 Oklahoma State is capable of pulling off a huge upset when the two teams play in the second round of the women's NCAA Tournament on Monday. The Cowgirls pushed the Bulldogs to the brink earlier this year before Mississippi State escaped with a 79-76 win. By David Brandt. UPCOMING: 550 words from early afternoon press conferences.
___
If you have stories of regional or statewide interest, please email them to
The Atlanta AP Bureau: apatlanta@ap.org
The Montgomery AP Bureau: apalabama@ap.org
The New Orleans AP Bureau: nrle@ap.org
The Jackson AP Bureau: jkme@ap.org
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.