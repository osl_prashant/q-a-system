California grower Fillmore-Piru Citrus Association has selected Linkfresh Inc.’s supply chain enterprise resource planning software to manage its operations.
 
Based in Piru, Calif., Fillmore-Piru Citrus Association has been packing, distributing and marketing oranges and lemons since 1897 under brands such as Belle of Piru and Mansion, according to a news release.
 
Fillmore-Piru Citrus Association will deploy Linkfresh software across its operations, providing a single, fully integrated solution for forecasting and planning, finance, purchasing, sales, grading and production, the release said.