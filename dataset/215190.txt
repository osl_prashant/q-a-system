At the ARA Conference and Expo, AGCO recognized its four finalists for Operator of the Year and named the recipient of the 12th annual award.
The four finalists are:

Russ Krans, Head Operator at West Central FS in Wataga, Illinois
John Manning, Operator and Shop Manager at CHS Elburn in Newark, Illinois
Andrew Myburgh, Certified Applicator at the Wheat Growers Co-Op in Aberdeen, South Dakota
Terry Wilson, Rig Operator at Ferti-Tex in Comanche, Texas

 
“These are outstanding individuals,” says Mark Moore, Mark Mohr, tactical marketing manager at AGCO Corporation. “The purpose of this award is to provide retailers a way to formally recognize their outstanding operators and applicators.”
The 3 criterion for the award are customer service, leadership within their organization and contribution to the community.
The 2017 AGCO Operator of the Year is Andrew Myburgh, with 1o years of service at Wheat Growers Co-op in Aberdeen, South Dakota. Myburgh annually applies 41,000 acres/year. His application noted a trifecta of characteristics in his application: professionalism, precision and speed.
In receiving the award, Myburgh says, “our job is made much easier with the type of equipment we are running today. Thank you AGCO.”
Originally from South Africa, Myburgh is a permanent resident of the U.S. and lives on a small farm where his two sons help care for 60 head of sheep, calves, a donkey, ducks and chickens. He also coaches the local soccer team, and often mentors and trains other applicators who want to learn the trade.
The grand prize is a new Harley-Davidson motorcycle. The nominator of the finalist that is chosen will also receive a brand-new Yeti cooler.
The four finalists are guests of AGCO at the 2017 Agricultural Retailers Association (ARA) Conference & Expo, Nov. 28-30 at the Arizona Biltmore in Phoenix.