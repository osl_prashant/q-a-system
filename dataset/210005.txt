As the industry searches for the right solution for in-season dicamba some states are turning back to the users and applicators to find what they think is happening.
In Illinois 124 retailers responded to a lengthy survey from the Illinois Fertilizer & Chemical Association about this year’s dicamba applications.

55% said date of application appeared to have an impact on symptoms shown in non dicamba-tolerant soybeans
85% said they saw symptoms in adjacent fields of non dicamba-tolerant soybeans even when the wind was not blowing toward that field during application
Find more survey results here 

In Tennessee 23 retailers and 24 extension agents shared their views on the estimated 386,000 acres of off target dicamba damage in the state. The group was asked what percent of damage was caused by certain types of off target movement.

Contaminated sprayer equipment: Extension 2%, retail 5%
Misapplication: Extension 22%, retail 19%
Volatility: Extension 67%, retail 21%
Temperature inversions: Extension 59%, retail 35%
Illegal dicamba formulations: Extension 4%, retail 20%

Larry Steckel, University of Tennessee Extension weed scientist also combined his experiences with survey results to highlight what he calls the “five big issues” of dicamba.

Tank contamination 
Following the label
Avoid inversions 
Old formulations 
Volatility in new formations

Research will continue on the best solution for dicamba. In the meantime stay up to date with dicamba news at AgWeb.