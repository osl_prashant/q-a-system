The latest U.S. Drought Monitor remains relatively unchanged in areas like Kansas and Texas that are currently experiencing D3 or extreme drought, or parts of northwestern Oklahoma in the D4 or exceptional drought stage.

Roughly half of the country is experiencing some level of drought or dryness.

Some parts of Texas and Oklahoma haven’t seen precipitation in more than 100 days, setting an all-time record in some places.

As the winter wheat crop starts to break dormancy, fields are dry, forcing some farmers to make difficult decisions on what to do with the crop. Cattle are moving off pastures early, and some hay is being fed to supplement their diets.

Farmers wanting to plant dryland cotton who had plans to increase acreage are now waiting to make any decisions until rain falls before planting.

Scott Irlbeck, a farmer from Swisher County, Texas is one farmer dealing with the dry weather. Hear his concerns on the weather on AgDay above.