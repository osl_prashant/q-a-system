Grain mostly higher and livestock higher
Grain mostly higher and livestock higher

The Associated Press



Wheat for May rose 7.50 cents at 4.7225 a bushel; May corn was off 1 cent at 3.8850 a bushel; May oats was up .50 cent at $2.3325 a bushel; while May soybeans gained 2.50 cents at $10.3375 a bushel.
Beef and pork were lower on the Chicago Mercantile Exchange. April live cattle was off 2.08 cents at $1.1222 a pound; April feeder cattle fell 2.15 cents at 1.3532 a pound; while April lean hogs was lost .33 cents at $.5212 a pound.