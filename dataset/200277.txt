Whether you buy your chemicals and dip in 15-gallon drums, or 55-gallon drums, dairy farmers across the country deal with the same problem.
Photo by Lorilee Schultz

Look familiar? Unfortunately, your inspector likely doesn't like the stash your saving at the dairy. Here are 15 ways you can use those empty drums, courtesy of the Dairy Girl Network.
1. Christmas Light Storage
Cut the top off and coil up your Christmas lights for tidy storage when they're not hanging on your house.
2. Trash Can or Trash Bin
Cut the top off the barrel and there you have it. You can get fancy and cut handles in the sides to make it easier to pack around like the photo below.

Photo by Lauren Sizen

3. Mineral Feeders
Cut the drum in half across the middle. You can either put salt or mineral in them along the feedbunk for cows, or you can hang them in the calf barn for calves to access.
4. Water Troughs
Cut the drum in half across the middle. Fill side without holes with water for 4-H animals or heifer pens.

Photo by Taylor CM

5. Container Garden
Cut the drum in half across the middle. Fill with soil and plant whatever you'd like in them. This tip would be great for sprucing up your barn or farm entrance.
6. Recycling Bins
Cut a hole in the top, leaving the handle intact. Use multiple barrels to sort cans, bottles and other recyclables.
7. Yard Tool Storage
Cut off the tops to use for organized storage of things like rakes, shovels and hoes.
8. Calf Sled
Cut in half lengthwise and tie a strong piece of rope to one end. Use to sled calves from maternity pens to calf raising facilities.
9. Feeders
Cut in half lengthwise and use for feeding grain to 4-H animals.

Photo by Amanda Rottigen

10. Boat or Raft
Put the cap back in the empty drum, seal it up and use to float on the lake!

Photo by Gail Carpenter

11. Saddle Rack
Mount on wooden legs and use to store saddles.
12. Garden Protectors
Cut off both ends and then cut in half. Pace over tomato, pepper plants or any other plants that need extra insulation in the spring.

Photo by Jennifer Zumbach

13. Concrete Forms
Cut of both ends and slice individual circles. Pour concrete in the center to make stepping stones or to set fence posts.
14. Rain Water Catcher
Cut off the top and set outside to catch rain for watering plants.
15. Recycle Them
Many chemical companies will take empty drums back for you if you ask them to. If not, seek a recycling center in your area.