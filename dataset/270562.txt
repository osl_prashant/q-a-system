The farm economy is facing tough times, and a new bipartisan effort is recognizing these struggles.

Senators Tammy Baldwin (D-Wis.) and Joni Ernst (R-Iowa) have introduced legislation to provide farmers with resources and to respond to difficult times.

The Facilitating Accessible Resources for Mental health and Encouraging Rural Solutions For immediate Response to Stressful Times (FARMERS FIRST) Act is seeking $50 million to provide the ag community with mental health support and resources, such as helplines, suicide prevention training, and the creation of support groups.

According to a report from the Centers for Disease Control and Prevention (CDC), agricultural workers have a higher suicide rate than other occupations.

“Farmers are the backbone of our rural economy and leaders in our rural communities,” said Sen. Baldwin. “The FARMERS FIRST Act will make sure that when there is a crisis on the farm, farmers know they are not alone and there are resources available to help them find a path through tough times.”

The FARMERS FIRST Act would provide funding through the USDA to state and non-profit organizations and would reestablish the Farm and Ranch Stress Assistance Network (FRSAN).
This bill has been endorsed by the Wisconsin Farmers Union, National Milk Producers Federation, National Corn Growers Association, National Farmers Union, National Family Farm Coalition, National Farm Medicine Center, Farm Aid, Female Farmer Project, National Rural Health Association, American Soybean Association, National Association of State Departments of Agriculture, Rural and Agricultural Council of America, and U.S. Cattlemen’s Association.