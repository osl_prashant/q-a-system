Business groups in the new Reason for Reform coalition made common cause to pursue immigration policy changes when a new Congress convenes in 2017, but there are differences of emphasis.
 
The United Fresh Produce Association wants to see an overhaul of immigration law beneficial to grower-shippers, but aims lower for the time being. It's focused on fixes to the H-2A labor visa program.
 
"Comprehensive immigration reform is still our ultimate goal, but it is apparent that for now H-2A is the only program that growers can use to find a viable and legal workforce, so it is essential that it be improved to meet growers' needs," United Fresh said in a statement supporting the campaign. 
 
Use of H-2A is up 50% since 2012 and is expected to increase 20% annually, according to the trade group.
 
Zippy Duvall, president of the American Farm Bureau Federation, recited a familiar litany of frustrations with the H-2A program in an Aug. 3 conference call introducing Reason for Reform. Administrative delays, application denials and high costs get growers steamed.
 
"We need a new, more flexible visa program," Duvall said. "This kind of reform will take time. Meanwhile, we need to make sure our current workforce stays."
 
"We need adjustment of status," he said.
 
Duvall aims high. I assume he would not turn down beneficial tweaks to H-2A. But the comments are revealing, however you weigh chances for comprehensive reform.
 
They show that workforce retention is the top priority for many in the industry.
 
You can see why. Growers are offering higher wages. Further attrition would increase that cost. 
 
And for all of the election season talk about Mexican immigration, agriculture sees no influx of labor. Harvesters are aging; so are the truckers who haul product.
 
"Farmworkers are going back to their home countries, especially Mexico, where there are now new opportunities," Western Growers Association president Tom Nassif said.
 
Unless the labor market changes, agriculture will bide its time until more automation arrives. It's a precarious situation. 
 
So what happens when an unsustainable labor pool meets with unsustainable political gridlock? Who will blink first? 
 
We'll find out in the next Congress.
 
Mike Hornick is The Packer's California-based staff writer. E-mail him at mhornick@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.