Trump draws ire of farmers targeted in Chinese trade dispute
Trump draws ire of farmers targeted in Chinese trade dispute

By DAVID PITT and STEVE KARNOWSKIAssociated Press
The Associated Press

DES MOINES, Iowa




DES MOINES, Iowa (AP) — From Iowa hog producers to Washington apple growers and California winemakers, farmers are expressing deep disappointment over being put in the middle of a potential trade war with China.
President Donald Trump announced plans Thursday for tariffs on products including Chinese steel to punish Beijing for stealing American technology.
Beijing responded Friday with a threat to slap tariffs on American products such as pork, wine, apples, ethanol and stainless-steel pipe.
Farmers voted overwhelmingly for Trump in 2016. But now many worry about the economic blowback from his combative approach.
Iowa farmer Wayne Humphreys says producers have invested a lot of time, talent and treasure in developing markets worldwide. And with the stroke of a pen, he says, that investment has been jeopardized.