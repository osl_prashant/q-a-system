Planting season is in full swing, and corn is going in the ground. As corn seedlings poke through the soil, producers will want to protecting them from pests that might cut their life short, particularly black cutworms."Black cutworms (BCW) can cause a reduction of plant population," says Michael Gray, professor and assistant dean of the Illinois College of Agriculture and Natural Resources Extension. A single BCW larva is capable of cutting about four corn plants in its lifetime‚Äîdepending on the size of the plants.
How Black Cutworms Attack Corn
As their name suggests, cutworm species chew through corn seedlings, typically targeting plants that have between one and four leaves. BCW attack corn plants early in the season and can result in substantial stand, and ultimately, yield loss. Now, as corn is peeking through the soil it is primed for exposure to damage.
"Don't assume 'Iplanted a Bt hybrid‚ÄîI don't need to worry about this pest,'" suggests Gray. "Not all Bt hybrids offer the same level of protection. If it just offers suppression, scout more diligently."
Corn is most susceptible when it's 15" or shorter, but can still suffer damage in later stages. The first sign may be pinholes in leaves,because when BCW are young, they do not cut the plant at the base. However, as the black cutwormsgrow, they will be able to cut plants completely.
Plants may be able to recover from BCW cuttings if the growing point is below ground. If the growing point is cut, there will likely be a total plant loss. As corn reaches later stages, BCW can tunnel into the plant and hollow out the stalk through the growing point.
When scouting, be sure to check low spots in fields. Take special caution when chickweed, shepherd's purse, peppergrass and mustard weeds are present;BCW favor these weeds for layingeggs.
How to Protect Your Fields
Plant to scout all fields at least once per week for three to four weeks after corn emergence. In each field check at least five locations and 50 plants. Keep a tally of how many plants have leaf feeding (pinholes), cutting, wilting or missing plants. If a plant has damage, dig around the plant to try to find the BCW larvae. They burrow the ground to moisture‚Äîthis could be 3" to 4" in dry areas. Be sure to give special attention to late planted or weedy fields. Illinois State University research suggests rescue insecticide treatment if a field has at least 3% damage and you find larvae present.
How can you make sure you are scouting effectively? Do it when your fields are at greatest risk. Purdue offers a handy chart (below) for guidance.

Chart courtesy of Purdue University
Farmers will want to determine which of their fields might be at risk. Here are some key pieces of information to use when evaluating farmland.
BCW overwinter in the Gulf states and migrate to Northern states in March and April.
An average BCW cuts 3-4 corn plants.
Eggs can be laid single or multiple at a time, and they hatch in 5-10 days.
There are 3 to4 generations of BCW per year.
Moths prefer to lay eggs in fields with established weeds or trash.
BCW are more likely in a corn after soybeans year or after winter wheat.
Fields with reduced tillage, late tillage and planting, no-till, next to permanent vegetation, weed infestation or with soybean residue are at higher risk of BCW infestation.
Give corn a strong start this year by keepingBCW from cuttingyour yield‚Äîand profits‚Äîshort.