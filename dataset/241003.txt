Perdue Foods fined for pollution violations in Delaware
Perdue Foods fined for pollution violations in Delaware

The Associated Press

DOVER, Del.




DOVER, Del. (AP) — State environmental officials have fined Perdue Foods for multiple wastewater violations at a southern Delaware poultry processing plant.
Officials said Friday that Perdue faces an administrative penalty of $77,300 and a $7,601 assessment for expenses associated with the investigation into the pollution at the company's Georgetown processing plant.
Officials say that from May to July of 2015, Perdue repeatedly exceeded the effluent limits in its pollutant discharge permit, including restrictions on the concentrations and loads of ammonia, total nitrogen and enterococcus bacteria, resulting in excess volumes of pollutants in surface waters.
To offset a portion of the penalty, Perdue has agreed work with The Nature Conservancy to convert 39 acres of farmland into forest, which will significantly reduce the amount of nitrogen and phosphorus going into the Broadkill River.