U.S. pork producers receive a positive return on their Checkoff investment, according to a 2017 study conducted and released by Harry Kaiser, the Gellert Family Professor in the Dyson School of Applied Economics and Management, Cornell University.

Additionally, 91 percent of pig farmers who took part in the annual producer survey in November acknowledge their overwhelming support of the Pork Checkoff, with a record-low opposition of just 3 percent.

Return on Investment Study Highlights
An economic analysis of Pork Checkoff programs is commissioned every five years by the National Pork Board. The study quantifies the returns generated by Pork Checkoff investments in research, pork promotion and producer education programs. The latest results, published in 2017, cover 2011 to 2016 programs.

“It’s important to producers – those who directly fund the Pork Checkoff – to understand and quantify the value of their investments,” said Terry O’Neel, National Pork Board president and a pig farmer from Friend, Nebraska. “The results indicate a positive impact of all aspects of the Pork Checkoff, from conducting production-focused research to growing consumer and export demand for pork.”

Specifically, the study documented a growing return on investment through defined benefit-cost ratios across several key program areas from 2011 to 2016: 

Production Research: Each dollar invested in production research to benefit on-farm practices yielded $83.30 in producer value.
Foreign Market Development: Each dollar invested in developing foreign markets yielded $24.70 in producer benefits.
Advertising and Non-advertising Promotion: Other pork promotion resulted in benefits of $14.20 for advertising and $12.40 for non-advertising promotion.
Research to Grow Demand: Research on market drivers returned $8.30 for each $1 invested.
Net Result: Collectively, the overall return of Checkoff program activities is $25.50 for each dollar invested.

The U.S. Department of Agriculture requires a return on investment analysis every five years. The 2001 to 2006 study showed an overall return of $13.80 to $1 invested, and the most previous study, released in 2012 for the time period of 2006 to 2011, found a return of $17.40 to $1 invested.

“This analysis provides a comprehensive review of program development, and more importantly, efficiency of our Checkoff program administration,” O’Neel said. “The net return of 25 to 1 on Checkoff investments demonstrates that we are meeting producer needs in the areas that drive sustainable production and grow consumer demand.”

Annual Producer Survey Results
The Pork Checkoff also reports findings from a study that gauges producer support of the Pork Checkoff. Since 2002, the National Pork Board surveys producers annually to gain insight about the condition of the industry, general attitudes on pig farming and their support of the Checkoff. The most recent survey of 550 pork producers, conducted Nov. 6-16, 2017, showed that for the eighth consecutive year, pig farmer support for the Checkoff has improved.

Support for the Checkoff remained at 91 percent, while opposition declined to a record low of 3 percent, which is down from 4 percent in 2016.

Other Highlights: 

Right direction/wrong track: Producers grew in their industry optimism despite market supply pressure and other issues they face. In 2017, 78 percent of producers said that the industry was heading “in the right direction,” up from 76 percent in 2016. Only 12 percent said that the industry was “on the wrong track,” an improvement from a 2016 score of 19 percent.
Support for the Pork Checkoff and general optimism of the industry was strongest among larger producers, or those that marketed more than 80,000 pigs in 2017. Support from this group was 95 percent.
The No. 1 challenge facing producers was “managing hog health and disease,” which was a change from 2016’s No. 1 concern of “too many rules/regulations.”

Regarding awareness and support of the strategic plan, the primary Pork Checkoff goals resonated with the random sample of producers surveyed. On a 10-point scale: 

Build Consumer Trust rated a mean score of 8.95, up from 8.91 in 2016.
Grow Export Demand rated a mean score of 8.69. This was a new category this year since “Grow Consumer Demand” was broken into two elements – export and domestic demand.
Grow Domestic Demand rated a mean score of 8.64, down from 8.70 in 2016.
Drive Sustainable Production rated a mean score of 8.28, up from 8.18 in 2016.

“These are the most positive results we have seen since we began the producer survey 15 years ago,” O’Neel said. “The findings underscore the value that the Pork Checkoff team delivers day-in, day-out to the pig farmers who fund the Checkoff. The results demonstrate that the board’s strategic goals are aligned with producers’ interest.”