Twenty-one young Iowa cattle producers from around the state participated in the 2016 Young Cattlemen's Leadership Program (YCLP). The Iowa Cattlemen's Association program consists of a series of educational sessions designed to develop leadership qualities in young cattle farmers. The group met four times, focusing on leadership strategies, policy development, production practices and legislative advocacy.The YCLP class also helps develop and employ the Iowa Cattlemen's Association's Carcass Challenge. We commend this year's YCLP class for their recruitment efforts. Earlier this fall, 80 head of feeder steers were delivered to Kennedy Cattle Company. The proceeds from the Carcass Challenge will be used to fund educational programming, leadership development, and advocacy training for cattle producers.
2016 Young Cattlemen's Leadership Program graduates include:
Ashlee Bell, Scott County
Scott Cherne, Clayton County
Darrin Crawley, Guthrie County
Will Longinaker, Fremont County
Ron Lyon, Jr, Madison County
Justin Mendenhall, Decatur County
Drew Van Laar, Madison County
Skyler Rinker, Boone County
Nick Andresen, Ida County
Dustin Balsley, Mitchell County
Melinda Brahms, Cass County
Ryan Carlson, Boone County
Kellin Chambers, Pottawattamie County
Aarik Deering, Allamakee County
Tanner Deering, Allamakee County
Dillon Green, Sioux County
Kellie Gregorich, Jackson County
Clayton Hester, Mahaska County
Dane Kuper, Mitchell County
Haley Pagel, Fayette County
The 2017 YCLP class has also been named, and will meet for the first time in January. Participants in 2017 include:
Darren Arthur, Cerro Gordo County
Darrin Axline, Cedar County
Patrick Bries, Dubuque County
Holly Bries, Dubuque County
Allison Brown, Wayne County
David Bruene, Story County
Molly Bruene, Story County
Wesley Christensen, Clay County
Adam Darrington, Pottawattamie County
Jake Driver, Pottawattamie County
Dawn Edler, Benton County
Ryan Healy, Marion County
Travis Hosteng, Story County
Delaney Howell, Louisa County
Grant Klopfenstein, Henry County
Tyler Krug, Benton County
Matt Lansing, Dubuque County
Clara Lauritsen, Audubon County
Andrew Lauver, Calhoun County
Tanner Lawton, Greene County
Jacob Louth, Jefferson County
Grant Rathje, Douglas County
Peyton River, Jackson County
Leslie Ruby, Clarke County
Patrick Ryherd, Marshall County
Shaniel Smith, Appanoose County
Brian Tuttle, O'Brien County
Drew Weyers, Marion County