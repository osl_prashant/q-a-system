There will be snow in the South this weekend.A winter storm is expected to sweep through the Southeastern US late Friday and early Saturday, bringing a potential mixture of snow, sleet and ice.

Raleigh, N. C., is likely to face the brunt of the storm, with 4 to 10 inches of snow predicted. Charlotte is also looking at some 3-4 inches of snow, and CNN's meteorologists believe at least an inch of snow is likely to fall in Metro Atlanta, with potential for up to 4 inches.


The National Weather Service has issued winter storm watches for Alabama, Georgia and the Carolinas -- including such metropolitan areas as Birmingham, Atlanta, Greenville, Charlotte and Raleigh. A winter storm watch means there is potential for significant snow, sleet or ice accumulations that may impact travel.

Lighter accumulations of snow are also forecast for portions of Mississippi, Tennessee and Virginia.