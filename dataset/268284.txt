Wastewater company to pay state $1.6 million to settle spill
Wastewater company to pay state $1.6 million to settle spill

By SARAH BETANCOURTAssociated Press
The Associated Press

PLYMOUTH, Mass.




PLYMOUTH, Mass. (AP) — A wastewater treatment firm agreed Tuesday to pay $1.6 million to settle a lawsuit with Massachusetts for a spill in which more than 10 million gallons of raw sewage flowed into state-owned woodlands in Plymouth and Plymouth Harbor.
The settlement by Veolia Water North America Northeast is believed to be the largest ever paid for violations of the state's Clean Waters Act, officials said.
Attorney General Maura Healey said the company failed to properly maintain a piping system that carried wastewater from customers to the treatment facility in Plymouth, causing a spill from December 2015 to January 2016.
Veolia also allegedly discharged hundreds of thousands of gallons of untreated wastewater into Plymouth Harbor in three separate incidences in 2012.
The contamination caused shellfish beds to temporarily close.
Company spokesman John Lamontagne said the discharges were the "extremely unfortunate result" of a poorly designed 4.5-mile (7 kilometer) sewage main that stretched from Plymouth Harbor to the plant. The company did not admit liability for the spill.
Veolia alleged that the Town of Plymouth chose the design for the pipes and "ignored warnings" from the company about the design.
The company will pay a $1.35 million civil penalty, along with $250,000 to an environmental trust to fund projects to improve the harbor's ecosystem.
Veolia continues to operate the Plymouth wastewater plant.
Plymouth has a separate suit against Veolia North America that contends the company also is responsible for a 2015 sewage spill that officials claim impacted the town. The Attorney General's office also has a separate lawsuit against Plymouth, filed in 2016.