The following commentary does not necessarily reflect the views of AgWeb or Farm Journal. The opinions expressed below are the author's own.
One of the biggest challenges for farmers today is finding and keeping good help. Farms need sophisticated, engaged, dedicated and hard-working employees. Not to mention, these job opportunities typically require candidates to live in rather remote corners of rural America.
No, recruiting and retaining top talent is not easy. Yet, some farm operations boast employees with decades of service. How do they do it?
In a recent blog post, (You Might Be Struggling to Find and Keep Good People If...) Richard Hadden, a leadership consultant at Contented Cow Partners headquartered in Jacksonville, Fla., offered some insights. Do any of these apply to you?

You wish all your employees were Baby Boomers. “This just in: They're not making any more Baby Boomers!” Hadden says. “While more experienced workers are a tremendous (and too often overlooked) resource, the truth is you're going to need to stop whining about Millennials (and the generations yet to come) and figure out how to create an organization that gets the most productivity from the available resources.”
Your job description is focuses only on technical skills. Don’t get hung up on specific experience and skills, Hadden advises. Instead, ask the question: Is this person a good match for our organization and this team? “Be honest,” he says. “How many times have you hired someone for skills and experience, but then they left (voluntarily or otherwise) because they simply weren't a good fit?”
“Thank you” and “good job” are rare in your vocabulary. Don’t miss opportunities to tell the people you work with how much you appreciate them for what they do, Hadden coaches.
People just don’t want to work for you. Have you confused high standards with being obnoxious? “Find someone you trust—someone who has the courage and character to tell you what you need to know, but may not want to hear,” Hadden says.

Read five other reasons you could be scaring away good help: You Might Be Struggling to Find and Keep Good People If...
Hadden spoke at the 2017 Top Producer Seminar. He shared many great insights, but one has really stuck with me:
"Your best recruiting tool is your reputation as an employer. But it is the reality of your workplace that will retain and engage people for the long-term. Your reputation recruits and reality retains."
Act now to build that reputation of an employer of choice. Small (and inexpensive) actions go a long ways.
We’ve published some other great advice from Hadden:
4 Hiring Mistakes to Avoid
How One New Mexico Dairy Implemented A Strict Hiring Process