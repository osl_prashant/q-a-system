Weaning is a stressful time for calves. This stress can contribute to reduced feed and water intake, which results in poor nutrition. The added stress of weaning can also depress the immune system, leading to greater risk for sickness and death. Any opportunity to reduce the stress of weaning has the opportunity to contribute to improved health and performance of calves, both during and after the weaning period.Typical weaning involving the abrupt removal of the calf from the cow will create a highly stressful situation. Any weaning process that makes the separation more gradual will reduce the stress and therefore potentially improve health and performance of the calf. Examples of low-stress weaning methods include fenceline and two-step weaning.
Fenceline Weaning
Fenceline weaning methods are most frequently used in weaning on pasture as a means to decrease stress on the calves, potentially resulting in increased performance and immunity compared to abruptly weaned counterparts. Research in Utah indicated that fenceline weaned calves vocalized less, spent more time eating, and had greater weight gains compared to calves that were abruptly and completely separated from their dams. The increased weight gain was maintained through a 10-week timeframe post-weaning. Additional research at Michigan supported the increased performance for the first 14 days and lower serum haptoglobin levels at day 5, however this performance did not continue through the study and there were no sustained performance differences based on weaning method. Haptoglobin is an indicator of stress, and is often present in the blood following stress. Thus, lower serum haptoglobin in fenceline weaned calves indicated they were less stressed after weaning. An additional study in Michigan evaluated calf behavior and found that the fenceline and two-step weaning methods appear to be less stressful on calves compared to abrupt weaning methods.
For fenceline weaning to be effective there are steps that need to be taken.
Place pairs in the pasture that the calves will be in following weaning so they become familiar with the fences and water in the pasture.
Upon weaning, place the cows in the pasture adjacent to the calves so they can see, hear and smell each other, but cannot nurse.
This may require some modifications to fences to ensure the cows and calves remain separated.
There are multiple options to fencing, which could be as simple or complex as desired.
A 5 strand barbed wire fence
A 5 strand electric if they have not been acclimated to electric fence prior to weaning.
A barbed wire fence with a single offset electric wire to ensure calves cannot reach through and nurse
Other options can work, but separation must be maintained


It may be valuable to place a cull cow or yearling with the calves to decrease the amount they walk fences.
After a few days the cows and calves will move farther from the fence and not be as concerned about being weaned.
Two-Step Weaning
Two-step weaning is another option for decreasing stress on the calves. The two-step process utilizes plastic nose tags that prevent the calves from nursing while they are in the same pasture as their dams. The calves are still able to eat forage and drink water.
The two steps to this process are:
Place plastic nose tags in all calves for 4 to 7 days. It is important to ensure they are placed correctly to minimize loss through falling out and calves figuring out how to nurse with them in.
After the 4 to 7-day period, remove the plastic nose tag and move the cows to a remote location.
Research from Canada shows that calves with nose tags do not bawl or walk any more than calves without nose tags. They also spend as much time eating each day during the time the nose tags are in. When stage 2 starts (separation from dams), there is no increase in bawling or walking compared to stage 1 or prior to inserting the nose tags. Meanwhile, contemporaries that were abruptly weaned in the Canadian studies displayed dramatic increases in bawling and walking during the weaning process.
As indicated above, research in Michigan indicated two-step weaning was less stressful than abrupt weaning. Whether this reduced stress leads to long-term improvement in health and performance appears to be unknown and requires additional research. Despite this, even the reduction in stress during weaning may be worth the effort to use less stressful weaning procedures.
The Bottom Line
Realize that less stressful weaning is not only an improvement for the calves, but may also be less stressful and more peaceful for cattle producers as well because of quieter cattle and a more peaceful environment during weaning.