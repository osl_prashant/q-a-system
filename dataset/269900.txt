Invasive swamp rodent spotted in 5th California county
Invasive swamp rodent spotted in 5th California county

The Associated Press

SACRAMENTO, Calif.




SACRAMENTO, Calif. (AP) — Wildlife officials say an invasive swamp rodent has been killed in San Joaquin County, putting the destructive nutria near the Sacramento-San Joaquin Delta.
The nutria was killed on agricultural land west of Stockton. It is the fifth county where the animal has been spotted since it was discovered in California in March 2017, the Sacramento Bee reported .
State and federal wildlife and water officials fear nutria could get a foothold in the ecologically fragile Delta, which supplies water to 25 million Californians and millions of acres of Central Valley farmland.
Prior to Tuesday's announcement, nutria were found in Fresno, Merced, Stanislaus and Tuolumne counties. It is not known how the animals got to California.
Once established, the rodents could cause loss of wetlands, damage to agricultural crops and levees, dikes and roadbeds.
A team of state biologists is being trained how to trap them. The Department of Fish and Wildlife is working with landowners to get access to private property for trapping and creating maps and grids to focus their search, said spokesman Peter Tira said.
Native to South America, nutria can reach up to 2.5 feet (1 meter) in body length and 20 pounds (9 kilograms) in weight.
A female nutria can give birth to more than 200 offspring within a year of reaching reproductive maturity.
___
Information from: The Sacramento Bee, http://www.sacbee.com