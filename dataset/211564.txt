Cape Cod promotes sustainability
Sustainability efforts are accelerating among cranberry growers in Massachusetts, said Brian Wick, executive director of the Cape Cod Cranberry Growers Association in Carver, Mass.
“We’re always looking for ways to help our growers any way possible, and we’re ramping up efforts to help that happen.”
The association also will be doing some promotions work in Boston this season, Wick said.
The association has about 330 growers, Wick said.
 
Committee plans website for retailers
The Wareham, Mass.-based Cranberry Marketing Committee plans to launch a website dedicated to retailers.
The site, planned to launch in 2018, will be a dedicated section of uscranberries.com that will provide retailers with recommendations and resources for successful in-store cranberry promotions, said Michelle Hogan, executive director.
 
Decas Cranberry boosts cooking line
Carver, Mass.-based Decas Cranberry Products Inc. has expanded a Paradise Meadow cooking and baking cranberry line it launched a few years ago to include Julienne Cranberry-Orange flavor that includes orange peel oil, said Mike McManama, president and CEO.
The sweetened dried cranberries are non-GMO-certified, he said.
The product is sold in the baking aisle in supermarkets, as well as by online retailers such as Amazon and the company's website: www.paradisemeadow.com. 
 
Naturipe Farms expands pouches
Salinas, Calif.-based Naturipe Farms LLC is expanding its grab-and-go bag format at retail this year, said Keith Parker, procurement manager.
“Last year, we did the grab-and-go bag, similar to grapes and bell peppers, for Costco in the 2-pound. Costco Canada loved it and is going to be taking it this year,” he said.
The bag is reclosable.
 
Ocean Spray sponsors festival
The Ocean Spray Family Farmer Owned brand launched in May and continues to gain traction, said Brett Libke, East Coast general manager of Vancouver, British Columbia-based The Oppenheimer Group, which markets Ocean Spray berries.
This year, Ocean Spray is a sponsor of the National Cooperative Business Association CLUSA’s inaugural Co-op Festival, Sept. 30 to Oct. 1 in Washington, D.C., said Sharon Newcomb, Ocean Spray’s manager of cooperative communications.
“We will transform the National Mall into a cranberry bog filled with a half-ton of fresh cranberries to fully immerse visitors in the taste, health and heritage of this exceptional fruit,” she said.