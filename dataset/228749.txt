Teen overcomes overwhelming odds, tackles life
Teen overcomes overwhelming odds, tackles life

By BONNIE BOLDENThe News-Star of Monroe
The Associated Press

MONROE, La.




MONROE, La. (AP) — What do you want people to know about you the most?
"I usually start with 'I have heart disease,'" Emily Gober, 18, quipped.
The issue doesn't define her, but she's a strong advocate for people to understand congenital heart defects and the people who live with them.
Emily was born with pulmonary atresia with ventricular septal defect (VSD). The high school senior has been through five surgeries with a sixth scheduled for this summer.
"Pretty much, I was born without both of my pulmonary arteries, and then the VSD is I have a hole in my heart — or had a hole in my heart," Emily said.
Her body has been repaired with man-made materials and tissue from pigs and cows. The next operation should be the last one she needs.
The idea of one more procedure isn't scaring her. Emily's picking a college to start a few weeks later.
Her mother, Bridget Gober, said they knew at birth that something was wrong. Emily, born at St. Francis Medical Center in Monroe, was a blue baby. She was airlifted to Tulane Medical Center in New Orleans, where she stayed for three months.
"It was up and down every day with her. We didn't know what was going to happen from one day to the next, really from hour to hour. She was so sick," Bridget said.
Emily was born prematurely, and doctors wanted to wait until she reached a certain weight for her first heart cath. She weighed 2 pounds, 11 ounces. They wanted her to weigh 4 pounds before doing the procedure but performed it when she weighed 3 and a half pounds.
"They had no choice but to do it," Bridget said.
Just going into a NICU as a young mother was scary. The whole experience brought new and unnerving sights and sounds.
Seeing her baby for the first time after open-heart surgery was terrifying. Post-surgery, babies are swollen with tubes coming out of everywhere.
Emily had her first operation when she was two months old. Once she was out of surgery, they realized the other side of her heart also was damaged. She had her second open-heart surgery a week later.
The Gobers brought Emily home for the first time on Christmas Eve, and she ate using a feeding tube. Her health was a constant battle.
"Her first year of life, she was blue. It was awful," Bridget said.
Emily also has a lung disease, bronchopulmonary dysplasia (BPD), because she spent so many months on a ventilator as a baby. It results in lower lung function.
"It took me a while to figure out she's just going to cough like this," Bridget said.
Emily had reconstructive surgery on her heart when she was a year old. Doctors took two conduits out and put in a pig artery and pig valve.
As Emily grew, the added valves didn't grow with her, so they had to be replaced.
In 2008, doctors put in a bovine valve and used some man-made materials to repair her heart. Now, she has a pig artery on the bottom and cow on top — covering it — and a plastic valve.
When Emily first got her bovine valve, at 8, she was frustrated because she'd always thought of herself as having pig valves. The change was jarring.
The surgery itself didn't scare her. She was only in the hospital about 10 days.
"I got ice cream after it, I'm fine," she said.
Her next surgery is scheduled for this summer. This should be the last operation because she's no longer growing.
"I've had it done before, and it seemed to work out about five times, so maybe if it's my time it's my time. I don't really have a choice in it, so I might as well be happy about it," Emily said.
She plans to be back on her feet in time to start college in the fall, majoring in English. (Long-term, she wants to teach.)
Dr. Robert Ascuitto, Emily's cardiologist, "will definitely get a graduation invitation this year," Bridget said.
Other people think her story is medically miraculous. Emily takes it all in stride.
"I feel like it's very different for a child to have heart disease than an adult," she said. Adults who develop heart disease, she said, are in for more of a shock. For her, it's just life.
Bridget told Emily she's in great shape and at low risk of a heart attack because she's constantly being monitored. They joke that one day, Emily will be told to change her diet.
"Apparently I like bacon too much. It's literally in me. I can't not eat it," she joked.
She said lots of friendships start with people asking about her scar. Until people see it, Bridget said, they often have no idea of what she's been through.
The scar, though, is just part of Emily. She never thinks about covering it up.
"I don't think about stuff like that. It's kind of like the color of your hair. I mean, you don't wake up every day and freak out about the color of your hair. It's just there," she said.
Sometimes while growing up, people were trying to be nice but said or did things that excluded her or made her feel different — that wasn't always negative.
Every year, Emily's family and friends make a big deal out of Heart Day. One year, the students in her grade at Ouachita Christian School in Monroe wore red just for her.
"Everyone has their own limitations," Emily said, and no one knows what those are based on appearances. Someone else with the same medical diagnosis would have different challenges.
Emily's never done physical education classes. Bridget said a blow to Emily's chest, like in playing dodgeball, could be fatal. Because she didn't grow up doing that, Emily said she didn't know what she was missing out on.
She loves to read, and she's in choir. She hangs out with friends and watches TV, just like every other teen.
"I like to make people laugh," she said.
Bridget said Emily likes to take up for underdog. She takes other students without friends under her wing and invites them to activities. Emily wants to make sure they're still supported after she graduates.
"She kind of got a big heart," Bridget said. "No pun intended."