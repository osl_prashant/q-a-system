While Texas onion grower-shippers reported nearly ideal winter growing conditions, some of their colleagues in southwestern Idaho and eastern Oregon were dealing with a bitter blast of winter.
Heavy snow and ice in the Treasure Valley of Idaho and eastern Oregon wiped out one-quarter of that region's onion production, according to published reports. The storms in December and January damaged at least 18 onion storage and packing facilities.
Texas grower-shippers, preparing for their own season, said they weren't sure how the damage would roll out on the market they would be entering in mid-March.
"It's much too early to make an educated assessment on that. There's no question that our friends in some areas of the Northwest experienced some catastrophic losses," said David DeBerry, owner DKD International LLC and partner in Southwest Onion Growers LLC, McAllen, Texas.
In a Jan. 20 Packer story, Herb Haun, general manager at Weiser, Idaho-based Haun Packing, said 30-40 onion buildings in the Treasure Valley region had roofs collapse or completely collapsed under the weight of snow and ice, including onion packinghouses and storage facilities.
Texas grower-shippers said they weren't sure how it would play out on the onion market.
"It kind of remains to be seen a little bit," said J. Allen Carnes, president and owner of Uvalde, Texas-based Winter Garden Produce Inc.
It's possible, though, that Texas product could reach its markets more quickly this year, Carnes said.
"I think if there's one thing it could do is speed up everything in the pipeline," he said. "We've had an unseasonably warm winter and stuff is ready to come out. That might speed some of that up and get some product out of the way before it gets to our time in shipping."
However, nobody wants to benefit from someone else's bad fortune, he said.
"You hate to see that happen to anybody in ag these days," Carnes said.
Supplies should be normal for a while, said Don Ed Holmes, owner of The Onion House LLC, Weslaco, Texas.
"You'd think if you took that many onions out of the deal, the market would go nuts, but it didn't," he said. "That's because of the improvements in storages there."
Any shortages likely will occur later, not sooner, Holmes said.
"Their shipments are back to 200 loads, where they normally are, so I think the shortage will show up at the end of their deal," he said.
Onion suppliers from Texas will have supplies to compensate for any shortfalls - from Idaho/eastern Oregon and elsewhere, Holmes said.
"New York also has had some problems," he said.
The cut in storage supplies out of Idaho and eastern Oregon did cause prices to rise, but that didn't last long, said Delbert Bland, owner of Bland Farms, Glennville, Ga., which grows onions and operates a distribution center in Donna, Texas.
"Don't get me wrong - it was a tragic event, but it was not a significant amount lost compared to the amount they have out there and the amount they shipped, so I don't think it's going to have an impact on the supply or affect Mexico or Texas that much," he said.
Jimmy Bassetti, owner of J&D Produce Inc., an Edinburg, Texas-based grower-shipper, said he wasn't aware of any long-lasting effects from the snowstorms out West.
"I have not heard of any inventory concerns in the storage resulting from snowstorms," he said.