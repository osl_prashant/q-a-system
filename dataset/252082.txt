BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Thursday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeMar                                12.89  Up   .10Apr        12.90   13.11   12.81   12.89  Up   .10May                                13.10  Up   .11Jun        13.10   13.29   13.01   13.10  Up   .11Sep        13.45   13.67   13.42   13.48  Up   .11Dec                                14.28  Up   .12Feb        14.27   14.47   14.21   14.28  Up   .12Apr        14.36   14.57   14.34   14.43  Up   .15Jun        14.44   14.64   14.43   14.54  Up   .18Sep        14.72   14.88   14.71   14.83  Up   .19Dec                                15.29  Up   .17Feb        15.20   15.33   15.18   15.29  Up   .17Apr        15.24   15.28   15.17   15.25  Up   .08Jun        15.31   15.31   15.23   15.27  Up   .02Sep        15.54   15.54   15.45   15.47  Down .01