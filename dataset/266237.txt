E-commerce sales from food and beverage stores are up, but that statistic doesn’t tell the full story.
The Department of Census recently reported 2016 e-commerce sales from food and beverage stores totaled $1.419 billion, up 25% from 2015 and 58% higher than 2014.
By way of context, the Census numbers said total e-commerce sales in 2016 were $332 billion, up 16% from $287 billion in 2015.
At $22.3 billion, e-commerce sales from electronics and appliance stores in 2016 were much higher than food stores, Census statistics showed.
That is to be expected, since the ritual of buying a television online is already a part of Americana, at least compared with buying a loaf of bread or a bag of apples.
However, the growth rate wasn’t as brisk as food and beverage stores. Online sales of electronics and appliances grew 13% in 2016 over 2015.
Census Bureau statistics show that total sales from food and beverage stores (mostly supermarkets) notched $700.8 billion in 2016, up 2% from $685.3 billion in 2015.
By way of comparison, 2016 sales of electronics and appliance stores totaled $98.8 billion in 2016, off 4% from $103.7 billion in 2015.
But what are the e-commerce sales from nonstore retailers, such as Amazon?
The Census Bureau reports that e-commerce sales of nonstore retailers tallied a whopping $226 billion in 2016, up 20% from $189 billion in 2015 and up 43% over 2015 sales of $158 billion in 2014.
How much does Amazon alone account for?
According to coverage of a One Click Retail study by CNBC, Amazon in 2017 accounted for 44% of all e-commerce sales and about 4% of total U.S. retail sales.
Amazon’s e-commerce sales for 2017 were expected near $197 billion, up 32% compared with 2016.
According to the study, Amazon’s grocery sales in 2017 totaled $1.5 billion, up 33% compared with 2016. Grocery is among the top growth categories for Amazon, along with pantry items (up 38% from 2016), luxury/beauty items (up 48% over 2016) and furniture (up 33%).
Again, Amazon’s electronic sales were much bigger, at $8.5 billion, but the year-over-year growth for electronics was just 4%.
When it comes to e-commerce, there is Amazon and everyone else.
President Trump’s public feud with Amazon over sales taxes on goods sold by third-party vendors sent the stock skittering nearly 5% lower on March 28. Will Trump try to break up Amazon with an anti-trust action?
Amazon has a more-than-daunting lead over other e-commerce rivals, even for food.
If everyman’s definition of Americana ever translates to ordering a loaf of bread and a bag of apples online, Amazon will likely be right there to deliver it.