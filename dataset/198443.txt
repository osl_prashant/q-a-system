The stress of birth may compromise the cow's immune system which can lead to metritis, the infection and inflammation of the uterus. Metritis decreases fertility and milk production and can lead to detrimental economic losses estimated at $100-$300 per case.Metritis is characterized as the presence of abnormal vaginal discharge that is typically yellow or brownish-red, as seen inFigure 1, with a very strong odor. Depending on the severity of the infection, metritis can also be associated with other systemic symptoms such as lethargic behavior and fever (> 102.5¬?F).

The majority of metritis cases occur within the first 14 days after calving with a peak frequency around 5 to 7 days after birth, which can be seen inFigure 2. Once metritis is diagnosed, the cow should be examined for other metabolic and infectious diseases, such as ketosis and displaced abomasum, as these diseases are commonly associated with metritis.

Figure 3. 
The Metricheck device is a practical and economic way to evaluate vaginal discharge (Figure 3, above). The device consists of a stainless steel rod with a rubber hemisphere that is used to retrieve the vaginal contents. The Metricheck device is cleaned with disinfectant, inserted into the vagina of the cow, and then removed while pulling the device out at an upward 45 degree angle (Figure 4, below).
Figure 4.
After the vaginal content is collected, it can be scored according to the quality and smell of the mucus, as seen inFigure 5.

Using this score system, the mucus can be described as:
Score 0 = clear or translucent mucus;
Score 1 = mucus containing flecks of white or off-white pus;
Score 2 = discharge containing ‚â§50% white or off-white mucopurulent material;
Score 3 = discharge containing ‚â•50% purulent material, usually white or sanguineous.

The smell is classified as not having smell (score 0) or a strong odorous smell (score 3). These scores reflect the presence and the amount of bacteria in the uterus. Vaginal discharge with a smell score of 0 means that there are a low number of pathogenic bacteria. A score of 3 with a strong smell indicates the presence of bacterial infection.

Since 95% of metritis cases occur within the first 14 days postpartum, utilize the Metricheck device during this time.
This process is easy to perform, does not require intense training, and is a comfortable and sanitary experience for the cow as compared to other methods. Click here for a step-by-step video of how to use the Metricheck device on the YouTube channel:
Cows with a combined Metricheck score and smell score that is above 3 should be treated.
The University of Illinois at Urbana-Champaign Dairy Farm evaluated dairy cows from parturition to early lactation using the Metricheck device. From the current data received, 69.7% of the cows on this trial had metritis between 4 to 17 days in milk post calving.Figure 6shows the percentages of cows with metritis between 4 to 30 days in milk post calving while specifically looking at Metricheck + smell score, smell score, and Metricheck score.

Figure 6.Percentage of cows with metritis for Metricheck + smell score > 3, smell score = 3, and Metricheck score > 2.
The graphs above follow a similar distribution, with the majority of metritis cases ranging from 4 to 17 days in milk.
Figure 7displays the average scores for Metricheck, smell, and Metricheck + smell in relation to the days in milk. Metritis was characterized in this study by a Metricheck score average above 2, a smell score average above 1, or a combined Metricheck and smell score average above 3. These graphs show that a higher Metricheck score and a high smell score were most prevalent in cows during the 4 to 17 days in milk range.
Figure 7.Average scores for Metricheck, smell, and Metricheck + smell. 
Diagnosing metritis at an early stage is beneficial for both the cow and the farm. The Metricheck device allows for accurate diagnosis of metritis so that the cow can be treated as early as possible.