Heading to the United Fresh Washington Conference from Sept. 12-14, I look forward to hearing conversations about the immigration, the presidential race, the next farm bill, nutrition programs, the implementation of food safety regulations and more. It should be another great event.
Most importantly, will Hillary's recent wobbles cost her?
I notice there is not a conference meeting with Food and Drug Administration officials in Maryland this year. That's somewhat disappointing; the back and forth dialogue between industry and FDA is always illuminating.
Still, I'm sure there will be plenty of focus on implementation and enforcement of produce safety rules during the always insightful  three-day event.

--
What age group spends the most on fresh fruits and vegetables? According to the latest Consumer Expenditures data, it is no contest. The 35 to 44 age bracket by far spent the most on both fresh fruits and fresh vegetables in 2015. That age group spent an average of $366 each on fresh fruit, compared with the overall U.S. average of $284. The group with the lowest spend on fresh produce was the under 25 age group, which purchased an a average of only $175 of fresh fruit in 2015.
For fresh vegetables, the 35 to 44 age group spent an average of $323 each in 2015, also markedly higher than the overall U.S. average of $247. Again, the under 25 group were the lowest spending consumers, buying an average of $143 of fresh vegetables in 2015.
---
The September Food Irradiation Update – published by Ronald Eustice – had a few interesting tidbits this morning:
Exports of irradiated Mexican fruit to the United States are growing at an annual rate of more than 15%;
In 2015, 11,700 ton of irradiated Mexican produce was exported to the U.S., up 17% over 2014; and
83% of irradiated Mexican fruit was guava followed by chile manzano (Capsicum pubescens) at 8.4%.

Keep calm and irradiate on…