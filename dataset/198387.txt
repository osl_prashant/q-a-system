Q. I want to transfer corn silage from a bag to a tower for feeding. How can I maintain stability when moving silage?
 
A.
During transfer oxygen will be introduced into the silage, which can then cause yeasts, the main cause of silage instability, to grow. This consumes dry matter (DM) and valuable nutrients.
Before moving silage, check if it is a good candidate for transfer. Silage that is unstable will heat up rapidly and spoil when exposed to air.
To know the challenges faced in transferring a specific silage, we need to go beyond pH. Samples need to be sent to a reputable analytical lab to obtain fermentation acid profiles. Silages that have a low pH may contain a high level of lactic acid and no other acids. Coupled with a high yeast level in the silage, this material could be highly unstable when exposed to oxygen and, therefore, heat up rapidly. Ideally the silage should contain reasonable levels (2 to 3 percent) of anti-mycotic acids (such as acetic or propionic) that kill yeasts and molds.
During the move, work as quickly as possible and avoid mixing to minimize oxygen exposure. Pay close attention to plastic management and packing density, both of which help reduce exposure to oxygen. If possible, plan to make the transfer during the coolest weather possible.
If the silage has not been treated at ensiling with an inoculant proven to help improve aerobic stability, consider treating it at transfer with a chemical preservative containing anti-mycotic compounds. Handle these materials with care and be sure to follow recommended dose rates. Do not waste thousands of dollars of feed by trying to save a few dollars on the chemical! If you know in advance the silage you will be moving, I recommend using an inoculant containing high rate Lactobacillus buchneri 40788, which has been reviewed by the FDA and allowed to claim efficacy in preventing the growth of yeasts and molds in silages and HMC.
I hope this information helps you successfully relocate your silage.
Sincerely,
The Silage Dr.
Question about silage management? Ask the Silage Dr. on Twitter, Facebook or visit www.qualitysilage.com.