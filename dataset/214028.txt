Texas lost more than $200 million in crop and livestock losses, according to economists with Texas A&M AgriLife Extension Service.
Cotton farmers were one of the worst hit as many were just starting to harvest one of the best crops ever in the state.
Cattle ranchers worked tirelessly to move cattle to higher ground ahead of the storm, but torrential rain and lingering flood waters made recovery difficult.  Ranchers had trouble accessing animals to provide feed, or having a way to move them out of the area.
While Harvey was a smaller storm than Hurricane Irma that made landfall on the East Coast only a few days later, the larger flood impact raised total losses near the same value.
Hay and feed donations were valued at more than $1.3 million, according to AgriLife Extension economists.
Hurricane losses by agricultural commodity include:

Livestock: $93 million
Cotton: $100 million
Rice and soybeans: $8 million

Livestock losses include not only cattle and calves that died during the hurricane but also industry infrastructure, said David Anderson, AgriLife Extension livestock economist in College Station. Beyond animals lost directly due to the storm, extensive supplies of hay for winter feeding were destroyed.
“What you must take into consideration is the replacement costs of hay that was destroyed from the high flood waters,” Anderson said. “We are right on the verge of entering winter feeding season and ranchers will have to find replacement hay that averages $63 per round bale. A rancher may typically feed two or more round bales per cow during winter, so even if there isn’t hay available they will still have to purchase some type of supplemental feed. All of this comes with a hefty price.”
Hay donations came as far as New Jersey, such as the photo above. This load of hay was unloaded in Sour Lake, Texas. Hay and feed donations were valued at more than $1.3 million, according to Texas A&M AgriLife Extension economists.
(Texas A&M AgriLife Extension Service photo by Blair Fannin)
The value of fences, barns and animal-handling facilities lost adds up quickly, Anderson said.
“Rebuilding fences can cost $2.50 or more per foot,” he said. “Overall, these livestock loss numbers could have been far, far worse had it not been for the quick action of ranchers ahead of and during the storm. Also important were the proactive actions of cattle industry associations in Texas, countless volunteers, and AgriLife Extension working together in coordination to either get cattle to dry points and make ample supplies of hay available, as well as the U.S. military airdropping round bales.”
Meanwhile, 200,000 bales of cotton lint on the stalk valued at $62.4 million was lost and another 200,000 harvested bales valued at $9.6 million had degraded quality, said Dr. John Robinson, AgriLife Extension cotton marketing economist in College Station. In addition, there were widespread losses of cottonseed.
Rice and soybean crop losses accounted for approximately $8 million in losses. In its October crop production estimates, USDA lowered Texas rice production 614,000 cwt compared to pre-storm estimates. The value of that production at current market prices is about $7.5 million, said Mark Welch, AgriLife Extension grains marketing economist in College Station.
AgriLife Extension and USDA-Farm Service Agency’s Texas state office have teamed to produce a series of videos to share disaster assistance program details for farmers and ranchers recovering from Hurricane Harvey. The information is available at https://agecoext.tamu.edu/resources/podcasts-videos/.