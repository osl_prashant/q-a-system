The Dairy Calf and Heifer Association (DCHA) announces the agenda and theme for their annual conference. The 2017 conference is set for April 11-13 in Madison, Wis. with the theme "Sky's the Limit.""We are looking forward to attending the conference again this year. Each year we take new tidbits of information back home to make our operations better," says Daphne Holterman, of Rosy-Lane Holsteins, Watertown, Wis. "I like DCHA because we're all committed to calves and all have a passion for calves.
Last year's conference drew more than 500 dairy calf and heifer raisers from 27 states and 10 countries, representing more than 1 million cattle.
Here is an overview of what you'll see at the 2017 conference:
Schedule highlights
Producer panels, presentations and breakout sessions will discuss hot-button topics. The conference will also include on-farm sessions and tours, and post-conference demonstrations by the Wisconsin Veterinary Diagnostic Laboratory. ¬?¬?¬?¬?
Farm tours with a focus on calf and heifer management at two progressive Wisconsin dairies:
¬? Endres Jazzy Jerseys
¬? Ziegler Dairy Farm
¬? Breakout sessions:
¬? Dynamic growth of replacement heifers
¬? Group housing do's and don'ts
¬? New strategies for heifer reproduction
¬? BRD prevention and treatment
¬? Producer panel topics:
¬? Employee management perspectives
¬? Animal handling protocols
Presentation topics:
¬? Rethinking colostrum, Dr. Mike Van Amburgh, Cornell University
¬? Farmer-consumer relationships, Stan Erwine, Dairy Management, Inc.
¬? Turning employees into fans, Ruby Newell-Legner, 7 Star Service
¬? Nutritional strategies to improve health, Dr. Michael Ballou, Texas Tech University
¬? Animal welfare issues, Dr. Marina von Keyserlingk, University of British Columbia
¬? Calves, consumers and communication, moderated by Emily Yeiser Stepp, National Milk Producers Federation
View full schedule and seminar descriptions: http://bit.ly/2gNG9Cl
Make plans to attend
To experience these great networking possibilities, register soon. You can do so online at calfandheifer.org under the "Conference" tab.
Tradeshow registration is also open. Attendees will have numerous chances during the event to browse the trade show, in addition to a reception Wednesday evening. To reserve booth space, contact info@calfandheifer.org.
Become a sponsor
Want to increase your exposure among top-notch calf and heifer raisers? The 2017 conference sponsorships have been specially designed to provide visibility and access to target audiences.
Created to help increase exposure at this one-of-a-kind event, there are opportunities for every budget. Learn more: http://bit.ly/2g0fiWt.
For more information about the 2017 DCHA Conference or to become a member visit www.calfandheifer.org, call (855) 400-3242 or email info@calfandheifer.org.