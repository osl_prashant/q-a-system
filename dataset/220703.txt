This edition of The Packer TV explores news in three fruit crops.
In Florida, researchers are investigating what types of citrus can replace the industry's iconic juice oranges, in an attempt to bypass citrus greening disease problems.
Apple marketers are seeing interest in organic apples continue to increase, mostly driven by demand among millennials. 
In California, strawberry acreage has dipped, but growers are expecting higher yields to boost production.
MORE: Organic apple share on the rise
MORE: California strawberry acreage down; volume expected up