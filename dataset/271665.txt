BC-APFN-Business News Preview
BC-APFN-Business News Preview

The Associated Press



Among the stories Tuesday from The Associated Press:
TOP STORIES:
FACEBOOK-SPELLING OUT WHAT'S FORBIDDEN — If you've ever wondered exactly what sorts of things Facebook would like you not to do on its service, you're in luck. For the first time, the social network is publishing detailed guidelines to what does and doesn't belong on its service — 27 pages worth of them, in fact. By Barbara Ortutay. SENT: 770 words.
With:
AP POLL-FACEBOOK-PRIVACY SCANDAL — Seventy percent of Americans who have heard of Facebook's Cambridge Analytica scandal have taken some action to change the way they use their social media accounts in the wake of revelations about the company, according to a new poll from the AP-NORC Center for Public Affairs Research. SENT: 890 words, photos, with video, graphic, social media.
And:
AP POLL-SOCIAL MEDIA USE — Nearly half of Americans who use the Internet say they use Facebook at least several times a day, with Facebook-owned Instagram coming in No. 2 among social media sites, according to a new poll from the AP-NORC Center for Public Affairs Research.  UPCOMING, with video, graphic, social media.
Also:
— UK FACEBOOK PRIVACY SCANDAL — A Cambridge University professor at the heart of the Facebook privacy scandal says that the former CEO of data firm Cambridge Analytica lied to a British parliamentary committee investigating fake news. SENT: 140 words, photos.
AMAZON-CAR DELIVERY — Amazon's latest perk ... free delivery to your car. The Seattle company said Tuesday that it has begun delivering packages in 37 cities to Prime members who own newer General Motors or Volvo vehicles. By Mae Anderson. SENT: 410 words, photos.
TRUMP-TRADE — President Donald Trump's escalating dispute with China over trade and technology is threatening jobs and profits in working-class communities where his "America First" agenda hit home. By Richard Lardner. SENT: 930 words, photos.
CONSUMER WATCHDOG-NAME CHANGE — Under its acting director Mick Mulvaney, the Consumer Financial Protection Bureau has been pushing publicly to be referred to as the Bureau of Consumer Financial Protection by lawmakers and the media. By Ken Sweet. SENT: 130 words.
PECAN BRANDING — The American pecan industry is getting organized. Farmers of America's only native tree nut are launching a marketing campaign Wednesday that they hope will help them better compete for a larger share of the multibillion-dollar U.S. snacking business and growing Chinese demand.  By Emily Schmall. UPCOMING: 700 words by 6 p.m., photos.
MARKETS & ECONOMY:
FINANCIAL MARKETS — Stocks are opening broadly higher on Wall Street as a handful of solid company earnings reports put investors in a buying mood. SENT: 160 words, photos. UPCOMING: Will be updated through 5 p.m.
CONSUMER CONFIDENCE — The Conference Board releases its April index on U.S. consumer confidence. By Martin Crutsinger. UPCOMING: 130 words after release of report at 10 a.m. 300 words by 10:45 a.m.
HOME PRICES — U.S. home prices jumped in February as buyers are fiercely competing over a dwindling number of homes for sale. By Christopher Rugaber. SENT: 410 words, photos.
NEW HOME SALES — The Commerce Department reports on sales of new homes in March. By Josh Boak. UPCOMING: 130 words after release of report at 10 a.m. 350 words by 10:45 a.m.
INDUSTRY:
CHINA AUTO SHOW PREVIEW — The biggest global auto show of the year showcases China's ambitions to become a leader in electric cars and the industry's multibillion-dollar scramble to roll out models that appeal to price-conscious but demanding Chinese drivers. SENT: 900 words.
SOUTHWEST-SAFETY HISTORY — Until last week, Southwest Airlines had a string of 47 years without a passenger dying in an accident. But it has also paid millions in fines over safety. Recently its mechanics union accused the airline of taking shortcuts. Some analysts, however, consider it a safe airline. Its safety record is coming under scrutiny after an engine failure at 32,000 feet last week hurled shrapnel at a Southwest jet, breaking a window and killing a passenger. By David Koenig. SENT: 1,060 words, photos.
IKEA-RECALL — Swedish furniture retailer Ikea is recalling for repair gas hobs sold across Europe because their carbon monoxide emissions are above the allowed limits. SENT: 130 words.
KIDS-AGING DISEASE-HFR — Children with a rare disease that causes rapid aging and early death may live longer if treated with a drug first developed for cancer patients, research suggests. While skeptics say the small, preliminary study is inconclusive, the researchers say the results show their work is on the right track. By Medical Writer Lindsey Tanner. UPCOMING: 500 words, photo for release at 11 a.m.
EARNINGS:
EARNS-CATERPILLAR — A boost in equipment sales propelled Caterpillar's first-quarter profit, as a strong global economy helped support construction and energy industry projects. SENT: 140 words, photos.
EARS-COCA-COLA — Coca-Cola's first-quarter profit jumped as the company introduced new flavors and continues to discard its bottling operations. SENT; 270 words, photos.
TECHNOLOGY & MEDIA:
BITCOIN-POWER DRAIN — Cheap electricity and chilly air near New York's northern border is attracting energy-hungry businesses that "mine" bitcoins and other digital currencies with stacks of computers. But can crypto-currency miners create a 21st century version of a gold rush or are they merely electricity vampires? By Michael Hill. SENT: 800 words, photos.
FILM-CINEMACON — Presentations from Disney, Warner Bros. and STX throughout the day at the annual gathering of theater owners in Las Vegas. Multiple stories expected throughout day. UPCOMING: Developing starting at 1 p.m.
With:
CINEMACON-SONY — Sony Pictures Entertainment Chairman Tom Rothman assured theater owners Monday that his studio is dedicated to appealing to a range of audiences — from global franchises such as "Spider-Man" and "The Girl with the Dragon Tattoo" series to family films, action pics, comedies and even Quentin Tarantino's Leonardo DiCaprio and Brad Pitt film "Once Upon a Time in Hollywood." By Lindsey Bahr. SENT: 730 words, photos, video.
SAUDI ARABIA-US TABLOID — The mystery behind the origins of a fawning pro-Saudi magazine that laid down a red carpet for Crown Prince Mohammed's visit. By Josh Lederman and Jeff Horwitz. SENT: 1,250 words, photo.
ISRAEL-TWITTER BLOCK — A hard-line Israeli lawmaker is sarcastically thanking Twitter for briefly freezing his account following a post he wrote about wishing a teen Palestinian protester had been shot. SENT: 140 words.
PERSONAL FINANCE:
NERDWALLET-ASK BRIANNA-FINANCIAL ADULT — Graduating from college and getting your first full-time job are major milestones that might make you feel you've finally reached adulthood. But you won't be a financial adult until you save regularly, spend mindfully, face reality and know when to ask for help. By NerdWallet columnist Brianna McGurran. UPCOMING: 770 words by 11 a.m., photos.
INTERNATIONAL:
GERMANY-ECONOMY — A closely watched survey showed a bigger-than-expected drop in German business confidence amid worries about international trade tensions, but Tuesday's report doesn't necessarily signal any serious problems for Europe's biggest economy. SENT: 300 words.
BANGLADESH-TEXTILE INDUSTRY- FACTORY COLLAPSE — Five years after a garment factory collapse in Bangladesh killed 1,134 people much remains to be done to improve conditions for workers. By Julhas Alam. SENT: 900 words, photos.
FRANCE-CORRUPTION — French billionaire Vincent Bollore has been detained for questioning in an investigation into alleged corruption during lucrative port deals in Africa. SENT: 130 words.