Appeeling Fruit Inc., headquartered in Reading, Pa., was founded 25 years ago in the fall of 1991.
The family-owned company began by selling peeled apple slices to bakeries for incorporation in desserts, but by 1998, it was marketing fresh sliced apples throughout the East Coast.
President and CEO Steve Cygan, along with wife and vice president Beth Ann Cygan, have guided AFI through a period of expansion and growth. The company recently upgraded its production equipment and packing its red and green apple slices under private labels.
It even added fresh sliced pears, honeycrisp apple slices, organic apple slices and destemmed grapes to its offerings.
On Aug. 18, AFI hosted an open house event for its customers and vendors, with one of Pennsylvania's senators, Judy Schwank, and the state's Secretary of Agriculture Russell Redding speaking toward its success.
In a press release covering the event, Steve Cygan thanked those customers and vendors for contributing to AFI's success.
"We are grateful for their continued support, look to build upon our partnerships and plan to create new connections so that we may continue to be a thriving business 25 years from now!"