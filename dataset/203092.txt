Lower-than-anticipated mushroom supplies - 15% to 20% less by some estimates - had caused some worries among marketers at precisely the wrong time - the approach of Thanksgiving and Christmas.
"Mushrooms were short," said Kevin Donovan, sales manager for Kennett Square, Pa.-based Phillips Mushroom Farms.
That's the time of year when demand zooms and supplies are expected to meet all calls, Donovan said.
"Demand increases quite a bit, and production just wasn't back to where it should be," he said. "It created some shortages, and some people were looking for mushrooms."
They found product, as it turned out, as growers turned out enough mushrooms to meet the seasonal peak demand, Donovan said.
Now, things are looking up, he said.
"Production is getting better," he said.
Donovan said production in 2015 had been better going into the holiday period.
When the holidays were done, though, there still was barely enough to scrape through.
Production was down for a time in 2016, but volume wasn't the only issue, Donovan said.
"I still see demand going up, and I don't think production has gotten caught up to it," he said.
The San Jose, Calif.-based Mushroom Council estimated demand for mushrooms grew by 3% in 2016, with similar growth anticipated in 2017.
The industry needs to expand production volume year by year, it seems, and growers have done a good job of keeping up, Donovan said.
"Demand (and) our production will vary from year to year and people will build new buildings and (use) different techniques to increase production, but demand has kept up with it," he said.
Crop yields appear to have come back to "near normal expected levels" since the supply shortage of September through November, said Vince Versagli, sales director with Kennett Square-based South Mill Mushroom Sales and Kaolin Mushroom Farms Inc.
"I expect crop yields to continue into next year at normal seasonal levels," Versagli said.
"There is nothing to indicate either a significant increase or decrease in crop yields and therefore total available pounds, assuming no significant external event that negatively impacts crop production over a wide area."
The factor most affecting supply will be increase in demand, especially with the continuing success of The Blend concept, Versagli said.
Bill St. John, sales director for Gonzales, Texas-based Kitchen Pride Mushroom Farms Inc., said his company weathered a temporary shortage with relatively few problems.
"We've been OK on supply. Our production was reduced in August and September but not a lot," he said.
"Supply came back quite a bit before the big demand time."
This year's crop
Crop yields appear to be rebounding to near-normal levels for 2017, said Bev King, communications director with the Avondale, Pa.-based American Mushroom Institute.
"Mushroom farmers are optimistic, barring any unforeseen compost or production issues," she said.
Anticipated growth areas include browns, organics and exotics, King said.
"Increased demand, with the continuing success of The Blend and other promotions, will be the major factor affecting supply," she said.
Mike Basciani, partner in Avondale-based Basciani Mushroom Farms, said holiday sales were strong.
"Especially in the East, we've been fighting warm temperatures, and the cost of everything is going up and prices are the same as last 40 years, but the sales have been fairly decent," he said.
Kevin Delaney, vice president of sales and marketing with Avondale-based To-Jo Mushrooms, Inc., said supplies are projected to "align more closely" with demand during the first quarter of 2017, after a "consistent" shortage in the last quarter of 2016.
"The industry is transitioning from a period where yields were significantly down due to poor-performing compost to a winter compost mix that traditionally produces higher yields," he said.
The crop outlook is "outstanding" for Watsonville, Calif.-based Monterey Mushrooms Inc., said Mike O'Brien, vice president of sales and marketing.
"Since we have 10 farms strategically located around the United States and Mexico and we make our own compost, we are prepared and committed to supply our valuable customers in 2017," he said.