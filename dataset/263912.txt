Nature Conservancy to work with Forest Service in Wisconsin
Nature Conservancy to work with Forest Service in Wisconsin

The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — An environmental organization and the U.S. Forest Service are working together to harvest timber in northern Wisconsin.
The 2014 Farm Bill has allowed the two groups to enter into a stewardship agreement, Wisconsin Public Radio reported . The conservancy will hire loggers, sell timber and use the proceeds for projects the Forest Service can't afford to do.
"I'm not here to say that we need every tree cut. I think we need old places and we need places that are being managed," said Matt Dallman, the director of conservation for the Nature Conservancy in Wisconsin. "While we want some places preserved, we also want places for jobs and the economy."
The conservancy plans to use some money to restore Simpson Creek by rerouting the channel and exposing the gravel floor that fish need to spawn.
"It's a trout stream that was pretty much beat up from the old logging era. You used to catch 15-inch brook trout in here," Dallman said. "Then they floated logs down it, and it got dammed up."
The group also plans to rebuild a handicap accessible boardwalk on the Oconto River and will use funds to restore habitat for the endangered Kirtland's warbler.
"The value of this is making some of these trout streams more accessible for people with disabilities," said Forest Supervisor Paul Strong. "People can get out there in wheel chairs and literally be in brook trout heaven."
The Forest Service's budget has been stretched by efforts to fight wildfire that have become more frequent and more intense, Strong said.
The conservancy plans to harvest about 2 million board feet of lumber in Chequamegon-Nicolet National Forest in Forest County, which will generate about $370,000 of revenue, Dallman said.
The group hired forester Ryan Grefe to supervise the project. Thinning the trees in the area will help increase the forest's health overall and also help the local economy, he said.
"The opportunity to have some funds directly from a local timber sale go right back into the local economy is just great," Grefe said. "The logging industry, the timber industry, I can't even think how many people they employ."
___
Information from: Wisconsin Public Radio, http://www.wpr.org