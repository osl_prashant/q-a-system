As peach growers in the Carolinas face the prospect of a leaner crop this year in the wake of a March freeze, blueberry growers in the region are looking at a similar prospect.
Strawberries, on the other hand, likely will have a good year, agriculture officials said.
The fallout from freezing temperatures as low as the upper teens in some fields likely will cut some growers’ crops in half across North and South Carolina, officials said.
Results will vary, in some cases, field to field, they said.
“Most (blueberry) growers I’ve spoken with lost somewhere between 34% and 50% of their crop,” said Matt Cornwell, marketing specialist with the South Carolina Department of Agriculture.
The outlook in North Carolina was less definite, said Dexter Hill, marketing specialist in peaches and berries with the North Carolina Department of Agriculture in Kinston, N.C.
“Blueberries, we’re still not sure of,” he said. “Initially, it looked really bad. I’d say it’s between a 40% and 60% loss, but it’s still too early to tell.”
Grower efforts to defend plants against the cold had mixed results, at least in some cases, Hill said.
“They had a hard time protecting berries,” he said. “Nozzles were freezing up, and wind was blowing the water around.”
Brad Thompson, North Carolina State University extension agent in Montgomery County, described the blueberry crop in his area as in “fair shape.”
“People that used heaters or irrigated have berries,” he said. “Probably 50% to 60% of the crop is still there. Some had smudge pots. Others irrigated through the night to frost-protect that way and had pretty good success that way.”
It could have been worse, Thompson said.
“The outlook for blueberries, I’d say, is fair — there will be berries,” he said. “They’re in better shape than peaches are.”
Freezing temperatures wiped out nearly all the rabbiteye blueberry crop in Georgia, but that wasn’t the case in North Carolina, Thompson said.
“We have highbush and rabbiteye, with more highbush in the southeastern part of the county, in the sand,” he said. “But if anybody’s growing blueberries, they have both.”
Most of North Carolina’s blueberry production is in the southeastern corner of the state, near Wilmington, Thompson said.
North Carolina’s blueberry season generally runs from June through August, Thompson said.
Strawberries, on the other hand, appear headed toward normal to above-normal production, officials in North and South Carolina said.
“Strawberries and other mixed berries seemed to have managed to make it through without a large loss, but some damage was incurred, depending on frost prevention techniques and effectiveness,” Cornwell said.
Dexter Hill, marketing specialist in peaches and berries with the North Carolina Department of Agriculture in Kinston, N.C., agreed.
“Strawberries are great this year. We’re going to have a really good crop,” he said. “The freeze did affect production early, but that was way back in March. All the growers have a good supply. Product looks excellent, and we look to go through Memorial Day.”
Thompson said growers managed to provide enough protection for their strawberry beds during the cold nights.
“Strawberries fared fine — everybody got their covers put on,” he said.
A “bumper crop” is expected, Thompson said.
“They started a little earlier and are getting reset on a lot, so they’re basically going to have two crops,” he said.
The first commercial volumes of berries shipped about three weeks earlier than normal this year, with a peak coming around Mother’s Day, Thompson said.
The strawberry season should wind down by mid-June, Thompson said.
A spate of springtime downpours — 4 to 8 inches in some areas across North Carolina — affected some berries, Thompson said.
“But that’s a very temporary thing,” he said.