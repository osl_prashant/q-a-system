Pork producers will get the latest information on animal health, nutrition and other vital topics at the Southern Indiana Pork Conference.The conference, sponsored by Purdue Extension and Indiana Pork, will be 10 a.m. to 4:30 p.m. Feb. 5 at the Schnitzelbank Restaurant, 393 Third Avenue, Jasper.
"The goal is to bring together pork producers, academics, veterinarians and others in the pork industry to talk about key industry issues," said Kenneth Eck, Extension educator in Dubois County and a conference organizer.
Topics and presenters:
"Health Concerns for Southern Indiana." John Baker, Warrick Veterinary Clinic.
"Feeding Betaine to Reduce Heat Stress in Sows and Boars." Allan Schinckel, Extension breeding and genetics specialist.
"State Regulatory and Legislative Issues and How They'll Affect You." John Trenary, executive director, and Ben Wicker, director of producer outreach, Indiana Pork.
"Pork Quality - How Does Animal Handling Affect It?" Stacy Zuelly, Extension meat science specialist.
"Can We Overuse Synthetic Amino Acids in Wean-to-Finish Operations?" Brian Richert, Extension swine specialist.
"Pork Quality Assurance Plus (PQA+) Certification." Eck.
Producers need to attend only the PQA+ session for certification. Producers planning to attend the PQA+ session should contact the Dubois County Extension office at 812-482-1782 to make reservations.
Indiana Pork will provide lunch. The conference is free, but registration is required by Jan. 29. To sign up, go to http://www.indianapork.com/ and click on the pork conference banner.
For more information on the conference, go to http://www.extension.purdue.edu/dubois or contact the Dubois County Extension office at 812-482-1782, duboiseces@purdue.edu.