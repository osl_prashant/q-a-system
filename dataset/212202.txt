(UPDATED, 2:53 p.m.) “Green gold” was apparently too much of a temptation for Mission Produce workers accused of selling the company’s high-priced fruit over several months and pocketing the money. 
Three workers were arrested on suspicion of stealing avocados from a Mission Produce avocado facility in Oxnard, Calif., and then illegally selling the fruit to customers unaware to the scheme.
 
The Los Angeles Times reported that the Ventura County Sheriff’s Office arrested Carlos Chavez, Rahim Leblanc and Joseph Valenzuela June 14 on suspicion of grand theft of avocados.
 
Steve Barnard, president of Mission Produce, told The Times that boxes of avocados were sold from the back door of the distribution center at prices well below the current f.o.b. market of near $50 per carton.
 
That raised the suspicions of some customers, who alerted Mission Produce officials. Barnard told The Times that security will be tightened at Mission facilities and that the losses will be covered by insurance.
 
“We believe this was an isolated incident, but the threat is always there especially with a shortage and high prices,” Barnard said in a comment e-mailed to The Packer.