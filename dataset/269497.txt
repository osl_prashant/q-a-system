BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Mon.Fri.No. 2 Soft wheat4.62½4.72½No. 1 Yellow soybeans10.1710.29¼No. 2 Yellow Corn3.65½e3.69¼eNo. 2 Yellow Corn3.82½p3.86¼p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted