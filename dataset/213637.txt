2017 has been a “wet” spring for planting which also can cause compaction issues; side-wall and pinch-row compaction. Compaction of any kind can lead to emergence issues and possible yield loss. Identifying compaction is the first step in understanding the consequences during the growing season and at harvest while also beginning to consider options for mitigating if long-term consequences appear to be in place.Today, many central-fill planters operate in Ohio with some carrying fertilizer tanks mounted at the center portion. This equates to a lot of weight being carried at the center of the planter and possibly by the tractor. Wet soil conditions at planting increase the risk of side-wall and pinch-row compaction. It is important as corn and soybeans emerge, to not only scout for pest, disease and nutrient issues, but also look for compaction issues caused during planting.
Once compaction has occurred, whether side-wall or pinch-row, no solutions exist for this season since the damage is done. Expect to see side-wall compaction when scouting. However, we recommend scouting and paying attention to areas where compaction exists to determine the impact.
Issues
Pinch-row and side-wall compaction can have a negative consequence to getting crops off to the right start.
Issues that can occur due to side-wall compaction include poor germination, non-uniform emergence, limit crop development and restricted root establishment and growth.
Restricted root growth will negatively impact nutrient uptake even when soil test values indicate sufficient nutrition.
Diagnose
Corn and soybean plants impacted by side-wall compaction will exhibit problems such as stunted or slowed growth, nutrient deficiency symptoms, less vigor, and lagging in growth stage.
Diagnosing the issue requires scouting and digging in ears where plants exhibit symptoms. You may also want to dig up a few plants.Visit low areas or regions on the

Side-wall compaction can be identified by root development.A healthy plant should have a 3-D root ball
Existence of side-wall compaction would be vertical root growth with little root development horizontally.
A hand soil penetrometer can be used to

For pinch row, scout rows where the center transport tires of the planter plus the tractor tires ran.Compare growth stages between the “pinched-rows” and those rows without traffic.
If issues exist, return to the rows or areas as the crop develops to note growth stage and any develop issues such as lagging behind in growth stage and reproductive timing.

Before harvest, take yield estimates from rows where 1) side-wall or pinch-row compaction was identified and 2) non-compacted areas. Compare yields for any differences.
Remember, yield monitors will not pick up pinch-row compaction unless only harvested those rows and not a full header.
Side-wall and Pinch Row Compaction
2017 has been a “wet” spring for planting which also can cause compaction issues; side-wall and pinch-row compaction.  Compaction of any kind can lead to emergence issues and possible yield loss.  Identifying compaction is the first step in understanding the consequences during the growing season and at harvest while also beginning to consider options for mitigating if long-term consequences appear to be in place. 
Today, many central-fill planters operate in Ohio with some carrying fertilizer tanks mounted at the center portion.  This equates to a lot of weight being carried at the center of the planter and possibly by the tractor.  Wet soil conditions at planting increase the risk of side-wall and pinch-row compaction. It is important as corn and soybeans emerge, to not only scout for pest, disease and nutrient issues, but also look for compaction issues caused during planting.
Once compaction has occurred, whether side-wall or pinch-row, no solutions exist for this season since the damage is done. Expect to see side-wall compaction when scouting.  However, we recommend scouting and paying attention to areas where compaction exists to determine the impact.
Notes
We encourage taking notes and pictures when scouting for pinch-row and side-wall compaction.
APPs such as FieldView Cab, Trimble Connected Farm, and others help collecting this information plus drop GPS pins.
Routine scouting is advised during stand establishment to help identify the existence of side-wall compaction.
Consider options for reducing the impact of side-wall compaction in the futureTracks
Lower tire pressure on tractor and planter transport tires.
Type of transport tire used
Adjust downforce pressure on row-units in wet conditions
Reduce closing wheel(s) down force in wet conditions
Utilize closing wheels such as spike to help reduce side-wall compaction
If possible, wait an extra day before planting