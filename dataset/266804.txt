BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Monday:
Open  High  Low  Settle   Chg.ORANGE JUICE                          15,000 lbs.; cents per lb.                May     141.20 142.00 137.45 139.20  —1.90Jun                          140.10  —1.45Jul     141.50 142.20 138.30 140.10  —1.45Aug                          140.80   —.90Sep     141.60 141.60 139.10 140.80   —.90Nov     140.30 141.40 140.00 141.40   —.90Jan     141.00 142.10 140.85 142.10   —.85Feb                          142.60   —.85Mar     141.80 142.60 141.80 142.60   —.85May                          143.35   —.85Jul                          143.45   —.85Sep                          143.55   —.85Nov                          143.65   —.85Jan                          143.75   —.85Feb                          143.85   —.85Mar                          143.85   —.85May                          143.95   —.85Jul                          144.05   —.85Sep                          144.15   —.85Nov                          144.25   —.85Jan                          144.35   —.85Mar                          144.45  +3.05Est. sales 1,614.  Thu.'s sales 471     Thu.'s open int 13,160,  up 32