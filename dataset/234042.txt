FieldWatch, Inc. is a non-profit company created to develop, expand and continue to innovate the DriftWatch Specialty Crop Site and BeeCheck Apiary Registry. FieldWatch relies on the outreach and educational efforts of its users and partners to spread the news about its stewardship platform.
In 2018, the company is celebrating 10 years since its founding and will hold a celebration in conjunction with the Midwestern Association of State Departments of Agriculture (MASDA) meeting in June.
FieldWatch is also celebrating additional developments. They are working to bring on a record number of new states into the FieldWatch Family in early 2018.
Stay tuned as FieldWatch will be launching two new apps in 2018: FieldCheck for applicators and BeeCheck for beekeepers.
Here are the new FieldWatch members in 2017:  

Ag Partners, LLC
Agritech Aviation
Blue Sky Spray
Borchers Supply
Centra Sota Cooperative
Stephanie Corbin
Crosswind Aviation Services
Delta Bee Co
Diversified Applications
Eldon Stutsman, Inc.
Farmers Cooperative
Gold Eagle Cooperative
Gowan Company
Industry Task Force II on 2, 4-D Research Data
Kokopelli Produce
Kubal Aerial Spray
Meyer Agri-Air, Inc.
Mid-Iowa Coop
Midwest Food Processors Association
Northeast Indiana Beekeepers Association
Oscar Aerial Spraying
Patriot Ag Air
Schneiders Milling
South Dakota Agri-Business Association
Speas Aviation
Dale Stouffer
Thompson Aero
Valent U.S.A. Corporation
Wickman Chemical