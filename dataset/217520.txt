Today Monsanto introduced its updated technology pipeline for seed, data and crop protection products. Products are in various stages of launch but aim to help farmers produce more in uncertain growing conditions.
The company says this is the fifth year of more than 20 pipeline advancements across its portfolio with seven projects advancing to launch. In addition there are more than 35 projects in the Climate FieldView pipeline with 17 advancements made in 2017.
Here are projects Monsanto announced:

Corn Technology:
	
3rd generation above ground insect control to provide corn earworm, fall armyworm, corn borer, black and western bean cutworm protection (launching)
3rd generation below ground insect control with three modes of action for corn rootworm control and a new RNAi mode of action (phase 4)
4th generation above ground insect control is mainly for South America to provide protection against lepidopteran pests and two modes of action for fall armyworm (advancing to phase 3)
4th generation below ground insect control (phase 2)
5th generation above ground insect control (advancing to phase 2)
3rd generation weed control with tolerance to glyphosate, dicamba and glufosinate (phase 4, submitted regulatory submissions for global approval)
4th generation weed control with tolerance to dicamba, flugosinate, glyphosate, FOPs and 2,4-D (phase 3)
5th generation weed control system that adds PPO herbicide tolerance (phase 1)


Soybean Technology:
	
2nd generation insect control—Intacta 2 Xtend will provide multiple modes of action against the podworm complex and expands protection to include armyworms (phase 4 will launch in 2021)
3rd generation insect control has multiple modes of action against an expanded spectrum of pests (phase 2)
3th generation weed control, XtendFlex soybeans, add glufosinate option to the Roundup Ready Xtend soybean system (phase 4)
4th generation weed control, displays tolerance to glyphosate, dicamba, glufosinate, HPPD and an addition mode of action (phase 2)
5th generation weed control, adds PPO tolerance to the 4th generation system (phase 1)


Cotton Technology:
	
Lygus and Thrips control biotech solution to control piercing and sucking pests (advancing to phase 4)
4th generation insect control, Bollgard 4, multiple modes of action against key pests and lepidopteran pests (advancing to phase 2)
4th generation weed control tolerance to up to five modes of action includes dicamba, glufosinate and glyphosate (advancing to phase 2)


Seed treatment:
	
Nemastrike provides nematode protection through a synthetic (opposed to bio) mode of action (advancing to launch)
Acceleron B-360 ST created as part of the BioAg Alliance to provide enhanced corn root development (advancing to phase 4)


Climate Corporation- at the core of its advances, Climate is employing more machine learning and predictive modeling to provide new options for farmers including:
	
Disease diagnosis
Seed scripting and selection
Fertility scripting