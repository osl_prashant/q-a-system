AP-OK--Oklahoma News Digest 1:30 pm, OK
AP-OK--Oklahoma News Digest 1:30 pm, OK

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Oklahoma. Questions about coverage plans are welcome and should be directed to the AP-Oklahoma City bureau at apoklahoma@ap.org or 405-525-2121.
Oklahoma Administrative Correspondent Adam Causey can be reached at acausey@ap.org or 405-996-1589.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times Central.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
.................
MEMBER FEATURES:
EXCHANGE-ROUTE 66
OKLAHOMA CITY — Representatives of cities in Oklahoma are coordinating efforts to redevelop part of Route 66 to boost economic potential. At about 400 miles, Oklahoma has more miles of the original Route 66 than any other state, according to the U.S. Department of Transportation. The corridor features a wide range of land uses and architectural styles in various states of repair. It's also at risk for decline and loss of physical remnants defining Route 66 character and heritage. By Brian Brus, Journal Record. SENT IN ADVANCE: 391 words.
EXCHANGE-ANGEL WINGS
NORMAN , Okla. — A local Oklahoma mom is using her faith-filled blog to inspire and encourage women and men struggling with infertility. S.J. O'Hara said she offers such inspiration out of the depths of her own six-year journey from the heartbreak of suffering several miscarriages to the joy of welcoming her "miracle" twins born via surrogate. O'Hara titled her blog "Angel Wings" because she had many "angels" to lift her up. By Carla Hinton, the Oklahoman. SENT IN ADVANCE: 822 words.
IN BRIEF:
— ARKANSAS-POWER LINE PROJECT: The U.S. Department of Energy has withdrawn support for a $2.5 billion power-line project through Arkansas opposed by landowners who feared they would be forced to sell their property against their will.
— VETERANS AFFAIRS HOSPITAL-OKLAHOMA: An inspector general's report says two construction projects at the Oklahoma City Veterans Affairs Medical Center are $10.8 million over budget and several years behind schedule.
— BODY FOUND-OKLAHOMA: Police in Tulsa say the discovery of a man's badly beaten body as a homicide.
— OKLAHOMA EARTHQUAKES: Two small earthquakes have shaken parts of northern Oklahoma.
___
If you have stories of regional or statewide interest, please email them to apoklahoma@ap.org and follow up with a phone call to 405-525-2121.
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867.
For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
The AP-Oklahoma City