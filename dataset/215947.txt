During the Federal Reserve’s final meeting of 2017, the strength of the economy provided enough fuel for the Fed to raise the benchmark interest rate for a third time this year.

Rates have increased one quarter of a point to 1.5 percent. Many anticipated and expected the move, and the only point of caution is a sluggish inflation rate.

With signals this inflation could continue to gain strength, the Fed is planning on increasing rate hikes in 2018.

Hear what ag bankers Alan Hoskins, president and CEO of American Farm Mortgage, and Keith Knudsen, president of Security Bank, have to say about this move on AgDay.