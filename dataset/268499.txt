AccuWeather reports an outbreak of severe thunderstorms, including the potential for a few tornadoes, is anticipated from portions of the central and southern Plains to the Mississippi, Ohio and Tennessee valleys spanning Friday and Saturday.
The eruption of thunderstorms will be inspired by a strong temperature contrast, fueled by daytime heating and a surge of Gulf of Mexico moisture and enhanced by strong winds and dry air aloft.
The same storm bringing the ingredients for severe weather together will also produce heavy snow and blizzard conditions in part of the north-central United States.

More than 22 million people will be at risk for severe weather on Friday alone. This day, the potential for damaging weather conditions will extend from near the Nebraska and Iowa border, southward to central Texas and eastward to Louisiana, Arkansas, Missouri and western Illinois.
During Friday, major cities at risk for violent storms and potential damage include Des Moines, Iowa; Kansas City and Springfield, Missouri; Fort Smith, Arkansas; and Dallas, Austin and San Antonio, Texas.

The storms will pick up forward speed on Friday night as the roll through Moline, Illinois; St. Louis; Little Rock, Arkansas; Memphis, Tennessee; Shreveport and Alexandria, Louisiana; and Houston.
The storms may bring the full spectrum of severe weather ranging from high winds, large hail, flash flooding, frequent lightning strikes and perhaps a tornado.
On Saturday, the severe weather setup is likely to be complex in that clouds and rain from Friday night's thunderstorms may race eastward and limit severe weather in parts of the Midwest and South, according to AccuWeather Senior Meteorologist Kristina Pydynowski.
"Two or three separate areas of severe weather may evolve on Saturday," Pydynowski said. "Where the sun is out long enough to allow some heating, ahead of the push of cooler air is where the greatest risk of severe thunderstorms will be."
A severe thunderstorm and flooding rainfall are possible in Peoria, Illinois; Indianapolis; Louisville; Kentucky; Jackson, Mississippi; New Orleans; and Mobile, Alabama; on Saturday.

One area may focus near and north of the Ohio River, while a second area is likely farther south, perhaps from Tennessee to the Gulf coast.
The risk of heavy, gusty and locally severe thunderstorms is likely to continue farther to the east on Sunday.
People from the eastern part of the Ohio Valley to the central and southern Appalachians and Piedmont should be on the lookout for rapidly changing weather conditions on Sunday. Downpours and gusty storms may affect the major airport hubs of Cincinnati, Pittsburgh, Nashville, Atlanta and Charlotte, North Carolina, to close out the weekend.
The NASCAR race at Bristol Motor Speedway, Tennessee, may be threatened by drenching downpours and gusty thunderstorms on Sunday afternoon.
A sweep of much cooler air will end the severe weather threat from west to east spanning Saturday to Monday.
Persistent well below-average temperatures has been a factor in lower-than-average tornadoes so far this year.
For example, in areas from Oklahoma City to St. Louis and Little Rock, temperatures have averaged 10 to 14 degrees below normal for April thus far. These areas lie within the heart of Tornado Alley.
As of April 10, based on preliminary reports, there have been 47 tornadoes this month, compared to a recent three-year average of 174 confirmed tornadoes, according to the National Oceanic and Atmospheric Administration.
The actual number of tornadoes through April 10 this year is likely to be less, once research has been completed.
During April 2017, when temperatures averaged above normal over much of the region, there were 211 confirmed tornadoes across the nation.
AccuWeather Storm Warning Meteorologists are estimating that between two and four dozen tornadoes may be reported with this multiple-day event. Most of the storms capable of producing tornadoes are likely to be from late Friday afternoon to Friday evening.