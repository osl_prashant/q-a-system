City worries about safety of Iowa plant's leftover stover
City worries about safety of Iowa plant's leftover stover

The Associated Press

NEVADA, Iowa




NEVADA, Iowa (AP) — Public safety officials are concerned about up to 500,000 bales of flammable stover that are stored across central Iowa and will be left over after a cellulosic ethanol plant is sold.
The Des Moines Register reports that the newly merged DowDuPont is selling its $225 million cellulosic ethanol plant in Nevada, leaving many residents asking what will happen to the remaining stover.
The plant's corn cobs, husks and stalks are a fire liability for the city and county. The leftover stover is scattered around 23 storage sites. A few bales catching fire could turn into a major blaze.
Ricardo Martinez is Nevada's public safety director. Hey says their concern is who will be responsible for the bales once the plant is sold. Martinez says DowDupont is working with the city.
___
Information from: The Des Moines Register, http://www.desmoinesregister.com