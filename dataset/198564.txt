International milk prices rose as volumes dropped at an auction on Wednesday, providing some hope for cash-strapped farmers.The fortnightly Global Dairy Price auction, held early Wednesday morning, showed prices had climbed 3.8 percent to $2,263 per tonne, the second consecutive auction of gains.
A total of 21,206 tonnes was sold at the latest auction, down 6.5 percent.
Analysts said lower supply was helping boost prices, but this might in turn lead more people into the market.
"The lower volumes on offer helped to support WMP (whole milk) prices but prices for skim milk powder are still weak due to the large quantities of this product currently being produced in the European market," said dairy analyst Susan Kilsby in a research note.
"But if WMP prices continue to improve then European processors are likely to produce more WMP."
Whole milk powder prices rose 7.5 percent while skim milk powder inched up 0.3 percent.
The New Zealand dollar rose to $0.7053 from as low as $0.6950 the previous day.
The auction results affects the New Zealand currency as the dairy sector generates more than 7 percent of the nation's gross domestic product.
An around 60 percent fall in dairy prices since early 2014 has also hit the country's economy.
The sector was until recently the backbone of the economy, representing around 25 percent of exports, but in the past two years farmers have had NZ$7 billion ($4.74 billion) wiped off their collective revenue.
The auctions are held twice a month, with the next one scheduled for May 3.
The auction platform was set up by Fonterra and is operated by trading manager CRA International.
Participants include Fonterra, Amul, Arla Foods, Arla Foods Ingredients, DairyAmerica, Euroserum and Murray Goulburn. Products traded on the twice-monthly auction include whole milk powder, skim milk powder, butter, cheese, anhydrous milk fat, among others.