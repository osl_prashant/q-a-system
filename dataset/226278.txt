After a three-day weekend, the soybean market set fresh multi-month highs.

On Tuesday, soybean futures continued to rally on weather pressure from Argentina’s soybean crop.

March futures surged past $10.30 per bushel to hit an 11-month high, and pulled back later in the day. Soybean meal has also been on the rise.

Hear from Jack Scoville, vice president of Price Futures Group, discussing the weather conditions in South America on AgDay above.