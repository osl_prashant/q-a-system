BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Wednesday:
OpenHighLowSettleChg.ORANGE JUICE15,000 lbs.; cents per lb.Apr139.40+1.40May138.45140.90138.45139.40+1.40Jun139.40+.80Jul139.65141.00139.05139.40+.80Sep141.00141.50139.60139.90+.60Nov141.30141.30140.40140.65+.45Jan141.35141.60141.35141.40+.35Feb142.55+.15Mar142.55+.15May143.00+.15Jul143.10+.15Sep143.20+.15Nov143.30+.15Jan143.40+.15Feb143.50+.15Mar143.50+.15May143.60+.15Jul143.70+.15Sep143.80+.15Nov143.90+.15Jan144.00+.15Est. sales 624.  Tue.'s sales 552Tue.'s open int 12,212,  up 168