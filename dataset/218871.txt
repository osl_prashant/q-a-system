Did you miss out on Veganuary?
I did, although according to one media outlet, “thousands” of Britons didn’t.
In an article titled, “Meat feasts to celebrate the end of Veganuary,” The Telegraph newspaper in Great Britain noted that, “The start of 2018 was dominated by Veganuary, with thousands pledging to give up animal products for a month for ethical, environmental or health reasons.”
Not sure that “dominated” is an accurate characterization of anything that “thousands” of people did, not when it occurred in a nation of more than 65 million people.
Nevertheless, the story offered the assertion that, “Many will continue to [give up animal foods] for the rest of the year.”
Really? I find that statement to be more aspirational than actual.
But regardless of how many, or how few, people actually participated in Veganuary — and for those who claim they did, I’m reminded of my many Catholic relatives who also “claim” to have observed the restrictions imposed during the Lenten season each year — the culinary suggestions published in The Telegraph provide a wonderful opportunity to compare vegan recipes head-to-head with meat-based entrées.
Seven on Seven
So let’s do that. Here are seven of the most appetizing Veganuary Specials highlighted by some presumably well-known chefs who provided descriptions and recipes for people to follow:

Layered No-bake Raw Pesto Tart (made with a carrot-and-walnut crust, cashew cream, spinach and basil)
Thai-inspired Spiralized Salad (made with peeled cucumbers, zucchini, cashews and mango)
Marinated Mushrooms (made with miso, lime juice and tamarind, described as “a sour, dark leguminous fruit”)
Strawberry and Black Pepper Slump (a stovetop cobbler made with coconut custard)
Squash Pasta (prepared with sage and pine nuts)
Mushrooms and Chickpea Stew (simmered with garlic and tomatoes)
Hummus with Shredded Carrots (served on rye bread points)

I’ll give the veganistas this much: They do a more-than decent job with culinary creativity as far as naming these dishes, with the possible exception of that last one — for hummus and carrots, about all you can say is that it’s exactly what it sounds like.
Honestly, some of those veggie creations sound pretty interesting — if we’re talking about a side dish or a dessert. None of them strike me as a mouth-watering entrée I can’t wait to dive into on some special occasion.
But that’s just me. You should judge for yourself.
So here are the “traditional” entrées for diners, who, as the story phrased it, “survived the month and are gasping for a rasher of bacon or some succulent roast beef.” Roast beef isn’t on the list, but check out the meat-based alternatives to the Veganuary menu that did make the cut.
Here are the top seven “meat feasts” The Telegraph’s food editors chose for people who endured a month of vegetarian meals:

Roast Pork (prepared with black grapes, rosemary and juniper)
Salmon Fillets (made with miso shiitake mushrooms and bok choy)
Baked Sausages (with apples and blackberries, with mustard and maple syrup)
Braised Beef Tacos (made with brisket)
Roast Venison Loin (with potatoes, porcini mushrooms and cavelo nero — ie, black kale)
Lamb Pie (made with feta cheese and spinach)
Pan-fried Ribeye Steak (needs no further explanation)

Arguably, the choices described above would cost more than some of the veggie alternatives. I mean, salmon or venison definitely command a higher price-per-pound premium than carrots or squash.
But there’s a reason for that: carrots belong in a small pile off to the side of the plate; mushrooms are supposed to be nestled in gravy generously applied on top of a pan-fired ribeye steak. Neither has the culinary clout to take over the role of main dish in a meal.
All things considered, I would agree that vegetarian alternatives are an intriguing change-of-pace from beef, pork and lamb, although if I were confined to dining only on the seven meat dishes listed above, it would take way longer than a month for me to get tired of any of those choices.
So for anyone who embraced Veganuary, kudos for being adventurous.
And welcome back to a meaty menu for the other 11 months of the year.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.