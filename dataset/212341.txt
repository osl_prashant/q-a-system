U.S. fresh market apple holdings on June 1 were up 12% over year-ago levels and 5% greater than the five-year average, according to the last monthly report from the U.S. Apple Association this season.  
Total fresh apple inventories on June 1 was 25.4 million cartons, 12% above the same time a year ago but 15% lower than two years ago, according to the association.
 
The U.S. Apple Association plans to release the 2017-18 crop estimate Aug 25, and the first storage report for that crop will be issued in early November for Nov. 1 holdings, said Mark Seetin, director of regulatory and industry affairs with McLean, Va.-based U.S. Apple. 
 
Washington state accounted for 24.03 million cartons (95%) of total U.S. fresh apple holdings on June 1, according to the report. 
 
The report said June 1 gala fresh inventories were 3.97 million cartons, up 64% from 2.42 million cartons from last year but off 3% from 4.09 million cartons in 2016. The average f.o.b. price on June 3 for Washington traypack galas was $23.04 per carton, according to the USDA, compared with $35.98 per carton at the same time a year ago.
 
Red delicious inventories of 11.8 million cartons were up 39% from a year ago but down 6% from 12.59 million cartons two years ago. The USDA reported Washington traypack red delicious had an average f.o.b. price of $14.42 per carton, off from $23.98 per carton the same time a year ago.
 
On the short side, granny smith inventories on June 1 were 2.117 million cartons, off 54% compared with 4.645 million cartons a year ago. 
 
“It looks like the industry has been doing a good job of moving apples,” Seetin said June 7.
 
The average f.o.b. price for Washington traypack granny smith apples on June 3 was $26.83 per carton on June 3, substantially up from $22.95 per carton the same time a year ago.
 
“I think the industry has done a really good job of marketing slightly larger crops,” Seetin said. “If you take a look back six or seven years ago and had that kind of a crop, you would have had a much more significant price impact.”
 
Better demand for preferred varieties and consumer acceptance of new varieties has helped build apple per capita consumption close to a 20-year high, he said.