When it comes to table grapes, many retailers have a preference for fruit from California.
“There are a lot of good things happening in the California grape category,” said Dennis Chrisman, vice president of produce and a store director for Dorothy Lane Market, a group of three stores based in Dayton, Ohio.
The stores display as many as 30 cases of grapes during promotions, he said.
Red grapes dominate the category for conventional and organic, Chrisman said.
The stores have a penchant for options beyond the traditional standbys.
“We love a lot of the new varieties of grapes,” he said.
The company sources a number of specialties, like Cotton Candy, Moon Drops, Flavor Promise and Sweet Celebration, from The Grapery in Shafter, Calif.
Shoppers at Ralph’s Red Apple, Bremerton, Wash., tend to prefer California grapes “because they’re more local,” said produce manager John Walker.
Walker merchandises grapes on an end cap year-round, but builds out the display when they’re on ad, which typically is about every three weeks.
In early June, he had two cases each of red, green and black grapes on display.
Green is the most popular variety at Ralph’s Red Apple, but not by much, Walker said.
Customers at the Hy-Vee Inc. store in West Des Moines, Iowa, were asking in May when the California grapes would come in, said produce manager Roman Tieg.
“I think it makes a huge difference for consumers,” he said.
Tieg said the size of his table grape display in summer is twice the size of his winter display.
“Grapes are probably one of the top categories in the store,” he said.
In all, he said he displays about 40 cases at a time “prominently upfront” in summer.
“We go through quite a few,” he said.
“I give them the space they deserve.”
 
Variety mix
Hy-Vee usually alternates between red and green grapes on ad each week and includes a specialty variety like Cotton Candy when available.
Dorothy Lane Market features at least one variety on ad about every other week during the summer, Chrisman said.
The stores sell conventional red and green varieties for the same price when they’re not on ad, and usually sell specialty varieties and organic grapes at separate price points.
If they have more than one specialty variety, Chrisman said he usually sets the same price for each.
In the past, when grapes were sold in bulk, he tried to keep them all at the same price to prevent confusion at checkout.
Now that most suppliers ship grapes in pouch bags with Price Look-Up numbers, it’s easier to set different price points for different varieties, he said.
Ralph’s Red Apple has three big tent sales in June, July and August where Walker displays about a pallet of grapes.
Sale price typically is $1.49 a pound versus a regular price of $3.79.
Sales rise about 25% during the summer compared to the rest of the year, he said.         
Walker sometimes puts a display of grapes near the cash registers or merchandises them on a slant rack outside the front door.
“We don’t want to hide them,” he said.
Grapes rank in the top five produce items in summer and in the top seven during the rest of the year.
“They’re definitely a staple,” he said.
Occasionally, Walker orders 2-pound clamshell containers as well as pouch bags.
“It’s a nice grab-and-go item,” he said.
Walker samples specialty grapes, such as the concord variety, when they’re available, he said.
Red grapes are more popular than green ones at Hy-Vee, Tieg said, but green varieties might inch out the red when they’re on sale.
The store features grapes on ad for 99 cents or less compared to a regular price of $1.99-2.99 during the summer. They retail for as much as $4.99 during the winter.
Tieg said he also pushes organic grapes during the summer, when they’re more readily available.
“It’s a growing category,” he said.