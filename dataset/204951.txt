What Causes Rapid Growth Syndrome?
Rapid growth syndrome occurs when corn leaves fail to unfurl properly and the whorl becomes tightly wrapped and twisted.
It is generally associated with an abrupt transition from cool temperatures to warmer conditions, resulting in a sharp acceleration in plant growth rate.
The rapidly growing new leaves are unable to emerge and will cause the whorl to bend and twist as they try to force their way out.



Corn plants in rapid growth phase showing wrapping of leaves at the whorl. Affected plants typically recover and resume normal growth.
Rapid growth syndrome most commonly occurs at the V5-V6 growth stage, but can be observed as late as V12.
As with many weather-related stress effects, it is common for some hybrids to be more prone to rapid growth syndrome than others.
Twisted whorls can also have other causes, most notably herbicide injury.
Growth regulators and acetamides are the herbicides most commonly associated with twisted whorls or "buggy-whipping."
Other herbicides may also interfere with leaf unfurling in rare cases.







Leaves that have been trapped in the whorl will often be bright yellow whenthey emerge, making them very noticeable in the field.

What is the Long-Term Impact?
Leaves of affected plants usually unfurl after a few days.
Newly emerged leaves will often be yellow as a result of being twisted up inside the whorl, but will green up quickly once exposed to sunlight.
Affected leaves may be wrinkled near the base and will remain that way throughout the growing season.
Development of individual plants may be slightly delayed due to rapid growth syndrome; however, yield is unlikely to be reduced.