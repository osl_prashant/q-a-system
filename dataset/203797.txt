(CORRECTED): Pacific Organic Produce has entered into a partnership with a Mexican grower-shipper.
The San Francisco-based Pacific Organic is working with Mexus Produce.
The Rancho Cucamonga, Calif.-based Mexus grows and ships a variety of items including certified organic cucumbers and squash, peppers, eggplant and brussels sprouts from Mexico.
Since 2010, Mexus has been growing organic vegetables in Baja and western Mexico and has created a network of year-round supplies of product through its Costa Azul Organics operation, said Allen Bernklau, Pacific Organic's senior commodity development and sales specialist.
Mexus recently began construction of a new packing facility in Ensenada, Mexico, which is south of Tijuana.
The packing operation is being built to handle increased production and to maintain the company's quality control, according to a news release.
Mexus product will be marketed along with Pacific Organic's warm weather vegetables and tree-fruit and offer a single source for organic fruits and vegetables, according to the release.
"We are really excited to join up with Mexus as this is a perfect fit and mutually beneficial to both companies," Bernklau said. "They are strong south of the border and we are strong north. We are looking forward to a long and strong partnership."

A part of the Providence, R.I.-based United Natural Foods Inc., Pacific Organic markets through the Hope and Purity Organic labels.