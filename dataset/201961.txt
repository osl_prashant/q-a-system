Farmers may have lingering questions about how much nitrogen is left in their fields after the incredibly wet spring the Midwest has had. They may even be wondering if they need more starter if they are replanting.Brian Krienke, soils Extension educator, University of Nebraska, has posted a Q&A article examining these questions and other crop fertility and crop nutrient concerns farmers may be having in the Midwest right now.
He explains that managers are most concerned with ensuring there is enough N in the field, and that what is applied such as fertilizer, manure, and irrigation water (inputs) is not lost to leaching, volatilization, and denitrification (outputs).
Other questions he answers include the following:
What are the University's objectives when recommending N management strategies?
But, what if I used the UNL algorithm or some other recommendation, applied a lot of N, and now am uncertain how much N is still there? Now, what do I do?
I applied starter fertilizer earlier this year, but now I have to replant. Should I use starter again?
To read Krienke's answers, click here.