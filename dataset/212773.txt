Democrats in the U.S. Senate have introduced legislation that would shield agricultural workers from deportation and give them a path toward legal status. 
On May 3, Sens. Dianne Feinstein, D-Calif., Patrick Leahy, D-Vt., Michael Bennet, D-Colo., Mazie Hirono, D-Hawaii and Kamala Harris, D-Calif., introduced legislation  titled the Agricultural Worker Program Act.
 
The bill would give farmworkers who have worked in agriculture for at least 100 days in each of the past two years the chance to earn lawful “blue card” status, according to a news release. Those workers who maintain blue card status for the next three or five years — determined by the hours worked in agriculture — would be in line to receive a green card or legal permanent residency, according to the release.
 
Agriculture is a $54 billion industry in California, and the University of California-Davis estimates that up to 70% of farmworkers there — roughly 560,000 people — are undocumented. That puts them at risk for deportation under Trump administration guidelines, according to the release.
 
“Everywhere I travel in California, I hear from farmers, growers and producers from all industries — wine, citrus, fruit and tree nuts, dairy — that there aren’t enough workers,” Feinstein said in the release.
 
While not explicitly endorsing the legislation, Western Growers president and CEO Tom Nassif said the legislation recognizes the importance of retaining the existing agricultural workforce.
 
“To reform our broken immigration system, Congress must pass bipartisan solutions that acknowledge the contributions and value of current farmworkers while also creating a workable program to enable the future flow of labor to American farms,” Nassif said in a statement. “We are hopeful that those members of the legislature that are working on agricultural immigration legislation accelerate the process because time is definitely of the essence and our labor situation has reached a critical stage.”