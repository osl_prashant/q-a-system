A company specialized in modified-atmosphere packaging has introduced new equipment for produce.
Point Five Packaging’s P5-A Automatic Seal System can produce up to 60 sealed trays and 32 MAP trays a minute, according to a news release from the Schiller Park, Ill., company.
The machine can be configured in multiple ways, according to Point Five Packaging, with options including filing and depositing systems, lid applicators and lane merging systems, according to the release.
Point Five is the North American distributor for Italian Pack S.r.l. of Como, Italy, and the P5-A is designed for the North American market.
The new equipment cuts cost and improves productivity, according to the company.
“The P5-A Automatic Seal System exemplified this level of innovation, flexibility and cost-effectiveness — three key factors in successful food packaging operations,” Greg Levy, Point Five Packaging president, said in the release.