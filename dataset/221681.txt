Don’t call Southern Exposure a big produce show.
The Southeast Produce Council’s annual event may have grown in attendance and prestige, but at its heart, organizers say it’s all about the community.
“Our people make it feel that way,” said David Sherrod, president and CEO of the council. “It’s the people that we started with years ago, and they’ve kept that feeling in their hearts.”
From a small, regional start-up in 2004 to today, many of the same faces continue to be involved with the core workings of the organization, and its premiere event.
Southern Exposure is scheduled for March 1-3 in Tampa, Fla., at the Tampa Convention Center, with more than 500 buyers expected to be among the expected 2,500 attendees.
And while the attendee numbers continue to grow, Teri Miller, last year’s chairwoman of the board for SEPC, said the organization is careful to make sure the event maintains its value proposition for attendees by keeping the show floor manageable.
“Everyone says we keep getting bigger, but our booth count remains under 280,” she said.
“The council is 100% committed to keeping the show to this size. We know that there is only so much time that we can commit to when we are away from our jobs.” 
This year’s chairwoman, Faye Westfall, vice president of DiMare Fresh Tampa, said the council’s membership strongly supports keeping the show an intimate experience.
“I think this is due to such a great membership that allows the council to keep the intimacy and charm while allowing the board members to make decisions that impact the growth,” she said.
Sherrod said organizers are focused on not only bringing the community together, but also bringing value, and they’ve got a big name keynote in John Mackey, co-founder and CEO of Austin, Texas-based Whole Foods Market Inc.
“We’re excited to have Mr. Mackey speak this year,” Sherrod said.
 “Especially with the merger (with Amazon), we think that it’s going to be of interest to the grower-shipper and the retail side of the business.”
Along with the Elvis-themed trade show, a packed education program, and plenty of time for networking, Sherrod said 2018’s Southern Exposure “has something for everybody who’s attending this year.”