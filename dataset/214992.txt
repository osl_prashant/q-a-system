According to this recent poll, 58% of people celebrating Thanksgiving “dread the thought of having to talk about politics at Thanksgiving dinner.”  That’s slightly up from the same poll a year ago.
So, how about a little more fun?
Here are nine light-hearted jokes—all turkey related but not even close to being serious or political hot potatoes.

Who is never hungry on Thanksgiving?

The turkey because he’s already stuffed!


What key won't open any door?
 

A tur-key


What do you call it when it rains turkeys?
 
 

Fowl weather


What happened when the turkey got into a fight?
 
 

He got the stuffing knocked out of him!


Which side of a turkey has the most feathers?
 
 

The outside!


If you call a large turkey a gobbler what do you call a small one?
 
 

A goblet


What’s the most musical part of a turkey?
 
 

The drumstick


Why did the turkey cross the road?
 
 

It was the chicken’s day off.


What do you get when you cross a turkey and a banjo?
 
 

 A turkey that can pluck itself.