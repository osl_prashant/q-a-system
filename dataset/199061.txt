The Canadian cattle herd has increased 0.3% in the past year to nearly12 million head.On January 1, Canada's cattle herd totaled 11,960,000 head. The same time in 2015 saw the herd count at 11,920,000 cattle. This is the first increase in herd numbers since 2013.
Canada's cattle numbers are still 19.9% lower than the peak seen in January 2005.
Heifer retention increased as producers in Canada rebuild their cowherd. This year 547,000 heifers were held back, a jump of 4% and the first increase in three years.
Manitoba and British Columbia were the provinces that saw the largest increase in cattle. The far western province of British Columbia was up 1.8% in the past year, while Manitoba was up 2.3%.
Feeder cattle inventories are down in Canada with 1.2% fewer heifers entering feedlots and 0.7% less steers filling pen space.
Cattle slaughter and live exports were down in 2015. Live export of cattle, the majority of which comes to the U.S. was down to 831,000 head in 2015, a drop of 33.2%. Domestic Canadian slaughter declined 8.2% to 2.9 million head last year.

The inventory was released on March 3 and compiled by Statistics Canada's CANSIM, a government database. A livestock survey was conducted amongst 9,000 Canadian farms from November 26 to December 31, 2015.
A release by Statistics Canada goes onto say, "Canadian cattle prices were strong in the first half of 2015, but moderated in the second half of the year. Relatively strong prices reduced the incentive for Canadian producers to send their cattle south."
In the U.S. herd numbers were also on the upswing. U.S. Department of Agriculture's inventory shows a 3% increase and that helped slow the demand for cattle from north of the border. Overall, the U.S. has approximately 80 million more cattle than Canada.