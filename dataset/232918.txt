Cattle abuse reported at ranch owned by former lawmaker
Cattle abuse reported at ranch owned by former lawmaker

The Associated Press

BILLINGS, Mont.




BILLINGS, Mont. (AP) — Montana authorities are investigating reports that cattle were deprived of food and water on a ranch owned by a former state lawmaker.
Rosebud County Sheriff Allen Fulton says authorities received an anonymous report of animal neglect in January. An inspector confirmed that report with a visit to the ranch owned by former state Rep. Don Roberts.
Fulton says Roberts has cooperated with authorities and immediately fired the manager of the property.
Roberts told the Billings Gazette that his attorney advised him not to answer questions.
Fulton says police are considering charges against the former ranch manager. He wouldn't discuss details of the abuse or neglect accusations, including whether any animals died.
Fulton says Roberts hired a new manager for the ranch and the animals are now being cared for.