Update marks shift in assessments compared to the April version






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






Economic activity continued to expand at a "modest or moderate pace" from early April through late May, though not all Federal Reserve districts shared that sentiment, according to the Beige Book, an anecdotal update of economic conditions in the 12 Federal Reserve Bank Districts. Link.
The exceptions on economic activity: "Boston and Chicago signaled that growth in their Districts had slowed somewhat to a modest pace since the prior Beige Book period, while New York indicated that activity had flattened out," according to the report prepared by the Philadelphia Fed based on information received prior to May 22.
Consumers not as active: "Consumer spending softened with many Districts noting little or no change in nonauto retail sales, while auto sales have edged down from last year's record highs in several Districts; tourism activity has continued to keep pace with the general economy."
Labor markets tighten: Most districts reported "shortages across a broadening range of occupations and regions," the report said, noting that "most firms across the Districts noted little change to the recent trend of modest to moderate wage growth, although many firms reported offering higher wages to attract workers where shortages were most severe."
Housing growth still noted, both in construction and sales: "Construction of new homes and nonresidential structures also continued to grow at modest to moderate rates, as did sales of existing homes; nonresidential leasing picked up a bit. Lending volume trends tended to mirror (and support) the general activity of the economy." Low inventories homes for sale "were pushing house prices higher in many markets," the report said.
Energy optimism, but…: "Most energy sectors tended to modestly improve. A majority of Districts reported that firms expressed positive near-term outlooks; however, optimism waned somewhat in a few districts."
Inflation? Pricing pressures were "little changed from the prior report, with most Districts reporting modest increases," the report observed. "Rapidly rising costs for lumber, steel, and other commodities tended to push input costs higher for some manufacturers and the construction sector. In contrast, some Districts noted falling prices for certain final goods, including groceries, apparel, and autos." Energy prices and farm prices were mixed across products and among Districts.
US dollar: The US dollar value was still mentioned as a limiting factor, but only by the Cleveland (industrial production) and San Francisco Fed districts (agriculture and manufacturing).
Comments: While the report has some cautionary components on the inflation side and the overall economic activity side in three Fed districts, it still is a report suggesting US economic activity continues to expand even as consumers are still not overly active participants. The CME FedWatch Tool is now north of 93 percent odds for a June rate increase. In other words, it is going to take a major shift in economic data between now and then to shift market sentiment away from the range on the Fed funds rate rising to 1 percent to 1.25 percent when the Federal Open Market Committee (FOMC) meeting concludes June 14.


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.