Cattle producers will have an opportunity to see how Angus-sired cattle from their operation perform in a feedlot during the North Dakota Angus University (NDAU) calf feed-out program this summer and fall.
Producers also will be able to learn about the potential profitability of keeping calves and feeding them instead of selling them after weaning or backgrounding.
North Dakota State University’s Carrington Research Extension Center is partnering with the North Dakota Angus Association to sponsor NDAU. This is the seventh year of the program.
Producers will be able to consign cattle to the feed-out program. Consigned animals should be steers with at least 50 percent Angus genetics and a desired target weight of 800 and 900 pounds at the time they enter in the program. Producers wishing to consign steers that are lighter than 800 pounds should contact Karl Hoppe, the center’s Extension livestock systems specialist, or Bryan Neville, animal scientist at the center, to discuss potential options.
Consigned cattle should be delivered to the center’s feedlot the first full week of June (4-8).
Producers who consign cattle pay the feeding costs based on the average cost of gain, veterinary costs and a modest yardage charge. The center carries these costs until the cattle are marketed. After the cattle are marketed, the center deducts all applicable fees from the sale price without an interest charge.
Participants in NDAU will receive periodic progress reports on their calves’ performance, as well as a final report on the overall performance, efficiency and carcass traits for their calves.
LaMoure-area producer Mike Wendel has been consigning calves to NDAU since it began.
“One benefit is getting carcass data back,” he says. “We also can compare how we are doing with other producers so we see how we fit and rank, compared with other producers. Also, we can compare input costs and rate of gain.”
Producers consigned 78 head of steers to the sixth annual NDAU project, held in the summer and fall of 2017.
“In addition to the valuable information producers received regarding the feedlot performance of their cattle, the steers were used in a feedlot research trial,” Hoppe says.
In the 2017 feed-out program, cattle:

Averaged 105 days on feed
Gained an average of 4.8 pounds per head per day
Ate 5.6 pounds of dry-matter feed for a pound of live weight gain

To consign a group of cattle or for more information, contact Hoppe or Neville at 701-652-2951 or karl.hoppe@ndsu.edu or bryan.neville@ndsu.edu, or call Wendel, North Dakota Angus Association program liaison, at 701-710-0425.