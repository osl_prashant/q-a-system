3 North Korean defectors visit Delaware to express thanks
3 North Korean defectors visit Delaware to express thanks

By MARGIE FISHMANThe News Journal
The Associated Press

WILMINGTON, Del.




WILMINGTON, Del. (AP) — Among North Korea's most wanted is a 13-year-old boy with a bowl haircut who wears Nikes and steals glances at his smartphone.
Flanked by two North Korean activists who had arranged for his escape from a brutal regime, last month Kim Moon Hyuk — who uses an alias to protect his identity — was introduced to some of the Cab Calloway School of the Arts students who funded his lifesaving journey.
Wiping away tears, their shoulders shaking, a half-dozen students in Cab's global humanitarian club encircled Kim in a Newark living room Tuesday afternoon. Outside, frozen raindrops pelted the driveway, a reminder that this intimate gathering would have never come to pass had a nor'easter not intervened.
Earlier in the day, the North Koreans appeared at a human rights forum hosted by Appoquinimink High School. A public event at the University of Delaware that evening was canceled due to the storm.
As a result, Kim made an impromptu visit to the home of a Cab teacher to meet with his benefactors. Just a few years ago, he assumed that all Americans were "bastards," using their images to practice toy shooting games.
He admitted that he was nervous to set foot on U.S. soil for the first time.
"I heard about the gun violence in America," Kim explained through an interpreter. "That's why I'm afraid."
Ruled by one family with an iron fist for 70 years, North Korea has been called the Hermit Kingdom and the most isolated country in the world, where starving families commit suicide while the elite frequent luxury Pyongyang department stores. More than 31,000 North Korean defectors entered South Korea from 1998 to 2017, according to South Korea's Unification Ministry.
Cab's 60-member club, P4, sold more than $2,800 in holiday cards over two years to fund a major leg of Kim's journey from China to South Korea. The boy had been diagnosed with leukemia, according to his mother, and was left to deteriorate in North Korea, where it's estimated that 40 percent of the country's 25 million people live below the poverty line.
"I'm living every day to prepare for unification," Kim, a cancer survivor and budding activist, told the students. "I want to help North Korea and South Korea become one country."
"You make it possible to have this vision and this dream."
"It was just so surreal that we could meet someone who could've died," said Cecilia Tadlock, a Cab junior who is studying Korean and wants to devote her career to advocating for the North Korean people.
"One by one," she said, "we're helping a country."
How a trio of North Korean defectors, including one recently highlighted in President Donald Trump's State of the Union Address, wound up in tiny Delaware on this frigid first day of spring is a tribute to the personal relationships forged years prior.
In 2015, Ji Seong-ho, a North Korean double amputee with uncrushable courage, was a featured speaker at the Oslo Freedom Forum.
After watching a video of Ji's speech on Facebook, Wilmington businessman Brian DiSabatino was shocked not only by Ji's harrowing story, but by the neglected humanitarian crisis in North Korea.
"When you hear the story, you have to be dead not to be moved by the struggle, the strength and the faith this kid has," DiSabatino said of Ji, who is now in his mid-thirties.
A few months later, DiSabatino, the chief executive of EDiS, a major construction management firm in Wilmington, coordinated Ji's first trip to Delaware. The polite, bespectacled activist toured Longwood Gardens, dined at the Charcoal Pit, met with Delaware's congressional delegation and spoke at multiple schools.
"The intention was to bring a ripple of understanding and empathy," DiSabatino remembered.
Cab piano teacher Margaret Badger, who founded P4, attended one of those assemblies. Over the years, the school club has funded literacy efforts in Pakistan and supported Syrian refugees and Rohingya refugees on the border of Myanmar and Bangladesh.
Badger sprang into action, enlisting students to design holiday cards and gather flash drives for North Koreans with information about democracy and the West. In North Korea, the Internet is highly restricted.
"As a teacher, what I'm most grateful for is that students believe that they can make a difference," she said recently.
DiSabatino never pegged himself a "social justice warrior." Yet Ji personalized the North Koreans' plight in a way that elicited a visceral response. Movies like "The Interview," which pokes fun at the assassination of North Korean leader Kim Jong Un, barely scratched the surface. News alerts about the growing nuclear threat posed by North Korea ignored daily life under a totalitarian state.
Ji exemplifies the "power of one person." said DiSabatino.
Now an international human rights icon, Ji grew up during the North Korean famine of the mid-1990s that killed 1 million people, including his grandmother. He and his family survived on roots and corn stalks. They fought rats for the seeds stashed in their burrows.
At age 14 and just 45 pounds, Ji jumped between freight trains to steal coal. One time, faint with hunger, exhaustion and coal dust in sub-zero temperatures, he blacked out.
When he awoke, he was lying on the tracks, his left leg dangling by a tendon. He recalls trying to stanch the gushing blood, before realizing that three of his fingers had also been sheared off when a train ran him over.
Ji underwent a four-hour operation without painkillers or blood for transfusions, shrieking in agony as the surgeon sawed off bones. During his recovery, his family bought penicillin on the black market, he recalls, bearing the labels of a misappropriated UN aid shipment.
Ji says he spent 240 days in constant pain and considered killing himself to no longer burden his family. His siblings gave him their meager rations, which stunted their growth.
Crippled, Ji slipped into China, where "common animals were eating better than the humans in North Korea," to beg for a few kilograms of rice.
On the way out, he was spotted by North Korean police, who confiscated his rice, beat him viciously and scolded him for "embarrassing our nation," Ji remembers. In North Korea, the disabled are viewed as inferior and exist in the shadows, he said.
That injustice motivated Ji and his brother, in 2006, to cross the Tumen River into China to begin an arduous, 6,000-mile trek through Laos, Myanmar and Thailand and eventually, by plane, to South Korea.
Ji nearly drowned that day on the river, after falling into a deep seam. His brother yanked him out by his hair, he recalls. The pair later separated at Ji's urging.
At one point on the journey, Ji was surrounded by Chinese police on a train. North Koreans passing through China aren't considered refugees, but illegal trespassers. Once they are sent back home, they are tortured in labor camps.
Not able to speak Chinese or present valid identification, Ji bowed his head in prayer and stood silent. The three officers shot him disdainful looks but eventually left, he recalls.
Ji learned later that his traveling helper had told police that Ji was mentally ill and suffered from a movement disorder.
"If you touch him, he will go crazy and jump off the train," the helper warned police.
Another time, while hobbling across the jungle, Ji cursed his lot as a North Korean when there are so many other progressive countries around the world. He vowed to work toward a reunified Korea so that no one would be forced to repeat his miserable journey.
Ji's father, who tried to escape after the rest of the family, was caught and tortured to death. Officials carted off his body in a wheelbarrow and dumped it in the family's empty house.
Upon arriving in South Korea, Ji received an artificial arm and leg courtesy of the South Korean government. At first, it was difficult to adjust, he remembered. In North Korea, "thank you" is reserved for the "Dear Leader."
Over time, Ji earned his law degree and established NAUH, Now Action and Unity for Human Rights, a Seoul-based nonprofit that has rescued 270 North Korean defectors over seven years, from babies to senior citizens.
Private brokers charge upwards of $4,000 for the journey, which is paid out of South Korean welfare benefits that are provided to defectors when they enter the country. NAUH charges about half that amount, Ji said, working through an underground network. The organization doesn't track where the defectors eventually settle.
NAUH also works to connect youth with quality education, camp activities and counseling. Some of these children are human trafficking victims, who were living under the radar in China due to the threat of being repatriated back to North Korea.
In January, Ji's efforts were recognized by Trump in his State of the Union Address. The president praised Ji's resilience as a "testament to the yearning of every human soul to live in freedom."
DiSabatino happened to be in the audience for the State of the Union that night, after requesting a spare ticket from his friend, U.S. Sen. Chris Coons. He watched as Ji triumphantly raised his makeshift crutches, his final gift from his father.
Before the 2016 election, Trump was criticized for mocking a disabled reporter. Asked if he was concerned about the president's attitude toward the disabled, Ji replied: "I felt that he was very sincere in his meaning of solving human rights violations."
Both Ji and Kim were joined this week by Cholnam Choi, who, like Kim, goes by an alias because he fears government retribution against his family still living in North Korea.
Gathered in Badger's home, the Cab students and several adults, including State Rep. Paul Baumbach, were warned not to snap any photos with Kim or Cholnam. A few shared images using cartoon smiley faces to mask the defectors' identities.
Now 29, Cholnam escaped to South Korea 12 years ago, after hiding in an international school in Beijing. He eventually reunited with his father, whom he hadn't seen in 15 years. After her husband defected, Cholnam's mother worried about his safety. She told her toddler that his father had died
"It's been too long for me to be proud of anything in North Korea," Cholnam said through an interpreter.
The Delaware visit was part of a five-day tour that took the defectors to the U.S. Department of State and to New York City, where Ji met with a publishing company to discuss a possible biography.
International sanctions continue to batter the North Korean economy as commodity prices soar, Ji said. But he is optimistic that the flow of information from defectors and from North Korean workers exported to Russia has pierced Kim Jong Un's propaganda machine.
"They are not loyal," Ji said of average North Koreans. "The real life in North Korea is 1,000 times worse than what I'm telling people."
Delaware, by contrast, is "quiet," ''friendly" and welcoming," he said. During the recent visit, the group ate at Bob Evans and Walt's Chicken and shopped at the Christiana Mall, where Kim bought a wallet with an integrated cell phone charger.
Back in South Korea, Ji only communicates with North Koreans who use Chinese cell phones near the border. Any other interactions would place them in grave danger, he said.
Asked if he thought a planned Trump-Kim summit this spring would result in meaningful change, Ji expressed reservations. The North Korean regime is inherently untrustworthy, he said. Lipstick diplomacy is the norm.
If talks do happen, Trump would be the first-ever sitting American president to meet a North Korean leader.
DiSabatino, now a board member for NAUH, said he was hopeful — but pragmatic — about a possible truce.
"The idea that a maniacal madman who killed his own family, inflicted horror on the community and threatened the world with annihilation suddenly has an Easter Day epiphany?"
"In my faith," he continued, "I believe that anything is possible."
Ji calls DiSabatino his "brother" and jokes about erecting a statue to him in his hometown when North Korea and South Korea are one. He is hopeful that sustained international pressure will eventually drive Kim out of the country.
"No dictatorship can overcome so many people who are united," he said.
___
Information from: The News Journal of Wilmington, Del., http://www.delawareonline.com