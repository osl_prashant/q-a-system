Trump's call for tariffs creating anxiety in the farm belt
Trump's call for tariffs creating anxiety in the farm belt

By THOMAS BEAUMONTAssociated Press
The Associated Press

HOSPERS, Iowa




HOSPERS, Iowa (AP) — In Sioux County, where swine barns interrupt the vast landscape of corn-stubbled fields, exports of meat, grain and machinery fuel the local economy. And there's a palpable sense of unease that new Chinese tariffs pushed by President Donald Trump — who received more than 80 percent of the vote here in 2016 — could threaten residents' livelihood.
The grumbling hardly signals a looming leftward lurch in this dominantly Republican region in northwest Iowa. But after standing with Trump through the many trials of his first year, some Sioux County Trump voters say they would be willing to walk away from the president if the fallout from the tariffs causes a lasting downturn in the farm economy.
"I wouldn't sit here today and say I will definitely support him again," said 60-year-old hog farmer Marv Van Den Top. "This here could be a real negative for him."
Last week, Trump announced plans to impose tariffs on a range of Chinese goods, a move aimed at punishing Beijing for stealing American technology. The Chinese government responded with a threat to tag U.S. products, including pork and aluminum, with an equal 25 percent charge.
That sent a chill through places like Sioux County, which ranks first among Iowa's 99 counties in agricultural exports. In 2016, the county sold $350 million in meat, grain, machinery and chemicals overseas. Far closer to the sparsely populated crossroads of South Dakota and Minnesota than Iowa's bustling Des Moines metro area, Sioux County is home to just 34,000 people, but more than 1 million hogs, 6 million chickens and nearly as impressive numbers of cattle and sheep.
Brad Te Grootenhuis sells about 25,000 hogs a year and could lose hundreds of thousands of dollars if the tariffs spark a backlash from China. He said it's possible he would abandon Trump if pork's price decline continues and lasts.
"Any time you're losing money, nobody's happy," the 42-year-old farmer said. "I've got payments to make, plain and simple."
Nationally, opinions on Trump's tariffs, which were a central part of his campaign pledges to get tough on China, are mixed.
Although GOP congressional leaders have argued tariffs would prompt a trade war and have urged Trump to reverse course, 61 percent of Americans who identify as Republicans nationwide favor a tariff, according to a national poll taken this month by The Associated Press-NORC Center for Public Affairs Research. Still, 39 percent of Republicans say it will lead to a decrease in jobs, according to the poll, compared to 32 percent who think it will lead to an increase. That's similar to the views of all voters, the survey shows.
Countermeasures by China, which is second only to Canada in importing Iowa products, could cause pain across the American agricultural sector, according to economists. For instance, a pork tariff imposed by China, which spent $42 million on Iowa pork products in 2017, would back up the Iowa market and force prices sharply downward.
"Retaliatory tariffs from China would have a devastating impact on U.S. agricultural exports, especially if they focus on products like soybeans and hogs," said Adam Kamins, a senior economist at Moody's Analytics. "This puts northwest Iowa and the Great Plains more broadly on the front line in a trade war."
For hog farmer Tim Schmidt, the fallout of a geopolitical spat with China would force him to hold off on any new construction or maintenance on the decades-old buildings on his family-run farm along the Missouri River.
"There is an uncertainty to exactly what the next two to three years are going to look like," Schmidt said. A Trump voter in 2016, Schmidt said that if "things are bad and someone better comes along, we're willing to take a look."
Sioux County seed dealer Dave Heying echoed a common refrain that any downturn in the farm economy would curb spending throughout the local economy, with direct impact on farm machinery dealers, mechanics and agricultural construction, among other businesses.
"Protecting our U.S. industries is important, but my concern is, at what expense to the farmer?" Heying said of Trump's trade moves. "It is too early to say whether or not I would support him. These types of decisions give you hesitation."
As a presidential candidate, Trump was a somewhat awkward fit for Sioux County, where a third of its residents are members of the Dutch Reformed Church of America, which holds strictly conservative social positions. In striking contrast, the bombastic New Yorker has been married three times and shadowed by allegations of sexual harassment and infidelity.
Trump finished fourth in Sioux County in Iowa's Republican presidential caucus, but carried 81.3 percent of the vote in the general election, his second-highest county share in the state. And a large core of voters in Sioux County, where Franklin Roosevelt was the last Democratic presidential candidate to win, remains with Trump, even if the farm economy suffers as a result of his trade policies.
"You have to have faith in our innovation and entrepreneurialship in this country," said Ed Westra, a grain cooperative manager and Trump devotee. "You've got to think of the big game."
___
AP polling director Emily Swanson contributed to this report from Washington.
___
Follow Thomas Beaumont at http://twitter.com/TomBeaumont