John Deere has received the American Farm Bureau Ag Data Transparent (ADT) certification.
This certification demonstrates the company’s compliance to the Ag Data Core Principles. ADT and its principles were developed in 2014 by the American Farm Bureau Federation, other ag industry groups, and ag technology companies, which includes John Deere.
The goal of ADT is to help farmers and ranchers understand how their ag data is used when signing up with technology providers to use products or services where their data is collected. Submissions for certification are reviewed and verified by an independent third-party administrator.

“It [ADT certification] addresses issues such as data ownership, transparency, consistency, choice, portability, collection, access, data control, disclosure and use, which are important in helping producers make informed decisions about the data partners they work with in their operations,” explains Matthew Olson, John Deere product marketing manager for precision ag.

Companies and products that comply with the ADT standards and are approved can use the Ag Data Transparent seal, which designates their compliance.

“Producers place a high value on their farm data and the security around it. At John Deere, we are focused on how we can provide them with control, transparency and value connected to that data,” says Olson. “ADT is one tool that aligns with those principles and supports producers’ efforts to make informed decisions around what service providers to use, how they use the data and with whom they share it.”

Information on John Deere’s ag data and privacy policies can be found at www.deere.com/trust. Additional information on Ag Data Transparent can be found at www.agdatatransparent.com.