Crop calls 
Corn: Fractionally to 1 cent lower
Soybeans: 1 to 2 cents higher
Wheat: 1 to 3 cents lower
Price action was fairly quiet again overnight, as traders are focused on evening positions ahead of tomorrow's USDA reports. Anticipation that USDA will raise its corn crop peg is maintaining a negative tone on that market, while expectations of a reduction to the size of the soybean crop is supporting bean futures. Funds were net sellers of corn and SRW wheat yesterday, but net buyers of soybeans -- a trend that lingered into overnight trade.
 
 
Livestock calls
Cattle: Mixed
Hogs: Lower
Cash cattle trade got underway yesterday in the Plains at $124.00, which is 50 cents firmer than the average of last week's cash trade. Some traders may be disappointed by cash trade given ongoing strength in the beef market, although packers have seen margins tighten. December live cattle ended yesterday at just a small premium to that price, while February futures hold around a $6 premium. Hog futures are called lower on followthrough selling, which if realized would be signs a top is in the works. The cash market is expected to weaken again today as packers deal with backed up supplies.