Tour de Fresh is nearing its fundraising goal with only five days left before the July 25-27 event.The three-day cycling event raises money for the Salad Bars to Schools campaign. It starts in Pismo Beach, Calif., and has a finish line in Monterey Calif., coinciding with the Produce Marketing Association’s annual Foodservice Conference.
Riders and sponsors have raised $138,291 of the $150,000 goal, according to a news release.
“We’re proud of the individual riders who have set the tone by meeting their goals early and can’t wait to give 16 schools an early surprise,” Cindy Jewell, vice president of marketing for California Giant Berry Farms, said in the release.
The 16 schools that will receive salad bars early are:
Cheney School District 360;
Eastonville School District;
Springfield Public Schools;
Fairfax County Public Schools;
Vista Unified School District;
Central Bucks School District;
Hamilton Southeastern Schools;
San Bruno Park School District;
Boulder Valley School District; and
Detroit Public Schools.
Donations can be made at tourdefresh.com/riders.