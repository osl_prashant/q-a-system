Same three commodities also received PLC payments for 2015 crops 



Eligible producers of 2016 corn, grain sorghum and canola enrolled in the Price Loss Coverage (PLC) program option will be receiving a payment based on program calculations.
Payments for covered commodities are made under the PLC program when the market year average price is below the reference price. For the Agriculture Risk Coverage (ARC), payments are made when the actual revenue is below the guarantee.
The following table shows the PLC payment rates for corn, grain sorghum, soybeans, dry peas, canola, and lentils:




 
Item


 
Corn


Grain
Sorghum


 
Soybeans


 
Dry Peas


 
Lentils


 
Canola




                   Dollars per Bushel


          Dollars per Pound




Reference Price


3.70


3.95


8.40


0.1100


0.1997


0.2015




National Loan Rate


1.95


1.95


5.00


0.0540


0.1128


0.1009




Marketing Year
Average (MYA) Price


3.36


2.79


9.47


0.1100


0.2850


0.1660




Higher of Loan
Rate or MYA Price


3.36


2.79


9.47


0.1100


0.2850


0.1660




PLC Payment Rate


0.34


1.16


0.00


0.0000


0.0000


0.0335




USDA published the market-year average price for the above commodities September 28.
Comments: The same three commodities also received PLC payments for 2015 crops, with corn at 9 cents per bushel, sorghum at 64 cents per bushel and canola at 4.55 cents per pound.