Buffalo gnats, or black flies, were blamed for deaths of cattle, horses and deer in Arkansas earlier this year. Now researchers want to quantify the risks and see what control efforts were effective.
Flooding and low spring temperatures created the perfect environment for large populations of flies to emerge in Arkansas County. Southern buffalo gnats typically decline when temperatures rise to 80 F or more.
“We’ve heard reports of livestock and deer deaths, but we’d really like to have some reliable data for this outbreak for the sake of comparison,” said Kelly Loftin, Extension entomologist for the University of Arkansas System Division of Agriculture. “Here’s where we need your help: If you lost cattle, horses or other livestock during this buffalo gnat outbreak, please let us know.”
Loftin said he’d like to know:

Date of loss
Number of animals lost
Type of animal lost, whether cow, bull, horse, mule, etc.
The county in which the losses occurred.

To report livestock losses due to buffalo gnats, contact Loftin at (501) 416-3684 or kloftin@uaex.edu.