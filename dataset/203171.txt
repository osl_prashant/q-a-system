Mexico's agriculture department issued a new pest risk assessment for U.S. potatoes in mid-January, but one industry leader reported no immediate breakthrough in trade access beyond the border area access already in place.
 
The new pest risk assessment is a positive development but the next steps in terms of opening the market have not occurred, said John Toaspern, chief marketing officer at Denver-based Potatoes USA, said Jan. 19. U.S. potatoes continue to be allowed into the 16-mile border zone, but it is uncertain when greater access will be allowed.
 
Mexico is the second largest export market for U.S. potatoes, even limited to the border zone.
 
From January through November 2016, U.S. fresh potato exports to Mexico totaled 83,298 metric tons, second only to exports to Canada of 230,681 metric tons during that same period.
 
Much more potential exists, however, as Toaspern said the border areas represent only about 10% of the total population of Mexico.
 
A 2003 U.S.-Mexico market access agreement that initially opened the border zone to U.S. potatoes called for consideration of full access by 2005. That didn't happen, and U.S. industry officials are still working with the U.S. and Mexican governments to resolve several lawsuits related to expanding access beyond the border zone.