An enlightened approach to potato packaging is to keep the product in the dark, marketers say.
Idaho Falls, Idaho-based Potandon Produce has seen the light and recently launched a limited line of Green Giant potatoes in light-blocker packaging, said Ralph Schwartz, Potandon's vice president of sales.
"It's bags that have basically black inside the bag that increases the shelf life of the product," he said.
The new bag, production of which began around Thanksgiving, is available at some retailers, Schwartz said.
The limited launch has had good reviews so far, Schwartz said.
"Really, when you look at the time frame, it's still kind of new, and we're evaluating the launch," he said.
The bag comes in 13-14 sizes and colors, he said.
Controlling light, which causes potatoes to turn green, is a focus for Collinsville, Ill.-based packaging manufacturer Sev-Rend Corp., which launched its Clear-View Pouch for potatoes last fall, said Jeff Watkin, the company's graphics/marketing director.
"We have a couple of big customers who are starting to utilize it," Watkin said.
The pouch, which has light-blocking technology and also provides a view of the product inside, can typically hold 2-3 pounds of mini potatoes, Watkin said.
"With this technology, it's pretty much geared toward the potato industry, but we do pouches for other products, as well," Watkin said.
Watkin said he could envision light-blocking technology catching on.
"Buyers say they've had issues with shrink on the yellow potato side because it can only sit in the light for three days before it starts to turn green," he said.
Sev-Rend's pouch will grab a shopper's attention, which is crucial, Watkin said.
"The one thing with the produce industry, a lot of people like to see what they're buying," he said. "It's kind of imperative with the produce industry. We've got a lot more real estate."
The pouch will accommodate a lot of information, Watkin said.
"Branding, highlighting gluten-free, if it's a non-GMO, organic - those are things that are really screened across that package," he said.
The pouch is a good fit for organic product, Watkin said.
"With the shorter shelf life of organics, this product lends itself to organics, doing whatever it can to extend shelf life," he said.
Mini potatoes are building demand for pouches, which is relatively new for the potato category, Watkin said.
"One thing we've seen with minis is they're starting to gravitate toward pouch, which hasn't been big in the potato industry in the past," he said.
The light-blocking technology enhances the package's value to potatoes, Watkin said.
"The customer can still see the product but it has a longer shelf life," he said.