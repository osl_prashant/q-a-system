The Canadian Produce Marketing Association participated in the third round of the North American Free Trade Agreement talks in Ottawa.
CPMA made recommendations on the sanitary and phytosanitary and biotechnology chapters to help modernize and improve NAFTA, according to a news release on the Sept. 23-27 free trade act talks.
The organization will continue working with the Canadian government to ensure fresh produce industry members’ voices are heard, as well as monitor potential impacts to the industry during discussions, according to the release.
The fourth round of NAFTA talks are scheduled for Oct. 11-15 in Washington, D.C.
CPMA also hosted “Taste of California,” a chef challenge dinner, during its semi-annual board meeting in Monterey, Calif.
The challenge was created to feature California’s Salinas Valley produce, according to the release. Todd Fisher of Folktales Winery, Tony Baker of Montrio, and Jeremiah Tydeman of Seventh and Dolores, competed in the event to create Half Your Plate inspired meals.
Chef Tydeman won for his steak with melted avocado, harissa cauliflower “couscous,” horseradish and artichoke gratin, shaved celery and cilantro salad, according to the release.