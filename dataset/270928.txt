It’s commonly called the Pineapple Express and it was the basis of a very long line of weather events hitting the West Coast during the winter of 2016-17. 
Pineapple Express sounds pleasant enough, at least in part, as its primary namesake is juicy, deliciously tangy and sweet. But in this case it was actually storm system after storm system attached to the warmth and water of Hawaii.
 
For fruit growers and the farmers of other crops the terms El Niño and La Niña have become familiar, one weather pattern usually warmer than normal and often drier, and the second cooler and often wetter.
 
Aligned with changes in Pacific Ocean temperatures, the La Niña this time around was said to be weak and ultimately fizzled. But in reality it didn’t behave like a classic La Niña.
 
This La Niña instead was responsible for a powerful Pacific jet stream frequently sweeping storms into California with states to the north also feeling the effect of this historic “pineapple.”  
 
When it rains it pours, and that’s usually good for growers, even if it can also come with the challenges occurring with an overabundance of wetness.
 
When it snows, especially in the mountains, that’s also good news for farmers as it acts as an irrigation bank account for later in the growing season. 
 
The April mountain snowpack in Washington state was at 121% of normal, and in California, generally over 160%. 
 
With the water bank more than able to support fruit grower accounts throughout the coming season, more immediate thoughts are on the potential of damaging springtime frost.
 
Those same growers have a number of weapons to fight those cold temperatures with the one most visually apparent in the orchard landscape being what are commonly called ‘wind machines.’
 
For those not familiar with orchards, these imposing looking towers with large fan blades are very effective when it comes to frost protection, as long as nighttime temperature readings are not reaching extremes.
 
Wind machines, often visual from all around, work by bringing down the warmer air in order to protect the tender young fruit buds and then their blossoms.
 
For those of us residing in orchard areas like the Yakima Valley for many decades, these monsters have added a whole new dimension to the environment when damaging temperatures arrive. 
 
You might wonder what comes to the mind of a first-time visitor driving through fruit country and seeing these machines. 
 
Not wasting the opportunity to witness a visitor’s reaction, I’ve even been guilty of spreading this one.
 
“That’s right, each propeller and tower is on the exact location of a crash. Sad, but true,” I’d say, trying to maintain a straight face.
 
“Wow! That’s really interesting,” the valley visitor would say. “Hope I’m never on a flight going over your orchard airspace.” 
 
Who says rural legends can’t be as much fun as urban legends?  
 
Alan Taylor is the former marketing director for Pink Lady America in Yakima, Wash., and now consults in the apple industry. E-mail him at proft@embarqmail.com.
 
What's your take? Leave a comment and tell us your opinion.