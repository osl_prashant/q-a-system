Sponsored Content
Bovine Veterinarian interviewed Dusty Abney, Ph.D., Beef Technical Specialist and Bryan McMurry, Ph.D., Beef Marketing Specialist, who shared details on the NEW Reloader 250™ Mineral Bolus from Cargill Animal Nutrition.

Reloader 250™ is the sure-shot approach to trace mineral supplementation. Administered orally, Reloader 250 Mineral Boluses deliver six trace minerals and three vitamins to cattle.
Every cow. Every day. For 250 days.

Sponsored by Cargill