Strip-till and no-till can offer benefits; find out if they’re right for your farm
Ten years ago, north-central Iowa farmer Dave Gerber did what few dare—he switched all of his 1,100 acres from conventional tillage to strip-till in just one season. He now spends 60% less on diesel fuel, 20% less on fertilizer and estimates 60% slower depreciation on his equipment. 

“In conventional tillage, my yields were leveling off in both corn and soybeans, and I wanted to see improvement,” Gerber says. “For the first seven years I just used a single disc to cut a hole in the ground, it was basically no-till, but in the past three years I bought a machine that tills 8" strips.”

Now he strip-tills half his ground and no-tills the other half in sync with his corn/soybean rotation. While he says there’s a learning curve, he also says money saved outweighs setbacks.

Reducing tillage can be a sizeable change for your farm. Consider the benefits as well as any potential disadvantages before getting started. 

New to reduced tillage systems, Iowa corn farmer Bruce Kent has kept an eye on Gerber’s fields and tillage for the past 10 years. When challenged to try to reduce tillage on his own farm, Kent turned to Gerber. 

“We tried no-till two years ago and saw a big enough yield loss I wanted to find another system,” Kent says. “This last year we called our neighbor [Gerber] and experimented with strip-till. He strip-tilled about 5% of our acres.”
Changing tillage can save money but often requires changes in management. No-till in cold, wet Iowa soil meant Kent either couldn’t plant as early as he wanted or would plant early and see seedlings suffer. In addition, he saw a yield drop. Now he says he’s found the best of conventional tillage and no-till in a strip-till system.

Gerber didn’t see a big change in yield after switching to no-till. However, he did note changes in soil composition didn’t happen as quickly as expected. He reduced his phosphorus and potassium application to about 14% to 20% of what he applied the first couple of years, and that was too little. However, he knows reduced tillage is helping build organic matter and nutrients in the soil. He also recognizes the need to slowly reduce synthetic fertilizer to avoid yield loss again. 

Kent and Gerber see savings from fewer tillage passes. Fewer miles helps them with depreciation and resale. They’re seeing savings from leaving residue on the soil’s surface combined with cover crops keeping nutrients in place. They’re excited to see less soil loss from wind and water because the soil is covered nearly all year.

“My reaction to strip-till was positive; yields are similar to conventional yields,” Kent says. “The biggest problem is the number of acres we farm. When you hit a certain number of acres you’re limited by the size of strip-till or no-till equipment.”

He typically runs a 36-row planter across 4,000 acres. But the largest strip-till machine he’s aware of is only a 16 rows. He says he likely won’t be able to reach 100% reduced tillage because of time restraints involved. In addition, he doesn’t want to risk missing his planting window in case of delays. 

“I think it’s easier for guys who are used to eight-row equipment to adapt to this type of system,” he adds. 

Tips for Reducing Tillage
Switching from conventional tillage systems to reduced tillage can be challenging. Here are some tips from new and veteran reduced tillers:

Use a chopping corn head to make residue more manageable.
Get fertilizer placement right, which might mean at planting.
Be prepared to plant later.
Consider experimenting on a small number of acres first to avoid operation-wide mistakes.
Know location and weather constraints for no-till.
Use effective residue movers during planting.
Be realistic about how many acres you can no-till or strip-till.