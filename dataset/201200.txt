July 5, 2017




Midweek Cash Markets





Click here for related charts.

CORN COMMENTS
The national average corn basis dropped 9 1/2 cents from last week to 20 1/2 cents below futures on the rollover to basis the September contract. The national average cash corn price firmed 26 1/4 cents to $3.72.
Basis is weaker than the three-year average, which is 2 1/4 cents above futures for this week.
 
SOYBEAN COMMENTS
The national average soybean basis dropped 4 cents from last week to stand 20 cents below August futures. The national average cash price surged 63 3/4 cents to $9.61 3/4.
Basis is well below the three-year average, which is 26 1/2 cents above futures for this week.
 
WHEAT COMMENTS
The national average soft red winter (SRW) wheat basis dropped 14 cents to 7 3/4 cents below September futures. The average cash price surged 88 3/4 cents from last week to $5.52 1/4. The national average hard red winter (HRW) wheat basis softened 8 1/2 cents from last week to 70 1/4 cents below September futures. The average cash price surged 99 3/4 cents from last week to $4.99 1/4.
SRW basis is firmer than the three-year average of 9 1/2 cents under futures. HRW basis is much weaker than the three-year average, which is 42 1/2 cents under futures for this week.
 
CATTLE COMMENTS
Cash cattle trade averaged $118.64 last week, down $2.86 from the previous week. Cash cattle trade is expected to be lower again this week, based on sales at the online Fed Cattle Exchange today. Last year at this time, the cash price was $122.50.
Choice boxed beef prices dropped another $6.85 from last week at $222.58. Last year at this time, Choice boxed beef prices were $209.08.
 
HOG COMMENTS
The average lean hog carcass bid firmed 43 cents over the past week to $91.93. Last year's lean carcass price on this date was $83.24.
The pork cutout value rose $1.06 from last week to $103.54.  Last year at this time, the average cutout price stood at $88.91.













Copyright © 2017 Pro Farmer

Pro Farmer, 6612 Chancellor Drive, Suite 300, Cedar Falls, IA 50613
1-800-772-0023