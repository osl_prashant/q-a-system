This market update is a PorkNetwork weekly column reporting trends in weaner pig prices and swine facility availability.  All information contained in this update is for the week ended August 25, 2017.
Looking at hog sales in February 2018 using April 2018 futures the weaner breakeven was $37.84, down $2.13 for the week. Feed costs were down $1.29 per head. April futures decreased $1.68 compared to last week’s April futures used for the crush and historical basis is unchanged from last week. Breakeven prices are based on closing futures prices on August 25, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig ReportCash-traded weaner pig volume was above average this week with 47,626 head being reported which is 124 percent of the 52-week average. Cash prices were $22.06, up $2.95 from a week ago. The low to high range was $15.00 - $29.00. Formula-priced weaners were up $0.35 this week at $36.99.
Cash-traded feeder pig reported volume was below average with 5,685 head reported. Cash feeder pig reported prices were $38.50, down $1.42 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through August 25, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential decreased $5.08 this week to a projected gain of $15.78 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.