Valencia, Calif.-based Sunkist Growers plans to kick off its California mandarin season in early November with increased volume, according to a news release.

Sunkist’s mandarins are now marketed under the Delite name, which was created by the first family to grow murcott mandarins in California, according to the release.
“Our family began growing mandarins because the variety offers so much for consumers — convenience, nutrition and of course sweet, delicious flavor,” Heather Mulholland, a fourth generation citrus grower in Orange Cove, Calif., said in the release. 
“California has an ideal climate for growing citrus, and we take pride in the quality we are able to achieve in our groves. Sunkist Delite mandarins will offer consumers everywhere a fresh taste of California.”
Joan Wickham, director of communications, said the new mandarin focus emphasizes quality and authenticity, in keeping with consumer trends.
The company will have new point-of-sale materials including display bins and other efforts such as sampling, coupons and direct mail ads to support the Sunkist Delite program, according to the release.