Fertilizer prices were mixed with UAN32% falling sharply to lead declines. 
Our Nutrient Composite Index firmed 1.11 points to 483.37 compared to last year's 491.61. 

Nitrogen

UAN32% and anhydrous ammonia accounted for the declines in the nitrogen segment.
Urea gained slightly more than UAN32% lost on the week as UAN28% fell just shy of 6 bucks per short ton.
With urea moving sharply higher this week and anhydrous off a few bucks, the midpoint between NH3 and urea firmed slightly, and UAN solutions are both priced below the midpoint.
However, both concentrations of UAN have a fair amount of work to do before they arrive at parity with anhydrous. We believe the wide spread between anhydrous and our indexed urea figure will require urea to throw its weight to the downside in a meaningful way before UAN softness to the level of NH3.

Phosphate

Phosphate prices were mixed on the week with DAP firming $2.93 and MAP off $2.10 per short ton.
The DAP/MAP spread narrowed to 5.27 which is a good 10 points more narrow than the historical norm.
Wholesale DAP and MAP prices continued higher this week at all terminals reported by MosaicCo. with NOLA values up $4 per ton.
Post harvest demand will buttress near-term DAP/MAP prices.

Potash 

Potash firmed this week on strength in Missouri and South Dakota.
Wholesale potash prices were sideways at Brazilian, and Cornbelt terminals this week and at NOLA.
On an indexed basis, potash still holds a strong premium to anhydrous ammonia, and, like phosphate, has likely encountered some post-harvest demand.

Corn Futures 

December 2018 corn futures closed Friday, November 17 at $3.87 putting expected new-crop revenue (eNCR) at $613.22 per acre -- lower $10.17 per acre on the week.
With our Nutrient Composite Index (NCI) at 483.37 this week, the eNCR/NCI spread narrowed 12.22 points and now stands at -129.85.





Fertilizer


11/6/17


11/13/17


Change


Current Week

Fertilizer



Anhydrous


$400.49


$409.12


-$2.01


$407.10

Anhydrous



DAP


$435.32


$437.25


+$2.93


$440.18

DAP



MAP


$445.44


$447.55


-$2.10


$445.45

MAP



Potash



$323.47




$325.51




+31 cents




$325.82


Potash



UAN28


$212.57


$208.73


+$5.84


$214.56

UAN28



UAN32


$241.20


$235.05


-$11.15


$223.90

UAN32



Urea


$333.53


$339.84


+$11.36


$351.21

Urea



Composite


481.31


482.26


+1.11


483.37

Composite