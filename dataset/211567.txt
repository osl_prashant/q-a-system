How’s this for truth in advertising: “One size fits all.” 
One-size-fits all doesn’t work for with clothing, and it doesn’t work very effectively in produce merchandising either.
 
I was speaking with a former produce director this past week. We agreed: Produce plan-o-grams, schematics — whatever you want to call them — have their limitations. 
 
Worse, they can suppress the very purpose of any produce department. Which is, of course, to generate sales and gross profit.
 
In all the chains I’ve been involved with, the most successful produce operations were those that allowed their produce managers free reign to merchandise how they saw fit. 
 
The result? Well, amid the 70 or so produce managers we oversaw at one chain, I admit that only about one-third really turned on the creative merchandising juices. The second third of the department managers were OK, but not great. The last third of the produce manager mix? They needed plenty of direction.
 
I’m betting many chains would fall under similar strength proportions, more or less, given the same no-holds-barred opportunity.
 
The trouble with this talent mix is that a grocer can’t afford to have one-third of its company with weak merchandising efforts. It doesn’t reflect well on the chain. A poorly merchandised department costs in terms of lost sales, higher shrink and lower profit margins.
 
The answer that many chains provide, of course, is the mandatory weekly schematic.
 
This brings all the produce departments into a set standard. Produce directors will attest that assigning set merchandising gives managers a guide to follow and removes the stress of merchandising decisions. Produce buyers may weigh in and say it helps monitor inventory levels, and supervisors might say set merchandising plans make it easier to monitor program compliance.
 
But are set schematics really the best course in terms of driving sales and profit?
 
Not really. At least not in this produce scribe’s experience. Most produce directors will acknowledge that every store’s customer base, its sales dynamics, potential, and product mix are different from one neighborhood to the next. Schematics look great on paper, but they don’t always translate into best practices.
 
Perhaps there isn’t a single, good solution. However, I can see how a compromise may work. Go ahead and send out the weekly schematic to all. To the less-experienced managers, make it mandatory. To the up-and-comers, make them follow most of the plan, allowing some flexibility. 
 
But to the produce superstars, give them the option and encouragement to spread their wings, get creative and merchandise as they see fit to best drive sales.
 
It’ll give the rest of the pack something to shoot for.
 
Armand Lobato works for the Idaho Potato Commission. His 40 years’ experience in the produce business span a range of foodservice and retail positions. E-mail lobatoarmand@gmail.com.
 
What's your take? Leave a comment and tell us your opinion.