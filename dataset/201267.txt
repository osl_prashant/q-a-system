Phosphates were lower on the week as potash firmed slightly.

DAP $29.29 below year-ago pricing -- lower 5 cents/st on the week to $445.05/st.
MAP $25.75 below year-ago -- lower 58 cents/st this week to $460.72/st.
Potash $8.02 below year-ago -- higher 4 cents/st this week to $333.87/st.


MAP was our downside leader in the P&K segment this week led by Illinois, which fell $7.40 as North Dakota softened $4.41 and Minnesota dropped 18 cents. Six states were unchanged as South Dakota firmed $4.37, Nebraska added 35 cents and Kansas gained 8 cents per short ton.
DAP was a nickel lower on the week with only Minnesota adjusting their price... down 57 cents per short ton. No state posted a higher DAP price as eleven states were unchanged.
Potash posted gains this week led by Kansas, which added $1.88 as Indiana gained 85 cents and Minnesota firmed 31 cents per short ton. Five states were unchanged as
North Dakota fell $1.72, Illinois softened 83 cents and Nebraska shucked 3 cents per short ton.
Potash remains priced at a slight premium to NH3 on an indexed basis this week, but we remain unconcerned that potash will firm beyond affordability without notice. Phosphates got a mixed bag of news last week. Trinidad was reportedly having trouble with natural gas supplies for ammonia manufacturing. That is of greater concern to phosphates than it is to nitrogen as the ammonia exported from Trinidad is used for DAP and MAP manufacturing and other industrial uses. Offsetting that news is the upcoming opening of a world-scale phosphate production plant in Saudi Arabia which will likely help fill the supply gap, once it is up and running.
So for the time being, our outlook for potash and phosphate is basically the same. Both appear poised to run sideways. Potash carries a mildly upward bias while phosphate carries a downside bias. Hold off for now on booking for futures P&K applications.


By the Pound -- The following is an updated table of P&K pricing by the pound as reported to your Inputs Monitor for the week ended June 23, 2017.
DAP is priced at 46 1/2 cents/lbP2O5; MAP at 43 1/4 cents/lbP2O5; Potash is at 28 1/4 cents/lbK2O.





P&K pricing by the pound -- 7/13/2017


DAP $P/lb


MAP $P/lb


Potash $K/lb

 



Average


$0.46 1/2


$0.43


$0.28 1/4

Average



Year-ago


$0.49 1/2


$0.45 1/2


$0.29

Year-ago