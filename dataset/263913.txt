Nature Conservancy to work with Forest Service in Wisconsin
Nature Conservancy to work with Forest Service in Wisconsin

The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — An environmental organization and the U.S. Forest Service are working together to harvest timber in northern Wisconsin.

                Wisconsin Public Radio reports that the 2014 Farm Bill has allowed the two groups to enter into a stewardship agreement. The conservancy will hire loggers, sell timber and use the proceeds for projects the Forest Service can't afford to do.
The conservancy plans to use some money to restore Simpson Creek by rerouting the channel and exposing the gravel floor that fish need to spawn. The group also plans to rebuild a handicap accessible boardwalk on the Oconto River and will use funds to restore habitat for the endangered Kirtland's warbler.
Forest Supervisor Paul Strong says the Forest Service's budget has been stretched by efforts to fight wildfire that have become more frequent and more intense.
___
Information from: Wisconsin Public Radio, http://www.wpr.org