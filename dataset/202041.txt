This market update is a PorkNetwork weekly column reporting trends in weaner pig prices and swine facility availability. All information contained in this update is for the week ended June 24, 2016.Looking at hog sales in December 2016 using February 2017 futures, the weaner breakeven was $24.57, up $2.16 for the week. Feed costs were down $6.89 per head. February futures decreased $2.38 compared to last week's February futures used for the crush. Breakeven prices are based on closing futures prices on June 24, 2016. The breakeven price is the estimated maximum you could pay for a weaner pig andbreakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations don't take into account market supply and demand dynamics for weaner pigs and space availability.

For the Week Ended


6/24/2016


Weekly Change


Weaner pig breakeven


$24.57


$2.16


Margin over variable costs


$43.84


$2.16


Pig purchase month


June, 2016





Live hog sale month


Dec, 2016





Lean hog futures


$66.63


($2.38)


Lean hog basis/cwt


($6.73)


$0.00


Weighted average sbm futures


$374.33


($31.37)


Weighted average sbm basis


($4.04)


$0.00


Weighted average corn futures


$3.93


($0.54)


Weighted average corn basis


($0.24)


$0.00


Nursery cost/space/yr


$35.00


$0.00


Finisher cost/space/yr


$40.00


$0.00


Feed costs per head


$75.12


($6.89)


Assumed carcass weight


205


0

From the National Direct Delivered Feeder Pig Report
Cash traded weaner pig volume was below average this week with 25,825 head being reported which is 85 percent of the 52-week average. Cash prices were $20.27, down $2.31 from a week ago. The low to high range was $15.00 to $25.00. Formula priced weaners were down $2.42 this week at $34.97.
Cash traded feeder pig reported volume was below average with 8,625 head reported. Cash feeder pig reported prices were $46.53, down $5.23 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through June 24, 2016.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential decreased $0.60 this week to ($0.77) per head.

Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest. NutriQuest Business Solutions is a team of leading business and financial experts that bring years of unparalleled experience in the livestock, grain producers and financial industries. At NutriQuest our success comes from helping producers realize improved profitability and sustainability through innovation driven by a relentless focus on helping producers succeed. For more information, please visit our website at www.nutriquest.com or email casey@nutriquest.com.