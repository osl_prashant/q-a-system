Maryland House OK's bill allowing farmers to grow hemp
Maryland House OK's bill allowing farmers to grow hemp

By BRIAN WITTEAssociated Press
The Associated Press

ANNAPOLIS, Md.




ANNAPOLIS, Md. (AP) — A measure to allow Maryland farmers to grow industrial hemp if they do it in partnership with a university or state agency has passed the House of Delegates.
The House approved the bill 136-to-1 Thursday. It now goes to the Senate.
Growing hemp without a federal permit had been banned because of its classification as a controlled substance related to marijuana. The 2014 federal Farm Bill allows state agriculture departments to designate hemp projects for research and development.
Hemp has historically been used for rope. Its fiber is also used in clothing and mulch, while hemp seeds are used to make cooking oil, and more recently have been touted as a nutritional snack.
Other uses for hemp include building materials, animal bedding and biofuels.