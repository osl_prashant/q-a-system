Mystery remains for South Dakota rancher's poisoned cattle
Mystery remains for South Dakota rancher's poisoned cattle

The Associated Press

HOWARD, S.D.




HOWARD, S.D. (AP) — The case of an eastern South Dakota rancher whose 44 cattle were mysteriously poisoned and killed on Halloween remains unsolved.
The state Department of Criminal Investigation has not been able to track down the cattle-killer.
Bernard Donahue told KELO-TV that his family had been keeping 65 cattle on a rented pasture in Miner County when someone poisoned the water tank last October. By the next day, 44 of their cattle were dead, including a bull.
Veterinarian Tom Heirigs was called to the property near Howard. By the time he arrived, seven cattle had already died. He said the water tank's smell burned his nose and was "almost enough to make you puke."
Donahue said water samples were taken for testing, but there hasn't been confirmation of what chemical was used.
"The water was definitely tainted," said Heirigs. "If you put an ammonia source like that in the water, it's going to dissolve and the ammonia source is going to disappear."
It's possible that Donahue's landlord could have been the target of the attack because the owners received a threatening anonymous letter, which was turned over as evidence. The letter stated, "You two are the most hated two in the area...I know karma comes around.... I hope you get what you deserve."
County Sheriff Lanny Klinkhammer said no one reported seeing anything suspicious that night, but he's confident the killer will be caught eventually.
"The DCI has been involved. They've also interviewed some individuals," Klinkhammer said. "There's nothing that has come of that as of yet. But they're not giving up."
The Donahues are offering a reward of $5,000 for any information that leads to an arrest.
___
Information from: KELO-TV, http://www.keloland.com