BC-APFN-Business News Digest
BC-APFN-Business News Digest

The Associated Press



Here are AP Business News' latest coverage plans, top stories and promotable content. All times EDT.
New & developing: YAHOO-DATA BREACH-FINE, EARNS-RESTAURANT BRANDS
TOP STORIES:
FACEBOOK-SPELLING OUT WHAT'S FORBIDDEN — If you've ever wondered exactly what sorts of things Facebook would like you not to do on its service, you're in luck. For the first time, the social network is publishing detailed guidelines to what does and doesn't belong on its service — 27 pages worth of them, in fact. By Barbara Ortutay. SENT: 770 words, photos, glance.
With: AP POLL-FACEBOOK-PRIVACY SCANDAL — Seventy percent of Americans who have heard of Facebook's Cambridge Analytica scandal have taken some action to change the way they use their social media accounts in the wake of revelations about the company, according to a new poll from the AP-NORC Center for Public Affairs Research. SENT: 890 words, photos, with video, graphic, social media.
— AP POLL-SOCIAL MEDIA USE — Nearly half of Americans who use the Internet say they use Facebook at least several times a day, with Facebook-owned Instagram coming in No. 2 among social media sites, according to a new poll from the AP-NORC Center for Public Affairs Research. By Mae Anderson. SENT: 590 words, photos.
— FACEBOOK PRIVACY SCANDAL — A Cambridge University professor at the heart of the Facebook privacy scandal says that the former CEO of data firm Cambridge Analytica lied to a British parliamentary committee investigating fake news. By Danica Kirka. SENT: 270 words, photos.
FINANCIAL MARKETS —After a strong start, U.S. stocks abruptly sell off after machinery maker Caterpillar said it doesn't expect to top its first-quarter profit for the rest of the year. The Dow Jones industrial average plunge as much as 619 points as investors fear that rising oil prices and other costs will slow down growth in company profits. By Marley Jay. SENT: 850 words, photos.
AMAZON-CAR DELIVERY — Amazon's latest perk ... free delivery to your car. The Seattle company says that it has begun delivering packages in 37 cities to Prime members who own newer General Motors or Volvo vehicles. By Mae Anderson. SENT: 400 words, photos.
TRUMP-TRADE — President Donald Trump's escalating dispute with China over trade and technology is threatening jobs and profits in working-class communities where his "America First" agenda hit home. By Richard Lardner. SENT: 900 words, photos.
CONSUMER WATCHDOG-NAME CHANGE — The Consumer Financial Protection Bureau is dead. Long live the Bureau of Consumer Financial Protection. That's the message the Trump administration is pushing, at least, in what on the surface seems like a minor tweak to the name of the federal consumer watchdog agency created after the Great Recession to protect consumers against banks, credit card companies, debt collectors and other financial companies. By Ken Sweet. SENT: 750 words, photos.
PECAN BRANDING — The American pecan industry is getting organized. Farmers of America's only native tree nut are launching a marketing campaign that they hope will help them better compete for a larger share of the multibillion-dollar U.S. snacking business and growing Chinese demand. By Emily Schmall. UPCOMING: 700 words by 6 p.m., photos.
MARKETS & ECONOMY:
CONSUMER CONFIDENCE — American consumers saw their confidence rebound in April to a level close to the 18-year high it had reached two months ago. By Martin Crutsinger. SENT: 270 words, photos.
HOME PRICES — U.S. home prices jumped in February as buyers are fiercely competing over a dwindling number of homes for sale. By Christopher Rugaber. SENT: 410 words, photos.
NEW HOME SALES — Sales of new U.S. homes jumped 4 percent in March, propelled by a surge of buying in the West. By Josh Boak. SENT: 300 words.
INDUSTRY:
CHINA AUTO SHOW PREVIEW — The biggest global auto show of the year showcases China's ambitions to become a leader in electric cars and the industry's multibillion-dollar scramble to roll out models that appeal to price-conscious but demanding Chinese drivers. SENT: 900 words.
SOUTHWEST-SAFETY HISTORY — Until last week, Southwest Airlines had a string of 47 years without a passenger dying in an accident. But it has also paid millions in fines over safety. Recently its mechanics union accused the airline of taking shortcuts. Some analysts, however, consider it a safe airline. Its safety record is coming under scrutiny after an engine failure at 32,000 feet last week hurled shrapnel at a Southwest jet, breaking a window and killing a passenger. By David Koenig. SENT: 1,000 words, photos.
KIDS-AGING DISEASE — Children with a rare disease that causes rapid aging and early death may live longer if treated with a drug first developed for cancer patients, research suggests. While skeptics say the small, preliminary study is inconclusive, the researchers say the results show their work is on the right track. By Lindsey Tanner. SENT: 800 words, photos.
E-CIGARETTE-CRACKDOWN — Federal health officials are cracking down on underage use of a popular e-cigarette brand following months of complaints from parents, politicians and school administrators. By Matthew Perrone. SENT: 600 words.
EARNINGS:
EARNS-RESTAURANT BRANDS — A new burger and a spicy chicken sandwich sold well at Burger King, helping boost profits for its owner Restaurants Brands. SENT: 250 words, photo.
EARNS-CATERPILLAR — A boost in equipment sales propelled Caterpillar's first-quarter profit, as a strong global economy helped support construction and energy industry projects. SENT: 230 words, photos.
EARNS-COCA-COLA — Coca-Cola's first-quarter profit jumped as the company introduced new flavors and continues to discard its bottling operations. SENT: 270 words, photos.
TECHNOLOGY & MEDIA:
YAHOO-DATA BREACH-FINE — The company formerly known as Yahoo is paying a $35 million fine to resolve federal regulators' charges that the online pioneer deceived investors by failing to disclose one of the biggest data breaches in internet history. By Marcy Gordon. SENT: 650 words, photo.
BITCOIN-POWER DRAIN — Cheap electricity and chilly air near New York's northern border is attracting energy-hungry businesses that "mine" bitcoins and other digital currencies with stacks of computers. But can crypto-currency miners create a 21st century version of a gold rush or are they merely electricity vampires? By Michael Hill. SENT: 800 words, photos.
CINEMACON-STATE OF THE INDUSTRY — Two film industry leaders tell theater owners that are optimistic about the movie and theatrical exhibition business despite concerns about declining attendance and competition from streaming services. By Lindsey Bahr. SENT: 550 words, photos.
With: CINEMACON-SONY — Sony Pictures Entertainment Chairman Tom Rothman assures theater owners that his studio is dedicated to appealing to a range of audiences — from global franchises such as "Spider-Man" and "The Girl with the Dragon Tattoo" series to family films, action pics, comedies and even Quentin Tarantino's Leonardo DiCaprio and Brad Pitt film "Once Upon a Time in Hollywood." By Lindsey Bahr. SENT: 730 words, photos, video.
SAUDI ARABIA-US TABLOID — The mystery behind the origins of a fawning pro-Saudi magazine that laid down a red carpet for Crown Prince Mohammed's visit. By Josh Lederman and Jeff Horwitz. SENT: 1,250 words, photos.
PERSONAL FINANCE:
NERDWALLET-ASK BRIANNA-FINANCIAL ADULT — Graduating from college and getting your first full-time job are major milestones that might make you feel you've finally reached adulthood. But you won't be a financial adult until you save regularly, spend mindfully, face reality and know when to ask for help. By NerdWallet columnist Brianna McGurran. SENT: 810 words, photos.
INTERNATIONAL:
GERMANY-ECONOMY — A closely watched survey showed a bigger-than-expected drop in German business confidence amid worries about international trade tensions, but the report doesn't necessarily signal any serious problems for Europe's biggest economy. SENT: 300 words.
BANGLADESH-TEXTILE INDUSTRY- FACTORY COLLAPSE — Five years after a garment factory collapse in Bangladesh killed 1,134 people much remains to be done to improve conditions for workers. By Julhas Alam. SENT: 900 words, photos.
MONEY & MARKETS SUMMARY:
SPOTLIGHT
Drive-by drop-off
Amazon's latest perk: packages delivered straight to your car, as long as you drive certain General Motors or Volvo vehicles.
CENTERPIECE
Aluminum foiled?
This month's wild ride for aluminum prices has foiled attempts by investors and businesses that depend on the metal to predict where they'll go next.
Business News Supervisor Richard Jacobsen (800-845-8450, ext. 1680). For photos (ext. 1900.) For graphics/interactives (ext. 7636.) For access to AP Newsroom and technical issues: customersupport@ap.org, or 877-836-9477. Questions about transmission of financial market listings, call 800-3AP-STOX.
The full digest for AP's Money & Markets service can be found at markets.ap.org. For questions about M&M content, contact Greg Keller at (212) 621-7958.