Grain mostly higher and livestock mixed
Grain mostly higher and livestock mixed

The Associated Press



Wheat for May rose 2.25 cents at 4.53 a bushel; May corn fell .50 cent at 3.7450 a bushel; May oats was up .50 cent at $2.3550 a bushel; while May soybeans gained 5.75 cents at $10.2825 a bushel.
Beef was higher and pork was lower on the Chicago Mercantile Exchange. April live cattle was off .65 cent at $1.1957 a pound; March feeder cattle lost .68 cent at $1.3777 a pound; while April lean hogs rose .10 cent at $.6325 a pound.