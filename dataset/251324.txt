In the AgPro Podcast, retailers were asked: How is technology helping to identify greater efficiencies and better connectivity in ag retail?
Ann Vande Lune
Sully, Iowa
Agronomy Administrator, Key Cooperative
A: “Joining the AgGateway Seed Connectivity Project gave us the ability to connect electronically with our seed and chemical manufacturers. Electronic ship notices, invoices, price sheets and orders are all connected directly to our manufacturers. That, in turn, allows us to know exactly what products we have at our facilities, when they will be arriving and when they are delivered. We have cut our entry time in our agronomy department by two-thirds. Employees are spending much less time worrying about inventory and more time concentrating on the needs of our customers.”