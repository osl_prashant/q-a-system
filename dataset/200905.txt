As we approach the end of 2016, we want to look back on the top 10 articles from Bovine Vet this year. Read the number sixbelow.
Winter storm Goliath left a wave of dead and stray beef cattle in its wake as the blizzard pushed across the Panhandle region of Texas last week.
Officials with Texas and Southwestern Cattle Raisers Association (TSCRA) estimate 4,000 feedlot cattle died from the storm, with another 6,000-8,000 stocker cattle perishing on the High Plains. Those numbers still could rise as snow drifts continue to melt, revealing more dead cattle from the Dec. 26 storm.
Muleshoe, Texas, rancher Blake Birdwell witnessed the devastation first hand, losing 15 cows to one snow drift. The Hip O Cattle Co. owner has at least another 100 dead out of the stocker cattle he custom grazes and partners on, in addition to his own cow-calf loses.
Birdwell manages 13,000 cattle primarily grazing on wheat pasture, and he was short 10,000 head after the blizzard swept through. He describes the aftermath as "mayhem," but notes the storm has brought the best out in people who are helping locate lost cattle.
"There's stockers, cows, calves, horses‚...heck I even helped gather a llama in stuff we had thrown together the other day," Birdwell says of the effort to roundup stray livestock.
These stocker cattle grazing wheat pasture on Jan. 2 outside Oklahoma Lane, Texas,are contained like many cattle in the region with electric wire fence.Texas A&M AgriLife Extension Service photo by Ted McCollum
Right after the blizzard approximately 25,000-40,000 cattle went missing from the storm impacted areas. Many cattle left pastures simply by climbing barbed wire fences covered with snow drifts. Others were able to escape wheat fields lined with electric fence that lost power during the storm.
When the cattle escaped they generally head south from their home pastures to get away from the blowing snow and seek shelter.
Birdwell had one group of stocker calves travel 70 miles, starting out north of Nazareth and ending up south of Shallowater.
Rounding up strays
Special Rangers with Texas and Southwestern Cattle Raisers Association are lending a hand to livestock owners as they help identify stray cattle and check brands. Also, they are assisting other producers to locate owners of livestock lost in recent winter storms.
Scott Williamson, who serves as the supervisor for Region 1 with TSCRA, has been based in Lubbock to support cattlemen with gathering livestock. Additional rangers from TSCRA have been called in forming a six-person team to contribute with the efforts.
"We're assisting producers who have comingled cattle. The first efforts were just to contain the livestock that had strayed out," Williamson says.
It snowed 8-10 inches in the region with winds ranging from 60-85 mph., creating those massive snow drifts. The worst affected areas have been running north between Interstate 20 to 40 along the New Mexico border and Interstate 27, Williamson said.
Many cattle are out on wheat pasture or crop residue right now. Working pens are hard to find. The roads are too muddy or drifted in to bring mobile corrals or stock trailers. It means the majority of stray cattle are commingled into groups and sorted on horseback. Then they are trailed back to separate pastures.
Some fields had up to 500 cattle from multiple owners.
"It is going to be a slow process getting it all sorted out, but it seems to be going well," Williamson says.
There has been no evidence of theft at this point. One animal was found butchered.
Word of mouth, cell phone calls andsocial media have all helpedin getting lost cattle back to their owners.
Feeders weathered the storm
Feedlots haven't witnessed massive losses like those seen by dairies in the region thatlost approximately 40,000 cattle. According to the Texas Cattle Feeders Association (TCFA), nearly 28 percent of the fed cattle in the U.S. can be found in the Panhandle region, including New Mexico, Oklahoma and Texas. Initial estimates by TCFA project just .2 percent of those cattle on feed dying during the storm.
Click here to read more on agweb.com