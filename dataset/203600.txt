After experiencing positive late summer growing conditions, Georgia growers report buyers should expect regular fall volumes of bell peppers, squash and eggplant.


Bell peppers

Ken Corbett Farms LLC, Lake Park, Ga., plans to begin harvesting bell peppers on Sept. 25, as normal.
"The transplants are looking great," Eric Bolesta, sales manager, said in late August. "If the South gets favorable weather with no adverse rains, and things hold true, we should see a great crop coming from Georgia, as expected for the fall."
Ken Corbett Farms expects to harvest through Nov. 15.
Summer demand was strong until early August, when Michigan and other northern growing regions began harvesting more volume, said Steve Sterling, general manager of Fresh Link Produce LLC, Lake Park.
"Quality and quantity should be good," he said in late August. "I don't see anything stopping it unless we get some tropical system."
Fresh Link plans to begin harvesting in early October with volume to commence by Oct. 15.
Norman Park, Ga.-based Southern Valley Fruit and Vegetable Inc. plans to overlap Evansville, Tenn., bell pepper production with its late September and early October Georgia start.
Nate Branch, a salesman, said the grower-shipper purposefully overlaps production to avoid supply gaps.
"The transition (from the north) usually goes very smoothly," he said. "We will go through any freeze. We have had falls in Georgia where we picked peppers until Thanksgiving, but sometimes the weather allows us to pick until Christmas."
Because of summer home-grown programs, demand dropped in early August, said Calvert Cullen, president of Cheriton, Va.-based Northampton Growers Produce Sales Inc.
Northampton plants for a one-week overlap between the end of its Michigan production and the start of its Georgia volume.
"As long as we don't have any adverse weather, there should be good supplies and quality should be good," Cullen said.


Squash

Southern Valley plans to begin harvesting squash in mid-September, as usual.
"The transition from the north usually goes well and is fairly consistent with the other items," Branch said. "Obviously, if Michigan gets cooler weather or rain, we should have incredible demand during the first part of the fall."
Fresh Link plans to start harvesting in mid-September, about a week earlier than normal, Sterling said.
"Volume should hit in late September, during the third week of September," he said. "Everything you can see now looks pretty good."
Though Ken Corbett Farms usually begins squash harvest in early September, following deliberate plantings for later production this year, the grower-shipper expects to begin harvesting Sept. 15-20, Bolesta said.
"They are looking good," he said in late August.
J&S Produce Inc, Mount Vernon, Ga., plans to begin harvesting Oct. 1.
The company harvests throughout the summer but fall volume typically commences then, said Joey Johnson, president.
"We expect good supplies," Johnson said. "Michigan and some of those areas start playing-out. Everyone begins moving their business back to the Southeast about that time."
Northampton plans to begin squash harvesting on Oct. 1, but could start a week earlier, Cullen said.
"Barring weather problems, we should have good quality and good volume," he said in late August. "Northern demand during the summer has been pretty good so far."


Eggplant

Ken Corbett Farms plans to start harvesting eggplant on Sept. 15.
"They are looking great," Bolesta said in late August. "They are young and healthy. There's no disease pressure on anything, which is great."
Eggplant does well in heat, so produce quality fruit this season should be good, given Georgia's high late-summer heat, he said.
Fresh Link expects to begin harvesting Oct. 10 with volume to begin by Oct. 15, as normal, Sterling said.
"Usually, the fall eggs are very good here," he said. "They like the hot weather. Buyers should expect good supplies and good quality."
Georgia volume should be heavy during its start as Michigan production winds down, Northampton's Cullen said.
"Volume is down in the northern areas," he said. "Heat is attributed to some of that. We should have good volume, good quality and good sizings."