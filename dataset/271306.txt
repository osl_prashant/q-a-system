Springtime is an exciting time when many 4-H families are adding new animals to their farms. Bringing new animals home also means potentially bringing home new diseases, too. You can make sure you’re reducing risk and bringing home healthy animals by asking a few questions and inspecting animals properly.
Pre-purchase
Vaccination status. Ask for complete vaccination records to make sure the animals you’re potentially integrating into your herd or flock have been properly vaccinated.
Herd or flock health status. Ask about the farm history of disease. Various species are susceptible to various diseases, and to protect your investment it is essential you are aware of any issues.
Deworming/anti-parasitic programs. Check fecal egg counts and get a history of past deworming practices. Animas should also be checked for signs of lice, mange, ringworm and wards.
Post-purchase
You have done your due diligence prior to purchase! Now, in order to protect your existing animal population at your farm, it is important to quarantine, or separate, your new animals to ensure their health.
Quarantine new animals for a minimum of three weeks before introducing them to your entire heard or flock.
Use separate housing, if possible, and separate feed and feeding areas and water sources.
Provide booster vaccinations if necessary.
Minimize stress as much as possible by providing clean, dry and comfortable housing with adequate ventilation.
Always tend to newly added animals last. This will help prevent human cross-contamination.
Once you have completed the quarantine period and none of the animals have exhibited any signs or symptoms of illness or disease, it is safe to integrate them into the rest of the herd.