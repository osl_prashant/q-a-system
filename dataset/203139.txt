(UPDATED, Jan. 24) Despite the support of many agricultural groups, the Trans Pacific Partnership is dead.
 
Following through on a campaign promise, President Donald Trump signed an executive order Jan. 23 formally withdrawing the U.S. from the 12-nation Trans-Pacific Partnership trade agreement. The trade agreement was signed last year but had not yet been approved by Congress.
 
Speaking Jan. 23 before the announcement was made, Ken Barbic, senior director of federal government affairs at Western Growers, said the group supports the agreement.