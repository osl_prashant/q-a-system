A simple thing like a printer stuck in my craw today. 
What happened was mine quit on me. I was catching up on some follow-up work when, wham! Nothing. A cute pop-up said my printer was out of black ink. Ugh.
 
As it happened late on a Saturday night (yes, I have no social life), I ventured into the cold to buy a replacement cartridge. 
 
The first office supply store had ample supplies, but not what I needed. I then drove to a nearby big box store, where the clerk tapped on his inventory gizmo and said the black ink I needed was only available online — oops.
 
I Google-searched office supplies from the parking lot inside my pickup as the rain poured down harder.
 
As luck would have it, I found another store and the exact cartridge I needed to continue working into the night. And for a mere $96 bucks. 
 
Yikes. Who prices these things anyway?
 
But this got me thinking. Just how many retailers are filing for bankruptcy or closing stores these days? How many brick and mortars are throwing in the towel, blaming increased competition or online sales for lackluster performance? 
 
The list includes prominent names such as Sears, Payless, JC Penney, Macy’s, The Limited, Abercrombie & Fitch, Crocs. Banners that just a few years ago seemed immune to failing now seem to be leaning that direction.
 
Can supermarkets be far behind?
 
While the Internet juggernauts such as Amazon play their hand with e-commerce grocery shopping, one can only imagine the effect if they can make a dent in the multi-billion-dollar food industry. 
 
If people can click their way through grocery shopping, where data compiles what shoppers buy most often, will we eventually even need a traditional grocery store, a pharmacy, or a produce department?
 
Which brings me back to my wacky printer ink shopping experience.
 
My humble advice to grocers, and specifically to produce managers, is simple: Keep your shelves stocked. And don’t run short of inventory.
 
I mean, c’mon, right? If, for example, I’m shopping for salsa ingredients, you need to have inexpensive (but hardy) roma tomatoes, white onions, cilantro, fresh jalapeños, a tomatillo or two, limes, and some green onions on hand. 
 
If you’re consistently out of one or more ingredients, I’m quietly heading to your competitor. If it happens more than a couple times, they very well may win me over as a regular shopper.
 
It may not be as headline-grabbing as the hardlines or clothing stores saga, but the long-term effect can have an equal impact.
 
Armand Lobato works for the Idaho Potato Commission. His 40 years’ experience in the produce business span a range of foodservice and retail positions. E-mail armandlobato@comcast.net.
 
What's your take? Leave a comment and tell us your opinion.