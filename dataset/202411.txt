A court has ruled that an Israeli grape grower infringed on the intellectual property rights of Bakersfield, Calif.-based Sun World International, LLC.
 
The Israeli Central District Court ruling said the interests of entities making large financial investments in the development of their varieties should be protected, according to a news release.
 
The court issued an injunction prohibiting the grower from marketing fruit produced from Sun World's proprietary grapevines. The grower has agreed to uproot his vineyard of Sugrathirteen grapes - a proprietary black seedless grape variety owned by Sun World and marketed by the company and its licensees under the Midnight Beauty brand - and cease growing the variety, according to the release.
 
"We are pleased with the court's unambiguous message that fruit breeders' rights prevent a protected variety's growth and proliferation without the lawful owner's permission," Sun World International executive vice president David Marguleas said in the release.
 
Growers in more than 40 countries are licensed to produce Sun World fruit, and the company and is committed to aggressively protecting its rights throughout the world, according to the release.
 
Marguleas is president of Sun World Innovations, the company's technology and licensing division and the entity that manages the California company's intellectual property portfolio. 
 
"This landmark case reflects the importance of intellectual property in agriculture and supports a shift of attitude amongst producers towards respect for these rights," Marguleas said in the release.