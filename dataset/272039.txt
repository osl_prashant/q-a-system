Russia's Supreme Court rules against Navalny
Russia's Supreme Court rules against Navalny

The Associated Press

MOSCOW




MOSCOW (AP) — Russia's Supreme Court has upheld a criminal conviction against opposition leader Alexei Navalny and his brother.
The 2014 conviction was formally the reason that Navalny was barred from running in the March presidential election.
Navalny and his brother, Oleg, were convicted in 2013 of embezzling timber worth $500,000 from a state-owned company. Critics saw the trial as a political vendetta.
Following unsuccessful appeals Navalny turned to the European Court of Human Rights, which in 2016 ruled in his favor. The Supreme Court ruled on Wednesday against a retrial, upholding the conviction that gave Navalny a suspended sentence and sent his brother to prison for 3½ years.
Navalny said after the hearing that the ruling shows Russia's refusal to respect its international obligations.