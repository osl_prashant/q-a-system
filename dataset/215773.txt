Construction of the $90 million "Cool Port" at Port of Oakland is one of several projects the port is counting on to bring record cargo volume in the coming years. File photo
Several construction projects have Port of Oakland officials bullish on the prospect of attracting additional business and reaching record cargo volume starting next year.
Maritime director John Driscoll predicted additional containerized cargo in 2018 and continuing through 2022 in an address to supply chain executives gathered  to review Oakland’s operating performance, according to a news release.
“I’m forecasting growth because of the development that’s going on here,” Driscoll said, according to the release. “It won’t be dramatic — it will be steady — but it will result in more cargo volume than we’ve ever had before.”
Driscoll said three international shipping lines are considering making Port of Oakland their first call due to recent port improvements. Any of them making the switch would increase cargo volume, according to the release.
Projects drawing the most interest, according to the release:

Cranes: Four ship-to-shore cranes at the Oakland International Container Terminal are being lifted by 27 feet to accommodate megaship loading and unloading, at a cost of $14 million to $20 million. The second crane will be lifted by the end of this year, with work on the other two finishing up in mid-2018;
Cool Port Oakland: Cool Port will process beef and poultry exports in a 280,000-square-foot refrigerated distribution center that is expected to handle 27,000 20-foot equivalent units of meat each year. The $90 million facility should open next August;
Seaport Logistics Complex: Construction of a $52 million, 440,000-square-foot transloading facility is expected to start in late 2018; and
Truck service center: An 8-acre facility with food stops, fueling stations and overnight parking for harbor drivers is still in the negotiation stage.

The Port of Oakland reported total volume of 2.37 million 20-foot equivalent units in 2016, and earlier this year projected it would handle 2.6 million containers by 2022, according to the release.