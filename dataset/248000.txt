BC-US--Cotton, US
BC-US--Cotton, US

The Associated Press

New York




New York (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Tuesday:
(50,000 lbs.; cents per lb.)
COTTON NO.2Open    High    Low    Settle     Chg.Mar       87.75   87.75   83.84   83.84  Down 1.57May       84.97   86.60   81.71   82.43  Down 2.80Jul       84.68   85.83   81.98   82.49  Down 2.49Aug                               77.98  Up    .03Oct       78.60   78.68   78.60   78.68  Down 1.11Oct                               77.98  Up    .03Dec       77.73   78.45   77.62   77.98  Up    .03Dec                               78.07  Up    .07Mar       78.01   78.39   77.88   78.07  Up    .07May       77.90   78.19   77.90   78.04  Up    .14Jul       77.64   77.89   77.64   77.83  Up    .19Aug                               72.60  Up    .11Oct                               75.48  Up    .19Oct                               72.60  Up    .11Dec       72.49   72.60   72.35   72.60  Up    .11Dec                               72.96  Up    .10Mar                               72.96  Up    .10May                               73.73  Up    .08Jul                               73.96  Up    .08Aug                               73.01  Up    .01Oct                               73.65  Up    .08Oct                               73.01  Up    .01Dec                               73.01  Up    .01