Crop calls
Corn: 1 to 2 cents higher
Soybeans: 1 to 3 cents higher
Winter wheat: 1 to 2 cents higher
Spring wheat: 2 to 5 cents higher
Short-covering lifted grain and soybean futures overnight as traders acknowledge the oversold condition of the markets. But traders will maintain a cautious attitude as they expect Friday's Grain Stocks Report to reflect ample supplies. USDA will also release its Prospective Plantings Report on Friday, which is expected to reflect farmers' plans to plant less corn than last year and more soybeans. A firmer tone in the dollar index and rains moving across the Central and Southern Plains limited buying in winter wheat futures overnight.
 
Livestock calls
Cattle: Mixed
Hogs: Lower
Some additional light cash cattle trade took place in Texas yesterday at around $125.50, which is down $4.50 from the previous week, but still well above where nearby futures are trading. Buying will be limited to short-covering in cattle futures as traders are also unimpressed with this week's beef market performance. Meanwhile, hog futures are called slightly lower on followthrough weakness as well as cautious trade ahead of tomorrow's Hogs & Pigs Report that's expected to reflect expansion in the industry. The cash hog market is also called weaker.