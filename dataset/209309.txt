Hurricane Irma is long gone now but dairy producers in Florida are still dealing with the aftermath from the storm nearly a week afterwards.Dakin Dairy Farms outside Myakka City, 60 miles south of Tampa, has on-farm bottling. Unfortunately, the dairy has been forced to dump milk because there is no way for consumers to store it while power is out.
Jerry Dakin, owner of Dakin Dairy Farms, says most of the milk goes to the southern part of the state where many stores have no power.
Dakin told Bay News 9, he estimates the losses to be $30,000 per day from dumping milk. Milk dumping started on Sept. 8, however some milk has been moved to production of other products.
“We're fortunate we have a separator where we’re separating the milk and taking the fat off so that cream can go to ice cream or butter,” Dakin says.
Additional damage at Dakin Dairy Farms included roofs blowing off and milk storage tanks being destroyed. Dakin believes there could be more than $250,000 in physical damage to the farm on top of lost milk revenue.
Down the road 90 miles to the east, dairies around Okeechobee are still without power a week after Hurricane Irma.
For the past few days Colleen Larson, a regional dairy extension agent with University of Florida, has been out in the field assessing damage and delivering MREs (Meal, Ready-to-Eat) to dairies.   
“A handful of dairies have power, mostly the ones closer to town,” Larson says. She estimates half of the 24 dairies in Okeechobee County don’t have power.

Hurricane Irma blew over grain bins and trees at C&M Rucks Dairy outside Okeechobee, Florida. (Colleen Larson, University of Florida Extension)
Complicating matters from the lack of electricity is the heat and humidity cows are enduring. Temperatures have been around 90°F for the high and humidity reached 84% on Sept. 15. Without power many dairies can’t effectively cool cows with fans and sprinklers as they normally would.
“Cows are getting hot and production is going down,” Larson says. She adds health problems could follow if dairies continue to deal with no electricity and high temperatures.
In addition to being in extension, Larson is married to Travis Larson, who co-owns and runs Larson Dairy Inc. with his family. Travis has had power for a few days now at his own dairy. During the storm power was kept on at the parlor thanks to a generator.
Larson’s brother-in-law, Jacob Larson, has a dairy just down the road and he is still without power, besides his own generator. “Jacob’s trying to get water to flush barns and he is struggling to operate normally,” Larson says.
Despite the lack of power at dairies, many have generators set up to keep the parlor going similar to the Larson family. Larson hasn’t heard of any dairies in her area dumping milk, but there is no milk in the grocery stores.
“I know that some of the processing plants had issues getting started because they were down longer than anticipated,” Larson says. “It is kind of frustrating to explain to the public why we don’t have milk in stores when we have been milking right along.”
Farms in the area were only down for three to six hours when the storm was at its worse, Larson adds.
The majority of corn silage has been harvested around Okeechobee, so very little corn was laid over by wind. Many silage piles lost plastic tarps, exposing the piles to more air and excess moisture in the forage. The quality of silage for some farms will likely be decreased.
High winds also tore the tin from some free stall barns, including a half of one side of Travis Larson’s barn. At Jacob Larson’s dairy there was very little damage to the barns.
It rained 10-14 inches in 24 hours across Okeechobee County, but flooding was limited.
Below are some photos from social media shared by dairies across Florida showing the damage from Hurricane Irma: