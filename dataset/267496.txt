BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Thursday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2539  Up    14May         2477    2534    2433    2508  Up    33Jul                                 2557  Up    14Jul         2520    2569    2483    2539  Up    14Sep         2540    2585    2502    2557  Up    14Dec         2544    2590    2507    2562  Up    14Mar         2535    2579    2500    2554  Up    15May         2548    2584    2511    2558  Up    14Jul         2512    2590    2512    2565  Up    15Sep         2519    2597    2519    2572  Up    16Dec         2526    2605    2526    2578  Up    18Mar                                 2600  Up    18