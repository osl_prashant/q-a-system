This week, the USDA released its wheat crop conditions. In Kansas and Oklahoma, roughly half of the states’ crops are in the poor to very poor range, and farmers will have to make some tough decisions.

Chip Flory, host of AgriTalk and AgriTalk After the Bell, says the next two to two-and-a-half-weeks will be interesting to see the decisions farmers will make: will they rip it or buy back insurance?
During the Markets Now segment at the U.S. Farm Report taping at Commodity Classic, Flory thinks if farmers plow the crop, it will have an impact on feed grain markets, whether that’s in sorghum or soybeans.

In Chicago, funds are “blowing out of their short wheat position,” according to Mark Gold, founder, Top Third Ag Marketing. With new crop wheat sales hovering in the $5 range, he says this is an opportunity to “sell what you want to sell and look to buy a call option.”

Arlan Suderman of INTL FCStone says the wheat picture is “critical” and reminds him of 2011. He advises farmers to think about the tools they’re going to use and to have a plan. Decisions will come sooner rather than later this season because of the situation.

“The crop is in rough shape,” said Mike North, president of Commodity Risk Management Group. “It’s one of those markets that blows off pretty fast. Make sure you’ve got some calls."

For North, this short-term window is an opportunity for farmers to make sales.

Hear their full comments on Markets Now on U.S. Farm Report above.