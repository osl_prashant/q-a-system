Research looking at the rates of pathogen die-off in strawberry and cilantro growing fields is the focus of a study funded by the Center for Produce Safety.
 
A $50,000 award was given to researcher Eduardo Gutierrez-Rodriguez of North Carolina State University and co-investigators Chris Gunter, Sid Thakur and Carole Saravitz, all from North Carolina State University.
 
The research project involves strawberries and cilantro. Neither of those commodities typically receive a post-harvest disinfection treatment, according to a news release.
 
As part of the project, researchers are looking at whether the implementation of pre-harvest intervention strategies - including chlorine applications through the irrigation system
- could reduce pathogen contamination when using agricultural water that does not meet the microbial standards, according to the release.
 
"The most tangible (benefit) is whether the application of those sanitizers prior to harvest could enhance in-field pathogen die-off without harming the crop and thus reduce the risk of contamination and pathogen persistence in cilantro or strawberry," Gutierrez-Rodriguez said in the release. "This information, hopefully, will help us further complete our understanding of in-field microbial die-offs in cilantro and strawberry."