Leaders of Dairy Management Inc. (DMI), the National Dairy Promotion and Research Board (NDB) and the United Dairy Industry Association (UDIA) announce the following dairy farmers as new officers.
The DMI and NDB elections were held during the February board meeting of DMI.
DMI officers:

Chair – Marilyn Hershey, Cochranville, Pa.
Vice Chair – Steve Maddox, Riverdale, Calif.
Secretary – David “Skip” Hardie, Lansing, N.Y.
Treasurer – Larry Hancock, Muleshoe, Texas

DMI, which manages the national dairy checkoff program, is funded by the NDB and United Dairy Industry Association (UDIA), which serves as the federation of state and regional dairy checkoff-funded promotion organizations.
NDB officers:

Chair – Brad Scott, San Jacinto, Calif.
Vice Chair – Connie Seefeldt, Coleman, Wis.
Secretary – Steve Ballard, Gooding, Idaho
Treasurer – Carol Ahlem, Hilmar, Calif.

The 37-member NDB, formed in May 1984 under the authority of the Dairy Production Stabilization Act of 1983, carries out coordinated promotion and research programs to help build demand, and expand domestic and international markets for dairy products.
UDIA officers, elected in October, are:

Chair – Neil Hoff, Windthorst, Texas
1st Vice Chair – Allen Merrill, Parker, S.D.
2nd Vice Chair, American Dairy Association (ADA) – Tom Woods, Gage, Okla.
2nd Vice Chair, National Dairy Council (NDC) – Audrey Donahoe, Frankfort, N.Y.
2nd Vice Chair, UDIA Member Relations – Rick Podtburg, Greeley, Colo.
Secretary – Jim Reid, Jeddo, Mich.
Treasurer – John Brubaker, Buhl, Idaho

The UDIA is a federation of state and regional dairy farmer-funded promotion organizations that provide marketing programs that are developed and implemented in coordination with its members. The UDIA is overseen by a board comprised of dairy farmers elected by their respective boards of their member organizations.