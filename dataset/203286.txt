SAN FRANCISCO - Recent trends have seen foodservice potato sales growing faster than retail sales, and one market analyst believes that pattern will continue in 2017.
 
Hot trends such as offering breakfast all day should be a money making opportunity for potatoes, according to Amy Shipley, managing director and partner at the Boulder, Colo., office of the Sterling Rice Group. Sterling Rice works with Potatoes USA in promoting potatoes to foodservice operators.
 
Shipley spoke at a fresh market breakout session Jan. 5 at the 2017 Potato Expo.
 
Revealing data from a just-released study by Technomic Inc., she said total foodservice sales topped $845 billion in 2016, with 1.7% growth expected for 2017. More than half of consumer purchases of food are spent away from home, she said. Fifty-seven percent of U.S. potatoes are sold in foodservice outlets.
 
For 2017, fast casual restaurant sales are expected to increase 6.1%, with fine dining predicted 3.2% higher and supermarket foodservice forecast to jump 6%.
 
Foodservice promotions with Potatoes USA also are paying more attention to noncommercial foodservice, such as university dining, she said. "The University of Colorado has one of the most innovative dining programs in the world, and I challenge you to go in there and think any differently about them versus one of the hottest restaurants in the country," she said. Shipley said university foodservice operations are often more open to input than top commercial chains.
 
"They are changing their menu constantly because the kids are eating there for break, lunch and dinner and snack," she said. "It's a big opportunity for us."
 
Young consumers are driving the conversation about foodservice sales, with millennial (born from the early 1980s to 2000) and generation Z consumers (those born after 2000) totaling close to $90 billion in spending power. For millennial consumers, 30% of meals are eaten away from home, with 27% eaten away from home for generation Z consumers.
 
Excluding alcohol, the foodservice market was a $736 billion market in 2016, according to the Technomic study, with restaurants representing 65% of the total, noncommercial sales representing 15%, retailers 8.7%, travel/leisure 10.4% and all other foodservice 0.5%.
 
Among the nation's top 500 chains, Shipley said the fast casual category is the fastest growing, gaining 11.5% in 2016 compared with a year earlier. Other above-trend categories are upscale quick service restaurants such as Culver's (8.2%) and quick service restaurants (4.4%).
 
"Fast casual is the bright spot the last five years - these guys are killing it," she said. Everyone is scrambling to see who will be the next Panera and Chipotle, she said, noting that 5 Guys restaurant is doing very well. Other fast casual restaurants on the rise include Zoe's Kitchen, Shake Shack, Blaze Pizza, Smashburger and Habit Burger Grill.
 
Trending
 
The top trends for 2017 will include breakfast all day, she said.
 
"Breakfast is growing like mad," Shipley said, noting that the minute McDonald's offered breakfast all day the whole industry responded.
 
The breakfast trend is very positive for potatoes.
 
Distribution trends are also changing, with the Amazon Go grocery concept - featuring no cashiers or checkout - expected to influence the foodservice industry. Another restaurant is distributing its product through commissaries with an online app, she said.
 
Takeout is popular with millennial consumers, with 61% stating that the takeout/delivery option is important when choosing a table service restaurant. The survey said that 46% of millennial consumers demand convenient technology for ordering, such as the app-driven Eatsa restaurant.
 
Meal kits also are a growing trend, with total sales in 2016 at about $1.1 billion.
 
Retail foodservice sales have climbed 30% sine 2008, and accounted for 2.4 billion foodservice visits and $10 billion in sales in 2015, an Nielsen Perishables Data report said.
 
Plant-based restaurants are also growing strong, Shipley said. "The reason why plant-based took off in foodservice, in my opinion, is because it is cheaper," she said. Higher costs with beef, pork and chicken have led chefs to use produce.
 
"This is a big opportunity for potatoes," she said, noting that chefs need to learn how to cook a plant-based menu. "They love potatoes because they offer a canvas for fresh global flavors, they can afford them and they feel like they are just starting to discover the vast portfolio of fresh potato options that are out there."
 
Some plant-based restaurants on the rise include Beefsteak, The Beyond Burger and Native Foods Cafe, Shipley said.
 
Potatoes are the top food eaten at foodservice for dinner, Shipley said. Potatoes have seen an 31% increase in core menus since 2014, and 18% of foodservice operators are incorporating more potato dishes on menus than two years ago, Shipley said.
 
Going forward, foodservice promotions for Potatoes USA will seek to: 
Position potatoes as the "it" ingredient;
Stress the potato's profit potential and value; and 
Underline the all-day versatility of potatoes on menus.