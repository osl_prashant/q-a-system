Robert Bennen Jr., owner and operator of Nogales, Ariz.-based Ta-De Produce Distributing, died Jan. 14. He was 45 years old.
Robert Louis Bennen Jr. was the son of Robert and Rosie Bennen and the grandson of Carlos Bennen, the founder of Ta-De Produce Distributing. Carlos Bennen, three-time president of the association that would later become the Fresh Produce Association of the Americas and marketing pioneer of vine-ripe tomatoes, died in 1988.
“He taught me to work smarter, not harder,” Robert Bennen Jr. said at a tribute to his grandfather Carlos in 2015. “If you’re going to do something, do it right, and we’re all competitors here, but we’re a community.”
Robert Bennen Jr. was a graduate of Nogales High School and received a Bachelor of Arts from the University of Arizona. He had worked at Ta-De Distributing since 1996, according to his LinkedIn profile.
An online guest book is available to express condolences to the family.
According to an obituary from Martinez Funeral Chapels in Nogales, services were at Sacred Heart Catholic Parish Jan. 19.
Survivors include his wife, Satenik Valenzuela Bennen, and children Victoria, Carlos and Caroline.
In lieu of flowers, the family has requested donations to the Kino Border Initiative, Salpointe Catholic High School and Boys and Girls Club of Santa Cruz.