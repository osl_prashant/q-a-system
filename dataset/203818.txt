Every time public scientists seek solutions to the challenges faced by Florida growers, they wear down buildings and equipment, run up utility bills, and come across a mountain of reporting paperwork.
 
Those hidden expenses - not directly related to lab or greenhouse work but necessary to it - are called indirect costs, or IDCs. They're just as real as the cost of paying scientists and staff to focus their expertise on a particular commodity's challenge.
 
For many years now, the University of Florida's Institute of Food and Agricultural Sciences has granted Florida commodity groups an exemption from these costs. But that exemption costs nearly $5 million a year.
 
Consider an analogy. What if we bought tractors to rent out to the state's farmers? Wouldn't it be fair that we charge for gas, engine wear and tear, repairs and fleet management?
 
I listen to growers and feel their pain. That's why I have already delayed for three years bringing back the indirect costs charge for producers - a charge that all other funding agencies are paying at a much higher rate than the 12% industry rate.
 
The growers' calls for help are also why IFAS has marshaled so many citrus researchers to work to save the industry from the scourge of huanglongbing, for example.
 
UF/IFAS has a building on its main campus that was about the only place in the state allowed to accept the first HLB-infected trees for research.
 
The building is nearly 25 years old now. IDC dollars are paying for repairs to the cooling system and for installing a backup system so we don't lose research in the event of a breakdown.
 
In fact, half the building has never been used because it wasn't outfitted with cooling and the kind of protections necessary to contain some of the most challenging of agricultural diseases. 
 
We're completing it with IDC dollars so we can effectively double our pathology research on HLB in that facility.
 
The building is one of the main centers in pursuing one of the HLB research grails - a way to culture the bacteria that causes the disease. UF/IFAS plant pathologist Dean Gabriel can't do his culturing work without the building. Nor would he and his team have been able to receive a $4 million federal grant for it.
 
IDC collections allow UF/IFAS the flexibility to address critical research infrastructure needs. Flexibility means speed. We don't have to wait for specific state appropriations to fix Gabriel's lab or for other approvals that serve as a drag on the pace of discovery.
 
The state doesn't give us an allowance for renovating our labs and greenhouses. Nor can we do it with tuition dollars. So far we haven't found a philanthropist willing to bankroll the quarantine facility.
 
We can't do research like Gabriel's without barbed-wire fencing, negative pressure chambers, electronic locking systems, an autoclave and other features for working with emerging infections.
 
When we recruit new scientists to join our anti-HLB team, they all want to know what tools they'll be provided. They ask about labs, offices, high-tech machines, and support for attending professional conferences. IDC dollars provide startup packages to recruit these researchers and enable them to excel when they arrive.
 
The citrus industry has long supported the UF/IFAS search for solutions to producers' challenges. We appreciate that producers increasingly recognize that infrastructure is a necessary part of the solution and that we need IDC dollars to pay for it.
 
IDC dollars increase the effectiveness of our research. They allow researchers to do what they do best - solve ag's problems.
 
Editor's note: This is in response to Eastern Editor Doug Ohlemeier's column "University research tax worries Florida growers."
 
Jack Payne is the University of Florida's senior vice president for agriculture and natural resources and leader of the Institute of Food and Agricultural Sciences. E-mail him at jackpayne@ufl.edu.
 
What's your take? Leave a comment and tell us your opinion.