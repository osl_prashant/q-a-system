Fowler, Calif.-based Bee Sweet Citrus is offering Chilean citrus, timed for back-to-school demand. 
Chilean navels and murcott mandarins are available in big volumes, according to a news release.
 
“Chilean navels and murcott mandarins both make excellent back-to-school snacks,” Bee Sweet Citrus director of sales Joe Berberian said in the release.
 
Bee Sweet Citrus offers Chilean Murcotts in 2-, 3-, and 5-pound bags, according to the release. Chilean navels are offered in 3-pound and in bulk.
 
U.S. Department of Agriculture shows Chilean citrus imports peak in the July through September period. In 2016, imports of Chilean citrus during those months accounted for 75% of total 2016 U.S. imports. 
 
Chilean citrus imports have been on the rise in recent years. The USDA said imports of Chilean citrus rose to 188,000 metric tons in 2016, up from 165,600 metric tons in 2015 and 118,000 metric tons in 2014.