Last week, China announced its 25 percent tariff on pork. As tariff talks escalated, China has talked about possible tariffs on some beef products.

The export picture for U.S. proteins wasn’t always in focus. According to Jim Mintert, director for the center of commercial agriculture at Purdue University, less than 2 percent of U.S. meat production was exported in the mid-1980s. Now, between 16 to 17 percent of pork, beef and poultry are exported.

“It’s become a very, very important source of demand,” he said. “We used to be almost entirely domestically focused. If you think about it from a long run perspective, all the growth opportunities are really in the export channels.”

China is the second biggest economy in the world, behind the U.S. In 2017, China imported 7 percent of U.S. pork by volume, a big difference since the mid-80s.

Watch his full analysis on AgDay above.