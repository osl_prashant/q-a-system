In recent years, the Chilean grape industry has made a substantial investment in developing new varieties.
Karen Brux, managing director of North America for the Chilean Fresh Fruit Association, San Carlos, Calif., said a number of new grape varieties have been planted in Chile, with the focus on larger berry size, better flavor, better color, specific shape (more round, more elongated, etc.) and higher productivity per acre.
"Our global markets want grapes that have better shape, size, color and flavor, and we are committed to delivering on this," she said.
"Numerous new varieties have been introduced to Chile and you will see them replacing some of the more traditional varieties within a very short time frame. Within these new varieties, most of them correspond to patented varieties from different parts of the world."


New variety boom

Brux said there is a long list of new varieties that have been planted in Chile during the past four to five years, and in the past two to three years, new orchards with patented varieties have materialized.
"We expect strong increases in export volumes of new varieties during the 2016-17 season," she said.
Tom Wilson, grape sales manager for Los Angeles-based The Giumarra Cos., said some of the company's proprietary varieties are being grown by its grower/export partners in South America.
The volumes of the new varieties will grow year-to-year moving forward and will likely eventually replace the old varieties now planted, he said.
"California has set the tone for what consumers, and therefore retailers, expect in the way of flavor and size," Wilson said.
"For the grower, these new varieties bring the benefits of larger yields, larger fruit, use less labor, less (pesticide) applications and are more sustainable for the environment. Our grower/exporter partners are currently growing our green seedless and red seedless, with plans to grow our black seedless varieties in the future."
Giumarra Cos.' main new varieties in production are Early Sweet and Arra 15, which are green seedless varieties, and Arra 19 and Arra 29, which are red seedless.
Nolan Quinn, commercial manager for Summit Produce, Fresno, Calif., said the company is expanding early season production from the north (Copiapo) region of Chile with new varieties, allowing it to bring better sizing and quality than what traditionally comes from that area.
"We're replacing older varieties (flames and perlettes) with newer, better varieties," he said.
"With our growth in the north and new variety development, Gesex (owner and primary grower of Summit Produce) is increasing from 3 million cartons of grapes last season to over 4 million cartons by 2020."