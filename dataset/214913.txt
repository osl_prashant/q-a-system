What makes the swine industry so special? I believe it is the many people that work in it: family members, managers, and all the barn employees. Being a swine caregiver, or a swine technician, can be a rewarding occupation. However, employee turnover seems to remain high. The National Pork Board 2016-2017 “Employee Compensation & HR Practices in Pork Production” report shows the average reported turnover rate for animal caretakers on large and small to mid-size producers was 35.03% and 20.34%, respectively. Turnover rates for farm managers was lower at around 9% for both large and small to mid-size producers. In the 2008 “Human Resource Needs Assessment for the Pork Industry” conducted by the University of Guelph for the Ontario Pork Industry Council, reported employee turnover on participating farms was 39.6%.
Voluntary vs. Involuntary Turnover
These percentages provide an average total employee turnover rate on an annual basis. So what does this tell us? To gain a better understanding of total employee turnover, let’s look at the different types of turnover: voluntary and involuntary. Voluntary turnover is employees choose to leave for whatever reason. Involuntary turnover includes layoffs and similar actions where the employee leaves because the company made the decision not the employee. Mr. F. John Reh (2017) states, “As a general rule, voluntary turnover is the measure used to discuss and compare employers. It is the type most directly affected by the front line supervisors. Involuntary turnover, caused by layoffs, can be a long-term result of high levels of voluntary turnover.”
Let’s look at an example of calculating these turnover rates based on averages from the 2016 National Pork Board’s report, and discuss the potential cost of the turnover rates.
Example Scenarios
Two Scenarios: A larger organization versus small to mid-size organization with cost of turnover being estimated around 150% of an hourly position’s salary and 250% of management position’s salary (Moore, 2012).
Barn Bacon (Larger organization)
Barn Bacon has 250 employees (general caregivers) at the beginning of the year, 50 voluntarily leave the operation and 5 are dismissed throughout the year.

Voluntary turnover rate:
	50 left voluntarily/250 total employees at beginning of year = 20% voluntary turnover
Involuntary turnover rate:
5 dismissed/250 total employees at beginning of year = 2% involuntary turnover
Total turnover rate: 
	(50 voluntary + 5 involuntary) / 250 total employees at beginning of year = 22% total turnover rate

General caregivers in Barn Bacon’s sow farm earn around $11.50/hour (starting wage, no experience) and work 45 hours/week, giving an annual salary of approximately $26,910 for 52 weeks considering no overtime. The estimated cost of turnover for one hourly employee at Barn Bacon’s sow farm is $40,365 ($26,910 x 150%). Let’s assume all 55 employees that left are general caregivers and not management positions. This comes to $2,220,075 for total turnover costs per year ($40,365 x 55 employees) to replace them! If any of the employees that leave held management positions with likely an annual salary of $70,000 or more, the cost of turnover can continue to grow by $175,000 per manager ($70,000 x 250%) to replace them.
Barn Pork Chop (Small to mid-size organization)
Barn Pork Chop has 9 employees (general caregivers) at the beginning of the year, 2 voluntarily leaves the operation and 0 are dismissed throughout the year.

Voluntary turnover rate: 
2 left voluntarily/9 total employees at beginning of year = 22% voluntary turnover
Involuntary turnover rate: 
0 dismissed/9 total employees at beginning of year = 0% involuntary turnover
Total turnover rate: 
	(2 voluntary + 0 involuntary) / 9 total employees at beginning of year = 22% total turnover rate

General caregivers in Barn Pork Chop’s sow farm earn around $12.50/hour (starting wage, no experience) and work 45 hours/week, giving an annual salary of approximately $29,250 for 52 weeks considering no overtime. The estimated cost of turnover for one hourly employee at Barn Pork Chop’s sow farm is $43,875 ($29,250 x 150%). Let’s assume the 2 employees that left are both general caregivers. This comes to $87,750 for total turnover costs per year ($43,875 x 2 employees). If one of these employees is a manager, then the total turnover costs could be around $193,875 ($43,875 + $60,000 x 250%) to replace them.
Understanding the Impact
Despite these two barns having the same total turnover rate, there is a different economic impact in terms of the potential cost of employee turnover. This cost of employee turnover may come from hiring and recruiting costs, training costs, new hire costs, disruptions to both animal and human productivity, and shifts in employee scheduling and work responsibilities to cope. The true cost of employee turnover can be difficult to quantify and the cost has been estimated as low as 30% of the employee’s salary for Ontario pig farms and as high as the 150% of an employee’s wage as I used in these examples.
Employee turnover can have potential impacts on animal well-being, though this area of research is under-investigated and difficult to accurately quantify (piglet survivability, morbidity rate, mortality rate, etc.). Managers should watch for frustration and fatigue with the remaining employees that take on the additional responsibilities of caring for the animals. Employee turnover is inevitable, but how managers respond to it and learn from it will determine the resiliency of the barn’s future and the level of productivity that is achieved.
Now that you know how to calculate the different kinds of turnover, look into the “Human resource Needs Assessment for the Pork Industry” resource (pp. 23-31) to learn tips on improving employee retention.