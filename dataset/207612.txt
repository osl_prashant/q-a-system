Alsum plans events, expansion
Alsum Farms & Produce Inc., Friesland, Wis., is partnering with the Green Bay Packers to give one lucky football fan free groceries for a month.
From Aug. 15 to Nov. 30, Packer fans can visit www.packers.com/fan-zone/contests.html to enter their name, address and e-mail for a chance to win.
Alsum also is launching a Harvest Delites brand in the fall with russet, red and gold potato bags.
Christine Lindner, national sales and marketing representative, said the brand eventually will expand to include other produce items as well.
Meanwhile, Alsum expects to complete a 16,800-square-foot addition to its storage facility in Friesland in the fall, just in time for new crop Wisconsin russets, Lindner said.
Alsum also is hosting the inaugural Tater Trot 5K Run/Walk on Oct. 28, in Friesland. The event will benefit local FFA chapters. 
 
Bushmans’ Inc. adds sales staff
Bushmans’ Inc., Rosholt, Wis., has added two salesmen for its Mukwonago, Wis., office. CEO Mike Carter said the company hired Chris Fleming and Michael Eader.
Fleming, who previously was responsible for new business development and outside sales for Strube Celery & Vegetable Co., Chicago, joined the company in October. Eader, previously a buyer for Aldi, was hired in March.
Carter said the hires were necessary, in part, because of the retirement of Rick Kantner. Kantner joined Bushmans’ subsidiary Katz Produce Sales Inc., Rosholt, in 2014 after several years as director of sales and marketing for Alsum Farms & Produce.
 
Okray installs grading equipment
Okray Family Farms Inc., Plover, Wis., is adding automatic graders to its packing lines, said owner Mark Finnessy.
“It’s a big project,” Finnessy said July 18.
“We started at the end of June. We have such a small window. We’re hoping we’ll be done by Aug. 11.”
Finnessy said grading previously was done by employees, but inconsistent labor supply made the new equipment necessary.
 
Tasteful Selections expands facility
Tim Huffcutt, marketing director for RPE Inc., Bancroft, Wis., said a 70,000-square-foot expansion has been completed on the Tasteful Selections facility in Arvin, Calif.
The addition includes cold storage, shipping docks and quality control space.
Huffcut said Tasteful Selections is the largest grower, shipper and marketer of bite-sized potatoes in the U.S., with 9,000 acres.