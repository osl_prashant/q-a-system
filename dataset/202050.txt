USDA has proposed new animal welfare standards for the National Organic Program that, if enacted, would be the first time such standards are codified in federal law and would present serious challenges to livestock producers, according to NPPC, which opposes the new standards There are a number of problems with the proposed new organic animal welfare rules, NPPC points out, including:Animal welfare is not germane to the concept of "organic."
Organic has pertained to foods produced without synthetic pesticides, antibiotics, synthetic fertilizers, genetically modified organisms or growth hormones.
The Organic Food Production Act of 1990 limited its coverage of livestock to feeding and medication practices.
While the Agriculture Secretary can consider additional provisions, they must be within the scope of the 1990 act.
Some of the proposed standards, such as requiring outdoor access and, for pigs, allowing for rooting behavior, conflict with other tenants of organic production such as environmental stewardship.
Consumer confusion about the meaning of "organic" should not drive rulemaking; consumer education campaigns should address any confusion.
Animal welfare is important to all producers and is not exclusive to organic production.

New standards add complexity, create barriers to existing and new organic producers.
Current organic producers have designed their enterprises around existing organic standards. The new requirements may make it cost prohibitive to retrofit operations to come into compliance.
The proposed standards, many of which run counter to best management practices used to protect animal health and the environment, could be a barrier to new producers entering organic production.
The standards would increase the cost of organic livestock products without making them more "organic."
Rather than expanding organic livestock production, the standards could reduce the number of organic producers.
The proposed standards are rigid, inflexible and not science-based; they will not allow organic producers the flexibility to respond to new housing and handling systems that may be developed to enhance animal welfare.
The standards are based on public perception of what is good animal welfare and do not reflect a consensus by experts in animal welfare and handling.
Producers need flexibility to make animal welfare decisions based on the needs and challenges of their particular animals, facilities and customer preferences. A one-size-fits-all approach eliminates that flexibility.
Livestock industry and other animal welfare programs are available and suitable for use by organic producers.
The pork industry's PQA Plus program is a good example of an industry program that provides a significant and outcome-based measure of animal welfare.
The World Animal Health Organization (OIE), of which the United States is a member, sets international animal welfare standards and has not yet issued a chapter on pigs.
It is premature to put any welfare practices into the Code of Federal Regulations since they may conflict with the international standards now under development.
Requirements on outdoor access, bedding and rooting behavior are in conflict with best management practices used to prevent swine diseases that pose a threat to animal and human health.
The United States made a significant and costly effort to eliminate pseudorabies from the commercial swine herd. Keeping pigs outdoors facilitates exposure to feral pigs, which are known to harbor the pseudorabies virus. There would be significant international trade ramifications if pseudorabies were reintroduced to farmed pigs.
Outdoor production is the major route of introduction for the trichinae parasite. Increased cases of trichinae in organic pork would lead to consumer trust problems for all pork products and to potential distrust of U.S. pork from America's trading partners.
Restrictions on tail docking and teeth clipping would not allow producers the freedom to make needed husbandry decisions, which are implemented to protect animal welfare.
Animal welfare is complex and needs to be science-based.
Standards should be outcome-based; there is not yet an international standard for pigs.
Proposed standards for pigs present challenges to animal and public health.
NPPC is urging pork producers and others to submit by the July 13 deadline comments in opposition to the proposed organic animal welfare standards, which not only don't add to what makes a product "organic" but could be broadened to encompass conventional livestock production. (Starting Monday, June 27,click here to submit comments.)