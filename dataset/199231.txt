Beef producers take pride in their cattle. When replacing toxic-fescue pastures, they can take pride in grass, says an extension forage specialist.Novel-endophyte fescues make cattle perform and look better, says Craig Roberts of the University of Missouri-Columbia.
Bad things happen when cattle graze toxic fescue. Shaggy hair looks bad, Roberts says. "Fescue cattle" retain their winter hair into summer. With new novel-endophyte fescues, cattle slick up in spring. That cuts heat stress.
There's more than better hair, Roberts adds. Cattle gain more pounds per grazing day.
Ergovaline, a toxin found in Kentucky 31 fescue, causes many other faults. The toxin lowers reproduction and milking ability.
In winter, the toxin restricts blood flow to extremities. That causes frozen ears and feet. Frozen ears look bad while fescue foot can cause death.
A series of one-day schools, starting March 28 in Welch, Okla., will continue on into Missouri that week.
The schools, in their fourth year, explain toxic dangers of Kentucky 31 fescue. K-31 fescue is hardy and productive, making it the most used grass in Missouri and many states in the fescue belt.
Most important, the schools tell how to kill old stands of fescue and replace them with healthy versions, Roberts says.
The schools held by the Alliance for Grassland Renewal are taught by MU Extension, USDA Natural Resources Conservation Service, A seed company and farmer representatives.
K-31 fescue was widely seeded before the toxic effects were noticed. An endophyte fungus growing between cell walls makes the toxin.
Now, seed companies offer several novel-endophyte fescue varieties. Plant breeders found natural occurring nontoxic endophytes. Those were bred into the new seeds.
Pasture renovation brings economic returns for beef producers. Calves gain faster and are healthier.
"Payback is vital," Roberts said. "But pride in cattle and grass also count in the pleasure of beef farming."
For years, producers accepted the downside of toxic fescue. They worked around it, Roberts says.
"Novel endophytes are like a cure for a disease," he says. "No longer will we treat symptoms. We have the cure.
"Once producers see the difference, they want to renovate. After four years, many have made the conversion. They benefit from higher production. It's nice to see the differences in calves at the sale barn. That's when pride kicks in."
Long ago, feeder-calf order buyers learned to identify fescue calves. Those scruffy calves take longer to start gaining when put on feed. They take a while to recover. "When slick-coated calves come into the ring, buyers bid more. Toxin-free calves bring premiums prices," he says.
The school at Welch is the first renovation school held outside of Missouri. "Northeast Oklahoma is a hotbed of toxic fescue." Says Roberts says, who grew up in the region.
Producers in the four corners of Oklahoma, Kansas, Arkansas and Missouri are urged to enroll. Early signup required for limited space. Go tograsslandrenewal.orgfor details.
School, dates, places, contacts:
March 28, Welch, Okla., Cherokee Red Barn. Shirley Hudson, 918-542-4576 orottawaccd@conservation.ok.gov.
March 29, Mount Vernon, Mo., MU Southwest Research Center. Eldon Cole, 417-466-3102 orColeE@missouri.edu.
March 30, Columbia, MU Beef Farm. Lena Johnson, 573-882-7327 orJohnsonL@missouri.edu.
March 31, Linneus, MU Forage Systems Research Center,A Racheal Foster-Neal, 660-895-5121 orFosterNealR@missouri.edu.
Producers at the second school will see research of all commercially available varieties in MU grazing plots.
Roberts said in recapping advantages: "It's more than profits and pride. It's peace of mind. No one should worry about the best bull or cow getting fescue foot. That could be the end of them."