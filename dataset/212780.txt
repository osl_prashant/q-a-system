(UPDATED, May 5) U.S. citrus exports to Europe are expected to increase following a change in citrus canker safeguard rules. 
U.S. Secretary of Agriculture Sonny Perdue and Acting U.S. Trade Representative Stephen Vaughn on May 3 said the European Union has dropped requirement that U.S. groves be surveyed for citrus canker. That, according to a news release, will make it easier to ship U.S. citrus to the EU and save growers production costs.
 
The new EU directive requires countries where citrus canker has been detected to have a disease management program and to ensure that exported fruit have no symptoms, according to the release. Because grove surveys for citrus canker will no longer be required, the USDA said that U.S. producers can save an estimated $5.6 million dollars per year, according to the release.
 
“We just think it is an elimination of duplication,” said Andrew Meadows, director of communications for Lakeland-based Florida Citrus Mutual.
 
Meadows said the protocols in place in the packinghouse are sufficient to protect the market place.
 
“It’s a great decision and we applaud the secretary and the trade representative for making it and working on it for us,” Meadows said. 
 
Dan Kass, senior director of export sales for Wonderful Citrus, said the company welcomes the announcement and the easing of restrictions of exporting Florida grapefruit to Europe. "This decision will give growers more options to export their grapefruit to Europe, which is the leading international destination for grapefruit," he said in an e-mail.
 
Citrus canker is a disease that has mostly affected the Florida citrus groves, though there have been canker findings in Louisiana and Texas citrus trees.
 
“The EU maintains a number of unwarranted sanitary and phytosanitary (SPS) barriers on U.S. agricultural exports, and we have long called on the EU to base its SPS measures on science,” Vaughn said in the release. “Today’s action removes a longstanding and unfair barrier and will help return U.S. citrus exports to the EU to the levels we had a decade ago.”
 
Florida producers grow 25,000 acres of grapefruit. In 2016, the Florida Department of Citrus reported fresh grapefruit exports to Europe totaled 108 million pounds, or about 41% of total exports that year of 264 million pounds.
 
Industry estimates that citrus exports are expected to increase by 25%, or about $15 million, during the first year of the revised export rules, according to the release.
 
The release said implementation of the new EU policy is projected by mid-November, in time for Florida’s grapefruit export season.