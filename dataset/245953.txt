AP-IL--Illinois News Digest 1:30 pm, IL
AP-IL--Illinois News Digest 1:30 pm, IL

The Associated Press



Here's a look at how AP's general news coverage is shaping up in Illinois at 1:30 p.m. Questions about coverage plans are welcome and should be directed to the AP-Chicago bureau at 312-781-0500 or chifax@ap.org. Caryn Rousseau is on the desk, followed by Herbert McCann. A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
TOP STORIES:
ILLINOIS-LEGIONNAIRES' DISEASE
SPRINGFIELD, Ill. — Gov. Bruce Rauner's administration now plans to rebuild dormitories at the Quincy veterans' home where Legionnaires' disease continues to be a problem. Illinois Department of Veterans' Affairs Director Erica Jeffries told a joint hearing of the House and Senate Veterans' Affairs Committees Monday that residence halls where Legionnaires' has caused the most problems will be razed and rebuilt within three to five years. By Political Writer John O'Connor. SENT: 130 words. UPCOMING: 450 words, photos.
With: ILLINOIS-LEGIONNAIRES' DISEASE-THE LATEST: The Latest on a legislative hearing on the Legionnaires' disease outbreak at the Quincy veterans' home. Will be updated as developments occur.
VETERANS HOME-ECONOMIC IMPACT
QUINCY, Ill. — A study shows that the economic impact of the Illinois Veterans Home extends far beyond the city of Quincy and well above the facility's $54 million annual budget. The study found that the facility's "total economic output" is $90.7 million a year in Illinois. The Quincy Herald-Whig reports that a steering committee assembled in January by Quincy Mayor Kyle Moore sought the study. The committee has focused on upgrading the Veterans Home after Legionnaires' disease outbreaks, which began in 2015. SENT: 130 words. UPCOMING: 250 words.
OPIOID LAWSUIT-NEWBORN
BELLEVILLE, Ill. — An Illinois family of a baby born addicted to opioids is suing 20 pharmaceutical companies alleging they're responsible for the mother's addition to opioids and heroin. The Belleville News-Democrat reports that a baby, identified by his initials T.W.B., was born in March 2017 and diagnosed with Neonatal Abstinence Syndrome, which can cause breathing and feeding problems. The baby's grandparents, Deric and Ceonda Rees, filed the lawsuit in the U.S. District Court of Southern Illinois Wednesday. SENT: 130 words. UPCOMING: 250 words.
LEAVING YEMEN BEHIND
DEARBORN, Mich. — People from Yemen have recently begun to see long-term futures in the U.S. and are making their culture part of their businesses. One such business is a cafe in the Detroit suburb of Dearborn that shares Yemen's centuries-old coffee tradition and uses coffee from a family farm in Yemen. The deeper roots that Yemeni are putting down is partly a result of the normal socio-economic evolution among first- and second-generation immigrants. But it's also due to war back home. By Jeff Karoub. SENT: 810 words, photo, video. Illinois editors note Chicago interest.
INDUSTRIAL FARMING IMPACT
WILLARD, N.C. — The Tar Heel State is home to 10.2 million humans and 9.3 million hogs — which produce three times the waste of their two-legged counterparts. All that waste is stored in open-air lagoons and sprayed onto fields as fertilizer. Rural residents say some of that waste gets onto and into their homes. Civil trials begin next month against a subsidiary of the world's largest pork producer, and people are watching to see whether things will change in the country's No. 2 hog state. By Allen G. Breed and Michael Biesecker. SENT: 2,010 words, photos, video.
MEDICAL:
MED--OPIOIDS HOSPITALIZATIONS-KIDS
CHICAGO — Opioid poisonings and overdoses are sending rising numbers of U.S. kids to the hospital according to a study also showing a surge in potentially life-threatening reactions. Prescription painkillers were often involved, but heroin and other opioids also were used. Hospitalizations were most common among kids aged 12-17 and those aged 1 to 5. Some youngsters were sickened after using parents' drugs out of curiosity. The study was published Monday in Pediatrics. By Lindsey Tanner. SENT: 440 words, photo.
IN BRIEF:
—CONSUMER COMPLAINTS-ILLINOIS: Identity theft was the top complaint consumers made to the Illinois attorney general's office last year.
—ILLINOIS GRAD STUDENT STRIKE: The University of Illinois graduate students' strike in Champaign-Urbana is set to enter a second week after bargaining between the union and school administrators failed.
—DUBUQUE NIGHTCLUB SHOOTING: Federal prosecutors have filed a weapons charge against a Chicago man, so Iowa has dropped charges filed over the same shooting incident in Dubuque.
—ILLINOIS BICENTENNIAL STAMP: The U.S. Postal Service is set to issue a stamp honoring Illinois' 200th anniversary of statehood.
—AURORA POLICE-LAWSUIT: A federal judge has given the green light to a lawsuit by seven police officers against the Chicago suburb of Aurora after an official sent their addresses, phone numbers and other personnel records to a purported street-gang leader they helped imprison to serve an 88-year sentence.
—OBAMA FOUNDATION: The Obama Foundation and the University of Chicago have teamed up to offer a master's degree program for the next generation of community leaders.
SPORTS:
BBN--CUBS-WILSON
MESA, Ariz. — For all the changes in Chicago's bullpen this spring, one of the biggest keys to its success might be Justin Wilson returning to form after he struggled following last summer's trade with Detroit. By Jay Cohen. UPCOMING: 500 words, photo.
BKN--CELTICS-BULLS
CHICAGO — Kyrie Irving and the Boston Celtics go for their fifth win in six games when they meet the Chicago Bulls on Monday. The Celtics are coming off a loss to Houston. By Andrew Seligman. UPCOMING: 600 words, photos. Game starts at 7 p.m. CT.
___
If you have stories of regional or statewide interest, please email them to chifax@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.