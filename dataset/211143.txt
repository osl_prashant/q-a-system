A new report from the World Organization for Animal Health (OIE) provides an overview of how countries around the world use antimicrobial agents in food-animal production.The report, titled “OIE Annual report on the use of antimicrobial agents in animals: Better understanding of the global situation,” is available online.
Key findings in the report include:
·         A total of 96 of 130 (74%) OIE Member Countries indicated that they do not authorize antimicrobial agents for growth promotion in animals.
·         Twenty-five Member Countries provided a list of antimicrobial agents authorised for growth promotion, in which Tylosin and Bacitracin were most frequently quoted.
·         Colistin was mentioned by 10 of 25 Member Countries.
·         A total of 89 of 130 OIE Member Countries (68%) submitted to the OIE their quantities on the use of antimicrobial agents in animals for years ranging from 2010 to 2015.
·         Forty OIE Member Countries reported use of antimicrobial agents through Reporting Option 1, the less detailed option, while 19 Member Countries reported through Reporting Option 2, and 30 Member Countries reported through Reporting Option 3 (the most detailed reporting option).
·         Tetracyclines and Macrolides were the most commonly reported antimicrobial agents used; differences however, were observed between OIE Regions. Tetracyclines and macrolides accounted for more than 60 percent of reported antibiotic use in the Americas but only 22 percent in Asia.
·         The main route of administration in animals was the oral route.
The authors note that “further efforts and support will be needed to improve the data collection system and the quality of the data collected by countries. Detailed interpretation of the data also needs further development, in particular to define a denominator (animal biomass) that will allow better data interpretation in the future.”
Read the full report from OIE.