When will markets get the acreage mix right? Are we nearing conditions where global supply and demand rebalance?Find out the answers to these questions and more at Pro Farmer’s Leading Edge Conference, which will take place July 17-18 in Des Moines, Iowa. Speakers will analyze current market conditions and explore events you should keep your eye on over the next 12 months.
On the agenda:
Global Business Outlook by Vince Malanga, LaSalle Economics president
Policy, Elections and Impacts on Ag by Jim Wiesemeyer, Pro Farmer Washington consultant
Trends in Land Values by Mike Walsten, LandOwner Newsletter consultant and contributor
What's Ahead for Fuel and Fertilizer Prices? by Davis Michaelsen, Pro Farmer Inputs Monitor eEditor
Developing Weather Events by Michael Clark, BAMWX.com chief meteorologist
Market Outlook by Brian Grete, Pro Farmer editor
For more information or to register, call 1-888-698-0487 or go to www.ProFarmer.com/events/leading-edge