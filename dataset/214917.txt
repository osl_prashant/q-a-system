There were 147 bred heifers sold in the Nov. 17 Show-Me-Select replacement heifer sale at Kirksville Livestock LLC. Average price was $1,872.
A sale-topping lot of two went for $2,100 per head, consigned by David Clark, Edina. He also had second-high price of $2,025 per head on a lot of seven. Overall, Clark sold 19 head, averaging $1,931. That was top consignor.
Third-high individual lots at $2,000 per head were sold by Kevin and Beth Strange, Edina, for seven head, and Randy Baker, La Plata, five head. All top sellers were bred through fixed-time artificial insemination.
Bennett Angus Farm, Browning, was second-high consignor, with an average of $1,918 for registered Angus heifers.
Randy Miller of Galaxy Beef consigned 32 head for an average $1,878 for Angus and Red Angus. He was third-high consignor.
Overall, 92 of 147 head were AI bred, which allows owners to use top proven sires in a breed.
“The sale was a success compared to recent bred heifer auction prices,” says Zac Erwin, University of Missouri Extension livestock specialist, Kirksville. “The north-central sale was significantly stronger. There was a good crowd and demand was good.”
All consignors are in the MU educational program to produce Show-Me-Select bred heifers.
This was the third Show-Me-Select sale at Kirksville Livestock LLC. Some producers haven't had time to build reputations. Not all bidders have learned the value of calving ease and short calving seasons that come from AI breeding.
“Repeat buyers improve prices,” says David Patterson, MU Extension beef specialist, Columbia. “Returning buyers know what they are looking for.”
Erwin added, “I’m starting to see repeat buyers. Any who try Show-Me-Select heifers and have good luck come back and bid more.”
Vannoy Farms, Shelbyville, bought the high-average price lot from David Clark.
The top buyer, taking home 24 head, was Larry Remick, Hillsboro, Iowa. He bought mainly from Kevin Strange. He also bought six head from Bennett Farms.
The north-central Missouri sale average of $1,872 was near the $1,867 average at the southwestern Missouri sale the same night.
Coming SMS sales: Kingsville, Nov. 25; Fruitland, Dec. 2; Farmington, Dec. 8; and Palmyra, Dec. 9.
Lower death loss at calving adds value to replacements. Erwin says national average for assisted calving for first-calf heifers is 20 percent. SMS buyers in the past found only 8 percent needed an assist.
Prebreeding exams boost value of Show-Me-Select heifers. Veterinarians find heifers with pelvic openings too small to pass a calf at birth. Also, exams increase calving rates.
Calving ease drew first attention to the Show-Me-Select heifer program. Now, the MU Extension program adds more value through quality beef.
“Increased ultrasound pregnancy exams improve predicted calving dates,” Patterson says. Buyers like knowing when to check their herds, which shortens calving seasons.
“Also, ultrasound fetal sexing can tell if the unborn calf is bull or heifer.”
Veterinarians promote the program, as it cuts calls for cesarian section births. Often those don’t end well for heifer, calf, farmer or vet.
Herd owners wanting to improve herd genetics and heifer development can enroll in Show-Me-Select. Contact the MU Extension center in any county. Erwin can be reached at 660-655-9866. For more information, visit agebb.missouri.edu/select.
Now Erwin returns to final details for the Missouri Livestock Symposium, Dec. 1-2, Kirksville.