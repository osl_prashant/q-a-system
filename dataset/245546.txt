Cheap grain has made feeding livestock a lot easier, contributing to an expanding sector.

The latest USDA Cattle On Feed Report shows there are 11.6 million head of cattle as of Feb. 1, an 8 percent increase from Feb. 1, 2017. Weights are continuing to increase as well, yielding more meat.

Paul Georgy, chairman of the board at Allendale, Inc., says this is concerning, and tough times might come quicker than expected.

“Everybody’s anticipating this June being a very tough time—we might see it a little earlier because cattle prices have a tendency of getting it done earlier,” he said on AgDay.

Hear why he says China and Japan need to step up their pace on AgDay above.