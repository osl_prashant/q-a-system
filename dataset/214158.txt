Salinas-based D’Arrigo Bros. of California is launching fall-themed recipes, culinary videos and photography to promote its specialty produce products marketed under the Andy Boy label.
The company, along with Taylor Stinson — author of popular blog The Girl on Bloor — created spooky broccoli rabe breakfast pastries to celebrate Halloween, according to a news release.
Consumers looking for Thanksgiving meal ideas can check out the company’s website to find a recipe for roasted sweet potato and broccoli rabe salad, the release said.
The company has also rolled out recipes for broccoli rabe layered root tarts, pumpkin and fennel soup with chili oil and beet, fennel and sweet potato crostini.
All of the recipes and videos can be found at www.andyboy.com.