Village fights railroad for taxes that could save its future
Village fights railroad for taxes that could save its future

By JUSTIN VICORYThe Clarion-Ledger
The Associated Press

GLENDORA, Miss.




GLENDORA, Miss. (AP) — Glendora Mayor Johnny B. Thomas walks on the bridge where two men tossed Emmett Till's lifeless body into the water below 62 years ago. This desolate spot, he says, is ground zero for the civil rights movement.
"This is where they dumped the body, here," Thomas says, as he points into Black Bayou, an appropriate name for a tributary of the Tallahatchie River that lacks any banks and where mighty oak trees float.
But as the country inched forward toward a goal of racial and economic justice, Glendora fell further and further behind, becoming one of the poorest communities in the country.
But an accounting error discovered about two years ago could mean close to $1 million for this struggling area — 10 times Glendora's annual budget.
Almost four days after Till was killed on Aug. 28, 1955, authorities recovered his body. Till's mother, Mamie Till Mobley, purposely left the casket open at his funeral. Pictures of Till's mutilated and bloated body galvanized the civil rights movement across the country. And while Till has never been forgotten, Glendora itself has become largely a footnote to history.
Located amid a patchwork of cotton, corn and soybean fields in the heart of the Mississippi Delta, Glendora developed as a small sawmill site and began to grow in 1883 when the railroad was built through the territory and a station was located there. The population has waxed and waned since, as has its fortunes.
Today, even in one of the most impoverished areas in the country, the village stands out for being extremely poor.
As of the last census, about 68 percent of families and 63 percent of the population were below the poverty level, including 83 percent of those under the age of 18.
The per capita income of Glendora residents was $7,044 in 2000 and $6,484 in 2016. The median household income was $14,375 and the median family income was $11,875, according to the U.S. Census.
Glendora's annual budget is about $100,000, Thomas says.
Utility tax statement
Canadian National Railway snakes through cities, towns and villages in Mississippi as it makes its way southward before splitting into two lines just south of Jackson.
Along the way, the counties the railway travels through calculate a utility tax on the railroad to go to cities and towns within its borders. Those statements go to the railroad every year for payment.
Glendora hasn't received a utility tax statement from Tallahatchie County as far back as officials there can find records — at least since 1973 — more than four decades ago.
Tax Assessor Dorothy Wright said she discovered the utility accounting error in 2015 after overhearing a conversation involving the former tax assessor.
allas-based CPA Russell Davis computes that with interest the railroad owes the village $990,551.97, or more than nine times Glendora's annual budget. Davis became interested in the issue after meeting Thomas on one of the accountant's trips to Glendora to visit blues and civil rights attractions throughout the Mississippi Delta.
"Mayor Thomas is literally the reason this village has any hope. He has kept it together. He's the one with his finger in the damn, and he's not backing away from the railroad — the opposite, in fact," Davis said.
Canadian National has expressed its disagreement with what is legally owed and has requested the state Department of Revenue weigh in.
Spokesman Patrick Waldron said the company has submitted a total payment of about $60,000 for the last four years.
But Thomas said if Glendora doesn't get what it believes it's entitled to by April, he's prepared to file a lien on the railway's property, and take steps to sell it.
Some Glendora residents are already deeply skeptical of the railway.
Derailments, deaths affect perspective
The train tracks arch close to 45 degrees just before turning into the middle of town, and trains often travel at a high rate of speed through the village. There have been at least five deaths from trains striking pedestrians, Thomas said, including one of his relatives.
Former volunteer fire department chief Tracey Rosebud said he helped develop a citizen's emergency response team specifically because of the danger the railway poses.
"If you spend a day there, there'll be about 23 to 24 trains coming through and with that amount of speed, you can imagine how dangerous it is, especially for children. At any given time, it could cause horrific damage to people on either side of the tracks and make it hard for rescue squads," he said.
In September 1969, an Illinois Central freight train engineer applied the brakes in "full emergency" in an attempt to avoid striking a pedestrian. The 149-car train buckled, forcing 15 cars off the rails, including eight tank cars loaded with vinyl chloride.
The coupler of one of the cars punctured one of the tank cars, spewing its contents on the ground and erupting into fire that spread to other tanks. The following morning a tank car exploded violently, seriously damaging four houses, several buildings, automobiles, and equipment, according to an NTSB report.
In 2005, a train derailed as it attempted to avoid slamming into a diesel truck stuck on the rails, instantly rupturing the tank and spreading a 30,000-ton mist of diesel fuel into the air, the mayor recalls.
The exodus
The rails, more specifically the train depot, stands as a symbol of Glendora's rocky racial history.
Thomas points to the depot as an early primer on white flight from Glendora.
For years, the depot served the white powerful men in town who would hold members-only meetings there. After Thomas was elected mayor in 1982, the men changed the locks to the public building. One night Thomas broke in and changed the locks himself. Not long after, the depot mysteriously caught fire and burned to the ground.
The village's white residents, who had the capital investment and owned all the property, began moving away. The exodus continues today.
Between 2000 and 2010, almost half the village's residents left, the population dropping from 285 to 151, a 47 percent decrease.
Abandoned commercial buildings and vacant lots remain. On Main Street, stray dogs wander looking for scraps of food. The mayor addresses them by name.
Thomas feels around with his foot to find the structural foundations of once-popular Delta destinations, including "King's Place," a famous juke joint where "King of the Harmonica" Sonny Boy Williamson frequently performed.
An entwined, brutal past
Thomas' own history is deeply entwined in the Emmett Till story. His father, Henry Lee Loggins, worked under J.W. Milam, who admitted after his acquittal that he killed Till. Milam was a plantation manager who serviced and maintained farm equipment.
Loggins and Leroy Collins were allegedly the two black men with Milam when Till was kidnapped and killed and his body tossed into the bayou.
Thomas said his father never spoke of what happened. Yet he suspects, based on the reality of the times, that his father probably did play a role — because he had little choice.
"I believe that my father was involved in the Emmett Till murder, was there with Milam and Bryant, and knew what was going on. However, out of fear for his life, he did not admit to taking part in the crime or witnessing the crime," Thomas said in "A Stone of Hope,"  a book he co-authored.
Most residents had what Thomas describes as a "plantation mentality" in Glendora — even into the '50s and '60s, often bowing to the will of white business owners such as Milam.
"Milam was a vicious, ruthless, roughneck who did the dirty work in keeping black workers in their place for the plantation owners in Tallahatchie County. (He) was feared by black people and even some white people, and kept black people under control by beating them and threatening to kill them if 'they had to be killed'...Milam beat my mother in front of her children because she refused to go to the fields to work because she was sick," Thomas says in the book.
The mayor alleges former Tallahatchie County Sheriff Clarence Strider rounded Loggins and Collins up and locked them up in Charleston on trumped-up charges and under aliases, so they couldn't testify at the Till murder trial.
Diamond in the rough
On the eastern edge of Glendora, past Main Street and the former home of Milam, lies the Emmett Till museum, called the Emmett Till Historic Intrepid Center, or ETHIC.
The museum is set in what was once the Glendora cotton gin, where Roy Bryant and Milam allegedly took a 70-pound fan and attached it with barbed wire to Till's body before dumping it off the Black Bayou Bridge about a mile south.
Inside, tourists can get a feel through visual recreations of what it was like to live in Glendora before, during and after the Till slaying. Glendora's history doesn't rest on Till alone, but the museum provides firsthand insight into one of America's most painful chapters of history.
Yet the museum is also marked by what's not there. It needs upkeep. There's little in the way of signage to let people know it even exists. The roads leading up to it need to be fixed or repaved. In short, it'll take a large investment to put it on the map.
Thomas is hopeful.
He knows there are many like Davis, who travel to the Delta for the blues and to immerse themselves in the history of the region.
If they travel to Clarksdale and Indianola, why not Glendora?
"It might not have much, but it has a heart," says Davis.