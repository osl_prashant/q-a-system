Banana volumes have been light in recent months thanks to inclement weather and other adverse conditions in numerous producing countries.
Importers expected the supply to improve in coming weeks, but the first quarter of 2018 has presented serious challenges.
Guatemala, Costa Rica, Ecuador and Honduras accounted for about 85% of bananas shipped to the U.S. in 2017, and importers have reported issues affecting production in three of those four countries and in others as well.
 
Weather hiccups
“Unfortunately, external conditions beyond our control have impacted our supply chain during the first months of 2018 and have impacted our vessel schedules, port operations and fruit availability,” said Jamie Postal, director of sales for North America for Chiquita, which has its U.S. headquarters in Fort Lauderdale, Fla.
“Temperatures have been as far as 10 (degrees) below normal for several weeks in Guatemala, Honduras and Mexico, slowing fruit growth, production and yield.
“Excess rainfall and flooding in Costa Rica and Panama have damaged plantations, infrastructure, roads and bridges while high winds and waves have resulted in shipping delays,” Postal said March 8.
“These factors, combined with national instability and supply interruptions in Honduras, come at a time when supply is already seasonally tight.”
There have been reports that Chiquita workers in Honduras recently engaged in a lengthy strike, but operations have since returned to normal, according to the company.
Westlake Village, Calif.-based Dole also noted inclement weather, including strong rains and low temperatures.
“These cold fronts have also affected port operations in Honduras and more so in Costa Rica, which has caused shipping delays of up to 4-5 days,” Bil Goldfield, director of corporation communications for Dole, said March 12.
“Installation of new port shore cranes have further slowed operations.
“In addition to the weather issues and port delays, local trucking and access to banana farms has also been affected by political issues in Colombia and especially Honduras,” Goldfield said.
Along with making transportation tricky, the weather has affected the quality of the fruit.
“Calibration and finger length has been low,” Goldfield said.
“We have also experienced more dehydrations and some issues related to weak fruit.”
Dennis Christou, vice president of marketing for Coral Gables, Gla.-based Fresh Del Monte Produce, said March 8 that volumes have been light but that the company expects them to gradually return to normal toward the end of March.
“Although limited in volume, the quality of bananas packed were within industry specifications,” Christou said.
“The impact on banana suppliers is mostly related to lower yields than in the past few years, again caused by cool weather.”
Jessica Jones-Hughes, vice president of Oke USA, the importing arm of fair trade-focused Equal Exchange, said the company has not had the same issues with supply. It sources organic bananas from Peru and Ecuador, which are south of where some of the cold fronts have hovered.
“We’ve really been able to maintain volumes and actually fill in for customers who are not able to get product from some of the big guys,” Jones-Hughes said.
“It’s been really interesting to see that happening in a way that I have not seen before.”
Quality in Peru is expected to be better this year than in 2017, when excessive rain led to problems with crown rot, Jones-Hughes said.
 
Steady demand
While production in many countries has been a challenge in recent months, demand has been steady. The U.S. imported roughly 10.6 billion pounds of bananas in 2017, up about 4.7% from the previous year.
“Despite supply issues, demand for bananas remains strong,” Goldfield said.
“Year after year, bananas continue to be one of the most popular items in the grocery store. Retailer statistics prove they are one of the most frequently purchased items in supermarkets, and are one of the few items to be purchased equally by both men and women, and are a favorite with the entire family.”
Importers also said organic and specialty bananas are growth areas for the category.