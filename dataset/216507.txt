As the end of the year approaches, producers across the country are scrambling to find all their receipts as they prepare for a year-end meeting with their accountants. If you’re like most, you might be seeking a better way to stay organized in the New Year.
Here are four ideas that have worked for other producers:
1. Leah Johnson James, Wisconsin
“I put receipts in file folders by month. I use Quicken Essentials to track expenses and print checks, and then put all receipts in the folders. Makes it pretty painless for filing, and if receipts are needed, I can reference them in Quicken and know which folder to pull them from.”
2. Carol Steger Young, Washington 
“My filing categories are: accounting (tax docs) and legal, banking, breeding, Darigold, DHIA, grain/hay, fuel, Holstein USA, Jersey USA, labor, Employment Security Dept., misc., repairs, sale barn, supplies, utilities, visa, etc. Put all receipts in those folders, and then at the end of the year just grab the files the accountant needs and hand them over.”
3. Holly Stankowski, Wisconsin 
“I tried filing by category, but I have a 12-month folder, so I put a couple individual folders in the back for things I want separate (income stubs, bank statements, etc.).”
4. Lorilee Schultz, Illinois 
“I use 12 cheap pocket folders. One for each month. Income and bank statements on the left, expenses on the right, including all receipts for the month in an envelope.”
 
Note: This story appears in the December 2017 magazine issue of Dairy Herd Management.