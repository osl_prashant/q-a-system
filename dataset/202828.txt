The Vidalia Onion Committee named Aries Haygood of M&T Farms grower of the year and Ronald Gitaitis was inducted into the hall of fame.
The committee's annual awards banquet was Feb. 4 in Vidalia, Ga.
Haygood, operations manager for M&T, Lyons, Ga., has been vice chairman and chairman of the committee, according to a news release.
 M&T Farms averages 400 acres of Vidalia onions each year, and has been in business for almost 30 years, according to the release. Haygood manages the farm for his father-in-law Terry Collins, who was named grower of the year in 2002. Haygood has also been involved with the Georgia Fruit and Vegetable Association.

Gitaitis, a plant pathologist at the University of Georgia's College of Agricultural and Environmental Sciences, studied bacterial diseases and discovered three species of bacterium of onion.
The committee chooses the grower of the year based on overall achievement, production quality and compliance. To be considered, farms must be registered Vidalia onion growers, follow marketing order standards and support brand recognition, according to a news release.