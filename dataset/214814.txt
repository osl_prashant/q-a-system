“Huge, huge, huge” is how Kevin Bartolotta describes the role locally grown fruits and vegetables play in the Connecticut marketplace.
“Between farmers markets and roadside stands, every year, it’s more and more,” said Bartolotta, minority owner and president of Bartolotta Inc. in Torrington, Conn.
In 2017, the market was particularly brisk, with plenty of supply to go around, said Ken Yandow, president of Hartford-based FreshPoint Connecticut LLC, a subsidiary of Houston-based FreshPoint Inc.
“We had a really robust summer and fall crop,” he said, and New England has good supplies on all varieties of local apples this year.
That was an improvement over the last couple years, when weather issues caused some losses in fruit, he said.
“This year, we had a really good run with stone fruit — local peaches, nectarines we had good supplies.”
Al Parziale, president of Hartford-based Tina Rose Produce LLC, concurred.
“Could have used more rain, but it was good,” he said.
 
Customer demand
There are plenty of ready customers across Connecticut, Yandow said.
“You’ve got companies like ourselves and other wholesalers that are very aware of the consumer desire to eat local, to buy local and the chain stores in the area have been very supportive and done a very nice job with local programs,” he said.
The buy-local trend remains strong, both in direct-to-consumer and wholesale markets, said Linda Piotrowicz, bureau director for agriculture development/resource preservation/regional market with the Connecticut Department of Agriculture.
“Farmers are scaling up to meet stronger wholesale demand and local products continue to gain popularity in traditional supply channels. Farms are adding, expanding, and renovating facilities to meet changing food safety requirements.”
Organics continue to attract attention across Connecticut, as well, Yandow said.
“Companies like ourselves and others are taking initiative to stock more organics and make sure that segment of the consumers that want to eat organic have the opportunity to buy that product at many different venues,” he said.
Price does remain an issue in organics, though, said Bill Driscoll Sr., president of Heart of the Harvest, a Hartford-based foodservice-oriented fresh-cut processor.
“Everybody’s interested, but when they get the price, it kind of chills them out,” Driscoll said.
 
Renovations on hold
Meanwhile, plans to renovate the state-run Connecticut Regional Market in Hartford have been put on hold.
“The state’s recent budget situation has not been conducive to redevelopment/renovation of the Regional Market,” Piotrowicz said.
“I think this has been a very difficult time for state of Connecticut and the city of Hartford,” Yandow said. “There’s been some financial woes and it has contributed to the lack of addressing the situation like the Connecticut Regional Market.”