Not a day goes by that some media outlet isn’t gushing over the entrepreneurs who commercialize alt-meat, factory food initiatives. But let’s consider its realistic role going forward.There has been plenty of agitation—here and in many other journalistic corners—over the rise of alt-meat, the creation of test-tube tissues that capture the organoleptic characteristics of meat or poultry, and are then marketed as alternatives and animal foods.
As is true with other related developments in biotechnology, a lot of investors are eyeing this emerging sector as a potential “next-generation” business category that will someday provide the same astronomical ROIs as the early-entry investors who bankrolled the first-generation computer operating software, smartphone technology and social media sites.
Good for them. That’s what capitalism’s all about.
When test-tube food production is touted as an eventual replacement for conventional agriculture, however, that’s a leap forward that’s far from being as definitively established as possible, not to mention arguably questionable as a strategy to deal with the energy issues, resource limitations and environmental impacts associated with global food production.
It’s not just venture capitalists who tend to fall in love with the advancements of science and technology. There is widespread belief among all the rest of us that some wonderful techno-breakthrough will solve all our problems.
Maybe not right now, but eventually.
 
The Limits of Technology
For example: We believe fervently that medicine is well on its way to eventually ending all human disease and suffering, that incredible developments in tissue engineering will create replacement organs for patients who suffer kidney or liver failure and cannot obtain organ donations. That new drugs will one day conquer cancer, and that biotech will provide options for reversing the damage done by genetically linked pathologies.
While exciting and revolutionary developments are occurring on all those medical frontiers, can anyone plausibly argue that we can affordably make them available to every single human being on earth who needs access to such treatments?
Truthfully, virtually every high-tech breakthrough only further divides the world’s haves and have-nots, so proclaiming that factory foods — like alt-meat — that will be created in giant tanks from specialized chemical substrates will somehow address (alleged) animal abuse has some traction for the affluent citizens of developed nations in the West. However, the technologies, no matter how exciting, cannot be positioned as “the” solution to global food security.
Don’t misunderstand: there is a role — a critical role — for what food industry visionary Henk Hoogenkamp has labeled “cellular agriculture.” Using cutting edge biotechnology to develop cultured protein foods that mimic the sensory qualities and nutritional profile of animal foods is a vital part of what needs to be a multi-pronged effort to advance the sustainable food production when the planet hits nine billion human beings by mid-century.
But cellular agriculture is only one aspect of the problem-solving that needs to take place, not the focal point of how we deal with food production and food security.
Here’s an analogy that might explain what I mean.
Take transportation, broadly defined, as a contemporary challenge affecting commerce and impacting lifestyles in a hugely significant way. What’s to be done about traffic congestion, energy availability and the associated problems of noise and air pollution? Nobody, with any plausibility, suggests that there’s some silver bullet that will “fix” those problems. However:
 
Should the automotive industry continue to advance fuel efficient vehicles? Absolutely.
Should governmental entities promote strategies such as telecommuting that lessen rush-hour traffic? Totally.
Should the public sector continue to invest in mass transit options? Of course.
Should policies and R&D funds support development of renewable fuels? A no-brainer.
 
All of those options are viable, each of them is additive in terms of a comprehensive solution, and collectively, they can contribute to significant progress in dealing with the challenges currently created by our existing transportation network.
Likewise, cellular agriculture can be an important element of a collective strategy to ensure food security and enhance sustainable production of the crops and ingredients needed to feed the growing world population.
It’s not a miracle that will end agriculture as we know it, and there’s a simple, intuitive reason why.
However test-tube foods are created, no matter which high-tech systems are invented, the substrates (ingredients) and the energy inputs needed to produce the millions of tons of edible products have to come from somewhere.
Does it really make sense that factory operations to synthesize faux foods would be more energy efficient, more economical that modern farming that uses soil nutrients, solar energy and “free” rainfall to grow food crops?
By the way? That was a rhetorical question. ?
 
The opinions in this commentary are those of Dan Murphy a veteran journalist and commentator