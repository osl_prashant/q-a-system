Soil moisture supplies in North Dakota remain low
Soil moisture supplies in North Dakota remain low

The Associated Press

FARGO, N.D.




FARGO, N.D. (AP) — Soil moisture supplies in North Dakota are low as spring planting season gets closer.
The Agriculture Department's monthly crop report shows both topsoil and subsoil moisture supplies rated at 53 percent short or very short.
Farmers in western and central North Dakota dealt with crippling drought last year. The most recent U.S. Drought Monitor map shows two-thirds of the state still in moderate to severe drought.
The Agriculture Department report shows that the state's winter wheat crop is still in relatively good shape as February ends, with only 18 percent rated in poor or very poor condition.
Cattle and calf conditions in the state are mostly good to excellent. Stockwater supplies are rated mostly adequate.