HILTON HEAD ISLAND, S.C. — The Southern Innovations Organics & Foodservice Expo debuted to positive reviews from attendees and organizers.
Numerous exhibitors reported good traffic Sept. 29 and said they liked the small format and laid-back vibe of the show.
 





 
“The show was fantastic,” said Stephen Cowan, account manager for Kingsville, Ontario-based Mucci Farms. “A very intimate atmosphere, a lot of good feedback from retailers coming by, giving a lot of face time. It’s an excellent show.”
More than 500 people registered for the event, including nearly 120 retail and foodservice buyers.
Several chef demos took place on the show floor, and SEPC president David Sherrod was breaking down the kitchen set after the expo when he paused to recap the event.
“We couldn’t have asked for anything any better,” Sherrod said. “We really thought that the turnout was really (good) this year, the retail-foodservice people really came through for us. Almost everyone that I talked to felt like they really had a great takeaway from the show.”
A keynote address and two educational sessions preceded the expo, which was at the Hilton Head Island Beach & Tennis Convention Center.
Television host and author Daphne Oz spoke during the keynote about changing consumer perspectives on nutritious food.
“More and more consumers are feeling like it’s a right to eat healthy” and that doing so is an indulgence rather than a deprivation, Oz said.
The look of those good food choices is part of the draw, which plays into the stylized meal photos that populate Instagram.
Kristin Yerecic, marketing manager for New Kensington, Pa.-based Yerecic Label, also noted that the aesthetic of healthier eating has been central to its appeal.
“Millennials want something that’s social media worthy,” Yerecic said during a panel about how foodservice companies can prepare to please future consumers.
For another panel discussion, 210 Analytics principal Anne-Marie Roerink described the difference between core organic shoppers and periphery organic shoppers, defining the latter term as people who buy organic on occasion but are not devoted to doing so.
That group represents serious growth potential for organic, but periphery shoppers are much more sensitive to price than core organic shoppers.
Trimming the price difference between organic products and their conventional counterparts is key to growing the category, said Mark Carroll, vice president of merchandising for produce at The Fresh Market, during the panel.
In addition to the various sessions, Southern Innovations included retail tours and networking activities like golf and fishing.
“We’ve loved it,” said Dan Wohlford, national sales representative for Wenatchee, Wash.-based Oneonta Starr Ranch Growers. “It’s been fantastic. Yesterday, last night and today, the retailer-foodservice response and participation has been fantastic.
“I think it’s because of the popularity of and how well the SEPC does their expo in the spring, the foodservice and retailers were confident that they were going to get a quality event in this, too,” Wohlford said.