The NAFTA renegotiations are covering a lot of ground. 

Things such as manufacturing and agriculture come to mind when thinking about the renegotiations. 

Beer might not be one of the things one thinks about, but Mexico prefers U.S.-grown malt barley, a key ingredient in the growing demand for the increasing number of microbreweries in Mexico.

That trend in the U.S. is increasing in strength, and a small region in the eastern U.S. is trying to capture from the western U.S.

Watch the story on U.S. Farm Report above.