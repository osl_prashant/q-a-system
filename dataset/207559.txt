KANSAS CITY, Mo. — Featuring a long look at the millennial consumer the sixth annual Midwest Produce Expo also delivered plenty of jazz, barbecue and trade floor discussions. 
“I like the intimacy of the show,” said Owen Herndon, with Associated Wholesale Grocers, Kansas City, Kan., adding that the four-hour expo was easy to navigate.
 
One Midwest exhibitor said the Aug. 14-16 event served him well.
 
“We really like it,” said Dennis Mouzin, co-owner of Mouzin Brothers Farms, Vincennes, Ind. “It is a lot of personal one-on-one with retailers and other allied (companies)in the industry.”
 
Exhibitor Chad Hartman, director of marketing for Truly Good Foods, Charlotte, N.C., said he appreciated the sessions on millennials, particularly a talk by Garland Perkins of The Oppenheimer Group. 
 
“The traffic has been good as far as quality, and we have seen a dozen good contacts and that’s worth the price of admission when you can see some good guys,” Hartman said.
 
“I am proud of the way Kansas City embraced the Midwest Produce Expo for the second consecutive year,” said The Packer Publisher Shannon Shuman. “A show floor that offered diverse options of great national suppliers combined with local suppliers provided a tremendous value to the buyers at the show.”
 
On Aug. 15, morning workshops included a presentation from Pyramid Foods CEO and president Erick Taylor on the future of e-commerce for independent retailers, Perkins from Oppy and keynote speaker
Matt Beaudreau spoke about marketing to millennials. 
 
The Packer’s Greg Johnson and Produce Retailer’s Pamela Riemenschneider also looked at millennial trends from the perspective of Fresh Trends research from The Packer.