$1.5B settlement in suit over Syngenta modified corn seed
$1.5B settlement in suit over Syngenta modified corn seed

By MARGARET STAFFORDAssociated Press
The Associated Press

KANSAS CITY, Mo.




KANSAS CITY, Mo. (AP) — A $1.5 billion settlement was reached in a class-action lawsuit covering tens of thousands of farmers, grain-handling facilities and ethanol plants that sued Swiss agribusiness giant Syngenta over its introduction of a genetically engineered corn seed.
Lawsuits in state and federal courts challenged Syngenta's decision to introduce its modified Viptera and Duracade corn seed strains to the U.S. market for the 2011 growing season before having approval for import by China in 2014. The plaintiffs said Syngenta's decision cut off access to the large Chinese corn market and caused price drops for several years.
The settlement, reached Monday, must be approved by a federal judge in Kansas. It will create a fund to pay claims by farmers and others who contracted to price corn or corn byproducts after Sept. 15, 2013. If approved, money could be distributed to class members in the first half of 2019.
The settlement does not include the exporters Cargill and ADM that are also suing Syngenta.
Four lawyers who led the litigation for corn producers said in a joint statement Monday that the settlement is believed to be the largest agricultural litigation settlement in U.S. history.
"America's corn farmers and related businesses were hurt economically and this settlement will provide fair compensation for their damages," the attorneys said. "It is an equitable result for all involved."
The preliminary settlement did not "constitute an admission by either side concerning merits of the parties' allegations and defenses," Syngenta spokesman Paul Minehart said in a statement.
The agribusiness giant contended that corn prices dropped because of market forces, not China's rejection of Viptera. Most of the farmers suing Syngenta didn't grow Viptera or Duracade, but China rejected millions of tons of their grain because elevators and shippers mix grain from several suppliers, making it impossible to find corn free of the trait.
Syngenta invested more than $100 million and 15 years in developing Viptera, which has a trait called MIR162 that protects against pests such as earworms, cutworms, armyworms and corn borers. Duracade, a newer variety, added protection against corn rootworm.
The company continued to defend the traits Monday, saying Viptera and Duracade provided a way to combat several pests and noting that the strains were "fully approved by all U.S. regulatory authorities at the time of their launch."
In June of 2017, a federal grand jury in Kansas awarded nearly $218 million to about 7,300 growers who sued Syngenta over the corn modifications. Plaintiffs' experts in that trial estimated the economic damage was about $5 billion. Another trial was in progress in state court in Minnesota in September when a preliminary settlement was reached.