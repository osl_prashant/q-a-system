AP-IA--Iowa News Digest 6 pm, IA
AP-IA--Iowa News Digest 6 pm, IA

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Iowa. Questions about coverage plans are welcome, and should be directed to the Des Moines Bureau at 515-243-3281 or apdesmoines@ap.org. Iowa News Editor Scott McFetridge can also be reached at 515-243-3281 or smcfetridge@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
UPDATES STORY LENGTHS AND ADDS ITEMS
TOP STORIES:
GLENWOOD ABUSE INVESTIGATION — Five former state workers were sentenced to probation after their conviction of mistreating disabled people in their care at an Iowa health care facility. SENT: 350 words.
ILLINOIS-ABORTIONS
CHICAGO — More women appear to be traveling to Illinois from out of state to have an abortion, according to Illinois Department of Public Health figures, and activists say it could be because surrounding states have tighter restrictions. SENT: 300 words. NOTE Iowa mention.
IN BRIEF:
BACHELOR-FATAL CRASH — A trial date has been set for Chris Soules, the Iowa farmer-turned-reality television celebrity who is charged with leaving the scene of a fatal accident.
CAR THEFT-FATAL CRASH — Police in southeastern Iowa say two people have died in a crash that began with the theft of a pizza delivery car in Ottumwa.
TRUMP-RENEWABLE FUELS — The White House says talks will continue on a dispute over the future of the renewable fuel standard, which has pitted lawmakers from corn-producing states against those representing refineries.
TRAFFIC CAMERAS-IOWA — The Iowa Senate has approved a bill that would ban automated traffic enforcement cameras in the state, though the measure's future is unclear.
DAY CARE DEATH-TODDLER — A Waterloo woman has taken a plea deal in the death of a toddler at her in-home day care.
XGR--IOWA BUDGET-SCHOOLS — Iowa lawmakers have approved K-12 education funding for the next school year and sent the spending plan to Gov. Kim Reynolds.
USDA-NORTHEY — Iowa Secretary of Agriculture Bill Northey has been confirmed by the U.S. Senate to become an under-secretary in the U.S. Department of Agriculture.
PEDESTRIAN KILLED — Des Moines police say they expect charges will be changed because a driver accused of fatally hitting a pedestrian lied about having children in her vehicle.
POLICE CHIEF FIRED-IOWA — An eastern Iowa police chief accused of sending racist and sexist emails has been fired.
SCHOOL RESTROOM-VIDEO PEEPER — A man accused of video recording four people in a Mason City school staff restroom has pleaded not guilty.
PAINKILLER THEFT-CHARGES — A former Des Moines hospital pharmacy worker accused of stealing painkillers from hundreds of patients has pleaded guilty to product tampering.
SPORTS:
BKC_OKLAHOMA ST-IOWA ST
AMES — Injury-plagued Iowa State wraps up home plays against Oklahoma State. By Luke Meredith. UPCOMING: 600 words, photos. Game starts at 6 p.m.
BKC--BIG TEN TOURNAMENT
MADISON, Wis. — What an odd year in the Big Ten. Michigan State looks Final Four-worthy but faces off-court questions. Wisconsin looks better of late but still faces a losing record. And Nebraska — Nebraska! — is up. By Genaro C. Armas. SENT: 780 words, photos.
___
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.