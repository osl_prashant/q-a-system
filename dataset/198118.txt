Grain and soy markets shot higher  as traders actively covered short positions. HRS wheat futures led the price recovery amid a hot weekend forecast and spreading drought across the Northern Plains that fueled spring wheat crop concerns. Winter wheat futures followed on contra-seasonal strength in the face of increased harvest activity. Corn and soybean futures also worked higher. While there’s potential for more near-term corrective buying in corn futures as funds still hold a massive net short position, we rewarded the rally with old- and new-crop sales as June rallies are typically short-lived in the corn market.
Pro Farmer Editor Brian Grete highlights this week's Pro Farmer newsletter below:



Live cattle futures posted weekly losses after failing to find followthrough buying on the early week rally to new contract highs in summer-month contracts. Lean hog futures also firmed amid continued strength in the cash hog market.
Click here for this week's newsletter.