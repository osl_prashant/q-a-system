Corn Area Planted for All Purposes and Harvested for Grain
Corn Area Planted for All Purposes and Harvested for Grain - States and United States:
2016 and 2017
-----------------------------------------------------------------------------------------
                 :   Area planted for all purposes   :     Area harvested for grain      
      State      :-----------------------------------------------------------------------
                 :      2016       :      2017       :      2016       :     2017 1/     
-----------------------------------------------------------------------------------------
                 :                              1,000 acres                              
                 :                                                                       
Alabama .........:        330               250               315               235      
Arizona .........:         95                85                50                40      
Arkansas ........:        760               680               745               665      
California ......:        420               460               100               100      
Colorado ........:      1,340             1,400             1,170             1,220      
Connecticut 2/ ..:         25                25              (NA)              (NA)      
Delaware ........:        170               190               164               180      
Florida .........:         80                80                40                45      
Georgia .........:        410               370               340               320      
Idaho ...........:        340               310               100                90      
                 :                                                                       
Illinois ........:     11,600            11,100            11,450            10,950      
Indiana .........:      5,600             5,500             5,470             5,370      
Iowa ............:     13,900            13,500            13,500            13,100      
Kansas ..........:      5,100             5,300             4,920             5,000      
Kentucky ........:      1,500             1,350             1,400             1,260      
Louisiana .......:        620               470               550               460      
Maine 2/ ........:         31                31              (NA)              (NA)      
Maryland ........:        460               510               400               450      
Massachusetts 2/ :         16                16              (NA)              (NA)      
Michigan ........:      2,400             2,500             2,040             2,120      
                 :                                                                       
Minnesota .......:      8,450             8,000             8,000             7,550      
Mississippi .....:        750               560               720               540      
Missouri ........:      3,650             3,250             3,500             3,100      
Montana .........:        115               105                55                55      
Nebraska ........:      9,850             9,800             9,550             9,500      
Nevada 2/ .......:         11                11              (NA)              (NA)      
New Hampshire 2/ :         15                15              (NA)              (NA)      
New Jersey ......:         80                75                71                66      
New Mexico ......:        120               140                41                56      
New York ........:      1,100             1,050               570               550      
                 :                                                                       
North Carolina ..:      1,000               880               940               820      
North Dakota ....:      3,450             3,700             3,270             3,450      
Ohio ............:      3,550             3,500             3,300             3,230      
Oklahoma ........:        400               370               350               330      
Oregon ..........:         80                95                39                55      
Pennsylvania ....:      1,400             1,400               950             1,000      
Rhode Island 2/ .:          2                 2              (NA)              (NA)      
South Carolina ..:        375               340               350               315      
South Dakota ....:      5,600             5,200             5,130             4,800      
Tennessee .......:        880               840               830               780      
                 :                                                                       
Texas ...........:      2,900             2,400             2,550             2,100      
Utah ............:         80                80                29                30      
Vermont 2/ ......:         90                90              (NA)              (NA)      
Virginia ........:        490               480               340               330      
Washington ......:        170               180                85                85      
West Virginia ...:         49                46                35                33      
Wisconsin .......:      4,050             4,050             3,220             3,050      
Wyoming .........:        100               100                69                66      
                 :                                                                       
United States ...:     94,004            90,886            86,748            83,496      
-----------------------------------------------------------------------------------------
(NA) Not available.                                                                      
1/   Forecasted.                                                                         
2/   Area harvested for grain not estimated.See the full report.