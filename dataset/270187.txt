Dead, diseased cattle found at farm; 3 people arrested
Dead, diseased cattle found at farm; 3 people arrested

The Associated Press

OVERTON, Neb.




OVERTON, Neb. (AP) — Authorities say three people were arrested after officers found dozens of dead and diseased cattle at a farm near Overton in Dawson County.
At least 65 cattle carcasses were found by deputies acting under a search warrant, and the three were arrested Tuesday. Authorities say the suspects haven't yet been formally charged.
About 75 other cattle, as well as llamas, donkeys, miniature ponies and other animals were moved elsewhere for care.