Award applications for the 12th annual National Beef Quality Assurance (BQA) Awards now are being accepted. The 2019 National BQA Awards recognize five winners in the areas of beef, dairy, marketing and education:

The BQA Cow Calf and BQA Feedyard awards recognize producers who best demonstrate the implementation of BQA principles as part of the day-to-day activities on their respective operations.
The BQA / FARM (Farmers Assuring Responsible Management) award honors those dairy operations that demonstrate the best in animal care and handling while implementing the BQA and FARM programs at the highest levels.
The BQA Marketer Award acknowledges livestock markets, cattle buyers and supply-chain programs that promote BQA to their customers and offer them opportunities to get certified.
The BQA Educator Award celebrates individuals or companies that provide high quality and innovative training to individuals that care and handle cattle throughout the industry chain. 

The National BQA Awards are selected by a committee of BQA certified representatives from universities, state beef councils, sponsors and affiliated groups. Nominations are submitted by organizations, groups or individuals on behalf of a U.S. beef producer, dairy beef producer, marketer or educator. Individuals and families may not nominate themselves, though the nominees are expected to be involved in the preparation of the application. Past nominees are encouraged to submit their application under the new nomination structure. Previous winners may not reapply.

The National Cattlemen’s Beef Association manages the BQA program as a contractor to the Beef Checkoff Program. Funding for the BQA Awards is made possible by the generosity of Cargill, which has supported the program since its inception, and Boehringer Ingelheim Animal Health, which sponsors the BQA Educator Award.

Find the application and nomination requirements here. Applications are due by June 1, 2018.

For more information about BQA visit BQA.org.