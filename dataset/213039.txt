Chicago Mercantile Exchange lean hogs reversed Tuesday's selloff, helped by short-covering and Wednesday morning's wholesale pork price rebound that lifted contracts to new highs, said traders. They said profitable packer margins and good pork demand during the summer grilling season could bode well for cash prices in the near term. 
June closed 1.425 cents per pound higher at 81.925 cents, and made a new high of 82.675 cents. July ended 2.025 cents higher at 83.025 cents, and posted a fresh high of 83.350 cents. 
U.S. government data on Wednesday afternoon showed the average wholesale pork price up 87 cents per cwt from Tuesday to $91.00. 
Wednesday afternoon's average cash hog price in Iowa/southern Minnesota was $73.07 per cwt, $1.27 higher than on Tuesday, USDA said.
Fund Buying Extends CME Live Cattle Futures Gains
The previous session's CME live cattle advances carried over into Wednesday, with support from fund buying and short-covering on the final trading session of the month, said traders. 
Futures' discounts to this week's initial cash prices provided additional market support, they said. 
June ended up 1.450 cents per pound to 124.425 cents. August finished 1.850 cents higher at 121.700 cents and surpassed the 20-day moving average of 120.833 cents. 
Wednesday morning's Fed Cattle Exchange yielded an average top price of $132.24 per cwt, down modestly from last week's $132.54 average high. 
Last week packers paid $132 to $133 per cwt for slaughter-ready, or cash, cattle in the Plains that brought $133 to $134 the previous week. 
A few packers may need cattle for the first full week of production after the Memorial Day holiday, a trader said. Extremely high packer profits, and beef demand not falling apart, is supportive for cash prices, he said. 
Wednesday afternoon's average wholesale beef price rose 29 cents per cwt to $245.54 from Tuesday. Select cuts fell $1.27 to $218.18, the U.S. Department of Agriculture said. 
The average beef packer margins on Wednesday were a positive $158.55 per head, up from a positive $142.10 last week, as calculated by HedgersEdge.com. 
"If beef demand in June supports packer margins as well as it is today, then packer demand for slaughter cattle ought to continue solid as well," said Cassandra Fish, author of industry blog The Beef. 
Technical buying and additional CME live cattle futures buying lifted the exchange's feeder cattle contracts for a third consecutive session. August feeders ended 2.650 cents per pound higher at 152.575 cents.