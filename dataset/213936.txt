Door County, Wisc. is a tourist destination on the east side of the state, near Green Bay.

The county picks up a lot of visitors from Illinois and for those wanting to see apple orchards, pumpkin patches and the beauty of Lake Michigan.

One business that’s embracing agritourism is Dairy View Country Store, an integral part of Schopf’s Dairy Farm.

Watch the story from the Wisconsin Milk Marketing Board on how the Schopf Family is seizing this opportunity to teach the public about modern agriculture on AgDay above.