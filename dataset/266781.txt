BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Mon.Thu.No. 2 Soft wheat4.46¼4.42No. 1 Yellow soybeans10.10½10.19¾No. 2 Yellow Corn3.67¼e3.67¾eNo. 2 Yellow Corn3.86¼p3.85¾p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted