New regulations to protect California citrus from huanglongbing are now in place.
Two new Asian citrus psyllid regional quarantines, one for bulk citrus movement and one for nursery stock movement, went into effect on Jan. 1, according to a news release.
The California Department of Food and Agriculture submitted the quarantine amendment as an emergency request to the Office of Administrative Law on Dec. 14 and it was approved on Dec. 26.
The boundaries of the quarantine zones for bulk citrus movement change under this regulation. Asian citrus psyllid-free compliance standards for bulk citrus movement remain the same when moving citrus between these new regional quarantine zones, according to the release.
Citrus growers, packers and haulers are being encouraged to complete new exhibit forms as soon as possible to be in compliance with the new regulations, according to the release. A deadline has not been established yet. 
Growers, packers and haulers can access the forms on CDFA’s website or wait to receive a hard copy in the mail.
To date, more than 340 HLB positive trees have been found in Southern California, all on residential properties. 
A document with frequently asked questions about the quarantine regulations is online.