Farm Journal Field Agronomist Ken Ferrie says there are technologies readily accessible to growers now that would be easy and profitable for them to adopt, such as row clutch shutoffs.
Ferrie told Farm Journal AgTech Expo attendees that when it comes to adopting precision ag, keep these two tips in mind.
First, read the manual. “For half of the service calls with our customers we find the answers are in the manual,” he says. He adds he has noticed more and more farmers keeping precision ag and technology manuals in their tractor cabs—not stowed away at the office.
Second, find out if the technology provider can provide you with adequate support. Before you buy, call the company’s customer service line. Check the service center’s responsiveness and wait times.
“There’s nothing worse than a farmer sitting in a field who’s shut down for the day. So unless you’re a total geek, you need support,” he says. “You want a guy that will show up if you need him at 8 p.m. on a Friday.”
Ferrie also sees technology coming to the farm at a fast pace.
“The slow rabbit to shoot is multi-hybrid technology,” he says. “It can bring us the biggest gains in the field, but we have a lot more to learn.”