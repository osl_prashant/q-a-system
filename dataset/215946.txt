MacDon has agreed to be acquired for $939 million (U.S.) by fellow Canadian company Linamar.
MacDon is based in Winnipeg, Manitoba, employs 1,400 and does business in more than 40 countries.
Linamar is based in Ontario and has focused on components mainly for the automotive industry as well as others. The company has an ag harvesting business in Hungary. In total, Linamar has almost 60 manufacturing facilities worldwide, employs 24,500 and has sales of over $5 billion (U.S.) in 2016.
MacDon has been reportedly seeking a buyer since 2013.