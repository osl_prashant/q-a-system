Written by Scott Dee
Recent stories in some well-read mainstream media publications make it difficult for consumers to weed out the difference between the truth about antimicrobial resistance (AMR) and assumptions. Media suggests the livestock industry is not properly managing the antibiotic issue. Assumptions like this have led to action, such as the recent San Francisco law requiring grocery stores and meat producers to label the average time and quantity of drugs given to livestock.  
Let’s talk about the truths of AMR.
Truth #1:  There is no direct evidence that links antibiotic use in livestock to clinical failure of therapy in humans.
Truth #2: Livestock producers must follow federal withdrawal times to make sure the antibiotic has left the treated animal’s body by time of harvest.
To ensure farmers follow withdrawal dates and our meat supply is safe, the USDA monitors and tests the meat supply for antibiotic residues in the meat.
Truth #3: Withholding treatment to sick animals is inhumane.
Veterinarians take an oath to protect animal health and prevent and relieve animal suffering, conserve animal resources, promote public health, and advance medical knowledge. What does this have to do with antibiotics? When an animal is sick, treatment with antibiotics is the ethical thing to do to relieve the animal from suffering.
Data from a recent Pipestone Applied Research scientifically validated study indicates the inability to use antibiotics in a judicious, responsible manner to treat sick animals can result in unacceptably high mortality levels. As a veterinarian and animal caretaker, it’s at my core to use every approved tool I can to save animals’ lives while being judiciously responsible for my family, community and environment.
Truth #4: Antibiotic Resistance has been occurring in bacteria for millions of years, long before the discovery and development of antibiotics.

Antibiotic-resistant bacteria have been identified in the stomach contents of mummified humans.
Antibiotic-resistant bacteria have been identified in 4-million-year-old caves in which humans have never set foot.
Samples of bacteria collected from pens of pigs indicate resistance to antibiotics that have never been used on the farm.

Truth #5: The environment is the real driver of resistance.
Often overlooked is the effect of environmental bacteria on the development and spread of resistance:

Bacteria and molds have been in our environment for millions of years.
To compete and survive, they have developed metabolites which are the precursors to antibiotics.
Genetic code for these metabolites are easily exchanged from one bacteria to another. 
The widespread exchange of genetic material results in the development of bacteria that are resistant to the metabolite (antibiotic).
Because of this phenomenon, bacteria can develop resistance to antibiotics, even if they are never used in the population or on the farm.

Truth #6: More research is needed to understand AMR. Antimicrobial resistance is complicated, and with research so new on the topic, it’s easy for people to jump to conclusions.
Pipestone takes AMR and responsible antibiotic use seriously, which is why we have dedicated time and resources to the Pipestone Antibiotic Resistance Tracker program. PART provides Pipestone veterinarians and livestock producers the tools for responsible antibiotic use and the ability to track it. Pipestone is also undergoing AMR research trials so we can better understand how to combat antibiotic resistance. We applaud those who are taking steps to better understand AMR and ask others to join us before making assumptions.