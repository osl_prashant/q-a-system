Mexico seizes ranches, exotic animals owned by ex-governor
Mexico seizes ranches, exotic animals owned by ex-governor

The Associated Press

MEXICO CITY




MEXICO CITY (AP) — The government of the northern Mexico state of Chihuahua says it has seized four ranches that allegedly belonged to former governor Cesar Duarte, who faces corruption accusations.
Herds of bison, llamas and wild boars roamed the ranches, along with cattle that were apparently imported from New Zealand by the Mexican government to be distributed among poor farmers.
The state government said Monday the land measured a total of almost 5,800 acres (2,344 hectares).
It said Duarte acquired the ranches during his 2010-2016 term, and they were part of about 20 properties totaling almost 100,000 acres that have been seized as part of the investigation against the former governor.
The state wants the federal government to press the U.S. to extradite Duarte, who allegedly channeled funds to the ruling party.