BC-Cash Prices, 1st Ld-Writethru,0342
BC-Cash Prices, 1st Ld-Writethru,0342

The Associated Press

NEW YORK




NEW YORK (AP) — Wholesale cash prices Thursday
Thu.        Wed.
F
Foods

Broilers national comp wtd av            .9124       .9124
Eggs large white NY Doz.                  1.56        1.49
Flour hard winter KC cwt                 14.95       15.15
Cheddar Cheese Chi. 40 block per lb.    2.1150      2.1150
Coffee parana ex-dock NY per lb.        1.1814      1.1846
Coffee medlin ex-dock NY per lb.        1.3874      1.3895
Cocoa beans Ivory Coast $ metric ton      2405        2405
Cocoa butter African styl $ met ton       6383        6383
Hogs Iowa/Minn barrows & gilts wtd av    63.71       63.84
Feeder cattle 500-550 lb Okl av cwt     181.00      181.00
Pork loins 13-19 lb FOB Omaha av cwt     91.61       91.29
Grains
Corn No. 2 yellow Chi processor bid     3.54¾       3.53¾
Soybeans No. 1 yellow                  10.13        10.15¼
Soybean Meal Cen Ill 48pct protein-ton 382.50      381.50
Wheat No. 2  Chi soft                   4.53¼       4.49¼
Wheat N. 1 dk  14pc-pro Mpls.           7.48         7.36¼
Oats No. 2 heavy or Better              2.74¾       2.72¼
Fats & Oils
Corn oil crude wet/dry mill Chi. lb.    .31¼         .31¼
Soybean oil crude Decatur lb.           .30½         .30¼
Metals
Aluminum per lb LME                    0.9933        1.0155
Antimony in warehouse per ton            8550          8550
Copper Cathode full plate              3.1763        3.1872
Gold Handy & Harman                    1328.35      1330.50
Silver Handy & Harman                   16.640       16.615
Lead per metric ton LME                2561.00      2569.00
Molybdenum per metric ton LME           15,500       15,500
Platinum per troy oz. Handy & Harman    990.00       998.00
Platinum Merc spot per troy oz.         999.40       996.60
Zinc (HG) delivered per lb.             1.6163        1.6272
Textiles, Fibers and Miscellaneous
Cotton 1-1-16 in. strict low middling    75.73       76.53
Raw Products
Coal Central Appalachia $ per short ton   60.10       60.10
Natural Gas  Henry Hub, $ per mmbtu       2.65        2.61
b-bid a-asked
n-Nominal
r-revised
n.q.-not quoted<29
n.a.-not available