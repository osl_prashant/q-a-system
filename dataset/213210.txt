Floods. Blizzards. Mother Nature dealt her fair share of calamity in April and early May, and the majority of farmers say the inclement weather will push back planting dates for 2017.On May 3, the latest Farm Journal Pulse poll asked: “How is your planting schedule this year compared to normal?” More than 1,300 farmers responded and 63% say they will face some level of planting delays in 2017.
About 32% of respondents say they are “way behind” their typical planting schedule, with another 31% saying they are “slightly behind.” One in five farmers say they are more or less on pace, with the remaining 18% reporting they are either “ahead” or “way ahead” of normal.
 


“How is your planting schedule this year compared to normal?” [5-3-2017]


© Farm Journal Media



As of April 30, the pace of planting the 2017 corn crop was in lockstep with the five-year average, with 34% of the crop in the ground, according to USDA-NASS Crop Progress reports. And 10% of soybeans have been planted this year, versus a five-year average of 9%.
The reports, which are updated every Monday afternoon, will be watched closely by farmers, analysts and others to see just how much a week’s worth of widespread precipitation across the Plains and Midwest will affect future Crop Progress reports.
Delays can make farmers feel antsy, but Chris Barron, an Iowa producer with Ag View Solutions, adds that soil and environmental conditions are more important than the actual planting date.
“We don’t want the calendar to dictate a lot of the things we’re thinking through,” he says. “Don’t let that cause us to go out and do some things in terms of conditions that might be costly.”