A popular configuration in Australia are right-hand discharge auger on grain carts.
For 2018, Elmer’s Manufacturing is introducing this as an option on its lineup of grain carts.
With more and more operators folding in their auger after unload, the advantages of the right-side discharge are generating interest as it allows for increased comfort and safety for the operator.
Elmer’s Manufacturing previously offered the right-hand auger option for the Australian Market. And the company says operators report that looking out the right-hand side of the tractor is much more natural when adjusting hydraulic and throttle controls while it's also more comfortable for using the tractor’s pedals during unloading.
Elmer’s is also looking into offering conversion kits for existing carts.