Even though you may be throwing another log on the fire as you read this, spring is quickly approaching, and so is the sense of urgency taking root in the catacombs of your mind. With all that’s going on, did you fail to launch a 2018 strategic planning initiative? Not to worry.
Stop Groundhog Days. We all know that feeling, the one depicted in the movie “Groundhog Day.” Bill Murray plays Phil, a weatherman doomed to repeat the same day over and over until he realizes he can change and get it right.
Do you have a few lingering groundhog issues? Perhaps a long overdue conversation about succession planning still hasn’t been scheduled. Maybe you’ve pondered changing direction in your business but no one has made the necessary decisions to do so.
There are always things that seem stuck. They neither move forward nor off of our proverbial plates. But take heart—it’s still winter, after all—and try my simple system called the Strategic Plan On A Page In One Hour Or Less.
The plan-on-a-page concept is a strategic-thinking tool for individuals or teams. One hour is set aside to work through five sections (more on that next) to create strategic direction. Shorter and more frequent planning efforts shouldn’t replace major initiatives, retreats or professionally facilitated sessions. But there is a case to be made for short quarterly planning bursts.
Strategy Essentials. Here are the five strategy essentials to incorporate into your conversation about strategic planning.
Values: Define core values for your life and farm. What motivates you? What is this operation’s legacy?
Vision: Define your vision going forward. What are your aspirational goals for the operation? Where is it headed under your tutelage?
Analysis: Consider analyzing strengths, weaknesses, opportunities and threats (SWOT). Go through each of these very quickly.
Goals: Set goals for the short- and long-term, and give them some meat. Ensure your objectives are specific, measurable, ambitious, realistic and with timelines (SMART).
Priority: Now, set a single top priority. Remember, priorities and goals are not synonyms.
Fail-Proof Accountability. Now that you’ve created the plan, use it. Accountability is the key to keeping even the most driven people on track. Several strategies can keep you focused on your plan. Schedule a review, then follow up after the meeting with a feedback request to everyone involved via email or in person. Also, name a quarterback to keep the plan alive and schedule the next meeting.
The year is still young: Give the plan on a page a try. It’s only an hour!
Create Your Plan On A Page In An Hour
Here are the steps you can take to create a strategic plan for your farm. Want a template? Visit sarahbethaubrey.com for a free worksheet.
1. Before starting the hour, divide up the five sections of the process outlined in this column. Spend five minutes on values, 10 on vision, 20 on analysis, 20 on goals and five on priorities.
2. Assemble your team along with supplies such as a white board, pens and paper.
3. Use a timer or stopwatch app or your phone and stick to the hour-long deadline.
4. If you’re doing this exercise as a group, complete the project by having a debrief discussion and compare notes.
5. Have someone summarize the messy notes into a nice plan on a page that you can read, share and use within your organization.