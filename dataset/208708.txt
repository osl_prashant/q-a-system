In research on finishing pigs, under modern conditions, growth promotion levels of antibiotics had no effect on growth parameters, say researchers at Kansas State University. 
“Thus, as producers, veterinarians and nutritionists develop strategies for replacing in-feed antibiotics, expectations on how feed additives can be used to replace the lost performance in the nursery, but not the finisher, should be a focus of the decision process,” says Joel DeRouchey, Ph.D, in his paper, “Alternatives to feed medication.”
 
DeRouchey and other researchers at K-State looked at these feed additives: enzymes, probiotics, phytogenics, acidifiers, and copper and zinc. Here is a summary of their findings.
 

Enzymes

“Enzymes are proteins that can be used to target different substrates in the diet to accelerate their breakdown,” DeRouchey says. “While phytase has been shown to be effective in improving phosphorus digestibility, carbohydrate-degrading enzymes have been less consistent in demonstrating responses.” 
 

Probiotics

DeRouchey explains probiotics are live cultures of organisms that are added to pig diets to improve the microbial balance in the gut of the host animal. Results are mixed, which might be partially explained by the use of different direct-fed microbial (DFM) strains. 
 
“A separate review indicated that results of growth performance trials with probiotics have been inconsistent,” DeRouchey says.
 

Acidifiers

These compounds have acidic properties and may be either organic or inorganic. Their use in commercial nursery diets is relatively common, DeRouchey says. 
 
“The age of the pig can affect the response to acidifiers, with newly weaned pigs showing the greatest response,” he explains. “The stomach of a weaned pig is not yet physiologically mature and may not be able to secrete a sufficient amount of acid to aid in digestion of solid food or inhibit proliferation of detrimental bacteria, but the exact mechanism of the response to acidifiers is not clear.”
 
He adds that a review of experiments shows acidifiers can improve the performance of grow-finish pigs under stressful or disease conditions and appear to be an effective measure to reduce scouring rate and mortality as well as sustain good growth performance.
 

Copper and zinc

In the nursery stage, when fed at high concentrations (100 ppm to 250 ppm for copper and 2,000 ppm to 3,000 ppm for zinc), these minerals are known to exert positive influences on growth rate,” DeRouchey says. “While the exact modes of action for the responses are unclear, they may contribute to antibiotic-like activities; however; long-term environmental limitations may prevent high levels from being fed.”
 

Research design

Because most feed additives give a modest response in the 1% to 3% range for growth, adequate replication is required, DeRouchey says. 
 
“Most often 10 to 20-plus replications of each treatment are required to determine a statistically significant effect,” he says. “Thus the ingredients used, ingredient interactions, weight range, use of appropriate control diets or use of factorials are all considerations when properly designing an experiment for feed additives.
 
“Also, the use of commercial barns, which have a higher number of pigs per pen, often allow for a decrease in variation, allowing greater sensitivity in determining and more confidently reporting the results,” he adds.