Leaders in Washington at both the USDA and EPA are searching for a win-win solution to disagreements about the Renewable Fuel Standard (RFS) and Renewable Identification Numbers (RINs) market.
AgDay host Clinton Griffiths sat down with EPA Administrator Scott Pruitt and USDA Secretary Sonny Perdue and asked them about the current debate.
“The president has to balance the whole economy,” says Purdue. “We've got some tough choices head about how we move forward.”
One choice will be adding demand to the marketplace for ethanol through year round approval of E15.
“Our focus is providing clarity on the [Reid Vapor Pressure] RVP waiver on whether that's something we can do legally,” said Pruitt. “We've been involved in about a four- to six-month process evaluating that question.”
He says it is a legal question and not a policy question.
“It isn't a matter of preference or at our discretion” says Pruitt. “It's a matter of whether the statute permits that kind of decision.”
Currently his team has found arguments on both sides.
“We're trying to evaluate the merits of those arguments and the language of the statue to determine what authority does exist at the agency on granting the ability to sell E15 12 months a year,” says Pruitt. “Absolutely it makes sense to be able to sell 12 months a year because it helps with the infrastructure.”
Pruitt believes they’ll have an answer to the statutory questions soon but that leaves questions about the RIN market and potential changes. Farm groups and ethanol advocates have been adamant that a cap on RIN prices isn’t a workable solution.
Pruitt says before hitting the blend wall, RINs were trading at a lower price.
“The question is, what's driving that? Is there speculation?” asked Pruitt “Is there some other some changes that can be made to enhance transparency in the trading platform?”
The search for transparency seems to be the mantra from leadership in Washington. Secretary Perdue says leaders are discussing ways to impact that market.
“You can have people who create RINs have to dispose of them in 30 days,” says Perdue. “Not hoarding or playing the market like the transparency of who's trading in the commodities.”
He says corn, wheat and soybean speculators can only hold certain positions.
“I have actually contacted to the exchanges, the CME Group and ICE to ask if they have ideas about how we could do a transparent market,” says Perdue.
Administrator Pruitt says this issue continues to be top of mind for President Trump.
“He's going to continue until we provide some answers and hopefully we can,” says Pruitt. “I pray that we can find an answer going forward, that helps address both those who produce ethanol and those who have obligations to pay the RINs because it matters to our economy on both sides the ledger.