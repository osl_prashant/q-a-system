Ohio senator seeks review of medical pot program after error
Ohio senator seeks review of medical pot program after error

By JULIE CARR SMYTHAP Statehouse Correspondent
The Associated Press

COLUMBUS, Ohio




COLUMBUS, Ohio (AP) — A state lawmaker moved Thursday to force a thorough review of Ohio's medical marijuana program because of mistakes in selecting grower applicants.
Republican Sen. Bill Coley, of Cincinnati, proposed legislation that would require State Auditor Dave Yost to conduct and release a performance audit of the program. The bill holds up grower, processor and tester licenses until program flaws can be addressed.
Ohio's medical marijuana law, passed in 2016, allows people with any of 21 medical conditions, including cancer, Alzheimer's disease and epilepsy, to buy and use marijuana if a doctor recommends it. It doesn't allow smoking.
The program was supposed to be up and running by Sept. 8. Coley said his bill would not affect that timing.
"As a human endeavor, there's going to be mistakes made," he said. "The question in somebody's character is what do you do when you find out about it."
The Ohio Department of Commerce acknowledged last week that a scoring error led to one company's inadvertent exclusion from the list of the dozen big marijuana growers receiving provisional licenses.
The agency said it identified the mistake after Yost expressed concern that two employees had complete access to the scoring data.
The department offered to put the program on hold, but Yost said in a letter to the agency sent Wednesday that it's too late for that. A message was left with the department Thursday seeking comment on Coley's bill.
The acknowledged error in scoring has been accompanied by additional allegations of mistakes by others.
A lawsuit filed by some unsuccessful applicants earlier this week claims state regulators failed to follow their own rules when awarding provisional licenses for growing facilities late last year.
Several groups allege various failures in the licensing process, including "scoring errors, undisclosed conflicts of interest, and undisclosed loopholes in the security of information." They ask a judge to revoke the licenses and prevent the department from issuing operators' permits to the companies.
Yost, a Republican running for attorney general, said Thursday that he and his staff will review Coley's legislation and work closely with him and the Legislature "to take the steps necessary to ensure Ohioans have confidence in the medical marijuana program."
He said his office also is continuing to review processes and controls at the Commerce Department to identify any additional system weaknesses.