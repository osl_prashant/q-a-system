Below we plug USDA's weekly crop condition ratings into our weighted (by production) Pro Farmer Crop Condition Index (CCI; 0 to 500 point scale). It shows the condition of the spring wheat crop declined by 14.7 points to 291.31 -- nearly 80 points lower than year-ago. While conditions declined in every state, North Dakota led the way with a 7.6-point drop from last week.





Pro Farmer Crop Condition Index






Spring wheat




This Week




Last Week




Year-Ago





Idaho (5.74%)*


21.52


22.66


22.96




Minnesota (12.99%)


53.00


53.65


46.80




Montana (14.95%)


32.74


36.03


61.60




N. Dakota (50.80%)


146.80


154.42


182.62




S. Dakota (10.36%)


20.82


22.27


36.19




Washington (4.22%)


13.67


14.05


17.19




Spring wheat total


291.31


305.99


371.14




* denotes percentage of total national spring wheat crop production.
Following are details from USDA's National Agricultural Statistics Service (NASS) state crop and weather reports:
North Dakota: For the week ending July 9, 2017, even though many areas of the State received rainfall, much more was needed to help both crops and livestock, according to the USDA's National Agricultural Statistics Service. Despite some recent rain, most of North Dakota was experiencing drought conditions. Some producers in the west planned to bale wheat for livestock feed. Temperatures averaged two to six degrees above normal in the east, while the west averaged six to ten degrees above normal. There were 6.7 days suitable for fieldwork. Topsoil moisture supplies rated 29 percent very short, 33 short, 36 adequate, and 2 surplus. Subsoil moisture supplies rated 20 percent very short, 31 short, 47 adequate, and 2 surplus.
Spring wheat condition rated 16 percent very poor, 19 poor, 29 fair, 32 good, and 4 excellent. Spring wheat jointed was 98 percent. Headed was 79 percent, behind 94 last year, but ahead of 72 average. Coloring was 10 percent, behind 23 last year.
Montana: Another week of hot dry weather with limited precipitation occurred for a majority of the state, according to the Mountain Regional Field Office of the National Agricultural Statistics Service, USDA. The high temperature for the week was Mizpah, reading 105 degrees, while the low temperature was recorded in Deer Lodge at 35 degrees. The highest amount of precipitation was recorded at Pompeys Pillar with 0.55 of an inch of moisture. Crop conditions continue to deteriorate due to the hot dry weather. Haying is running at least two weeks ahead of schedule in some parts of the state, but little is on the market as livestock operations are hesitant to sell given that much of the state is experiencing drought. Only 19 weather stations are reporting above normal precipitation since April 1, 2017, ranging from 0.03 to 1.38 inches above normal.
Minnesota: Thunderstorms in the latter part of the week limited field activity for a fourth straight week, according to Greg Matli, Indiana State Statistician for the USDA's National Agricultural Statistics Service. Producers in southern counties and in the Northwest part of the state welcomed the rain as it helped to improve crop condition in those areas. Elsewhere, excessive rain saturated soils, drowned crops, and left standing water in fields. As water levels of rivers rose, so did concerns for the potential of additional flooding with the forecast of more rain. The statewide average temperature was 74.0 degrees, 0.7 degrees below normal. Statewide precipitation was 1.37 inches, above average by 0.60 inches. There were 4.3 days available for fieldwork for the week ending July 9, unchanged from the previous week.
The spring wheat crop was nearly finished heading, with 95 percent of the crop headed. Fifteen percent of spring wheat was coloring. Spring wheat condition rated 85 percent good to excellent.