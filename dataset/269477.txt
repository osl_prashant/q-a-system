BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Monday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeApr        12.12   12.26   11.96   11.98  Down .10May                                12.17  Down .03Jun        12.20   12.42   12.13   12.17  Down .03Jul                                12.42  Down .02Sep        12.45   12.64   12.37   12.42  Down .02Dec                                13.57  Down .05Feb        13.68   13.78   13.53   13.57  Down .05Apr        13.85   13.94   13.68   13.73  Down .04Jun        13.86   14.04   13.77   13.82  Down .05Sep        14.04   14.25   13.96   14.01  Down .07Dec                                14.60  Down .07Feb        14.66   14.80   14.57   14.60  Down .07Apr        14.66   14.66   14.63   14.65  Down .05Jun        14.73   14.74   14.72   14.72  Down .05Sep        14.94   14.94   14.93   14.93  Down .04Dec                                15.26  Down .04Feb        15.27   15.27   15.26   15.26  Down .04