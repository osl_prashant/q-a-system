Tonisity Appoints General Manager and Chief Operations Officer

Tonisity is growing its senior leadership with the addition of Daniel Faidley as general manager and E. Michael Amos as chief operations officer (COO) of its U.S. business, the company announced recently.
Faidley has more than 20 years of experience building sales teams and developing and implementing results-driven business models, a news release said. “His focus on leadership development makes him a perfect fit in his new role as general manager, where he will execute a business strategy to bolster product adoption of Tonisity Px, the first isotonic protein drink for pigs,” the release said.
Faidley was recently employed by WinField United as regional sales director for the crop protection business, where he played a critical role in the 2017 WinField and United Suppliers merger. For more than 15 years, he also worked for Elanco Animal Health, growing within the company from sales representative to senior director of global marketing.
“I welcome the opportunity to lead and manage the U.S. business and expand its capabilities,” Faidley says. “I look forward to returning to the livestock production sector, where I can use my passion for the industry and leadership experience to help pork producers adopt new technologies.”
Amos Fills Role of Chief Operations Officer

E. Michael Amos has nearly four decades of experience in the animal health sector, working for companies such as Pfizer Animal Health and Zoetis Inc. in a variety of senior sales and marketing roles, the news release said. In addition to his leadership skills, he has a vast understanding of animal therapeutic treatments and preventatives and has worked on a multitude of projects aimed at improving producer profitability.
“Tonisity’s vision for the future aligns with my passions. I am ready to roll up my sleeves and put a plan in place that executes this vision,” says Amos. “I am incredibly grateful for the opportunity to help lead Tonisity to its next phase of innovation.”
Amos holds a bachelor’s degree in microbiology from McGill University. His ability to collaborate and explain technical information gives him the skill set needed to work closely with the general manager to execute Tonisity’s day-to-day operations.  
For more information about Tonisity, visit www.tonisity.com.

Diamond V at 75: New Manufacturing Facility Springing Up

Diamond V program host, Kevin Corizzo (left), interviews Diamond V Executive Vice President Mike Goble at the expansion site of Diamond V’s Cedar Rapids manufacturing facility as part of the April video "Diamond V at 75: Immune Strength for Life."

Construction on Diamond V’s 97,000 sq. ft. plant expansion resumed with the spring weather at the corporate campus and global headquarters on 60th Ave. SW in Cedar Rapids, the company reports. The initial investment in the expansion – announced in 2016 – is $30 million. When all phases of the expansion are complete, the total cost is expected to exceed $70 million.
“We're continuing to expand to supply the needs of the industry and meet the demands of consumers who want our natural, non-antibiotic solutions,” said Diamond V Executive Vice President Mike Goble in a news release. “As we meet those needs, not only domestically but internationally, we see that growth continuing to accelerate.”
Project manager Ryan Gusta said all the structural steel for the expansion is expected to be in place by the end of June and the new plant will be enclosed by the end of September. Machinery will be brought in around February and March of 2019 with the goal of being operational starting in May, 2019.
When all systems are installed, the expansion is expected to double the current production capacity at Diamond V’s south manufacturing facilities, which includes the current 126,000 square foot plant. The plant expansion is expected to create nine new jobs initially with the potential for 26 new jobs in the next five years. Diamond V also operates its north plant on G Ave. NW in Cedar Rapids.
The "Diamond V at 75" video series will continue throughout 2018 and highlight Diamond V science, technology, and animal health and production expertise. To view the video, visit http://www.diamondv.com/media/video-library/75-years/. For more information about the company, call 1.319.366.0745, email info@diamondv.com, or go to the website www.DiamondV.com.


Bob Barclay Joins GlobalVetLINK as Ag Product Manager

GlobalVetLINK (GVL) announces it has named Bob Barclay as Product Manager: Ag Focused. At the animal health software company, Bob will oversee the research, development and marketing of ag products for the organization throughout the product's life cycle.
“I look forward to helping GlobalVetLINK further develop and expand its technology-based solutions for regulatory compliance within the animal health industry, and advancing the infrastructure and SaaS platform to provide solutions for veterinarians and their customers,” Barclay said in a news release from the company.
Barclay brings more than 15 years of experience in product lifecycle management and commercialization to GVL, the release said. He held previous roles in marketing and business development at Zoetis and Renessen, a Cargill - Monsanto joint venture. Most recently, he was in business development for Hologic, a leading medical device and diagnostic company.
“Bob’s experience in the industry will be invaluable in delivering quality solutions to the production animal markets,” said Cliff Smith, CEO of GlobalVetLINK. “We look forward to having Bob’s input and knowledge on our product management team.”
The company offers complete Herd Health Management Solutions for the food animal industry. Learn more about GlobalVetLINK at www.globalvetlink.com.