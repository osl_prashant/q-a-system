Livestock production depends largely on the feeding program, and what you should feed your animals will depend on forage quality analyses. Forage quality determines the potential of a forage to produce the desired animal response. It can measure intake, palatability, digestibility, nutrient content, animal performance, and anti-quality factors. During the last couple of years, analytical procedures have improved as well as the knowledge of how to use test results to increase animal efficiency and performance. Interestingly enough and despite these advances, many livestock producers do not yet recognize the potential of forage quality testing as a management tool for their operations. Knowing the quality of the forages you sell or buy is economically sound and should been taken in consideration.How do I sample forage for quality testing?
Samples of Baled Hay:
Take a separate sample from each field and cutting.
Always sample with a bale core to get representative samples.
Insert the sampler diagonally from the surface and full depth into each bale. This step will insure an accurate sample.
Take at least 20 samples cored from each lot of hay.
ALWAYS mix the 20 cores in a clean bucket and place in a plastic bag.
Label each bag with your name, location, date, address, forage mixture, stage of maturity, and date harvested.
Send your samples to a laboratory of your preference that analyzes forage quality.
Samples of Haylage and Silage at Harvest:
Take a sample of the silage, collecting from three to five handfuls of silage or haylage from the first load of the day in a plastic bag, and place in refrigerator or freezer IMMEDIATELY.
Do not shake the sample since you will alter the real composition of the silage.
Follow the same procedure for several loads of forage throughout the day and proceed to combine samples by mixing well, until you obtain a representative sample.
Repeat this process for each field if more than one field is harvested in one day.
Label the plastic bag with your name, address, sample number, forage mixture, stage of maturity, and date of harvest.
Send your samples to a laboratory of your preference that analyzes forage quality.
Preparing and Storing Collected Samples:
Try to keep hay samples in a cool place, and keep the haylage and silage samples frozen.
Proceed to mail them or bring them to a laboratory early in the week, to prevent bacterial decay over the weekend that might alter the final results.
Results will depend on the amount of sample taken as well as if it was collected randomly across the field.
Summary
Proper sampling ensures that the forage analyses accurately reflect the hay, silage, or pasture being sampled. It is important to follow the steps to obtain a sample of forage in hay or silage, or else inadequate sampling will result in less accurate forage quality data analysis.
View the iGrowFeed Testing Laboratoriespublication for a list of certified laboratories where samples can be sent for forage quality testing
Reference:Collins M and Fritz J (2002) Forage Quality. In R.F. Barnes, C.J. Nelson, M. Collins, and K. Moore (eds.), Forages: The Science of Grassland Agriculture, vol.2. Ames, Iowa: Iowa State University. Press. P: 363-389