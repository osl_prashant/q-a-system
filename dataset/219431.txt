To start the year, cotton prices were seeing a steady climb. Recently, they have seen a bit of a pull back. To John Payne of This Week In Grain, this signifies that the gins are finally catching up to the strong supply.

“At the beginning of harvest and as the gins started to fill up, we had all these hands go up from overseas,” he said on AgDay. “The price spiked to try to get some of those sales slowed. The gins have really worked the last couple of months.”

Payne thinks moving into 2018, the price could be right to see more cotton acres replace wheat.

“It certainly beats trying to throw a loser after the wheat and a lot of them throw in the towel on the wheat market, and with corn prices where they are, not that attractive,” he said.

Hear why he thinks some farmers should consider growing milo wheat on AgDay above.