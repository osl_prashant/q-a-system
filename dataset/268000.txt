BC-US--Cotton, US
BC-US--Cotton, US

The Associated Press

New York




New York (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Monday:
(50,000 lbs.; cents per lb.)
COTTON NO.2Open    High    Low    Settle     Chg.May       82.89   83.69   82.70   82.91  Up    .37Jul       82.50   83.34   82.39   82.76  Up    .58Aug                               78.17  Up    .23Oct       80.21   80.35   80.01   80.01  Up    .29Oct                               78.17  Up    .23Dec       77.90   78.23   77.88   78.17  Up    .23Dec                               78.27  Up    .15Mar       78.23   78.42   78.07   78.27  Up    .15May       78.38   78.60   78.34   78.47  Up    .13Jul       78.42   78.60   78.30   78.51  Up    .17Aug                               73.39  Up    .04Oct                               75.52  Up    .08Oct                               73.39  Up    .04Dec       73.45   73.45   73.15   73.39  Up    .04Dec                               73.44  Up    .04Mar                               73.44  Up    .04May                               73.86  Up    .04Jul                               73.94  Up    .04Aug                               72.28  Up    .04Oct                               73.51  Up    .04Oct                               72.28  Up    .04Dec                               72.28  Up    .04Dec                               72.32  Up    .04Mar                               72.32  Up    .04