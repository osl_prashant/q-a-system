Salinas, Calif.-based Taylor Farms has recalled more than 6,500 pounds of chicken and pork salad products due to possible listeria contamination.
Taylor Farms issued the recall in response to notification by Sargento Foods that it was recalling Bevel Shred Pepperjack cheese products, which it supplies to the Taylor Farms facilities that produced the affected salad products, according to a news release.
Sargento recalled the products due to possible listeria contamination.
The salads were produced and packaged Feb. 6-9 at facilities in Dallas and in Tracy, Calif. Those products were then shipped to distribution centers in Los Angeles and Tracy, to Portland, Ore., and to Houston, San Antonio and Roanoke, Texas.
The following salad products were recalled:
10.5-oz. plastic trays of Signature Cafe Southwest Chicken Premade Salad with use-by dates of Feb. 13-15
10.5-oz. plastic trays of Signature CafÃ© Southwest Style Salad with Chicken with use-by dates of Feb. 14-16
10.5-oz. plastic trays of H-E-B Shake Rattle & Bowl Rowdy Ranch Hand with use-by dates of Feb. 17-19
The products subject to the recall include the numbers 34013 or 34733 in the USDA mark of inspection label.