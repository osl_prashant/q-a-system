Barrasso proposal to help ranchers and farmers with regs
Barrasso proposal to help ranchers and farmers with regs

The Associated Press

CHEYENNE, Wyo.




CHEYENNE, Wyo. (AP) — U.S. Sen. John Barrasso has introduced legislation he says would protect ranchers, farmers, and rural communities from federal overregulation.
The Wyoming Republican says the proposed act would help defend agricultural industries from punishing federal rules and duplicative permitting requirements. The bill also includes language supporting an efficient permitting process for predator control at the U.S. Fish and Wildlife Service.
Among other things, the proposal would protect personal producer information and identity privacy, end duplicative environmental permitting for pesticide application requirements and prevent penalties to farmers who are conducting normal agricultural operations that could be considered "baiting" of migratory game birds.
Barrasso's state Republican colleague, Sen. Mike Enzi, is among the co-sponsors.