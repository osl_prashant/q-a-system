The Allen Lund Co. has opened an office in Nashville, Tenn.
John Maxwell is the office manager.
"John brings a great deal of experience and enthusiasm to our company and we are excited he is joining our team," said Jim McGuire, vice president of sales branch operations, in a news release. "Nashville is a terrific city and opening an office there fits in perfectly with our plans to grow."
Maxwell
Maxwell has 17 years of experience in the third-party logistics industry.
"I am honored to join Allen Lund Co., with its rich history and stellar reputation with customers, carriers and employees. I look forward to sharing that same high level of service, commitment and integrity here in Nashville," Maxwell said in the release.