This market update is a PorkNetwork weekly column reporting trends in weaner pig prices and swine facility availability.  All information contained in this update is for the week ended August 4, 2017.
Looking at hog sales in February 2018 using February 2018 futures the weaner breakeven was $38.53, up $5.46 for the week. Feed costs were down $1.13 per head. February futures decreased $0.28 compared to last week’s February futures used for the crush and historical basis is improved from last week by $2.36 per cwt. Breakeven prices are based on closing futures prices on August 4, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was below average this week with 37,541 head being reported, which is 99% of the 52-week average. Cash prices were $19.04, down $1.01 from a week ago. The low to high range was $11.50 - $30.00. Formula-priced weaners were up $1.58 this week at $38.25.
Cash-traded feeder pig reported volume was below average with 4,485 head reported. Cash feeder pig reported prices were $45.71, down $2.42 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through August 4, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential increased $6.47 this week to a projected gain of $19.49 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.