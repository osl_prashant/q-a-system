As America's cow herd dropped to 60-year lows two years ago, ranchers saw an economic incentive to save and breed replacement heifers. Demand for the young females spiked, creating a profitable niche market which now appears to have flamed out.Heifer prices were strong throughout 2013 and 2014 as the U.S. cowherd rebuilt and prices rose. Profits were high for those who capitalized on developing breeding heifers during the market rally. Now, developing heifers isn't as attractive to those who bought high-priced heifer calves in the spring with hopes of selling higher-priced bred cattle in the fall.
The past year saw bred and open heifer markets go through a downward spiral. It followed similar beef market trends witnessed throughout 2015 heading into 2016, as fat cattle and calf prices retreated to prices last seen in 2013.
Video Volatility
Video auctions provided an example of the volatility of selling replacements. In October 2013, for instance, Superior Livestock Auction held its first replacement female sale. Bred heifers topped out at $2,125 for the inaugural sale where 9,000 head of breeding stock were offered, including cows, pairs and open heifers.
Demand was strong and prices for bred heifers continued to rise at the Select Female Auction. The market high reached $3,250 at both the September and November sales in 2014.
At the latest female video auction on Dec. 18 bred heifers sold from $1,450 to $2,125 per head, and open replacements went on the block at $1,175 to $1,325. In 2014, the same auction had much better numbers, with the price floor being higher than 2015's ceiling. Bred heifers sold for $2,175 to $3,000 in 2014, with replacements going for $2,050 to $2,250.

"I think the decline was a combination of the lower cattle prices combined with an oversupply of heifers," says Danny Jones, president of Superior Livestock.
Jones estimates the bred heifer price would have fallen at least $200 even if the feeder calf price had maintained at the $225/cwt. level. He adds it was a good idea to develop heifers. The only problem: "everybody had the same idea."
Andrew Griffith, economist with University of Tennessee, observed more producers in his area holding back or buying heifers which should have probably gone to the feedlot.
"I guess they thought we needed to grow the cattle herd all over night, and they threw out the window that we need high-quality heifers," Griffith says. "Those low-quality heifers should be on your dinner plate."
U.S. Department of Agriculture data indicates replacement heifer retention has been on the rise for at least the past two years. Both in 2011 and 2012 (no data was available from 2013) the replacement heifer herd was at 4.2 million head. Then in 2014 it jumped 400,000 head to 4.6 million. Last year retention rates improved 7 percent with 4.9 million total beef replacement heifers.
Established Sales Not Immune
In the fescue covered pastures of Kentucky, a group of like-minded cattlemen gathered their resources the last 12 years to develop replacement heifers. 
Quality heifers bred to calving ease bulls and cared for with a preset vaccination program has helped build business for Central Kentucky Premier Heifer Sale (CKPHS). Even when the cattle market started to trend downward in the spring, CKPHS's June 2015 auction averaged $2,965 for fall-calving heifers.
"It sure has been a different cycle here for us," says David Sandusky, a Lebanon, Ky. farmer and chairman for CKPHS. "We've seen a huge decrease in price and demand for these heifers."
The latest CKPHS auction in November for spring calving heifers saw prices drop more than $750 per heifer, an average of $2,201.
"We still have about 500 heifers that we don't have a home for. Demand is very depressed," Sandusky adds.
Another established heifer sale has seen similar price fluctuation. November's Show-Me-Select Program hosted by University of Missouri Extension saw prices dip after reaching records in 2014. At this fall's auction bred heifers sold for $2,477. The sale at the Joplin Regional Stockyards was down an average of $412 per head compared to 2014.
The Show-Me-Select heifer sales saw softer prices in 2015 after a record year for the various auctions across Missouri.Photo by University of Missouri Extension
Two months later Joplin Regional Stockyards (JRS) hosted their own female sale on Jan. 14 with most of the cattle originating from local farms. Approximately 500 bred heifers sold from $1,400 to $2,000, with the majority of those females slated to calve in February and March. Cattle went to neighboring states, like Oklahoma and Arkansas, which are still rebuilding from drought.
JRS co-owner Jackie Moore notes the amount of cattle walking through the ring wasn't as high as he would expect. "I think a lot of those people have quite a bit of money tied up in those heifers."
Moore believes many bred heifers will be calved this spring and sold as pairs.
Another popular marketing option might be to hold those females back for a second breeding. Joplin's latest female sale saw second-alf heifers bring $2,000 to $2,400.
The participating producers of CKPHS have similar plans with the 500 head of remaining spring bred heifers. The surplus heifers will be calved out and rebred to sell next year as three-year-olds.
"There are lots of people who have bred heifers of various quality," Sandusky says. "I think that's part of the problem. There were a ton of heifers that were bred and they weren't all what we would consider cow quality."
Long-term, Sandusky believes the market drop will remove some of the one-time people out of the bred heifer market who jumped in to make a quick profit.
"We hope that reputation gets out there that we're not one-time players. We're not a flash in the pan. We're here for the long haul," Sandusky says of the 12-year program.
Recycled Cattle Cycle
The influx of cheap replacement heifers has created a buyers' market. Creating an opportunity to buy bred heifers at a reduced rate to expand or start a cow herd. It is also a case of the markets working in a cyclical nature.
"Periodically, the market reminds you that what goes up must go down," says Stephen Koontz, economist with Colorado State University Extension.
Superior Livestock's recent December sale was down 21.6% for market-high heifers compared to the April auction. CKPHS had an even more dramatic drop of 25.8% in five months between sales.
"The folks that were buying open heifers and selling them as bred heifers a few months later made really good money when that market was rallying. Now that it's turned the corner it won't be the case," Koontz says. "I think that strategy is out the window for the next couple of years."
Producers are seeing a traditional cattle cycle, says Lance Zimmerman, an analyst with CattleFax.
The highs for the cycle were reached in late 2014 or early 2015. Prices for calves dropped $550 per head from the fall 2014 to fall 2015.
This bred heifer went through the Central Kentucky Premier Heifer Sales program and would have sold when demand for females was at an all time high.Photo by Wyatt Bechtel
"A good rule-of-thumb as an industry is your bred female cost during expansion era peak is likely to be 1.5 to 1.7 times the value of your calves," Zimmerman says. "Producers are just bidding appropriately now with that price depreciation into the value they're willing to pay for bred females."
Profitability for those producers developing heifers primarily depended on the price cattle were bought at prior to breeding.
"The buy side is so incredibly important to the cost of that bred female because that is the biggest cost you're likely going to have in developing that heifer," Zimmerman says.
Prices for a 550 lb. weaned calf at the end of 2015 were around $180/cwt. The cow would then be worth $1,500 to $1,650.
At end of 2014 prices were near $280/cwt. for weaned calves. A bred cow was worth $2,300 to $2,500.
"You're talking about a market that basically gave up $900 per head based on what we're implying with calf values," Zimmerman says.
Despite those losses, Zimmerman expects to see cow herd expansion continue into 2018.
"We're likely to return this beef cow herd back to 31-32 million head as we get to the peak of the expansion," Zimmerman says.
2015 (and Maybe 2016) Better Than 2013
Beef producers don't have to look too far in the rearview mirror to witness a lower market year in 2013.
USDA calculates choice steers averaged $148.12/cwt. in 2015. This year prices are projected to drop to $132.14/cwt. For 2013, the final fat cattle average price was only $125.89/cwt.
"We've gone from this period of record prices for 18 months to a pretty sharp drop in prices in the fourth quarter of 2015. We're kind of getting back to where things used to be," says John Nalivka, president of Sterling Marketing, Inc., Vale, Ore.
Heifer prices two years ago for the Show-Me-Select sale at Joplin were $350 behind 2015, averaging $2,127 in 2013.
When this bred heifer sold through the Central Kentucky Premier Heifer Sales program prices had dropped 25.8% in five months.Photo by Wyatt Bechtel
"It looks to me that we're going to stay above 2013, but 2014 and 2015 are behind us and we're not going back (to those prices)," Nalivka adds.
Cow-calf profitably as calculated by Sterling Marketing was $243.05 per cow in 2013. Profits for 2015 were $429 per cow, $97 less than the year prior. Cow-calf margins are projected to drop in 2016 to $237 per cow.
"The thing I think we can count on is herd expansion will bring more calves this year, increased production and lower prices. It has been a while since we've had that, but that is what's coming," Koontz says.
Koontz believes that downward trend will continue for quite some time until calf prices discourage producers from holding back more replacement heifers.