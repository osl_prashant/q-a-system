Farm Diesel

Farm diesel was unchanged across the board this week.
Two factors must be in play here. 1) Farmer's have already booked farm diesel needs for harvest. 2) The distillate market is looking for direction.
According to EIA, distillate supplies fell 2.6 million barrels last week, now 25.3 million barrels below the year-ago peg.
Heating oil futures are trending downward in a range between $1.73 and $1.77.

 
Propane

Propane is a penny higher at $1.32 per gallon this week.
A rise in Missouri prices contributed to this week's LP price strength as most states were unchanged.
According to EIA, propane supplies fell 0.396 million barrels on the week, now 25.996 million barrels below year-ago.

 





Fuel


9/25/17


10/2/17


Week-over Change


Current Week

Fuel



Farm Diesel


$2.14


$2.16


Unchanged


$2.16

Farm Diesel



LP


$1.28


$1.31


+1 cent


$1.32

LP