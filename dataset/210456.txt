Elk have emerged as the key transmitter of bovine brucellosis in the Greater Yellowstone Area, according to a report the National Academies of Sciences will release this week. The report’s authoring committee, led by Terry F. McElwain, Regents Professor, School for Global Animal Health, Washington State University, will present their findings in a webinar scheduled for Wednesday, May 31, 2017 11:00 am Eastern Daylight Time.
The webinar will include a Q&A session with study committee members, including:
·         Terry F. McElwain, Regents Professor, School for Global Animal Health, Washington State University.
·         Michael B. Coughenour, Senior Research Scientist, Natural Resource Ecology Laboratory, Colorado State University.
·         Paul C. Cross, Research Scientist, US Geological Survey.
·         Dustin P. Oedekoven, State Veterinarian and Executive Secretary, South Dakota Animal Industry Board.
More information and registration for the webinar are available online from the National Academies of Sciences.