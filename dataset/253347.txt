This episode of Packer TV covers likely retaliation against U.S. produce exports as a result of Trump's metal tariffs, as well as Sonic's nationwide launch of its mushroom-meat blended burger. 
MORE: Trump’s new tariffs spark retaliation worries
MORE: Packer Review — Amelia and Ashley test out the new Sonic Slinger