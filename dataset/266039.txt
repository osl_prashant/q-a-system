Thursday’s USDA report could bring some surprises to farmers, traders and the market. Since corn and soybean acres make up the most acres of crops planted, they tend to get the most focus.

At the USDA Outlook Forum in Arlington, Virginia last month, the USDA expected corn and soybean acres to be split at 90 million acres each. However, a lot has changed in a month with drought in the Plains, increased corn exports and sluggish soybean exports.

Its factors like these that could sway a farmer’s decision. According to Matt Bennett, owner of Bennett Consulting, there are some producers that think an increase of up to 3 million acres of soybeans could be possible.

“The trade’s probably looking at maybe a 90.5 to 91 [million acres of] soybeans and maybe 89.5 [million acres] on corn,” he told AgDay host Clinton Griffiths. “If you get above 91, 92 on soybeans, it could be something that would be a bit of a wet towel on the fire right now.”

Looking at the long-term picture, if there are 89 million acres of corn planted, Bennett says this would be friendly. If there are that many soybean acres planted, he says it would be a hindrance, but it would make a rally tough.

There are other crops looking to steal acres from the 180 million acres of corn and soybeans. Some producers are looking at planting spring wheat and cotton.

“A lot of folks in the upper Midwest are saying this makes a bit more sense,” said Bennett. “The bottom line is 180+ [million acres] could happen—I don’t think it’s going to get a whole lot more than that though.”

Watch his full analysis on AgDay above.