Sen. Markey to hold meeting to discuss offshore drilling
Sen. Markey to hold meeting to discuss offshore drilling

The Associated Press

BOSTON




BOSTON (AP) — U.S. Sen. Edward Markey is bringing together members of the fishing industry, tourism, environmental organizations and academia to discuss President Donald Trump's proposal to allow oil and gas drilling in coastal waters.
The Massachusetts Democrat is a member of the Senate Environment and Public Works Committee and chair of the Senate Climate Task Force. He opposes the plan.
Markey says he's planning to discuss the proposal at the meeting he's sponsoring at the New England Aquarium IMAX Theater in Boston at 10 a.m. Monday.
Markey's event comes a day before the federal Bureau of Ocean Energy Management holds a hearing in Boston Tuesday.
Democratic attorneys general from a dozen coastal states — including Massachusetts, Rhode Island and Connecticut — have also written Interior Secretary Ryan Zinke protesting the drilling plan.