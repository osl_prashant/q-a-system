Lawmakers say Trump exploring rejoining Pacific trade talks
Lawmakers say Trump exploring rejoining Pacific trade talks

By KEN THOMAS and KEVIN FREKINGAssociated Press
The Associated Press

WASHINGTON




WASHINGTON (AP) — President Donald Trump has asked trade officials to explore the possibility of the United States rejoining negotiations on the Pacific Rim agreement after he pulled out last year as part of his "America first" agenda.
Farm-state lawmakers said Thursday after a White House meeting with Trump that he had given that assignment to his trade representative, Robert Lighthizer, and his new chief economic adviser, Larry Kudlow. The Trans-Pacific Partnership would open more overseas markets for American farmers.
"I'm sure there are lots of particulars that they'd want to negotiate, but the president multiple times reaffirmed in general to all of us and looked right at Larry Kudlow and said, 'Larry, go get it done,'" said Sen. Ben Sasse, R-Neb.
Eleven countries signed the agreement last month. Trump's rejection of the deal has rattled allies and raised questions at home about whether protectionism will impede U.S. economic growth.
Kansas Sen. Pat Roberts, the chairman of the Senate Agriculture, Nutrition and Forestry Committee, said he was "very impressed" that Trump had assigned Kudlow and Lighthizer "the task to see if we couldn't take another look at TPP. And that certainly would be good news all throughout farm country."
The discussions came during a meeting in which Trump told farm-state governors and lawmakers that he was pressing China to treat the American agriculture industry fairly. Midwest farmers fear becoming caught up in a trade war as Beijing threatens to impose tariffs on soybeans and other U.S. crops, a big blow to Midwestern farmers, many of whom are strong Trump supporters.
Trump has mused about re-joining TPP negotiations in the past but his request to his top aides show a greater level of interest in rejoining the pact he railed against during his 2016 campaign.
During a February news conference with Australian Prime Minister Malcolm Turnbull, Trump raised the possibility of rejoining TPP if the negotiators offered more favorable terms. In a CNBC interview in January, Trump said, "I would do TPP if we were able to make a substantially better deal. The deal was terrible."
The White House meeting was aimed at appealing to the Midwest lawmakers at a time of high anxiety because of the China trade dispute.
During the exchange, Trump suggested the possibility of directing the Environmental Protection Agency to allow year-round sales of renewable fuel with blends of 15 percent ethanol.
The EPA currently bans the 15-percent blend, called E15, during the summer because of concerns that it contributes to smog on hot days. Gasoline typically contains 10 percent ethanol. Farm state lawmakers have pushed for greater sales of the higher ethanol blend to boost demand for the corn-based fuel.
The oil and natural gas industries have pressed Trump to waive some of the requirements in the federal Renewable Fuel Standard law that would ease gasoline and diesel refiners' volume mandates. Farm state lawmakers fear that would reduce demand for the biofuels and violate the RFS law.
North Dakota Gov. Doug Burgum said Trump made some "pretty positive statements" about allowing the year-round use of E-15 ethanol, which could help corn growers.
The administration is also considering the possibility of the federal government aiding farmers harmed by retaliatory tariffs from China, according to lawmakers on Capitol Hill and advocacy groups. But some key senators oppose the approach. "We don't need that. We do not want another subsidy program. What we want is a market," Roberts said during a congressional hearing this week.
The meetings came as an array of business executives and trade groups expressed alarm to federal lawmakers Thursday about the impact that tariffs will have on their business.
Kevin Kennedy, president of a steel fabrication business in Texas, said tariffs on steel and aluminum imports have led U.S. steel producers to raise their prices by 40 percent. He said that's shifting work to competitors outside the U.S. including in Canada and Mexico because they now enjoy a big edge on material costs.
Representatives for chemical manufacturers and soybean farmers also expressed their concerns to the House Ways and Means Committee, which is examining the impact of the tariffs.
The U.S. and China are in the early stages of what could be the biggest trade battle in more than a half century. Trump campaigned on promises to bring down America's massive trade deficit - $566 billion last year - by rewriting trade agreements and cracking down on what he called abusive practices by U.S. trading partners.
Scott Paul, president of the Alliance for American Manufacturing, urged lawmakers and the administration to stay the course in getting tough on China. He said China's theft of intellectual property has inflicted serious damage to U.S. companies and threatens the country's future economic outlook.
__
Associated Press writers Catherine Lucey, Jill Colvin and Matthew Daly in Washington and James MacPherson in Bismarck, North Dakota, contributed.
__
On Twitter follow Ken Thomas at https://twitter.com/KThomasDC and Kevin Freking at https://twitter.com/apkfreking