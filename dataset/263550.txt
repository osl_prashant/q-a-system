BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Thursday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeMar                                12.74  Down .02Apr        12.84   12.87   12.56   12.74  Down .02May                                12.95  Down .02Jun        13.05   13.08   12.80   12.95  Down .02Sep        13.40   13.45   13.17   13.33  unchDec                                14.25  Up   .02Feb        14.29   14.34   14.10   14.25  Up   .02Apr        14.51   14.51   14.32   14.40  Up   .02Jun        14.53   14.56   14.46   14.51  Up   .02Sep        14.86   14.86   14.74   14.77  unchDec                                15.25  unchFeb        15.34   15.34   15.25   15.25  unchApr        15.36   15.36   15.27   15.27  Down .01Jun        15.44   15.44   15.36   15.36  Down .02Sep        15.67   15.67   15.62   15.62  Down .01