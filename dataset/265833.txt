The USDA will release its Prospective Plantings Report on Thursday. Historically this report is a real market mover over speculation about what farmers will plant in 2018.

At the USDA’s outlook forum in Arlington, Virginia, their economists expect to see a 90 million acre split between corn and soybeans.

Pro Farmer’s survey results show there could be record soybean plantings this year at 90.9 million acres. Corn, on the other hand, is estimated at 88.4 million acres.

Michelle Rook talks to farmers about what they think the report will show on AgDay above.