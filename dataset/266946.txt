The average value of farm real estate increased by 47% between 2009 and 2017, according to the 2018 U.S. Baseline Outlook compiled by Food and Agricultural Policy Research Institute (FAPRI) at the University of Missouri.
Average U.S. farmland values are expected to soften over the next decade. Analysis from FAPRI shows the average value of farm real estate to decline slightly—to $3,002—between 2017 and 2021.

What is keeping farmland prices under pressure? Here are four key factors to watch. 

Interest Rates. On March 21, the Federal Reserve raised the key interest rate by 0.25% to 1.75%. This is the highest level it’s been since 2008. In December, Fed officials said they intend to raise rates three times in 2018. The U.S. economy has the potential to expand 2.7% in 2018, according to the Fed, and steeper hikes could be in store for 2019 and 2020. 

	“A rising interest-rate environment is a drag on farm profitability,” says Jackson Takach, Farmer Mac economist. “The more debt you have and the higher the interest rate, the more interest expense you’re going to have to pay. That eats directly into profitability.”
	 
Commodity Prices. “Corn and soybean prices directly affect farmland values,” says Doug Hensley, president of real estate services for Hertz Farm Management. After reaching all-time highs a few years ago, commodity prices remain subdued. 
	 
U.S. Trade Policy. “Exports are a big part of our corn and soybean market,” Hensley says. “Any significant change in our trade markets will have a significant impact on farmland values.” 
	 
Farm Income. Since 2013, income from farming and ranching has fallen by 50%. Meanwhile, farm debt has increased more than 30% in the past decade, according to USDA. 

	“Lenders have become more cautious in the amount of money they will loan for land purchases,” says Randy Dickhut, senior vice president of real estate operations for Farmers National Company. “Lower incomes and reduced cash flows have been a major contributor to the gradual decline in land values the past four years. Projected lower incomes in 2018 will continue the pressure on land values.”

Yet, you can still make a case for a somewhat bullish farmland market. Many regions have been showing relatively strong, or even increasing, farmland values, according to the 2017 fourth-quarter reports from four Federal Reserve banks. See our recent snapshot in LandOwner newsletter.

Hear more farmland market analysis from Hensley:

Want more farmland news? Subscribe to LandOwner Newsletter, a must-have resource for both farmers and landlords.