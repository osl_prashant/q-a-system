CHS Inc., has named Rick Dusek executive vice president for its Country Operations division. The Country Operations unit has a network of more than 60 locally-governed service centers, adding up to more than 400 locations. CHS Retail serves more than 77,000 farmer-owners and customers and employs more than 5,400 people across 16 U.S. states and Canada.

"A strong leader with a diverse background in commodity businesses, Rick brings a depth and breadth of industry knowledge and experience that will help us continue to create value for our owners through our Country Operations retail platform," says CHS President and CEO Jay Debertin. 

Dusek has almost 30 years career experience in the cooperative business. He recently served as vice president, CHS Agronomy. Previously, Dusek served as vice president, grain marketing, where he oversaw North American commodity trading, logistics and transportation and risk management. His team led the strategic expansion of the company's North American grain origination platform, including inland river terminal acquisitions and development and upgrading of export terminal facilities. 

Dusek joined CHS in 1988 and spent his early days on the wheat export trading desk. He was named director of merchandising in 2006 and vice president of grain marketing in 2012. He currently serves on the board of directors for The Fertilizer Institute, and is a past board member of the Minneapolis Grain Exchange. 

Born and raised in Grafton, N.D., Dusek earned a bachelor's of science degree in agricultural economics from North Dakota State University.