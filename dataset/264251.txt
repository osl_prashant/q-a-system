For third-generation Virginia farmer David Hula, challenges are the historical foundation of farming. 
 
“You would think prices would be one challenge and the weather would be one, but we farm the banks of the James River, so water quality is a big challenge,” said Hula. 
 
He says farming along the James River, he's part of the Chesapeake Bay program. In order to reduce runoff, he hasn't touched his soils with tillage equipment for nearly 30 years—a choice that hasn't hampered his yields. 
“2017 we had our best soybean crop ever,” said Hula. 
 
With a farm average of 52 bushels per acre across, he says most of those acres were double-crop soybean acres. 
 
“We did see some 80 and 90 bushel single crop beans,” he said. 
 
In 2017, corn yields struggled overall on his Virginia farm fields, he had one field break the prized 500 bushels per acre mark and setting a new world record corn yield. 
 
“We have figured there are three good successes for high yielding corn,” said Hula. “The good Lord’s got to bless you; two, you have to use Pioneer Hybrids; and three, you have to use John Deere paint,” said Hula. “With those three things, you're on the way to success.”
 
His crop plan for 2018 is split, but based on his rotation, he’s growing half of his acres in corn.
 
“I just hate chasing the market because usually I chase the wrong way,” said Hula. 
 
He doesn't chase crop prices or tractor deals, instead, sticking to an annual rotation with his equipment. 
 
“We run our combines two years before we trade them in, and we'll run the larger tractors two years,” said Hula. 
 
Machinery Pete says it's the interest in tractors that's caught the market by surprise this year. 
 
“Auction prices on good condition used tractors, whatever the size, have been just kind of shockingly strong,” said Greg Peterson, host of Machinery Pete TV. 
 
Sales are strong, and he says it's still condition that's key. 
 
“If it's in nice shape, there's definitely a premium being paid out there, so feels like people are kind of catching up a little bit,” said Machinery Pete. 
 
He says it's an uptick that started the first week of November and hasn't slowed down yet. 
 
“This market has now almost beyond solidified a little bit,” he said. 
 
For equipment dealer James River Equipment, and their stores across Virginia and North and South Carolina, sales of the big equipment are still lagging. 
 
“Our used inventory level is still pretty high, a lot higher than I would like for it to be,” said Matthew Fleet, equipment marketing manager, James River Equipment. 
 
While large ag is suffering, he says it's small ag that continues to shine. 
 
“In James River's market, we have one of the largest compact utility markets in the United States,” said Fleet. 
 
It’s the 20 to 100 horsepower market currently in high demand for the dealership. 
 
“You never know what the next customer is going to walk in and what their use or application is for a tractor,” said Fleet. 
 
It's also planters also gaining strength on the East Coast. 
 
“Finally this spring we've seen sort of back to normal inventory levels,” said Fleet. “We've sold a lot of our aged inventory, and there's quite a lot of demand for the used planters.” 
 
Machinery Pete says nationwide, it's also planters posting impressive prices and sales. 
 
“Planters have been the one category that's the highest jump we've seen, with a 106 percent increase in December versus third quarter and that's held early in 2018,” said Machinery Pete. 
 
With newer planters, comes new technology, but fleet says it's all about finding the right fit for your farm. 
 
“It also comes back to who's in the driver’s seat,” said Fleet. “Who's holding the steering wheel. They have to be comfortable with the technology and the level of driver input it takes to understand the planter and feel comfortable.”
 
For Hula, technology drives most of his equipment purchasing decision. 
 
“We do like to keep up with the technologies,” said Hula. “Just like seed, they change new hybrids every year or every so often, well so do the equipment manufacturers.”
 
He says the planter is the most beneficial tool on his farm, and that's where the data collecting begins. 
 
“We look to see where did we get the best singulation; where did we have the right down pressure,” said Hula. “Then we start correlating that with emergence, then we correlate that with our sprayer to make variable rate applications.”
 
As planting 2018 quickly approaches, Hula says he's ready for when mother nature says it's go-time. 
 
“We hope we get a good spring like we had in 2017, where we have decent moisture, good temperatures, and we do all the things that we can do,” said Hula. “I'm like every other grower, I want to maximize my yields.”
Yields that continue to climb, proving the yield ceiling may be higher than most farmers think.