Agriculture Secretary Sonny Perdue has named Carmen Rottenberg as Acting Deputy Under Secretary for Food Safety and Paul Kiecker as Acting Administrator for the Food Safety and Inspection Service. 
“Ensuring the safety of our nation’s food supply is our most important responsibility, and it’s one we undertake with great seriousness,” Perdue said in a news release. “Both Carmen and Paul have dedicated their careers to the mission of food safety and I am pleased to have appointed them to these important roles within the USDA.”
 
The two will serve until presidential nominees are confirmed by the Senate for those roles, according to the release.

 
The appointments come after Al Almanza, Acting Deputy Under Secretary for Food Safety and FSIS Administrator, retired after 39 years with the agency.