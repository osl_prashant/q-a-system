Powerful earthquake rattles remote Papua New Guinea
Powerful earthquake rattles remote Papua New Guinea

By NICK PERRYAssociated Press
The Associated Press

WELLINGTON, New Zealand




WELLINGTON, New Zealand (AP) — A powerful earthquake rattled forest villages and a large gold mine in central Papua New Guinea on Monday, and the government sent officers to the region to assess unverified reports of fatalities and check the extent of the damage.
Officials said that as of Monday afternoon there were no confirmed deaths or injuries in the Pacific Island nation.
Chris McKee, acting director of geohazards management for the Papua New Guinea government, said tens of thousands of people live subsistence lifestyles in the remote forested highlands region affected by the quake.
"There seem to be quite a few reports of the quake being felt strongly," he said.
The magnitude 7.5 quake hit about 89 kilometers (55 miles) southwest of Porgera, the site of a large gold mine that employs more than 2,500 residents. The area also is home to a number of oil and gas operations and coffee plantations.
ExxonMobil Papua New Guinea said it had shut down the Hides Gas Conditioning Plant, where there was damage to the administration buildings, living quarters and mess hall.
The company said it had also suspended flights from Komo airfield, which it built to service its operations, and was planning to evacuate nonessential plant staff.
Oil Search Managing Director Peter Botten said the company was closing down some production operations in the region as a precaution.
Both Oil Search and ExxonMobil said there were no reports of any injuries to their staff, and ExxonMobil said its staff was accounted for and safe.
An official at the Porgera gold mine wrote on Facebook that he planned to find out what caused power blackouts and other damage after the quake shook the whole area.
Indonesia's Disaster Mitigation Agency said the quake caused panic and damaged buildings in the Boven Diguel area of Papua province, which borders Papua New Guinea. Information was still limited because of the remoteness of the area and its lack of disaster response personnel, the agency said, adding that it didn't yet know if there were any fatalities.
A mosque, military post, district office and a house were damaged in the border area, and photos of the Boven Diguel district office, provided by the agency, showed a heavily damaged roof and interior.
The quake hit at a relatively shallow depth of 35 kilometers (22 miles), and shallow quakes tend to be more strongly felt than deep ones.
The area lies along an earthquake zone known as the Papuan Fold Belt, which is the fault responsible for the mountain range that forms the spine of the nation, McKee said.
A series of strong aftershocks had rattled other parts of the fault line.
McKee said the quake was caused by one side of the fault moving over the other side, squeezing the ground together and causing a thrust process.
The Geological Survey website had dozens of reports of people feeling the quake, including some saying the shaking was violent.
Papua New Guinea is located on the eastern half of the island of New Guinea, to the east of Indonesia. It is home to about 7 million people.
___
Associated Press writer Stephen Wright in Jakarta, Indonesia, contributed to this report