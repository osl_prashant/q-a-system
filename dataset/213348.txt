Good morning!
Corn favors upside overnight, for a change... For the first time in a week, corn futures favored the upside overnight and most contracts are around a penny higher as of 6:30 a.m. CT. Soybean futures are also higher at the close of the overnight session with most contracts up 1 to 2 cents. Winter wheat futures are steady to a penny higher in most contracts, while spring wheat futures are up 1 to 2 cents. The U.S. dollar index is down slightly, with crude oil futures also facing pressure.
Export sales report expectations... 



Commodity

2017-18
			(MT)



Corn
800,000 to 1,100,000


Wheat
250,000 to 450,000


Soybeans
1,300,000 to 1,700,000


Soymeal
100,000 to 250,000


Soyoil
5,000 to 22,000



Bloomberg: Trump orders EPA to sustain current biofuel levels... The Trump administration will back off plans to reduce the volume of biofuels in the nation's motor fuel supply, according to Bloomberg News, amid growing pressure from corn and soybean producing states that stand to lose jobs and revenue from the proposal. After President Donald Trump and Environmental Protection Agency Administrator Scott Pruitt took a call from Iowa Governor Kim Reynolds, a Republican, the White House instructed the agency not to lower the amount of biofuel mandated under the agency's annually revised Renewable Fuel Standard (RFS), according to Bloomberg, which cited "people familiar with the decision." That RFS decision is due from the agency no later than Nov. 30. Senators Chuck Grassley, Joni Ernst (R-Iowa) and several other agricultural state lawmakers also met with EPA Administration Scott Pruitt over the biofuel volumes on Tuesday. Get more details.
Attache weighs in on Brazil's corn and wheat crops... USDA's ag attache in Brazil estimates its 2016-17 corn crop at a record 97.7 MMT, a 46% surge from the year prior but under USDA's official crop peg of 98.5 MMT. The post goes on to say it expects record production to lead to record exports, forecasting them at 35 MMT, more than double the previous year's shipments. Again, this was shy of USDA's official corn export forecast for Brazil that stands at 36 MMT. Looking ahead to 2017-18, the attache pegs the crop at 95 MMT (in line with USDA) with exports expected to total 35 MMT, up 1 MMT from USDA's October estimate. Also of note, the post expects the 2017-18 wheat crop to total just 5.15 MMT, down 23% from year-ago due to "dry conditions and a mid-season frost."
Chinese GDP reflects solid growth... China's economy expanded 6.8% from year-ago for the third quarter, with gross domestic product still running well ahead of the government's target for annual growth of 6.5%. This was in line with expectations for a 0.1-point decline from the second quarter. For the first three quarters of the year, GDP is up 6.9% from year-ago.
Stabenow presses farm bill resolution via FY 2018 budget move... Senate Republicans want to clear a fiscal 2018 budget today that among other things will up the odds of a tax reform/cut bill ahead. Despite expectations that the final budget resolution will not contain mandated cuts for agriculture spending (current House version does, but Senate does not), Senator Debbie Stabenow (D-Mich.) wants to propose an amendment to the budget resolution that would create a point of order against any legislation that reduces agriculture spending. But there could be procedural hurdles for any such amendment. The Senate is expected to adopt its budget late tonight after a marathon session of back-to-back votes on amendments.
USDA nominees set for ag panel approval... The Senate Agriculture Committee will meet today to approve the nominations of Bill Northey to be undersecretary of farm and conservation programs and Greg Ibach to become undersecretary for marketing and regulatory programs. If so, the Senate could approve the two nominees as soon as today.
Farm Policy Facts disputes ERS prediction on farm income rise... A new Farm Policy Facts report argues that the Economic Research Service's August prediction that farm income would rise by 3% in 2017 isn't telling the whole story. The group said the 2017 bounce predicted by ERS is indeed a "positive sign," but really the focus should be on USDA's adjusted net farm income figures for 2016, which came out to $61.5 billion. That number, Farm Policy Facts said, shows net farm income dropped 50% over three years.
Chinese hog production down marginally from year-ago for the first nine months of the year... Between January and September, China's pig herd stood at 427.97 million head, down 0.8% from the same period last year, according to its National Bureau of Statistics. Government efforts to crack down on pollution have hit its hog industry hard. Pork output climbed 0.7% to 37.17 MMT over this period.
Expectations building for a bit of a setback for the cash cattle market... Fearing the cash cattle market may be due for a setback soon, traders have been hesitant to push live cattle futures much ahead of last week's $110 to $111 action. It appears such ideas may be correct, as yesterday's online Fed Cattle Exchange Auction featured a few sales at $109 and a few hundred head traded for $108 in the Iowa/Minnesota market and around $109 in Kansas. Around a thousand head traded in the $110 area in both Nebraska and Texas.
Momentum favors market bulls in lean hog futures... Lean hog futures settled high-range yesterday, giving market bulls the near-term advantage. Also encouraging, cash hog prices climbed once again on Wednesday and December futures hold just a $2.75 premium to the cash hog index, which is not overdone. While the pork cutout value fell yesterday, movement was solid at 332.65 loads.
Overnight demand news... Jordan made no purchase in its tender to buy 100,000 MT of milling wheat. Egypt tendered to buy an unspecified amount of wheat from global suppliers. Japan bought a total of 64,658 MT of food-quality wheat from the U.S., as well as 34,928 MT from Canada and 24,140 MT from Australia. An Israeli group of private buyers purchased 10,000 MT of feed wheat from optional origins (likely from the Black Sea region), but it passed on its tender to buy 10,000 MT of barley.
Today's reports:

7:30 a.m., Weekly Export Sales-- FAS
7:30 a.m., Drought Monitor -- USDA/NWS
7:30 a.m., Extended Weather Outlook -- NWS
2:00 p.m., Livestock Slaughter -- NASS