.style16 {font-size: larger;
	font-weight: bold;
}

Click here for related charts.

CORN COMMENTS
The national average corn basis improved 9 cents from last week to 29 1/2 cents below December futures. The national average cash corn price firmed 6 3/4 cents to $3.16 1/2.
Basis is weaker than the three-year average, which 24 cents below futures for this week.
 
SOYBEAN COMMENTS
The national average soybean basis improved 9 1/2 cents from last week to stand 39 cents below November futures. The national average cash price firmed 16 1/2 cents from last week to $9.26 1/4.
Basis is well below the three-year average, which is 16 cents below futures for this week.
 
WHEAT COMMENTS
The national average soft red winter (SRW) wheat basis firmed 3 3/4 cents from last week to 11 3/4 cent below December futures. The average cash price softened 5 cents from last week to $4.21 1/2. The national average hard red winter (HRW) wheat basis firmed 2 1/2 cents from last week to 81 1/4 cents below December futures. The average cash price dropped 5 1/4 cents from last week to $3.47.
SRW basis is firmer than the three-year average of 47 cents under futures, while HRW basis is weaker than the three-year average, which is 70 cents under futures for this week.
 
CATTLE COMMENTS
Cash cattle trade averaged $109.45 last week, which is up $1.44 from the previous week. Expectations are for $1 to $2 higher cash cattle trade this week. Last year at this time, the cash price was $102.65.
Choice boxed beef prices dropped $1.09 from last week at $196.32. Last year at this time, Choice boxed beef prices were $181.50.
 
HOG COMMENTS
The average lean hog carcass bid improved $3.35 over the past week to $58.32. Last year's lean carcass price on this date was $52.57.
The pork cutout value firmed $1.10 from last week to $73.00.  Last year at this time, the average cutout price stood at $72.14.