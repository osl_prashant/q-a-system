Farming in North Dakota looks a lot different than it did a decade ago, when wheat was king.Today, "it's mainly small grain, corn and soybeans," says David Barnick, a farmer near Jamestown, N.D. "Some of the newer genetics with corn and soybeans have really allowed them to flourish up here in North Dakota."
Living so far north also means farmers play a gambling game with Mother Nature each year. So, when farmers get the green light to go, all systems must be ready. "It's critical that when it's time to go, they are able to run nonstop, and they have the people to do it, so we need the machinery to be on spot, everything has to work like it's supposed to," says Tom Joyce, CEO of Green Iron Equipment.
Having the equipment to run efficiently and quickly is one reason Barnick decided to upgrade to a larger planter last year. With 32 rows, he's able to cover more ground, quicker. "We sure like the way it works with seed placement," says Barnick. "I'm saving money on my soybean seed versus an air drill."
Atlocal equipment dealerGreen Iron Equipment, they are seeing more interest in farmers trying to upgrade their fleet to used equipment.
"There's land being swapped around andfolks are picking up a little bit more land, so they're looking for an extra row crop tractor maybe another used planter to cover more acres," says Nick Schuetz, store manager, Green Iron Equipment.
Micheal Sandness farms 8 miles north of the dealership, and even as a young farmer, his strategy is finding the opportunities to be more efficient, rather than cutting back.
"I think the more important part is getting to know your ground, your soil health," he explains. "Soil science is something I've been really interested in, and I'mgetting into variable rate technology. I think you need to find equipment that's going to help you be more efficient on the farm."
He says the technology is helping him cut back on unnecessary spending and allowing him to save money in the long run.In fact, he runshis entire farm from one tiny device.
"I have everything on my iPad; my whole farm pretty much," says Sandness. "So, everything from my balance sheet, income and expenses, my maps for variable rate technology, and I'm constantly utilizing those tools to be more efficient with my ground and my money."


<!--

    function delvePlayerCallback(playerId, eventName, data) {
        var id = "limelight_player_351368";
        if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
            DelvePlayer.registerPlayer(id);
        }

        switch (eventName) {
            case 'onPlayerLoad':
                var ad_url = 'http://oasc14008.247realmedia.com/RealMedia/ads/adstream_sx.ads/agweb.com/multimedia/prerolls/agwebradio/@x30';
                var encoded_ad_url = encodeURIComponent(ad_url);
                var encoded_ad_call = 'url='   encoded_ad_url;
                DelvePlayer.doSetAd('preroll', 'Vast', encoded_ad_call);
                break;
        }
    }

//-->


<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->

Want more video news? Watch it on U.S. Farm Report.

He's not the only one spending money on farm technology."I think there's a lot of investment going to be made on the technology piece, as customers work on getting more efficient," says Joyce.
Green Iron Equipment is already seeing that investment start to blossom,but the biggest surprise this year has been how steady equipment values are holding."It's been really busy this spring, which surprisingly, we weren't expecting that with the current market prices," says Joyce.
Greg "Machinery Pete" Peterson agrees. "Generally, used values are higher than we thought," he says.
Click here to search 7500 combines for sale on MachineryPete.com
The areaof interest is another surprise, with the larger equipment like older,high horsepower 4WD models and newer model mower tractors proving popular with customers.
That's a change from just a few years ago."In the past, wehad a glut of used combines, so I think that also negatively impacted the pricing for a couple years," Joyce says.
Peterson agrees. Two years ago, he recalls, combine values fell 25% in some cases. "Supply and demand is simple (and)it impacts values," he says.
So, if you're in the market to buy, as the calendar flips to spring, now could be the time.
"Late spring to early summer, historically, has been the best time of year to be a buyer before harvest, and I think, again, there's still inventory out there," Peterson says. "There's going to be a lot of sales, there's still inventory on dealer lots, and there's a lot of opportunity to get a lot of value from the 4- to 5-year old equipment."
But these prices aren't permanent, and Petersonsays the gift of higher soybean prices could edge equipment values higher.
"When I've seen the markets go up over the years, it's almost like a spigot that gets turned on immediately, and used values go up and new values go up," Peterson says. "If you can beat the spigot,that's the opportunity right now, I think."
Despite a surprise rally this spring, the credit crunch across rural America is still in full effect, impacting buyers.
"We're seeing an increase in financing," says Schuetz. "A lot of folks are interested in low interest rates, one of competitive advantages is we can offer some lower interest rates."
While farmers are keeping an eye out for deals in buying equipment, they're keeping a closer eye on the volatility in today's markets. And conquering those challenges is what makes the game of farming even more rewarding. "If farming was easy, it wouldn't be farming," says Sandness.