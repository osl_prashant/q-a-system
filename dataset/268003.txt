Cindy Hyde-Smith sworn in as 1st female Mississippi senator
Cindy Hyde-Smith sworn in as 1st female Mississippi senator

By LISA MASCARO and EMILY WAGSTER PETTUS
The Associated Press

WASHINGTON




WASHINGTON (AP) — Republican Cindy Hyde-Smith made history Monday when she was sworn in as the first woman to represent Mississippi in the U.S. Senate, a milestone that strategists hope will propel the former agricultural commissioner and early President Donald Trump supporter when she faces a tough election in fall.
Vice President Mike Pence arrived at the Capitol to swear her in as colleagues looked on. Hyde-Smith was tapped by the state's Gov. Phil Bryant as a surprise pick to fill the seat for GOP Sen. Thad Cochran, 80, who resigned April 1 amid health concerns.
"I know I speak for all senators on both sides of the aisle in welcoming our new colleague," said Senate Majority Leader Mitch McConnell, R-Ky. Fellow Mississippi GOP Sen. Roger Wicker and others joined in the chamber.
In a twitter message, Hyde-Smith said she was "grateful and honored" as she started the day, "pretty historic in my life, for sure, and for the state of Mississippi."
Hyde-Smith emerged as an unlikely choice to replace Cochran after Bryant declined suggestions from Trump and other GOP leaders to appoint himself. But in the 58-year-old who runs a family cattle ranch, the governor saw a history-making appointment of a Republican who had twice won statewide office and served 12 years in the state Senate, though she previously had been a Democrat before switching to the GOP in 2010.
Republican strategists are now warily watching the Mississippi race. Republicans should have a lock on the state Trump easily won, where Hyde-Smith co-chaired his Agriculture Policy Advisory Council during the election.
But she faces several challengers in the wide-open Nov. 6 special election to serve the remainder of Cochran's term, through January 2021.
Among them is Republican Chris McDaniel, a tea-party conservative state senator who almost toppled Cochran in a primary four years ago, and Mike Espy, a Democrat who in 1986 became the first African-American since Reconstruction to win a congressional seat in Mississippi and in 1993 became President Bill Clinton's first agriculture secretary. The Democratic mayor of Tupelo, Jason Shelton, is also running, and the deadline for candidates to file for the race is later this month.
Hyde-Smith is keeping most staff members who worked for Cochran, said Brad White, who will stay on as chief of staff. She will serve on the same committees as Cochran — Appropriations, Agriculture and Rules — but without leadership roles. Cochran had been chairman of the powerful Appropriations Committee.
White said Hyde-Smith will keep the three offices Cochran had in Mississippi, in Jackson, Oxford and Gulfport.
___
Follow Mascaro on Twitter at https://twitter.com/LisaMascaro and Wagster at https://twitter.com/ewagsterpettus