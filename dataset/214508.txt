The October reading of the Purdue/CME Group Ag Economy Barometer is showing increased farmer sentiment than in September, but not by much. Sentiment ticked three points higher to 135 in October.

The barometer has been hovering in the 130 to 139 range for several months after hitting its peak of 153 in January 2017.

Of the farmers surveyed, a majority aren’t expecting to see an improvement in grain prices in the year ahead, and a growing number are planning to adjust their production practices in 2018. According to the barometer, 18 percent of farmers plan to adjust their seeding rates, down one point from 2017. 33 percent plan on lowering their fertilizer rates, down 13 points from 2017.

“Part of that difference is probably attributed to the fact that fertilizer prices have come down in the intervening 12 months,” said Jim Mintert, an economist at Purdue University. “Anhydrous ammonia price is a primary input, especially for corn production in the Corn Belt, are down about 20 or 25 percent compared to where they were this time last year.”

Compared to their expenses in 2017, 62 percent of producers don’t expect their expenses will change in 2018.