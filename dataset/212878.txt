The Tyler Phipps Memorial Scholarship Foundation chose Alberto Diaz as a recipient of the 2017 Produce Marketing Association Executive Leadership Conference Scholarship.  
Diaz is the managing director of Spring Valley Fruits, McAllen, Texas.
 
The scholarship covers the registration for the Produce Marketing Association’s Center for Growing Talent Executive Leadership Symposium May 16-17 in Dallas, according to a news release.
 
“It is an invaluable opportunity to acquire knowledge related to the strategy, culture and talent of successful organizations based on their experience,” Diaz said in the release.
 
The Executive Leadership Symposium is an industry-specific conference that offers top executives an opportunity to connect with others in the industry.
 
This is the second year the Tyler Phipps Memorial Foundation has offered this scholarship.
 
Diaz has been in the produce industry for more than 10 years, according to the release. 
 
Phipps was the senior director of sourcing for Market Fresh Produce LLC when he died in 2015 at age 30.