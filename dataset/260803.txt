BC-Cotton
BC-Cotton

The Associated Press

NEW YORK




NEW YORK (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Tuesday:
Open  High  Low  Settle   Chg.COTTON 2                                50,000 lbs.; cents per lb.                May      83.19  84.21  82.04  82.98   —.30Jul      83.21  84.08  82.21  82.86   —.35Sep                           78.49   —.23Oct                           79.57   —.31Nov                           78.49   —.23Dec      78.60  78.94  77.95  78.49   —.23Jan                           78.67   —.16Mar      78.85  79.02  78.13  78.67   —.16May      78.70  78.94  78.13  78.62   —.08Jul      78.60  78.65  78.06  78.42   +.04Sep                           73.37   +.23Oct                           75.82   +.05Nov                           73.37   +.23Dec      73.36  73.50  73.01  73.37   +.23Jan                           73.62   +.20Mar                           73.62   +.20May                           74.32   +.19Jul                           74.43   +.13Sep                           72.89   —.07Oct                           74.02   +.10Nov                           72.89   —.07Dec                           72.89   —.07Est. sales 37,513.  Mon.'s sales 27,618 Mon.'s open int 270,947,  up 439