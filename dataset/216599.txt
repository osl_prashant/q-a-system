Consumers are more likely to recommend Aldi than traditional supermarkets, according to a study by the Retail Feedback Group.
Aldi scored a 4.54 on the 5-point scale, with supermarkets rated a 3.66. For overall satisfaction, supermarkets eked out a win with a 4.40, compared to 4.28 for Aldi.
Some of the disparity in the likelihood-to-recommend category could be due to the news-making status of Aldi this year.
Leading up to the U.S. market entry of Lidl, the longtime hard discounter competitor to Aldi in Europe, Aldi announced a $1.6 billion investment to remodel and expand 1,300 U.S. stores by 2020. Only a few months later, Aldi committed another $3.4 billion to expand to 2,500 stores by the end of 2022.
With so many stores getting makeovers and new locations popping up, it makes sense that the newness factor could be spurring recommendations.
Aldi also appears to be gaining in another area, with 65% of consumers planning to shop there the same amount over the next year and 33% planning to shop there more. In comparison, 21% of consumers said they planned to shop at supermarkets more.
Aldi received ratings lower than supermarkets for quality and freshness but still fairly favorable — 4.32 compared to 4.45.
The discounter was a close second in cleanliness — 4.39 versus 4.40 — but fell shorter in the areas of selection (4.02 to 4.38) and staff knowledge (3.99 to 4.24).
Where Aldi reigned, however, was in the key category of value for money. In that area, Aldi received a score of 4.68, by a significant margin its highest in any category. Supermarkets scored a 4.18 in the value category.
RFG cautioned supermarkets to be aware of the areas where Aldi is outperforming them.
“Supermarkets continue to maintain the lead in quality and freshness, cleanliness and item variety/selection, but Aldi has stronger marks than supermarkets in value for money spent and checkout speed,” RFG said in its report.
“These vulnerabilities should be examined closely with an eye to improvement.”