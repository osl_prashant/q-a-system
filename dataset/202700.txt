(UPDATED March 1) Strong avocado market conditions in February are likely to persist through the summer amid reports of lower than expected volume from Mexico and reduced volume of California fruit.
 
"We anticipate strong market conditions because of the short California crop and our reduced volume to the U.S." said Ramon Paz-Vega, strategic advisor for the Avocado Producers and Exporting Packers Association of Mexico (APEAM), in an e-mail. 
 
While labor unrest last year resulted in economic losses of more than $100 million to avocado producers in Michoacan, the latest market spike is unrelated to those issues, marketers said. 
 
"We do not foresee those types of issues going forward," Paz-Vega said in the e-mail. "Our industry has been working under very stable conditions."
 
Strong tone
 
Current market conditions for hass avocados reflected higher prices and lower volumes compared with the same time a year ago.
 
The U.S. Department of Agriculture reported that the average f.o.b. price for Mexican avocados crossing through Texas the week ending Feb. 25 was $47.25 per carton, up from $31 per carton on Feb. 11 and more than double the f.o.b. of $22.83 per carton in late February a year ago.
 
Shipments of Mexican avocados through south Texas in February this year totaled about 135.1 million pounds, compared with 166.25 million pounds shipped in February last year, according to the USDA.
 
The Hass Avocado Board projects that U.S. volume of hass avocados from all sourced in March will total about 187 million pounds, down from about 199.3 million pounds in March 2016.
 
"Apparently the crop in Mexico is a little bit lower than originally anticipated, and the prices have been very strong," said Phil Henry, president of Escondido, Calif-based Henry Avocado Corp. "We expect reasonable volume coming in from Mexico, although not at the levels we were seeing last year." 
 
F.o.b. prices for size 60s and 48s Mexican hass were in the mid-$40 range on Feb. 28.
 
Mexico's hass crop hasn't been picking to estimates, said Patrick Lucy, vice president of organic sales for Fallbrook, Calif.-based Del Rey Avocado Co.
 
Paz-Vega of APEAM said in an e-mail that yields were lower this season but Mexico has more acreage registered in the U.S. export program. "Total crop estimate from orchards in this program for the 2016-17 season is around 2.2 billion pounds, out of which we estimate to ship to the U.S. around 1.7 billion pounds," he said the e-mail.
 
Paz-Vega said Mexico expects to ship about 30 million to 35 million pounds per week as an average for the remaining of the season, with end of season volumes decreasing in June.
 
Lucy said some believe Mexico's crop may only reach about 1.6 billion pounds, and possibly less than that.
 
"They are running out of fruit fast down there," he said. Mexico was harvesting about 30 million pounds per week compared with normal volumes in the 40 million pound per week range, Lucy said. 
 
Expected volume for the next six to eight weeks will range from 27 million to 32 million pounds per week, Lucy said, and then start to fall off. 
 
Warm and dry conditions won't allow growers to keep fruit on the tree as long as they would like, Lucy said.
 
California
 
Rain in Southern California growing regions has held back harvest activity there, though the state's volume is light compared with Mexico - about 4% of total supply in late February, according to numbers from the Hass Avocado Board.
 
California hass supply will increase in response to market conditions but a limiting factor is sparse supply of preferred sizes, Henry said.
 
Lucy said the California crop is about half of what it was a year ago. This year's California volume anticipated close to 190 million to 200 million pounds, compared with close to 400 million pounds in 2016, he said. Larger avocado crops are often followed by smaller crops the next year, and industry sources believe that the alternate bearing cycle played a role in this year's smaller crop.
 
California volume could accelerate by mid-March and into April, but Lucy said volume may reach 8 million to 10 million pounds per week compared with volumes as high as 15 million pounds per week last year.
 
Organic hass prices are in the $50-plus range and could reach the $60 per carton range, Lucy said. 
 
Conventional hass prices could be firm for the next seven to eight months, Lucy said. "I don't think (the f.o.b. market) will dip below $40 per carton at least until September or October."