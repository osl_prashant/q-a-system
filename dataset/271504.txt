BC-US--Cotton, US
BC-US--Cotton, US

The Associated Press

New York




New York (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Monday:
(50,000 lbs.; cents per lb.)
COTTON NO.2Open    High    Low    Settle     Chg.May       85.70   86.44   82.90   83.19  Down 2.28Jul       84.89   85.39   84.04   84.12  Down  .61Aug                               79.35  Down  .08Oct       81.45   82.00   81.16   81.16  Down  .51Oct                               79.35  Down  .08Dec       79.45   79.90   79.25   79.35  Down  .08Dec                               79.21  Down  .03Mar       79.31   79.74   79.15   79.21  Down  .03May       79.09   79.75   79.09   79.14  Up    .05Jul       78.78   79.49   78.78   78.88  Up    .17Aug                               73.82  Down  .16Oct                               76.12  Up    .20Oct                               73.82  Down  .16Dec       74.10   74.20   73.82   73.82  Down  .16Dec                               73.87  Down  .16Mar                               73.87  Down  .16May                               74.29  Down  .16Jul                               74.23  Down  .16Aug                               72.34  Down  .16Oct                               73.59  Down  .16Oct                               72.34  Down  .16Dec                               72.34  Down  .16Dec                               72.38  Down  .16Mar                               72.38  Down  .16