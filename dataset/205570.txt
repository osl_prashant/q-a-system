The top headlines on AgProfessional.com in 2016 highlighted the industry's consolidations and mergers. Here are the top 10 news stories from 2016:Retailers Focus on Customer Service Amid Change
ChemChina Moving Closer To Potential Syngenta Deal
Monsanto And BASF May Reap More From Partnership After Syngenta Sale
Bayer CEO can bide time on agri-business spinoff
DuPont Doubles Down On Cost Cuts Ahead Of Dow Chemical Merger
DuPont To Cut 1,700 Delaware-Based Jobs Ahead Of Dow Merger
The 4 Trump Policies With Biggest Implications for Ag Industry
Farmland Partners Continues Buying Thousands of Farmland Acres
Court Rules EPA Cannot Vacate Enlist Duo Label; May Review It
Why Agriculture is Consolidating