Produce distributors in Nogales, Ariz., seem wary of President Donald Trump's plans to meddle with the North American Free Trade Agreement that they say has boosted trade among the U.S., Mexico and Canada over the past two decades.
But at the same time, some see an opportunity to enhance the pact.
In late January, the Nogales-based Fresh Produce Association of the Americas, along with dozens of other trade groups and food- and agriculture-related businesses, signed a letter to the president in which they expressed a desire to work with the Trump administration "on ways to modernize the North American Free Trade Agreement in ways that preserve and expand upon the gains achieved."
"In the 20 years since NAFTA was implemented, the U.S. food and agriculture industry has become increasingly efficient and innovative," the letter said.
"(I)ncreased market access under NAFTA has been a windfall for U.S. farmers, ranchers and food processors," it continued.
For the most part, North American food and agriculture trade now is free of tariffs, the letter said. "However, barriers still exist for U.S. exporters, and we look forward to working with your administration on reducing the non-tariff trade barriers that continue to inhibit our exports to the North American marketplace, as well as to addressing the remaining tariffs impeding access for some U.S. export sectors."
It's "inevitable" that the current NAFTA agreement will be tweaked, said Ricardo Crisantes, general manager at Wholesum Family Farms in Nogales. But he doesn't think that's necessarily a bad idea, as long as the U.S. doesn't view the deal as a "we win, you lose" situation.
An attitude like that, he said, "could lead to bad outcomes."
"The best deals are those in which both parties get something," he said. "We need an agreement where we can both walk away feeling good about it."
Jerry Havel, director of sales and marketing for Fresh Farms in Rio Rico, Ariz., would like to keep NAFTA just the way it is.
"We like NAFTA," he said. "For us and for our industry, we believe that it's been a good thing."
NAFTA has sparked "incredible growth for all three countries involved," said Chris Ciruli, partner at Ciruli Bros. LLC, Rio Rico.
If the Trump administration feels it needs to be renegotiated, "we have a lot of confidence in our representation from Arizona from both the Senate and the House level that they will negotiate something that is very, very business friendly," he said.
"I think, as we go forward, the president is going to re-evaluate some of the situations with Mexico and see they are a great trading partner for NAFTA," Ciruli said.
Some might be overly concerned about the president's rhetoric, said Chuck Thomas, owner and president of Thomas Produce Sales Inc., Nogales.
"I don't think President Trump can do as much as he thinks he can by himself," he said. "The courts and the legislature have to go along with it, too."
It's too early to tell what might happen, he said, but he's hopeful that cool heads will prevail, since Mexico is the second-largest trading partner of the U.S.
A misstep would "throw a hammer in that deal and affect a lot of U.S. manufacturers and growers who are shipping into Mexico," Thomas said.
"It's going to upset a lot of people if he (imposes) restrictions on NAFTA, and Mexico in turn puts their own restrictions on product from the U.S," he said. "It works both ways."
Many comprises already were made when the agreement originally was negotiated, he said.
Thomas doesn't see a point in starting those negotiations over again.
"If it ain't broke, why fix it?" he asked.
Any renegotiation of NAFTA would be a long-term process, said Fried DeSchouwer, president of Greenhouse Produce Co. LLC, Vero Beach, Fla.
"I believe common sense will prevail, and the rhetoric will eventually calm down as bigger and more pressing issues will take priority, such as maintaining world peace," he said.