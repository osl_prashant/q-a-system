Click here for related charts.

CORN COMMENTS
The national average corn basis improved 2 cents from last week to 20 3/4 cents below September futures. The national average cash corn price softened 5 3/4 cents to $3.44 1/4.
Basis is much weaker than the three-year average, which 2 3/4 cents above futures for this week.
 
SOYBEAN COMMENTS
The national average soybean basis declined 11 1/2 cents on the switch in contract months from last week to stand 24 1/2 cents below November futures. The national average cash price softened 22 3/4 cents from last week to $9.53.
Basis is well below the three-year average, which is 42 1/2 cents above futures for this week.
 
WHEAT COMMENTS
The national average soft red winter (SRW) wheat basis firmed 3/4 cent from last week to 5 1/4 cents below September futures. The average cash price softened 16 1/4 cents from last week to $4.55 1/2. The national average hard red winter (HRW) wheat basis softened 2 1/2 cent from last week to 73 1/2 cents below September futures. The average cash price declined 13 1.2 cents from last week to $3.91.
SRW basis is firmer than the three-year average of 21 cents under futures. HRW basis is much weaker than the three-year average, which is 51 cents under futures for this week.
 
CATTLE COMMENTS
Cash cattle trade averaged $117.16 last week, down $2.17 from the previous week. Cash trade has traded lightly at mixed levels compared to last week. Last year at this time, the cash price was $114.64.
Choice boxed beef prices dropped 66 cents from last week at $207.07. Last year at this time, Choice boxed beef prices were $199.08.
 
HOG COMMENTS
The average lean hog carcass bid softened $2.57 over the past week to $89.84. Last year's lean carcass price on this date was $81.87.
The pork cutout value declined $3.71 from last week to $100.01.  Last year at this time, the average cutout price stood at $74.89.