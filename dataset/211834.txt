Capital City Fruit is expanding its network of local growers and now works with 25 growers in Iowa, Minnesota, Nebraska, Missouri, Illinois and Wisconsin. 
Brendan Comito, chief operating officer of the Norwalk, Iowa-based company, said the firm’s local growers supply more than 40 commodities and are constantly trying new items.
 
Capital City Fruit, with 155 employees, operates out of an 83,000-square-foot facility it moved into about five years ago. The firm supplies produce to both retail and foodservice customers in a seven-state region, from central Nebraska in the West to Emporia, Kan., in the south to Minneapolis and Aberdeen, S.D., in the north and St. Louis, Mo., and Illinois in the east.
 
Local pull
 
For 2017, Comito said the local season got off to a slow start and lagged behind 2016 harvest progress. 
 
Even so, early August comparisons with last year show that Capital City is up 6% over year-ago levels in local produce sales. Double digit sales gains are possible by season’s end, he said.
 
Capital City offers exclusive locally grown varieties for some customers, Comito said. While some experiments don’t work out — endive and red romaine, for example — the growers are open to trying new items, he said.
 
In all of its business, Comito said the company puts an emphasis on being the supply chain manager for its customers. “We deal with a whole network of growers (so) if something happens with one grower, we have got other growers that can fill in,” he said. “If you don’t have the product, if you are a store or restaurant, you have lost a sale. Our goal is to make sure that our customers have that product and don’t have lost opportunity cost.”
 
After a couple of slow years, Comito said organic sales are increasing for the firm. 
 
“Twenty years ago you couldn’t go into Hy-Vee and Fareway and find organics and now they have whole sections in the fresh departments,” Comito said.
 
Hy-Vee has recently introduced meal kits to create a new way to meet consumer needs, Comito said.
 
“A lot of retailers also are doing foodservice within their own building and are trying to figure out how to deliver food in different ways than just, ‘here is a store,’” he said. In terms of foodservice, Comito said more and more chefs are making produce the center of the plate or at least even with protein.
 
He said many popular food trends, such as the Whole 30, prominently feature produce in their meal recommendations. 
Comito said the company is paying attention to production trends such as urban farming and greenhouse and the potential influence those trends could have on sourcing product.
 
New staff
 
The company has added new positions to support the company’s business development work and other departments, Comito said.
 
Kelly Sixta started last fall as the sales manager and manager of the business development staff, and Kelly Nelson was brought on to support the business development staff. “We have opportunities in our region and we are targeting that,” he said. Building business relationships take time in today’s produce industry, he said. “It is not like one month you meet with them and next month you are doing business with them,” he said.
 
In other departments, Capital City Fruit hired Lara Olson to lead the company’s newly created quality control department.
 
Quality control was handled by the firm’s production and dock departments, but Comito said the company felt it was important to create an independent department that could focus on managing inventory and quality. Olson, hired in January, reports to Comito and oversees a staff of 10.
 
Capital City recently promoted Josh Henss to oversee its organics program. 
 
In other moves, the company appointed Corey Martin as purchasing manager to help develop strategic relationships with growers. 
 
“We are utilizing his experience as a purchasing manager in the hospital industry to review contracts, which are now predominant in the produce industry,” Comito said. The company also hired Jeffrey Allen as a salesman and Dena Larson as a sales administrator.