Community Suffolk hires salesman
Chris Dagg has joined the sales staff at Community-Suffolk Inc., Everett, Mass.
Dagg, a 2016 college graduate, previously had worked for Community-Suffolk part-time in the summer and other breaks from school, said president Steven Piazza.
 
Coosemans Boston adds to sales staff
Coosemans Boston Inc., Chelsea, Mass., is in the process of hiring warehouse and office help. One newcomer is salesman Jack McGinn.
“He has many years in the produce business, most with Sid Wainer,” said salesman Maurice Crafts.