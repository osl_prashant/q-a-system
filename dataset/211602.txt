Kim Dietz of the J.M. Smucker Co. has been elected president of the Organic Trade Association’s board of directors. 
Dietz, senior manager for environmental, natural and organic policy at The J.M. Smucker Co., was first appointed as an Organic Trade Association director in 2014 for a three-year term and again was elected this summer for another three-year term, according to a news release.
 
“I have been an active member of the Organic Trade Association in many different capacities over the past 20 years, and am now honored and excited to be chosen to lead this incredible organization,” Dietz said in the release. “This is an exciting and challenging time for the organic community. I will do my very best to represent the membership and to promote and protect organic.”
 
Dietz had served as vice president of the board since 2016, according to the release. She succeeds Melissa Hughes of Organic Valley who served as president since 2014.
 
The release said other officers announced at the Organic Trade Association’s annual members meeting were Marci Zaroff of Under the Canopy/MetaWear as secretary, and Rick Collins of Clif Bar as treasurer.
 
Other board members elected to the board were new board members Britt Lundgren, director of organic and sustainable agriculture at Stonyfield Farm; and Mark Squire, owner of two Good Earth Natural Food stores.
 
In addition, the Board named Bob Kaake of Annie’s Inc. and Mike Menes of True Organic Products to fill two appointed seats, according to the release.
Other members of the board include:
Perry Clutts of Pleasantview Farm, who holds the farmer seat;
Doug Crabtree of Vilicus Farms;
David Lively of Organically Grown Company;
Kelly Shea of DanoneWave;
Leslie Zuck of Pennsylvania Certified Organic; and 
Ryan Benn of Alive Publishing Group Inc., chosen by the Canada Organic Trade Association.