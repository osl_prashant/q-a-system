San Francisco-based CDS Distributing Inc. will have a significant increase in volume of Smitten variety apples this season, as the company marks its 40th year in business, said Jan Garrett, vice president of marketing.
The firm started marketing Smitten apples the week of Sept. 10 and expects to ship them at least until the end of the year.
So far, the company has had good reaction to the Smitten variety.
“People who purchase it continue to come back and ask for it over and over,” Garrett said.
Smitten is a “sweet, yet tart” cross between Fiesta, Falstaff, gala and braeburn varieties.
It seems to be an apple that is good for consumers, growers and the industry, she said.
CDS imports Smitten, fuji, braeburn and several other varieties from New Zealand during the summer, she added.
CDS is the exclusive distributor of the Smitten apple variety in the Bay Area and works with every major Northwest shipper to procure apples, she said.
The company also started marketing the fuji variety from the Northwest the week of Sept 10.
Harmony Orchards, an affiliated company near Yakima, Wash., also grows heirloom apples, which are popular for processing and foodservice.
CDS will offer Lady apples, a rather small, flat, greenish-red round variety, from October until mid-January, she said.
Gravensteins are an excellent cooking apple and are a great ingredient for pies, cider and sauce, Garrett said.
CDS’ variety is a red striped apple, while some others are green with red striping.
California’s crop starts in late July and runs until the end of October. Gravensteins from the Northwest started in mid-September.
Supplies typically are tight and Garrett said they likely will remain so.
The Ashmeads Kernel is an older, tart/sweet variety that dates back to 18th century England and often is used as a dessert apple or in apple juice and sauce, she said.
It’s a dark golden variety with a lot of rusting that’s available during November.
All of the company’s specialty apples are organically grown, and many end up at the company’s hard cidery in Washington.
CDS brokers apples, pears and cherries from the Northwest and also grows them through Harmony Orchards, she said.
The company provides year-round availability of all of its products except cherries thanks to a strong import program from Chile and New Zealand.
Cherries are imported from December through February and sourced domestically from May through early September.
This year, for the first time, the company will offer Tasmanian cherries during December.