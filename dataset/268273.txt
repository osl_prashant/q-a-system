BC-Cotton
BC-Cotton

The Associated Press

NEW YORK




NEW YORK (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Tuesday:
OpenHighLowSettleChg.COTTON 250,000 lbs.; cents per lb.May82.8883.5982.3183.53+.62Jul82.7183.3082.2683.17+.41Sep78.38+.21Oct80.09+.08Nov78.38+.21Dec78.0578.4077.9278.38+.21Jan78.48+.21Mar78.2278.5078.0578.48+.21May78.3578.5678.1978.56+.09Jul78.2978.4278.0378.42—.09Sep73.23—.16Oct75.32—.20Nov73.23—.16Dec73.1573.2373.1573.23—.16Jan73.28—.16Mar73.28—.16May73.70—.16Jul73.78—.16Sep72.12—.16Oct73.35—.16Nov72.12—.16Dec72.12—.16Jan72.16—.16Mar72.16—.16Est. sales 67,116.  Mon.'s sales 60,307Mon.'s open int 275,976