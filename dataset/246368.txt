AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:30 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
TOP STORIES:
MARIJUANA-SANCTUARY STATES
JUNEAU, Alaska — Taking a cue from the fight over immigration, some states that have legalized marijuana are considering providing so-called sanctuary status for licensed pot businesses, hoping to protect the fledgling industry from a shift in federal enforcement policy. Just hours after U.S. Attorney General Jeff Sessions announced on Jan. 4 that federal prosecutors would be free to crack down on marijuana operations as they see fit, Jesse Arreguin, the mayor in Berkeley, California, summoned city councilman Ben Bartlett to his office with a novel idea. By Becky Bohrer. SENT: 800 words, photos
TEXAS-NEW MEXICO WATER FIGHT
ALBUQUERQUE, N.M. — A lawsuit pitting Texas against New Mexico and Colorado over access to water from the Rio Grande must be sent back to an arbitrator, also known as a special master, to resolve the dispute, the U.S. Supreme Court ruled Monday. Justice Neil Gorsuch noted the federal government has an interest in ensuring water commitments are kept involving one of North America's longest rivers, citing an international agreement with Mexico and the decades-old Rio Grande Compact. The federal government has said it may pursue claims for compact violations involving the dispute. By Russell Contreras. SENT: 470 words, photos.
IN BRIEF:
— SUPREME COURT-BAIL COMMISSION — The chief justice of Colorado's Supreme Court has formed a commission to study pretrial procedures in the state.
— ASSAULT RIFLE BAN-BOULDER — A Colorado police chief says he would support a proposed ban on assault rifles, high-capacity magazines and bump stocks in his city.
— HIT-AND-RUN CRASH — Police say a 50-year-old Colorado woman was killed in a hit-and-run crash near Moab in eastern Utah.
— OVERPAID PROPERTY TAXES-ENCANA — As a result of an error in natural gas company Encana's own reporting, Garfield County, along with Colorado Mountain College and several school, fire and special taxing districts, will have to return $5.7 million in overpaid property taxes to the oil and gas producer.
— COLORADO WEATHER — Strong winds continue to raise the risk of wildfires across eastern Colorado.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for March advanced 10.25 cents at 5.0225 a bushel; March corn was up 1 cent at 3.7825 a bushel; March oats was off .25 cent at $2.6475 a bushel; while March soybeans rose 6 cents at $10.6675 a bushel.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.