Audelina Villagrana has run her ranch in Mexico's Western Sierra Madre mountains on her own since the death of her husband 23 years ago, herding livestock, hiring local Huichol people and even raising a young Huichol boy like a son.Now she and other ranchers are locked in tense confrontation with their indigenous neighbors over land that has been in contention for centuries. A series of recent legal decisions has brought the dispute to a boiling point.
"It's a strange situation, when on the one hand I share my home with them, and on the other, they're suing me for my land," Villagrana told the Thomson Reuters Foundation from her terracotta-tiled farmhouse in the mesquite-studded hills.
At issue are vast stretches of property that ranchers want for intensive agriculture and grazing, but the Huichols - also known by the traditional name of Wixarika - want it for subsistence farming and to practice their traditional ways of life.
Each side wants the Mexican government to settle the dispute, but so far it has failed to do so.
Traditional Way of Life
The Huichol people hold land grants dating back to the 1700s from the Spanish crown, but the ranchers hold titles from the Mexican government, dated before the decade-long national revolution that began in 1910.
Now, after a series of lawsuits were decided in favor of the Huichols, they are moving in to claim 10,500 hectares (nearly 26,000 acres) in the state of Nayarit, beginning with a 184-hectare (454-acre) hillside ranch.
Since September, hundreds of Huichols have organized themselves to take turns camping on the land and standing guard.
"This land is an inheritance that the ancestors left to us," said Luis S?°nchez Carrillo, a Huichol elder who said he believes the land is necessary for upholding his people's traditions.
The Huichols object to the ranchers' intensive grazing and planting, and use of chemicals and deforestation practices. They prefer subsistence farming and reforestation efforts.
The Huichols also practice rituals to honor sacred sites such as the Cerro Cuate, a towering peak, where they leave offerings for ancestors and deities believed to reside there.
The conflict echoes the Standing Rock dispute in the U.S. state of North Dakota, where Native American activists and supporters have camped on federal property to demand a halt to an oil pipeline project, said Paul Liffman, a professor of anthropology at Rice University in Texas and a Huichol expert.
Indigenous groups have been making land claims more forcefully since a 1989 United Nations convention provided them with a legal framework, Liffman said.
"There's been a major revival of indigenous claims amidst the enhanced possibilities that were afforded by the ratification of Convention 169," he said. "Even ... the countries that did not sign onto that have felt the pressure."
'Loss of Livelihood'
But the ranchers in Nayarit fear they are losing their livelihoods.
"I ask you, who generated the problem? Was it the Huichols, or us?" said rancher Lucio Gamboa. "Wouldn't you agree that it was neither of us? It was the government. So, who is responsible for solving this problem? The government."
The Mexican government has rejected a request by Huichol leaders to reimburse ranchers with federal funds designated to help prevent land conflicts.
A committee of Huichol leaders and ranchers recently wrote to the government asking for a commission to be set up to address the dispute, but the government has yet to respond.
A spokesman for Mexico's Secretariat of Agrarian, Territorial and Urban Development, which is charged with resolving land disputes, told the Thomson Reuters Foundation that it lacks the resources to get involved and is already faced with some 323 pending land conflicts.
But Gamboa said the government should step in.
"If the two sides want an arrangement, why doesn't the government want that?" he said.