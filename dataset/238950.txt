A Michigan dairy processing plant has gained support from state and local officials.
On Feb. 27, the Michigan Strategic Fund (MSF) approved a "private activity bond inducement" worth $12 million to help support the construction of a new Foremost Farms dairy plant.
The $57.9 million project was announced in November with the plant being slated to process milk solids by the start of 2018. Located on a 96 acre lot in Greenville the plant would be 30 miles from the center of Foremost Farms’ cooperative members in Michigan.
Michigan’s Department of Agriculture and Rural Development (MDARD) reports that dairy farmers contribute $15.7 billion to the local economy.
"Value-added processing developments like the one at Foremost Farms are the future of the food and agriculture industry," says MDARD Director Jamie Clover Adams.
MDARD has offered a grant of $500,000 grant from its Food and Agriculture Investment Fund and the city of Greenville is offering a 12-year property tax abatement valued at $2.33 million.
Jeff Mason, CEO of Michigan Economic Development Corp (MEDC), believes the processing plant will help elevate the profile of Michigan’s dairy industry, while also increasing farmer profitability.
"Foremost Farms' decision to further invest here demonstrates to other global companies Michigan's standing as one of the great agriculture centers in the U.S. with a business climate that enables their success," Mason says.
The cooperative primarily has processing in plants in Wisconsin with seven cheese plants, three ingredients plants and a butter plant. Another ingredients plant is located in Minnesota. 
"It is an ideal location as a manufacturing base for Foremost Farms USA to unify our seven-state cooperative membership's milk," says Foremost Farms' President and CEO Michael Doyle.
Foremost Farms has cooperative members in Wisconsin, Minnesota, Iowa, Illinois, Indiana, Ohio and Michigan.
Michigan was the 6th largest producer of milk in 2017 according to U.S. Department of Agriculture data, ranking just behind Texas and ahead of Pennsylvania. Last year Michigan produced 11.2 billion lb. of milk, 355 million lb. more than in 2016. 
MDARD estimates that dairy producers in Michigan lost $164 million in 2017.
The plant is projected to process 3.2 million lb. and employ 33 people upon opening. Future expansion would have the plant processing 6 million lb. of milk daily.