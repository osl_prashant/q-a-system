Preconditioning kiwifruit is an important step for grower-shippers who see sales potential in ready-to-eat product.
It’s a key to success of Sun Pacific Marketing’s Mighties line of preconditioned and packaged kiwifruit, said Howard Nager, vice president of business development for the Pasadena, Calif.-based company.
“We are firm believers in preconditioning the fruit with our proprietary ripening technology, providing the consumer with a terrific eating experience,” Nager said.
Others agree.
“Preconditioning is an excellent tool to use to ensure customers get a good, consistent eating experience,” said Jason Bushong, division manager for the Giumarra Wenatchee division of the Los Angeles-based Giumarra Cos.
“Preconditioning stimulates more timely consumption, as opposed to waiting three or four days for the fruit to ripen,” Bushong said, which should lead to repeat sales sooner.
“Faster consumption plus faster (repeat sales) moves more product and increases category sales,” he said.
Preconditioning is an important step in the kiwifruit process, said Sarah Deaton, marketing manager with Zespri North America in Newport Beach, Calif.
“People don’t realize preconditioning kiwifruit starts on the vine,” she said.
“Zespri grows for taste and never picks too early. We have high taste standards that all of our growers must achieve before we export any fruit under the Zespri label.”
Zespri uses temperature to ripen its kiwifruit, Deaton said.
“Because of our best practices and our orchard standards at harvest, we know that our kiwifruit will reach its optimal maturity by manipulating the temperature,” she said.
That helps customers know they are getting a product that will ripen and reach its maximum maturity potential for a consistent flavor, Deaton said.
“Once harvested, Zespri works with its distributors to make sure our kiwifruit is conditioned and cared for all the way to shelf,” she said. 
An advantage to Zespri’s new SunGold Kiwifruit is that it has a longer shelf life than green varieties, Deaton said.
“And when customers keep it refrigerated, it lasts even longer,” she said.
 
Start with quality
Preconditioning isn’t a cure-all for poor quality fruit, though, particularly if ethylene gas is used to speed up the ripening process, said Chris Kragie, import and deciduous sales manager with Madera, Calif.-based Western Fresh Marketing.
“We’ll only start if we have the proper sugars,” he said.
“We have guys gasing to attain the minimum sugars required by the marketing order. We will have natural sugars before we start selling. We won’t have ethylene-induced sugars.”
Not speeding up the ripening proces assures a longer-lasting piece of fruit, Kragie said.
“It holds better (if gas is not used),” he said. “If you’ve induced ethylene, all it’s going to do is go from rock-hard to shriveled and no taste.”
If the fruit is ripened naturally, it will hold that ripeness longer “than if you’re forcing it,” Kragie said.
“I’m against ripening by introducing an outside agent. I don’t feel that’s the right way to do it,” he said.
“I think going through a natural ripening at room temperature is the best way to do it. I can naturally ripen a piece of kiwi, put it in the cooler and the fruit will hold.”
Gridley, Calif.-based Gridley Packing preconditions its hayward variety green kiwifruit, but without the use of ethylene, said Rau Ratana, field operations manager.
“That wouldn’t be a good idea if you’re storing it,” he said.
“We don’t pick it until the brix is ready on the early fruit we do pick.”