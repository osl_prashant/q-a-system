Sunkist Growers will feature its Delite mandarins and other citrus offerings at its Fresh Summit booth Oct. 20-21.
The cooperative has double the mandarin volume this season, and it starts shipping mandarins in November, according to a news release.
Sunkist will also preview specialty varieties like cara cara navels, which begin in December, along with blood oranges, minneola tangelos and pummelos.
“Selling specialty citrus is all about education,” director of retail marketing Julie DeWolf said in the release. “We offer a wide array of retail marketing materials to help retailers inspire consumers to try citrus varieties that they may not have tried before.
“Highly visual materials and programs that highlight flavor, usage, nutrition and recipe ideas engage consumers in-store and encourage them to explore the citrus category,” DeWolf said.
Recipes will be prepared at booth No. 2217 by chef Jill Davie, who will prepare Flatiron Steak with Sunkist Orange Chili Sauce and Citrus Spiked Sweet Potatoes with Sunkist Lime and Chayote Orange Slaw.
Sunkist is celebrating its 125th citrus season.
“We share this milestone with the entire produce industry,” Joan Wickham, director of communications, said in the release. “Our cooperative was founded on the principle that we are stronger together, and we approach our trade relationships with that same sentiment.”