Anhydrous ammonia firmed 54 cents to $525.77





UAN28% firmed 47 cents to $252.06; UAN32% gained 82 cents to a regional average of $278.19.





Urea added 50 cents to $356.05.





DAP firmed 56 cents to $448.04; MAP softened 41 cents to $467.89.





Potash firmed 14 cents to $335.55.





Our Nutrient Composite Index firmed 0.61 points to 536.99.