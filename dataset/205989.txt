Crop calls
Corn: 1 to 2 cents lower
Soybeans: Steady to a penny lower
Wheat: 1 to 3 cents lower
Grain and soy futures ended with a weaker tone after mildly favoring the upside for much of the overnight session. Light followthrough selling is expected on the open this morning, but two-sided trade is likely. USDA announced daily sales of 261,000 MT of soybeans to China and 126,000 MT to unknown destinations -- both for 2017-18 delivery. Also, dryness in central Brazil and too much rain in central Argentina are concerns, which should limit selling in the soybean market. 
 
Livestock calls
Cattle: Steady to firmer
Hogs: Steady to firmer
Cash cattle traded at $105 to $106 in the Plains late Friday, which was steady to $1 higher than the previous week. While cattle futures hold a premium to that cash trade, signs the cash market has posted a short-term low should be mildly supportive to open the week. Hog futures closed strong Friday amid corrective buying, which is expected to produce followthrough buying to start the week. The upside is limited to modest corrective buying, however, as the cash hog market continues to weaken seasonally amid building market-ready supplies.