As U.S. rice farmers bring in a new crop, rough rice prices have rallied a dollar since June. While U.S. prices vary, farmers rely heavily on consistent consumption at home each year.“The biggest market is the domestic U.S. market overall,” said Johnny Sullivan, Producers Rice Mill, located in Stuttgart, Ark. “That's where I'd say about 60 percent of the rice grown in the U.S. stays in the U.S.”
 

  


September is National Rice Month, celebrating the various uses of rice. The grain is gluten free, and also features whole grain qualities. It’s those traits that mills say are helping the crop grow in popularity domestically.
“Many ingredients that are made from rice are also gluten free for additives, for nutritional bars and different things such as that,” said Sullivan.
Once the American made rice leaves the field, it’s processed in the mill and turned into popular products like beer. Sullivan says consumers of Budweiser or Bud Light are already a big supporter of home-grown rice.
“Anheuser-Busch is virtually the largest buyer of rice in the world,” said Sullivan.
From brews to puppy food, milled U.S. rice is growing in use.
“Even puppy rations use a large amount of rice now because they found that it's very soothing to the puppy's tummies,” said Bill Reed of Riceland Foods, the world’s largest rice miller.  
What doesn’t get consumed domestically needs a home, and today, there are a few countries that top the export list.
“Most of our rice is marketed into the export markets in the Western hemisphere - many of the Caribbean Islands, as well as Central America are big rice eaters,” said Reed.
“Most of that rice in California is consumed domestically, and then about 25 percent is shipped to Japan,” said Chris Crutchfield, CEO of American Commodity Company, based in Williams, Calif.
The rice industry says they’re lockstep with the rest of agriculture on current trade deals like NAFTA, urging the administration to do no harm in agriculture’s slice of current trade pacts. 
“Mexico is our largest market,” said Crutchield. “Almost of its rough rice market or raw material market, and then Canada’s is our third largest.
Another scrutinized deal that Crutchfield fears could potentially pose fallout for rice is South Korea.
“That’s a deal that rice was excluded from,” said Crutchield. “While we don't necessarily have a say in the technical aspect of the agreement, we do export a lot rice to South Korea every year, and if we get into some sort of a trade, that could have a negative effect on the overall trade aspect.”
Crutchield says rice producers want to protect current markets already in place, but as some key buyers wane, it’s encouraging the industry to find new markets.
“Iraq is still purchasing U.S. rice, but four years ago [the country] was buying several hundred thousand tons of rice a year - this past year they bought 30,000 tons,” said Crutchfield. “With the loss of those markets, you either need a resumption of trade or you need to pick up new markets.”
In Sullivan’s eyes, Cuba is one market that could come back on board, ideally positioned to take on more exports from the rice belt in the Southeast.
“Cuba buys somewhere between 500,000 and 600,000 tons per year,” he said. “They import that much rice U.S. rice crop, and the U.S. proximity to Cuba is the closest that there is which takes out the logistic issues.”
An even bigger potential buyer would be the world’s most populous country – China.
“China is a monster of a market,” said Sullivan. “The facts are based on the consumption rates of rice in China that in a 14-day period they could eat the entire U.S. crop. So, it's an unreal market.”
“There's no doubt that China needs rice,” said Crutchfield. “Over the last 10 years they’ve gone from being a competitor of the U.S. and others on the export stage, to being the world's largest importer of rice”
The Trump administration paved the path for trade to China this year when leaders signed a deal in July, agreeing to start exporting rice to China for the first time ever. It was a deal more than a decade in the works and happened after both U.S. and China agreed on “phytosanitary protocol,” which helps put more parameters around terms of sanitary conditions for U.S. milled rice.
“One of the things that was unique about this phytosanitary agreement was this is first time the United States government has ever allowed a foreign government to send inspectors to the United Sates to review and inspect potential facilities or individual companies,” said Crutchield.
The U.S. calling the deal a major win, but the reality is little progress has been made since that initial step with the signing in July.
“Taking 11 years to get the first step behind us and being able to see how many steps we have in front of us is a little disconcerting,” Crutchfield said.
Agriculture Secretary Sonny Perdue recently set an aggressive timeline, saying the first shipment of U.S. rice could hit within the next three months. Crutchield is hopeful that’s the case.
“If they move forward in any kind of a timely fashion, we can see that happen,” he said. “My most realistic estimate would be to say that I’m confident we'll see some rice from the 2017 crop be exported from the United States to China.
Once the first shipment breaks through, Crutchfield thinks the demand growth is huge, possibly turning into a 6 figure market in the future. 
?