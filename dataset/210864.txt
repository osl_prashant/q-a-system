Uncertainty in Washington is sending the dollar on a week of shaky trade. However, the currency found footing Wednesday as the Federal Reserve released meeting notes, signaling a June rate hike is likely.A weaker U.S. dollar is offering dairy farmers a favor as it makes U.S. milk more attractive on the world market and the milk supply continues to get bigger.
Mike North, president of Commodity Risk Management Group, says the market isn’t seeing any end in sight.
“The reality for us is we haven’t gone low enough to take profitability down far enough to disincentivize the dairymen from keeping cows and adding cows to the herd,” said North.
He said with the ample feed supply and great managers, cow numbers are continuing to grow.
While exports have been impressive, it hasn’t been enough to offset the increase in supply, but North thinks with the changing value of the U.S. dollar, some doors could open.
Watch North discuss his price outlook for the rest of 2017 on AgDay above.