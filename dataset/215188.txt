IPR Fresh, formerly based in Rio Rico, Ariz., moved to a new, larger, 32,000-square-foot location in Nogales, Ariz., in early November 2017. (Photo courtesy IPR Fresh)

Big Chuy adds romas
Big Chuy Distributing Co. Inc., Nogales, Ariz., is adding roma tomatoes to its product line this year, said Jesus “Chuy” Lopez Jr., sales manager.
Shade house-grown romas were scheduled to begin arriving from Sonora, Mexico, Nov. 10 and should continue through June.
The company also ships watermelons, honeydews, hard squash and sweet corn.
 
Calavo Growers installs line

Calavo Growers Inc., Santa Paula, Calif., has installed another optical processing line in its Culiacan, Sonora, Mexico, packinghouse, enabling the company to run multiple pack sizes or configurations simultaneously, said Brian Bernauer, director of operations and sales for fresh tomato.
“Our efficiency level went up at the packinghouse since we put in an additional line, so that we can do more volume in the same time period and become more diversified in the packaging formats we can offer,” he said.
The company also added a stickering machine that is connected to the optics computer and can custom sticker tomatoes on the line before they get to the packing station, he said.
 
Ciruli Bros. hires general manager
Ciruli Bros. LLC, Rio Rico, Ariz., has hired Mark Cassius as general manager, said partner Chris Ciruli. Cassius most recently was with SunFed in Rio Rico.
Ciruli Bros. will maintain its same grower base as last year and should have plentiful supplies of everything by the first week of December, Ciruli said.
 
Crown Jewels increases volume
Crown Jewels Produce Co. LLC, Nogales, Ariz., expects to increase its volume of all commodities out of west Mexico by 15% to 20% this season compared to last year, said Jesus Gonzalez, general manager.
The company’s main imports from Mexico are zucchini, yellow and gray squash, cucumbers and green and colored bell peppers.
Most of the increases will come from existing growers as well as the addition of a green bean grower and a shade house grower of colored bell peppers.
The company had success with its first green bean program last year. “We’re expanding that program this year, and we hope to keep increasing it,” Gonzalez, said.
 
Del Campo Supreme hires salesman
Miguel Lopez, most recently with MAS Melons & Grapes LLC, Rio Rico, Ariz., has joined Del Campo Supreme, Nogales, Ariz., as a salesman, said Jim Munguia, sales manager.

Lopez will sell all commodities, including beefsteak, roma, vine-ripe and grape tomatoes, red bell peppers and a small organic program of romas, round and grape tomatoes and colored bell peppers.
The company also is conducting a small test of some heirloom tomatoes this season.
 
Divemex to start English cucumbers
Divemex, an organic and conventional greenhouse vegetable producer with facilities in Culiacan, Etzatlan and Ahualulco in Mexico, plans to add long English cucumbers and mini peppers in time for holiday promotions, said Aaron Quon, executive greenhouse category director for Vancouver, British Columbia-based The Oppenheimer Group, which markets Divemex products from September through May.
Divemex already has begun shipping organic and conventional sweet bell peppers, he said.
“While English-style cucumbers have long been popular in Canada, recent Fresh Look data indicates that they are growing in popularity across the U.S. market as well,” Quon said, especially in the Southeast.     
The facility in Ahualulco is in its first year of production and is 100% organic.
 
Pepper volume up for Fresh Farms
Fresh Farms, Rio Rico, Ariz., will have a 15% volume increase of green bell peppers this season and has added the flat la rouge bell pepper variety, said Jerry Havel, director of sales and marketing.
La rouge peppers fit in well with other items the company offers and is becoming a more popular product to offer consumers, he said.
 
IPR Fresh moves to new location

IPR Fresh, formerly based in Rio Rico, Ariz., moved to a new, larger, 32,000-square-foot location in Nogales, Ariz., in early November, said Jose Luis Obregon, president.
The company’s main commodity is bell peppers, but the firm also has offered European cucumbers for three seasons and added slicer cucumbers this year, he said.
IPR Fresh also promoted sales coordinator Manny Robinson to the sales desk and hired Sergio Valencia as sales coordinator.
 
MAS Melons offers soft squash
Rio Rico, Ariz.-based MAS Melons & Grapes LLC has added a grower in Hermosillo, Sonora, said salesman Mikee Suarez.
“They do grapes, watermelons, honeydews and soft squash,” he said.
That means that MAS Melons & Grapes now also handles zucchini, yellow straight neck and Mexican gray squash, he said.
The deal will cover U.S. markets for the fall and winter seasons.