Washington and Oregon potato growers have been slowed slightly by a wet winter and spring. But so far, the crops appear to be worth the wait.Washington growers planted 170,000 acres of potatoes in 2016 and acreage and volumes are expected to be similar in 2017.
“Last year was crazy early, as the weather promoted early season growth,” said Chris Voigt, executive director for the Washington State Potato Commission, Moses Lake.
“This year seems to be much more normal.”
Voigt said Washington state has some of the most consistent growing conditions of anywhere in the country, so there is little fluctuation in volumes.
“We typically produce about 10 billion pounds of potatoes each growing season,” he said.
“It has been a perfect growing season. We got off to a slow start due to cooler temperatures, but once the soil temperature got up, the crop had great emergence and continues to grow in a steady fashion.”
Voigt said there’ve been zero crop issues so far from pests or weather.
“It’s been perfect growing weather — warm days and cool nights. Potatoes love that,” he said.
 
Oregon
Bill Brewer, executive director of the Portland-based Oregon Potato Commission, said Oregon’s fresh potato market represents just under 13% of total U.S. production.
“This season is a few days later than last year, but last year was earlier than normal,” Brewer said. “My board is predicting no change — around 2.5 billion pounds of production.”
Ralph Schwartz, vice president of marketing, sales and innovation for Idaho Falls, Idaho-based Potandon Produce LLC, said the company starts supplying russet and colored potatoes out of Osceola, Wash., in July.
“So far, it’s been a rollercoaster summer with weather,” he said. “We had early heat and then it’s been cooler with rains in June and temperatures in the 50s and 60s — 70s to 80s is normal.”
Schwartz said everyone had adequate water this year from the heavy snow pack during the winter.
Troutdale, Ore.-based Strebin Farms LLC’s potatoes are one week later this season than they have been the past three years, said Dan Strebin, owner and manager.
Strebin said sizing also is behind for all potatoes — yellows, reds and russets.
Strebin Farms will pack storage crop through the end of July and then new crop the first week of August.
“The set looks nice on the new plants — a nice profile with a full range of size,” Strebin said.
Dale Hayton, sales manager for Valley Pride Sales, Burlington, Wash., said the company grows red, gold, white and purple potatoes in the Skagit Valley of northwestern Washington.
Amstad Produce LLC, Sherwood, Ore., expects its new potato crop to be ready after the first week of August because of weather delays. Normally, it would be ready the last week of July, said Jeff Amstad, partner.
“Carton prices on russets have been low for quite a while, but it’s been picking up the past couple months.”
“For the most part, we saw good results and decent returns,” Chin said. “But russets were weak in returns
Amstad Produce expects to have red and yellow potatoes August through Jan. 1 out of the Willamette Valley.
Myron Ayers, sales manager for Norm Nelson Inc., Burlington, Wash., which produces Double-N Potatoes, said the company should start potato harvest in September.
“At this point of the growing season, we expect an average crop in yield and pack out,” he said.
Wong Potatoes Inc., Klamath Falls, Ore., finished a decent potato season May 1, said Dan Chin, president.”
Like others, Wong Potatoes’ 2017 crop is delayed because of the wet winter and spring, with about half of the crop a couple of weeks behind normal growth.