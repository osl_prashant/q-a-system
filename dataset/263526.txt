The North American fertilizer industry pledged $6 million to fund research to strengthen fertilizer best management practices and reduce nutrient loss to the environment. The industry hopes the donation can be leveraged with public sector dollars to develop further funding.
“While the industry is heavily invested in research through the 4R Research Fund, we need to leverage these dollars with additional funding from the public sector to make the most efficient use of these limited resources,” says Chris Jahn, president of The Fertilizer Institute. “TFI supports public funding and incentives that encourage farmers to adopt 4R Nutrient Stewardship practices on their land.”
The fertilizer industry has donated $5.8 million to 4R Nutrient Stewardship since 2013, and those funds have generated $7 million through leveraging public and private dollars.
The 4R fund encourages using the right fertilizer source, at the right rate, at the right time and in the right place.
“There is a delicate balance between reducing nutrient losses through nutrient management while maintaining or improving soil carbon and thus soil health,” says Terry Roberts, president of the International Plant Nutrition Institute. “Across the U.S. and Canada, the fertilizer industry is partnering with universities, watershed stakeholders and government agencies to expand the data linking agronomic and environmental performance of 4R practices."
Priorities of the new resources include the following.

assessing source, rate, time and placement effects of enhanced efficiency
advancing technologies for nitrogen and phosphorus fertilizers
testing 4R practices in more locations, over longer periods of time and on more cropping systems