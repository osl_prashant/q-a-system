Western Growers president and CEO Tom Nassif will participate in an Oct. 5 immigration policy event.

Called “Leading the Way: A New Approach to American Immigration,” the National Immigration Forum’s event is set from 9:30 a.m. to 8:30 p.m. at the JW Marriott Hotel in Washington, D.C. Western Growers is one of the sponsors for the event, according to a news release.

The event will bring together faith, law enforcement, business leaders and others for a conversation on immigration policy solutions, according to the release. Four panel discussions are on the agenda, in addition to a keynote address from Sen. Thom Tillis, R-N.C.

Nassif will be a part of a panel discussion on “Growing American Jobs through a Balanced System” from 11:15 a.m.–12:15 p.m. according to the release. Others on the panel are Tim Brown, president and COO of Chobani; Rosanna Maietta, senior vice president of communications and public relations for The American Hotel & Lodging Association; and Kathleen McLaughlin, senior vice president and chief sustainability officer and president, Wal-Mart Foundation. Pia Orrenius, vice president and senior economist for the Federal Reserve Bank of Dallas, will moderate the discussion.
The three other panels will focus on security and the border, welcoming immigrants and refugees during times of conflict, and how to address America’s cultural and economic anxiety about immigration, according to the release.
Registration is available online.