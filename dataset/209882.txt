Baldor Specialty Foods plans to soon debut a vegetable side kits line under its Urban Roots brand.
The kits serve two as a side or one as a meal, and the suggested retail price is $7.99. Retailers can expect five or six days of shelf life.
Baldor will distribute the kits to retailers in the Northeast.
Chili Cilantro Cauliflower Rice, Tabouli Style Cauliflower Rice and Moroccan Spiced Cauliflower Rice are the first three items in the new line.
In conceptualizing the new products, Baldor considered the success of both meal kits and value-added produce and decided to combine those elements.
“We sell normal cauliflower rice, and we’ve seen that item absolutely just explode,” said senior director of marketing and development Benjamin Walker. “We also make salsa verde, we make fresh salsas every day in our production facility, so our Chili Cilantro Cauliflower Rice has a small container of salsa verde, pumpkin seeds, fresh cilantro, fresh-shucked corn that we’re cutting in our facility every day.
“These are items that we’re making fresh for our high-end restaurants every day, but we compiled them in such a way where you create a really exciting, flavorful side dish in just minutes,” Walker said.
Baldor plans to add to the line in the near future. Debuting along with the new products is the refreshed look of the Urban Roots brand.
“We wanted something young, very active, very healthy, bright and energizing, so you’ll see bold colors, you’ll see Urban Roots in a white script,” Walker said. “We got a little edgy with the design, tried to have a little bit of fun with it ... because produce has typically lacked the innovation in these types of products.
“You saw cauliflower rice and veggie noodles kind of light the world on fire in the last two years, and we kind of wanted to take that momentum and take it to the next level,” Walker said.
The company is already working on its next group of products, which will be geared toward the winter season.