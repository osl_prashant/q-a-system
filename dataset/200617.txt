Farm Diesel

Farm diesel is unchanged once again this week.
Our farm diesel/heating oil spread indicates lower near-term diesel price action.
Seven of the twelve states in our weekly survey were unchanged on the week.
Distillate supplies firmed 4.4 million barrels last week and refining activity is expected to continue at high levels for the time being.

 
Propane

Propane firmed 2 cents per gallon this week on strength in Wisconsin and Nebraska.
Seven of the twelve states in our survey are unchanged this week.
Propane supplies firmed 3.3 million barrels on the week
This week's price action is further evidence that summertime prices .






Fuel


5/29/17


6/5/17


Week-over Change


Current Week

Fuel



Farm Diesel


$1.97


$1.97


Unchanged


$1.97

Farm Diesel



LP


$1.24


$1.24


+2 cents


$1.26

LP