Last week brought plenty ofnegative news for the milk markets. While USDA did reduce the production forecast for the remainder of the year, the cutwasn't enough to move milk prices higher. In addition, USDA announced the predicted all-milk price for the remainder of the year, which at$14.95 to $15.55 per cwtleft most dairy farmers looking for risk management.Unfortunately, they won't find it infutures contracts right now.
Robin Schmahl, hedge analyst with AgDairy, says 2017 contracts declined significantly last week. He says the futures saw losses in the double digits for several days.
"It seems like some of the hedgers were looking at some of the projected outlooks and realized it's not looking that much better," Schmahl says. "We've got Class III prices from January toMay 2017 below $15 now. They are taking some of the premium out of the market that had been there before."
Dairy market movement highlights:
Barrels: up $0.01.
Blocks:down $0.02.
Butter:down $0.0525.
NFDM:up $0.03.
Listen to the Weekly Dairy Market Wrap below: