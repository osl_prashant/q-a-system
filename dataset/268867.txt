BC-Wheat KX
BC-Wheat KX

The Associated Press

CHICAGO




CHICAGO (AP) — Winter Wheat futures on the Chicago Board of Trade Thu:
OpenHighLowSettleChg.WHEAT5,000 bu minimum; cents per bushelMay512¾514¼501¼507½—9¼Jul532533¼520½526—9¾Sep553½553½540545¾—9¼Dec576576564¾570¾—8½Mar588588581586½—7¾May590¼595½588¾594¾—7Jul601¾601¾592598½—7Sep606¾—5¾Dec613618¾613618¾—6¾Mar624¼—6¾May624¼—6¾Jul617½—6¾Est. sales 85,993.  Wed.'s sales 75,330Wed.'s open int 270,614