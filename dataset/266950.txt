BC-Merc Early
BC-Merc Early

The Associated Press

CHICAGO




CHICAGO (AP) — Early trading on the Chicago Mercantile Exchange Tue:
Open  High  Low  Last   Chg.  CATTLE                                  40,000 lbs.; cents per lb.                Apr      112.65 113.35 111.77 112.07   —.33Jun      100.95 101.82 100.22 100.45   —.50Aug      100.62 101.42 100.05 100.27   —.33Oct      105.20 105.97 104.70 104.92   —.30Dec      109.92 110.52 109.42 109.60   —.30Feb      111.90 112.52 111.62 111.65   —.30Apr      112.67 113.02 112.42 112.45   —.22Jun      106.70 106.70 106.42 106.47   —.23Aug      105.57 105.57 105.30 105.30   —.35Est. sales 28,148.  Mon.'s sales 71,207 Mon.'s open int 356,314                FEEDER CATTLE                        50,000 lbs.; cents per lb.                Apr      132.02 132.92 131.17 131.42   —.45May      132.47 133.55 131.57 131.85   —.52Aug      139.00 140.15 138.20 138.42   —.53Sep      140.12 141.12 139.52 139.55   —.57Oct      140.50 141.50 139.85 139.87   —.70Nov      140.40 141.35 139.85 140.00   —.45Jan      137.15 137.15 136.12 136.12   —.13Est. sales 4,565.  Mon.'s sales 16,480  Mon.'s open int 54,104,  up 835        HOGS,LEAN                                     40,000 lbs.; cents per lb.                Apr       54.35  54.40  52.27  52.27  —2.00May       64.75  64.87  63.20  63.20  —1.62Jun       73.35  73.55  71.92  72.05  —1.50Jul       74.50  74.65  73.35  73.47  —1.23Aug       74.77  74.85  73.60  73.72  —1.18Oct       63.85  64.02  63.07  63.12   —.75Dec       58.70  58.80  58.27  58.30   —.45Feb       62.85  63.00  62.55  62.60   —.30Apr       66.82  66.82  66.10  66.10   —.77Est. sales 22,226.  Mon.'s sales 36,277 Mon.'s open int 235,092,  up 606       PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.