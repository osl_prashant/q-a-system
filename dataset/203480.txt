Over the past year, the City of Brotherly Love hosted two major international events.
In September 2015, the pope called on the city and in late July, the Democratic National Convention met at the Wells Fargo Center, several blocks from the Philadelphia Wholesale Produce Market.
Distributors on the market gave the DNC's visit mixed reviews.
Mark Levin, co-owner of the Philadelphia-based M. Levin & Co Inc., contrasted the papal and DNC events.
Three months ahead of Pope Francis' visit, people were advised to stay out of the city.
Though the pope received a large turnout, the visit wasn't as disastrous as initially thought because the early warnings scared many visitors away from the city, he said.
"When the DNC was here, it was a whole different ballgame," Levin said. "They (the city) advised people to visit the city, enjoy the restaurants and see the historic sites. The restaurant and foodservice business increased with both visits. I believe it was a real boost to the city."
For Mike Maxwell, president of Philadelphia's Procacci Bros Sales Corp., the DNC was positive.
"Many came to Philadelphia," he said. "The wholesalers we sell to that sell directly to the restaurants did a nice job. I think they had a little bump. Logistically, it didn't bother us at all that the convention was two blocks from us."
Chip Wiechec, president of Hunter Bros. Inc. of Philadelphia, was disappointed.
"A lot of people, like the restaurants, were encouraged and thinking they would do a lot of business, but they didn't really get any kind of bump at all because the conventioneers, the ones here for the politics, got started early in the afternoon. Whether they were protesting or working at the convention center, they were busy from 2 p.m. to Midnight," he said.
"I know there were a lot of restaurants that had lead in their pencils but no one to write to. They were kind of let down."
Though the convention helped increase foodservice business, it made for a logistical nightmare, said Tom Curtis, president of Tom Curtis Produce Inc. in Philadelphia.
"The convention really created a tremendous amount of turmoil to run a business," he said.
Authorities stopped trucks weighing more than three tons from traveling on Interstate 95 and a Bernie Sanders rally shutdown Center City traffic for a day, Curtis said.
"It was catastrophic, but as far as people coming in and touring the facility, it was a boom for the city in terms of restaurant business," he said. "It helped improve business. It was a big deal here. It was an inconvenience but everything was jumping."
Martin Roth, secretary-treasurer of Coosemans Philadelphia Inc., said many didn't experience any business increases.
"It (the convention) slowed traffic to the market and made it inconvenient for a lot of people," he said. "A lot of the small guys, the ones that want to shop around, didn't come to the market. The demonstrators didn't go to restaurants, they had coolers or may have bought a hoagie, a hot dog or slice of pizza. It didn't improve business."
The market posted alerts on its website and social media during the July 25-28 convention, said Christine Hofmann, marketing coordinator.
The pope's Sept. 26-27 visit wasn't as bad as anticipated, she said.
"The DNC showed a spotlight on the city," Hofmann said. "No matter your political view, it was electrifying. The city was really alive. Traffic didn't seem too bad. People managed better than we anticipated."
Four Seasons Produce Inc., in Ephrata, Pa., sells mostly to retail customers.
Ron Carkoski, president and CEO, said the convention didn't bring any increase in retail sales.
"From what I saw on the weekly sales reports and category breakdowns of those sales against what was planned, I did not see any major uptick from it," he said. "I am not sure if retailers would've gotten that kind of a kick."