Clayton Rawl adds Florida acreage
Lexington, S.C.-based Clayton Rawl Farms Inc. has expanded its presence in Florida, said Chris Rawl, president.
“We’ve ventured into an operation in South Florida and purchased a facility from Cabbage Inc. and are leasing some property in Indiantown, Fla., of about 900 acres,” Rawl said.
“This spring has been kind of experimental, but we have pulled some sweet corn, cabbage and some green peanuts, and we’re looking forward to the next winter season.”

The deal was completed in December, Rawl said.
George Davis manages the new operation, which is south of Lake Okeechobee, Rawl said.
“We will expand our squash and cucumber program for the whole winter, hopefully, and sweet corn — everything,” Rawl said.
The deal will start in November, Rawl said.
“All the crops that are sensitive to freeze” will be grown there, Rawl said.
“Maybe we can secure the markets for the whole year on some of those products,” he said. “As long as you can grow your own, you keep your customers all year round, and that’s the goal.”
 
Farm Fresh rebrands, adds acreage
Faison, N.C.-based Farm Fresh Produce Inc. has unveiled a new logo, is developing new packaging and has updated its website, farm-fresh-produce.com, said Bethany Malcolm, vice president.
In January, the grower-shipper held a grand opening for its new warehouse location just outside of Clinton, N.C., Malcolm said.
“We currently store and pack sweet potatoes from this location,” she said. “We will be storing our spring and summer vegetable crops there, as well.”
Farm Fresh also has been working on sustainability issues, “with solar power and water harvesting,” Malcolm said.
“We hope to have this initiative completed by summer’s end,” she said.
The company also has hired a domestic salesman, Bob Riggle, who will focus on “growing sales in the Northeast, South and Midwest,” Malcolm said.
Riggle, from Vardaman, Miss., owns Pontotoc, Miss.-based United Growers & Packers Inc., but “will be jump-starting our domestic sales, as we’ve drastically increased our acreage and storage this year,” Malcolm said.
Riggle started with Farm Fresh Produce in April, Malcolm said.
 
Ham Produce hires marketing manager
Abby Hines is the new marketing manager at Snow Hill, N.C.-based Ham Produce Co. Inc., which grows and ships cabbage, cucumbers, pickle cucumbers, squash, sweet potatoes, vegetables and watermelon.
Hines, a native of Rocky Mount, N.C., earned a communications degree at North Carolina State University in 2006.
“Basically, I will be handling the marketing and development side, all social media accounts, trade shows and special programs and some sales,” she said. She started April 10.
It’s Hines’ first job in the produce industry, after having worked in the pharmaceutical sales industry.
“I looked into a sales job with Ham about three years ago. I decided to stay in the pharmaceutical sales industry, but they reached back out about three months ago and it was a good fit, and I ended up coming this way,” Hines said.
 
Jackson to hire marketing specialist
Autryville, N.C.-based Jackson Farming Co. will hire a marketing and branding specialist in June, said Matt Solana, vice president of operations/supply chain.
Solana declined to identify the newcomer.
“She will be working on the social media impact with our consumers and chain partners to help communicate with our ever-changing customer base and telling the story of farming and production to a generation that for the most part has been far removed from it, based on societal changes,” Solana said.
 
Kornegay harvests first asparagus
Princeton, N.C.-based Kornegay Family Produce has added asparagus to its product line, said Kim Kornegay-LeQuire, vice president.
“We cut our first asparagus this spring after planting the crowns in March of 2016,” she said. “Yields were small, which we expected, but quality and taste were excellent.”

Asparagus made sense, since it’s not widely grown in the region, Kornegay-LeQuire said.
“We like having something that makes us stand out a little, and we like to grow something that we like to eat ourselves,” she said.
The company also is increasing its watermelon acreage, Kornegay-LeQuire said.
“Last year was our first crop in years, and it was a great success … so it made sense for us to try to expand.”
 
Salesman joins L&M Cos. Inc. 
Raleigh, N.C.-based L&M Cos. has been experimenting with different varieties and products in a protected ag environment over the past year, said Hurley Neer, eastern vegetable sales and operations manager.
The company also has hired Cody Dunn for its Raleigh-based sales staff. Dunn is a graduate of North Carolina State University and “brings a wealth of agricultural experience to our sales team, as he was farming the past six years,” Neer said.

Nash Produce refreshes brand
Nashville, N.C.-based Nash Produce LLC will roll out a new brand, logo and packaging “in late summer or early fall — definitely in time for our busy season of Thanksgiving and Christmas,” said Laura Hearn, marketing and business development director.
“People want a fresh, simple and crisp look, so we’re trying to kind of refresh,” she said.
The company’s packaged items bear the Mr. Yam label, the company’s primary brand, Hearn said.
“Mr. Yam will still be around, but we’ll have a completely new brand for everything, even for bulk,” she said. “We’ll still have other brands, but we’ll have one new fresh brand to offer primarily to retailers. We hope it will be a brand with more appeal and reach more consumers. It’s just in line with current data and the demands of consumers and millennials.”
 
N.C. ag department to host reception
The North Carolina Department of Agriculture will have its annual reception — something it has done since 2002 — at the Produce Marketing Association’s 2017 Fresh Summit in New Orleans, said Tommy Fleetwood, marketing supervisor with the department.
The reception will be at 5:30 p.m. Oct. 20 in the River Room at the New Orleans Hilton Riverside Hotel, Fleetwood said.
“We’ll have a lot of buyers and industry people attending it,” Fleetwood said. “We always have a band and some entertainment and a good time.”
 
Pamlico Shores upgrades equipment
Swan Quarter, N.C.-based potato grower-shipper Pamlico Shores Produce has upgraded some equipment to be more efficient, said Hunter Gibbs, partner.
“We put in a new double-wicket bagger. We did have a single-wicket,” he said.
The bagger will be ready for the start of the deal, which generally runs from about June 20 to the end of July, Gibbs said.
 
Scott Farms adds consumer packs
Lucama, N.C.-based sweet potato grower-shipper Scott Farms has finished two major construction projects in the past two years, said Jeff Thomas, marketing director.
In October, the company, which grows sweet potatoes on 2,800 acres, completed construction of an 80,000-square-foot curing and storage facility, he said.
The facility, with a capacity of 600,000 bushels, gave the company a total of 2.2 million bushels now under environmentally controlled storage, Thomas said.
In April 2015, Scott Farms opened a 60,000-square-foot packing facility.

“We have to make sure we’re an end-to-end packing operation, making sure we go crop to crop,” he said. “We have a full 12-month supply of sweet potatoes.”
Scott Farms also recently launched two products, Thomas said.
One is a 1.5-pound Steam-In Bag, which is packed in cases of 12 units. The suggested price is $2.99, Thomas said.
The other is a four-count tray pack, which the company calls its “family pack,” he said.

“It’s very consistently sized product for a family of four. It’s a nice baking-size potato — roughly 8 ounces — and works well with recipes, as well,” Thomas said.

Both products were introduced last fall, he said.