Duda Farm Fresh Foods, Oviedo, Fla., has reorganized leadership positions in Oxnard and Salinas, Calif., and in Oviedo and Wellington, Fla.“We are pleased to make these promotional announcements for our key management team leaders for Duda Farm Fresh Foods,” Dan Duda, president and CEO, said in a news release. “All are extremely dedicated, professional, and very deserving of this recognition and advancement for their careers. We are confident that these promotions and new reporting relationships will position our company strongly for the future, ensuring our success and growth continues for many years to come.”
The reorganization reflects promotions and a new structure, according to the release.
“Our organizational changes were designed to accomplish two things for us,” Duda said in an e-mail. “First, to position Duda Farm Fresh Foods for future growth while running it as efficiently as possible, and second, to recognize and acknowledge through career advancement some very deserving employees who have worked hard and dedicated themselves to improving our business.”
As of April, the following people were in their new positions:
Sammy Duda, senior vice president of national operations (previously vice president of Western operations);
Mark Bassetti, senior vice president of sales, marketing and development (previously senior vice president customer development);
Dean Diefenthaler, vice president of western operations (previously vice president of Western farming);
Amy Kinder, vice president of food safety and sustainability (previously senior director of food safety);
Alberto Cuellar, vice president of global business (previously director of global business);
Larry Pierce, senior director of research and development/plant breeding (previously director of celery research);
Manuel Alcala, senior director of field and harvesting operations (previously director of harvesting);
Greg Lewis, director of western production (previously production manager);
Nichole Towell, senior director of marketing services (previously director of marketing); and
Jeff Goodale, director of domestic sales (previously manager of business development).