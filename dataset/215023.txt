The University of Florida’s Institute of Food and Agriculture Sciences Citrus Research and Education Center, Lake Alfred, Fla., is celebrating its 100th anniversary.
The anniversary celebrations are from 9 a.m. to 3 p.m. EST Nov. 29 at the center, according to a news release.
Fred Gmitter, professor of horticultural sciences at UF/IFAS, will be the keynote speaker of the event. He will give a presentation on tracing the genetics of citrus breeding back millions of years, the release said.
Other speakers include:

 W. Kent Fuchs, UF president;
 Jack Payne, UF senior vice president of agriculture and natural resources; and
 Michael Rogers, director of the UF/IFAS Citrus Center.

Attendees will get the chance to learn how the university’s researchers work with the citrus industry to battle huanglongbing and other diseases, according to the release.