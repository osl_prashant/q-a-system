Powerful earthquake rattles remote Papua New Guinea
Powerful earthquake rattles remote Papua New Guinea

By NICK PERRYAssociated Press
The Associated Press

WELLINGTON, New Zealand




WELLINGTON, New Zealand (AP) — Government officers in Papua New Guinea are traveling to the area hit by a powerful earthquake to assess unverified reports of fatalities and to see the extent of the damage.
A magnitude-7.5 quake hit early Monday about 89 kilometers (55 miles) southwest of Porgera in the Pacific Island nation. The quake rattled forest villages and a large gold mine.
The area also is home to a number of oil and gas operations and coffee plantations.
Oil Search Managing Director Peter Botten said the company was closing down some production operations in the region as a precaution, and that partner ExxonMobil had also shut down a facility. Botten said there had been no injuries reported among its employees.