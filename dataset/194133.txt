Soybean futures faced pressure this week from a weakening Brazilian real, which threatens to slow export demand for U.S. soybeans. However, key support at the April lows held, which helped trigger a modest recovery to close out the week. Back-and-forth trade continued in the corn market this week as weather uncertainty offset pressure from a weakening Brazilian real. Corn futures showed little net movement for the week. Winter wheat futures declined despite more late-season weather threats, proving how difficult it is for the market to fight off seasonal pressure with harvest starting in the far Southern Plains. Spring wheat futures mildly firmed amid concerns of another low-protein winter wheat crop.
Pro Farmer Editor Brian Grete highlights this week's Pro Farmer newsletter below:



Cattle futures continued their corrective pullback, though seller interest eased. Lean hog futures extended their price rally amid seasonally strengthening cash hog prices.
Click here for this week's newsletter.