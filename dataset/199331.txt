It's tempting to paint the environmental activist community with a broad brush.A brush filled with hot tar, followed by buckets of feathers, that is.
Many eco-activists are shrill, antagonistic and absolutely uninterested in compromise,
They're right, and anyone who opposes their demands for regulations limiting farming, forestry and ranching is dead wrong.
Pretty much the other side of the Bundy clan's coin.
But when scientists who collect data based on factual observations weigh in on controversial positions, the industry owes it to itself to listen with an open mind. Researchers ‚Äî the ones who aren't on some think tank's payroll ‚Äî aren't pushing an agenda. They're looking for insights based on the reality of what scientific observations tells them.
That's why the following story needs to be judged objectively, because it runs counter to the (understandably) emotional response most ranchers have toward the no two-decades-old project to reintroduce wolves to rural areas where livestock tend to graze.
A group of researchers at Washington State University have determined that killing off wolves to protect livestock is exactly the wrong way to go.
In fact, according to the data they have exhaustively analyzed, shooting and trapping end up causing more sheep and cattle kills, not fewer.
Writing in the journal PLOS ONE, WSU wildlife biologist Rob Wielgus, Ph.D., an associate professor and Director of WSU's Large Carnivore Conservation Lab, and data analyst Kaylie Peebles reported that for each wolf killed, the odds of more livestock depredations increase significantly.
According to a news release from the university, that trend continues until 25% of the wolves in an area are killed. Ranchers then can expect to see a "standing wave of livestock depredations," Wielgus said. Moreover, that rate of wolf mortality "is unsustainable and cannot be carried out indefinitely if federal relisting of wolves is to be avoided."
Understanding the backstory
So how did Wielgus and Peebles come to that conclusion? And are their data credible? The answers require a bit of background.
In 1974, the gray wolf, Canis lupus, or the timber wolf, as most people know it, was federally listed as endangered. The recovery effort, involving the introduction of breeding pairs to public lands in the northern tier states ‚Äî including Washington ‚Äî also involved government-funded predator control efforts to keep wolves from killing sheep and cattle. When wolves were delisted in 2012, sport hunting was also authorized.
The effectiveness of using "lethal control" has been a "widely accepted, but untested, hypothesis," according to Wielgus and Peebles.
They studied 25 years of lethal control data from U.S. Fish and Wildlife Services Interagency Annual Wolf Reports in Montana, Wyoming and Idaho, and here's the bottom line: Killing a single wolf increases the odds of livestock depredations by 4% for sheep and 5% to 6% for cattle. If 20 wolves are killed, livestock deaths double.
That parallels a similar study in 2015 by Wielgus, Peebles and other WSU colleagues, which found that lethal controls of cougar populations also tend to backfire, disrupting their populations so much that younger, less-disciplined cougars end up attack more livestock.
Even with those previously published data pointing the way, Wielgus said he did not expect to see the same result with wolves.
"I had no idea what the results were going to be, positive or negative," he said in a statement. "I was surprised that there was a big effect."
The explanation why killing off wolves cause more livestock predation, although based on reams of analytical data, is surprisingly simple: When wolves remain in intact packs, led by mature breeding pairs ‚Äî alpha males and females ‚Äî they seem to prefer wild animals that populate the same wide-ranging hunting territory as the wolf pack. Here's how Wielgus explained it:
But when legal or illegal hunting ends up killing off the alpha wolves, breeding pairs escape the normal pack controls.
"While an intact breeding pair will keep young offspring from mating, disruption can set sexually mature wolves free to breed, leading to an increase in breeding pairs," he was quoted in the WSU news release. "As they have pups, they become bound to one place and can't hunt deer and elk as freely. Occasionally, they turn to livestock."
His findings, although solid, don't offer a comprehensive alternative, although he and his team suggested that such non-lethal interventions as greater use of guard dogs, range riders on horseback and the use of flags, spotlights and "risk maps" that discourage grazing animals in hard-to-protect areas where wolves are numerous.
All that involves costs, and who supplies the funding is an unanswered question.
But at the very least, this new study cannot be dismissed just because its conclusions are unpleasant.
Science has a nasty habit of doing exactly that. ‚ñ°
‚Ä? To review the full study, click here.
Dan Murphy is a food-industry journalist and commentator