Dairy Farmers of America (DFA) recognizes the importance of developing future leaders in the dairy industry and is committed to investing in their education. Since its inception 11 years ago, the DFA Cares Foundation Scholarship has grown to honor an increasing number of outstanding students who are pursuing careers in the dairy industry.
Selection criteria include a commitment and passion for a career in the dairy industry; extracurricular activities, awards and work experience; and academic achievement. This year, DFA’s scholarship committee identified 45 recipients who will receive a combined total of $57,000.
Pre-college Students

Abigail Albin, Fortuna, Calif., planning to attend Cal Poly San Luis Obispo or Oklahoma State University and major in agriculture education
Jared Baudhuin, Brussels, Wis., planning to attend University of Wisconsin–Madison and major in dairy science
Madison Bock, Ashley, Ill., planning to attend Rend Lake College and major in agriculture education
Reese Burnett, Carpenter, Wyo., undecided on university, planning to major in animal science
Grant Fincham, Marysville, Kan., planning to attend Kansas State University and major in feed science
Austin Freund, Concordia, Mo., planning to attend State Fair College–Sedalia and major in agriculture animal science
Allen Graulich, Cobleskill, N.Y., undecided on university, planning to major in animal science/dairy science
Johnathan King, Schuylerville, N.Y., planning to attend Cornell University and major in animal science/ dairy science
Taylor Klipp, Hanover, Kan., planning to attend Southeast Community College–Beatrice and major in agriculture business
Grace Koester, Red Bud, Ill., planning to attend Southern Illinois University–Carbondale and major in animal science
Aaron Lay, Madisonville, Tenn., planning to attend Tennessee Tech University and major in agriculture communications
Sarah Lehner, Delaware, Ohio, planning to attend The Ohio State University and major in animal science
Morgan Marotz, Wahoo, Neb., planning to attend Wayne State College and major in elementary education
Morgan Mickell, Circleville, Utah, planning to attend Southern Utah University and major in health science
Jessica Nash, Elsie, Mich., planning to attend Michigan State University and major in dairy management
Jack Palla, Clovis, N.M., planning to attend Oklahoma State University and major in animal science
Sonora Palmer, Preston, Idaho, planning to attend Brigham Young University and major in animal science
Matthew Peck, Schuylerville, N.Y., planning to attend Cornell University and major in animal science with an emphasis on dairy
Seth Racicky, Mason City, Neb., planning to attend Nebraska College of Technical Agriculture and major in animal science with an emphasis on dairy reproduction
Gavin Rankins, Cusseta, Ala., planning to attend Auburn University and major in animal science-production/management
Michelle Ruijne, Plainview, Texas, planning to attend Texas A&M University and major in animal science
Cole Schaap, Clovis, N.M., planning to attend Cornell University and major in animal science with an emphasis on dairy
Ryan Siegel, Otterville, Mo., planning to attend the University of Missouri–Columbia and major in animal science
Derek Vander Hoff, Hillsdale, Mich., planning to attend Michigan State University and major in agriculture business management
Lora Wright, Verona, Mo., planning to attend Oklahoma State University and major in animal science and agriculture communications

Undergraduate Students

Christy Achen, Logan, Utah, attends Utah State University and majors in agriculture communications and journalism
Nicholas Achen, Lakin, Kan., attends Kansas State University and majors in agriculture education
Jaimee Frederick, Clifton Springs, N.Y., attends Cornell University and majors in agriculture science
Laura Geven, Syracuse, Kan., attends Kansas State University and majors in animal science and industry production management
Shana Hilgerson, Elkader, Iowa, attends Iowa State University and majors in animal science
Kalista Hodorff, Eden, Wis., attends University of Wisconsin–Madison and majors in dairy science
Dawn Klabenes, Chambers, Neb., attends University of Nebraska–Lincoln and majors in animal science
Gloria Koester, Owen Hall, Ind., attends Purdue University and majors in agriculture economics
Jacobus McClure, Las Cruces, N.M., attends University of New Mexico and majors in accounting and finance
Kristi Menson, Dyersville, Iowa, attends University of Wisconsin–Platteville and majors in agriculture education
Emily Mikel, Stafford, N.Y., attends Morrisville State College and majors in dairy science
Jay Moon, Buckhead, Ga., attends University of Georgia and majors in agriculture education
Dexter Ormond, Kennedy, N.Y., attends Penn State University and majors in agriculture engineering
Brittany Rennhack, Watertown, Wis., attends University of Wisconsin–Platteville and majors in agriculture education
Robin Ritsema, Lakin, Kan., attends Fort Hays State University and majors in international business and economics
Abby Sterner, Barto, Pa., attends Delaware Valley University and majors in dairy science
Kristina Wagner, Altoona, Pa., attends Penn State University and majors in plant science
Nate Yates, Rutledge, Tenn., attends University of Tennessee–Knoxville and majors in agriculture business

Graduate Students

Stephanie Geven, Syracuse, Kan., attending Saxion University, pursuing business administration
Rachel O’Leary, Janesville, Wis., attending University of Wisconsin School of Veterinary Medicine, pursuing a doctorate of veterinary medicine