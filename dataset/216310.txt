Managers should be communicating positive and negative feedback to employees on a regular basis. Performance evaluations are an opportunity for positive recognition and restatement of the critical points.
Reviews are important to your business, but also to your employees. Effective performance evaluations do the following:
• Give employees a chance to ask for candid feedback, express likes and dislikes, and gauge performance
• Allow a mechanism for planning, monitoring, and evaluating employee performance and development
• Ensure that defined job expectations and goals are focused and directly aligned with business goals
• Believe it or not, employees want to hear how they are doing and it’s important they hear it from you! This provides further opportunity for feedback, which supports employee growth and development
• Support the organization’s reward, recognition, and other personnel and professional decisions
• If done well, reviews can effectively improve employee morale and retention
There is a structured process in completing an effective performance evaluation that includes the following three steps:
Preparation
Preparation sometimes does not receive as much attention as managers should devote to it. The goal of this stage is to do everything possible to pave the way for a well-formed, productive discussion.
Schedule a meeting time; provide plenty of advanced notice and ensure enough time is allotted to conduct the review. Have the employee submit a self-assessment in advance of the meeting. This self-assessment consists of a summary of the results, outcomes and contributions related to the employee’s job expectations, development plan and goals.
Managers should review their organization’s performance evaluation documentation, rating definitions, recent and prior performance reviews, employees’ self-assessments and notes from the supervisory file, along with employees’ goals and evaluation criteria. Your notes should include a summary of an employee’s contributions, results and any associated work behavior. You also want to elaborate on your comments with very specific examples.
In a recent AgCareers.com poll, employees said the most important thing a manager could do was to provide specific goals and objectives. While you do not need to detail every contribution and result for the entire period, you should highlight key job expectations and any individual or team goals.
Discussion
This simple process includes allowing a two-way dialogue with your team member. Open the discussion by asking your team member to summarize his or her self-assessment. Be sure to listen and acknowledge areas of consensus. You will also want to highlight where you have a differing perspective from your team member’s assessment. Follow this with your assessment.
Articulate your thoughts by considering, “What are the most important points that need to be communicated to the employee about performance during the review?” Share your assessment with the team member by providing both strengths and areas that you’ve previously discussed that need improvement.
Try to anticipate questions from the employee, and most importantly, support your comments through the use of specific examples. Allow the team member to ask questions and address other potential concerns. Be sure to review the core points and clarify the points of agreement as well as differences. Outline any changes or new goals for the next period and follow by assigning deadlines and next steps.
Documentation
It is recommended that you have signatures and the date on your performance documentation before filing. If for some reason your employee does not agree with the evaluation and refuses to sign it, it is okay to note the date you presented it and then sign the copy you presented. Retain the signed document in the employee’s personnel file, and provide the employee with a copy.
Key Takeaways 
When it comes down to it, performance reviews should not be used as a time to take out frustrations and criticisms on employees. Unfortunately, this is how performance reviews get a negative connotation and do nothing for the organization or the employee. Make sure you are communicating both praise and constructive criticism throughout the year and not saving it all for evaluation time.
There should be no surprises during a performance review!
Editor’s Note: AgCareers.com is a leading supplier of human resource services. For further information on this article or to learn more about human resource management, email AgCareers.com at agcareers@agcareers.com.