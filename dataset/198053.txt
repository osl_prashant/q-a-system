Crop calls
Corn: Fractionally to 1 cent firmer.
Soybeans: 2 to 3 cents firmer
Wheat: Fractionally to 1 cent lower
While temps rose across the Plains and Midwest over the weekend, weather didn't generate much enthusiasm in the crop markets overnight, with two-sided trade seen. Droughty conditions across the Northern Plains triggered a bullish response last week, but followthrough buying overnight was limited given the overbought condition of the market. Traders believe drier weather across the Midwest this week will give producers a chance to catch up on fieldwork. A firmer tone in the dollar index limited buying interest in commodities. Also this morning, USDA announced an unknown buyer has purchased 60,000 MT of old-crop soybeans and 60,000 MT of new-crop soybeans.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Livestock futures are expected to see a choppy start to the day as traders reevaluate positions. Cattle are nearing overbought territory and vulnerable to profit-taking, but nearby futures should be supported this morning by the discount they hold to last week's higher cash cattle trade and current feedlot situation. Meanwhile, nearby hog futures are vulnerable to profit-taking due to the premium they hold to the cash index. But a steady-to-firmer tone in the cash hog market to start the week is expected to result in two-sided trade in futures.