Do you trust The New York Times food writers to be arbiters of what important in culinary matters?Not if we’re talking Southern barbecue or Texas smoked brisket or the best bratwurst in Wisconsin.
But the ultimate corned beef on rye? Perfectly seared Porterhouse? Best bagels and lox for noshing?
Yeah, I’ll check their picks for that kind of fare.
So how credible is The Times in rating hot dogs, since they arguably fall somewhere between steaks on the grill and microwaved burritos as summertime gastronomical staples? I’d say somewhere between “the final authority” and “are you serious?”
That ambiguity didn’t stop journalism’s Gray Lady from plunging into a story that purports to rate the nation’s best hot dogs with the gravitas of the pope issuing a papal encyclical on the divinity of the Holy Trinity.
We’ll get to The Times’ trinity of frankfurters in a moment.
But first, consider the bias blatantly on display in the very first words of the newspaper’s “We Taste-Tested 10 Hot Dogs. Here Are the Best” feature article:
“Back when hot dogs were on every list of foods to avoid — alarming additives, questionable cuts, salt and fat galore — home cooks didn’t want to know too much about what was in them.”
Really? To invoke a classic New Yawk retort: “Yeah? Sez who?”
The story went on to state that, “Cooks are different now, and so are hot dogs ... made from better ingredients, with fewer additives.:
And that justified the newspaper’s “first official hot dog blind tasting” project, noting that each brand was gas-grilled until well-browned, then evaluated on “seasoning, beefiness, snap, texture,” with each judge applying the same condiments to all of the samples.
 
The short-short list
There’s lots more word salad about “the intrinsic qualities of the hot dog,” “the melding of meat and bread,” and “how the bun should hug the hot dog closely,” but let’s get to the winners.
For starters, only all-beef hot dogs had even a remote chance of making the finals. Even the beloved organic, kosher and boutique brands had to be 100% beef to make the final cut.
What The Times’ writers never mentioned, however, is that the perfect hot dog is made with bull meat: it’s brighter, beefier and has the preferred “snap,” or bite. We’ll forgive them for that lack of insight, and go straight to their Top Ten, although actually there was a Top Two that got the best accolades:
·        Wellshire Farms Premium All-Natural Uncured Beef Franks. “Smoky, herby — is this fancy? We all loved its levels of garlic and spice.” ($8 bucks for a package of eight).
·        Hebrew National Kosher Beef Franks. “Classic. The people’s hot dog.” ($6.29 for a package of seven).
Believe it or not, every other brand of hot dog sampled was relegated to the newspaper’s “Middle of the Pack” category, which by its very label is excluded from my shopping list. Who cares how a whole bunch of middle of the pack products compare with each other?
That’s like reading a review of the fall lineup of new automobiles, and two models were judged to be “Terrific; handles beautifully, drives like a dream,” and then 20 other models that were lumped into something called, “Well, if you can’t afford quality, here’s a bunch of cars that won’t disappoint you too badly.”
Why even go there?
As The Times archly declared, all the other brands of hot dogs “were good over all, but missed greatness because the sausage was either too sweet, too salty, too smoky or too tough.”
All of the other bands, including hometown favorites Nathan’s Famous and The Brooklyn Hot Dog Company, were found wanting in one aspect or another, with special vitriol reserved for Ball Park brand beef hot dogs, which were described as “soft and mortadella-like in texture,” and in the ultimate insult, labeled as “flaccid.”
Ouch. I don’t care what the category is, “flaccid” is bad.
In the end, you could almost duplicate the final rankings published by The Times simply by doing what expense account aficionados do at a fancy restaurant.
You make your choice by looking only at the right side of the menu. 
The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator