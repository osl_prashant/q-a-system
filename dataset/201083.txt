Crop calls
Corn: 2 to 3 cents lower
Soybeans: 2 to 3 cents lower
Winter wheat: 6 to 8 cents lower
Spring wheat: 3 to 4 cents lower

Corn and soybean futures saw followthrough from yesterday's losses in overnight trade. This further weakens corn's technical stance, as the December contract moved to its lowest level since May 18. An active weather pattern has recharged soil moisture across the bulk of the Corn Belt. Winter wheat futures saw stepped up profit-taking overnight, with lesser losses seen in spring wheat futures amid ongoing crop concerns in spring wheat country. Weekly corn export sales were at the lower end of expectations, soybean sales were well below expectations, but wheat sales topped expectations.
 
Livestock calls
Cattle: Lower
Hogs: Mixed
Cattle futures are vulnerable to followthrough from yesterday's losses after cash cattle trade deteriorated from $122 earlier yesterday to $121 late in the day -- down from mostly $130 last week. This week's softer tone in the beef market and larger showlist gave packers the upper hand in cash negotiations. Meanwhile, pork cutout values firmed $1.67 yesterday to improve packer margins. While that bodes well for the cash market, futures are expected to be mixed this morning as traders are cautious about the cash market after bids softened yesterday.