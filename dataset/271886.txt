BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Tuesday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMay                              120.45  Up   1.50May      117.00  118.50  116.65  118.50  Up   1.50Jul                              122.50  Up   1.50Jul      119.05  120.80  118.50  120.45  Up   1.50Sep      121.05  122.75  120.55  122.50  Up   1.50Dec      124.50  126.20  124.05  126.00  Up   1.50Mar      128.00  129.75  127.60  129.50  Up   1.50May      130.30  131.95  129.85  131.75  Up   1.45Jul      132.50  133.95  132.10  133.90  Up   1.40Sep      134.40  135.80  134.00  135.75  Up   1.35Dec      137.20  138.50  136.75  138.45  Up   1.25Mar      140.80  141.15  140.40  141.15  Up   1.15May      142.60  142.90  142.30  142.90  Up   1.05Jul      144.20  144.60  143.90  144.60  Up   1.10Sep      145.75  146.20  145.45  146.20  Up   1.10Dec      148.15  148.55  147.80  148.55  Up   1.00Mar      149.15  150.60  148.85  150.60  Up    .90