The farming community has stepped up to help a Mont. Hutterite colonyrebuild in the wake of a recent pig barn fire.The fire struck in mid-April at the Milford Hutterite Colony in in Lewis & Clark County, Mont., ripping through the barn and killing more than 3,000 pigs.
Despite efforts of fire crews, the barn was a total loss and no animals could be saved.
The fire sent a shockwave through the colony. Ben Wipf, secretary of the colony, described the situation, saying everyone was "pretty shook up."
For more on the fire, read "3,100 pigs killed on Montana Hutterite colony"
Now the Canada-based Connect OnFarm, which provides feed and nutrition program consultation to the colony, has stepped up in to help.
According to the group's news release, Connect OnFarm donated $10,000 to help the colony rebuild, joining many others offering helping hands or funds within the local farming community.
"Milford Colony is a great customer of Connect OnFarm. But more than that, through our close relationship, they have truly become a part of our family and we want to stand alongside them in this time of need," says Cal Ginter, part of the Connect OnFarm team along with Wes Friesen and Cleuza Friesen. "On behalf of myself, Wes and Cleuza, we are pleased to provide this donation as part of our ongoing support for Milford Colony. All of our thoughts and prayers are with them in their recovery."

While the barn was insured, Connect OnFarm pointed that it is still unclear how much of the loss will be covered or how quickly it will rebuild.