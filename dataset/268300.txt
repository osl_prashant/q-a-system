Eight companies received kudos for products and services on display at the Viva Fresh Expo, through the inaugural Gateway to Innovation Awards.
Dante Galeazzi, president and CEO of the show’s organizer, the Texas International Produce Association, said nearly 40 entries were submitted.  
The categories and winners are:

Most Innovative Fruit Product: Cabefruit Produce LLC — Marvelous Jackfruit as Meat Substitute
Most Innovative Vegetable Product: Love Beets USA — Honey + Vinegar Golden Beets
Most Innovative Technology Application/Software: Mucci Farms — Mucci International Logistics App
Most Innovative Product Solution-Food Safety: ScanTech Sciences — Electronic Cold Pasteurization
Most Innovative Fruit Packaging or POS Solution: Wonderful Citrus — Halos tree and tractor display
Most Innovative Vegetable Packaging or POS Solution: Mucci Farms — Veggies to Go
Most Innovative Value-Added Solution: Mastronardi Produce — Sunset You Make Me Pasta Kits
Most Innovative Equipment: Fox Packaging — Pouch Bagger.

ScanTech’s Electronic Cold Pasteurization, the process of a non-nuclear methodology of irradiation to treat fruits and vegetables, received the Best of Show honors. ScanTech is building a facility in the Lower Rio Grande Valley in McAllen, Texas. It is scheduled to open later this year.
The facility will treat fresh produce from Mexico, most entering the U.S. at the nearby Pharr International Bridge. The process treats for bacteria, pests and mold, and helps increase the shelf life of fresh produce.
The Viva Fresh Expo was April 5-7 in San Antonio.