(CORRECTED) San Miguel Produce Inc. has hired Bob Cummings as vice president of sales and marketing.
Cummings originally started his career in the meat industry, according to a news release, leading to senior manager positions at Hormel, Con-Agra and Farmland Foods. He has been in the fresh produce industry for 18 years, according to a news release, and has a fresh-cut background.
He was at Reichel Foods and most recently at Crunch Pak, where he’s been for 11 years.
“His passion is a great fit for our company culture and we believe that his leadership and experience will help our family farm reach its strategic goals,” San Miguel Chief Operating Office Jan Berk said in the release.
Note on correction: Bob Cummings' name was misspelled in the original article.