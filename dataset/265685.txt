Fishing boat sinks, crew saved hundreds of miles off Hawaii
Fishing boat sinks, crew saved hundreds of miles off Hawaii

By CALEB JONESAssociated Press
The Associated Press

HONOLULU




HONOLULU (AP) — A commercial fishing crew and a federal observer were rescued after their vessel sank and they spent hours in a life raft hundreds of miles off the coast of Hawaii, the U.S. Coast Guard said Monday.
The agency said it received an emergency distress alert from the Princess Hawaii late Sunday morning about 400 miles (644 kilometers) north of the Big Island. A few hours later, a Coast Guard plane got to the area, where rescuers saw a flare and found eight people in a life raft.
The longline fishing boat was mostly submerged with only the stern above water.
Officials said the Coast Guard air crew dropped a radio to the life raft and helped establish communication with the vessel's sister ship, the Commander, which was fishing nearby and went to rescue the survivors. It arrived nearly 12 hours after the distress call and brought the crew aboard, Coast Guard spokeswoman Tara Molle said.
She said the crew was in good condition and was expected to arrive back in Honolulu later this week.
Most longline fishing vessels in Hawaii use foreign crews with no U.S. work visas. The workers cannot legally enter the United States so they are required to live aboard their vessels for the duration of their contracts, often a year or two at a time.
Most workers come from impoverished Southeast Asia and Pacific island nations and are paid between $300 and $600 dollars a month.
The observer on the boat was part of a National Oceanic and Atmospheric Administration program that monitors the actions of commercial fishing crews at sea. Observers log data about catch, interactions with endangered species, vessel conditions and crew safety.
NOAA officials said they could not identify the observer who was aboard the Princess Hawaii. The agency is working with the Coast Guard to determine what role the observer played in alerting authorities to the sinking, spokeswoman Jolene Lau said.
The boat was inspected by the Coast Guard in February, and no safety violations were found. It was in 10-foot (3-meter) seas with winds around 20 mph (32 kph) before it sank, authorities said.
The Coast Guard said in the statement Sunday that it "called the registered owner, who confirmed the vessel had gone out early that morning to fish." The agency said Monday that it could not confirm the name of the owner or any information about the crew.