Recent editorials published in Nebraska newspapers
Recent editorials published in Nebraska newspapers

By The Associated Press
The Associated Press



Omaha World Herald. April 5, 2018
Nebraska should continue to nurture its spirit of welcoming refugees
The number of refugees entering the United States is expected to decrease this year to about 20,000, the Associated Press reports. That's less than half of the 45,000 that President Donald Trump had approved and is well below the 53,716 who entered during 2017.
Nebraska has an admirable, long-standing tradition of welcoming refugees. Even with diminished national numbers, the mission here continues.
The Refugee Empowerment Center, the smaller of Omaha's two resettlement agencies, remains busy, resettling about 100 refugees so far.
"Business as usual," Marilyn Sims, the group's executive director, told World-Herald columnist Erin Grace.
Lutheran Family Services, which laid off much of its resettlement staff last year in the wake of a refugee slowdown, continues its efforts and has been able to line up sponsors for every incoming refugee household.
The number of refugees coming to Omaha from Myanmar has fallen to 32 so far this year, compared with about 166 refugees during the same period for each of the past five years, Grace reported. Omaha received no refugees so far this year from Somalia or Syria, compared with a total last year of 91 Somalis and 63 Syrians. The number of refugees from Bhutan coming to Omaha has remained steady, however.
Since 2002, more than 11,000 refugees from 35 countries have resettled in Nebraska. During 2016, Nebraska led the nation in resettling the most refugees per capita: a total of 1,441 refugees, or 76 per 100,000 Nebraskans.
Nebraska can be proud of the success that local organizations and communities have had over the years in helping men, women and children find new lives here after being uprooted by war and other tumult. Nebraska needs to continue to nurture that spirit of welcoming and support.
_______
The Grand Island Independent. April 6, 2018.
Crime, poverty taking more of Nebraska's taxes
The budget passed this week by the Nebraska Legislature and quickly signed by Gov. Pete Ricketts dealt with some unpleasant choices. A clear example is less money for higher education (the University of Nebraska, state and community colleges) and more money for prisons and poverty.
In the case of crime and prisons, too many people can't or won't obey our laws. Their numbers are growing and the reasons are numerous and often complex. Whatever the causes, the outcomes are damaged or ruined lives, impairments to the safety and well-being of communities, and financial drains on Nebraska's taxpayers.
What can be done about this? Many authorities say that to significantly reduce crime, more money must be spent on lessening the causes. But hard questions quickly arise. Do we really understand the causes? If so, do we really know how to effectively deal with them? How much money will it take? Where will the money come from?
On poverty, the basic issue is people who cannot or will not take care of themselves. In theory, our nation offers almost everyone the chance to enjoy some measure of financial independence. But in fact, such independence is absent for millions of people. According to the Institute for Family Studies, this is because too many people do not follow the basic rules for avoiding poverty. The rules are the same for everyone, rich or poor:
Get at least a high school diploma, get a job, and get married (before having children) . in that order.
Researchers at the institute recently reported that of millennials (ages 28 to 34) who followed these rules, only 3 percent were in poverty. But for those who broke all the rules, a staggering 53 percent were poor.
The study further reports that young people from upper-income families are likely to know and follow the rules. Unfortunately, however, too many who are less-privileged not only don't follow the rules, but they don't even know they exist.
We think the rules make good sense, and society should do more to help people follow them. Until that happens, however, Nebraskans will continue to spend more money on crime and poverty at the expense of education, recreation and other positive programs that improve our quality of life.
Our goal should be to turn this around, and it is time for some fresh thinking on how to deal with crime and poverty.
___
Lincoln Journal Star. April 6, 2018
Tariff threats endanger agriculture, Nebraska economy
The sheer folly of President Donald Trump's now-infamous quip that "trade wars are easy to win" has been laid bare.
Trade wars produce only losers - and Nebraska now stands to be among the starkest of them.
Following the president's insistence on levying tariffs against China, the world's most populous nation proposed a second round of retaliatory tariffs Wednesday. The new list includes corn, beef and soybeans - some of Nebraska's top exports - joining pork from the original proclamation.
For the good of this state, this madness must stop. Americans need adults in the room who recognize the positive effects of trade on our economy mustn't be superseded by scoring political points.
Tariffs would bring utter disaster to Nebraska's already struggling agricultural economy. China is the state's single largest ag trading partner, with $1.4 billion in annual commodities sales representing nearly a quarter of all 2016 farm exports.
China's choice of those three products targets the Midwest - a core Trump constituency - with almost surgical precision. Nebraska leads the nation in beef exports and ranks third and fifth in corn and soybeans, respectively, according to the state Department of Agriculture.
Other industries, too, would suffer as a result of these tariffs, but none in this state would be harmed as agriculture, which supports one in four jobs.
Nebraska's Republican congressional delegation and Trump himself, among others, have expressed justified concerns about China's attempts to skirt intellectual property laws. On that topic, the U.S. must be stern with China - but roping trade into this discussion wreaks unnecessary collateral damage.
Tariffs, again, are the worst conceivable means of achieving Trump's desired end.
On imports, they're taxes ultimately paid by consumers. For exports, producers struggle with vital market access overseas. Both are worse off when political pressures produce protectionism.
The gravity of China's announcement has also overshadowed the juvenile debacle Trump ignited regarding the North American Free Trade Agreement, another boon for Nebraska. He nonsensically threatened to exit the deal over misplaced blame he heaped on Mexico for Deferred Action Childhood Arrivals - though those aren't related in the slightest.
What's most disheartening about these debacles is that they so closely follow a successful trade negotiation by the Trump administration. The KORUS agreement between South Korea and the U.S. was recently approved after a few points of contention were ironed out by both nations.
That outcome represents the requisite mature, measured approach to improving mutually beneficial trade deals, rather than threatening to nuke them over perceived slights.
A hard line on China's intellectual property standards is defensible. Stoking the flames for a trade war, particularly given the economic disaster hanging over Nebraska, is downright reckless.
______
Kearney Hub.  April 4, 2018
Farmers victims of aid formula, high taxes
As policymakers, our state's lawmakers are charged with deciding which schools ought to receive state aid and which schools should stand on their own. Based on legislators' desires, officials with the Nebraska Department of Education employ an extensive list of factors. The numbers help them determine which districts are most in need of aid vs. those with tax bases broad enough to shoulder the burden of public schools without the state's help.
In an extensively researched Weekend Hub report by staff writers Erika Pritchard and Tiffany Stoiber, we learned that among the factors that elevate a school district's needs are poverty level and limited proficiency in English. The most significant drivers of need are enrollment and per-pupil spending measured against other comparable schools.
That is the "needs" side of the formula.
On the other side are school districts' "resources," or to state it in simpler terms, their tax bases. That includes houses, businesses, factories, and other real property that's taxable — and farmland.
We can agree with the school aid formula to the extent that it measures needs vs. resources, two of the most basic and credible factors to steer state aid decisions. However, for all of its complexity, Nebraska's school aid formula is absent one essential factor, and that is the property owners' ability to pay.
Officials depend greatly upon assessed values of property — and we can understand why they do so. Those numbers are available through current records. However, they fall short of credibly identifying resources, especially in Nebraska's rural counties where the values of agricultural land shot up during a spike in commodities markets a few years ago. Those were the days of $8-per-bushel corn. Today, corn is worth $3 per bushel, a fact that seriously erodes a farmer's ability to purchase goods and services on Main Street, and certainly complicates his ability to keep up with the expenses of operating his farm — especially taxes.
Our question to Department of Education officials and to our state's elected officials is: If Nebraska's school aid formula can consider various intricate but important factors about needs and resources, why can it not consider one more factor, the property owners' ability to pay?
It is easy to understand that farmers are struggling today. What is hard to understand is why our state's policymakers have not figured a method to account for that truth. Until we can consider ability to pay, our tax policy is fundamentally and ethically flawed because we're willingly overburdening our state's major industry.
We're banking on a false economy as long as we're willing to place such a heavy tax load on farmers. Nebraskans need to admit, if farmers are hurting, our entire state is hurting.
___