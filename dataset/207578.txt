Although growing organic berries can be challenging for suppliers due to the stringent three-year process, they continue to increase organic berry production to meet demand. 
Wish Farms, Plant City, Fla., offers organic varieties of strawberries, blueberries and blackberries.
 
“We continue to see the demand for organic berries increase, especially for organic strawberries,” said Amber Maloney, director of marketing. 
 
“Growing organically is certainly a challenge, but if the consumer demand is there, growers will continue to take the risk. Rather than persuading consumers to purchase one or the other, Wish Farms is proud to grow both organic and conventional berry varieties.” 
 
Well-Pict Inc., Watsonville, Calif., offers organic strawberries in 1- and 2-pound clamshells. 
 
“Organic produce in general has been trending up, and strawberries are a big part of that upward movement,” said Jim Grabowski, director of marketing. “We have increased our organic acreage slightly, but as I am sure others have mentioned, new organic land is difficult to find and the three-year transition period from conventional grown land to organic land is costly.”
 
Cherie France, marketing manager for Homegrown Organic Farms, Porterville, Calif., said the company has expanded its organic categories across the board. “Most relevant, we’ve been able to extend our organic blueberry season with new acreage in the Pacific Northwest and on the California coast,” she said.
 
Naturipe Farms, Estero, Fla., offers a full line of organic berries: strawberries, blueberries, blackberries, raspberries and cranberries. 
 
“Organic strawberry and organic blackberry production will be great into September,” said Brian Vertrees, director of business development-west for Naturipe. 
 
“Organic blues will be snug as the domestic season wraps up, but on deck is a great import organic blueberry season. Volumes are increasing and we are excited to work with retail partners to create successful plans for the upcoming import season.”
 
Vertrees said he continues to see certified-organic berry demand increase. “Naturipe has focused on increasing our available volume on organic strawberries, organic blueberries and organic blackberries to help meet extra demand,” he said.
 
Naturipe also trialed new packaging  — a new pulp-fiber organic blueberry pint  — this year to better meet organic-minded consumer demand. 
 
“Berries are a key organic item to promote at retail and will help lift the category as a whole,” Vertrees said. “Carrying a robust organic berry line is critical to any produce department that wants to remain competitive.”