The winter storm "Goliath" that swept across the Southern Plains in late December killed, by some estimates, as many as 40,000 dairy cattle and 13,000 beef cattle, with thousands more missing in the weeks following the storm. As of mid-January, total death-loss numbers remained unclear.As devastating as those immediate losses are, veterinarians and producers are expecting animal-health problems associated with the storm to persist for months.
"There is some potential for latent effects of the sustained low windchill temperatures during the recent blizzard conditions," says Ted McCollum, PhD, AgriLife Extension beef cattle specialist in Amarillo.
He says frozen ears and tails, in most cases, will not present a long-term threat to animal well-being. Damage to cows' teats and udders on the other hand, could result in sensitivity, mastitis or partial loss of udder function as cows begin nursing this spring. McCollum recommends monitoring cow body condition into the spring, and keeping a watch on cows and calves to observe nursing behavior.
McCollum also notes that bulls could have suffered frostbite to their penis or scrotum, and he recommends full breeding soundness examinations well ahead of the breeding season.
In addition to ranches and feedyards, the region most affected by the storm also is home to numerous large dairies, and those producers also expect lingering health problems. News reports indicate that many dairies missed one or more milkings as employees were unable to reach the operations for their shifts, which likely will lead to increased incidence of mastitis in those herds.
Shortly after the storm, Clinton Griffiths, editorial director and host of AgDay TV, a Farm Journal Media property, toured the area and conducted interviews with producers. During his visits, Griffiths heard of one large dairy where only six employees, from a normal shift of 80, reported for work during the two-day storm.
Watch this story on "AgDay":

<!--

    function delvePlayerCallback(playerId, eventName, data) {
        var id = "limelight_player_351368";
        if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
            DelvePlayer.registerPlayer(id);
        }

        switch (eventName) {
            case 'onPlayerLoad':
                var ad_url = 'http://oasc14008.247realmedia.com/RealMedia/ads/adstream_sx.ads/agweb.com/multimedia/prerolls/agwebradio/@x30';
                var encoded_ad_url = encodeURIComponent(ad_url);
                var encoded_ad_call = 'url='   encoded_ad_url;
                DelvePlayer.doSetAd('preroll', 'Vast', encoded_ad_call);
                break;
        }
    }

//-->


<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->


While visiting the Hereford, Texas area, Griffiths interviewed Landon and Laramy Friemel, whose family operates a 40,000-head feedyard near Hereford.
"We pulled trucks up and down our feedlot bunks for two days just to get those cattle fed," Landon says. "And half of that feed didn't hit the bunks because the wind was blowing it off the spout into the pens. It was a rough two days but I think we faired better than a lot of people."
Laramy says the family also had significant problems with stocker cattle on wheat pastures. "We had some cattle turned out on wheat and we chased them for 30 miles south of where they were supposed to be but finally got them back," he says. "However we're still missing a few here and there. It was pretty rough." He adds that cattle in the feedyard, where they had access to feed in the bunks, seemed to fare better.
The brothers expect, however, that their cattle will face ongoing health problems. "I think we have a long way to go to get over it," Laramy says. "You can just drive around and you can still see the dead animals that the rendering companies haven't been able to pick up yet because they've been so slammed.Then you have to think about the future with the animals health and how bad this hurt them. You go stand out in the cold and this blizzard for three days and tell me you're not going to be sick later on because of that."