Entrepreneurs tend to do things the rest of us might see as crazy. Shenandoah Valley Organic chicken would fit into that category.
Americans will eat upwards of 91 lb. of chicken this year, and a handful of names you know are very successful at producing and marketing chicken. Tyson, Pilgrim’s Pride, Sanderson Farms, Perdue Foods and Koch Foods account for more than half of U.S. broiler production. Together, ready-to-eat chicken sales by those five companies totaled more than $28 billion last year.
Into that crowded field jumps, Jefferson Heatwole, the co-founder of Shenandoah Valley Organic, headquartered in Harrisonburg, Virginia. His start-up is not just another organic chicken brand, but a new business model. Shenandoah Valley Organic (SVO) aims to become the “microbrewery of chicken.”
What, you must be asking, is “microbrewery chicken,” and how could it relate to beef?
SVO’s business model targets millennial customers that have little trust in big ag institutions, crave an artisan product, yet want to feel connected to a community. Some are calling SVO the “anti-Tyson.”
SVO’s signature chicken brand is called “Farmer Focus,” which is described as “Organic Hand Cut Chicken.” SVO says, “At Farmer Focus, our farmer-owned organic chicken is raised on small family farms committed to the highest ethical standards.”
Attributes of all Farmer Focus chicken:

Farmer owned and raised
Free range
Certified humane, sustainable
No antibiotics, ever
No animal byproducts
Non GMO verified
The code on the label tells you the farm where the chicken was raised

We can argue there’s nothing in that list that makes SVO chicken any better, safer or more environmentally friendly. Little good it will do. Millennials want what they want, and they’ll pay for it.
Consider that last attribute again: The code on the label tells you the farm where the chicken was raised. Consumers can enter a Farm ID code into the company’s website to see the farmer’s story where that chicken was raised.
How long before Tyson and the other chicken companies follow suit? Remember, it’s a $28 billion business to the top five, so you can bet they have people working on it.
The obvious message for beef and pork producers is that a similar system is coming. Consumers are demanding more information, and companies like Tyson, Pilgrim’s Pride, etc., didn’t build successful businesses by ignoring their customers.