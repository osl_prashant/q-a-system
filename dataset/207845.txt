Tualatin, Ore.-based wholesale distributor Caruso Produce Inc. saw two longtime employees retire in the last year, including its president.President Joe Caruso, whose father and uncles started the company in 1945, retired after having been with the company for 50 years. Sam Caruso, his son, succeeded him as president.
Joe Caruso had been president since 1975.
Arnie Franks, a secretary/salesman, retired after 32 years with Caruso Produce.
In other moves at the company, Matt Weber now is senior vice president and Jason Robbins is vice president in charge of sales and marketing.