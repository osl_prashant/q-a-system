After beef prices saw an upward movement in the month of November, prices continued to climb in December ending just before the Christmas holiday with an increase of $3.30 per cwt month over month.
Prices were pretty consistent throughout the month with the most dramatic price change during the week of Dec. 5 to Dec. 12, where prices dropped by $4.50 per cwt. Our group of cattle industry stakeholders and experts who make up the Monday Market Sentiment also guessed pretty consistently each week and came in +/- $1 to $3 per cwt off the actual five-area prices. Their average estimate for the week of Dec. 19 was only $1.06 off the five-area fed cattle price of $110.28 per cwt. Their largest gap in estimation occurred the week of Dec. 5, with an underestimation of $2.84 per cwt.

Boxed beef prices were at $191.77 as of Jan. 23, moving the 27-day average to $196.06. The highest price for the past 27 days was $203.65 and the lowest price was at $189.10. Choice boxed beef saw the biggest increase in price during Dec. 23 to the new year.
For primal rib meats, the price as of Jan. 23 was at $288.89 bringing the 27-day average to $311.99, with a high of $372.25 and a low of $287.76. The highest prices occurred in the beginning of December, then prices continued to fall into the new year.
Although prices have grown more consistent in the last 10 days with an average of $290.10, primal rib prices have been pretty volatile over the past 27 days.


Note: This story appears in the February 2017 issue of Drovers.