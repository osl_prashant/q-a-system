We want to a couple's house the other night and were introduced to Alexa. 
 
I found that Alexa - Amazon's silky-toned "virtual personal assistant" that can help you create shopping lists, read the news, and even play 20 questions  - was captivating.
 
We asked Alexa if she was "prettier" than Apple's Siri, Alexa's less capable AI cousin residing on the iPhone. She said something to the effect "I like all AI."
 
Human interaction with computers will only increase in the years ahead, possibly to the point we know no longer think of artificial intelligence devices as things, but as personalities and allies along life's long road.
 
But there is a danger the tech industry may go far too fast. 
 
For example, this morning I (@tckarst) tweeted a link to a story for the UK's Sun about Amazon's plan to build stores with "virtually" no employees.
 
The headline reads: "SHOP-POCALYPSE Amazon planning to open robotic supermarket staffed by just THREE humans, sources claim;
Online retail giant reportedly developing prototype of a futuristic store which could finally finish off the humble British cornershop"
 
 
 
Well there you have it. Let's fold up shop. The corner grocer is dead.
 
The story said that Amazon is considering a two-story grocery store where robots will collect items for shoppers. Supposedly an upsized version of Amazon's pilot "Amazon Go" store, the new concept store could be as big as 40,000 square feet and could include as many as 4,000 items, including fresh produce. The most notable characteristic of the store, according to the article, is that it could operate with as few as three employees at a time.
 
Pamela Riemenschneider (@produceretailer), editor of Produce Retailer, responded to the tweet about the Amazon concept with this response:
 
"@tckarst they need to ask @FreshandEasy how that works out..."
 
Pamela has a point. As consumers, we are looking for goods and services, but many of us are also looking for connections.
 
Among Fresh and Easy's missteps, a lack of store staffing likely contributed to their underwhelming performance in the U.S.
 

One problem in this triple jump to conclusions, however. An Amazon spokesman said in an email to me that The Sun's report was faulty.
 
"As we told the New York Post, we have no plans to build such a store and their story is incorrect," the spokesman said Feb. 6.

 
Still, someday Amazon or other retailers may have grander ambitions to reduce employee count.  It may be a perilous urge.
 
If a person goes to a grocery store and returns home without one point of human contact with the retailer, how effective can that retailer's business plan be? 
 
Perhaps that is a question I can ask Alexa, though I doubt she will sell her AI friends short.