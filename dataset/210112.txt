In the Gulf Coast region where rain was plentiful thanks to hurricanes Harvey and Irma, re-establishing pastures might be a challenge. Planting an annual forage such as ryegrass might be a good option to graze in the winter, says Vanessa CorriherOlson, forage specialist for Texas A&M AgriLife Extension.
“Annual ryegrass is fairly easy to establish, and it is the most productive of the cool season annual grasses in terms of quantity. It could provide quite a bit of forage in the winter and springtime,” Corriher-Olson says.
When overseeding ryegrass into established bermudagrass or bahiagrass, ryegrass could continue producing into June. In April or May, graze down the ryegrass or harvest for hay to open the canopy for the warm season perennial grasses to break the dormancy cycle.
“For those fields standing in water, the existing warm season perennials are probably going to struggle coming back. If we can reduce that competition from ryegrass it can help the perennials recover after a flood,” she says.
In flooded pastures, make it a priority to test the soil and control weeds in the spring, too.
Producers farther north from the hurricane-impacted areas could also benefit from annual, cool-season forages this winter using winter wheat, oats, small grain rye or ryegrass. When considering this type of program see how it fits into your grazing system, Corriher-Olson says. Each forage prefers certain soil types, temperature range and moisture condition, yielding more growth at particular times of the year. Remember to factor in costs for seed and fertilizer, too.
Some people get sticker shock when they look at the cost of seed, Corriher-Olson says, but consider the potential production and forage quality gains from those cool season annuals for a complete assessment.
 
For more on winter feeding in corn growing regions of the U.S. read the following story:

Feeding Corn and Grazing Stalks Affordable Winter Feeding Options

For more on hay testing in drought impacted areas read the following story:

Drought Impacted Areas Should Test Hay Prior to Winter Feeding

For more on grain buying advice read the following story:

Removing Guesswork from Feed Buying