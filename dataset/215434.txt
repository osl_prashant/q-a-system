Many hurdles remain for cultured meat, the largest of which are related to commercial viability, consumer acceptance, and regulatory issues.
The impact of protein products derived from plant sources, insects or cultured animal cells on livestock and poultry demand is not expected to be significant in the foreseeable future, according to a new report from CoBank’s Knowledge Exchange Division. However, these technologies bear watching.

“Cultured meat developers are in a race to match price and quality to traditional meat offerings, CoBank says. “Products currently in development are prohibitively expensive and years away from widespread commercial viability.”

“The future success of alternative meat lies squarely with rising global demand for protein rather than a battle for the existing market share of animal protein food products,” Trevor Amen, an economist with CoBank, said in the article. “The road to commercial viability and consumer acceptance of cultured meat is long and this type of product is unlikely to have a marked effect on traditional animal protein demand through at least the next decade.”

The alternative protein category is certain to grow in the coming years, allowing pathways for more diversified protein products. However, the alternative protein market will be overshadowed by the current retail market size of $49 billion in sales for the entire meat and poultry category.

Commercial Viability and Consumer Acceptance
Rising global incomes will continue to drive consumers to a higher protein diet, says Dennis DiPietre, and economist from Columbia, Mo. CoBank says global gross domestic product is projected to grow by $38 trillion from 2016 to 2030, generating a 46% increase in meat and poultry consumption. Technology companies and alternative protein providers are exploring new protein products.

“The timeline for commercial viability of cultured meat products remains the greatest unknown,” said Amen in the article. “The consensus projection points to an initial market introduction in the next 3 to 5 years, most likely in restaurants and specialty stores and offered at a premium price to traditional meat offerings.”

Supermarket adoption of these products is projected to take another 2 to 3 years as the technology becomes more affordable and acceptable to consumers.

Technological and Regulatory Hurdles
CoBank believes the timing and degree of market penetration for meat alternatives will largely depend on advancements in technology that reduce price and improve quality attributes.

“In addition to start-up companies, we’re seeing agribusiness leaders investing in research and development projects surrounding meat alternatives,” said Amen. “Similar projects are also underway in China, Israel, Japan and France.”

Newly created cultured meat products will also need a regulatory framework before entering the market. Both the FDA and the USDA are closely monitoring developments in the cultured meat industry. It is unlikely that the agencies will rule on terminology that can be used to describe and market cultured meat until the technology is more developed.

A brief video synopsis of the report “Lab-grown Cultured Meat - A Long Road to Market Acceptance” is available on the CoBank YouTube channel. The full report is available to media on request.