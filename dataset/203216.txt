It might be an understatement to say 2016 was a tough year for a lot of people, and Denver-based Chipotle Mexican Grill was no exception. 
 
The chain faced a class-action lawsuit and struggled to regain customers and sales after E. coli and norovirus outbreaks were linked to some of its restaurants in the latter half of 2015. 
 
Chipotle revamped its food safety protocols throughout 2016 in response to the outbreaks, then announced recently it plans to install air purifying equipment and ice machine sanitizers at its 2,200 exisiting restaurants and all new stores, according to Food Safety News. 
 
The air purifiers, from Florida-based RGF Environmental Group Inc., are designed to cut down on mold, bacteria and viruses in the restaurants, FSN reported. 
 
The company recently released its fourth quarter results, which saw profits of $1.04 billion versus 2015's $997.5 million, according to The Wall Street Journal, so things are looking up for the company, but same-store sales in the quarter dropped 4.8%, more than the 3.5% Chipotle was expecting. 
 
Chipotle chief financial officer Jack Hartung said high avocado prices he attributed to drought in California and labor issues in Mexico cut into profits in the quarter, the Journal said.
 
To improve sales this year, the company says it plans to simplify its bafflingly complex store-level hiring system (which asks every restaurant employee to interview every job applicant) and launch its biggest ad campaign ever.
 
Part of that promo plan, according to Advertising Age, is "a scripted TV series for kids."
 
I couldn't find many more details about this show, although CNBC says it will "focus on real ingredients."
 
I'm not sure what, precisely, Chipotle means by "real" ingredients - as opposed to what, imaginary? - in this context, but the chain has been vocal in its opposition to "industrial" sources and its commitment to using local suppliers when possible.
 
"We care deeply about where our ingredients come from. While industrial farming practices have evolved to maximize profits and production, we make an extra effort to partner with farmers, ranchers, and other suppliers whose practices emphasize quality and responsibility," the "Food With Integrity" portion of its website states.
 
"Quality and responsibility" are of paramount importance to companies in the produce industry - you don't survive for long otherwise.
 
I don't fault Chipotle for not wanting to serve mystery meat or "guacamole" that tastes more like green mayo, as some fast-food restaurants do, but I'm a bit leery about its forays into TV. 
 
In 2014 the chain produced a miniseries with Hulu called "Farmed and Dangerous," a satire that cast "Big Ag" and "Big Food" as the bad guys and a free-range cattle rancher as the hero. 
 
Its animated commercials "Back to the Start" and "The Scarecrow" also paint bleak pictures of havoc wreaked on an idyllic world by large-scale ag operations. 
 
(Also in 2014, it blamed high avocado prices on climate change rather than normal fluctuations in the avocado market.)
 
I'm all for a TV series that shows kids how animals are raised or how tomatoes, lettuce, avocados and limes are grown - that can only have positive implications for the produce industry. 
 
But if it's along the same lines as the company's other productions and equates large-scale growing operations with corrupt, profit-hungry corporations, that cynical attitude is doing nobody - including Chipotle and its kid viewers - any favors.
 
Amelia Freidline is The Packer's copy chief. E-mail her at afreidline@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.