BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Wednesday:
Open  High  Low  Settle   Chg.ORANGE JUICE                          15,000 lbs.; cents per lb.                May     138.95 139.65 137.75 139.25  +1.00Jun                          139.90   +.90Jul     140.00 140.15 138.60 139.90   +.90Aug                          140.70  +1.00Sep     140.15 140.70 139.75 140.70  +1.00Nov     141.50 141.60 141.50 141.55   +.90Jan                          142.35   +.90Feb                          142.75   +.85Mar                          142.75   +.85May                          143.50   +.85Jul                          143.60   +.85Sep                          143.70   +.85Nov                          143.80   +.85Jan                          143.90   +.85Feb                          144.00   +.85Mar                          144.00   +.85May                          144.10   +.85Jul                          144.20   +.85Sep                          144.30   +.85Nov                          144.40   +.85Jan                          144.50   +.85Feb                          144.60   +.85Mar                          144.60   +.85Est. sales 1,105.  Tue.'s sales 1,074   Tue.'s open int 13,682