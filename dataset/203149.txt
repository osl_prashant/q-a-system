Favorable weather in Florida has brought on ample supplies of a variety of Florida citrus items, grower-shippers say.
"It's peak season now for Florida grapefruit, and we have fresh Florida oranges, tangerines and tangelos out," said Sarah Spinosa, fresh marketing manager for the Florida Department of Citrus.
The state's grapefruit crop should reach 9 million boxes, and orange volume should be about 71 million boxes, according to the U.S. Department of Agriculture.
"The quality of Florida citrus is good," Spinosa said. "We have had all the environmental factors Florida is known for - plenty of sunshine and abundant rain - that contribute to great tasting and juicy oranges."
Heller Bros. Packing Corp., Winter Garden, Fla., planned to start valencia oranges and honey tangerines in late January, said Rob Rath, sales manager.
The company was packing "excellent quality" grapefruit in mid-January.
"It doesn't eat any better than what we're shipping right now," he said Jan. 13.
Heller Bros. started shipping grapefruit the first week of October, as usual, and expects to continue its grapefruit deal until mid- to late March.
Weather has been "perfect," he said, with cool nights and warm days.
Volume was down slightly from past seasons.
"Everybody's a little bit lighter than in the past with citrus greening and some of the other issues," he said.
Prices have been decent, he said.
The company's honey tangerine crop also is doing well, he said, and should be similar to past years in terms of volume and quality.
Seald Sweet International, Vero Beach, Fla., was preparing to ship Mexican citrus in mid-January and already was shipping grapefruit, oranges and tangerines from Florida, said CEO Mayda Sotomayor-Kirk.
So far, the season has been going well, she said.
"We skirted a hurricane here," she said, and the region had been enjoying good weather with no freezes.
"We could always use a little bit more rain for sizing," she added.
Duda Farm Fresh Foods, Oviedo, Fla., will harvest a variety of citrus fruit, including red grapefruit, hamlin and valencia oranges and easy-to-peel mandarins, said Nichole Towell, director of marketing.
"Our tangos - easy-to-peel mandarins - are expected to do very well this year, since the fruit is seedless, high-quality flavor and trending with consumers," she said.
The company also participates in Fresh from Florida, a holiday and spring promotion that leverages the Florida-grown name on packaging, she said.
"This partnership is very well received with consumers because they actively seek products that are locally grown in Florida during the winter and spring months."