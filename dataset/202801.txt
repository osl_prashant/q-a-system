TORONTO - As Ontario Food Terminal buyers enjoy protection from rain, snow and sun thanks to new covered walkways and an overhead bridge, the next round of major construction is about to begin.
"We're trying our best to provide one of the best market facilities on the continent and I think we're succeeding," said general manager Bruce Nicholas.
The parking deck that acts as a roof for the farmers market is being upgraded, and Nicholas said new numbered parking spots connected to the walkway have reduced traffic congestion significantly.
"The buyer can now park in a designated spot and have their order delivered from any of the growers to that central location, as is done in the main buyer's court," he said.
In addition, buyers can pull into the spots any time after midnight. In the past, they couldn't park in front of the farmers' stalls before 6 a.m. to ensure there was enough space for the farmers to get in.
Renovations to the front entrance are also complete.
Along with five automated gates, a second tractor trailer entry gate about 700 feet from the entrance is open from 2-10 a.m., which keeps the big rigs from blocking the main entrance.
Drivers of smaller trucks can now enter through the main gate with a proximity access card located in the cab of their truck.
During the next two years, Nicholas said the east side of the property will be redeveloped, including a ring road around the entire site.
The plan includes 20 new buyer's docks, each 40 feet deep, while the 20 existing docks will be extended from 20 feet to 40 feet to line up with the new ones.
"The new southeast docks will accommodate tractor trailers, which we don't have enough space for right now, and the 20 additional docks should mean quite an improvement for buyers," Nicholas said.
North of the expanded docks, a second cold storage shipping and receiving area will be built with nine receiving doors, while the existing cold storage will be expanded east with eight doors.
A fully enclosed waste separation facility is also in the works to streamline separation and keep the trash compactors from freezing in winter.
Once City of Toronto permits come through, Nicholas expects construction to begin by mid-July. The entire project may take 2½ years.

"To do all this while keeping the market running is a complete nightmare, but we'll get it done," he said.