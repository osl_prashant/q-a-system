Limoneira studies battery efficiency
Santa Paula, Calif.-based Limoneira is involved in a project with battery-powered automobile manufacturer Tesla that looks at energy storage in batteries in order to cut costs for peak-time energy use, said John Chamberlain Limoneira’s marketing director.
“A new project in partnership with Tesla is studying the efficacy of battery systems paired to one of our solar arrays — 5 acres, 6,400 photovoltaic panels — to reduce energy demand from utilities during the day, which is the high-cost period, and offset that use with less expensive night time energy that is stored in the battery,” he said.
Limoneira also has a natural wastewater system, which is a low-carbon footprint project with a series of gravity-fed ponds that circulate and clean water with natural vegetation and fine gravels, Chamberlain said.
Ultraviolet rays remove any bacteria in the water to achieve Title 22 drinking water standards, he said.
“Water is currently used for landscape irrigation, and as recycled water becomes more of the norm in daily use and approved for food safety, we will irrigate our orchards with it,” Chamberlain said.
 
Stemilt Growers to use solar power
Wenatchee, Wash.-based Stemilt Growers LLC soon will have solar power in its distribution center in Wenatchee, said Roger Pepperl, marketing director.
It’s the company’s first venture into solar power, he said.
“We have 100 solar panels going on this building and will become the largest producer of solar energy in Chelan County, producing about 10% of the facility’s power needs, initially.”
Stemilt already gets much of its power from water coming out of nearby mountains, Pepperl said.
“We are excited to bring these sustainability elements to our new building and learn more about the benefits of solar,” he said.
Stemilt also offers all employees inlets to free medical care through its Stemilt Family Clinic.
The clinic, which has a doctor and nurse practitioners on staff, provides free primary health care to employees and their dependents, Pepperl said.
The clinic also has a pharmacy that fills prescriptions for no cost, Pepperl said.
“Whenever we tell people about it, they find it hard to believe, but our family owners, the Mathisons, are so passionate about the Stemilt team members and their well-being that they have made this clinic a reality,” Pepperl said. “It’s a huge benefit for our employees and available to anyone who works here from Day 1 of their job.”
 
Taylor Farms adds cogeneration
Salinas, Calif.-based Taylor Farms recently added cogeneration to its renewable energy mix at its Gonzalez, Calif., plant, said Nicole Flewell, the company’s director of sustainability.
“That plant has wind turbine, solar and now cogeneration,” she said.
The cogeneration system uses natural gas to produce power.
“We’re capturing the waste heat off that and reducing the thermal energy off that, as well as reducing the baseline power,” Flewell said.
Now, the plant occasionally produces as much power as it consumes, averaging about 90%, Flewell said.