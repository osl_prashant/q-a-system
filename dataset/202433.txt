A package of goldenberries labeled with the U.S. Department of Agriculture organic seal raised suspicions in early March, causing an investigation into the origin of the berries.
The berries, marketed as Pichuberry, from Colombia, caught the eye of Sam Welsch, president and CEO at organic certification company OneCert, Lincoln, Neb.
"The problem with this product was obvious just by looking at the label, there was something not right," Welsch said. "There was a USDA organic sticker on the product, but the rest of the label had no mention of organic and did not have an organic certifier identified."
Fresh organic goldenberries are grown exclusively in Colombia and distributed by Bogota, Colombia-based Andes Fruit, according to the company.
"They bought conventional fruit from another supplier in Colombia and sold it as organic," Matt Aaron, general manager of North America for Andes Fruit, said in early March.
Pichuberry is calling the mistake an oversight.
"Pichuberry has recently learned about the quality escape concerning an incorrect organic certificate used as part of a repository," the company said in a statement. "It appears the certificate reflected an old supplier for organic pichuberry/goldenberry products which mentioned CERES - a certification body from Europe."
Welsch contacted the certifier listed on the certificate, a representative of CERES, who confirmed his suspicions.
"We made some inquiries, and the certificate provided was false."
Michelle Person of the USDA-Agricultural Marketing Service (which oversees the National Organic Program) public affairs office confirmed the case was reported to the NOP for review.
"NOP has received the complaint and is investigating the matter," she said in an e-mail. "We cannot comment on the specifics of an ongoing investigation."