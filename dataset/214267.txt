Avocados From Mexico has elected Ramón Paz-Vega as chairman to its board of directors.
Paz-Vega has 35 years of industry experience. He served as vice chairman for APEAM from 2000-2002, according to a news release, and is currently a strategic advisor for the organization.
For more than 15 years, he worked as a professor at the Universidad Don Vasco and at the Technologico de Monterrey, the release said, and also was a professor for six years at the Business School of the Universidad de Monterrey.
Paz-Vega currently grows avocados in Mexico and is serving as secretary for the Board of the International Alliance of Fruit and Vegetable Exporters for the Advancement of Social Responsibility in Mexico, the release said.
Since 2013, he has been a member of AFM’s board.
The organization also elected a board of directors:

Mike Browne;
Ron Campbell;
Armando Lopez;
Ygnacio Valerio;
Adrian Iturbide;
Gary Caloroso; and
Carlos Genel.