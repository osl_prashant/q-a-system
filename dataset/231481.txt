National forest acreage could go to Arizona under land swap
National forest acreage could go to Arizona under land swap

The Associated Press

FLAGSTAFF, Ariz.




FLAGSTAFF, Ariz. (AP) — Thousands of acres of national forest land near Flagstaff could be transferred to the state under a proposed exchange as part of an effort to meet a federal commitment to the Hopi Tribe.
The Arizona Daily Sun reports that in exchange for federal land, the state would provide large tracts of land elsewhere in northern Arizona to the Hopi Tribe under a 1996 agreement approved by Congress to resolve a long-festering dispute with the Navajo Nation.
The Flagstaff-area property includes acreage west of Walnut Canyon, and land near the Forest Highlands development.
The office of Sen. John McCain and the Arizona State Land Department began preliminary discussions last year.
McCain spokeswoman Julie Tarallo says the senator will remain engaged with local communities before a proposal moves forward.
___
Information from: Arizona Daily Sun, http://www.azdailysun.com/