Fall calving season is (or soon will be) upon the Oklahoma ranches that have fall and winter calving. An issue facing the rancher at calving-time, is the amount of time heifers or cows are allowed to be in labor before assistance is given. Traditional text books, fact sheets and magazine articles stated that "Stage II" of labor lasted from 2 to 4 hours. "Stage II" is defined as that portion of the birthing process from the first appearance of the water bag until the baby calf is delivered. Research data from Oklahoma State University and the USDA experiment station at Miles City, Montana clearly show that Stage II is much shorter, lasting approximately an hour in first calf heifers, and a half hour in mature cows.Table 1. Research Results of Length of Stage II of Parturition

Location of Study


No. of Animals


Length of Stage II


USDA (Montana) *


24 mature cows


22.5 min.


USDA (Montana) *


32 first calf heifers


54.1 min


Oklahoma State Univ. **


32 first calf heifers


55.0 min

*Doornbos, et al. 1984. Journ. of Anim. Science: 59:1
**Putnam, et al. 1985. Therio: 24:385
In these studies, heifers that were in stage II of labor much more than one hour or cows that were in stage II much more than 30 minutes definitely needed assistance. Research information also shows that calves from prolonged deliveries are weaker and more disease prone, even if born alive. In addition, cows or heifers with prolonged deliveries return to heat later and are less likely to be bred for the next calf crop. Consequently a good rule of thumb: If the heifer is not making significant progress 1 hour after the water bag or feet appear, examine the heifer to see if you can provide assistance. Mature cows should be watched for only 30 minutes.If she is not making progress with each strain, then a rectal examine is conducted. If you cannot safely deliver the calf yourself at this time, call your local veterinarian immediately. Before applying chains and beginning to pull, make certain that the cervix is fully dilated.
Most ranches develop heifers fully, and use calving ease bulls to prevent calving difficulties. However, a few difficult births are going to occur each calving season. Using the concept of evening feeding to get more heifers calving in daylight, and giving assistance early will save a few more calves, and result in healthier more productive two-year-old cows to rebreed for next year. For more information on topics concerning assisting cows and heifers at calving time, download and read an Oklahoma State University circular E-1006 "Calving Time Management For Beef Cows and Heifers". This free publication can be downloaded from this website.