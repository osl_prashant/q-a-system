Bancroft, Wis.-based RPE plans to showcase its new SteamPak Mini at its Fresh Summit booth Oct. 20-21.
The product is a single serving of potatoes that can be cooked in four minutes in a microwave, according to a news release. The item is designed at a snack or side dish and aims to address consumer desires for smaller packs in the face of concerns about food waste.
RPE will also be featuring its Tasteful Selections Take & Shake single-serve potato cups, which won an Impact Award last year. Some changes have been made to address consumer preferences, according to the release.
Flavors available now are Black Pepper, Rosemary & Thyme and Cheddar Cheese, and a new flavor — Sea Salt & Cracked Pepper — is on the way.
RPE also will display its Old Oak Farms Party Potatoes.
Preparing dishes at booth No. 4669 will be chef Joey Elenterio, a Michelin star recipient.