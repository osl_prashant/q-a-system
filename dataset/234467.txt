The National Mango Board, Orlando, Fla., has been working with university researchers on a project to improve mango packaging, and the results are expected to be available this summer.
Objectives for the project, which started in August, were developed by a packaging task force of growers, packers, exporters, importers and retailers.
“The mission of the task force was to identify the packaging and palletization issues affecting the mango supply chain and to emphasize the steps that are necessary to improve the current practices and reduce shrinkage, while also increasing mango movement at the retail level,” said National Mango Board marketing manager Angela Serna.
“The results of the project will be made available as guidelines to the mango industry.”
California Polytechnic State University researchers have been working to design a pallet that meets chain store requirements and allows for “excellent vertical ventilation during container transit,” according to the National Mango Board.
Researchers were also asked to create corrugated trays that work with the new pallet.
Important considerations in the development of the new trays are cost, strength, cooling rates, shipping density and ability to protect the fruit.
The trays also need to work well on marine and truck containers and in forced-air cooling and ripening rooms.
The packaging task force also asked that sustainability attributes be considered, as those are becoming a priority for numerous retailers.