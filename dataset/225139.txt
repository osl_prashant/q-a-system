The Vidalia Onion Committee has added R.T. Stanley to the Vidalia Onion Hall of Fame.
Stanley, the former owner of Vidalia, Ga.-based Stanley Farms, which is now Generation Farms, Lake Park, Ga., was honored at the committee’s annual banquet Feb. 10.
Stanley, who served for a decade-plus as chairman of the committee, helped form the marketing order for Vidalia onions.
“He was one of the original pioneers,” said Bob Stafford, interim executive director of the committee and director of the Vidalia Onion Business Council. “He was very instrumental in the trips to Washington and so forth, to Atlanta, while they were putting this marketing order together.
“He did a lot of interviewing when we would have news people down, he was always able and willing, very capable, and he’s always been a farmer,” Stafford said.
 
Grower of the Year
The Grower of the Year award went to Omar Cruz, an agronomist who works with Glennville, Ga.-based Bland Farms.
“We were so fortunate to have a person of Omar’s caliber,” Stafford said. “He’s so knowledgeable (that) most of the people in the industry call him at some time or other during the seasons to ask for growing advice. He seems to know so much about what it takes.
“He’s one of the ones that originally started talking about heat units, (which) it takes to grow onions,” Stafford said. “He’s been very helpful to the whole Vidalia onion industry.”
Stafford said the committee is excited about the quality of its product this year.
“We’ve made a lot of headway with our research programs,” Stafford said. “I think this coming year you will see a definite increase ... in the flavor and the shelf life and the looks. That’s what we’ve really been thriving on and put a lot of time and money and effort into it, and we’re real proud of a lot of the results that we’ve gotten.”
In the last five years, the committee has increasingly focused on research rather than promotion.