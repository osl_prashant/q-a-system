Russia's Supreme Court rules against Navalny
Russia's Supreme Court rules against Navalny

The Associated Press

MOSCOW




MOSCOW (AP) — Russia's Supreme Court has upheld a criminal conviction against opposition leader Alexei Navalny and his brother.
The conviction was the reason given for why Navalny was barred from running in the March presidential election.
Navalny and his brother, Oleg, were convicted in 2014 of defrauding a cosmetics company in a trial that was widely described as a political vendetta.
Following unsuccessful appeals, Navalny turned to the European Court of Human Rights, which in 2016 ruled in his favor. The Supreme Court ruled on Wednesday against a retrial, upholding the conviction that gave Navalny a suspended sentence and sent his brother to prison for 3½ years.
Navalny said after the hearing that the ruling shows Russia's refusal to respect its international obligations.
___
This story has been corrected to show that Oleg and Alexei Navalny were convicted in 2014 of defrauding a cosmetics company, not embezzling timber worth $500,000. Alexei Navalny was convicted of embezzling timber in 2014.