BC-CO-Denver Area Cash Grain, CO
BC-CO-Denver Area Cash Grain, CO

The Associated Press



GL—GR110Greeley, CO    Mon Feb 26, 2018    USDA-CO Dept of Ag Market NewsDaily Grain Bids for Denver and Surrounding AreasSpot bids to producers for grain delivered to terminal and countryElevators.  Bids dollar/bu. except for Barley which is dollar/cwt.Bids are as of 3:00 PM MST.Bids         Change (cents)US 1 Hard Red Winter Wheat       4.08-4.23    9 higherUp to 12 percent proteinUS 2 Yellow Corn                      3.69    3 higherUS 2 Barley                           - -     not availableSource: USDA-CO Dept of Ag Market News Service, Greeley, COTammy Judson, Market Reporting Assistant (970)353-975024 Hour Market Report (970)353-8031Greeley.LPGMN@ams.usda.gov www.ams.usda.gov/lsmarketnewswww.ams.usda.gov/mnreports/GL—GR110.txt1430M   tj