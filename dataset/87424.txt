Corn




The national average corn basis firmed a penny from last week to 8 1/4 cents below May futures. The national average cash corn price firmed 2 1/2 cent from last week to $3.58.
Basis is weaker than the three-year average, which is 2 1/4 cents above futures for this week.












Soybeans





The national average soybean basis firmed 3/4 cent from last week to stand 28 3/4 cents below May futures. The national average cash price is 4 1/4 cents lower than last week at $9.10 1/2.
Basis is softer than the three-year average, which is 1 1/2 cents above futures for this week.












Wheat





The national average soft red winter (SRW) wheat basis softened 7 1/2 cents from last week to 17 1/4 cents below May futures. The average cash price declined 2 1/4 cents from last week to $4.14. The national average hard red winter (HRW) wheat basis softened 1/2 cent from last week to 90 1/2 cents below May futures. The average cash price improved 5 cents from last week to $3.39 1/4.







SRW basis is firmer than the three-year average of 18 cents under futures. HRW basis is much weaker than the three-year average, which is 38 cents under futures for this week.










Cattle




 




Cash cattle trade averaged $124.33 last week, down $3.05 from the previous week. Cash cattle trade is expected to be weaker this week. Last year at this time, the cash price was $133.90.
Choice boxed beef prices are 34 cents lower than last week at $209.37.  Last year at this time, Choice boxed beef prices were $222.21.











Hogs





 




The average lean hog carcass bid dropped $1.95 over the past week to $64.65. Last year's lean carcass price on this date was $66.70.
The pork cutout value firmed 23 cents from last week to $74.57. Last year at this time, the average cutout price stood at $77.57.