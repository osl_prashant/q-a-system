Wisconsin farmers interested in growing industrial hemp
Wisconsin farmers interested in growing industrial hemp

The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — Wisconsin farmers strapped by stagnant dairy and grain markets are expressing interest in growing industrial hemp this year.
More than 50 producers have applied since March for one-time licenses to grow industrial hemp, the Wisconsin State Journal reported .
"I'd say interest from farmers is almost extreme," said Brian Kuhn, the director of the plant industry bureau at the state Department of Agriculture, Trade and Consumer Protection. "I wouldn't be surprised if we had upward to 100 applications by the time it's all said and done."
Farmers should be careful not to rely on the crop to solve their financial problems, Kuhn said. Farmers also must pay attention to rules for selling hemp, which differ from rules for selling corn, soybeans and other mainstream crops, he said.
"I'm telling farmers to be very cautious," Kuhn said. "I don't look at this as the savior for the farm economy. The farmer who's on the edge shouldn't be rushing out there to put 500 acres into hemp. While it's got great potential and great promise, it could also cause great harm if they haven't worked through the details of the market side."
Ken Anderson, the owner of seed company Legacy Hemp, is building the state's first grain processor in Prescott. The state needs more processors and receiving centers where hemp grain can be cleaned, conditioned and stored, he said.
"The grain doesn't do very well as soon as you take it off of the plant," Anderson said. "Farmers can lose an entire crop if they don't have a post-harvest plan in place and get it cleaned and conditioned quickly after harvest."
State lawmakers approved a research pilot program in November allowing farmers to grow industrial hemp. The crop has many high-tech, health, manufacturing and food applications. Hemp can be used for rope, clothing, milk, soaps, building materials and biofuels.
___
Information from: Wisconsin State Journal, http://www.madison.com/wsj