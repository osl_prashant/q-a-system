The Philippines is consistently a top 10 market for U.S. red meat exports, and a particularly strong destination for raw materials imported for further processing. But just before the Thanksgiving holiday, the Philippine Department of Agriculture issued a new decree aimed at preventing smuggling of imported agricultural products and evasion of import tariffs. This has created challenges for legitimate Philippine meat importers who must now re-confirm their orders, and raised concerns about delays and interruptions for U.S. pork and beef products entering the market.Joel Haggard, U.S. Meat Export Federation (USMEF) senior vice president for the Asia-Pacifichas more details in the audio report below.Haggard notes that this type of market access issue periodically surfaces in the Philippines, which is a large importer of red meat but also has significant domestic pork production. USMEF is working with the USDA office in Manila to keep exporters informed of the situation, and hopes to minimize any interruption of red meat shipments to the Philippines.


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.