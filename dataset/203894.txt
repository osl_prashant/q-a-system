The Mission Viejo, Calif.-based Hass Avocado Board has commissioned nutrition research into the role of avocados and infants and has found that health professionals increasingly are recommending the fruit to help consumers improve their physical and mental well-being.
For the first time, in 2020, the Dietary Guidelines for Americans issued every five years by the U.S. Department of Agriculture and the Department of Health and Human Services, will look at children up to 24 months, Escobedo said.
Until now, the guidelines have applied to those who are 2 years old and older.
In anticipation of the new guidelines, HAB has commissioned two studies conducted by several researchers that looked at the role avocados play in the nutritional health of mothers and infants during the pre-pregnancy, pregnancy and lactation periods.
"We believe that avocados can play a role in early childhood as well as during pregnancy and lactation periods for the mother and the child," he said.
Researchers sought out optimal foods that are low in sugar but rich in nutrients, texture, consistency, neutral flavor profile.
"A lot of parents, without realizing it, are serving foods that are too sweet or too salty for a child," Escobedo said.
One study suggests that avocados can play a role because they are nutrient dense, contain good fats, have the right consistency, have a neutral flavor profile and have a soft texture, so they can be pureed or mashed, he said.
The paper, titled "The Role of Avocados in Complementary and Transitional Feeding," was published in the journal Nutrients.
The board also discovered some good news in its 2016 tracking survey of health professionals, Escobedo said.
"Health professionals are a very influential group of people in our country," he said.
They often make recommendations to consumers about how to choose nutritious foods, so the board regularly tracks the attitudes of registered dietitians, nurse practitioners, physicians and other healthcare professionals. 
This year's survey showed some positive attitudes about avocados.
When health professionals recommend that people increase their consumption of fruits and vegetables, 96% included avocados in 2016, up from 50% in 2013.
When recommending increasing consumption of foods with unsaturated fat, 98% recommended avocados compared with 54% three years ago.
In 2016, 77% recommended avocados as part of a heart-healthy diet versus 31% in 2013.
Many health professionals used to perceive avocados as fattening, Escobedo said, but in 2016, two-thirds of them recommended the fruit for a weight loss diet compared to 8% in 2013.
Escobedo attributes much of the success the industry has had spreading the word about the health benefits of avocados to the board's Love One Today campaign.
Launched in 2013, the campaign is a call to action that encourages consumers, especially those who eat avocados regularly, "to buy one today, not tomorrow," he said.
The program has its own logo, which has been licensed to several suppliers and retailers, as well as its own website, loveonetoday.com.
It's a unifying campaign that allows the industry to speak with one voice as it focuses on two nutrition attributes that score high among consumers - naturally good fats and cholesterol-free - and it "gives permission to consumers to love one today," Escobedo said.
Retailers, importers and marketers have used the Love One Today mark on consumer packaging, cartons and on their websites, he said.
"It is becoming widely recognized that consumers associate avocados with good health," he said. "(Licensees) are promoting nutrition benefits to consumers who we know appreciate the nutrition benefits."
In the past, taste has been the main reason why consumers purchase avocados, he said.
Today nutritional benefits are the No. 1 reason or are tied with taste.
"That's a great place to be," Escobedo said.