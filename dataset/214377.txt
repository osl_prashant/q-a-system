Variations occur in every cowherd. When evaluating the cowherd, many producers would say they have a uniform bunch of females while others know there is a wide array of variations with their herd. Cattle should meet the producer’s goal(s), so they may have a specific breed, size and mating program to achieve a profitable operation. Some tools used to determine the “right” type of cattle might be visual appraisal, EPD (Expected Progeny Differences), production records, and DNA testing. All these tools have value in making critical management decisions as well as other information that can be gathered.
SDSU Calf Value Discovery Program
The SDSU Calf Value Discovery program provides producers with feedlot performance and carcass characteristics for individual animals from previous mating decisions. This information is one more tool that is helpful in making selection and/or mating decisions. The table below provides producers with the pen average as well as three individual herd averages.
Understanding the Data
A few details to understand the information in Table 1. Animals were selected for harvest based on an estimated backfat of 0.40 inches or if the animal was at risk of heavy weight carcass discounts (> 1,050 pounds). Cattle were shipped in semi loads on May 3rd, May 24th and June 14th. This year the carcass base price started at $210/cwt, hit $224 for the May 24th group and dropped slightly to $220/cwt for the final load. Initial feeder calf values were estimated using the weighted average feeder steer prices obtained from the South Dakota Auction Market Summary report (USDA Agricultural Market Service report SF_LS795) for the week ending November 7, 2016. These prices were regressed on selling weights resulting in the following equation:
Price ($/cwt) = 285.46 + ((Initial Wt/100) * (-39.87)) + (((Initial Wt/100)^2) * (2.37)); r2 = 0.98.
Comparing the pen average to the mean USDA carcass grade traits (Table 2) quality grade was higher, HCW was similar and LM area, yield grade and fat thickness were slightly lower. Similar averages between these cattle and the USDA carcass means, may indicate that these cattle owners are making the right decision. However, remember that half of the cattle are greater than the average and the other half are less than the average.
Producer Analysis
The challenge for producers is to identify those animals that produce progeny that are above or better than the average. Additionally, comparing cattle to the other producers with different goals or objectives is not the correct benchmark. Three producers (denoted at A, B and C) with greater than 15 steers enrolled in the CVD program are shared in the table for some possible discussion points.
Producer A
What can be seen in Producer A’s closeout? Producer A averaged $1,388 income per steer after paying the costs associated with the finish phase; however, the low steer income was $939 compared to the high steer income at $1,606. This is a difference in revenue is $667. More animals similar to $1,606 steers should be produced. Based on the data provided to producer A, the high revenue steer was a Choice 901 lb yield grade 2 carcass that was on feed for 204 days with ADG of 3.84 lb/d. This animals hit the May 24th harvest date with the base price of $224/cwt. The characteristics of the $939 steer was Select 724 lb yield grade 2 carcass that was on feed for 225 days with an ADG of 3.11 lb/d. The select discount ranged from $12 to $25 per cwt, 22% of this group of steers graded select. Multiple breeds and mating combinations were present within Producer A’s steer group.
Animals with similar phenotype may exhibit substantial differences in feedlot performance, carcass traits and financial revenue as reflected in the differences between the high and low feeding revenue. Identifying the characteristics of the more profitable animals allows producers to make management and genetic changes if necessary. The group of steers from Producer B and C had similar breed makeup; however, differences in feedlot performance and carcass characteristics were reported.
Producer B
A quick examination of Producer B steers shows 18.2% Prime with no yield grade 4’s. Additionally, in-weight and ADG (3.78 lb/d) combined resulted in less days on feed and hitting the lowest base market price. Note “normally” the earlier harvest date during this time frame would result in the best base price; however, not in 2017. However, carcass value averaged over $2,000 and feeding revenue was near $1,600.
Producer C
Producer C provides an interesting scenario to discuss. These steers averaged final weights of 1,440 lbs (HCW = 884.5 lb) with 12.5 in2 ribeye area and 43.5% were yield grade 4 carcasses. Compared to Producer A’s steers with lighter final weight by 66 lb and by HCW 44 lb but a 1.2 in2 larger ribeye area. Based on this limited information, what EPD’s should be highly considered when selecting bulls? Considering that marbling score averaged 535 (Choice Quality Grade) and an ADG at 3.76 lb/d, selecting an EPD focused on muscling such as Ribeye Area would improve muscling present on the carcass. Remember, that bull selection can consider more than one factor at a time, so this producer would not need to reduce their emphasis on marbling.
The Bottom Line
Gaining knowledge on how animals perform post-weaning could provide producers with additional information when making mating decisions that impact the end product of their cow/calf operation. Prices received for feeder calves are influenced by the expected future performance. Determining the results of previous mating decisions provides valuable insight into how progress toward your production goals is occurring.
The Calf Value Discovery program or similar programs provide you with one more tool to use when making critical production decisions.




Table 1. Closeout results from 2016/2017 Calf Value Discovery Program


 
Pen Average
Ad
Bd
Cd


Number
189
18
22
23


Initial wt, lb
643.6
593.9
			(478 -672)
729.8
			(662 - 870)
710.9
			(596 - 806)


Feb wt, lb
986
890.8
			(690 - 988)
1078.5
			(948 - 1326)
1063.7
			(976 - 1142)


Final wt, lb
1405.7
1373.6
			(1182 - 1462)
1460.5
			(1350 - 1684)
1440.3
			(1338 - 1562)


Days on Feed
206.5
219.2
			(204 - 225)
194.4
			(182 - 224)
194.8
			(182 - 224)


ADG, lb
3.71
3.56
			(3.11 - 4.28)
3.78
			(3.15 - 4.47)
3.76
			(3.11 - 4.54)


Feed cost, $
276.31
283.87
273.92
272.14


Total cost, $
415.09
434.49
398.81
401.51


Feed COG, $
0.38
0.38
0.39
0.39


Total COG, $
0.57
0.58
0.57
0.58


HCW, lb
857.8
840.0
			(723.8 - 911.3)
895.7
			(825 - 1030)
884.5
			(834 - 988)


Dressing %,
63.6
63.7
63.9
64


Yield Grade
2.6
2.17
2.64
3.43


REA, in2
13.53
13.7
			(11.9 – 15.6)
14.02
			(12.7 - 15.7)
12.50
			(10.2 - 14.4)


Marbling scorea
465.3
405.6
			(313 - 625)
526
			(361 - 741)
535.2
			(396 - 691)


Backfat, in
0.48
0.40
			(0.21 - 0.57)
0.50
			(0.30 - 0.65)
0.63
			(0.42 - 0.89)


Grid Base Price, $/cwt
$219.33
$221.11
$216.64
217.74


Net Carcass Price, $/cwt
$219.07
$219.08
$224.27
217.78


Carcass Value, $
$1,880.03
$1842.89
			($1,739 - 2,040)
$2,010.22
			($1,633 - 2,358)
$1,925.53
			($1,758 - 2,101)


Feeding Revenueb, $
$1,424.94
$1,388.19
			($939 - 1,606)
$1,591.14
			($1,244 - 1,920)
$1,503.79
			($1,370 - 1,671)


Fdr calf value, $
$821.77
$785.93
			($712 - 837)
$884.16
			($830 - 1,026)
$869.07
			($787 - 952)


Profit/Lossc, $
$620.65
$602.27
			($431 - 771)
$706.68
			($414 - 894)
$634.72
			($461 - 845)



Quality grade


Prime
5.90%
 
18.18%
4.35%


Choice
77.40%
78%
77.27%
95.65%


Select
16.10%
22%
4.54%
 


No-Roll
0.54%
 
 
 


Commercial
 
 
 
 



Yield Grade


1
6.45%
22.20%
 
 


2
39.25%
38.90%
36.36%
 


3
43.01%
38.90%
63.63%
56.52%


4
11.29%
 
 
43.48%


5
 
 
 
 



a Marbling score Slight = 300-399; Small = 400 – 499; Modest = 500-599; Moderate = 600 – 699; and Slightly Abundant = 700 – 799; and Moderately Abundant = 800 - 899
b Feeding Revenue is Carcass value minus costs associated with feedlot expenses (does not include price of feeder calf).
c Profit/Loss is Feeding revenue minus feeder calf initial value.
d Averages are listed and range for each trait in ( )







Table 2. Mean USDA carcass grade traits


Factor
Value


USDA yield grade
3.1


USDA quality gradea
696


Adjusted fat thickness
0.56 in


Hot Carcass Weight (HCW)
860.5 lb


Longissimus muscle area
13.9 in2


Source: Navigating Pathways to Success: 2016 National Beef Quality Audit
a USDA Quality Grade 600 = Select00 and 700 = Choice00