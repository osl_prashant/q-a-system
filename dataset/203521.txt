Yes, the supermarket produce department is a wonder for its wide variety of fresh, tasty and healthy items. 
 
But not always considered is that it also represents a range of products, some from points far away - often very far away.
 
While horticultural research programs in the U.S. have done a good job providing new varieties, there continues to be, and will likely always be, an international connection directly affecting those supermarket selections.
 
A major reason for this availability is the fact that apples can be grown successfully in many regions around the world. This includes in both hemispheres, providing valuable alternating seasons. 
 
The Pink Lady apple is a good example of an "out-of-towner" making it big here, and in doing so represents a history shared by some other apples.
 
With around 15 years since the initial shipments of this apple to markets in the U.S., it's easy for most to believe that it takes a long time for an apple to be considered established.
 
Yes, a long time, but its decade-and-a-half of delighting millions of consumers here is only a more recent part of the story. You really have to go back to the early 1970s in western Australia to find where this apple actually got its start.
 
This was when John Cripps made 300 crosses of golden delicious and lady williams, with only two of them looking to be commercially viable. 
 
Cripps pink was to be trademarked Pink Lady, with great international success, while cripps red became Sundowner - with a track record that's hardly been dazzling.
 
Some of those now enjoying a Pink Lady most likely have no idea that its flavor and crunch can be traced back to before many of them were born. 
 
Sadly, it's also likely the case that most of those couldn't care less about the apple's history, and, even worse, there could be some who believe it and other apples just magically appear out of the backroom of the supermarket.
 
This is where those in the industry may be missing out, especially with those having the smarts to know there are real trees involved. 
 
This could also be part of the more recent desire to know more about the primary source of that food.
 
They do want to know the exact source of their food, so why not tell them - but do it the right way, and that means making the most out of it. Make it interesting and use it to help sell the product! 
 
This can be accomplished in promotions by just including USA as country of origin and where that particular apple actually got its start, if from abroad. That is, where its primary family first produced marketable fruit. 
 
While it's important to refer to the product basis as grown in the USA and/or considered local, it can in some cases also be beneficial to make reference to the region or country where a variety got its start. Make it worldly and sophisticated.
 
As the world continues to become smaller and with sales points not necessarily multiplying at a rapid rate, why not look for ways to demonstrate the effort that goes into product availability? 
And if that work was accomplished thousands of miles away, that should be considered as noteworthy.
 
In the tree fruit industry, like many others, there are respected domestic sources and there are respected import sources. 
 
Including information on all of those apple variety sources is a good way to let the consumer know they have direct access to the best of all worlds.
 
For apple variety or trademark origins check with your shipper or send me an e-mail.
 
Alan Taylor is the former marketing director for Pink Lady America in Yakima, Wash., and now consults in the apple industry. E-mail him at proft@embarqmail.com.
 
What's your take? Leave a comment and tell us your opinion.