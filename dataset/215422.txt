HumaneWatch.org released a new report this week with analysis of the Humane Society of the United States (HSUS) 2016 tax returns and an advertisement that premiered on Tucker Carlson Tonight on Tuesday.

View the video here: https://www.youtube.com/watch?v=X9nJSWpV49w

The advertisement depicts a bedraggled, shaggy dog begging for money at a public park. A man comes along and steals the money from the dog – “just like HSUS takes donations intended for animal shelters from unsuspecting donors who just want to help the sad dogs they see on TV,” says HumaneWatch. The ad ends with the request: Give to your local shelter, not the Humane Society of the United States.

“Despite its name, the Humane Society of the United States is not affiliated with the many humane societies in towns and counties that operate pet shelters,” HumaneWatch says. “The Humane Society of the United States does not run a single pet shelter. Public polling shows significant confusion between HSUS and local humane societies.”

The new report from HumaneWatch shows that only about 1% of HSUS’s nearly $132 million budget last year went to pet shelters in the U.S. Meanwhile, $69 million went to fundraising-related expenses – more than 50% of the group's whole budget, the group reports.

Some other highlights from the report include: $51 million in Caribbean accounts, $4.25 million used for lobbying, and $2.9 million in compensation for 13 executives.

View the report here: https://www.humanewatch.org/wp-content/uploads/2017/12/2017_HW_Not_Your_Local_Shelter.pdf

“While HSUS is spending over half of its budget running a factory fundraising operation, 3 million pets are euthanized in shelters across the United States every year,” said Will Coggin, managing director of the Center for Consumer Freedom. “HSUS's fat-cat executives are laughing on their way to the bank as donors are being duped and dogs are dying. If people want to help, they should give to local shelters that can make a difference.”

Visit HumaneWatch.org and HelpPetShelters.com, to learn more about the organization and HSUS.