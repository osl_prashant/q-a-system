One of the biggest headlines last week was talks of China putting tariffs on U.S. agricultural imports. One of the biggest tariffs would be on U.S. pork. But left off the list of more than 100 agricultural products? Soybeans.

If the U.S. and China happen to get into a trade war, Matt Bennett, owner of Bennett Consulting, says no one will win.

“It’s going to be pretty tough for producers all around the board,” he said on U.S. Farm Report. “Ultimately, it’s probably going to be to be able to source cheaper soybeans because they have to buy beans off us.”

If China, one of the top importers of U.S. soybeans, decides to slap a tariff on the product, Bryan Doherty, vice president of brokerage solutions with Stewart-Peterson, thinks prices could plummet in the beginning.

“China cannot not buy beans from the United States, and so it might be rhetoric,” he said. “Let’s call it a ploy potentially to drive prices down to sneak back in and buy at a cheaper level.”

Hear Doherty and Bennett discuss China’s impact on U.S. commodities and what they think the acreage mix could be like on Thursday on U.S. Farm Report above.