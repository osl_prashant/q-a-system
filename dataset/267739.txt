No yield or number on a check is worth someone getting hurt on your farm. Take care to mind safety guidelines for both farm practices and products.
The United Soybean Board and University of Illinois offer reminders for staying safe during planting season.

Be aware of your transportation risks and make sure your farm vehicles are visible. Some tractors have flashing lights, extremity markings or slow-moving vehicle signs. If yours doesn’t, be sure to pick up a high-visibility sticker or sign to let drivers know you’re there.
Read herbicide, pesticide, fungicide and seed labels. Following precautions about wearing long sleeves, using a dust mask or protecting your eyes can save you from injury. Keep labels handy or snap a picture with your phone for quick reference.
Don’t forget about eating and sleeping. You will likely be spending long hours in the field, and skipping meals and sleep can decrease your reaction time and awareness. The Center for Disease Control and Prevention says the average person needs between seven and nine hours of sleep nightly.
Watch out for children on or around equipment. If a child is with you in the cab, make sure he or she is wearing a seat belt. Teach children to stay a safe distance from moving tractors and other farm equipment.

In addition to equipment safety it’s important to be mindful of treated seed because it can contain various fungicides, insecticides or nematicides. Avoid spills to be a good environmental steward.
First, make sure you handle treated seed as described on the label. Next be aware of areas that you’re more likely to spill seed—this is most likely area for spills is where the seed is loaded and field entry points. If any treated seed is spilled pick up all you can or cover it with soil so birds and other animals don’t see it to eat.
According to the American Seed Trade Association these are some of the birds are at risk of eating treated seed:

American Crows
Blackbirds
Blue Jay
Brown Thrasher
Canada Geese
Mourning Doves
Ring-Neck Pheasants
Sparrows