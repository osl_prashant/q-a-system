USDA’s monthly cattle on feed report estimated the total inventory on Oct. 1 at 10.8 million head, up 5% from the same period a year ago. USDA’s number was relatively close to the pre-report estimates from market analysts, with about 400,000 head more than the analysts expected.
 
Placements during September totaled 2.15 million head, 13% higher than September 2016. Placements were significantly higher than the pre-report estimate of 107.5%. 
 
Marketings during September totaled 1.78 million head, 3% higher than 2016. Marketings were close to the pre-report guess of 102.6%.
 
Cash fed cattle trade was relatively light, with trades in the south at $110 per cwt., steady with last week. Dressed trade in the north was also light at $174 to $175 per cwt., again steady with last week. 
 
Feedyards believe there is a stronger undertone to the market, and packer margins remain excellent, but the market is slow to advance. 
 
A softer undertone was noted this week on calves and yearlings. The fall run is underway and supplies have increased. USDA's Agricultural Marketing Service reported steer and heifer calves "very uneven," with prices $4 lower to $4 higher. Unweaned and lightweight calves in the Southern Plains were called up to $10 lower. AMS reporters, however, noted some "pretty lofty prices" on "higher quality stock" in Nebraska and South Dakota auctions.
 
Boxed beef cutout prices advanced modestly on the week. The Choice cutout ended Friday at $199.86, up $1.64 per cwt. from the previous week. Select cutouts closed at $191.14, up $1.09 from last Friday. The Choice-Select spread was $8.72 per cwt., compared to $8.17 per cwt. last Friday.