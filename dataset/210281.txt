This growing season has been a challenge across the upper Midwest. Whether your crops have been hit with drought or hail the odds are that we are going to see an increase potential for feed contaminants such nitrates or molds which cause mycotoxins. This article focuses on mycotoxins and how to manage them.
What is the difference between molds and mycotoxins?
Molds produce mycotoxins, but molds by themselves do not cause problems to animals. However, the mycotoxins molds produce can cause significant problems for livestock. Molds such as Aspergillus, Gibberella, Fusarium, and Diplodia may be an issue. The mycotoxins with higher prevalence are Aflatoxin (reduced performance/death more prevalent in drought), Ergot / Scab which causes high levels of Zearalenone (reproductive problems), DON / Vomitoxin (feed refusal). Fumonisins and T-2 toxins may occur here, but less frequently and shouldn’t be ruled out.
Why are aflatoxins a concern for livestock producers?
Conditions that promote the production of the mycotoxin aflatoxin are hot weather and drought stress. The prime conditions for this fungal toxin development are days when the temperature is greater than 70 degrees Fahrenheit at night and the corn grain is in the latter stages of grain fill during drought stress. The mold found on corn is gray-green to olive-green in color, and the two most common fungi that produce this toxin are Aspergillus flavusand Aspergillus parasiticus.
Aflatoxins are a concern for livestock producers, especially dairy producers, as the toxins are harmful and even possibly fatal to livestock. They are also carcinogenic to animals and humans. Most commonly, aflatoxins negatively affect feed efficiency, reproduction, and suppress the immune system of livestock. The most common type of toxin, aflatoxin B1, is a carcinogen. This can become a concern as it can appear in milk when corn with aflatoxin concentrations over 20 ppb is fed to dairy cows. The legal limit for aflatoxin in milk, according to FDA regulations is 0.5 ppb (aflatoxin M1).
What are the FDA guidelines for using aflatoxin-contaminated grain?
Typically, elevators do not accept corn with aflatoxin concentrations of 20 ppb (parts per billion). In addition, the Food and Drug Administration (FDA) has an “action level” of 20 ppb for aflatoxins in corn that regulates interstate commerce. This is the level at which federal agencies may take action, including the seizure of the corn or prohibition of its sale.
The FDA has established guidelines for using aflatoxin-contaminated grain as follows:
Less than 300 parts per billion (ppb) for corn intended for finishing beef cattle;
Less than 200 ppb for corn intended for finishing swine weighing 100 pounds or greater;
Less than 100 ppb for corn intended for breeding beef cattle, breeding swine, or mature poultry;
Less than 20 ppb for corn intended for lactating dairy animals, immature animals; animal species, and uses not specified above or when the intended use is unknown
Will using preservatives in the feed (such as propionic acid) kill the toxin if it is already present in the feed?
NO. Adding mold inhibitors, drying corn, or adding acids will only stop additional mold growth, but it will do nothing against mycotoxins already produced and present in the feed. There are additives such as sodium bentonite, zeolite, and calcium aluminosilicate that work well as a binder and work best on aflatoxin. Yeast cell wall extracts (MOS and glucomannans) and enzymatic products are more effective when dealing with T-2 toxins, DON, and zearalenone. These products sequester the mycotoxins in feed and reduce the bio-availability of mycotoxins and increase excretion in the feces. The best advice is if you have seen mold on corn, test it before feeding to determine what, if any mycotoxins may be present, especially if it’s being fed to the milking or breeding animals.
Does going through the fermentation process or drying process in ethanol plants remove toxins from DDGS or Syrups?
No, in fact it makes it worse since it triples the concentration of mycotoxin in DDGS without inactivating it. Since molds could affect fermentation and ethanol production plants do screen corn for molds and mycotoxins.
So where can I go to get help dealing with mold and mycotoxin issues?
There are several reference sources that are available as producers consider options if they are dealing with mycotoxins in their corn. The resources speak to harvest, storage, and utilization considerations of corn that maybe contaminated with mycotoxins. In addition to contacting an SDSU Extension office with further questions, and these on-line resources are also available:
Dealing with Mycotoxin-contaminated Feeds at Feeding Time, SDSU Extension
Aflatoxins in Corn, Iowa State University Extension
Aflatoxin & Mycotoxin Risks Video, Mike Hutjens - University of Illinois
Feed Testing Services
Producers wishing to have their grain or feed tested to be used in animal feeding operations can send samples to either the SDSU Plant Diagnostic Clinic in Brookings, SD or the NDSU Veterinary Diagnostic Laboratory in Fargo, ND.

South Dakota State University153 Plant Diagnostic Clinic (SPSB), Box 2108Brookings, SD 57007

Phone: 605.688.5545Fax: 605.688.4024