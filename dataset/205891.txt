USDA Weekly Grain Export Inspections
			Week Ended August 31, 2017




Corn



Actual (MT)
797,555


Expectations (MT)

650,000-850,000



Comments:

Inspections are down 24,828 MT from the previous week and the tally was near the upper end of traders' expectations. Inspections for 2016-17 are up 21.7% from year-ago, compared to 23.1% ahead the previous week. USDA's 2016-17 export forecast of 2.225 billion bu. is up 17.0% above the previous marketing year.




Wheat



Actual (MT)
252,465


Expectations (MT)
500,000-600,000 


Comments:

Inspections are down 422,635 MT from the previous week and much lighter than expected. Inspections are running 6.0% ahead of year-ago versus 12.4% ahead last week. USDA's export forecast for 2017-18 is at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual (MT)
644,909


Expectations (MMT)
575,000-700,000 


Comments:
Export inspections are down 92,434 MT from the previous week, and within expectations. Inspections for 2016-17 are running 12.1% ahead of year-ago, compared to 13.0% ahead the previous week. USDA's 2016-17 export forecast is at 2.150 billion bu., up 10.7% from year-ago.