The Red Angus Association of America and the American Hereford Association have teamed up to create a genetically verified program for developing commercial females. “Premium Red Baldy” is hoped to capitalize on the best characteristics of both breeds while increasing the bottom line for producers utilizing the tagging program.
The emphasis of the tagging program is for producers to take advantage of hybrid vigor by cross breeding Hereford and Red Angus genetics. Premium Red Baldy is only for heifers and is targeted at achieving the goals of breeding females for longevity, fertility, adaptability and efficiency.
An announcement about Premium Red Baldy was made at the 2018 Cattle Industry Convention and NCBA Trade Show in Phoenix by AHA Executive Vice President Jack Ward and RAAA CEO Tom Brink. A press conference was initially held on Feb. 1, followed by a reception later that same evening.
Brink said both breed associations are committed to providing profitable pathways to success for commercial cow-calf producers that will help find them the right genetics.
"This new program will help producers access genetically verified females that are packed with heterosis and ready to go to work on farms and ranches all across the country. It all starts with the right cow traits, and Premium Red Baldy females will excel in that regard," Brink said.
Ward echoed thoughts saying the breeds will help identify “identifies genetically superior F-1 females” with the help of Premium Red Baldy.
“The AHA and RAAA are the only two breed associations that implement a mandatory whole herd reporting performance program which gives strength and reliability to their respective genetic evaluations,” Ward said.
To participate in the program producers must verify that eligible females are sired by AHA or RAAA registered and transferred sires. Bulls need to rank in the top 50 percent of their respective breed for AHA's Baldy Maternal Index (BMI$) or RAAA's Herdbuilder Index (HB).
It is recommended cattle producers who are interested in participating call either breed association based on sire used to verify females and order ear tags for the program. After completion of an interview over the phone tags will be sent for the correct amount of females with red bodied and white or brockle-faced females born on the operation. Premium Red Baldy is not a Process Verified Program (PVP) through the USDA.
For more information about the Premium Red Baldy program, please contact Trey Befort, AHA Director of Commercial Programs at tbefort@herefordbeef.org or Chessie Mitchell, RAAA Tag Program Coordinator at chessie@redangus.org.
The announcement made at the joint Red Reception can be watched below:


 
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


Live from the Red Reception at NCBA
Posted by American Hereford Association on Thursday, February 1, 2018



A full press conference from the American Hereford Association can be watched below:


 
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


Good morning from the American Hereford Association Press Conference in Phoenix.
Posted by American Hereford Association on Thursday, February 1, 2018