Big crops to get 'slightly' bigger... That's the sentiment heading into Thursday's October Crop Production Report. Traders expect USDA to raise its corn yield peg by 0.2 bu. per acre and the soybean yield peg by 0.1 bu. per acre to 170.1 bu. per acre and 50.0 bu. per acre, respectively.
Traders expect USDA to raise harvested corn acreage by around 35,000 acres to result in a crop of 14.204 billion bu., up 20 million bu. from September. Traders look for USDA to raise soybean harvested acreage by 286,000 acres for a crop of 4.447 billion bu., up 16 million bu. from last month.




 


Yield -- in bu. per acre


Production -- in billion bu.






Commodity


Avg.


Range


USDA Sept.


Avg.


Range


USDA Sept.




 




Corn


170.1


168.7-171.5


169.9


14.204


14.060-14.355


14.184




Soybeans


50.0


49.1-52.1


49.9


4.447


4.335-4.490


4.431




While traders expect USDA to slightly raise its 2017-crop estimates, it looks for 2017-18 carryover to be trimmed. The drop is expected to come via higher usage estimates, but instead by lower carryin. USDA set 2016-17 carryover in last month's Quarterly Grain Stocks Report, which surprised traders by coming in much smaller than expected. The lower carryins for both crops is expected to result in a 46-million bu. drop in 2017-18 corn carryover from last month to 2.289 billion bu. and a 28-million-bu. reduction in soybean carryover to 447 million bushels.
Meanwhile, traders look for 2017-18 wheat carryover to be raised by around 13 million bu. from last month to 946 million bu., while cotton carryover is expected to drop by around 300,000 bales to 5.7 million bales.




 


2016-17


2017-18






Commodity


*Grain Stocks


USDA Sept.


Avg.


Range


USDA Sept.




in billion bushels




Corn


2.295


2.350


2.289


2.168-2.450


2.335




Soybeans


0.301


0.345


0.447


0.375-0.500


0.475




Wheat


--


1.184


0.946


0.928-0.971


0.933




Cotton (mil. bales)


--


2.75


5.70


5.20-6.30


6.00





Pre-report expectations from Reuters and Bloomberg newswires.
* The Sept. 29 Grain Stocks Report set 2016-17 corn and soybean carryover.
USDA's Crop Production and Supply & Demand Reports will be released at 11:00 a.m. CT on Oct. 12.