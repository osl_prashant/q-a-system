Super Starr International’s imported honeydew season is starting, with supplies available through April.
The melons, grown in Tecoman, Mexico, are Fair Trade certified, according to a news release.
“We’ve had a really good start to the new year with a solid supply of high-quality melons,” Lance Peterson, president of Super Starr International, representing the third generation of growers for the company, said in the release.
The company also grows and packs watermelons, and papayas, include Maradol papayas. The company ships throughout the U.S. and Canada, and to Japan.