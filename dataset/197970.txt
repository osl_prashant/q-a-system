It’s no secret that weeds are a farmer’s enemy—but which weeds pose the biggest threat? The Weed Science Society of America (WSSA) recently deployed a survey to nearly 200 weed scientist across North America to find out more about weeds in farmers’ fields.
WSSA asked weed scientist to identify the most troublesome weeds as well as the most common weeds in their state. They considered herbicide resistance, frequency and crop affected by the weeds while making their rankings.
While palmer is only listed as the most troublesome weed in cotton, it’s ranked first overall because most respondents listed it as problematic.
Most Troublesome
1. Palmer Amaranth
2. Common Lambsquarters
3. Horseweed (Marestail)
4. Morning Glory (ivyleaf, pitted, tall)
5. Waterhemp (tall, common)
6. Nutsedge (yellow, purple)
7. Kochia
8. Common Ragweed
9. Giant Ragweed
10. Nightshade (eastern black, hairy) 

Most Common
1. Common Lambsquarters
2. Foxtail (giant, green, yellow)
3. Morning Glory (ivyleaf, pitted, tall)
4. Palmer Amaranth
5. Redroot Pigweed
6. Waterhemp (tall, common)
7. Horseweed (Marestail)
8. Common Ragweed
9. Barnyardgrass
10. Velvetleaf 
 




Broadleaf Crop


Most Troublesome Weed


Most Common Weed




Alfalfa


Canada Thistle


Dandelion




Canola


Kochia


Wild Oat




Cotton


Palmer Amaranth


Morning Glory




Fruit and nuts


Field Bindweed


Horseweed (Marestail)




Peanuts


Nutsedge


Palmer Amaranth




Pulses


Common Lambsquarters


Common Lambsquarters




Soybeans


Horseweed, Waterhemp


Waterhemp




Sugar Beets


Common Lambsquarters


Common Lambsquarters




Vegetables


Nutsedge


Common Lambsquarters




 

Does this match what you consider the worst weeds in your fields? Let us know!