Preclosing
Preclosing

The Associated Press

CHICAGO




CHICAGO (AP) — Futures trading on the Chicago Board of Trade Wed:
Open  High  Low   Last   Chg. WHEAT                                   5,000 bu minimum; cents per bushel    May      466¼  476    465½  474¼   +8  Jul      481½  490½  480½  488½   +7  Sep      499½  508¼  498½  506¼   +7  Dec      522¾  531¼  521¾  529½   +6¾Mar      542¼  548¾  542    547¾   +6¾May      553    557    553    555     +4¾Jul      552    559    552    558     +5½Sep      563¼  565½  562¼  562¼   +1¾Dec      578¾  578¾  578¾  578¾   +4¼Mar      587    587    586    586     +3  Jul      565½  565½  565½  565½   — ¼Est. sales 111,199.  Tue.'s sales 142,821Tue.'s open int 471,652                CORN                                     5,000 bu minimum; cents per bushel    May      380¾  383½  380    382¼   +2  Jul      389½  392¼  388¾  391     +1¾Sep      397    399¾  396¼  398½   +1¾Dec      406    409    405¼  407½   +1½Mar      414    416¾  413½  415½   +1¾May      417½  420¼  417½  419¼   +1  Jul      422¼  424½  421¾  423¼   +1¼Sep      408¼  408¾  407¼  408     + ¼Dec      410    412½  410    411¼   +1  Mar      418¼  418¼  418¼  418¼   — ¼Jul      426½  426½  426½  426½        Dec      416    416    416    416     +1¼Est. sales 277,411.  Tue.'s sales 554,140Tue.'s open int 1,854,949               OATS                                      5,000 bu minimum; cents per bushel    May      228¾  234¼  228½  232½   +3¾Jul      235    240    235    235     — ¾Sep      244½  245    244½  245     +1½Dec      255¾  255¾  254½  255¾   +1  Est. sales 1,091.  Tue.'s sales 538     Tue.'s open int 6,411                  SOYBEANS                               5,000 bu minimum; cents per bushel    May     1046¼ 1052½ 1039   1043¼   —2¾Jul     1057¼ 1063¾ 1050½ 1054¾   —2½Aug     1058¾ 1065¼ 1052½ 1056½   —2  Sep     1051¼ 1057   1046¼ 1050½   — ½Nov     1044¾ 1052¾ 1042   1046¾   +1½Jan     1047   1055   1045¾ 1050¾   +2½Mar     1036   1042   1034½ 1041½   +4¾May     1032¾ 1038   1030¾ 1038     +5¾Jul     1036   1041¾ 1035¾ 1041     +4¾Nov      996¾ 1003½  996¼ 1003¼   +5½Est. sales 230,704.  Tue.'s sales 211,352Tue.'s open int 970,469