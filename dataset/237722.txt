BC-Merc Table
BC-Merc Table

The Associated Press

CHICAGO




CHICAGO (AP) — Futures trading on the Chicago Mercantile Exchange Wed:
Open  High  Low  Settle   Chg.CATTLE                                  40,000 lbs.; cents per lb.                Feb     128.35 129.77 127.50 127.50   —.52Apr     124.25 125.17 122.92 123.27   —.98Jun     116.25 117.00 115.27 115.60   —.75Aug     113.45 114.30 112.65 112.97   —.60Oct     115.75 116.67 115.30 115.57   —.45Dec     118.00 118.75 117.47 117.85   —.42Feb     118.45 119.02 117.90 118.27   —.33Apr     117.92 118.55 117.45 118.17   +.02Jun                          111.35   +.03Est. sales 79,248.  Tue.'s sales 57,222 Tue.'s open int 376,912                FEEDER CATTLE                        50,000 lbs.; cents per lb.                Mar     146.25 147.00 144.40 144.75  —1.80Apr     148.90 149.60 146.55 147.00  —2.20May     149.92 150.82 148.12 148.55  —1.70Aug     153.00 154.00 151.82 152.35  —1.07Sep     152.57 153.65 151.57 152.05  —1.00Oct     151.75 153.10 151.05 151.50  —1.05Nov     150.95 152.20 150.02 150.77   —.70Jan     145.40 146.45 145.40 146.35   —.65Est. sales 16,957.  Tue.'s sales 17,584 Tue.'s open int 57,960,  up 387        HOGS,LEAN                                     40,000 lbs.; cents per lb.                Apr      69.40  69.50  67.12  67.22  —2.73May      76.00  76.22  74.87  74.87  —1.53Jun      81.47  81.80  80.52  80.67  —1.35Jul      83.20  83.20  81.90  82.20  —1.02Aug      83.20  83.20  82.22  82.47   —.80Oct      70.60  70.60  69.80  70.20   —.47Dec      64.35  64.35  63.45  64.10   —.32Feb      67.82  67.82  67.20  67.60   —.40Apr      70.77  70.97  70.77  70.77   —.40May                           76.15   —.80Jun                           78.42   —.55Jul                           79.45   —.45Est. sales 69,381.  Tue.'s sales 45,049 Tue.'s open int 230,704,  up 2,009      PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.