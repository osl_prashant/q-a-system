AP-FL--Florida News Digest,1st Ld-Writethru, FL
AP-FL--Florida News Digest,1st Ld-Writethru, FL

The Associated Press



Good afternoon! Here's a look at how AP's general news coverage is shaping up in Florida. Questions about coverage plans are welcome and should be directed to the AP-Miami bureau at 305-594-5825 or miami@ap.org. Ian Mader is the news editor and can be reached at imader@ap.org. David Fischer is on the desk and can be reached at dfischer@ap.org. A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Eastern. Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
TOP STORIES:
SCHOOL SHOOTING-FLORIDA-COMMISSION
COCONUT CREEK — Faulty classroom design and failures in the police radio and 911 systems contributed to the chaos and deaths during the recent Florida high school massacre, a commission investigating the shooting was told at its first meeting Tuesday. Broward Sheriff's Office Detective Zachary Scott told the Marjory Stoneman High School Public Safety Commission that teachers trying to lock down their students as the gunman began his attack couldn't lock classroom doors from the inside, but had to grab a key, open the door and turn the lock from the outside. By Terry Spencer.
—WITH:
SCHOOL SHOOTING-FLORIDA-COMMISSION-THE LATEST
SCHOOL SHOOTING-FLORIDA
LAWMAKER-PARKLAND SURVIVOR-TWEET
PECAN BRANDING
FORT WORTH, Texas — The humble pecan is being rebranded as more than just pie. Pecan growers and suppliers are hoping to sell U.S. consumers on the virtues of North America's only native nut as a hedge against a potential trade war with China, the pecan's largest export market. By Emily Schmall.
ENDANGERED TURTLES-RELEASED
BOSTON — Several sea turtles are basking safely in the warm Florida surf following a long stay in Massachusetts. Biologists from the New England Aquarium released 14 endangered and threatened sea turtles into the Atlantic Ocean off the coast of Florida on Monday. The animals spent the winter near Boston recovering from various illnesses.
HUSBAND KILLED-FUGITIVE
MINNEAPOLIS — A Minnesota woman who led authorities on a weekslong manhunt after she allegedly killed her husband, then went to Florida and befriended — then killed — a woman who resembled her, is now facing charges in two states. Lois Riess is in custody in Texas, awaiting transfer to Florida or Minnesota for trial. If the Florida charges against Riess are elevated to first-degree murder, she could face the death penalty. By Amy Forliti.
IN BRIEF:
SEVERE WEATHER-GULF COAST — The National Weather Service confirms six more tornadoes hit parts of southern Alabama and the Florida Panhandle during storms on Sunday.
FATAL FIGHT — A Florida man has been sentenced to 11 years in prison in the 2016 death of a co-worker.
DEPUTIES SLAIN-FLORIDA — Two Florida sheriff's deputies who were gunned down at a Chinese restaurant last week have been laid to rest.
PARKED TRAIN CRASH — Authorities say a woman and her 3-year-old son died when their car crashed into a parked train in Florida.
POOL WATER ILLNESS — A Florida woman says her 4-year-old daughter nearly died after ingesting pool water.
SCAFFOLDING ACCIDENT — Two construction workers were rescued from the side of a five-story building near Miami when the scaffolding they were standing on malfunctioned.
RACIST PROM PROPOSAL — A Florida high school student stirred a backlash when he held up a sign with a racially offensive message to invite his girlfriend to the prom.
CARIBBEAN-SEABORNE AIRLINES — Florida-based Silver Airways has bought the Caribbean's largest regional airline.
GUN CLEANING INCIDENT — A Florida man told sheriff's deputies he accidentally discharged a pistol while he was teaching gun safety to three young people.
SECURITY GUARD ARRESTED — Police say a security guard arrested in connection with two attacks on women in a Florida neighborhood has told investigators he has a problem.
DRIVE-BY SHOOTING-FLORIDA — Authorities say a 46-year-old woman was critically wounded in a drive-by shooting while her three teenage children slept inside their home.
RAGBRAI INJURY-CONVICTION — A jury has convicted a Louisiana man whose pickup truck hit a bicyclist who was camping after a ride in Iowa.
JUDGE VS ELDERLY INMATE — A Broward judge will not return to the bench after a video captured her angry exchange with a frail inmate in a wheelchair who died a few days later.
MALL FIREWORKS-ROLEX — Police have arrested a man they say was part of a ruse to set off fireworks inside a massive Florida outlet mall on New Year's Eve to steal a nearly $17,000 Rolex watch.
BREAKER'S GENERATORS-PUERTO RICO — An opulent Florida hotel is spending tens of thousands of dollars to ship its two large generators to Puerto Rico.
IN SPORTS:
BKN--HEAT-76ERS
PHILADELPHIA — The 76ers, with Joel Embiid in his first playoff game before home fans, can close their first-round series in Game 5. The Miami Heat need to slow a Philadelphia team averaging 117 points in the series. Dwyane Wade, his plans for next season unclear, could be playing his last game. By Dan Gelston. UPCOMING: 750 words, photos. Game starts at 8 p.m.
_____
If you have stories of regional or statewide interest, please email them to miami@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477. MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Florida and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.
_____
The AP, Miami