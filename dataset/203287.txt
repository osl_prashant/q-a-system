SAN FRANCISCO - Considering the double-digit annual growth of online retail grocery sales, potato marketers can't ignore the future impact of the emerging sector on their sales strategies.
 
Matt Lally, project analyst of The Perishables Group Inc., Chicago, spoke to the opportunity and dangers of online food sales at a Jan. 5 fresh market breakout session at the 2017 Potato Expo.
 
With fewer trips by younger consumers, U.S. supermarket store traffic has declined from about 17.5 billion in 2012 to about 16 billion in 2015, Lally said.
 
Brick and mortar cracking?
 
Projected annual sales growth for supermarkets is set at 1.9% from 2015 to 2020, according to Nielsen analysis. That compares with 2.8% annual growth for super centers, 3.7% annual growth for dollar stores and a whopping 12.2% for e-commerce retailers.
 
The share of sales by all e-commerce retailers rose from 2% in 2001 to a projected 13% by 2020.
 
However, e-commerce shares of most consumer packaged goods today is estimated only at 2% to 4%, Lally said, 
 
Consumers are demanding solutions in a variety of formats, Lally said, noting that the Amazon Echo can now be used to order groceries, the checkout-free Amazon Go store is being piloted and 7-11 has experimented with drone delivery. Some retailers are offering online ordering and delivery, and meal kit companies like Blue Apron are turning goods into services.
 
According to a Nielsen study, 85% of shoppers appreciate the price/value of online shopping, with 80% liking the convenience and 74% happy with the experience. Only 56% of online shoppers said they were happy with the variety offered online.
 
Online shoppers like to stock up when they buy online, Lally said. That's potentially good news for potatoes, where generally two-thirds of sales occur during stock-up trips, he said. For consumers, 18% said they stock up when they buy online, compared with 2% of shoppers who use online sales for quick meals and 5% of consumers who use online shopping for immediate needs.
 
Fresh food a fit online?
 
Harris Research from mid-2016 shows 31% of Americans said they purchased food online in the past six months, Lally said, and 52% of those bought fruits and vegetables. The survey found that urban consumers are more likely to purchase food online, and that the same proportion of men and women purchased food online.
 
However, the survey found that three in ten who purchased food online don't plan on doing it again.
 
Young Americans, age 18 to 34, are more likely than other consumers to purchase food online. The study showed that parents (37%) are more likely than non-parents (28%) to have purchased food online.
 
Only 2% of shoppers say they used online shopping to replace all routine shopping and 7% say they use online shopping to replace at least some routine shopping. Sixteen percent of shoppers say they buy food online to buy items not found in grocery stores, and 7% when running lower on a critical item.
 
For the 35% of consumers who plan to buy food online, the survey found that 81% are "at least somewhat likely" to use an online non-grocery retailer such as Amazon.
 
Other "somewhat likely" options are:
Online purchase from grocery retailer delivered (49%);
Online purchase from grocery retailer, curbside (45%);
Online-only grocery retailer (42%);
Online meal planning (34%).
 
People who buy food online value "long shelf life and nonperishable foods," with 49% stating they value that attribute compared with 48% who value online food that is "difficult to purchase" in stores.
 
Along the same line, consumers said that fresh produce ranks low on the list of food that is a "good fit" for online shopping. Consumers said food that is a good fit for online purchases include: 
 
Dry packaged foods (57%);
Snacks (51%);
Canned goods (48%);
Condiments, sauces (41%);
Frozen foods (9%);
Fresh fruits and vegetables (7%); and 
Dairy (4%). 
 
Because fresh fruits and vegetables rank so low in consumer perceptions of a "good fit" for online shopping, Lally said that the industry should seek to build trust in the fresh produce selection process for online sales. In addition, marketers should describe to online consumers the health benefits of fresh potatoes.
 
Marketers should seek to pair potatoes with food - such as fresh fish or canned vegetables - that are often purchased online.
 
Lally said e-commerce sales are expected to outpaced brick and mortar growth in the next three years, with millennial consumers leading the way. 
 
The time to develop a strategy is now, he said.
 
Potatoes have the advantage of being one of the most widely purchased fresh produce items, with nearly 88% of households purchasing fresh potatoes last year.
 
"By understanding what connections exist and building and bridging that with products that consumers are more likely to purchase online today ultimately can help us build the basket and drive incremental sales," he said.
 
Lally said to make sure consumers are thinking about potatoes when they make their online shopping lists and navigating the services, the industry should expand web-based consumer engagement with potatoes, including newer varieties.