One of the challenges in coming up with a value for standing hay is the lack of established market price information like corn and soybeans. Another challenge is multiple cuttings of hay versus a single harvest for grains. So it’s no wonder the price for standing hay can vary greatly between farms, even between fields. Here’s one approach for pricing standing hay in 2017.Assuming four ton dry matter (DM)/acre for the entire year of dairy quality alfalfa hay worth $100 to $150/ton baled ($0.06 to $0.09/lb DM), half the value is credited to the owner for input costs (land, taxes, seed, chemical and fertilizer), and half the value is credited to the buyer for harvesting, field loss and weather risk. Obviously, estimated yield is an important factor when negotiating price. This formula will help determine pre-season maximum alfalfa dry matter yield potential…(0.10 x stems/ft2) + 0.38. Actual yield will likely be lower due environmental conditions and individual harvest / management practices. Wait until stems are at least 4-6 inches tall and count only stems upright enough to be cut by the mower. Using yield distribution based on recent multi-year UW-Extension field research in NE WI for a three cut (43% / 31% / 26%) or four cut (36% / 25% / 21% / 18%) harvest system, the following price range (rounded to the nearest $5) may offer a starting point for buyers and sellers to negotiate a sale of good to premium quality standing alfalfa in 2017:
 
4 cuts
3 cuts
1st crop
$ 85–130/a
$100–155/a
2nd crop
$ 60– 90/a
$ 70–110/a
3rd crop
$ 50– 75/a
$ 60– 95/a
4th crop
$ 40– 65/a
–
In this example, the standing value for the entire alfalfa field could range from $230 to $360/acre for the entire growing season. Keep in mind ownership costs can run $300-400/acre when the seller considers lost rent, establishment costs and top-dress fertilizer to maintain soil fertility. That’s why the same price is not always the right price for everyone. Ultimately, a fair price is whatever a willing seller and an able buyer can agree to.
To help farmers and landowners better evaluate their pricing options, Greg Blonde, UW Extension Agriculture Agent developed a mobile app for pricing standing hay. With more than 1500 downloads and 600 users across the country, the app provides quick access to baled hay market prices for reference calculations, with value per acre by cutting displayed using annual yield and harvest cost projections. The Android app is free to download at the Google Play store (search for Hay Pricing) or by going to: https://play.google.com/store/apps/details?id=com.smartmappsconsulting.haypricing.
Keep in mind ownership costs can run $300-400 per acre when considering lost rent, establishment costs and top-dress fertilizer to maintain soil fertility. That’s why the same price is not always the right price for everyone. As the old saying goes ”a fair price is whatever a willing seller and an able buyer can agree on”.