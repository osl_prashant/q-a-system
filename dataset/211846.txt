Produce suppliers in Philadelphia say value-added is a big segment of business — in terms of products and services — in the retail and foodservice channels.
“I see more and more going to the prepackaged,” said Tom Curtis, president of Tom Curtis Produce Inc., a Philadelphia distributor.
The trend is most apparent in greens and salad mixes, Curtis said.
“We’re seeing more people going to the processed product, as green leaf filets, blends — the romaine, green leaf, red leaf, escarole blend,” he said.
Cost is an issue, Curtis said.
“The price is very competitive because they have a 14-day lifespan on it and, if the market fluctuates $2-3, the processed price stays the same, so for restaurants, it’s kind of a boon,” Curtis said.
“If it goes up $2-3 on the table stock market, it doesn’t affect the price of the processed stuff.”
So, he said, there’s more balance.
“We see more going that way,” Curtis said. “We also see more restaurant chains contracting with growers. You have to load their product out of that grower, so the price is set. They give you so much a package to deliver it.”
Retail and foodservice customers also are seeing more buying options, Curtis said.
“The other thing we’re seeing is more and more people, even the receivers, are turning into wholesalers, so there’s competition,” he said.
“If they’re out of New York, you’re seeing one guy getting bigger and another guy sees that and now they’ve got to get bigger,” Curtis said.
“Guys are running product up and down the coast. You’ve got a bunch of people in New York delivering all the way to the Carolinas and the guys in the Carolinas delivering in Maine, so there’s a lot more competition in delivering now.”
Delivery services are an added value that is becoming more common, Curtis said.
“If I need eight to 10 pallets, and the guy gives me a price, I could save $300, but if I have to drive 125 miles to market, it’s going to cost me $500, so deliver it to me,” he said.
“It’s impacting all the terminal markets.”
The business, in general, is evolving rapidly, Curtis said.
“This business has changed three times in the last, I’d say, 12-15 years,” he said.
“It just keeps metamorphosing into a different type of business, and every time it does, you’re slicing that meat thinner and thinner.”
Meanwhile, value-added product continues to find more interest among retailers and foodservice operators, said Stephen Secamiglio, partner in Colonial Produce, a Philadelphia wholesaler.
“Everybody’s in the juices and all, and the cauliflower is trendy, because people are making rice out of it, and people are buying a lot of that,” he said.
Kale is also is a big seller, Secamiglio said.
“It got real big over the last couple of years, ever since people started doing the kale shakes,” he said.