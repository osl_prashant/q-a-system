Club Chef LLC, Covington, Ky., has announced a recall of snack kits, including Kroger private-label brand Fresh Selections, for possible listeria contamination. 
No illnesses have been reported and no contamination has been found on any product associated with the recall, according to a June 8 news release.
 
Club Chef initiated the recall because the presence of listeria was identified by the company during environmental testing iin the processing facility, according to the release. All have use-by dates of June 11 or June 12.
 
The release said the recalled products, processed on May 30-31, are:
 
Fresh Selections Veggie Tray with Apples, 6.25 ounces, with Universal Product Code 11110 91544 and lot code 7150KT8;
Fresh Selections Fruit Tray with Carrots, 7 ounces, with UPC 11110 91451 and lot code of 7150KT5 or 7151KT3;
Fresh Selections Veggie Tray with Pretzels, 5.75 ounces, with UPC 11110 91477 and lot code of 7150KT or 7150KT8;
Fresh Selections Veggie Tray with Snap Peas, 6 ounces, with UPC 11110 91484, and lot code of 7150KT7 or 7150KT8;
Fresh Selections Veggie Tray with Ranch Dip, 6.75 ounces, with UPC 11110 91472, and lot code of 7151KT3 or 7150KT6;
Fresh Selections Snack Tray with Almonds & Apples, 5.75 ounces, with UPC 11110 91481, and lot code 7150KT5 or 7150KT7;
Club Chef Snack Tray with Baby Carrots, Sliced Apples, Raw Almonds and Cheddar Bar, 5.75 ounces, with UPC 0 17278 00703 7 and lot code 7150KT6;
Club Chef Snack Tray with Peanut Butter, Celery Sticks, Sliced Apples, Peanut Butter and Raisins, 5.5 ounces, with UPC 0 17278 00704 4 and lot code 7150KT6;
Club Chef Snack Tray with Sliced Apples, Almonds, Raisins and Caramel Dip, 4 ounces, with UPC 0 17278 00701 3, and lot code 7150KT6;
Club Chef Veggie Tray with Carrots, Celery, Broccoli Florets and Ranch Dip, 6.75 ounces with UPC 0 17278 00702 0, and lot code 7150KT6
 
States potentially included in the recall, according to release, are South Carolina, Georgia, Alabama, Indiana, Illinois, Missouri, Ohio, Kentucky, West Virginia, Tennessee, Mississippi, Arkansas, Virginia, North Carolina and Michigan.