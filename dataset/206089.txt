When USDA's weekly crop condition ratings are plugged into Pro Farmer's weighted (by production) Crop Condition Index (CCI; 0 to 500 point scale, with 500 representing perfect), the corn crop improved by 1.18 point from last week, but is still 27.38 points lower than year-ago. Meanwhile, our CCI for soybeans gained 2.28 points, but is 27.87 points lower than year-ago.





Pro Farmer Crop Condition Index






Corn




This week



Last week



Year-ago





Colorado (1.03%*)

3.74




3.78





3.79




Illinois (15.40%)

52.83




52.83





62.70




Indiana (6.64%)

22.64




22.58





26.67




Iowa (17.72%)

62.37




62.55





67.70




Kansas (4.29%)

14.64




14.81





14.46




Kentucky (1.57%)

6.21




6.26





6.34




Michigan (2.35%)

8.10




8.05





8.91




Minnesota (9.66%)

37.87




37.87





38.03




Missouri (3.81%)

13.86




13.75





14.02




Nebraska (11.62%)

43.01




41.97





45.26




North Carolina (0.71%)

2.88




2.88





2.73




North Dakota (2.70%)

9.16




9.06





9.71




Ohio (3.80%)

13.88




13.92





13.64




Pennsylvania (0.98%)

4.33




4.24





3.58




South Dakota (5.62%)

17.19




17.07





19.51




Tennessee (0.89%)

3.81




3.82





3.41




Texas (2.06%)

8.19




8.19





6.98




Wisconsin (3.61%)

13.64




13.64





14.49




Corn total









358.19












357.01





385.57









Pro Farmer Crop Condition Index






Soybeans




This week



Last week



Year-ago





Arkansas (3.78%*)

14.17




14.13





14.39




Illinois (13.85%)

47.65




47.10





55.28




Indiana (7.41%)

25.49




25.42





29.13




Iowa (13.35%)

47.13




46.33





52.40




Kansas (3.96%)

12.79




12.95





14.24




Kentucky (2.14%)

8.28




8.30





8.68




Louisiana (1.59%)

5.73




5.73





5.69




Michigan (2.38%)

8.01




8.17





9.02




Minnesota (8.82%)

33.09




33.26





33.89




Mississippi (2.38%)

9.32




9.10





9.43




Missouri (5.86%)

20.97




21.27





22.26




Nebraska (7.46%)

27.17




26.57





29.51




North Carolina (1.50%)

5.89




5.72





5.70




North Dakota (5.24%)

17.72




17.30





17.90




Ohio (6.14%)

21.75




21.93





22.14




South Dakota (5.93%)

19.81




19.40





20.66




Tennessee (1.86%)

1.00




1.00





1.00




Wisconsin (2.29%)

8.93




9.03





8.62




Soybean total









348.95












346.67





376.82




* denotes percentage of total national corn crop production.
Iowa: It was mostly dry in Iowa with above normal temperatures for the week ending September 17, 2017, according to USDA, National Agricultural Statistics Service. Statewide there were 6.2 days suitable for fieldwork. With increased heat and little moisture, crops matured rapidly in the past week. Activities for the week included seeding cover crops, spreading manure, harvesting seed corn, chopping corn silage, and hauling grain. Topsoil moisture levels rated 21 percent very short, 30 percent short, 49 percent adequate and 0 percent surplus. According to the September 12, 2017 U.S. Drought Monitor, parts of south central and southeast Iowa remain in extreme drought status. Subsoil moisture levels rated 20 percent very short, 34 percent short, 46 percent adequate and 0 percent surplus. Eighty-eight percent of the corn crop has reached the dent stage or beyond, eight days behind last year and three days behind the 5 - year average. Thirty percent of corn had reached maturity, six days behind last year and average. Reports were received from throughout the state that corn harvest for grain has begun. Corn condition declined slightly to 59 percent good to excellent. Seventy-four percent soybeans were turning color or beyond, two days behind last year but one day ahead of average. Thirty - one percent of soybeans were dropping leaves, one day behind average. Scattered soybean fields across most of the state have been harvested. Soybean condition dropped to 58 percent good to excellent.
Illinois: Fall harvest is underway throughout the state. There 6.7 were days suitable for fieldwork during the week ending September 24. Statewide, the average temperature was 76.8 degrees, 14.3 degrees above normal. Precipitation averaged 0.59 inches, 0.20 inches below normal. Topsoil moisture supply was rated at 32 percent very short, 45 percent short, and 23 percent adequate. Subsoil moisture supply was rated at 23 percent very short, 50 percent short, and 27 percent adequate. Corn dented was at 94 percent, compared to 98 percent for the 5 - year average. Corn mature was at 57 percent, compared to 84 percent last year. Corn harvest was 11 percent complete, compared to 24 percent for the 5 - year average. Corn condition was rated 4 percent very poor, 10 percent poor, 34 percent fair, 43 percent good, and 9 percent excellent. Soybeans coloring was at 85 percent, the same as the 5 - year average. Soybeans dropping leaves was at 57 percent, compared t o 58 percent last year. Soybean harvest was 9 percent complete, compared to 5 percent last year. Soybean condition was rated 5 percent very poor, 10 percent poor, 29 percent fair, 48 percent good, and 8 percent excellent.
Indiana: HA hot and mostly dry week left West Central Indiana in moderate drought conditions going into harvest season, according to Greg Matli, Indiana State Statistician for the USDAs National Agricultural Statistics Service. A portion of Northern Indiana and Southwestern were rated as abnormally dry by the U.S. Drought Monitor. Each of these areas did receive rainfall during the week, but it was not enough to offset the abnormally hot temperatures seen across the State. The statewide average temperature was 76.1 degrees, 1 3.9 degrees above normal. Statewide precipitation was 0.87 inches, above average by 0.16 inches. There were 5.9 days available for fieldwork for the week ending September 24, unchanged from the previous week. Regionally, corn was 42% matured in the North, 58% in Central, and 69% in the South. Corn was 4% harvested for grain in the North, 6% in Central, and 28% in the South. Corn rated in good to excellent condition was 56% in the North, 50% in Central, and 59% in the South. Soybeans were 65% dropping leave s in the North, 66% in Central, and 61% in the South. Soybeans were 5% harvested in the North, 13% in Central, and 12% in the South. Soybeans rated in good to excellent condition were 58 % in the North, 48 % in Central, and 54% in the South.
Minnesota: Northwest Minnesota received substantial rainfall while other of parts of the state remained dry enough to begin soybean harvest during the week ending September 24, 2017, according to USDAs National Agricultural Statistics Service. There were 4.4 days suitable for fieldwork . Harvest continued for corn silage, dry beans, potatoes, sugarbeets and alfalfa hay. Topsoil moisture supplies rated 1 percent very short, 9 percent short, 76 percent adequate and 14 percent surplus. Subsoil moisture supplies rated 3 percent very short, 12 percent short, 79 percent adequate and 6 percent surplus. Ninety - three percent of the corn for grain crop reached the dent stage, 6 days behind the 5 - year average. Thirty - three percent ha d reached maturity, 9 days behind average. There were scattered reports of corn for grain being harvested in southeast Minnesota. Corn harvested for silage was 60 percent complete, one week behind average. Corn condition was unchanged at 81 percent good to excellent. Ninety - three percent of the soybean crop was turning color with 64 percent dropping leaves. Soybeans were 7 percent harvested . Soybean condition declined slightly to 7 1 percent good to excellent.