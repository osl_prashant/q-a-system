September apple volumes and sales were down this year at retail compared with last year because of a harvest gap, Nielsen Fresh Facts data show.
Washington state apples had a record early harvest start last year, according to a news release from Wenatchee, Wash.-based Stemilt Growers, and started about 10 days later than normal this year, causing the lag at retail. 
Volume and sales should pick up soon as harvests conclude and retailers have promotable supplies, Brianna Shales, Stemilt communications manager, said in the release.
Apples were 5.9% of total produce department sales in September, compared with 6.5% last year, according to Stemilt’s latest Fast Facts video analyzing Nielsen data. 
Gala, red delicious, fuji, Honeycrisp and granny smith were the top five varieties, and club variety Sweetango cracked the top 10, according to the release.
The average September retail price for all varieties was $1.66, and nearly 66% of sales were in bulk. Two-thirds of bagged apple sales in September were 3-pound bags, according to the release.
“Sizing is smaller on apples than in 2016, which presents plenty of promotion opportunities on bags,” Shales said in the release.
She said 5-pound bags also provide a good opportunity as consumers seek more apples for their holiday baking.