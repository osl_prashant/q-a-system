Almost five months after wildfires ripped across much of the northeastern Texas Panhandle, the Texas A&M AgriLife Extension Service will host a daylong Range Recovery Meeting and Field Discussion.The traveling event is set to cross the fire-stricken counties Aug. 3, beginning with breakfast at 7 a.m. in Pampa and concluding with a steak dinner at 6:30 p.m. in Lipscomb, said Danny Nusser, AgriLife Extension agriculture and natural resources regional program leader in Amarillo.
“This program is designed to help producers evaluate their rangeland and make decisions related to quality and quantity of forages, nutritional decisions regarding livestock, future preparedness and weed or brush control,” Nusser said.
There is no charge for any part of the meetings, but those planning to attend are asked to contact Jordan Chandler at 806-677-5600 or their county AgriLife Extension office for planning purposes.

Ranchers are ready to move beyond the hay feeding after wildfires in March. (Texas A&M AgriLife photo by Kay Ledbetter)

Nusser said the program is designed to accommodate individuals in all the affected counties. Emphasis will be placed on burned pastures, but the information is pertinent to all producers and current range conditions.
The series of meetings will begin with the Capital Farm Credit Volunteer Recognition Breakfast at the AgriLife Extension-Gray County office, 12125 E. Frederick Ave., Pampa. Topics and speakers there will be:
Economics of recovery efforts, Dr. Steve Amosson, AgriLife Extension economist, Amarillo.
USDA-Natural Resources Conservation Service opportunities and report, Eddy Corse, district conservationist, Pampa.
Cost vs. Benefits of Pasture, Rangeland and Forage Rainfall Index Insurance, Raven Spratt, senior crop insurance agent, Pampa.
The next stop will be at 10 a.m. at the Little Red Schoolhouse north of Pampa. Topics and speakers will be:
Grass/Range Evaluation in Roberts/Gray Counties, Max Payne, area rancher.
After Fire Rangeland Response and Grazing Decisions, Dr. Tim Steffens, AgriLife Extension range specialist, Canyon.
The group will travel to Mesa Vista Ranch for the lunch program, where AIM Bank of Miami will sponsor lunch. Topics and speakers will be:
Range Nutritional Value and Beef Cow Nutritional Needs, Dr. Ted McCollum, AgriLife Extension beef cattle specialist, Amarillo.
Utilization of Prescribed Burns to Help with Prevention, Dr. Morgan Russell, AgriLife Extension range specialist, San Angelo.
The group will then travel to the Locust Grove Baptist Church in Lipscomb County. Topics and speakers will be:
Grass/Range Evaluation in Lipscomb, speakers will be from the Abbott, Mann and Walker ranches.
After Fire Rangeland Response and Grazing Decisions, Steffens.
The meeting will wrap up with the Volunteer Recognition Steak Dinner at the Lipscomb County Museum, sponsored by Plains Land Bank and Capital Farm Credit.
“We encourage you to come spend the day with us or meet us at the location of your choice,” Nusser said. “The program is designed to help all ranchers make decisions about their livestock and ranch.”
 

Want to help out with the efforts to rebuild fencing destroyed by the wildfires? You only have a few more weeks to donate to the Droves/Farm Journal Foundation Million Dollar Wildfire Relief Challenge. Every dollar donated is matched by the Howard G. Buffett Foundation, up to a million dollars. To learn more, visit www.wildfirerelieffund.org.