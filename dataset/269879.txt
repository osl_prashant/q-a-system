Grains mixed and livestock mixed
Grains mixed and livestock mixed

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mixed Wednesday in early trading on the Chicago Board of Trade.
Wheat for May delivery rose 5.40 cents at $4.7240 a bushel; May corn was off .60 cent at $3.8140 a bushel; May oats was up .80 cent at $2.3140 a bushel while May soybeans fell 3 cents at $10.4160 a bushel.
Beef was mixed and pork was lower on the Chicago Mercantile Exchange.
April live cattle rose .57 cent at $1.1862 a pound; Apr feeder cattle lost .88 cent at $1.3850 a pound; April lean hogs was up 1.05 cents at .6910 a pound.