Baxter Black is known for his ag-related poetry and witticisms. As a former large animal veterinarian, how did he begin his career in entertainment?

This week on U.S. Farm Report, he addresses how he became a cowboy poet in the only way he can!

Watch Baxter Black weekends on U.S. Farm Report.