If you need time to rest your feet at the National Farm Machinery Show (NFMS) in Louisville, multitask by attending a live taping of the U.S. Farm Report marketing roundtable.

The taping will begin at 2:00 p.m. in Room 105 of the South Wing B Conference Center at the Kentucky Exposition Center, but you’ll want to come early to snag a seat because this event is a standing-room only event.

The four panelists will discuss markets, ag lending, and how to prepare as 2018 is underway.

Host: Tyne Morgan

Time/Date: Feb. 15, 2018 at 2:00 p.m.

Guests: Bob Utterback, Utterback Marketing
             Chip Nellinger, Blue Reef Agri-Marketing
             Jarod Creed, JC Marketing Services
             Alan Hoskins, American Farm Mortgage
Cost: FREE

Sponsored by: Kubota and Trivapro