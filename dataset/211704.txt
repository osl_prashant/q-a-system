Chelan Fresh Honeycrisp up
Chelan, Wash.-based Chelan Fresh will have about 40% more Honeycrisp this season, its first after the merger with Yakima, Wash.-based Borton Fruit.
Overall volume will be up by about one-third thanks to the deal, said director of marketing Mac Riggan.
The company is ramping up its organic program, with about 1 million boxes this season expected to increase to 2 million or 2.5 million in the next three years.
It will also have about 14,000 to 16,000 boxes of new variety Sugar Bee.
 
CMI boosts new varieties
CMI Orchards expects to have more boxes of several newer varieties this season.
“We’re anticipating a 37% increase in Honeycrisp, 56% increase in Envy, 19% increase in Ambrosia, 120% increase in Kanzi and a 49% increase in Kiku,” said Steve Lutz, senior strategist for the Wenatchee, Wash.-based company.
Columbia Fruit Packers, one of the grower-owners of CMI, has a new packing line.
“They’ve been working on it over the summer and it should be ready to go with the new crop,” Lutz said.
New to CMI since last season are George Harter, vice president of marketing, taking over for Lutz, and Danelle Huber, marketing specialist, succeeding Katharine Grove.
Harter was most recently in charge of produce merchandising for the Atlanta division of Kroger, and Huber worked for years at the Washington Apple Commission.
 
Domex looks for more Autumn Glory
Yakima, Wash.-based Domex Superfresh Growers expects about 200,000 boxes of its proprietary variety Autumn Glory this season, about double the amount it had in 2016-17.
“Last year we sold out earlier than planned, so we’re excited to continue the high volume this year,” said Catherine Gipe-Stewart, communications manager for Domex.
The apples will start shipping in early November and ideally will be available through April, although if demand stays high they may not last that long.
Autumn Glory is not a year-round option yet but will be in the future.
Domex is also increasing its organic offerings.
 
FirstFruits adds lines, storage
Yakima, Wash.-based FirstFruits Marketing of Washington has two new packing lines for conventional apples.
The company has also added robotic segregation lines, a major investment.
“After the boxes are packed now, robots put them all together, put them on pallets,” said general manager Chuck Zeutenhorst.
FirstFruits has also expanded its controlled-atmosphere storage, and next year the company will have a new loading and packed inventory building.
 
Oppy promoting Jazz, Envy
The Oppenheimer Group expects higher volumes of Washington-grown Jazz and Envy this season.
Organic volumes of those varieties will also be up, according to the Vancouver, British Columbia-based company.
Promotions will include a consumer sweepstakes for Jazz and a November roadshow stop in Texas for Envy.
The Pacific Rose variety continues to sell well overseas, but it is also growing in popularity in the U.S., particularly with Asian and specialty retailers.
“Washington Pacific Rose now sits at No. 8 among premium apples for retail sales dollars earned,” said Oppy vice president of categories David Nelley, citing IRI data for September 2016 through May 2017. “Envy and Jazz are ranked No. 5 and 4, respectively.
“This trio of high-performing varieties is becoming essential to a strong apple set at retail,” Nelley said.
 
Honey Bear gets Smitten
Honey Bear Tree Fruit Co. will have its first big crop of Smitten apples this year.
The Wenatchee, Wash.-based company expects about 200,000 boxes of its newest variety.
Honey Bear will partner with retailers in October to promote a fundraiser for breast cancer awareness foundation Susan G. Komen.
For every box of Smitten sold, Honey Bear will donate to the organization.
The Smitten logo features the color pink, which has been associated for years with Breast Cancer Awareness Month, which is October.
 
Northern Fruit expecting more Honeycrisp
With new plantings coming of age, Northern Fruit plans to have more Honeycrisp to offer this season.
Overall, the Wenatchee, Wash.-based company has remained focused on mainstream varieties, but it planted lots of Cosmic Crisp trees this spring, said domestic sales manager Bill Knight.
Northern Fruit planned to start harvesting galas the week of Aug. 21.
 
Oneonta expecting more Juici
Oneonta Starr Ranch Growers plans to have double the volume it had last season on its Juici variety.
“The markets that we’ve been selling in have gotten really good results,” said Scott Marboe, director of marketing for the Wenatchee, Wash.-based company. “We’ll go back into those existing markets and put some more in there and go forward with it.”
Oneonta planned to begin harvesting its earliest varieties around the end of August.
“Right now the crop looks to be spread out real well with sizes,” Marboe said. “We’re pretty happy with what we’re seeing on the harvest window.
“We think it’s going to be real orderly,” Marboe said. “Labor doesn’t seem to be an issue, so we’re real happy with that.”
 
Pacific Pro ready for e-log changes
Randy Hartmann, president of Bellevue, Wash.-based Pacific Pro, said the company has been preparing for the new transportation regulations that go into effect this year.
Trucks will be limited on how may hours they can run in a 24-hour period — meaning customers may not be able to get deliveries on as quick a turnaround as they once did — so Pacific Pro developed a plan to help save some of those hours.
“We kind of saw the writing on the wall about two years ago, so we started a cross-dock and consolidation facility in the middle of apple country. The expectation is if we run our own trucks around and consolidate from all the multiple warehouses to one location, our warehouse, then the long-haul trucks can come in and just do one pickup,” Hartmann said. “We can set an appointment, they can come in at that time and get loaded and then be down the road so they won’t have to lose a day or two in the loading process.”
 
Rainier warehouses adding lines
Selah, Wash.-based Rainier Fruit continues to grow, with more acreage and equipment updates coming.
The company will be growing more Envy, Jazz, Lady Alice, Honeycrisp, Pink Lady, fuji and gala, said executive vice president of sales and business development Randy Abhold.
Rainier is also increasing its organic volume 10% to 15%.
“We’ll be able to and set to promote not only breadth (but) depth in the organic program, so breadth would be more items to be promoted on a more regular basis in volume but also to go deeper in the season,” Abhold said.
Additionally, two warehouses are scheduled to get new lines, one in time for the current crop and the other for the 2018-19 season.
The line scheduled for next year will significantly increase capacity, Abhold said.
 
More Breeze coming for Sage Fruit
Sage Fruit Co. expects its total apple volume to be up 12% to 15% this season.
The Yakima, Wash.-based company is projecting about 50,000 boxes of its new Breeze variety.
Sage had only a few loads of Breeze last season, said Chuck Sinks, president of sales and marketing.
The company will also have much higher organic volume this year, up about 13-fold over 2016-17.
“We’ve been transitioning for a while and everything’s coming to fruition this crop season, so we’re excited about our growth in organics and our total organic program,” Sinks said.
 
Honeycrisp volume growing for Stemilt
Wenatchee, Wash.-based Stemilt Growers will have 60% more conventional Honeycrisp and 230% more organic Honeycrisp this season.
The bulk of the growth is in a new Honeycrisp variety called Royal, said marketing director Roger Pepperl.
“This Royal tends to grow redder and a nicer size range, and what it allows us to do is to pick the fruit when it’s red but it still has a lot of starch inside of it, so it can be put into controlled-atmosphere storage and stored for a longer period of time, and then when you come out of CA storage that starch converts to sugar, and you have a delicious piece of fruit in the late spring, earlier summer ... that still has great condition,” Pepperl said.
Stemilt continues work on its nearly 500,000-square foot distribution center, which is expected to be complete in mid-2018.
“The big feature of it is that it has the automated storage retrieval system, so the pallet positions are all selected based on automation, so it’s a lot different than we do things here today, so it’s really exciting,” said Brianna Shales, communications manager. “That’s going to be the main feature of it, but we’ll also have quality control areas, inspection areas, repackaging, palletizing, a lot of stuff housed in that building as well as this pear ripening center.”