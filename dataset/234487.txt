AP-KS--Kansas News Digest 5 pm, KS
AP-KS--Kansas News Digest 5 pm, KS

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Kansas.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
NEW & UPDATED:
Updates — DOCTOR FINED-OPIODS, CHILD MISSING-KANSAS
New APNN - KANSAS WHEAT, FATAL OFFICER SHOOTING-SOUTHWEST KANSAS, KANSAS FATAL PURSUITS-LAWSUIT, KOBACH-NRA CONVENTION
AROUND THE STATE:
DOCTOR FINED-OPIOIDS
OVERLAND PARK, Kan. — The Kansas medical board has fined a doctor $2,500 and put his license on probation after an investigation revealed improper and potentially dangerous opioid prescribing. The Kansas City Star reports that the Kansas Board of Healing Arts released its order this month saying its investigation into Overland Park doctor Joseph Baker began after a complaint from a pharmacist. SENT: 276 words.
CHILD MISSING-KANSAS
WICHITA — The stepmother of a missing Wichita 5-year-old is now criminally charged with endangering a child. Emily Glass, 26, tepmother if Lucas Hernandez, was charged Monday and will be held on $50,000 bond. She said during a brief court hearing that she would hire her own attorney before her next court appearance March 13. UPCOMING: 300 words.
EXCHANGE-KANSAS CHEMIST-IMMIGRATION ARREST
LAWRENCE, Kan. — From war-torn Bangladesh to wheat-rich Kansas, vulnerability has followed scientist Syed Ahmed Jamal. Before the story of his arrest and potential deportation went viral and attracted the support of thousands, Jamal wrote from jail about the persecution he faced in his home country. By Rick Montgomery, The Kansas City Star reports. SENT: 1,609 words.
IN BRIEF:
— KANSAS WHEAT — A new government report estimates that nearly half of the winter wheat crop in Kansas is struggling for lack of adequate moisture.
— FATAL OFFICER SHOOTING-SOUTHWEST KANSAS — Authorities have determined that no charges will be filed in a fatal officer-involved shooting in southwest Kansas.
— KANSAS FATAL PURSUITS-LAWSUIT  — The family of a woman killed by a driver fleeing from a Kansas Highway Patrol trooper is suing the state of Kansas.
— KOBACH-NRA CONVENTION — Kansas Secretary of State Kris Kobach is urging the National Rifle Association to bring its annual convention to Kansas.
— WICHITA HOMICIDE — Wichita police say a 22-year-old man is jailed on suspicion of murder after a fatal shooting in southeast Wichita during the weekend.
— IDENTICAL TRIPLETS — A Kansas couple is celebrating after become parents to a rare set of identical triplets.
SPORTS:
LAWRENCE — The Texas Longhorns might be fighting for an NCAA tournament berth when they visit Kansas on senior night in Lawrence. Upcoming from 8 p.m. start.
___If you have stories of regional or statewide interest, please email them to apkansascity@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Missouri and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.