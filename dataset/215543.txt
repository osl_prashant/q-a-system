Q: With an extremely wet spring and summer, my silage corn has started to develop some mold. The mold isn"t excessive; however, I am concerned about potential mycotoxins that could end up in the feed. Is there anything I can do to address this issue at ensiling? 
A:  It is always good to hear someone wanting to address this problem head on and as early as possible. As you know, mycotoxins can be dangerous to cows and affect cow performance, fertility and health.
While it is always a good idea to get your forage tested for mycotoxins, keep a couple things in mind:

Toxins tend to occur in hot-spots and therefore can be missed in testing.
There are more toxins existing than are currently tested for with commercial analysis.  If you are at risk for toxins in your silage, or other feeds, it may be worth including a good toxin binder in your TMR for 30 days to gauge if there is a production response.

Since mycotoxins are a product of molds, you will want to minimize mold growth in silages.
Molds tend to grow after ensiling at high pH levels and when exposed to air at feedout. During ensiling, you can work to keep the existing molds from growing by ensuring an efficient fermentation, as well as minimizing air exposure and maintaining aerobic stability. 
A proven fermentation aid inoculant will rapidly lower the forage pH to a level that will inhibit most mold growth. A spoilage inhibitor inoculant including the FDA reviewed high-dose Lactobacillus buchneri 40788 will reduce mold growth and spoilage at feedout. For maximum protection, combine these two types of inoculants with Biotal® Buchneri 500.
I hope this information helps.
 
Sincerely,
The Silage Dr.