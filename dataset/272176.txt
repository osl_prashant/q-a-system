NY governor condemns federal raid on upstate dairy farm
NY governor condemns federal raid on upstate dairy farm

By DAVID KLEPPERAssociated Press
The Associated Press

ALBANY, N.Y.




ALBANY, N.Y. (AP) — New York Gov. Andrew Cuomo threatened to sue federal immigration officials Wednesday following a raid on an upstate dairy farm during which armed federal agents allegedly trespassed on private property and handcuffed an American citizen who tried to videotape the incident.
John Collins said the agents who entered his farm in Rome, New York, did not identify themselves or produce a warrant before they arrested a farm worker on charges that he unlawfully re-entered the U.S. following deportation. Collins, who said the worker has proper documentation to work in the U.S., said the officers grabbed his phone and threw it — destroying it — when he used it to videotape the arrest. Collins said he was then placed in handcuffs and threatened with arrest.
Cuomo, a Democrat, sent a cease and desist letter to the U.S. Immigration and Customs Enforcement agency on Wednesday in response to conduct that he deemed "un-American."
"We believe ICE is violating the law and endangering public safety," said Cuomo. "If they continue, the state will sue them. Period."
A spokesman for the U.S. Department of Homeland Security fired back in response to Cuomo's letter.
"Gov. Cuomo's disregard for the rule of law is a slap in the face to the hardworking men and women of ICE whose mission it is to uphold the laws Congress passed," spokesman Tyler Q. Houlton said in a statement.
According to an affidavit provided by ICE, the officers were visiting the farm in an attempt to locate a second individual when they encountered the farm worker, Marcial de Leon Aguilar. The documents say Aguilar was lawfully arrested by deportation officers, who identified themselves and had a proper warrant. ICE said it has removed Aguilar from the U.S. three times, most recently in January 2014. Aguilar was also convicted of reckless aggravated assault, a felony, according to ICE.
In a statement, ICE Deputy Director Thomas D. Homan said "ICE officers acted professionally and within their legal authorities under federal immigration law."
The incident has also prompted scrutiny in Washington, where Democratic U.S. Sen. Kirsten Gillibrand has called for an investigation.