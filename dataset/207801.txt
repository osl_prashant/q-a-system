The Chilean Fresh Fruit Association and Chilean Citrus Committee are offering a full menu of promotions this year, said Karen Brux, managing director of the San Carlos, Calif.-based association.
That includes activities at the recent Southern California Fresh Produce & Floral Council Expo, July 18 in Anaheim, Calif.
“The Chilean Citrus Committee has exhibited at this show for a number of years,” Brux said. “We partner with Tajin throughout the season on navel demos, so at the FPFC show, we hand out citrus samples sprinkled with Tajin, as well as citrus-infused water.”
The association also showcases marketing support available for lemons, navels and mandarins, Brux said.
“We’re in the heart of our season, which extends through October, and the booth was busy throughout the day with retailers interested in promotion ideas,” she said.
Such regional shows offer value to the Chilean citrus industry, Brux said.
“They’re the perfect venue for connecting with Chilean citrus importers, wholesalers and retailers,” she said.
“We can introduce what’s new for the season and discuss promotion opportunities. They’re also a very effective forum for communicating crop updates.”
Direct contact with buyers at the shows is crucial, Brux said.
“We offer tailored, targeting marketing programs to all retailers, so the one-on-one interaction with key buyers and merchandisers attending the various produce trade shows is incredibly beneficial,” she said.
“At the West Coast Produce Expo alone, our West Coast merchandiser was able to connect with 17 retail chains. Imagine how long it would take him to do that via phone or e-mail, or how many trips he’d have to take in order to meet with all of these customers.”
The Chilean Citrus Committee exhibits at the West Coast Produce Expo, FPFC Southern California Expo and the New England Produce Council’s Produce, Floral and Food Service Expo, which takes place in mid-September, Brux said.
The association is working with more than 20 retail chains on everything from digital coupons and demos to sponsorships of cooking classes and marathons, Brux said.
On the social media front, the Chilean Citrus Committee has launched four months of social media promotions, running from July through the end of October.
The program consists of three monthlong Facebook promotions in July, August and September, as well as an ongoing Instagram promotion extending from July through October.
“A Day in the Life of a Lemon” launched on the Fruits from Chile Facebook page on July 6, with four weekly drawings.
Each week, participants are invited to share recipes, photos or usage ideas connected to a different eating occasion. The first week started with breakfast, with consecutive weeks turning to lunch, dinner and dessert.
There are weekly drawings for a $100 gift card, along with a cooking utensil or appliance connected to the week’s theme.
“In August, the focus will shift to navels,” Brux said.
The third and final Facebook promotion in September will highlight mandarins, which Brux said is Chile’s “largest and fastest-growing citrus category.”
“Meanwhile, an Instagram promotion will encourage consumers to share their favorite lemon, navel orange or easy peeler photo, with weekly gift card drawings through October,” Brux said.
In addition to promotions on the Fruits from Chile social media platforms, the Chilean Citrus Committee also has new images, usage videos and infographics available for retailers to use in their social media programs.