In Episode 9 of Silage Talk, the Dairy Herd Management team gets helpful advice on high moisture corn from Dr. Bob Charley, Forage Products Manager with Lallemand Animal Nutrition.

Dr. Charley addresses the following questions:

High moisture corn (HMC) season is here.What is the interest in HMC for dairy producers?
How many types of HMC are there?
What issues could producers face making HMC?

Sponsored by Lallemand Animal Nutrition.