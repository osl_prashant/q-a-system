Wal-Mart and Costco have made market share gains in the Canadian retail market in recent years, a new government report says.
In a report on the Canadian retail sector, the U.S. Department of Agriculture’s Foreign Agricultural Service said consolidation has fueled the shift from independent retailers to regional and national chain stores.
A relatively stronger U.S. dollar also has increased the challenges facing food retailers seeking to remain competitive, according to the USDA.
In 2016, about 58% of food sales occurred at traditional grocery stores, with the vast majority under the umbrella of Canada’s three major chains: Loblaws, Sobeys and Metro. Canada’s retail food sales totaled $73 billion in 2016, about 17% of total Canada’s retail sales.
However, the report said non-grocery retailers, particularly Costco and Walmart, have gained market share in the last five years, accounting for 20% of the retail grocery market in 2016.

Each of Canada’s major supermarkets has a discount banner, and the discount channel grew from 27% in 2011 to 36% in 2016, according to the report. That trend is predicted to grow, as discount merchandisers, such as Dollarama and Dollar Tree, continue to expand food offerings.
The report said many Canadian consumers have become bargain shoppers due to the slow economic recovery and rising food prices in 2015 and 2016. The Royal Bank of Canada has reported that 57% of shoppers carefully compare food prices and have reduced impulse purchases, according to the report.
Online grocery shopping continues to be slow to take off in Canada, despite Amazon’s presence there since 2014.
Total online retail food sales was a little over 1% in 2016, according to the report, compared to 2.5% in the U.S. Amazon’s 2017 acquisition of Whole Foods will translate to increasing online grocery sales in the coming years, according to the report.
Major Canadian retailers have had challenges with home delivery orders, and instead have encouraged in-store pick-up of online orders.
The top food retailers in Canada in 2016 were:

Loblaw Cos. Ltd./Shoppers Drug Mart, $28.9 billion;
Sobeys Inc. (Empire)/Safeway, $20.8 billion;
Metro Inc., $11.1 billion;
Costco Canada Inc., $10.1 billion; and
Walmart Canada Corp., $7 billion.

Fruit and vegetable import growth
Canadian imports of U.S. fresh vegetables in 2016 were $1.8 billion, while fresh fruit imports totaled $1.6 billion. Retailers have enjoyed duty free access to U.S. fruits and vegetables since 1998.
Canadian consumers are graying and ethnically diverse.
There are more Canadians over 65 than under 15 years of age, according to the report, and seniors account for the fastest-growing age group.
“However, with nearly 1 in 5 Canadians born overseas, the increasing ethnic diversity of the Canadian population is reflected in retail offerings and the spread of ‘ethnic foods,’” the report.
The report said taste continued to be the driving force influencing most Canadians in retail aisles.
Freshness also is associated with quality among Canadian consumers.
“The consumption of fruits and vegetables by Canadians has increased significantly over the last decade, as more than 40% of Canadians consume fruits and vegetables five or more times a day,” the report said.