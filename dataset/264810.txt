As the Potatoes USA annual meeting came to a close, the group welcomed a new chairman and executive committee.
Dan Moss, Declo, Idaho, succeeded John Halverson, Arbyd, Mo., as the new Potatoes USA chairman, for a one-year term.
Other executive committee members are:

Jaren Raybould, Saint Anthony, Idaho, and Phil Hickman, Horntown, Va, co-chairmen (Domestic Marketing Committee);
Marty Myers, Boardman, Ore., and Steve Streich, Kalispell, Mont., co-chairmen (International Marketing Committee);
Sheldon Rockey, Center, Colo., and Heidi Randall, Friesland, Wis., co-chairman/woman (Industry Outresch Committee);
Chris Hansen, Bliss, N.Y., and Eric Schroeder, Antigo, Wis., co-chairmen (Research Committee); and
Jason Davenport, Arvin, Calif., chairman (Finance and Policy Committee).