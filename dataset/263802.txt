Brazil halts poultry exports to EU from company under probe
Brazil halts poultry exports to EU from company under probe

The Associated Press

SAO PAULO




SAO PAULO (AP) — A major Brazilian food processor says the government has temporarily suspended the production and health certification of the company's poultry exports to the European Union.
BRF did not give a reason for the Ministry of Agriculture's decision in a statement posted on the company's website Friday. But BRF is involved in a food scandal related to whether it evaded safety checks.
The ministry did not immediately have a comment.
BRF said that any products sent to the EU before Friday could be sold without restriction. It said that Agriculture Ministry officials plan to meet with EU food safety authorities next week and the suspension will be reviewed after the meeting.
The company has said it is cooperating with the investigation and that it follows all Brazilian and international regulations.