This market update is a PorkNetwork weekly column reporting trends in weaner pig prices and swine facility availability.  All information contained in this update is for the week ended May 12, 2017.

Looking at hog sales in November 2017 using December 2017 futures the weaner breakeven was $27.24, down $1.38 for the week. Feed costs were down $0.01 per head. December futures decreased $0.68 compared to last week’s December futures used for the crush and historical basis was the same as last week. Breakeven prices are based on closing futures prices on May 12, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations don’t take into account market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was below average this week with 36,375 head being reported which is 99% of the 52-week average. Cash prices were $31.08, up $0.20 from a week ago. The low to high range was $22.00 - $36.50. Formula priced weaners were up $0.17 this week at $37.74.
 
Cash traded feeder pig reported volume was above average with 11,800 head reported. Cash feeder pig reported prices were $56.42, up $1.67 per head from last week.
 
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through May 12, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential decreased $1.58 this week to a projected loss of $3.84 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.