Minnesota watershed uses woodchips, wetlands to treat water
Minnesota watershed uses woodchips, wetlands to treat water

The Associated Press

HASTINGS, Minn.




HASTINGS, Minn. (AP) — Watershed experts are using wetlands and woodchips to try and reduce nitrate runoff in Vermillion River and other Minnesota waters.
Runoff from local farms has polluted the waters, Minnesota Public Radio reported . High concentrations of nitrates can give adults headaches and cramps, and cause life-threatening blue baby syndrome in infants.
Nitrate levels in groundwater near the river's mouth in Hastings have risen over the past two decades, despite local farmers trying to curb runoff. Recent tests in one of the river's branches detected nitrate levels at more than twice the safe drinking-water standard.
Vermillion River Watershed experts have created a 3-acre artificial wetland with more than 1,000 cubic yards of woodchips mixed into the topsoil in an effort to filter out those nitrates. There's an additional 5-acre wetland that can be used for overflow when the first wetland is full.
Wetlands are effective natural filters for water, according to a recent University of Minnesota study. The bacteria that grow on woodchips are also useful nitrate filters, said Travis Thiel, Vermillion's senior watershed specialist.
"These specific bacteria ... they more or less feed on the nitrate," he said.
Vegetation will need to grow in the area before the experiment is fully functional. Then water can be rerouted to fill the wetland.
The project is estimated to cost between $200,000 and $300,000 and could last up to 30 years, Thiel said. The watershed organization estimates the project will treat 13,600 pounds of nitrate annually.
If the hybrid treatment method is successful, it could be used more widely at other Minnesota waters, the watershed organization said.
___
Information from: Minnesota Public Radio News, http://www.mprnews.org