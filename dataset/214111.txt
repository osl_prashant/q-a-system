Months after Hurricane Harvey dumped feet of rain on Houston and Texas, economists with the Texas A&M AgriLife Extension Service are totaling up the damages caused by the summer storm, now believed to be a combined $200 million between livestock, hay, feed, cotton, rice and soybeans.

Cotton was the hardest hit crop, totaling $100 million in losses. An estimated 200,000 bales of cotton lint on the stalk was lost, valued at $62 million. An additional 200,00 harvested bales had degraded quality and are valued at $9.6 million.

The flooding also caused cattle to drown after farmers and ranchers were unable to save them. A&M believes those losses coupled with destroyed infrastructure, like fencing and hay, to be at $93 million.

“The effects of Hurricane Harvey will linger for quite some time with our Texas farmers and ranchers,” said Doug Steele, agency director of AgriLife Extension, in a statement.