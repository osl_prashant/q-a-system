BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Friday:
OpenHighLowSettleChg.ORANGE JUICE15,000 lbs.; cents per lb.Apr138.30—.50May138.90139.95137.40138.30—.50Jun138.55—.45Jul139.75140.25137.85138.55—.45Sep139.50140.80138.50139.00—.60Nov140.10140.10139.15139.60—.70Jan140.00140.25140.00140.25—.75Feb140.75—1.30Mar140.75—1.30May141.20—1.30Jul141.30—1.30Sep141.40—1.30Nov141.50—1.30Jan141.60—1.30Feb141.70—1.30Mar141.70—1.30May141.80—1.30Jul141.90—1.30Sep142.00—1.30Nov142.10—1.30Jan142.20—1.30Est. sales 1,017.  Thu.'s sales 355Thu.'s open int 12,379,  up 110