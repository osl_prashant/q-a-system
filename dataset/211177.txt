Tyson Foods Inc reported stronger-than-expected quarterly results on Monday, sending its shares up 5 percent, and said it would ramp up chicken production in the face of record demand from U.S. consumers.Tyson said it spent more money on advertising and marketing for chicken in the third quarter ended on July 1, bringing down operating income for that segment.
Still, the company is working to increase its production capacity for fully cooked chicken and raw meat sold in packages in grocery stores to meet demand, Chief Executive Officer Tom Hayes said on a conference call with analysts.
Tyson is "essentially busting at the seams on both those areas," he said. "What we are focused on is making sure our supply meets our demand, and we're having a challenge right now."
U.S. per capita consumption of chicken is expected to hit a record high this year, according to National Chicken Council annual data that runs from 1965.
Sales of chicken have increased as consumers seek more protein in their diets. It is cheaper than beef and pork and considered more healthy.
Tyson said its sales volumes for chicken rose 1.6 percent in the third quarter ended on July 1, and average prices were up 2.9 percent.
"Higher prices for chicken haven't dampened consumer demand," Hayes said.
Beef prices rose 5.3 percent on average, while pork prices rose 3.3 percent. Sales volumes for each were up less than 1 percent.
For the fiscal year, Tyson raised the low end of its profit forecast by 5 cents a share to $4.95, excluding special items. It kept the top end at $5.05 per share.
The company said it expected sales of more than $38 billion. It previously said they would remain flat at $36.88 billion.
For the quarter, Tyson said profit dropped 22 percent in its chicken unit, and operating margins fell to 10.4 percent from 13.9 percent. The company expects those margins to remain around 10 percent next year, with a nearly 3 percent growth in volume.
Net income attributable to Tyson fell nearly 8 percent to $447 million from $484 million a year earlier. Excluding items, earnings of $1.28 per share beat the analysts' average estimate of $1.18, according to Thomson Reuters I/B/E/S.
"The 3Q beat was driven by the two segments – chicken and prepared foods," JPMorgan analyst Ken Goldman said.
Sales rose 4.8 percent to $9.85 billion, increasing for only the third time in two years. Analysts on average had expected $9.48 billion.