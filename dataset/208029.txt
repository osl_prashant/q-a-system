With Northwest cherry shipments nearing their halfway point for the season, Chinese e-commerce outlet Fruitday.com is doing its share to boost online U.S. cherry sales sales. 
The Chinese fresh fruit e-commerce site Fruitday.com has published what is being called the “2017 American cherry map” to inform consumers where U.S. cherries are coming from and other characteristics of American cherries.
 
Fruitday.com is an substantial importer of cherries and other fruit and TechCrunch reported in 2015 that the online fruit marketer was used by 10 million customers in China that year. While brick and mortar sales in China still far exceed online sales, B.J. Thurlby, president of Northwest Cherry Growers, Yakima, Wash., said the entire Chinese market is performing well.
 
At close to 300,000 boxes of Northwest cherries shipped to China as of early July, China rivals South Korea as the top Asian market for Northwest cherries.
 
“We think by the end of the season we should be close to 1 million boxes in both of those markets,” he said.
 
Running strong
 
Starting harvest June 10, Northwest cherry shippers had moved close to 9 million boxes of cherries through July 5, Thurlby said. 
 
The total Northwest cherry crop has been estimated at more than 23 million cartons for 2017. 
 
Season to date rainier cherry shipments as of July 5 were 650,000 boxes, with total rainier volume expect near 1.5 million.
 
“We saw pretty good (retail) support across the U.S. markets during the Fourth of July holiday,” Thurlby said. Unlike last year, cherry promotion opportunities will continue into August this year, he said.
 
So far this year, the biggest volume shipping day for Northwest cherries was June 26, when a one-day record 650,000 boxes were shipped. 
 
“If we have another one-day single record, my guess it would probably be the early part of the week of July 9, with the confluence of the back end of the bing crop and the early part of the skeenas and lapin crop,” Thurlby said.
 
Because of the larger fruit set, fruit sizing of the Northwest cherry crop is expected to be a half-size lower than last year, Thurlby said. 
 
He said perhaps 70% to 75% of the crop is expected to be 10.5 row and larger by the end of the season, compared with 83% 10.5 row and larger last year.
 
The U.S. Department of Agriculture reported the f.o.b. price for Washington bing cherries, size 10.5 row, was mainly $36-39 on July 3, up from $30-33 per carton the same time last year.
 
In the June 30 retail report, the USDA said that 20,484 U.S. retail stores were promoting red cherries the week of June 30 at an average price of $2.75 per pound, compared with 23,397 stores promoting red cherries the same week last year at an average ad price of $2.67 per pound.