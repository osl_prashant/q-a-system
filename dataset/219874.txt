On Thursday, the USDA released its monthly World Agricultural Supply and Demand Estimates (WASDE) report, showing corn is seeing increased exports and reduced stocks. Exports have been increased to 125 million bushels, and stocks have been lowered 125 million bushels from the January report.

On the other hand, soybean exports aren’t experiencing the same strength as cor. It’s been no secret that exports have been sluggish. Exports for the 2017-18 marketing year are down 60 million bushels to 2,100 million.

“It’s bad news, but all things considered, the market has held together pretty well,” said Joe Vaclavik, president of Standard Grain, during a Facebook live.

There’s been rumors that the soybean crop in South America is strong in Brazil. Andy Shissler of S&W Trading says the USDA is looking at the weather situation in dry Argentina.

“If they don’t get rain in Argentina, you’re looking at a 7 to 10 million metric ton drop in production,” he said. “It could be even more than that. This next rain event, [it] need[s] to happen or it will be market changing on price.”


According to Kirk Hinz, meteorologist at BAMWX.com, this dry weather pattern could hang around until mid-February, or the rest of the month.

“Things will get worse before they get better,” he said on AgriTalk.

If this dry weather stays in Argentina, Shissler thinks the U.S. will see “a more dynamic market.”

“It could add a buck in beans,” he said. “With our weather here—it could be even more than that.”

While this price sounds better, there’s still a question of acreage. Some analysts believe soybean acres will top corn at upwards of 91.5 to 92 million acres. Vaclavik thinks with the price of December corn, the acreage mix will be 50/50 this year.

“I don’t see a big acreage rotation much different than last year—I don’t think there’s the incentive for either crop,” he said.

Corn exports in the WASDE report were especially impressive for Vaclavik. He said if this trend continues, farmers could see stronger corn prices, upwards of $4.

“You can figure the rest out, but you can’t make a market rally,” said Shissler.