Farm Journal Associate Field Agronomist Missy Bauer recommends taking a couple of days yet this winter to give your planter a comprehensive check-up, for that reason.
 
“Every 1,000 ears per acre is worth 5 bu. to 7 bu.,” she says. “It’s pretty common to pick up several thousand ears per acre as a result of good planter setup, and that can result in significant additional revenue.”
 
In the video, Bauer reviews some of the critical parts of the planter that you’ll want to evaluate. If you’re interested in learning more details on how to get your planter ready for spring, consider attending the planter clinic Bauer and her team are hosting in March. 
 
You can get more details here:
https://www.bmcropconsulting.com/events-2/