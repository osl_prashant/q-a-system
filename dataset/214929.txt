U.S. Customs and Border Protection officers at the Pharr (Texas) International Bridge cargo facility recently interdicted heroin and marijuana in loads of fresh produce.
CBP officers on Nov. 12 encountered a blue 1997 Freightliner tractor-trailer hauling a shipment of broccoli, according to a news release.
The rig was referred for a secondary inspection, where officers using non-intrusive imagining and a canine team located and seized 1,372 packages of alleged marijuana comingled within a shipment of broccoli, the release said.
CBP estimated the street value of the drugs at almost $237,000.

On Nov. 14, CBP officers referred a white 2014 Kenworth tractor-trailer hauling a shipment of tomatoes for a secondary inspection, where they extracted 90 packages containing more than 54 pounds of alleged heroin with have an estimated street value of nearly $2.2 million, according to the release.
Customs officers seized the drugs, and the cases remain under investigation by Homeland Security Investigations special agents.