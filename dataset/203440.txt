Retail and foodservice customers are looking for any edge they can find in a competitive St. Louis marketplace, and they're looking to local produce vendors for help, suppliers say.
It adds up to a healthy marketplace, said Joan Daleo, president of Ole Tyme Produce Inc., a foodservice distributor that moved to St. Charles, Mo., in late August.
"I think it's healthier than it's been since the recession," she said.
A changing marketplace is the cause, Daleo said.
"While St. Louis lags, it ultimately reflects that trends that are out on the East and West coasts," she said.
In the restaurant business, fast-casual sector is a good example, Daleo said.
"It's hot here and it was hot everyplace else a year or two ago," she said. "You're seeing a decline, in general, in the casual-dining segment, but there are a lot of segments doing well here."
A local economy is helping that along, Daleo said.
"Since the economy is doing well here, you're ticking up in almost all segments," she said.
Dominic Greene, vice president of operations and sales manager with St. Louis wholesaler United Food and Produce Co. Inc. says he has seen a similar trend.
"Just starting out now, we believe over the last five years we have seen an increase in promotion and sales directed toward foodservice," he said.
Helping that is an increased focus on promotions for fresh food aimed at children, Greene said.
Competition also applies to distributors, and some things in that area don't change, said Jim Heimos Jr., president of wholesaler Heimos Produce Co.
"I'd definitely say pricing keeps you competitive most of the time in this day and age," he said. "Price and quality drives how much business you're getting."
Dan Pupillo Jr., president of St. Louis wholesaler Midwest Best Produce Co. Inc., agreed.
"Basically, all we are is a service arm: If we don't provide some extra service to make their job easier we wouldn't be here," he said.
IT also helps to try to carry some items competitors might not have, said R.J. Barrios, buyer for Sunfarm Foodservice Inc. in St. Louis.
"We carry the biggest supply of the specialty items, not the regular conventional items you would see; that's a lot of the customers we have," he said.
Customers will find items like watermelon radishes, durian, ackee, and dragon fruit at Sunfarm, Barrios said.
"They like the sort of craft, artisan vegetables we can get them," he said.
Meanwhile, competition is heating up on the retail side, with the entrÃ©e of new chains, such as Lucky's, Ruler Foods, Fresh Thyme and others, distributors said.
"It's an interesting question regarding how a couple of small chains with a couple of stores here and there can possibly affect the market in St. Louis," Greene said. "There's probably 160 or more stores in this area, so it's hard to say that maybe eight or 10 new stores can affect things greatly, but that's one of the things we've heard from retailers is there is increased competition."
Retailers have noticed and are responding.
"We're just trying to do what we do best and improve on that," said Steve Duello, produce director at Dierbergs Markets, as Chesterfield, Mo.-based chain that operates 25 stores in the St. Louis area.
Duello cited, for example, an increased emphasis on "seasonal organic items and locally grown." He noted, also, that value-added is becoming more and more important.
"We just have to be better at what we do," Duello said.
Organic and value-added are the key retail sales drivers, Duello said.
"They still don't total sales of other commodities, but they are the growth areas, with good seasonal fruit and good value," he said.
Greg Lehr, produce director at Straub's Markets, a Clayton, Mo.-based chain that operates four stores in the metro area, said the competition already has forced at least one banner to leave the market.
"We had a Fresh Market that already moved out; they felt the pressure," he said. "It's a pretty saturated marketplace, and, we have the tried-and-trues that have been here."
Lehr also said it's important to lean on strengths when competition grows fierce.
"Customer service has always been our niche, and we're going to stick to that," he said. "Ready-made convenience is always growing fur us, so convenience is a growing trend."
Schnuck Markets Inc., a St. Louis-based chain that has about 60 stores in the metro area, competes strongly in the produce department, said Steven Mayer, vice president of produce.
"We have a very strong produce department and have some very seasoned people in the store, so that's our natural edge," he said.