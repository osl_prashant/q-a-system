There’s an open window for passing a farm bill in early 2018, says Scott Brown, University of Missouri policy analyst. But potential passage gets tough by late spring.
Getting action depends on Congress quickly agreeing to pass a farm bill much like what exists.
Mostly, farmers are pleased with current legislation, Brown told the MU Crop Management Conference. But every commodity group has ideas for tweaks to their parts of the farm bill.
Insurance to cover disaster losses has gained favor with farmers. However, dairy farmers don’t care for their margin protection plan added to the last farm bill.
If House or Senate ag committees open talks for dairy changes, other groups will want changes as well. “Proposed changes likely will cost more, not less,” Brown says.
In writing a farm bill, spending will be important. With concern about federal deficit, cutting costs will drive most decisions.
If debate opens over spending priorities, that slows passage to a standstill.
Farm groups must watch what’s in the appropriations bill that keeps being pushed back. Budget will decide what happens in many areas.
“What happens in dairy support may be affected more by budget than by farm bill,” Brown says.
“How much can this Congress agree upon?” Brown asked the audience.
Action on major legislation often slowed or stalled.
At the start of each farm bill debate there are attempts to remove food stamps from the farm bill. From the standpoint of votes, the removal isn’t likely, Brown says.
The Congressional Budget Office reports 77 percent of farm bill spending goes for nutrition aid.
Legislative votes from farm districts are limited. “Nutrition matters for many more legislators,” Brown says. Previous congressional votes to cut nutrition from the farm bill failed.
“There may be moves to change who qualifies for food stamps,” Brown says.
Congress has been moving away from ad hoc disaster assistance toward insurance programs, Brown says. Some attempt to reduce subsidies to insurance programs.
Overall, insurance spending is a “drop in the bucket” compared to many federal expenditures. Agricultural aid was eased with higher crop prices after the last farm bill was passed. Support comes from the market.
The current farm bill expires in September 2018.
When it comes to writing a new farm bill, there’s a common belief it happens every five years.
“Not so fast,” Brown says. “Few farm bills are written in exactly five years.” Legislators stick with what they have. That could happen with the present farm bill.
“Overall, there’s support for what we have,” Brown says.
Passage of the last farm bill dragged on for four years. “From 2011 to 2014 the ag committees were exhausted updating their legislation annually before one finally passed,” Brown says.
“It took a lot of baling wire to tie together provisions that gathered votes needed to pass a bill.”
The House and Senate ag committee have held many hearings, Brown says. That increases the chance of something happening quickly, but if delayed until after spring other issues take priority.
A mid-term election year ahead slows all action. However, legislators will look for bills they can agree on so they can show what they have done. The window for a farm bill remains open.