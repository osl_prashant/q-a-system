Cherries are arguably the last seasonal produce item.
Marketers say retailers can leverage the unique appeal of cherries to drive department sales all summer long.
“There is a lot of excitement and people asking for ad pricing on the domestic side,” said Maurice Cameron, sales manager for Flavor Tree Fruit Co., Hanford, Calif. “It is a nice spring time fruit (for retailers) to kick off their summer season.”
Cherries signal the kickoff of the summer season and have become an even bigger sales item over the past couple of decades as retailers and marketers have moved away from bulk presentations at retail to 2-pound pouch bags, said Dick Spezzano, president of Monrovia, Calif.-based Spezzano Consulting Service.
“Fifteen years ago, perhaps 90% of cherries were bulk. Now I think are 95% are all prebagged,” Spezzano said.
While the 2-pound pouch bag is the most common in supermarkets, club stores offer larger clamshell options.

The U.S. Department of Agriculture reports per capita use of fresh cherries is rising slowly over time.

“Cherries is always that item that (retailers) love to drive traffic through the stores, and I think the opportunity will be there this year to really promote it,” said Matt Nowak, domestic and export account manager for Grower Direct Marketing LLC, Stockton, Calif.
Two-pound pouch bags dominate but Rich Sambado, sales manager for Primavera Marketing Inc., Linden, Calif., said Primavera also has experimented with a 1.33 pound bag early in the season.
“With a light crop and a high f.o.b., the 1.33-pound bags helps offset retail shock at the checkout stand,” he said.
However, by mid-May, Sambado said the company goes to the 2-pound bag exclusively, save for larger clamshell options for club stores.
The U.S. Department of Agriculture reports per capita use of fresh cherries is rising slowly over time.
For 2016, the latest year with statistics, the USDA reports that per capita use of fresh cherries was 1.2 pounds, up from 1.1 pounds in 2015, 0.9 pounds in 2005 and up from 0.4 pounds in 1996.
Beyond the change in packaging, Spezzano said the cherry season also has been lengthened.
“They started with some early varieties out of California and they stretch out the season in the Northwest going into mid-August and in some years into late August,” he said.
“It used to be at the end of July, it was done, but they have planted on high elevation in Utah, Washington and British Columbia and they have developed later varieties.”
Spezzano said marketers and retailers have upped sizing expectations, as growing practices have allowed them to get premium sizes.
“Out of Washington, 11 and larger was always the standard to use if you wanted a larger cherry, and now it is a 10 and 10.5 and bigger, because they have that much larger fruit” he said.
 
Cherry power
Cherry sales carry big weight in the produce department.
“The big thing about cherries with retailers is it is truly about the only seasonal item left,” Spezzano said.
The pull of demand for cherries can be powerful, he said.
“For a lot of retailers, when (they) put cherries on sale, and many retailers have told me — and it was my experience also — that it could be 10% of total sales for that week,” Spezzano said.
For one item, the lift on sales is huge.
“If you are typical produce department and you are doing $40,000 a week in produce, you put cherries in the ad and that week you might go to $50,000, that’s how much impact cherries can have, especially with cherries with the right size and color,” he said.
For most fruits put on ad, Spezzano said 50% to 75% of those sales are trade-off sales.
“When cherries are on ad, it doesn’t take away from other fruits,” he said. “It is an extra purchase.”
Another characteristic of cherries is that many customers will buy cherries on ad multiple times during the same week.
“There is a core amount of customers will come in the day the ad breaks to buy cherries but will return at least one more time during that 7-day period to purchase them again,” he said.
Often, Spezzano said, how cherries go is how the department goes.
“Cherries have a huge impact on produce retail sales to the point if the season is two to three weeks shorter than usual, you see a decline in the produce department sales,” he said.
“If you don’t have a season that goes as long as you want it to go, you will miss your sales budget because of that.”
At the same time, a longer than normal cherry season can help a produce manager surpass his target.