Growing at a steady clip, organic produce increased from 7.5% of produce department sales in 2015 to 9% in 2016. Sales are expected to eclipse 10% in 2017, according to Fresh Look Marketing data.

The Top 10 categories of organic produce all increased dollar share in 2016, according to the latest United Fresh Produce Association FreshFacts on Retail: 2016 Year in Review report.
Take a look at your organic produce category, by the numbers.