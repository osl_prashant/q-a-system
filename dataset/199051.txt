Often forgotten in the chase for profitability is the importance of getting cows pregnant quickly after calving.Doing so means cows spend a greater percentage of their lifetimes in early lactation, when milk production is highest.
"Increasing a herd's pregnancy rate from 15% to 22% will increase profit by more than $100 per cow per year," says Jim Salfer, a dairy specialist with the University of Minnesota.

Here are minimum reproductive goals all herds should aspire to, Salfer says:
Pregnancy rate: >22%
Cows inseminated within 21 days of the end of the voluntary wait period: >90%
Cows pregnant by 150 days in milk: >70%
Lactating herd confirmed pregnant: >50%
Cows culled for reproduction: <5%
Age at first calving: 22 to 24 months
Reaching these goals is well worth the effort, Salfer says.