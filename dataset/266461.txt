BC-Cash Prices, 1st Ld-Writethru,0342
BC-Cash Prices, 1st Ld-Writethru,0342

The Associated Press

NEW YORK




NEW YORK (AP) — Wholesale cash prices Thursday
                                         Thu.       Wed.
F
Foods

 Broilers national comp wtd av            1.0613     1.0613
 Eggs large white NY Doz.                   2.74       2.79
 Flour hard winter KC cwt                  15.00      15.00
 Cheddar Cheese Chi. 40 block per lb.     2.2025     2.2025
 Coffee parana ex-dock NY per lb.         1.1982     1.1863
 Coffee medlin ex-dock NY per lb.         1.3943     1.3803
 Cocoa beans Ivory Coast $ metric ton       2834       2892
 Cocoa butter African styl $ met ton        7558       7728
 Hogs Iowa/Minn barrows & gilts wtd av     48.35      49.06
 Feeder cattle 500-550 lb Okl av cwt      170.25     170.25
 Pork loins 13-19 lb FOB Omaha av cwt      83.98      82.79
Grains
 Corn No. 2 yellow Chi processor bid      3.67¾       3.53½
 Soybeans No. 1 yellow                   10.19¾       9.93 
 Soybean Meal Cen Ill 48pct protein-ton  374.80      376.90
 Wheat No. 2  Chi soft                    4.42        4.36½
 Wheat N. 1 dk  14pc-pro Mpls.            7.13½       7.39½
 Oats No. 2 heavy or Better               2.40        2.36¾
Fats & Oils
 Corn oil crude wet/dry mill Chi. lb.     .32          .32 
 Soybean oil crude Decatur lb.            .30          .30 
Metals
 Aluminum per lb LME                     0.9112      0.9230
 Antimony in warehouse per ton             8588        8588
 Copper Cathode full plate               2.9942      3.0124
 Gold Handy & Harman                     1323.85    1332.45
 Silver Handy & Harman                    16.322     16.306
 Lead per metric ton LME                 2403.00    2397.00
 Molybdenum per metric ton LME            15,500     15,500
 Platinum per troy oz. Handy & Harman     936.00     939.00
 Platinum Merc spot per troy oz.          927.30     934.70
 Zinc (HG) delivered per lb.              1.4929      1.5088
Textiles, Fibers and Miscellaneous
 Cotton 1-1-16 in. strict low middling     78.22      77.49
Raw Products
 Coal Central Appalachia $ per short ton   63.00      63.00
 Natural Gas  Henry Hub, $ per mmbtu        2.64       2.60
b-bid a-asked
n-Nominal
r-revised
n.q.-not quoted<29
n.a.-not available