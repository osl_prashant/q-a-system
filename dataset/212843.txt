Growers in California’s Kern County are cautiously optimistic after this year’s rains and winter storms filled the reservoirs and replenished the snowpack, lifting the drought designation in nearly all counties.However, the rains, though welcome, did disrupt the planting season for many.
“We love the rain but it came at a time where we needed to stay busy. If it had come in November or December, there would’ve been no delays,” said Danny Andrews, owner of Bakersfield, Calif.-based Dan Andrews Farms LLC.
The company experienced some soil-related diseases on a few acres of this season’s cabbage crop because of an oversaturated ground and above-average temperatures in March.
This will reduce the firm’s yields on early plantings, he said.
The rains also delayed the firm’s honeydew and watermelon plantings by a month.
“We’re just a little bit behind,” he said. 
The winter rain similarly skewed the potato planting season at Bakersfield-based Top Brass Marketing Inc., president and sales manager Brett Dixon said.
Although he was glad for the rain, Dixon said “it will take many years of this and proper planning to directly affect the overall water table.”
He also said groundwater management regulations will continue to affect allocations for farming.
Arvin, Calif.-based Kern Ridge Growers LLC, which ships carrots year-round, anticipates potentially lower yields in June, according to sales manager Andrew Bianchi.
Citrus crops at Edison, Calif.-based Johnston Farms were also delayed a little bit, salesman Derek Vaughn said.
For Delano, Calif.-based table grape grower Jasmine Vineyards Inc., however, the winter rains did not cause any delays or disease, vice president Jon Zaninovich said.
Zaninovich hopes the dry weather continues throughout the spring as the vines come out of dormancy and begin to fruit.
“Wet weather now can lead to problems that require fungicide use,” he said.
Similarly, for Bakersfield-based Grimmway Farms, although recent rains have been “challenging,” they have not created any “major supply gaps or quality issues,” communications and engagements manager Valorie Sherman said.
On April 7, Gov. Jerry Brown ended the drought state of emergency in nearly every county, Kern County included.
Kern County growers were paying a premium on water during the drought.
“We won’t know our final water cost until May, but we’re optimistic it will be reduced,” Andrews said.
Although the drought might be over, other challenges remain.
“The problem now is storage. If we had more dams throughout California, we’d probably be in a lot better shape,” Zaninovich said.