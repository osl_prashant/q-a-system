China could import up to 20 million tons of corn a year, more than six times the current level, to meet a switch to greater use of ethanol in fuel, an analyst predicted on Thursday.Beijing plans to roll out a gasoline known as ‘E10’ - containing 10 percent ethanol - across the world’s largest car market by 2020, state media reported last week.
As much as 15 million tons of ethanol would be needed when the policy is implemented, according to Reuters calculations, or about 45 million tons of corn, the major raw material its production.
More corn is already being consumed closer to cornfields in the top growing region in the north-east, after a series of measures introduced last year to use up state stocks.
Meanwhile ongoing expansion of the corn processing sector will leave less available for sale to feedmakers in other parts of the country, Cherry Zhang, a senior corn analyst with Shanghai JC Intelligence Co, said.
“This will cause a supply gap in the south, and we might need imports to fill it,” Zhang told a conference in Hangzhou.
Corn transported out of northeast China will fall to less than 30 million tons in the coming 3-5 years from current levels of around 50-60 million tons, she added.
China may need to import between 10 million and 20 million tons in average harvest years to fill the gap, said Zhang.
The forecast is significantly higher than projections by the United States agriculture department, which expects corn imports to reach just 3.8 million tons in the 2019/20 crop year.
Imports are forecast at 1.5 million tons in the 2017/18 crop year, China’s agriculture ministry says.
China’s corn consumption in 2017/18 is expected to increase 8.4 percent to 221.97 million tons, according to the China National Grain and Oils Information Center (CNGOIC).
Of that, industrial demand from producers of sweeteners, starch and ethanol is expected to rise 14.1 percent to 73 million tons in 2017/18, CGNOIC forecasted.
“There will be more processed products transported from the northeast instead,” said Zhang Guogang, business development manager at SDIC Biotech Investment, the ethanol division of state-owned conglomerate SDIC.
The company is building a 300,000-ton fuel ethanol plant in Liaoning province, which is expected to go online in August next year.