Big Thanksgiving demand is adding to what is already a fast-moving season for potatoes and onions.
Consumer bags of potatoes are tight, said Nick Beahm, salesman at Eagle Eye Produce, Idaho Falls, Idaho.
“The poly (bag) situation is going to be brutal here in Idaho,” he said.
Growers harvested larger potatoes this year, leaving fewer small sized spuds for the consumer bag market. 
The U.S. Department of Agriculture reported that 50-pound cartons of five 10-pound film bags of russet norkotahs increased from $5.50 per carton on Sept. 5 to $6 per carton by Nov. 1, with russet burbanks at $6.50 per carton for the same pack on Nov. 1. Prices at the same time last year were $4.50 to $5 per carton.
Russet norkotah 50-count potatoes were $9 per carton on Nov. 1, compared with $12  for 80 count cartons.
Prices for both sizes were well above a year ago, when 50 count norkotahs were $5.50 per carton and 80 count norkotahs were $6 per carton in early November.
“Everybody has big stuff and trying to find the small,” he said. “It won’t be a cheap poly year for sure.”
Beahm said larger potatoes, sized 40s and 50s, may be the softest, along with No. 2 potatoes.
“All your stuff in the middle — your 60s, 70s, 80s, 90s — you can’t find it,” Beahm said. 
Rail car volume to meet Thanksgiving demand — at Nov. 23, Thanksgiving is early this year — has to be shipped out by early November, Beahm said. 
Truck shipments will be heavy the weeks of Nov. 5 and Nov. 12. “It depends on how proactive these guys are, if the retailers are ahead of the game in trying to bring in product before hand and getting their distribution centers and stores full, they already have trucks lined up for next week.”
“Prices are strong for this time of year, and I feel like it is gathering strength,” said Ralph Schwartz, salesman for Idaho Falls-based Potandon Produce.
Added to that, he said, is the fact that trucks are tighter than normal.
“The market is already at $6 on 10-pound poly and people are quoting higher,” he said.
Russet norkotahs are heavy to the 40-to-60 count size profile.
“Anything that is 70 count and smaller we have been sold out every day,” he said. 
With 100-count potatoes commonly being put in bags, combined with foodservice demand for those sizes, Schwartz said it hasn’t been hard to sell out that size profile.
Some think overall production of Idaho potatoes could be down 3% to 5%, he said. Another fact is that perhaps 20% of the Idaho crop was harvested after the state had several cold days, which likely could result in a higher cull rate for those potatoes, he said.
In the near term, Schwartz said the market could increase to $6.50 per carton for 10-pound bags, above price levels in recent years. Last year, delivered prices for cartons of 10-pound bags of Idaho potatoes to Midwest markets was about $9.50-10. 
This year, the delivered cost is $11 per carton and higher, he said. 
The early Thanksgiving may take some people by surprise. “My gut is telling me that come Nov. 6 there is going to be a whole different dynamic out there for people suddenly looking for product.”
Onion strength
Beahm said onions are experiencing higher market prices this year because of a shorter crop, with some Treasure Valley (Idaho-eastern Oregon) growers down 20% to 30% in crop volume. 
Price for jumbo onions were $9.50-10 per carton in early November, compared with $4 per carton in early November last year, according to the USDA.
Schwartz said the reduced crop and size profile could drive a strong onion market all season.
Celery, cranberries and sweet potatoes
Abbie Hannon, market news reporter for the Phoenix office of the U.S. Department of Agriculture’s Specialty Crops Market News, said Nov. 1 the Thanksgiving demand for celery appeared to be strong. 
Salinas shipping point prices for cartons of two dozen were $14-16.45 on Nov. 1, up from average prices near $10 per carton in October and $7.40 in September. 
Shipping point celery prices in November 2016 averaged $14.80 per carton
Sweet potato demand was strong and supplies were abundant in early November, said Autumn Campbell, sales manager for Matthews Ridgeview Farms, Wynne, Ark. The shipping point price for No. 1 sweet potatoes was expected to be at $16.50-17 per carton for Thanksgiving demand.
The USDA reported the average shipping price for sweet potatoes from North Carolina, Louisiana and Mississippi averaged $12.40 per carton in October, slightly above the average prices recorded in October and November last year.
The USDA reported average Boston terminal market prices for cranberries in November this year for cartons of 24 12-ounce packages was $26.25, down slightly from $26.95 per carton the same time a year ago.