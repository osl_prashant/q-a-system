The United Fresh Produce Association is offering training for food recalls on March 27 in Salinas, Calif.
The United Fresh Recall Ready Training Workshop will feature hands-on training workshops that help industry leaders understand the fundamentals of a product recall, according to a news release. The event will be from 8 a.m. to 5 p.m. at Hartnell College in Salinas, according to the release.
The workshop also will inform participants on:

The role of the Food and Drug Administration during a recall;
How to limit liability; and 
How to manage customer expectations. 

Registration and more information is available online.