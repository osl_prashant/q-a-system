A handful of strawberry varieties developed at the University of California-Davis show enough promise that some growers believe they will be the varieties of tomorrow in the state’s Santa Maria and Watsonville/Salinas growing areas.The varieties, the cabrillo and the grenada, were the last ones developed by longtime university breeders Doug Shaw and Kirk Larson, who recently left the school’s breeding program to do research for the upstart California Berry Cultivars, a partnership of four strawberry grower-shippers.
Salinas-based Naturipe Berry Growers is growing the cabrillo variety for the first time this season, said Craig Moriyama, director of berry operations.
“It looks like a promising variety,” he said.
Some in the industry believe the cabrillo, which is a day-neutral variety, eventually could replace the albion and monterey in the north, said Matt Kawamura, a partner in Irvine-based Orange County Produce LLC.
Meantime, the monterey, which Moriyama described as a good-producing berry with good flavor, shape and color, is the most widely fall-planted strawberry variety in the state, accounting for 26.4% of the winter, spring and summer strawberries, according to the California Strawberry Commission’s 2016 acreage survey.
The Monterey accounts for 32% of the strawberries in the Santa Maria district and 37% of those in the Watsonville/Salinas district.
Watsonville-based Well-Pict Inc. plants the same numbered proprietary varieties in Santa Maria and Watsonville, said Jim Grabowski, director of marketing.Different varieties are planted in Oxnard.
California Giant Berry Farms, Watsonville, plants the monterey and san andreas varieties in Santa Maria and Watsonville, said Cindy Jewell, vice president of marketing.
The two are similar, day-neutral varieties, she said.
“Santa Maria is a hybrid district,” she said. “It can be an early district and support volume from Oxnard during the early season, and it stays in all summer long and supports the Watsonville/Salinas district.”
The company plants the portola variety in Santa Maria for fall production.
The portola should account for about 55% of the summer plantings this year, with acreage up about 5% compared to last year, according to the strawberry commission.
Another new university variety, the granada, seems to do well in the southern districts and may replace the san andreas and compete with the fronteras, another new university variety that a number of growers have planted this season, Kawamura said.
Conducting research to find a better variety is a never-ending task at Well-Pict, Grabowski said.
“There’s a limited lifespan for these varieties,” he said.
Strawberry varieties typically are retired after five to seven years, he said. Sometimes, they may need to be replaced sooner.
“It’s an arduous task trying to find a variety, from the time you start with seedlings to actually put them into production,” he said. “You keep your fingers crossed that you can get five or six years out of the variety.”
What qualities does the company look for when developing new kinds of berries?
“We always want to have a flavorful berry,” Grabowski said.
Yields, shelf life, shape and color — inside and out — are important, as well.
Many of Well-Pict’s are a bright red throughout, he said.
But when all is said and done, “Our primary goal is flavor.”