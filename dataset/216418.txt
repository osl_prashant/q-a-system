State and industry officials are beginning to estimate crop damage from California's December fires. Photo courtesy Mission Produce/Albert Munoz
Representing 65% of fire-damaged farm land, avocado groves by far bore the brunt of the losses from December’s Thomas Fire in California, Ventura County Agricultural Commissioner Henry Gonzales said Dec. 20.
Gonzales and other agricultural officials were beginning to make preliminary fire damage estimates for avocados and other crops in Ventura County after the devastating Thomas Fire swept through 272,000 acres in the region. 
On Dec. 19 - 16 days after the Thomas Fire began - the blaze was still only 55% contained and fire officials in Ventura and Santa Barbara counties said the inferno was not expected to be fully contained until early Jan. 7. 
“There is a lot of uncertainty at this point, and even the growers don’t know what the effects are going to be, particularly with the avocado and citrus trees,” Gonzales said Dec. 20. “I’m telling folks it is kind of like a human burn victim, that you don’t know how long the recovery will take and to what degree they will recover - the same is true for each tree.”
 
By the numbers
Gonzales said about 10,000 acres of agriculture land was damaged or destroyed by the fire, including about 6,603 acres of avocados, 1,767 acres of lemons, 542 acres of oranges, 166 acres of tangerines, 155 acres of walnuts, 45 acres of strawberries, 23 acres of nursery crops, 4 acres of cut flowers and 2 acres of raspberries.
In total, about 38 crops were in the burn area, and many growers lost structures or homes.
Gonzales said the preliminary agricultural damage estimate, expected Dec. 21 or Dec. 22, will lead to a request for emergency disaster status.
The county also is in the process of contacting every grower in the affected area and also using aerial photography.
“We are going to take a look at what that looks like now, but we are also going to have pictures taken in about a month and another in perhaps four months to measure recovery,” he said.
Some growers estimate that perhaps 15% of the 2017-18 avocado crop in the county was lost in the fire, Gonzales said, noting that high winds knocked some of the developing fruit off the tree. 
Ventura County avocado area totals about 18,500 acres of avocados, Gonzales said.
Ken Melban, vice president of industry affairs for the California Avocado Commission, said Dec. 20 that the commission had likewise identified substantial acreage of avocados within the perimeter of the fire in Ventura County. Assessments are still being made on damage to the groves, however. “Parts of the groves could have been really torched and others areas had less impact,” he said. That makes it hard to nail down a precise estimate of volume lost to the fire, he said.
The commission also identified fire damage to about 200 acres of avocados in Santa Barbara County and 130 acres of avocados in the Lilac fire near Fallbrook, Calif.
Gonzales said lemon fruit loss in Ventura County was not expected to be as significant, as the lemon trees have three blooms and three crops per year. 

“It was only (the lemons) that (were) on there now that will be impacted, and it is not as bad as the avocados,” he said.
A better idea of long-term avocado damage is expected by the spring, when either the blossoms come back or they don’t, Gonzales said. 
A full accounting of the damage is not anticipated until the 2018-19 season.
“Some trees that seem like they are fine are probably going to die and others that seems shaky will pull through,” he said.