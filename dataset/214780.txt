Good morning!
Firmer tone for grains overnight, soybeans mixed... Corn futures are steady to fractionally higher as of 6:30 a.m. CT, while soybean futures are narrowly mixed, with nearbys favoring the downside. Winter wheat futures are 1 to 2 cents higher amid some corrective trade, with spring wheat mostly 2 to 3 cents higher. The U.S. dollar index is up slightly this morning, with crude oil futures just under unchanged.
Weekly Export Sales Report expectations...



Commodity

2017-18
			(MT)


2018-19
			(MT)



Corn
1,200,000 to 1,700,000
0 to 200,000


Wheat
350,000 to 550,000
0


Soybeans
1,100,000 to 1,500,000
0 to 50,000


Soymeal
125,000 to 350,000
0


Soyoil
14,000 to 40,000
0



Senator Johnson first Republican opposed to tax bill... Senator Ron Johnson (R-Wis.) said Wednesday he won't support the Senate GOP tax plan, becoming the first GOP senator to say he will oppose the proposal. Johnson, a businessman before he became senator, contends the current plan helps big corporations more than smaller companies, taking issue with the pass-through rate. Republicans can only afford to lose a total of two GOP votes, unless they get some Democrats to vote for tax reform. And several other Republicans have voiced their own concerns about the measure. The Senate knows how to lay the spoiler: The House passed a repeal of ObamaCare in May, only to see it fail in the Senate twice. Meanwhile, the president will be on Capitol Hill today to rally support for the House's tax bill, which is expected to pass later today. Read more.
NAFTA leaders agree not to meet for fifth round of talks... Top trade officials from the U.S., Canada and Mexico will not participate in the fifth round of NAFTA 2.0 negotiations in Mexico City, according to a joint statement released by the three countries. The development comes as ministers from the three countries met at the Asia Pacific Economic Cooperation (APEC) meeting in Vietnam and agreed not to attend the fifth round "so negotiators can continue to make important progress on key chapters advanced in Round 4," the statement said. Ministers had already agreed at the close of the fourth round to allow more space between the negotiating round to "provide negotiators with enough time to analyze the proposals that all three countries have tabled so far and to conduct internal consultations." But the leaders did say they will be in "constant communication" with the negotiating teams in Mexico City and will report on progress.
USTR Lighthizer in London with goal of eventual U.S./U.K. bilateral trade deal...  U.S. Trade Representative (USTR) Robert Lighthizer is in London to confer on what is hoped to be an eventual bilateral trade agreement with the United Kingdom after it exits the European Union. "The USA is our single biggest trading partner, and this dialogue will help provide certainty and confidence to businesses on both sides of the Atlantic, and identify existing barriers to trade," said British Secretary of State for International Trade Liam Fox.
Strategie Grains trims EU wheat crop estimate, raises corn... Strategie Grains cut its soft wheat estimate for the European Union by 300,000 MT to 142.5 MMT, which is still up 5% from 2016-17. On the other hand, the French consultancy raised its corn estimate for the block by 300,000 MT to 59.3 MMT, a drop of 1% from the previous marketing year. Its total grain crop estimate now stands at 299.5 MMT, up 3.5 MMT (1%) from 2016-17.
French farm office trims wheat export forecast... France will likely ship 9.9 MMT of soft wheat outside the EU in 2017-18, estimates FranceAgriMer, down 300,000 MT from its previous estimate. This also prompted a 100,000 MT uptick in its wheat ending stocks outlook, which now stands at 3.3 MMT, as the reduced shipments outside of the bloc are expected to be only partially offset by an uptick in shipments within the EU.
Rebound in Indian cotton production expected... India's cotton crop will likely total 37.5 million bales this season, according to the Cotton Association of India. This would be up 11.2% from last year's crop thanks to expanded area, according to the trade body. It also expects India's cotton consumption to climb from 30.78 million bales last year to 32 million bales in 2017-18.
Cash cattle trade picks up across the country Wednesday... After some early sales in the $118 to $119 area in Iowa and Kansas, trade picked up across the country at prices ranging from $119 across the Southern Plains to nearly $120 in Iowa, Nebraska and Colorado, with some unconfirmed reports of action up to $120.50 in Nebraska. This was down roughly $5 from week-ago levels in Kansas and Texas with other locations seeing lighter losses for the week. December futures are trading in line with the upper end of this range.
Cash hog bids continue to slide, but December futures are well below the cash index... Cash hog bids fell 55 cents on a national basis yesterday, which was an improvement after the western Corn Belt registered steep losses in the morning. While the cash market has led lean hogs lower over the past month, futures now hold a wide discount to the cash hog index. This along with the technically oversold condition of some contracts based on the nine-day Relative Strength Index could help futures to stabilize.
Overnight demand news... Jordan canceled its international tender to buy 100,000 MT of barley. Egypt tendered to buy an unspecified amount of wheat from global suppliers; Russia has the lowest offer of the six presented. Japan purchased 65,530 MT of food-quality wheat from the U.S. as well as 35,181 MT from Canada and 24,900 MT from Australia.
Today's reports:

7:30 a.m., Weekly Export Sales -- FAS
7:30 a.m., Extended Weather Outlook -- NWS
7:30 a.m., Drought Monitor -- USDA/NWS
11:00 a.m., Livestock, Dairy, & Poultry Outlook: November 2017 -- ERS
2:00 p.m., Farm Labor -- NASS
2:00 p.m., Turkey Hatchery -- NASS