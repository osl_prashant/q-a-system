Fly control is always a hot topic with organic dairy producers because there are not a lot of viable options to alleviate fly pressure. Three important blood sucking pest flies on grazing cattle in the Upper Midwest are the stable fly, horn fly, and face fly. Stable flies develop as maggots in a wide array of decomposing organic matter, including soiled animal bedding and soiled feed debris that accumulates wherever cattle are confined. The horn fly and face fly develop in fresh cattle dung pats and nowhere else, so they are troublesome to organic herds when pastured. Horn flies, stable flies, and face flies on organic cows can cause a 10 to 30% reduction in milk production. Furthermore, these flies can reduce pasture feed intake, cause pinkeye, and may spread disease from one animal to another.At the University of Minnesota West Central Research and Outreach Center dairy in Morris, we have been evaluating two unique methods (Bruce Trap and Spalding Cow-Vac‚Ñ¢) for controlling pasture flies. Bruce traps and the Cow-Vac are compatible with organic dairying because a trap can be positioned at the entrance to a milking parlor, where cows come and go twice per day.
To combat horn flies, W. G. Bruce, a USDA entomologist, built a box with one-way fly-screen baffles on its otherwise transparent sides, and walked fly infested cattle through it to remove and capture their flies. Bruce's simple design is now known as the Bruce walk-thru fly trap, and different versions have been studied for horn fly control in various parts of the country. The fabric dislodges flies, drawn by light to sides through baffles, and the flies are trapped and then die.
The Cow-Vac is a new way to control horn flies on dairy cows. It can be placed at the entry or exit of the milking parlor or barn. As the cows walk through, the Cow-Vac will blow horn flies off the back, belly, face, flanks and legs into a vacuum system that collects them in a removable bag for disposal. An electrician will need to install a 220v outlet that can be reached by the 10-foot power cord. For the cows at the Morris dairy, it took about one week to get cows conditioned to going through both fly traps.
During the summer of 2015, we evaluated the efficacy of the Cow-Vac in on-farm organic dairy production systems to control horn flies, stable flies, and face flies. The study partnered with eight organic dairy farms in Minnesota, and herds ranged from 30 to 350 cows in size. The farms were divided into pairs by location in Minnesota and during the first period of the summer (June to July) the Cow-Vac was set up on one farm and during the second period of the summer (August to September) the Cow-Vac was sent to its paired farm. Farms were visited once per week to collect flies from the Cow-Vac, as well as count and record flies on cows.
The results of fly counts and milk production for the presence or absence of the Cow-Vac on farms are in Table 1. Horn fly numbers on cows were reduced by 44% in the presence of a Cow-Vac compared to the absence of a Cow-Vac. Stable fly and face fly numbers were similar whether the Cow-Vac was present or absent.

Table 1. Results of fly counts and milk production for the presence or absence of the Cow-Vac


Cow-Vac present
Cow-Vac absent
Horn fly (fly/side)
11.4
20.5*
Stable fly (fly/leg)
5.4
7.1
Face fly (fly/cow)
1.0
1.0
Milk (lb/d)
34.2
33.7
Fat (lb/d)
2.9
2.9
Protein (lb/d)
2.2
2.4
Somatic cell count
315
322

*Significant difference of absence of Cow-Vac compared to presence of Cow-Vac

Milk production was similar for farms with the Cow-Vac compared to those without the Cow-Vac. In summary, these results indicate the Cow-Vac was effective in reducing horn fly numbers on cows and reduced horn fly growth rates during the pasture season in organic dairy production systems.
Finally, for those who are interested, Roger Moon and I hosted an eOrganic webinar on March 24, 2016 on fly control methods for organic dairy farms. For more information and to view the recorded webinar, clickhere. Happy Summer Grazing!