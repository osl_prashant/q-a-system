Research Finds ADM’s CitriStim Enhances Sow Reproductive Performance
ADM Animal Nutrition, a division of Archer Daniels Midland Company (NYSE: ADM), today announced new product performance milestones for CitriStim, a proprietary, whole cell, inactivated yeast currently used in more than 500 ADM feed formulations for livestock, poultry, aquaculture, and swine.
Field studies recently performed at University of Arkansas find key benefits for swine producers who incorporate CitriStim into their nutritional program. According to the University of Arkansas’ study, sows fed a diet supplemented with 0.1% CitriStim throughout their gestation and lactation period saw not only an overall increase in the number of live piglets born, but also a reduction in the percentage of piglets who weighed less than two pounds at birth, and an increase in the total number of piglets weaned per litter.*
“CitriStim has proven to be very effective at impacting animal performance across a wide variety of species. With this data from the University of Arkansas, we see a compelling benefit for swine producers,” said Peter Bergstrom, international business manager for ADM Animal Nutrition. “Many swine producers have yet to utilize yeast products, but we look forward to showing customers how CitriStim can positively impact the bottom line of their operation.”
CitriStim is a strain of Pichia guilliermondii which is comprised of a unique structure and ratio of whole-cell components shown to help modulate immunity, shift bacterial populations, bind pathogenic bacteria, and reduce nutritional feed stressors across a variety of species. It is produced at the ADM facility in Southport, North Carolina, and exported to more than 25 countries worldwide.
For more information about CitriStim, contact ADM Animal Nutrition at 800-245-9746, animalnutrition@adm.com, or visit www.ADMAnimalNutrition.com/Citristim-Swine.
*Assuming an average sow intake of 800 pounds per litter, fed at a rate of 4 pounds per ton.
ARM & HAMMER Introduces CERTILLUS
ARM & HAMMER is pleased to introduce CERTILLUS, the newest brand in the ARM & HAMMER family. Pronounced “sir-till-us,” this exciting portfolio of products presents specific solutions to the challenges producers face related to pathogenic challenges on their specific operation.

“CERTILLUS is the newest brand in the ARM & HAMMER family and will serve as the portfolio brand across all species for products powered by Microbial Terroir,” explains Ben Towns, ARM & HAMMER Director, Global Business.

“We’re eager to share the possibilities CERTILLUS will provide to our customers,” he adds. “These Targeted Microbial Solutions deliver offerings uniquely fitted to their Microbial Terroir.”

Microbial Terroir is the microbial makeup of the environment, the soil, the animals and the weather on a specific farm location. The Microbial Terroir in which animals live will ultimately impact how they perform. On-farm sampling and testing can reveal a farm’s Microbial Terroir and what is needed to combat harmful pathogens that slow down animal productivity

Using sampling protocols and advanced molecular biological techniques, ARM & HAMMER maps out the complex Microbial Terroir that shapes—and enhances—the gastrointestinal (GI) tract of animals within a herd or flock. A customized solution is then developed from a database of proprietary Bacillus strains for the CERTILLIS portfolio of products to effectively combat pathogens impacting animal performance.

“This name brings together two key points about the brand—certainty and Bacillus,” says Scott Druker, General Manager, Arm & Hammer Animal Nutrition.

“Our thorough testing and analysis process combined with a proprietary multi-strain solution offer our customers the confidence that they can effectively combat the challenges lurking in their own Microbial Terroir,” he notes.

To learn more, visit www.AHanimalnutrition.com.