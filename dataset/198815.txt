The traditional way to manage dry cow programs is being challenged. Most dairy programs have a 60-day dry cow program with two unique rations for far-off and close-up periods. More and more producers are considering and transitioning to shorter, one-group programs of 40 to 42 days coupled with a negative dietary cation-anion difference (DCAD) nutrition program. Consider the benefits of this approach: 1. More milk - cows are in production for an additional 18 to 20 days. Regardless of the milk price, this additional milk can improve cash flow and profitability.
2. Better health - cows that calve early (twins, wrong breeding dates, etc.) will benefit by being on a negative DCAD ration the proper length of time (minimum 21 days) prior to calving and will have fewer transition cow diseases. Feeding a negative DCAD diet stimulates calcium absorption and mobilization, which will help prevent clinical and subclinical hypocalcemia in a properly formulated diet.
3. Less labor - management and labor resources are saved by reducing the number of pen moves for cows. Less time is spent sorting and moving cows from far-off to close-up groups. Labor costs are further reduced by spending less time mixing separate rations for two dry cow groups.
4. Less stress - social stress is reduced since cows spend less time reestablishing social order leaving more time for eating and lying down. Increased feed intake during the dry period may result in higher fresh cow milk production and fewer transition cow diseases.
5. Simplified ration - a simplified, one-group ration - instead of both a far-off and close-up TMR - creates multiple benefits:
a. Increased TMR load sizes results in less sorting and more uniform mixing
b. More consistent supply of feed in the bunk
c. Reduced number of commodities and feedstuffs to store in inventory
d. Less change in rumen microbial population transition
Research has suggested that 30-day dry periods may reduce milk production, so dry periods of less than 40 days are not recommended. Research also has shown cows with shorter dry cow periods of 45 or 30 days had the same milk components and milk quality in the following lactation.
Housing considerations
Going from a two group to a one group dry cow strategy provides the option to separate prepartum heifers from mature cows. By reducing social competition and allowing for better feed bunk access, heiferstransition better when given the opportunity to consume more energy and produce more milk in the subsequent lactation.
Acidify the diet
The benefit-to-cost ratio of a short dry period is improved by feeding a negative DCAD diet, because transition diseases such as subclinical hypocalcemia, metritis, mastitis and ketosis may be reduced. This is especially true for cows that calve earlier than anticipated and are more vulnerable to the transition diseases.
A University of Georgia study published in 2014, demonstrated no adverse effects of feeding a negative DCAD diet for as long as six weeks prior to calving. In the study, no differences were observed in health or milk production whether cows were fed a negative DCAD ration for three, four or six weeks. In addition, serum calcium levels post-calving were similar in cows fed the negative DCAD diet for only three weeks or up to six weeks prior to calving. If cows do not calve as early as expected, benefits of the negative DCAD diet and calcium mobilization are still effective for a planned or unplanned longer dry period.
Recommended Guidelines for a Negative DCAD Diet 
Fully acidify cows to a urine pH range between 5.5-6.0
Measure cows that have been on a negative DCAD diet for 14 to 21 days
Feed 1.40-1.60% DM (165-190 grams) calcium
Feed 0.45-0.50% DM (53-59 grams) magnesium
Feed 0.43-0.47% DM (51-55 grams) sulfur
The combination of all of these benefits simplifies dry cow feeding and provides an opportunity for a one-group dry cow DCAD program. A one-group dry cow program helps reduce social stress of cows undergoing pen moves and ration changes. A negative DCAD diet also has been shown to reduce health problems observed during the transition period while increasing production during the lactation prior to the dry period.
Negative DCAD diets are proven to work. Shortened dry cow periods have many benefits. Combining the two can benefit the dairy in numerous ways and add to the bottom line.
For more information about negative DCAD diets, visit Animate-Dairy.com.
Glenn Holub can be contacted at glenn.holub@pahc.com.
References

Church, G. T., L. K. Fox, C. T. Gaskins, D. D. Hancock, and J. M Gay. 2008. The effect of a shortened dry period on intramammary infections during the subsequent lactation. J. Dairy Sci. 91:4219-4255.
Gulay, M. S., M. J. Hayden, K. C., Bachman, T. Belloso, M. Liboni, and H. H. Head. 2003. Milk production and feed intake of Holstein cows given short (30-d) or normal (60-d) dry periods. J. Dairy Sci. 86:2030-2038.
Rabelo, E., R. L. Rezende, S. J. Bertics, and R. R. Grummer. 2003. Effects of transition diets varying in dietary energy density on lactation performance and ruminal parameters of dairy cows. J. Dairy Sci. 86:916-925.
Wu, Z., J. K. Bernard, K. P. Zanzalari, and J. D. Chapman. 2014. Effects of feeding a negative dietary cation-anion difference diet for an extended time prepartum on postpartum serum and urine metabolites and performance. J. Dairy Sci. 97:7133-7143.