Mushroom marketers say consumers are more knowledgeable about the category than in the past, but there are still education efforts needed, especially as specialty varieties gain in popularity.
"I think people get it now, for the most part, but they are still looking for more detailed information about nutrition and new recipes," said Kevin Donovan, national sales manager for Kennett Square, Pa.-based Phillips Mushroom Farms.
Others agree that information needs to be available for consumers who want to know how to better work with mushrooms.
"Cleaning mushrooms is very easy, but there are some misconceptions regarding rinsing mushrooms or cleaning off the gills on some varieties like the highly flavorful portabella," said Pete Wilder, marketing director of To-Jo Mushrooms, Avondale, Pa.
"Speaking with many chefs, scraping the gills is really dependent on the sauce or color of the dish you will be preparing, as the darker gills could impact the finished color of certain sauces," Wilder said.
Wilder said the company also works to teach buyers and retail consumers on the proper way to handle and clean its products.
Another side of consumer education efforts aims to teach the general public more about how mushrooms are cultivated.
"There's a great interest in where food comes from and how efficiently it's produced," said Bill St. John, sales director for Kitchen Pride Mushroom Farms Inc., Gonzales, Texas.
However, information about health benefits is the basics of what consumers really want to know about mushrooms these days.
St. John said working with retail dietitians is one of the best ways mushroom growers can educate the public about the health benefits mushrooms offer.
"They are a great resource," he said.
Kathleen Preis, marketing manager for the San Jose, Calif.-based Mushroom Council, agreed.
"One of the best ways to promote mushrooms is working with the in-store dietitian to include fresh mushrooms in their newsletters and customer discussions to better promote mushrooms," she said.
Many customers are unaware of the nutrients mushrooms offer.
"Increased awareness of the many vitamins and nutrients mushrooms provide, such as vitamin D, B vitamins, potassium and more can increase sales growth, and consumers continue to search for medicine through food rather than supplements," Preis said.