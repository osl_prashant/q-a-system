For years, Pride of New York focused on the “homegrown” aspect of the state’s fruit and vegetable production.That focus has changed, starting with this year, under a new banner — New York State Grown & Certified.
The focus on New York-grown produce hasn’t changed, but now the program embraces food safety and environmental standards consumers seek, said Richard Ball, New York State Agriculture Commissioner.
“The goal is similar — we still want to take advantage of the great grower community we have here in New York and want to connect with the most amazing marketplace in the world,” he said.
But, after meeting with industry groups and allied constituencies, including “foodies” and other consumers, program planners decided it was time to step up the program, Ball said.
“At the end of the day, we have this program which is going to tell our consumer, wholesaler or retailer three things: that it’s local, that it’s a farm with a (Good Agricultural Practices)-audited program and it has an agriculture environmental management plan,” Ball said.
“In other words, when people see that symbol for New York-certified, there’s food safety and environmental stewardship, as well as the local aspect.”
 
New focus
The changes in the program have been in the works for the past year and a half, and they were formally unveiled last August, in conjunction with the dedication of a 120,000-square-foot “destination point” for New York produce in New York City, Ball said.
“Since then, we’ve been talking to the Food Industry Alliance, the chain stores across the state and grower groups, and signed up a lot of farmers who are excited about the program,” Ball said.
Produce is the foundation of the new effort, but the chief goal is to extend certification to all categories of New York agriculture, Ball said.
The program will unveil a marketing component, but Ball declined to estimate its total cost.
“Right now, it costs time and some thinking,” he said.
“The governor is going to market the program. Really, what it’s taken so far is the development of ideas, including those of the industry and lots of thinking and education and meetings.”
Grocery stores will feature point-of-sale materials about New York Grown & Certified products, Ball said.
“I think the media campaign is where we’ll start to spend some money, and we have some social media highlights available,” he said.
Consumers will see products grown “to a higher standard” when they see New York State Grown & Certified labels on them because of the program’s food safety and environmental stewardship requirements, Ball said.
“It doesn’t mean just local. We feel like the timing was just about perfect to answer concerns we were hearing from the public.”
Growers say they like the changes.
“I’m sure it is definitely better than it was. It’s bringing products to everyone’s attention,” said John Williams, partner with Williams Farms LLC in Marion, N.Y.
The program likely will be especially effective in bringing state-grown fruits and vegetables into schools and other institutions, Williams said.
“They’re trying to establish hubs in central locations, where all these smaller New York State farms can bring, say, two pallets of homegrown vegetables to the location and they’ll send a truck to bring it to prisons and schools,” he said.
“There’s such a huge gap between the farmers and the buyers.”