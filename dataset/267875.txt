Wisconsin lawmakers update nutrition curriculum guidelines
Wisconsin lawmakers update nutrition curriculum guidelines

The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — A new Wisconsin law says nutrition instruction in school health classes must be based on guidelines that are updated every five years by federal agriculture and health officials.

                Wisconsin Public Radio reports that while many school districts have updated their health curriculum, the law updates a statute that hadn't been updated in more than 70 years. The statute previously required school boards to have instruction on the vitamin content of food and the health value of dairy products.
Christina Lemon is a registered dietitian and president of the Wisconsin Academy of Nutrition and Dietetics. She says there's more to nutrition than that.
The federal government recommends limiting added sugars and saturated fats, reducing sodium intake and eating nutrient-dense foods. The guidelines are meant to help people maintain a healthy weight and prevent disease.
___
Information from: Wisconsin Public Radio, http://www.wpr.org