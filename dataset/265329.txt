AP-ME--Maine News Digest 6 pm, ME
AP-ME--Maine News Digest 6 pm, ME

The Associated Press



Maine news from The Associated Press for Friday, March 23, 2018.
Here's a look at how AP's general news coverage is shaping up today in Maine. Questions about today's coverage plans are welcome, and should be directed to the northern New England desk at 207-772-4157.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking news and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date.
TOP STORIES:
CHINESE LOBSTER IMPORTS
BIDDEFORD, Maine — China's hunger for American lobsters is helping keep prices high to U.S. consumers, but a tariff on the seafood does not appear imminent. The Asian country imported more than 17.8 million pounds of lobster from America in 2017, eclipsing the previous record of about 14 million pounds in 2016, federal statistics state. The value of the imports also surged from $108.3 million in 2016 to $142.4 million last year, according to data from the National Oceanic and Atmospheric Administration. By Patrick Whittle. SENT: 515 words, photos.
IN BRIEF:
SCALLOP FISHING: Federal fishing managers say they've set new measures to try to resolve a dispute between members of the East Coast scallop fishery.
GOVERNMENT INVESTIGATIONS: Maine's legislative watchdog committee will probe the roll-out of an unemployment program and the state's diversion of lumber to a logging company.
MAINE UNEMPLOYMENT: The state says Maine's unemployment rate is the lowest in more than four decades.
SUPERINTENDENT-VOTER REGISTRATION: A Maine superintendent is calling for an end to a student voter registration drive that is part of a campaign to force her ouster.
CHILD DEATH-MAINE: A legislative watchdog in Maine is issuing subpoenas to schools and state education agencies as they probe the state's response in the cases of two girls who recently died following abuse.
GOVERNMENT INVESTIGATIONS: Maine's legislative watchdog committee will probe the roll-out of an unemployment program and the state's diversion of lumber to a logging company.
COD FISHING: U.S. fishing regulators are soliciting feedback from the public about a plan to increase catch quota for a slew of key fish stocks, some of which are jointly managed with Canada.
GRANDPARENTS-OPIOIDS: The U.S. Senate has passed an act proposed by senators from Maine and Pennsylvania to provide support for grandparents who are raising grandchildren as a result of the opioid epidemic.
NEWSPAPERS SOLD: The owner of a newspaper group that dominates Maine media says he will expand his footprint by acquiring two more daily papers and a weekly group.
NALOXONE RULES: The Maine Senate has voted unanimously to increase availability of the anti-overdose drug naloxone in pharmacies.
ARMED ROBBERIES-CUMBERLAND COUNTY: Police in Maine are investigating whether a string of armed robberies in Cumberland County are connected.
UNLICENSED TATTOO ARTIST-DISEASE: Maine health officials are warning residents about an unlicensed tattoo artist who may have exposed multiple people to hepatitis C.
DAM REMOVAL: A Down East Maine environmental group is encouraging the public to show up for a public meeting about the relicensing of dams on a river in a rural part of the state.
PRISON SHUTDOWN: A minimum-security prison that Gov. Paul LePage shuttered without legislative approval is getting five new guards and an unknown number of inmates.
___
If you have stories of regional or statewide interest, please email them to apmaine@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867.