Chicago Mercantile Exchange live cattle futures settled mostly lower on Tuesday as a steep drop in the stock market dragged futures from the highest levels of the month, posted early in the session, traders said.
Strong packer margins and climbing beef prices had supported early gains before outside markets undercut the rally. The Dow Jones Industrial Average tumbled more than 400 points.
“Cattle weren’t able to hold the early strength. When the stock market turned lower it pushed the futures into more of a defensive tone,” said Matthew Wiegand, broker at FuturesOne.
“There’s always some jitters that when the stock market goes, that’s a reflection of confidence. Usually that hurts beef more because it’s viewed as a premium protein,” he said.
CME April live cattle closed 0.625 cent per pound lower at 121.100 cents, June futures ended up 0.175 cent at 105.050 cents and August fell 0.150 cent to 104.925.
Packer margins widened to an estimated $57.65 per head on Tuesday, from $44.60 the previous day, according to livestock marketing advisory service HedgersEdge, as wholesale beef prices extended recent gains.
The U.S. Department of Agriculture quoted the choice boxed beef cutout on Tuesday at $217.54 per cwt, up $2.54 from Monday and up $5.52 from a week ago. The select cutout gained 39 cents to $202.11 per cwt, up $2.53 from last week.
Cash cattle trading at U.S. Plains feedlot markets has not materialized yet this week, although traders expect prices to be at least steady with last week.
Feeder cattle futures were also mostly lower on spillover pressure from live cattle.
April feeders eked out a 0.075 cent per pound daily gain to settle at 139.100 cents. May fell 0.450 cent to 140.150 cents and August shed 0.650 cent to 146.475 cents.
Lean hog futures dropped along with the cattle markets, pressured by persistent concerns about ample hog supplies and narrowing packer margins.
The average pork packer margin on Tuesday slipped to $13.10 per head, compared with $29.10 a week ago, according to HedgersEdge.
May hogs closed down 0.550 cent per pound at 67.450 cents and actively traded June dropped 1.550 cents to a two-week low of 74.825 cents, down nearly 5 percent after four straight days of declines.