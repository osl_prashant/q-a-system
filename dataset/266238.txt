As consumer demand for avocados surges, so does their popularity among foodservice operators.
Suppliers say they’re seeing more and more restaurants ordering avocados, and those restaurants are using them for more than just guacamole.
“Foodservice just keeps on plowing along,” said Bob Lucy, partner in Del Rey Avocado Co., Fallbrook, Calif.
“We’re fortunate that many restaurants now have to have avocado toast on the menu,” he said. “It’s become an important thing to have.”
Restaurants are serving avocados for breakfast these days as well as for appetizers at the bar, Lucy added.
At Henry Avocado Corp., Escondido, Calif., where foodservice accounts for at least half of the business, president Phil Henry credits expanding foodservice sales to restaurants adding avocado items to their menus, especially at breakfast time.
The development of breakfast has resulted in incredible growth in offerings like avocado toast and sliced avocados on omelets, he said.
“Everything involving breakfast has been a huge growth area,” Henry said.
Avocado toast is a relatively new item for the U.S., but it’s been popular in places like Chile for years, said Rankin McDaniel, president of McDaniel Fruit Co. Inc., Fallbrook.
The popularity of avocado toast and other items featuring the fruit has helped boost foodservice business for McDaniel Fruit.
“Foodservice is an area that McDaniel Fruit Co. has been focusing on because it’s a growth area,” McDaniel said.
Demand from existing operations that use avocados has increased, he said, and new restaurants and institutions have added avocados to their menus as well.
“That has created significant growth,” he said.
Companies that sell processed guacamole seem to be doing fine with that side of the business as well, Lucy said.
Foodservice business also is very good at Calavo Growers Inc., Santa Paula, Calif., said Rob Wedin, vice president of sales and marketing.
Business seems to be bouncing back after some eateries cut back on the fruit last year because of tight availability and higher prices, he said.
This year, restaurants seem to be putting avocados back on their menus “big time,” he said.
Operators are using avocados in spreads, slicing them and putting them on top of eggs or in salads or sandwiches, he said.
Last year was flat when it came to foodservice sales, agreed Robb Bertels, vice president of marketing for Mission Produce Inc., Oxnard, Calif.
But with volume on the rise this year from California as well and Mexico and Peru, it should get foodservice operators back into the mindset of increasing the amount of items that include avocados, he said.
New restaurant concepts also are springing up, Wedin said, and “It seems like just about every new concept has avocados on its menu.”
One of those new concepts, Avocaderia in New York, touts a menu that consists solely of offerings that feature avocados.
Owners recently made a pitch for financial backing on the “Shark Tank” TV show and managed to get an investment totaling $400,000 from two of the “sharks” — billionaire Mark Cuban and real estate mogul Barbara Corcoran.
Foodservice operators are using avocados in a number of unique ways, agreed Steve Taft, president and CEO of Eco Farms Corp., Temecula, Calif.
“You would never have thought of using avocados in smoothies, but a lot of people are doing that,” he said.
Foodservice sales also are beneficial because they provide an outlet for all the avocados on the tree, grower-shippers say, even second-grade fruit.
“Historically, foodservice business has been partially supplied with No. 2 fruit that has some surface blemishing but has the same quality inside as the No. 1 fruit,” Henry said.