Editorials from around New England
Editorials from around New England

By The Associated Press
The Associated Press



Editorials from around New England:
___
VERMONT
The Caledonian-Record
April 11
A recent decision by the United States Department of Commerce — to impose duties on Canadian newsprint imports — could end up killing many of America's last remaining newspapers.
The move followed a complaint by a single paper mill in Washington State — the North Pacific Paper Company (NORPAC) — which is owned by a New York-based hedge fund.
All other U.S. newsprint mills, plus the American Forest and Paper Industry, rejected the NORPAC complaint. They knew what we know. These tariffs (as high as 32-percent) poses a mortal threat to newspapers at a time when we collectively can least afford it.
As the NewsMedia Alliance explains:
— Historically, advertising revenue supported the newspaper. The recession and market forces have cut our print ad revenues in half over the last 10 years. With less print advertising, newspapers are using less newsprint. In fact, the demand for newsprint in North America has declined by 75 percent since 2000. The decline in the newsprint market is not caused by any unfair trade.
— Since these tariffs have been announced and collected at the border, newspapers have experienced price increases of 20-30 percent. Publishers are not able to absorb these costs. We will have to consider raising prices on readers and advertisers, cutting back on news distribution and reducing our workforce. Not only will newspapers suffer, but so will our workers, readers and advertisers.
We join other publishers nationwide in calling on the International Trade Commission to put a stop to NORPAC's trade case and remove these tariffs. Local jobs and news coverage are at stake.
Online: https://bit.ly/2qltDQw
___
MAINE
The Portland Press Herald
April 11
A bad year for the already struggling North Atlantic right whale has everyone concerned with its survival eager to respond. But even with the clock ticking, it would be wrong to act out of panic, or just for the sake of taking action.
That's why we're glad to see that Maine will receive a $700,000 federal grant to investigate what role the lobster industry in the Gulf of Maine might be playing in the surge of deaths among the ocean behemoths. With fishing gear entanglements one of the factors in the rising mortality, the industry is facing calls from some scientists for new regulations. But no concrete evidence has yet linked the deaths to lobstering, and no new rules should be put in place unless it has.
The National Oceanic and Atmospheric Administration grant will fund a three-year project to collect data on how and where fishing gear is used throughout the Gulf of Maine. It is part of an investigation launched after the 2017 deaths of 17 right whales, reducing the overall population to about 450.
Some scientists are arguing that unless more-stringent restrictions are placed on the U.S. fishing industry, including Maine's lobstermen and women, right whales will reach "functional extinction" — with too few breeding females to survive — in years, not decades.
But the situation on the water is not that clear. While fishing gear entanglement and ship strikes are part of the problem, they don't tell the whole story, and it isn't anywhere near certain that additional regulations on the lobster industry would save any whales.
Of the 17 dead right whales located last year, five were found in the United States. One was struck by a ship, and the cause of death of the remaining four is undetermined. None of the deaths has been tied to the lobster industry, state and industry officials say.
In contrast, several of the whale carcasses found in Canada had been entangled in snow crab fishing gear, leading Canada to announce new regulations.
Canadian regulations have been more lax than those in the United States, and that may be part of the problem.
Warming temperatures in the Gulf of Maine mean that the zooplankton consumed by right whales is moving northward, into the less-regulated Gulf of St. Lawrence. As the right whales move there in search of food, they may be more likely to be hit by a speeding boat or get caught in fishing gear.
Or, the whales may be too worn out by the longer journey, or by fighting through gear, to mate in sufficient numbers, another idea being floated.
It is important that we understand whether and how much each of these factors is playing into the rising mortality rates for right whales, particularly when Maine's $434 million lobster industry is at issue.
Regulations work — they helped restore the right whale population when it fell to 295 in 1997. But they shouldn't be pursued unless we are certain they can make a difference.
If lobstering is not killing right whales, then new rules on lobster fishing will do nothing to save them, and ultimately, saving right whales is the goal.
Online: https://bit.ly/2Hjggdn
___
NEW HAMPSHIRE
Foster's Daily Democrat
April 9
The city of Dover, the greater Seacoast region, and the state of New Hampshire will suffer a great loss come June 1 with the retirement of longtime Dover Police Chief Anthony Colarusso.
Colarusso spent his entire 33-year career with the Dover Police Department, but his impact was felt in much wider circles across the state and region. He took on leadership roles speaking out on some tough and controversial issues such as help for drug addicts, advocacy for immigrants, and legislative support for transgender rights.
Colarusso, who became chief in 2007, started out as a patrol officer, spent about 15 years in the detective unit and climbed up the ranks from detective to detective sergeant, lieutenant and captain. He spent time working on juvenile crime and prosecuting juvenile cases. He investigated child sex abuse and physical abuse cases along with crime prevention programming.
The chief built a police force with a reputation for the highest professional standards as noted by the department's continuing success in achieving accredited status. The department received accreditation from the Commission on Accreditation for Law Enforcement Agencies, Inc. (CALEA) and is the only department in the state to be accredited nine times and one of about 30 police agencies internationally to have achieved that level of professionalism.
The deportment of officers and philosophies of the department come from the top down and Colarusso has always made it clear that police officers are here to serve.
"When you can be on a first-name basis with officers, that's the sign of a vibrant community. That's very important to us. That's how you build a community," Colarusso said in 2016, explaining his views on community policing.
Rest assured community policing doesn't mean soft on crime. A suspect who robbed a Central Avenue bank in Dover last month found that out when he was forced to surrender himself just hours after the incident. The Dover Police Department is a well-oiled crime-fighting machine and will find and prosecute criminals. But, as Colarusso has shown time and time again, that's not all there is to it.
As Dover's chief, Colarusso received a rare honor several years ago when he was named the Greater Dover Chamber of Commerce's Citizen of the Year. That's a title not many police chiefs are given. The selection committee described Colarusso as "a man of integrity, professionalism and dedication."
The chief collected that award with modesty, saying, "I don't consider myself a chief, I am a sworn officer."
Given the high moral and ethical standards by which Colarusso lives and works, it is not surprising that he has made some headlines championing underdogs.
When the opioid crisis hit hard, he was among the first local police chiefs to get his department to host a Recovery Day for those suffering from substance misuse disorder and their loved ones. The event featured an "amnesty box" in which attendees were allowed to anonymously dispose of illicit drugs, no questions asked.
Colarusso was among local police chiefs to denounce comments by President Donald Trump last year calling for rough treatment by police of criminal suspects.
The chief also rejected the idea that the Dover Police Department would become an arm of federal enforcement to rid the country of immigrants. Instead, he met with frightened immigrants at a public forum to reassure them that his department would not target them because of their ethnic background.
Most recently, Colarusso took on another controversial issue — state legislation aimed at establishing transgender rights. Colarusso came out strongly in favor of the bill when he testified before the N.H. Legislature on behalf of the New Hampshire Association of Chiefs of Police. Colarusso noted that in places where legal protections are in place, rates of violence against transgender people go down with no uptick in public safety incidents.
Colarusso's words about this sensitive issue and so many others make us proud to call him chief. He is a compassionate leader who has etched an indelible mark on his community, region and state. There will be big shoes to fill come June 1.
Online: https://bit.ly/2qopZ8t
___
CONNECTICUT
The Meriden Record-Journal
April 10
Connecticut saw a 10 percent increase in fatal overdoses in 2017, driven by a nearly 40 percent rise in those attributed to fentanyl, a synthetic drug much stronger than heroin.
State officials aren't waving a white flag, however. In fact, the Department of Public Health has employed a new tactic in its fight against opioid abuse.
The department's epidemiologists have written algorithms to detect symptoms from data routinely sent by the emergency rooms at 27 hospitals to the DPH.
The Syndromic Surveillance System has traditionally been used to track cases of rare diseases, influenza, and other conditions, but DPH has used it to trace 1,317 drug overdoses in hospital emergency rooms statewide.
In a statement announcing the data system, DPH Commissioner Raul Pino said, "The real-time nature of the data will allow us to respond more quickly to changes in patterns of nonfatal and fatal opioid overdoses and to design, target, implement and monitor more effective interventions to break the cycle of overdose and death and curtail opioid addiction in Connecticut."
We hope this effort will help. It's certainly worth a shot. Opioid addiction is a public health crisis.
Speaking at a recent legislative appropriations hearing, Connecticut's Chief Medical Examiner James Gill said the number of accidental drug intoxication deaths in Connecticut has increased almost 300 percent over the past five years, "which has contributed to a 70 percent increase in our autopsies."
Some more jarring numbers: Emergency room departments in our state saw an average of 156 suspected drug overdose visits per week in January and February. New Haven County saw the highest numbers, with 405 suspected drug overdose visits, followed by Hartford County, 398, Fairfield County, 186, Middlesex County, 80, Litchfield County 68, Tolland County, 64, Windham County 62 and New London, 54.
We commend the Department of Public Health for utilizing the Syndromic Surveillance System to curtail opioid abuse. If we're to get a handle on this epidemic, we'll need all hands on deck and more out-of-the-box thinking such as this.
Online: https://bit.ly/2INe7U0
___
MASSACHUSETTS
The Cape Cod Times
April 8
You've seen it yourself. The shoulders of roads blanketed in litter. Discarded furniture in nearby woods. Plastic items on the beaches after a storm.
"We have a huge litter problem in Massachusetts that seems to be getting worse," said Neil Rhein, executive director of Keep Massachusetts Beautiful, a nonprofit group based in Mansfield. "Adding to the problem is the fact that many of the materials that people litter do not decompose. So all those plastic bottles, nip bottles, Styrofoam coffee cups, and plastic bags will remain in place for years if they are not cleaned up."
A study by Keep America Beautiful from 2009 showed that approximately 85 percent of littering is done with "notable intent." Which means that most people know they are littering but either don't understand that it's wrong or just don't care. And that's a shame.
Wendy Northcross, CEO of the Cape Cod Chamber of Commerce, said litter is also an economic problem. She pointed to research conducted by the Keep America Beautiful organization. In addition to the direct costs of litter, Keep America Beautiful also explored the indirect costs of litter, particularly to property values and housing prices.
?... Evidence indicates that the presence of litter in a community decreases property values by 7 percent," according to its website. "The reported data bear out the impact of litter on property values, as 40 percent of homeowners surveyed think that litter reduces home values by 10-24 percent, while 55 percent of Realtors think that litter reduces property values by about 9 percent." Sixty percent of property appraisers surveyed said they would reduce a home's assessed value if it were littered.
The study by Keep America Beautiful found:
— Motorists and pedestrians combine to contribute nearly 70 percent of litter over 4 inches. Along roadways and highways, motorists generate 52.2 percent of litter, and pedestrians 17.5 percent.
— Motorists not properly securing truck or cargo loads, including collection vehicles, represent 20.7 percent of roadway litter 4 inches and larger. Vehicle debris and improperly secured containers, dumpsters, trash cans or residential waste/recycling bins represent another 8.1 percent of litter.
— Along U.S. roadways, cigarette butts are the most frequently identified item. Tobacco products comprise roughly 38 percent of all U.S. roadway litter in overall aggregate analysis. Paper (22 percent) and plastic (19 percent) are the next largest contributors of litter on roads and highways.
— Packaging litter comprises nearly 46 percent of litter 4 inches and greater. This includes fast-food, snack, tobacco and other product packaging. And 61 percent of beverage containers on U.S. roadways are soft drink and beer containers.
A study by Keep Massachusetts Beautiful found that 82 percent of Bay State residents believe litter is a serious issue in their community.
The Massachusetts Department of Transportation estimates that 90,000 bags of trash — about 360 tons — are picked up along our major highways each year. And that does not include larger items that are removed from the roadsides, such as refrigerators, television sets, tires, etc. MassDOT spends about $800,000 to $1 million per year for inmates to pick up litter.
Unfortunately, the total quantity of litter and the amount of money spent cleaning it up are often underestimated. "Even though there is some government spending on street sweeping and litter control, and private organizations, property owners, volunteers pick up litter, the litter problem overwhelms the capacity of all these efforts," said Clint Richmond of the Massachusetts Sierra Club, who is also a member of the Brookline Solid Waste Advisory Committee. "We have a crisis ...."
Simply put, litter collection efforts are overwhelmed by the amount of litter on our roads and in our neighborhoods.
Listen to an interview with the president of the Cape Cod Anti-Litter Coalition
During last year's second annual Great Massachusetts Cleanup, more than 7,000 volunteers in 84 communities collected 136 tons of trash. These numbers do not include cleanups that occur in many other communities, Rhein said. "So it's probably safe to estimate that more than 1,000 tons of litter is collected each year, but tons still remain uncollected."
Rhein said uncovered trucks, illegal dumping, lack of education or respect for the environment, a lack of prevention programs and a lack of littering enforcement all contribute to the problem.
Anyone over the age of 40 probably remembers Keep America Beautiful's public service announcements from the 1970s that featured the "crying Indian." And Massachusetts had similar educational campaigns, such as "Give a Hoot, Don't Pollute."
"But those messages have disappeared from the public consciousness and, as a result, an entire generation has grown up without understanding the basic message that people need to clean up after themselves and dispose of their trash responsibly," Rhein said.
And it appears anti-litter programs in Massachusetts are not a priority when it comes to state funding. Rhein said Keep Massachusetts Beautiful receives no state funding. "Many other states provide funding through their departments of transportation," he said. "We are 100 percent volunteer at this time. Of course, we would like to see that change. We have discussed DOT funding with them but nothing is imminent."
As for the problem of illegal dumping and uncovered loads, Rhein said enforcement of anti-littering laws is "virtually nonexistent" in Massachusetts. However, he said the Massachusetts Environmental Police have done some good work using remote cameras to catch illegal dumpers.
Brad Verter of the Mass Green Network, an environmental collective linking grass-roots activists and local groups across the state, said litter is not just an environmental problem.
"It is also a problem of social justice," he said. "Our most vulnerable communities do not have the same resources as wealthier neighborhoods, and cannot afford to invest as much in keeping streets and parks clean."
Verter said he believes insufficient and unequal waste management budgets are part of the problem. "Show me a beach and I can tell you if it's a wealthy town or a poor one," he said in an email. "At a deeper level, the problem is a cultural one. As Americans, we have become accustomed to a culture where everything is disposable. We get our coffee in to-go cups instead of bringing our own mugs. We get our groceries in disposable bags instead of bringing our own. We buy disposable razors, disposable silverware, disposable cameras and disposable vape pens. We need to get rid of disposables."
Richmond of the Massachusetts Sierra Club said litter is a global problem.
"While we most often think of litter on streets and in our parks, litter is an even larger concern in waterways and the ocean," he said. "All water is connected, which is why we have gigantic problems like the plastic gyres ... in the oceans."
More than 87,000 tons of ocean plastic, or 1.8 trillion pieces of plastic trash, are floating inside "The Great Pacific Garbage Patch" between California and Hawaii. Twice the size of Texas, the floating mass of plastic is up to 16 times larger than previously thought, according to scientists who performed an aerial survey.
The results, published last month in the journal Scientific Reports, reveal that this plastic blight in the Pacific Ocean is still growing at what the researchers called an "exponential" pace.
"Plastic litter is permanent," Richmond said. "Massachusetts has been a national leader in addressing this problem at the local level with regulations promoting sustainable bags and food service packaging."
While local action sparks conversation and provides public education, there are limits in what can be done at the local level. "We need regulation at the state level," he said, "and we need manufacturers and retailers to shift to more sustainable products."
Jack Clarke, director of public policy and government relations for the Massachusetts Audubon Society, said butts, bags and balloons are a big problem along the coast.
"The remnants of butts, bags and balloons contribute to the 150 million metric tons of plastic now circulating in the planet's marine environment," he wrote in an op-ed to the Times. "This refuse is so ubiquitous that plastic is found in 60 percent of all seabirds, and in every species of sea turtle." The Woods Hole Oceanographic Institution has found billions of bits of plastic now accumulating in a new massive garbage patch in the Atlantic Ocean.
According to the journal "Science Advances," by midcentury the oceans will contain more plastic waste than fish, ton for ton.
Year after year, cigarette butts remain the most prevalent form of litter on Earth. The Ocean Conservancy consistently cites them as No. 1 in the "Dirty Dozen" types of trash dumped along America's coastlines. Last year, Massachusetts' annual coastal cleanup netted 44,703 butts.
"Tossed butts are not just ugly in the sand, they are toxic to toddlers who can ingest them and suffer serious medical complications," Clarke wrote. "Discarded butts are found in the stomachs of coastal water birds, sea turtles, fish and whales, where their hazardous chemical makeup can wreak havoc on marine animals' digestive tracts and lead to death."
Butts can take up to 25 years to biodegrade. While the paper and tobacco are dissolvable, the filters are not. Butts contain cellulose acetate — a form of plastic that persists in the environment similar to other forms of plastic. "Cigarettes also contain around 4,000 chemicals, 50 of them known carcinogens, such as cadmium, lead and arsenic, which can leach into the marine environment within an hour of contact with water," he said.
Smoke-free beach laws can help reduce such trash. So we're giving it a try in Massachusetts where today, at the six public beaches at our own Cape Cod National Seashore, and in 17 cities and towns, smoking on beaches is now banned.
Another problem is plastic bags.
The Sierra Club says Massachusetts residents use more than 2 billion plastic bags per year — about a bag per person per day. They contribute to litter, kill wildlife, require fossil fuels to produce and threaten human health by introducing toxic substances to the food chain.
In 2015, the Massachusetts Senate passed a statewide plastic bag ban, but the House of Representatives killed it. So instead, cities and towns are passing their own prohibitions on the distribution of disposable, single-use plastic shopping bags, with 55 of 351 municipalities having banned them thus far, including 12 of the 15 towns on Cape Cod.
Then there is the problem with balloons.
For more than a quarter-century, Beacon Hill lawmakers have refused to take action to stop the ceremonial release of balloons into the sky. Although balloons are great at birthdays, weddings and graduations, when released they're carried to the coast on prevailing winds as airborne litter. When they land, they threaten coastal critters. The U.S. Fish and Wildlife Service has found that coastal water birds, sea turtles, and other animals commonly mistake balloons for food such as jellyfish — and choke to death, or starve as their digestive system is blocked.
Five states have banned balloon releases, and Bay State municipalities are starting to ban them, starting with the coastal outposts of Nantucket and Provincetown. More Cape communities should ban them as well.
Rhein said Massachusetts will not begin to get a handle on our litter problem until it commits more resources to education, prevention, collection and enforcement.
Online: https://bit.ly/2GXZfSv
___
RHODE ISLAND
The Providence Journal
April 10
The timing seemed ironic, to say the least. On March 21, Rhode Island Gov. Gina Raimondo blew a prediction about the weather, expensively canceling state government for a snowstorm that never came. Then, two days later, she blasted National Grid for, in effect, blowing a prediction about the weather.
We kid you not.
National Grid, she complained, failed to have enough repair crews and equipment waiting before a freak windstorm of unexpected severity hit the state back in October.
"They didn't do a good job. They dropped the ball in a number of ways and I am disappointed," said Ms. Raimondo, who is up for re-election this fall.
The post-mortem, in the works for five months by her Division of Public Utilities and Carriers, came out days after the non-snowstorm. Rhode Island blew that call, even though it has two taxpayer-funded meteorologists on the payroll: Lenny Giuliano ($83,988) and Steve Cascione ($69,750). That's $153,738 in total.
In its report, the administration recommended that National Grid supplement its weather forecasting services to make more accurate predictions. But anyone who has followed New England weather for any length of time knows it can be maddeningly difficult to predict with perfect accuracy — for an energy distribution company no less than for a governor.
Back in October, a windstorm that turned out to be far more severe than predicted — the governor, for instance, failed to ramp up emergency operations ahead of time — slammed into Rhode Island, uprooting trees and toppling poles.
Some 144,114 customers — nearly 30 percent of those National Grid serves — were left without power. Astonishingly, the storm affected more customers than Superstorm Sandy in 2012 and did more damage to the company's poles than Tropical Storm Irene in 2011.
The company swiftly contacted the North Atlantic Mutual Assistance Group to rush in crews once the impact of the storm became clear. Within two-and-a-half days of the storm, 90 percent of customers were back up, and power to all Rhode Island customers was restored faster than in Massachusetts.
The administration's report gave high marks to National Grid for doing better responding to a storm in January and a string of nor'easters that hit in recent weeks.
But the governor remains unhappy with the October response. She said the company could have completed its repairs 36 hours earlier — something National Grid emphatically denies, arguing "the facts demonstrate to the contrary."
"The delay was not because crews were not working quickly enough, it was because Grid's management failed their customers," Ms. Raimondo said. "They failed to communicate. They failed to adapt."
National Grid should work with the Department of Public Utilities to determine whether any of its findings and recommendations might lead to what the report calls more "nimble" service in the future. (Too much regulation, though, could make it less "nimble" in such cases, not more). Energy is a life-and-death matter for people, and it is greatly in the public interest to have repairs done as quickly as possible after nature strikes.
But it seems pretty clear the biggest problem with the October storm was an inaccurate weather forecast, which led to a failure to have enough crews on hand early on. As the governor perhaps knows too well, that is a problem no one in New England has been able to solve perfectly yet.
Online: https://bit.ly/2GUphGg