"McDonald's recently sold its 2 billionth package of sliced apples."
That was Jim Allen's way of assessing the health of the foodservice business for eastern apples.
"That's good news for New York State growers," said Allen, president and CEO of the Fishers-based New York Apple Association.
The fast-food giant bought 15 million pounds of New York apples in 2014, which were the latest figures available, Allen said.
"The Albany-area McDonald's restaurants source the apple slices they sell from right here in New York State. That supports our local growers," he said.
Slicing has established itself as a reliable revenue stream, said Tim Mansfield, sales and marketing director with Burt, N.Y.-based Sun Orchard Fruit Co.
"It continues to be a strong part of the overall apple marketing plan," he said.
It also has helped apple growers "tremendously" over the past decade or so by putting a floor on grades that, before, ended up going to the juice market, Mansfield said.
John Teeple, owner of Wolcott, N.Y.-based Teeple Farms, described slicing as "a real bright spot" for the eastern apple business.
"They seem to be looking for more fruit, paying a premium price," he said.
So-called club varieties have not yet found a place in the slicing market, said Mark Russell, grower, vice chairman of the board and chairman of the marketing committee with Wolcott-based Crunch Time Apple Growers.
"Right now, I see a reluctance across the club spectrum to develop a branded slice - all the various club apples, whether it's Jazz or PiÃ±ata or Opal," said Russell, whose organization controls the marketing of the Cornell University-developed RubyFrost and SnapDragon club varieties.
"We're still trying to figure out what the value proposition of putting the branded apple in a slice is."
That process is ongoing, he said.
"For us, even if we were thinking about that, we would want a certain amount of volume to do a slice program," he said. "We're still a year or so away from having that kind of volume (on SnapDragon and RubyFrost). We've been approached but aren't ready to go there yet."
Slicing has been a boon to the apple category at large, though, said Jaime Williams, president of Timberville, Va.-based Turkey Knob Apples Inc., the marketing arm of Bowman Fruit Sales LLC.
"It's taking a larger chunk out of our fresh market each year," he said. "The prices once again on that are up. It all helps the bottom line."
Not all Eastern apple grower-shippers get involved with the slicing market, though.
Aspers, Pa.-based Bear Mountain Orchards Inc. stays out of the slicing market due to "liability" issues, said Patrick Malloy, salesman.
"We don't sell to slicers because no slicer as of yet will provide us with a release of liability in the process," he said.
"They're rolling the dice on us from a food-safety standpoint. It's good when it leaves here but it can be contaminated at their place, and it falls back on us, so from the liability standpoint, we don't sell to slicers."
On the other hand, if he got a release of liability from a slicer, "I'd ship tomorrow," he said.
Foodservice success, however, doesn't have to be related to slicing, some marketers say.
"We are not involved in the fresh sliced apple deal, but we do a lot of business in the foodservice sector," said John Rice, president of Gardners, Pa.-based Rice Fruit Co.
Rice Fruit sells most of its "third grade" apples - the fruit with lighter color - to schools and other institutions, such as prisons, that need a lower-cost apple, Rice said.
Rice also pointed to applesauce in squeeze pouches as a new entry in the foodservice category.
"These have been hugely popular, especially with kids and schools, and this has increased the demand for off-grade apples used to make this product," he said.
Foodservice in general is a "very definitely growing" part of the Eastern apple business, said Henry Chiles, owner of Covesville, Va.-based Crown Orchard Fruit Co.
"I think there's good demand there," he said.
Jack Bream, owner of Bream Orchards in Orrtanna, Pa., agreed.
"Let's face it: Half of what we put down the packing line goes to foodservice for a blemish issue or a color issue," he said.
Schools and other institutions are becoming bigger customers, and the U.S. Department of Agriculture puts out regular bids, Bream said.
"It's wonderful when they put apples out for bids," he said. "Sometimes we get them, sometimes not." Schools are easy orders to fill, Bream said.
"The local wholesaler can call and order three or four skids, and when they're local, they can come pick them up and deliver them to the school, and that doesn't go through the USDA," he said