BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Monday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMar                              121.10  Down 1.10Mar      119.10  119.20  118.15  119.20  Down 1.15May                              123.35  Down 1.05May      121.65  121.65  119.95  121.10  Down 1.10Jul      123.95  123.95  122.20  123.35  Down 1.05Sep      125.85  126.00  124.45  125.60  Down 1.00Dec      129.00  129.30  127.80  128.95  Down  .95Mar      132.50  132.60  131.15  132.30  Down  .95May      135.05  135.05  133.50  134.40  Down  .90Jul      135.60  136.25  135.55  136.25  Down  .85Sep      137.25  137.95  137.25  137.95  Down  .75Dec      139.90  140.50  139.90  140.50  Down  .75Mar                              143.05  Down  .75May                              144.80  Down  .75Jul                              146.50  Down  .75Sep                              148.10  Down  .75Dec                              150.50  Down  .75