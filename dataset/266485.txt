BC-KS--Kansas News Digest 5 pm, KS
BC-KS--Kansas News Digest 5 pm, KS

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Kansas.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
NEW & UPDATED:
— CHILD WELFARE, updated
— KANSAS-SOMALIS TARGETED, updated
— TRANSGENDER LAWSUIT, updated
— CROP REPORT, updated
— KANSAS MEASLES,  nn
— WOMAN KILLED-ESTRANGED HUSBAND, nn
TOP STORIES:
WATER PARK FATALITY-CHARGES
TOPEKA, Kan. — Amusement and water park rides are regulated by states, and their rules are inconsistent. When the world's largest waterslide was built in in Kansas, it was known for its lax regulations. Even after those regulations were strengthened in the wake of a 10-year-old boy's decapitation in 2016, criminal indictments in his death are raising questions about whether the rules are strong enough. By John Hanna. SENT: 130 words. UPCOMING: 800 words by 4 p.m.
AROUND THE STATE:
KANSAS ADOPTION-RELIGIOUS GROUNDS
TOPEKA — Adoption and foster care organizations contracting with Kansas welfare officials would be allowed to refuse placements to gay and lesbian couples based on religious beliefs under a bill that has passed the Senate. The Kansas City Star reports that the bill that passed Thursday on a 28-12 vote now heads to the House, where nearly identical legislation has also been introduced. UPCOMING: 300 words.
KANSAS-SCHOOL FUNDING
TOPEKA — Kansas lawmakers have proposed a $500 million increase in school funding to satisfy a state Supreme Court mandate. The legislation that a House committee advanced Wednesday night would phase in the increase over five years, The Kansas City Star reports . The plan emerged one day after Senate Democrats endorsed a $600 million increase for schools that was rejected by Republican senators. The state's high court has given lawmakers until April 30 to respond to its ruling that found schools are inadequately funded. SENT: 380 words.
TRANSGENDER LAWSUIT
COLUMBUS, Ohio — Four transgender individuals filed a lawsuit Thursday against Ohio saying the state won't allow them to change the gender listing of their birth certificates to properly reflect their identities. The American Civil Liberties Union, which is representing the plaintiffs, said the requirement prevents the three females and one male from obtaining a document essential to everyday living and subjects transgender people to discrimination and potential violence. By Julie Carr Smyth.  SENT: 575 words.
KANSAS-SOMALIS TARGETED
WICHITA — A militia member who tipped off law enforcement to an alleged plan by three men to bomb Somali immigrants in Kansas is expected to testify at their trial. Jurors will hear the testimony of Dan Day on Thursday. The paid informant wore a wire for the FBI, capturing months of profanity-laced recordings that are key to the prosecution's case. By Roxana Hegeman. SENT: 400 words.
CHILD WELFARE
TOPEKA, Kan. —The Kansas House has unanimously passed legislation that would require the state to release information after a child dies of abuse or neglect. SENT: 200 words.
WITH:
— MO-XGR-FOSTER CARE — The Missouri Senate has approved a measure to make it easier for state agencies to share information about potential child abuse.
CROP REPORT
DES MOINES, Iowa — Corn has been dethroned as the king of crops as farmers reported Thursday they intend to plant more soybeans than corn for the first time in 35 years, the U.S. Department of Agriculture said in its annual prospective planting report. By David Pitt. SENT: 565 words, photos.
BRIEFS:
— KANSAS MEASLES — Kansas officials have identified three new cases of measles, bringing the state's outbreak total to 13.
— WOMAN KILLED-ESTRANGED HUSBAND — A Topeka man convicted in the killing of his estranged wife last summer has been sentenced to life in prison.
SPORTS:
— BKC--FINAL FOUR-THE LATEST — The Latest on the Final Four of the NCAA Tournament (all times local). With AP photos.
— BKC--WICHITA ST-SHAMET — Wichita State guard Landry Shamet has declared for the NBA draft and intends to hire an agent, effectively ending his college career with two seasons of eligibility left.
___
If you have stories of regional or statewide interest, please email them to apkansascity@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Missouri and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.