Despite the controversy in 2017, it’s likely Roundup Ready 2 Xtend soybean and cotton acres will increase this year. Monsanto says there is enough supply to double soybean acres in 2018, compared with 2017, to 40 million acres, which is nearly half of the 91 million soybean acres.
“You’ll have fewer sensitive varieties out there, so that leads to the potential for a decrease in damaged soybeans,” says Bob Hartlzer, Iowa State Extension weed scientist. “And I think, I hope, applicators are going to pay a lot more attention to what is adjacent to their field.”
Regardless of the state you farm in, if you use dicamba tolerant soybeans or cotton you will face more restrictions this year per EPA’s label changes:

Products are restricted use, which means only certified applicators can apply them, and they must have dicamba-specific training.
Farmers must maintain records regarding use of dicamba products.
Dicamba can only be applied when wind speed is below 10 mph.
Reduction in time of day dicamba might be applied (specifics not stated).
Clean-out language has been added to prevent cross contamination.
Awareness of risk to nearby sensitive crops has been heightened by enhancing susceptible crop language and record keeping.

These label changes apply to new formulations of Engenia, FeXapan and XtendiMax. Several states have imposed further restrictions on the products.
State-Specific Restrictions on Dicamba
Arkansas
The Administrative Rules and Regulations Subcommittee of the Arkansas Legislative Council approved a proposed rule from the Arkansas State Plant Board that bans applicators from using dicamba herbicide between April 16 and Oct. 31.
The regulations include exemptions for dicamba use in pastures, rangeland, turf, ornamental, direct injection for forestry and home use. In addition to banning dicamba products for use in row crops, the state is increasing fines for violations to $25,000. These decisions come after nearly 1,000 official complaints in the state.
At press time, the rule will again be considered by the full Arkansas Legislative Council.
Illinois
While Illinois did not add any additional restrictions to EPA’s requirements, it is focusing more on education. Mandatory classes started in November and will continue through March to ensure all applicators can attend.
The training is for use of Engenia, FeXapan and XtendiMax; all other dicamba formulations are illegal in soybeans. The classes review application methods and provide details about what label changes mean for applicators.
“It is important that everyone in the stewardship chain understand the proper use of these products and effective weed management principles,” according to the Illinois Fertilizer and Chemical Association.
Indiana
On Aug. 30, 2017, the Indiana Pesticide Review Board voted to place all dicamba products used for agricultural purposes under restricted use. This decision means only certified applicators will be authorized to apply the product.
The proposed rule would restrict “any dicamba containing pesticide product that (A) contains a dicamba active ingredient concentration greater than or equal to 6.5% and (B) is intended for agricultural production uses but does not also contain 2,4-D as an active ingredient; or is not labeled solely for use on turf or other non-agricultural use sites.”
Iowa
Iowa officials issued a Special Local Need label for XtendiMax, which requires expanded dicamba application training that includes:

New use pattern for dicamba-tolerant soybeans.
Application requirements including wind speed and direction and buffers.
Information about temperature inversions.
Detailed record keeping.
Required spray tank clean-out.
Information about off-target movement.

The department worked with Iowa State University to develop these topics and will approve the training to reduce off target movement.
Minnesota
Minnesota has imposed restrictions for all new dicamba formulations. All three of the new formulations are restricted use pesticides for retail sales and can only be applied by Minnesota Certified Applicators.
The formulations can’t be applied after June 20. Soybeans are still in the vegetative growth stage by June 20. Research shows plants in the vegetative stage are less affected by dicamba than in the reproductive stage.
Applications aren’t allowed if air temperatures in fields are above 85°F or if the National Weather Service forecast high for the nearest location exceeds 85°F.
Missouri
After more than $145,000 in fines in 2017, the Missouri Department of Agriculture has issued restricted use labels for new dicamba herbicides.
The restrictions state applicators must complete an online dicamba notice of application daily before applying the product and they cannot apply dicamba before 7:30 a.m. or after 5:30 p.m. Cutoff dates vary by county. Farmers in Dunklin, Pemiscot, New Madrid, Stoddard, Scott, Mississippi, Butler, Ripley, Bollinger and Cape Girardeau counties cannot use the product after June 1, 2018. All other counties must stop use after July 15, 2018.
North Dakota
North Dakota’s Department of Agriculture has added restrictions for new dicamba products.
Requirements include:

June 30 or first bloom (whichever comes first) application cutoff.
No application if actual or forecast temperature is more than 85°F.
Only apply one hour after sunrise to one hour before sunset.
Drive 12 mph or less.
Applicators must notify the Department of Agriculture and include contact info, certification, date, time and location of application.
15 gal. per acre.
No applications with 80° or less nozzles.

Tennessee
The Tennessee Department of Agriculture is considering new rules for dicamba. The group is seeking a Special Local Needs label to restrict use of new formulations.
If approved the state will require certified applicators to complete dicamba-specific training through the University of Tennessee Extension or dicamba manufacturers, apply product between 7:30 a.m. and 5:30 p.m. and use a hooded sprayer for all applications from July 15 to Oct. 1.
Tennessee is also trying to prohibit application of generic formulations from May 15 to Oct. 1.