The Hunts Point Produce Market is waiving its entry fees for buyers through the end of the year.
 
Seeking to attract more business, Michele Turkel, executive director of the trade association for the Hunts Point Produce Market, said Sept. 8 the market's cooperative board elected to waive the fees for buyers to enter the market on beginning on Labor Day weekend. The fees are expected to return after Dec. 31, she said.
 
Normal entry fees for buyers range from $2 to $20 per day, depending on the type of car or truck used by the buyer, she said.