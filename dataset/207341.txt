Chicago Mercantile Exchange live cattle futures gained sharply on Wednesday, fueled by short-covering in response to higher-than-expected preliminary cash prices, said traders.They said fund buying contributed to market advances.
August ended 2.200 cents per pound higher at 117.275 cents and above the 20-day moving average of 115.838 cents. October closed 2.000 cents higher at 118.875 cents.
Wednesday morning's Fed Cattle Exchange (FCE) yielded market-ready, or cash, sales from $118 to $118.50 per cwt. Animals there last week fetched $117.25 to $118.75 per cwt.
FCE's generally steady cash returns encouraged bullish market investors despite seasonally lackluster wholesale beef values and declining, but still profitable, packer profits.
Some worried that futures would react bearishly to Tuesday's news of a reported case of atypical mad cow disease in Alabama, which was not considered a threat to humans.
Investors had expected lower FCE cash prices following the BSE news, but none of the cattle there sold below $118 per cwt which could mean steady money for remaining cattle in the U.S. Plains, said Domenic Varricchio, a broker with Schwieterman Inc.
Packer bids for unsold cash cattle were $115 to $117 per cwt against up to $122 asking prices, said feedlot sources.
Market participants await the U.S. Department of Agriculture monthly Cattle-On-Feed report on Friday.
USDA will simultaneously release the semi-annual cattle inventory report.
CME feeder cattle closed more than 2 percent higher on short-covering and live cattle futures buying.
August feeders ended 2.900 cents per pound higher, or up 2.21 percent, at 154.500 cents. 
Higher Hog Market Settlement
Fund buying and strong wholesale pork values, led by near-record pork belly prices, boosted CME lean hog futures, said traders.
They said futures' sizable discounts to CME's hog index for July 17 at 92.46 cents attracted more buyers.
August closed 2.075 cents per pound higher at 82.575 cents, and topped the 10-day moving average of 81.903 cents. October ended up 0.925 cent to 68.800 cents, matching the 40-day moving average.
The recent cash price slide suggests the market might have topped out seasonally in anticipation of a supply buildup ahead, a trader said.
He said surging pork belly prices in the midst of the bacon-lettuce-tomato sandwich season was supporting wholesale pork values, or the cutout.
"When belly prices come down after peaking soon, it's going to be ugly for cash and the cutout," the trader said.