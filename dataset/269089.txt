Indiana lawmaker hopes to protect forests from logging
Indiana lawmaker hopes to protect forests from logging

The Associated Press

BLOOMINGTON, Ind.




BLOOMINGTON, Ind. (AP) — An Indiana lawmaker plans to reintroduce legislation to protect the state's forests after seeing the outcome of a timber cut that removed more than 1,700 trees.
Democratic Rep. Matt Pierce toured the Yellowwood State Forest Thursday after logging had finished, The Herald Times reported . The state forestry division sold the timber to loggers for $110,000.
Pierce said he's trying to protect the forests and understand the forestry division's stance, which is that tree removals can be positive for the forest and promote healthy growth.
State officials have defended the timber sale as selective logging that's part of a well-researched forest management plan.
Pierce introduced a bill in 2014 that would have protected areas of state-owned land in Yellowwood, Morgan-Monroe, Jackson-Washington and Clark state forests from motorized access, commercial activity and logging. The bill failed to pass.
Pierce said he's concerned about the devastation left in the wake of the logging. He said he hopes to see up to 40 percent of state forests protected from logging.
"I think it's very important for the public to be aware of what is going on in their state forests," Pierce said. "Once it's disturbed like this, it will take a while to come back, for this forest to regenerate."
Brown County resident Dave Seastrom opposes logging but said positive steps were taken this time to help preserve the soil, limit erosion and stop invasive species from spreading.
___
Information from: The Herald Times, http://www.heraldtimesonline.com