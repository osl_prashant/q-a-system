The National Restaurant Association plans to spotlight food safety through information and tools to help people avoid illnesses during September's National Food Safety Month.
The Washington, D.C.-based organization is using the month to highlight the importance of detecting and preventing foodborne illnesses, according to a news release.
This year's campaign concentrates on protecting against some of the leading causes of foodborne illnesses, including norovirus and Hepatitis A.
The organization plans to show those viruses' sources and provide tips and tools on detection and preventing the spreading of foodborne illness through proper hand washing and cleaning procedures, according to the release.
Each week, the association is scheduled to introduce new training tools and resources which include activities, posters, information graphics and videos.
The weekly themes are:
Week 1: Viruses: Know The Basics;
Week 2: Norovirus;
Week 3: Hepatitis A;
Week 4: Stop The Spread; and
Week 5: Keep Your Guests Safe.
The effort reflects the association's continuing commitment to food safety, according to the release.
"Food safety and security is the top priority for America's one million restaurants," Sherman Brown, the organization's senior vice president of ServSafe food safety and handling program, said in the release.
"With over 130 million meals served daily, learning how to detect and protect against foodborne illness is top of mind in the restaurant and foodservice industry. This September, we'll provide the tools foodservice professionals need to protect themselves and their guests against foodborne illness."
In its sixth year, the food safety month is sponsored by SCA, maker of the Tork-branded portable professional hygiene products.
The association created the month in 1994 to increase public awareness about the importance of food safety education.