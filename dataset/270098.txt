BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Wednesday:
Open  High  Low  Settle   Chg.ORANGE JUICE                          15,000 lbs.; cents per lb.                May     145.20 145.35 143.00 144.70   —.95Jun                          144.65  —1.00Jul     145.00 145.45 143.05 144.65  —1.00Aug                          145.20   —.75Sep     145.95 145.95 144.20 145.20   —.75Nov     146.65 146.65 145.00 145.90   —.75Jan     146.00 146.45 146.00 146.45   —.35Feb                          146.95        Mar                          146.95        May                          147.70        Jul                          148.10        Sep                          148.40        Nov                          148.70        Jan                          149.20        Feb                          149.50        Mar                          149.50        May                          149.80        Jul                          150.10        Sep                          150.40        Nov                          150.70        Jan                          151.00        Feb                          151.30        Mar                          151.30        Est. sales 1,629.  Tue.'s sales 1,815   Tue.'s open int 13,421