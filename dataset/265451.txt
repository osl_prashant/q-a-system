AP-MI--Michigan News Digest 1:30 pm, MI
AP-MI--Michigan News Digest 1:30 pm, MI

The Associated Press



Good afternoon! Here's a look at how AP's general news coverage is shaping up in Michigan at 1:30 p.m. Questions about today's coverage plans are welcome, and should be directed to the AP-Detroit bureau at 800-642-4125 or 313-259-0650 or apmichigan@ap.org. Ken Kusmer is on the desk. AP-Michigan News Editor Roger Schneider can be reached at 313-259-0650 or rschneider@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date. All times are Eastern.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
TOP STORY:
OPIOID CRISIS-FUNDING
CHERRY HILL, N.J. — The federal government will spend a record $4.6 billion this year to fight the nation's deepening opioid crisis, which killed 42,000 Americans in 2016. But some advocates say the funding included in the spending plan the president signed Friday is not nearly enough to establish the kind of treatment system needed to reverse the crisis. A White House report last fall put the cost to the country of the overdose epidemic at more than $500 billion a year. By Geoff Mulvihill. SENT: 865 words, photos. Editors: Note Michigan material.
AROUND THE STATE:
ARTISTIC INNOVATION
MARQUETTE, Mich. — A group of artists has formed in northern Michigan with an interest in growing the area's creative class and drawing creative industries to the Upper Peninsula. Evolve MQT is supported by the Marquette Chamber of Commerce, The Mining Journal reported. SENT: 290 words.
EXCHANGE-CHICKEN RULE CHANGES
PORT HURON, Mich. — A Michigan10-year-old girl has inspired change in her township. Avery Baker wrote a letter to Clay Township last year because she wanted to raise a few chickens, but her family's Harsens Island property was half an acre shy of the minimum allowed. The township is now tweaking its rules for recreational animals. An ordinance change was approved by the planning commission in February. By Jackie Smith, The Times Herald. SENT IN ADVANCE: 747 words.
EXCHANGE-HIVE HEAVEN
DETROIT — A growing number of people in Detroit and around the globe are cultivating urban beehives as part of social missions and small businesses. It's a trend that has prompted cities to lift beekeeping limitations and inspired entrepreneurs to sell beekeeping starter kits and the bees' honey.. By Frank Witsil, Detroit Free Press. SENT IN ADVANCE: 1,609 words.
IN BRIEF:
— DEPUTY AGRICULTURE DIRECTOR — Ken McFarlane has been named chief deputy director of the Michigan Department of Agriculture and Rural Development.
— IRON BELLE TRAIL — Nearly 30 projects in 18 Michigan counties will share $515,000 in funding for work on the Iron Belle Trail .
SPORTS:
BKC--NCAA-LEGIT LOYOLA
ATLANTA — Loyola-Chicago is much more than just the feel-good story of the NCAA Tournament. The Ramblers can play, and they showed how capable they are with a convincing rout of Kansas State in the Elite Eight. The nation's hottest team in the tournament is heading to the Final Four, and Loyola's dominating performance in its latest victory is the strongest evidence yet that the Ramblers belong. By Charles Odum. SENT: 560 words, photos.
Also:
— BKO--PLAYER COLLAPSES: An NBA G League player for the Grand Rapids Drive who collapsed on the court near the end of a game with the Long Island Nets is currently under care by doctors. SENT: 140 words.
___
If you have stories of regional or statewide interest, please email them to apmichigan@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.