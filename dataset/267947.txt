Weather through the first few months of 2018 has been ideal for asparagus production.
The Caborca deal in Mexico was winding down the last week of March; California production goes into May; Michigan growers typically begin harvesting in late April or early May; and production is year-round in Peru.
Companies that source from each location reported cooperation from Mother Nature.
“We did have some dormancy, and now we’re experiencing a nice little chunk of moisture here for the spring, and I believe we’re going to see a good crop out of the acreage that is left in the Stockton delta,” said James Paul, director of sales and marketing for Stockton, Calif.-based Greg Paul Produce Sales. “I think the weather’s going to provide some really awesome yields and great volume.”
He said demand for California asparagus would likely increase in coming weeks as volumes from Mexico decreased.
Product from Mexico is typically less expensive given the lower production costs there, particularly since asparagus remains an incredibly labor-intensive crop.
The U.S. Department of Agriculture reported March 30 that 11-pound cartons or crates of bunched large asparagus were mostly $20.75-22.75.
 
Michigan
Growers in Michigan were also optimistic about the 2018 season.
“This year is shaping up to be a great year due to very cooperative weather,” said Trish Taylor, account manager for Sparta-based Riveridge Produce Marketing. “We continue to have cool days with a slow, gradual warm-up, creating ideal conditions to kick off the growing season.”
She noted that buzz around local produce has boosted interest in fresh product.
“Fresh asparagus demand continues to grow after years of much of the Michigan product going to processing,” Taylor said. “As more consumers desire fresh produce that is locally grown, we expect to continue to see an increase in growth for fresh.”
John Bakker, executive director of the Dewitt-based Michigan Asparagus Advisory Board, also shared a positive outlook on the season.
He noted the winter has been a bit unusual because the majority of the snow cover was gone early in February.
“Anytime the snow leaves, the ground will start warming up, and if it gets to high 40s, low 50s, we’ll get bud break and we’ll start getting some growth, but so far the ground has remained cold despite the fact that we’ve not had widespread snow cover,” Bakker said. “As of today, going into the first of April, we’re going to be right where we normally would be.”
 
Mexico and Peru
Importers said the Caborca region in Mexico enjoyed a strong season before finishing up at the end of March.
“It was warmer than normal in January, which enabled fields to start 2-3 weeks earlier than normal,” said Don Hessel, vice president of grower relations for Los Angeles-based Progressive Produce. “But now in March the weather has cooled and Washington (state) season will start normally around April 15-20.”
He reported strong demand so far in 2018.
“Weather has really helped with production and quality of asparagus in the first (three) months of the year, enabling a lot of retailers to promote asparagus and restaurants easily incorporate it into their menus,” Hessel said. “While quality and pricing has enabled many customers to enjoy asparagus in February and March, the low prices have had a negative effect on grower profitability.”
Bakker also noted that prices have been very low this winter, starting around early February.
The USDA reported March 30 that prices for 11 pounds of bunched large asparagus crossing from Mexico were mostly $16.75, up from prices of mostly $9.75-10.75 in mid-February.
Hessel said volumes this year could be down from Peru but up from Mexico.
“According to the growers we have talked to and worked with, Peru asparagus volume could be down another 5% to 8% this year,” Hessel said. “There is some acreage being planted but more being taken out. From Mexico production is higher this spring due to a change in harvesting.
“Growers in Obregon, Mexico, have been experimenting with harvesting in April (and) May and have seen very good results in production,” Hessel said. “Many growers are now moving significant amounts to harvest in the spring that in past years were harvested in the fall.”
He said Peru fields were expected to start in March but that main production is planned for September through December.