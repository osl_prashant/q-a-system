Bob Whitaker, chief science and technology officer for the Produce Marketing Association, moderates a session featuring Neal Gutterson, vice president for research for DuPont Pioneer, and Kristen Rainey, global procurement and resource utilization manager for Google.

NEW ORLEANS — Google has been tracking waste in its massive food program for employees and using the data to help the company adjust accordingly, and now Google is looking to take the system to the next level through artificial intelligence.
Kristen Rainey, global procurement and resource utilization manager for the company, spoke about those endeavors Oct. 19 at Fresh Summit.
The company partnered with LeanPath, which works with commercial kitchens to log food items that end up destined for compost or trash, to be able to track which items are being thrown away at each location and in what quantities.
“It’s really great to have all this data,” Rainey said.
It matters to Google when food is tossed because it is a labor issue as well as a waste issue, because the staff member who made the uneaten food could have been doing something else.
The company even uses the results of the system for quality control, to a limited extent. Photos show when expensive items are being wasted or when vegetables aren’t being cut correctly, for example, and those findings are communicated to kitchen personnel.
Despite the benefits of the system, Google has been working to improve it because manual classification can result to information being incomplete, vague or inaccurate, as food items are not always easy to identify precisely.
Having employees input that information for every transaction is also time-consuming, Rainey said.
The ideal situation would be automatic recognition of each food item by the computers. For that to happen, the system has to have data — thousands of photos of wasted food items with accurate labels.
To jumpstart that process, Google brought in a group of chefs who worked nonstop for several days to precisely label images of wasted food. The company is still working on the system, but the results are looking promising, Rainey said.
She challenged produce industry members to ask the following question as a takeaway: What onerous tasks do you see in your business that could be done better and more consistently by a machine?