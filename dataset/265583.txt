For years, we’ve been in the age of “bacon mania” where consumers can’t get enough of delicious cured pork belly and bacon-flavored foods. Consumers can’t get enough of the flavor!
But there’s a long history on America’s meat candy. Click below to see bacon’s rise to fame:
(Produced by: Alison Fulton, Farm Journal's PORK)