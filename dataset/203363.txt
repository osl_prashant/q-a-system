Every month, Produce Retailer magazine features some of the best merchandisers, managers and clerks helping make produce the most sought-after department in grocery retail.   
 
Editor Pamela Riemenschneider interviewed winners of the 2016 Retail Produce Manager Awards, sponsored by Dole Food Co. and administered by the United Fresh Produce Association.
 
Nominations for the 2017 Retail Produce Manager Awards are open through Jan. 6 at unitedfresh.org.
 
Amy Lincoln, Coborn's, Ramsey, Minn.
 
What do you love most about your job? What I love most about my job is coming up with new ideas for building displays, whether it's for a MN-grown display, BLT (bacon-lettuce-tomato) display, tropical display, something for Potato Lover's Month, Easter or any and all of the other holidays. I like the creativity, uniqueness and craftiness needed to build exciting displays.
 
What's your biggest challenge as a retail produce manager? What I think is the biggest challenge as a retail produce manager is keeping up with the growing diversity of demands that customers have. They want so many different things; they want fresh, local, organic, value-added, variety, good prices and more.
 
What has changed most in the produce department during your time in retail? The produce department is much more multi-faceted than ever before. Some examples of growth and change that I've seen is in organics and value-added, as well as new varieties of products including fruits and vegetables that people may not have been able to get in the past. The accessibility of all these things is much easier now.
 
What's your favorite fruit? Vegetable? As far as what my favorite fruit or vegetable is, I honestly like them all. I truly have not found a fruit or vegetable that I did not like.
 
What do you think are the most important things we can do to raise fruit and vegetable consumption? I think that the most important things we can do to raise fruit and vegetable consumption is to create more opportunities for education; helping people understand how to tell if something is ripe, how to prep it, use it, cook it, and the health benefits associated with it. As well as making sure that fresh produce is always accessible and continuing to encourage people young and old to try new things.