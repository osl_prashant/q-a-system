Each year QS World University, with Elsevier, ranks agriculture and forestry colleges. Rankings are based on academic reputation, employer reputation and research impact. This list is world-wide, so you’ll find many universities not located in the U.S. and if you don’t see your Alma matter try checking a little lower on the list here.

Wageningen University, Netherlands
University of California, Davis
Cornell University
Swedish University of Agricultural Sciences
University of California, Berkeley
University of Reading, United Kingdom
University of Wisconsin, Madison
Michigan State University
Purdue University
Agro, ParisTech, France
ETH Zurich, Swiss Federal Institute of Technology
University of British Columbia, Canada
Iowa State University
University of Tokeyo
Norwegian University of Life Sciences
China Agricultural University
University of Copenhagen
University of Guelph, Canada
The Australian National University
Texas A&M
University of Illinois, Urbana-Champaign
Massey University
Oregon State University
Pennsylvania State University
The University of Queensland

Top Ag Universities
.embed-container {position: relative; padding-bottom: 67%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}