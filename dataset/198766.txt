2017 will be here before we know it, and that's when you will face the full effects of the FDA's new rules regarding antibiotic stewardship in food-animal production.Under the FDA's Guidance for Industry 213, the labels for medically imporant antibiotics will no longer allow their use for enhancing performance. Also, the FDA's revised veterinary feed directive (VFD) rule will end over-the-counter purchases of medically important antibiotics used in feed. You will need to work through your veterinarian, with in a valid veterinarian-client-patient relationship (VCPR) and obtain VFD orders to use those products.
We want your opinions on how these changes will affect your business. Please answer these two critical questions, and we will summarize your answers in upcoming articles.
What do you see as the greatest challenge(s) in complying with the upcoming antibiotic rules?
What do you see as positive aspects of the rules as they take full effect beginning Jan. 1, 2017?
You can submit your anwers two ways - either through the "comments" feature on this website, or by sending me an e-mail atjmaday@farmjournal.com.
Thank you.
John Maday, editor,Bovine Veterinarian
jmaday@farmjournal.com