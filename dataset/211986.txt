Salinas, Calif.-based Moxxy Marketing has launched a website summarizing information presented at its 2017 Produce Packaging Seminar. 
The website, producepackagingseminar.com, contains resources, videos and downloadable content on produce packaging law and design that were presented to more than 100 people who attended one of the three seminars this spring, according to a news release.
 
Produce marketing veterans Karen Nardozza and Terry Feinberg brought together a team of industry professionals to deliver content along with speakers Chris Passarelli (Dickenson, Peatman & Fogarty), Dale Stern (Downey Brand LLP) and Thomas Hoops (Garlock Printing and Converting), the release said.
 
The speakers provided packaging tips and advice along with pitfalls to avoid, legal case studies, and real-world examples of award-winning designs. 
 
The seminar content covered a range of topics including:
value-added branding through packaging;
proprietary rights in packaging shape and design;
competition and intellectual property considerations;
trademarks and copyrights: use and infringement;
FDA, USDA and Canadian food labeling compliance;
regulated terms vs. unregulated terms; 
litigation risks of unsubstantiated labels;
materials selection to protect products and extend shelf life;
inks and printing processes;
sustainable packaging; 
what’s outside sells what’s inside; and
maintaining consistent brand look. 
The presentations, along with supporting documents and other resources, can be viewed for free at producepackagingseminar.com.