The oilseed sell-off continued in a big way last week as speculators smashed through the previous week’s record short in the soy complex.Combining Chicago-traded soybeans, soybean meal, and soybean oil, money managers set a new record bearish view of 156,123 futures and options contracts in the week ended May 30, according to data from the U.S. Commodity Futures Commission. The combined short was a then-record 106,436 contracts in the week prior.

When adding the grains to this mix – K.C. wheat, Minneapolis wheat, and CBOT corn and wheat – hedge funds now claim the second most bearish stance across all seven grains and oilseeds of 462,667 futures and options contracts. Corn single-handedly led this boost in pessimism from the grains side.
The combined grains and oilseeds stance in the week ended May 30 narrowly missed the all-time short of 464,376 contracts set back in the week ended April 25.

Most-active CBOT soybean futures crashed to the lowest level in nearly 14 months on May 30 as big global supplies, export competition with Brazil, and improving U.S. crop weather continued to weigh on the market.
Money managers extended their soybean short in the four-day week ended May 30 to 89,310 futures and options contracts from 62,355 in the week prior. Funds now hold their fourth most bearish position on the oilseed behind the three weeks spanning from May 13 to June 2 of 2015.

Selling also continued in CBOT soybean meal as funds widened their net short to 44,500 futures and options contracts from 35,461 in the previous week. The only other time specs were more bearish on meal was for seven weeks between Dec. 30, 2015 and March 1, 2016.

CBOT soybean oil underwent the biggest fund selloff in almost two months after spending five of the last six weeks seemingly working on an exit from bearish territory. But the new stance of 22,313 contracts short represents a decisive boost in doubtfulness compared with the previous week’s 8,621 contracts.

Sources suggest that in the three trading sessions since, speculators have continued to sell soybean oil but have been net buyers of both soybeans and soymeal, largely based on bargain-buying.
Grain Feeling Mixed
Despite some questionable U.S. planting weather, speculators simply cannot get behind a bullish corn story, increasing their bearish bets on the yellow grain in the week ended May 30. They now sit short 200,981 CBOT corn futures and options contracts, a respectable increase from the 176,503 in the week before.

Chicago wheat bets remained unchanged this week, as the new 113,760-contract net short is barely an expansion from the previous week’s 113,211 contracts. Funds slightly raised their bullish bets in K.C. wheat to 3,221 contracts from 2,166 in the week prior.
Money managers’ favorite grain or oilseed contract last week was Minneapolis-traded hard red spring wheat. They extended their modest long to 4,976 futures and options contracts from 2,983 in the week before, representing the most bullish view on the grain since the week ended March 7.
Since last Wednesday, funds have been slight net buyers of wheat. The U.S. Department of Agriculture’s crop progress after the market close last Tuesday showed mildly declining winter wheat conditions and spring wheat conditions well below what the market had expected.
Chicago and K.C. wheat futures were mixed late last week but Minneapolis wheat on Friday reached the highest spot price since mid-January.