Alaska Fish and Game denies hatchery on Baranof Island
Alaska Fish and Game denies hatchery on Baranof Island

The Associated Press

JUNEAU, Alaska




JUNEAU, Alaska (AP) — The Alaska Department of Fish and Game has announced that a proposal to open a salmon hatchery on Baranof Island has been rejected.
The Juneau Empire reports that the March 29 rejection ended a disagreement between Juneau businessman Dale Young and the residents of Baranof Warm Springs.
The springs are home to a small, seasonal community of about a dozen cabins at the southern tip of Baranof Island — known for its numerous hot springs and scenic waterfalls.
Jim Brennan, who has owned a cabin there since the 1950s, said the hatchery would have been "way out of whack with the scenery and recreational values of the bay."
Young could not be reached for comment.
___
Information from: Juneau (Alaska) Empire, http://www.juneauempire.com