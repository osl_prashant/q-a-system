CMI Orchards is launching a Season of Flavor promotion that includes the company's club variety apples.
 
The Season of Flavor promotion lineup includes Ambrosia, Kiku, Kanzi, Jazz and Pacific Rose, according to a news release.
 
The 10-week event, starting at the beginning of March, offers participating retailers incentives and prizes for promoting the branded apples with event signage and in-store displays, according to the release. The promotion features a different apple every two weeks, with 2,000 retail locations expected to participate.
 
Late winter through early spring is an important time for retailers to capture incremental sales with branded apples, Steve Lutz, vice president of marketing for CMI Orchards, Wenatchee, Wash., said in the release.
 
"The data reveals that retailers with the strongest performance in the overall apple category focus on extracting incremental sales by highlighting branded apples for their customers," Lutz said in the release.
 
With most of the branded apples priced at over $2 per pound, the Season of Flavor event allows retailers to grow category sales and experience bigger sales, he said.
 
"Supermarkets that can shift consumer purchases to higher-priced apples simultaneously drive the average category transaction," Lutz said in the release. "Promoting club apples at this time of year just makes good financial sense for retailers."
 
Along with retailer incentives and produce manager display contests, CMI has signage and point-of-sale materials for the promotion.