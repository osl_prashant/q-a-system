Though regional supermarkets and Wal-Mart boast by far the biggest share of retail sales, Shelby Report statistics show discounter Aldi is gaining market share in several Midwest markets. 
As of March, market share for Kansas/Missouri (Kansas City, Topeka, Wichita and Columbia) showed Aldi’s market share for its 45 stores in the region was 2.8% of total retail sales (all commodity value) of $10.27 billion, compared with 1.5% market share in 2016, according to the Shelby Report.
 
In the $13 billion  Missouri/Illinois/Indiana/Kentucky market (St. Louis, Springfield, Champaign, Terre Haute, Evansville, Paducah), Aldi’s market share with 78 stores increased from 2.4% in 2016 to 4.4% in 2017. 
 
Aldi’s market share also grew in the $10.88 billion Minnesota market (Minneapolis, St. Paul, Duluth), where the discounter’s 48 stores notched a 3.2% market share, up from 1.4% in 2016.
 
In the $18.79 billion Ohio/West Virginia/Kentucky (Cincinnati, Dayton, Columbus, Toledo, Huntington) market, Aldi’s 86 stores scored 2.9% of the market, up from a share of 1.6% in 2016, according to the Shelby Report.In some respects, the Midwest retail market may be insulated from trends that start on the East or West Coast, said Bruce Peterson, president of Peterson Insights Inc.
 
Yet Aldi and the arrival of Lidl in U.S. markets is starting to cause disruption in all parts of retail, including the Midwest.
 
Supermarkets are finding themselves trapped in the middle between the discounters and the emergence of digital shopping options.
 
“Aldi and Lidl are putting huge price pressure on the bottom side of the market, and people will have to start to invest in prices,” he said. On the other end of the market, the digital revolution — and the unknown effect of Amazon’s bid to purchase Whole Foods — suggest keen competition for the digitally savvy shopper, he said. Anthony Totta, director of marketing and business development for Talley Farms and long-time Kansas City resident, said that perhaps the biggest looming question is the potential effect of Amazon.
 
The firm is building a large distribution center in the region and if that facility is used for fresh, Totta said that would be a “game changer.” “People ordering food off of handheld devices will change the brick and mortar world from a fresh produce standpoint,” he said. 
 
Because consumers feel more comfortable seeing the produce before they buy, retailers should be playing up their perishable options.
 
Kansas/Missouri
 
The leader in the Kansas/Missouri market was Wal-Mart, which commanded a 32.9% market share for its 103 stores in March, up slightly from 32.5% share in 2016.
 
Second in the market was Associated Wholesale Grocers, a wholesaler supplying 236 stores in the region and boasting a 25.5% market share of total retail sales. AWG market share declined 1.3 points from 2016, when company’s market share was 26.8%, according to the Shelby Report. 
 
Dillons (a Kroger division), with 60 stores in the area, notched 15.6% of the market, no change from 2016.Hy-Vee, with 34 stores, recorded a market share of 10.2%, down from 10.3% in March 2016. SuperTarget, with nine stores, captured a retail market share of 2.3%, unchanged from last year.
 
The full list is below:
 
Kansas/Missouri Market Share
ACV = $10.27 Billion
Kansas City, Topeka, Wichita, Columbia
 
Retailer/wholesaler                          #Stores       %Market                     Change
Walmart                                             103               32.90%                     -0.40
AWG*                                               236               25.50%                     -1.30
Ball’s                                                   27                 6.00%                     -0.40
Cosentino’s                                         26                  6.40%                    +0.10
Price Chopper                                     14                  3.90%                    Newly Added
Dillons (Kroger)                                 60                 15.60%                     N/C
Hy-Vee                                                34                 10.20%                     -0.10
Aldi                                                     45                    2.80%                    +1.30
SuperTarget                                          9                     2.30%                     N/C
Save-A-Lot                                        25                      1.40%                     -0.10
United Natural*                                 16                      1.40%                     +0.40
SpartanNash*                                      5                       1.10%                      N/C
Whole Foods                                       4                        1.10%                     -0.20
Source: Shelby Report