Kingsville, Ontario-based Double Diamond Farms is adding a 62-acre facility in Leamington.
The expansion will significantly increase production capacity, according to a news release. The project includes a warehouse and packaging facility.
“Growing consumer demand in the organic category, as well as in multiple specialty items, are really what’s driving this expansion,” Chris Mastronardi, CEO of Double Diamond, said in the release. “We have an extensive selection of high-quality, superior flavored items that have really been a hit with our customers.”
Sustainable practices at the facility will include bumblebees for pollination, energy curtains for insulation, and the collection and use of carbon dioxide for growth.
The facility will also have automation and technology including grow lights, according to the release.
Double Diamond grows tomatoes, peppers, cucumbers, eggplants and other items.