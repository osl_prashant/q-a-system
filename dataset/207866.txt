California Giant Berry Farms, Watsonville, Calif., raised more than $12,000 for local charities at its annual summer barbecue July 18.The staff served more than 1,000 lunches, according to a news release.
“We are humbled by just how supportive the community of Watsonville is when 1,000 people all take the time to share in the opportunity to give back,” CEO and president Bill Moncovich said in the release.
Pajaro Valley Loaves and Fishes, a Watsonville food pantry and kitchen, received 200 meals from the fundraiser that individuals donated, according to the release.
Several companies donated money to help with fundraiser costs including: Markon Cooperative, Watsonville Coast Produce, Custom Produce, Pacific Cookie Company, Titan and Norcal/Planasa.
According to the release, eight nonprofits will split proceeds, which the company owners are matching.
The eight recipients are:
CASA of Santa Cruz County;
Digital Nest;
Grind out Hunger;
Jacob’s Heart;
Monarch Services;
Pajaro Valley Shelter Services;
Teen Kitchen Project; and
Watsonville Senior Center.