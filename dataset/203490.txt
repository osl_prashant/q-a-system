St. Louis likely trails other markets in terms of consumption of organic produce, but consumption is increasing, fruit and vegetable vendors there say.
"We believe it lags the national averages," said Dominic Greene, vice president of operations and sales manager with wholesaler United Food and Produce Co. Inc.
Organics' popularity has ebbed and flowed in St. Louis, and it's on an upswing now, Greene said.
This time, it may sustain that growth, Greene said.
"There's more of it around, more items available in the stores around St. Louis," he said.
Greene said the inaugural Organic Produce Summit in July 2016 in Monterey, Calif., was a sign of growing interest nationwide in the organic category.
"It obviously lagged behind the summer organic program here, but judging from all the literature in the industry, I really think, with the capacity we've seen across the nation and all the news alerts of people committing acreage to organic, we think it's probably here to stay," Greene said.
The category has risen and fallen over the years, but now, young adults are leading a sustained sales surge, and that likely won't go away, Greene said.
"It is being promoted in St. Louis stores," he said.
That is a change from the past, Greene said.
"When you see ads on organic items in the past, you wouldn't see a promotion for, say, organic grape tomatoes; now, you do," he said. "We believe prices have evened out enough for retailers to promote organics in ads."
The category still doesn't attract the interest of all produce dealers in St. Louis.
"It's a growing category, from everything I hear, but we have enough just trying to keep up with our own items we deal with on a regular basis," said Dan Pupillo Jr., president of wholesaler Midwest Best Produce Co. Inc.
Some wholesalers will offer organic product for special orders.
"We don't sell a lot of organic, but it's definitely picking up for everybody," said Dale Vaccaro, president and owner of Vaccaro & Sons Produce Co., a St. Louis wholesaler.
Vaccaro said organic sales in St. Louis still lag behind other markets.
"But there's still a decent demand for organic here in St. Louis," he said.
St. Louis-based Sunfarm Foodservice Inc. supplies organics to "a few customers," said R.J. Barrios, buyer.
"We keep a small supply of organic," he said.
The company has one a buyer designated to handle the organic side, Barrios said.
Retailers are hearing the cries for more organics from shoppers.
"It's up and continues to grow," said Steve Duello, produce director at Dierbergs Markets in Chesterfield, Mo.
Organics can be challenging, Duello noted.
"It's a very difficult commodity to manage, in that, relatively speaking, it's still a small percentage of your total produce sales, but it's growing more aggressively than most commodities in produce," he said.
Dierbergs works to "put a lot of emphasis on it storewide," but it's difficult because the vast majority of customers are still buying conventional, Duello said.
"Across the country, there's a battle of how much space to give it based on the fact that it's a small percent of total produce sales, but you can't ignore it; it's still a big deal for us," he said.
Organic produce sales are increasing at St. Louis chain Straub's Markets, said Greg Leher, produce director.
"We're carrying more and more all the time, and customers are very receptive to it and it's working well," he said.
Organic items are becoming "more readily available," which is fueling the growth, he said.
More people in St. Louis are "getting in tune" with organics, said Steven Mayer, vice president of produce at Schnuck Markets Inc., a St. Louis-based retail chain that operates about 60 stores in the metro area.
The so-called "price premium" for organic items doesn't seem to be a major barrier, Mayer said.
"It does not seem to be income-specific; people are willing to experiment, and organic pricing is getting closer to conventional pricing â€“ certainly less expensive than it was 10 years ago," he said.