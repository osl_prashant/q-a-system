California is forecasting 70 million cartons of navel oranges in 2017-18, of which 68 million will come out of the Central Valley, according to the latest estimate from the California Department of Food and Agriculture.
As September ended, the markets seemed to be reflecting the anticipated volume numbers.
As of Oct. 3, 15-kilogram containers of navel oranges from Chile entering through Philadelphia were mostly $28 for sizes 40s to 56s, according to the U.S. Department of Agriculture.
A year earlier, prices for 15-kilogram containers from Chile were $20-22 for sizes 40s to 56s.
The crop will be light, but that could be good for grower returns, said Vincent LoBue, manager of Porterville, Calif.-based California Fresh Citrus Co.
“It’s a light crop, but we all tend to make more money on light crops and don’t expect to see anybody lowering the price just for the sake of moving fruit,” he said.
The varieties forecast in this report include conventional, organic and specialty navel oranges, including pigmented varieties, such as cara cara and blood oranges.
Survey data indicated a fruit set per tree of 273, below the five-year average of 348. The average Sept. 1 diameter was 2.34 inches, above the five-year average of 2.24 inches.
“There’s a lighter fruit set on fewer acres — that’s what’s driving the smaller crop,” said Bob Blakely, vice president of the Exeter-based California Citrus Mutual.
Acreage is 115,000 this year, down from 120,000 bearing acres a year ago and 135,000 from 2006-09.
The other side of the forecast is stronger pricing, Blakely said.
“We’re expecting a really strong market,” he said. “The crop will be down, and it was down the year before.”
California also appears to have broken a yearslong pattern of drought, although few growers declared the dry spell gone for good.
The drought cost growers across the state some acreage, and the survey took that into account in this year’s forecast, Blakely said.
“That was still left over from the trees removed from the drought,” he said.
“We had some storms during bloom period in spring and had some pretty heavy rainstorms coming through,” Blakely said. “Depending on where the grove was at, that could have contributed to our lighter fruit set, as well. We’re seeing a lot of variability grove to grove, which would support our theory of sporadic weather damage.”
It also has rendered the upcoming crop volume difficult to estimate, Blakely said.
“Everybody says it’s down, but how much down is questionable,” he said.
Fruit sizing should be good, Blakely said.
“We’re going to start with a good range of sizes,” he said.
A weeklong gap between the end of the valencia crop and the start of peak navel season will give “offshore fruit a chance to get out of the pipeline,” Blakely said.
“All of that is encouraging and leads us to believe we’re going to get off to a really strong start,” he said.
The first California navels were expected to ship by the second half of October, slightly later than they have started the last two years, Blakely said.
“By early November, you’ll see some pretty good supplies,” he said.
The drought may not be done, but there was plenty of rainfall to help this year’s crop, Blakely said.
“They certainly had good water this year. It’s hard to tell if the lighter crop this year was carryover from drought,” he said. “Now the trees have recovered and some groves look as good as they have in years. You have every reason to expect we’d be having an average to bigger crop next year. Barring a freeze, we’ve made a good recovery from the drought.”
Optimism reigned, at least for the short-term, as the navel season approached, said Randy Jacobson, sales manager with Orange Cove, Calif.-based Cecelia Packing Corp.
“We’ll have ample irrigation water this year,” he said. “Everybody is optimistic about water, at least for now, but you don’t undo four or five years of drought in one wet season.”