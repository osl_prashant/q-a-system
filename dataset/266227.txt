BC-Wheat KX
BC-Wheat KX

The Associated Press

CHICAGO




CHICAGO (AP) — Winter Wheat futures on the Chicago Board of Trade Wed:
OpenHighLowSettleChg.WHEAT5,000 bu minimum; cents per bushelMay466½467457¾461—3Jul486¼486½477½480—3½Sep503505496½498¾—3¼Dec527528520¼522½—2½Mar536542¼535½537¼—2¼May546½546½544½544½—2½Jul548551½547548¾—2¾Sep559¾—2¾Dec575—3¾Mar581—3¼May581—3¼Jul562—3¼Est. sales 43,542.  Tue.'s sales 48,217Tue.'s open int 280,634,  up 608