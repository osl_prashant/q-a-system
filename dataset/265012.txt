Outside money or funds can be a driver of price in the grain and livestock market: they can either drive prices up or down.

To Mike Florez of Florez Trading, the motivation behind the driving force of the funds is a mystery or a black hole.

“Money chases money,” he said. “[For momentum traders] as long as the market is going their way, they continue to pile on, whatever the direction is.”

This fall, there was a lot of love for livestock markets. Now, that attention has shifted toward grains.

“One of the keys to watching them is when open interest really builds in the final days of the move—it kind of signals the end of that trend,” he told U.S. Farm Report’s Tyne Morgan.

Hear why the funds are chasing the momentum of the grain markets on AgDay above.