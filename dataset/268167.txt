Man fatally trampled at Otter Tail County farm
Man fatally trampled at Otter Tail County farm

The Associated Press

HEWITT, Minn.




HEWITT, Minn. (AP) — Sheriff's authorities say an 85-year-old man has died after he was trampled by a cow at a farm in Otter Tail County.
Officials say Delbert Horn, of rural Hewitt, was trying to tag a calf Sunday when he was trampled.
Emergency responders were unable to revive Horn.