BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Thursday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMay                              121.20  Up    .20May      119.35  119.65  118.15  119.00  Up    .15Jul      121.45  121.80  120.35  121.20  Up    .20Sep      123.90  124.05  122.60  123.40  Up    .15Dec      127.25  127.25  126.00  126.80  Up    .15Mar      130.75  130.75  129.55  130.25  Up    .15May      132.20  132.60  131.90  132.45  Up    .05Jul      134.30  134.50  133.95  134.50  Up    .05Sep      136.15  136.30  135.75  136.30  Up    .05Dec      138.90  139.00  138.45  139.00  Up    .05Mar      141.05  141.65  141.05  141.65  Up    .05May      142.70  143.40  142.70  143.40  Up    .05Jul      144.30  145.10  144.30  145.10  Up    .05Sep      145.70  146.65  145.70  146.65  Up    .05Dec                              149.05  Up    .05