The Latest: Trump says he'll 'make it up' to farmers
The Latest: Trump says he'll 'make it up' to farmers

The Associated Press

WASHINGTON




WASHINGTON (AP) — The Latest on U.S.-China trade relations (all times local):
12:10 p.m.
President Donald Trump says he's going to "make it up" to farmers who would be impacted by China's proposed retaliatory tariffs on U.S. exports.
Trump says: "It's not nice when they hit the farmers specifically because they think that hits me."
Trump is promising during a Cabinet meeting that American farmers "will be better off than they ever were" over the last eight years, despite China's threat to place duties on pork and soybean imports from the U.S.
China is threatening the tariffs in response to Trump moving to enact protectionist measures as punishment for Chinese theft of U.S. intellectual property. The U.S. bought more than $500 billion in goods from China last year and now is planning or considering penalties on some $150 billion of those imports.
___
6:45 a.m.
President Donald Trump is complaining about "STUPID TRADE" with China, saying that Chinese tariffs for U.S. cars are much higher than U.S. tariffs for Chinese automobiles.
In a tweet on Monday morning Trump says that when a Chinese-made vehicle is sent to the U.S., the tariff is only 2.5 percent, while American cars exported to China are slapped with a 25 percent tariff.
Trump asks, "Does that sound like free or fair trade." His says, "No, it sounds like STUPID TRADE."
China charges total duties of 25 percent on most imported cars — a 10 percent customs tariff plus a 15 percent auto tax. Since December 2016, Beijing also has charged an additional 10 percent on "super-luxury" vehicles priced above 1.3 million yuan ($200,000).
Investors across the globe are bracing for uncertain markets over an escalating trade dispute between the U.S. and China. Trump has called on Beijing to ease trade barriers "because it is the right thing to do."
__
12:15 a.m.
Investors across the globe are bracing for uncertain markets as President Donald Trump tries to downplay fears of a trade dispute between the U.S. and China.
Trump is suggesting that Beijing will ease trade barriers "because it is the right thing to do" and that the economic superpowers can settle the escalating conflict.
But as Trump tries to project confidence that a dispute that has rattled financial markets, consumers and businesses can be soon resolved, his top economic advisers are offering mixed messages as to the best approach with China.
Beijing has threatened to retaliate if Washington follows through with its proposed tariffs.
Trump hasn't explained why, amid a week of economic saber-rattling between the two countries that shook global markets, he feels confident a deal can be made.