By Jim Noel
November often turns off cold and cloudy in Ohio, but this year there's a different mix of weather expected. This year, look for both temperatures and rainfall to be above normal.
It appears we will see a series of weak weather systems the next two weeks. Most of the state will be at or below normal rainfall into the start of November. However, the far northern tier of Ohio will see normal to above normal rainfall putting pressure on wet conditions and challenge harvest in the far north and northeast part of the state.
After a start to November not really wet except far north, most climate models indicate a return to a more active weather pattern for middle and later November. Along with the wetter pattern will come temperatures several degrees above normal.
Most areas will see the delayed freeze that was forecast for many months come to pass in early November, a few weeks behind schedule.
The outlook for winter calls for near normal temperatures and slightly above normal precipitation.
Early indications for next planting season in spring call for near normal temperatures and slightly above normal precipitation.
Over the next two weeks, as show on the image, the NOAA/NWS/Ohio River Forecast Center 16-day weather models mean rainfall is around 1 inch in southern Ohio to 3+ inches in far northern Ohio.