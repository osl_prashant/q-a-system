Smoke taint worries southern Oregon winemakers
Smoke taint worries southern Oregon winemakers

By GREG STILESMail Tribune
The Associated Press

MEDFORD, Ore.




MEDFORD, Ore. (AP) — You may remember the marketing phrase: "We will sell no wine before its time."
For some vintages, that line of thinking remains timeless.
When it comes to vintages impaired by smoke taint, however, it's best to move them quickly and consume the wine before the taint fully manifests itself.
Northwest grape growers repeatedly have seen smoke from wildfires envelop the region in recent years, raising legitimate concerns about the effects of smoke on the finished product.
Elizabeth Tomasino of Oregon State University's Oregon Wine Research Institute had disconcerting news about smoke taint for growers and winemakers during a recent symposium.
"To the disappointment of a lot of people, there is currently no way to get rid of it," Tomasino said in an interview this week. "Any single place that experienced smoke during the growing season will have smoke taint to some extent."
The issue began to percolate in discussions about three years ago.
"It's now in the top half to top third of growers' worries," she said. "People ask, 'What can we do?' And I have to say, 'I'm sorry, we don't have anything.' There are things you can do for almost anything else, but this one is out of your control."
While ash might be bothersome when it builds up on your car, it doesn't pose issues in the vineyard.
"A lot of people think when you have ash on the grapes that you have a problem," Tomasino said. "It's actually not."
Instead, aroma compounds are absorbed through the leaves and grape skins and make their way into the berries, she said.
Some varietals are more susceptible to taint and some are more resistant. Pinot noir — synonymous with Oregon beyond the state boundaries — isn't among the resistant.
"Pinot, merlot, chardonnay, can't handle a lot of oak, so they are going to have some taint problems," Tomasino said. "Wines that can typically handle a lot of oak can handle a decent amount of smoke, because they aren't overly complex."
Among the more resistant grapes, she said, are cabernet sauvignon and syrah.
"There hasn't been a lot of white grape research," she said.
Wildfires at the beginning of the century created hardships for Australian wineries, and over the past decade Western wine regions have seen more than their share of fire.
"Basically, the entire West Coast is having smoke taint problems this year," Tomasino said.
Liz Wan, an Applegate Valley wine consultant, said the unknowns can be haunting.
"The most frightening thing is not only knowing what it can do, but you don't know when it manifests once the precursors are there," Wan said.
Early tests aren't necessarily indicative of potential pitfalls, said Wan, who is also the assistant winemaker at Serra Vineyards.
"It can be nerve-racking because it's not intuitive," she said. "Most of the time, if your wine is good going into the bottle, it's going to stay good."
When smoke taint is in question, there are few satisfactory options. Wan suggests doing three pressings in separate tanks: Gentle, medium and hard.
"To get out ahead of the smoke taint, you test the lots separately for certain compounds before they create smoke taint," she said. "The compounds might be in there individually, but during fermentation, the compounds warm up, divide or coalesce."
It's one thing to test for the telltale signs of taint, it's another to avoid the fallout.
Tomasino suggests adding a little oxygen to a small amount of the juice to test its direction.
"Don't try to have fast fermentation, but have a complex wine that can help incorporate the smoke content without it being so dominant," Tomasino said.
Once it's in the bottle, there is no time to dally.
"You have to sell the wine young and get it out the door," Tomasino said. "Because the smoke taint gets worse over time."
The good news is that Oregon State, University of California-Davis and Washington State University have reached the second round in obtaining a multi-million-dollar, USDA specialty crop research grant that would fund research over a three- to five-year period.