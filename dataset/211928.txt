Hudson River Fruit Distributors, Milton, N.Y., has a new high-graphic apple display bin this season. 
The bin highlights locally-grown and family-farmed apples on the front and other elements showcase a quote from company co-founder Harold Albinder and the tradition of New York state apples.
 
“We are very excited to launch this marketing tool to help our customers bring the farm to their stores this fall,” Pat Ferrara, sales director of Hudson River Fruit Distributors, said in a news release. “The display bin can be used to display local loose apples, totes, poly bags or our 2-pound Lil Chief bags. We are excited to share our story and products with customers at store level.”
 
The fourth-generation, family owned grower-shipper ships more than 2 million boxes of more than 20 apple varieties, according to the release. The company has been in business for more than 54 years.