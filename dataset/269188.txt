Some Nebraska advocates criticize SNAP changes in Farm Bill
Some Nebraska advocates criticize SNAP changes in Farm Bill

The Associated Press

LINCOLN, Neb.




LINCOLN, Neb. (AP) — Advocates for low-income families in Nebraska are criticizing proposed changes to the federal Farm Bill that would require food-stamp recipients to find a job or attend job-training classes.
Advocates say an estimated 176,000 Nebraska residents receive food assistance from the Supplemental Nutrition Assistance Program.
They say more than 75 percent of the state's recipients are children, and 27 percent are in families which have people with disabilities. Additionally, they say recipients most commonly work in service, office or production jobs with wages below the state average.
James Goddard of Nebraska Appleseed says the bill would result in drastic cuts to SNAP and take food away from thousands who struggle to afford basic meals.
Congressional Republicans who drafted the legislation say it's designed to help people recover from hard times.