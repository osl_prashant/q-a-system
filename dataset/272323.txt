The Latest: Kansas House fails to debate cutting sales tax
The Latest: Kansas House fails to debate cutting sales tax

The Associated Press

TOPEKA, Kan.




TOPEKA, Kan. (AP) — The Latest on the Kansas Legislature's debate on school funding, the state budget and cutting taxes (all times local):
1:43 p.m.
Kansas lawmakers who want to lower the state's 6.5 percent sales tax have failed several times to get the House to debate the idea.
The House debated a bill Saturday to allow the Kansas State Fair to keep sales tax revenues generated on its grounds. The fair would then use those revenues to pay off bonds for improvements.
Democratic Rep. Tim Hodge of North Newton and Republican Rep. John Whitmer offered amendments to lower the sales tax on groceries.
Hodge would have offset his tax cut by raising income taxes for wealthy Kansans. Whitmer would have closed sales tax exemptions.
The House Rules Committee declared both amendments out of order.
Whitmer also proposed lowering the entire sales tax to 6.15 percent. That amendment also was ruled out of order.
___
1:04 p.m.
Kansas legislators have advanced a bill that fixes a flaw in a recently enacted education funding law that would cost public schools $80 million.
The House gave first-round approval to the bill Saturday on a voice vote. Members expected to take a final vote later Saturday to determine whether the measure goes to the Senate.
The education law enacted earlier this month was designed to phase in a $534 million funding increase over five years. The Kansas Supreme Court ruled in October that the current funding of more than $4 billion a year is inadequate.
The law set a minimum for local property tax revenues to be raised for schools and counted those dollars toward the state's total aid. The technical calculation inadvertently replaced state dollars with local dollars.
___
11:28 a.m.
The Kansas House has rejected a Democratic proposal to nearly double the amount of new spending on public schools provided by an education funding law enacted earlier this month.
The vote Saturday in the GOP-controlled House was 78-42 against Democrats' plan to phase in a nearly $1 billion increase over five years.
The House was debating a bill to fix a flaw in the new education funding law that inadvertently cost public schools $80 million. The new law was meant to phase in a $534 million increase in education funding over five years.
Democrats had argued the law would not comply with a Kansas Supreme Court in October that the current education funding of more than $4 billion a year is not adequate under the state constitution.
___
9:24 a.m.
The Kansas House has approved a bill that would add millions of dollars of new spending to $16 billion-plus state budgets approved last year for the state's current fiscal year and the next one beginning in July.
The vote Saturday was 92-24 and sends the bill to the Senate.
The measure includes nearly $8 million to provide pay raises for employees in the state's court system and restores $12 million in past cuts in the state's higher education system.
The Senate expects to debate its own budget legislation next week. The final version will be drafted by negotiators for the two chambers.
Top Republicans also are pursuing tax cuts to offset increases in state taxes for some Kansans caused by changes in federal tax laws last year.