Deputy Secretary | Undersecretary positions






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






Key USDA positions are set to be announced. Sources signal the possible new spots include:

Deputy Secretary: Steve Censky, currently CEO of the American Soybean Association, who worked at USDA in the Reagan and George H. Bush administrations.
Undersecretary for the new Farm Production and Conservation: Bill Northey, Iowa’s agriculture commissioner. The position includes overseeing the Farm Service Agency, Risk Management Agency and the Natural Resources Conservation Service.
Undersecretary for Trade and Foreign Agricultural Affairs: Ted McKinney, Indiana’s state agriculture director. He is close to Vice President Mike Pence and is a former director of global corporate affairs for Elanco Animal Health.

The recently announced teaming of NRCS with FSA and RMA makes a lot of sense, said agricultural consultant Randy Russell, for three reasons: “One, there is no functional overlap between the Forest Service and NRCS, where they used to be together; Two, for years now, counties have co-located FSA and NRCS offices, so this just means Washington is catching up with reality; and Three, Grouping the three agencies together makes good sense from a farmer services perspective.”
Other top USDA spots are also close to being finalized, including possibly Greg Ibach as Undersecretary of Marketing and Regulatory Programs (APHIS, GIPSA) — he is currently Director of Nebraska Department of Agriculture), and reportedly Sam Clovis as Undersecretary for Research, Education, and Economics.


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.