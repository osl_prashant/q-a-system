Crop calls
Corn: Fractionally to 1 cent firmer
Soybeans: 1 to 3 cents lower
Winter wheat: 1 to 3 cents lower
Spring wheat: Fractionally to 1 cent firmer
Early buying in the soybean market overnight gave way to fresh selling as traders' focus remains on ample global supplies and the record South American crop. Corn benefited from light short-covering overnight and sharp weakness in the U.S. dollar index. Winter wheat futures were lower on forecasts for rains across the HRW Wheat Belt, while spring wheat firmed amid spreading. 
 
Livestock calls
Cattle: Lower
Hogs: Lower
While Friday's Cattle on Feed Report reflected a current feedlot situation and nearby live cattle remain at a sharp discount to the cash market, cattle futures are vulnerable to pressure after China and several other countries lifted their ban on imports of meat from Brazil over the weekend. Hog futures are vulnerable to spillover from cattle as well as face pressure from a steady to softer tone in the cash hog market.