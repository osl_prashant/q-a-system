Earlier this week the Chinese government announced $50 billion in proposed retaliation tariffs against President Trump’s menu of tariffs aimed at ending intellectual property theft. While the media has been focused on soybeans and pork, other agriculture commodities are targeted to be taxed too.  Did you know corn, cotton, sorghum, wheat and beef are on the list?
[See the full list of proposed tariffs here.]
The reason they haven’t been at the forefront of the discussion is that we don’t do a ton of business with them on those products.
“We hardly do any business with them on that stuff,” explains Andy Shissler of S&W Trading. “It’s really, really small dollars.”
According to Shissler the amount of money spent on the other agriculture commodities listed, pales in comparison to soybeans and that’s why the trade isn’t concerned about them.
“The soybean tariff is the scary one because they buy a lot of from us,” he says.
This year China is also poised to buy a fair amount of cotton from the U.S., says John Payne author of the This Week in Grain blog.
“China is 25% of our export market for cotton for this marketing year,” he says. “If they had the luxury to back off on cotton imports I’m sure they would do it.  They are going to be in here because they have to be.”
According to Payne, the cotton markets could suffer from the tariffs.
“For right now I do look for the cotton markets to take it on the chin a little bit,” he says. “Tariffs on cotton are going to be much more detrimental than anything else they are going to want to do.”
Corn is another grain on China’s hit list. Both analysts agree China imports so little corn from the U.S. that those tariffs won’t matter much.
“Our markets for DDGs and corn are so diversified I don’t think [the tariffs] even make a dent in the shorter run,” Payne says.
Will they actually do it?
It’s important to keep in mind these tariffs are proposed, not implemented. Shissler isn’t sure China will actually go through with the tariffs.
“I just don't know how a tariff would work, I really don't,” he says. “If they put a 25% tariff on soybeans and they still have to buy them, what does that do? From an end user standpoint we wouldn't want to pay 25% more for the two thirds that we use.”
Like other analysts, Shissler thinks the tariff talk is a negotiation tactic.
“I think the [administration] wanted to negotiate or whatever and then they didn't hear much back, so they do this and have probably gotten a response,” he says adding that President Trumps goal to have $100 billion trade deficit is “impossible.”