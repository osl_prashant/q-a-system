Marijuana dispute pits county against state of Oregon
Marijuana dispute pits county against state of Oregon

By ANDREW SELSKYAssociated Press
The Associated Press

SALEM, Ore.




SALEM, Ore. (AP) — Officials in an Oregon county who have tried to restrict commercial marijuana production sued the state in federal court, asserting state laws that made pot legal are pre-empted by federal law that criminalize it.
The lawsuit filed Tuesday in U.S. District Court escalated a long-running battle between the state and the Josephine County Board of Commissioners.
The panel says pot farms are a nuisance. The county is in a prime marijuana-producing region of southern Oregon.
Voters in the state legalized marijuana with a 2014 ballot measure, prompting a "green rush" as pot entrepreneurs set up shop in the fertile, rainy mountainous area.
County Commissioner Dan DeYoung has said rural residents, many of them retirees, are fed up with the proliferating farms in areas zoned as rural residential.
"The good people are leaving and the marijuana people are staying," DeYoung said, according to the Daily Courier newspaper.
The commission in December tried to ban commercial pot farming on rural residential lots of five acres or less and to drastically reduce the size of some larger grow sites.
But the state Land Use Board of Appeals later put the restrictions on hold, saying the county failed to properly notify land owners.
Pete Gendron, a marijuana grower in the county and president of the Oregon SunGrowers' Guild advocacy group, said the growers have invested large sums to start operations and were shocked when the county tried to restrict them.
One grower had a letter from the county dating back a year or more stating that cannabis cultivation was farm use and was allowed, Gendron said.
"He invested a half-million dollars in the county," Gendron said. "He would not have made those investments if not for those assurances."
The lawsuit by the commission contends the state cannot dictate marijuana regulations over county restrictions because marijuana remains illegal under the federal Controlled Substances Act.
"Any person in any state who possesses, distributes, or manufactures marijuana for medical or recreational purposes, or attempts or conspires to do so, is committing a federal crime," Wally Hicks, a lawyer for the county, wrote in the lawsuit that names state Attorney General Ellen Rosenblum as a defendant, along with the state.
Rosenblum's spokeswoman Kristina Edmunson said she can't comment on pending litigation but noted in an email: "We will defend the state laws of Oregon related to marijuana."
The lawsuit calls on a federal court in Medford to declare that two ballot measures in 1998 and 2014 that legalized medical and recreational marijuana, respectively, are pre-empted by federal law.
U.S. Attorney General Jeff Sessions, who famously declared that "good people don't smoke marijuana," recently gave U.S. prosecutors more leeway to pursue federal anti-marijuana laws in states that have legalized pot.
___
Follow Andrew Selsky on Twitter at https://twitter.com/andrewselsky
Find complete AP marijuana coverage here: https://apnews.com/tag/LegalMarijuana