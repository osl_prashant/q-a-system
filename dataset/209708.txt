There have been various dietary recommendations promoted by government entities as well as nonprofit consumer groups in numerous countries here and abroad. Virtually all of them, in one form or another, echo a singular message: People need to change their diets.
People in virtually every Western country are being harangued about eating more fruits and vegetables, legumes and nuts. They’re told to cut back on fried foods, while limiting the intake of meat, dairy and all foods high in fat.
That’s been standard operating procedure for nutritionists and dietary authorities for the last 25 years.
Now, however, the Flemish Institute of Healthy Life in Belgium has gone totally off the rails. In a new graphic purporting to guide Belgians to a healthier lifestyle, the classic food pyramid has been inverted (see chart).
What are the most important dietary components, at least for the country’s residents who speak Dutch? Water, fruits, vegetables, tofu and noodles.
That’s a wonderful diet — if you’re a devotee of a monastic order living an existence of seclusion and sacrifice.
Meanwhile, Belgians are being “educated” that beef is relegated to the bottom of the inverted pyramid. And such processed meats as bacon, pepperoni, and deli meats aren’t even in the pyramid. Instead, they’re shoved off to the side in a not-so-subtle banishment from a “good” diet. They’re on the outside looking in, along with such “bad” choices as soft drinks, cookies, pizza, and french fries.
Belgians are being advised to eat those foods, “as little as possible.”
Less meat, more cancer?
This latest outrage is a direct outcome of the 2015 World Health Organization pronouncement on red meat, a statement that has been widely misinterpreted by the media. The WHO statement noted that:
“A Working Group of 22 experts from 10 countries convened by the IARC Monographs Programme classified the consumption of red meat as probably carcinogenic to humans, based on limited evidence that the consumption of red meat causes cancer in humans.”
As has been explained numerous times in this space and elsewhere, there is solid evidence that certain toxic substances, such as dioxin, can trigger the onset of cancer. That’s because they’re chemical compounds, not foods, that don’t exist in Nature and which are identified as significant environmental contaminants to which people should avoid any exposure, much less ingestion.
With various foods — like red meat — that have been part of humanity’s diet for millennia, the basic question has to be asked — again: Why now? Why in the 21st century is beef and pork suddenly responsible for causing cancer? Why, as both domestic and European per-capita consumption of red meat has continued to decline over the past four decades, would cancer be connected with animal foods at this point in time?
And even worse, there have been several gleeful accounts of this new recommendation, such as this one on Quartz.com, that compared Belgium’s actions to USDA’s recommendations in the Dietary Guidelines for Americans:
“Meat industry groups recently lobbied heavily to stop the [federal] government from advising people to eat less meat. They were successful.”
No, they weren’t.
Take a look at the new Plate.gov, the replacement for the old Food Guide Pyramid. The basic food groups are Fruits, Grains, Vegetables and Protein (Dairy is off to the side). No mention of meat, red or white, anywhere at all.

If that outcome is “successful” from the perspective of “meat lobbyists,” I’d hate to see what defeat would look like. Maybe a graphic of a hamburger formed into a skull and crossbones?
Meanwhile, Belgian policymakers are defending the new recommendations.
“We want to make it clear that we don’t need these products,” a representative of the Institute of Healthy Life told Flanders Today. “We don’t forbid them, but they should be rather an exception than rule.”
The Belgian government also recently released a second pyramid designed to guide the country towards recommended physical activity, and that one is about as plausible as its upside-down food pyramid.
The “fitness” recommendations include getting up from your chair every half an hour, and exercising once a week with an activity that “increases your heart rate, such as aerobics or jogging.”
That rigorous program would make a real difference in someone’s health status.
If they’re a couch potato who hasn’t lifted anything heavier than a bag of a chips since the 1990s.
Washed down with lots of water and a side of tofu, of course.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.