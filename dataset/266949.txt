Dairy feedlot appeal will proceed in Waukesha, not Madison
Dairy feedlot appeal will proceed in Waukesha, not Madison

The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — The Wisconsin Supreme Court says an appeal in an environmental case involving a large dairy feedlot should be heard in Waukesha, not Madison.
The Supreme Court, in a 5-2 decision Tuesday, agreed with the Department of Natural Resources that the proper venue is Wisconsin Court of Appeals District II, not District IV. The ruling clarifies the right of an appellant to select the venue under changes to Wisconsin statutes in 2011.
The case dates from 2012, when Kinnard Farms in Kewaunee County applied to expand in an area where groundwater contamination had created tensions between farmers and non-farmers. The DNR granted the permit. A dispute ensued over the proper venue for the farm's opponents to appeal.
Conservatives typically file in the appeals court in Waukesha, located in the most conservative part of the state, rather than in Madison, a liberal stronghold.
The appeal on the merits of the case will now proceed in District II.