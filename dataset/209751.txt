Among all the occupations in America, those who work in farming, forestry and fisheries are most likely to commit suicide—by a mile, according to the U.S. Centers for Disease Control and Prevention (CDC). Nearly 85 farmers, forestry and fishery workers per 100,000 end their own lives every year, according to the CDC, the most among 30 standard occupational groupings studied. 

The last time these numbers were compiled, it was 2012 when corn hovered around $6 to $7 and soybeans in the $11 to $12 range. 

Fast-forward to today and farmers could be in for a stressful harvest. That has southern Illinois farmer and banker Alan Hoskins of American Farm Mortgage and Financial Services concerned for his neighbors and customers.

“Obviously, the price situation has created a lot of uncertainty,” Hoskins says. “One fear I have is the size of the crop we’re producing. Most operations aren’t built to hold 100% of their grain in a normal year, and this year, farmers will be faced with tough decisions on what to do with bushels.” That’s enough to cause major stress for any farmer, he says.

If the hustle and bustle of harvest slides into overpowering stress, it can produce long-term effects and even threaten your health. Learn the symptoms of harmful stress and how to manage it while still managing your farm this harvest season.

“The typical pattern is alarm, wearing down of the body and brain capacity and gradual onset of depression,” says Mike Rosmann, agriculturalist, clinical psychologist and founder of AgriWellness Inc., a nonprofit program promoting behavioral health sciences for those in agriculture.

If you or others involved in your operation aren’t sleeping, are becoming irritable, are losing all optimism or starting to talk about selling off or getting out of the business—those are sure signs stress is affecting your thinking, Rosmann says. 
Stress manifests itself differently in each person. Left unmanaged, stress can lead to life-altering effects such as anxiety, heart attacks, depression and damaged personal relationships. Experts offer these management tips:

First and foremost, track your sleep and make sure you’re getting enough. Once you get 10 hours behind on your normal sleep routine, your body will react just like you have a 0.08 blood-alcohol level, affecting your reaction time, judgement and making you irritable, Rosmann says.
Take time to restore your mind and body. A couple of times a day or when you’re feeling strained, take five minutes to shut down the combine, get out, stretch and think about something cheerful, not work-related.
Reach out for support when you’re feeling bad. Talk to your spouse, friends, consultants or other trusted advisers and let them know what concerns you, Hoskins says. They might be able to offer advice or just a listening ear.
Be sure to exercise. Riding in the combine all day can be exhausting, but you’re not getting very much exercise. Take 30 minutes a day for a walk, run or some form of movement. Exercise releases the brain chemical serotonin, which assists with stress relief.
Meditate, pray or enjoy whatever peaceful, quiet time means the most to you. Even try yoga—anything that forces your concentration on relaxing. This reduces stress-related hormones.
Your spouse and pets can also be very helpful in reducing stress hormones. Odd as it might sound, petting animals or having your spouse stroke your hand releases serotonin and helps reduce stress, Rosmann says.

Your mind and body are irreplaceable. Use these tips to make sure you’re caring for them effectively, even when it feels like you don’t have time.