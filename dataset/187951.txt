After several days of rain in recent weeks the majority of corn growing states are officially drought free—and in some cases looking more like lakes than fields. For young corn seedlings, continued pounding rain could compound pest problems and even lead to replant.
 
Observed Precipitation - May 5 to May 11, 2017
 
.embed-container {position: relative; padding-bottom: 67%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}



 

National Weather Service. Christopher Walljasper/Farm Journal Media

Pre-Emergent Watch Outs
Seeds need to imbibe a certain amount of moisture—about 30% of their seed weight to begin germination, and conditions like cold, saturated soils, can hurt growth and put seeds at risk of dying below ground. When in a field that hasn’t emerged, dig in several places and check on seeds. If they’re mushy and off-colored, there’s a good chance disease has taken over and that seed won’t germinate.
Sitting seeds can also serve as a snack for attacking pests. “I’ve seen more white grubs than I’ve ever seen before due to cooler conditions and therefore slower corn growth,” says Melissa Bell, Mycogen commercial agronomist in Illinois. “Sitting idle opens the crop up for attack from secondary pests.”
 




 


White grubs can eat seeds underground, decimating stands.


© Melissa Bell







Manage Seedling Damage
“Watch for pythium, fusarium and even Rhizoctonia,” says Dean Grossnickle, Syngenta agronomic service representative. “Especially in cool, wet conditions, but know that certain species of pythium and fusarium can attack in warm, wet conditions, too.”
To check for disease dig plants in four to five locations across fields and examine the roots, seeds and the mesocotyl. A bright white root system and mesocotyl root is a good sign, but if roots appear brownish, translucent or otherwise discolored and seeds are mushy, it’s a good sign of disease. When the mesocotyl root has damage, no nutrients get to the plant, which leads to death.
 




 


Excess moisture can lead to damaging disease such as pythium.


© Jeremy Wolf







“Since we’ve had rains come up from the south, it’s also brought in black cutworm moths that have laid eggs and already started causing damage in emerged cornfields,” Bell says. Along with black cutworms many states have seen a large influx of armyworms and bean leaf beetles—that, despite their name, will snack on corn.
Scout early and often to catch insect pests before they lead to damage that even insecticide can’t fix. Some seed treatments or trait packages include protection against these pests, but that doesn’t mean scouting is optional. Once a certain threshold is reached, foliar insecticide might be necessary in even traited or treated corn.
Replant Considerations
Double check the calendar, acres left to plant and cost before pulling the replant trigger. The later in the season it gets, the higher the risk for replant.
“First figure out what your stand is—how many plants per acre, is it even across the field or patchy?” Grossnickle says. “Look at the time of year, can you replant to corn again or do you need to switch to soybeans? What herbicide did you use—are soybeans even a viable alternative?”
Check the calendar to find out how much yield is still available. If there is 70% of a stand left in a field and the calendar indicates there’s only 70% yield potential, it’s better to stick with what’s out there. In addition, keep an eye on weather forecasts to see if there could be more rain coming that could drown out more plants, which could make replanting virtually useless.
If replant is the best option, experts recommend keeping the same maturity for corn or soybeans as long as possible. It’s also important to wait for the field to be ready, just because the top ½” is dry doesn’t mean below ground is. Travelling over fields too early could lead to sidewall compaction and poor root growth throughout the season.
 




 


Crusting can make it difficult for corn to emerge.


© Jeremy Wolf