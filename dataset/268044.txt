Australian farm sells melons after fatal listeria outbreak
Australian farm sells melons after fatal listeria outbreak

The Associated Press

CANBERRA, Australia




CANBERRA, Australia (AP) — An Australian farm has recommenced selling cantaloupes despite being blamed for an outbreak of the deadly listeria bacteria that the World Health Organization said killed seven people and caused a miscarriage in Australia.
The U.N. agency said Monday the Australian cantaloupes, known as rockmelons, were exported to Hong Kong, Japan, Kuwait, Malaysia, Oman, Qatar, Singapore, the United Arab Emirates and Bahrain and may also have been sent to the Seychelles.
The agency said that between January and April, Australia reported 19 confirmed cases and one probable case of listeria poisoning, including seven fatal.
Australian authorities tracked the contamination to Rombola Family Farms at Nericon in New South Wales state.
The New South Wales Food Authority said last week Rombola had recommenced supplying rockmelons after testing cleared the property of contamination.