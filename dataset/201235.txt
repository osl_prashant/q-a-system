By Sonja Begemann and Susan Skiles Luke
As of 12:01 a.m. Tuesday, Arkansas farmers will no longer be able to use dicamba when a 120 day ban takes effect. Missouri officials issued a similar ban this past Friday but say they'd like to reinstate applications during the current growing season, pending their investigation.
Missouri Agriculture Director Chris Chinn delivered a statement on the department’s YouTube channel Friday afternoon.
“(This) means distributors cannot sell dicamba products and more importantly it means that farmers should not apply dicamba products until we lift this order,” Chinn said in the video. She gave no indication of when exactly that order might be lifted.
The Missouri Soybean Association issued a statement saying more than 200,000 Missouri soybean acres currently show signs of suspected dicamba damage. “It’s clear that action is needed,” the state soybean checkoff president, Matt McCrate, said in the statement.
Chinn said the department is actively investigating the matter. “I’ve asked the makers of these approved, post-emergent products and farmers to work with us to determine how we can expeditiously allow applications to resume this growing season,” she said in the video. “As soon as we’ve completed this process we plan to issue a special label and resume applications.”
Monsanto released a statement saying they are complying with the order and encourage "all growers, retailers and distributors to do the same. 
"We spent years developing the XtendMax with VaporGrip Technology to minimize the potential for off-site movement.  We want to stress how important it is that growers and applicators who use our product follow the label requirements and any local requirements," the company said. "Monsanto is committed to remaining actively engaged in this conversation and doing our part to help farmers use the Roundup Ready Xtend Crop System successfully."
For its part, the Arkansas Agriculture Department Friday announced an emergency rule banning the use and sale of dicamba products in that state.
The 120-day ban will go into effect starting Tuesday, July 11, at 12:01 a.m., according to the Department. The group also raised civil penalties for dicamba misuse to a maximum of $25,000 from $1,000 previously.
To date, Arkansas farmers have filed nearly 600 complaints in which dicamba is the suspected pesticide.
On June 30, Arkansas Gov. Asa Hutchinson gave a green light to an emergency rule proposed by the Arkansas State Plant Board (ASPB) placing a ban on in-crop dicamba use. Today’s decision by the Arkansas Legislative Council marks the final process needed to pass the ban.
In a recent letter to Wes Ward, secretary of the Arkansas Agriculture Department, and Terry Walker, director of the ASPB, Hutchinson wrote: “…I am concerned that more limited options were not fully debated and considered because of the need for quick action. I know the Plant Board also shares my concern that this action is being taken in the middle of a growing season, but the volume of complaints do justify emergency action.”
Monsanto also issued a statement Friday on the Arkansas action.
“We sympathize with any farmers experiencing crop injury, but the decision to ban dicamba in Arkansas was premature since the causes of any crop injury have not been fully investigated. While we do not sell dicamba products in Arkansas, we are concerned this abrupt decision in the middle of a growing season will negatively impact many farmers in Arkansas,” Monsanto said. 
Both states have released maps indicating damage by county.
 




 


Nearly 600 alleged dicamba misuse complaints have been filed as of July 7, 2017.


© Arkansas Agriculture Department







 




 


Missouri has 200,000 acres of alleged dicamba damage, according to the state's soybean checkoff.


© University of Missouri Weed Science