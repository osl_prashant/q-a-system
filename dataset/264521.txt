Mississippi agriculture official expected to go to US Senate
Mississippi agriculture official expected to go to US Senate

By EMILY WAGSTER PETTUSAssociated Press
The Associated Press

JACKSON, Miss.




JACKSON, Miss. (AP) — Mississippi Gov. Phil Bryant is expected to appoint state Agriculture Commissioner Cindy Hyde-Smith to succeed a longtime U.S. senator who is resigning because of poor health.
Bryant has scheduled an announcement at noon Wednesday in Hyde-Smith's hometown of Brookhaven. Three state Republican sources told The Associated Press that he chose the Hyde-Smith to succeed Sen. Thad Cochran, who is 80 and is stepping down April 1. The sources spoke Tuesday on condition of anonymity because the announcement was not yet official.
Hyde-Smith, 58, would be the first woman to represent Mississippi in Congress.
She would immediately begin campaigning for a Nov. 6 nonpartisan special election to fill the rest of Cochran's term, which expires in January 2020.
Hyde-Smith won a state Senate seat in 1999 as a Democrat. She switched to the GOP in late 2010 and was elected agriculture commissioner in 2011, holding the job since then. In 2016, she was one of many agriculture advisers to Republican Donald Trump's presidential campaign.
Bryant is a Trump supporter and has said he believes the president will campaign for his Senate appointee in the special election, which could attract several candidates.
Chris McDaniel, a tea party-backed state senator who nearly unseated Cochran in a bruising 2014 Republican primary, said last week that he is running in the special election. Democrat Mike Espy, who was President Bill Clinton's first agriculture secretary, also intends to run. Espy in 1986 became the first African-American in modern times to win a congressional seat in Mississippi, and he has publicly supported both Democrats and Republicans in various races.
Cochran's resignation creates two Senate races this year in Mississippi as Republicans are trying to maintain their slim Senate majority. Although it is a deeply conservative state, Democrats are hoping to capitalize on divisions among Republicans in hopes of winning a Nov. 27 runoff, if there is one.
Hyde-Smith served 12 years as a Democrat in the state Senate from a rural southwest Mississippi district, switching to the Republican Party in late 2010.
In 2011, she won a three-way GOP primary for agriculture commissioner without a runoff. She beat Democratic opponents even more easily in the 2011 and 2015 general elections.
Hyde-Smith is one of only four women ever elected to statewide office in Mississippi. It and Vermont are the only two U.S. states that never have elected a woman to Congress.
Bryant has said he was focused on naming a senator who could serve at least 20 years. Mississippi has a tradition of sending the same people to Washington for decades to build seniority and influence. Cochran is in his second stint as chairman of the powerful Appropriations Committee.
____
Follow Emily Wagster Pettus on Twitter: http://twitter.com/EWagsterPettus .