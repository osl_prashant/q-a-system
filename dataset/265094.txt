China may hike tariffs on US pork, aluminum, other goods
China may hike tariffs on US pork, aluminum, other goods

By JOE McDONALDAP Business Writer
The Associated Press

BEIJING




BEIJING (AP) — China announced a $3 billion list of U.S. goods including pork, apples and steel pipe on Friday that it said may be hit with higher tariffs in a spiraling trade dispute with President Donald Trump that companies and investors worry could depress global commerce.
The Commerce Ministry urged Washington to negotiate a prompt settlement to the conflict over Trump's tariff hike on steel and aluminum but set no deadline.
Separately, the ministry also criticized Trump's decision to approve a possible tariff hike on billions of dollars of Chinese goods in a dispute over Beijing's technology policy. The ministry slammed that as "trade protectionism" but gave no indication how Beijing might respond.
The ratcheting up of tensions sent a shiver through world financial markets. Shares tumbled on Wall Street and slumped in Asia, where Japan's Nikkei 225 index fell 3.5 percent while the Shanghai Composite index slipped 3.1 percent and Hong Kong's Hang Seng lost 2.8 percent.
The dollar dipped to 104.85 yen as investors shifted into the Japanese currency, which is viewed as a "safe haven" from risk.
China's proposed tariff hikes in response to the steel and aluminum duties appeared to be aimed at increasing domestic U.S. pressure on Trump by making clear which exporters, including farm areas that voted for the president in 2016, might be hurt.
"Beijing is extending an olive branch and urging the U.S. to resolve trade disputes through dialogue rather than tariffs," said economist Vishnu Varathan of Mizuho Bank in a report. "Nevertheless, the first volley of shots and retaliatory response has been set off."
The ministry said Beijing was considering a tariff increase of 25 percent on pork and aluminum scrap, mirroring Trump's 25 percent charge on steel. A second list of goods including wine, apples, ethanol and stainless steel pipe would be charged 15 percent, mirroring Trump's tariff hike on aluminum.
The ministry said Chinese purchases of those goods last year totaled $3 billion. That would be less than 1 percent of Chinese imports of U.S. goods and far smaller than the range of imports targeted by Trump's order Thursday in the technology dispute.
American business groups have warned Trump his aluminum and steel tariffs could hurt the U.S. economy and disrupt exports.
Abroad, companies worry the dispute could spiral into tit-for-tat import controls by governments worldwide that could dampen global trade.
China's top economic official, Premier Li Keqiang, appealed to Washington on Tuesday to "act rationally" and said, "we don't want to see a trade war."
The higher American duties on aluminum and steel have little impact on China, which exports only a small amount of those products to the United States. But private sector analysts have said Beijing would feel obligated to take action to avoid looking weak in a high-profile dispute.
The Commerce Ministry said the higher U.S. tariffs "seriously undermine" the global trading system. It rejected Trump's contention they are needed to protect U.S. national security.
"The Chinese side urges the U.S. side to resolve the concerns of the Chinese side as soon as possible," the ministry said. It appealed for dialogue "to avoid damage to overall Chinese-U.S. cooperation."
Beijing reported a trade surplus of $275.8 billion with the United States last year, or two-thirds of its global total. Washington reports different figures that put the gap at a record $375.2 billion.
The technology dispute stems from complaints Beijing unfairly compels foreign companies to hand over technology in exchange for market access. Companies in many industries including auto manufacturing that want to operate in China are required to work through local partners, which requires them to give technology to potential Chinese competitors.
The U.S. Trade Representative's office said Trump's action was in response to "unfair and harmful acquisition of U.S. technology." The USTR said Trump had ordered it to pursue a World Trade Organization case against Beijing's "discriminatory technology licensing."
The U.S.-China Business Council, which represents American that do business in China, said it agreed Chinese "technology transfer" policies need to be improved but it appealed to both governments to reach a negotiated settlement.
"American business wants to see solutions to these problems, not just sanctions such as unilateral tariffs that may do more harm than good," said USCBC president John Frisbie in a statement.
On Tuesday, the Chinese premier promised at a news conference Beijing will "open even wider" to imports and investment as part of efforts to make its economy more productive.
Li said that included eliminating tariffs on drug imports. But he gave no other details and it was unclear whether the planned changes might mollify Washington, the European Union and other trading partners that complain China improperly blocks access to its markets and subsidizes exports.