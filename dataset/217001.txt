Designed by the National Pork Board, the Youth Pork Quality Assurance (YPQA) Plus Program was created to educate youth producers and help to prevent drug residues, ensure the wholesomeness of pork and pork products, and promote consumer confidence in pig well-being. 
In 2017, a new redesigned, multi-species quality assurance program for youth ages 8 to 21 was unveiled, called the Youth for the Quality Care of Animals (YQCA) program. The new curriculum offers about estimated 60 minutes of education each year, for youth producing and/or showing pigs, beef cattle, dairy cattle, sheep, goats, market rabbits, and poultry. 
At press time, only a few states and fairs have announced they will accept YQCA certification. All county and state fairs in Arizona, Nebraska and North Dakota will accept the certification. Shows such as the California State Fair, Santa Barbara (CA) County Fair, National Western Stock Show and National Pork Expo, will also accept the certification.
An online version of the YQCA program requires the participant to pass three quizzes to earn certification. An in-person YQCA workshop requires complete attendance.
Youth with a current YPQA Plus certification will remain certified until the expiration date has passed. This certification, as well as YQCA certifications, will be accepted by and could be required by processors and certain fairs and livestock exhibitions.
YQCA focuses on three core pillars: food safety, animal well-being and character development. This program mimics the YPQA Plus Program and highlights 10 good production practices for youth producers to review. The biggest difference between the programs is that YQCA is geared toward all market animal species and not limited to only youth pork producers. This new program includes information for youth raising the following food animals:

Pigs
Beef cattle
Dairy cattle
Sheep
Goats
Market rabbits
Poultry

Regardless of the method that is used to access the program, a standardized fee will be charged with payment made directly to the YQCA Program. For utilizing the online certification system, youth will have to pay a $12 fee. When attending a face-to-face, instructor-led session, a $3 fee will be charged per participant. These fees will be used to maintain the YQCA program, keeping the content up-to-date with industry standards and government regulations, and delivered in an engaging and on-demand platform.