The U.S. Department of Agriculture (USDA) January 2017 Crop Production Summary confirmed farmers harvested a record amount of soybean acres in the United States.
Why has the soybean market been in the green since the report? Is the rise due to a weather market?
AgDay national reporter Betsy Jibben talks with Jarod Creed of JC Marketing about the current mechanics of the soybean market.