Staff writers Ashley Nickle and Kate Walz discuss the top stories of the week, including Whole Food's CEO comments about the Amazon purchase, our print redesign, and avocado theft.Want more to the story? Read the latest in Grand theft avocado, as well as other produce thefts:
Authorities in search of stolen produce trucks
Cargo thieves target California nuts
A case of stolen tomatoes
Two arrested for walnut theft