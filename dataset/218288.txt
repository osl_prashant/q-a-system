As trade negotiators gear up for the next round of talks to renegotiate the North American Free Trade Agreement in Montreal, Canada, some industry stakeholders are frustrated with the lack of progress made thus far.
“So far the negotiations have not really gone anywhere,” said Mike Johanns former Secretary of Agriculture and former U.S. Senator.
Still, many are hopeful that the President is committed to renegotiating the deal, not pulling out of it.
“When you talk about NAFTA, all I can think about is Art of the Deal and Master Negotiator,” Iowa farmer Tim Recker told AgriTalk host Chip Flory this week referencing two of President Trump’s books. “He wants a plan that’s good for all of the United States.”
Some industry stakeholders have wondered if the president fully understands how important the NAFTA agreement is to U.S. agriculture. According to Johanns, the president gets it.

“My take is the president has certainly come to realize how critical NAFTA is to parts of the country that supported the president when he ran for office,” he said. “The key issue for the president is going to be ‘How I come up with something that saves face and doesn’t blow this thing up?’”
Nebraska farmer Greg Anderson agrees.
“The farm sector has been overwhelmingly for Trump in the election,” he says. “With trade for Nebraska for example, if beans aren’t going off to the PNW for exports they are going direct rail to Mexico.”
Anderson admits there are some non-agricultural things within NAFTA that could be looked at and says he’s confident the president’s team, including Agriculture Secretary Sonny Perdue, will have the president’s ear on the issue to ensure agricultural interests are not forgotten.
As for pulling out of the deal, that could be devastating to U.S. agriculture, Johanns says.
“Without NAFTA agriculture is in trouble,” he says. “Couple that with the fact that we’re on the edge of a trade war with China, there’s really some consequences.”
Not only are there impressive statistics related to how much NAFTA has grown the U.S. economy, but the farm gate has felt its impacts too.  

“In the real world, this is so very, very important for the average farmer or rancher out there that is just trying to make a living,” Johanns says. “There’s a good story to tell, we just need more people telling the story.”
Informa Economics policy analyst Roger Bernard says if anything should open the president’s eyes to the consequences of pulling out of NAFTA, the stock market dive when there was a rumor the U.S. was in the process of leaving the deal last week should give the administration an indication. On that news the stock market, U.S. dollar, Canadian dollar and Mexican peso all took a dive.
“Once you get that kind of reaction that should send a very clear signal to the White House that even just the prospect,” he says. “We’re looking at something that is very significant for the whole economy.”