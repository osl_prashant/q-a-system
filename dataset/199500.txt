From the May 2016 issue of Drovers. Recognizing that verifiable health programs add value to feeder cattle, numerous animal-health companies, cattlemen's associations, auction barns, seedstock producers and others offer marketing programs for qualified preconditioned cattle. These programs help link producers who invest in proven health protocols with buyers who recognize the positive impact those protocols have on feedyard performance and carcass value.
This list provides a sample of the types of marketing programs available to producers but does not include every program available. If you know of a program we didn't include, email jmaday@farmjournal.com the information and we'll update the list.
44 Farms: Feeder Calf Buyback program
Producers follow the 44 Farms RightWay animal health and welfare protocols to give each steer and heifer calf the best opportunity to qualify for the 44 Farms all-natural branded beef program.
www.44farms.com/calf-buyback
Boehringer Ingelheim Vetmedica: Market Ready
The Market Ready Quality Feeder Calf Program helps producers deliver healthy, high-performing calves, while giving you the flexibility to manage your herd the way you want.
www.bi-vetmedica.com/species/cattle/keep_calves_healthy/KCH_Market_Ready.html
Elanco Animal Health: Full Value Beef
Full Value Beef is a business relationship providing you with additional resources, data and analytic capabilities to uncover hidden opportunities and product and service solutions forbetter returns.
www.elanco.us/products-services/beef
Joplin Regional Stockyards
Producers have the opportunity to market cattle in special sales when certain value-added practices have been utilized, such as health programs, optional feeding programs and weaning.
www.joplinstockyards.com/value_added_sales.php

Kentucky CP45 program 
The Kentucky Certified Pre-Conditioned for Health (CPH-45) program was created by Kentucky beef producers to increase the quality and value of state-originated cattle.
www.cph45.com
Merck Animal Health: Prime Vac
PrimeVAC preconditioning programs improves efficiency and performance for healthier, higher-value calves.
www.merck-animal-health-usa.com/species/Cattle/primeVAC_45.aspx
Merial: Sure-Health
Sure-Health gives you the information and resources you need to raise healthier, higher-value calves.
www.surehealth.com/Pages/default.aspx

MFA: Health Track/PowerCalf 
PowerCalf helps producers measure and manage all aspects of their operation, from forage quality, genetics, health and nutrition protocols and reproductive efficiency.
www.mfa-inc.com/healthtrack.aspx
Where Food Comes From, Inc.: Source Verified
Where Food Comes From, Inc., offers verification and certification services to help producers, brands and consumers differentiate certain attributes and production methods in the marketplace.
www.wherefoodcomesfrom.com
Nichols Farms Value Added Programs
Nichols Farms, located in southwest Iowa, markets bulls, semen and embryos throughout the world. Nichols Farms and its franchises are family farms, selling Nichols Beef Genetics and offering Nichols Value Added Programs.
www.nicholsfarms.biz
Okeechobee Livestock Market: OKEE VAC 
The Okeechobee Livestock Market, Okeechobee, Fla., herd health program, OKEE VAC, aims to help boost the price of cattle enrolled.
www.okeechobeelivestockmarket.com
Verified Beef
Verified Beef (VB) is an independent third party offering verification services needed to export beef to many countries.
www.verifiedbeef.net
Zoetis: SelectVAC 
The SelectVAC program helps you produce healthy, in-demand, high-reputation calves.
www.selectvac.com
From the special section: Drovers Insight. This is a bonus section published periodically to provide additional information to cattlemen about issues important for the production of healthy, wholesome beef.