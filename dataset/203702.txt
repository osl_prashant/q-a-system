Giro Pack Inc. has introduced a box filling system designed to save produce packinghouses time, labor and money.
In late August, the Vidalia, Ga.-based packaging system developer was rolling out the GBF-100.
The equipment was designed to automate and streamline packinghouse bagging operations, said Jennifer Doxey, West Coast sales and marketing manager.
No U.S. operations are using the machinery yet, she said.
By filling boxes with net bags, the equipment represents the final process in the packing line before product is palletized, Doxey said.
The machinery can accommodate a variety of pack sizes, styles and weights and can pack many fruit and vegetables, including avocados, citrus, potatoes and onions and other non-delicate fruit, she said.
Bag weights range from 1-10 pounds.
A major attribute is minimal manual operation, Doxey said.
"This is very important, especially in California where labor rates are increasing," she said. "A lot of companies are looking to automate more areas of their packinghouses and want to reduce the amount of manual intervention. Over the next couple of years, this machinery will be quite instrumental."
Other features include a vibrating/ compacting device to guarantee proper positioning of bags inside boxes.
To prevent bags from jumbling inside the boxes, the machinery shakes the boxes to make the bags become flush so they can be stacked onto other boxes on palletizers, Doxey said.
Optional features include a rejection system with a load cell for over and underweight boxes.
A box feeder device which can automatically load nine empty boxes in a magazine is also available as an option.
Another option involves one- or two-layer feeding, which allows the packing of different layers inside boxes.
Product specifications:
100 boxes per minute output;
Can fill corrugated and plastic containers; and
Can work with boxes with dimensions up to 600x400 millimeters.
The company officially introduced the machinery in February.
Based in Badalona, Spain, Grupo Giro was founded in 1925 and in 1958, received the first patent for net bags to handle melons and other produce, according to the company's website.
It opened its U.S. operation in 1996 and has other operations throughout the world including Europe, Russia, the Middle East, Africa, China, Japan and Australia.