Arkansas hog farm applies for separate permit amid appeal
Arkansas hog farm applies for separate permit amid appeal

The Associated Press

MOUNT JUDEA, Ark.




MOUNT JUDEA, Ark. (AP) — An Arkansas hog farm has applied for another permit to keep operating near the Buffalo National River after the state denied its permit three months ago.
C&H Hog Farms has submitted application documents for a Regulation 6 individual permit, which is a wastewater discharge permit under federal regulations implemented by the state Department of Environmental Quality, the Arkansas Democrat-Gazette reported. If approved, the farm would be able to continue operating for five years.
The move comes after the farm appealed the department's denial of its Regulation 5 individual permit to the Arkansas Pollution Control and Ecology Commission. The application for the permit was denied because it didn't review the groundwater flow direction or develop an emergency action plan.
The new permit application documents submitted don't appear to include an emergency action plan or a study on the groundwater, but it's unclear whether the farm has submitted its entire application.
Jason Henson, a farm co-owner, said C&H would close their recent application for a Regulation 6 permit if they can acquire a Regulation 5 permit.
The farm operates near Mount Judea, along where Big Creek feeds into the Buffalo National River. Environmental groups have become concerned with the farm's manure potentially leaking into the river.
Richard Mays, an attorney for those who oppose C&H, said that he and his clients were unaware C&H had applied for a Regulation 6 permit.
"They're trying to cover all their bases," Mays said.
The farm can remain open during the appeal process. A hearing is scheduled Aug. 6.
___
Information from: Arkansas Democrat-Gazette, http://www.arkansasonline.com