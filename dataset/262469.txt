Sunspot activity has been monitored for the last 250 years, and cycles occur every 11 years, according to phys.org. The current cycle began in 2008 and sunspot activity peaked in 2014.

At one end of the 11-year cycle, the sun is relatively quiet, meaning there are few sunspots and flares. The solar max, the other end of the cycle, has an increased number of sunspots and more frequent solar storms.

Sunspots are one factor Shawn Hackett of Hackett Financial Advisors is observing that could play a large role in agriculture over the next five years, and we won’t be experiencing normal weather that we are used to seeing.

“There’s a very consistent solar radiation hits the earth, and it causes a certain amount of weather volatility that’s reasonable and the world can grow plenty of food within the confines,” he told AgDay host Clinton Griffiths.

According to his firm’s research, the sun is less active every 200 years and “all the weather variables get turned upside down.”

He cites fourth quarter drought in Argentina and the U.S. and flooding in France as evidence.

“Things are happening that haven’t happened in a long time,” said Hackett. “It’s just the beginning.”

He says we are entering a 5-year trough, which is where the greatest weather volatility.

“We have data that goes back to the 1600s and 1800s from ice core samples, tree rings, and from actually written testimony of the kind of problems that they had,” said Hackett.

If this pans out, Hackett expects to see a world with localized famines and food scarcity. He says he’s not out to spread gloomy news.

“It’s not going to be pleasant,” said Hackett. “It’s going to be a very difficult time. We’re going to have to come up with new ways of growing food in this more difficult environment.”