It took almost the first 100 days of Donald Trump’s presidency to finally have Sonny Perdue approved as Secretary of Agriculture, but he is now ready to hit the ground running.
“It has been a great honor and opportunity to work for a president who I think is keeping his promises,” Perdue says during a press conference April 27 at the Kansas City U.S. Department of Agriculture (USDA) Beacon Facility.
For agriculture, Perdue says he believes that the campaign promises of rolling back regulations that hurt farmers and ranchers, including Waters of the United States (WOTUS), are being kept.
Trump has also asserted he will rework trade agreements to benefit the U.S. such as the North American Free Trade Agreement (NAFTA) and do away with the Trans-Pacific Partnership (TPP). On Jan. 23, the U.S. withdrew from TPP, and just this week, there have been rumblings of the U.S. leaving NAFTA if the agreement is not reworked.
“[President Trump] uses statements many times as a negotiation. Saying something was contemplated is not necessarily saying something is done or going to be done,” Perdue says.
Perdue says he is encouraged by recent developments with Mexico and Canada that should lead to renegotiating the 23-year-old NAFTA deal. He says the move should benefit agriculture and other industries in the U.S.
“I’m persuaded he has the leadership and tenacity to make a good deal for Americans,” Perdue says.
President Trump recently ramped up discussions on NAFTA by implementing a 20% tariff on soft woods from Canada as a retaliation for Canada's protectionist trade practices related to ultra-filtered milk.
Not many dairy farmers have been immediately impacted by the ultra-filtered milk trade battle with Canada, but Perdue is happy to see the president go to bat for them.
“He called their hand,” Perdue says of Trump dealing with Canada. “This is a president who is going to fight for America.”
Perdue likes what he has seen so far, but adds that current trade agreements won’t necessarily be dismantled.
“While on balance NAFTA has been good for American agriculture, we think it can be improved,” Perdue says.
Perdue has been advising the president on NAFTA’s benefits for agriculture and what areas need help. For instance, U.S. fruit and vegetable growers aren’t benefiting from NAFTA relative to their counterparts in Mexico, he says.
“My job as USDA Secretary is to go around the world and make sure we’re able to market and sell the products we grow…and make sure our producers continue to prosper and thrive,” Perdue says.
Going forward, here are a list of items on Perdue’s plate as Secretary of Agriculture:
He plans to visit areas of the High Plains impacted by wildfires and continue aiding them through established programs like Conservation Reserve Program grazing, fencing reimbursement and the Livestock Indemnity Program. Perdue was not allowed to visit the devastating fires because he was waiting to be sworn in.
Getting a farm bill put together that hopefully won’t suffer from delays seen by other farm bills. He’d like to rework programs for cotton and dairy producers.
With up to 21% budget cuts to USDA, Perdue understands the department will need to do more with less, but he will stress the importance of having enough funding to do what is best for farmers.
Currently it has not been determined who the Under Secretary of Agriculture will be.
The top priority for Perdue is to make USDA a well-run organization with better services and products for producers.
Perdue believes the Trump Administration is helping create “a spirit of optimism and hopefulness in this nation that we vastly needed.” He says he looks forward to working with USDA and moving the department further towards helping producers and the consumers who depend on agriculture.