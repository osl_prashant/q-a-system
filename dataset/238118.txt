AP-WY--Wyoming News Digest, WY
AP-WY--Wyoming News Digest, WY

The Associated Press



Wyoming at 6:15 p.m.
The desk can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
TOP STORIES:
REDRAWING THE INTERIOR
DENVER — U.S. Interior Secretary Ryan Zinke's plan for a major realignment to put more of his department's decision-makers in the field has a fundamental flaw in the eyes of some who spent their careers making those decisions: They're already out there. Eleven former Interior Department officials with decades of experience in both Washington and in local offices told The Associated Press the agency already has a well-established system for decentralized decision-making. By Dan Elliott. SENT: 840 words, photos.
OF WYOMING INTEREST:
TRUMP-CLIMATE PLAN REPEAL
SAN FRANCISCO — California stands in "complete opposition" to a Trump administration plan to scrap a policy slashing climate-changing emissions from power plants, its top air official said Wednesday at a U.S. hearing in a state helping lead the fight against global warming. State officials joined hundreds of other opponents, including technology billionaire Tom Steyer, in speaking out against ending the Obama-era Clean Power Plan at the hearing in San Francisco, billed as the latest in a series of national "listening sessions" by the U.S. Environmental Protection Agency. By Ellen Knickmeyer. SENT: 500 words, photo.
AP MEMBER EXCHANGES:
CUSTOM PUBLISHING BUSINESS
GILLETTE — It all started with a homemade book. It was 2006, and Christmas was approaching. Kathy Downey was trying to think of a gift she could give her brothers, and she decided to sit down with her mom, Viola Foulks, and tell her story. Downey made a memory book — similar to a photo album — full of pictures and stories of Foulks that she gave to her brothers. By Jonathan Gallardo, Gillette News Record. SENT: 1,200 words, photos.
DOCTOR HOME-VISIT PROGRAM
CASPER — When Pam Shellard wants to see her doctor, she takes a seat on her green couch. From the comfort of her living room, she answers questions about her shaking arm and leg, about the hallucinations, about the swelling. As a fish swims lazily in the aquarium next to her, she complains about how one of the 19 pills she takes robs her of sleep. She wears slippers and leans back against a floral pillow as she remembers the hand injury that led to her surgery, which led to a brief brush with death, which led her here, to Parkinsonism and to Dr. Andy Dunn. By Seth Klamman, Casper Star-Tribune. SENT: 1,600 words, photos.
STUDENTS SELLING COFFEE
CHEYENNE — Alta Vista Elementary sells some of the sweetest coffee in town. It's not the flavor or the sugar that makes it sweet, though - it's the service. Five days a week, students in kindergarten through sixth grade sell coffee, tea and hot chocolate to staff and any community members who wander in. By Kristine Galloway, Wyoming Tribune Eagle. SENT: 720 words, photos.
IN BRIEF:
— DAY CARE-CHILD SPANKED — The 57-year-old husband of a day care operator in Wyoming has pleaded not guilty to spanking a child to the point of bruising the child.
— WELL APPLICATION WITHDRAWN — An oil and gas company has withdrawn one of its three applications to pump hydraulic fracturing wastewater underground in southeast Wyoming.
SPORTS:
COMBINE-ELWAY
INDIANAPOLIS — John Elway is giving his clearest indication yet that he's going to pursue Kirk Cousins, the prize of this year's free-agent quarterbacks. "We're going to explore all options in free agency and see where that goes," Elway said Wednesday at the NFL combine. "Obviously we've got the fifth pick in the draft, too. That will all play into it. We'll continue to look at all of the options out there when it comes to quarterback." By Pro Football Writer Arnie Stapleton. SENT: 700 words.
FLAMES-AVALANCHE
DENVER — The Colorado Avalanche hosts the Calgary Flames on Wednesday night. (Game starts at 7:30 p.m. MT) (With AP Photos)
NEW MEXICO-COLORADO STATE
FORT COLLINS — Colorado State hosts New Mexico in men's college basketball. (Game starts at 7 p.m. MT) (With AP Photos)
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.