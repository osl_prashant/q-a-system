The California Avocado Commission has a comprehensive recipe database for foodservice operations looking to enhance their menu options.
 
The commission's website offers foodservice operators  a chance to review a variety of recipes.
Operators can browse the California Avocado Commission recipe database for more spring and summer menu solutions for all dining segments and dayparts, according to a news release.
The website also offers resources on nutrition, selection, storage and handling information, according to the release.
 
"Foodservice is a very important channel for California avocados," Jan DeLyser, vice president marketing of the commission said in an e-mail. "CAC works year-round to promote recipes and menu concepts that foodservice operators can feature during the California avocado season."