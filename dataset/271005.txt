“Millionaire to Millennials: Stop Buying Avocado Toast If You Want to Buy a Home” 
Yes, that headline really appeared on a story recently published on Time.com/money.
 
The Packer’s newsroom was sharing few laughs about the story, which was written about advice given by an Australian millionaire to younger consumers there.
 
Expanding on the headline, the first couple of graphs convey the gist of the piece. The story examines why more Millennials in Australia aren’t buying houses. The millionaire “property mogul” complained free spending on avocado toast was a symptom of the lack of fiscal discipline.
 
“When I was trying to buy my first home, I wasn’t buying smashed avocado for $19 and four coffees at $4 each,” Tim Gurner told the Australian news show 60 Minutes, according to the article.
 
Rather than the smashed avocado on toast, one wonders if the toast alone would be approved fare by Gurner. “By God, eating nothing but toast and water for 365 days straight will allow you to make a down payment on my overpriced condo,” one imagines he might earnestly advise.
 
No thanks, Tim. We’ll just keep eating smashed avocado and toast - and loving it.
 
--
 
The USDA posted another round of news about fraudulent organic certificates.  
 
The only produce operator listed was Renagrotec SPR, Hidalgo, Texas. The firm's organic certification of  of pineapple, lemons and kent and tommy atkins mangoes was fraudulent, according to the notice.
 
--
 
When I was at the recent Food Safety Summit, I was pointed to a resource about consumer eating habits that I never knew existed. The CDC uses this survey to determine how common it is for consumers to eat a particular food, which can be illuminating during a foodborne illness investigation. 
 
The 2006-07 Atlas of Exposures, which will be updated within a couple of years, gives insights on the frequency of fresh produce consumption. For example, the 2006-07 report show that 6.7% of all consumers surveyed had eaten fresh Brussels sprouts in the past week, compared with 23% who had fresh cauliflower, 53% for fresh broccoli, 60% for tomatoes, 71% for onions and 77% for potatoes.
 
Among the fresh fruit, the report shows that 70% had consumed bananas in the past week, compared with 62% for apples, 54% grapes, 45% oranges, 22% blueberries and 3% for papayas.
 
In addition to giving the U.S. average, it also provides data from 10 states.
 
For example, the data also provides info on consumers reporting consumption of “any organic produce” in the past week. That number was 27.4% for the U.S. average, but ranged from a low of 16.2% in Tennessee to a high of 52% in California.
 
For avocados/guacamole, the low was New York with 9% and the high was California with 54%. Plenty of smashed avocado on toast in Cali.
 
I”ll be anxious to see the updated version of this report, but for the time being, the “Atlas of Exposures” occupies a space among my bookmarked reference websites.