Robby Cruz is now vice president of fresh at Stillwater, Minn.-based Cub Foods. 
Formerly with Target Corp., Cruz took the position with Cub Foods in May, according to his LinkedIn profile. Cruz could not immediately be reached for comment.
 
Most recently, Cruz was senior director of procurement for perishables at Minneapolis, Minn.-based Target Corp. Before that, he had worked at Safeway for more than 20 years in various roles, according to his LinkedIn profile.