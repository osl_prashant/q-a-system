In the city infamous for snarling traffic and constant horn-honking, produce distribution can be challenging.
"There are always traffic problems but that's a part of doing business in this area," said Bruce Klein, director of marketing for Secaucus, N.J.-based Maurice A. Auerbach Inc. "It's also difficult because we have to make several deliveries a day and at each stop, you have to wait. You run out of time in a day."
One of the most difficult factors involving distributing in New York involves tolls.
A truck running from New Jersey to Long Island, for example, pays $75 in tolls, Klein said.
Holtsville, N.Y.-based J. Kings Foodservice Professionals Inc., finds deliveries easier to make in its Long Island home.
"We're all in the city at the same time," said Joel Panagakos, salesman. "We try to get as much done in New York City before the sun comes up. If night drops are available, it makes the delivery process a lot easier. Once the sun rises and we have to make second runs, it becomes very challenging not only getting it there with a couple of guys in a truck, but trying to not get ticketed."
J. Kings distributes to foodservice and retail customers in a 150-mile radius of central Long Island, including those in Connecticut, New York and New Jersey.
Traffic is a nightmare, said Joe Granata, director of produce sales for West Caldwell, N.J.-based FreshPro Food Distributors.
Granata gives a lot of credit to his company's drivers and said he finds it amazing how they can get through the crawling traffic.
"(The traffic's) unbelievable," he said. "It's so difficult. Between the parking tickets and congestion, a lot of people don't want to deliver to the city. They don't want the business. It's nuts."

Englewood, N.J.-based Riviera Produce Corp., sends smaller trucks to the more congested areas.
"It's a challenge but with logistics and fleet management, it can be overcome," said Benjamin Friedman, owner. "It's not as challenging to distribute produce in other areas."
On the Hunts Point Terminal Market, wholesalers find many of their customers want to reduce long trips to the facility.
E. Armata Inc. is focusing on supplying foodservice purveyors that don't want to travel to the market.
"It's the volume they want to get from the market but want delivered to them," said Paul Armata, vice president. "We have many restaurants that come here but like to keep their business outside. There are many that don't want to come to the market and deal with all the congestion. With 10-hour days, it can be longer for them if they have to come to the market."
One restaurant group buys a trailer of potatoes a week to make their french fries, said Panagakos of J. Kings Foodservice Professionals.
That helps by placing the distributor in the position of being able to receive potatoes via rail freight, which reduces transportation costs and the wholesaler can pass those savings to the customer, Panagakos said.
"No company I know of can stand still unless they're very specialized," he said. "If you're a distribution company, to stand still with a group of customers with the times that are out there today, you have to grow and get better. You have to develop your business in the growth pattern so you don't forget what it takes to satisfy your customers."
Robert T. Cochran & Co. Inc. serves mostly walk-up customers but also delivers some of its produce.
"The foodservice purveyors we service that deliver to restaurants, their business seems to be the same," said Mike Cochran, sales manager and vice president. "The restaurants I visit are usually pretty busy."
Market shoppers are the customers Jerry Porricelli Produce services through a full line of produce.
"I don't think people want to play around with cheap product anymore," said Ciro Porricelli, partner. "They want the good stuff. If they have an off package, they realize they can't sell it. Though at some other houses, certain people can move product for less, but to me, quality is a major factor. The product has to be good."