Americans are confused about food and don't know where to turn for answers. In fact, more than one-third of Americans do not know that foods with no genetically modified ingredients contain genes, according to the new nationally representative Food Literacy and Engagement Poll conducted at Michigan State University.For the record, all foods contain genes, and so do all people.
The majority of respondents who answered this question incorrectly were young and affluent, and also more likely than their peers to describe themselves as having a higher-than-average understanding of the global food system.
The full survey revealed that much of the U.S. public remains disengaged or misinformed about food.
These findings are problematic because food shapes our lives on a personal level, while consumer choices and agricultural practices set the course for our collective future in a number of ways, from food production impacts to public health.
Informing Food DiscussionsThe Food Literacy and Engagement Poll, which we plan to conduct annually, is part of Food@MSU, a new initiative based in Michigan State University's College of Agriculture and Natural Resources.
Food@MSU's mission is to listen to consumers, promote dialogue and help the public make more informed choices about food.
Many factors make those decisions challenging for today's consumers. Rapid scientific innovation has made it possible to engineer crops that can grow without fertilizer, survive flooding and supply vital nutrients to communities in the developing world.
But further progress may be limited without public awareness and support for research on urgent food and agriculture challenges.
Meanwhile, the proliferation of online content with conflicting messages makes it hard for Americans to separate valid nutritional information from fads and fraud.
Influential multinational corporations push ideas that aren't always based in science, but rather intended to promote their own products.
Our inaugural poll reveals that the public lags far behind current scientific understanding when it comes to food. Equally troubling, Americans aren't turning to scientists for answers.
Disconnected from FarmsToday fewer than 2 percent of Americans live on farms. As the US population continues to shift away from rural areas into cities and suburbs, we are ever more removed from the agricultural practices that sustain us.
More than 1,000 Americans age 18, were sampled online. Results were weighted to reflect U.S. census demographics for age, sex, race/ethnicity, education, region and household income to bring them into line with their actual proportions in the population.
The survey revealed that 48 percent of Americans say they never or rarely seek information about where their food was grown or how it was produced.
As we grapple with energy and resource conservation challenges in the United States and around the world, it is more important than ever to recognize how we use limited resources – and what we waste along the way.
Agriculture is a major source of pollutants that produce algae blooms and dead zones in the Great Lakes, Gulf of Mexico and other water bodies. 
More than half of respondents in our survey (51 percent) were willing to pay higher prices for foods with a less damaging impact on the environment, but consumers need to know how food is produced before they can take action.
Food SafetyHalf of respondents in the poll (50 percent) expressed concern over the safety of food available for purchase in their community. This included 56 percent of those earning an annual household income of US$75,000 or more and 46 percent of those earning less than $75,000.
They are right to be worried. According to the US Centers for Disease Control and Prevention, 48 million Americans become sick from food-borne illnesses every year.
These events lead to 128,000 hospitalizations and 3,000 deaths from viruses such as hepatitis A and norovirus, and bacteria such as Salmonella and Campylobacter.
The more we understand about how these bugs are transmitted, and how to store and handle food safely, the better we can protect our families and ourselves.

Global Food SecurityThe United Nations currently projects that the world's population will rise from 7.5 billion today to 9.7 billion people by 2050.
If food production fails to keep pace with anticipated growth, billions of people will go hungry. The biggest 21st-century agricultural challenge we face will be to produce more grains, fruits and vegetables on less land with fewer resources in the face of climate change.
The vast majority of scientists agree that one tool for meeting growing global food demand will be developing genetically modified crops that can survive with less fertilizer or water, promote disease resistance, improve yield or add vitamins for malnourished communities in the developing world.
Unfortunately, the poll found that much of the public does not embrace the promise of transgenic agriculture.
Although genetically modified organisms are currently found in over 75 percent of packaged food in the U.S., and we encounter them daily in corn, sugar and soy, most Americans remain unaware of their potential.
Forty-six percent of poll respondents either don't know whether they consume GMOs or believe they rarely or never do.
While the Food and Drug Administration has said that genetically modified foods are safe, large and vocal advocacy groups continue to stoke public fears and influence consumer choices away from their adoption.
The result is widespread misinformation and mistrust, which ultimately sets back progress toward allowing the technology to meet its full potential domestically and internationally.

Mistrustful of ExpertsWhen it comes to food, many Americans do not trust experts. Just 59 percent of respondents in our survey said that they trusted information from academic scientists on nutrition and food safety.
Less than half (49 percent) trusted government scientists, and only one-third (33 percent) trusted industry scientists.
Instead, consumers wade through conflicting recommendations from friends, relatives and celebrities that compete with fake news online for attention.
Meanwhile, advertisements and talking heads argue over the health benefits of staples like chocolate and coffee.
This may explain why a 2016 Morning Consult/New York Times survey found that nutritionists and Americans have vastly different ideas about what kinds of foods can be called "healthy".
Consumers face the exhausting task of sifting through the noise for reliable and accurate information on food.
Unfortunately, it's often difficult to find objective experts to listen to their concerns and provide answers that are grounded in science and easy to understand and put into practice.
Food for Thought, and ConversationThe Food Literacy and Engagement Poll is intended to provide baseline data for what Americans know about a variety of food topics.
A centerpiece of Food@MSU, called Our Table, will bring scientists, farmers, consumers and policy experts together to explore issues ranging from organic farming and health to GM crops and sustainability.
Over time, the poll will track public attitudes to guide research, as well as allow us to listen to consumers in order to help them make informed decisions about food.
Editor's Note: Sheril Kirshenbaum, is with Food@MSU, Michigan State University and Douglas Buhler, is Director of AgBioResearch and Assistant Vice President of Research and Graduate Studies, Michigan State University. This article was originally published by The Conversation. Read the original article.