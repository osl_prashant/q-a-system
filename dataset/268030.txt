Here are six government-related issues that impact your business.
1.  Trade
No one knows the full or long-term implications of the 25% tariff on U.S. pork going to China. Fortunately, U.S. producers and the organizations representing them have cultivated new markets in the Americas and Asia. In addition, pork exports to China have been on the decline recently, due to the glut of pork from China’s domestic production.
The North American Free Trade Agreement has much larger implications for U.S. producers than China’s tariff at this time, and it is hoped an updated NAFTA will soon be announced.
Producers were extremely pleased with the announcement of a new Korea-U.S. trade agreement (KORUS). South Korea is an important market for U.S. pork.
2. Farm Bill
Farm policy experts around town say before the farm bill can advance, the Omnibus must be completed. Additionally Congress will need to pass a disaster package, which may provide new funding for dairy and cotton producers. 
To keep moving forward on a new Farm Bill, the budget baseline must be identified and the cotton and dairy provisions need to be settled. Without these two items it is next to impossible to move forward on a new bill.
If the funding issues are resolved and the Congressional Budget Office can complete its reports on baseline findings, committee members will have some dollar figures to use as they address program funding.
USDA officials continue to evaluate existing programs as they look for ways to lower the budget needs of the department and prepare to address possible new program funding needs.
3. Flake Releases Hold on Doud Nomination
Arizona senator Jeff Flake released his hold on President Trumps’ nomination of Greg Doud to be the Chief Agriculture negotiator for the U.S Trade Repetitive office. 
Flake removed his hold after being reassured by Senate Finance Committee Chair Orrin Hatch and U.S. Trade Repressive Robert Lighthizer they would work with him on the seasonal produce proposal to NAFTA.  
Doud is the current president of the Commodity Council. He is well known in the livestock community having served as the chief economist for the National Cattleman’s Beef Association for eight years and later as a professional staff member of the Senate Agriculture committee.
4. USDA Nominations
President Trump added Ken Barbic to his list of nominees for key leadership positions at the USDA. Barbic is the current Sr. Director of Federal government affairs at Western Growers.   Barbic is well respected in agriculture circles following his tenure as the Deputy Assistant U.S. Trade Representative in the GW Bush administration.   
Still on the agenda is the consideration nomination of Stephen Vaden to be General Counsel of the Department of Agriculture. Friends on Capital Hill say some type of action needs to be take place soon on the Vaden nomination. The position is one that is key during the farm bill development proceedings.
5.  Waters of the United States
The tone of the discussion on the Waters of the United States will change now that EPA and U.S. Department of the Army finalized a rule that would delay implementation of the controversial 2015 Clean Water Rule for two years. Just as they have been in the past agriculture producers are expected to engage as the new rule is developed.  
The existing rule, which sought to clarify the definition of “waters of the United States” regulated under the Clean Water Act, was strongly opposed by agriculture, states and businesses. 
6. USDA Farm Bill and Legislative Principals
The announcement of the United States Department of Agriculture Farm Bill and Legislative principals in late January was viewed as a signal the Trump administration is ready to get a farm bill passed before the farm bill expire expires later this year.
The principals are based on the conversations Secretary of Agriculture Sonny Perdue had with farmers while traveling to 33 states since April of last year.