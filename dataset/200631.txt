Adding poultry litter or nitrogen to toxic fescue pastures grows more grass, but also boosts toxins in the grass.
A three-year study at the University of Missouri shows liming helps grass but doesn't increase ergovaline. That toxin harms grazing cattle many ways, mostly in lost production. In recent frigid cold, farmers reported cases of fescue foot caused by the toxin. Cattle losing their hooves must be put down.

"Before the study, we didn't know the impact of lime on toxin in infected fescue," said Sarah Kenyon, MU Extension agronomist, West Plains.

She completed her graduate degree with a study on a farmer's pasture in her area. The site was a 20-year-old stand of pure fescue with 98 percent infection rate. It was "hot" with toxin. Kenyon replicated her tests on 22 plots over three years.

No one had studied lime impact on fescue toxicosis, a major problem for grazing herds. The toxin is estimated to cause $900 million losses annually in U.S. cow herds.

"A major finding of her work is that liming causes no harm. We didn't know that," says Craig Roberts, MU Extension forage specialist. "We did know that nitrogen fertilizer fed the fungus living in the grass."

It was long known that adding lime is the first step to improve pasture fertility. Calcium boosts pH, which cuts soil acidity. This allows fertility to be released for grass roots.

"Nitrogen fertilizer boosts forage yields, as farmers have long known" says Rob Kallenbach, MU Extension agronomist. But there is a flip side, he says. "Nitrogen fertilizer also feeds the fungus, which in turn creates more toxins."

A common control of fescue toxicosis is to withhold nitrogen. That drops yields, which cuts gains on grazing livestock. That loss is on top of loss from fescue toxicosis.

Problems with toxic fescue can be solved by killing the old fescue and reseeding a new variety of novel-endophyte fescue.

Fescue must have an endophyte to survive insects, diseases, drought and overgrazing. The most widely grown grass across the southeastern United States is Kentucky 31 fescue. It happens to contain the toxic endophyte. Other endophytes found in nature do not make toxin.

Seed producers now use nontoxic novel endophytes. Many of these new fescues are sold by several companies.

The Alliance for Grassland Renewal, a cooperative group started in Missouri, promotes use of novel endophytes. One lesson for farmers is that endophyte-free fescue fails after a year.

Missouri plant breeders introduced an endophyte-free variety. Those varieties are nontoxic, but cannot survive.

The Alliance has been holding fescue schools across Missouri the last four years. Those teach spray-smother-spray eradication of old fescue. That is followed with management of the new plantings.

Grazing novel-endophyte fescue takes extra care. Cattle graze it to death, left untended. That is different from the toxic fescue. The endophyte causes both heat and cold stress. In summer heat, herds stop grazing. Animals go stand in ponds to cool their feet.

With novel endophyte, daytime grazing isn't lost. Cattle gain faster.

The heat stress also lowers conception rates with smaller calf crops for herds on toxic fescue.

This year the Alliance plans schools in three states: Kansas, Missouri and Kentucky. Kentucky was the first state to widely promote fescue before it was known to be toxic.

Extension services with Kansas State University and University of Kentucky help.

School dates and locations:
March 6, Mound Valley, Kan., at the Community Center.
March 7, Mount Vernon, Mo., at the MU Southwest Center.
March 9, Lexington, Ky., at the UK Veterinary Diagnostic Lab.
Each school runs from 9 a.m. to 5 p.m. Advance registration is required at all schools. Registration details are at grasslandrenewal.org.