Roger Schroeder, longtime vice president of produce for Stater Bros. Markets, plans to retire in January.“There comes a time when it is time,” Schroeder said with a laugh, noting he has been in the industry for 58 years.
Schroeder noted, as he did when The Packer recognized him in 2011 in its Packer 25, that a career in produce was not his original plan.
Many years ago, he started in the industry with a summer job that he took to get him through school with an engineering degree.
“It’s been a long summer,” Schroeder said.
Throughout his tenure, Schroeder has focused on flavor, from ordering preconditioned pears, avocados and melons to hunting for tastier varieties of strawberries.
Along with flavor itself, Schroeder has pursued consistency of flavor — a trickier task in produce, thanks to Mother Nature, than in consumer packaged goods.
Schroeder was named Produce Retailer of the Year in 2011 by The Packer’s Produce Retailer magazine, and was honored by The Packer in 1994 as Produce Marketer of the Year.
 
Before he joined San Bernardino, Calif.-based Stater Bros., Schroeder served in the same role for Hughes Markets, and he worked before that at Vons.
His advice to others in the industry would be to continue to pay attention to customer behavior. Figuring out what millennials want and need is of particular importance as the generation accounts for an increasingly large number of dollars being spent.
Suppliers typically do a good job keeping up with the macro trends, Schroeder said, so retailers should talk with them and consider how to apply findings to their specific audiences, keeping in mind factors like age, income and ethnicity.
Schroeder does not plan to go into consulting in his retirement. Instead, he expects to continue his hobby of photographing covered bridges.
There are about 1,000 in the U.S., and he has photos of about 350, so he should be able to stay busy for a while.