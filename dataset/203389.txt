When retail giants Wal-Mart and Target decided to step up their grocery games, many conventional retailers probably thought about what Barnes & Noble had done to independent bookstores and Starbucks to coffee shops and shuddered.
 
But in hindsight, we can see that many clearly did not. And some who were initially scared of getting muscled off the playground by the big kids have realized they can hold their own - and then some.
 
After all, there are plenty of independent bookstores and coffee shops out there still thriving.
 
Recent quarterly reports from Wal-Mart and Target seemed to paint different pictures. 
 
Fresh food gains at Wal-Mart helped produce 1.6% growth in the second quarter of 2016, while grocery sales at Target struggled, contributing to a 1.1% sales decline at the chain. 
 
On the whole, though, the message I got from analysts was pretty clear: both Wal-Mart and Target have a ways to go if they want their fresh produce departments, and grocery departments in general, to be successful.
 
The problems with Target start with scale, said Howard Davidowitz, chairman of Davidowitz & Associates Inc., a New York-based retail consulting and investment banking firm.
 
Target devotes about 20% of its stores to food, compared to Wal-Mart, which is closer to 50%. That's a problem for all food items, but especially perishable ones, Davidowitz said.
 
"The No. 1 problem at Target is produce. To do it, you've got to turn the merchandise, and they don't. They don't do enough business to support freshness and variety."
 
But while Wal-Mart may have Target beat on inventory, it has other problems, said Davidowitz and other analysts, including retail consultant David Livingston.
 
For starters, Livingston doesn't think consumers trust Wal-Mart produce the way they trust the fruits and vegetables they buy at other retailers.
 
"Wal-Mart is not that good, and their competitors are slowly eating away at them. People are scared of their perishables. Either Kroger is doing something right, or Wal-Mart is doing something wrong."
 
Kroger, Aldi, Sprouts, regional chains like Wegman's and H-E-B: these are some of the names that pop up when analysts talk about what gives Wal-Mart and Target nightmares.
 
Still, it's not all gloom and doom in the Goliath sector. David Marcotte, senior vice president of Boston-based Kantar Retail, said that if Target remembers "it's not a grocery store - it's grocery in a department store," it can make fresh produce work.
 
Bill Bishop, founder of Barrington, Ill.-based Willard Bishop Consulting and chief architect for brickmeetsclick.com, is similarly optimistic about Wal-Mart's produce future, citing gains in quality, merchandising and selling higher-margin items.
 
As someone who almost always shops at a conventional grocer but is grateful when I can snag a few groceries at Target and Wal-Mart when I'm there for something else, I wish them luck. If you know your niche and do a good job, there's room for everyone. 
 
Andy Nelson is The Packer's markets editor. E-mail him at anelson@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.