What do cows think of daylight savings? 'Not receptive'
What do cows think of daylight savings? 'Not receptive'

By JON KELVEYCARROLL COUNTY TIMES
The Associated Press

UNION BRIDGE, Md.




UNION BRIDGE, Md. (AP) — Fun bovine fact: Cows do not sleep in long, contiguous segments as do people and some other animals.
"Cows sleep 10 to 15 minutes at a time," said dairy farmer Katie Dotterer-Pyle.
Also, "Cow tipping is actually a myth. They don't sleep standing up. Horses do that, so you can go horse tipping," she said.
Nevertheless, Dotterer-Pyle said, cows do take issue with schedule changes, like the "springing forward" of the clocks this Sunday with the return of daylight savings time.
She and her husband David Pyle milk a herd of 350 cows at their Cow Comfort Inn Dairy, in Union Bridge.
"They are like most humans, they are not receptive to change," Dotterer-Pyle said. "They are very habitual animals, they like to do the same thing every day."
So when Dotterer-Pyle, her husband and a team of five employees come out to milk those cows an hour earlier on Sunday, some of them will react, a lot like humans.
"A few of them are just a little confused about what's going on," she said. "They are just being moody cows."
Aside from the feelings of her cows, Dotterer-Pyle said there's a practical result of that one-hour shift in the milking schedule, and it's not a positive one for the farm — they produce less milk.
"The way you get a cow to produce milk is you want that cow either eating, drinking or resting — it's a very leisurely life they live," she said. "They will have one less hour to do that."
The cows, like people, will eventually get over it, Dotterer-Pyle said.
Humans generally adjust to a one hour change in time in about one day, according to Dr. Amit Narula, medical director of the Sleep Disorders Center at Carroll Hospital.
Cows can take a little longer, according to Dotterer-Pyle, and for good reason.
"They are pretty big animals. They are anywhere from Jerseys, which are around 1,200 pounds, to Holsteins, which are about 2,000 pounds," she said. "They kind of have a lot to adjust."
But ultimately, the life of a dairy cow mostly consists of grazing and snoozing anyway, Dotterer-Pyle said.
"They live the life. It only takes 10 minutes to milk a cow," she said. "We milk twice a day here, 10 minutes in the morning and 10 minutes at night, so they only work 20 minutes a day."
Dotterer-Pyle enjoys teaching people about farm life and answering questions about dairy cows, even silly ones like their reaction to daylight savings time, so long as they are legitimate and are not "vulgar or rude." Cow Comfort Inn Dairy is on Facebook, where those interested can arrange a farm tour for $6; as well as Instagram and twitter, or follow the hashtag, #AskAFarmerNotGoogle.
"As an industry we are really trying to engage with the public about where their food comes from," Dotterer-Pyle said. "We want people to realize, Google doesn't milk cows, farmers do."