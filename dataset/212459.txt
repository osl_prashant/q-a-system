Yakima, Wash.-based Holtzinger Fruit Co. has changed its name to Fourth Leaf Fruit Co. 
“This is more than just a name change,” David Henze, president and chief grower success officer, said in a news release. “This is part of a bold new vision and support for our grower-first mission.”
 
The company has operated under the Holtzinger name since 1908. In 2004, the company divested its own fruit orchards and focused on fresh fruit packing. Today, the company is owned and supported by a Seattle investment group and managed by a group of agricultural professionals in Yakima, according to the release.
 
Henze said in the release that the fourth leaf is when a new apple tree begins to yield significant fruit.
 
The Fourth Leaf Fruit Co. will continue to market fruit under the Royal Purple, Regal Red and Adolfo’s Organic labels, according to the release.