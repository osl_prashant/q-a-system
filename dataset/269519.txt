AP-PA--Pennsylvania News Digest, PA
AP-PA--Pennsylvania News Digest, PA

The Associated Press



Good evening! Here's a look at AP's general news coverage today in Pennsylvania. For questions about the state report, contact the Philadelphia bureau at 215-561-1133. Ron Todt is on the desk. Editor Larry Rosenthal can be reached at 215-446-6631 or lrosenthal@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with updates.
MONDAY'S TOP STORIES:
BILL COSBY
NORRISTOWN. Bill Cosby's chief accuser spent late nights at the comedian's home, drove four hours to see him at a casino and called him twice on Valentine's Day, about a month after she says he drugged and molested her, jurors learned Monday as the defense sought to undercut her account. By Michael Sisak. SENT: About 910 words, photos.
With:
— BILL COSBY-THE LATEST.
STARBUCKS-BLACK MEN ARRESTED
PHILADELPHIA — About two dozen chanting protesters have entered a Philadelphia Starbucks where two black men were arrested after store employees called 911 to say they were trespassing. The protesters moved to the front counter shortly after 7 a.m. Monday and chanted "Starbucks coffee is anti-black" and "We are gonna shut you down." By Kristen DeGroot. SENT: About 400 words, photos.
With:
— Q&A-STARBUCKS CEO
MEEK MILL
PHILADELPHIA — Rapper Meek Mill's drug and gun convictions should be thrown out and he should be granted a new trial, prosecutors said in court Monday in an announcement that led his supporters outside to break into an impromptu dance party. By Natalie Pompilio. SENT: About 410 words.
GOVERNOR 2018-PENNSYLVANIA
HARRISBURG — Laura Ellsworth is trying to take advantage of back-and-forth TV attack ads in Pennsylvania's Republican gubernatorial primary to win votes, as rivals in the three-way contest clashed Monday over fundamental issues for leading business groups. By Marc Levy. SENT: About 660 words.
MEDICAL MARIJUANA
HARRISBURG — Patients in Pennsylvania's medical marijuana program should be able to obtain the drug in dry leaf or flower form for vaporization by sometime this summer, the Wolf administration announced Monday. By Mark Scolforo. SENT: About 410 words.
DOWN SYNDROME-ABORTION
HARRISBURG — A proposal to prohibit abortions in Pennsylvania when the sole reason is that the fetus has or may have Down syndrome is headed to the state Senate after passing the House by a comfortable margin.
UNIVERSITY STABBING
BINGHAMTON, N.Y. — A 19-year-old freshman was fatally stabbed on New York's Binghamton University campus and his assailant remained at large Monday, authorities said. SENT: About 350 words.
MULTISTATE INTERNET POKER
ATLANTIC CITY — Internet poker players in three states will be able to face off against each other starting May 1, in a long-awaited expansion of online gambling. Two poker brands of Caesars Interactive Entertainment, WSOP.com and 888Poker.com, plan to go live on that date in New Jersey, Nevada and Delaware, the company told The Associated Press on Monday. By Wayne Parry. SENT: About 680 words.
EXCHANGE:
EXCHANGE-CYCLISTS-SEPT 11
SHANKSVILLE — Each of the five members of the September 11th National Memorial Trail Alliance advisory board who pedaled their bikes through Somerset and Cambria counties on Sunday wore a red, white and blue windbreaker with one star on the back. The image was meant as a symbol of unity for the group, which is taking the first-ever bicycle tour to the three sites involved in the Sept. 11, 2001, terrorist attacks: a field in Somerset County, the former site of the World Trade Center in New York City and the Pentagon in Arlington, Virginia. Dave Sutor, The (Johnstown) Tribune-Democrat. SENT: About 540 words.
IN BRIEF:
FAMILY SICKENED — An ex-manager of a pest-control company in the U.S. Virgin Islands faces criminal charges of poisoning four members of a Delaware family who were exposed to a pesticide in their vacation rental.
XGR--DOMESTIC ABUSE-BAIL — A new Pennsylvania law is in place to help guide judges setting bail for defendants accused of domestic abuse.
GUN PERMIT-RECIPROCITY — Pennsylvania is recognizing the concealed-carry gun permits issued in two more states, but it is dropping its recognition of permits from Virginia.
ESCAPED PRISONER-CREEK — Prosecutors say they are investigating the death of a handcuffed prisoner who broke away from law enforcement officers and was later found dead in a Pennsylvania creek.
KARATE INSTRUCTOR-ASSAULT — A former karate instructor has been convicted of assaulting students in western Pennsylvania.
HOUSE 2018-PENNSYLVANIA — Democrat Conor Lamb's last remaining primary opponent is now supporting him, clearing the way for the newly sworn-in Pennsylvania congressman to win the party nomination to challenge third-term Republican Rep. Keith Rothfus in November.
TRAFFIC DEATHS — Pennsylvania traffic deaths are at their lowest number since such records began to be kept in 1928.
FIREFIGHTERS HURT-BRUSH FIRE —Two firefighters were hurt in falls while battling a brush fire in central Pennsylvania, and two others were treated for dehydration and heat exhaustion.
SEPTA OFFICER-DOG GONE — A Pennsylvania transit police officer and his family will be reunited with the police dog they say helped them heal after the death of a son.
SPORTS:
BKN-HEAT-76ERS
PHILADELPHIA -- The Philadelphia 76ers try and take a 2-0 lead against the Miami Heat in the playoffs. Joel Embiid won't play again. By Dan Gelston. UPCOMING: 650 words, photos. Game starts at 8 p.m. EDT.
BBN--PHILLIES-BRAVES
ATLANTA — The Philadelphia Phillies go for their seventh straight win as Aaron Nola faces Julio Teheran. Philadelphia is 9-5. The Braves are 8-6. By George Henry. UPCOMING: 650 words, photos. Game starts at 7:35 p.m. EDT.
FBN--HARRISON RETIRES
PITTSBURGH — James Harrison is taking another shot at retirement, and this time it's likely to stick for the longtime Pittsburgh Steelers linebacker. SENT: About 570 words.
OBIT-GREER
PHILADELPHIA — Hal Greer, a Hall of Fame guard and the Philadelphia 76ers' career leading scorer, has died. The Sixers said Greer died Saturday night in Arizona after a brief illness. He was 81.
___
If you have stories of regional or statewide interest, please email them to phillyap@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.