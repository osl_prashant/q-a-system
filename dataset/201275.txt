Crop calls 
Corn: 4 to 6 cents higher
Soybeans: 6 to 8 cents higher
Winter wheat: 4 to 6 cents higher
Spring wheat: 10 to 15 cents higher

Grain and soybean futures benefited from short-covering overnight after heavy fund selling the previous two days. Corn, soybean and winter wheat futures are working on sharp weekly losses and traders' focus will be on evening positions ahead of the weekend. Overnight gains brought spring wheat futures slightly above last Friday's closing level as traders remain concerned about the size of the crop. Also this morning, USDA announced China has purchased 1.3 MMT of new-crop soybeans -- the 7th largest daily sale on record.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Livestock futures are expected to see choppy price action as traders even positions ahead of the weekend. Cattle futures are working on weekly gains thanks to this week's steady to $2 higher cash cattle trade. Beef prices have declined sharply this week, but movement has picked up. Meanwhile, pork movement has declined, signaling elevated prices are curbing demand. The cash hog market has stabilized this week, adding to expectations a seasonal top is near.