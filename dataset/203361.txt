Registration is open for a Produce for Better Health web seminar on food waste.
The free event, called "Weighing in on Wasted Food - Part 1," is scheduled for Jan. 11 at 2 p.m. to 3 p.m., according to a news release. Registration is available online.
The hour-long event is geared toward dietitians and other health professionals who work with consumers to improve diets by increasing fruit and vegetable consumption, according to the release.
Elizabeth Pivonka, CEO for the Produce for Better Health Foundation, will moderate. The primary speaker will be Chris Vogliano, former nutrition manager at The Greater Cleveland Food Bank and current clinical research associate at the scientific wellness startup Arivale.
The session will cover several topics, according to the release:
Identify where food waste occurs along the food supply chain;
List three benefits of reducing food waste;
Understand how to implement food waste reduction strategies.