Immigration raid: Guatemalan worker arrested on dairy farm
Immigration raid: Guatemalan worker arrested on dairy farm

The Associated Press

ROME, N.Y.




ROME, N.Y. (AP) — A dairy farmer says seven federal immigration officers came onto his property without permission, arrested a Guatemalan worker without producing a warrant and handcuffed the farmer when he video-recorded their actions.
Sen. Kirsten Gillibrand, a Democrat, tweeted that she was "deeply troubled" by what happened and called for an immediate investigation.
Farmer John Collins said officers handcuffed worker Marcial de Leon Aguilar in his milk house in central New York on Wednesday morning. Aguilar lives on the farm with his wife and children. Collins said Aguilar has proper documentation to work there and has taxes withheld from his paychecks.
U.S. Immigration and Customs Enforcement released a statement Thursday saying Aguilar was lawfully arrested by deportation officers under an "administrative arrest warrant for immigration violations." ICE said it has removed Aguilar from the U.S. three times, most recently in January 2014.
Officials said Aguilar has criminal convictions for reckless aggravated assault and illegal re-entry.
Collins said Aguilar's pregnant wife recently was caught crossing the U.S.-Mexico border illegally with the couple's four children and has been meeting with ICE officers as she seeks asylum because of violence in Guatemala. Guatemala, which is in Central America just south of Mexico, has been ravaged by shootings and gang brutality in recent years.
Collins said he tried to take photos and video as Aguilar was led to a vehicle but officers grabbed his phone, threw it in the road and handcuffed him, threatening to arrest him for hindering a federal investigation. He said they released him before driving off with Aguilar.
"This was something you see on TV," Collins told the Syracuse Post-Standard . "You don't expect it to be here."
Gov. Andrew Cuomo, also a Democrat, said he's concerned with a "dramatic increase" in ICE raids and "overly aggressive tactics." He didn't specifically reference Aguilar's arrest.
Cuomo said this year's state budget allocates $10 million to the Liberty Defense Fund, a program launched last year to provide legal assistance for immigrants targeted by ICE raids.