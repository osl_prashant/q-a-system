By DCHA Board of Directors
Learn how to take your calf and heifer program to the next step on the podium by attending the 2018 Dairy Calf and Heifer Association (DCHA) annual conference. With the theme “One team. Gold dreams.” the conference is set for April 10-12 in Milwaukee, Wis. 
At the conference, learn how to take your calf and heifer management to the next level, gain insight from industry-leading experts, engage in hands-on farm tours, experience an industry-focused tradeshow and network with peers.  
Four reasons why you should attend

Learn about hot topics. Breakout sessions will cover a wide range of hot topics including: calf immunity, group housing, ventilation systems, managing employees, feed center management, feedlot management and cattle transportation. Topics are identified based on last year’s attendee feedback and industry trends. Learn from the best in the business.
Get an outside perspective. Learn how to use influence and integrity to inspire and engage others. Regardless of your role on-farm or in the industry, great leadership is the driving force behind your growing business. Learn more about “The Power of Influence” through keynote speaker, Ty Bennett.
Network with your peers. Network with other calf and heifer raisers from across the U.S. and world. All aspects of calf and heifer raising will be represented at the conference. Take time to learn from your peers during meals, between sessions or during evening social events. 
Enjoy a new location. This year’s new conference location in Milwaukee, Wis. makes for easy travel arrangements. All conference activities will be held at the Potawatomi Hotel and Casino, except the industry and farm tours.

Farm tour
Prior to the conference, participants will have the opportunity to view calf and heifer facilities and learn about management practices at a Wisconsin dairy farm. 
At Vir-Clar Farms, a 2,000-cow dairy, exceptional calf care protocols and cleanliness are key to calf management success Hear from the dairy’s calf manager, Katie Grinstead, about their approach to getting calves and heifers off to the best start possible.
Industry tours

New to this year’s conference are industry tours. Conference attendees can select one of two tours to attend:
 

STgenetics: A leader in bovine genetics, you will get a glimpse into the research and technology that supports the growing industry. Hear from the experts on the latest in genomic testing and sexing technology.

 

Milk Products: A behind-the-scenes tour of milk replacer manufacturing. Learn about the research and expertise that goes into calf milk replacer, and milk replacer for a variety of species.

 
ARPAS Credits
The conference has been pre-approved by the American Registry of Professional Animal Scientists for up to 11 continuing education units. Participants should request credits for this event at arpas.org.
Make plans to attend
To register and for a full conference schedule with session descriptions, visit calfandheifer.org. 
DCHA Member Network

The DCHA member network is a private Facebook discussion group, exclusive to members. It was developed to ensure the valuable peer networking you experience at the DCHA conference is available year-round. The network provides members a continuous platform to connect with fellow DCHA members, ask questions and discuss calf and heifer raising topics.
Join here: https://www.facebook.com/groups/DCHAmembers/
Gold Standards
The Gold Standards provides a framework for raising dairy calves and heifers, including research-based benchmarks and best management practices. Have you seen the new edition? The Gold Standards are available online for download in the Members Only section of our website. You can also request a printed version by completing our online form at calfandheifer.org.