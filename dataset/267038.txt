AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:15 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
TOP STORIES:
EXPLOSIVES THREAT
DENVER — A man who barricaded himself inside a minivan with propane tanks strapped to its roof tried to enter an Air Force base on Tuesday before being arrested, Colorado authorities said. Police found no explosive material inside the van and no charges will be filed against the man, El Paso County Sheriff's Office spokeswoman Jacqueline Kirby said. By Kathleen Foody and Dan Elliott. SENT: 450 words.
KASICH-2020
COLUMBUS, Ohio — Republican Ohio Gov. John Kasich was set to return Tuesday to New Hampshire, where a second-place finish in the 2016 presidential primaries gave him a welcome boost in national popularity. Such moves bolster speculation that Kasich is considering a challenge to Republican President Donald Trump in 2020. Kasich has joined Democratic Colorado Gov. John Hickenlooper in spearheading a series of policy compromises on key issues. By Julie Carr Smyth. SENT: 480 words, photo.
NATIONAL PARKS-FEE INCREASE
WASHINGTON — The Interior Department is backing down from a plan to impose steep fee increases at popular national parks in the face of widespread opposition from elected officials and the public. The plan would nearly triple entrance fees at 17 of the nation's most popular parks, including the Grand Canyon, Yosemite, Yellowstone and Zion, forcing visitors to pay $70 per vehicle during the peak summer season. By Matthew Daly. SENT: 410 words, photos.
ARIZONA STATE MOTTO
PHOENIX — Lawmakers in Arizona's House of Representatives on Tuesday clashed over whether public schools should be allowed to display the English translation of the state motto — "God Enriches" — and whether doing so blurs the line between church and state. The Republican-backed proposal would allow teachers to read or display state's motto, "Ditat Deus," which translates from Latin to "God Enriches." After passing along party lines in the state Senate, it passed a preliminary voice vote in the House Tuesday, and awaits another House vote before possibly heading to Gov. Doug Ducey's desk. By Melissa Daniels. SENT: 570 words.
IN BRIEF:
— OFFICER STABBED-CRUISER — Police in southern Colorado are looking for a person who stabbed an officer and took off in his marked cruiser.
— COLORADO WILDFIRES-THE LATEST — Authorities are still assessing the damage caused by a wildfire near Grand Junction but say at least one home was destroyed.
— MOTHER KILLED — Jurors are deciding the fate of a man being re-tried for killing his wife in western Colorado.
— MULTI-STATE BANK ROBBER — A Colorado man who's already serving time for an Arkansas bank robbery has pleaded guilty to robbing a Kansas bank.
— ANADARKO-ABANDONED WELLS — An oil and gas company has notified Longmont officials of plans to plug and abandon several wells near the northern Colorado city.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for May rose 11.5 cents at 4.5750 a bushel; May corn was up 1.25 cent at 3.8850 a bushel; May oats gained 1.75 cents at $2.3250 a bushel; while May soybeans was higher 2.5 cents at $10.3800 a bushel.
SPORTS:
PACERS-NUGGETS
DENVER — The surging Denver Nuggets continue on their quest to earn a postseason berth Tuesday night against an Indiana team that's already locked up a spot. By Pat Graham. UPCOMING: 650 words, photos. (Game starts at 7 p.m. MT)
ROCKIES-PADRES
SAN DIEGO — Tyson Ross returns to the Padres after a year away and starts against the Colorado Rockies, trying to lead the Padres to their first victory of the season. The Rockies counter with Kyle Freeland. By Bernie Wilson. UPCOMING: 600 words, photos. (Game starts at 8:10 p.m. MT)
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.