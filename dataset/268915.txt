China traders, farmers shrug off risk of US soybeans tariff
China traders, farmers shrug off risk of US soybeans tariff

The Associated Press

SHANGHAI




SHANGHAI (AP) — Chinese soybean farmers, importers and processers say they are unconcerned about potential Chinese tariffs on American soybeans in an escalating dispute with the administration of President Donald Trump over trade and technology.
Chinese soy growers at an industry expo in Shanghai this week were cheering the proposed 25 percent Chinese tariff on American soybeans announced last week that could slash U.S. soy exports by 70 percent.
They relish the prospect of new business and higher prices, while farmers from Iowa to Indiana fret over losing their biggest overseas customer. Chinese soy traders and processers say they believe tariffs could be an opportunity to curb dependence on the U.S. and find alternative soy suppliers.
China has already been looking for other soy sources in places like Brazil, Canada and Russia.