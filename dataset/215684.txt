Central American melon imports have increased in volume and value steadily during the past 10 years, trade statistics show.
For the upcoming season, one major importer estimates volume should be close to last season.
The U.S. Department of Agriculture reports that U.S. imports of melons from Central America have climbed from 593,000 metric tons in 2006 to 698,000 metric tons in 2016, a gain of 18% over 10 years.
The value of the U.S. imports of Central American melons rose 90% over the same decade, from $188 million in 2006 to $358 million in 2016.
While imports of melons have been rising modestly, per capita use numbers show the melon category has slipped some in per capita consumption.
“It looks like volume should be about the same as the past year,” said Brad Edmonds, salesman with Pompano Beach, Fla.-based Central American Produce Inc.
Edmonds said Dec. 11 the company was expected to bring in cantaloupes and honeydew from Honduras in mid- to late December.
The company will begin its seedless watermelons out of Guatemala and Honduras by early January.
Central American Produce, the first importer to bring melons from Central America more than 40 years ago, sources cantaloupes and honeydew from Honduras from Christmas to May, with imports of seedless watermelon from Guatemala and Honduras active at the same time.
USDA statistics show per capita use of cantaloupes has declined from 9.25 pounds in 2006 to 7.12 pounds in 2016.
Honeydew per capita use has dropped from 1.88 pounds per capita in 2006 to 1.60 pounds per capita in 2016.
The category of “other melons” per capita use has risen slightly, from 0.60 pounds in 2006 to 0.68 pounds in 2016.
By percent of total volume, the timing of U.S. imports of Central American melons begins in November (6% of annual imports) and gains seasonal volume in December (15%), January (14%) and February (16%) and peaks in March (23%) and April (20%) and tails off in May (6%).

Guatemala accounted for 445,000 metric tons in U.S. imports in 2016, or about 64% of total Central American melon imports that year.
Honduras accounted for 189,000 metric tons of U.S. melon imports in 2016, or 27% of total U.S. imports of Central American melons.
Costa Rica accounted for 62,000 metric tons of melon imports in 2016, or about 9% of total Central American melon shipments to the U.S.

The average weekly f.o.b. price (mostly high price) in South Florida for size 12 imported cantaloupes from Nov. 26 to May 20 was $11.68 per carton, according to the USDA Market News Service.
For honeydew melons, the weekly average f.o.b. price in South Florida for imported size 5 fruit (mostly high price) was $10.65 per carton from Dec. 12 to May 20.