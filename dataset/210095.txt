By: Marilyn Thelen, Michigan State University Extension
Fall is a time of changing colors, pumpkins, football games and harvest. Whether driving a piece of farm equipment or car, it is important to understand that speed and reaction time are two major factors in safely navigating country roads this time of year.
So let’s look at the numbers.
Speed. A car traveling 60 mph with a reaction time of 1 second will be able to stop in 304 feet. This is nearly the length of a football field and can take up to 7 seconds.
Reaction time. Reaction time is influenced by two factors: when you see it and how quickly you react. Farmers put slow moving vehicle emblems and flashing lights on equipment to improve visibility. Studies have shown texting can double the time to react. If the average reaction time is 1-2 seconds, a distraction such as a phone has now increased the delay to 3-4 seconds. So whatever the delay—didn’t see it or slow to react—with a 4 second delay in hitting the breaks, the vehicle traveling 60 mph now requires 568 feet to come to a stop, over 1.5 times the length of a football field. That is a long ways down the road and a potential accident!
Whether you are the slow moving vehicle working to harvest the crops or the car out enjoying the fall colors and the football games, keep these tips from Michigan State University Extension in mind.
Motorists:

Slow down. The slower you drive, the shorter the stopping distance. When speed doubles from 30 mph to 60 mph, the stopping distance more than triples.
Wait for a safe place to pass. Never pass with limited visibility or in a no-passing zone, and make sure the farm equipment is not turning left.

Farmers:

Machinery should display a slow moving vehicle emblem when traveling under 30 mph.
Use proper lighting on farm equipment, including flashing amber lights in the front and rear. Use lights at all times of the day and night.

Both:

Look down the road as far as possible to be aware of what is coming and increase your warning time. At 60 mph, a vehicle is covering 88 feet per second.
Distractions can double your reaction time. Pay attention while on the road.
Respect each other! Both motorists and farmers have places to go and things to do.

Fall is a beautiful time to get out and see the colors, take in a football game and enjoy the countryside. With a little patience, everyone can share the roads safely.