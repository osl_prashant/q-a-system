After nearly three decades in the business, Danny Dumas said he is still attracted to the daily challenge the industry gives.
“In produce you never know what to expect — it is a constant challenge,” said Dumas, vice president of sales and product management for Del Monte Fresh Produce NA.
Part of the challenge is meeting consumer needs in news ways, such as with millennial consumers.
Selling commodities is not just about quality and quantity, but taking into account the convenience-oriented consumer that also cares about social accountability.
“You need to be able to demonstrate what good you are doing for the planet and your workers,” he said.
Working for Del Monte for the past 20 years, Dumas began his career with Dole Fresh Fruit Co. in 1989. He worked for nine years with Dole, finishing with the role of district sales manager for Dole’s Montreal office.
After his time at Dole, Dumas had a brief stop with a wholesaler in Montreal, but after just a few months Del Monte asked him to open their office in Montreal.
Emanuel Lazopoulos, senior vice president of North American sales, marketing and product management for Del Monte Fresh Produce NA, said Dumas is a dynamic person who embraces his job, the produce industry and living in the U.S.
“He is fully committed to the company, he is fully committed to the produce industry and fully committed to the nation as whole,” he said.
Dumas is secretary-treasurer on the United Fresh Produce Association board of directors.
Serving on the board of directors and the executive committee at United Fresh gives him a reminder of the challenges the industry faces.
For example, Dumas said one of the big challenges for the fresh produce industry is dealing with different audit requirements, which layer in more cost and time to prepare for audits.
Susan Reimer-Sifford, United Fresh chairwoman and vice president and general manager for CC Kitchens at Castellini Group of Cos., said Dumas brings produce expertise, a sense of humor and sensibility to the United Fresh Board.
“Danny is willing to give his time, talent and expertise to newcomers in the produce industry and lead the way for up and coming members,” Reimer-Sifford said.
Dumas, a native of Quebec, said his role opening Del Monte’s Canadian office was good for the company and for him professionally.
Dumas’ leadership in Canada opened the door to new opportunities in the U.S. with Del Monte, with a move to the U.S. in 2006 to take the lead for the banana and pineapple program in North America.
Dumas also had the chance to move to Europe with the fresh division in Monaco in 2010, but he returned to the U.S. in 2013 because of family considerations.
In his role as Del Monte’s vice president of sales and product management, Dumas is responsible for the business in North America, serving as the link between production and sales.
His No. 1 management principle is to be open to new ideas.
“No ideas are stupid. Being stupid is having no ideas,” he said.