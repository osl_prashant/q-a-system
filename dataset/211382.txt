Chinese meat importers are racing to get their hands on the first shipments of beef from the U.S. in 14 years, as strong demand for premium steaks continues to grow in the $2.6 billion beef import market.China and the United States last week settled the conditions for American beef exports after the two sides agreed in May to resume the trade. Pent-up demand for U.S. meat could erode sales of Australian beef, China's current top supplier of premium steaks.
"We have ordered 56 to 58 tonnes of whole carcasses, which are expected to arrive by the end of July," said Chen Fugang, owner of Aoyang International, a Shanghai-based trading company.
Chen said he expects the product to be a hit in the Chinese market, where total beef sales grew around 4 percent last year to reach 5.9 million tonnes, according to Euromonitor.
"We're especially interested in several barbecue products, like rib eye and fillet steak, which we believe Chinese customers would like," added Chen.
American beef is known for its quality in China, but was banned in 2003 after a mad cow disease scare. Since then, other beef imports have surged, as domestic production has struggled to keep up with demand from the expanding middle class.
Total beef arrivals rose 22 percent to 579,836 tonnes last year and foreign suppliers will meet about 20 percent of demand by 2020, forecasts Rabobank.
"The number of enquiries to our exporters number in the hundreds, if not low thousands, since the announcement of the agreement," said Joel Haggard, senior vice president for the U.S. Meat Export Federation in Asia-Pacific.
Increased competition for the lucrative premium market will stir concerns in Australia, where a drought has cut the herd size.
Similar quality cuts of U.S. beef are expected to be cheaper than Australian meat because of low U.S. grain prices, a large component of the cost of raising cattle.
Chen, who sells Australian and New Zealand beef to five-star hotels and high-end restaurants, declined to reveal the price for his U.S. cargoes, adding that customs and handling fees still needed to be factored in.
But the product would be cheaper than Australian beef, he added.
"Price is a key (selling) point, as U.S. beef is cheaper than Australian beef of the same quality," he said.
Despite the high interest, strict Chinese import conditions will limit shipments of American beef initially. And U.S. prices are also high currently, warned the Meat Export Federation's Haggard.
"This isn't going to be a wave of product coming in. It's going to roll in slowly," he said.
Australian beef will still see strong demand, said Michael Finucan, general manager of international markets at industry body Meat & Livestock Australia.
Australia's grass-fed beef is seen as "more green" than American in some markets, he said. And, unlike the U.S., the country can export refrigerated beef to China, tapping into demand from high-end retailers.
However, for some, U.S. beef has the edge.
"I have been in this industry for a long time. In my view, U.S. beef tastes more tender," said Aoyang's Chen.
(Reporting by Dominique Patton and Beijing Newsroom; Editing by Christian Schmollinger)