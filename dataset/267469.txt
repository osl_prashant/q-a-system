Tyson Foods expanding safety programs to 12 poultry plants
Tyson Foods expanding safety programs to 12 poultry plants

The Associated Press

OMAHA, Neb.




OMAHA, Neb. (AP) — Tyson Foods is expanding the safety programs it has been testing in its beef plants for several years to 12 poultry plants because of the results.
The Springdale, Arkansas-based company said Thursday it developed the measures in cooperation with the United Food and Commercial Workers International union.
The policies empower workers to stop the production line if they see a safety issue, and workers are involved in plant safety committees.
Tyson Fresh Meats President Steve Stouffer says there has been a measurable decrease in injuries and turnover at plants using the measures over the past three-to-five years.
Mark Lauritsen with the union says it's clear Tyson values worker input.
Tyson officials say the company has been working closely with the union for three decades since it began developing ergonomics programs.