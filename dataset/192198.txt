Farm diesel was unchanged at $2.02.





This week, our farm diesel/heating oil spread is at 0.50 indicating sideways to lower near-term price action.





Propane firmed a penny to $1.25 per gallon.