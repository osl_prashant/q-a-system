Researchers at the Swiss Federal Laboratories for Materials Science and Technology (Empa) have developed a “fake” fruit that promises new insights on core fruit temperatures during commercial transit. 
Swiss researchers have developed several versions of the artificial fruit sensors, according to a news release. Researchers are looking for potential industry partners to develop the sensors commercially. The project was funded by the Swiss-based Commission for Technology and Innovation.
 
Spy fruit
 
The replica fruit sensor is the same shape and size as the real version of the fruit and also mirrors its composition, researchers said, and the “spy fruit” can be packed in boxes of real fruit to record data during transport.
 
After the fruit sensor arrives with the load, information can be retrieved easily, according to the release.
 
“We analyzed the sensors in the Empa refrigeration chamber in detail and all the tests were successful,” project leader Thijs Defraeye from the Laboratory for Multiscale Studies in Building Physics said in the release.
 
He said field tests are ongoing at the Swiss research facility Agroscope in Wädenswil.
 
Defraeye said in the release that researchers are developing separate sensors for each type of fruit, and even for different varieties within the same fruit species.
 
Sensors have been developed so far for braeburn and jonagold apple varieties, the kent mango, oranges and the classic cavendish banana. Based on the characteristics of each type of fruit, the fruit is X-rayed, and a computer algorithm creates the average shape and texture of the fruit, according to the release.
 
From there, researchers determine the composition of the fruit’s flesh and simulate the flesh with a mixture of water, carbohydrates and polystyrene, according to the release.
 
After the fruit is molded and produced on a 3D printer, the sensor is placed inside the fruit, where it can record data — notably the core temperature of the fruit, according to the release. Defraeye said the system provides better data than existing devices that measure core temperatures.
 
The cost of the reusable fruit sensor is expected to be less than $50 each, according to the release. Capabilities like real-time data transmission haven’t been engineered into the sensor yet, according to the release.