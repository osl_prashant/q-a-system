If you haven’t experienced a freeze yet this fall, you soon will. And remember, a freeze can cause hazards for using some forages.

When plants freeze, changes occur in their metabolism and composition that can poison livestock. But you can prevent problems.

Sorghum-related plants, like cane, sudangrass, shattercane, and milo can be highly toxic for a few days after frost. Freezing breaks plant cell membranes. This breakage allows the chemicals that form prussic acid, which is also called cyanide, to mix together and release this poisonous compound rapidly. Livestock eating recently frozen sorghums can get a sudden, high dose of prussic acid and potentially die. Fortunately, prussic acid soon turns into a gas and disappears into the air. So wait 3 to 5 days after a freeze before grazing sorghums; the chance of poisoning then becomes much lower.

Freezing also slows down metabolism in all plants. This stress sometimes permits nitrates to accumulate in plants that are still growing, especially grasses like oats, millet, and sudangrass. This build-up usually isn't hazardous to grazing animals, but green chop or hay cut right after a freeze can be more dangerous.

Alfalfa reacts two ways to a hard freeze, down close to twenty degrees, cold enough to cause plants to wilt. Nitrate levels can increase, but rarely to hazardous levels. Freezing also makes alfalfa more likely to cause bloat for a few days after the frost. Then, several days later, after plants begin to wilt or grow again, alfalfa becomes less likely to cause bloat. So waiting to graze alfalfa until well after a hard freeze is a good, safer management practice.

Frost causes important changes in forages so manage them carefully for safe feed.