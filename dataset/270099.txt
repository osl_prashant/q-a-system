Chicago Mercantile Exchange live cattle futures rose on Wednesday, supported by steady-to-higher early-week cash prices, traders said.
Hogs drew support from higher cash and wholesale pork prices. Futures were further supported by lower week-over-week hog weights.
“(It is a) good sign that these weights are coming down,” said Dan Norcini, an independent CME livestock futures trader. “I am wondering if this week’s cold weather across the northern tier of the country will take them down a bit more.”
On the cattle front, a small number of animals at Wednesday’s Fed Cattle Exchange brought $120 - $122 per cwt. A week ago, slaughter-ready, or cash, cattle in the U.S. Plains moved at $116 to $122.
April live cattle closed 0.925 cent per pound higher at 118.975 cents. June ended up 0.375 cent at 105.275 cents.
April CME feeder cattle rose 0.025 cent per pound to 138.750 cents.
May hogs closed up 1.950 cents per pound at 69.850 cents. Most actively traded June ended 1.775 cents higher at 78.525 cents.