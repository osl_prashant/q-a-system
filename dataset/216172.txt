Hamburger chain Wendy’s Co laid out plans on Friday to trim the use of antibiotics that are important to human medicine from its beef supply, the latest step by a food company to fight concerns about resistance to the drugs in people.
Starting in 2018, the company will buy about 15 percent of its beef from producers that have each pledged to reduce by 20 percent their use of Tylosin, the one medically important antibiotic they routinely feed to cattle, according to Wendy‘s.
Wendy‘s, which says it is the world’s third-largest quick-service hamburger chain, plans to increase the amount of beef it purchases from these producers and others that raise cattle in similar ways.
The company also said it finished removing antibiotics important to human medicine from its chicken supply, after pledging to do so last year.
“Moving away from routine antibiotic use in their beef production is certainly welcome, and we’d urge them to move quicker,” said Matt Wellington, antibiotics program director for advocacy group U.S. PIRG.
Scientists and public health experts for years have warned that the regular use of antibiotics to promote growth and prevent illness in healthy farm animals contributes to the development and spread of drug-resistant superbugs that can infect people.
Last month, the World Health Organization recommended that meat producers end such practices.
There is “a growing public health concern about antibiotic resistance,” Wendy’s said, adding that the company believes it “could help by reducing or eliminating antibiotic use in our food supply.”
In the United States, the sale and distribution of antibiotics approved for use in food-producing animals fell by 10 percent from 2015 to 2016, in the first such decline since the Food and Drug Administration started tracking the data in 2009.
The drop came as restaurants such as McDonald’s Corp and meat suppliers inducing Tyson Foods Inc backed away from using antibiotics in U.S. chicken supplies.
Removing antibiotics from cattle is more difficult, experts said, because the animals live longer than chickens and have more chances to fall ill.
In August, Consumers Union, the policy division of Consumer Reports, said McDonald’s told the group that the company hoped to have a timeline soon for reducing medically important antibiotics from its beef supply.
McDonald‘s, the world’s largest restaurant chain by revenue, says on its website it prefers beef raised with a “responsible use of antibiotics.”
Wendy’s plan is more concrete, Wellington said.
“It’s starting to set the example of what the industry should be following,” he said.