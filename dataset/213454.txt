The countdown for comments on the Waters of the U.S. Rule has begun, and you have only 24 hours left to share your views. The comment period ends tomorrow, Sept. 27.
A statement from Illinois Farm Bureau President Richard Guebert, Jr., notes that the Bureau believes the 2015 “rule was fatally flawed, allowing the government far too much latitude to oversee private property.”

 

The rule, which was halted by the courts, had more to do with controlling land than protecting water and only created confusion for farmers, notes American Farm Bureau Federation senior director of regulatory relations Don Parrish.
A number of Agriculture associations and groups challenged the 2015 rule in court. The Sixth Circuit Court of Appeals is determining whether it has jurisdiction in the matter
Guebert adds, “We look forward to working with the agencies in the future to write a new rule that protects our nation’s waters without penalizing farmers.”
Learn more about the rule and submit your comments here, at the EPA’s website.