The cranberry industry is looking to foodservice as a path to sales growth, marketers say.
"We believe there is lots of potential in this category," said Kellyanne Dignan, senior manager of corporate communications with Lakeville-Middleboro, Mass.-based Ocean Spray Cranberries.
Ocean Spray marketers are working with industry allies to reach chefs through recipes and contests to encourage them to introduce fresh cranberries into their menus, Dignan said.
"Ingredient versatility is key here, and the fact that the cranberries' bold, unique taste profile pairs well with everything from chocolate to jalapenos provides chefs with a wonderful ingredient to spur their creativity," Dignan said.
Making inroads in restaurants, hospitals and even schools is a worthy goal that will require a lot of effort, said Mary Brown, owner of Glacial Lake Cranberries and Honestly Cranberry in Wisconsin Rapids, Wis.
"There's always potential; it's about people getting the word out," she said.
Brown said she and her staff are working to spread to word, particularly with Honestly Cranberry's unsweetened dried cranberries.
"We spend a lot of our days explaining who we are, what we are and why people need to buy Honestly cranberries," she said.
Cranberries have found their way into foodservice mostly in the form of juice, sauces and drinks, marketers note.
"You see a lot of pre-made cranberry sauce that's sold in large packages or sweet and dried cranberries or juice that goes to bars as a mixer," said Bob Wilson, managing member of Wisconsin Rapids-based The Cranberry Network LLC, which markets fruit grown by Tomah, Wis.-based Habelman Bros. Co.
There's plenty of demand for cranberries in the foodservice category, but there are challenges involved, Wilson said.
Now, it's time for fresh product to build a foodservice following, Wilson said.
"With foodservice, as long as you're going into a category that doesn't mind some preparation and you work with your targeted client on recipes and so forth, it's very doable," he said.
However, Wilson said, The Cranberry Network hasn't spent "an inordinate amount of time" promoting cranberries in foodservice.
Others have.
"With all the holiday menu offerings it's a great opportunity to provide fresh product to them this time of year," said Doug Hurst, sales director with Sheridan, Ore.-based HBF International Inc.

Foodservice is an important venue for cranberries, said Michelle Hogan, executive director of the Wareham, Mass.-based Cranberry Marketing Committee.
"Marketing to the foodservice industry is an important part of what we do," she said. "Not only do we want to reach these influencers who are providing so many meals annually, but we want to start creating that newest group of cranberry lovers: students."
Indeed, schools may be a key sector for cranberry suppliers going forward, said Tom Lochner, executive director of the Wisconsin Rapids-based Wisconsin Cranberry Growers Association.
"There's has been a lot of effort to work with school nutrition groups and school lunch programs," he said.
The opportunity to get in front of students, in particular, so that they know the benefits and versatility of the fruit at an early age is a crucial step in fostering a generation of lifelong cranberry consumers, Hogan said.
"We have a dedicated foodservice website and we attend annual conferences to get our resources into the hands of foodservice professionals," Hogan said.
It helps that cranberries have a kind of "health halo," Hogan said.
"We know that this market recognizes the importance of a healthy, quality, versatile product and we work to keep cranberries in the forefront of their minds throughout the year," he said.
Foodservice isn't a huge part of the business at Glassboro, N.J.-based Sunny Valley International Inc., but the shipper does dabble in the sector, said Bob Von Rohr, marketing and customer relations manager.
"We do have some customers that are foodservice-oriented and do buy some from us for restaurants," he said.