Seven states and counting as the dominoes fall. PPO herbicide resistance in Palmer amaranth is spreading in the same manner glyphosate resistance advanced a decade ago. (Good luck even finding a Palmer population that’s responsive to glyphosate in some states.) An empty quiver of PPO chemistries (Group 14 herbicides) is no academic footnote; it carries a devastating loss of post-emergence options. Simply, lack of post-emergence weapons can mean adios to soybeans for some growers.
With alarm bells ringing in Arkansas, Illinois, Indiana, Kentucky, Mississippi, Missouri and Tennessee over confirmed PPO-resistant Palmer, is the weed control cavalry expected in soybean fields anytime soon? Bolstered by new technologies, help might be on the way within the next five years.
Balance GT
Developed jointly by MS Technologies and Bayer, Balance GT soybeans feature a double herbicide trait stack that provides tolerance to glyphosate and a new isoxaflutole-based herbicide called Balance Bean, which has built-in residual control along with a unique property: reactivation technology.
Balance Bean can be applied as a pre-emergent, and the benefits of residual control and reactivation technology allow the herbicide to provide long-lasting control. Bayer is waiting on EPA registration for Balance Bean herbicide.
Eventually Balance GT soybeans will evolve into a triple herbicide trait stack including LibertyLink technology, according to Lindsey Seitz, MS Technologies brand manager. “Balance Bean provides great control, especially for PPO- and ALS-resistant weeds,” Seitz says. “This is a completely new mode of action for soybeans.”
Enlist 
Dow AgroSciences hopes the 2018 crop season will see full commercialization, once import approvals are obtained, of Enlist soybeans triple stacked with tolerance to new 2,4-D choline, glyphosate and glufosinate. The 2,4-D choline is 88% less volatile than 2,4-D amine formulations and 96% less volatile than 2,4-D ester formulations, according to Dow.
Enlist Duo, the accompanying herbicide, employs Colex-D technology to pack a double-punch of new 2,4-D choline and glyphosate. Enlist soybeans will give growers an opportunity to use multiple effective modes over the top of an Enlist soybean crop.
How does Dow expect the Enlist herbicide tolerant traits and new herbicide solution to perform against PPO-resistant Palmer? “Enlist Duo uses two of the most trusted herbicides combined to deliver exceptional control against resistant and hard-to-control weeds,” says John Chase, Enlist commercial leader. “It provides control of major, tough weed species.”
MGI




 


Lack of post-emergence weapons can mean adios to soybeans for some growers.


© Chris Benentt


 




The resistant weed march has pushed many growers toward glufosinate to get away from glyphosate dependence. However, glufosinate can be trickier to use on Palmer due to the need for timely applications, and results in an emphasis on a residual program. The MGI system will raise the bar on residual control in soybeans, according to Duane Martin, commercial traits manager for Syngenta. “MGI will provide corn class weed control up front with pre-emergence products to accompany the system, followed by a well-timed post-emergence application of a herbicide such as glufosinate, depending on weed pressure,” he says.
Currently under regulatory review, MGI trial sites are set up for viewing and Martin expects market introduction by 2020 or possibly earlier. Mesotrione and isoxaflutole are HPPD products, and Martin expects MGI to raise the bar for broadleaf control in soybeans.
“MGI provides multiple technologies on an acre with overlapping modes of action, and these are critical factors in controlling resistant weeds,” he adds.
Sumitomo
In June 2016, Monsanto and Sumitomo Chemical announced a collaboration to develop an integrated weed control system based on a Sumitomo PPO herbicide for over-the-top and conventional application use. The PPO herbicide will not be available until after 2020.
“The product’s low use rate, together with a unique mode of action effective against resistant PPO weeds, will be valuable to corn, soy and cotton growers,” said Robb Fraley, Monsanto executive vice president and chief technology officer, in an announcement.
Metal solutions
Despite the hopes offered by multiple pipeline soybean products, past herbicide lessons loom large. A repeated pattern of riding a given herbicide to the point of exhaustion and moving on to the next in line has ultimately fostered weed resistance. “Simplicity has got us to the resistance situation we’re in, but chemical mixes and stacked traits provide multiple options that will definitely help in battling resistant Palmer,” notes Jason Norsworthy, an Extension weed scientist with the University of Arkansas.
However, Norsworthy is quick to warn against sole reliance on chemical solutions, regardless of pipeline promise. “We have to think beyond spray applications because non-chemical approaches are also vital. Weeds are constantly in motion and we’ll never spray our way out of the problem.”
“If we’d done things differently in the 1990s, we’d still have the efficacy of glyphosate, once the world’s greatest herbicide. No more,” he adds.
Norsworthy cites two Australian equipment innovations as potential non-chemical weapons against Palmer and other resistant weeds. The Seed Terminator (ST) is a combine addition that intercepts and pulverizes seed as it exits the cleaning shoe through multi-stage hammer mill technology. ST prototypes are on a variety of combines in Australia, including John Deere, Case IH and New Holland. ST is preparing prototypes for testing in the U.S. during the 2017 harvest.
A closely related technology to the ST, the Harrington Seed Destructor (HSD) shows tremendous promise as a non-chemical means to fight multiple types of resistant weeds. Initially designed as a combine pull-behind unit and expanded as a bolt-on, integrated version (iHSD), the seed pulverizing machine relies on a cage mill apparatus. The same technology currently used in the mining industry is unleashed on weed seed. Two counter-rotating mills spin at more than 3,000 rpm and fracture passing weed seed when funneled chaff hits the crossbars at super-high speed. The steel on seed-battering cycle is highly effective and requires no chopping or mashing.
Built by de Bruin, an Australian manufacturing firm, the iHSD has no U.S. market entry date, but de Bruin officials say it could see limited release in 2019. Contingent on manufacturing location, shipping and distribution, de Bruin aims for a starting iHSD retail price below $100,000. Norsworthy hopes to test an iHSD at several Arkansas locations during the 2017 harvest.




 


The Harrington Seed Destructor (HSD) shows tremendous promise as a non-chemical means to fight multiple types of resistant weeds.


© de Bruin







“We’ve got to use all types of strategies because we’ve never seen pigweed like this before,” explains Norsworthy in regard to the rapid advance of PPO-resistant Palmer. “We need more weapons because PPO-resistant pigweed is such a difficult beast to control.”
“Even if you’ve got a weed control system that works, change it up this year and add something different,” he adds. “Versatility is key.”