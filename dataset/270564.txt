BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Friday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2729  Down  70May         2805    2805    2741    2749  Down  82Jul                                 2756  Down  68Jul         2777    2795    2704    2729  Down  70Sep         2803    2816    2731    2756  Down  68Dec         2794    2806    2725    2754  Down  59Mar         2761    2777    2700    2732  Down  53May         2733    2764    2693    2723  Down  52Jul         2725    2725    2722    2722  Down  51Sep         2718    2723    2718    2723  Down  50Dec         2719    2724    2719    2724  Down  49Mar                                 2733  Down  49