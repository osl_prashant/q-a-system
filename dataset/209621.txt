After an unusually light harvest in 2016, the California prune harvest is projected to rebound this year, according to a news release.
The 2017 California prune harvest is projected at 105,000 tons, a 99% increase over 2016’s 52,851-ton crop, according to the U.S. Department of Agriculture’s National Agricultural Statistics Service.
The estimate is based on surveys of California prune growers, according to the release.
“California prunes consistently represent the world’s finest quality fruit,” Donn Zea, executive director of the California Dried Plum Board, said in the release. “This year, the industry is returning to a normal size harvest that will help meet consumer and trade demand.”
This year’s harvest got a slightly later start and was expected to conclude in mid-September, Zea said in the release.
Weather in recent years has created challenges for prune growers, but growers say the trees are rebounding this year, according to the release.
“We are on track for a good year for California prunes,” John Taylor, vice president and owner of Taylor Bros. Farms, said in the release. “We are seeing strong, highly productive orchards and delicious, premium caliber fruit that sets the global gold standard for prunes.”
California is the world’s largest producer of prunes, accounting for 40% of the world’s supply and nearly of all the supply in the U.S., according to the release.