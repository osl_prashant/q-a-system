Each year presents its own unique series of weather, pest and market challenges to corn and soybean farmers. It’s a lot to keep up with, which is why farmers need their seed to work as hard as they do. With the release of 46 new corn hybrids, 14 of which are new genetic families, and 18 new soybean varieties for 2018, Golden Harvest is expanding its strong portfolio of seed options that farmers can count on to get the most yield out of every field.
The new hybrids and varieties further grow a portfolio engineered to equip farmers with agronomic options for all kinds of field types, soil characteristics and weather conditions.
“Rooted in genetics, agronomy and service, the new Golden Harvest hybrids and varieties build on a legacy of genetically diverse, high-yielding seed that farmers can depend on,” said Eric Boersma, Golden Harvest Corn product manager lead. “Because no two fields are the same, we are committed to providing a range of unique, genetically diverse corn and soybean seed options.”
On the corn front, the 2018 hybrids feature proven genetics protected by high-performing Agrisure traits and technologies.

Eight hybrids will include the Agrisure Duricade trait, which features a different mode of action to effectively control corn rootworm, will be available in select geographies to growers who agree to feed on farm or deliver to approved feedlots
Fourteen hybrids will feature the Agrisure Viptera trait, which contains the most comprehensive insect control in the industry
Twelve Agrisure Artesian hybrids will help maximize yield when it rains and increase yield when it doesn’t
Twenty hybrids will be available as E-Z Refuge options, providing convenient, integrated single-bag refuge offerings

In select geographies, Golden Harvest will also offer nine new Enogen hybrids, featuring an in-seed innovation that benefits farmers marketing grain to ethanol plants and those producing grain for livestock feed. In the ethanol market, Enogen grain enhances the ethanol production process by improving process efficiency, while the same technology increases the value of corn as feed for dairy or beef cattle due to improved digestibility.
The new Golden Harvest Soybean varieties range in relative maturity from 0.08 to 4.5 and protect against many of today’s toughest challenges, including soybean cyst nematodes, sudden death syndrome, brown stem rot and Phytophthora root rot. Sixteen of the new varieties also include dicamba-tolerant Roundup Ready 2 Xtend® technology, which provides another line of defense against weed resistance.
“With the new Golden Harvest varieties, soybean farmers will find reliable options to take on any agronomic challenge,” said Scott Erickson, product marketing manager, soybean seed for Golden Harvest.
The Golden Harvest seeds portfolio combines high-yielding seed options with the local agronomic know-how of a Golden Harvest Seed Advisorfor in-depth field insights. To find your local independent Seed Advisor and gain more information on Golden Harvest hybrids and varieties, visit the all-new GoldenHarvestSeeds.com.
 

 
With company roots dating to the mid-1800s, Golden Harvest draws on a rich heritage that combines genetics, agronomy and service to deliver high-quality corn hybrids and soybean varieties. (Photo credit: Golden Harvest)