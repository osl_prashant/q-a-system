Organic onions remain a tough sell in the Treasure Valley.
Paul Skeen, owner of Skeen Farms Inc., Nyssa, Ore., said a combination of factors has made it hard for valley growers to experiment much with organics.
At or near the top of the list is the fact that ground that had been used for conventional growing must lay fallow for three years.
"That makes it pretty tough. There's some in southern Idaho, but I'm not doing any."
Organics aren't hard to grow once you find the land, said John Vlahandreas, onion sales manager for Wada Farms Marketing Group LLC, Idaho Falls, Idaho.
But growers aren't likely to stop production on existing land and wait three years for it to transition to organic, and Vlahandreas said that when given the choice between planting onions or crops like asparagus or mint on new land, they might well choose the latter category.
Organic onions haven't caught on yet in the Treasure Valley, agreed Chris Woo, national sales manager for Idaho and Oregon for Murakami Produce, Ontario, Ore.
"I don't think you get the yields, size or storeagability" with organic onions as with conventional, Woo said.
In addition, organic onions cost more to grow.