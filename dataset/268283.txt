The Latest: Buffer strip fines plan likely to die Thursday
The Latest: Buffer strip fines plan likely to die Thursday

The Associated Press

ST. PAUL, Minn.




ST. PAUL, Minn. (AP) — The Latest on the dispute over enforcing Minnesota's buffer strip law (all times local):
4:10 p.m.
The executive director of a state board says a controversial proposal that could have meant big fines for landowners who violate Minnesota's requirement for buffer strips between farm fields and waterways says the idea will likely die Thursday.
John Jaschke of the Board of Water and Soil Resources says the board's buffer committee meets Thursday, and he expects the committee to reject the draft proposal and not pursue it further.
Jaschke spoke after Democratic Gov. Mark Dayton joined with key GOP legislative leaders in criticizing the proposal, which was meant to give local governments a new option for imposing higher administrative penalties than current regulations allow.
Jaschke says he's sorry that he and his staff made mistakes in their hurry to put the proposal out for public comment.
1:40 p.m.
Gov. Mark Dayton says he's "surprised and disturbed" by a proposal for adding new enforcement teeth to one of his signature environmental initiatives,  a state law requiring farmers to plant buffer strips between fields and waterways to trap pollutants.
Dayton says in a letter to the Board of Water and Soil Resources the proposed fines are unreasonable, and came as a shock to him and Minnesota farmers. The Democratic governor is urging the board to reconsider the draft proposal, which also has drawn sharp criticisms from key Republican lawmakers who accuse his administration of overreach.
The proposal would allow local governments to impose higher fines than current regulations allow on landowners who don't comply with requirements.
Compliance is at 98 percent already. Dayton says penalties should be a last resort.
10:30 a.m.
The Republican chairmen of agriculture committees at the Minnesota Legislature are accusing the Dayton administration of overreach as it implements a state law requiring farmers to plant buffer strips between fields and waterways to trap pollutants.
The Board of Water and Soil Resources is considering a draft proposal to give local governments a new option for imposing higher administrative penalties on the few landowners who don't comply.
The chairmen call the proposal "ludicrous ," ''absurd" and "heavy-handed ." Committee hearings on it are set for Wednesday and Thursday.
The board on Monday apologized for what calls a "communication misunderstanding." It says the draft is meant only as an option for local governments to achieve compliance. And it says the board probably won't adopt it without broad support from landowners or local governments.