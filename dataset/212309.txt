Granny found some get up and go in the spring and summer of 2017.
Contrasting with flat market conditions for red delicious and gala, shipping point prices for granny smith apples are sharply higher in early June. Shippers said prices solidly in the high $30s per carton — double the U.S. Department of Agriculture’s reported f.o.b.s for red delicious in early June — could creep higher as Washington state inventories dwindle.
“(Granny smith) is a bright spot, for sure,” said Mac Riggan, director of marketing for Chelan Fresh, Chelan, Wash.
Riggan said Chelan Fresh conditions led to even higher prices than the USDA has reported, up to $36-40 per carton on June 8.
Meanwhile, imports of granny smith from Southern Hemisphere countries apparently weren't easing tight market conditions.
“We are seeing numbers we haven’t seen for a while — maybe ever — on grannys,” said Bill Knight, domestic sales manager for Northern Fruit Co., Wenatchee, Wash.
“There hasn’t been the imported (supply) to take the sting out,” he said June 8.
Granny smith apples are a popular variety for fresh apple slicers, said Tony Freytag, senior vice president of sales and marketing for Cashmere, Wash.-based Crunch Pak. Industrywide, he said granny smith accounts for perhaps 30% of apples used by fresh-cut apple slicers. Shortage of the variety is a concern, he said.
“We want to make sure we cover our customers and that’s the key.”
The USDA reported prices for Washington Extra Fancy granny smith, size 80s, were $28-31 per carton on June 8, compared with $13-16 per carton for size 80 red delicious and $18-22.50 for size 80 galas.
 
Tight supply
Marketers said low domestic inventories of the granny smith variety created a supply crunch for the tart apple.
The U.S. Apple Association reported June 1 inventories of granny smith fresh apples at 2.12 million cartons, down 54% from the same time last year and 45% from two years ago. The granny smith volume in the U.S. was short from the beginning of the crop year, but the gap compared with last year was not as far. The first storage report of the season from U.S. Apple, issued on Nov. 1, showed marketers had 13.78 million cartons of granny smiths in storage, off 12% from the 2015 Nov. 1 figure.
Riggan said the industry had moved about 85% of the granny crop by early June, compared with 73% shipped at the same time last year.
Andrew Eakin, in domestic sales with Orondo, Wash.-based Auvil Fruit, said the company was reserving some granny smith volume to run with its new packing line. That line is expected to open Aug. 14, he said, and those apples may be shipped by the end of August. New crop granny smith is expected to begin about Oct. 1, he said.
Whether there will be a gap between the 2016 supply of granny smith and the new crop that begins Oct. 1 is hard to say, Riggan said.
“That’s the whole reason we raise prices, to slow (movement down),” he said
Knight said the market — $38 to $40 per carton on top fruit — has the potential to keep climbing. He said processors are having finding it harder to get fruit, since packers are shifting lower-grade fruit to the fresh market because of high prices.
Some fresh-cut slicers or fresh apple ingredient users are buying fruit in cartons with no stickers because since they can’t buy as much fruit in bulk bins as they would in a normal season.
California granny smith harvest will begin by mid- to late August. In the 2015-16 season, California shipped about 443,000 cartons of granny smith apple, about 25% of the state’s total fresh apple shipments.
 
Import view
Though no variety breakdown is provided, imported apples accounted for about 15% of the total U.S. apple supply in late May, according to the U.S. Department of Agriculture.
About about 70% of apple imports were from Chile, with lesser amounts from Argentina and New Zealand.
The f.o.b. market for Chilean granny smith apples arriving into the East Coast was securely in the low $30 range and increasing about $2 each week, said Jordan Cohn, sales and marketing representative with Moderna, N.Y.-based Stanley Orchards.
Cohn said June 8 that most packers in Chile were finishing up granny smith apples in June, with sporadic controlled atmosphere rooms expected to open as late as July or early August. Some Chilean granny smith shipments have experienced bruising and bitter pit issues this year, he said.
While some volume of granny smith is coming in from South Africa and New Zealand, it will be less than what Chile will ship to the U.S., he said.
 
Consumer preference
While the Honeycrisp is in a league of its own in terms of popularity, Desmond O’Rourke, economist and president of Belrose, Inc., Pullman, Wash., said granny smith and Pink Lady/cripps pink seem to be fairly immune from competition with the new varieties.
“The granny is green and there is a certain segment of the population that wants green apples,” he said.
Eakin said granny smith is in high demand from fresh apple slice processors.  The trend for consumers to juice their own fruit may also have added to granny smith demand, he said.
Riggan said granny smith apples are a go-to tart apple and also are favored by processors for juicing. He speculated young consumers — growing up with sour candies — may be drawn to the tart apple as well.