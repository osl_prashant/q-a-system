Grains mostly lower and livestock mixed
Grains mostly lower and livestock mixed

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mostly lower Tuesday in early trading on the Chicago Board of Trade.
Wheat for May delivery fell 2.20 cents at $4.58 a bushel; May corn was up 1.80 cents at $3.79 a bushel; May oats was down 14.20 cents at $2.19 a bushel while May soybeans was off 2.60 cents at $10.22 a bushel.
Beef was higher and pork was lower on the Chicago Mercantile Exchange.
April live cattle rose 2 cents at $1.2198 a pound; Apr feeder cattle was up .32 cent at $1.3962 a pound; April lean hogs fell .35 cent at .6810 a pound.