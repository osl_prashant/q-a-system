Black Gold Farms will soon begin harvesting sweet potatoes in Louisiana.
On Sept. 1, Grand Forks, N.D.-based Black Gold announced it was about to begin harvesting from the Macon Ridge area of northeast Louisiana, with cured product ready to ship from the company's Delhi, La., facility in October, according to a news release.
Black Gold dodged a bullet with the weather, and demand was above normal heading into the Louisiana new-crop season, Joe Millman, Black Gold's director of sweet potato operations, said in the release.
"Our first few acres look very promising," he said. "We did have some heavy rain events early on, but thankfully, that didn't set us back."
This is the third year Black Gold is growing sweet potatoes in Louisiana, following its 2014 purchase of Delhi-based Dawson Farms.
Black Bold has made major investments in the Dawson packing shed to help guarantee quality and keep up with ever-growing demand, Millman said.
"We know that this crop is special, so we've devoted some major resources in making sure we have the ability to sort, pack, and ship out whatever our customers need - efficiently and safely," he said.
Black Gold ships its Louisiana sweet potatoes in cartons, three-pound bags and its Steamin' Sweets value-added product.