Locally grown produce is a hot commodity in the Baltimore-Washington, D.C., region as in the rest of the country, and organic produce is growing as well.The organic category continues to show steady growth, said Lolo Mengel, CEO and partner with Coosemans DC Inc., a distributor in Jessup, Md.
“Interest in organics has grown substantially. Every major chain in the area offers the category now,” she said.
Consumers like to feel they are supporting the local economy while getting fresher produce, Mengel said.
As with locally grown produce, when in-season organics can be found virtually anywhere in the metro region, Mengel said.
And that’s just retail.
Restaurants offer plenty of organic choices, too, said Roy Cargiulo, sales manager of Landover, Md.-based Keany Produce Co., which focuses on the foodservice category.
The growth of organics hasn’t showed any signs of slowing, even though “locally grown” may be the latest buzzword, Cargiulo said.
Perhaps that’s due to the seasonality of local produce, he said.
Seasonality is a major limit on the local deal, particularly among some potential institutional customers, such as schools, Cargiulo said.
“One of the big drivers for local has to do with K-12 schools and universities and colleges, besides the local chefs,” he said. “But, most of the schools are not in session when the most plentiful part of the local season is available. That’s a challenge.”
Local growers are doing what they can to lengthen their seasons, Cargiulo said.
“The farming community is responding to that and doing stuff differently, trying to extend seasons with tools like tunnels on the front end and back end because the demand is there,” he said.
Organics has graduated from niche to mainstream in the metro region, said Larry Quinn, president of G. Cefalu & Bro. Inc., a Jessup-based distributor.
“We’re not a big player in organic, but it clearly was a niche product 10 or so years ago and now everybody has it and just about everybody wants it,” he said.
John Gates, president of Jessup-based Lancaster Foods Inc., said organic demand continues to increase “across the board.”
The region is home to a wide swath of local production, and that helps sales of homegrown product, Gates said.
The organic category has grown immensely over the last five years at Washington-based distributor Pete Pappas & Sons Inc., said Gus Pappas, president.
“In berries, raspberries, tomatoes — we’re doing some test plots at our farm in Tennessee to see how they do,” he said.
Local is enjoying the same type of growth, Pappas said.
“To buy local, to save money and time and transportation and get fresher stuff, there’s a lot of demand for it,” he said.