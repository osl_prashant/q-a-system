The nutritional halo that organic produce wears is bright and, perhaps, golden, according to marketers.
Few marketers make any claims that organic product is healthier than its conventional counterpart.
They also acknowledge, in some cases, they don't have to. Consumers simply assume organic is better for them.
"Certainly, we're seeing this, too, which is one of the reasons we're intensifying our research involving organic fruits and vegetables," said Bil Goldfield, director or corporate communications for Westlake Village, Calif.-based Dole Food Co.
Dole's nutritional arm, Dole Nutrition Institute, is studying the nutritional benefits of organic produce at its North Carolina laboratory, Goldfield said,. The results of the study should be out soon, he said.
The health-halo effect is not all good news for organics, said Bruce Turner, national salesman for Wenatchee, Wash.-based Oneonta Starr Ranch Growers.
"To quote Mark Bittman, (The) New York Times columnist, at the recent Organic Produce Summit: 'Organic junk food is still junk food,'" Turner said.
However, Turner said, if a high-nutrition reputation also continues to drive more consumption of organic produce, that's good for consumers and producers.
The good-health perception of organic produce is no surprise, said Samantha Cabaluna, managing director of brand communications for San Juan Bautista, Calif.-based Earthbound Farm. "Many consumers are trying to avoid needless exposure to toxic substances, and they make an intuitive connection that food produced without the use of toxic and persistent chemicals must be healthier for them," she said.
Laura Batcha, executive director of the Washington, D.C.-based Organic Trade Association, said she's a firm believer in the health halo of organics.
"In terms of the produce category, absolutely, I think there is always new science coming on about the risks of chemical exposure and solid data that you reduce exposure by choosing organics," she said.
Batcha said there are studies that support her contention that organic is healthier.
"There is a growing scientific body of knowledge that is looking at the influence of the production system on the quality of the finished product - how does it impact the profile of the finished product? Does the quality of the soil in terms of micronutrients impact the antioxidant level? There's an emerging body of knowledge that does see a connection there," she said.
Marketers acknowledge that the conventional vs. organic nutritional-properties discussion is not officially settled.
"This is still a heavily debated topic. However, research does suggest that organics are higher in antioxidants," said Rachel Mehdi, organics category manager for Vancouver, British Columbia-based The Oppenheimer Group.
On the other hand, as growers face pressure to produce greater volumes to meet market demands, they may be compelled to select varieties for yield instead of flavor or nutrition in time, Mehdi noted.
"It's a balance - there's a huge benefit to making more organics available to a wider customer base, but not at the cost of the very characteristics that make organics valued today," she said.
When a consumer begins to eat organically, often it's connected to a health issue, said Scott Mabs, CEO of Porterville, Calif.-based Homegrown Organic Farms.
"So there is a recognition, from a health perspective, that if you're going to take every precaution you can to try to maintain a certain health aspect you're trying to attain, organic is a part of that," he said.
Not all marketers in the organic category agree that organics are a healthier choice.
"I think, really, nutritionally, there's no difference," said Patrick Lucy, vice president of organic sales for Fallbrook, Calif.-based Del Rey Avocado. "I think consumers are just more concerned about what chemicals are being used."