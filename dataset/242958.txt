Oregon Legislature approves hemp expansion
Oregon Legislature approves hemp expansion

The Associated Press

SALEM, Ore.




SALEM, Ore. (AP) — A proposal to ease commercial hemp cultivation has passed the Oregon Legislature.
Under a measure approved by the Senate on Saturday, hemp seed would be classed as an agricultural seed. The state Department of Agriculture must lay out standards, including identification documents. A variety of cannabis, hemp is bred to produce fiber which is used to manufacture paper, fabric, and other products.
The measure passed the Senate unanimously during a special Saturday meeting of the Legislature, as lawmakers moved through the last bills of the 2018 session.
Before the vote, Rep. Carl Wilson said he hopes the bill will help farmers, who he said have struggled with unclear hemp standards.
The measure now goes to Gov. Kate Brown for signing.