USDA Weekly Grain Export Inspections
			Week Ended April 27, 2017




Corn



Actual (MT)
1,093,729


Expectations (MT)

1,000,000-1,300,000



Comments:
Inspections fell 370,654 MT from the previous week and the tally matched expectations. Inspections for 2016-17 are up 59.0% from year-ago, compared to 62.2% ahead the previous week. USDA's 2016-17 export forecast of 2.225 billion bu. is up 17.2% above the previous marketing year.



Wheat



Actual (MT)
574,588


Expectations (MT)
400,000-600,000 


Comments:

Inspections are down 57,461 MT from the week prior and the tally met expectations. Inspections for 2016-17 are running 32.5% ahead of year-ago compared to 31.8% ahead last week. USDA's export forecast for 2016-17 is at 1.025 billion bu., up 31.0% from the previous marketing year.




Soybeans



Actual (MT)
521,218


Expectations (MMT)
400,000-600,000 


Comments:
Export inspections fell 125,615 MT from the previous week, and the tally was as expected. Inspections for 2016-17 are running 15.2% ahead of year-ago, compared to 14.5% ahead the previous week. USDA's 2016-17 export forecast is at 2.025 billion bu., up 4.6% from year-ago.