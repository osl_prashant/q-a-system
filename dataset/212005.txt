Organic fraud threatens the contract growers have with consumers. 
It’s wise for the industry to be vigilant against fraud. The Organic Trade Association is creating a task force to address the issue, and three U.S. senators have requested that Agriculture Secretary Sonny Perdue increase enforcement of organic imports. 
 
While we agree with the sentiment, we think there’s one area the organic industry should focus on more.
 
Consumers buy most of their organic fruits and vegetables from retailers, so we’d like to see representatives from retail highly involved in any best practices guide “to mitigate the risk and occurrence of organic fraud,” according to the task force’s mission.
 
One of the weaknesses of the National Organic Program, which administers organic standards within the U.S. Department of Agriculture, is the lack of representation from mainstream food retailers. 
 
The makeup of the National Organic Standards Board, for instance, looks more like the organic industry of 20 years ago with bit players, not the big business it has become.
 
Several large fruit and vegetable grower-shippers quoted in our story say they’re not worried about the risk of fraudulent imports, and that’s understandable.  But retailers can’t always find enough organic product from these well-known producers, and they need to ensure that less well-known producers who are USDA certified organic are 100% legitimate. 
 
The organic produce industry needs to act and insist on being governed as the large and important business that it is, and that necessitates participation from the most important retail groups.
 
Did The Packer get it right? Leave a comment and tell us your opinion.