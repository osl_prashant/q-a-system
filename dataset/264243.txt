AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:30 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
TOP STORIES:
DENVER JAIL DEATH
DENVER — The Denver Sheriff's Department mishandled an investigation into the 2015 death of a man restrained by deputies at the jail and must change its disciplinary and investigative process, a law enforcement watchdog agency said in a blistering report Monday that focused new attention on a case that has already resulted in a $4.6 million settlement. The report made public by Denver's independent police monitor, a civilian oversight agency for Denver's police and sheriff departments, recommends that a civilian be put in charge of the Internal Affairs Bureau of the sheriff's department that investigates allegations of officer misconduct. By Kathleen Foody. SENT: 600 words, photos.
COLORADO BUDGET
DENVER — Colorado's strong economy and the new federal tax law are boosting the state government's revenues. That's the word from the state's chief legislative and executive branch economists. They told lawmakers Monday that general fund revenue could increase significantly in the fiscal year that begins July 1 — up to $1.3 billion more than is budgeted for the current fiscal year. By James Anderson. SENT: 130-word APNewsNow. Will be expanded.
IN BRIEF:
— INMATE ESCAPE-THE LATEST — Immigration authorities asked to be notified if a man facing charges for the attempted murder of a police officer was released by the Denver Sheriff's Department.
— MISSING CHILD FOUND — Colorado authorities say they are investigating the death of a 2-year-old boy who was reported missing as an accidental drowning.
— FORT CARSON WILDFIRE-THE LATEST — Three days after a wildfire broke out at Fort Carson during a training exercise, the Army has not yet said whether soldiers were using live ammunition.
— CHILDREN ABANDONED-SENTENCE — A 26-year-old Colorado woman has been sentenced to eight years in prison for leaving her two young children locked in a bedroom for days while she traveled out of state in March 2017.
— BRAZIL-MEATPACKER-US SALE — A Brazilian meatpacking company has sold its U.S.-based Five Rivers Cattle Feeding operation as part of a $1.8 billion divestment of assets.
— ELECTRIC BUS DEMONSTRATIONS — A pair of Colorado towns will host electric bus demonstrations as part of a project to upgrade their public buses to battery-electric.
— SLEEPING IN CAR-ARRESTS — Police in Massachusetts say they found more than four pounds of marijuana, as well as heroin, two guns and more than 200 rounds of ammunition when they arrested two Colorado men sleeping in their car.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for May fell 17 cents at 4.5075 a bushel; May corn fell 7.75 cents at 3.75 a bushel; May oats was off 7.25 cents at $2.35 a bushel; while May soybeans declined 27 cents at $10.2250 a bushel.
SPORTS:
DIAMONBACKS ROTATION
SCOTTSDALE, Ariz. — Zack Greinke won't be the opening-day starter for the Arizona Diamondbacks. Manager Torey Lovullo said what's perceived to be a minor health issue will bench his ace for the opener. By Bob Baum. SENT: 800 words, photos.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.