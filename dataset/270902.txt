The biggest online retailer in the U.S. is taking its first small step into the world of brick-and-mortar convenience food stores. 
The first Amazon Go store is open and available to use by Amazon employees in Seattle according to a news release from the company.
 
A video of the store shows a section devoted to ready.-to-eat packaged salads, but the company did not say what other fresh produce-based items will be available.
 
The store, located at 2131 7th Ave, Seattle, Wash.. is expected to open to the public in early 2017, according to a news release from Amazon.com.
 
The company did not reveal its expansion plans for the Amazon Go format, though the Wall Street Journal said in October that Amazon was looking to expand its physical presence in brick-and-mortar food retailing.
 
Using what the company calls its “Just Walk Out Shopping Experience” that includes an Amazon Go smartphone app, the 1,800-square-foot store allows shoppers to just put the items they want in a bag and leave, with no checkout required.
 
“Our checkout-free shopping experience is made possible by the same types of technologies used in self-driving cars: computer vision, sensor fusion and deep learning,” the release said. The technology automatically detects when products are taken from or returned to the shelves and keeps track of them in a virtual cart, and then charges the shopper’s Amazon account and sends an electronic receipt, the company said in the release.
 
Amazon Go will offer ready-to-eat breakfast, lunch, dinner and snack options made fresh every day, according to the release. The store will also feature Amazon Meal Kits, which include ingredients that can create a meal for two in about 30 minutes.
 
“I think it is a very intriguing concept,” said Diana Sheehan, director of retail insights at Kantar Retail. For the millennial shopper, she said Amazon’s use of technology to increase convenience will be attractive. 
The advanced technology in the store alone will drive visits from younger consumers, she said. The strategy also answers questions about how Amazon will handle its operations and minimize the number of store employees.
 
The question will be whether the concept is too much of a change, she said. “Is the average shopper going to be feel comfortable with something that really does go so far down the path of relying on technology?,” she said. “Even though we hate the idea of grocery shopping, there is still a connection to our local grocery stores and so by removing that connection that serves as a disadvantage for them for the average shopper.” 
 
Sheehan said the Seattle store is such a unique concept that Amazon can’t go for a full scale launch until they determine if it works. “If it does work, what parts don’t within the concept?” she said. 
 
Limiting the number of fresh produce items makes sense, she said, since Amazon Go will primarily be competing with traditional convenience stores rather than small format grocery stores.
 
“This is all about a convenience trip play, less about a stock-up trip play,” Sheehan said.
 
In time, Sheehan said that consumers have enjoyed Amazon’s brick-and-mortar bookstores, so the Amazon Go concept may also find fans. “I think that Amazon can’t succeed in groceries without some form of physical connection to their shoppers,” she said. However, Sheehan speculated that Amazon may try a number of formats before it decides how to expand.
 
For Amazon Go, the technology must be flawless, she said.
 
“In major urban markets I think you can see the concept expand if they can make the technology work without hiccups,” she said. “If they run into any hiccups from a technology or security perspective, I think PR is going to be so incredibly negative that it will be hard to recover.”
 
Sheehan said shoppers still value the experience of the grocery store trip. In that sense, the growth of online grocery sales has been slower than that of book or music sales online. “Shoppers still need to look and touch and feel and really get what grocery shopping is,” she said.