This week's Ranch of the Week features JNelson Farms, owned and operated byJon and Tammy Nelson just northwest of Midland, Mich. You can find them on their website atwww.jnelsonfarms.comor onFacebook atJNelson Farms.

The vision of JNelson Farms is to provide healthy food to help people live a healthy life. The family has 200 acres of pasture land to support our herd of grazing cattle. In 2011, they started to learn about improving the health of our land and supplying healthy food for our customers, they began transitioning from a cash crop operation to raising grassfed beef. Currently, the ranch raises grassfed beef that never taste grainfrom birth to finish providing healthy beef to families throughout lower Michigan. 

What makes your operation unique?
We are pasturing cattle on previously high producing cash crop land. Most other pasture operations utilize land that is considered to not be high enough quality to raise crops. We believe that the quality of the land supporting the forages that the cattle graze is reflected in the quality of the beef harvested from our cattle. 

Do you operate under a specific philosophy?
Ourphilosophy isto provide healthy food for people who want it and improve the health of the land as we grow it. In addition, we want our farm to be a healthy environment for anyone involved with it. 
Interested in your ranch being featured or know a great ranch we should show off? Nominate them below: