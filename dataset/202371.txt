ORLANDO, Fla. - An event should reflect the host's goals and values.
 
Face-to-face meetings are so important in the fresh produce industry, and The Packer gets to cover many of them and even host a couple.
 
They all have a signature that makes them unique.
The upcoming Canadian Produce Marketing Association features the Canadian friendliness and American businesses trying to suit their products to another nation's consumers.
United Fresh hosts an event with world-class speakers, hard-hitting sessions and some of the industry's biggest decision-makers.
The Produce Marketing Association's Fresh Summit is the biggest and the only can't-miss event in the industry, but you better make an appointment with someone because you may not just run into him or her among the 20,000-plus attendees.
The Packer has its own events with a certain feel: the more laid-back family feel of the Midwest Produce Expo and the California business retreat at the upcoming West Coast Produce Expo. And we're launching what we plan to be the best organic-only produce event in Miami next January.
And then there's the Southeast Produce Council's Southern Exposure, which many members relate to a family reunion.
 
Every year, there are hugs, laughs, tears and prayers, each year more of all of them than the last. 
 
This year in Orlando, March 9-11, nearly 500 retail and foodservice buyers visited, so there was certainly business taking place.
 
But the SEPC culture is about so much more than the work.
 
For instance, the keynote general session March 11 before the expo showed all of this.
 
Council president Teri Miller, produce senior category manager for The Fresh Market, Greensboro, N.C., said the council's core values are networking, education and community.
 
She teared up only a few times talking about her time on the board.
 
I don't think Garry Bergstrom, recently retired director of produce and floral for Lakeland, Fla.-based Publix Super Markets, shed a tear at all as he received the SEPC's Lifetime Membership Award. 
 
But he was emotional in his own way.
 
"I was once told not to take the industry so personally - it's just business," Bergstrom told the audience. "I thought to myself, 'We pour our hearts into this business. We love this business. Of course it's personal - because we care," he said, in accepting the award. 
 
"I think, too, that this business I'm in â€¦ is all about people. This is a people business. We just happen to sell produce."
 
Sheila Carden, Richmond, Va.-based regional merchandiser manager for the National Watermelon Promotion Board, was honored with the Terry Vorhees Lifetime Achievement Award March 11, and of course the tears flowed a few times. 
 
"I've never been around people who have been so caring and generous," she said of her SEPC colleagues.
 
David Sherrod, executive director of the council, seems to laugh and cry in every presentation, and this one was no different. He even laughs about crying.
 
That's the way it is at the SEPC.
 
It's also an unapologetically Christian organization.
 
The invocation March 11 was long, detailed, filled with gratitude and finished with praise for our Lord and Savior Jesus Christ.
 
That notion may seem odd to other groups or areas of the country, but it's not at the SEPC. It's that way because its members are that way. They say grace before meals.
 
Keynote speaker Willie Robertson of "Duck Dynasty" fame is openly Christian on his TV show and was at SEPC. 
 
He told the story of how his father was a broken and wrecked man and terrible father and husband.
 
"One day he asked for forgiveness and turned to the Lord," Robertson said. "His faith turned his life around, and he became a different person."
 
Robertson said his father's conversion led to the creation of the Duck Commander company, the TV show and the ability to reach millions of people across the world and spread the gospel of Jesus.
 
Robertson is just the latest example of how SEPC says who they are, what they stand for and what they'll deliver if you're a member or just an attendee of Southern Exposure.
 
Greg Johnson is The Packer's editor. E-mail him at gjohnson@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.