International milk prices slipped on Wednesday, weighing on hopes that prices were beginning to recover after two consecutive auctions of gains.The fortnightly Global Dairy Price auction, held early on Wednesday morning, showed prices had dipped 1.4 percent to $2,203 per tonne.
A total of 20,615 tonnes was sold at the latest auction, falling 2.8 percent from the previous one.
However, analysts said the news was not all bad, especially for New Zealand, which has been struggling with the loss in value of its largest export.
Whole milk powder (WMP) prices, the main dairy commodity exported by the Pacific nation, rose 0.7 percent to $2,176.
"The lift in WMP prices on GDT was supported by the lower volumes on offer as we near the end of NZ's milk production season," said AgriHQ dairy analyst Susan Kilsby in a research note.
"The weakness in the other dairy commodities is not surprising given these commodities are influenced to a much greater degree by supply from the Northern Hemisphere," Kilsby added, saying milk production in Europe and the U.S. was currently strong.
The New Zealand dollar fell to $0.6917 from as high as $0.7054 the previous day.
The auction results affects the New Zealand currency as the dairy sector generates more than 7 percent of the nation's gross domestic product.
An around 60 percent fall in dairy prices since early 2014 has also hit the country's economy.
The sector was until recently the backbone of the economy, representing around 25 percent of exports, but in the past two years farmers have had NZ$7 billion ($4.74 billion) wiped off their collective revenue.
The Global Dairy Trade auctions, which were set up by Fonterra and operated by trading manager CRA International, are held twice a month, with the next one scheduled for May 17.