Cedarcut Burdette Clove Colata took home her third title of Grand Champion of the International Junior Ayrshire Show on October 3. The winning Five-Year-Old Cow was named Senior Champion and before going on to be named Grand Champion Ayrshire. Colata, exhibited by Erin Curtis Szalach, of Cazenovia, N.Y., took home the $500 Udder Comfort Grand Champion Cash Award and the Lillian & Keith King and Jim King Grand Champion Junior Show Award. Reserve Grand Champion of the International Junior Ayrshire Show was Four-Hills Abush Sammy 3559 exhibited by Britney and Bradley Hill of Bristol, Va. The duo was awarded the Lillian & Keith King and Jim King Reserve Grand Champion Junior Show Award.
Evaluating the 100 entries were official judge Allyn ‘Spud” Paulson, Arlington, Wis. and associate judge Kurt Wolf, Epworth, Iowa.

Overall show results and top class placings are as follows:
Spring Heifer Calf:

Nor-Bert Reinholts Anique, D, N, R, T, J & T Reinholt and D, D, & B Freeman, Bremen, Ind.
Woodman-Farm Barrel Gabby, Tyler Woodman, Claremont, N.H.

Winter Heifer Calf:

Hi-Ayr-View Miche Lob Golden, Addison Steinlage, Knute Hovden and Diane Borba, Lawler, Iowa
Hall’s Marksman Rosetip, Rachel Hefel, Epworth, Iowa

Fall Heifer Calf:

Sweet-Pepper Predator Shale, leased by Kylie Jordan and owned by Craig and Bonnie Hawksley, Hope Valley, R.I.
Maple Dell Booth Betty, Morgan Ann Murray, Woodbine, Md.

Summer Yearling Heifer:

Wingert’s Gibbs Jules, Trent J. Wingert, Jr, Kent, Ill.
Ridge View Dempsy Jazzlyn, Ridge View Farm, Shaelyn Scoon, Lancaster, Mo.

Spring Yearling Heifer:

Royale-Divide Star Jumper, Collin Costello, Long Grove, Iowa
Toll Gate Doublewhammy Spice, Megan J. Davenport, Litchfield, Conn.

Winter Yearling Heifer:

Lazy M Aint Love Grand-ET, Joseph Hubbard, Thurmont, Md.
Vallowhill Prime Thyme, Kenzie Emery, Sullivan, Wis.

Fall Yearling Heifer:

I-Rainbow Rckstr Marquee, Reed Franke, Richland Center, Wis.
Brone-Ayr Skid Tiffany, Hayden, Allison, Abby Adrian, Cuba City, Wis.

Junior Champion Female:

Sweet-Pepper Predator Shale, leased by Kylie Jordan and owned by Craig and Bonnie Hawksley Hope Valley, R.I.

Reserve Junior Champion Female:

Nor-Bert Reinholts Anique, D, N, R, T, J & T Reinholt and D, D, & B Freeman, Bremen, Ind.

Yearling Heifer in Milk:

Haynes Farm Brachiosaurus, Molly Doty, Fort Edward, N. Y.
Four-Hills Gibbs Angie 5985, Britney, Brad, Jon, Megan & Sara Hill, Bristol, Vt.

Junior Two-Year-Old Cow:

P&A Mack Els Free Beer Welcome , P&A Ayrshires – T, S, K, C & C Kruse, Dyersville, Iowa
Hall’s Millionaire Catalina, Dakota Hall, Cushing, Okla.

Senior Two-Year-Old Cow:

Four-Hills Gibbs Shay 5484, Britney, Brad, Jon, Megan & Sara Hill, Bristol, Vt.

Junior Three-Year-Old Cow:

Onword Lazy M Distinct Arrow, Mackenzie Ullmer, Tyler Boyer and Monica Schwittay, Seymour, Wis.
SL-Acres Homerun Itsouttahere, Starlight Acres-Quentin Scott and Jacob Schaefer, Little Falls, Minn.

Senior Three-Year-Old Cow:

Reinholts Burdette Anneka-ET, D, N, R, T, J & T Reinholt and D, D, & B Freeman, Bremen, Ind.
Trinkle Farms Bjack Emily, Bridey Nolan, Eagle Bridge, N.Y.

Intermediate Champion Female:

Onword Lazy M Distinct Arrow, Mackenzie Ullmer, Tyler Boyer and Monica Schwittay, Seymour, Wis.

Reserve Intermediate Champion Female:

P&A Mack Els Free Beer Welcome, P&A Ayrshires – T, S, K, C & C Kruse, Dyersville, Iowa

Four-Year-Old Cow:

1. Hi-Ayr-View Free Beer Me, Alexis Williams and K, C, & C Kruse, Dyersville, Iowa
2. Old-Bankston JC Bri’s Bikini-ET, Landree Fraley, Muncy, Pa.

Five-Year-Old Cow:

Cedarcut Burdette Clove Colata, Erin Curtis Szalach, Cazenovia, N.Y.
Starlight-Acres Edge Cinabun, Sydney Kleingartner, Gackle, N.D.

Six-Year-Old & Over Cow:

Four-Hills Abush Sammy 3559, Britney & Bradley Hill, Bristol, Va.

Total Performance Winner:

Cedarcut Burdette Clove Colata, Erin Curtis Szalach, Cazenovia, N.Y.

Senior Champion Female:

Cedarcut Burdette Clove Colata, Erin Curtis Szalach, Cazenovia, N.Y.

Reserve Senior Champion Female:

Four-Hills Abush Sammy 3559, Britney & Bradley Hill, Bristol, Vt.

Grand Champion Female:

Cedarcut Burdette Clove Colata, Erin Curtis Szalach, Cazenovia, N.Y.

Reserve Grand Champion Female:

Four-Hills Abush Sammy 3559, Britney & Bradley Hill, Bristol, Vt.

For over five decades, the global dairy industry has been meeting in Madison, Wis. for World Dairy Expo. Crowds of nearly 75,000 people from more than 100 countries attended the annual event in 2016. WDE will return Oct. 3-7, 2017 as attendees and exhibitors are encouraged to “Discover New Dairy Worlds.” Visit worlddairyexpo.com or follow us on Facebook and Twitter (@WDExpo or #WDE2017) for more information.