Eden Prairie, Minn.-based Robinson Fresh continues to grow the avocado category under the Green Giant label with additions to its stickering and packaging capabilities, said Sergio Cordon, avocado commodity manager.
"The strength of the Green Giant label, coupled with the demand for ripe fruit, led Robinson Fresh to develop a ripe sticker that can be applied to fruit being used under any ripe or pre-conditioned program," he said.
Receivers don't want the sticker to say "ripe when ready," he said, they want it to be identifiable to the end user as simply "ripe."
"Most shippers use a sticker that says 'ripe when ready,' but that sticker is also on hard, green fruit and can be confusing to the end user," Cardon said.
Also, Robinson Fresh now offers a gusseted bag under the Green Giant label as an alternative to the mesh bag, he said.
Green Giant was named the No. 3 advertising icon of the 20th century by Ad Age magazine, he said.
Robinson Fresh also is near completion of building a service center in San Bernardino, Calif., which eventually will have the capability of ripening avocados for customers on the West Coast, Cardon said.
With the addition of avocado industry professionals Dan McGrath, market specialist, and Tim Hallows, key account manager, both from Oxnard, Calif.-based Mission Produce, Robinson Fresh is "prepared to help its customers take this category to the next level," he said.
During the fall, the company's main avocado supply will come from Mexico, he added. Robinson Fresh also is expanding its global offerings, supplying avocados to Europe and Asia.
Green Giant fresh avocados are available year-round, either hard or pre-conditioned, from all growing regions, Cordon said.