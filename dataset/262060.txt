Everyone is piling on the red delicious apple.
The latest example crossed my inbox the other day. Another editor with The Packer noticed a story headlined “The Red Delicious is an apple atrocity. Why are we growing billions of pounds of them each year?” published in the Newfoodeconomy.org
Here is how the article, by Tove Danovich, begins:
Almost everyone agrees: The Red Delicious is a crime against the apple. The fruit makes for a joyless snack, despite the false promise of its name, with a bitter skin that gives way to crumbling, mealy flesh.
Ouch. That’s pretty harsh. The author points out reddit.com threads dedicated to the topic “I’ve got honeycrisp tastes on a red delicious budget”  and the “The Awful Reign of the Red Delicious - How the worst apple took over the United States, and continues to spread.”
It is ridiculous for people to say they have never eaten a “delicious” red delicious. I have eaten plenty of sweet red delicious fruit. Not enough, I’ll grant you, but plenty.
It is the “easy take” to say the gala and the Honeycrisp, and perhaps a score of other new varieties, seem certain to eclipse red delicious in the fridge drawer of the American shopper.
Quoting a bevy of solid sources, the author concludes the variety is now the apple of choice for "captive" audiences, such as schools, food banks, hospitals and hotel chains. Consumers in China and Latin America who prefer a cheap, deep red apple, also are among the dwindling throng who appreciates the apple.
The author concludes the durability and low price make the “reviled and rejected” red delicious a utilitarian choice for many. It may be too soon to completely write off the red delicious, but production is diving quickly.
The March 1 U.S. Apple Association storage report showed red delicious holdings of 19.5 million cartons on March 1 were 20% less than last year but still above holdings of 13.9 million cartons of galas and 11.9 million cartons of granny smith and 4.3 million cartons of Honeycrisp.
Size 72 Red delicious apples were priced at $12-17 per carton, well below the $19-30 for gala and $45 to $64 for Honeycrisp.
The American consumer has spoken. They have not come to praise the red delicious, but to bury it.
I ask, simply, what’s the rush? The USDA reports the average retail ad price for red delicious for March 2 was 91 cents per pound, compared with $1.20 per pound for fuji, $1.69 per pound for Pink Lady, $2.39 per pound for Honeycrisp and $1.38 per pound for gala.
Red delicious should be the apple of choice for consumers on a tight budget.
In any case, it is an apple variety that is hard to kill, as this brief history of the red delicious on Stemilt Growers website confirms:
The Red Delicious apple variety was discovered in 1875 as a chance seedling growing on Jesse Hiatt’s farm in Peru, Iowa. Thinking it was a nuisance, Hiatt tried to chop down the seedling, but the tree grew back repeatedly. On the third time, Hiatt allowed it to grow and produce apples.
In 1893, Hiatt took his apple, called Hawkeye, to a fruit show in Missouri. Following the show, Stark Brothers Nursery purchased the rights to market the apple and renamed it Red Delicious. Today, the classic coke-bottle shape and color of Red Delicious makes it one of the most well-known apple varieties in the United States.
 
Once commanding 75% of Washington apple output, the red delicious is in steep decline. The red is not dead yet, but the day is drawing closer. A 2017 Washington apple acreage report shows red delicious acreage in the state dropped from 121,175 acres in 1986 to 39,207 acres in 2017. As a percentage of the state total, red delicious dropped from 75% of total apple acreage in 1986 to 22% in 2017.
Once it leaves the mainstream of consumer apple choices, the red delicious may well be recalled with fondness and nostalgia.
When we are tooling around nursing homes 30 years from now, we will regale each other with stories and reminiscences.
“Remember that bag of Christmas treats that included a big red delicious apple?,” Archie will recall. “Remember when red delicious and golden delicious were about the only apples you could buy? Times were simpler back then!” Edith will say.
Speaking of captive audiences, the nursing home might be the one place that we Baby Boomers will still find America’s one-time favorite apple.