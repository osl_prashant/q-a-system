A Canadian man is facing severe backlash and death threats from animal activists after he and his partner slaughtered and ate a pot-belly pig they had adopted from an animal shelter only a month before.
Now the man says he “feels remorse and regret” for eating the three-year-old Vietnamese pot-bellied pig, named Molly.
 

Molly, a pot-bellied pig rescued last year by the B.C. SPCA, was slaughtered by new owners
https://t.co/60pN0vQlf6
— CBC News (@CBCNews) February 25, 2018

 
The backlash started after the couple posted photos on social media of them preparing the meat. Then, in a Facebook post, he said the pig had attempted to break through a glass door and acted aggressively toward a dog.
"I do promise that Molly died humanely and it was not done for fun or for sport," the Canadian man wrote online.
His apology was met with an outcry of criticism and death threats BBC reports. The post has since been removed.
Animal activists from the SPCA say they were shocked. The adoption process included counseling and the owners committed in writing not to use her for food, says members of the British Columbia Chapter of the Society for the Prevention of Cruelty to Animals.
There are no laws against killing a pet in Canada, as long as it is done humanely. Authorities who investigated the matter found that Molly was killed humanely.
The couple who adopted the pig will no longer be allowed to adopt an animal from any of the SPCA branches in the province.
Animal rights groups are urging people that are upset about the issue to advocate for stricter animal cruelty regulations in Canada.