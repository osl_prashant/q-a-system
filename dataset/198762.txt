According to Monday's Crop Progress report,96% of the corn crop was planted. Next week, the agency plans to release its highly anticipatedAcreage report. Most analysts expect to see a surge in soybean acres, but Ted Seifried of Zaner Ag Hedge says thatwhile there are some compelling ideas why corn acres could be lower, he thinks the corn acreage will actually rise in the June 30 report.Here's why:
Farmers wanted to plant corn. Corn acres went in fast while soybean prices rallied. "We got off to a very quick pace," Seifriedsays. Meanwhile, cornprices rallied along with soybeans this spring, and on a percentage basis, Seifriedsays the corn rally looked better than soybeans. "Guys wanted to plant corn," he says.
Prevented planting acres are going back in. In 2015, heavy rains caused many farmers to take prevented planting. Seifried believesthose acres will come back into production this year and have likely been planted with corn.
Wheat is under pressure. As wheat prices faceheavy downward price pressure due to worldwide oversupply,Seifried thinks some farmers have chosen to plant corn instead.
Seifried looks for the report to reveal 94 million acres of corn, which would be a 400,000-acre increase from the March numbers. He thinks the market has likely factored in 92.5 million acres of corn."That seems to be the darling number everybody wants to talk about," he says.
The acreage increase would be negative for corn prices, but it's not the only factor affecting the market, according to Seifried, who saysweather and yield projections are more important right now."Without yield, a couple million more acres won't matter," he says.
Watch Ted Seifried and Tyne Morgan on AgDay:


<!--
LimelightPlayerUtil.initEmbed('limelight_player_268270');
//-->