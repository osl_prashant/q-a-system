Police arrest 2 in connection with chickens stolen from farm
Police arrest 2 in connection with chickens stolen from farm

The Associated Press

SEEKONK, Mass.




SEEKONK, Mass. (AP) — Police have stopped two people suspected of engaging in fowl behavior in Massachusetts.
Seekonk Police say an officer on patrol noticed a suspicious vehicle parked near a farm around 1 a.m. Sunday. Further investigation revealed feathers strewn along the back seat of the car and a backpack that appeared to be moving on its own.
Police say the officer opened the backpack to discover seven chickens inside.
The car's male driver and female passenger have been arrested on charges of larceny and animal cruelty. Police say they received reports of several chickens and ducks stolen from farms in the area before Sunday's arrest.
Police say the chickens found in the backpack were unharmed and have been returned to their owner.