Editor's note: The following article appeared in the January 2016 issue of Dairy Herd Management.When Lizzy French hears a dairy producer say milk is decreasing and efficiency seems poor, her mind runs through a series of questions to troubleshoot the potential cause.

Dr. Lizzy French, dairy management adviser with DeLaval

"Is this happening on robot one or two? Are there any incompletes, kickoffs, or change with pellet consumption? Does the answer lie in different stages of lactation, time per milking or milkings per day?" Dr. French, dairy management adviser with DeLaval, said. "Are there clues in the milk quality reports, feed bunk consumption, milk yield, stall maintenance, cow or farm hygiene?"
With this range of areas to investigate, the simple answer to finding solutions is to check the numbers. As robotic systems move beyond milking, including automatic feeding and on-farm lab analysis, more information becomes available. And, robots have altered how data is received and processed.
"While robots often are seen as hardware, the true heavy hitter is the software behind the scenes," French said. "Conventional milking has used data management software for years. What is new is the potential to gather information from all areas of the dairy."
Timely and accurate data helps provide the needed feedback to meet robotic herd goals. The system helps improve management capabilities in areas such as reproduction, milk quality, nutrition and housing management.‚Ä®
‚Ä¢ REPRODUCTION. Heats should be entered as well as activity, progesterone, rumination or other methods of observed heat confirmation. Insemination timing relative to detection will help determine the method leading to the greatest conception rate. If heats are not detected, a bigger conversation might be warranted.
‚Ä¢ MILK QUALITY. Somatic cell count, conductivity and lactate dehydrogenase testing help provide an indication of milk quality. Using software to chart this information helps gauge the effectiveness of standard operating procedures over time, she said.
‚Ä¢ NUTRITION. In a robotic system, pellet feed factors into a cow's overall diet. And when using an automated body condition scoring system, feeding routines can be measured and adjusted on the fly to maximize production.
‚Ä¢ HOUSING MANAGEMENT. Robots require cows to come voluntarily, so comfort must be optimal. Data from the addition of bedding, cultures of new bedding and lame- ness observations all give an indication of comfort levels.
"At the end of the day, a clean environment, clean water, wholesome feed, comfortable footing and bedding will show in the numbers," French said.

French is producing an in-depth, year-long education series to enable dairy producers to put their robots to work.