This year’s pistachio crop is expected to be down significantly from last year. The walnut crop is also down, with almonds projected to be up 5%.
Jim Zion, managing partner for Fresno, Calif.-based Meridian Growers, said estimates for this year’s pistachio crop are for about 650 million pounds. Last year’s harvest was more than 900 million pounds, but the decrease was expected because pistachios are an alternate bearing crop and this is the off year.
California’s 2017 walnut crop is forecast at 650,000 tons, down 5% from 2016’s record production of 686,000 tons, according to the U.S. Department of Agriculture’s National Agricultural Statistics Service annual objective measurement report.
“This news was announced the week of Sept. 4, and by Sept. 8, the price increased 20-25 cents per pound, and this was before harvest began,” said Larry Griffith, salesman with Winters, Calif.-based Mariana Nut Co.
Griffith added a big worry is that, with the heat in the Sacramento Valley, the crop will be darker, which will be an issue.
For almonds, NASS’ objective measurement report puts the 2017 California crop at 2.25 billion meat pounds from 1 million bearing acres, an increase of 5.1% from last year’s 2.14 billion pounds.
Wonderful Pistachios & Almonds owns, cultivates and harvests more than 65,000 acres of pistachio and nut orchards and delivers more than 450 million pounds of nuts globally each year, according to the company.
Los Angeles-based Wonderful Pistachios launched a football campaign earlier this month featuring Richard Sherman of Seattle and Clay Matthews of Green Bay.
The football campaign represents part of a $55 million promotion.
The company recently launched multipacks that feature nine pistachio packs.
Joseph Setton, vice president of domestic sales for Commack, N.Y.-based Setton International Foods Inc., said the pistachio industry has seen consumer demand grow over the past decade.
“In response, additional acreage has been planted and those fields are now producing,” he said.
“We are preparing for those additional pounds by continuously building new silos, which hold up to 2 million pounds of pistachios each.”   
Setton International offers a wide variety of value-added products, including its Dark Chocolate Covered Pistachios, now available in a 24-ounce bag.
The company plans to introduce a 21-ounce resealable bag of its Salt & Pepper flavored pistachios this fall.