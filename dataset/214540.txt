In early November, iceberg lettuce in Yuma, Ariz., was about a week away from harvest, says Mark Adamek, director of romaine, Artisan and mixed leaf production for Tanimura & Antle Inc. (Photo courtesy Tanimura & Antle Inc.)

Growers of winter desert vegetables anticipate ample supplies of good-quality product for the holidays.
Some say markets could be stronger than usual this season if hurricane-related shortages pop up in the Eastern growing regions, prompting buyers there to switch to California for some of their winter items.
Peter Rabbit Farms, Coachella, Calif., will ship a variety of vegetables by mid-November, said John Burton, general manager of sales and cooler.
The company’s product line will include butter, red, green and romaine lettuce and spinach.
The plants are showing good yields, he said, and the fields look “absolutely outstanding. We should have plenty of product for shipping the week of Nov. 13.”
Peter Rabbit Farms will have desert vegetables available as late as the first week of April.
Pasha Marketing LLC, Mecca, Calif., will have a good supply of green beans heading into Thanksgiving, said Franz De Klotz, vice president of marketing.
“We gear the harvest to coincide with the Thanksgiving holiday,” he said.
The company expects to have green beans through November.
“Everything looks to be optimum quality,” De Klotz said. “We’ll have promotable quantities on all of our commodities for the Thanksgiving pull.”
Salinas, Calif.-based Tanimura & Antle Inc. will have the same volume as last year on its full line of winter items except romaine hearts, which will see an increase because of growing demand, said Mark Adamek, director of romaine, artisan and mixed leaf production.
The company grows in Yuma, Ariz., during the summer, where many growers are dealing with warm weather-related soil diseases.
There could be shortages of some products out of that area, but Tanimura & Antle is expected to be in good shape because of its proactive farming practices, he said.
Salinas-based The Nunes Co. looked forward to a strong year on its 35 conventional items and a similar number of Foxy Organic vegetables, said Doug Classen, vice president of sales.
The company’s product line includes iceberg, romaine, cauliflower, broccoli and celery.
Growing conditions have been good, he said, and the company expected to be shipping all items out of California’s Imperial Valley by mid-November.
Salinas-based Coastline Family Farms anticipated a smooth transition from Salinas to Yuma and then to Brawley, said salesman Mark McBride.
The lettuce deal was scheduled to start Nov. 13 out of Yuma, then transition to Brawley in December.
“The early lettuce in Yuma and in the Imperial Valley is looking surprisingly good for everything that it’s been through,” he said.
Weather problems stifled some early plantings, he said.
“If you were in the wrong place at the wrong time, you’re going to see some big holes in your acreage out there,” he said.
Romaine, green leaf, red leaf, cauliflower and broccoli all should be shipping out of the desert by the end of November, he said.
There was some speculation that there could be more demand for some items out of the desert this year because of the recent hurricanes in the East, said Jeremy Lane, sales manager for Baloian Farms, Fresno, Calif.
“It’s been a really tough read as to how things are going to play out in the month of November,” he said.
The company kicked off its green bean program Oct. 19 in the Coachella Valley and will have a variety of vegetables.
Castroville, Calif.-based Ocean Mist Farms should have ample supplies of a number of Thanksgiving vegetables out of Salinas, Calif., from its early fall/winter desert deal in California’s Coachella Valley and in Yuma, Ariz., said Jeff Percy, vice president of production for the southern desert region.
The company is using its new expanded cooler in the Coachella Valley for the second year, he said. The facility has more truck docks than before and can offer better service.