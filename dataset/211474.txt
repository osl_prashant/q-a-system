Cattle owner's do mineral supplementation in a variety of different ways according to Eldon Cole, livestock specialist, University of Missouri Extension."Whenever I'm in a pasture with cattle, it is not unusual to find the feeder empty, and it may look like it had been empty for several days. Fortunately, minerals do not require a daily intake as the cattle's body can help tide them through most shortages," said Cole.
However, salt is an item that should be available at all times. Cattle like salt and when deprived of it for a while they respond with a high intake of it for several days.
Most mineral supplements state on the label that consumption averages 2 to 4 ounces per day. Intake depends on the animal's weight, age, forage type and availability.
"Mineral supplements do not need to be complicated if forage quality is good," said Cole.
Quality Forage Helps
A grass-legume mix from southwest Missouri soils may test borderline in copper and zinc.
"Research shows the forage cattle select and consume rates better on nutritional value than what humans collect if they sampled a pasture," said Cole.
During much of the year, the quality pasture may only need salt as the supplemental item.
"Exceptions may be with stressed, undernourished cattle or those with high genetic levels of milk production. It's often difficult to see an economic response to elaborate mineral mixes," said Cole.
Certain trace elements may interact with other minerals causing them to be less available. Molybdenum is an example when its level is high enough it ties up copper so a copper supplement could be needed.
"I encourage farmers to invest a few extra dollars in testing a forage sample for energy and protein, so they know the mineral levels in their hay and pasture. This enables them to make smarter supplement buying decisions," said Cole.
Understand Mineral Levels
Understand the mineral levels on the feed tag. A free-choice, all-purpose beef cow mineral usually will have from 10 to 30 percentage or more plain salt. Calcium will run around 12 to 15 percentage, and phosphorus will be 5 to 12 percentage.
Magnesium will run around one percentage.
"An exception is during the winter or grass tetany season. At that time a ten percentage magnesium level is advised if you have tetany prone cows and suspect forage conditions," said Cole.
Trace mineral levels on the tag expressed as parts per million (ppm) will run as follows: copper, 1000-1500; zinc, 3000-3500; selenium, 12-15; manganese, 2000-3000.
Mineral levels more than requirements may not improve performance and likely will increase cost. The cost of minerals will be higher if used as a carrier for one or more additives such as ionophores or those used for parasite control.
"Mineral supplementation is not a cut and dried or one-size-fits-all matter. Know your animal's requirements and the makeup of the forage or other feed you are providing. Then make sure you keep the mineral/salt out for them free choice," said Cole.