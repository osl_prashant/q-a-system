In its latest crop progress report, the USDA is pegging a 169.5 bushel per acre yield for corn despite declining crop conditions in the I-states.
According to Dan Hueber of the Hueber Report, 2017 started on a lower base than in 2016. He said this time of year, the focus is on the supply, but the demand side is ignored.
 
“Any slip on the yield and you start reducing carryout,” said Hueber. “The old line of reasoning is if carryouts are moving, lower prices tend to move steady to higher. It’s a question mark on how much do we take that down.”
He said if corn slips below 2 billion on ending stocks, people pay attention.
Hear Hueber’s full thoughts on AgDay above.