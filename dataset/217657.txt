Scientists have joined the battle against whiteflies in Georgia, where the pests have plagued produce crops as well as cotton and peanuts.
The University of Georgia Cooperative Extension now has three people — up from one — researching the whitefly.
It is expected that increase will be funded by the addition of about $220,000 to the state budget.
Georgia Rep. Terry England anticipates that amount will be included in the recommended budget the governor will release the week of Jan. 8.
Some companies considered not planting certain vegetable crops this fall given how severely they were damaged by the pest, England said.
The possibility of growers exiting some deals concerned England, who noted that getting back in the mix could be difficult.
“If (customers) can’t count on you year-in and year-out, they’re going to go somewhere else,” England said.
He hopes the research being done at the university can give the growers more information to work with by late spring or early summer.
There is agreement in the state government that action has to be taken on whiteflies, England said.
While the pest might not look like a disaster in the way a hurricane or a tornado does, “this is an emergency within the largest sector of our economy,” England said.