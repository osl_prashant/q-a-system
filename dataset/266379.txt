The American Farm Bureau Federation (AFBF) has released its annual Spring Picnic Marketbasket Survey of 16 items. Of the items surveyed, nine increased in price while seven decreased.

According to John Newton, director of market intelligence for AFBF, most of the increases are because of higher egg prices. A South Korean bird flu outbreak has caused U.S. egg exports to increase.

“A surge in egg exports combined with relatively flat production led to the strong rise in retail egg prices,” said Newton.
Another reason for the price increase is oranges. Florida was hit by a hurricane last year, destroying the orange crop. Growers harvested the smallest crop in 70 years.
Retail price changes from a year ago:

eggs, up 37 percent to $1.80 per dozen
orange juice, up 8 percent to $3.46 per half-gallon
bagged salad, up 4 percent to $2.42 per pound
deli ham, up 3 percent to $5.59 per pound
vegetable oil, up 2 percent to $2.61 for a 32-ounce bottle
shredded cheddar cheese, up 2 percent to $4.20 per pound
ground chuck, up 2 percent to $4.01 per pound
bacon, up 2 percent to $4.75 per pound
sirloin tip roast, up 2 percent to $5.12 per pound
white bread, down 7 percent to $1.60 per 20-ounce loaf
whole milk, down 6 percent to $3.07 per gallon
chicken breast, down 2 percent to $3.10 per pound
toasted oat cereal, down 2 percent to $2.78 for a 9-ounce box
apples, down 1 percent to $1.53 per pound
flour, down less than 1 percent to $2.34 for a 5-pound bag
potatoes, down less than 1 percent to $2.67 for a 5-pound bag


The U.S. spends the lowest average on food compared to any country in the world, spending less than 10 percent of their disposable annual income on food, according to the USDA.