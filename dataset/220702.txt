The U.S. Department of Agriculture has imposed sanctions on four produce firms for failing to pay reparation awards issued under the Perishable Agricultural Commodities Act, including one for more than $1.3 million.
Taylor Produce LLC, Rigby, Idaho, is restricted from the produce industry for failing to pay a $1,327,478 award in favor of an Idaho seller, with Alan Taylor Produce and Alan Taylor listed as members of the business.
USDA previously cited the growers’ agent in November 2015 for failing to remit nearly $4 million to five growers for 605,492 cwt. of potatoes purchased in 2013. The company was banned from operating in the produce industry until October 2017.
While USDA didn’t name the unpaid growers in 2015, several civil cases were filed in 2013 and 2014 against Alan Taylor and the companies associated with him.
Federal court records show companies seeking payment at that time included:

Sunrise Potato, Shelley, Idaho, $2.1 million;
Silver K Farms, Rexburg, Idaho, $611,400;
Kirk Jacobs Farms, Hamer, Idaho, $719,600; and
Reynolds Bros. LLP, Ashton, Idaho, $208,000.

The current $1.3 million reparation award is related to amounts still owed to related companies Silver K Farms, Kirk Jacobs Farms and Reynolds Bros., according to records of a three-day bench trial in U.S. District Court in Idaho in November 2016. 
 
Other sanctions
In addition to Taylor Produce, three other companies have been sanctioned and are currently barred from operating in the industry:

Olympic Wholesale Produce Inc., Chicago, for failing to pay a $32,751 award in favor of a Florida seller. Nicholas Doumouras was listed as the officer, director and major stockholder of the business. USDA also filed an administrative complaint under PACA against the wholesaler in November for allegedly failing to pay $898,725 to four produce sellers over six months, from December 2016 through May 2017.
Tumi Produce International Corp., Bronx, N.Y., for failing to pay a $20,310 award in favor of a Michigan seller. William Bracho and Catherine Bracho were listed as the officers, directors and/or major stockholders of the business.
G. Hinojosa Produce Co. Inc., Houston, for failing to pay a $25,547 award in favor of a Texas seller. Teresa Hinojosa and Greg Hinojosa were listed as the officers, directors and/or major stockholders of the business.