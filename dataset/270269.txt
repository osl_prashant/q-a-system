After reading a news report from Great Britain about a new form of packaging, I had a negative reaction I’m sure many others — at least in my demographic — would share:
The story in The Telegraph newspaper was headlined, “Millennials so squeamish about handling raw meat it is to be sold in touch-free packs.” According to the story, the UK supermarket chain Sainsburys has introduced touch-free packaging, as the article phrased it, “to help squeamish Millennials who are afraid to touch raw meat before cooking it.”
In a statement, Sainsburys said it was offering the “straight-to-pan plastic pouches,” known in the industry as “doypacks,” after a survey revealed that the prospect of coming into contact with uncooked meat products induced high levels of anxiety among shoppers under the age of 35.
“Customers, particularly younger ones, are quite scared of touching raw meat,” Katherine Hall, Sainsburys product development manager for meat, fish and poultry, told The Sunday Times. “These bags allow people, especially those who are time-poor, to just ‘rip and tip’ the meat straight into the frying pan without touching it.”
Sorry, Ms. Hall, but if being “time-poor” were the driving factor, then shoppers would prefer fully prepared, or at least ready-to-cook products, rather than raw meat, which no matter how it’s packaged, still has to be cooked.
Hall also cited an abiding fear of contamination by bacterial pathogens such as campylobacter, a fear that is apparently so great one woman admitted in a focus group to “coating her chicken with antibacterial spray” before cooking it.
A Disturbing Distance from Food
Granted, every new generation adopts lifestyles that often seem foolish to their elders. Witness the music festivals, psychedelic drugs and sexual experimentation of the hippie era as relics of a ridiculous …
Hey, wait a second. Those were good times!
But in all seriousness, the distance from which too many younger people are removed from food production is worrying.
“A lot of younger people are … not preparing as much food in their home,” Hall said. “If they are not used to it, they may think, ‘Ugh! I’d prefer someone else to do it for me.’ ”
Millennials born after 1980 have been dubbed “Generation Snowflake” for their political correctness, particularly about food animals and as The Telegraph article noted, “Their apparent inability to be reconciled to life’s everyday truths, such as the fact that meat may come from a dead animal.”
Of course, those snowflakes aren’t alone in the modern world with their disdain for handling uncooked meat. As the newspaper story noted, a recent report by the consumer research firm Mintel found that nearly 40% of young people do not want to touch raw meat, compared with about one-quarter of the overall population.
That’s not exactly a night-and-day difference. As part of our 21st century lifestyles, many of us are quite comfortable avoiding any contact with raw foods — or learning anything about them.
Witness a grade schooler at the farmer’s market the other day who happened to spy a bunch of carrots that still had the green tops attached.
“What are these?” she asked her mom in genuine confusion.
Where will that girl’s “food preferences” be in another 10 years?
There’s no doubt that convenience is a powerful dynamic driving food marketing, and Sainsburys’ no-touch packaging strategy has been bolstered by strong sales of chicken that can be purchased and roasted right in the bag.
“We have seen the sales data of those, and we are aware they have done very, very well,” Ruth Mason of the National Farmers’ Union told The Telegraph.  “[But] we know one of the reasons is because consumers do not have to touch a raw bird.”
I guess the ultimate destination of this emerging squeamishness is when we no longer have the time or the inclination to touch, prepare or cook anything we eat.
And to think that back in the ’60s, people were ridiculed for wearing bell bottoms.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.