Crop calls
Corn: Fractionally to 1 cent firmer
Soybeans: Narrowly mixed
Winter wheat: Fractionally to 1 cent firmer
Spring wheat: 3 to 6 cents higher
Following yesterday's strong gains in the soybean market, futures were little changed overnight. This led to limited price action in the corn market. Pressure on these markets was limited as rains are expected to delay harvest activity across much of the Corn Belt through early next week. Traders are also concerned about the return of dry conditions across Brazil's soybean belt. Also this morning, USDA announced an unknown destination has purchased 195,000 MT of corn for 2017-18, which signals prices are a value. Spring wheat futures benefited from short-covering, which limited pressure on winter wheat futures.
 
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Livestock futures are expected to see a mixed start to the day as traders even positions ahead of the weekend. Live and feeder cattle futures are working on slight weekly gains and front-month live cattle hold around a $2 premium to last week's $108 cash cattle trade. So far this week, only a few hundred head of cattle have sold at steady levels with week-ago. Meanwhile, hog futures are working on sharp weekly gains and nearby futures hold a lofty premium to the cash market, which appears to be working on an early seasonal low. Hog futures are nearing overbought territory, making them vulnerable to pre-weekend profit-taking.