Dairy farmers across the country are feeling the pinch of tight milk prices, but some are coping by getting bigger. According to a reportreleased by Rabobank last week,the number of large dairy farms (those with 1,000 cows or more) has grown and continues to increase.One analyst says that's because many large farms use risk management strategies to smooth out the volatility of the milk business."A lot of big producers have got the time, andthey've got the resources and energy to really understand the marketing aspect of the dairy business," says Brian Doherty,a strategist with Stewart Peterson. "A number who work with us have been hedged up for a while and forward sold, sothey won't feel the pain as much as somebody who's done nothing in this situation."
Watch the AgDay story:

<!--

    function delvePlayerCallback(playerId, eventName, data) {
        var id = "limelight_player_351368";
        if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
            DelvePlayer.registerPlayer(id);
        }

        switch (eventName) {
            case 'onPlayerLoad':
                var ad_url = 'http://oasc14008.247realmedia.com/RealMedia/ads/adstream_sx.ads/agweb.com/multimedia/prerolls/agwebradio/@x30';
                var encoded_ad_url = encodeURIComponent(ad_url);
                var encoded_ad_call = 'url='   encoded_ad_url;
                DelvePlayer.doSetAd('preroll', 'Vast', encoded_ad_call);
                break;
        }
    }

//-->


<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->

Want more video news? Watch it on AgDay.
Doherty says some large farms have even been able to pay down debts and build working capital despite the global glut of milk. However, small and mid-size farms are feeling the squeeze.
"I think the small and mid-size are on hold or reducing," he says.