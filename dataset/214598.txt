Ensuring workers are legally able to work in the U.S. and are properly documented is important for every agricultural business, whether it is a processing plant, a dairy or an orchard, said two Texas A&M AgriLife Extension Service specialists.
Ellen Jordan, AgriLife Extension dairy specialist in Dallas, and Tiffany Dowell Lashmet, AgriLife Extension agricultural law specialist in Amarillo, have joined forces to write a new publication to help those in the agricultural industry deal with the issue.
They wrote the publication after the U.S. Bureau of Immigration and Customs Enforcement, known as ICE, conducted audits and raids at dairies, slaughter plants and other agricultural businesses in recent years.
“The Difference between an ICE Raid and an ICE Audit: Are You Prepared?” can be downloaded from the AgriLife Bookstore at: http://bit.ly/2iEwwLc.
“Whether an ICE audit or raid occurs at a traditional agriculture business such as a dairy or produce farm or at an urban business, the impacts can cripple a business,” Jordan said. “In animal agriculture, if employees are lost or quit out of fear, it might be difficult in the short term to take proper care of the animals. For produce farms and fruit orchards, crops might rot in the field when insufficient numbers of workers are available.”
Both Jordan and Lashmet said compliance with immigration laws is critical for all businesses, as “knowingly hiring” someone unauthorized to work in the U.S. is illegal and can result in legal fees, fines and prison time, and could threaten a business’ survival.
ICE audits and raids are two different events and employers should understand the differences and their rights with regard to both.
“For example,” Lashmet explained, “an ICE audit will generally involve a Notice of Inspection rather than a search warrant, and an employer has three business days to comply with the documents requested. On the other hand, an ICE raid will involve a search warrant and agents will likely demand information identified in the warrant be provided immediately.”
Lashmet said the key to surviving an ICE raid or audit is to be prepared for such an incident before it occurs. She recommends having company policies and procedures in place to ensure every employee knows what to do in the event ICE comes knocking.
“This is particularly critical on a dairy, where cows do not like their daily routines changed,” Jordan indicated. “The procedures should also include biosecurity measures to protect the health of the cows.”
Additionally, it is critical for all employers to comply with the rules governing an Employment Eligibility Form, or I-9, compliance for all employees.
“Ensuring a company has its Form I-9’s complete and in compliance with the law is the best defense to claims of knowingly hiring illegal workers,” Lashmet said.
To ensure a business is ready for an audit, she suggested conducting an internal audit through an outside human resource consultant or an attorney. These internal audits review information and check compliance just as an ICE audit would do.
In the event there are corrective measures needed, employers would have the chance to seek additional information from employees and ensure compliance before an actual audit ever occurred.
Other tips offered in the publication state employers should:

Have an appointed spokesperson to meet with ICE agents.
Immediately contact counsel if ICE comes to the business.
Always be respectful to agents.
Never alter or destroy any documents.
Always ask to inspect a search warrant if one is being relied upon.
Insist upon a receipt identifying all written documentation removed from the business by ICE agents.

“Ensuring compliance with immigration laws, being prepared in the event an audit or raid occurs, and understanding one’s rights is critical for all businesses to ensure success and survival,” Lashmet said.