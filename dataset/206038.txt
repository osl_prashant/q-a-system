Click here for related charts.

CORN COMMENTS
The national average corn basis widened 4 3/4 cents from last week to 31 3/4 cents below December futures. The national average cash corn price declined 6 1/4 cents to $3.18 1/4.
Basis is weaker than the three-year average, which 15 cents below futures for this week.
 
SOYBEAN COMMENTS
The national average soybean basis softened 6 cent from last week to stand 28 cents below November futures. The national average cash price firmed 2 1/2 cents from last week to $9.42.
Basis is well below the three-year average, which is 56 cents above futures for this week.
 
WHEAT COMMENTS
The national average soft red winter (SRW) wheat basis firmed 3 1/2 cents from last week to 21 1/4 cent below December futures. The average cash price improved a dime from last week to $4.28 1/2. The national average hard red winter (HRW) wheat basis improved 1 1/2 cents from last week to 85 1/2 cents below December futures. The average cash price improved 5 1/4 cent from last week to $3.62 1/2.
SRW basis is firmer than the three-year average of 44 cents under futures, while HRW basis is weaker than the three-year average, which is 71 cents under futures for this week.
 
CATTLE COMMENTS
Cash cattle trade averaged $105.89 last week, up 97 cents from the previous week. Only light cash trade has been reported so far this week, but it's occurred at 50 cent to $1 higher than week-ago. Last year at this time, the cash price was $1`05.89.
Choice boxed beef prices firmed $1.64 from last week at $192.04. Last year at this time, Choice boxed beef prices were $187.17.
 
HOG COMMENTS
The average lean hog carcass bid dropped $5.45 over the past week to $60.92. Last year's lean carcass price on this date was $57.63.
The pork cutout value declined $3.70 from last week to $75.75.  Last year at this time, the average cutout price stood at $78.03.