BC-NY--New York News Digest,ADVISORY, 6 pm Update, NY
BC-NY--New York News Digest,ADVISORY, 6 pm Update, NY

The Associated Press



Members can send stories and tips to apalbany@ap.org for upstate and apnyc@ap.org for the New York metro area. The New York desk can be reached at 212-621-1670. The photo desk is reachable at 212-621-1902.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
NEW:
— NYC SCHOOLS-BULLYING LAWSUIT, GOTTI GRANDSON-SENTENCING, STUDENT WALKOUTS-GUN VIOLENCE, NY CORRUPTION-MANGANO, OFFICERS SLAIN-PAROLE, STATE BUDGET, ANIMALS DEATHS ON PLANES, FBN--BILLS-FREE AGENCY, FBN--BILLS-MURPHY
TOP STORIES:
CUOMO AIDE-CORRUPTION TRIAL
ALBANY, N.Y. — The conviction of a former top aide to Democratic Gov. Andrew Cuomo spurred fresh demands to address Albany's culture of backroom dealing Wednesday — calls that New York state lawmakers have shrugged off in the past. By David Klepper. SENT: 610 words.
STUDENT WALKOUTS-GUN VIOLENCE
It's expected to be the biggest demonstration yet of the student activism that has emerged following the massacre of 17 people at a Florida high school. Students from Maine to Hawaii have planned walkouts Wednesday to protest gun violence. Some students plan roadside rallies. Others are to hold demonstrations in school gyms or on football fields. Others are taking aim at lawmakers, planning to converge on state capitols demanding gun control laws. By Collin Binkley. SENT: 820 words, to be updated from protest actions.
SHOOTING DEATHS-BROOKLYN
NEW YORK — The bodies of four apparent shooting victims, including a 1-year-old girl, were found in a Brooklyn apartment Wednesday, hours before students throughout New York City participated in walkouts to demand action on gun violence. Police were trying to determine whether there were three homicides and a suicide. SENT: 350 words, photos, video.
XGR--CHILD SEX CRIMES
ALBANY, N.Y.  — Actor Corey Feldman, who says he was sexually molested as a young teen, gave his support Wednesday to childhood sex abuse victims and their decade-long effort to get the New York state Senate to pass the Child Victims Act. SENT: 430 words.
NY CORRUPTION-MANGANO
CENTRAL ISLIP, N.Y. — Opening arguments began on Wednesday in the bribery trial of former Nassau County Executive Edward Mangano, his wife and a former town supervisor in a case that a federal prosecutor described as "corruption and greed at the highest level." SENT: 412 words.
NYC SCHOOLS-BULLYING LAWSUIT
NEW YORK — New York City has settled a federal lawsuit filed in 2016 by public school parents who charged that school violence and bullying were not being adequately addressed. By Karen Matthews. SENT: 300 words.
NORTHEAST STORM
BOSTON —  The scrape of snow shovels and the drone of snow blowers filled the New England air on Wednesday as the region cleaned up from a storm that left 2 feet of snow in some places and tens of thousands without power. SENT: 391 words, photos, video.
With:
—  NORTHEAST STORM-NEW YORK
— NORTHEAST STORM-NEW YORK OUTAGES
— NORTHEAST STORM-THE LATEST
SEXUAL MISCONDUCT-STUDYING MEN
NEW YORK — Michael Kimmel may be made for this moment. The 67-year-old sociologist at Stony Brook University is a leader in what's known as "masculinities studies," and an in-demand purveyor of insight on why men are the way they are. The field he helped develop has long had men's misdeeds as an area of focus, but it's gained newfound exposure and relevance with #MeToo and #TimesUp. By National Writer Matt Sedensky. SENT: 1256 words, photos.
ANIMALS DEATHS ON PLANES
United Airlines, under siege over the death of a puppy on one of its flights, says the flight attendant who ordered a passenger to put her pet carrier in the overhead bin didn't know there was a dog inside. By David Koenig. SENT: 700 words.
IN BRIEF:
— GOTTI GRANDSON-SENTENCING: The namesake grandson of notorious mob boss John "Dapper Don" Gotti has been sentenced to a five-year prison term in connection to the mob-linked revenge burning of another man's car.
— STUDENT WALKOUTS-GUN VIOLENCE: Students from at least two Long Island school districts have been told they will be disciplined for participating in a national school walkout to protest gun violence.
— NY CORRUPTION-MANGANO: Opening arguments have begun in the bribery trial of a former suburban New York county executive, his wife and a former town supervisor.
— OFFICERS SLAIN-PAROLE: A former member of a 1970s black radical group who was convicted of fatally shooting two New York City police officers has been granted parole.
— STATE BUDGET: The New York state Senate and Assembly have both endorsed competing budget recommendations, a key step toward passage of a final state spending plan.
— STATE BUDGET-PREVAILING WAGE: Construction workers and union leaders are renewing their call for the state Legislature to close a loophole in New York law that allows companies to avoid paying the prevailing wage on certain publicly funded projects.
— LAKE ONTARIO-WATER LEVEL: Regulators are pushing a record amount of water out of Lake Ontario to avoid a repeat of last year's flooding.
— COMMUNITY SOLAR: Gov. Andrew Cuomo says the state's largest community solar project has been completed in Sullivan County.
— BROOKLYN-SAKE: New York City's first sake brewery has opened in Brooklyn, using American-grown rice and New York water to make the distinctive Japanese rice wine.
— FAKE LAWYER: Prosecutors say a New York City man pretended to be a lawyer specializing in wrongful convictions in order to swindle as much as $15,000 from convicts and their families.
— HERRING FISHING: Some vessels that harvest herring off the East Coast will be limited in how much they can bring to shore for the rest of the year.
ARTS & ENTERTAINMENT:
OPERA-OVERDUE DEBUT
NEW YORK — Veteran soprano Evelyn Herlitzius is making coast-to-coast U.S. opera debuts in daunting Wagner roles. The German dramatic soprano recently triumphed at New York's Metropolitan Opera as Kundry in Wagner's "Parsifal," and in June she will portray Bruennhilde in his "Ring" cycle in San Francisco. By Mike Silverman. SENT: 986 words, photo.
FOX NEWS-SLAIN DNC STAFFER
NEW YORK — The parents of a Democratic National Committee employee who was killed in 2016 allege Fox News exploited the slaying of their son as a "political football." By Karen Matthews. SENT: 415 words.
— MUSIC-MILEY CYRUS LAWSUIT: Jamaican artist Flourgon has sued Miley Cyrus and Sony Music for the singer's 2013 song, "We Can't Stop."
SPORTS:
FBN--BILLS-FREE AGENCY
ORCHARD PARK, N.Y. — Bills general manager Brandon Beane wasn't fooling around when saying he had a lot of work to do shortly after Buffalo enjoyed its first playoff appearance in 18 years. By John Wawrow. SENT: 720 words, photos.
HKN--PENGUINS-RANGERS
NEW YORK— Mats Zuccarello and the New York Rangers look for their fifth win in eight games as they host the second-place Pittsburgh Penguins. New York is 4-2-1 in its last seven. Pittsburgh, winner of 10 of its last 14, trails Washington by one point for the Metropolitan Division lead.
BKC--NCAA-BUFFALO-CLARK
AMHERST, N.Y. — Wes Clark went from his college career being stuck in limbo over a two-year span to playing a key role in helping the University at Buffalo win the Mid-American Conference championship and secure its third NCAA Tournament berth in four years. By John Wawrow. SENT: 833 words, photo.
FBN--GIANTS-SOLDER
EAST RUTHERFORD, N.J. — The New York Giants have started fixing their woeful offensive line. A person familiar with the negotiations says the team has an agreement with offensive tackle Nate Solder. The person spoke on condition of anonymity because the Giants did not announced the deal with the former New England Patriots left tackle when free agency kicked off Wednesday. By Tom Canavan. SENT: 566 words, photos.
BBN--METS-TEBOW
PORT ST. LUCIE, Fla. — Tim Tebow was reassigned by the New York Mets to their minor league camp on Tuesday after he went 0 for 4 with four strikeouts in an exhibition against the Houston Astros. SENT: 351 words, photo.
BBN--METS-WRIGHT:
PORT ST. LUCIE, Fla. — After yet another medical setback that will stretch his layoff from the major leagues past two years, New York Mets captain David Wright hopes to start baseball workouts in May. SENT: 520 words, photos.
— BBA--YANKEES-WALKER: Neil Walker doubled twice in four at-bats in his first game action since signing Monday with the New York Yankees.
— FBN--JETS-WILLIAMSON: The New York Jets have signed linebacker Avery Williamson to a three-year contract worth $22.5 million.
— FBN--JETS-ENUNWA: The New York Jets have tendered wide receiver Quincy Enunwa at the second-round level, worth $2.9 million.
— FBN--BILLS-MURPHY: Two people with direct knowledge the decision confirm to The Associated Press the Buffalo Bills have agreed to sign free agent defensive end Trent Murphy to a three-year contract.
If you have stories of regional or statewide interest, please email them to apnyc@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867.