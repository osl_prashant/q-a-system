Global dairy prices edged higher on Wednesday as volumes fell, bucking a recent trend of declining prices.
The GDT Price Index rose 0.4 percent, with an average selling price of $3,091 per tonne, in the fortnightly auction held in the early hours of the morning.
The index rose after falling for four consecutive auctions, underpinned by higher whole milk and skim milk powder.
Prices had risen in the second quarter on strong global demand and as production eased, but have since had a bumpy ride as supply has picked up.
"Auction volumes were down ... so in this context a lift was expected and actually quite modest," ASB said in a research note.
It said it was sticking to its 2017/18 milk price forecast of $6.50 per kg and expected prices to consolidate in coming auctions.
"Short-term price risks are largely evenly balanced, with dry (New Zealand) weather on one hand and improving global production on the other."
Prices of whole milk powder, the most widely traded product, rose 1.7 percent and skim milk powder rose 4.7 percent. But butter slumped 11.1 percent.
A total of 29,514 tons was sold at the latest auction, falling 15.8 percent from the previous one, the auction platform said on its website.
The auction results can affect the New Zealand dollar as the dairy sector generates more than 7 percent of the nation's gross domestic product. The New Zealand dollar last traded 0.2 percent higher at $0.6875 to the U.S. dollar.
GDT Events is owned by New Zealand's Fonterra Co-operative Group Ltd , but operates independently from the dairy giant.
U.S.-listed CRA International Inc is the trading manager for the twice-monthly Global Dairy Trade auction.