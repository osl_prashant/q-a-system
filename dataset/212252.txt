CHICAGO – Taylor Farms CEO Bruce Taylor kicked off the FreshTec portion of the 2017 United Fresh Expo by imploring all produce industry stakeholders to partner and invest in innovation and technology.“With technology, what we’re looking for isn’t yet available on the expo floor,” he said.
Taylor was keynote speaker in the June 13 conference focusing on the future of innovation.
Taylor Farms provided good examples of the struggles of innovating and growing with the challenges with labor, changing consumer trends, food safety and regulation.
He referenced the labor problems Taylor Farms had the week before that saw the union employees strike for a day demanding a wage increase. The company settled with the employees and will now pay $1.50 an hour more with another $1 increase in January.
“Labor cost last week cost us $30 million in one day,” he said.
But that only highlighted the importance of innovation and hiring skilled workers.
“Automation should replace a job that is tough to fill today,” Taylor said. “We want to create jobs that Americans will aspire to hold.”
Along those lines, the produce industry needs to attract young skilled talent.
“We’re not in old school farming,” he said. “We’re in the food business, and we need to attract new people to the industry.”
He said his company has a long list of technologies he’s looking for that don’t exist. But he’s convinced they will if enough stakeholders partner and invest.
“We can probably run a processing plant in the dark,” he said, referring to such automation that workers aren’t even necessary. “We’re not even close to that now.”