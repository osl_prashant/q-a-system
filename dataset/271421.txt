Taylor Farms has added three chopped salad kits with some unique flavors: Avocado Ranch, Maple Bourbon Bacon and Buffalo Ranch.
The salads are available nationwide, and bring the total Taylor Farms chopped salad line to more than 13 kits.
The new kits include, according to a news release:
Avocado Ranch — green cabbage, romaine, carrots, green onions and cilantro with corn, taco-seasoned cheese and avocado ranch dressing;
Maple Bourbon Bacon — romaine, broccoli, red and savoy cabbage, green onions, carrots and coarse-cut smoky bacon, golden honey almonds and maple-bourbon vinaigrette; and
Buffalo Ranch — romaine, broccoli, red and savoy cabbage, green onions and carrots, topped with buffalo-seasoned crouton crumbles, Monterey Jack cheese and buffalo ranch dressing
Taylor Farms Project Manager Charis Neves said the company created the salads by studying what’s trending in quick-service restaurants and extensive recipe research with consumers.
For example, the company noted the increase in avocado sales/consumption in relation to the Avocado Ranch salad kit.
"Avocados are continuing to grow in popularity as consumers purchase these superfoods at a rapid pace. Retail sales of avocados are up 17.1 percent year-over-year," according to the release.
Chopped salad kits is fastest-growing segment in the salad category — 18% in year-over-year sales in the last 12 weeks, and 16% in the last 24 weeks, according to figures from Nielsen in the release.
“ … By bringing to market flavors that resonate with today’s consumer, we make it easy to enjoy a fast and healthy meal in no time,” Neves said in the release.
Other Taylor Farms chopped salad kits include:

BBQ Ranch
Sweet Kale
Farmhouse Bacon
Greek
Mediterranean Crunch