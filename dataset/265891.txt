Correction: Hawaii-Fishing Boat Sinks story
Correction: Hawaii-Fishing Boat Sinks story

The Associated Press

HONOLULU




HONOLULU (AP) — In a story March 26 about a fishing vessel that sank near Hawaii, The Associated Press, relying on information provided the U.S. Coast Guard, reported erroneously that the Coast Guard observed only the stern of the vessel above water. Images provided by the Coast Guard show the bow, not the stern, of the vessel above the waterline. The Coast Guard also erroneously reported the length of the vessel. It was 61 feet, not 89 feet.
A corrected version of the story is below:
Fishing boat sinks, crew saved hundreds of miles off Hawaii
The U.S. Coast Guard says a commercial fishing crew and a federal observer have been rescued after their vessel sunk hundreds of miles off the coast of Hawaii
By CALEB JONES
Associated Press
HONOLULU (AP) — A commercial fishing crew and a federal observer were rescued after their vessel sank and they spent hours in a life raft hundreds of miles off the coast of Hawaii, the U.S. Coast Guard said Monday.
The agency said it received an emergency distress alert from the Princess Hawaii late Sunday morning about 400 miles (644 kilometers) north of the Big Island. A few hours later, a Coast Guard plane got to the area, where rescuers saw a flare and found eight people in a life raft.
The 61-foot longline fishing boat was mostly submerged with only the bow above water.
Officials said the Coast Guard air crew dropped a radio to the life raft and helped establish communication with the vessel's sister ship, the Commander, which was fishing nearby and went to rescue the survivors. It arrived nearly 12 hours after the distress call and brought the crew aboard, Coast Guard spokeswoman Tara Molle said.
She said the crew was in good condition and was expected to arrive back in Honolulu later this week.
Most longline fishing vessels in Hawaii use foreign crews with no U.S. work visas. The workers cannot legally enter the United States so they are required to live aboard their vessels for the duration of their contracts, often a year or two at a time.
Most workers come from impoverished Southeast Asia and Pacific island nations and are paid between $300 and $600 dollars a month.
The observer on the boat was part of a National Oceanic and Atmospheric Administration program that monitors the actions of commercial fishing crews at sea. Observers log data about catch, interactions with endangered species, vessel conditions and crew safety.
NOAA officials said they could not identify the observer who was aboard the Princess Hawaii. The agency is working with the Coast Guard to determine what role the observer played in alerting authorities to the sinking, spokeswoman Jolene Lau said.
A request for the NOAA observer's log from Sunday's Princess Hawaii voyage was denied.
The Magnuson-Stevens Act states that "all observer information is confidential and may not be disclosed, subject to certain very narrow exceptions," said NOAA's Office of General Counsel Pacific Island Section Chief Frederick Tucher in an email.  "Observer information includes all information collected, observed, retrieved, or created by an observer."
The boat was inspected by the Coast Guard in February, and no safety violations were found. It was in 10-foot (3-meter) seas with winds around 20 mph (32 kph) before it sank, authorities said.
The Coast Guard said in the statement Sunday that it "called the registered owner, who confirmed the vessel had gone out early that morning to fish." The agency said Monday that it could not confirm the name of the owner or any information about the crew.
According to NOAA permit records, the Princess Hawaii is owned by Holly Fishery LLC. The Western and Central Pacific Fisheries Commission lists the captain of both the Princess Hawaii and the Commander as Loc Nguyen of Honolulu.
State commercial fishing license records show the two boats employ workers from Indonesia, Vietnam, and Kiribati.
Attempts to reach the owners Monday were unsuccessful. A call to the satellite phone aboard the Commander was answered, but the connection was bad and the man who answered hung up.