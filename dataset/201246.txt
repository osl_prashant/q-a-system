The midyear appraisal update conducted by Farm Credit Services of America (FCSAmerica), Omaha, Neb., found farmland values held steady in Iowa and Nebraska while slipping slightly in South Dakota. The exceptions were Wyoming, which saw a slight boost in values, and eastern Kansas, which saw prices slide somewhat. The update comes as the large lender's team of appraisers, working in alliance with Frontier Farm Credit in eastern Kansas, update 71 benchmark farms across their service area. The update is conducted twice a year.
Benchmark farm values overall remain unchanged in Nebraska and increased a slight 0.4% in Iowa and 0.7% in Wyoming. South Dakota’s benchmark farm values inched down a moderate 1.8%. Meanwhile, in eastern Kansas, where FCSAmerica operates in alliance with Frontier Farm Credit, benchmark values were off about 3%.
FCSAmerica notes sales activity across the five states down 21% in the first half of 2017 compared to the same period in 2016.
“It appears that the pace of decline in land values that we have seen during the past two years is slowing even though pressure on profit margins continues for grain producers,” states Mark Jensen, senior vice president and chief risk officer for FCSAmerica and Frontier Farm Credit. “Our customer conversations remain focused on cost management, marketing plans that align with cash flow, including living expenses, and balance sheet structure needed for optimal risk protection.”
State-by-state trends 
IOWA - Cropland prices, which increased marginally in the second quarter of 2017, are in line with those reported in 2015, but remain 19% below the record prices of 2013. The average price for unimproved ground sold during the second quarter of 2017 topped $8,100. Twenty-one percent of all sales in the second quarter had a per-acre price above $10,000. This was up 11% compared to the previous year.
Public land auctions are down 4% for the year compared to the first half of 2016. However, second quarter sales were 18% higher than in the second quarter of 2016. The percentage of "no sales" fell from 4.7% in the first half of 2016 to 1.7% for the same period this year.


KANSAS - Cropland prices declined minimally from the last half of 2016. An acre of farmland sold for an average of $3,900 during the first six months of 2017. Public land auctions were down 38% from the first half of 2016.

NEBRASKA - Prices on dry cropland fell sharply during the second quarter of 2017, in part because sales were down. Sales also occurred in atypical locations, as reflected in a historically low soil quality average. About a third of the sales occurred in western Nebraska.
Prices on irrigated cropland, by comparison, were consistent with the last half of 2016 and reflected better soil quality in the first half of 2017. The average per-acre price was near $6,600.
Public land auctions dropped 28% from the same period in 2016.



SOUTH DAKOTA - Unimproved cropland values weakened in the first six months of 2017, which was consistent with a decline in the average quality of ground sold. The average price per acre was $4,000.

Public land auctions dropped 29% compared to the same period a year ago. "No sale" at auction increased to 5.4%, up from 3.9% for the first half of 2016.


WYOMING - The number of completed sales in Wyoming so far this year has declined 47%. The limited number and diverse nature of sales, as well as highest and best use of the land, make it difficult to identify sales trends.