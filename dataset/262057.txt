Senate committee OKs Arkansas animal waste disposal bill
Senate committee OKs Arkansas animal waste disposal bill

The Associated Press

LITTLE ROCK, Ark.




LITTLE ROCK, Ark. (AP) — A committee of the Arkansas Senate has approved a bill designed to designate when and for how long third-parties can protest against farms that require liquid animal waste permits.
Farmers say state law should spell out that, if state regulators grant a waste permit, others should not be able to mount continuous challenges. The bill's supporters say the proposal would give banks a certain peace of mind if they know their clients aren't being sued often.
The Senate Public Health, Welfare and Labor Committee approved the bill on a voice vote Wednesday. An identical version of the bill passed a House panel.
Arkansas legislators are meeting in a special session that started Tuesday. Gov. Asa Hutchinson also wants them to look at reimbursement rates for prescription drugs, and other matters.