Pat Dockstader’s California farming entity, P&T Enterprises, had been growing organic citrus for other packers for years.
Now, he and his nephew, Dusty Dockstader, are launching Doc’s Organics, a grower-shipper of organic lemons, grapefruit, minneolas and sweet limes under their own label — Doc’s Organics — to retail customers in the U.S. and Canada.
Doc’s Organics plans to open a $7 million, 29-acre exclusively organic packing and office facility July 27 in Brawley, Calif., and is scheduled to pack its first lemons around Aug. 1, said Mike Rietman, general manager.
Rietman said it’s the first organic fruit packing line in the Imperial Valley. 
Dockstader decided it was time to stop sending his fruit from the Imperial Valley to other packers, Rietman said.
“One of the ideas was in a sense he was competing against himself. Now, he won’t be sending it to them,” Rietman said.
The company will market its fruit under its Doc’s Organics label.
Doc’s, which has about 630 acres of citrus, anticipates an annual volume of up to 400,000 to 450,000 cartons from districts 2 and 3, with a packing capacity of 300-350 bins a day August through May, Rietman said.
“Some of the acreage is still young,” he said.
The company will source fruit from California’s coastal growing region during its peak season, from January to May, with the balance of production coming out of the Imperial Valley, Rietman said.
“And, we intend to pack for some outside growers,” he said.
The combined square footage of the five buildings on the property is about 40,000 square feet, and the packing plant has a potential capacity of about 1 million cartons per year, Rietman estimated.
The company also plans to construct a date-packing operation on the property in the next few years, he said. Doc’s Organics expects to pack 250,000 pounds of medjool dates its first season, he said.
For the present, Doc’s will market all of its fruit under its own label, but the company likely will expand into custom packing for other growers, Rietman said.
“We’re still sorting out who will do the marketing in different regions,” he said.
At the start of the season, Doc’s likely will provide up to 70% of the total organic lemon supply in the U.S., Rietman said.
“We’re one of the largest suppliers of organic lemons at the beginning of the season,” he said. P