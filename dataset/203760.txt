Researchers and Food and Drug Administration officials will discuss the use of whole genome sequencing in surveillance and investigative programs during a web seminar set for Aug. 25.
 
The free web event, offered in partnership between Western Growers, Produce Marketing Association and the United Fresh Produce Association, will take place at 2:30 to 3:30 p.m. Eastern on Aug. 25, according to a news release.
 
The release said FDA experts on whole genome sequencing will give an overview of the technology and how it is being used for regulatory and produce safety research. 
 
Featured FDA speakers at event, according to the release, are:
Eric Brown, director of the Division of Microbiology in the Office of Regulatory Science;
Errol Strain, director of the Biostatistics and Bioinformatics Staff in the Office of Analytics and Outreach; and 
Karen Jarvis, research microbiologist in the Virulence Mechanisms Branch.
Registration for the event is available online.