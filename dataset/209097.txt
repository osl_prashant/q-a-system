You gotta love the Aussies.
Not only have they invented an entire genre of everyday slang that’s practically another dialect of English, they find unique ways of describing even technical processes.
Here’s the lead of a story appearing on a website called Startsat60.com:
“The prospect of cartridges filled with liquefied offal and mince may be stomach-churning, but Meat and Livestock Australia isn’t ruling out the technology as having a place on your plate in the future.”
I don’t think so.
Waiter: “And you sir?”
Diner: “I’ll have the liquified offal and mince, thanks, and can I get some extra ketchup with that?”
But according to a recent story in The Australian, representatives of Meat and Livestock Australia told attendees at a conference at Monash University in Melbourne that 3D food printers “would soon be as common in restaurants as coffee machines and microwaves.”
“We are not saying this technology will replace all sausages and steaks,” said MLA Tomorrow food manager Michael Lee, “but that on some occasions, 3D-printed meat will be available.”
A 3D food printer made in The Netherlands was display at the conference, producing what The Australian described as “delicate flower-shaped morsels of reconstituted beef” that a chef turned into canapes and other appetizers.
It’s a long way from 3D printing to a professional chef creating canapes, but the real reason industry leaders are excited by the potential of 3D meat isn’t because some super chef can create appetizing hors d’œuvres out of whatever fleshy blob the printer spits out, but because the process could potentially add value to ground meat and trim.
Natural vs. Synthetic
There are three serious problems with 3D meat printing, beyond the obvious connection of the term “printing” with food.
One is the use of what consumers would perceive as “chemicals,” “hormones” and “additives” to describe the biological slurry of proteins used as the substrate for a 3D meat -printing system. You think the strident organic advocates, who demonize conventional meat and dairy foods with every pejorative in the book are going to embrace a totally artificial process that’s as far removed from Nature — figuratively speaking — as humanly possible?
Test-tube meat and 3-D steaks are going to provide a field day for advocates of natural organic, grass-fed, free-range, and every other alternative positioning to conventional livestock production.
And a lot of consumers who are becoming accustomed to the antibiotic-free, non-GMO, family-farm-friendly format for animal foods may not be so quick to embrace 3D meat.
You love factory-farmed foods? Then step right up and hit the button that says “PRINT.”
Second, there is the challenge of taste and texture.
We modern consumers are pretty particular about what we like, and what we don’t like in our staple food products. Do you think the big food processors and marketers spend multi-millions running taste panels and focus groups to adjust spice levels and modify mouthfeel and even fine-tune the apparent degree of doneness for all the thousands of prepared food products that line the shelves and stock the cold cases of America’s supermarkets for no apparent reason?
By the way? That was a rhetorical question.
Even the most highly processed, totally “artificial” food products, i.e., formulated veggie shamburgers as one example, are branded and positioned as clean, natural and healthy, with lots of fresh-from-the-farm imagery of wholesome produce and grains — all to create an aura that a food product manufactured in high-tech systems like the ones that turn out industrial components is somehow akin to what a farm family 100 years ago would be sitting down to eat at Sunday dinner.
We’re happy to accept cutting-edge technology when it powers our smartphones and iPads. But when it comes to what we feed our families, not so much.
The convenience of a 3D food printer may indeed find a place in consumer kitchens. But for the foreseeable future, it’s likely to be similar to those high-end, all-purpose juicers that cost a week’s pay.
And about as commonplace.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.