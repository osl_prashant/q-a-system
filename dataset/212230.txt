Quality Fruit and Vegetable Co., El Paso, Texas, was awarded a maximum $41 million contract for fresh fruits and vegetables on June 13 by the Defense Logistics Agency. 
The contract, according to a news release, is for five years, containing one 24-month tier and two 18-month tiers. The contract service area includes Texas and New Mexico, with a June 12, 2022 performance completion date, according to the release. 
 
Customers for  the  contract include the Air Force, Marine Corps, U.S. Department of Agriculture schools and Native American reservations, according to the release.