Amazon Fresh received strong marks from consumers who recently tested the delivery service, according to research firm Field Agent.The firm recruited 20 shoppers to spend at least $10 on groceries — including chilled and fresh produce items — from Amazon Fresh.
Women accounted for 75% of the group. The mission represented the first encounter with Amazon Fresh for 60% of the shoppers. More than one-third were between the ages of 25 and 34 years old, and more than one-third were between the ages of 45 and 54.
The vast majority of the shoppers — 80% — reported that the groceries arrived in very good condition, with another 10% describing the condition as good.
Consumers gave similarly high marks for the freshness of the produce they received, with 75% marking it extremely fresh and another 20% describing it as very fresh.
The time-saving element was mentioned repeatedly by consumers as a plus, according to Field Agent.
“I usually spend four to five hours on Saturdays with my three kids going to three to four stores,” one shopper said, per the report.
On the flip side, delivery fees and monthly membership were sour notes — and so was another reason frequently cited as a major obstacle to online grocery.
“I most dislike that you cannot pick your own produce,” one shopper told Field Agent. “Sometimes I want to be able to see what I am buying first.”
As for whether they would use the service again, 58% of the shoppers said it was completely likely or very likely. Only 17% said it was not very likely.