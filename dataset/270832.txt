Guess who’s coming to dinner? 
At Easter, it’s easier to say who is not coming to join the feast. I’m talking, of course, about fresh produce and how so many items will have a strong and sustained lift — as many like to say in the grocery game. Spread out on the facings, as you’ll need the holding power. Here’s a sneak peek at the guest list.
Fruit
Strawberries lead the list here, for fruit salads, platters and desserts. Pineapple will warrant equal space for many applications but primarily to top and surround the big ham. Other fruit that will move well include grapes, citrus (oranges for salads, lemons and limes for zest and libations), apples for Waldorf salads, melons for chunks and more.
Vegetable sides
Potatoes top the must-have vegetable dish for Easter. Count on staples such as asparagus, broccoli, summer squash, corn, green beans, sweet potatoes, cauliflower and mushrooms to have substantial sales increases too.
Relish vegetables
Imagine the relish platters from years past and you will recall: carrots, celery, bell peppers (all colors), green onions, radishes, parsley, cherry tomatoes and even jicama and Belgian endive earns a spot with the relish crowd. Order plenty of all. It’s hard to be long on this category going into the holiday weekend.
Salads
All leafy greens will move well for Easter. The same with head lettuce and spinach. Salad fixings will include cucumbers, peppers, avocados, tomatoes, radishes, apples and mushrooms. The green or wet rack person will need extra help or extra preparation to stay ahead, as this is the hardest-hit section of the produce aisle during any holiday week.
To-go trays
Many shoppers are apt to grab ’n’ go as they head out as a guest at a dinner and promised to bring something. That something can be the fresh-cut fruit or vegetable tray (made in-house or otherwise). Order extras as these sell well and are a big produce ring at the checkstand.
Other
Lots of extras help drive sales. Consider ramping up your orders on refrigerated dips and dressings, ready-mix dressing or dip-mix packs. Fresh herbs will sell tenfold in many stores (remember Thanksgiving?) as will some root vegetables (even fresh horseradish in some areas). 
 
Because Easter is synonymous with baskets, why not make some fruit baskets for the occasion? These make a great item for those visiting friends or relatives who wish to offer the host a fresh gift.
 
Easter is a welcome holiday in all respects. As for holiday dinners, fresh produce plays an important role. Ensure you are ready with fresh, abundant displays and enough hands on deck in order to maximize your sales and profits during this all-important week.
 
Armand Lobato works for the Idaho Potato Commission. His 40 years’ experience in the produce business span a range of foodservice and retail positions. 
 
armandlobato@comcast.net
 
What's your take? Leave a comment and tell us your opinion.