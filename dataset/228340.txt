BC-USDA-Natl Sheep
BC-USDA-Natl Sheep

The Associated Press



SA—LS850San Angelo, TX    Wed Feb 21, 2018    USDA Market NewsNational Sheep Summary for Wednesday, February 21, 2018Compared to last week at Sioux Falls, SD all classes steady.Slaughter Lambs:   Choice and Prime 2-3 90-150 lbs:Sioux Falls:    shorn and wooled 120-130 lbs 155.00-159.00; 135-140 lbs143.00-148.50.Virginia:       wooled 90-110 lbs 182.00-210.00; 130-160 lbs 141.00.Slaughter Lambs:  Choice and Prime 1-2:Sioux Falls:    60-70 lbs 215.00-220.00; 79 lbs 182.50; 86 lbs 187.50.Virginia:       60-90 lbs 180.00-235.00.Slaughter Ewes:Sioux Falls:    Good 3-4 (very fleshy) 67.00; Good 2-3 (fleshy)66.00-77.00; Utility 1-2 (thin) 57.00-67.00; Cull 167.00-70.00.Virginia:       Good 2-4 82.00.Feeder Lambs:   Medium and Large 1-2:Sioux Falls:    55 lbs 270.00; 76 lbs 245.00.Virginia:       no test.Replacement Ewes: Medium and Large 1-2:Sioux Falls:    no test.Virginia:       no test.Sheep and lamb slaughter under federal inspection for the week to datetotaled 22,000 compared with 22,000 last week and 21,000 last year.Source:  USDA Market News Service, San Angelo, TXRebecca Sauder 325-653-1778www.ams.usda.gov/mnreports/SA—LS850.txtwww.ams.usda.gov/LSMarketNews1600  rs