Crop calls 
Corn: 2 to 3 cents lower
Soybeans: 1 to 2 cents lower
Wheat: 1 to 3 cents lower

Corn and wheat futures faced profit-taking overnight after traders added some weather premium into the market last week. Rains across the eastern Corn Belt were more scattered than previously forecast, although heavy rains were seen across the Delta and areas of the eastern Corn Belt over the weekend. Soybean futures held up better than corn and wheat futures overnight despite their bearish technical outlook. Soybean traders are reevaluated positions as futures are oversold according to the nine-day Relative Strength Index.  Also this morning, USDA announced an unknown buyer has purchased 130,000 MT of old-crop soybeans.
 
Livestock calls
Cattle: Lower
Hogs: Higher
Cattle futures are vulnerable to followthrough from Friday's negative Cattle on Feed Report. The report showed all categories above pre-report guesses and signals marketings down the road will be larger than previous expected. Expectations the cash cattle market has posted a seasonal high and showlists will be larger this week will limit buying in nearby futures to short-covering. Meanwhile, hog futures are expected to see a boost from steady to $1 higher cash hog bids amid strong packer demand thanks to positive margins. The premium nearby hog futures hold to the cash index could be a limiting factor for bulls.