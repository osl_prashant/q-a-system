URI will try to improve fisheries management in Philippines
URI will try to improve fisheries management in Philippines

The Associated Press

PROVIDENCE, R.I.




PROVIDENCE, R.I. (AP) — The University of Rhode Island will try to improve fisheries management in the Philippines with a new $25 million federal grant.
URI said Monday it's the single largest grant in the university's history.
It says the fishing industry in the Philippines is in peril, largely because of destructive fishing practices, typhoons and coastal degradation.
The Coastal Resources Center at URI's Graduate School of Oceanography will implement the five-year project to increase fish stocks by improving fisheries management and building the resilience of fishing communities. It's expected to benefit up to 2 million people.
The center has partnered with the U.S. Embassy in the Philippines' U.S. Agency for International Development and a consortium of universities and non-governmental organizations.
The center is also working on fisheries projects in Africa and Central America.