Fruit crops grown in Fresno County last year fared better than vegetable crops and other agricultural products.
The 2015 Fresno County, Calif., crop report shows the total value of agriculture produced in the county was $6.61 billion, down 6.55% from 2014.
Fresno County Farm Bureau CEO Ryan Jacobsen said the dip was expected.
"The cumulative effect of drought, commodity prices, as well as production, particularly when you look at things such as pistachios, which slipped out of the top 10 crops - all contributed to a weaker return," Jacobsen said.
"It was down probably more than I expected that it would be down, but nevertheless the number is still a fairly strong number when it comes to the reflectiveness of how important ag is to this economy."
The production value of the top 10 crops accounts for about 75% of the value of agricultural products in Fresno County, Jacobsen said.
That list included seven items from the produce department - almonds (down about 7.4%), garlic (down nearly 2%), grapes (down about 1%), tomatoes (down about 1%), peaches (up 16.3%), oranges (up 15.2%) and mandarins (up 171.3%).
The fruit and nut category accounted for 50.7% of the value of agriculture produced in the region, and the vegetable category was the second-largest at 18.9%.
The non-produce items on the top 10 list - poultry, cattle and calves, and milk - all saw declines.
Jacobsen offered cautious optimism about what he expects the story will be next year.
"As far as forward-thinking, when we start talking about what, in 2017, what the 2016 crop report's going to look like, hopefully looking at some strengthening there," Jacobsen said. "... For many of our top 10 crops last year, it happened to be a year in which we saw values slide. We've seen a lot of those values either stop sliding or maybe start to just slightly turn back upwards, so I would suspect that the drought's still going to play some havoc on our numbers there, but I would suspect that our number next year would probably be fairly close to what we saw this year, if not just a very, very, very small percentage upwards."
Mandarins saw by far the biggest increase of any product on the top 10 list. The value of mandarins sold in Fresno County skyrocketed from $72.8 million in 2014 to $197.6 million in 2015.
The amount of acreage harvested barely changed from 2014-15, but the overall yield was much better. Even so, the jump in total value corresponded mainly to growers receiving a higher price for the fruit.
Jacobsen said mandarins have become increasingly popular for multiple reasons.
"It's the branding as well as the ease that folks can consume this fruit with," Jacobsen said.
"It's obviously a lot of marketing geared at children asking their parents to buy them, but I know the parents and grandparents love them as well."