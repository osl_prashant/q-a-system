More than 150 cattlemen from 14 states gathered in Springfield, Mo., for Drovers Cowboy College that began today. The program designed specifically for cow-calf producers, focuses on animal health, management and genetic strategies that improve profitability.
Two of America’s leading veterinarians, Dan Thomson and Mike Apley, both from Kansas State University, led the morning program with a discussion about management programs including antibiotic protocols for better animal health.
Dr. Thomson told the group there are two reasons an animal or person gets sick: extreme exposure to a pathogen, or suppressed immune response.
The afternoon session was led by Mark Gardiner, Gardiner Angus Ranch, Ashland, Kan., who described the tremendous progress his family’s operation has achieved through genetic selection.
The afternoon session concluded with “low-stress animal handling techniques,” presented by Tom Noffsinger, DVM, Binkleman, Neb. 
Cowboy College will conclude on Wednesday with more discussions on nutrition, antibiotic use and sustainability led by Dr. Apley and Dr. Thomson.
The next Cowboy College will be feedlot focused and is scheduled for August 16-17, in Kearney, Neb.