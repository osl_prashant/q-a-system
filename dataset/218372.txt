H-E-B holds a market share of an astounding 60% in south Texas, but its number in the region slipped slightly in 2017.
The Shelby Report reveals that H-E-B market share in south Texas (San Antonio, Austin, Waco, Laredo, Corpus Christi, McAllen, Brownsville) as of 2017 was 59.6%, down 1.6 points compared with the previous year. 
Total retail sales in south Texas were rated at $17.09 billion by The Shelby Report.
Walmart was a distant second at 26.8%, and its market share grew a modest 0.1 points in 2017, according to Shelby Report numbers. 
Retail market shares in south Texas for 2017, compared with year-ago levels:

H-E-B: 204 stores, 59.6% market share, down 1.6 points;
Walmart: 138 stores, 26.8% market share, up 0.1 points;
Albertsons/Safeway: 15 stores, 2% market share, no change;
Grocers Supply: 52 stores, 1.8% market share, down 0.1 points;
Affiliated Foods (wholesale): 45 stores, 1.7% market share, down 0.1 points;
Lowe’s: 43 stores, 1.7% market share, no change; and 
Whole Foods: 7 stores, 1.5% market share, up 0.1 points.