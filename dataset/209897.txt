Steven Muro, president and CEO of Chatsworth, Calif.-based Fusion Marketing, tells members of the Anaheim, Calif.-based Fresh Produce & Floral Council that, “Retail isn’t dying, it’s transforming,” during the FPFC’s Oct. 4 luncheon meeting. Photo by Tom Burfield.
 
CERRITOS, Calif. — For those who fear that the retail industry may be dying, Steven Muro had some somewhat reassuring words.
“Retail isn’t dying,” he told members of the Anaheim, Calif.-based Fresh Produce & Floral Council. “It’s transforming.”
Speaking at the FPFC’s Oct. 4 luncheon, the president and CEO of Chatsworth, Calif.-based Fusion Marketing said factors like online purchasing, home delivery and new channels — including drug stores and dollar stores — are transforming retail.
He pointed out that 6,000 retail stores of all kinds have closed so far this year and offered suggestions to help supermarket chains and their produce suppliers survive.
There really aren’t too many stores, he said, there are just too many stores selling and promoting the same things.
“There will be a shakeout,” Muro said, and that shakeout will be determined by shoppers.
“We need to put ourselves in the shoes of our shoppers,” he said.
Addressing Amazon’s recent purchase of Whole Foods Market, he said that acquisition likely won’t be Amazon’s only foray into the grocery business.
Muro expects Amazon eventually to expand its grocery store presence by snapping up regional operations one at a time.
Some traditional grocery retailers have responded by developing their own home-delivery service, but he said it may make more sense to purchase an existing company than to spend time building one.
He cited Albertsons recent acquisition of Plated, the meal kit home delivery service, as an example.
He advised retailers to “create a shopping experience” with new formats, more open space, visual appeal and appetite appeal.
Add fun and entertainment, he suggested, and make shopping easy and enjoyable.
At the same time, vendors need to work with retailers to leverage their product in new ways, he said.
Produce suppliers should try to sell their products into the deli or foodservice section of the supermarket in addition to the produce department.
“Retail foodservice is huge,” Muro said.
They also should work with retail dietitians, who can promote a product not only to shoppers in-store but through mainstream media and through their own social media channels.
“Reframe your product line,” was another recommendation Muro made, citing Minis brand small avocados from Mission Produce in Oxnard, Calif., that are used to make treats like avocado toast.
Finally, he urged retailers and suppliers to “give shoppers the information you want them to have” by offering recipes, nutrition facts and other information at point-of-purchase and on packaging.