Purdue researchers look to take grain storage bags worldwide
Purdue researchers look to take grain storage bags worldwide

The Associated Press

WEST LAFAYETTE, Ind.




WEST LAFAYETTE, Ind. (AP) — Purdue University researchers who designed specially sealed bags to keep insects from eating harvested grain are looking to go worldwide.
The Purdue Improved Crop Storage bag is designed to cut off the oxygen supply, eliminating insect damage in storage of dry grain without using chemicals. The Purdue Research Foundation says it was invented by entomology professor Larry Murdock, who used grants from the Bill & Melinda Gates Foundation to introduce it in several African nations over the past decade.
The researchers created a company called PICS Global Inc. to sell the bags in larger counties such as India, where they don't have a major donor to fund the project. The research foundation says the bags have reached more than 56,000 villages and at least 12.5 million bags have been sold.