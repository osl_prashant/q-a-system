Lots of suppliers of a range of fresh fruit and vegetable categories — including organic, locally grown, fresh-cut and specialty items — benefit consumers in the Baltimore-Washington, D.C., metro region, produce suppliers say.
The benefits include price, said Lolo Mengel, CEO/partner with Jessup, Md.-based Coosemans DC Inc.
“Consumers are looking for value — the market is much more competitive,” she said. “Many more independent stores, independent chains (and) large domestic and international supermarket chains and big box retailers are all competing for business.”
Consumers also have a wide variety of “ethnic produce” in major chains, Mengel said.
Restaurants are plentiful as well, said Larry Quinn, president of Jessup-based distributor G. Cefalu & Bro. Inc.
“Foodservice seems to be the growth area, as well as fresh-cut,” he said, noting that there also has been consolidation in some of the wholesale and retail markets.
Organics is the main driver of business at Jessup-based wholesale distributor Lancaster Foods Inc., said John Gates, president.
“Organics remains a push for the area,” he said.
Lancaster also is involved in a busy fresh-cut produce segment, Gates said.
“People are wanting a high level of service and we’re doing some forward distribution,” he said. “People who want to buy direct, we’re providing services for that.”
The area’s diverse demographic mix also feeds sales of specialty produce items, Gates said.
“What happens is, you start seeing that stuff in stores, and people try them,” he said. “We’re seeing the number of (stock-keeping units) we handle increase.”
At Washington-based wholesale distributor Pete Pappas & Sons Inc., tomatoes, berries, peppers and watermelons are driving sales, said Gus Pappas, president.
“Sales seem to be increasing each year, with good demand,” he said. “I’d say sales over the last seven years have just exploded for us.”
Landover, Md.-based Keany Produce Co. continues to work on expanding its distribution base. The company opened a new distribution center 2 1/2 years ago in Richmond, Va., which has helped expand the company’s distribution area, said Roy Cargiulo, sales manager.
“It helped expand our footprint into the state of Virginia and probably at some point will get us into the Carolinas,” he said. “It’s made our footprint grow from North Carolina state line to Princeton, N.J.”