Cold stress with cattle is often associated with below zero temperatures, especially in the Northern Plains. In reality, cattle that are adapted to the cold conditions found in Northern Plains winters can function and perform well under a wide range of conditions. Under dry conditions with a heavy winter hair cost, a mature cow can tolerate temperatures down into the teens, especially if she has some wind protection.
That adaptation goes out the window if we introduce wet conditions. A wet hair coat has no more insulative value than a slick summer coat (Table 1). If a cow is soaked and the air temperature is 20° F, that is 39 degrees below her critical temperature. That would be the same amount of cold stress as 21 degrees below zero if she had a dry, heavy winter coat. To add insult to injury, the storms that cause these conditions typically come with wind. The effect of wind speed on effective temperatures is shown in Table 2.
Table 1. Lower Critical Temperatures for Beef Cattle



Coat Condition

Critical Temperature, °F.



Wet or Summer Coat

59°



Dry, Fall Coat

45°



Dry, Winter Coat

32°



Dry, Heavy Winter Coat

18°




 
Table 2. Wind Chill Temperature, Degrees F.



Wind Speed

Air Temperature, °F.



Calm

15°


20°


30°


35°


40°



5 mph

8°


13°


23°


28°


33°



10 mph

3°


8°


18°


23°


28°



20 mph

-5°


0°


9°


14°


19°



30 mph

-21°


-16°


-6°


-1°


3°




Management Challenges
Energy Requirements
The added stress due to cold and wet conditions caused by spring storms comes at a critical time in a spring-calving cow/calf ranch. Early lactation represents the highest energy requirements of the entire production cycle. Furthermore, the clock begins ticking after calving for the cow to resume cycling activity and maintain 365 day calving interval. So any increased maintenance energy requirements due to cold stress makes the challenge of providing her sufficient energy that much more difficult.
Newborn Calves
There are also the newborn calves to consider. The risk of death loss from hypothermia is very real under cold and wet conditions. Even mild cases of hypothermia can lead to problems with colostrum intake and absorption. A more thorough discussion of the impacts of cold stress on calves is available in the iGrow Cold Stress and Newborn Calves publication.
Steps to Take
While there is little that can be done to change adverse weather conditions, there are steps that producers can take to deal with spring storms and minimize negative impacts. Some of those steps include:

Provide for as much shelter as practical, whether natural or man-made. Shelterbelts, wooded draws and windbreaks can be invaluable forms of shelter.
Bedding will help minimize heat losses from the body, especially for calves. Newborn calves spend about 80% of the time laying down, so providing a dry surface out of the wind will help the calf keep dry and preserve body heat.
Be prepared to warm newborn calves in the case of hypothermia. Immersing calves in warm water, physically drying them off with towels, or placing them under heat lamps are all methods that have been used successfully.