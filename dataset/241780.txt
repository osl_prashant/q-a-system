Washington Legislature phase out Atlantic salmon farming
Washington Legislature phase out Atlantic salmon farming

By PHUONG LEAssociated Press
The Associated Press

SEATTLE




SEATTLE (AP) — The Washington Legislature has voted to phase out marine aquaculture of Atlantic salmon in the state.
Atlantic salmon net pens have operated for decades in Washington but have come under fire after tens of thousands of nonnative salmon escaped into local waters last summer.
After lengthy debate Friday night, the Senate passed House Bill 2957 on a 31-16 vote. The bill now goes to Gov. Jay Inslee, who has expressed support.
The bill would end state leases and permits for operations that grow nonnative finfish in waters; the last current lease expires in 2025.
The measure targets Cooke Aquaculture, the largest producer of farmed Atlantic salmon in the U.S., which currently has two active leases with the state.
Sen. Kevin Ranker, a Democrat, said Friday night that the measure would protect the Salish Sea and thousands of jobs that depend on it.
Republicans who opposed the bill said it would put people out of work, shut down a vital industry and set a bad precedent.