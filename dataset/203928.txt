A Kansas City, Mo.-based nonprofit that gets fresh produce to people in need has reached a milestone and inked new partnerships with area groups.
For its efforts, After the Harvest is winning an award this year.
In 2016, the group, which was founded in 2014, passed the 5 million mark - 5 million pounds of food shipped through food banks and food pantries in Kansas and Missouri.
Through June, said executive director Lisa Ousley, the organization had shipped 5.4 million pounds.
"We're in the middle of a good year."
After the Harvest collects off-grade fruits and vegetables and donates them to food banks in Missouri and Kansas.
The organization accepts full truckloads and product gleaned from fields, and pays for the packaging and transportation of the produce, even picking up rejected loads from the site of rejection.
Donors also get a tax write-off, Ousley said.
After the Harvest has two new industry partners, The Giving Grove and Fresh Farm HQ.
The Giving Grove plants fruit and nut trees and perennial food plants around the Kansas City area, Ousley said. Thousands of trees have been planted in recent years, and this year, many of them are starting to bear fruit.
Produce from those trees and plants will be donated through After the Harvest to area food banks and pantries.
The group's second new partner, Fresh Farm HQ, is a Kansas City-area food hub that connects local growers with area restaurants and other buyers.
Produce that Fresh Farm HQ can't find a home for gets donated to After the Harvest, Ousley said.
Also happening in 2016, in recognition of its partnership with Kansas City-based Harvesters food bank, After the Harvest is winning the organization's donor of the year award, Ousley said.
"We're their largest produce donor by far, and the second-largest donor of all products."
Until 2015, After the Harvest was housed in Harvesters. Now, the organization operates out of a VFW building in Kansas City's Midtown area.
Also new at After the Harvest, the nonprofit is partnering with Kansas City-based Cross Lines, supplying fruits and vegetables to the organization, which has classes teaching people in need how to cook.
Ousley said After the Harvest serves a clientele that, while not starving, faces malnutrition and the health problems - high blood pressure, diabetes and others - that can come with it.
"A lot of people can't afford to eat right, or they don't have access to healthy foods. The key thing is that what we get is all nourishing: fresh produce, the best food you can buy."