BC-Cash Prices, 1st Ld-Writethru,0342
BC-Cash Prices, 1st Ld-Writethru,0342

The Associated Press

NEW YORK




NEW YORK (AP) — Wholesale cash prices Wednesday
                                         Wed.       Tue.
F
Foods

 Broilers national comp wtd av            1.0875     1.0875
 Eggs large white NY Doz.                   1.31       1.36
 Flour hard winter KC cwt                  15.30      15.20
 Cheddar Cheese Chi. 40 block per lb.     2.2175     2.2175
 Coffee parana ex-dock NY per lb.         1.1853     1.1749
 Coffee medlin ex-dock NY per lb.         1.3936     1.3801
 Cocoa beans Ivory Coast $ metric ton       2834       2834
 Cocoa butter African styl $ met ton        7558       7558
 Hogs Iowa/Minn barrows & gilts wtd av     58.58      58.28
 Feeder cattle 500-550 lb Okl av cwt      172.00     172.00
 Pork loins 13-19 lb FOB Omaha av cwt      90.40      91.09
Grains
 Corn No. 2 yellow Chi processor bid      3.70½       3.65¼
 Soybeans No. 1 yellow                   10.12½      10.07¼
 Soybean Meal Cen Ill 48pct protein-ton  377.10      376.80
 Wheat No. 2  Chi soft                    4.86¼       4.73½
 Wheat N. 1 dk  14pc-pro Mpls.            7.49½       7.52¾
 Oats No. 2 heavy or Better               2.53¾       2.49¼
Fats & Oils
 Corn oil crude wet/dry mill Chi. lb.     .29          .29 
 Soybean oil crude Decatur lb.            .29¼          .29½
Metals
 Aluminum per lb LME                     1.0078      1.1121
 Antimony in warehouse per ton             8275        8275
 Copper Cathode full plate               3.1691      3.1401
 Gold Handy & Harman                     1321.65    1328.85
 Silver Handy & Harman                    16.574     16.728
 Lead per metric ton LME                 2348.50    2348.00
 Molybdenum per metric ton LME            26,000     26,000
 Platinum per troy oz. Handy & Harman     916.00     919.00
 Platinum Merc spot per troy oz.          906.60     928.90
 Zinc (HG) delivered per lb.              1.4698      1.4582
Textiles, Fibers and Miscellaneous
 Cotton 1-1-16 in. strict low middling     80.39      77.94
Raw Products
 Coal Central Appalachia $ per short ton   61.45      61.45
 Natural Gas  Henry Hub, $ per mmbtu        2.77       2.74
b-bid a-asked
n-Nominal
r-revised
n.q.-not quoted<29
n.a.-not available