Wonderful Pistachios wouldn’t be the fastest growing snack brand without the right packaging, said Adam Cooper, vice president of marketing for the largest pistachio grower and marketer, The Wonderful Co., Los Angeles.
The company has new packaging for its in-shell line and later this winter will update the look of shelled pistachios.
Matt Mariani, director of sales and marketing for Mariani Nut Co., Winters, Calif., said the health attributes of many nut varieties have elevated the category.
“We’re marketing simplicity on our packaging, allowing (consumers) to see product quality and freshness,” he said.
Mariani said walnut pieces and almond slivers are both showing strong growth, taking advantage of the consumer trend toward convenience.