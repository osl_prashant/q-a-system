Senate out all week; House in session from Tuesday forward



Immigration reform, NAFTA 2.0 talks, a farm bill listening session and key USDA reports are the highlights of the week ahead for Washington-related events. The Senate is not in session this week, but the House works from Tuesday forward. 
White House sends Congress plans for immigration enforcement. The White House sent Congress an expansive set of principles that would increase immigration enforcement at the border and inside the U.S. and would limit new legal arrivals, recommendations guaranteed to frustrate most if not all Democratic lawmakers. 
The Trump administration is calling for the funding of a border wall, a crackdown on minors from Central America, and cuts to grants for “sanctuary cities.” The principles were denounced by Democratic leaders, who had hoped to forge a deal to protect younger immigrants who were brought to the country illegally as children.
NAFTA 2.0 negotiations continue in Washington with the fourth round slated for Oct. 11-15. The latest talks are expected to get more specific, with U.S. trade officials offering some detailed proposals, including ones on contentious agriculture matters such as new antidumping provisions sought by Florida tomato operations and other produce growers to challenge Mexican exports. 
House Ag Chairman Mike Conaway (R-Texas) said in a statement Saturday evening that the meetings he and other lawmakers held in Ottawa this past weekend were “productive” and that farmers would stay engaged with the talks “to ensure their interests are taken into account. This is too important to screw up.” 
The House Agriculture Committee continues its farm bill listening sessions with a stop in upstate New York, with dairy issues likely a key topic. The panel has previously held listening sessions in Florida, Texas, Minnesota, California and Illinois.
The new chairman of the Commodity Futures Trading Commission, Chris Giancarlo, will testify before the House Ag Committee on Wednesday about the agency's agenda. Conaway said he wanted to hear from Giancarlo “about his plans for the CFTC and his agenda to promote strong risk management markets.”
USDA sub-Cabinet positions will see some activity this week with Steve Censky as deputy secretary and Ted McKinney as the new undersecretary for trade being sworn in Tuesday morning during a ceremony at USDA. 
The Waters of the U.S. (WOTUS) rule is the topic at the Supreme Court on Wednesday, where the High Court will hear arguments over where legal challenges to rule should be brought to the federal district courts or the federal courts of appeals.
Economic reports and events this week include Wednesday's release of FOMC minutes, and Friday releases of the Consumer Price Index for September and Retail Sales. Several Federal Reserve officials will speak this week. 
Key USDA reports come Thursday with the Crop Production and World Agricultural Supply and Demand Estimates reports. Both Crop Progress (Tuesday) and Weekly Export Sales (Friday) are delayed one day from their usual releases due to the Columbus Day government holiday on Monday. 
Other events on tap this week include: 
Tuesday, Oct. 10 

Wall Street Journal Global Food Forum, New York. FDA Commissioner Scott Gottlieb and USDA Secretary Sonny Perdue will be interviewed during the afternoon sessions. 
China trade: US Trade Representative holds a meeting on China's acts, policies, and practices related to technology transfer, intellectual property, and innovation.
World Bank/IMF: World Bank and International Monetary Fund (IMF) fall meeting October 10-15, including the release of the World Economic Outlook report.

Wednesday, Oct. 11

Highways and transit infrastructure: House Transportation and Infrastructure Highways and Transit Subcommittee hearing on "Building a 21st Century Infrastructure for America: Highways and Transit Stakeholders' Perspectives."
World Bank/IMF: World Bank President Jim Yong Kim; and IMF Director Christine Lagarde, deliver remarks.
Canadian Prime Minister Justin Trudeau will meet with House Ways and Means Committee members to discuss NAFTA and a range of other trade issues.

Thursday, Oct. 12

World Bank/IMF: World Bank President Jim Yong Kim; and IMF Director Christine Lagarde, hold press conferences.

Friday, Oct. 13

American Enterprise Institute hosts panel discussion, “U.S. Agricultural Policy in Disarray.”