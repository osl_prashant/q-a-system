Lawmakers say Trump exploring rejoining Pacific trade talks
Lawmakers say Trump exploring rejoining Pacific trade talks

By KEN THOMAS and KEVIN FREKINGAssociated Press
The Associated Press

WASHINGTON




WASHINGTON (AP) — President Donald Trump has asked trade officials to explore the possibility of the United States rejoining negotiations on the Pacific Rim agreement after he pulled out last year as part of his "America first" agenda.
Farm-state lawmakers said Thursday after a White House meeting with Trump that he had given that assignment to his trade representative, Robert Lighthizer, and his new chief economic adviser, Larry Kudlow. The Trans-Pacific Partnership would open more overseas markets for American farmers.
"I'm sure there are lots of particulars that they'd want to negotiate, but the president multiple times reaffirmed in general to all of us and looked right at Larry Kudlow and said, 'Larry, go get it done,'" said Sen. Ben Sasse, R-Neb.
Eleven countries signed the agreement last month. Trump's rejection of the deal has rattled allies and raised questions at home about whether protectionism will impede U.S. economic growth.
Kansas Sen. Pat Roberts, the chairman of the Senate Agriculture, Nutrition and Forestry Committee, said he was "very impressed" that Trump had assigned Kudlow and Lighthizer "the task to see if we couldn't take another look at TPP. And that certainly would be good news all throughout farm country."
The discussions came during a meeting in which Trump told farm-state governors and lawmakers that he was pressing China to treat the American agriculture industry fairly. Midwest farmers fear becoming caught up in a trade war as Beijing threatens to impose tariffs on soybeans and other U.S. crops, a big blow to Midwestern farmers, many of whom are strong Trump supporters.
__
On Twitter follow Ken Thomas at https://twitter.com/KThomasDC and Kevin Freking at https://twitter.com/apkfreking