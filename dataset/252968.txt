Female firefighter to lead Forest Service amid scandal
Female firefighter to lead Forest Service amid scandal

By MATTHEW BROWNAssociated Press
The Associated Press

BILLINGS, Mont.




BILLINGS, Mont. (AP) — A female wildland firefighter has been tapped by the Trump administration to steady the U.S. Forest Service as it reels from allegations of sexual misconduct and struggles to change its male-dominated culture.
Vickie Christensen was appointed interim chief of the 35,000-employee agency late Thursday. The move came roughly 24 hours after former Chief Tony Tooke abruptly retired following revelations of an investigation into alleged relationships with subordinates.
Christiansen has been with the Forest Service for seven years and became a deputy chief in 2016. Before joining the federal government she'd worked in forestry for 30 years at the state level, in Arizona and Washington.
Agriculture Secretary Sonny Perdue said she was tasked with two goals: improving the agency's response to sexual misconduct while effectively managing more than 300,000 square miles (777,000 square kilometers) of forests and grasslands in 43 states and Puerto Rico.
The agency's widespread problems mirror recent misconduct scandals within the nation's other major public lands agency, the Interior Department.
Tooke's departure came just days after PBS NewsHour reported he was under investigation following relationships with subordinates prior to his appointment last August.
The events renewed calls from Congress to more aggressively address longstanding and rampant problems of sexual harassment, bullying and in some cases rape.
Abby Bolt, a Forest Service employee in California with a pending sexual discrimination complaint against her male supervisors, told The Associated Press that rumors of Tooke's relationships started circulating within the agency as soon as he was appointed.
Bolt, a fire battalion chief now on leave, said she was hopeful Christiansen would bring some "fresh eyes" to the Forest Service's problems. But she also wanted Tooke held to account for any wrongdoing.
"If we just have somebody retire and step down," Bolt said, "then we never learn the facts, who else might be involved nor do we have the opportunity to learn from it."
Preliminary results of a sexual harassment audit released Thursday by the Agriculture Department's inspector general said that almost half of employees interviewed expressed distrust in the process of reporting complaints.
In an email to Forest Service employees, Perdue said more steps already were being taken to protect victims from retaliation. Those include using outside investigators for at least the next year to investigate sexual misconduct allegations, according to the agency's response to the Inspector General's audit.
Just over a third of the Forest Service's permanent employees are women, a figure that drops during the summer with additional seasonal hires, according to agency statistics.
Perdue's office has not responded to repeated questions about whether the investigation into Tooke would continue. Agency officials also declined to answer if an outside investigator was handling his case.
Tooke said in a final note to employees that the agency deserved a leader with "moral authority" as it addresses reports of rampant misconduct and bullying of female employees. He did not directly deny the allegations against him but said he "cannot combat every inaccuracy that is reported."
Lawmakers expressed outrage over the events and called for a hearing and investigation.
"I plan to use every tool to ensure all bad actors are held accountable," said Republican U.S. Sen. Steve Daines of Montana, who chairs the Senate agriculture subcommittee that oversees the Forest Service. He said he'll hold a hearing on sexual harassment in the agency.
Rep. Jackie Speier of California, a Democrat and leading voice in Congress against sex harassment, said a wide investigation was needed into the Forest Service's "toxic culture" by the inspector general of the U.S. Department of Agriculture.
After Congressional hearings on sex harassment at the Forest Service and Interior Department in 2016, senior officials repeatedly vowed to address the problem, both during the administration of former President Barack Obama and more recently under President Donald Trump.
Tooke, a native of Alabama who joined the Forest Service at the age of 18, had worked in Florida, Alabama, Georgia and Mississippi. Prior to becoming chief he served as regional forester for the southern U.S.
___
Follow Matthew Brown on Twitter at www.twitter.com/matthewbrownap .