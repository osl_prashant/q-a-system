Dakota Access LLC still owns North Dakota ranchland
Dakota Access LLC still owns North Dakota ranchland

The Associated Press

BISMARCK, N.D.




BISMARCK, N.D. (AP) — The North Dakota Attorney General's Office has extended an agreement with Dakota Access LLC, which still owns about 7,000 acres of ranchland along the Dakota Access Pipeline.
One year after most protesters left the Morton County area, the office says it will continue to review whether the company's ranch ownership complies with North Dakota's anti-corporate farming law. Corporations and limited liability companies are banned from owning or leasing farmland and ranchland, with some exceptions.

                The Bismarck Tribune reports that Dakota Access LLC purchased land in the Cannonball Ranch north of the Standing Rock Sioux Reservation when demonstrations were ongoing in 2016 to provide safety to workers. The company planned to transfer ownership after the pipeline's construction completed.
The pipeline from the Bakken to Illinois has been in service since June.
___
Information from: Bismarck Tribune, http://www.bismarcktribune.com