Chicago Mercantile Exchange live cattle futures ended higher on Tuesday for the first session in the past nine, aided by short-covering and fund-buying, traders said.
"I think that the major reason for the rally is the extreme oversold condition of the cattle futures," said Joe Ocrant, president of Oak Investment Group.
Investors said caution about this week's prices for slaughter-ready, or cash, cattle pulled futures off the morning's highs.
December live cattle finished down 0.975 cent per pound at 116.150 cents. February ended 1.425 cents higher at 119.150 cents.
A few cash cattle bids in Texas and Kansas surfaced at $114 per cwt on Tuesday, down from last week's trade in the U.S. Plains of $115 to $118.
The Fed Cattle Exchange sale of roughly 700 animals on Wednesday might offer an indication of the week's cash cattle prices overall.
For the most part, processors will need fewer animals before plants shut down during the Christmas and New Year holidays, traders and analysts said.
Generally dry conditions in the Plains created less stress on cattle in feedlots, which allows them to put on weight quicker while pumping more meat into the retail pipeline, they said.
Market bulls hope Tuesday's futures rally, improved packer margins and less cattle for sale than last week underpin cash prices in parts of the Plains.
CME feeder cattle contracts drew support from buy stops, short-covering and live cattle futures gains.
January feeder cattle closed up 1.650 cents per pound at 147.100 cents.  
Hog Futures Mostly Weaker
Lower cash prices and softer wholesale pork demand pressured CME lean hog contracts, except thinly-traded December that is set to expire on Thursday, said traders.
Sell stops and technical selling contributed to future's weakness, they said.
December hogs finished up 0.150 cent per pound at 63.750 cents. Most actively-traded February closed down 0.500 cent to 66.525 cents, and below the 100-day moving average of 66.886 cents. April ended 0.650 cent lower at 71.175 cents.
Farmers are moving hogs to market earlier than they had planned as cash prices trend lower heading into back-to-back holiday plant closures, a trader said.
Wholesale pork belly prices fell sharply on Monday and Tuesday, a sign that end-users balked at storing bellies in freezers for later use following the run up in prices in recent weeks, he said.