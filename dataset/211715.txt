Starting on Jan. 1, the gem variety avocado will face the same assessment rate as hass avocados. 
The U.S. Department of Agriculture said the variety will be assessed at the same $0.025 cents per pound rate as other varieties of hass avocados marketed or imported for the fresh market, according to a news release.
 
The Hass Avocado Board recommended the addition to the Hass Avocado Promotion, Research and Information Program, according to the release. The board provided evidence to USDA that the gem avocado variety is so like hass avocados that consumers can’t tell them apart, according to the release.
 
The pear-shaped gem variety was developed by University of California Riverside researchers and was named for the initials of researcher Grey E. Martin, who selected the variety, according to a news release from the university.