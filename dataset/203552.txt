Alsum Farms & Produce Inc. wants to help time-starved families eat healthier meals as they return to the fall school routine.
The Friesland, Wis.-based grower-shipper is marketing ready-to-eat potatoes that can be used as fast dinner sides or as healthy after-school snacks, according to a news release.
Throughout the year, Alsum markets its Fast & Fresh!-branded 12-ounce microwave-ready potatoes with olive oil and seasonings.
The company promotes the products as fresh and convenience items for busy parents and millennial shoppers, according to the release.
The line of pre-packaged steam tray creamer and fingerling potatoes are available in:
Parmesan & Garlic: red creamer potatoes with parmesan and garlic seasoning;
Gourmet Garlic & Paprika: yellow fingerlings with garlic and paprika spices; and
Fiesta: yellow and red fingerlings mixed with bold spices.
"Today's parents are juggling their kids after school activities and have less time to plan and prepare meals," Heidi Alsum-Randall, national sales and marketing manager, said in the release. "Our goal is to provide consumers with a ready-to-serve healthy potato offerings to meet the growing trends for fresh foods fast."
The packages are merchandised in select retail produce aisles alongside Alsum's line of conventional, organic and specialty potato offerings.
A graphic sleeve is made from recycled paperboard and protects product from light to prevent greening on store shelves or in shoppers' pantries, according to the release. 
The company is harvesting a new crop of russets, reds, golds and fingerling potatoes.