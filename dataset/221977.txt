It’s 25 years old now, but there are some great scenes and memorable dialogue in the 1993 movie “A Bronx Tale.”
The film is set in the New York boroughs of the 1950s and 60s, when racial tensions began infecting the city. The script was written by perennial gangster character actor Chazz Palminteri (“The Usual Suspects,” “Mulholland Falls”) and was directed by the ultimate gangster-actor Robert De Niro, who also starred in the movie.
One noteworthy scene featured Palminteri’s character Sonny, the neighborhood Mafia boss, as he instructs his young Italian protégé Calogero how to figure out if a girlfriend he’s interested in is someone he should consider seriously.
Sonny: “You give her the door test. You pull up where she lives, right? Before you get outta the car, you lock both doors. Then, get outta the car, you walk over to her. You bring her over to the car. Dig out the key, put it in the lock and open the door for her. Then you let her get in. Then you close the door. Then you walk around the back of the car and look through the rear window. If she doesn’t reach over and lift up that button so that you can get in: dump her.”
Calogero: “Just like that?”
Sonny: “Listen to me, kid. If she doesn’t reach over and lift up that button so that you can get in, that means she’s a selfish broad, and all you’re seeing is the tip of the iceberg. You dump her, and you dump her fast.”
I’ve never had the occasion to try that test on a new girlfriend, but I was immensely gratified a couple days after first seeing the movie, when my wife reached over and unlocked the driver’s door for me as we were getting ready to leave a local supermarket parking lot.
Grandma’s Rule
Salty language aside, there’s a lot of truth in that fictional scene, and now we have another barometer for evaluating someone’s character, as explained in an entertaining article in Bon Appetit magazine.
The title of the article was, “My Grandmother’s Very Specific Meat-Related Rule for Finding Love: Feminism expressed in the medium she knew best: food.”
The writer noted that her Grandma Daniels “did not believe in that dusty, sexist adage about the stomach being the expressway to a man’s affections” (betraying her East Coast connections, as nobody west of Pennsylvania uses the term expressway, instead of freeway).
But she explained that her grandma had “one unbreakable rule of courtship,” in homage to A Bronx Tale, hereafter to be known as “The Meat Test.”
Allow me to quote from the article:
“Sitting at her linoleum-top table, she laid down the law to me as I ate another piece of her seven-layer chocolate cake. She slowly peeled an orange — her answer to an apple a day — and patted her mouth daintily with a napkin.
“ ‘When you go to a man’s house to meet his family for the first time, if they don’t give you two pieces of meat, you get up and leave,’ ” she said firmly. She punctuated that with pursed lips and a nod, her gray hair bun dipping in affirmation to her own statement.”
To paraphrase the fictional Sonny, “You dump him, and you dump him fast.”
The author of the article explained that her grandmother was the “mother” of the family’s African-Methodist Episcopal church, a position that required “decades of diplomacy over potluck fundraisers. She schooled me ‘how to be Jesus’ to other people, including eating someone’s inedible food with nary a complaint.”
Hearing such a harsh admonition from such a calm, reasonable grandmother, “whom local lore said never uttered a cuss word in her 96 years on earth,” was quite surprising, she wrote.
Here’s the takeaway.
For The Meat Test, or any other “test” to have validity, it has to involve a meaningful value or principle. Sharing food is as fundamental an expression of generosity as exists, and the meat that is the centerpiece of most meals is thus a critical test of just how considerate and generous a family — and thus one of its progeny — really is when the occasion demands.
As the title of this commentary series suggest, The Meat of the Matter is meant to be a forum for discussing meaningful issues of some importance in our lives.
And meat is not only a measure of health and nutrition, but it’s now a test of character, as well.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.