There is an old saying in the retail world: The customer comes first. 
 
There are plenty of times when I have disagreed with that phrase on account of a customer's rudeness and all, but for the most part the saying rings true. 
 
You want your customer to come into any store and feel welcomed. Their concerns should be your concerns as well. 
 
I have worked in the same produce department for nearly seven years and have found that building a relationship with customers can add to the shopping experience. 
 
Not only do some people come into our produce department to buy food, they also come in to catch up with employees whom they have known for years, such as myself. 
 
The supermarket is a part of the community, and food shopping is an intimate experience in many ways. Customers come in to handpick foods they want to eat, and we, as produce clerks, are there to ensure they are satisfied with their overall experience and that the selection and quality of food on the stands is up to their expectations. 
 
It would be bad for everyone if the customer is dissatisfied with the experience and does not wish to come back.
 
About a year and a half ago my store changed owners and store names. 
 
Sure, the store is still in the same location, so people know where it is, but it was more than just location that brought people into the old store. It was comfort. And changing owners led to people wondering if it would be worth shopping at the new store. 
 
Will the prices be the same? Will the quality be as good? Will the workers be the same? 
 
This inevitably led to the concern by the new owners that the change could affect the customer base and that customers might take their business elsewhere. 
 
This is where the employees came into play. The new ownership made it a point to keep all the workers and trusted them to keep the customers, because they knew the importance of the customer relationship. 
 
There are people who have been coming in to shop every week for years, so they were concerned for their store. Many customers asked if any store employees would be let go at the switchover, and were relieved to hear would not be the case. 
 
And ultimately it was our job, as employees, to let customers know that everything was going to be run the same. 
 
I know those words could be seen as empty promises, but since many of the employees have been at the store for years, they are familiar faces and therefore trusted by those who shop there, more so than if the company decided to bring in new workers. 
 
Our words worked.
 
Once the switch was complete, customers constantly asked me how different it was and my reply was always the same. 
 
"My smock went from green to blue and that's about it." 
 
Usually it drew a laugh or a smile, but more importantly, it made customers understand that, even though the store changed owners and names, the experience would remain the same. 
 
The prices would be what they were, and the quality would not drop. Again, this instills the comfort factor of food shopping. The customer wants to know that the product is still in good hands, and I was there to convey that message. 
 
Me, Matthew. Not just a random new worker, but a familiar, friendly face.
 
Matthew Golda is a produce associate at a Foodtown store in Long Island, N.Y.
 
What's your take? Leave a comment and tell us your opinion.