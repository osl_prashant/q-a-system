Genetic editing to produce trees and fruit resistant to citrus greening is one way researchers at the University of Florida plan to use a $10.5 million in grants to fight the disease.
The grants, from the U.S. Department of Agriculture’s National Institute of Food and Agriculture, are aimed at rebounding the Florida citrus industry, whose production has plummeted by more than half since greening, aka huanglongbing (HLB), was first reported in Florida in 2005, according to a news release from the university’s Institute of Food and Agricultural Science.
The grants go to the institute’s Citrus Research and Education Center in Lake Alfred and the main university campus in Gainesville.
“As we get closer to having that situation (HLB) in hand, these grants received from the USDA are critical to completing the research underway that shows the most promise for winning the fight against HLB,”
Citrus Research and Education Center director Michael Rogers said in the release.
The grants are:

$3.6 million to Nian Wang, associate professor of microbiology and cell science, who is editing genes that make citrus susceptible to HLB.
$2.95 million to Zhonglin Mou, associate professor of microbiology and cell science, who is targeting citrus’ immune system to combat HLB.
$3.5 million to Arnold Schumann, professor of soil and water science, who is working on screens to protect trees from the Asian citrus psyllid, which spreads the disease.