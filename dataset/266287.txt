AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:15 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
TOP STORIES:
ENDANGERED WOLVES
ALBUQUERQUE, N.M. — On the edge of the Mogollon Rim in eastern Arizona, snow covered the ground and blizzard conditions were setting in as biologists prepared to open the gates to a trio of pens, releasing three packs of Mexican gray wolves that would soon have the distinction of being the first of their kind to roam the wild in decades. Thursday marks the 20th anniversary of that release and a major milestone for an effort that started decades earlier when the predators were first declared an endangered species. By Susan Montoya Bryan. SENT: 760 words, photos.
MARIJUANA-ONLINE ADVERTISING
LOS ANGELES — Weedmaps is a go-to website for people looking to find a marijuana shop. With a few clicks on a cellphone, customers can find virtually any type of cannabis product, along with the fastest route to the place selling it and ratings from other consumers to help them decide what to buy. But legal and illegal operators advertise next to each other, and licensed operators in California say that's put them at a disadvantage in a cutthroat marketplace. By Michael R. Blood. SENT: 980 words, photos.
IN BRIEF:
— XGR-COLORADO-TRANSPORTATION — Colorado's Republican-led Senate has unanimously approved a bill to ask voters in 2019 if the state can issue $3.5 billion in bonds for roads and bridges.
— COAL PRODUCTION-COLORADO — Data shows that Colorado coal production surpassed 15 million tons (14 million metric tons) last year, an improvement from 2016.
— SKIER DEATH — Authorities say a skier died after hitting a tree at a Colorado ski resort.
— 4-DAY SCHOOL WEEK — A Colorado school district will operate on a four-day week beginning next school year.
— WOMAN IN DUMPSTER — Vail police say they found a 31-year-old woman injured inside a small, neighborhood dumpster.
— PINE RIDGE HOMICIDE — A fourth man has been charged with murder in the daytime killing of a man in the parking lot of a Pine Ridge Indian Reservation youth center in 2016.
— MOTHER KILLED — A Colorado man being re-tried in his wife's murder denies any involvement in her death or their daughter's disappearance.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for May fell 3.50 cents at 4.4550 a bushel; May corn was off .50 cent at 3.7350 a bushel; May oats lost 4.25 cents at $2.2175 a bushel; while May soybeans dropped 1.50 cents at $10.18 a bushel.
SPORTS:
FLYERS-AVALANCHE
DENVER — The Philadelphia Flyers and Colorado Avalanche meet Wednesday night in a matchup of two teams trying to get into the playoffs. By Pat Graham. UPCOMING: 650 words, photos. (Game starts at 8 p.m. MT)
With: AVALANCHE-VARLAMOV SICK — Colorado Avalanche goaltender Semyon Varlamov is sick and may not start a pivotal game against the Philadelphia Flyers on Wednesday night. SENT: 130-word APNewsNow.
ROCKIES-DIAMONDBACKS OPENER
PHOENIX — The last time the Colorado Rockies played at Chase Field was one wild October night. It was the National League wild-card game, and the Arizona Diamondbacks emerged with an 11-8 victory in a game that featured 30 hits, 17 by Arizona. The Diamondbacks had four triples, two by Ketel Marte, a crucial one late by A.J. Pollock and a highly unexpected one, driving in two runs, from reliever Archie Bradley. By Bob Baum. SENT: 820 words, photos.
BRONCOS-REDSKINS TRADE
DENVER — Su'a Cravens already appears to be relishing in his change of scenery and his chance at a fresh start. In a video posted on his Twitter account , the song "A Whole New World" from the Disney movie "Aladdin" played in the background before a smiling Cravens let out a loud "Woo!" Underneath was a caption that read: "Broncos!!!! Here I come!!" By Pat Graham. SENT: 450 words, photo.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.