Dole Food Co. has launched its first app with the “Unite for a Healthy Galaxy” Star Wars-themed campaign. 
Fans who download the free app can scan Dole fruits and vegetables to unlock photo filters, character bios and recipes inspired by “Star Wars: The Last Jedi,” according to a news release.
The app is part of Dole’s multiyear nutritional collaboration with Disney, according to the release from the Westlake Village, Calif., company. The app can be downloaded online. It is available for Apple and Android devices.
The 18-week initiative features character-inspired recipes, limited-edition banana stickers and a sweepstakes for weekly prizes including toys related to the “The Last Jedi,” set for release Dec. 15. 
The company plans to include salads, berries and vegetables in the campaign as well.
App features include:

Scan banana and pineapple labels to unlock Star Wars characters and vehicle, bios, recipes and photo filters that can be collected, customized and shared;
Unlock certain characters to receive Facebook and Instagram posts, with “UnitewithDole” and “sweepstakes” hashtags that can be shared to enter sweepstakes; and 
Receive rewards by collecting all images in each of three phases of collector sets.