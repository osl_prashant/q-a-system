Fertilizer prices were mixed with anhydrous, potash and UAN32% falling along with diesel and LP. 
Our Nutrient Composite Index firmed 3.07 points to 496.08 compared to last year's 498.36. 

Nitrogen

Last week, anhydrous ammonia led gains. This week, NH3 retreats to lead declines.
Many more states posted price changes this week than did last week.
As anhydrous fell $4.50, urea firmed $4.35 per short ton.
UAN28% led gains this week, firming more than $8 per short ton.

Phosphate

Phosphate prices were higher this week with DAP and MAP, once again gaining roughly the same amount.
The DAP/MAP spread narrowed slightly to 13.37, which is slightly more narrow than our expected margin.
Wholesale DAP and MAP prices were mixed around the world, but lower at U.S. terminals.

Potash 

Potash declined this week on softness in Kansas.
Wholesale potash prices were higher at all world terminals, including the U.S.
As with phosphates, upside risk for vitamin K will increase as harvest progresses.
We are 70% filled on potash for fall applications and 50% filled for spring.

Corn Futures 

December 2018 corn futures closed Friday, October 13 at $3.98 putting expected new-crop revenue (eNCR) at $631.86 per acre -- higher $3.39 per acre on the week.
With our Nutrient Composite Index (NCI) at 496.08 this week, the eNCR/NCI spread widened 0.32 points and now stands at -135.78. This means one acre of expected new-crop revenue is priced at a $135.78 premium to our Nutrient Composite Index.





Fertilizer


10/2/17


10/9/17


Change


Current Week

Fertilizer



Anhydrous


$411.44


$421.11


+$9.66

$416.60
Anhydrous



DAP


$440.63


$442.51


+$1.88

$445.63
DAP



MAP


$454.14


$456.02


+$1.88

$459.00
MAP



Potash


$328.97


$331.14



+$2.18


$328.47
Potash



UAN28


$221.20


$220.81


-39 cents

$229.07
UAN28



UAN32


$243.04


$244.21


+$1.17

$243.50
UAN32



Urea


$327.48


$336.87


+$9.38

$341.22
Urea



Composite


488.28


493.01


+3.07

496.08
Composite