We have all heard it before. Life is a series of ups and downs and bumps in the road.Sometimes the bumps are just little potholes in the pavement. Sometimes the bumps are the ones that take you sideways off into the ditch, on a deserted road, in the middle of the night, when it's raining and no one is around.
I'm sure we are all nodding our head "yes" at this one. But sometimes while you are sitting in that ditch, soaking wet and wondering what to do, a light appears off in the distance.

And lo and behold, through the raindrops falling on your face, you see the headlights pop up over the hill and head straight for you.
It just so happens to be a tow truck driving late at night and it is here to pull you out. Life has a funny way of working like that sometimes, and we are so blessed that it does.
These girls are that tow truck.
In this journey of sharing the "good word" of the farm, they have become my "pig posse."
We are all on the front lines together, trying to connect the 98% of people who yearn to know about farming with the 2% who actually farm.
It's an overwhelming task and we manage it with style and grace (or at least we would like to think so.) They are on the other end of the phone line constantly, sometimes talking about the farm but mostly just talking about life.

The emails shift back and forth between us, proofreading, critiquing, praising. The group texts are the highlight of my day, with funny memes, random motherhood fails, and of course, talks about pigs.
The snapchats are epic and never cease to put a smile on my face and brighten my day just when I need it.
Then out of nowhere, it hits me as I happily take a day away from home on my birthday to go and speak with one of my best friends in the business. We went to talk to students on a subject we both love: farming. During the day together, we laugh, sing karaoke in the car, and speak together (almost to an empty room).
At the end of the talk, the pig posse surprises me and whips out a birthday cake only to force everyone at the conference to reluctantly sing Happy Birthday.
The girls hand me a pig trophy for the Reserve Champion at the 1976 Jasper County fair. It is gracefully inscribed, "Happy Birthday Erin!" with a black Sharpie. It was the best gift anyone could ask for.
As I drove home by myself that night from the fun-filled day, my heart swelled. I had never thought of myself as the kind of person that people would think of so much on her essentially un-special 34th birthday. How extraordinary they are, how perfect that was, how blessed I am.
If I have one word of advice for anyone in the pig farming world, it is to find YOUR pig posse. Talk, help, and grow with one another and you won't regret it.
These girls came from out of nowhere and threw me their tow hook.
Well girls, I caught it.
And I am so thankful for your smiles, generosity, and impeccable timing to waltz into my life and make a difference. You are beautiful and I am honored to have you as part of my farm story. Thank you.