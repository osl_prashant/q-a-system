AP-NC--North Carolina News Digest, NC
AP-NC--North Carolina News Digest, NC

The Associated Press



Hello! The interim Carolinas News Editor is Jeffrey Collins. The supervisor is Emery Dalesio.
A reminder that this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with updates.
TOP STORIES:
EXCHANGE-BUSINESS LESSON
HICKORY — The bottom line for any business is making a profit, but Lenoir-Rhyne University students in Ralph Griffith's entrepreneurship capstone class this semester are learning profit can be defined any many different ways. His students have reached out to Unique World Gifts in Hickory to work as consultants to find ways they might be able to help the nonprofit grow. By John Bailey, The Hickory Daily Record. SENT: 800 words.
IN BRIEF:
— DUKE ENERGY-CEO PAY, from CHARLOTTE — Duke Energy says its top executive's compensation has more than doubled over the past two years. SENT: 130 words.
— CANDIDATE WEBSITE, from RALEIGH — A North Carolina congressional candidate says the website she used when she campaigned for another office is now owned by a Russian. SENT: 130 words.
SPORTS:
BKC--T25-A10-DAVIDSON-RHODE ISLAND
WASHINGTON — Bubble teams all across the land were keeping an eye on the Atlantic 10 Conference Tournament final and Davidson, a school still waiting for its first victory in the NCAA Tournament since a guy by the name of Stephen Curry was leading the way. Freshman Kellan Grady's go-ahead baseline floater with 74 seconds left ended a nearly 13-minute drought without a field goal for Davidson, and lifted Curry's alma mater to a 58-57 victory over 25th-ranked Rhode Island for the A-10 title. By Howard Fendrich. SENT: 680 words, photos.
___
If you have stories of regional or statewide interest, please email them to apraleigh@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, (statephotos@ap.org) or call 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
The AP, Raleigh