Grand Champion was awarded to Palmyra Berkely P Ruth-ET, owned by Evan Creek of Hagerstown, Md. on October 3, in the International Ayrshire Show at World Dairy Expo. Ruth earned first place in the Senior Three-Year-Old Cow class before being named Intermediate Champion and Grand Champion Female being merited with the $1,000 Udder Comfort Grand Champion Cash Award. The 2016 Reserve Grand Champion Female, Bear Ayr Burdette Ray, owned by Peter Vail and Mike and Linda Hellenbrand, Cross Plains, Wis., reclaimed her title as Reserve Grand Champion Ayrshire. Premier Breeder of the Ayrshire show went to Family-Af-Ayr Farm, Caledonia, Ill. and Premier Exhibitor went to Glamourview – Iager and Walton, Walkersville, Md. 
267 animals were presented to official judge Allyn “Spud” Paulson, Arlington, Wis. and associate judge Kurt Wolf, Epworth, Iowa.

Overall show results and top class placings are as follows:
Spring Heifer Calf:

Old-Bankston JC Banner, Glamourview-Iager & Walton, Walkersville, Md.
Nor-Bert Reinholts Anique, D, N, R, T, J & T Reinholt and D, D, & B Freeman, Bremen, Ind. 

Winter Heifer Calf:

Old-Bankston Brdt Jiselle-ET, Stephen McDonald, Princeton, Ill.
Bavaroise Dreamer Adele-ET, Amelie Hardy Demers, Sainte-Cecile-De-Levrard, Qué. 

Fall Heifer Calf:

Lazy M Dream Mckenzie-ET, Michael Maier, Stitzer, Wis.
Old-N-Lazy Gentleman Whammy-ET, Riley Bohrer, Walkersville, Md. 

Summer Yearling Heifer:

Grand-View Dream Daquiri-ET, Stephen McDonald, Princeton, Ill.
Yellow Briar Pick Me Up, Ridale Genetics and Paragon Acres, Worthington, Mass. 

Spring Yearling Heifer:

Old-N-Lazy Gentle Wipeout-ET, P Vail, M & L Hellenbrand and F & D Borba, Cross Plains, Wis.
Marilie Reality Mellini-ET, Brady Mcconnell, Whitewater, Wis. 

Winter Yearling Heifer:

Edgebrook Remington Pistol-ET, Leslie & Linda Bruchey, Westminster, Md.
Stone Springs Rolling The Dice, Duncan Bailey and Jeff Atherton, Fillmore, N.Y. 

Fall Yearling Heifer:

Glenmar-Dale Burdette Booyah, Mark & Becky Brown, Fennimore, Wis.
Mill Valley Burdette’s Faith, Trevor, Lane & Blake Greiwe, Quincy, Ohio 

Junior Champion Female:

Old-N-Lazy Gentle Wipeout-ET, P Vail, M & L Hellenbrand and F & D Borba, Cross Plains, Wis. 

Reserve Junior Champion Female:

Marilie Reality Mellini-ET,  Brady Mcconnell, Whitewater, Wis. 

Junior Best Three Females:

Lazy M Farm LLC- Michael and Herman Maier, Stitzer, Wis.
Stillmore Cattle Company, Steve Searles, Pine Island, Minn. 

Premier Breeder Heifer of Heifer Show:

Kurt Wolf and Michael Maier, Epworth, Iowa
Mike and Julie Hemp, Beaverville, Ill. 

Premier Sire of the Heifer Show:

Palmyra Tri-Star Burdette-ET
Nexus Dreamer

Yearling Heifer in Milk:

Haynes Farm Brachiosaurus, Molly Doty, Fort Edward, N.Y.
Palmyra Dreamer R Brenna-ET, Mark Creek, Hagerstown, Md. 

Junior Two-Year-Old Cow:

De La Plaine Vicking Lady, Glamourview-lager & Walton, Walkersville, Md.
Old-N-Lazy Dream Of A Diva, Rosedale Genetics and Mike Maier, Oxford, Wis. 

Senior Two-Year-Old Cow:

Blue-Spruce Nemo 12821-ET, Blue-Spruce Farm, Bridport, Vt.
Yellow Briar Petula 2, Jessica Peter and Garrett Schmidt, Melrose, Wis. 

Junior Three-Year-Old Cow:

Ridale Nemo Serenity-ET, Ryan & Marjorie Rida; Ridale Genetics, Worthington, Mass.
Onword Lazy M Distinct Arrow, Mackenzie Ullmer, Tyler Boyer and Monica Schwittay, Seymour, Wis. 

Senior Three-Year-Old Cow:

Palmyra Berkely P Ruth-ET, Evan Creek, Hagerstown, Md.
De La Plaine Bingo Stringer, Blue-Spruce Farm, Bridport, Vt. 

Intermediate Champion Female:

Palmyra Berkely P Ruth-ET, Evan Creek, Hagerstown, Md. 

Reserve Intermediate Champion Female:

De La Plaine Vicking Lady, Glamourview-Iager & Walton, Walkersville, Md. 

Four-Year-Old Cow:

Marilie Gentelman Karmina, Budjon Farms, P Vail, M & L Hellenbrand, Lomira, Wis.
Mowry’s Popo Mistletoe, Glamourview-Iager & Walton, Walkersville, Md. 

Five-Year-Old Cow:

Bear Ayr Burdette Ray, Peter Vail and Mike & Linda Hellenbrand, Cross Plains, Wis.
Cedarcut Burdette Clove Colata, Erin Curtis Szalach, Cazenovia, N.Y. 

Six-Year-Old & Over Cow:

Sunny Acres Riggins Ramona, Kathryn Evans, Georgetown, N.Y.
Mowry’s Burdette Summer, Jason Mowry, Garrett, Pa. 

100,000 Lb. Production Cow:

Right-Angle T Harley, Yarrabee Cows and Ski-Pal Ayrshire, Brooklyn, Iowa
Moy-Ayr Poker Lavender-ET, Steve, Pauline, Rebecca & Emily Schmidt, Delavan, Wis. 

Total Performance Winner:

Mowry’s Burdette Summer, Jason Mowry, Garrett, Pa. 

Senior Champion Female:

Bear Ayr Burdette Ray, Peter Vail and Mike & Linda Hellenbrand, Cross Plains, Wis. 

Reserve Senior Champion Female:

Cedarcut Burdette Clove Colata, Erin Curtis Szalach, Cazenovia, N.Y. 

Grand Champion Female:

Palmyra Berkely P Ruth-ET, Evan Creek, Hagerstown, Md. 

Reserve Grand Champion Female:

Bear Ayr Burdette Ray, Peter Vail and Mike & Linda Hellenbrand, Cross Plains, Wis. 

Nasco International Type and Production Award:

Mowry’s Burdette Summer, Jason Mowry, Garrett, Pa. 

Premier Breeder:

Family-Af-Ayr Farm, Caledonia, Ill.

Premier Exhibitor:

Glamourview- Iager and Walton, Walkersville, Md. 

Premier Sire:

Palmyra Tri-Star Burdette-ET
Nexus Dreamer 

For over five decades, the global dairy industry has been meeting in Madison, Wis. for World Dairy Expo. Crowds of nearly 75,000 people from more than 100 countries attended the annual event in 2016. WDE will return Oct. 3-7, 2017 as attendees and exhibitors are encouraged to “Discover New Dairy Worlds.” Visit worlddairyexpo.com or follow us on Facebook and Twitter (@WDExpo or #WDE2017) for more information.