New Jersey fruit and vegetable growers and shippers appear to be headed toward a bountiful year, growers and agriculture officials there said.The state skirted damage from March freezes that hit blueberry and peach crops in Georgia and the Carolinas, officials said.
“So far, I think everything has lined up as well as it could have,” Tom Beaver, director of marketing and development with the Trenton-based New Jersey Department of Agriculture, said in late May.
Weather conditions have been acceptable, if not ideal, Beaver said.
New Jersey is the fourth-largest peach-growing state in the U.S., and the crop appeared to be progressing in a timely fashion, Beaver said.
“We had some peach buds earlier than normal, but because we were going from hot to cold, they didn’t go into full flower, and when we went from hot to cold, they kind of tightened up, so, by and large, we’re expecting a full crop,” he said.
In 2016, an April freeze left some damage on the peach crop, he said.
“This year, the blossoms have been abundant,” said Pegi Adam, marketing and promotion consultant with the Glassboro-based New Jersey Peach Promotion Council.
A bumper crop is possible for 2017, she said.
“The weather has been very cooperative,” she said.
Most growers in southern New Jersey had begun to thin off their heavy peach crop by mid-May, said Jerry Frecon, technical and horticultural consultant to the council.
“A few growers were brave enough to even thin blossoms with mechanical and string thinners — brave, because there is always a high probability of low temperature injury during bloom, so thinning at this time can be very risky,” he said in a news release.
The council estimates New Jersey peach/nectarine acreage at about 5,500, for an anticipated volume of 55 million to 60 million pounds this year.
Yellow freestones should be ready to ship by June 20, weather permitting, and a full crop should be flowing by July 10, Adam said.
Peach harvest should continue through August and into September, she said.
Beaver said New Jersey’s blueberry crop looked good, as well, with an anticipated start in the first or second week of June.
“There was a lot of cautious optimism going into this season, so we’re certainly looking forward to having that product come to market,” he said.
For Glassboro-based Sunny Valley International Inc., the growing season has been relatively problem-free, said Bob Von Rohr, marketing and customer relations manager.
“As of right now, we’re looking for a full crop,” he said.
Sunny Valley was looking to start its peach shipments by about the first week of July, Von Rohr said.
“Everything is looking outstanding,” he said. “Winter was perfect with chill and rainfall. Spring was perfect.”
New Jersey blueberry acreage is estimated at 9,100, with volume this year forecast to be perhaps 50 million pounds, said Tim Wetherbee, sales manager with Hammonton, N.J.-based Diamond Blueberry Inc. and chairman of the Hammonton-based New Jersey Blueberry Council.
He said last year’s volume was around 30 million pounds.
“Things look very good right now — I wouldn’t say it is a bumper crop, but weather has been cooperating considerably,” he said.
A stronger-than-usual market awaits the berries, Wetherbee said.
“I think (the market will be) stronger than the last couple of years because those people aren’t going to have as much product out there as they had in the past,” he said, alluding to freeze-related losses in Georgia and the Carolinas.
Sunny Valley was scheduled to start its blueberry shipments around June 8-12, Von Rohr said.
“We were very fortunate this year,” he said. “I feel bad for the other states that had the misfortunes of Mother Nature.”
Some of New Jersey’s vegetable deal already is underway, said Bill Nardelli Jr., vice president of sales for Cedarville, N.J.-based grower-shipper Nardelli Bros. Inc., which grows more than 80 commodities.
“We had some cool weather early, which slowed us down a little getting started,” he said. “Right now, we’re in full swing on lettuce, as well as leafy greens, cilantros, parsley and herbs.”
“The weather has a way of evening things out,” he said.
Cedarville, N.J.-based Eastern Fresh Growers started cutting asparagus in early April and got into its romaine and iceberg lettuce harvests the week of May 22, said Tom Sheppard, president.
“Peppers and cucumbers are in the ground. Asparagus has been good. Prices are good. Volume is good,” he said.