AgLaunch365 has accepted three new agtech startups into Phase I of its farm-centric innovation platform. AgLaunch365 provides startups with the tools needed to integrate with farmers to create and implement sophisticated field trial protocols and prepare for funding by aligned capital sources. 
 
The phase I teams are:

DryMAX, based in Minnesota, is commercializing a low-energy, low-heat radio wave process for drying grain that keeps grain integrity and maximizes nutrition delivery. 


EarthSense, based in Illinois, has developed TerraSentia, an ultra-compact, autonomous, easy-to-use robot with multiple sensors and embedded data collection and analytics software for plant phenotyping. They are working with the seed industry and crop R&D partners to test and optimize TerraSentia.  


Rabbit Tractors, based in Indiana, is developing an autonomous, mobile, multi-purposed, swarm-capable and high-ground clearance farm-production tractor for all pre-harvest activities. 

Phase I teams will receive intensive business development support and customer discovery assistance. Teams will have access to leading accelerator programming, the AgLaunch Farmer Network, the Cultivator Mentor Network, among a range of other services. 
 
This 6-week program will be delivered through remote programming and in-person sessions held in Memphis. It will culminate at the Farm Journal AgTech Expo in Indianapolis December 11-13, 2017 (https://www.farmjournalagtechexpo.com/). 
“We are excited to support these three innovative teams who are committed to solving problems for the agriculture industry,” says Pete Nelson, President and Executive Director of AgLaunch. “Following the Phase I business development and customer discovery, these entrepreneurs will be better prepared to launch their products in the field with our extensive group of farmers during 2018.”
AgLaunch365 consists of three phases of support that closely align with a typical growing season. In Phase I, teams develop their business models and gain access to the Cultivator Mentor Network from October through December. Phase II participants begin in January and refine their product or technology to be “field ready” for on-farm trials with AgLaunch365’s Farmer Network beginning in the spring. Phase III teams work directly with growers in the region to test their innovation in the field during the growing seasons. 
AgLaunch365 teams are eligible for investment from a key AgLaunch partner, Innova and its USDA-certified Rural Business Investment Company fund, a $31 million fund backed by eight Farm Credit banks.  
AgLaunch is a joint initiative of Memphis Bioworks Foundation and Tennessee Department of Agriculture. Innova was founded by Memphis Bioworks Foundation. 
AgLaunch365 focuses on helping entrepreneurs find success by providing agriculture expertise, incubator resources, and a strong global ag specific network. Paired with the natural resources of Tennessee and the surrounding Mid-South region, AgLaunch365 has the power to help launch startups into successful businesses.
“Tennessee and the surrounding Mid-South region is becoming known as a leader for field testing and trialing new technologies and innovations in agriculture,” says, Commissioner Jai Templeton, Tennessee Department of Agriculture. “We are pleased to be able to facilitate that process to grow the overall agtech sector.”
Applications to participate in Phase II can be found here: http://aglaunch.com/aglaunch-365/.  Deadline for Phase II is November 15, 2017.