The U.S. Department of Agriculture has sanctioned four produce companies that did not pay Perishable Agricultural Commodities Act reparation awards.
The following businesses and individuals are currently restricted from operating in the produce industry, according to a news release:

Safe Produce Inc., Nogales, Ariz., for failing to pay a $7,760 award in favor of a California seller. Rebecca Favela and Scott Favela were listed as the officers, directors and/or major stockholders of the business.
Manuel Gomez, doing business as Steele Canyon Produce, Chula Vista, Calif., for failing to pay a $49,809 award in favor of a California seller. Manuel Gomez was listed as the sole proprietor of the business.
Victor Guerrero, doing business as Camila Fresh, McAllen, Texas, for failing to pay a $23,006 award in favor of a Texas seller. Victor Guerrero was listed as the officer, director and major stockholder of the business.
Maria Soto, doing business as Mary’s Produce, McAllen, Texas, for failing to pay a $1,290 award in favor of an Illinois seller. Maria Soto was listed as the sole proprietor of the business.