The booming market for fresh herbs and greens is keeping Mont-Royal, Quebec-based Fruit Dome Inc. busy, said Gaetan Thibault, sales.
“Sales were up 10% to 15% last year,” said Thibault, who works with a Quebec grower in summer and Mexican farms in winter for fresh herbs and greens such as kale, chard and dandelion.
Last year, Fruit Dome moved into a new 45,000-square-foot facility with three packing lines located 10 minutes from Pierre Elliott Trudeau airport. 
With 10 trucks on the road, the company has also become a broker for Mexican winter staples such as carrots, broccoli and avocados.
Demand is also up for its cooked, vacuum-packed beets from France, in conventional and organic.