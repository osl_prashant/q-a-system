Citrus grower-shippers in Florida continue to evaluate their losses in the wake of Hurricane Irma.There will be fruit available for the market, although certainly not as much as originally planned.
Doug Bournique, executive director of the Fort Pierce, Fla.-based Indian River Citrus League, noted that the storm hit just as growers were expecting a larger collective crop for the first time since citrus greening befell the industry more than a decade ago.
“This storm couldn’t have come at a worse time for the peninsula of Florida,” Bournique said. “It’s a tough hit.”
Even so, he said, growers have dealt with these types of disasters before and will get through as they always do.
“We’re going to be able to supply our markets, just not as much as we would have,” Bournique said.
He and others in the industry mostly refrained from giving loss estimates early the week of Sept. 18 because they were still going through groves. Also, they expected a second fruit drop, the result of tree limbs and fruit stems being twisted in the storm.
Another variable is the health of trees that have been under water in flooded groves.
“Many of our groves were inundated with rain and have only dried out in recent days,” said Matt Kastensmidt, domestic sales manager for Vero Beach, Fla.-based IMG Citrus. “We should have a better idea within about a week.”
Growers in the Gulf region took the hardest hit as far as water in groves because power outages there made it difficult or impossible to pump out water, said Dave Brocksmith, Florida citrus category manager for Vero Beach-based Seald Sweet.
The Indian River region also had flooding but had fewer power outages.
Bournique said trees can survive longer if growers can get the water moving, so those have a chance even if they are still flooded.
“If the water’s stagnant, you’re dead” after 80-100 hours pass because the oxygen gets used up, Bournique said.
He said damage has varied from grove to grove. Some with wind breaks still had plenty of fruit on the trees, while other locations are still under water and inaccessible.
Steven Clark, vice president of corporate communications for Los Angeles-based The Wonderful Co., said the company does not expect its Fort Pierce-based DNE operations to be disrupted. The storm has meaningfully affected volume, however.
“There has been significant damage to our grower partners’ crops, which will reduce available supplies from Florida this season,” Clark said. “These losses may reach as high as 50% of the citrus crop.”
Lisa Lochridge, director of public affairs for the Florida Fruit & Vegetable Association, said she had heard loss estimates around the industry of 50-70%. Hurricane Irma cut such a wide swath that all growers were affected, she said.
 
Harvest plans
Before the hurricane, Seald Sweet had planned to begin harvesting early-season oranges, tangerines and grapefruit the week of Sept. 18 or the week of Sept. 25, but that has been postponed between two to four weeks in the wake of Irma.
Packing will probably not start until Oct. 16, Brocksmith said.
Al Finch, president of Dundee-based Florida Classic Growers, said the company is going block by block through its groves to assess the damage but is looking forward to soon beginning the season.
Florida Classic Growers expects to begin shipping small volumes of Fall Glo tangerines and navel oranges the week of Sept. 25, and the company plans to have juice oranges available the week of Oct. 9.
“We’re very optimistic that it’s still going to be a good season,” Finch said.
 
Other crops
Most vegetable and strawberry growers in Florida did not have much product in the ground when the hurricane struck, Lochridge said.
However, those producers were set back on their field preparation. The high winds whipped up the plastic on the ground and in some cases brought irrigation systems with it, so getting fields back to where they were before Irma will be expensive.
Labor is a concern, Lochridge said, especially because the supply was tight already.
In Pasco County, more than 200 volunteers replanted 24 acres of blueberry bushes for Frogmore Fresh Farm after its brush with Irma.
People came from churches, middle and high schools, 4-H and elsewhere in an effort organized by the University of Florida Institute of Food and Agricultural Sciences, according to a news release.
“We were really surprised by the outpouring of love and commitment from the community,” Leonard Park, general manager for Frogmore Fresh Farm, said in the release. “When the going got tough, people who are still without power, students and faculty who live in Gainesville, kids and families showed up to help.
“We can’t thank UF/IFAS Extension enough for pulling this together,” Park said. “We are truly touched.”
 
Federal assistance
Guidance on evaluating the safety of crops exposed to flooding is available from the Food and Drug Administration.
“Crops may be submerged in flood water, exposed to contaminants, or susceptible to mold,” FDA commissioner Scott Gottlieb said in a news release. “Some of the major concerns for crop safety are heavy metals, chemical, bacterial and mold contamination. In many cases, it is challenging to determine what contaminants are in crops that were submerged by floodwaters.
“Both human and animal food must meet well-established safety requirements,” Gottlieb said. “FDA has experts that are working closely with state regulators and directly with producers to address questions and concerns.”
Agriculture Secretary Sonny Perdue took an aerial tour of hurricane damage in Florida on Sept. 18 and met with affected producers at Clewiston, Fla.-based Southern Garden Citrus, a major juicing operation.
Bournique said he has been encouraged by the outreach from Florida Gov. Rick Scott and from Congressman Tom Rooney, who has advocated for emergency funding for Florida agriculture.
“I’m just proud of some of the leadership that’s going on in the aftermath,” Bournique said.