Click here for related charts.
CORN COMMENTS
The national average corn basis widened 2 cents from last week to 16 3/4 cents below September futures. The national average cash corn price declined 14 1/2 cents to $3.12 3/4.
Basis is weaker than the three-year average, which 5 cents below futures for this week.
 
SOYBEAN COMMENTS
The national average soybean basis softened 3/4 cent from last week to stand 10 3/4 cents below November futures. The national average cash price declined 12 3/4 cents from last week to $9.12 1/4.
Basis is well below the three-year average, which is 91 cents above futures for this week.
 
WHEAT COMMENTS
The national average soft red winter (SRW) wheat basis improved 3 1/2 from last week to 1 1/4 cent below September futures. The average cash price firmed 3 3/4 cents from last week to $4.02 1/4. The national average hard red winter (HRW) wheat basis firmed 1/4 cent from last week to 70 1/4 cents below September futures. The average cash price improved 3 cents from last week to $3.30 1/4.
SRW basis is much firmer than the three-year average of 31 cents under futures, while HRW basis is much weaker than the three-year average, which is 55 cents under futures for this week.
 
CATTLE COMMENTS
Cash cattle trade averaged $106.78 last week, down $2.90 from the previous week. This week, cash trade is expected to be $1 to $2 lower. Last year at this time, the cash price was $114.60.
Choice boxed beef prices dropped 61 cents from last week at $191.72. Last year at this time, Choice boxed beef prices were $196.49.
 
HOG COMMENTS
The average lean hog carcass bid softened $5.90 over the past week to $74.59. Last year's lean carcass price on this date was $64.51.
The pork cutout value declined $3.24 from last week to $84.23.  Last year at this time, the average cutout price stood at $76.61.