BC-NC--North Carolina News Digest, NC
BC-NC--North Carolina News Digest, NC

The Associated Press



Hello! The interim Carolinas News Editor is Jeffrey Collins. The supervisor is Skip Foreman.
A reminder that this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with updates.
TOP STORIES:
LGBT RIGHTS-NORTH CAROLINA
RALEIGH — North Carolina is again an attractive location for big out-of-state corporations. The state no longer sits squarely in the crosshairs of culture wars over transgender rights. College basketball tournament games are back, after a one-year hiatus. But the saga of the state's so-called bathroom bill hasn't been forgotten, and many Republican incumbents who passed House Bill 2 in 2016 are being targeted this election year, with the flap over the law still an issue. By Gary D. Robertson. SENT: 880 words, AP Photos NCCB202, NCCB203, NCRAL201, NCRAL202.
DEPUTY-FATAL SHOOTING
RALEIGH — A North Carolina sheriff's office can't prove a compelling reason to keep evidence secret while it fights a lawsuit alleging a pattern of excessive force that culminated in a deputy fatally shooting a man at his home, a group of media outlets argued. A lawyer for the news outlets filed a motion to intervene in the case late Thursday, asking the federal judge to unseal deposition transcripts and other evidence. The group includes The News & Observer, The Fayetteville Observer, WRAL-TV, WTVD-TV and The Associated Press. By Jonathan Drew. SENT: 390 words.
ATLANTA-CYBERATTACK
ATLANTA — Atlanta police officers had to write reports by hand. Residents can't pay water bills online. Municipal court dates are being reset. All are fallout from a ransomware attack last week that hobbled the city's invisible infrastructure. Another ransomware attack hit Baltimore's 911 dispatch system over the weekend, prompting a roughly 17-hour shutdown of automated emergency dispatching. The Colorado Department of Transportation suffered two attacks just over a month ago. And the North Carolina county that's home to Charlotte totally rebuilt its system after a December attack. By Kate Brumback. SENT: 890 words.
EXCHANGE-ASHEVILLE BARBECUE PIONEER
ASHEVILLE — The scene at Little Pigs BBQ last Friday was about the same as it ever was: hushpuppies dancing in hot peanut oil; barbecue sandwiches flying over the counter; platters of the restaurant's famous broasted chicken getting devoured with crisp sides of slaw. The lines were long, but they moved quickly. If, like some of the construction workers laboring over fat plates of ribs, you didn't look up from your meal, you might not notice the friendly-but-sad eyes behind the register. You might not discern that some diners ate quietly. By MacKensy Lunsford, Asheville Citizen-Times. SENT: 800 words.
IN BRIEF:
— MURDER CASE-SUSPECTS DEAD, from CHARLOTTE — Police in North Carolina say the suspects wanted in a 2016 murder are both dead. SENT: 110 words.
— MISSING WOMAN-SUSPECT SOUGHT, from FAYETTEVILLE — Police have identified a suspect wanted in the death of a North Carolina woman. SENT: 110 words.
— BANK FRAUD CONVICTION, from RALEIGH — Prosecutors say a North Carolina man must forfeit $1.3 million and $300,000 in gold bullion following his conviction on bank fraud and other charges. SENT: 130 words.
— MURDER TRIAL-LIFE SENTENCE, from GOLDSBORO — The attorney for a North Carolina found guilty of murder says he is appealing the verdict. SENT: 120 words.
— WITNESSES KILLED, from CHARLOTTE — A federal judge has sentenced a gang member in North Carolina to 13 years in prison for her part in the murders of a businessman and his wife. SENT: 120 words.
— SHOT TWICE, from EDEN — Authorities say a North Carolina man has been shot twice in three days. SENT: 130 words.
— DINOSAUR EGGS, from RALEIGH — A North Carolina museum is displaying two eggs found in Utah from a dinosaur that one expert calls "a 15-foot (4.5 meter)-tall chicken." SENT: 130 words.
— HATTERAS-OCRACOKE FERRY, from HATTERAS — Ferry runs between Hatteras and Ocracoke Island are being adjusted to help with an emergency dredging effort. SENT: 130 words.
SPORTS:
HKN--HURRICANES-CAPITALS
WASHINGTON — The Washington Capitals host the Carolina Hurricanes on Friday night UPCOMING: 600 words, AP photos. Game starts at 7 p.m.
___
If you have stories of regional or statewide interest, please email them to apraleigh@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, (statephotos@ap.org) or call 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
The AP, Raleigh