Hronis Inc. has hired Jennifer LoBue to market citrus and table grapes. 
“I’m excited to join the Hronis team,” LoBue said in a news release.
 
The Hronis “emphasis in family values, innovative marketing and sustainable farming practices attracted me instantly,” she said in the release. “I am excited to begin this next chapter of my career.”
 
LoBue spent her junior high and high school breaks working in family orange groves in the Lindsay/Exeter area.
 
In 2002, she earned her bachelor’s in agricultural business with a minor in business administration from California State University, Chico.  
 
From 2002-04 she worked in agricultural export sales in the Bay Area and on the East Coast, joining LoBue Citrus to handle domestic sales in 2005, the release said. 
 
“Her experience in both citrus and grapes goes unmatched, and her reputation speaks for itself,” Pete Hronis, senior vice president of sales and marketing for the Delano, Calif.-based grower-shipper, said in the release.