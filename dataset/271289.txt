The food and farming industry is worth nearly a trillion dollars to the U.S. economy in 2015, according to data collected from USDA’s Economic Research Service (ERS). That’s 5.5% of the U.S. gross domestic product.
In total, ag and ag-related industries contributed $992 billion to the GDP. Output from U.S. farms specifically contributed $136.7 billion, or about 1% of the U.S. GDP.

ERS counts other sectors that rely on agricultural inputs such as food and materials for textile production. Therefore, contributions from the food service and food/beverage manufacturing industries are also calculated toward the total amount.
Farming’s contribution to GDP has retreated two consecutive years after reaching an all-time high of $189.9 billion in 213.
ERS updated this chart in March 2017. Track this and related data at Ag and Food Statistics: Charting the Essentials.c