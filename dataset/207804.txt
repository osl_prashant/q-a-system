An ample Vidalia onion crop is expected to keep customers supplied through Labor Day.“We kind of had, I reckon, as good weather as we’ve ever had for harvesting,” said Bob Stafford, interim executive director of the Vidalia, Ga.-based Vidalia Onion Committee.
The crop is expected to rival last year’s total volume of 6.2 million 40-pound units of Vidalia onions, from the district’s nearly 12,000 acres, Stafford said.
“Our quality was excellent, and the sizing was very good,” said Stafford, longtime director of the Vidalia Onion Business Council. He is overseeing the Vidalia Onion Committee until a new executive director is hired, following the retirement of previous director Susan Waters in late spring.
The most common size is in the jumbo range, 3 inches and up, Stafford said, noting there is a “small percentage” of mediums.
As of July 26, there were still about 750,000 40-pound units in storage, which should satisfy customers’ needs at least until Labor Day, Stafford said.
“You’ve got to have some onions on Labor Day, and we have several growers who store them and they hold back on shipments up until Labor Day,” he said.
Vidalia growers dealt with few negative weather issues this year, while blueberry growers just to the south of the Vidalia district dealt with killing freezes in mid-March.
“We had that freeze come in, too, but the cold has to be in the teens for several hours to do any damage (to onions),” Stafford said. “That cold kind of nipped a few leaves but it did not do any damage.”
Troy Bland, chief operating officer of Glennville, Ga.-based Bland Farms, said the company expects to market strong quality Vidalia onions out of storage through late August or early September, with a seamless transition expected to Peruvian supply in September. Supplies of Peruvian onions will continue into early next year, he said.
“We’re very excited about what we have in storage and at the same time looking forward to a great Peruvian program.”
Bo Herndon, owner of Lyons, Ga.-based L.G. Herndon Jr. Farms, said he should have enough onions to reach the Labor Day market.
“We’ve got good supplies, and the onions are good,” he said. “We had plenty of colossal and plenty of jumbos.”
Weather posed no issues this year, Herndon said.
“Everything as a whole has been real good,” he said. “We get ‘em in storage and under cover and really don’t have to worry about anything.”
Herndon described the market conditions as “decent”.
“It needs room for improvement, always, but been it’s been decent overall,” he said. “We’d like to see it higher overall, but it is what it is. There’s a lot of onions around.”
Not everybody will have supplies on Labor Day, however.
“We’re gonna wind up no later than (Aug. 4),” said Keith Smith, field manager with Smith Farms Inc. in Cobbtown, Ga.
That, despite a harvest that was 15-20% better this year than last, he said.
“It was a very good crop,” he said. “The warm winter led to a pretty large crop and we had pretty large yields on everything.”
Lyons, Ga.-based M&T Farms expects to be out of onions somewhere around Aug. 9, said owner Terry Collins said.
“Quality was good this year,” he said. “Yields were great.”
Prices were not adequate throughout the season, however, Collins said.
“It started down and stayed down,” he said.
As of July 25, 40-pound cartons of yellow granex sweet onions from Vidalia were $15-19 for jumbos. A year earlier, the same product was $17-20.