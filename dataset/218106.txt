ORLANDO, Fla. — Drawing 165 exhibitors and 1,700 attendees, the 2018 Potato Expo featured discussions of hot-button issues of trade, trucks, immigration and technology.
Blair Richardson, CEO of Potatoes USA, said the Potato Expo gives the industry a sense of communal identity to discuss issues facing the industry in different sectors and different parts of the country.
“At the end of the day, we are all focused on one thing, to build opportunities for potatoes and potato farmers,” Richardson said.
The lack of trucks has been a big topic among attendees, said John Keeling, CEO and executive vice president of the National Potato Council.
“We know the electronic logging device mandate has a lot to do with it, but I think there are a lot of layers to the problem,” Keeling said Jan. 11. Most of the trucks potato shippers use are independent truckers and Keeling said an improving economy may translate to less people interested in the trucking business.
“It’s a real shortage and causing real problems, and we are going to have to see what we can do to get to the bottom of it,” Keeling said.
Potato Expo attendees also are anxious about trade and the North American Free Trade Agreement, Keeling said. President Trump pulling out of NAFTA would be catastrophic for the potato industry, Keeling said.
“We are losing ground not just in Mexico if NAFTA doesn’t come together, we are losing ground in Japan and Indonesia and other places — those countries may be less likely to negotiate with us because the process is a little uncertain at this point.”
Keeling said immigration reform could be a fast developing issue if the White House and Congress include guest worker reform for agriculture with a package that addresses the Deferred Action for Childhood Arrivals program and border security.
“There is a need to fix the Dreamers issue and that creates an opportunity to do that as well as to link up to other issues that need to be fixed, such as the need to have an agriculture workforce and border security,” Keeling said.
 
Expo floor buzz
Celebrating 10 years, the Potato Expo was a success on the trade show floor as well, exhibitors said.
“Orlando still draws so many people, it just feels like a good vibe at the show,” said Eric Chastaine, senior field marketing manager for the Western territory for Rabo AgriFinance, Spokane, Wash. “There has been a lot of traffic and people have been in a good mood down here.”
Doug Cole, senior manager of marketing and biotech affairs for Simplot Plant Sciences, Boise, Idaho, said the show offered education sessions and relationship building.
“It is a one-stop shop where you can see everybody all in one place and you don’t have to jump on an airline,” Cole said.
Next year’s show is set for Jan. 9-11 in Austin, Texas.
“The Potato Expo hasn’t been to Austin, so it will be an opportunity for people to see some new sights and enjoy the atmosphere, and of course potatoes are grown in Texas,” Cole said.