Judge says 6 Arkansas farmers can spray dicamba on crops
Judge says 6 Arkansas farmers can spray dicamba on crops

The Associated Press

LITTLE ROCK, Ark.




LITTLE ROCK, Ark. (AP) — An Arkansas judge has authorized six farmers who challenged a ban on the in-crop use of dicamba to spray the herbicide on their crops this summer while their colleagues remain under a statewide ban on its use.
The Arkansas Democrat-Gazette reports that the farmers had sued the state Plant Board in November for banning the use of dicamba from April 16 through Oct. 31.
Pulaski County Circuit Judge Tim Fox on Friday dismissed the lawsuit, citing the state's sovereign immunity. But Fox says that the farmers' rights to due process and their right to appeal the Plant Board's ban on dicamba had been curtailed.
Arkansas enacted the ban after receiving nearly 1,000 complaints last year about the weed killer drifting onto fields and damaging crops not resistant to the herbicide.
___
Information from: Arkansas Democrat-Gazette, http://www.arkansasonline.com