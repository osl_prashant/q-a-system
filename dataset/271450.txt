New farming technology helps preserve root crops
New farming technology helps preserve root crops

The Associated Press

JERICHO, Vt.




JERICHO, Vt. (AP) — A Vermont university professor's idea is changing how farmers store root crops over the winter.
WCAX-TV reports Chris Callahan at the University of Vermont Extension has introduced the VESTA Control System. The device controls humidity to help crops such as carrots stay healthy in the colder months.
Callahan says he was inspired by the trend of Vermont farmers looking to operate year round.
Jericho Settlers Farm owner Mark Fasching says he experienced about a 20 percent carrot crop loss four years ago. With the new device priced at about $2,000, he says his crop loss is now at 2 percent.
The company that installs the device says they have completed beta testing and are starting mass production.
___
Information from: WCAX-TV, http://www.wcax.com