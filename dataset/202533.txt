The University of California-Davis is accepting applications for its 2017-18 Online Applied Sensory and Consumer Science Program.
 

In its 16th year, the online Applied Sensory and Consumer Science Certificate Program is approved by the Department of Food Science and Technology at UC-Davis, according to a news release.
The program is taught by UC-Davis professor Jean-Xavier Guinard and Tragon Corp. chief sensory officer Rebecca Bleibaum, according to the release.
 
"Our program fulfills a clear need for professional development in this area of study, and the sharing of experiences and knowledge it allows is invaluable," Howard Schutz, program founder and professor emeritus of UC-Davis, said in the release.
 
The 16-unit program is offered entirely online and allows students to interact with faculty and peers from around the world, according to the release.
 
Information about the program is available at http://extension.ucdavis.edu/sensory.