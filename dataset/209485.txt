As National Dairy Month kicks off, we took some time to look at what people have been talking about leading up to the month dedicated to milk, cheese, yogurt and the rest of dairy’s delicious offerings. Here’s what we found:Look Who's Talking Now
While the whole country is in on the dairy conversation, California, New York, Illinois, Florida, Wisconsin and Texas are the most prolific.

Say Cheese
When looking at social media, cheese is the talk of National Dairy Month so far.

When Your State Food has a State Food
That attention to curds is largely thanks to a move by Wisconsin Governor Scott Walker to make cheese the state dairy product of Wisconsin.
The cheesiest new law: Gov. Scott Walker signs bill making cheese Wisconsin's official dairy product. https://t.co/wGisjHhfjb #odd
— AP Oddities (@AP_Oddities) June 2, 2017
 
Cheese to Your Health
The good news is, a new study says Wisconsin’s favorite dairy product is also good for you.
Full-fat dairy stuff — cheese, etc. — isn't bad for you, study finds. https://t.co/F1dqIZiymH
— USA TODAY (@USATODAY) May 9, 2017
 
Fat Cheese Stacks
Cheese isn’t just a big deal in Wisconsin. In fact, one of the richest men in the cheese industry is in Denver, Colorado.
This cheese supplier spent more than $600 million to build the safest mozzarella factory in the world https://t.co/H3ljDfma0l pic.twitter.com/atJKPuq4rD
— Forbes (@Forbes) June 7, 2017
 
Big Yogurt Goes to China
Even China wants in on the dairy action.
China’s largest dairy company plans an $850 million bid for Stonyfield Farm as organic yogurt gains popularity https://t.co/EIz0omkyJY
— Wall Street Journal (@WSJ) May 11, 2017
 
... and Cats
Because why not?
Cats catching squirts of milk during milking at a dairy farm in California, 1954 pic.twitter.com/0jD2T6w6BW
— Old Pics Archive (@oldpicsarchive) May 14, 2017
  And with a whole lot of June left, there’s sure to be a lot more dairy news coming out of National Dairy Month.