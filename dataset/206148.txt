Colfax, N.C.-based Foster-Caviness Foodservice has been awarded a maximum $93.75 million contract by U.S. Defense Logistics Agency Troop Support to supply North Carolina military bases with fresh fruits and vegetables.
 
According to a Department of Defense news release, the company’s contract is for five years, with no option periods. The company beat out two other companies competing for the contract, according to the release.
 
Foster-Caviness will supply fresh fruits and vegetables to Army, Navy, Air Force and Marine Corps bases and in North Carolina. The contract completion date is Sept. 26, 2022, according to the release.