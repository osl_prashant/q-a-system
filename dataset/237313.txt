$115M lumber mill planned for central Louisiana
$115M lumber mill planned for central Louisiana

The Associated Press

BATON ROUGE, La.




BATON ROUGE, La. (AP) — A $115 million lumber mill is planned for construction in central Louisiana, expected to create 110 onsite jobs.
Ruston-based Hunt Forest Products announced the plans Wednesday, saying it would build the facility in Urania, in LaSalle Parish, in a joint venture with Canadian forestry company Tolko Industries.
Construction is expected to start in April, with operations slated to begin in early 2019.
Gov. John Bel Edwards hailed the project as a "great economic boost" in the heart of Louisiana's timber country.
Edwards' office says the Urania sawmill will produce about 200 million board feet of lumber a year.
Jobs at the mill will have an average annual salary of $46,000 plus benefits.
Tolko will own a 50 percent share of the facility, which will be managed and operated by Hunt.