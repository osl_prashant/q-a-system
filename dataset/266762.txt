BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Monday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeApr        12.33   12.55   12.27   12.52  Up   .17May                                12.63  Up   .17Jun        12.43   12.65   12.38   12.63  Up   .17Jul                                12.98 Down 2.32Sep        12.86   13.01   12.79   12.98  Up   .11Dec                                14.19  Up   .06Feb        14.07   14.24   14.07   14.19  Up   .06Apr        14.29   14.43   14.29   14.35  unchJun        14.46   14.54   14.41   14.45  Down .02Sep        14.71   14.79   14.64   14.69  Down .03Dec                                15.27  Down .05Feb        15.30   15.38   15.23   15.27  Down .05Apr        15.30   15.34   15.23   15.23  Down .05Jun        15.32   15.34   15.26   15.26  Down .05Sep        15.53   15.53   15.46   15.47  Down .06Feb                                15.65  Up   .35