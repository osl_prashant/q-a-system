The Nogales, Ariz.-based Fresh Produce Association of the Americas recently added Erika Dominguez as policy analyst/communications coordinator. 
The Nogales native has worked previously for the FPAA as programs membership coordinator and has worked on the Unisource rate case for FPAA, according to a news release.
 
She graduated from the University of Arizona with a bachelor’s degree in political science in 2015.
 
“It is an honor to be working with an organization that does so much for our community,” Dominguez said in the release.