Foreign worker saves 7 lives as Hawaii fishing boat sinks
Foreign worker saves 7 lives as Hawaii fishing boat sinks

By CALEB JONESAssociated Press
The Associated Press

HONOLULU




HONOLULU (AP) — Khanh Huynh has been a commercial fisherman since he was 12 years old. For the past six years, he's been living on a fishing boat in Hawaii, catching premium ahi tuna for some of the world's most discerning consumers.
The 28-year-old fisherman from outside Ho Chi Minh City, Vietnam, recently saved the lives of two Americans and helped rescue five others after the fishing vessel he was working on sank hundreds of miles off Hawaii's Big Island.
But Huynh isn't the captain. He works 12- to 20-hour days for less than $10,000 a year in one of the most dangerous occupations. In fact, it's illegal for Huynh to act as the master of a commercial vessel in federal waters because he's not a U.S. citizen. But the American captain, who was supposed to be in charge of the Princess Hawaii, had never worked a longline vessel in the Pacific before.
A federal observer who was one of eight people on the boat said the Vietnamese worker was in charge of the vessel from the time it left port to when it sank.
"I never once saw the American captain make any attempt to operate the vessel, to issue any commands, directions or anything to make the vessel more seaworthy, more stable," the federal contractor, Steve Dysart, told The Associated Press. "I only saw him in his bunk."
The American captain, Robert Nicholson, declined to comment.
The U.S. Coast Guard is investigating.
The sinking is the latest in a string of potentially deadly mishaps in a fleet that has been plagued with concerns about exploitative, dangerous and sometimes abusive labor practices.
State and federal labor laws do not extend to Huynh and the roughly 700 other foreign workers like him in Hawaii. Because they have no U.S. work visas, the workers are refused entry into the United States and must live aboard vessels for years at a time, even when docked in Honolulu.
A 2016 Associated Press investigation found men living in squalor on some of these boats, suffering sores from bed bugs and sometimes lacking sufficient food. It also revealed claims of human trafficking .
The arrangement is facilitated by both federal and state officials and allows boat owners to pay brokers to bring in low-wage crews from Southeast Asia and Pacific island nations.
___
When he's not at sea, Huynh, who grew up in Vietnam's Dong Nai province, spends his nights locked behind a gate on a fishing pier a few miles from Waikiki's postcard-perfect beaches.
On the Sunday the ship sank, he was working 400 miles (644 kilometers) offshore, cleaning the cabin as the crew set about 15 miles (24 kilometers) of fishing hooks.
Huynh was hired as a deck hand but served as the vessel's de facto captain, Dysart said. Under a known tactic in federal waters, U.S. captains are sometimes listed as master to comply with law but in fact do little to run the ship.
No federal regulations prohibit foreign workers from standing watch over a boat when the American captain is asleep or otherwise unavailable, but the American aboard is required by law to be in charge of the vessel. Penalties include hefty fines for ship owners.
Just before noon, the 61-foot (19-meter) Princess Hawaii started to rock. As ocean swells rolled across the Pacific, it dipped to the waterline, filled with salty water and started to sink.
In the wheelhouse, Huynh tried to right the ship, but seawater was already sloshing against the cabin's windows.
"I rushed to the control cabin, grabbed the steering wheel and tried to turn it, but I couldn't," Huynh told the AP. "The boat kept on tilting. It was almost vertical, and I fell on the window."
Huynh grabbed a hammer, smashed the window and escaped. He then saw five men already in the water.
Still inside the boat were the two Americans: the National Oceanic and Atmospheric Administration observer and the captain.
Dysart heard Huynh yelling, "Get out, get out, get out!" Dysart grabbed a life jacket and headed for the only exit, a door at the rear of the vessel.
"I was really worried because when I was heading toward the door, I'm looking out the porthole, and all I see is water," Dysart said.
Huynh stretched out his hand to grab Dysart.
"He saved two people's lives, my own included," Dysart recalled. "He grabs my arm and literally drags me over this hook box."
To flee the sinking ship, the observer and captain had to swim underwater, find the doorway and swim back to the surface.
Huynh had already deployed their life raft. He pulled the Americans aboard, and the three men rescued the five crewmembers, a mix of fishermen from Vietnam and Kiribati.
__
The vessel, formerly named the Lihau, was recently altered, owner Loc Nguyen said.
Dysart was on the same boat years earlier and thinks the changes could have made the vessel unstable. He noted it was listing days earlier as it left Honolulu Harbor.
"The boat had been so extensively modified, I did not recognize it," Dysart said.
Nguyen said he added an icemaker, water machine and some steel support elements, but blamed the sinking on two huge rogue waves.
"A big wave in the back come in and clobber the boat, and one more time and on the side," said Nguyen, who was not on the ship.
Dysart disagrees about the rogue waves. "We got about an 18-knot wind, about 6- to 10-foot (2- to 3-meter) seas. ... It wasn't real bad."
The Coast Guard confirmed the conditions observed by Dysart, who believes the boat was simply positioned incorrectly — not facing into the oncoming waves — when it sank.
NOAA's observer program manager in Honolulu also said he had no reason to doubt Dysart's account.
"He's telling it like it is," John Kelly said. NOAA contracts observers to monitor and log information about the commercial fishing industry.
Commercial fishing vessels undergo routine safety inspections, but they mainly involve "primary life-saving equipment," said Charlie Medlicott, with the Coast Guard in Honolulu. "It doesn't really get into machinery and hull and structural integrity."
The Coast Guard inspected the boat in February and found no violations.
There are no federal licensing requirements to operate smaller commercial fishing vessels in federal waters, other than being a U.S. citizen. Some states require a license for state waters, but not Hawaii.
__
A single tuna can fetch more than $1,000. And while the Hawaii fleet is among the nation's smallest, it routinely ranks in the top 10 in terms of value.
In Vietnam, Huynh worked on fishing boats for 10 years, usually earning about $140 to $180 per month.
On American-flagged ships in Hawaii, he can make three times that — $500 a month— much of which he sends to his parents in Vietnam.
Despite the terrifying ordeal, Huynh has no plans to return home.
"When I was inside the boat, I was scared," he said. "I'm not scared of getting back out to the sea."
___
Associated Press writers Sophia Yan in Honolulu and Hau Dinh in Hanoi, Vietnam, contributed to this report.