The average American is at least three generations removed from the farm, making them more curious about where the food on their plate came from and how it got there.There tends to be a lot of (mis)information when it comes to  genetically modified organisms (GMOs), and a new organization is working to provide trustworthy, research-based information to consumers about the benefits of GMOs.

A Fresh Look’s representative Rebecca Larson told AgriTalk host Mike Adams the organization will serve as a platform for growers to share how farming produces safe and nutritious food.
Since there are several groups out promoting the safety of GMOs, Larson says they directly connect the consumer with the grower.
“This is not a bio-tech industry funded program, so that sets it apart differently,” she said.
This grower-lead campaign is made of 1,600 independent growers, sharing on-the-farm experience with consumers.
Hear how farm moms are using the platform to educate others and how A Fresh Look is tackling the pesticide issue “head on” on AgriTalk above.