Produce Retailer magazine is featuring exceptional personnel, winners of the 2017 United Fresh Retail Produce Manager Awards. The program, sponsored by Dole, recognizes 25 produce managers, the “best of the best” in produce departments across North America.  
> How long have you worked in retail and produce?
 
41 years.
 
> What do you love most about your job? 
 
I’m involved with customers every day. Produce is always interesting every day with many different challenges, such as building displays, pushing for sales, market issues that arise, weather challenges in the growing areas.
 
> What’s your biggest challenge as a retail produce manager?
 
Providing the level of service and standards that I, myself, expect daily in my department.
 
> What has changed most in the produce department in the past five years? 10 years?
 
There is more automation and varieties coming into the marketplace.
 
> What are some fruits and vegetables whose sales are growing faster than others right now?
 
Value-added vegetables and fruits that are ready to consume by the customer without having to prepare themselves.
 
> What do you think the produce industry should do to raise consumption?
 
It should educate and market new products to explain to our customers how to use, what it tastes like, recipes, etc.
 
> What are some things you wish consumers understood better about the produce department?
 
Seasonality and how weather affects products.
 
> What would you tell someone who was thinking about getting into the retail industry?
 
That it is both rewarding and challenging at the same time.
 
Carlo Scafati is the produce manager for Metro Inc., Etobicoke, Ontario.