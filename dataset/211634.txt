Los Angeles-based The Wonderful Co., has acquired Firebaugh, Calif.-based Ruby Fresh under its Pom Wonderful division, the largest grower and producers of fresh pomegranates, arils and pomegranate juice.Ruby Fresh, which processes and distributes fresh pomegranates and arils from its domestic and international grower partners, will provide Pom Wonderful with greater supply during the North American season and increased access to arils beyond the traditional October to January season, according to a news release.
The Ruby Fresh brand will remain intact.
“This season’s crop is shaping up beautifully, and we’re thrilled to be able to offer both delicious and healthy brands to even more consumers through this acquisition,” Elizabeth Stephenson, president of Pom Wonderful, said in the release.
Ruby Fresh employees and operations will be integrated into Pom Wonderful.
“Pom Wonderful is a well-respected leader within the pomegranate industry. We’re excited to be joining The Wonderful Co. family and look forward to working together to grow the market for fresh pomegranates and arils,” David Anthony, head of sales for Ruby Fresh, said in the release.
Ruby Fresh's brand will remain intact after the acquisition by Pom Wonderful.
Ruby Fresh founder Jason Hall died Nov. 29 and the family has continued to run the business in his absence.
Pom Wonderful is investing heavily in marketing for their growing pomegranate brands and the acquisition fits into that growth.
Adam Cooper, vice president of marketing for Wonderful, said Pom Wonderful juice saw a sales increase of 25% compared to a year ago.
Arils saw a near 30% increase in sales and a volume growth of almost 20%.
“Pom accounts for the majority of the growth at 75% and Pom Poms dollar share is now at a record 79%,” Cooper said.