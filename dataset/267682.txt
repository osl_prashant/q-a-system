Dairy producers are facing another low price cycle in the milk market, but that’s part of the battle they’re facing to stay in business. Labor, dairy policy and trade have also been at the forefront of the discussion. 

The industry is showing its support for the Agricultural Guestworker (AG) Act, also known as the H-2C program, and there’s talk about additional changes in the Margin Protection Program (MPP).

During the Central Plains Dairy Expo in Sioux Falls, South Dakota, Michelle Rook talks to producers to find out how they’re managing in 2018 on AgDay above.