Spring is the time of year when fall calving cow-calf operations wean their fall-born calves and summer stocker operators place calves into summer grazing programs. The purpose of this article will be to examine the profitability of cow-calf operations that have recently sold, or will soon sell, their fall born calves. A very similar article was written last year that took this same basic approach and overall profitability is very similar to where it was at that time.
Table 1 summarizes estimated spring 2018 costs and returns to a traditional fall-calving cow-calf operation. Every operation is different, so producers should modify these estimates to fit their situation. Average weaning weight is assumed to be 550 lbs and the steer / heifer average calf price is assumed to be $1.45 per pound. This price is based on the mid-April 2018 market, which actually decreased slightly from March. Weaning rate is assumed to be 90%, meaning that it is expected that a calf will be weaned and sold from 90% of the cows that are managed and exposed to a bull. This is a relatively high weaning rate as this analysis will generally assume a well-managed operation and reflects more favorable weather during the breeding and calving seasons for fall calving cows. Based on these assumptions, calf revenue per cow is $718.
The pasture stocking rate is assumed to be 2 acres per cow-calf unit and pasture maintenance costs are assumed to be relatively low. At $25 per acre, this would include one pasture clipping and seeding some legumes on a portion of the pasture acres each year. Producers who apply fertilizer to pasture ground would likely see much higher pasture maintenance costs and these costs should be adjusted accordingly. Producers should also consider the stocking rates for their operation as this will vary greatly, especially for fall calving herds. Stocking rate impacts the number of grazing days and winter feeding days for the operation, which has large implications for costs on a per cow basis.
The primary cost difference between a fall-calving herd and a spring-calving herd is winter feed. Since fall calving cows are lactating during the winter, their nutrient requirements are higher when stored feed is typically fed. For the initial purposes of this analysis, fall calving cows are assumed to consume 2.5 tons of hay through the winter and that hay is valued at $90 per ton. This hay value is considerably above “market” price in most areas, but is high due to the greater hay quality needs of fall calving cows. In some settings, fall calving cows may be fed lower quality hay, in which case weaning weights (and revenues per cow) would be lower. An alternative strategy for some operations might be to feed lower quality hay and supplement cows during the winter. If this is done, both the cost of the supplemental feed and the additional feeding labor should be considered. Regardless, winter nutrient needs are higher for fall calving cows, and this comes at an additional cost. Mineral cost is set at $35 per cow, veterinary / medicine costs $25, trucking costs $10, machinery costs $20 (primarily for feeding hay as this does not include machinery for hay production or pasture clipping as they are included in those respective costs), and other costs $25. Marketing costs are assumed to be $30 per cow, but larger operations may market cattle in larger groups and pay lower commission rates.
Breeding stock depreciation is a key cost that is often overlooked. Breeding stock depreciate just like any other asset on the farm. For example, if the “typical” cow entered the herd as a bred heifer valued at $1,700 and her expected cull value was $700, then she would depreciate $1,000 over her productive lifetime. If we assume a typical cow has 8 productive years, then annual cow depreciation is $125 using a straight line depreciation method. This is the assumption made in this analysis, but the actual depreciation will vary across farms. When buying bred replacement heifers, this cost is obvious. With farm-raised replacements, this cost should be the revenue foregone if the heifer had been sold with the other calves, plus all expenses incurred (feed, breeding, pasture rent, etc.) to reach the same stage as a purchased bred heifer.
Finally, breeding costs are assumed to be $40 per cow and are one of the most misunderstood costs on a cow calf operation. Breeding cost on a per cow basis should include annual depreciation of the bull and bull maintenance costs, spread across the number of cows he services. For example, if a bull is purchased for $3,500 and sold two years later for $2,500, the bull depreciated $500 each year. Then, if his maintenance costs were $500 per year (feed, pasture, vet / med, etc.), his ownerships costs are $1,000 per year. If that bull covers 25 cows, breeding cost per cow is $40. A similar approach can be used for AI, but producers should be careful to include multiple rounds of AI for some cows and the ownership costs of a cleanup bull, if one is used. Breeding costs per cow may be much higher for many operations as these assumptions are likely conservative.
Note that based on our assumptions, total expenses per cow are roughly $585 and revenues per cow are $718. So, estimated return to land, labor, capital, and management is $133 per cow managed. This is very similar to our estimates for spring 2017. At first glance, this return can be misleading, so some additional discussion is warranted. A number of costs were intentionally not included in this analysis because they vary greatly across operations. Notice that no value is placed on the time spent working and managing the operation, no depreciation on facilities, equipment, fences, or other capital items is included, and no interest (opportunity cost) is charged on any capital investments including land, facilities, and the cattle themselves. So, the return needs to be thought of as a return to the operator’s time, equipment, facilities, land, and capital.
As one thinks about quantifying these additional costs, it likely makes sense to start with land. Cow-calf operators should at least cover the rental potential of that pasture ground. Similarly, there is a great deal of capital investment on a cow-calf operation in facilities, fencing, and equipment that should be considered. Finally, a cow-calf operator should expect some return to the time they spend managing the operation. This might be best illustrated by using a simple, bare-bones illustration. At a relatively low land rental rate of $30 per acre, this would represent another $60 per cow in opportunity cost given the two acres per cow stocking rate. A similarly low $50 per cow estimate for depreciation and interest on equipment, fencing, facilities, etc. (this would not include hay equipment as hay is valued at market price in the analysis) and $30 value for the operator’s labor and management, would suggest that return to land, capital, labor, and management would need to be $140 per cow. Again, these numbers are likely low and variable across operations, but thinking through them is important to understanding current cow-calf profitability. Put simply, well-managed fall calving herds are likely covering cash costs and breeding stock depreciation right now, but are not likely receiving anything but minimal returns to the their capital investment, labor, and management.



Table 1: Estimated Returns to Fall Calving Cow-calf Operation: Spring 2018


 
 
 
 
 
 


Revenues


Steer / Heifer Calf Average
 
550
lbs
$1.45
$798


Discount for Open Cows
 
10%
open
 
$80


Total Revenues per Cow
$718


 
 
 
 
 


Expenses
 
 
 
 


Pasture Maintenance
2.0
acres
$25.00
$50


Hay
2.5
tons
$90.00
$225


Mineral
 
 
 
$35


Vet
 
 
 
$25


Breeding
 
 
 
$40


Marketing
 
 
 
$30


Machinery
 
 
 
$20


Trucking
 
 
 
$10


Breeding Stock Depreciation
 
 
 
$125


Other
 
 
 
$25


Total Expenses per Cow
$585


 


Return to Land, Labor, and Capital
$133



It is likely that the two most variable factors impacting cow-calf profitability are calf prices and hay / winter feed costs. So, table 2 shows estimated returns to this same fall calving cow-calf operation given a range of winter feed costs and calf prices. Note that the center of the table, which represents a steer / heifer average price of $1.45 and hay costs of $225 per cow perfectly matches the detailed budget shown in table 1. From there, calf prices are increased and decreased by $0.10 and $0.20 per lb.
Winter feed costs are increased and decreased by $50 per cow in table 2. This is done to capture a wider range of hay costs, winter feeding days, or other nutritional approaches employed by the cow-calf operator. For example, at 2.5 tons per cow through the winter, a $50 increase in winter feed cost would value hay $20 higher per ton and a $50 decrease in winter feed costs would value hay at $20 less per ton. Producers should consider where their operation likely lies on table 2 to better estimate their likely profit levels in this environment. Both tables 1 and 2 should help producers understand current returns to a fall calving cow-calf operation.



Table 2: Estimated Returns to Fall Calving Cow-Calf Operation given Winter Feed Costs and Calf Prices: Spring 2018


 
Avg. Steer/Heifer Price, 550 lbs


Winter Feed Costs
$1.25
$1.35
$1.45
$1.55
$1.65


$175
$84
$133
$183
$232
$282


$225
$34
$83
$133
$182
$232


$275
-$16
$33
$83
$132
$182


Note: Returns above are returns to land, labor, and capital based on the same assumptions used in Table 1.



Much like last year, it appears that fall-calving herds are likely covering their cash costs and breeding stock depreciation. However, each operator should also consider what return they need to adequately compensate them for their investment in land, capital (including depreciation), labor, and management. For example, if a producer felt that they needed a minimum of $140 return to compensate them for their time and investment as was previously discussed, our initial estimates in table 1 suggest that we are not reaching that level. Once enough producers start to feel this way, we will start to see herd liquidation in response to unsustainable profit levels over time. In the meantime, cow-calf operations should work to better understand their cost structure and what calf prices are needed to reach their profit goals. This will help them determine their best strategy as they make long-term decisions about their cowherds.