Industry groups are asking for emergency assistance from the U.S. Department of Agriculture to address milk prices.Both the American Farm Bureau Federation (AFBF) and National Milk Producers Federation (NMPF) have sent letters to Agriculture Secretary Tom Vilsack in regards to emergency assistance.
Officials from NMPF would like to see the USDA buy $100 to $150 million of cheese through the Section 32 program.
"Dairy producers here in the United States need assistance to help endure this 18-month depression in milk prices," says Jim Mulhern, NMPF President and CEO. "This type of assistance would both help economically-strapped farmers, and also help those without ready access to nutritious dairy products."
Buying up to $150 million in cheese could help distribute 90 million lb. of cheese to nonprofit food banks and remove 900 million lb. of milk from the market. According to NMPF, it would help bring $0.16/cwt back to producers and increase farm incomes approximately $380 million nationwide.
AFBF has been in support of a request made by Congress members to provide emergency assistance of dairy farms.
"The decline in dairy farm revenue has led many dairy farm families to exit the industry," says AFBF President Zippy Duvall. "In 2015 we lost 1,225 dairy farms - many of those small dairy farm operations where the average herd size is fewer than 200 milking cows."
Duvall and AFBF would also recommend the USDA buy dairy products for nutrition programs and food banks.
"If the Department spent $50 million, it could purchase 28 million pounds of cheese for domestic feeding programs. This would not only be beneficial to those in need of food, but also would help reduce the record high inventories and would provide a positive price impact for dairy producers," Duvall says.
Farm Groups Ask USDA to Buy Cheese Surplus


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.