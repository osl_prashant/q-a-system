AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:45 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
TOP STORIES:
COLORADO PRISON MISTAKE
AURORA — A Cuban immigrant who was pardoned by Colorado's governor after he was ordered released from prison only to be arrested by immigration authorities is free again. A smiling Rene Lima-Marin walked out of an immigration detention center in suburban Denver on Monday after winning his deportation case. His lawyer, wife, father and a niece and nephew were there to greet him. By Thomas Peipert and Colleen Slevin. SENT: 440 words, photos.
With: COLORADO PRISON MISTAKE-TIMELINE
FORT CARSON WILDFIRE
FORT CARSON — A wildfire that destroyed at least two homes in southern Colorado was sparked by an Army aviation training exercise that used live ammunition, the military said Monday. The fire started on Fort Carson amid dry, windy weather on March 16 and spread to private land, charring 5 square miles (13 square kilometers) before it was contained. It prompted evacuations of residents and livestock, but no injuries were reported. SENT: 350 words, photos.
IN BRIEF:
— CIVIL RIGHTS COMPLAINT — The developer of an apartment complex in Colorado Springs filed a civil rights complaint against the complex's neighbors who are looking to keep the housing project from breaking ground.
— ESCAPED INMATE — Authorities are looking for an inmate who escaped from a prison in southern Colorado.
— UNIVERSITY OF OKLAHOMA-PRESIDENT — A longtime energy company executive and major donor has been named the next president of the University of Oklahoma.
— GREEN FLASH BREWERY — Two days after listing all the brewing equipment from its Virginia facility for sale, the state's largest brewery in terms of production has announced it will stop distributing to the East Coast entirely.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for May fell 6 cents at 4.5425 a bushel; May corn was off 3.25 cents at 3.74 a bushel; May oats was up .25 cent at $2.2650 a bushel; while May soybeans lost 2.75 cents at $10.2550 a bushel.
SPORTS:
DAUNTING RETIREMENT
SAN FRANCISCO — Todd Helton now regularly drives his two daughters to school or other activities back home in Tennessee, a huge life change for Colorado's former All-Star first baseman. He had no idea walking away from baseball would be such a daunting and overwhelming adjustment. The daily routine that had become part of his DNA — the bantering, the batting practice, the games — replaced by chauffeuring kids, helping around the house and some golf. By Janie McCauley. SENT: 1,750 words, photos.
NUGGETS-76ERS
PHILADELPHIA — The Denver Nuggets take on the Philadelphia 76ers. (Game started at 5 p.m. MT)
With: 76ERS-FULTZ — Markelle Fultz will return to the Philadelphia 76ers' lineup on Monday. SENT: 130-word APNewsNow.
AVALANCHE-GOLDEN KNIGHTS
LAS VEGAS — The Golden Knights host the Colorado Avalanche on Monday night. UPCOMING: 700 words, photos. (Game starts at 6 p.m. MT)
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.