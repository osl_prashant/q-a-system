As summer fruit winds down, expect tropical displays to take a more prominent role in retail produce departments.
“Summer fruits dissipate, and that leaves more space on the shelf for tropical items like mango, papaya and pineapples,” said Rick Feighery, vice president of sales for Procacci Bros. Sales Corp., Philadelphia.
That added emphasis extends beyond tropical fruit. Jessie Capote, partner and executive vice president of J&C Tropicals, Miami, said items like yuca root, taro root, white yams and chayote squash are available year-round but get more attention from consumers and retailers from October through April when the weather is colder.
Capote said demand for tropical roots isn’t limited to particular cultural groups.
“Demand is growing with those populations, but it’s also becoming more mainstream,” he said.
Here is a look at what to expect from some other tropical items this fall.
 
Cherimoya
The Chilean season peaks from May through November, but Robert Schueller, director of public relations for Los Angeles-based World Variety Produce, said there might be a gap in late November and early December when California’s season is getting started.
 
Limes
Hurricane Franklin caused extensive damage to the lime crop in Veracruz, Mexico, in early August, said Charlie Eagle, vice president of business development for Southern Specialties, Pompano Beach, Fla.
“One hundred percent of the flowering in Veracruz was lost, which means there’s a shortage coming three to four months from now, and prices will most likely go unusually high,” he said.
 
Mangoes
Greg Golden, partner and sales manager for Amazon Produce Network, Mullica Hill, N.J., said Mexican keitt supplies could last into October, depending on the weather.
“This fruit can grow to enormous size with counts as large as 3s and 4s, which some people will not even recognize as a mango,” he said.
Brazil is expected to produce more than 7 million boxes with peak volume from mid-September to mid-October, he said.
“Quality should be fantastic due to the excellent and dry weather lately in the growing regions combined with the ample availability of irrigation water,” Golden said.
Brazilian mangoes are predominately tommy atkins, but Golden said Amazon is working to develop the market for the palmer variety and programs for kents, keitts and ataulfos.
Ataulfos from Ecuador (followed by tommy atkins and hadens) will start arriving in the U.S. in mid-October with a strong peak right before Thanksgiving, Golden said.
He also said Mexico, Brazil and Ecuador are all running a little later than last year, but it is not creating a problem because no gaps or large overlaps are expected.
Michael Warren, president of Central American Produce Inc., Pompano Beach, Fla., said fruit from Ecuador looked even better than last year when quality was “very good and arrived at ideal maturity.”
Schueller said World Variety Produce expects a volume increase of more than 20% during its third season of Australian mangoes, which runs November through February.
Varieties include kensington pride, R2E2, honey gold and keitt.
 
Others
Florida star fruit will peak in October, slightly later than usual, said Peter Leifermann, sales and procurement director for Brooks Tropicals LLC, Homestead, Fla.
Harvests of Florida avocados, red guava, dragon fruit and passion fruit are expected to maintain good volumes throughout the fall, said Mary Ostlund, director of marketing.
Leifermann said Florida’s winter passion fruit harvest will start in November and continue through February.