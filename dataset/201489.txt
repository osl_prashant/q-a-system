Farm Diesel

Farm diesel is a penny higher on the week at $1.94.
Last week we advised subscribers book 70% of expected harvest diesel needs and to prepare to book the remaining 30% soon.
Distillate supplies fell 0.2 million barrels last week, now 3.7 million barrels below the year-ago peg.
Our farm diesel/heating oil futures spread is at 0.30 which is a strong indication of pending near-term farm diesel price support.

 
Propane

Propane is unchanged at $1.16 cents per gallon this week.
Iowa and Illinois each posted slightly higher prices this week, but both were offset by a decline in Kansas.
Propane supplies firmed 1.672 million barrels on the week but remain 22.260 million barrels below year-ago.
Propane may be consolidating at this price point and preparing for a spring higher in response to seasonal demand. Prepare to book harvest and winter propane needs soon.

 





Fuel


7/24/17


7/31/17


Week-over Change


Current Week

Fuel



Farm Diesel


$1.89


$1.93


+1 cent


$1.94

Farm Diesel



LP


$1.19


$1.16


Unchanged


$1.16

LP