President Donald Trump plans to nominate Alexander Acosta for the position of Secretary of Labor. 
 
Acosta is currently dean of Florida International University Law School, according to a White House news release.
 
Andrew Puzder, Trump's first choice for the position, withdrew Feb. 15 amid media reports of criticism of his company's record of labor violations when he was CEO of CKE Restaurants.
 
Acosta was previously a member of the National Labor Relations Board, according to the White House release. He was the first Hispanic to hold the rank of Assistant Attorney General and went on to serve as the U.S. Attorney for the Southern District of Florida. 
 
"Alex is going to be a key part of achieving our goal of revitalizing the American economy, manufacturing, and labor force," Trump said in the release.
 
In a statement released Feb. 16, Irvine, Calif.-based Western Growers president and CEO Tom Nassif said the group looks forward to working with Acosta.
 
"American farmers are looking to the Trump administration for leadership on many challenging issues, and none ranks higher than the need for an immigration policy that protects American farmers from the loss of their valued employees and ensures access to labor in the future," Nassif said in the statement. "Within American agriculture, fresh produce growers are especially impacted by our broken immigration system, and it is our hope that as a resident of Florida, one of the nation's major fresh produce states, Mr. Acosta will bring a well-informed perspective to the cabinet."