Sometimes we are willing to look past the most wobbly propositions because we have invested our emotions and perhaps our dollars to the rightness of a particular idea.
 
It can be a million and one different ideas, from the choice of orange flowered wallpaper to styling in a sky blue polyester suit, buying into a religious cult, falling for an e-mail scam or voting for Candidate X for president. We have our ideas, and by golly, they are just as they should be.
 
"You don't like my Ford Pinto? Well, you are certifiably crazy. And look at you - you have a tinny Toyota Corolla from Japan!"
 
"Of course I should respond to the e-mail from the aging, cancer-stricken widow asking for my help to spend her fortune for the good work of God and humanity. It's just the right thing to do."
 
All of that to say that there are points in time when we have to face the facts and realize that our previous convictions were off base. The Ford Pinto's gas tank blows up, after all. And widow Sandra just wanted to scam me, it seems.
 
When it comes to this type of "emperor has no clothes" epiphany, we realize the error of our ways and course-correct, if we can. 
 
Decca Records eventually realized their blunder when they passed on signing the Beatles in 1962, but by then it was too late because the Fab Four had inked a record deal with EMI.
 
But perhaps more often than not, we will never be moved to consider that our path could have been easier, our goals accomplished with more surety, if we had simply chosen differently. Seven times out of 10, inertia will keep us moving in the same direction, or non-direction.
 
As it relates to the industry's effort to promote consumption of fruits and vegetables, I wonder if there will be a point in time where a definitive judgment is passed on what has been done and if it, indeed, has ultimately accomplished its stated goal.
 
For example, is the Produce for Better Health Foundation doing what it is supposed to do? 
 
Is the Sesame Street Eat Brighter! campaign working? Is Team FNV valuable enough to keep or should it be discarded?
 
These are big questions and not necessarily easy to answer. Gee, we love and support the folks at Produce for Better Health Foundation and the Produce Marketing Association to the max. 
 
The only question that is easy to answer is this: Are Americans eating enough fruits and veggies? 
 
No, they still aren't.
 
So what should be done, beside another five-year strategic plan by the executive committee and a new round of consumer research to explore why moms, dads and kids continue to fall short of recommended consumption goals? 
 
Did you really expect more? Can PBH make a silk purse out of a sow's ear, considering its meager funding?
 
It is better to light a candle than curse the darkness, but at some point we should go back to the drawing board and look for a strategy that really moves the needle.
 
Or perhaps we are happy enough with the status quo, leaving untouched the vague sense we could do better.
 
One persistent voice about the topic of raising per-capita consumption is John Sauve of the Food and Wellness Group. 
 
In response to a recent Fresh Talk post, John said that the industry is not focused enough on the health message.
 
From John: 
 
"Assume for the moment all you (meaning lots of produce folks) cared about was getting people to eat more fresh fruits and veggies ... maybe reach the DGA recommendation of 5 cups a day from the current 2 cups a day ... so every retailer could double their sales (potential). 
 
"Let's say that objective was all consuming. Now assume you knew, for a fact, an absolute fact, that the number one reason people ate fruits and vegetables was for their health. 
 
"Assume you also were informed enough and didn't confuse the reason to eat fruits and veggies - health - with the reason to buy a granny smith versus a golden delicious - taste. As you know, these two 'reasons' get mixed up all the time by researchers ... who didn't do their homework before developing the questionnaire nor handled the analysis of the data well. 
 
"Taste is not king as once professed by PMA. Never has been for why people eat fruits and veggies. Health is king. In fact, health belongs to fruits and veggies ... although supermarkets and industry abdicate that positioning to cereals and supplements and other food groups who chose to leverage the claim."
 
John has more to say in his comment, so I urge you to check out my Fresh Talk post headlined "Fresh produce matters, but is it enough?" Is the health message being ignored to the harm of the industry? 
 
Such a claim might stir the industry to reconsider its assumptions. 
 
Sad to say, reexamining our assumptions is not something that comes to any of us very easily.
 
Tom Karst is The Packer's national editor. E-mail him at tkarst@thepacker.com.
 
What's your take? Leave a comment and tell us your opinion.