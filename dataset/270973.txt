Increased transparency in labeling will be a leading industry trend among food and produce companies for 2017 and beyond. 
That’s according to Angela Fernandez, vice president of retail grocery and foodservice for the New Jersey-based standard setting GS1 US. She said the momentum toward transparency is related to consumers’ demand to know more about their purchases at retail and at restaurants, as well as changes in regulations relating to nutrition facts.
 
While some portion of local and regional producers still aren’t providing a unique Produce Traceability Initiative label to their buyers, more are coming on board, she said. In addition, most loose produce at retail has a GS1 databar sticker. The databar is a barcode that has traceability information like an item’s batch number or expiration date, in addition to other attributes used at the point-of-sale.
 
“The (databar) penetration is getting to a very high rate,” she said.
 
Many big retailers are transitioning away from generic Universal Product Code numbers lacking traceability info to item-specific unique UPC codes, Fernandez said.
 
In addition, Fernandez said there are increasing numbers of brand owners — though none yet in fresh produce — considering the adoption of SmartLabel technology to provide transparency to consumers. The program started in late December 2015 and gives consumers detailed information about products through scanning a QR code or visiting the SmartLabel website.
 
“Essentially what it does is that if the consumer is picking up their products, (the consumer) has the ability to scan that product (either a quick-response code or the UPC bar code) to take them directly to a page that the brand owner controls to provide the consumer information about the product,” she said.
 
The label can address nutritional information, allergens, whether the produce was genetically modified and claims relating to sustainability. 
 
“The SmartLabel is about putting control in the hands of the brand owner and providing that information in a consistent way, to make sure the consumer can access that information,” Fernandez said.
Right now SmartLabels — an initiative from the Grocery Manufacturers Association, the Food Marketing Institute and GS1 US — are mostly applied in center store brands, she said. Only a few center store brands have implemented SmartLabels so far, but the GS1 US team is working with the Produce Marketing Association to share the benefits of SmartLabels to produce marketers. 
 
Fernandez said there is a lot of applicability of SmartLabel to the fresh produce arena.
 
“When you think about how advanced we are getting as a produce industry, looking at temperature control, looking at how we can get to being able to harvest product out of the field, get it into the cooler, having it cooled down and having it staged within that 12- or 24-hour period — all of the things that we as trading partners are doing to harvest, pack, distribute and deliver that product — that all can come together with SmartLabel,” she said.
 
Brand owners can take all that information and disseminate directly to the consumer as they are interacting with the product at the local retailer, she said.
 
In a study by Label Insights called “Transparency ROI Study 2016”, 56% of consumers said that additional product information about how food is produced, handled or sourced would make them trust that brand more. In fact, 73% said they would be willing to pay more for a product that offers complete transparency in all attributes, according to the study.