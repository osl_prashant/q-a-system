The Latest: Administrative law judge rules on Enbridge
The Latest: Administrative law judge rules on Enbridge

The Associated Press

MINNEAPOLIS




MINNEAPOLIS (AP) — The Latest on Enbridge Energy's proposed Line 3 replacement (all times local):
5:15 p.m.
An administrative law judge says Minnesota regulators should approve Enbridge Energy's proposal for replacing its aging Line 3 crude oil pipeline only if it follows the existing route rather than the company's preferred route.  
Administrative Law Judge Ann O'Reilly recommended Monday that the Public Utilities Commission reject a route that avoids sensitive areas in the Mississippi River headwaters region where American Indians harvest wild rice and hold treaty rights. The proposal has drawn opposition because the line would carry Canadian tar sands crude.  
The commission is expected to make its final decision in June.  
Line 3 was built in the 1960s. Alberta-based Enbridge says a replacement is needed to ensure reliable deliveries of crude to Midwestern refineries. It has said proposed route alternatives are unworkable.
___
9 a.m.
An administrative law judge is due to recommend whether Minnesota regulators should approve Enbridge Energy's proposal for replacing its aging Line 3 crude oil pipeline across northern Minnesota.
The proposal has drawn strong opposition because the line would carry Canadian tar sands crude across environmentally sensitive areas in the Mississippi River headwaters region where American Indians harvest wild rice and hold treaty rights.
Enbridge, based in Calgary, Alberta, says the project is necessary to ensure the reliable delivery of crude to Midwestern refineries. The existing line, which was built in the 1960s, can run at only half its original capacity.
Administrative Law Judge Ann O'Reilly's recommendations, expected Monday, on whether the line is needed and routing, should guide the Minnesota Public Utilities Commission when it announces its decision in June.