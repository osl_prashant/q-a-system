San Joaquin County, California, saw almond, walnut and cherry crop values drop significantly in 2016, leading to an overall drop in agriculture production and a decrease of $161.56 million in fruit and nut crops alone.
Even so, the county grape crop  increased in value, deposing almonds as the top fruit and nut crop.
The office of county agriculture commissioner Tim Pelican released its annual report in late August, showing a 14.5% drop in overall production value from 2015, at $2.34 billion. Fruit and nut crops fell $11.7% to $1.22 billion. The report contributed that decrease mostly to almond meat production, dropping from $433.41 million to $348.82 million in 2016, and cherries, with a striking drop in fresh-crop value from $177.17 million in 2015 to $51.18 million in 2016. That crop, however, is prone to wild swings, according to recent production reports from the county. In 2013, the San Joaquin County fresh cherry crop was valued at $140 million, followed by $84 million in 2014.
The leading fruit and nut crops in San Joaquin County (in millions) in 2016 (and 2015 values) were:
Grapes (fresh/processed), $425.78; $351.45;
Almond meat, $348.82; $433.48;
English Walnuts, $273.97; $319.72;
Cherries (fresh), $51.18; $177.17;
Blueberries, $26.6; $21.37;
Apples (all), $21.52; $37.31
The overall organic fruit/nut crop value in the county in 2016 was $21.87 million. Organic vegetables were valued at $2.07 million. The county did not tally organic production in 2015.
The report is available at http://bit.ly/2vrscVg.