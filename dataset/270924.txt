Ready Pac Foods Inc. is introducing elevAte, a line of eight single-serve salads that are organic, non-GMO and gluten free.The elevAte brand is on eight superfood blends, each with a suggested retail price of $4.99. All have certifications including U.S. Department of Agriculture Organic; NSF Gluten Free; and Non-GMO Project.
They start shipping nationwide Nov. 3 and will be available at Wal-Mart, H-E-B, Albertsons, Safeway, Stater Bros., Wakefern and other retailers, according to a news release.
The salads are:
Blu-Rugula: Organic baby greens and baby lettuces with sorghum, aronia berries, almonds, organic chia seeds, cacao nibs and a blueberry honey vinaigrette dressing;
Go-Go-Goji: Organic chopped kale, carrots and red cabbage with azuki beans, goji berries, roasted cashews, broccoli and cauliflower florets, black sesame seeds and a green tea matcha dressing;
Kale Caesar: Organic chopped kale with non-GMO chicken raised without antibiotics, quinoa, beluga lentils, hempseeds, pepitas, cranberries and a Caesar vinaigrette dressing;
Organic Power Grains: Organic spring mix lettuce with quinoa, wheat berries, carrots and red cabbage, raisins and sliced almonds with an orange vinaigrette;
Organic Sunny Caesar: Romaine lettuce with grilled chicken, roasted sunflower seeds, parmesan cheese and a creamy Caesar dressing;
Organic Nutty Cranberry: Organic spring mix lettuce with dried cranberries, chopped walnuts and almonds, feta cheese and a raspberry vinaigrette dressing;
Organic Spinach Pow: Organic baby spinach, grilled chicken, dried cranberries, sliced almonds, Monterey jack cheese and a honey mustard dressing;
Organic Southwest: Organic romaine lettuce with carrots, fire roasted corn, black beans, Monterey jack cheese, tortilla strips and a southwest ranch dressing.
The elevAte brand has full-sleeve packaging, a first for Ready Pac Foods products, and it tells a separate story for each flavor combination. It encourages consumers to remove the sleeve to see what’s inside. Ingredients are identified on the sleeve and visible in the package.
“With elevAte, our goal is to create something innovative that speaks directly to today’s health conscious consumer at an affordable price,” Tony Sarsam, CEO of Irwindale, Calif.-based Ready Pac, said in the release.