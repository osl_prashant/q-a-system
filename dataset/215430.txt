Doug Culbertson is CMI Orchards’ new food safety manager.
Culbertson will work with food safety manager at CMI’s seven packing facilities in Washington, according to a news release.
He succeeds Bob Carter, who is retiring early next year. Culbertson previously was quality assurance and food safety manager at National Frozen Foods. He has a bachelor’s degree in food science from Ohio State University.
CMI President Bob Mast said Culbertson’s role is key.
“We know that our relationship with our retail partners and consumers rests in delivering safe and healthy products every day,” Mast said in the release.