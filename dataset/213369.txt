Using a killed vaccine during the cow’s dry period can benefit the long-term health and performance of her calf.
 
A calf’s first few days lay the groundwork for a lifetime of health and productivity. And, a vaccination during its dam’s dry period can help make sure those first few days go well for the calf and get it started on the right foot.
Dr. Linda Tikofsky, senior associate director of dairy professional veterinary services, Boehringer Ingelheim, said failure of passive transfer, when a calf doesn’t receive enough high-quality colostral antibodies, has both short- and long-term impacts.
“Immediately that calf does not have protection against the common viral and bacterial infections that are in its environment,” she said. “Calves with failure of passive transfer are more likely to have scours or pneumonia, and are at greater risk of death.”
Long-term, Dr. Tikofsky said the calf can potentially grow slower and produce less milk.
That’s why killed vaccines, safely administered at the beginning of a cow’s dry period, are becoming increasingly popular.
She said producers generally have three common questions about killed vaccines: If they should use them; what are the benefits; and how to fit them into their current protocols.
Should I use a killed vaccine?
The answer, Dr. Tikofsky said, varies from operation to operation.
“I always encourage producers to work with their herd veterinarian to make that decision,” she explained. “I don’t want to just go in and change a protocol when that veterinarian knows more about their management practices and the health of their herd.”
What are the benefits of a killed vaccine?
Killed vaccines offer an advantage over their modified-live counterparts: safety.
“One of the biggest advantages of a killed vaccine is producers can administer the same vaccination to the whole herd and not have to worry about separating out pregnant animals,” Dr. Tikofsky explained. “It’s safe for all ages and stages.”
How do I fit killed vaccines into my protocols?
Dr. Tikofsky says her No. 1 piece of advice is to maintain a good relationship with your veterinarian and make sure he or she is developing the protocols that are best for your operation, because every operation is different.
Good management practices can help ensure you’re getting the most out of your vaccines.
“The first thing to do is to make sure your neonatal calf management is top notch,” Dr. Tikofsky said. That means cleanliness of the operation and the right quantity of high-quality colostrum have to be optimal before enhancing immunity with strategic vaccination.
But while calves may be the focus, cows hold an important piece of the puzzle, too.
“The health, comfort and nutrition of the dam as she goes into the dry period will affect her ability to not only respond to a vaccine, but make better colostrum,” she explains.