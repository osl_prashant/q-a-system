Since the initiation of the BQA program and the national audits, animal handling has become a key component in the overall beef-quality conversation, says Ron Gill, Texas A&M Extension livestock specialist. More people recognize the relationships between stress, immunity, antibiotic use, carcass traits and human safety. 

 
The industry trend toward larger, heavier weight cattle could drive the need for changes in the design of facilities and equipment. 
 
For example, at Colorado State University’s (CSU) Agricultural Research and Education Center, researchers and crews use cattle-processing facilities designed by Temple Grandin that were installed around 20 years ago. The team uses the facility to process feeder calves, mature cows, heavy steers and yearling bulls. A few years ago, the team realized some cattle had simply outgrown the facility, says Jason Ahola, CSU animal scientist. 
 
The university team welded several inches to the width of the snake leading up to the chute, an illustration of how producers might adapt facilities to accommodate today’s cattle.
 
 

 

 
Read other stories from the Beef's Quality Revolution series: 
A Generation of Quality Gains
25 Years of Beef's Quality Challenges
Meat, Millennials, Meal Kits
Consumers Shift Attention to Cultural Concerns
Adapt Facilities, Equipment to Your Cattle
Resources to Improve Your Operation