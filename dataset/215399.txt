Can avocados stay hot?
In a story in The Packer August this year, Emiliano Escobedo, executive director of the Hass Avocado Board, Mission Viejo, Calif., said the industry’s goal is to see avocado consumption rise from the current 7 pounds to 14 pounds in four years. 
He said in the story that the vision of HAB is to be the catalyst to make fresh avocados the No. 1 consumed fruit in the U.S.
That is big-time ambition. 
Bolstering California’s well-established supply pipeline, the addition of Mexican avocados to the U.S. market 20 years ago has taken consumption of the fruit ever higher.
U.S Department of Agriculture statistics show total avocado imports totaled $28 million in 1997. Incredibly, the value of U.S. fresh avocado imports in 2016 was more than $1.9 billion, and imports from January through October this year topped $2.2 billion.
Mexico accounted for 85% of U.S. imports in 2016, with Peru, Chile and the Dominican Republic also bringing in substantial volume.
I recently asked the LinkedIn Fresh Produce Industry Discussion Group this question: 
Avocados - can they stay hot? Per capita use of avocados has increased from 1.6 pounds in 1995 to 7 pounds in 2017. What has contributed to the explosion and can it continue?

Great to read all 14 responses so far.
Some excerpts from a few that have weighed in:
J.S. I think the avocado industry has done a masterful job of marketing their product to take it mainstream, for example focusing on foodservice influencers - have you noticed that sliced avo or guac is on just about every fast casual menu these days?

G.S. Hispanic population is growing at a 2.2% per year. With this in mind, we have to consider the impact of their culture in the US consumption patterns.
E.V. Other reasons for the growing demand is the availability of ripe, ready-to-eat avocado and all year round availability.
K.S. Millions of dollars in Advertising and heavy promotions helped introduce Avocados (Green Gold) to the rest of the United States. Thank you Avocados from Mexico, Hass Avocado Board and the California Avocado Commission. Demand has risen from 10 Million lbs a week to over 40 Million lbs a week over that same time period.
 
TK:  Supply constraints and high prices, more than the ability to grow demand, may eventually hamper the growth of per capita consumption. But, based on the industry consensus now, that’s not going to happen anytime soon.