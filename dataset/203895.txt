The San Carlos, Calif.-based Chilean Avocado Importers Association has adopted a new logo, changing from "Avocados from Chile" to "Chile Avocados," said Karen Brux, managing director.
"Our goal was to develop a lively, contemporary logo focusing on the origin of our avocados," Brux said.
Although it might look simple at first glance, she said, there are multiple layers to the new logo.
The "C" also serves as an avocado, with the red, white and blue colors representing the colors of Chile's flag.

"We feel it's a more striking and bold representation of our brand," she said.
The association's website, social media and marketing materials have been updated to incorporate the new logo.
The association also offers merchandising materials for the trade through avocadosfromchile.org and a fully integrated social media program encompassing Facebook, Twitter, Instagram, Pinterest and YouTube.
"Our merchandising materials cover three themes - nutrition, taste and seasonal - with catchy supporting taglines such as 'The Game Changer,' 'Taste that Tops Everything' and 'Open Up and Say Ahhvocado,'" she said.