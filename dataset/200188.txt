Q. Can organic acids increase stability?
A. There are a couple of strategies for using organic acids ‚Äî propionic, acetic and benzoic acids ‚Äî to control aerobic stability.
The first is to apply high rates of the acid or acid mixture to achieve complete preservation of the forage material. To be effective, 10 to 20 lb. of active ingredient (AI) is required per ton of feed ensiled. This could mean applying 30 lb. plus of a product containing 70% actives.
The second strategy is to apply organic acids at low rates (2 to 4 lb. of AI per ton) at ensiling to control yeast populations at feed out. This technique does not provide full preservation, and the forage material is still dependent on having an ensiling fermentation, which will be slowed down as the organic acids also will affect the growth of the lactic bacteria required for this process.
In either case, it is very important to follow the manufacturer's recommended application rates when using organic acids or the situation can actually be made worse and can even result in the stimulation of mycotoxin production.
I am sometimes asked if an inoculant can be used alongside organic acids to overcome the fermentation delay.
 
Organic acids and inoculants cannot be mixed together in an applicator, which leads to practical application issues, plus using both an organic acid and inoculant significantly increases production costs. Keep in mind that the high application level of Lactobacillus buchneri 40788 on its own has been reviewed by the FDA and allowed to claim improved aerobic stability.
 
Research studies comparing corn silage or HMC normally fermented or treated with organic acids have shown no differences in palatability, intake or animal performance.
For additional silage tips, visit www.qualitysilage.com or Ask the Silage Dr. on Twitter or Facebook.