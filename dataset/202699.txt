Belgium-based Greenyard, which merged with Univeg in 2015, reported sales of $1.058 billion for the quarter ending Dec. 31, an increase of 6.3% compared with year-ago results.
 
The company said fresh division sales rose 4.7% for the quarter, according to a news release. Vero Beach, Fla.-based Seald Sweet International is a part of Greenyard company but the quarterly report did not break down fresh sales results for Seald Sweet.
 
Operating in 25 countries with 8,200 employees, Greenyard is a marketer of fresh, frozen and prepared fruit and vegetables, flowers, plants and growing media, according to the release.