The USDA’s prospective plantings report will be released in 37 days, and analysts and producers are making their own estimations as to what the acreage mix will be.

Some analysts are pegging soybean acres to reach upwards of 92 million acres, while others think corn and soybean acres will be split 50/50.

During the U.S. Farm Report roundtable taping at the National Farm Machinery Show in Louisville, Chip Nellinger of Blue Reef Agri-Marketing, Inc., thinks if there are extra soybean acres, they won’t all come from corn.

As for the driving force behind what crops will be planted, Nellinger thinks bankers will steer decisions, especially after the spring wheat rally and impressive cotton prices.

However, Alan Hoskins, president and CEO of American Farm Mortgage, is hoping bankers aren’t the ones calling the shots because the producers are more at risk.

“The banker obviously is there to serve as a resource to the customer,” he said. “If the producer is hearing from their banker [that] this is what you have to do, it’s certainly a valid question for the producer to look at the banker and say, ‘I want you to explain to me why.’”

Watch Nellinger, Hoskins, Jarod Creed of JC Marketing Services, LLC., and Bob Utterback of Utterback Marketing on U.S. Farm Report above.