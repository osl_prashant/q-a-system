The Houston Food Bank, along with the Houston Fresh Fruit & Vegetable Association, is asking for donations through the Summer Gift of Produce.The HFFVA event supplies the Houston Food Bank with fresh produce to distribute to its clients and programs.
The groups are looking for donations of No. 1- or No. 2-graded produce items to support the July 17-28 drive.
Judy Henrichsen, the food bank’s agriculture industry manager of logistics and supply chain, said updated tax laws allow for donated out-of-spec/overproduced products to be valued at the same price as other similar food products.
Donations of processing and transportation costs are also valuable for the Gift of Produce, Henrichsen said in a news release.
The food bank receives at all hours, or it can pick up. Receipts are sent as product is received.
For additional information, please see the below and contact Judy at veggielady@houstonfoodbank.org or 832 731 9436.