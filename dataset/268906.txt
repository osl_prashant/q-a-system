BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Thursday:
Open  High  Low  Settle   Chg.ORANGE JUICE                          15,000 lbs.; cents per lb.                May     139.50 140.85 139.00 139.95   +.70Jun                          140.35   +.45Jul     139.90 141.00 139.80 140.35   +.45Aug                          141.15   +.45Sep     140.70 141.75 140.60 141.15   +.45Nov     141.70 142.80 141.55 142.15   +.60Jan     142.50 143.00 142.50 143.00   +.65Feb                          143.40   +.65Mar                          143.40   +.65May                          144.15   +.65Jul                          144.25   +.65Sep                          144.35   +.65Nov                          144.45   +.65Jan                          144.55   +.65Feb                          144.65   +.65Mar                          144.65   +.65May                          144.75   +.65Jul                          144.85   +.65Sep                          144.95   +.65Nov                          145.05   +.65Jan                          145.15   +.65Feb                          145.25   +.65Mar                          145.25   +.65Est. sales 1,258.  Wed.'s sales 1,105   Wed.'s open int 13,759,  up 77