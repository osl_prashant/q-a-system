Eastern apple shippers say the export market is an important outlet, and its value is only increasing.
“Most of our fruit is sold domestically, although we are making a more concerted effort this season to export,” said Dave Williams, vice president of sales and marketing for Wolcott, N.Y.-based Fowler Farms.
It would appear to be a good year to be in the export market, said Cynthia Haskins, president and CEO of the Fishers-based New York Apple Association.
“Exports should be up this year compared to last year because the crop size is up and more fruit will be available to export,” she said. “Our exports should return this year to our five-year average levels.”
New York’s best export customer is Canada, though its apples travel the world, Haskins said.
“This season, we will be shipping to countries all over the world, including Central America, India, Israel, Thailand, Malaysia, even Dubai,” she said.
NYAA is a member of the U.S. Apple Export Council, which markets apple exports under the brand name USA Apples from New York, California, Michigan, Pennsylvania and Virginia.
The outlook for Virginia’s export numbers is positive, said David Robishaw, regional marketing specialist with the Virginia Department of Agriculture and Consumer Services.
“I’m expecting export numbers to increase for this shipping season,” he said.
Export sales were down in 2016 because there was less fruit available, Robishaw said.
That’s not the case this year, he said.
“This year the quality should be very good and we’ll have more fruit to offer, so the trend is moving upwards,” he said.
Brenda Briggs, vice president of sales and marketing with Gardners, Pa.-based Rice Fruit Co., said she anticipates normal export demand this year.
“An area of opportunity for U.S. apples may be found in Europe, as the continent suffered a fairly impactful frost this spring,” she said.
The European crop is down 21% this year, said Tim Mansfield, sales/marketing director with Burt, N.Y.-based Sun Orchard Fruit Co.
“India and other parts in Asia are developing. Europe demand will be strong this year due to a smaller European crop,” he said.
Poland’s crop, in particular, sustained weather damage, which should create opportunities for more U.S. fruit in markets Poland ships to, said Mark Nicholson, executive vice president of Geneva, N.Y.-based Red Jacket Orchards.