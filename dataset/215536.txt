Inoculants have been well documented to help with silage fermentation and quality; however, they contain live bacteria and have specific requirements for effective use. To get the best results, handle these living organisms with care. Then, maximize the return on your investment by applying them accurately and consistently.
Living organisms
Always read and follow the label directions for the inoculant purchased. However, sometimes label information is limited, so here are some general tips that work for most products:
Always keep the product refrigerated or on ice, in a cooler, until use.
Use whole pouches at a time. Microbes are sensitive to oxygen and moisture and partially used packs cannot be resealed.
Some inoculants are particularly sensitive to chlorinated water. Choose a product that will tolerate the level of chlorine in your water: ideally this should be below the level allowed for drinkable water (1 ppm) in any event.
Use clean (drinkable), cool water to dilute the product.
Use an applicator with an insulated or light-colored tank. Microbial inoculants die off more quickly when the temperature of the water mix exceeds 100 degrees Fahrenheit.
Select an inoculant that will remain stable for some time following hydration. Keeping the product out of the sun will help it remain viable.
Apply the inoculant at the chopper so the bacteria can start working immediately at harvesting.
Apply wisely
All inoculants must be applied accurately to achieve maximum, consistent effectiveness. Even distribution onto the forage is a key component to obtaining a strong front-end fermentation that will help preserve nutrients.
Maintain and calibrate applicators to match inoculant delivery rates. This will help ensure proper application of the product. Monitor uniformity of delivery several times a day.
Application precision improves with higher application rates, which is especially important for drier forages.  Using a calibrated spray applicator, liquid inoculants should be applied at a minimum of one pint per ton for silage and haylage, one quart per ton for high-moisture corn and grains, and a minimum of  two quarts per ton for dry baled hay.
For silage crops, liquid inoculants can also be applied using a low volume applicator (e.g. Dohrmann Dohrect Enject systems), typically at c. 1.28 oz. per ton.  This significantly reduces the amount of water required and some of the better systems keep the rehydrated inoculant cool  in an insulated tank.
Whatever application system is employed, it is important to monitor application rates throughout the silage harvest to ensure that the inoculant is evenly distributed and thus maximize the return on your inoculant investment.
Ask the Silage Doctor at QualitySilage.com, @TheSilageDoctor and at facebook.com/TheSilageDoctor if you have questions about applying liquid inoculants.