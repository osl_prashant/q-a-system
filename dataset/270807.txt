The Bowery, a New York hydroponic indoor farm, launched its growing operation, which grows leafy greens for the New York tri-state area, on Feb. 23.The company, which markets itself as “post-organic,” uses no pesticides and 95% less water than traditional agriculture, said Irving Fain, co-founder and CEO, but is not certified organic.
“We do not grow under (U.S. Deparment of Agriculture) organics certification, and I do not know that we will pursue that, either,” Fain said. “The product is so pure. We’re growing a product that has no pesticides, no herbicides, no fungicides, no insecticides.”
The company sells six retail stock-keeping units: baby kale, a romaine blend called “Bowery blend,” butterhead lettuce, arugula and kale mix, which are sold in 5-ounce clamshells. Basil is sold in a 0.75-ounce clamshell.
The greens are available at two New Jersey Whole Foods and a New York City Foragers Market, and are used at two of restaurateur Tom Colicchio’s New York restaurants.

Post-organic?

Post-organic, Fain explains, is the next step for sustainable food.
“Post-organic is really the next iteration, the next evolution beyond where organic is now. It’s producing the purest produce possible in a very efficient way that uses less resources at the time. It’s a net positive for the consumers,” Fain said.
The USDA does not allow the word “organic” on any package labels without certification, raising questions about the use of the phrase “post-organic.”
“When there are questions about use of the word ‘organic’ in marketing materials the agency would investigate the nature of the claims before making a decision on whether the use is acceptable under the law,” an agency spokesperson said.
Fain noted that “post-organic” is on their website, but not their packaging.
“Post-organic is on our website because most consumers are unaware that many organic foods are actually grown using pesticides,” he said. “Bowery products are grown with absolutely zero chemicals in a completely controlled environment to guarantee the freshest and purest produce imaginable.”
The company’s packaging, even without an organic label, is unique.
“We worked with an agency in New York to rethink the way greens packages were being created, and how could we create something that was both compelling on the shelf and leads to a better product,” Fain said.
“The packaging allows for more airflow between the packages themselves when they’re sitting on the shelf so the air circulates more effectively.”
The company’s website went live with their launch. More information can be found at boweryfarming.com.