Friday’s release of Japanese import data could pull the trigger on major disruptions in the U.S.-Japan beef trade.The Japanese government is considering raising tariffs on frozen beef imports from the U.S. and some other countries from 38.5% to 50%—effective through next March—to protect the countries domestic producers. The tariff increase is automatic if quarterly beef imports from all nations and from countries that do not have economic partnership agreements (EPAs) with Japan both rise more than 17% from a year earlier, reports an official with Japan’s Ministry of Agriculture, Forestry and Fisheries (MAFF). 
Japan was just approaching pre-BSE import levels of U.S. beef, but growth this year has outpaced many expectations. “Our exports of U.S. beef  to Japan are up 28% this year,” says Greg Henderson, editor of Drovers, on AgriTalk this morning. “The value of those exports are up 30% from this time last year.”

The tariff would only be on frozen products, mostly used for processed foods and hamburger, not the muscle cuts and high end steaks the Japanese like, Henderson told AgriTalk. Click below to listen to the morning commentary.
 

 
Market Impacts
“Because it’s an automatic trigger, I think it takes some sting out of this for the market,” says Chip Flory, Farm Journal economist and host of Market Rally radio show, on AgriTalk.
The increase in tariff doesn’t mean we won’t ship frozen beef to Japan, a higher tariff will have an impact but it doesn’t mean zero, Flory reminded AgriTalk listeners.
While Japan was just reaching its pre-BSE import levels of U.S. beef, the increase tariff might stymie trade. The lack of a deal with Japan, and the U.S. backing away from the Trans-Pacific Partnership, means Australia, the other major beef supplier to Japan, has a greater opportunity to supply the country’s beef. Australia, Mexico and Chile have EPAs and would be exempt from tariff hikes. Together, the U.S. and Australia provide 90% of Japan’s beef imports.
The Japanese import data will be released on Friday.