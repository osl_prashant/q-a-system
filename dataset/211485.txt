U.S. live cattle futures fell more than 1 percent on Tuesday, reversing from earlier life-of-contract highs on pressure from investment fund and technical selling, traders and analysts said.Feeder cattle futures declined by their daily price limit of 4.500 cents per pound while lean hog futures were mostly 1 percent higher.
Cattle prices were buoyed in recent weeks by rising wholesale beef prices and lower cattle weights - factors that suggested strong demand. But beef prices typically trend lower in the early summer months as retailer buying slows.
Uncertainty surrounding top global beef processor JBS also triggered profit-taking in cattle futures, the traders said.
"This was a healthy market correction," a cattle trader in Chicago said.
Chicago Mercantile Exchange June live cattle fell 1.900 cents to 130.200 cents per pound. Most-active CME August live cattle were down 2.600 cents to 123.600 cents per pound, after touching a contract high of 127.650 cents.
CME August feeder cattle declined 2.7 percent to finish at 155.375 cents per pound, reversing at midday amid heavy selling.
JBS Sells South American Beef Plants 
JBS SA announced a sale of its meat plants in Argentina, Paraguay and Uruguay to rival Minerva SA amid a scandal in which JBS admitted to paying politicians bribes.
JBS has beef plants in Texas, Nebraska, Michigan and Wisconsin. A U.S. cattle industry group, R-CALF, also released a letter asking President Donald Trump to investigate JBS' business in the United States.
The U.S. Department of Agriculture after the close of futures trading said choice-grade wholesale beef was up $2.10 to $250.45 per cwt, its highest in more than a year. Wholesale pork eased 93 cents to $90.25 per cwt, USDA said.
Lean hogs futures gained in a rebound from losses on Monday. Prices continued to trend sideways near last week's multimonth highs, with strong pork demand met by abundant hog supplies.
CME June hogs were up 0.925 cent to 81.650 cents per pound and most-active July hog futures up 0.950 cent to 81.375 cents.