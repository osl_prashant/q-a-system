Grains mostly lower, livestock mixed
Grains mostly lower, livestock mixed

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mostly lower Friday in early trading on the Chicago Board of Trade.
Wheat for March delivery was rose 3.30 cents at $4.5260 a bushel; March corn was fell .80 cent at $3.6560 bushel; March oats was off 2 cents at $2.5840 a bushel while January soybeans lost 5 cents at $10.3040 a bushel.
Beef was lower and pork was higher on the Chicago Mercantile Exchange.
February live cattle was off .87 cent at $1.2763 a pound; March feeder cattle fell .58 cent at $1.4570 a pound; February lean hogs rose 1.40 cents at $.7148 a pound.