A group of 25 honorees have been revealed for the United Fresh Produce Association's 2017 Retail Produce Manager Awards Program.
 
The program, in its 13th year, is sponsored by the Dole Food Co. and has honored more than 300 retail produce managers representing more than 90 different retail banners since 2005, according to a news release.
 
"We are grateful to Dole for once again sponsoring this program and for their partnership in recognizing these 25 deserving honorees," United Fresh CEO Tom Stenzel said in the release.
 
 The 2017 Retail Produce Manager Award Winners are:
Luis Fernando Almada, Northgate Gonzalez Market, Los Angeles;
Mike Caceres, Haggen/Albertsons Cos., Olympia, Wash.;
Mike Davis, FoodMaxx/Save Mart Cos., Oroville, Calif.;
John Dedie, Tops Friendly Markets, Henrietta, N.Y.; 
Michael Dyer, Raley's, Reno, Nev.;
Stefanie Galeana, Roundy's Pick N Save, Appleton, Wis.; 
Albert Garnett, Stop and Shop/Ahold, Horwich, Mass.;
Andrew Gass, Sobeys, Moncton, New Brunswick;
Lou Kwisnek, Big Y, Northampton, Mass.;
Laura Llewellyn, The Food Co-op, Port Townsend, Wash.;
Dillon Maple, Hy-Vee Inc., West Des Moines, Iowa;
Gordon Matthew, Fred Meyer/Kroger, Seattle;
Jack Myers, Weis Markets Inc., Bellefonte, Pa.;
Erin Niedbalski, Martin's Super Markets, South Bend, Ind.;
Patrick Odell, Price Chopper/Golub, Oneonta, N.Y.;
Matt Pace, Lowes Foods Inc., Winston-Salem, N.C.;
Stephanie Peter, DeCA Commissary, Miramar, Calif.;
Jerod Proctor, Schnucks, Springfield, Ill.;
Ricky Rommell, Food Lion, Wallace, N.C.;
Carlo Scafati, Metro Inc., Etobicoke, Ontario;
Tim Sexton, Harris Teeter, Chapel Hill, N.C.;
William Stewart, Food City/K-VA-T, Paintsville, Ky.;
Tim Wilson, Meijer, Grand Rapids, Mich.;
Bridget Winkelman, Coborn's Inc., Isanti, Minn.; and
David Wood, Brookshire Grocery Co., El Dorado, Ark.
 
The winners and their corporate produce directors, will be honored at the Retail-Foodservice Celebration Dinner on June 15 during the association's annual convention June 13-15 in Chicago. Five grand prize recipients will each receive a $1,000, according to the release.
 
"This is Dole's third year as title sponsor of the United Fresh Produce Manager of the Year Award, and we could not be more proud to be a part of this important recognition program," Tim Stejskal, senior vice president of sales for Dole Fresh Vegetables, said in the release. "These men and women do so much to promote the increased consumption of fresh fruits and vegetables, and this gives us an opportunity to recognize that contribution and genuinely say thank you."