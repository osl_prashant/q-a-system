A New Zealand grower-shipper is using a social media campaign to encourage shoppers to purchase kiwiberries.
The Auckland, New Zealand-based Freshmax NZ Ltd. is employing hashtags including #sweet, #small, #cool, #newlover,#happy to increase the kiwiberries' consumer popularity, according to a news release.
Freshmax is marketing the fruit as a convenient and child-friendly snack food and the campaign has shown the popularity of its Munch'n kiwiberry brand, according to the release.
Consumers are posting sentiments including "I found kiwiberries again" and #my new afternoon snack delight, according to the release.
The Munch'n kiwiberries are improved varieties that have been developed to satisfy shoppers' convenience needs, according to the release.
Since 2013, Freshmax has sold the product in the U.S. but only in small volumes, said Tracey Burns, export division manager.
From February to April, during its production season, the company plans to market the product in the U.S. and other target export markets and is finalizing deals with distribution partners, he said.
"New Zealand kiwiberries have been going into the U.S. for a few years, but we wish to launch a focused marketing campaign this season to lift product awareness around this magnificent berry," Burns said.
In 2017, New Zealand growers expect to harvest their largest kiwiberry crop.
A subsidiary of Freshmax Pty Ltd., the company also grows and distributes apples, pears, citrus, stone fruit, avocados, berries, tropicals and vegetables including asparagus.
Freshmax Group is a major stakeholder in Valleyfresh Exports Pty Ltd. and Pasadena, Calif.-based Valleyfresh North America, a produce distributor and packer which exports through offices in the U.S., Chile, Peru and South Africa.