CME Group Inc is considering switching to a cash settlement process for its live cattle futures, a managing director said on Thursday, as traders continued to complain about extreme volatility.Dave Lehman, managing director of commodity research and product development, said discussions about the new settlement procedure were part of an all-encompassing review of the market by CME, driven by concerns about price swings.
The CME change, if implemented, would be a major attempt to restore confidence in the market by adjusting the way it operates.
"It's on the table," Lehman said at the Reuters Commodities Summit. He added that CME was also looking at potential modifications to the physical delivery process, without providing details.
Private and government officials have been scrutinizing trading in futures and the cash cattle market since the start of the year, following a sharp setback in prices in the second half of 2015.
Futures are either cash settled or deliverable. Under the delivery procedure, a producer using the market as a hedge can buy back the contract or take delivery. Few contracts are actually delivered.
For cash-settled futures, a debit or credit is issued to a trader's account when a contract expires, based on the difference between his entry price and the settlement.
CME's feeder cattle and lean hog futures are already cash settled, leaving live cattle as the last remaining livestock market with physical delivery.
CME is taking a deliberate approach to listing new live cattle contract months in case it needs to make a major change, Lehman said. He said the company was not considering closing the market, and has already reduced trading hours and made other changes to reduce volatility.
Lehman said volatility has worsened as farmers have been selling cattle to meat packers months in advance, rather than negotiating prices shortly before slaughter.
Some producers have said that shift has created a questionable pricing system because packers often base prices for the longer-term contracts on what the U.S. government reports was the average price paid in the dwindling spot or negotiated market.
Derrell Peel, Oklahoma State University's extension livestock marketing specialist, said tying CME's futures settlement to cash prices may be unwise because reporting on cash prices has become unreliable.
"It looks kind of dangerous to me," he said.
In January, the National Cattlemen's Beef Association, which represents producers, blamed high-speed traders for futures volatility.
The market "absolutely is as volatile as it's ever been," said Joe Ocrant, president of Chicago-based Oak Investment Group and 40-year live cattle futures trader.
CME Announces Possible Cattle Changes


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.