Talks on restarting U.S. beef exports to China are moving fast and final details should be in place by early June, the U.S. Department of Agriculture said on Friday, allowing American farmers to vie for business that has been lost by rival Brazil.As part of a trade deal, U.S. ranchers are set to face tests over the use of growth-promoting drugs to raise cattle destined for export to China and to log the animals' movements, according to the USDA.
The two sides are negotiating to meet a deadline, set under a broader trade deal last week, for shipments to begin by mid-July.
Finalizing technical details in early June should mean beef companies such as Tyson Foods Inc and Cargill Inc can sign contracts with Chinese buyers to meet the deadline, the USDA said.
China banned U.S. beef in 2003 after a U.S. scare over mad cow disease. Previous attempts by Washington to reopen the world's fastest-growing beef market have fizzled out. But now, the quick progress of the latest talks is raising hopes of U.S. farmers.
"Both sides feel the urgency to get it done by the deadline," said Joe Schuele, spokesman for the U.S. Meat Export Federation, which represents Tyson, Cargill and other meat companies.
China's embassy in Washington could not immediately be reached for comment.
Brazil Woes
The timing of the new deal allows U.S. producers to benefit as Brazil, the world's top beef exporter, is struggling with scandals and rival shipper Australia is suffering from a drought that is hurting production, analysts said.
China accounted for nearly one-third of the Brazilian meat packing industry's $13.9 billion in exports last year.
But in March, Beijing briefly banned Brazilian imports after Brazilian police accused inspectors of taking bribes to allow sales of rotten and salmonella-tainted meat.
JBS SA, the world's largest meatpacker, was involved in the probe and in separate allegations this week that Brazil's president conspired to obstruct justice with the company's chairman.
The food-safety probe hit Brazil's beef exports, which fell by 24.6 percent to $378 million in April from March, according to Abiec, an industry group that represents meat processors accounting for about 90 percent of Brazil's exports.
"This is a very opportune time for the U.S. to step up," said Derrell Peel, an agricultural economist at Oklahoma State University.
Chinese appetite for beef has climbed due to its expanding middle class. In 2003, its imports totaled just $15 million, or 12,000 tons, including $10 million from the United States, according to the USDA.
Tracking Cattle
Brazilian exporters hope China's trade deal with Washington will not inflict more pain on meat companies in the country because U.S. exporters will be targeting different, higher-end customers, said Abrafrigo, an association representing Brazil's small meatpackers.
To reopen U.S. trade, Beijing has accepted a U.S. proposal in principle that would require producers to document the locations where cattle raised for beef exported to China are born and slaughtered, the USDA said. The system would be less onerous than tracking cattle throughout their entire lives, during which they can be kept at up to four different locations.
Peel, a livestock expert, estimated that U.S. producers trace the movements of less than 20 percent of the nation's cattle.
Under another proposed rule, U.S. beef exported to China must pass tests showing it is free from detectable residue of a class of growth-enhancing drugs known as beta-agonists that includes Elanco's Optaflexx, according to the USDA. Elanco, owned by Eli Lilly and Co, declined to comment.
A trade group for veterinary drug companies, the Animal Health Institute, said China should accept beef from cattle raised with beta-agonists because they are safe.
U.S. beef shipments to China also will have to come from cattle under the age of 30 months, according to the USDA. Most U.S. cattle will meet that requirement, the U.S. Meat Export Federation said.
The terms of the deal are a win for the United States over Canada, which is approved to ship only frozen beef to China.
China already bans meat from Canadian cattle fed with Optaflexx, according to the Canadian Meat Council. It also requires that Canadian beef be produced from cattle that are less than 30 months old and can be tracked to the farm where they were born.