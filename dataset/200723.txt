In spring, lush green grass grows in pastures-except when odd weather hits. Herd owners should watch grass this spring to see if it meets livestock grazing needs.After a long spell of weather stress, pastures, especially of tall fescue, may set seed heads instead of leaves. Missouri pastures had two extreme-weather seasons, winter and spring.
Winter gave record-setting drought. Spring held early warmth followed by cold and finally rain. Heavy precipitation fell in some areas.
Bad weather upsets plant growth, says Craig Roberts, forage specialist with University of Missouri Extension.
Winter had only 7 inches of rainfall. The state broke records on dryness in February. Recent rains ended the drought. However, temperatures varied, said Pat Guinan, MU Extension climatologist.
Both specialists spoke on the first MU teleconference of the growing season. They heard reports from MU Extension regional specialists and they answered questions.
Roberts warned that forages under stress may set seed heads early this year. Unstable weather can trigger a survival response. The plants create seeds to ensure continuation of the population.
"If recent rains and temperatures continue in April, spring forage growth could be great," Roberts says. "If not, pastures may look lush, but be stems and seeds instead of leaves."
Grass seed stems do not provide nourishment for grazing herds. This is true in all grass species, but tall fescue carries a potential threat. Most Missouri pastures are in tall fescue, and most of that is toxic Kentucky 31. The toxic alkaloids concentrate in stems and seeds.
The toxin comes from an almost invisible fungus growing between fescue plant cells. The alkaloids protect fescue plants by repelling insects, fungi, diseases and even grazing livestock.
Cows eating the toxin stop grazing. That protects grass but harms cattle.
Alkaloids create heat stress in cattle. They stop eating to stand in shade or cool water. In winter, the vasoconstrictors cut blood flow to body extremities-the feet, ears and tail. That can cause frostbite or the more serious "fescue foot." Cattle losing frozen hooves must often be put down.
Many other side effects occur. The toxin cuts weight gains, conception rates, and milk production.
Nationally, the toxic impact hits $1 billion in lost production.
Forage specialists urge replacing toxic fescue with a new variety of fescue that carries nontoxic endophyte. Plant breeders have inserted a natural, nontoxic novel endophyte to protect the plant. Fescue needs an endophyte to survive, but not the toxin.
Producers try to control the loss through management.
Roberts says management-intensive grazing allows cattle to tip seed stems before they set seed.
The toxic fungus concentrates in the seed to propagate the grass and the fungus into the next generation.
This year may require clipping seed heads with a mowing machine, as the seed stems develop early. Usually, tall fescue becomes most toxic around June 1, when seed matures.
To clip, set the mower blade high enough to cut the seeds but not clip the grass.
Another preventive is to move cattle from cool-season grass onto paddocks of warm-season grasses.
Information on grazing schools is available from regional MU Extension specialists.