The numbers speak for themselves: organic produce is a big deal.
 
According to Fresh Look Marketing's most recent data, as published in The Packer's Produce Market Guide, organic produce made up 7.5% of all retail produce sales in 2015, at a value of $4.54 billion. 
 
That's a 12.6% increase over the previous year. When I joined The Packer staff 17 years ago, we pegged year-to-year increases at an even 20% for several years running, but as sales continue to grow, a 13% growth rate is still very respectable.
 
In the past two weeks, two actions by the U.S. Department of Agriculture seek to boost the organic sector even more. 
 
First, the agency is considering adding a program that's been touted by the Organic Trade Association as a method to entice more growers to convert crops from conventional to organic. 
 
This week, The Packer reports on another USDA proposal that was backed by the OTA, a national research and promotion order for all organic products.
 
But as The Packer has reported, both of these programs don't seem to be gathering much interest around the industry.
 
For the transitional program, there's really no mystery for the ho-hum attitude.
 
The OTA in the past has said the mandatory transition phase - before acreage can be certified organic, it must be free of synthetic pesticides and other banned inputs for three years - is a tough hurdle for growers who want to market organic products.
 
It's true that growers must follow organic regulations for three years, without the promise of higher price tags at retail.
 
But that's just the necessary cost of making the business decision to enter a growing, dynamic market, one that the OTA continues to remind us that demand outstrips supply.
 
If the transitional program is approved, no product would be allowed to use the word "organic," and there's no label or process for letting consumers know about it.
 
This program doesn't hold promise for the produce industry anyway. It's most beneficial to the grain industry, where demand is really outpacing supply, according to Nate Lewis, farm policy director for the OTA. 
 
That makes sense, with everything from breakfast cereal to cattle feed for organic beef and dairy producers needing special ingredients.
Organic assessment?
A more complex issue, however is the push to establish an assessment for organic growers/handlers/processors, for everything from produce and other food to clothing and toiletries.
 
It's a fact that fresh produce makes up the greater share of organic food sales - 43% of the total, according to the U.S. Department of Agriculture - so why aren't fresh produce marketers jumping on the bandwagon?
 
There's no lack of support. The OTA lists about 1,400 individuals and the companies they represent on its site as supporters of the program, officially known as the Organic Research, Promotion, and Information Order.
 
There certainly are people from well-known companies in our industry listed: Tanimura & Antle, C.H. Robinson, the Master's Touch brand, McDaniel Fruit and Albert's Organics. And it's no surprise that companies that specialize in organics favor the program, including Awe Sum Organics, Viva Tierra Organic and Wholesum Organic.
 
What seems to be missing, however, is the support from the companies that will bear the brunt of the assessment: the larger-scale companies that sell conventional and organics. 
 
These companies, by and large, have expanded both the range of products, including value-added salad kits and more specialty items, and the overall volume of what's available at retail.
 
According to some sources contacted by The Packer, there are concerns that smaller growers will be exempt from paying into the organic program, much in the way produce companies have questioned the Food and Drug Administration's allowing smaller producers to be exempt from the Food Safety Modernization Act.
 
It's a fair concern. There's no way to tell, but how many of the 1,400 supporters fall into the exempt category?
 
Asking growers of a diverse range of commodities to find a common voice through a program that promotes not the commodity but what the USDA label stands for is a bit much. 
 
Readers of The Packer are no stranger to the lawsuits against commodity-specific assessments arguing First Amendment violations for paying into generic promotion programs. 
 
What's a Northwest cherry shipper going to think about his money promoting everything from organic wine to hemp drawstring pants? What images will be chosen for the campaign?
 
The produce industry has come a long way in advancing the demand for organic produce through innovation and expansion of acreage. 
 
Dozens of new products are released every year, and have been a mainstay of Produce Marketing Association and United Fresh Produce Association shows' expo floor booths for many years.
 
And last year the industry welcomed the Organic Produce Summit, scheduled for July in Monterey, Calif., this year, a testament to the strength of the organic sector. 
 
Many of the companies showcasing products no doubt rely heavily on their conventional produce brands, and have found there's no either/or when it comes to organic versus conventional.
 
It's interesting to note that only 30% of the sponsors on the Organic Produce Summit website have asked the OTA to include their companies as supporters of the proposed organic program.
 
If that's any indication, then thanks OTA and USDA, but we're doing fine on our own.
 
Chris Koger is The Packer's news editor. E-mail him at ckoger@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.