A recent freeze may cost Georgia three-quarters of its $400 million blueberry crop, agriculture officials there say.
"Conservatively, blueberries are where we have our greatest challenge," Georgia Agriculture Commissioner Gary Black said. "Conservatively right now, we pretty much believe 60% of the crop is gone - that's very conservative; that could grow to 70-75%."
It will be a while before officials can arrive at a more precise total, Black said.
"There's no doubt - it's a pretty devastating blow to our producers," he said.
Similar reports were coming out of south-central Georgia, the heart of the state's blueberry production.
"Within 50 miles of my area, you've got 60-70% of the blueberry crop in Georgia, and we've probably lost 70-90% of the crop, and some farms, 100% of the rabbiteye crop," said Allen Miles, owner of Miles Berry Farms in Baxley, Ga.
Temperatures in the area got as low as 21 degrees for three nights in a row in mid-March, he said.
"The forecast had called for 28 or 29 degrees, which would have been somewhat of a bite, but some farms, it got to 21," he said.
There's nothing left to do but get ready for next year, Miles said.
We're looking at trying to get our insurance companies to turn us loose and go ahead and call this variety 100% gone, and you can prune and do what you need to get a better crop next year and rejuvenate," Miles said. "But it's going to take a federal disaster (declaration) to get that going."
As of March 22, there was no word yet on federal assistance.
Other crops were somewhat more fortunate, Black said, noting that peach production could emerge 50-60% intact.
"It may be a bit better than we thought," he said. "The lagging information we have, with the lack of chill hours in middle Georgia that delayed the budding process, and now those buds are coming out and we'll have to wait and see what's there. Since they're so late, it may have preserved the crop. We may be a little better than 50%, from what some people are telling me."
As if Jaemor Farms in Alma, Ga., hadn't endured enough with a freeze that hammered its peach and strawberry crops, a hailstorm hit the operation March 21.
Some strawberries that came up, we're going to have to pick and throw off, but it wasn't as bad as it could have been," said Judah Echols, partner at Jaemor Farms.
The more immediate concern is the fallout from the mid-March freeze, in which the crops endured overnight lows in the low 20s, Echols said.
"We won't know definitely until the dead blooms fall off the trees, and that may take another week or two," he said. "And, later on, there's what they call a May drop, where any faulty peaches fall."
Echols said he still could retain as much as 50-60% of his peach crop, if no other freezes hit the area.
"If we don't get another late freeze, we'll do OK with that," he said. "Some early varieties were wiped out."
Miles said his strawberries may be a bit late, but they should make it to market.
"They just come back, so they'll be a couple of weeks late," he said.
Miles said his blackberry crop likely will be unaffected, barring any unforeseen bad turns in the weather.
Other crops appeared to be OK, Black said.
"The Vidalia crop came through in fine fashion, and we're very much looking forward to our April 12 opening," he said. "Frankly, it's the best crop I've seen in a long time. We're very encouraged with that."
Other vegetables had mixed results through the freeze, Black said.
"We've lost some bell pepper and other traditional vegetables, but we've got some that were damaged but appear to be capable of recovering," he said. "It's a little tough (figuring) a percentage of that at this point."
The freeze left some damage on Georgia's watermelon crop, although that damage likely will be relatively limited, Black said.
"I think the folks who are in the best position right now are those who have not set their plants, but there's definitely some damage in that area," he said.
Black said some fallout of the freeze may be found in the bee population.
"This interruption of blueberry is going to hit our honeybee producers," he said. "What kind of impact this is going to have on the honeybee population and eventually honey production is something to be concerned about. I know our honeybee producers are very much concerned."
It will be a while to learn the full extent of that damage, Black said.
"When we saw the damage (March 17) ... that was one thing, but our honeybee producers don't see the full picture of what's going to happen right away," he said. "It will be weeks before we know the full impact of that."
The freeze was felt elsewhere, too. North Carolina reported widespread damage to early varieties of its peach crop.
"A lot of early varieties, there's not a lot on the trees," said Brad Thompson, an extension agent with North Carolina State University, who was out surveying the damage left by temperatures in the low 20s. "Maybe 30% of the crop is still on the tree. It depends on whether you had any kind of frost protection."
Growers who didn't have any safeguards in place may be left with only 5-7% of their peach crop, Thompson said.
The blueberry crop in North Carolina was damaged, as well, but "strawberries look they're in pretty good shape," Thompson said.
"We're going to be short, but we'll take what we got," Thompson said.