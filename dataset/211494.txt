Colombian police have seized more than 7 tons of cocaine — worth about $200 million  — from a banana farm belonging to the Gulf Clan. 
Authorities arrested four men and one woman, and agents exchanged fire with members of the gang during the raid, according to a report from www.mercadomilitar.com.
 
“The shipment was being stored for subsequent transportation to Central America, with final destination of the United States, camouflaged in legal shipments of bananas and fruits,” Colombian Defense Minister Luis Carlos Villegas said in the report.
 
Colombian authorities are investigating who owns the property.
 
The Gulf Clan was born from demobilized paramilitary groups in 2006 and has about 1,800 members, according to the report.