The Port of Oakland (Calif.) has a new five-year strategic plan that will serve as a blueprint for expansion.
The plan, dubbed “Growth with Care,” outlines projections for record business volumes for aviation and maritime businesses, capital investments for major projects and an emphasis on sustainability.
“We can grow, but we want our neighbors to grow with us,” Port of Oakland Executive Director Chris Lytle said in a news release about the 21-page document.
Cargo volume should reach 2.6 million 20-foot-equivalent containers (TEUs) by 2022, according to the plan, an increase of 8%.
Two projects will help with that increase, according to the release:

Cool Port Oakland, a 283,000-square-foot refrigerated distribution center that is set to open this summer, and
A 440,000-square-foot distribution center planned at the nearby Seaport Logistics Complex.

Curbing diesel emissions is also a part of the strategic plan, according to the release, and truck emissions at the port have been cut 98% since 2009, and vessel emissions have declined 76%.