New rule bans vessels from releasing sewage into Puget Sound
New rule bans vessels from releasing sewage into Puget Sound

The Associated Press

SEATTLE




SEATTLE (AP) — Recreational and commercial vessels will not be able to release treated or untreated sewage into Puget Sound waters under new rules approved by the state.
The Department of Ecology on Monday officially designated a new "no discharge zone" in Puget Sound to improve water quality and protect shellfish beds and swimming beaches from harmful bacteria.
Under rules that begin May 10, boats will not be allowed to pump sewage, whether treated or not, into waters in an area that extends from near Sequim to south Puget Sound to the Canadian border. Lake Washington is included. Boats would need to use a pump-out station or wait until they are out of the zone.
There are dozens of such zones across the country, but this is the first in Washington state.
Critics have said the rule is too broad and would being costly for many who would have to retrofit vessels to accommodate waste holding tanks.