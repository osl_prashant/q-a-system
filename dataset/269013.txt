Bovines online: Farmers are using AI to help monitor cows
Bovines online: Farmers are using AI to help monitor cows

By RYAN NAKASHIMAAP Technology Writer
The Associated Press

SAN FRANCISCO




SAN FRANCISCO (AP) — Is the world ready for cows armed with artificial intelligence?
No time to ruminate on that because the moment has arrived, thanks to a Dutch company that has married two technologies — motion sensors and AI — with the aim of bringing the barnyard into the 21st century.
The company, Connecterra, has brought its IDA system , or "The Intelligent Dairy Farmer's Assistant," to the United States after having piloted it in Europe for several years.
IDA uses a motion-sensing device attached to a cow's neck to transmit its movements to a program driven by AI. The sensor data, when aligned repeatedly with real-world behavior, eventually allows IDA to tell from data alone when a cow is chewing cud, lying down, walking, drinking or eating.
Those indicators can predict whether a particular cow is ill, has become less productive, or is ready to breed — alerting the farmer to changes in behavior that might otherwise be easily missed.
"It would just be impossible for us to keep up with every animal on an individual basis," says Richard Watson, one of the first four U.S. farmers to use IDA since it launched commercially in December.
Watson, who owns the Seven Oaks Dairy in Waynesboro, Georgia, says having a computer identify which cows in his 2,000-head herd need attention could help improve farm productivity as much as 10 percent, which would mean hundreds of thousands of dollars to his family.
"If we can prove out that these advantages exist from using this technology ... I think adoption of IDA across a broad range of farming systems, particularly large farming systems, would be a no-brainer," Watson says.
Dairy farming is just one industry benefiting from AI, which is being applied in fields as diverse as journalism, manufacturing and self-driving cars. In agriculture, AI is being developed to estimate crop health using drone footage and parse out weed killer between rows of cotton.
Yasir Khokhar, the former Microsoft employee who is the founder and CEO of Connecterra, said the inspiration for the idea came after living on a dairy farm south of Amsterdam.
"It turns out the technology farmers use is really outdated in many respects," he says. "What does exist is very cumbersome to use, yet agriculture is one of those areas that desperately needs technology."
Underlying IDA is Google's open-source TensorFlow programming framework, which has helped spread AI to many disciplines. It's a language built on top of the commonly used Python code that helps connect data from text, images, audio or sensors to neural networks — the algorithms that help computers learn. The language has been downloaded millions of times and has about 1,400 people contributing code, only 400 of whom work at Google, according to product manager Sandeep Gupta.
He says TensorFlow can be used by people with only high-school level math and some programming skills.
"We're continuing this journey making it easier and easier to use," Gupta says.
TensorFlow has been used to do everything from helping NASA scientists find planets using the Kepler telescope, to assisting a tribe in the Amazon detect the sounds of illegal deforestation, according to Google spokesman Justin Burr.
Google hopes users adapt the open-source code to discover new applications that the company could someday use in its own business.
Even without AI, sensors are helping farmers keep tabs on their herds.
Mary Mackinson Faber, a fifth-generation farmer at the Mackinson Dairy Farm near Pontiac, Illinois, says a device attached to a cow's tail developed by Irish company Moocall sends her a text when a cow is ready to give birth, so she can be there to make sure nothing goes wrong. Moocall doesn't use AI — it simply sends a text when a certain threshold of spinal contractions in the tail are exceeded.
While she calls it a "great tool," she says it takes human intuition to do what's right for their animals.
"There are certain tasks that it can help with, and it can assist us, but I don't think it will ever replace the human."
___
AP videographers Marina Hutchinson in Waynesboro, Georgia, Teresa Crawford in Pontiac, Illinois, and Carrie Antlfinger in Milwaukee contributed to this report.