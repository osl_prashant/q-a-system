Thrip pressure on Washington onions was unusually high in July thanks to hot weather.
Tim Waters, associate professor for Washington State University Extension, Franklin and Benton Counties, said he had received a higher than typical number of calls in July on thrips management.
Nymph and adult onion thrips cause damage to plants by puncturing individual leaf cells and sucking up the sap, according to a Washington State University Extension factsheet that Waters cowrote.
Because thrips mainly feed within the sheaths of newly emerging onion leaves, detection can be difficult, and large populations of onion thrips can cause smaller onion bulbs and reduced yields, according to the factsheet.
Waters said he’s also seen more iris yellow spot virus this season in seed crops, which onion thrips are a vector for.
The virus causes iris yellow spot disease.
The disease was first reported in Washington onions in 2004, and has since caused significant losses to onion seed and bulb crops.
“When onions get the virus, they don’t grow correctly and the tops die off prematurely,” Waters said.
“Once a farm gets the virus, it needs to manage thrips to reduce spread within a field.”
Thrips can feed on onions in storage and cause scars that reduce the quality of bulbs, particularly on red onion cultivars.
Waters said he thinks one of the reasons there has been a slower increase in the planting of organic onions versus other organic commodities is thrips control, as there is only one organic insecticide available, and that can only be used five to eight times before thrips become less affected by it.
“You probably lose 20% of your yield from thrips damage on an average organic onion crop,” he said.