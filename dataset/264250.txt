Catholic Charities, Purdue extension harvest first crop
Catholic Charities, Purdue extension harvest first crop

By MARK FITTON(Terre Haute) Tribune-Star
The Associated Press

TERRE HAUTE, Ind.




TERRE HAUTE, Ind. (AP) — Catholic Charities and Purdue Extension want people to eat their vegetables.
To that end, volunteers and employees on Thursday harvested their first crop of lettuce and kale from the Catholic Charities greenhouse in the 1400 block of Locust Street in Terre Haute.
The harvested produce then went to Loaves and Fishes Soup Kitchen in the lower level of Ryves Youth Center, where it will be prepared and served as part of the meal.
The crop, about 80 to 90 bibb lettuce and red leaf lettuce plants — plus some kale — doesn't equate to self-sufficiency for Catholic Charities in Terre Haute.
But that's OK — the goals are as much about community involvement and education as they are about growing your own food.
The entire story of the greenhouse project is one of the people coming together.
The greenhouse came about when Jim Speer at Indiana State University in 2014 helped Catholic Charities obtain funding from the Lilly Foundation to build a passive solar greenhouse on Catholic Charities'-managed property in the Ryves Neighborhood.
Community partners gathered around project leaders including Allison Finzel of Purdue Extension, John Etling of Catholic Charities and Purdue Extension employee and master gardener Patti Weaver.
Those volunteers included John Rosene and students from Rose-Hulman Institute of Technology, Sister Betty Hopf of the Hux Cancer Center and students from Ivy Tech. Local businesses donated materials, as well.
By late 2017, the greenhouse was ready for its first crop. Weaver, the gardening expert, said the growing went surprisingly well, given they lost some of the lettuce plants to the stress of cold winter days.
The produce harvested Thursday is "not a lot, but it's a start," Weaver said
Now, she said, the greenhouse will turn out some more produce before the end of May, when it likely will get too warm for efficient production. It can and will, however, go back into production in September.
And the partners expect another harvest: education.
"This, I think, will be more of a learning greenhouse as much as anything," said Etling.
It can be used to train volunteers, to teach kids and to be an inspiration to families, he said.
"It plants the seed, if you will," Etling said. "On a small scale, maybe we'll spread throughout this neighborhood, anyway, or maybe just this block to start."
He said he hope the example and lessons of the little greenhouse will inspire backyard gardens for fresh vegetables or even just windowsill gardens to provide some herbs and spices for cooking.
Catholic Charities feeds more people than its greenhouse can serve — as many as 37,000 meals each year through the Soup Kitchen alone. And its Foodbank provides the equivalent of 2.5 million meals each year to 32,000 people who are hungry in west central Indiana.
While Catholic Charities is certainly in the business of feeding people, the greenhouse is less about mass production than a triggering a paradigm shift, Etling said.
Fresh produce, he said, has many health benefits — lowering blood pressure, reducing risk of heart disease and stroke, preventing some types of cancer, lowering the risk of eye and digestive problems, and having a positive effect upon blood sugar.
If Catholic Charities and its little greenhouse can help point the way toward better eating, then it's also helping bring about a healthier community.
___
Source: (Terre Haute) Tribune-Star
___
Information from: Tribune-Star, http://www.tribstar.com


This is an Indiana Exchange story shared by the (Terre Haute) Tribune-Star.