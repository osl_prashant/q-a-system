While in Washington state for cherry coverage in late April, I asked a few marketers their thoughts about online grocery sales and how that business is changing the market they sell into.
 
While cherries may not be the perfect commodity for online ordering and home delivery, Northwest marketers aren’t underestimating the potential impact that retailers like Amazon could have on the tree fruit industry.
 
B.J. Thurlby, president of Northwest Cherry Growers, said the group has done promotions with Chinese online retailer Tmall.com, which boasts 380 milion users. Those promotions have yielded sales of 50 tons of Northwest cherries three years in a row, he said.
 
In the U.S., Amazon has yet to really wade into cherries very deeply, Thurlby said, while the brick and mortar strength of Walmart sells 5% of the cherry crop.
 
Amazon has made people account for home delivery and online grocery purchases, said Mike Preacher, director of marketing and customer relations for Domex Superfresh Growers, Yakima, Wash. “All major retailers are exploring home delivery and some like Walmart will pick fruit out of stores,” he said, noting a pilot store in Bentonville selects produce from store shelves and puts the produce in their customers’ car trunks. “You see every version of everything.”
 
The United Fresh Produce Association FreshFacts 2016 Year in Review reports that nearly one third of households have purchased groceries online in the past six months, including 16% of households buying fresh produce online.
 
“We talk about being omnichannel all the time,” Preacher said. “To be the best produce supplier you have to be integrated with all that and we are.” Preacher said selling to online grocery outfits requires the ability to assemble small batch orders and mix fruit.
 
In terms of packaging, Preacher said packaging will be needed to withstand more stops along the way and deliver on the perception of being a premium product.
 
Store layouts may change when grocers build stores to fulfill orders for home delivery or curbside pickup.
 
“Ten years ago, some in the industry were saying online sales were a short lived fad and not going to be able to compete against brick and mortar,’ said Randy Abhold, vice president of sales and marketing for Rainier Fruit Co., Selah, Wash. “Most would say today that the (prediction) missed it and we should have put more energy in this earlier.”
 
Still, Abhold said it is very challenging to look at something as perishable as cherries and say how that would fit this model. 
 
“We would be ill-advised to not pay attention to mistakes in the past,” he said. “Somebody will figure it out somehow, maybe not today, maybe not tomorrow, but it will be figured out.”
 
Bob Mast, president of Wenathcee-based CMI Orchards said the company is paying attention to the rise of online grocers and works with them on apples and pears.
 
At the same time, cherries - shipping during the hottest month of the year - may not be the best option for online grocers with questions in their distribution system, he said.
 
“It is not the best focus for our fruit because we have a responsibility to take our growers fruit and give customers the best eating experience we can,” he said.
 
Online grocery is a trend that is certainly coming, said George Harter, vice president of marketing for CMI Orchards. “Whether it works or not is yet to be seen,” he said. 
 
Developments like a “click list” of grocery and produce items that can be picked up at designated stores by customers is growing, noting the success of Kroger with that concept.
 
Amazon will have to deal with the reality of shrink in perishables and Harter said that will create a learning curve for them in their ambitions.
 
Mast said the center store supermarket departments are feeling the impact of online grocers, but there is still work to be done for online grocers to deliver the best consumer experience for fresh departments.
 
If online grocers can deliver both quality and convenience, they can win, marketers believe.
 
“Initially, the hesitation from consumers is ‘I want to pick out my fresh produce’ but when you have somebody that can pick out their best quality product, that is going to start gaining the confidence of consumers,” Mast said.
 
Harter said the young consumers with children and older shoppers may find the most appeal with shopping online for a click list. 
 
While retailers could lose impulse sales, evidence points to high customer satisfaction and a larger basket size for online customers, he said.
 
“There is no impulse (purchases) but (online shopping) is so much more convenient and that may be a bigger power play than the limited amount of time you are going to be in the store,” Harter said. 
 
Mast observed that Amazon is a master of suggesting to their customers that “people who bought this also bought these items.” 
 
With the retail investments being made and consumer acceptance growing, greater online fresh produce sales are bound to happen in coming decades - but perhaps not so much with cherries this year.