Welcome to the redesigned Drovers.com. This week Farm Journal Media launched redesigned sites for many of our brands, all in an effort to make them more reader friendly. We’ve also added new functions to provide you more of the information you need.
 
Most of the changes aren’t dramatic, which helps retain the consistency and comfort level for users while making information more accessible. You should find it easier to search the site, find back issues of Drovers magazine and provide feedback.
 
Simply put, the redesign give our site a cleaner look, with improved functionality. We’ve also added easy-to-find entry points for resource centers, topic-focused news and news from our sister sites such as Dairy Herd Management, Farm Journal's PORK, Bovine Veterinarian, Top Producer and AgWeb.
 
You’ll also find easier access to markets and weather information, plus easier to find and read back issues of our digital magazines. Additionally, our new site makes it easier for you to comment on stories and deliver feedback on the issues of the day.
 
We hope you find the new design useful. If you have comments or suggestions, please let us know.