Making feeding programs economical is always a concern for feedlot operations and a major goal of our program. One of the feedstuffs we’ve been researching for a long time is corn silage. Of course, corn silage is not a new feed ingredient. It’s been around for thousands of years, but today we have new approaches. Corn silage becomes more economical when grain is expensive, and it also can interact well with distiller’s grains. Even for growing programs that use silage, we have learned that protein from distillers is a great fit as silage. Silage is much lower in bypass protein than we thought, while distillers provides bypass protein.


Cattle become less efficient as corn silage is increased above traditional inclusions of 15 percent. However, if silage is accurately priced for the comparison of economics between corn grain and corn silage, it is still quite economical. The best fit is for operations that raise their own corn, recycle manure back to those acres and do a great job managing shrink losses.


Other research shows brown midrib (BMR) corn silage can improve average daily gain (ADG) or feed conversion or even both. In addition, kernel processing has shown a numerical improvement in feed conversion in a low energy diet. Both BMR and kernel processing are interesting results that warrant additional research in beef cattle, which is coming soon.


Dry matter (DM) of the silage as dictated by harvest time can also play a role in economics and feed efficiency. Research on finishing rations shows that putting up drier silage did not have an impact on performance. In growing studies where the cattle were fed 88 percent silage, feed conversion was a little worse. Targeting DM in upper 30s or moistures in upper 60s is a better approach than getting too wet or too dry, so be patient getting silage harvest started. One challenge is accurately predicting moisture/DM of silage standing in the field.


Silage inclusion can affect performance, but it can still be profitable. In almost all our growing and finishing diets, feeding distiller’s grains — or other economical sources of bypass proteins — complemented beef cattle rations.


To hear Dr. Erickson’s full presentation, visit http://beef.unl.edu/silage-beef-cattle-conference to watch the video, listen to a summary or read the 2016 Husker Corn Silage Conference proceedings. The conference was sponsored by Lallemand Animal Nutrition, the University of Nebraska Extension and the Iowa Beef Center.