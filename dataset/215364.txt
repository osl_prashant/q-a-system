Mastronardi Produce, through its Sunset brand, is releasing four “You Make Me Pasta” value-added meal kits at retailers nationwide.
The Kingsville, Ontario, greenhouse grower’s kits include fresh tomatoes, noodles, spices and infused olive oil. Sunset’s culinary director, Roger Mooking, developed the pasta kits, according to a news release.
The line is an extension of the company’s Minzano pasta kits introduced in March.
“At Sunset, we identify with busy customers who don’t always have time to prepare home-cooked meals from scratch using our fresh produce,” CEO Paul Mastronardi said in the release. “Ready in minutes with fresh tomatoes included, our ‘You Make Me’ pasta kits deliver ‘grab and go’ convenience without sacrificing flavor or compromising on nutrition.”
The four kits are:

Spicy Arrabbiata (You Make Me Hot)
Classic Italian (You Make Me Fresh)
Creamy Parmesan (You Make Me Blush)
Cold Pasta Salad (You Make Me Chill)

The kits make dinner for two, or enough for four side dishes, and can cook in 15 minutes, according to the release.
Looking for more on Sunset's new pasta kits? Check out Pamela's Kitchen for her experience with the kits.