China’s Ministry of Agriculture reported on Monday a case of foot and mouth disease in a pig breeding farm in southern Guangdong province.
The O-type strain of the disease was found in 30 pigs on a farm in Huizhou city last week, the ministry said, leading to the cull of 71 pigs to bring the disease under control.
Another case of the disease was found in Guangdong earlier this year.
China is the world’s top producer and consumer of pork.