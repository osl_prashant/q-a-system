After more than 3 million soybean acres were damaged in 2017, the Environmental Protection Agency (EPA) reached an agreement with dicamba manufacturers and licensees to change product labels to minimize the potential for off-target damage. The label changes take effect in 2018.
“Actions are the result of intensive, collaborative efforts, working side by side with the states and university scientists from across the nation who have first-hand knowledge of the problem and workable solutions,” says EPA administrator, Scott Pruitt.
BASF, DuPont and Monsanto have voluntarily agreed to the following label changes for over-the-top dicamba use next year:

Products are restricted use so only certified applicators can apply the products, and they must have dicamba-specific training.
Farmers must maintain records regarding use of dicamba products.
Dicamba may only be applied when wind speed is below 10 mph (formerly 15 mph).
Direction on the time of day dicamba can be applied (specifics are not yet available).
Add tank clean-out language to prevent cross contamination.
Increase applicator risk awareness for nearby sensitive crops, with enhanced susceptible crop language and record keeping.

“DuPont appreciates how the industry, university experts and the government have worked together to establish these new EPA label guidelines,” says Laura Svec, U.S. communications lead for DuPont Crop Protection. “DuPont also remains committed to helping ensure appropriate stewardship of FeXapan plus VaporGrip Technology.”
Changes to the label could help keep dicamba in farmer hands. “We’re pleased that our XtendiMax with VaporGrip technology will be available to growers throughout 2018,” says Scott Partridge, Monsanto vice president of global strategy. “We’re pleased with the restricted use label because it addresses off-target issues.”
Restricted use requires more application education and record keeping.
“We know that contributors to off-target movement in 2017 included factors such as physical drift and applications made during temperature inversions,” BASF says in a prepared statement. “When the label is followed, these updates will help address these concerns.”
While some view the label changes as a “step in the right direction,” they wonder if the changes address every issue. “You can’t argue against the label changes,” says Bob Hartzler, Iowa State University weed science professor. “But the biggest concern among most academics is the changes don’t address volatility.”
Next year Hartzler hopes to see fewer acres of damage, but says it won’t be comparing apples to apples. Monsanto expects Roundup Ready 2 Xtend will be on half of all planted soybean acres in 2018—compared to just 25 million acres this past season.
“You’ll have fewer sensitive varieties planted so that leads to the potential for a decrease in damaged soybeans, and I hope applicators will pay more attention to what is adjacent to their field,” Hartzler says. “It’s easy to focus on the soybean damage, but we need to be concerned about other plants in the landscape—ag could make enemies quickly if we impact plants outside of ag.
“EPA set this under a two-year label, and if we have a similar magnitude of problems in 2018 as we did this year I don’t see how EPA could renew the label in its current state,” he adds.