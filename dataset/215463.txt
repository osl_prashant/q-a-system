By: Randy Saner, Nebraska Extension Educator, and Matt Stockton, UNL Farm and Ranch Management Economist 
What is a respectable value to pay for a beef replacement heifer for the coming 2018 production season? Like many decisions, this can be complicated by many factors, but none the less important to have a handle on to make the best production and business choices for continued success of the ranching operation. For this reason, the University of Nebraska Beef Economics team have made some preliminary forecast of heifer values using publically available projected price and costs scenarios. 

For further information or a more in-depth explanation, the authors refer you to the whole document or welcome any questions or comments.

When purchasing replacement heifers producers face many factors that should be considered. While all of these factors contribute to selecting, either by retention or by purchase, the appropriate brood cow replacements. However of all of these factors two stand out and are most impactful on profitability and are used as the primary focus and drivers of the model used to generate the predicted results of this work. These two factors are: 

Current and future expected difference between costs and revenues for producing cows (Both external and internal forces, i.e. markets and costs).
The heifer’s ability to stay in the herd as a productive cow (longevity).

Since it is difficult to anticipate and quantify (capture the true costs and revenues) all the possible conditions, types and choices that producers might make; three general costs structures (low, medium and high) are combined with three different replacement rates measured as average annual cull rates. The costs were measured as cost per cow per year ($/hd/yr) and were set at averages near $630, $730 and $830/hd/yr respectively. The replacement rates were set to average near 28% for the high rate, 20% for the middle rate and 14% for the low rate. These replacement rates are designated as aged, middle and young cow herd types. When combined, a total of 9 different outcomes were predicted, i.e. low cost production with an aged cow herd, high cost production with a young cow herd, etc. 

Results will be reported in two different ways: 1) As the actual forecast by scenario type and 2) As marginal changes. 

The 2017-2018 Forecasts

Predicted, Forecast 2017-2018 Heifer Replacement Values by Scenario


The scenario where replacement rates and costs are lowest has the highest available replacement value, $1664.15/hd, whereas the highest replacement rate with the highest cost structure has the least available replacement breakeven value of $627.24/hd, more than $1000/hd less.

Marginal Results

In the case of costs a $1 reduction in annual production costs for the young herd from the highest to lowest costs results in an average $2.75/hd/yr increase in what can be paid in replacement cost while maintaining a breakeven value. In the instances of the middle and aged herd this amount increases to $3.52/hd/yr and $4.23/hd/yr respectively. Lower rates of replacements result in increased buying power for replacements while the reverse reduces that power.

For every 1 month, increase in average herd age of the young herd from the highest cost to the lowest cost structure there is a $21.36/hd/yr average increase in heifer replacement breakeven value. This amount drops to $14.84/hd/yr for the middle herd and $8.00/hd/yr for the aged cows for every 1 month increase in herd‘s average age. Increasing cow longevity increases replacement buying power and vice versa. The younger the average age of the herd the more effect a one month increase in that average has on increasing dollars available to purchase replacements.