U.S. livestock futures ended lower on Thursday in technically-driven selling, with live cattle falling over 1 percent on fund-driven profit-taking, traders said.
Declines in wholesale beef prices added to bearish cattle sentiment. Choice-grade wholesale beef prices fell $1.91 to $204.64 per cwt, a factor that helped drive the most-active CME February live cattle contract down to a 1-1/2-week low at 123.600 cents per pound.
February live cattle ended down 1.625 cents at 124.975 cents per pound and December cattle fell 0.575 cent at 119.900 cents.
CME January feeder cattle were down 1.400 cents to 154.175 cents per pound.
Some traders think wholesale beef prices have topped out in the short term, suggesting retailers were winding down purchases of higher-end cuts that sell well during the year-end holidays.
But cattle futures pared losses after higher trades in U.S. Plains cash cattle markets. Fed cattle traded in Texas and Kansas at $121 per cwt, about $3 higher than last week.
"Early on in the day, it was month-end profit-taking and a lack of news ... It was algorithmic trading, without any real substance or reason," a cattle futures trader said, adding: "When the (cash) bids started to pop up, the futures started to come back."
Hog futures fell for a second straight day, despite higher cash prices in the top market of Iowa and southern Minnesota.
Some traders noted follow-through technical selling after Wednesday's weak close, in which February lean hogs ended lower after climbing to a three-week high.
Commodity funds hold a net long position in lean hog, live cattle and feeder cattle futures, leaving the markets vulnerable to bouts of long liquidation.
Most-active CME February hogs settled down 1.125 cents at 69.775 cents per pound while December hogs were down 0.800 cent to 64.275 cents.