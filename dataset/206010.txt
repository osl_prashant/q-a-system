Consumer perceptions of beef quality involve what you produce, and also how you produce it. Conducting a new National Beef Quality Audit (NBQA) every five years achieves a few distinct goals. First, it allows the industry to track results in addressing quality concerns identified in prior audits, and the reports have documented significant progress. Each audit also tracks the ongoing evolution of public perceptions regarding beef quality. 
In the early audits, physical defects dominated the challenges identified (see graphic here). Some of those challenges persist, but the most recent audits show, as beef becomes more consistent, consumers have shifted some attention toward more “cultural” concerns, such as “how and where cattle were raised” which ranks in the top-five concerns in the 2016 NBQA.
Texas A&M Extension livestock specialist Ron Gill, conducts training sessions on low-stress animal handling and other aspects of husbandry across the country. This fall, he is part of several Stockmanship and Stewardship workshops, hosted by the Beef Checkoff with sponsorship from Boehringer Ingelheim. 
 
The human element is critical.
Since the initiation of the BQA program and the national audits, animal handling has become a key component in the overall beef-quality conversation, Gill says. More people recognize the relationships between stress, immunity, antibiotic use, carcass traits and human safety. 
“We’ve also come to realize,” Gill says, “there are multiple ways to achieve stockmanship goals, depending on an operation’s facilities, management practices and, especially, people.” 
When Colorado State University scientist Temple Grandin, first introduced new designs for processing facilities, there was a perception that we could “engineer our way out of problems.” Since then, scientists and producers have realized that even the best facilities are only as good as the people moving the cattle. Crews need training, retraining and constant reinforcement to ensure the use of low-stress handling.
Changing behavior generally takes more time than changing hardware, Gill says, but the industry is coming along. 
Injection-site lesions, one of the most troublesome problems revealed in the initial audit, did not even make an appearance in the 2016 version, demonstrating the industry can change and improve, once made aware of a problem.
Colorado State University animal scientist Jason Ahola, agrees animal-handling issues come with their own challenges. Faced with excessive carcass bruising or injection-site lesions, industry stakeholders recognize the BQA implications. With animal handling and stress reduction, the relationships, while real, are less direct and more difficult to measure. Ahola says early research from Temple Grandin and others showed relationships between animal temperament, performance and carcass quality. Genetic selection for calm temperament can help, but good handling can benefit health and performance even in flighty cattle.
Gill says the industry did well in initiating efforts to improve animal handling years ahead of demands from consumers or retailers. Packers got on board early and the national BQA program has placed significant emphasis on animal handing for more than a decade. 
“We can still improve, Gill says, but mostly we need to do better at telling our story.”

Concerns over antibiotic use in livestock have reinforced the industry’s emphasis on stockmanship.
With consumer pressures and legislative policy favoring reductions in antibiotic use, producers and veterinarians need to apply management practices to build immunity and optimize performance. Numerous studies show physical and psychological stress can negatively affect immunity and vaccine response in cattle, and low-stress handling has emerged as a key tool for preventing disease and protecting performance. Animal-health companies, in efforts to protect the efficacy of their products, have embraced and supported those efforts. 
Veterinarians, organizations such as the American Association of Bovine Practitioners and veterinary schools have recognized the opportunity to become more involved in training crews, modifying facilities and building a culture of animal husbandry in client operations as a means of improving health and performance. 
Ahola adds that production efficiency has become increasingly important, with high input costs, high cattle values and tight margins. Investments in crew training and facilities that reduce labor costs and improve cattle performance can pay off over time. Also, operations need to emphasize human safety. While not a direct component of BQA, efforts to protect human safety must be in place before an operation can implement other BQA principles.  

Read other stories from the Beef's Quality Revolution series: 

A Generation of Quality Gains
25 Years of Beef's Quality Challenges
Meat, Millennials, Meal Kits
Consumers Shift Attention to Cultural Concerns
Adapt Facilities, Equipment to Your Cattle
Resources to Improve Your Operation
A Call for Transparency (commentary)