Norway-based Tomra notched record revenues in 2016 and enters its 45th year in business in expansion mode. 
The company, a maker of sensor-based solutions for sorting food and materials collection equipment, posted record revenues of more than $750 million in 2016, according to a news release.
 
The firm, founded in 1972, now has 90,000 systems installed in over 80 markets worldwide, according to the release.
 
“Our growth and longevity is due in part to the growing understanding that the world needs to utilize its natural resources in a better way to ensure sustainability,” Stefan Ranstrand, Tomra president and CEO, said in the release. “We are proud of the success our company has achieved thus far, and look forward to building on this in the years ahead as we continue to develop cutting-edge solutions for helping our customers meet their business needs and contribute to a better and more sustainable future.”