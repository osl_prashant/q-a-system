Mission Produce co-hosted an avocado industry event in China.
Oxnard, Calif.-based Mission and its Chinese distributor partner, Lantao International, organized the inaugural Chinese Avocado Congress, July 28 in Guangzhou, China, according to a news release.
More than 200 wholesalers, distributors and other industry members were invited to the event, which included sessions led by Mission and Lantao officials.

In his speech at the meeting, John Wang, Lantao's CEO, announced that Mission will build China's first avocado ripening center in Shanghai.
The facility will have four ripening rooms, which will allow Mission and Lantao's distribution network to reach 85% of mainland Chinese consumers with ripened fruit.
Mission officials who spoke at the congress included Keith Barnard, director of global grower relations; Ben Barnard, director of North American operations; and Tommy Padilla, Asia export sales manager.
More than 70% of all avocados in the Chinese market are distributed by Lantao, which began importing fruit from Mission in 2013.