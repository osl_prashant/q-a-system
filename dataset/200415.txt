Lameness is a major animal welfare issue afflicting dairies across the country and around the world.
Limping cattle are a concern for the public anytime they visit a farm or see videos where cattle are visibly lame, says Marcia Endres, a dairy specialist at the University of Minnesota. It's not good for cattle, and it isn't good for the perception of our industry, Endres says.

She acknowledges lameness will never fully be eliminated from dairy herds, but producers need to be proactive in reducing the incidence. Research studies in both North America and Europe indicate there is a 21% to 55% prevalence of lameness on dairy farms. All these studies used locomotion scoring and the most severe cases occurred 5% to 15% of the time.

Of the cases researchers would see with locomotion scoring on an entire herd, dairy farmers only identify about 25%. This is probably because dairy producers are only looking for those severe cases where there is head bobbing or an animal holding their leg off the ground.

"There is a need for producers to see those cows that are mildly lame, so they can be found and treated," Endres says. In doing so it will help reduce the cases of severe lameness and get cows back in the parlor.

Economic downfalls follow lameness with costs varying from culling, lost production, treatment and reproduction. The estimated cost of lameness ranges from $300 to $500 per case with severely lame cows.
Reducing prevalence
A good first step dairy farms can take to lower the occurrence of lameness is to get cows off their feet.

When cows are hot, they will stand more to dissipate heat. Endres says it is important to have heat abatement such as fans and ventilation where cows are encouraged to lie down.

Many facilities have concrete throughout the areas where cattle move to water, feed or be milked. It's important the flooring is not slippery or coarse enough to cause lesions. Sharp turns in facilities should also be avoided.

"Rubber flooring may be an option in holding pens, transfer lanes or parlors to provide a more cushioned surface for the animal," Endres says.

However, when using rubber flooring, the stalls or bedding should still be comfortable enough the cows will lie down. Otherwise they will stand on the rubber flooring making the effort counterproductive.

"We want to first of all make sure our stalls are well designed before putting in that rubber flooring," Endres adds. There should be enough room in stalls for cows to lunge forward and ample bedding so cows can sink their feet in when standing up. Mattresses can be a problem because there might not be deep enough cushion when compared to sand, recycled manure solids or compost bedded packs.

Proper stocking rates are vital, too. Overstocking limits the benefits of having proper stalls and bedding. Ideally, there should be enough stall space for all cows to lie down or no more than 120% stall stocking density.

Milking is also a focus area. When cows are standing in the holding pen or parlor, it takes away valuable rest time.

Endres recommends cows should be resting 10 to 12 hours per day because the risk factor for lameness goes up when cows are spending four hours or more away from the pen.

Implementing low stress handling is vital for reducing lameness. As cows are moved faster by handlers there is a higher likelihood of an animal slipping on concrete or running into facilities.

"Proper cow handling doesn't cost us anything and it is the right thing to do," Endres says.
Footbaths are essential
Footbaths are important to limiting foot lesions like digital dermatitis. However, footbaths in cold environments can be difficult, especially in winter months.

Endres recommends footbaths be at least 10 feet to 12 feet long so cows will get more than one immersion per hoof. Chemicals used most commonly in North American footbaths are copper sulfate and formaldehyde. In the U.S., a 5% to 10% mixture of copper sulfate tends to be used.

Formaldehyde is more often handled in Mexico with 80% of farms dipping with it because it is less expensive and more effective. Unfortunately, formaldehyde causes skin irritation to both humans and cattle.

Hoof trimming can help identify lameness issues more effectively, but it occasionally creates problems if hoof trimmers use incorrect techniques. It's important hoof trimmers, and employees who trim hooves, are properly trained, Endres says.

In the future Endres believes there is the potential to develop technology for early detection of lameness. Going forward, dairies should continue focusing on genetics to ensure cows have good feet and legs to support themselves, but producers should still focus on improving the cows' environment because there is no such thing as perfect housing. 

Note: This story appears in the January 2017 issue of Dairy Herd Management.