Here’s a quote to consider:“You may not realize it when it happens, but a kick in the teeth may be the best thing in the world for you.”
You know who said that?
No, not Chuck Norris.
Hint: He was the founder of a business empire, and many years later, one of that empire’s entities just endured that proverbial kick in the teeth.
The business titan quoted above was Walt Disney, and the company that now bears his name has become a global entertainment conglomerate that posted nearly $10 billion in net income last year. Along with a slew of properties (Pixar, Marvel Entertainment, Lucasfilm, ESPN), the Burbank, Calif.-based mega-firm owns the Disney-ABC Television Group, which operates the ABC television network, which produces the news show “20/20,” which ran a story five years ago that explored the use of the lean, finely textured beef product infamously dubbed “pink slime.”
And the kick in the teeth was the announcement this week that ABC and South Dakota-based Beef Products Inc. reached a settlement in a lawsuit BPI filed against the network over its reporting on the pink slime segment.
As is true in almost all civil lawsuits, news sources reported that “the terms of the settlement are confidential.” That, of course, is step one in the agreement both parties must reach to dismiss the court case.
The first rule of Lawsuit Club is that you don’t talk about lawsuit money.
The second rule is that both parties must declare themselves vindicated, generally with high-minded statements we’re supposed to swallow as an antidote to the months of bitter, highly charged name-calling that preceded the trial, and depending how far along these legal proceedings go, are often part of the public record.
BPI’s original filing argued that ABC’s coverage “misled consumers into believing that the product was unsafe, was not beef and was not nutritious.”
But following the settlement, Julie Townsend, an ABC spokesperson, said in a statement that the network “reported the facts reports accurately” and “presented the views of knowledgeable people” who spoke on camera about the product.
“Although we have concluded that continued litigation of this case is not in the company's interests,” Townsend said, “we remain committed to the vigorous pursuit of truth and the consumer’s right to know about the products they purchase.”
Oh, yes. It’s all about truth. And justice. And the American way.
After all, isn’t “American” in the very name of the noble TV network so unfairly accused when they were merely advocating for consumers’ right to know?
A Prudent DecisionHere’s the reality. When a lawsuit is settled out of court, ie, before a verdict is rendered, the defendants can argue all they like about how they settled only because the litigation was counterproductive to their lofty corporate mission.
But agreeing to a settlement means they figured they were going to lose, either in the South Dakota state court where the case was being heard or in the larger court of public opinion.
It’s not like The Walt Disney Company couldn’t pony up the legal fees to see this case through to verdict.
Meanwhile, Dan Webb, an attorney for BPI, told CNNMoney that his clients were “extraordinarily pleased” the lawsuit was settled.
“This was a long road to travel for BPI,” Webb said in an interview posted on the business website. “We felt the trial was necessary to rectify the enormous financial harm that had been suffered by BPI as a result of what we believed to be extraordinarily biased and baseless reporting by ABC in 2012.”
Actually, ABC was merely reporting on the fallout from the pink slime moniker with which rogue USDA microbiologist Gerald Zirnstein tagged BPI’s product. Unfortunately, that fallout included the closure of BPI plants in Iowa, Kansas and Texas, layoffs involving more than 700 workers and losses that allegedly reached hundreds of millions of dollars.
Had the plaintiffs prevailed, the damages in this lawsuit could have gone as high as $1.9 billion, according to the Walt Disney Company’s filing with Securities and Exchange Commission, since South Dakota's Agricultural Food Products Disparagement Act allows punitive damages that are triple any actual monetary losses.
That’s the truth and justice that ABC’s corporate owners were forced to confront.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.