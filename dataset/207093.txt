Mired in a system that rewarded mediocrity a generation ago, America’s beef industry was headed for oblivion. That was not just the perception of a handful of skeptics, but a view supported by the 1991 National Beef Quality Audit (NBQA), which caused former American Hereford Association executive secretary Hop Dickinson to succinctly describe American beef as, “Too big, too fat and too inconsistent.”
The quality audits, funded by the Beef Checkoff, have been conducted every five years for the past 25 years to provide a set of guideposts and measurements for producers and others to help determine quality conformance of America’s beef supply.
The landmark 1991 NBQA accurately placed the beef industry at a crossroads and identified necessary improvements if beef was to remain a significant player in America’s diet. Foremost, the 1991 audit identified $280 per head losses in carcass non-conformities—$220 of which related to excessive fat production.
“That’s when the beef industry started to change,” says Bill Mies, a Texas A&M University animal scientist who played a role in both conducting the quality audits and disseminating the findings to producers. “That 1991 audit provided evidence that the beef industry was leaving an estimated $2 billion laying on the table in non-conformities.”
Mies, now professor emeritus at Texas A&M, says the 1991 NBQA helped producers realize they weren’t in the cattle business, but “producing steak that would end up on their neighbor’s table.”
 

The quality audits, funded by the Beef Checkoff, have been conducted every five years for the past 25 years to provide a set of guideposts and measurements for producers and others to help determine quality conformance of America’s beef supply. As the years past, notice that some factors have been resolved, while new challenges have emerged. (Lori Hays/Farm Journal Media)

Beef’s marketing system 25 years ago was just as much to blame as the quality of the product for beef’s declining market share.
“Producers didn’t get paid for excellence,” Mies says. “They were rewarded for mediocrity. Pens of cattle all sold at the same price regardless of quality, so there was no incentive to change.”
With the awakening from the 1991 audit, many producers saw opportunities and began an industrywide quality revolution that pulled beef back from the brink. 
“No question—those in the production sectors of the beef supply-
chain have responded to the call regarding waste fat, injection-site lesions, animal care/handling and genetic improvements,” says Gary Smith, a meat scientist who also worked to conduct the NBQAs and deliver the sobering results, and now a visiting professor at both Texas A&M and Colorado State Universities. “Results of the 1991 NBQA provided targets, and BQA outlined a road map. Fortunately, producers listened and acted. With the support of agriculture-related media, allied industries, packers and processors and the Extension Service, the beef industry recharted its course and successfully navigated a change in culture. Without such effort, beef could have slowly eroded into a minor dietary source of protein.”
 
Fast forward to the 2016 NBQA, released in July of this year, and you’ll find dramatic and profitable improvements that led to greater consumer satisfaction and rewards to producers.
“We’ve come a long way as an industry in terms of improving beef quality,” says Jeff Savell, professor, meat science and E.M. “Manny” Rosenthal chair in animal science, Texas A&M University, and one of three 2016 NBQA principle investigators. “Changes suggested by the audit through the years are significant. The opportunities for further improvement—and success—are unmistakable.”
One critical fact laid bare by the 1991 NBQA was most producers operated as an island. Industry segments—cow-calf, stocker, feedlot and packer—tended to view other segments as adversaries, which prevented the sharing of information about cattle quality and performance, and, ultimately, hindered the industry’s efforts to produce uniform and consistent products.
 To demonstrate the value of cooperation between segments, the Strategic Alliances Pilot Project was initiated in 1992 that included all sectors of the beef chain. Fifteen ranchers sent about 1,500 steers to Decatur County Feed Yard, Oberlin, Kan., where they were sorted and fed to optimum endpoints, regardless of ranch of origin. The cattle were all slaughtered by Excel (now Cargill), with the beef sold through Safeway.
“It was an attempt to put all segments of the beef industry on the same level and to tear down the walls of distrust,” says Warren Weibert, who co-owned and managed Decatur County Feed Yard for 37 years before retiring in 2014. “It was a tremendous learning experience for all of us. We demonstrated combining efforts and sharing data could lead to better profits for all and a better quality product presented to consumers.”
 
 
Continue to read more about beef carcasses have changed, in Part 2: 25 Years of Beef's Quality Challenges. 
Read other stories from the Beef's Quality Revolution series: 
A Generation of Quality Gains
25 Years of Beef's Quality Challenges
Meat, Millennials, Meal Kits
Consumers Shift Attention to Cultural Concerns
Adapt Facilities, Equipment to Your Cattle
Resources to Improve Your Operation