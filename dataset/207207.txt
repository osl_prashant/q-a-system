The August USDA Cattle on Feed report shows an August 1 feedlot inventory of 10.604 million head, 104.3 percent of last year. This is the largest August feedlot inventory since 2012. July marketings were 104.1 percent of one year ago and the largest since 2014. Placements in July were up 2.7 percent year over year, the largest since 2013. This report was close to pre-report expectations with placements slightly less than average expectations but within the range of estimates.July placements, though up year over year, were up less than the double-digit levels of the previous four months. The very surprising 16 percent year over year increase in placements in June included increased lightweight placements. These were likely placed early_in June_rather than July and contributed to the rather modest year over year increase in July placements.
A question for a couple of months has been the extent to which the drought in the Northern Plains has contributed to increased feedlot placements. Certainly, placements in South Dakota, up 48 percent year over year since May, have increased much more than the average increase for other cattle feeding states and it is likely that much of that increase is drought-forced feeder cattle movement. Drought related changes in placements in other states are difficult to estimate but it appears that 6 to 13 percent of the total year over year increase in placements (518 thousand head) in May, June and July may be due to the drought.
USDA currently estimates that about 12 percent of the total cattle herd in the country is in regions experiencing some level of drought. I estimate that 6.5 to 7 percent of the total beef cow herd or roughly 2.1 million beef cows are in regions suffering with severe (D2) to exceptional (D4) drought conditions. This includes nearly a million head of cows in Montana and 500-700 thousand head each in North and South Dakota. Parts of the region have also experienced large wildfires. Lack of forage has led to significant destocking in the worst areas with cows culled or relocated to other regions. Heading into fall and winter with limited pasture and hay supplies means that cattle producers in the region will continue to struggle at least into spring 2018.
Roughly 1500 miles southeast of the drought, hurricane Harvey hit the Texas coast last weekend causing much damage and widespread rains that will continue for several days. Torrential rains and massive flooding are impacting much of southern and eastern Texas and southern Louisiana. In addition to a large human population, this region is home to a large number of beef cows. Upwards of a million or more cows in Texas and Louisiana will be impacted. Many unweaned calves are still on ranches at this time of year. Cattle losses are likely as deep water and widespread flood conditions are expected to persist for many days in some regions.