Life is good, Laura Batcha says.
The CEO and executive director of the Washington, D.C.-based Organic Trade Association oversaw an industry that set a record for sales in 2015, with further gains projected for this year and beyond.
In 2015, the organic market hit over $43 billion - the largest dollar growth ever in one year, according to the OTA's 2016 Organic Industry Survey.
"That is significant," Batcha said.
Fresh produce continues to set the sales pace for the organic category, the OTA reported.
"Produce continues to be the largest category, with tremendous growth," Batcha said.
She noted that sales standards fell even at a time when sector participants have voiced increasing concern about product availability.
"That growth happened with the industry sort of holding the reins on growth because there were concerns about supply," Batcha said.
The organic industry has been working to address the supply challenge, to improve and develop infrastructure, and to advocate for policy to advance the sector, she said.
Were supplies to keep up with demand, there would be no telling how sharply organic sales would jump upward, Batcha said.
"The upside is really unlimited and the pace at which the industry moves forward is much more about availability of products and not about the consumer's appetite," she said.
The industry saw its largest annual dollar gain ever in 2015, adding $4.2 billion in sales, up from the $3.9 billion in new sales recorded in 2014, the OTA reported. Of the $43.3 billion in total organic sales, $39.7 billion were organic food sales, up 11% from the previous year, and non-food organic products accounted for $3.6 billion, up 13%.
Nearly 5% of all food sold in the U.S. is organic, according to the OTA.
Produce led the way, as has been custom. The organic produce category notched sales of $14.4 billion, up 10.6%, in 2015, OTA reported.
Produce has set the pace for a number of reasons, Batcha said.
"Our data shows the organic shopper strongly equates eating fruits and vegetables as a healthy choice, so organic consumers are produce consumers," she said.
She said that consumers choose organic fruits and vegetables to avoid "toxic chemicals."
"The most tangible benefit perceived by the consumer matches very strongly with that product category," she said.
Organic also provides a direct connection to a kind of "purity" in production, Batcha said.
"It's easier to connect to the farm and so it's easier for them to connect to the farming practices. It's just tangible," she said.
Almost 13% of the produce sold in this country is now organic, OTA reported.
By comparison, dairy, the second-biggest organic food category, accounted for $6 billion in sales in 2015, which was an increase of over 10%. Dairy accounts for 15% of total organic food sales, the OTA said.
"Farm fresh foods - produce and dairy - are driving the market, Batcha said, noting that, combined, the two account for more than half of total organic food sales.
Batcha said consumer demographics would sustain the category's growth trajectory.
"The largest consumers are millennials, so the demographics for organics are in a good place. Also, the demographics of the shopper are becoming more diverse, racially, income-wise," she said.
"The future of the demographics of organics is very positive."