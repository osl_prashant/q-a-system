Carolina produce grower-shippers are expanding organic acreage to meet consumer demand.
“Organics is continuing to grow, and we’re seeing that trend nationwide,” said Heather Barnes, organics-focused marketing specialist with the North Carolina Department of Agriculture.
Organic production is an opportunity for growers to access that expanding market, Barnes said.
“We’re seeing increased demand on the consumer side, and our farmers are ready to meet that demand,” she said.
In the past, the Carolinas’ warm, humid climate served as a deterrent to organic production, and that remains an issue, Barnes said, but not an insurmountable one.
“In any climate, you have challenges, whether with weeds, insects or disease,” Barnes said. “Even in the best climate, nature can throw a curve. We’ve certainly seen our farmers can grow in conventional or organic practices crops that grow in North Carolina.”
North Carolina has seen perhaps the most notable increase in organic sweet potato production, Barnes said.
“Part of that is increased demand and part is organic tobacco production in the state, and sweet potatoes are an obvious fit with those who are growing organic tobacco in their rotation,” Barnes said.
Nashville, N.C.-based Nash Produce LLC is a large organic sweet potato supplier on the East Coast.
“The same with sweet potatoes in general, there’s been a steady increase in organic demand,” said Laura Hearn, the company’s marketing and business development director.
“It’s not the biggest commodity where there’s demand for organic, but there’s certainly those sectors, and we’d expect to continue to see that rise.”
Organic sweet potatoes also are working out well for Faison, N.C.-based Farm Fresh Produce Inc., said Bethany Malcolm, vice president.
“Our organic sweet potatoes are in high demand, particularly with our export customers,” she said.
Some crops are more challenging to grow organically than others, Barnes said.
“But our farmers are working to promote and get research dollars to help develop tools that organic farmers can use to manage pest issues,” she said.
In South Carolina, organic production has increased as well, said Matt Cornwell, marketing specialist for the South Carolina Department of Agriculture.
“We have seen an increase in demand for organics and have also seen growers who are looking to capitalize enter the organic market,” he said. “This includes some conventional operations that have added organic acreage to their operations.”
Ridge Spring, S.C.-based Titan Farms LLC is just entering the organic field this year, said Chalmers Carr, owner and CEO.
“We just put our first organics in the ground,” he said. “We converted about 40 acres of peaches, and next year, we’ll have some acres in vegetables. We’re looking to be a solutions provider to our retail customers.”
It makes business sense, he said.
“Everybody has to have (organics), and we’re a large supplier to several key retailers. If they come to me and ask me to do it, within our limits, we’ll try to work with them,” Carr said.
Lexington, S.C.-based Clayton Rawl Farms Inc. has about 20-30 acres of organic vegetables, said Chris Rawl, president.
“It’s just something we started a couple of years ago, and (we’re) kind of getting our feet wet,” he said. “We just wanted to get on the bandwagon. Some customers demand it.”