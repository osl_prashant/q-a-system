Georgia Organics celebrated its 20th year at the Georgia Organics Conference.
The conference, which took place Feb. 17-18 in Atlanta, had over 1,000 attendees and featured educational programs, awards, a tree sale and an organic dinner.
"It was an unbelievable," James Carr, Georgia Organics communications coordinator, said. "The energy was really high, (and) there was a lot of learning. We put a lot of energy into our farmer tracks. We had three farmers-only farm tours where they could dig deep into specific issues."
Author Barbara Brown Taylor and chef Matthew Raiford gave keynote speeches.
Rashid Nuri, founder and CEO of Truly Living Well Center for Natural Urban Agriculture, was awarded the Land Steward Award, an acknowledgement of his contribution to the Georgian organic movement.
"It would be difficult - and time consuming at the very least - to rattle off the very long list of Rashid's successes," Robert Currey, board member of Georgia Organics, said in a news release. "But all of us who know him can easily recall the moment we first met him."
A standout feature this year was a timeline project, documenting 20 years of Georgia organics history.
"There's not a lot of history about that, so we did a 20-year timeline and a lot of people added their achievements at their farms," Carr said. "It's fun to see all the pieces that have gone into making it such a special movement."