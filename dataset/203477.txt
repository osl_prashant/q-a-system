Mid-August rains pushed across the Corn Belt this week. While not all areas received needed rains, they were enough to keep corn and soybean futures reeling in the aftermath of USDA’s bearish August crop reports. Weather is expected to remain non-threatening next week, which limits the upside to modest corrective buying and keeps downside risk open. Aside from weather, traders will be intently focused on what we find on the Farm Journal Midwest Crop Tour. Winter wheat futures slid along with the corn and soybean markets this week, marking new contract lows. Spring wheat futures stabilized after early week price pressure and showed little net price movement for the week.
Pro Farmer Editor Brian Grete provides weekly highlights:



A corrective rebound attempt early this week in the cattle market was thwarted by sharply lower cash cattle trade, which resulted in sharp losses in futures. The narrowing of the spread between hog futures and the cash index ended and futures posted sharp weekly losses.
Notice: The PDF below is for the exclusive use of Farm Journal Pro users. No portion may be reproduced or shared.
Click here for this week's newsletter.