Montana considers limiting fishing guides on Madison River
Montana considers limiting fishing guides on Madison River

The Associated Press

HELENA, Mont.




HELENA, Mont. (AP) — Montana fish and wildlife regulators are proposing to cap the number of commercial fishing guides on the Madison River.
Fish, Wildlife and Parks officials say the proposed recreational management plan between Quake Lake and the Jefferson River confluence is the result of increased boat traffic.
FWP officials say the number of commercial outfitters on the blue-river trout stream has increased 72 percent since 2008.
They propose capping the number of outfitters at current levels, limiting the number of commercial fishing trips in different river sections and barring all commercial use on a portion of the lower river.
The proposal must go through a public comment period and be approved by the Montana Fish and Wildlife Commission.
The commission will take up the proposal at its April 19 meeting.