ORLANDO, Fla. - Forty-five years in the industry, and Garry Bergstrom still felt the same way about the most important aspect the produce business - the people.
 
"I was once told not to take the industry so personally - it's just business," Bergstrom told an audience at the Southeast Produce Council's Southern Exposure on March 11. Bergstrom, director of produce and floral for Lakeland-based Publix Super Markets, received the SEPC's Lifetime Membership Award. He retired from Publix March 1.
 
"I thought to myself, 'We pour our hearts into this business. We love this business. Of course it's personal - because we care," he said, accepting the award. "I think, too, that this business I'm in â€¦ is all about people. This is a people business. We just happen to sell produce."
 
Bergstrom started his career at Publix in 1972 working nights in the produce department, while attending college to study medicine. By 1975, he made produce manger, and held positions including merchandiser, buyer, director of the company's Lakeland division, and corporate purchasing manager.
 
He was named director of produce and floral in 2005, a title he had until his retirement.