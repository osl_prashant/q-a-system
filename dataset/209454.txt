Half a world away, on the Western Cape of South Africa there’s a dairy that isn’t too different from many progressive U.S. dairies.The Loubser family has been farming 22 miles from downtown Cape Town, South Africa since 1875 and dairying for the past 70 years. Fair Cape Dairies—as the operation encompassing the milking herd and processing facility is known—has been in business since 1995.
Five fifth-generation Loubser brothers run and own shares in the business: Melt, Eduard, Jr., Louis, Johannes and Viljee. Their 81-yearold father, Eduard, Sr., comes to the farm every day to checkup on things.
“The South African dairy industry started out as a totally regulated industry around 1930,” says Melt, CEO and director of Fair Cape Dairies. “Government interfered in everything.”
Government control was minimized starting in 1990, a time when the racial segregation system of apartheid ceased. The reins of government regulation were completely taken off in 1994.
“This harsh free market system dawned, and we really didn’t know how to deal with it,” Melt says.
During those first years transitioning into a free market system in the mid-1990s, Fair Cape Dairies, with their “Do the Right Thing” mantra, began bottling milk and manufacturing yogurt.
Today, products produced by Fair Cape Dairies’ processing plant include: fresh milk, flavored milk, UHT milk, yogurt, fruit juice, latte, mousse, custard and milkshake.
The family business has grown to include retail alliances with the company supplying 70% of
Woolworths’ yogurt and Ayrshire milk for the grocery store’s brand. Fair Cape Dairies’ yogurt can found on international flights via South Africa Airways.
South Africa has no subsidies, quotas or safety net systems. Milk prices in South Africa average about $16.55 per cwt (5 Rand per liter).
“You’ve got to be competitive, study market signals and know what is happening in the world. If you are not competitive you are out,” Melt says.

Melt relays there were more than 20,000 dairies 20 years ago, now there are fewer than 1,600.
“They are producing much more milk than they were 20 years ago.”
Fair Cape Dairies is an example of one of the farms that has increased production while staying competitive. Per year the dairy produces 159 million pounds (70 million liters) of milk from 2,500 cows milking three times a day on two farms.
The main farm houses 1,850 cows primarily made up of Holsteins that average 90 lb. (40 liters) per day.
Another 350 Ayrshire cows are milked for the branded program at Woolworths and 60 Jerseys have been added to boost milk solids. The Jersey herd will grow to 250 to 300 head. A 64-cow rotary milks 360 head per hour.
The other farm milks 700 cows in two double-18 herringbone parlors across the road from the main farm.
Director of dairy farming operations, Johannes says a move to bring more technology helped with the production increases. In 2002, a visit to Israel pushed Johannes toward using a system for monitoring cow performance and health.
“You can’t afford for a cow to show a problem. You have to identify the problem long before,” he says.
Milk production is monitored on each cow at every milking, conductivity is measured to watch for mastitis and weights are taken when cows go through the parlor. Heat detection through the monitoring system has helped reduce the need for employees to do visual inspections.
“I want to know where the problem cows are so I can give them early attention,” Johannes says.
Because of sexed semen there has been an influx of females in the herd, making culling decisions easier. Cows average 2.5 to 3 lactations.
At the main farm dry cows are in an open drylot with shades, while milking cows are housed in four barns. Two of the barns are open compost bedded packs and two are compost bedded freestalls. If Johannes could rebuild the barns he would go to freestalls only.
Fans are used in the barns and parlors to aid in cooling cows during the summer, but winds off the Atlantic Ocean help keep temperatures relatively cool year-round.
With 50 employees, access to labor is not a problem with South Africa having a 26.6% unemployment rate.
For feed, 3,700 acres is dedicated to oats, corn silage and lupin beans.
With the farm situated in an arid region they also grow 2,470 acres (1,000 ha) of dryland wheat, and they have a 100 ha irrigated vineyard for wine grapes, helping further diversify the Fair Cape Dairies portfolio.
Fair Cape Dairies lives up to their tagline of “Do The Right Thing.”
 
Note: The author, Wyatt Bechtel, is attending the International Federation of Agricultural Journalists world congress in South Africa. While at the congress he is learning about agriculture in South Africa while networking with journalists from across the globe.