Essay winners in Balchem Corporation's "Real Faces of Dairy" campaign, were recently announced, with winners selected in three youth and one adult category.The campaign, which is highlighted in each issue of Dairy Herd Management, profile individuals who made or are making significant contributions to the dairy industry. The campaign honors the dairy industry's past, present and future in both the magazine and on-line.
As part of the Real Faces of Dairy campaign, children and adults were encouraged to tell their dairy story through the Real Faces of Dairy websiteand Facebook page. Each entry answered these questions; What does dairy mean to you? How has the dairy industry impacted your life?
The finalists were announced mid-summer, and the public voted on the Real Faces of Dairy Facebook page for their favorite essay. Winners for each age category are as follows,
Adults: 
First Place: Lisa Ruble - Waterford, OH
Second Place: David Alexander - Conneaut Lake, PA
Third Place: Ashley Eckard-Maule - West Grove, PA
Fourth Place: Annah Nigon - Spencer, WI
15-18 Years
First Place: Derrek Hardy - Browning, MO
Second Place: Sarah Lehner - Delaware, OH
Third Place: Peyton Bond - Stockton, NJ
11-14 Years
First Place: Emily Makos - Juda, WI
Second Place: Lily Charapata - Coleman, WI
Third Place: Clarissa Ulness - Valders, WI
Fourth Place: Brock Strassburg - Bowler, WI
6-10 Years
First Place: Whitney Ulness - Valders, WI
Second Place: Abbie Ainslie - West Winfield, NY
Third Place: Brady Strassburg - Bowler, WI