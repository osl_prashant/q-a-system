Avocado growers are finding that sales of organic fruit seem to be edging upward every season.The Mission Viejo, Calif.-based Hass Avocado Board estimates there are 60 million to 80 million pounds of organic avocados consumed in the U.S. annually.
“The rate of growth in organic avocados is outpacing the rate of growth for conventional avocados,” said Sergio Cordon, avocado category manager for Robinson Fresh, Eden Prairie, Minn.
Consumer organic research recently conducted by Robinson Fresh indicated that organic is becoming mainstream, and shoppers are beginning to substitute organic items for conventional ones, he said.
Organic avocado sales have grown over the past five years for mainstream supermarkets and natural food stores, he said.
Organic is a growing segment for organic retailers as well as for mainstream supermarkets, said Dana Thomas, president of Index Fresh Inc., Riverside, Calif.
Most of the growth has been in mainstream markets, many of which have an organic section and sell organic as well as conventional product, he said.
“Avocados are perceived to be a very healthy product, and that has caused the avocado organic segment to grow,” he added.
About 7% to 10% of the volume at Henry Avocado Corp., Escondido, Calif., is organic, said president Phil Henry.
“Our organic business continues to grow,” he said.
The company sources organic fruit from Mexico as well as California.
“We have converted a number of our California groves from conventional to organic,” Henry said. “The demand has been very strong.”
Demand for organic avocados has risen significantly over the past 10 years, said Rob Wedin, vice president of sales and marketing for Calavo Growers Inc., Santa Paula, Calif.
“Today, organic demand far outreaches supplies,” he said, adding that organic fruit is a “driver in the market.”
 
Price premium
The cost or “value-gap” between organic avocados and conventional avocados is much greater than with other items, he said.
He estimated that 5% to 10% of the company’s avocados are organically grown.
Calavo strives to be the best buyer of organic avocados, he said.
“It comes down to relationships with growers, paying the right price and responding to the dynamics of the market in California and Mexico,” he said.
Del Rey Avocado Co. Inc., Fallbrook, Calif., may be the largest California organic avocado grower-shipper, said partner Bob Lucy.
The company also sources organic product from Mexico and Peru.
“Organic has always been a very important part of our program,” Lucy said.
Orders for organic product continue to increase.
“We’ve seen our business grow, and we’ve noticed that a number of our competitors in California are switching a number of their growers to organic fruit,” he said.
With a short avocado crop in California this season, growers are receiving higher returns on organic product than usual, he said.
“It’s very attractive for the farmer.”
But those bloated prices — said to be $65 per box in late July — were a cause of concern for Paul Weismann, president of Healthy Avocado Inc., Berkeley, Calif.
“The question is, how much volume can (buyers) take at these high prices?” he asked. “At what price will there be a downturn in demand?”
Conditions were pretty dire in late July, when California had little organic fruit to offer and Mexican growers, hindered by bad weather, were not picking organic fruit because of the small sizes, he said.
Supermarkets had no product to sell, he said, and buyers were in a quandary.
“Buyers don’t want to pay these kinds of prices, but they also don’t want to have empty shelves,” he said.
Weismann expected conditions to ease by mid-August, but still wondered if consumers might be reaching the limit as to what they’ll pay.