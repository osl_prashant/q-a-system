Yargus Manufacturing, established in 1968, recently joined Ag Growth International (AGI), a leading manufacturer of grain-, feed- and fertilizer-handling, storage and conditioning equipment. Yargus and AGI joined forces, increasing its global reach and product collaboration in domestic and international ag and industrial markets.  Yargus is a full-service company with experienced sales, engineering and design, marketing, production, installation, transportation and technical assistance personnel to serve all your material-handling needs.
Yargus Manufacturing customizes material-handling solutions for fertilizer, feed and industrial retail facilities, terminal and port locations.  Yargus offers turn-key solutions including:
•  Receiving equipment ranging from 50 to 1,500+ TPH
•  Batch blenders including rotary drum and tapered vertical blenders
•  Continuous-blend systems including declining-weight and volumetric blenders
•  Wholesale and blend towers
•  High-performance mixer
•  LAYCOTE liquid and powder coating systems

Click here to download the full pdf