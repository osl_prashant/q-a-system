Think that retail shoplifting isn’t that big of deal for retailers? Consider this recent report in The Times on the “evolutions” of shoplifting:
“First it was clothes‚ then electronic goods and baby formula. Now it is meat.
“Retailers waging a battle against grocery store shoplifting are adopting unorthodox crime prevention measures by placing electronic security tags on expensive cuts of meat,” the article stated. “The food tags work in the same way as on items such as clothes. Exit the shop without paying for the chunk of steak, and an alarm goes off.”
Previously‚ electronic tagging was reserved for expensive but easily stolen items such as DVDs, the article stated. But the drastic increase in tagging is directly due to the rise in shoplifting of “unconventional goods,” such as meat, in tough economic times. As a result, security at major supermarkets has been stepped up ahead of the holiday season.
“We put tags on the meat because it gets stolen a lot lately‚” an employee at a Pick n Pay supermarket told The Times. “But the thieves have gotten smarter, so we have to find other ways of protecting the meat.”
Sound familiar? It should, except for the fact that the story above was excerpted from The Times of South Africa.
Turns out, meat theft is a global problem.
Way Worse than it Was
Here at home, shoplifting has reached rampant proportions. The data explode several myths, namely, that only a tiny minority of people steal from stores; it’s mostly kids, anyway; and store security measures pretty much catch most of the scofflaws.
Not true.
Consider just a few factoids about shoplifting from the National Association for Shoplifting Prevention:
There are more than 27 million Americans (estimated) who have shoplifted from retail stores. That’s 1 out of every 11 people. More than 10 million people have been caught shoplifting in just the last five years.
Men and women shoplift about equally as often, and only one-quarter of all shoplifters are kids. The vast majority — more than 75% — are adults, half of whom admit they started stealing in their teens.
The vast majority of shoplifters are “non-professionals” who steal, not out of criminal intent, financial need or even greed, but as a response to social and personal pressures. Only 3% of shoplifters are “professionals” who steal for resale or profit.
But here’s the most shocking fact about shoplifting: According to the NASP, shoplifters claim they are caught, on average, only once in every 48 times they steal. And half the time, they’re not even turned over to law enforcement.
Granted, that 3% who are professional shoplifters, including drug addicts who steal to feed their habit, are often members of “shoplifting gangs” who steal as a business. In fact, they are responsible for more than 10% of total retail losses from theft, according to the NASP.
Of course, psychologists have long explained that the excitement of “getting away with stealing” produces a neurological reaction that’s described as a “rush” or a “high.” Many shoplifters acknowledge that this feeling is the real reward of shoplifting, rather than the value of the merchandise itself.
In that sense, it’s similar to gambling. Any objective analysis confirms that regular gamblers lose way more than they win. Those high-rise hotels and casinos in Las Vegas didn’t get built because the average gambler is consistently coming out ahead of the house.
Explanations aside, it is somehow perversely comforting that even though many shoplifters steal just for the rush, many more actually target items they perceive to be valuable.
Read any stories latterly about somebody stuffing a package of tofu into their pockets?
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.
 
Murphy: Feelin’ the stealin’
 
Summary:
Shoplifting’s a perennial problem, but the anti-theft measures typically employed to secure ‘big-ticket’ electronics are now migrating to a new area: the supermarket meat department.
 
 
Think that retail shoplifting isn’t that big of deal for retailers? Consider this recent report in The Times on the “evolutions” of shoplifting:
“First it was clothes‚ then electronic goods and baby formula. Now it is meat.
“Retailers waging a battle against grocery store shoplifting are adopting unorthodox crime prevention measures by placing electronic security tags on expensive cuts of meat,” the article stated. “The food tags work in the same way as on items such as clothes. Exit the shop without paying for the chunk of steak, and an alarm goes off.”
Previously‚ electronic tagging was reserved for expensive but easily stolen items such as DVDs, the article stated. But the drastic increase in tagging is directly due to the rise in shoplifting of “unconventional goods,” such as meat, in tough economic times. As a result, security at major supermarkets has been stepped up ahead of the holiday season.
“We put tags on the meat because it gets stolen a lot lately‚” an employee at a Pick n Pay supermarket told The Times. “But the thieves have gotten smarter, so we have to find other ways of protecting the meat.”
Sound familiar? It should, except for the fact that the story above was excerpted from The Times of South Africa.
Turns out, meat theft is a global problem.
Way Worse than it Was
Here at home, shoplifting has reached rampant proportions. The data explode several myths, namely, that only a tiny minority of people steal from stores; it’s mostly kids, anyway; and store security measures pretty much catch most of the scofflaws.
Not true.
Consider just a few factoids about shoplifting from the National Association for Shoplifting Prevention:
There are more than 27 million Americans (estimated) who have shoplifted from retail stores. That’s 1 out of every 11 people. More than 10 million people have been caught shoplifting in just the last five years.
Men and women shoplift about equally as often, and only one-quarter of all shoplifters are kids. The vast majority — more than 75% — are adults, half of whom admit they started stealing in their teens.
The vast majority of shoplifters are “non-professionals” who steal, not out of criminal intent, financial need or even greed, but as a response to social and personal pressures. Only 3% of shoplifters are “professionals” who steal for resale or profit.
But here’s the most shocking fact about shoplifting: According to the NASP, shoplifters claim they are caught, on average, only once in every 48 times they steal. And half the time, they’re not even turned over to law enforcement.
Granted, that 3% who are professional shoplifters, including drug addicts who steal to feed their habit, are often members of “shoplifting gangs” who steal as a business. In fact, they are responsible for more than 10% of total retail losses from theft, according to the NASP.
Of course, psychologists have long explained that the excitement of “getting away with stealing” produces a neurological reaction that’s described as a “rush” or a “high.” Many shoplifters acknowledge that this feeling is the real reward of shoplifting, rather than the value of the merchandise itself.
In that sense, it’s similar to gambling. Any objective analysis confirms that regular gamblers lose way more than they win. Those high-rise hotels and casinos in Las Vegas didn’t get built because the average gambler is consistently coming out ahead of the house.
Explanations aside, it is somehow perversely comforting that even though many shoplifters steal just for the rush, many more actually target items they perceive to be valuable.
Read any stories latterly about somebody stuffing a package of tofu into their pockets?
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.