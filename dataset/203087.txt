<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


Top Producer magazine has named Titan Farms, owned by Chalmers and Lori Anne Carr, as its 2017 Top Producer of the Year.

Top Producer and The Packer are owned by by Farm Journal Media Inc.

"My husband and I are truly living a dream," Lori Anne Carr said at the annual Top Producer's banquet in Chicago on Jan. 25, as reported by AgWeb.
"On the farm, we truly have a motto that nobody's going to come up and pat you on the back, or nobody's going to say, 'Great job,'" Chalmers Carr said. "We get our achievements and our personal goals set by going out and protecting the natural resources we have, cultivating those into crops and producing staples and food for other people to eat. My personal joy comes from doing that day in and day out."
Lori Anne and Chalmers Carr, owners of Titan Farms
The Top Producer of the Year contest is intended to represent the best in the farming business. The other two finalists were Pagel's Ponderosa Dairy and Gumz Farms, a row crop and vegetable operation in Wisconsin.
Each finalist received a trip for two to attend the seminar and session with a CEO coach. As the winner, Titan Farms will have the opportunity to be enrolled for a year in the Top Producer Executive Network peer group program, courtesy of Bayer. They may also choose between 150 hours of use with a Steiger Rowtrac or a Magnum Rowtrac courtesy of Case IH.