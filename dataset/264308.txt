When the waters of the United States (WOTUS) were defined in a 2015 rule under the Clean Water Act, lawsuits were everywhere, saying the definition was too broad.

When President Trump took office, he vowed to eliminate two rules or regulations for each new one. WOTUS was part of that promise.

In Washington D.C. last week, AgDay host Clinton Griffiths sat down with Scott Pruitt, administrator of the Environmental Protection Agency (EPA) about the rule.

According to Pruitt, a substitute or replacement definition will be issued sometime this year, a definition that will recognize private property ownership and the roles of states, and will answer the question of what exactly is a water of the United States.

“We’re going to get that right going forward, and the definition is going to provide clarity, objective measurements by which we know where federal jurisdiction begins and ends,” he said.

The purpose of the old, “deficient” rule, according to Pruitt, was about power and a governmental agency had jurisdiction on how to use land.

“That’s wrong,” he said. “We’re fixing that and we’re providing clarity moving forward.”

Hear what Pruitt has to say about the RFS, the culture changes and other issues impacting farmers and ranchers on the video above.