Two of The Oppenheimer Group Tour de Fresh riders are sporting Jazz apples to promote the brand at the July 25-27 event.Tour de Fresh raises money for the Salad Bars to Schools campaign. It started in Pismo Beach, Calif., and has a finish line in Monterey, coinciding with the Produce Marketing Association’s annual Foodservice Conference.
Chris Ford, organics category manager, and Karin Gardner, marketing communications manager, are riding for Oppy and boosting the refreshed Jazz apples with branded gear, according to a news release.
“We’re taking the idea of elevating brand awareness literally,” Ford said in the release.
The brand’s logo was updated with “contemporary colors and eye-catching graphics” as well as a new “compelling brand message”, which debuted in North America in May.
The apples will be promoted by Ford, Gardner and T&G Global, Jazz-brand owner, during the tour through the branded gear, media coverage and social media.
Riders can enter the Jazz Summer Vacation Sensation Instagram contest with pictures from the event, for a chance to win $3,000, according to the release.
Jazz apples are also offered at nutrition stops for cyclists along the course.
“The fresh-crop New Zealand fruit offers exceptional pressures in the summer, flavor is excellent, and the bright new graphics are creating attention and interest at the store level,” David Nelley, vice president of categories, said in the release.
Donations can be made until July 27 at tourdefresh.com/riders.