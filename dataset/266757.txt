BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Monday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMay                              118.50  Down 1.70May      118.70  119.10  116.15  116.40  Down 1.75Jul      120.80  121.10  118.25  118.50  Down 1.70Sep      122.70  123.20  120.45  120.70  Down 1.65Dec      126.45  126.50  123.90  124.10  Down 1.60Mar      129.95  130.10  127.40  127.65  Down 1.55May      132.30  132.30  129.80  130.00  Down 1.55Jul      134.00  134.00  132.00  132.15  Down 1.50Sep      134.20  134.20  133.85  134.05  Down 1.50Dec                              136.80  Down 1.55Mar                              139.45  Down 1.55May                              141.25  Down 1.55Jul                              143.00  Down 1.55Sep                              144.60  Down 1.55Dec                              147.00  Down 1.55Mar                              146.65  Down 1.55