Sonora grape volume is projected to be about 15.85 million boxes this season, down from 21 million boxes in 2017.
Two weather conditions contributed to the lower volume — not enough chill during the winter, and not enough sunshine during early growth.
Between 200 and 300 chill hours is normal, and most areas had 150 or fewer, said John Pandol, director of special projects for Delano, Calif.-based Pandol Bros.
“If we didn’t have a goofy spring, you’d probably be talking an 18 million-box crop,” Pandol said. “And if you had a decent winter you’d be talking a 20 million-box crop.”
The Sonora association of table grape growers, AALPUM, released the Mexican grape estimate at the Sonora Spring Summit April 19-20 in Hermosillo.
The association forecasts there will be 2.6 million boxes of early green seedless grapes, down 28% from 2017; 7.75 million boxes of red seedless, down 23%; and 3.09 million boxes of mid-season green seedless, down 30%. Grouping varieties into those three segments is new this year.
“As the grape industry goes from everyone having the same five varieties to each marketer having a few of the 50 varieties available, the challenge is how to present industry information in a way that is complete and meaningful but respects the commercial privacy of individual operations,” Pandol said.
In addition, the association estimates 1.11 million boxes of black grapes, 584,000 boxes of red globe grapes and 713,000 boxes of other varieties — all down 15% from 2017.
Pandol Bros. expects to start shipments in May, but the bulk of the volume will be in June.
“The first three weeks of May will be much lighter than they’ve been in (past) years,” Pandol said. “Somewhere in the week of May 21 we’ll all of a sudden see this radical change from scarcity to normal supplies.
“In the stores, the Memorial Day weekend will have some action, but really the five weekends in June — that’s where the supply is,” Pandol said.
He noted a positive factor for the season will be the 50% increase in inspectors at the Nogales, Ariz., crossing. The change should cut down on delays at the border.
Pandol said that development is particularly important in light of the electronic logging devices mandate, which has truckers on more restricted schedules.