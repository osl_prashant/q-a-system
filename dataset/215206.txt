Anhydrous is $63.86 below year-ago pricing -- higher 73 cents/st this week at $407.84.
Urea is $36.28 above the same time last year -- higher $4.03/st this week to $355.24.
UAN28% is $14.44 below year-ago -- lower 35 cents/st this week to $214.21.
UAN32% is priced $25.43 below last year -- higher $1.39/st this week at $225.29.

Urea was our upside leader in the nitrogen segment this week. Illinois gained $25.99 and Kansas firmed $23.99. Eight states were unchanged as Minnesota fell $4.93 and North Dakota softened 67 cents by the short ton.
UAN32% firmed $1.32 this week, led by Illinois which added $11.13. No other state posted a price change on UAN32% this week.
Anhydrous ammonia rounds out the list of gainers in the nitrogen segment. Illinois was up $6.19 and Kansas gained $1.87 per short ton.
UAN28% posted the only decline in the nitrogen segment this week. Minnesota softened $21.88 as North Dakota fell $10.83. Eight states were unchanged as Illinois firmed $28.33 and Indiana gained 50 cents per short ton.
In the grain markets, we are entering a period known as the holiday doldrums. Volume decreases sharply and interest in the markets sags. The same appears to be true of the fertilizer and fuel markets... sort of. Farmers do not always have the luxury of taking days like the Friday after Thanksgiving off. The good folks at your preferred local ag retail outlet probably did have that Friday off, and Friday is the day when our contributors send us their updated prices. This week, as I mentioned in our Weekly Fertilizer Bulletin on Monday, many states are unchanged suggesting price updates were not made on the Friday after Thanksgiving. In fairness, I should disclose that Pro Farmer and your Inputs Monitor were dark on that Friday, and we were all grateful for the extra time with family and friends.
With the grain markets, a sharp decline in the number of market participants generally equates to skewed results, and wild price swings. I believe that is what we are left to report on this week on the fuels and fertilizer side. Next week, we expect a full compliment of price updates. This week's price action still shows the same trend we have been watching, even with fewer contributors. That is, urea extending its premium to NH3 and UAN unsure of which one to follow. Our outlook is unchanged and we expect nitrogen prices to fall after post-harvest applications are complete. At that point, we will look for opportunities to finish booking nutrient for spring.
So thanks to the hardcore reporters that updated their results last week, and no hard feelings for those who didn't. As i said, your editor had the day off too. We look forward to next week's updates.
December 2018 corn closed at $3.87 on Friday, November 24. That places expected new-crop revenue (eNCR) per acre based on Dec '18 futures at $613.22 with the eNCR17/NH3 spread at -205.38 with anhydrous ammonia priced at a discount to expected new-crop revenue. The spread narrowed 1.16 points on the week.





Nitrogen pricing by pound of N 11/28/17


Anhydrous $N/lb


Urea $N/lb


UAN28 $N/lb


UAN32 $N/lb



Midwest Average

$0.24 3/4


$0.39 1/2


$0.38 1/2


$0.35



Year-ago

$0.29 1/4


$0.35 1/4


$0.40 1/4


$0.39





The Margins by lb/N -- UAN32% is at a 1/4 cent premium to NH3. Urea is 9 3/4 cents above anhydrous ammonia; UAN28% solution is priced 1 3/4 cent above NH3.





Nitrogen


Expected Margin


Current Price by the Pound of N


Actual Margin This Week


Outstanding Spread




Anhydrous Ammonia (NH3)


0


24 3/4 cents


0


0




Urea


NH3 +5 cents


39 1/2 cents


+14 3/4 cents


+9 3/4 cents




UAN28%


NH3 +12 cents


38 1/2 cents


+13 3/4 cents


+1 3/4 cents




UAN32%


NH3 +10 cents


35 cents


+10 1/4 cents


+1/4 cent