Fertilizer prices were mixed on the week with UAN28% posting the only higher regional average fertilizer price this week.
Our Nutrient Composite Index softened 1.54 points on the week, now 41.99 points below the same week last year at 508.67.

Nitrogen

Anhydrous ammonia led overall declines in the nitrogen segment once again this week.
UAN28% was led higher by gains in Missouri amid scattered back-and-forth price movement across the Midwest.
On an indexed basis, UAN28% remains our most expensive form of nitrogen this week although 28% has rarely been an upside leader. We generally expect UAN28% to spike below the rest of the nitrogen segment before a segment-wide move to the upside.

Phosphate

Phosphates were lower with DAP falling much more aggressively than did MAP.
Price gains in DAP and MAP were fairly meek and mostly offset by declines in other states.
The DAP/MAP spread is slightly wider than our expectations at 20.76.
Support for DAP and MAP stems from higher import prices in Central Florida and NOLA terminals over the past few weeks.

Potash 

Potash prices fell slightly this week with most states +/- less than $2 per short ton.
On an indexed basis, the premium potash prices hold to anhydrous ammonia widened to $19.99 on softness in NH3.

Corn Futures 

December 2018 corn futures closed Friday, August 4 at $4.12 putting expected new-crop revenue (eNCR) at $655.58 per acre -- lower $8.47 per acre on the week.
With our Nutrient Composite Index (NCI) at 508.67 this week, the eNCR/NCI spread narrowed 6.93 points and now stands at -146.91. This means one acre of expected new-crop revenue is priced at a $146.91 premium to our Nutrient Composite Index.





Fertilizer


7/24/17


7/31/17


Week-over Change


Current Week

Fertilizer



Anhydrous

$484.91
$475.02

-$9.89

$471.46
Anhydrous



DAP

$446.32
$446.48

+16 cents

$550.93
DAP



MAP

$460.30
$461.74

+$1.44

$571.69
MAP



Potash

$333.24
$332.43

-81 cents

$491.45
Potash



UAN28

$239.82
$235.46

-$4.36

$496.94
UAN28



UAN32

$257.74
$256.51

-$1.23

$488.56
UAN32



Urea

$328.44
$325.51

-$2.92

$489.65
Urea



Composite

513.79
510.21

-1.54

508.67
Composite