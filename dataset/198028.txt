Saturated areas of the southern and eastern Corn Belt down through the Delta and Southeast will remain wet during June, based on the updated 30-day forecast from the National Weather Service. The outlook calls for above-normal precip over these states.

In addition, a bubble of below-normal temps are expected over the southern Corn Belt and Mid-South.

If the forecast verifies, saturated soils will struggle to dry out and some acres will likely go unplanted.