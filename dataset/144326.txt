Crop calls
Corn: Narrowly mixed
Soybeans: 1 to 3 cents lower
Winter wheat: 1 to 2 cents higher
Spring wheat: 3 to 5 cents higher
Corn favored a firmer tone overnight, but ended the session narrowly mixed on strength in the dollar and a weaker tone in soybean futures. Key this morning will be if traders view weakness as a buying opportunity given the cool and wet system that's moving into the Corn Belt that will delay planting. Meanwhile, spring wheat was the leader in the wheat market overnight due to concerns about planting delays and winter wheat benefited from concerns about a frost threat in the Central Plains.
 
Livestock calls
Cattle: Higher
Hogs: Higher
Livestock futures are called higher on followthrough from yesterday's strong gains. A more cautious attitude may be seen in cattle futures this morning as traders wait on the midmorning online cash auction. While this week's cattle showlist is up from last week, the beef market has been solid. Meanwhile, the cash hog market saw some signs of life yesterday to signal a near-term low may be in the works. That should encourage more bottom-building in futures.