Elk from extreme southwestern Montana exposed to brucellosis
Elk from extreme southwestern Montana exposed to brucellosis

The Associated Press

BOZEMAN, Mont.




BOZEMAN, Mont. (AP) — State wildlife and livestock officials say an elk captured in the Tendoy Mountains in southwestern Montana tested positive for exposure to brucellosis.
The Montana Department of Livestock said Wednesday this is the first time a positive test has been found in the mountains southwest of Dillon, which is outside the area where ranchers are required to test livestock for brucellosis. The bacterial disease can cause cattle, bison and elk to abort or have weak offspring.
The Department of Fish, Wildlife and Parks says 100 elk were sampled.
Thirty elk were fitted with radio collars to study their movements and understand how the disease may spread.
State Veterinarian Marty Zaluski says finding an exposed elk in the Tendoys emphasizes the importance of elk surveillance near the boundary of the designated sampling area.