Chicago Mercantile Exchange live cattle futures settled higher on Wednesday after bargain buying and short-covering offset early-session selling following Tuesday's heavy losses, said traders.They said discounts in deferred months to this week's expected cash prices contributed to market advances.
June, which will expire on Friday, closed 0.650 cent per pound higher at 120.250 cents. Most actively traded August ended 0.600 cent higher at 115.625 cents.
A small number of animals at Wednesday morning's Fed Cattle Exchange brought $119 to $120 per cwt, down from $123 a week ago.
Last week market-ready, or cash, cattle in the U.S. Plains brought $118 to $123 per cwt.
Some processors will keep reining in cash spending given the seasonal slump in beef demand and plant closures during the U.S. Independence Day holiday, said traders.
They said phenomenal packer profits, and fewer cattle for sale than last week, might underpin cash prices in parts of the Plains.
The weaker dollar, which typically makes U.S. goods more attractive to foreign buyers, provided additional market support.
Recent market volatility might carry over into Thursday as investors adjust positions ahead of the holiday and end of the quarter, said traders and analysts.
Short-covering, technical buying and subsequent live cattle futures' advances rallied CME feeder cattle from session lows.
August feeders ended 1.525 cents per pound higher at 146.400 cents. 
Hogs Close Higher
CME lean hog futures' discounts to the exchange's hog index for June 26 at 91.10 cents attracted buyers, said traders.
Market participants also tweaked positions before Thursday's U.S. Department of Agriculture quarterly hog report.
Pre-report positioning capped back-month advances but boosted July futures to a new high.
July ended 1.450 cents per pound higher at 87.925 cents, and hit a fresh high of 88.000 cents. August finished 0.925 cent lower at 79.475 cents.
Packers cut cash hog bids with a few plants giving employees the day off on Monday prior to Tuesday's holiday, when most facilities will be closed, said traders and analysts.
They said retailers bought pork to round out Fourth of July grilling inventories and to offset potential meat shortages due to holiday plant shutdowns.
Current hog supplies are plentiful as farmers rush pigs to market as cash prices decline, a trader said. But the return of hot summer weather could slow animal weight gains, which makes hogs less available to processors, he said.