The packed product storage building of Nyssa, Ore.-based Snake River Produce collapsed after the second storm, and debris spilled onto highway US-26. Photo courtesy Snake River Produce
Grower-shippers in the Treasure Valley region said onion prices are likely to spike as snow continues to hamper production.
Idaho and eastern Oregon received a second severe snowstorm in less than two weeks Jan. 18 and Jan. 19, prompting more building collapses.
Eight onion companies confirmed to The Packer that they had lost at least one building; at least five had three or more cave in.
Numerous grower-shippers shut down production after the second storm to dedicate all resources to shoveling snow off roofs of remaining buildings. Some had lost packing sheds, and others lost storage sheds with onions inside.
"This is a bona fide catastrophe," said Kay Riley, general manager at Nyssa, Ore.-based Snake River Produce.
Ontario, Ore.-based Murakami Produce was one of few that had escaped damage immediately after the second storm.
"We're crossing our fingers," onion sales manager Chris Woo said Jan. 20. "It's terrible down here."
With the storms causing so much loss, some companies have invoked "act of God" clauses in contracts because they cannot fill orders, Woo said.
Murakami Produce has been receiving calls from quite a few such companies and has been able to help some of them.
"We just have to be selective," Woo said, noting the company is making sure to take care of its loyal customers. "We can only do so much."
Woo said onion prices had already been going up as a result of the weather-induced chaos. Around the holidays the price was between $4 and $5, and after the second storm the price had been $8 and was expected to go up further, Woo said.
Dwayne Fisher, vice president of marketing at Parma, Idaho-based Champion Produce, also said the market will definitely be affected.
"We've lost a considerable amount of product," Fisher said.
Shay Myers, general manager at Nyssa-based Owyhee Produce, said he expected the market to spike 20%-30% as a result of the widespread damage.
Riley said any effect of the weather on volume and price would be "much more significant" after the second wave of building collapses.
In the most recent storm, Snake River Produce lost its packed product storage building. The facility had about 20,000 bags of onions inside, Riley said. Two more structures came down in the weekend that followed, the company announced on its Facebook page.
Owyhee Produce had four buildings collapse, including the company's main storage facility, due to the second storm.
About 20 millions pounds of onions were lost, Myers said, but the packing shed had minimal damage, so Owyhee planned to resume shipping on a limited basis the week of Jan. 23.
Nyssa-based Golden West Produce has lost six buildings, three in each storm, said secretary Patty Henderson. The latest damage included the packing shed, a loss Henderson called "devastating."
Payette, Idaho-based Riverfront Produce Co. has lost three buildings to the snow. The first storm caused the collapse of one covered storage building and the partial collapse of another. The second storm brought down a lean-to covering 6,000 empty onion bins and prompted the partially collapsed building to fall completely.
"It looked like something that was hit by a bomb," office manager Kristi Gomeza said. "My mouth just dropped open."
The scene of the collapse included debris 100 to 150 feet from the structure, Gomeza said.
Gomeza estimated the company lost between one million and 1.5 million pounds of onions, and production is down about one-third from normal.
Like other companies, Riverfront has struggled to meet orders as a result of the overwhelming snow, which has also complicated traffic.
"It truly is a nightmare," Gomeza said.
Parma, Idaho-based Champion Produce has also lost three buildings - a Champion West storage building that went down Jan. 9, and in the latest storm another Champion West storage building and a storage building of Wilder, Idaho-based Tamura Farms, a partner of Champion.
Champion had shut down production Jan. 23 to clear the roofs of its remaining facilities. The packing sheds had been uncovered, Fisher said, and Champion hoped to have its main facility running again Jan. 25.
Ontario-based Frahm Fresh Produce had lost its main storage shed and was working Jan. 20 to save its packing shed, which was standing but missing a wall.
Weiser, Idaho-based Four Rivers Onion Packing lost its packing shed in the first storm, which took place about 10 days before the second storm, and Weiser-based Haun Packing lost a storage building that had some equipment inside.
So far, no injuries have been reported in connection with the many roof failures.
"It literally is miraculous," Fisher said. "That's the blessing in this whole thing."