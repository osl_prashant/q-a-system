There’s no shortage of controversy regarding so-called ritual slaughter, ie, the Kosher and Halal requirements that animals be alive when their throats are cut.On one hand, such religious practices have traditionally been exempt from the oversight of civil government — including having to pay taxes in some countries.
But animal welfare activists, who are quick to decry the Christian assertion of “dominion” over the animals of the Earth, have embraced the thinly disguised anti-Semitic and anti-Muslim sentiments rising across Western Europe as a common cause in their efforts to attack all of animal agriculture and meat processing.
In Switzerland, which has long banned ritual slaughter since the 1890s, a new bill was introduced in Parliament. Although not specifically identifying Kosher or Halal, it aims to outlaw all imports of meat products from animals that were “mistreated,” according to a Bloomberg report.
“We take this [bill] seriously, but are rather optimistic that it will not result in an import ban on kosher meat,” Herbert Winter, president of the Swiss Federation of Jewish Communities, told the Jewish newspaper Hamodia.
Winter noted that the language of the bill authorizes an import ban for “products produced in a manner which is cruel to animals.”
“It is by no means clear that Kosher meat would qualify as such a product,” he said.
Nice try, Herbie.
The Real Reason for a Ban
Several countries already forbid the slaughter of food animals without stunning, or have attempted to impose such a ban, including:
Belgium. In May, a bill affecting the country’s French-speaking Wallonia region was proposed to limit religious slaughter. In February, a similar attempt by the Walloon (regional) parliament was overturned by a national constitutional court, which ruled that it would “contradict basic human rights laws and religious rights.”
Denmark. In 2014, the government banned the religious slaughter of animals for production of halal and kosher meat. Minister for agriculture Dan Jørgensen told news media at the time that, “Animal rights come before religion.”
New Zealand. Most of the country’s lamb and mutton is Halal, as Indonesia and the Middle East are major export markets. However, virtually all of those animals are “pre-stunned” before ritual slaughter.
Poland. In 2013, the Polish parliament rejected the ruling government’s attempt to allow abattoirs to resume ritual slaughter of food animals, primarily for export to the Middle East.
Meanwhile, the ban-the-imports bill in Switzerland, introduced by Matthias Aebischer, a member of Switzerland’s Social Democratic Party, primarily forbids importation of foie gras, but also specifically bans imports of fur and frog legs.
Yet Katharina Büttiker, president of the Swiss Animal Alliance, which consulted on the bill, according to Hamodia, stated frankly that both Kosher and Halal meat ought to be covered by the proposed ban, which she presumably believes would drive devout Jews or Muslims into embracing vegetarianism.
Again, nice try.
Here’s one other factor complicating the ban-the-import-of-Kosher-or-Halal-meat movement: Some legal observers have stated that such a ban could conflict with free-trade rules enforced by the World Trade Organization.
There is no denying that the emergence of legislation designed to outlaw religious slaughter is an outgrowth of efforts by nationalist elements to curtail the influence of Muslim communities. And there’s no denying that, as would be expected, animal-rights groups, who have no problem sidestepping the entire religious controversy, have joined forces with those reactionary politicians to advance their vegetarian cause.
Experts such as Temple Grandin have stated unequivocally that ritual slaughter, done properly, causes no more pain or suffering to an animal than electrical stunning. The issue really isn’t about animal welfare.
The imposition of a ban on ritual slaughter is motivated by a political agenda, not by (alleged) humane concerns for livestock.
But since when have animal activists ever put principles above pragmatism?
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.