Horton Fruit Co. Inc. is recalling bags and containers of fresh spinach because it has the potential to be contaminated with Listeria monocytogenes.The Louisville, Ky.-based company was alerted to the potential of contamination by a supplier, and a subsequent internal investigation, according to a Horton Fruit news release posted on the Food and Drug Administration’s website.
There have been no reports of illness related to the recalled product, and no other products produced by Horton are implicated, according to the May 12 release.
The products were distributed in North Carolina, Illinois, Kentucky and West Virginia and distributed through retail stores, wholesale and foodservice distributors.  
“We are treating this incident very seriously because we want to ensure that our customers are provided with only the safest, most wholesome, and high-quality products available,” Michael Wise, Horton’s president, said in the release.
The company has ceased distribution of the product as it continues its investigation. 
The products in the recall are:
10-ounce bag of Peak brand Fresh Curly Leaf Spinach, with label “Best Used By 05/11/17” and Universal Product Code of 0-78951-50002-3;
10-ounce bag of Harris Teeter Farmer’s Market brand Ready-To-Eat Leaf Spinach, “Best If Used By 05/11/17” and UPC 0-72036-88023-9; and 
2.5-pound bag of Peak fresh spinach, with an orange sticker dated 05/13/17 and case label pack date 05/01/17.