Agency re-designates 12 counties as natural disaster areas
Agency re-designates 12 counties as natural disaster areas

The Associated Press

BULLHEAD CITY, Ariz.




BULLHEAD CITY, Ariz. (AP) — The U.S. Department of Agriculture announced the re-designation of 12 Arizona counties as primary natural disaster areas due to losses and damages caused by recent drought.
The Mohave Valley Daily News reports farmers in eligible counties have eight months to apply for emergency loans to help cover part of their actual losses.
The designation immediately offers the availability of low-interest Farm Service Agency emergency loans in all primary and contiguous counties.
Farmers and ranchers in the neighboring counties of La Paz, Yavapai and Yuma also qualify for natural disaster assistance, as do farmers and ranchers in the counties of San Bernardino, California; Montezuma, Colorado; Clark and Lincoln counties in Nevada; Catron, Cibola, Grand, Hidalgo, McKinley and San Juan counties in New Mexico; and Kane, San Juan and Washington counties in Utah.
___
Information from: Mohave Valley Daily News, http://www.mohavedailynews.com