Pure Flavor, Leamington, Ontario, sponsored the Leamington Pure Kid Triathlon Series.
The event was part of the annual Leamington Triathlon weekend, featuring Kids of Steel triathlon races for children ages 3 to 13. 
“We are happy to have the opportunity to promote a healthy lifestyle to the children in our community,” Jamie Moracci, president of Pure Flavor, said in a news release. “This race has truly become a great tradition where families can spend time together and kids can participate in physical activities in a fun environment.”
Three more races are planned in the next two months. Children who participate in all four races will receive a Fitbit and chance to win a $500 Bicycle World gift card. 
Pure Flavor is handing out its Mini Munchies snacks at the events. 
Proceeds from the events go to Erie Shores Hospice. 
For more information, visit pure-flavor.com/purekids.