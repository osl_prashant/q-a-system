Most beef producers understand that when the weather gets colder their cattle need more energy for maintenance. The questions are when do cattle start experiencing cold stress and then how much more energy do they need? Both the actual temperature and the wind speed interact to determine the effective temperature. Table 1. shows the dramatic effect of wind speed on the effective temperature the cattle experience. Any kind of available protection, whether natural or man-made, can be very valuable in reducing wind chill and the negative effects of cold environments.

When do cattle feel cold stress?
The second consideration is just exactly when do cattle begin to feel cold stress? The point of cold stress, or lower critical temperature, depends in large part on the amount of insulation provided by the hair coat. As shown inTable 2., that insulation value changes depending on the thickness of the haircoat and whether it is dry or wet.
As a general rule, for every degree that the effective temperature is below the lower critical temperature, the cattle's energy needs increase by 1 percent. For instance if the effective temperature is 17 degrees F., the energy needs of a cow with a dry winter coat are about 15% higher than they would be under more moderate conditions. That energy requirement jumps up to about 40% higher under those conditions if the hair coat is completely wet or matted down with mud.



Performance & Energy Impact
Performance:It's important to remember that cattle can adapt to short term weather changes relatively well without a significant impact on performance. Cattle can deal with a few cold, miserable days without suffering long-term effects. However, ignoring the energy costs of long-term cold stress greatly increases the risk of poor performance later.
Feedlot Condition:Muddy conditions in a feedlot are especially detrimental to performance. Any steps that can reduce cold stress, such as providing wind protection or bedding will reduce maintenance requirements and maintain performance. View additional information on theuse of bedding in a feedlotto learn more.
Feed Intake:Beef cows fed high-roughage diets often respond to cold stress by increasing voluntary feed intake. That is not necessarily true for feedlot cattle being fed high-concentrate diets, especially if cattle must contend with poor lot conditions due to mud, ice, or snow.
Management Considerations
There are some management considerations to keep in mind regarding changes in feed intake in response to cold stress:
Make sure that water is available. If water availability is restricted, feed intake will be reduced.
If the feed availability is limited either by snow cover or access to hay feeders, the cattle may not have the opportunity to eat as much as their appetite would dictate.
Be careful providing larger amounts of high concentrate feeds. Rapid diet changes could cause significant digestive upsets.