Imports from Chile offer retailers a counter-seasonal supply of fruits from cherries and blueberries to grapes and stone fruit. Supply and quality look good, marketers say.
According to the Chilean Fresh Fruit Association, cherries will enter the U.S. from December to January, blueberries from December to February, grapes from December to March or early April, and stone fruit in February and March.
And reports across the full assortment from Chile are very positive, thanks to a good winter and spring and an early growing season, said Eric Coty, executive director of South American imports for The Oppenheimer Group, Vancouver, British Columbia.
“Demand for grapes, for example, looks good based on California moving through inventories quickly and Peru well under forecast,” Coty said. “The European market looks to be short at the moment, and this added demand may help strengthen the U.S. market.”
A simple, steady season of sales and strong demand would be welcome, considering the Chilean deal has been faced with numerous challenges during the past few years, Coty said.
Paul Newstead, vice president for Nathel International, Pittsgrove, N.J., agreed, saying that barring any weather-related events, this will be close to a perfect season for growing conditions.
“Chile had ample chill hours this winter,” he said. “The rains of last year replenished much of the shortages, especially in the northern regions where water has been in critical stages for years. Additionally, spring weather has been ideal with a lack of frost and optimal pollination temperatures.”
 
Grapes
This year should be much more normal in the timing of grape arrivals, said Josh Leichter, general manager for Pacific Trellis Fruit/Dulcinea Farms.
“Last season, arrivals were concentrated in December and January with almost 2 million more boxes arriving in those months compared with the five-year average,” he said, even though the total volume of Chilean grapes was in keeping with the five-year average.
The market should be stronger this year, with grape arrivals anticipated to be more spread out instead of the concentrated supply at the beginning of the season, Leichter said.
As more growers convert their vineyards into the newer grape varieties, retailers will have attractive product for their customers, Newstead said.
Pacific Trellis Fruit’s Leichter said volumes of Timco, Allison and Krissy and other grape varieties continue to increase.
 
Stone fruit
Craig Padover, account and category manager for Jac Vandenberg Inc., Yonkers, N.Y., said stone fruit from Chile will also have more traditional timing this season than last year, which was a couple weeks early.
Also, for the first time, Chile can export nectarines to mainland China, so this will take some of the surplus supply away this year and offer a more stable market, he said.
For apricots, supply appears to be limited but there have been good growing conditions, Padover said.
For plums, he said volumes on the black cat variety should be good in March — and the same for angelinos.
 
Cherries
Chile continues to plant cherries at a healthy rate, with more non-bearing acreage coming into play each year, Newstead said. “It’s an exciting time for the cherry business down there with demand from Asia being so strong.”
Padover said this could be the long-promised year that Chile produces a big cherry crop.
“More than 90% of Chilean cherries go to markets outside the U.S., but if there’s a big crop, we should have plenty of supply to promote around Christmas.”