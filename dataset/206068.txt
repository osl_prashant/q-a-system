For the sixth year in a row, sweet corn marketer GloriAnn Farms is supporting the National Breast Cancer Foundation through the Corn for the Cause campaign.

For every case of GloriAnn sweet corn sold to participating retailers, the company plans to donate to the National Breast Cancer Foundation.

“GloriAnn Farms is honored to once again partner with this incredible organization,” Mark Bacchetti, GloriAnn Farms principal, said in a release. “With this campaign, we hope to not only raise a significant donation again this year, but to raise awareness to the importance of early detection and education, because that is what can truly save lives.”

GloriAnn Farms, Tracy, Calif., has donated almost $100,000 to the foundation and its Helping Women Now mission, a goal that the company expects to surpass this year, according to the release.