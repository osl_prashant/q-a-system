Minnesota wineries' lawsuit over source of grapes dismissed
Minnesota wineries' lawsuit over source of grapes dismissed

The Associated Press

MINNEAPOLIS




MINNEAPOLIS (AP) — A federal judge has dismissed a lawsuit filed by two Minnesota wineries over a law that restricts the source of grapes they can use.
Alexis Bailly Vineyard and Next Chapter Winery are farm wineries, meaning they can sell their products to wholesalers, retailers and directly to consumers as long as 51 percent of the grapes or grape juices come from Minnesota.
The wineries claimed that requirement is unconstitutional and hurts their business. They plan to appeal.
The state argued the wineries could seek exemptions, or could apply for a different license — which wouldn't have the same grape restrictions but also wouldn't allow them to sell straight to the public.
U.S. District Judge Wilhelmina Wright dismissed the case, saying the wineries have a choice and can get the manufacturer's license to avoid their asserted injury.