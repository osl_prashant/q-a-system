Here's a look back at some of the most read stories on AgWeb this year. From markets to dicamba, politics and more ... here are some of the highlights.

10. Frost to Creep Into Corn Belt as early as Sept. 6 - August 31, 2017, Ashley Davenport
9. Corn to be "Darling of Market" in 2017, Expect $6 Soybeans in Fall - January 10, 2017, U.S. Farm Report
8. Why the IRS Just Raided Caterpillar's Peoria Headquarters - March 2, 2017, Bloomberg
7. Emergency Dicamba Ban Nears - June 20, 2017, Chris Bennett
6. How Corn Could Rally to $5.20 - July 12, 2017, Nate Birt
5. Bald Eagles a Farmer's Nightmare - April 3, 2017, Chris Bennett
4. Who Killed the Finest Soybean Soil in the World? - October 29, 2017, Chris Bennett
3. Farmers Face Catastrophic Loss After Viral Grain Bin Collapse Video - August 3, 2017, AgDay
2. When a Farmer Punches Back at the Feds - April 28, 2017, Chris Bennett
1. Trump Signs Executive Order for Ag - April 26, 2017, Ben Potter
 
What was the AgWeb story that helped you the most this year? Post a link in the comments, or share on our facebook page.