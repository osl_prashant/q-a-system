Louisiana ag officials seek help solving cattle deaths
Louisiana ag officials seek help solving cattle deaths

The Associated Press

BATON ROUGE, La.




BATON ROUGE, La. (AP) — The Louisiana Department of Agriculture and Forestry and the St. Landry Parish Sheriff's Office are seeking the public's help in finding whoever's responsible for the recent deaths of cow and three calves.
On March 3, agriculture inspectors received a complaint from a rancher in reference to cattle killed on the levee near Port Barre. The animals were found dead in the pasture portion of the levee. Inspectors say it appears the cattle were run over by a truck.
Witnesses say they heard what sounded like trucks mud riding in the area around midnight on March 3.
Anyone with information is urged to call Livestock Crimestoppers at 1-800-556-9714, the LDAF Hotline 1-855-452-5323 or St. Landry Parish Crimestoppers at 337-948-TIPS.
All calls are anonymous and could earn up to a $1,000 reward.