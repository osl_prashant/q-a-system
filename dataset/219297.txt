Chicago Mercantile Exchange live cattle futures on Monday finished lower for the first time in three session due to profit-taking and continued heavy stock market losses, said traders.
"People were just watching what happened with stocks over the past few days and decided they wanted out of everything and figure it all out later," a trader said.
February live cattle closed 0.725 cent per pound lower at 126.125 cents. April settled 0.650 cent lower at 125.475 cents.
Live cattle futures traders await this week's prices market-ready, or cash, cattle. Last week, packers paid $125 to $126 per cwt for cattle in the U.S. Plains that sold for $127 a week earlier.
Some packers might need supplies due to cold weather in the U.S. Plains that is not allowing cattle to put on weight as fast, said analsyts and traders.
Improved packer profits and prospects for increased beef demand might lend near-term cash price support, they said.
They cautioned that slower cattle weight gains are backing up livestock in feedlots, contributing to U.S. government forecasts for increased supplies in the coming months.
Wednesday's Fed Cattle Exchange sale of nearly 1,000 animals may set the tone for this week's cash cattle trade. None of the cattle for sale there last week attracted buyers.
Monday evening is the first notice day for live cattle deliveries against the February contract that will expire on Feb. 28.
Profit-taking, lower CME live cattle and steady to softer cash feeder cattle prices sank the exchange's feeder cattle contracts.
March feeders ended 1.250 cents per pound lower at 149.675 cents.
Most Hogs End Lower
CME lean hogs closed mostly lower after a few investors sold deferred months and at the same time bought February futures ahead of its expiration on Feb. 14.
Technical selling, along with weaker cash and wholesale pork prices, discouraged prospective buyers of the April contract and beyond.
February lean hog futures closed 0.625 cent per pound higher at 75.825 cents, and reached a new contract high of 75.900 cents. Most-active April finished down 0.225 cent at 73.325 cents, and May ended 1.025 cents lower at 78.300 cents.
Packers resisted paying more for hogs after that strategy over the past three sessions wore down their margins, a trader said. Monday's wholesale pork value fell led by sharply lower pork belly prices for product going into storage for later use, he said.