Chicago Mercantile Exchange lean hog futures were mostly lower on Monday, pressured by bear-spreading and big U.S. supplies that continued to weigh on cash markets, traders said.The front-month October hogs contract notched the biggest declines, easing 0.975 cent, or about 1.7 percent, to settle at 60.000 cents per pound. December hog futures fell 0.425 cent to 58.050 cents.
"There's just too much supply," said Rosenthal Collins Group broker James Burns.
Pork packers were slaughtering nearly record amounts of U.S. hogs, but some hog producers and traders expected more aggressive buying after two new packing plants opened earlier this month in Iowa and Michigan, Burns said.
"Demand is decent but one of the problems is (hog) producers ramped up supply in anticipation for the new facilities and we have had some delays in some of those openings," he said.
Cash hogs traded $1.31 lower to $52.17 per cwt in the top market of Iowa and southern Minnesota, the U.S. Department of Agriculture said after the close of futures trading.
Wholesale pork was up 33 cents to $78.09 per cwt, led by firmer prices for rib and belly cuts, USDA said.
Hog futures reversed from gains of more than 3 percent on Friday, with the October contract on Monday hitting upside resistance at its 10-day moving average before turning sharply lower. 
Cattle Higher
Both live cattle and feeder cattle futures were mostly higher, bolstered by light chart-based buying and following modest gains late on Friday in Plains cash cattle markets.
CME October live cattle settled 0.175 cent lower at 107.575 cents per pound while December live cattle futures gained 0.125 cent to 112.950 cents.
CME October feeder cattle settled up 1.300 cents to 151.950 cents per pound, after earlier hitting a nearly two-month high of 152.450 cents.
Plains cattle fetched $105 to $106 per cwt on Friday, in deals that were flat to up $1 from the previous week. Relatively light-volume trading suggested that beef packers likely will need to buy more cattle soon, INTL FCStone said in its Daily Livestock Report.