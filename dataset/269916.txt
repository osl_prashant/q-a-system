BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Wednesday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2800  Up    96May         2736    2825    2736    2824  Up    96Jul                                 2824  Up    94Jul         2700    2856    2696    2800  Up    96Sep         2730    2877    2724    2824  Up    94Dec         2722    2854    2718    2806  Up    78Mar         2711    2827    2702    2780  Up    69May         2694    2799    2694    2770  Up    62Jul         2721    2772    2721    2769  Up    60Sep         2736    2773    2736    2773  Up    59Dec         2744    2776    2744    2776  Up    56Mar                                 2785  Up    56