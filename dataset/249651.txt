Profit taking and lower wholesale pork prices weighed on CME lean hogs, said traders.
Processors competed for supplies while taking advantage of their impressive margins, but the Lenten season will make it harder for packers to move significant amounts of meat, a trader said.   
Uneasiness over NAFTA negotiations after U.S. President Trump signaled that he will raise tariffs on steel and aluminum imports further weighed on CME hog futures.
"If the Trump administration does anything with NAFTA, there could be repercussions on the meat side, specially the hogs," French said.             
April hogs closed down 0.600 cents per pound lower at 68.200 cents, and May finished 1.500 cents lower at 74.250 cents.