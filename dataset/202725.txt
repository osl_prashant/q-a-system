Among the "sayings about Canada" you will find on the interweb, I like: 
 
"Americans are benevolently ignorant about Canada, while Canadians are malevolently well informed about the United States." J. Bartlet Brebner
 
Perhaps J. Bartlet Brebner, the speaker of that sentence, underestimates the American interest in Canada and overstates the Canadian opinion of the U.S.
 
As to the first point, the U.S. Department of Agriculture's Foreign Agricultural Service only this week issued a 61-page report on the Canadian retail landscape.
 
What does the report tell us?
 
The USDA says Canada's 36.2 million consumers in 2016 generated $397 billion in retail sales, of which food and beverage sales account for 17% of retail sales. The agency said that 2016 U.S. agricultural exports totaled more than $20 billion to Canada, with $16 billion or nearly 80% comprised of high-value consumer food products.
 
Some highlights of the report. From the USDA:
 
In 2016, approximately 58% of food sales were sold through traditional grocery stores. This channel is primarily dominated by the 'Big Three' â€“ Loblaws, Sobeys, and Metro.
However, nongrocery retailers, particularly Costco and Walmart have gained market share in the last five years as they now account for 20% share of the retail grocery market.
The discount retail channel has grown since the recession, whereby today 36% of all grocery items are sold either through this channel or through sales promotions, compared to 27% in 2010. Each of the Big Three grocery retailers maintains discount banners, like Loblaws' No Frills and Sobeys' FreschCo. This trend is predicted to grow as even discount merchandisers as Dollarama are offering more food staples to their customers.
 
Some characteristics of the Canadian shoppers, according to the report:
Graying Population: 40% of the population is 45 years or older, and that will increase to 48% by 2026. Because a healthy diet can influence quality of life, that demographic trend could be positive for fruit and vegetable marketers;
Household size has decreased from almost 4 in 1970 to less than 3 in 2015; single size portions create market opportunities for retailers and suppliers;
Statistics Canada projects that by 2031, ethnic shoppers will represent 31% of the consumers in Canada;
Consumption of fruits and vegetables by Canadians has increased significantly over the last decade; Statistics Canada reported close to 41% of Canadians consume fruits and vegetables five or more times a day;
Canadian consumers have become bargain shoppers; the weak Canadian dollar has caused food prices at the stores to increase. For example produce prices in 2015 increased by 12.4% from the previous year. The Royal Bank of Canada reported that 57% of shoppers carefully compare food prices and are reducing their impulse purchases, according to the USDA;
Canadians are pretty honest about themselves and apparently want to do better. In a 2015 survey conducted by Nielson, 59% of Canadians considered themselves obese and a total of 84% reported they intend to change their current diets;
About 26% of Canadian children ages 2-17 years old are currently obese and moms seek healthy options for their children; and
The U.S. supplies approximately 74% of the organic market in Canada; Among millennial consumers, 27% reported they are willing pay more for organics.
 
That's only a small appetizer of the data available on the Canadian market from the USDA FAS report. There is no excuse for ignorance, whether benevolent or otherwise.