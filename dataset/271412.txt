Fearing Ethiopian dam, Egypt bans water-intensive crops
Fearing Ethiopian dam, Egypt bans water-intensive crops

The Associated Press

CAIRO




CAIRO (AP) — Egypt's parliament has passed a law banning the cultivation of crops that require a large amount of water, amid fears that a massive Ethiopian dam being built upstream could cut into the country's share of the Nile.
The law passed late Sunday would ban the cultivation of rice, bananas and other crops, with violators facing prison time and a fine of up to $3,000. Egyptian President Abdel-Fattah el-Sissi would need to approve the law.
The Nile provides virtually all the freshwater for Egypt's 100 million people.
El-Sissi says the country is building desalinization plants on the Red Sea to reduce its dependence on the river as Ethiopia nears completion of the dam.