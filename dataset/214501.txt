Rochester, N.Y.-based Wegmans was the No. 1 retailer, and No. 2 overall Best Company to Work For on the most recent Fortune Magazine list. 
This is the 20th anniversary for the list, which ranks companies on the Trust Index, based on feedback with Great Places to Work-Certified companies. Employees are asked questions about their employer’s honesty, ethics, camaraderie, fair and respectful treatment and more. 
Small things made a difference, like cake on birthdays and free hot chocolate in winter for anyone who works outside, for Wegmans employees, according to the rankings. Wegmans ranked No. 4 last year. 
Other grocery and convenience retailers that made the list include: 

Nugget Market, Sacramento, Calif., ranked No. 2 among retailers and No. 30 overall; 
Publix Super Markets, Lakeland, Fla., ranked No. 3 among retailers and No. 21 overall; 
Whole Foods Market, Austin, Texas, ranked No. 58; 
QuikTrip Corp., Tulsa Okla., ranked No. 5 in retail and 68 overall
Sheetz Inc., Altoona, Pa., ranked No. 87 overall.

Whole Foods, Wegmans and Publix earned special recognition from Fortune for being among only 12 companies that have made the Top 100 list all 20 years. Mountain View, Calif.-based Google Inc. was No. 1 overall.