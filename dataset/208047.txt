I’m a huge fan of fresh cherries, with that fondness starting as a teen when they were consumed under the cover of semi-darkness in a neighbor’s orchard. 
 
My thievery represented just one of the many ways a cherry grower can see a reduction in a crop, but for me, my love for his fruit wouldn’t stop there. 
 
No, for me it was to become an eight-year adventure in cherry promotion, which also presented an unbelievable situation never appearing in my wildest dreams or, in this case, nightmares. 
 
This was a position in the late ’70s to the mid-’80s as sales promotion manager for Northwest Cherry Growers that had me directing a program for cherry producers in four states.
 
Without a doubt, the most memorable was the 1980 cherry crop, which in April looked like a good one based on normal growing conditions in the weeks ahead. 
 
All seemed fine, as normal as any cherry season can be for a crop that can see it all fall apart in a day or so of heavy rain.
 
This was also the year Mount St. Helens erupted. 
 
The major blow was the media saying the St. Helens ash landing on the cherry orchards, some seven tons per acre, was poisonous and this meant the cherries in those orchards would also be poison.
 
So, here we were. A crop weeks away from harvest, a promotion program in place to help in the marketing, and all feedback telling us our future consumers thought the cherries were toxic.
 
With no time to clear our minds, we had to move quickly. We had to hit the phones and try to turn this unbelievable situation around.
 
Our two solid weeks of calls to produce industry merchandisers and the major media focused on three critical areas.
 
First, the cherry acreage hit by the ash amounted to only about one-sixth of the overall production. 
 
Next, the ash was not poisonous. As scary and uncomfortable as it was, as much of a burden as it was (34 wheelbarrow loads in my driveway), it was not harmful — that is, not counting potential problems with one’s lungs when breathing the stuff.
 
Finally, we said it was just not poisonous, but since our already volcanic soil contributed to growing superior crops, what St. Helens had actually done was provide growers a gift. Thanks to the infusion of ash, we had received even more of a good thing. 
 
Oh, I forgot, after our efforts had indeed turned around views in the markets, it didn’t really matter. Yes, it rained and much of it was ruined.
 
Alan Taylor is the former marketing director for Pink Lady America in Yakima, Wash., and now consults in the apple industry. E-mail him at proft@embarqmail.com.
 
What's your take? Leave a comment and tell us your opinion.