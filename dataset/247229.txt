It happens online and in person. You are engaged with someone in a discussion, but when you leave the conversation, you feel you didn’t get your point across to them.
One key step in effective communication is understanding generational intelligence, says Amy Hays, adult education manager with the Noble Research Institute. She says generational intelligence helps us understand how people in any generation learn and make decisions.
“Individuals in different generations think and take action differently,” Hays says. “This isn’t about their age; it’s how they operate.”
An example of how different generations interact is currently being illustrated within agriculture. Hays says generational intelligence is even more important at this time because of the pending land transfer that will predominantly occur from grandparents to grandchildren. The land transfer will skip a generation due to the overall size of the groups. Baby boomers (born 1946 to 1964) and Generation Y (born 1979 to 2004) are larger generations than the generation between them—Generation X (born 1965 to 1984).
“One note is that grandparents are transferring land but not transferring the decision-making. They stay involved. And that can cause conflict,” she says.
In general, Hays says each generation carries a different outlook and mentality in approaching situations. A generation’s cultural ethos and outlook affect how people think, act and learn, and she provides the following overviews of each generation: 
The Greatest Generation (born before 1946) may be summarized as “together we can.”
Baby boomers have the mentality of the individual. Their perspective is very individualized. Think “you matter”—not a selfish you but a focus on individual recognition.
People in Gen X approach things as if “whatever you want to do, you better prove it.” They grew up in an  information age and may ask to be shown data as support.
Millennials (born 1979 to 2004) are the first generation that grew up globally connected. And they appreciate that things happen in the U.S. and affect people elsewhere. You have to answer why it matters for them to buy in, and this generation sees the big picture.

Blueprint For Advocacy. As a professional in the agriculture industry, you are probably often compelled or asked to provide answers regarding the science and technology of agriculture. With only 2% of the U.S. population being farmers, those conversations—as an individual or a group and online or in person—can often get sidetracked by a lack of understanding of on-farm practices—or emotion.
To provide a blueprint for successful ag communications, consultant and scientist Tim Pastoor developed the ABC approach—awareness, bridging and content.
First, awareness allows you to assess the situation and whether the person with which you are engaging has a similar position or an adversarial position. After you identify the person’s position, you can use a statement to bridge into something that is more helpful. And for content, he admits that as a scientist himself, he immediately starts to talk about facts, but after you recognize who you are talking to and how to address that person’s question, you’ll be able to generate a more positive experience.
Where To Start. Everyone will fall on a spectrum from being adversarial to being supportive to your position. After you’ve identified where someone falls on that line, you can use bridge statements accordingly. When someone has an adversarial position, show your concern, and address how they are anxious about the subject. A few examples include, “I understand your concern,” “Maybe I can reassure,” and “Let’s talk about that.”
When engaging with someone who is supportive of your positions, bridge statements will build that person as an ally. Examples of such statements include, “Great point. Let me add to that,” “Glad you asked,” and “Tell me more. I’m listening.”
The bridge statements are meant to successfully extend the conversation so you can then provide content to support your point of view.
Talking About GMOs. As an example, he explains when questioned about GMOs, he focuses on  three main content points:

It’s equivalent—it’s another way to hybridize.
It’s very well-tested and reviewed.
It has been endorsed by major global science organizations—declared by 10 scientific organizations to be a safe technology.

“I joke that no one likes pesticides until they have a flea infestation or bed bugs—then they want nuclear options,” Pastoor says. “But it is important to remember that with content, people love to hear benefits and positive things.”
The ABC approach provides a pattern to have something to say that’s constructive and engage in successful conversations.
Factor In Generations. Pastoor also recommends using generational intelligence to inform your process.
“In addition to thinking if someone is supportive or adversarial, you have to think about it from their generational perspective as well.”
Here are how the four generations may come into a conversation about GMOs and, in response, how you may frame your conversation and interactions with them.
Greatest Generation: “We will starve.” / Your approach: “We can produce enough.”
Baby boomer: “I will starve.” / Your approach: “Safe, affordable food for you and your family.”
Gen X: “Who says I’ll starve?” / Your approach: “Well-studied, carefully regulated.”
Gen Y: “I’m fine. I’ll figure it out.” / Your approach: “Let’s work together to do what needs to be done.” 
For the content in your conversations, he recommends doing your own homework. Websites Pastoor recommends as resources include croplifeamerica.org, giveacrop.org and gmoanswers.com. 
 
What’s Going On With Gen Z?
Amy Hays with the Noble Research Institute says if you need proof of what makes people in Generation Z tick, then look no further than the 4-H county fairs.
“There are more entries in the ‘recycled and repurposed’ category than any other,” she says. “Generation Z is a tinkerer generation, and we haven’t had one of those in 100 years—since Henry Ford and the Wright brothers, and look at what they did.”
Generation Z is typically defined as anyone born after 2000. And as this generation enters the workforce and the farm operations, they will bring their own perspectives in decision-making and conflict resolution.