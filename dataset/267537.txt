BC-IL--Illinois News Digest 6pm, IL
BC-IL--Illinois News Digest 6pm, IL

The Associated Press



Here's a look at how AP's general news coverage is shaping up in Illinois at 6 p.m. Questions about coverage plans are welcome and should be directed to the AP-Chicago bureau at 312-781-0500 or chifax@ap.org. Herbert McCann is on the desk. A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
NEW ON THIS DIGEST: DELTA AIRLINES-CYBER ATTACK; CHICAGO VIOLENCE-PENDLETON; FAMILY STRANGLED; MOTHER KILLED-ILLINOIS; BBA--WHITE SOX-GROUNDSKEEPER RETURNS.
TOP STORIES:
ILLINOIS FARMING
BLOOMINGTON, Ill. — The latest report from the U.S. Department of Agriculture says Illinois farmers are expected to continue planting more corn than soybeans even as the trend changes nationally. The Pantagraph reports that the USDA's prospective plantings estimate says there will likely be 11 million corn acres planted in Illinois this year, about 200,000 fewer acres than last year. The USDA predicts 10.6 million soybean acres this year, which is about the same as 2017. SENT: 280 words.
SCHOOL CONSTRUCTION-UNMARKED GRAVES
CHICAGO — Construction workers building a school in a northwest Chicago neighborhood are working carefully to avoid disturbing human remains that may lie underneath the construction site. The Chicago Tribune reports that the $70 million school in Dunning is being built at the site of the former Cook County Poor House. An estimated 38,000 people were buried at the location in unmarked graves, including people too poor to afford a funeral, unclaimed bodies and patients from the county's insane asylum. SENT: 280 words.
BUSINESS:
DELTA AIRLINES-CYBER ATTACK
Delta now says that payment-card information for about "several hundred thousand" airline customers may have been exposed by a malware breach last fall that also hit Illinois-based Sears and other companies. The airline says that the malware attack may have exposed customers' names, addresses, credit card numbers, card security codes and expiration dates. By David Koenig. SENT: 350 words.
IN BRIEF:
—CHICAGO VIOLENCE-PENDLETON: The murder trial of two men charged in the shooting death of a Chicago teen days after she performed with her high school band at President Barack Obama's 2013 inaugural festivities won't begin this month as planned.
—FAMILY STRANGLED: An attorney is asking an Illinois state court to reconsider the conviction of a man serving life in prison for the 2009 killings of his wife and their two young sons.
—MOTHER KILLED-ILLINOIS: A judge has ruled that a 16-year-old western Illinois girl will be tried as an adult on charges she tried to cover up the killing of her friend's mother.
—ILLINOIS TORNADOES: National Weather Service surveys have confirmed four tornadoes in southern Illinois after a round a storms Tuesday.
—SYNTHETIC MARIJUANA-ILLINOIS: Illinois public health officials say the number of people in Illinois who have experienced severe bleeding after using synthetic marijuana has increased to 81 cases.
—UNIVERSITY OFFICER-SHOOTING: A University of Chicago student who was shot after rushing at campus police officers brandishing a long metal pipe has been charged.
—PUZO PAPERS-DARTMOUTH: Illinois Gov. Bruce Rauner has donated his collection of "The Godfather" author Mario Puzo's papers to his alma mater, Dartmouth College. Photo.
—CHILD PORNOGRAPHY-INDICTMENT: A federal grand jury in central Illinois has returned a two-count child pornography indictment against a 20-year-old man.
—JUDGE-FRAUD CHARGES: A Cook County judge who was convicted in federal court in Chicago of fraudulently obtaining mortgages for investment properties is fighting to keep her seat on the bench.
—RESTAURANT BOOKKEEPER-FRAUD CHARGES: A former bookkeeper for two famed Chicago restaurants has been accused of stealing more than $600,000 from the businesses over a six-year period.
—ILLEGAL CAR SALES: The Illinois Attorney General's Office has filed a lawsuit against two Cook County men for illegally selling used cars with altered titles.
—U OF ILLINOIS-WOMEN: The University of Illinois is recognizing accomplishments made by women during the university's 150-year history.
SPORTS:
BBA--TIGERS-WHITE SOX
CHICAGO — James Shields starts the home opener Thursday as the Chicago White Sox meet the Detroit Tigers. Shields settled down after a four-run first in the season opener at Kansas City last week, going six innings to help the White Sox rout the Royals 14-7. Daniel Norris makes his first start for the Tigers — off to a 1-4 start. By Andrew Seligman. UPCOMING: 650 words, photos. Game starts at 3:10 p.m. CT.
With:
BBA--WHITE SOX-GROUNDSKEEPER RETURNS
CHICAGO — Imprisoned 23 years for a crime he didn't commit, Nevest Coleman couldn't imagine a day like this. He was back in his old job as a groundskeeper for the Chicago White Sox, working the home opener against the Detroit Tigers on Thursday. "When you sit back when you're locked up, you don't think about (a day like this)," Coleman said. "You just think about what's going on trying to move forward in life, trying to figure out what I'm gonna do when I get out, how I'm gonna support myself. The White Sox gave me the opportunity." By Andrew Seligman. SENT: 345 words, photos.
BBN--CUBS-BREWERS
MILWAUKEE — The Milwaukee Brewers emerged as surprising challengers last season to the Chicago Cubs in the NL Central. The division rivals meet for the first time this season when they open a four-game series at Miller Park. UPCOMING: 650 words, photos. Game starts at 7:10 p.m. CT.
___
If you have stories of regional or statewide interest, please email them to chifax@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.