Pomegranate arils have proven to be a driving force for many, with most seeing the majority of growth in that category.
"Arils are indeed where we're seeing the most growth," said Tom Tjerandsen, manager at the Sonoma, Calif.-based Pomegranate Council.
Mike Forrest, president of Reedley, Calif.-based Youngstown Distributors Inc., agreed arils are the perfect way to drive category growth to new groups of consumers.
"This will be our ninth season of doing arils and we're seeing good growth year over year. Less than 10% of the population have eaten a pomegranate, but that's why we offer arils," Forrest said.
Aril production has greatly improved as companies have invested in technology.
"The technology is improving and giving pomegranate marketers a longer shelf life than we've seen in the past which has helped to considerably build that side of the business," Tjerandsen said shelf life is improving, which has led to more retailers adding more aril options.

Successful strategies
Marketing pomegranate arils with the berries is one way to boost sales, said Adam Cooper, vice president of marketing for the POM Wonderful brand.
The company's Pom Poms product has seen strong growth over the past few years.
"Arils have been growing at a rate of 60%, and we're hoping to see that for this year as well. As we think about evolving how to merchandize in the store, we're seeing a lot of success with placing Pom Poms in the berry case. People are interested in trying different types of antioxidants and pomegranates and berries work well together," Cooper said.
The placement doesn't appear to have a negative effect on berry sales.
"Berries have been shown to have a down season in late fall, and that's when Pom Poms are in their prime. It doesn't cannibalize the berries, and retailers we worked with last year saw tremendous upselling, incremental to the whole portfolio," Cooper said.
Another option is to remove a lower-performing cut fruit option and replace it with aril offerings in various convenient sizes.
David Anthony, salesman, Ruby Fresh Pomegranates, Firebaugh, Calif., said another good marketing option is to place arils with ready-to-eat meal solutions in a deli or other similar area.
"Placement is important. With snack cups, a key location near the front of the store where prepared sandwiches, salads and cut fruit are located have realized great traffic for arils. When available, customers like to choose a Ruby Fresh Jewel snack cup for a quick healthy snack or meal replacement," Anthony said.
"We continue to realize double digit growth in the aril category year on year, and have for the past five seasons," Anthony said.
Getting arils to retailers earlier is also important.
Cooper said POM Wonderful is being aggressive with its arils season.
"Consumers want arils as early as we can as long as the color and sweetness is there, so in the past we may have done fresh products first and then arils, but research has shown it's valuable to offer arils earlier," Cooper said.
Foodservice
Arils are also making a big impact in the foodservice arena.
"The reason for that is that pomegranates are a great way for chefs and caterers to differentiate themselves," Tjerandsen said.
"They can pour a glass of champagne and charge $4, or they can add a few arils to the glass and charge $5," Tjerandsen said.
Growers often offer a bag of arils for foodservice.
"We offer a one-pound food service pack that is very popular. This allows restaurants and meal prep stations at the retail grocery level to include fresh and ready to eat pomegranates with their recipes," said David Anthony, salesman, Ruby Fresh Pomegranates, Firebaugh, Calif.