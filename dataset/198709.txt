About Carinata MealCarinata (Brassica carinata) is a new oilseed that shows great potential for the Midwest region. This oilseed has been developed to have high amounts of oil that are very good for biofuels production. Additionally, after the oil extraction a high protein meal (40-48% of crude protein) is obtained. Recent research results from the SDSU Dairy Science Department indicated that carinata meal (CRM) is highly degradable in the rumen and is a comparable protein source to soybean meal and linseed meal in total digestibility (Lawrence et al., 2015). We are also part way through a feeding study with dairy heifers with promising results so far (Rodriguez-Hernandez et al.,unpublished).
However, CRM is like other oilseeds in the brassica family, exhibiting high concentrations of a glucosinolate, known as sinigrin, which limits its potential use as a feedstuff to 10% or less of cattle diets. Glucosinolates in themselves are non-harmful, but when degraded during chewing and digestion, they form substances that cause a bitter taste which affects the palatability of the meal. In some cases and when fed at high amounts they result in health problems such as hypothyroidism, potentially affecting animal growth. Therefore, finding options to decrease the glucosinolate content would allow more flexibility in use of CRM for cattle. Acids, heat, and certain enzymes can break down glucosinolates. It was hypothesized that ensiling CRM with forages could decrease the glucosinolate content. An added benefit is that as CRM has high crude protein, ensiling it with corn forage would increase the nutritive value of the corn silage. Since producers can only ensile corn forage one time per year, we also wanted to test ensiling CRM with haylage as another option since it's harvested several times per year.
CRM Trials
To test this theory, two trials were conducted, one on ensiling CRM with alfalfa haylage and the second one with corn forage. For both trials, three blends of carinata meal to forage were used with the following proportions, on a dry matter basis, 0:100, 25:75, and 50:50. In both trials, each of the three blends were packed in PVC micro-silos in quadruplicate for 0, 7, 21 and 60 days of ensiling. The packing density was 16 lbs DM/ft3.
The fermentation profiles on both alfalfa haylage and corn silage blends results were within normal ranges for haylage and corn silage (data not shown). As the proportion of CRM increased in the blends in both trials it resulted in decreased fiber and increased crude protein content (see Table 1).
Table 1.Nutrient composition of the blends of carinata meal and forage on day 60 of ensiling.
Item
CRM: corn silage
CRM: alfalfa haylage
% of Dry matter
0:100
25:75
50:50
SEM
0:100
25:75
50:50
SEM
Dry Matter, %
31.6
38.0
47.7
0.29
40.5
47.0
56.5
0.26
Crude Protein
6.2
15.6
25.0
0.28
24.6
29.6
32.6
0.95
Neutral Detergent Fiber
33.9
30.4
28.7
0.22
38.5
34.6
31.8
0.25

Sinigrin (glucosinolate) content was greatest in the 50:50 and decreased over time in the 25:75 and 50:50 in both trials (see Figure 1 and 2). The sinigrin concentration decrease was more in the 25:75 than in the 50:50 in alfalfa haylage blends. In contrast, in the corn silage blends the percent decrease was more in the 50:50 than in the 25:75.
Figure 1.(Above) Sinigrin (glucosinolate) decrease during ensiling in blends of carinata meal and alfalfa haylage.
Figure 2.(Above) Sinigrin (glucosinolate) decrease during ensiling in blends of carinata meal and corn silage.
Conclusion
In conclusion, ensiling carinata meal with forages decreased glucosinolate concentrations, without major detriment to silage fermentation. Additionally, the crude protein of silages was increased with the most promising blend being 25:75 of CRM:corn silage. Animal feeding studies with the ensiled blends are still needed to determine the impacts on palatability and animal performance of the compounds left from the glucosinolate breakdown. Ensiling, however, shows good potential as an on-farm method to decrease glucosinolates in carinata meal.
Reference:Lawrence, R. and J. Anderson.Camelina meal and carinata meal: potential protein sources for dairy cattle.