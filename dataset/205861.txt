Crop calls
Corn: Fractionally to 1 cent lower
Soybeans: Fractionally to 1 cent higher
Winter wheat: Mixed
Spring wheat: 3 to 7 cents lower
The bleed lower in the corn market continued overnight, with futures inching to another round of contract lows to worsen the oversold condition of the market. Meanwhile, buying in soybean and winter wheat futures was limited to corrective short-covering. A non-threatening near-term forecast and ample global supplies will limit buying interest, as well as ongoing concerns about a slowdown in exports along the Gulf Coast due to damage from Hurricane Harvey. Meanwhile, this morning's weekly export sales data showed new-crop corn and soybean sales above expectations. Also this morning, USDA announced an unknown buyer has purchased 132,000 MT of new-crop soybeans.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Livestock futures are expected to see a mixed tone this morning on a combination of followthrough from yesterday's gains and profit-taking. August live and feeder cattle futures expire at noon CT today and are trading in line with their respective cash markets. Limited cash cattle trade has occurred around $105 in the Plains, roughly $2 below last week. This will also limit buying interest in cattle futures. Meanwhile, the cash hog market was mostly steady yesterday, but packers are having no difficulty securing supplies, as week-to-date kill is running 26,000 head above week-ago and 47,000 head above year-ago levels. As a result, traders are comfortable with futures at a discount to the cash index.