Citing frustration with state bureaucracy and ineffective communication with farm workers about their rights, William B. Gould IV resigned Jan. 13 as chairman of the California Agricultural Labor Relations Board. 
 
For his replacement, California Gov. Jerry Brown designated Genevieve Shiroma as chairwoman of the board. She has served as a member of the board since 1999, according to a news release.
 
In his resignation letter, published by the Los Angeles Times, Gould said his final day will be no later than Feb. 22; he has served about three years in the post.
 
Gould said in the letter that he believes the Agricultural Labor Relations Act is "irrelevant to farmworkers" because they aren't aware of its provisions.
 
"More than 99% of the agricultural workforce (a group disproportionately plagued by homelessness, diabetes and lack of health insurance) appears to be unrepresented and the instances of unfair labor practice charges and invocation of the Mandatory Mediation and Conciliation Act are few and far between," he said in his letter. 
 
To address that issue, Gould said he proposed a rule that would publicize the Agricultural Labor Relations Act on grower property.
 
"Regrettably, though the board adopted the proposed rule 14 months ago for worker education after extensive hearings in Fresno, Salinas and Santa Maria, the rule has languished in the bowels of state bureaucracy for the past 14 months," he said in the letter. Gould said one of the reasons the Agricultural Labor Relations Board is not effective because it is not an independent administrative agency.
 
Western Growers vice president and general counsel Jason Resnick had been appointed by Gould to serve on his advisory Labor Management Committee, according to a news release.
 
"We did not always see eye to eye with Chairman Gould, but we have great respect for him," Resnick told the Los Angeles Times. "We hope that the administration will restore a balance of perspectives and views on the board," Resnick said.