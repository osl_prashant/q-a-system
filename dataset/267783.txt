In the Missouri legislature, a bill that was introduced this session by Rep. Jeff Knight, a Republican representing District 129 in the central part of the state, about 180 miles southwest of St. Louis.
Knight’s proposed bill would prohibit the labeling of food products as “meat” that are not, in fact, meat. It’s obviously aimed at the slew of plant-based, alt-meat analogs that have entered commercial channels labeled as beef, pork or chicken.
According to the Missouri House of Representatives’ website, Knight’s bill is represented as legislation that “Prohibits misrepresenting a product as meat that is not derived from harvested production livestock or poultry.”
If the bill becomes law, Section 265.494 of the state statutes would be amended as follows (the changes are in bold type, as they appear in the text of the bill):
“No person advertising, offering for sale or selling all or part of a carcass or food shall engage in any misleading or deceptive practices, including:
(7) Misrepresenting the cut, grade, brand or trade name, or weight or measure of any product, or misrepresenting a product as meat that is not derived from harvested production livestock or poultry.”
Pretty non-controversial, right?
Not if you’re one of our pals at PETA.
David Perle, PETA’s assistant media manager, responded to a report on that bill by a colleague with a statement from PETA President Ingrid Newkirk.
Without even reading her statement, I could guess most of the points she would make, but in this case, I wasn’t prepared for a citation from the Bible as the counter to a piece of legislation attempting to clarify food products labeling.
But then again, this is PETA we’re talking about.
Along with the usual overblown rhetoric characterizing livestock producers as “environmentally destructive perpetrators of animal abuse and purveyors of ill health,” Newkirk attempted to invoke religion as the basis for her opposition to civil policymaking.
“In biblical times,” Newkirk wrote, “[meat] meant ‘food,’ not ‘flesh.’ ”
Apparently, she’s referring to Genesis 9:3: “Everything that lives and moves about will be food for you. Just as I gave you the green plants, I now give you everything,” which would obviously include meat.
However, that verse seems to indicate that, unlike Newkirk, the Almighty Himself was making a distinction between animal foods and green plants — not lumping them all together as “meat.”
Dictionary Doesn’t Lie
But PETA’s counter-argument to Knight’s bill didn’t rely solely on the Good Book. Oh, no. Newkirk invoked another authority to bolster her case: the Merriam-Webster Dictionary.
That’s right. She claimed that Rep. Knight should not be allowed to use the word “harvest” in the language of his bill, because that term’s definition dates back to 1828 and was originally applied to “apples, corn and other crops.”
Well, I looked up the word “harvest” in the Merriam-Webster Dictionary, and not only does it date the world’s origins back to medieval times — not 1828 — but one of the definitions cited refers specifically to “The quantity of a natural product gathered in a single season; the salmon harvest,” and includes a usage example taken from a March 2018 story in the San Antonio Express-News as follows: “Hunters can … report their harvest directly on the turkey page at www.tpwd.texas.gov/turkey.”
Nice try, Ms. Newkirk, but your attempt to inject etymology into an argument against a bill intended to define food labeling, something the Food Safety and Inspection Service does every week of the year, failed as miserably as your organization’s ongoing efforts to assign the rights and privileges of people to every other member of the animal kingdom.
Of course, PETA has long embraced the philosophy that the ends justify the means, and when you start with the conviction that your cause is of supreme importance, it’s but a small step to believing you have justification to do “whatever’s necessary” to advance your cause — hence the exploitation of near-naked women protestors as a tool to protest the exploitation of animals.
When industry officials or media members dismiss the effectiveness of PETA’s shamelessly over-the-top stunts, they often acknowledge that at least the group’s leaders talk a good game, that they’re clever with the use of language.
In this case, however, even that concession doesn’t apply.
And that’s not just me, that’s the dictionary talking.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.