If you build packer space, the hogs will come. With an unprecedented amount of packing capacity open and more slated to come, producers have been quick to respond by increasing supplies.
According to USDA’s inventory of all hogs and pigs on Sept. 1, 2017, total inventory was 73.5 million head, a record high. This was up 2% from Sept. 1, 2016, and up 3% from June 1, 2017.
Prices Steady As Packing Capacity Increases
Market hog inventory, at 67.5 million head, record high, was up 3% from last year, and up 3% from last quarter.
While estimates were higher than expected, the increase “won’t alter price expectations much,” says Ron Plain, professor emeritus, University of Missouri-Columbia.
Seasonal trends will still still rule the coming year. Plain estimates fourth quarter prices to range from $50-$52 cwt; first quarter 2018, $58-$62; second quarter $68-$72; third quarter $67-$71. 
“The market has given producers ample opportunity to lock in profitable margins,” says Altin Kalo, senior analyst, Steiner Consulting Group, Merrimack, NH. Lower feed costs have helped create a profitable environment for growth.
While focus is often on production increases, Kalo adds, a lot of the pork supply is getting used up. “It is taking some lower prices to get that done,” he says, but it’s not hitting back onto the producer. Growth in export demand is a large factor. “Demand for pork continues to be good and it’s allowing producers to remain profitable through this expansion.”
 
“Measured Pace” of Expansion
Breeding inventory, at 6.09 million head, was up 1% from last year, and up slightly from the previous quarter.
The June-August 2017 pig crop, at 33.0 million head, was up 2% from 2016. Sows farrowing during this period totaled 3.10 million head, up 2% from 2016. The sows farrowed during this quarter represented 51% of the breeding herd.
An increase in production efficiency is credited with some of the improvement in pork supplies, with only modest increases in the sow herd.
The average pigs saved per litter was a record high of 10.65 for the June-August period, compared to 10.58 last year. Pigs saved per litter by size of operation ranged from 7.80 for operations with 1-99 hogs and pigs to 10.70 for operations with more than 5,000 hogs and pigs.
"Producers say they will continue to expand the herd by about 1% with more farrowings. Again, with the increase in pigs per litter, that will give us about a 2% greater supply of pork next year," says Chris Hurt, ag economist, Purdue University, during an interview with AgDay.
U.S. hog producers intend to have 3.07 million sows farrow during the September to November 2017 quarter, up 1% from the actual farrowings during the same period in 2016, and up 5% from 2015. Intended farrowings for December to February 2018, at 3.02 million sows, are up 1% from 2017, and up 3% from 2016.
The total number of hogs under contract owned by operations with over 5,000 head, but raised by contractees, accounted for 47% of the total U.S. hog inventory, unchanged from the previous year.
Read the full report here.