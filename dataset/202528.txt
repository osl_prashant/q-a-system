Despite labor concerns and a few weather-related issues, Southeast blueberry growers are looking forward to a good start to the season.
The Southeast blueberry season begins in Florida in mid- to late March. Georgia starts its southern highbush harvests in mid- to late April, followed by rabbiteyes in late May. North Carolina rounds out the region's production with harvests in mid- to late May.
Mark Villata, executive director with the U.S. Highbush Blueberry Council, Folsom, Calif., said general demand for blueberries remains favorable heading into March, particularly for fresh product. However, he said frozen inventories are above previous year-to-date levels.



Weather OK

Mark Greeff, vice president and general manager of the eastern region for Watsonville, Calif.-based Driscoll's Inc., said the company is looking forward to the blueberry season in the Sunshine State.
"The winter has been warmer than normal, but the early chill in the fall was critical for the plants to enter dormancy at the desired time," he said. "Compared to a year ago, the industry is poised for a much better crop that should be of around normal timing over the late March and April period. At this time, it looks like there should not be a significant overlap between different southeastern production areas."
Greeff said the bloom and pollination look good on most cultivars. "The weather over the coming weeks will obviously play a significant role in further fruit development and yield curve," he said. "Some evergreening cultivars have been in production since late December and will continue to pick until late March."
CarrieAnn Arias, marketing manager for Dole Fresh Vegetables, Monterey, Calif., said the rain patterns in the Southeast have slowed some of the production on blueberries and blackberries grown in North Carolina and Georgia during February and affected quality in some cases.


Labor tight

However, Greeff said most growers he's spoken with are less worried about the coming weather and more concerned about the labor availability for harvesting.
"It seems that labor has been a bit tight for the strawberry deal in Florida this winter, and this may be an indication of the situation over the coming spring season," he said. "More growers are using the H-2A programs, and the interest in this from growers seems to be higher than ever before."
Arias agreed, saying labor concerns in all regions could create supply challenges.


Volume up

Mario Flores, director of blueberry product management for Naturipe Farms LLC, Salinas, Calif., said the company grows blueberries in central and northern Florida, South Georgia and North Carolina.
"The Southeast will have a positive increase versus last season in all areas," he said. "Florida could be double, Georgia could be 30% up from last year and North Carolina could be up 20%, as well."
Flores said he expects general crop timing for the company to be seven to 10 days earlier than last year, which could change depending on the weather.
"As we see more 'normal' timing and volumes from the Southeast this year, pricing should fall in line with those historical periods," he said.
Stacy Spivey, the Vidalia, Ga.-based North American berry program director for Alpine Fresh, Miami, said the company has domestic blueberry production in Florida, Georgia, North Carolina and Michigan.
"We have increased organic and conventional (blueberry) production significantly in Florida, adding two packing facilities for this season," he said. "Modest increases have been made in the other production regions as well."
Spivey said Alpine Fresh began shipping light blueberry volumes out of Florida on Feb. 5 and received positive feedback on product quality from its retail customers.
Eric Crawford, president of Fresh Results LLC, Sunrise, Fla., said the company sources blueberries from central and northern Florida, south Georgia and North Carolina.
Crawford said the biggest change this season will be Florida having a crop, barring unforeseen weather events.
Last year, the Florida crop didn't develop fruit because there weren't enough chill hours.
"Florida will be up from last year due to last year's crop failure," he said. "Georgia should be up slightly from last year due to normal maturity. North Carolina could be down from last year due to Hurricane Matthew's strong storm surge, which pushed water up into the blueberry growing regions close to the coast."
Crawford expects overall pricing on blueberries to be down due to a growing supply and offshore options, such as Peru and Mexico.
"The strategy for our marketing team is to commit to seasonal fixed-price programs as much as possible," he said. "The open market will be a very volatile place to be this year."