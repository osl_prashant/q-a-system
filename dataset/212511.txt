YAKIMA, Wash. — Organic cherry production is growing in the Northwest, but not at the same quick rate as apples.
The organic share of Northwest cherry volume has increased from about 1% of total crop in 2009 to about 3% of the crop in 2016, according to B.J. Thurlby, president of Northwest Cherry Growers.
That is far less than the 10% and higher share for organic apples that many Northwest shippers report.
At 3% of the volume in 2016, organic cherry volume totaled about 500,000 20-pound cartons, Thurlby said.
Demand is high for organic cherries, but finding organic fruit is a challenge. There are not as many organic-approved tools for growers to use to control thrips and mildew, industry leaders say.
“If you can get organic cherries in the box, you’ve got a real winner,” said Bruce Turner, national salesman for Wenatchee-based Oneonta Starr Ranch Growers.
“We expect the organic cherry category to grow this year,” said Randy Abhold, vice president of sales and marketing for Rainier Fruit Co., Selah.
 

Big ticket

Price premiums were strong for organic cherries in 2016, according to the U.S. Department of Agriculture Market News Service.
U.S. average wholesale market prices for dark sweet organic cherries last year averaged $78 per carton on June 25, $60 per carton on July 2 and $67 per carton on July 9.
That compares with average conventional cherry prices of $40-42 per carton at wholesale markets for the same weeks.
CMI Orchards has organic dark sweet cherries and rainiers, said Bob Mast, president of CMI Orchards, Wenatchee.
“We have the ability to grow (organic) tonnage, depending on how the demand curve works,” he said. “We haven’t seen the big increase in demand on (organic) cherries that we have seen on some of our other commodities,” Mast said.
Already one of the higher priced retail produce items, conventional cherries have a fairly low consumption level compared with other commodities, Mast said.
The added premium for organic cherries can diminish demand, he said.
“It is something that can grow, and retailers are certainly interested in growing it,” he said.
 

Price premiums

When conventional cherries can be $4.99 per pound, the added organic premium can be hard for consumers to pay, said Mac Riggan, director of marketing for Chelan Fresh, Chelan.
Retail-promoted prices for organic cherries averaged $4.74 per carton in 2016, according to the USDA, compared with the retail average promoted price for conventional dark sweet cherries of $3.91 per carton.
The average f.o.b. for Washington conventional cherries last year was $43.32 per carton; the USDA did not report an average price for organic cherries.
Mast said CMI is exploring smaller packaging on the organic side to keep the price down. “We don’t want the sticker shock when they get to the register.”
Thurlby said the market for organic will persist and grow.
“There is a growing segment of (consumers) who would only want to buy organic,” Thurlby said.
“When you look at the mass merchandisers, the big chains, they are seeing this and they are expanding organic selections and they are asking about organic.”
Thurlby said some retailers are merchandising cherries separately, which may take away from some of their potential.
“You will see a lot of grocery stores and see a display of cherries out on the floor, then the organic is put back in a cold box,” he said.
Thurlby said the better approach to capitalize on the seasonality of cherries is to merchandise organic cherries near conventional fruit.
“I think you just have to get them out there and get them by the consumer,” Thurlby said.
“I feel like there is a bigger opportunity for organics than is being taken advantage of,” he said.
Putting up merchandising signs for organic cherries and drawing attention to the display could create even more demand for organic cherries, Thurlby said.
“If you put them back in a cold box, I think you end up selling fewer than you would have.”