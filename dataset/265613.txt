While winter wheat acres declined slightly this year due to drought in the plains, spring wheat acres are anticipated to grow by roughly 1 million acres.
According to USDA, spring wheat plantings are expected to be up, leading to an increase of 1.2 million acres in 2018. Wheat acres aren’t expected to grow in the Texas Panhandle region, but instead in the Northern Plains.
“Right now farmers in the Dakotas and Minnesota farmers have made the decision to plant more wheat than corn,” says Brian Doherty of Stewart Peterson. He says wheat price rallies last summer provided farmers in the region to lock in profitable prices for the 2018 growing season.
According to Farm Journal economist, Chip Flory, even the prices for wheat available today will give northern farmers incentive to move away from corn and soybeans in favor of spring wheat.

“I would think we could see spring wheat up even 1 million acres from where we were a year ago,” he says.
Flory says weather will be the biggest factor in spring wheat plantings.
“[For] those guys it almost feels like it’s a last second decision, because they know the weather is going to have a huge impact,” he explains. “If they have any kind of delayed planting on spring wheat that takes them to corn and then to soybeans.”
Even with an increase in spring wheat acres, Doherty points out the U.S. is still looking at the “second lowest or lowest number of wheat acres in 100 years.”