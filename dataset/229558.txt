Internet-famous eagle lays her 1st egg of the year
Internet-famous eagle lays her 1st egg of the year

The Associated Press

DECORAH, Iowa




DECORAH, Iowa (AP) — A famed Iowa bald eagle watched worldwide on the internet has laid its first egg of the year.
The eagle named Mom Decorah laid the egg a little before 7:30 p.m. Wednesday, as seen on the Raptor Resource Project eagle webcam .
The nonprofit organization says it's the 30th egg the eagle has dropped at a nest near the Decorah Trout Hatchery in northeast Iowa.