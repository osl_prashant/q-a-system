As the cherry growing environment in California changes, Modesto, Calif.-based Zaiger Genetics has come up with a couple of alternatives to the ever-popular bing variety.
“Climates are changing, and temperatures are changing,” said Leith Gardner, a fruit breeder for the company.
The bing needs plenty of chill hours in the winter, she said. Otherwise, “it doesn’t set as good a crop as it should.”
Over the past several years, the Modesto area had exceedingly warm springs, she said.
“That dries the pistils out too fast and doesn’t give them time to get pollinated, so the set is not very good,” she explained.
Two fairly new varieties — the Royal Tioga and the Royal Hazel — have been doing very well in tree sales, Gardner said. They’ve been around four or five years.
The Royal Tioga is one of the first cherries of the season to come off.
It’s a self-fertile cherry of medium to large size that ripens around April 18 and is available until the end of the month.
“The buyers seem to like it,” she said.
The Royal Hazel, which starts around May 4, is larger and firmer than the Tioga and also has been enjoying good results from buyers, she said. However, it is not a self-pollinator.
It boasts good flavor and shelf life and goes through the packing line well, she said.
The company has sold more Royal Hazel trees recently than any other, Gardner said, adding that growers, packers and consumers seem to like it.
Both the Royal Tioga and Royal Hazel bloom earlier than the bing, which means they bloom in cooler temperatures, so their pistils stay viable longer and they get a good pollination set every year.
Size is the main attribute breeders look for when developing a new variety, then color and flavor, Gardner said.
“It has to have all those attributes or it won’t be a good variety at all,” she said.
Developing varieties that perform well in California is challenging because there often aren’t adequate cold hours in the winter, and spring can be very hot.
Hot weather can cause “doubles” on the tree. That’s one thing researchers try to avoid.
Developing a commercial cherry variety can take 15 to 18 years, Gardner said.
Fortunately, because of their short season, cherry varieties don’t go out of favor as quickly as, say, peaches or nectarines, “which are constantly having to renew their varieties because consumer tastes are different,” Gardner said.
After a potentially viable variety is developed, another year or two is needed to set a good crop, see if it maintains the qualities the breeder wants then find a grower willing to chance planting it, she said.
“Five or six years after that, he can tell you if he liked it or not,” Gardner said. “It’s not a quick project.”