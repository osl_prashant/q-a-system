Early coverage of the E. coli outbreak linked to romaine lettuce from the Yuma region is rolling in.
Find a story by Ashley Nickle of The Packer here.
New York Times coverage is found here.
Gizmodo coverage here.
The Food and Drug Administration has a web page devoted to the outbreak here.
The Centers for Disease Control also has a page devoted to the outbreak here.
Here is the CDC  advice:
Advice to Consumers:
Consumers anywhere in the United States who have store-bought chopped romaine lettuce at home, including salads and salad mixes containing chopped romaine lettuce, should not eat it and should throw it away, even if some of it was eaten and no one has gotten sick. If you do not know if the lettuce is romaine, do not eat it and throw it away.
Before purchasing romaine lettuce at a grocery store or eating it at a restaurant, consumers should confirm with the store or restaurant that it is not chopped romaine lettuce from the Yuma, Arizona growing region. If you cannot confirm the source of the romaine lettuce, do not buy it or eat it.
Advice to Restaurants and Retailers:
Restaurants and retailers should not serve or sell any chopped romaine lettuce, including salads and salad mixes containing chopped romaine lettuce, from the Yuma, Arizona growing region.
Restaurants and retailers should ask their suppliers about the source of their chopped romaine lettuce.
The CDC also tweeted: E.coli Outbreak: Don’t buy or eat chopped romaine lettuce from a store or restaurant if it’s from the Yuma, Arizona growing region. If they don’t know, don’t eat it. http://go.usa.gov/xQbwb
 
Another tweet from Joe’s Pasta House alerted their followers they were taking action on the news:
“The @CDCgov has reported an #ecoli outbreak in chopped #romaine coming out of Yuma, AZ.  In being proactive to protect our guests & staff members; all of our produce is now coming out of CA & we are only serving organic baby greens until the CDC clears this situation.  #WeCare”
Later Joe's Pasta House tweeted: Update... we received the all clear from the CDC to serve the romaine coming out of CA!

Look for more communication to consumers like the tweet from Joe’s Pasta House from foodservice and perhaps retail accounts in the next few days.
Google Trends reports that in the past 24 hours the fastest rising searches related to “romaine lettuce” were:

fda romaine lettuce
romaine lettuce recall brands
 cdc romaine lettuce
cdc
romaine lettuce ecoli

The seasonal transition between Yuma region romaine production and growing districts in California central coast/Oxnard region is nearly complete, though the USDA said shipments from Arizona still accounted for about half of U.S. romaine volume as of early April.
 The USDA reported shipping point prices of romaine lettuce from Oxnard on April 12 - the USDA issued a last report out of Yuma last week - showed market prices of $16.95-18.95 per carton of 24s. Where will the market go from here?
Here’s hoping growers of romaine in other districts won’t be punished by buyers and consumers in this outbreak linked to Yuma area chopped romaine. The sooner health authorities can identify recall brands, the better for the rest of the industry.