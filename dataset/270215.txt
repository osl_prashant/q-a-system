There’s no typical mango consumer.
Consumers of many types enjoy mangoes, because they are one of the produce items showing year-over-year increases in likelihood of purchase, according to The Packer’s 2018 Fresh Trends annual consumer survey.
Fresh Trends is a survey of more than 1,000 consumers on their fresh produce buying habits. We only survey primary household shoppers and balance our demographics to that of the U.S. population.
We publish our findings every spring.
Overall, 23% of customers purchased mangoes within the past 12 months, which represented a one percentage point increase from last year.
This year, we saw the tropical fruit found a niche with specific groups of shoppers. For instance, ethnicity heavily influences mango purchase. Asian consumers were the most likely to buy mangoes overall, followed by Hispanic shoppers — a trend now in its fourth year.

The tropical fruit is also popular among young consumers, generally those age 50 and younger.
While most fruits and vegetables in Fresh Trends 2018 found the 18-39 year old consumer segment lagging behind other ages, for mangoes, in five out of the past six years, shoppers age 59 and older have been much less likely to buy the fruit than those younger than them.
Geographic region also plays a role in mango purchases. For the fourth consecutive year, Western shoppers have been more likely to grab mangoes than those in other regions.
This year Midwestern consumers were the least likely to buy when region was considered.
We also asked respondents about ripeness and found 36% of mango buyers said they always bought the fruit ripe, while 29% said they preferred to buy ripe fruit.
Only about one in five buyers (21%) said they preferred to purchase unripe mangoes.
Organic mango purchases are also on the increase, as 14% of buyers said they always bought organic mangoes.
When it came to periodic organic purchases, 35% said they bought organic fruit at least some of the time, while last year 31% said the same.