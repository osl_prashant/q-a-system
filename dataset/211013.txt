Crop calls
Corn: Fractionally to 1 cent lower
Soybeans: 3 to 5 cents lower
Wheat: 1 to 2 cents firmer
Corn and soybean futures faced mild profit-taking overnight following last week's gains. Key this morning will be if traders view overnight weakness as a buying opportunity in light of ongoing weather concerns in South America. Hot and dry conditions lingered in central Brazil and soggy conditions were seen in southern Brazil, which is not supportive of planting. Also positive, USDA announced an unknown destination purchased 227,300 MT of 2017-crop soybeans.   Meanwhile, wheat futures were lightly supported overnight by news the lowest price offered submitted in a tender from Iraq's state grain board was for U.S. supplies.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Livestock futures are expected to see a choppy tone to start the week as traders gather cash opinions and keep an eye on outside markets. Pressure on live cattle should be limited by last week's cash that took place in a range of $110 to $111.50, although trade picked up at the upper end of the range Friday afternoon. A weaker tone in the corn market is supportive for feeders this morning. Meanwhile, the cash hog market is expected to start the week $1 higher amid strong packer demand. But buying in hog futures should be limited by the premium nearby futures hold to the cash index.