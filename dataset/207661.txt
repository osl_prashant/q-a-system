The Produce Facts app is now available in an Android version. 
The University of California’s Postharvest Technology Center Produce Facts app is now available both in Android and iPhone versions, Trevor Suslow, extension research specialist and  director of the UC Postharvest Technology Center, said in an e-mail. The iPhone version, first released in January, also has been updated for easier navigation. The app for the iPhone is available in the iTunes
App Store and can be found be searching “Produce Facts”. 
 
To download the app for Android Devices, users can search “UC Produce Facts” in the Google Play Store, Suslow said.
 
App content is free and includes optimum storage recommendations, maturity and quality guidelines, and physiological and physical disorder identification tools for 132 fruits, vegetables and ornamentals, according to Suslow. 
 
With the app, users can read content in English, Spanish, or French; an internet connection is not necessary to use the information, he said.