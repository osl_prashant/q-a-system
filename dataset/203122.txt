Philadelphia-based food hub The Common Market has earned SQF Level 2 certification by the Safe Quality Food Institute.
 
The nonprofit local food distributor's audit scored the SQFI's highest rating, according to a news release.
 
"This certification demonstrates our commitment to food safety and puts us on the cutting edge of this issue for a local food hub," The Common Market founder and COO Tatiana Garcia-Granados said in the release. 
 
The food hub worked more than a year to meet SQF standards in food safety training, hazard analysis, allergen prevention, site security, crisis management, waste disposal, sanitation, and other areas, according to the release.
 
By August of last year, all of the farms supplying The Common Market in the Mid-Atlantic region were certified by the USDA-compliant, third-party Good Agricultural Practices food safety program, according to the release. Some of those suppliers were small enough to be exempt from Food and Drug Administration produce safety rule, according Carolyn Wyman, spokesperson for the market.
 
The release said that just 14% of food hubs polled in a recent National Food Hub Survey required GAP certification of their suppliers.
 
The food hub also has case-level traceability from when product is picked up at the farm to when it is delivered to customers, according to the release.
 
Garcia-Granados said the food safety certification helps small growers do business with large chains.
 
"One major reason we sought SQF certification is to make sure our small farmers aren't cut out from doing business with these large food buyers and that those they serve don't miss out on the opportunity to enjoy local, fresh and healthy fruits and vegetables," she said in the release.