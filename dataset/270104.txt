US sorghum growers fear China tariffs could cost them dearly
US sorghum growers fear China tariffs could cost them dearly

The Associated Press

OMAHA, Neb.




OMAHA, Neb. (AP) — American sorghum farmers fear they will lose their largest export market if China follows through with a tariff on their crop.
China imposed preliminary anti-dumping tariffs of 178.6 percent on U.S. sorghum this week as part of its ongoing trade dispute with the U.S.
President Donald Trump has threatened to raise tariffs on up to $150 billion of Chinese goods to counteract what he says are that country's unfair trade policies.
Sorghum farmers like Don Bloss in southeast Nebraska are caught in the crossfire. Bloss says the tariff news isn't enough for him to change what he plants this year, but farmers will have to adjust if the tariff is enacted.
Last year, Chinese buyers purchased more than 90 percent of the 245 million bushels of sorghum America exported.