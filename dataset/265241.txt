AP-Oregon News Coverage,ADVISORY
AP-Oregon News Coverage,ADVISORY

The Associated Press



Hello! Here's a look at AP's general news coverage in Oregon. Questions about coverage plans are welcome and should be directed to the AP-Portland bureau at 503-228-2169 or apportland@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times Pacific.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
Oregon at 1 a.m.
SEA LIONS VERSUS SALMON
NEWPORT — The 700-pound sea lion blinked in the sun, sniffed the sea air and then lazily shifted to the edge of the truck bed and plopped onto the beach below. Freed from the cage that carried him to the ocean, the massive marine mammal shuffled into the surf, looked left, looked right and then started swimming north as a collective groan went up from wildlife officials who watched from the shore. By Gillian Flaccus. AP Photos ORDR401, ORDR409, PDX901, ORDR413, ORDR411, RPGF403, OREUG401, ORDR403, RPGF401, PDX904, PDX902, PDX401.
INITIATIVE TITLE ORDER
SALEM, Ore. — The Oregon Supreme Court partially sided with the sponsors of a union transparency initiative, after they appealed the title given to their proposal by the state Attorney General's office. By Tom James. SENT: 220 words.
BUDGET BATTLE-WILDFIRES
WASHINGTON — A spending bill slated for a vote in Congress includes a bipartisan plan to create a wildfire disaster fund to help combat increasingly severe wildfires that have devastated the West in recent years. The bill sets aside more than $20 billion over 10 years to allow the Forest Service and other federal agencies end a practice of raiding non-fire-related accounts to pay for wildfire costs, which approached $3 billion last year. By Matthew Daly. SENT 710 words. AP Photo WX204.
LAWYER WALKS OUT
BEND — A defense attorney stunned a courtroom by walking out on his client during a hearing Wednesday in Deschutes County. SENT 300 words. AP member photo requested.
ENDANGERED SUCKERS
KLAMATH FALLS — It isn't easy being a Klamath Basin sucker. There has been a 75 percent decline in populations of two endangered Klamath Basin sucker species over the past two decades, and juvenile fish are not surviving beyond their first year of life, according to U.S. Fish and Wildlife. As part of an effort to keep the species from extinction, officials are releasing upward of 2,500 hatchery-reared juveniles into Upper Klamath Lake. By Holly Dillemuth of the Herald and News. SENT 880 words. File photo.
PORTLAND STREETCARS
PORTLAND, Ore. — Portland officials have approved a $10 million deal to buy two new streetcars with the option to buy additional vehicles later. SENT: 300 words.
EARNS-NIKE
NEW YORK — Nike reported its first quarterly loss in 20 years, due to a $2 billion tax expense related to recent changes in U.S. tax law. But the sneaker company's third-quarter results easily beat expectations and its shares jumped in after-hours trading. By Joseph Pisani. SENT: 460 words. With AP photos.
SPORTS
BKS--NCAA LEXINGTON
LEXINGTON, Ky. — The NCAA Tournament just wouldn't feel the same for Stanford if the Cardinal was not playing in the Lexington Region. By Gary B. Graves. SENT: 860 words. With AP photos.
ALSO:
—INMATE ESCAPE— Authorities say a Multnomah County inmate is missing after he allegedly stole a work truck while working as part of a cleanup crew in Northeast Portland.
—HP EXPLOSION— Authorities say two people were injured in an explosion at the HP Inc. campus in Corvallis.
—FORMER US ATTORNEY OREGON-SUSPENSION— Former U.S. attorney for Oregon Amanda Marshall has been barred from practicing law for 90 days because she lied about a sexual relationship with a subordinate.
— BICYCLIST KILLED — Eugene police say a bicyclist was killed in a collision with a Toyota Scion.
— CHIHUAHUA KILLED — An Oregon woman has been charged with first-degree animal abuse after police say she killed a Chihuahua by cooking it in an oven.
— BARGE BOAT CRASH — Officials say a tugboat and barge hit a smaller vessel on the Columbia River, tossing the smaller boat's occupants into the water.
— POLICE-RADIO TROUBLES — Officials say antenna panels on the roof of a Bend hospital were installed facing the wrong direction, causing a portion of the radio troubles for police.
— DAIRY COMPANY LAWSUIT-SETTLEMENT — The state has reached a settlement with a northern Oregon dairy company accused of endangering drinking water by mismanaging manure and wastewater.
— STANDOFF SHOTS FIRED — Authorities say a man in southwest Washington fired six shots in the direction of sheriff's deputies and surrendered after a standoff four hours later.