Book 70% of fall nitrogen, phosphate and potash at today's price. Book 50% of spring nitrogen needs and 50% of spring potash needs. Hold off on booking spring phosphate for the time being as we believe DAP/MAP prices will sink through the winter offseason.
Nitrogen -- Book 70% of fall nitrogen needs and/or 50% of your nitrogen needs for spring 2018. 

Prices on all forms of nitrogen in our weekly survey are just off the lowest prices recorded in five years.
Anhydrous ammonia gained 77 cents this week after having gained 11 cents last week. We were targeting $450 per short ton to book NH3 and since anhydrous has firmed to $451.39, we feel a change of direction is imminent.
Urea has confirmed a clear price floor is in place after softening to $316 per short ton. As with anhydrous, the last few weeks of mild upward motion in urea exhausts our appetite for risk on 100% of our 2107-18 nitrogen needs.
As NH3 and urea begin to firm, UAN solutions will follow. Consider anticipated sidedress needs and cover 50% for spring/summer UAN applications.

Phosphate -- Book 70% of fall phosphate needs but wait on booking phosphate for spring applications.

We do not expect a sharp near-term rebound in DAP/MAP, but wholesale prices are on the rise and seasonal demand will add price support.
Phosphate remains our highest-priced nutrient on an indexed basis but is still priced at a discount to expected new-crop revenue.
As with nitrogen, DAP and MAP appear to have placed a near-term, seasonal low.

Potash -- book 70% of fall potash needs and/or 50% of your potash needs for spring 2018.

Potash is also off its summertime price low and is unlikely to soften again before the first of the year.
Supply-side management on the part of North American producers is beginning to buttress retail prices.
As with phosphate, we expect potash prices to sink during the winter offseason, but we are unwilling to leave 100% of our potash price risk on the table.

Get in touch with your preferred retailer today and book 70% of your nitrogen, phosphate and potash needs for fall and be prepared to book the remainder of your needs within the next few weeks. Book 50% of your anticipated spring nitrogen and potash needs, but wait until after the first of the year to begin booking spring phosphate.