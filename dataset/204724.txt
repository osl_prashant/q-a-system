Florida has very limited commercial papaya volumes now, but University of Florida researchers say genetically modified papaya varieties designed to resist ringspot virus could spur grower interest.
The Environmental Protection Agency accepted comments through Jan. 4 on its preliminary decision to approve the plant incorporated protection (papaya ringspot virus coat protein gene) in the genetically modified papaya varieties developed by the University of Florida's Institute of Food and Agricultural Sciences.

In a comment to the EPA filed earlier in 2015, Jonathan Crane, tropical fruit crop specialist and Edward Evans, agricultural economist ‚Äî both at the University of Florida ‚Äî said the ringspot virus-resistant papaya varieties could help growers increase volume from current levels of near 300 cultivated acres. The researchers noted that the Hawaiian papaya industry has rebounded after growers there began planting a genetically modified papaya developed by the University of Hawaii in the 1980s.

"Since 1998, Hawaii has been growing and marketing genetically modified papaya cultivars, a substantial portion of which are sold to consumers in mainland U.S.," the authors said. "During the past 17 years no adverse human or environmental effects have been documented from consuming these papayas."

The University of Florida researchers said in their comment that a survey of current and potential papaya growers in south Florida indicated that more than 60% of them would be willing to grow a genetically modified papaya if it were available. In its work with the EPA, Food and Drug Administration and the U.S. Department of Agriculture to gain approval of the genetically modified papayas, the researchers said they have demonstrated that the ringspot virus resistant papayas pose no health risk to consumers and have the same nutrient content as a non-GM papaya.

"Florida producers need the option of producing papaya plants not debilitated or killed by papaya ringspot virus and to reduce costs of production by reducing the need to replant fields every 18 to 24 months," Evans and Crane said in their comment. Crane said Jan. 6 the EPA approval ‚Äî formally expected by perhaps the end of January ‚Äî is the last step in the government's part in approving the genetically modified papaya varieties. The USDA and FDA deregulated the genetically modified variety more than eight years ago.

"This would mean we could go ahead and commercialize it if we desire," Crane said. That process won't begin until the University of Florida sets the rules for how the variety will be made available, and Crane said that process could take a couple of years or more.

Crane said he believes papaya acreage in Florida could double as a result of the new varieties.

The University of Florida genetically engineered varieties are targeted for strains of the papaya ringspot virus found in Florida, but Crane said he assumes the variety eventually could be sold to growers in other countries. Propagation material for the Hawaiian papaya varieties is not allowed to be sold to growers outside of Hawaii, but the fruit can be marketed to the mainland.

"Somebody in the Caribbean might say, "Let's grow (the Florida genetically modified varieties) here,' but they may or may not be successful," he said.

Bill Brindle, vice president of sales and marketing for Homestead, Fla.-based Brooks Tropicals, said the company does not market Florida papayas, but imports papayas from the Caribbean, Belize, Guatemala and the Dominican Republic.

"Florida has a lot of other disadvantages compared with the Caribbean locations where we already grow, so I don't think we would jump on growing in Florida any time real soon," he said, citing high labor and land costs in Florida compared with other growing areas. "The other problem is, that as a general rule, our customers aren't interested in a genetically modified papaya."

Brindle said Florida's papaya output is currently very small, with limited fruit available typically harvested before maturity because of the ringspot virus and mostly sold as cooking papayas.

"It makes a lot of sense what the University of Florida has done, but unfortunately consumers aren't quite ready to receive genetically modified papayas," he said. "We're not planning to get into Florida grown papayas in the short term until we see consumers showing interest in them."

While the market reaction could cause growers to be cautious, Crane noted that Hawaiian growers have had no problem marketing genetically modified papayas to the U.S. West Coast. There is no federal requirement that genetically modified fruit must be labeled, he said.