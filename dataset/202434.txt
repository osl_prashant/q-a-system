FreshPACKmoves 2017 is bringing together specialists to talk about e-packaging for fresh produce, safety and logistics. 
The event, an educational seminar from Cal Poly's Packaging Program, is May 22-24 at the Hyatt Regency Hotel in Monterey, Calif. 
A series of presentations and panel discussions on May 23 focus on e-packaging innovations, food safety, temperature-sensitive logistics, traceability, and the role of packaging in food waste. The panels will provide participants with opportunities to ask questions about packaging challenges facing their industries, according to a news release.
Attendees also can browse exhibits to connect with suppliers offering products and services for perishables. 
A May 24 field tour will give deeper insight into issues through first-hand observation of grower-packer-shipper operations, according to the release. 
Speakers at the event are: 
Cory Zavagno, director of national replenishment at Blue Apron; 
Tom Karst, national editor of The Packer;
Cynthia Klein, CEO of CK Solutions Group LLC;
Quint Marini, packing engineering manager for UPS Customer Solutions;
Ronald Clark, co-founder and chief supply officer of Imperfect Foods;
Michael McCartney, principal of QLM Consulting;
Peter Hill, operations manager for North, Central, and South America, Zespri International;
Robert Verloop, consultant; and
Capt. Brent Patterson, U.S. Marine Corp, officer-in-charge for the West Coast Food Management Team
Registration for the event is available online.