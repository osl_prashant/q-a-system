Fresh fruit and vegetable importer CarbAmerica added Troy Mesa to its in-house sales staff.
Mesa has 30 years of produce and agricultural experience, most recently having worked for Sol Group Marketing as an account manager for retail, foodservice and wholesale customers, according to a news release.
Over the course of his career, Mesa worked for Rosemont Farms, Pero Family Farms, the California Asparagus Association, Rivermaid Trading and North Bay Produce. He earned his degree from Chico State University.
"We've been looking for an individual with his experience," said Jeff Friedman, president of CarbAmericas, in the release.
"Troy's knowledge and experience will fit nicely with the different categories we manage here at CarbAmericas."
Mesa will be working in CarbAmericas headquarters in Fort Lauderdale, Fla.