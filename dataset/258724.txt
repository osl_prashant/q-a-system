Citrus marketer Limoneira expects its lemon volume to pick up significantly in the coming years.
The company has another 1,500 acres coming into production over the next four years and plans to plant another 500 acres in the next two years, president and CEO Harold Edwards said on an earnings call March 8.
Limoneira projects to add between 900,000 and 1.3 million cartons over the next four years at a rate of roughly 250,000 cartons per year.
The company will also be increasing its acreage by recruiting third-party growers.
“Through the first quarter we are way ahead of our schedule and somewhere around the order of magnitude of 300,000 cartons of new fruit recruited (for 2019),” Edwards said on the call.
Limoneira currently grows fruit in the U.S. and Chile and sells and markets fruit for growers in South Africa, Chile, Argentina and Mexico.
Edwards noted the company has been pleased with its Santa Paula, Calif., packinghouse that came online in 2016. He said it allowed for a big increase in efficiency in 2017 and can handle about double the number of lemons that went through it last year.
 
Earnings report
For the first quarter of 2018, Limoneira reported $31.6 million in net revenue, up 12% from $28.1 million for the first quarter last year.
“We are pleased with our first quarter fiscal 2018 results, generating consolidated revenue growth of 12% and positive (earnings before interest, taxes, depreciation and amortization) in a seasonal soft quarter,” Edwards said in a news release. “Our core citrus business is performing consistent with plan, and we are encouraged by the early results from our strategic alliance with Suntreat, which is advancing our competitiveness in the orange and specialty citrus category. We continue to seek new avenues to build upon our One World of Citrus marketing program and improve the availability of fresh citrus to our growing ranks of global customers.”
Limoneira moved about 912,000 cartons of lemons at an average price of $26.32, up from about 909,000 cartons at $23.10 each during the first quarter of 2017.
Orange sales were $1.3 million in the first quarter, up from $500,000 for the same period last year.
 
Weather effect
That jump in revenue represented a return to expected production after rain events in 2017 interfered with harvesting, Edwards explained.
“It kept us from getting our crop into the packinghouse and then into the export markets, so we were almost completely shut out of our export markets last year, where we in essence butter our bread with our oranges,” Edwards said on the earnings call. “This year, with our new alliance with Suntreat and more cooperation from Mother Nature with weather, we’re much more on track with our orange shipments.
“It was really more of a function of last year being bad and this year returning to normalcy, which is great news (as far as) ongoing sustainability of the orange and other citrus performance, but also then highlights how substandard last year’s performance was,” Edwards said.
He described markets as strong but noted that the Thomas Fire in California has had a negative effect on the average f.o.b. price for the company.
“The issue that we will face for the remainder of our fiscal year 2018 relates to quality issues that were the result of very, very strong winds that accompanied that terrible Thomas Fire that we all heard about and Limoneira experienced firsthand, and so how that manifests itself in our forecast ... is that creates more second-grade fruit,” Edwards said.
The fire also affected avocado production for the company.
“We believed initially we had somewhere between 9-10 million pounds hanging in the trees, and after the winds and the fire, we downgraded that to 6 million pounds, so we felt like we lost about a third of the fruit in the wind events, and that was pretty consistent across all growers down in this part of California,” Edwards said.