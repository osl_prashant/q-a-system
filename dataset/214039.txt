Sponsored Content
Weed scientists agree marestail is one weed growers may want to take control of and get a jump on managing in the fall.
As corn and soybean harvest wrap, growers should start scouting for winter annuals, primarily marestail.
This tough, resistant to glyphosate and ALS-inhibiting herbicides weed is a surface germinating, facultative winter annual weed, meaning no-till operations, where there is not much surface disruption, is an ideal place for it to germinate.
Fall herbicide application of a 2,4-D product is recommended as a base treatment.
See these university postings for more information on fall marestail management:
UNL CropWactch: Fall is optimal for marestail management
ISU ICM News: Fall marestail (horseweed) management
Sponsored by Nufarm