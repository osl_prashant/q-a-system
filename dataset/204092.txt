Perry Hill, an infield coach for the Miami Marlins, has spent a lifetime in baseball.Hill uses a simple process for helping infielders to position themselves to field and throw more effectively. Doing so enables Hill to put players into position where they can succeed.
That's a lesson any manager can learn. And so let me offer three tips for any manager seeking to connect with his players more effectively:
Tell them what you want them to do. Set clear expectations for the job as well as performance. Give them concrete examples of what you are asking. Provide them with the resources they need.
Teach them how you want them to behave. Set the right example for how a leader conducts himself in the workplace.
Make time for them. Let your employees know you care about their performance. Practice an open-door policy so that they know you are available when they need you.
Employing the 3T model won't make you a Perry Hill but it will signal to your employees that you are serious about helping them succeed.
We want to perform for people who care about us and want to make us better.