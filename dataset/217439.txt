Western Growers is hosting an event to bring agricultural technology leaders to the Imperial Valley growing region.
The Feb. 8 event, “Innovation in the Imperial Valley,” will bring growers, researchers, technologists and entrepreneurs for discussions and education sessions on future farming technology.
The first-time event in the Imperial Valley will include outdoor demonstrations in addition to indoor panels and networking, said Hank Giclas, senior vice president for strategic planning, science and technology for Western Growers.
Western Growers hosted a similar ag tech event last year in Fresno that was very successful, Giclas said. About 200 people registered for last year’s Fresno event. 
“I think one of the things we are trying to do in conjunction with (Western Growers’) Center for Innovation and Technology is that we are trying to get ag tech and venture capital leaders out of the Bay area and into actual production areas,” he said.
Giclas said some of the topics covered will include automation, data and field demos. 
“I would have to say automation is our highest priority,” Giclas said. “We are talking about how we can accelerate development in that space.”
Set for 3 p.m. to 7:30 p.m. Feb. 8 at the Stockmen’s Club in Brawley, Calif., the event includes:

Showcase of ag tech startups and technologies;
Panels on automation and water technology; and 
Pitch session from ag tech startups.

The event also will include dinner and a keynote speaker on innovation, along with a discussion with agricultural leaders in the Imperial Valley, according to the release. Registration is available online.
Giclas said Western Growers is committed to starting conversations about innovation and technology in growing regions. A similar event is being planned for late February in Phoenix, Giclas said, in addition to later workshops for Fresno and Salinas.