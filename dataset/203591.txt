This summer, the Philadelphia Wholesale Produce Market turned five years old.
Following numerous delays due to building issues, produce distribution at the 667,000-square-foot market officially began on June 5, 2011.
Wholesalers who made the move from the older facility between Packer and Pattison avenues say the costs to relocate into the $218.5 million operation was worth it.
"This new market has helped increase our business," said Tom Curtis, president of Philadelphia's Tom Curtis Produce Inc. "It's the only market in the country where one can enter, buy their product, assemble it and load the trucks in an entirely self-contained refrigerated atmosphere. In 30 to 45 minutes, they're out of here. Try to do that in another market."
Though time has passed quickly, the anniversary is a good time to reflect on the value the facility brings to customers, suppliers and wholesalers, said John Vena Jr., president of John Vena Inc., Philadelphia.
"We have certainly all seen an increase in business across market segments since moving from our old location," Vena said. "Besides creating well-paying jobs, we provide the most modern wholesaling environment in the country."
Many people are interested in becoming a part of the market, said Chip Wiechec, president of the Philadelphia-based Hunter Bros. Inc.
The facility should be at full capacity within a year, he said.
"You look at our market. It's still the newest and most modern market in the U.S.," Wiechec said. "It's still the model not only for our country, but also for the world."
After three consecutive weeks of heat advisories in July and early August, the building proved its worth in maintaining the cold chain and preserving product integrity, said Mark Levin, co-owner of Philadelphia's M. Levin & Co Inc.
"The facility has not let us down as far as being able to maintain the quality and integrity of the fruits and vegetables," he said. "With all the government regulations coming down on food safety, airborne diseases and all the other things, this building is the wave of the future. It eliminates 95% of those problems."
There was a cost to relocate but the change was necessary, Levin said.
"Everyone looks at their bank accounts and asks if they can afford to do this," Levin said. "We looked at it and said we could not afford not to do it."
On the old market, Levin said people would stand and watch produce samples in the unenclosed units melt in front of their eyes in the overbearing heat. Product would freeze in the winter.
With the new operation, produce maintains its quality, he said.
"That in itself is worth a lot of money to us," Levin said. "There is a lot of money in no loss of shelf life."
Mike Maxwell, president of Procacci Bros Sales Corp., Philadelphia, said one customer's tractor-trailer refrigeration unit was always straining and would never automatically turn off reaching proper temperature following loading at the old market.
Today, his unit immediately shuts off after hitting the correct temperature before driving away from the market, he said.
"In the produce business, the first step you take is to take care of your product," Maxwell said. "The old market was exposed to all those temperatures. We recently went through a heat spell of seven days above 90 degrees. The people who buy from us see product last long. This is the most modern market."
Christine Hofmann, the market's marketing coordinator, said the facility serves many customers throughout the Northeast, into Ohio, north to Canada with some distributors trucking product to and from Florida.
"This facility is crucial and we are really proud of what we've done here in Philadelphia," she said. "The New York market is obviously north of us but we sometimes draw those customers because our market is really accessible, clean and easy to use."
Hofmann said many operators of farmers markets are patronizing the market.
Construction on the plant that can store produce in 550,000 square feet of coolers began in the fall of 2008.
The 48-acre site on Essington Ave. is twice as big as the older south Philadelphia market.