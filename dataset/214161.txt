Photo courtesy Sunny Valley International

California Giant boosts volume
Blueberry volume from Chile should be up 12% to 15% this year for Watsonville, Calif.-based California Giant Berry Farms, said Nader Musleh, executive director of the blueberry division.
He attributed the increase to the addition of three growers in the southern region that should extend the season and provide steady volume from November through March.
The firm also has added another organic grower, which will enable Cal Giant to expand its four-year-old organic program by about 50%, though the organic program is not a large part of the company’s volume, he said.
 
Naturipe invests in Chilean plants
Santiago-based Hortifrut SA, the Chilean partner of Naturipe Farms, Salinas, Calif., has made a multimillion dollar investment in facilities and operations in Chile, especially in its blueberry packinghouses, where new technology and upgraded packing equipment has been installed or expanded this season, said Mario Flores, director of blueberry product management for Naturipe.
New technology was installed in all of the company’s major packinghouses in Chile.
 
Oppy expands Ocean Spray label
The Oppenheimer Group, Vancouver, British Columbia, continues to expand the Ocean Spray label in all of Oppy’s berry categories, said Jason Fung, director of category development.
“We’ve had a longstanding partnership with Ocean Spray on their fresh cranberries,” Fung said. “In June, we came to market with an entire berry category lineup under a very highly recognizable consumer brand in Ocean Spray.”
 
Early arrivals for Miami broker
Chilean blueberries arrived earlier than usual this fall at Miami-based The Perishable Specialist, said Frank Ramos, president and CEO.
The company, which is marking its 16th anniversary, had received several shipments by Oct. 9, which is “way early,” he said.
The company will continue to receive air shipments as volume builds up until around Thanksgiving. Then it will switch to ocean arrivals, which should stay strong until mid-March, he said.
 
Volume up 10% for Sunny Valley
Chilean blueberries will be up about 10% from last year for Sunny Valley International Inc., Glassboro, N.J., because of additional volume from growers, said Bob Von Rohr, director of customer relations.
Air arrivals of conventional and organic blueberries were expected by mid- to late October, with vessels arriving in late November, a little earlier than last year, he said.
The company will ship 6-ounce, pint and 18-ounce containers.
Organic volume also will be up about 10%.