Global dairy prices rose for the fifth time in a row in fortnightly auctions, putting to rest any jitters that a recovery in dairy in 2017 was temporary.The Global Dairy Trade Price Index climbed 3.2 percent, with an average selling price of $3,313 per tonne, in the auction held in the early hours of Wednesday morning.
After two years of declining prices, farmers and analysts had been concerned that a 50 percent rebound during 2016 could be temporary. Prices were also dented at the beginning of the new year as global supply increased.
Much of the gain was for high-fat products such as butter, which rose 11.2 percent, as demand from Asian markets increased.
"Strong demand for milkfat is attributed to many consumers in developed nations eating more natural foods rather than artificial or processed foods," said Amy Castleton, analyst at AgriHQ.
"Some of the poorer nations are also developing a taste for butter. Cakes and cream are being consumed more often by wealthy Asians," she added.
Prices for milk powder also rose, though more modestly. Whole milk powder prices gained 1.3 percent while skim milk powder prices grew 1 percent.
A total of 21,236 tons was sold at the latest auction, falling 6.2 percent from the previous one.
The auction results can affect the New Zealand dollar as the dairy sector generates more than 7.0 percent of the country's gross domestic product.
However, market reaction was relatively muted, with the Kiwi currency rising from $0.6868 to $0.6894 after the auction, but then tracing back down to around $0.6885.
GDT Events is owned by New Zealand’s Fonterra Co-operative Group Ltd , but operates independently from the dairy giant.
The auctions are held twice a month, with the next one scheduled for June 6.