Nitrogen (N) fertilizer is one of the most critical inputs in corn production. Its importance is highlighted in years when excess rainfall leads to N deficiencies and yield reductions. Under prolonged wet field conditions and warm temperatures, N can be lost from the soil. Losses may be moderate or severe, depending on the form of N fertilizer applied and the extent of wet, warm conditions that favor its loss.N stabilizers (also called additives) are available to help reduce N losses from the soil. These products must be used with compatible N formulations, and their use should also be based on time of application and field and climate considerations to maximize their benefits.
Common N Fertilizers
The most common forms of N fertilizer include anhydrous ammonia, urea and urea-ammonium nitrate (UAN) solutions.
Table 1.N fertilizers most commonly used for field crop production in North America

Anhydrous ammonia (NH3) is the most basic form of N fertilizer. Ammonia, a gas at atmospheric pressure, must be compressed into a liquid for transport, storage and application. Consequently, it is applied from a pressurized tank and must be injected into the soil to prevent its escape into the air. When applied, ammonia reacts with soil water and changes to the ammonium form, NH4+. Most other common N fertilizers are derivatives of ammonia transformed by additional processing, which increases their cost. Due to its lower production costs, high N content that minimizes transportation costs and relative stability in soils, NH3 is the most widely used source of N fertilizer for corn production in North America.
Ureais a solid fertilizer with high N content (46%) that can be easily applied to many types of crops and turf. Its ease of handling, storage and transport; convenience of application by many types of equipment; and ability to blend with other solid fertilizers has made it the most widely used source of N fertilizer in the world.
Ureais manufactured by reacting carbon dioxide (CO2) with NH3in two equilibrium reactions:
2NH3+ CO2‚άν NH2COONH4ammonium carbamate
NH2COONH4‚άν (NH2)2CO + H2Ourea + water
The urea molecule has two amide (NH2) groups joined by a carbonyl (C=O) functional group.

UAN solutionsare also popular N fertilizers. These solutions are made by dissolving urea and ammonium nitrate (NH4NO3) in water. The composition of common N solutions is shown in Tables2and3.
Table 2.Pounds of urea and NH4NO3in 100 lb. of UAN solution

Table 3.Percent of N by type in various UAN solutions

AsTable 3indicates, one-half of the total N in UAN solutions is NH2- derived from urea; one-fourth is NH4+ derived from NH4NO3; and one-fourth is nitrate N (NO3-) derived from NH4NO3.
For more information about N sources, click here.