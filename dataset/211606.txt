Eden Prairie, Minn.-based third-party logistics services provider C.H. Robinson is collaborating with Transflo to offer carriers a discounted electronic logging device solution. 
“We understand the extensive time and cost that can be involved in equipping an entire fleet with ELD technology,” Kevin Abbott, CHR vice president of North American surface transportation, said in a news release. “This collaboration helps carriers connect with an evolving ecosystem of shippers and loads and become part of the digital economy without making a significant capital investment.”
 
The offering provides carriers with a compact and easy-to-install electronic logging device.
 
While the ELD solution supports all carriers, the data analytics and advanced functionality make it especially appealing to carriers with fleets ranging from 11 to 400 trucks, the release said.
 
 
“We are working to simplify the purchase and implementation of ELD solutions for our carrier network,” Bruce Johnson, CHR’s director of capacity development, said in the release. “C.H. Robinson contract carriers are familiar with the Transflo brand and recognize that our collaboration offers a simple yet comprehensive mobile and electronic logging solution.”
 
In 2012, legislation required the Federal Motor Carrier Safety Administration to develop a rule mandating the use of electronic logging devices.
 
Federal regulations give fleets until December to implement certified electronic logging devices to record hours of service data.
 
Fleets already equipped with electronic logging technology will have until December 2019 to ensure compliance with the published specifications.