No one needs to be reminded that modern media ‚Äî including so-called "serious" news shows ‚Äî is in the business of providing a 24-7 menu of infotainment.Even something as serious as the political campaigning to elect a new president is all about the horse race, not the issues involved. Watch even an hour of "analysis" from the pundits, and you'll find out more about the candidates' peccadillos than anything substantive about national security, economic policy or any of a myriad of serious challenges facing the American electorate.
But the worst part of the prominence of celebranistas among the nation's (alleged) thought leaders isn't so much the unwarranted publicity movie stars and athletes seem to command, but the tendency to attribute credibility to people who might be talented in their field but have zero expertise once the subject strays from talking about how they "never stopped believing in their dreams."
Case in point: Jennifer Lawrence, the actress well-known for her acclaimed role in the "Hunger Games" franchise, is in the spotlight ‚Äî not for her tips on acting or insider's perspective on Hollywood politics, but for what to eat in the evenings.
Seriously. And it's not even Lawrence who's garnering the attention ‚Äî it's her personal trainer!
Of course, the trainer's advice is "fascinating," according to that well-regarded scientific journal Glamour magazine.
Dalton Wong, the trainer who's also worked with such superstars as Amanda Seyfried ("Les Miserables," "Mamma Mia!") and Zoe Kravitz ("Mad Max Fury Road," "X-Men: First Class"), you should know, advised Lawrence that eating meat at dinner is a big no-no.
An attempt at credibility
Now, of course, if Jennifer Lawrence is following his nutritional advice, we all should be following suit, right? Look at her!
Wong claimed that eating meat in the evening "will cause your body to "go into overdrive" to break it down for digestion." Instead, he recommended relegating meat to breakfast or lunch.
Even Glamour was hard-pressed to take his statement seriously, but they made a yeoman effort to establish some credibility for his "no meat at night" messaging.
"Is this a real thing?" the magazine's online article asked. "Kind of," was the response from certified dietitian-nutritionist Lisa Moskovitz, R.D., CEO of NY Nutrition Group.
Which neatly sums up how celebrity-driven dietary science ought to be evaluated: "Kind of" credible.
"While it is true that high-protein foods require extra time and energy to properly digest and break down," Moskovitz stated, "it depends on how much protein you're eating, how active you are at night, and other factors in your schedule. Simply avoiding meat at night is a very broad and vague statement and is not a necessary rule to live by."
End of story? No, no, no. We're talking a big-time actress here, people ‚Äî and her personal trainer-to-the-stars!
So Glamour took another swipe at wrapping Wong's advice in serious science by quoting celebrity nutritionist Beth Warren, author of "Living a Real Life With Real Food." And once you've got a "best-seller" on your resume, you're qualified to weigh in on any and all celebrity lifestyle pronouncements.
"The concern with eating meat at night is that it may leave you with a heavy stomach," Warren told the magazine. "And that can disrupt your sleep or even contribute to acid reflux."
I'm not sure that "heavy stomach" is an actual diagnosis listed in the Current Medical Diagnosis and Treatment manual, but it gets worse.
All meat is not created equal, Warren cautioned.
"Red meat like rib-eye and T-bone steaks may cause issues," she stated, "while leaner cuts like chicken and fish may not be a problem."
Nice. After the big tease of Glamour's "Why You Shouldn't Eat Meat at Night" headline, we end up right back in the 1980s, with the "red meat bad, white meat good" meme that has been so thoroughly discredited.
Which is right where 99.99% of celebrity advice needs to be stashed.
In the trash bin of history.
Dan Murphy is a food-industry journalist and commentator