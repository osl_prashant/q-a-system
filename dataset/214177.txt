British Columbia-based greenhouse vegetable grower SunSelect Produce Inc. has promoted Len Krahn to CEO.
Krahn began working for SunSelect in 1993, according to a news release. He most recently was COO and vice president of sales for the company.
Krahn is one of four siblings who manages the company’s facilities in the U.S. and Canada, the release said.
He is taking over the role from previous CEO, Reinhold Krahn.
“We’re pleased to see Len take the reins at SunSelect,” John Anderson, The Oppenheimer Group’s chairman, president and CEO, said in the release.
“He has a proven track record of strong and thoughtful leadership alongside his practical expertise of all facets of their operation. We are looking forward to building our business with this excellent grower through Len’s innovative management style and strong go-to-market collaboration.”