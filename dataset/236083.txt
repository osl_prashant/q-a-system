Minnesota farmers tests water quality
Minnesota farmers tests water quality

By JOHN WEISSPost-Bulletin
The Associated Press

PRESTON, Minn.




PRESTON, Minn. (AP) — "We brought in some dirty water," Joni Mehus said, holding a glass jar filled with brown water from field runoff.
She and her husband, Bryan Mehus, recently went to the National Trout Center to test the runoff water and some cleaner-looking spring water and well water from their rural Spring Grove farm. The water was tested for nitrates, a pollutant in itself but one that also tends to indicate if other contaminants are present.
A large, confined hog feedlot is being proposed for nearby and the Spring Valley couple wanted to get baseline data to learn if there are changes if the feedlot is built.
"We're just wanting to make sure it's in a good location," she told the Post-Bulletin .
If the feedlot's manure lagoon, which can hold up to 975,000 gallons, should break, the manure could run into a trout stream or the Root River, both nearby. The well and spring are on their land and the dirty water was runoff from land they and the feedlot proposer own.
They weren't worrying about nitrates until the feedlot was proposed.
"We are now," Bryan Mehus said.
The federal safe drinking water level is 10 parts per million. The Mehus's spring tested at 1.5 ppm, the well at 0.9 ppm and the runoff at 5.39 ppm.
George Spangler, who's on the center's board and is an expert in aquatic ecology, told them "we have a saying in the trout community, 'We all live downstream.'"
Modern lagoons are better but not perfect, he said. He suggested feedlots be required to post bond before they are built so there is money for cleanup in case of a failure.
The center began the free testing program several years ago in winter because nearly all the water from springs and creeks is from ground water, not runoff, he said. While some trout anglers came in with water, many people came in because they were concerned about what they are drinking.
"People are generally aware of the fact that water quality is declining and they are concerned about the quality of the water in their wells," Spangler said.
Before the Spring Grove couple came in, Ed Pfannkoch, of rural Chatfield, came in with water from a spring on land he moved to a few years ago. He's an analytical chemist by training so, "I have a natural interest in that," he said.
The spring tested at 6.47 ppm but he was told it came from groundwater-bearing rock called aquifers closer to the surface and they tend to be more contaminated.
"That didn't seem too bad to me," he said "It's within the safe range anyway."
The expert there was Jeff Broberg, a retired geologist and president of the trout center. He is also a leader of the new Minnesota Well Owners organization.
He said a 2013 Minnesota Geological Survey study of the region found, in general, that much of the water farther west is more contaminated because it comes from higher aquifers. Springs, wells and streams in the eastern part come from aquifers lower down and often protected by impervious layers of rock, he said.
The worst place for nitrates is south of St. Charles where some wells test as high as 80 ppm because they are closer to the surface. The soil is also sandy, so there's more chance of fertilizer spread in spring ending up in the ground water, he said. At such high levels "you can kill cattle," he said, and it's very bad for people, especially those very young.
Areas to the far west tend to have levels of 6 to 9 ppm, those in the middle are closer to 4 to 6 ppm and those in Houston County are zero to 3 ppm. It's possible some of the worst water was contaminated several decades ago, he said.
Broberg said he was in Brown County a few months ago to talk about water and well testing because the county board didn't approve of having the state offer free nitrate testing for fear farmers would get the blamed for bad water.
"They feel threatened by the blame game, and I don't think people are offering solutions to them," Broberg said. "They fear that they will lose cropland."
Trying to get data to blame others is not what the trout center or well owners group want to do, he said.
You have to know what is contaminated and then "you start moving toward conservation management," Broberg said. If your well is bad, you can use reverse osmosis or go to bottled water, he said.
"Knowledge is power," he said.
___
Information from: Post-Bulletin, http://www.postbulletin.com


An AP Member Exchange shared by the Post-Bulletin.