South African citrus is arriving to the U.S. in volume in July. 
The first vessel of South African citrus in the U.S. this summer arrived in early July at the Port of Philadelphia, according to the Citrusdal, South Africa-based Summer Citrus from South Africa. The shipment was mainly navel oranges and easy peelers.
 
The group represents South African citrus growers who consolidate their logistics, marketing and sales efforts to export citrus to the U.S., according to a news release.
 
“We are proud partners of Summer Citrus from South Africa,” Leo Holt, president of Holt Logistics, at the port, said in the release. “Their dedication to this market has been steady, bringing excellent summer citrus here for the past 17 years.”
 
Promotional efforts in the U.S. include a “Swing into Summer” campaign that includes influencer partnerships to expand the brand’s reach, according to the release.
 
“We’re excited that Summer Citrus from South Africa producers have once again teamed up with Seatrade to bring dedicated shiploads of fresh and delicious citrus from sunny South Africa to eager consumers in the U.S.,” Howard Posner, general manager of Seatrade USA, said in the release.
 
According to the U.S. Department of Agriculture, U.S. imports of South Africa citrus in 2016 were valued at $56.3 million, compared with $60,3 million in 2015 and $51.9 million in 2014.