Produce sales are trending upward, but not dramatically, according to produce vendors there.
Retail sales, we believe, are following more of a population demographic, so they're marginally up in the St. Louis area," said Dominic Greene, vice president of operations and sales manager at United Food and Produce Co. Inc., a St. Louis wholesaler founded in 1922.
Some stores report "flat" sales while others report growth, Greene said.
"It's kind of holding its own to slightly up in St. Louis and the surrounding area," Greene said.
Weather issues caused some problems with the summer local deal, but overall, things worked out for area growers, said Dan Pupillo Jr., president of Midwest Best Produce Co. Inc. in St. Louis.
"This year sales have been good; we can't complain," Pupillo said.
Weather problems, both in the area and in other growing regions, caused a few problems, Pupillo said.
"We've been chasing to catch up the whole summer," he said. "We're finally getting there. Supply has been steady and quality decent, so I can't complain."
Traffic at the St. Louis Produce Market, along the Mississippi River in downtown St. Louis, fluctuates with the weather, said Jim Heimos Jr., president of Heimos Produce Co., a wholesaler on the market.
"It's been hard this year, but the market's pretty strong," he said.
Trends have generally pointed toward sales growth, but Heimos said he couldn't identify any area that stood out.
"To be honest, it's the typical full case," he said.
Competition has defined the retail marketplace in recent years, with the appearance of chain stores such as Lucky's Markets, Ruler Foods and Fresh Thyme Farmers Market, said Steve Duello, produce director at Dierbergs Markets in Chesterfield, Mo..
"I think the whole area can say the same thing, and that is there's a lot of competition," Duello said.
Newcomers have changed the dynamic of the business, but more-established stores have been aggressive, too, Duello said.
"They all peck away at you," he said.
As a result, Duello said, sales, while "not bad," are "somewhat flat," Duello said.
Greg Lehr, produce director at Straub's Markets, based in suburban Clayton, Mo., agreed.
"It's becoming kind of saturated; we've got a lot of competitors now. There are a lot of new players, a lot of chains," he said.
Wholesaler Vaccaro & Sons Produce Co. reported "very strong" sales.
"Everybody wants fresh; everybody wants local," owner Dale Vaccaro said. "It's summertime. Business seems to be good for everybody."
Duane Talbert, salesman and partner at wholesaler Adolph & Ceresia Produce Co. said his business was brisk.
"One of the keys to our success is communication," he said when asked what was driving the sales.
The emergence of new restaurants is feeding the upward sales trend, said R.J. Barrios, buyer for Sunfarm Foodservice Inc., a foodservice-focused wholesaler in St. Louis.
"It's a growing market of small restaurants around here; it's definitely one of those new, niche restaurants that are keeping us busy," he said.
It's happening "all over the area," he said.
The summer local deal was central to sales in mid-August, said Steven Mayer, vice president of produce at Schnuck Markets Inc., a St. Louis-based retailer that operates about 60 stores in the area.
"Local for all of produce is starting to become very important," he said.
The organic and value-added categories were growing, as well, Mayer said.
"Convenience items are getting more play, so I think customers are trying precut veg and those kinds of things," he said. "I think people in general across the country and here are buying more produce, which is good to see."