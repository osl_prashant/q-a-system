Steep price swings were seen for fed steers in the past year, especially in recent months.Look for more of that in beef in coming months with lower prices ahead. However, annual average fed-steer price for 2017 can end near that received last year. Declines resume in 2018, say University of Missouri Extension economists.
Domestic demand and exports help prices in face of growing supplies, say Scott Brown and Daniel Madison.
The economists updated their outlooks in the 2017 MU Food and Agricultural Policy Research Institute (FAPRI) update.
Prices swung from below $1 per pound in mid-October 2016 to near $1.45 per pound in May of this year. Prices softened on two counts. Summer grilling ends, cutting demand. Also, large cattle-on-feed supplies helped push prices down.
A bright spot in beef supplies comes as feed yards send cattle to market at lighter weight. This cuts meat tonnage. With recent higher prices, they pushed cattle forward faster.
Slaughter weights now trail year-ago levels for the 15th month running. Expect weights to regain in 2018. "That'll add only modest tonnage as more cattle go to market," Brown says.
Projected U.S. beef cow herd growth continues through 2019. Cows will reach 31.8 million head, up from 31.1 million now. By end of the outlook in 2022, the herd will drop back to 31.1 million cows.
Help to producer prices came from surprising growth in demand for quality beef. August saw record-high prime-choice price spreads for boxed beef. At times, buyers paid more than $55 per hundredweight premiums for top-grading USDA prime.
Per capita meat consumption remains strong as retail beef prices rose.
The growth in beef tonnage comes amidst increases in all meats. Pork, chicken and turkey compete for consumer dollars. However, prime beef sees little competition from low-price offerings.
Brown and Madison are in the Department of Agricultural and Applied Economics. That's in the MU College of Agriculture, Food and Natural Resources.
Their livestock outlook joins other commodities in the MU FAPRI midyear update. That's atfapri.missouri.edu.