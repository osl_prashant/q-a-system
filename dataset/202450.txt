Dan Coborn, 86, former chairman, president and CEO of Coborn's Inc., has died.
 
He was CEO of the St. Cloud, Minn.-based employee owned grocery chain from the late 1950s until 1999, according to a news release from Coborn's.
 
Coborn began delivering groceries when he was 10 years old, according to the release, and his personal characteristics of tenacity and courage of convictions allowed him to excel.
 
Coborn graduated from Minnesota's Sauk Rapids High School in 1948 and attended Saint Johns University. He graduated with a degree in economics in 1952.
 
In 1959, Dan and his brothers were thrust into leadership of Coborn's Inc. after their father, Duke, died suddenly.
 
The brothers opened a second store in 1963, according to the release, and under Dan Coborn's leadership the independent grocery expanded. The retailer now has 8,000 employees and 54 stores across Minnesota, North Dakota, South Dakota, Iowa, Illinois and Wisconsin.
 
Coborn was active in charitable giving and in 1999 Coborn's Inc. was named one of the Ten Most Generous Companies in America, according to the release.
 
Coborn was named Minnesota Grocer of the Year by the Minnesota Grocers Association in 1987 and also received recognition and honors from College of Saint Benedict, National Grocers Association, St. Cloud Area Chamber of Commerce, St. Cloud State University and United Way of Central Minnesota, according to the release. In 2002, he received the Father Walter Reger Distinguished Alumnus Award from Saint Johns University.
 
"It is rare that a person has the opportunity to work so closely with their father for most of their life," Chris Coborn, son of Dan and current president and CEO of Coborn's, said in the release. "I was blessed to have had my dad be such an influential personal and professional mentor."
 
Coborn is survived by his wife of 64 years, Mabel, five children, ten grandchildren and one great grandchild, according to the release.