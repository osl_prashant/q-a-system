Editor's Note: The article that follows is part of the 13th edition of The Packer 25 Profiles in Leadership. These reports offer some insight into what drives successful people in produce. Please congratulate these industry members when you see them and tell them to keep up the good work.

When Jackie Caplan Wiggins was young, her trail-blazing mother Frieda, who introduced kiwifruit to the U.S. and built her own specialty produce company, was just Mom.
Though she didn’t bake cookies, she was always available for her children.
Wiggins did her share of 2 a.m. shifts at the Los Angeles Wholesale Produce Market before earning a business degree at San Diego State. After graduation she traveled the world, telling her family she’d never move back to Southern California.
While traveling through the U.S., however, she realized how much she enjoyed working in produce, where every day is different and challenging and people are passionate about what they do.
In 1983, she asked older sister Karen Caplan, now CEO, about joining the family business, now based in Los Alamitos.
“I’m not sure who was more surprised,” she said.
Starting in sales, Wiggins worked with buyers at the market and in supermarkets across the country. Since 2012 she’s been chief operating officer.
She describes her leadership style as honest, open and collaborative. She makes sure she knows the challenges of each department, from IT to food safety, and says it’s OK for a leader to make mistakes as long as you learn from them.
She also works hard to understand the communication style of the people she deals with. For example, while she prefers to talk through a problem, her sister Karen is a visual learner.
“When she sends an e-mail I’ll pick up the phone and call her,” Wiggins said.
A brush with breast cancer in 2011 — “I created a plan and tackled it” — taught her the importance of delegating.
“I realized I didn’t have to do everything,” she said, “and I learned that my way is not the way, and that sometimes when you empower people they come up with a better way.”
She also discovered the joy of helping others, whether it’s speaking with women diagnosed with cancer or sharing what she’s learned in her 34 years in the industry with new employees.
And she’s grateful to the peers who nominated her for the Timothy Vaux Outstanding Alumni Leadership Award in 2014, which honors individual graduates of the United Fresh Produce Association’s leadership program.
This year she was selected by The Shelby Report as one of the food industry’s Women of Influence.
Cindy Jewell, vice president marketing for Watsonville-based California Giant Berry Farms, has worked with Wiggins on the United Fresh board.
“I have seen how engaged Jackie is and always ready to attack issues facing our industry head on,” Jewell said. “She is a tremendous representative for all of us in the produce industry and I am proud to work with her.”