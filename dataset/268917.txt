Trump flips on trade pact, weighs rejoining Pacific-Rim deal
Trump flips on trade pact, weighs rejoining Pacific-Rim deal

By KEN THOMAS and KEVIN FREKINGAssociated Press
The Associated Press

WASHINGTON




WASHINGTON (AP) — In a striking reversal, President Donald Trump has asked trade officials to explore the possibility of the United States rejoining the Trans-Pacific Partnership agreement, a free trade deal he pulled out of during his first days in office as part of his "America first" agenda.
Trump's request comes as he faces pressure from farm-state Republicans anxious that his protectionist trade policies could spiral into a trade war with China that would hit rural America. Trump spent the 2016 presidential campaign ripping into the multi-national pact, saying he could get a better deal for U.S. businesses by negotiating one-on-one with countries in the Pacific Rim. Now, faced with political consequences of the action, Trump appears to be reconsidering.
"Last year, the president kept his promise to end the TPP deal negotiated by the Obama Administration because it was unfair to American workers and farmers," the White House said in a statement. The president assigned his top trade advisers, U.S. Trade Representative Robert Lighthizer and his new chief economic adviser, Larry Kudlow, "to take another look at whether or not a better deal could be negotiated."
Trump first disclosed his request Thursday to a group of lawmakers at a White House meeting on trade. Lawmakers have been pressing Trump to shift course after escalating trade threats, including China's plan to slap tariffs on soybeans and other U.S. crops.
The apparent decision comes after the 11 other TPP countries went ahead last month and signed the pact in Santiago, Chile — without the United States. The agreement is meant to establish freer trade in the Asia-Pacific region and put pressure on China to open its markets to compete with and perhaps eventually join the bloc.
Japan cautiously responded early Friday to Trump's request. Government spokesman Yoshihide Suga in Tokyo said Japan welcomes the request if it means Trump recognizes the significance of the pact. He added, though, that it would be difficult to renegotiate only parts of the TPP, describing the agreement as delicate.
It was not immediately clear how committed Trump was to embarking on a new path of potentially thorny negotiations. Trump frequently equivocates on policy when faced with opposition, only to reverse course later.
"I'm sure there are lots of particulars that they'd want to negotiate, but the president multiple times reaffirmed in general to all of us and looked right at Larry Kudlow and said, 'Larry, go get it done,'" said Sen. Ben Sasse, R-Neb., who attended the meeting.
The president has mused publicly about rejoining the deal before, suggesting he would re-enter if he could negotiate more favorable terms. He has not said precisely what provisions he would want changed.
It's unclear how willing the other 11 countries would be to reopen the agreement and make concessions to lure the United States back, though its economic power would likely be an appeal.
"If the Trump administration doesn't pose too many demands, it is likely that the other TPP members will see the value of the bringing the U.S. back into the fold," said Eswar Prasad, Cornell University professor of trade policy. "Undoubtedly, a TPP that includes the U.S. would be stronger and more formidable than one that does not."
Lawmakers on Capitol Hill have been renewing their pitches for TPP — rather than Trump's threats of steep tariffs on steel and other products — as a way to counter China on trade. Sen Ron Johnson, R-Wis., was among a handful of senators who recently visited China to meet with government and business leaders there.  He said it's time to work with a coalition of trading partners to increase pressure on China.
"I have to believe President Xi is smiling all the way to regional domination as a result of our pulling out of TPP. I don't think we can get back into the TPP soon enough," Johnson said when talking to reporters about the trip.
Meanwhile, administration officials are escalating their pressure campaign against China. Kudlow said last week the U.S. may soon release a list of products that would be subject to the new tariffs Trump has threatened to slap on $100 billion in Chinese goods. And the U.S. Treasury is working on plans to restrict Chinese technology investments in the United States.
Public Citizen's Global Trade Watch, which was highly critical of U.S. involvement in a pact it viewed as lowering labor and environmental standards, said Trump's reversal on the issue would signal that the president "cannot be trusted on anything," said Lori Wallach, the group's director.
The U.S. International Trade Commission, an independent federal agency, has projected in 2016 that TPP would increase economic growth and create jobs, but the gains would be small: After 15 years, the deal would add just 128,000 jobs, an increase of less than a tenth of 1 percent. Exports would increase, but imports would increase more. Agriculture and the business services industry would see gains, but manufacturing output and employment would decrease slightly under TPP.
In the meeting with farm state lawmakers, Trump also suggested the possibility of directing the Environmental Protection Agency to allow year-round sales of renewable fuel with blends of 15 percent ethanol.
The EPA currently bans the 15-percent blend, called E15, during the summer because of concerns that it contributes to smog on hot days. Gasoline typically contains 10 percent ethanol. Farm state lawmakers have pushed for greater sales of the higher ethanol blend to boost demand for the corn-based fuel.
North Dakota Gov. Doug Burgum said Trump made some "pretty positive statements" about allowing the year-round use of E-15 ethanol, which could help corn growers.
The White House meetings came as an array of business executives and trade groups expressed concerns at a congressional hearing about the impact that tariffs will have on their business. Still, there were some supporters, too.
"Withdrawing the threat of tariffs without achieving results would be tantamount to waiving the white flag of trade surrender," said Scott Paul, president of the Alliance for American Manufacturing.
__
Associated Press writers Catherine Lucey, Jill Colvin, Paul Wiseman and Matthew Daly in Washington, James MacPherson in Bismarck, North Dakota, and Ken Moritsugu in Tokyo contributed.
__
On Twitter follow Ken Thomas at https://twitter.com/KThomasDC and Kevin Freking at https://twitter.com/apkfreking