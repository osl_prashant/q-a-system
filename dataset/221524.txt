Soybean producers are keeping careful watch on weather in South America. It’s been no secret Argentina has been battling drought conditions, and soybean markets have been reflecting that weather.

Andy Shissler of S & W Trading s suggesting farmers build their bean position before the USDA releases its prospective plantings report March 29.

“Guys sell this rally and I think if it doesn’t rain in Argentina, you continue to sell into it next week and the week after,” he said on AgDay.

Citing last year, he said the soybean market low was hit three days after what he calls the “terrible acres number.”

“If I had to predict a reoccurrence, I think that’s what it would be,” said Shissler.

Hear his full thoughts on AgDay above.