Beneficial rainfall for northern Brazil next 10 days
Showers favored most of the major growing areas in Brazil over the weekend, with the heaviest amounts of 2-4” (50-100 mm) favoring southern Goias, southwestern Minas Gerais, eastern Mato Grosso do Sul, Sao Paulo, Parana and western Santa Catarina. Rainfall will be widespread again this week, but the heaviest showers should finally reach Mato Grosso, northern Goias, and western Bahia, which will improve conditions for soybeans and first crop corn germination.
Rains should continue to favor northern growing areas during the 6-10 day period, leading to further improvements, while drier weather is expected in southern areas.
 

Rains to favor southern Argentina today and tomorrow
Showers favored northern Santa Fe, far northern Entre Rios, southern Buenos Aires and La Pampa over the weekend in Argentina. Showers are expected to favor southern and western Cordoba, La Pampa and central and southern Buenos Aires today and tomorrow, which will slow fieldwork and may lead to some areas of wetness. Very little rainfall is expected across eastern Cordoba, Santa Fe and Entre Rios over the next 10 days. The dry weather in these areas will favor fieldwork, but will also allow soil moisture levels to decline.

For more information, visit weather.mdaus.com or email metops@mdaus.com. You can follow MDA Weather Services using @MDA_Weather on Twitter.
The views, opinions and positions expressed by the author are theirs alone and do not necessarily reflect the views, opinions or positions of Pro Farmer.