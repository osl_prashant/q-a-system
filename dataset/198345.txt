I gave my animal a shot, now what?Animals receive shots for various reasons throughout their life, just like people. Sometimes they are used to prevent diseases, in the case of vaccinations; and sometimes they are used to help an animal recover from a bacterial illness, as with antibiotics. Regardless of why the animal received a shot, it is important to dispose of the needle in a safe way. Other sharp items such as scalpel blades used to perform various medical procedures on animals should be disposed of safely as well. Needles, scalpel blades, and other sharp items are sometimes referred to as "sharps." They can inadvertently injure people and/or expose them to potentially harmful substances (e.g. blood, medicines, etc.) if they are not disposed of carefully.
Remember, disposal methods can vary by state, county, and city so please check with your local landfill or waste disposal service to verify approved methods of household disposal.
Medical waste defined
Sharps used in livestock husbandry practices are considered medical or infectious waste. The Administrative Rules of South Dakota (ARSD)74:27:07:01(41A)use the definition in the Code of Federal Regulations40 CFR ¬ß60.51cto define medical/infectious waste as:
"any waste generated in the diagnosis, treatment, or immunization of human beings or animals, in research pertaining thereto, or in the production or testing of biologicals...Sharps that have been used in animal or human patient care or treatment or in medical, research, or industrial laboratories, including hypodermic needles, syringes (with or without the attached needle), Pasteur pipettes, scalpel blades, blood vials, needles with attached tubing, and culture dishes (regardless of presence of infectious agents)."
Disposal process
Now that we know "what" we are dealing with, what do we actually do with it on the farm?

THE CONTAINER:All sharps should be placed into a puncture-proof, leak-proof container. This can be either a purchased sharps container or a household container. If you use a household container, it must bepuncture-proof,leak-resistantplastic with atight-fittinglid that is properlylabeledto warn of the medical waste inside the container, for example "Used Sharps".


Purchased sharps containersPhoto bySuicide Cleanups.
Household sharps container.Photo byFDA.



DISINFECT CONTAINER:The Administrative Rules of South Dakota (ARSD)74:27:13:17require that medical/infectious waste be rendered non-infectious prior to disposal in a municipal solid waste landfill. To accomplish this,pour bleach into the container and let it soak for 15-30 minutes. If using a laundry detergent bottle, it may be easier to pour the bleach in the container if the pour spout is cut out. Then carefully pour the bleach down the sink or toilet (or leave it in container). It is recommended to wear gloves and use the cap to ensure sharps do not fall out and injure you.


SEAL CONTAINER:Now that the contents are considered "non-infectious" from the bleach, seal the container. Secure the lid on the container with asufficient amount of duct tapeto guarantee the contents will not be spilled during transport and disposal. The goal is to ensure the safety of any person that may come into contact with the sharps container.


DISPOSE OF CONTAINER:Once the container is properly sealed you have a couple options.

If your garbage is picked up and disposed of in one of the fifteenregional municipal solid waste landfills, the container may be placed in the regular garbage container for pickup.Small town dumps or disposal sites are not permitted to accept medical waste, so please contact your local waste management service to verify that they use one of the fifteen regional landfills.
Regional municipal solid waste landfills in South Dakota.Photo bySD DENR.


Askyour veterinarianif they take used sharps for disposal. Many veterinarians have a contracted service for disposing of medical/infectious waste.


Why is proper disposal important?
Proper disposal of used sharps from livestock husbandry practices is about minimizing risks. Proper disposal minimizes the risk of injury to people and animals, both on the farm and during disposal processes carried out by waste disposal company employees handling sharps containers. Additionally, all livestock quality assurance programs (e.g. BQA, PQA, and SSQA) recommend proper disposal of used sharps to continually ensure the safety of the meat products these animals provide us. As good stewards of the animals, the food or fiber these animals provide, and the environmental resources we manage, proper disposal of used sharps is just one simple practice that producers and veterinarians take responsibility for on a daily basis.
Questions?
Producers and veterinarians with questions about disposal of "medical/infectious waste" may contact the South Dakota DENR Waste Management Program at 605.773.3153 or byemail.