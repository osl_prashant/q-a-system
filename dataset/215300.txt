This market update is a PorkBusiness weekly column reporting trends in weaner pig prices and swine facility availability. All information contained in this update is for the week ended December 1, 2017.

Looking at hog sales in June 2018 using June 2018 futures the weaner breakeven was $74.18, up $5.12 for the week. Feed costs were up $0.23 per head. June futures increased $0.93 compared to last week’s June futures used for the crush and historical basis is improved from last week by $1.67 per cwt. Breakeven prices are based on closing futures prices on December 1, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was below average this week with 24,854 head being reported which is 68% of the 52-week average. Cash prices were $55.51, up $3.78 from a week ago. The low to high range was $47.00 - $62.00. Formula-priced weaners were up $2.72 this week at $45.60.
Cash-traded feeder pig reported volume was below average with 6,222 head reported. Cash feeder pig reported prices were $66.83, up $2.91 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through December 1, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential increased $1.34 this week to a projected gain of $18.67 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.