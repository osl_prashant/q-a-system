LENEXA, Kan. — The Signature Slinger, a mushroom-meat burger from Sonic, made its nationwide debut March 5, and two members of The Packer staff promptly headed out for a taste test.
Numerous establishments have tried using chopped mushrooms in burger patties, often in conjunction with The Blend initiative championed by the James Beard Foundation and the Mushroom Council, but Sonic is the first quick-service restaurant to roll out such an option.
Designer Amelia Freidline and staff writer Ashley Nickle tried the new burger on the day of its full-scale reveal. Check out the video for their reaction and the links below for more coverage of this growing trend.
MORE: Nutrition facts, availability, and what the Slinger means for mushrooms
MORE: How Sonic developed its mushroom-meat patty with help from the Mushroom Council
MORE: Blended Burger Project event in KC draws crowd