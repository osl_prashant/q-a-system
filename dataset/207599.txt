Onions grown in Washington and Oregon appear to be delayed anywhere from one to three weeks from normal harvest due to early-season weather issues. But growers anticipate a healthy market once things get going.
In the Columbia Basin region, Brenden Kent, vice president of Sunset Produce in Prosser, Wash., said there was a two or three week delay in plantings, and although there was favorable weather in June and July, there just wasn’t enough time to close the planting gap faced at the beginning of the season.
Kent said the company already had strong volumes in late July last season, and expected to start 10 days later this year.
“So I expect very manageable volumes during early to mid-August up until some of the storage varieties begin coming in.”
Kent expects the Columbia Basin overall will be down around 15% from last season’s crop in both yield and acreage due to poor market conditions from the previous season.
In late July, Kent said onion markets seemed strong and pricing was relatively good for the start of the Washington season, which he hoped would continue for a while.
“We should see a better price this year due to the lower volumes out of the Northwest,” he said.
Tim Waters, associate professor for Washington State University Extension, Franklin and Benton Counties, agreed there is a slight decrease in Washington onion acreage.
Apart from the delay in crop maturity due to the wet spring, “quality seems like a pretty normal year so far — I’ve seen no ‘disasters’ yet,” Waters said.
Waters said there’s some thrip pressure on the crop in the Washington growing areas.
Dan Strebin, owner and manager of Troutdale, Ore.-based Strebin Farms, said the company will be packing Walla Walla sweets into late August, including some transplant reds.
“These crops are growing and looking good,” he said.
Packed in Umatilla, Ore., the sweet onions have good quality and sizing.
Keystone Fruit Marketing Inc. in Greencastle, Pa., grows hybrid C-variety storage onions in the Columbia Basin.
Harvest started in July and runs through September.
The first new red onions began at the end of July.
Dan Borer, general manager for Keystone, said a cycle of cool and hot weather was not too detrimental to the crop.
Michael Locati, president of Mike Locati Farms Inc., Walla Walla, Wash., and co-owner of Pacific Agra Farms LLC, said the company’s transplant sweet onion harvest was progressing well, with quality, size and yield looking good.
Walla Walla sweet onions are available from mid-June to mid-August and Locati said the market started decent but has dropped off due to Vidalia and California crops.
Seattle-based onion distributor F.C. Bloxom Co. grows onions from Moses Lake, Wash., to Hermiston, Ore.
Bryon Magnaghi, head of the company’s office in Walla Walla, said most growing areas of the storage varieties should be seeded by July 31, starting with yellows and some reds and then moving on to whites.
Shawn Hartley, president of Syracuse, Utah-based Utah Onions Inc., said the company grows onions in Horse Heaven Hills in the lower Columbia Basin.
Acreage will remain the same as the past couple years, he said.
“If the weather holds out, as long as it does not get too hot during harvest, we should be like last year,” Hartley said.
He anticipates demand will be good to start the season as customers look to try out the new crop. He also thinks pricing will be up this year.