There are disagreements, as there are in any business—family or otherwise. As Kent and Barry Holden assumed sole ownership responsibilities of their family's multi-generational hog farming operation, there were growing pains.
When disagreements happen, they have one-on-one or two-on-one discussions between whomever is most involved in a particular situation, Kent says. “They communicate and work through their disagreements to reach a solution.” 
When asked if they’ve had consultants talk to them about effective communication, the brothers chuckle.
“We have had a lot of outside people come in to tell us how to do it, even though they might not have done it themselves. They have all sorts of advice.”
In the end, they say it comes down to mutual respect.
“We talk openly with our kids, and we’ve been very transparent about what’s happened in our past,” Kent says. “But unless you’ve dealt with it firsthand you don’t have the same feeling about it. 
“If they don’t learn how to communicate, talk to each other and understand each other, not only will the business suffer, but their personal relationships with each other will suffer too,” he adds. 
It’s obvious the five members of the fifth generation are comfortable with one another, and with their father-owners. Communication is key as each member of the fifth generation consider their part in Holden Farms.
 

Read more:
All in the Family—Holden Farms Has Room to Grow
Communication Keeps Holden Farms A Family
Holden Makes Family the First Priority
Holden Farms: Secure Transfer to the Fifth Generation