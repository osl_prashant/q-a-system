Wall Street rebounded after news broke that the U.S. and China were willing to negotiate differences in the trade spat.

China announced it would be willing to talk to the U.S. on the trade tariff issue on Monday.

One day prior, Treasury Secretary Steve Mnuchin said he’s “cautiously hopeful” the countries will be able to find common ground before the U.S. proceeds with the tariffs.

Chris Hurt, professor of agricultural economics, told AgDay if there were tariffs on U.S. pork, there could be stark price losses.

Watch the full story on AgDay above.