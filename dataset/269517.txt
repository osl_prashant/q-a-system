BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Monday:
OpenHighLowSettleChg.ORANGE JUICE15,000 lbs.; cents per lb.May141.05143.85140.35143.50+2.45Jun143.30+2.20Jul139.80143.60139.80143.30+2.20Aug143.80+1.95Sep142.00144.00141.55143.80+1.95Nov143.15144.60143.15144.50+1.75Jan144.00144.65144.00144.65+1.10Feb145.15+1.10Mar145.15+1.10May145.90+1.10Jul146.30+1.10Sep146.60+1.10Nov146.90+1.10Jan147.40+1.10Feb147.70+1.10Mar147.70+1.10May148.00+1.10Jul148.30+1.10Sep148.60+1.10Nov148.90+1.10Jan149.20+1.10Feb149.50+1.10Mar149.50+1.10Est. sales 2,651.  Fri.'s sales 1,689Fri.'s open int 13,816