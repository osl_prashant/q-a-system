With several similarities to human organs, pigs offer a beneficial role in organ and tissue research.
In a recent issues of Science Robotics, Karl Price and his colleagues at Harvard Medical School and Boston Children’s Hospital said they used robotic implants to stimulate the growth of tissue inside a living organism. In a test of five pigs, the scientists inserted the implant to lengthen the animal's esophagus, while they were awake, moving and able to eat normally. Another three pigs acted as controls.
The robotics were about the size of two thumbs pressed together, and affect any tissue of any tubular organ, such as veins, intestines, arteries and esophagus.
The aim is to make this type of tissue growth standard practice for children, then adults, who might face any number of medical problems, such as genetic defects, infections or blockages. The advantage would be less sutures and a greater chance of recovery.
In the pig experiments, the chunk of esophagus that was missing was at least three centimeters. In about nine days, the new technique had stimulated growth of cells to lengthen the esophagus segment by 77%.

Read the procedure details here.
Read the article from Science Robotics (login required).