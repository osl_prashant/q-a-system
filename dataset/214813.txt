The growth rate for U.S. imports of agricultural goods has more than doubled the pace of U.S. ag exports since 1995, a new report on agricultural trade reveals.
Rising incomes in developing countries will create more export demand for U,S, meat, dairy, fruits and nuts, according to the U.S. Department of Agriculture’s Economic Research Service report, “The Global Landscape of Agricultural Trade, 1995-2014.”

Reflecting a growing middle class, developing economies accounted for 42% of all global agricultural imports in 2010-14, up from 28% in 1995-99.
Global agricultural trade, at about $1 trillion in 2014, has risen 3.5% per year for the last two decades, the report said. World trade in farm products has been boosted by technological change, productivity gains, and trade liberalization, according to the report.

Import hot streak
U.S. consumer demand for imported fruits and vegetables in the off-season is one reason why U.S. imports of consumer-oriented products have grown faster than total agricultural imports, according to the report.
At $110.6 billion in 2012-14, the U.S. was the world’s third-largest importer of agricultural products, behind the European Union ($133.9 billion) and China/Hong Kong ($132.1 billion).
U.S. imports of fresh fruit, snack foods, and carbonated soft drinks grew especially quickly between 1995 and 2014, according to the report.
The USDA said the fastest growing sources of consumer-oriented imports were Vietnam (cashews, pepper), China (fruit and vegetable preparations), Peru (fresh fruits, asparagus), Switzerland (carbonated soft drinks), and Mexico (fresh/processed vegetables and fruits, beer, snack foods, beef). 
U.S. import growth outpaced exports, rising at an average annual rate of 4.3% from a real value of $51 billion in 1995-99 to $96 billion in 2011-15, according to the report.
By way of comparison, the real dollar value of all U.S. agricultural exports grew at an average annual rate of 1.4% over the last two decades, from an average of $85 billion during 1995-99 to $105 billion during 2011-15.
Tree nuts lead exports

The U.S. is neck-and-neck with the European Union as the world’s biggest exporter of agricultural goods, with U.S. exports averaging $148.1 billion in 2012-14, compared with $149.1 billion for the EU for the same period.
The report said U.S. tree nut exports, led by almonds primarily destined for the EU and Hong Kong, grew twice as fast as other U.S. consumer oriented exports. U.S. exports of fresh fruits and vegetables grew more slowly than pork and dairy exports, according to the report.