Sessions scheduled during the Sept. 21-23 New England Produce Council Produce and Floral Expo will give attendees a peak into the mind of consumers and provide other industry information.
On Sept. 21, Tom Stenzel, president and CEO of the United Fresh Produce Association in Washington, D.C., is scheduled to be the conference's keynote speaker.
Laura Sullivan, executive director of the Burlington, Mass.-based council, said Stenzel will talk about current hot topics including marketing specialty produce, organics and natural products.
"He's a great industry expert who can speak on a variety of topics," she said. After Stenzel's presentation, Kevin Coupe, a food industry expert, journalist and author, is scheduled to host a session.
As last year's keynote speaker, Coupe discussed how people, technology and culture are affecting the competitive landscape and his talk was informative, Sullivan said.
This year, the session plan to tackle a different topic and Coupe's talk will feature a consumer and industry expert panel.
In the panel, the council plans to use shoppers not connected to the produce industry, secured through a Boston research firm."Kevin will do an across-the-ages panel of consumers," she said. "It's interesting to see how people in the different ages shop. Some may clip coupons while others shop with apps on their phones.
"This will be a win for everyone and will be a session that both the retailers and the vendors will find interesting in hearing direct from consumers as to how they shop for their products."
Attendees should find the sessions' speakers intriguing, said council president Anthony Sattler, vice president of produce and floral procurement for Hatfield, Mass.-based C&S Wholesale Grocers.
"They are two great industry experts," he said.
"They will add a lot of insight when they speak and host the consumer panel. Overall, the show allows the opportunity to see the latest industry trends and collaborate with grower-shippers and packers and always looking to increase sales and to network."The sessions are an important part of the conference, said expo committee member Bruce Klein, director of marketing for Secaucus N.J.-based Maurice A. Auerbach Inc.
"We tried to do it a little different this year," he said.
"We wanted to make it different and more interesting. With the education sessions, we want to give attendees a reason to attend the show besides the expo."
The retailers who attended last year's event said they were pleased and enjoyed it, Klein said.
The council hopes more plan to attend, he said."These days, it's not the easiest thing to get out of the office for a couple of days," Klein said.
"The show is getting bigger and is attracting a lot of retail interest."
Encouraging more foodservice buyers to attend is another goal, he said. "There are big produce buyers that are in foodservice," Klein said. "We are trying to attract more and would certainly like to see their attendance."
Foodservice representation on the council's board of directors is helping in that goal, Sullivan said.
"We are encouraged that there will be some foodservice attendees at the show," she said.