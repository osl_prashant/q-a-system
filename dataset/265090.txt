Chicago Mercantile Exchange lean hogs hit a four-month low on Thursday as U.S. stocks plummeted amid fears of a potential trade war between the United States and China, said traders.
On Thursday U.S. President Donald Trump signed a presidential memorandum that could impose tariffs on up to $60 billion of imports from China.
China is the fifth-largest importer of U.S. pork, industry experts said.
“There’s some trade war concerns, but there also has been larger-than-anticipated hog slaughters,” said Livestock Marketing Information Center director Jim Robb.
Hog farmers ramped up production driven by affordable feed, robust U.S. pork exports and increased U.S. hog slaughter capacity.
The hog supply buildup pressured prices for slaughter-ready, or cash, hogs and for wholesale pork, said traders.
Thursday afternoon’s U.S. Department of Agriculture (USDA) monthly cold storage report showed total February pork stocks at 614.9 million pounds. That was up 6 percent and 8 percent from the previous month and last year, respectively.
April hogs closed down 1.250 cents per pound at 61.325 cents, and May finished 2.175 cents lower at 68.150 cents.
LIVE CATTLE SNAP LOSING SKID
CME live cattle emerged from a five-session slump, helped by higher wholesale beef values and futures’ discounts to this week’s cash returns, said traders and analysts.
Bargain-hunters bought deferred live cattle contracts that were pressured during recent sessions by expectations for bigger supplies ahead.
Investors await USDA’s monthly Cattle-On-Feed report on Friday.
Futures gained on the day, but were off session highs as uneasiness about a potential trade war rattled U.S. equities.
“There was probably some spillover from this overall environment of the financials being down hard today,” said Robb.
This week cash cattle in the U.S. Plains brought $125 to $126 per cwt versus $126 to $128 last week.
Adequate near-term supplies, and the prospect of plentiful cattle numbers ahead, weighed on cash prices, said traders and analysts.
Packers sold small amounts of beef to supermarkets at higher prices as they prepare for post-Easter holiday spring grilling advertisements.
Thursday’s USDA cold storage data showed beef inventories in February totaled 460.3 million pounds, down 8 percent from both January and a year ago.
April live cattle closed up 0.450 cent per pound at 118.150 cents, and June ended 1.025 cents higher at 108.400.
CME feeder cattle followed higher live cattle futures.
March feeders ended 0.750 cent per pound higher at 137.525 cents.