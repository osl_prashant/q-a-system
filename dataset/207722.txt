Wenatchee, Wash.-based Stemilt Growers recently began harvesting its organic peaches and nectarines and expects promotable volumes soon.The fruit will be ready for promotions in mid-August, and Stemilt suggests that stores carry large quantities through mid-September, according to a news release.
“Retailers should be planning big callouts for advertisements,” communications manager Brianna Shales said in the release.
“Promote the fact that these fruits are organic, tree-ripened and jumbo-sized.
“As mid-August arrives, retailers should start running two to three ads over the course of a month to take advantage of the flavors and high qualities of this late-season and all-organic stone fruit,” Shales said.
Stemilt also suggests retailers use its display bins, which are designed to tell consumers about the origin of the fruit.
Fruit quality has been high, in part thanks to quick and even cooling of the peaches and nectarines once they have been sorted and packed, according to the company.
“The jet coolers are making a significant difference in quality and shelf life,” Shales said in the release.