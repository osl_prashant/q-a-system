Six mango industry members have been appointed to lead the National Mango Board's operations.
Agriculture Secretary Tom Vilsack has appointed the importers and growers to serve the Orlando, Fla.-based organization's board.
The appointments consist of four new members and two returning members.
Board appointees are scheduled to serve three-year terms starting Jan. 1 and their terms are set to end on Dec. 31, 2019.
The new board members are:
Importers:
Chris Ciruli, Tubac, Ariz.;
Jiovani Guevara, Phoenix; and
Marsela Mcgrane-Vogel, San Pedro, Calif.
International producers:
Norberto Galvan, Tapachula, Mexico;
Joaquin Balarezo, Piura, Peru; and
Eddy Martinez, Guatemala City, Guatemala.
These board members have completed their terms:
Jorge Perez, Sinaloa, Mexico;
Reynaldo Hilbck, Piura, Peru;
Altamir Martins, Fortaleza, Brazil; and\Sergio Palala, San Carlos, Calif.
Ciruli and Guevara are to return to the board to each serve second consecutive terms.
"We would like to thank the NMB members that will be completing their term this year for serving on the board and for representing the mango industry," Manuel Michel, the board's executive director, said in a news release. "The knowledge and expertise they bring to the NMB truly makes a difference and drives the success and growth of our industry.
"The NMB also extends a welcome to those members who have just been appointed to serve on the board during the 2017-19 term. We look forward to working with them to develop new strategies and projects that will continue to support the industry and increase mango awareness and consumption."