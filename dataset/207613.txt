The Wisconsin Potato & Vegetable Growers Association is putting a huge emphasis on state and regional promotions, from TV commercials and point-of-sale materials to a display contest and promotions related to Green Bay Packers games.
The state’s potato shippers say emphasizing local and regional promotions makes a lot of sense.
“The grown local movement continues to flourish,” said Tim Huffcutt, marketing director for RPE Inc., Bancroft, Wis.
“Retailers understand that consumers are more aware than ever about how food is sourced.”
Milwaukee-based Roundy’s, for example, uses large signs featuring Wisconsin growers in its stores, said Mark Finnessy, owner of Okray Family Farms Inc., Plover, Wis.
“Local is a big push,” Finnessy said. “We help customers every way we can to keep it local.”
Huffcut said some retail customers have asked RPE to incorporate locally grown messages on bag designs.
“Callouts might range from mention of a specific state, or in some instances, we may highlight a growing region,” he said.
“Because families are ever-more conscientious about food quality, they are seeking more answers about its origin. We have accommodated retailer requests for state or regional callouts, which we believe are strategic and in response to what consumers want. We also believe there are marketing advantages to highlighting specific growing regions.”
Christine Lindner, national sales and marketing representative for Alsum Farms & Produce Inc., Friesland, Wis., said the company has expanded its efforts to supply in-state customers not only with potatoes but sweet potatoes, onions, celery, asparagus and more.
“Shoppers recognize Wisconsin locally grown potatoes at retail and it is a benefit that supports local farmers, communities and economies,” she said.