Ohio company recalls more than 7 tons of BBQ beef
Ohio company recalls more than 7 tons of BBQ beef

The Associated Press

HARRISON, Ohio




HARRISON, Ohio (AP) — Federal authorities say an Ohio-based food company is recalling more than 14,000 pounds (6,000 kilograms) of pulled barbequed beef products that could be contaminated with rubber.
The U.S. agriculture department's Food Safety and Inspection Service reports that J.T.M. Provisions Co. of Harrison had received two consumer complaints of extraneous material contamination. There have been no confirmed reports of adverse reactions from consuming the products.
They come in a sealed plastic tray with a paper sleeve in products labeled "Bar-B-Q Sauce With Pulled Beef." Federal authorities say they were produced last September, but they are concerned that some remain frozen in customers' refrigerators or freezers.
They were shipped to retailers in Indiana, Kentucky and Ohio.
The service says they should be thrown away or returned to the place of purchase.