Ranchers in July placed 2.7 percent more cattle in U.S. feedlots than a year ago, the U.S. Department of Agriculture reported on Friday, which fell short of average analysts' forecasts.Dwindling profits for feedyards, after being paid less for their cattle by packers, discouraged them from buying calves for fattening on their way to beef processors, said analysts.
They said fewer cattle are now winding up in commercial feeding pens after ranchers rushed them to market earlier than they had planned to avoid lower prices expected in the coming months amid increased supplies.
Cattle that entered feedlots in July could begin arriving at packing plants in early 2018.
On Monday, Chicago Mercantile Exchange live cattle futures could open higher in response to the report's bullish placement outcome, the analysts said.
USDA's report showed July placements at 1.615 million head, up from 1.572 million a year earlier and below the average forecast of 1.670 million. Still, it was the largest July placement figure since 1.684 million in 2013.
The government put the feedlot cattle supply as of Aug. 1 at 10.604 million head, up 4.3 percent from 10.165 million a year ago. Analysts, on average, forecast a 4.7 percent increase.
USDA said the number of cattle sold to packers, or marketings, were up 4.1 percent in July from a year ago, to 1.784 million head.
Analysts had projected a gain of 4.9 percent from 1.713 million last year.
"Cattle feeders are facing clear losses in the months ahead due to their very heavy placement schedule in the previous eight months," said Allendale Inc chief strategist Rich Nelson.
The Denver-based Livestock Marketing Information Center calculated that feedlots in July, on average, made a profit of $40 per steer sold to meat companies versus $84 the month before. They project August margins at minus $20 per head.
Friday's report will ease some concerns about supplies during the first half of 2018, but does not change the outlook for significant cattle numbers during the fall, said Nelson.
Texas A&M University economist David Anderson agreed that "pulling cattle ahead" left fewer animals placed into feeding pens in July - which historically tends to top June placements.
He pointed out that some feedlot placements in Corn Belt states rose, partly due to livestock that continue to be displaced by drought in the Northern Plains.