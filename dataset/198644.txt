Global pesticides, seeds and fertilizer companies may be forced to re-engineer their business models asfarmers adopt specialist technology that helps maximize harvests while reducing the use of crop chemicals.New businesses are springing up that promise to tellfarmers how and when to till, sow, spray, fertilize or pick crops based on algorithms using data from their own fields.
Their emphasis on reducing the use of chemicals and minerals known asfarming inputs is a further challenge for an industry already struggling with weak agricultural markets worldwide.
"If our only goal is to sell as much inputs as possible by the liters of chemicals, I think we would have a real problem going forward," said Liam Condon, head of Crop Science at Bayer , the world's second-largest pesticides supplier.
Bayer bought proPlant, a developer of software for plant health diagnostics, earlier this year. Rivals are also investing in digitalfarming with the aim of generating service revenues that could offset any future drop in chemicals volumes.
"If you only spray half of the field that's much less inputs," Condon added. "The knowledge to get to the fact that you only spray that part of the field -- that, you can sell."
After an aborted takeover move for Syngenta, U.S. seeds giant Monsanto says data science and services are the "glue that holds the pieces together" of its strategy for future growth.
Monsanto's $1 billion purchase in 2013 of the Climate Corporation, which analyzes weather conditions, was the digitalfarming sector's biggest deal to date.
DuPont is investing in digitalfarmmanagement services under its Encirca brand, which it said in March had customers representing more than 1 million acres offarmland.
Monsanto's failed swoop on Syngenta triggered a bout of M&A activity that has left the global seeds and pesticides industry in turmoil. The sector has annual sales of more than $100 billion, while fertilizers are worth around $175 billion.
Dow Chemical and DuPont are set to merge in the second half of this year while state-owned ChemChina agreed a takeover of Syngenta in February.
ON THE GROUND
At the 970-hectarefarmin Bavaria where Juergen Schwarzensteiner rotates corn, potatoes and grains, satellite maps and software supplied three years ago by a unit offarming goods distributor BayWa have prompted many changes.
These include reducing the overuse of nitrogen fertilizer -- a risk to drinking water quality and the environment -- and cutting down on other fertilizers.
"This plot has had top yields consistently over the years, where I used to just say, that's great," says thefarmer, eyeing a red and green patterned computer map showing big discrepancies in how well plants are growing just half a mile apart.
"Then we got the digital maps and differences became apparent that were not clear to the eye before."
Schwarzensteiner's experiences using the technology have secured him a side job advising afarmin Siberia that is about 100 times the size of the one he manages at Irlbach, near the river Danube in Germany.
As well as BayWa'sFarmFacts,farmmanagement software startups include Iowa-basedFarmers Business Network Inc, backed by Alphabet Inc and investor Kleiner Perkins, and Missouri-basedFarmLink LLC.
All aim to providefarmers with individualized prescriptions on how to work each field down to a fraction of an acre, using data they have collected on soil and weather conditions, the use of crop chemicals and crop yields. Feedback from thefarmers they have advised in turn allows the companies to fine-tune their computer models of plant growth.

RISKS AND CHALLENGES
According to market research firm AgFunder, venture capital investments in food andagriculturetechnology nearly doubled to $4.6 billion last year, with "precisionagriculture" startups raising $661 million in 2015, up 140 percent from 2014.
Syngenta bought seven agricultural technology firms last year alone, AgFunder said.
For now, the main aim of these companies is to helpfarmers using their drones, field robots, decision support software and smart irrigation systems to boost yields, said Carsten Gerhardt, a chemicals industry specialist at advisors A.T. Kearney.
"But in the mid- to longer-term, I also expect there to be a reduction in the use of input factors by about 30 to 40 percent," he added.
"There's a risk for established players if digital services providers can convincefarmers that they can settle for the second-best herbicide and show what really counts is a more precise way of using it."
Eric Bartels, a partner at McKinsey who focuses on the agricultural industry, said developing new pesticides would help companies hedge against any drop in sales, however, becausefarmers will pay a premium to keep their fields pest-free.
Another question is whether today's chemicals andfarmnutrients giants can capture thefarmmanagement software market for themselves. Gerhardt said digital startups would struggle to catch up with established players' knowledge of plant biology and thefarmbusiness, and to build a global sales network.
But Rabobank'sfarmsector analyst Harry Smit says crop chemicals and seed players diversifying into such services will struggle to be seen as providers of impartial advice.
That was one of the reasons why German grain and sugar beet seed maker KWS Saat, among the world's top five seed makers, decided not to invest in digitalfarming platforms.
"Farmers want independence," KWS finance chief Eva Kienle said. "They don't want to get the impression they are being recommended a product just because the supplier is earning a profit on it."
(Editing by Catherine Evans)