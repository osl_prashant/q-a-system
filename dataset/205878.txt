Traders covered some short positions in the corn and soybean markets to close out August amid forecasts calling for a cold start to September and a pickup in export sales activity. But buyer interest was light and will remain limited unless temps this week turn out to be colder than anticipated heading into the long holiday weekend or there’s a continued surge in export sales. Winter wheat futures continue to trade in-step with the corn market, meaning corn likely has to bottom before wheat finds a low. Spring wheat futures continued their sharp pullback from the early July highs as harvest activity picked up in the Northern Plains.
Pro Farmer Editor Brian Grete provides weekly highlights:



Cattle futures pressed lower as the market was unable to find sustained buying from the friendly Cattle on Feed Report given continued pressure on cash cattle prices. Lean hog futures also posted weekly declines as a late-week rebound failed to erase earlier losses.
Notice: The PDF below is for the exclusive use of Farm Journal Pro users. No portion may be reproduced or shared.
Click here for this week's newsletter.