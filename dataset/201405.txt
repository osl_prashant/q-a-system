The latest Drought Monitor shows soils continue to dry out and crops are suffering as drought and abnormal dryness continue to expand or intensity across the Plains, Midwest, northern Rockies and Virginia.
In Montana, exceptional drought, the most severe level, jumped up 10 points in a week. Nearly 12% of the state is seeing exceptional drought conditions with another 24% under extreme conditions. In neighboring North Dakota, nearly 8% of the state is seeing exceptional drought. Thirty either percent of the state is under extreme drought, according the Drought Monitor.



 
In the Corn Belt, severe drought conditions have now appeared in south central Iowa but it’s limited to 1.66%. Iowa’s moderate drought grew 12 points to 34%. All states east of the Mississippi River remain drought free but there are patches of abnormal dryness. This could change as early as next week.
According to Brad Rippey, USDA meteorologist, drought conditions are intensifying across central U.S., the Corn Belt especially. While places like North Dakota, Montana and South Dakota saw drought conditions increase by a few points, the Corn Belt has increased significantly—seeing percentage increases in the double digits.
Nationally, drought coverage is at its, rising to 10.98% with overall colored areas on the map increasing to 32.69%, both increases are the highest since March 28.
Visit www.agweb.com/weather for continued weather news, forecasts and growing conditions throughout the season.