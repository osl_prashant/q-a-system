For most of the Olympics this year in South Korea dairy was getting positive publicity. However, that was until an anti-dairy commercial ran during the closing ceremony’s broadcast.
MilkPEP has ran Milk Life campaigns since 2016 Olympics as partners with athletes from Team USA. Prior to this a number of Olympians donned milk mustaches in the “Got Milk?” campaign. The XXIII Olympic Winter Games were no different with athletes like two time gold medal winner snowboarder Jamie Anderson and gold medalist skier Maddie Bowman promoting dairy.
This year to end the Olympics in PyeongChang, South Korea a campaign called Switch 4 Good made it known that some athletes are choosing to go dairy-free, although none of them were competing. The 30 second advertisement aired regionally in six major markets including:  New York, Los Angeles, Chicago, Washington D.C., San Francisco and Dallas.
Six former Olympic athletes participated in the campaign for Switch 4 Good, they are as follows:

Kendrick Farris, USA, weightlifter (2008, 2012, 2016)
Rebecca Soni, USA, six time medalist in swimming (2008, 2012)
Malachi Davis, Great Britain, track and field sprinter (2004)
Kara Lang, Canada, soccer (2008)
Dotsie Bausch, USA, silver medalist in pursuit cycling (2012)
Seba Johnson, Virgin Islands, alpine skier (1988, 1992)

During the advertisement the non-dairy athletes, many of them who are vegan, say things like “I got stronger,” “to take back my life” or “run faster” was the reason for their dietary switch.

The advertisement from Switch 4 Good ends with the non-dairy athletes saying “they don’t drink cow’s milk” and “they are proud to be dairy free.”
The Switch 4 Good campaign is marketing itself as being worried about eating or drinking dairy for health reasons. Listening to comments from the non-dairy athletes and those behind the scenes it appears to be really about animal rights.
In a campaign promotion video Dotsie Bausch says she’s “leaving violence out of her diet.”
In the case of Seba Johnson she claims to have been a vegan since birth. “I’ve never had cow’s milk and I’m never going to,” Johnson says in the commercial.
Johnson has also been an outspoken animal rights activist, even petitioning the International Olympic Committee not to let Japan host the 2020 Summer Olympics because of dolphin and whale hunts.
The production company behind the Switch 4 Good has been actively involved with animal rights films, including the documentary The Cove, which looks at dolphin hunting in Japan.
 Anecdotal insights in an anti-dairy advertisement are a far cry from the nutritional facts shared by athletes in pro-dairy videos from Milk Life.
“Milk is a nutrient powerhouse,” skeleton racer Katie Uhlaender says in an on-farm promotional video. “It has nine essential vitamins, including 8 grams of protein. Protein is huge for creating muscle mass and recovering from workouts.”
Figure skater Kristi Yamaguchi, a 1992 gold medalist, shares that protein and essential nutrients found in milk are important for athletes.
“Now as a mom I have young athletes and I really want them to get the nutrients that they need,” Yamaguchi says in a Milk Life video.
Olympic athletes overwhelming use dairy in their diet. This fact is also shared in the Milk Life advertisements with it saying “9 out of 10 U.S. Olympian grew up drinking milk.” This stat is backed up by recent data from 675 U.S. Olympic athletes who responded to a survey sent to all U.S. Olympic athletes.
There are 26 Olympians or Olympic hopefuls from 18 states who are currently part of Team Milk.