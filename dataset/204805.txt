Planters are rolling during this cool, hazy morning in Northern Indiana. It's a start to a spring day here where climate is largely controlled by one thing: the proximity to Lake Michigan."Lake Michigan determines us every year," says La Porte, Ind., farmerTom Parker.
When cold air masses move over the warmer lake, it creates what is known as "the lake effect."Water vapor picks up and rises into the colder air above.That brings the possibility of rain, snow or lake-effect-driventemperatures near the well-known body of water.
Cool Temps
But the occasional roll of cloud cover is just part of life to farmers like Harold and Tom Parker.
"Right now we're getting the lake breeze.We're probably colder than any other place around. The lake is only 40 degrees, so determining the temperature is hard," Harold says.
"It really does affect things quite a bit here," agrees Tom. "We have to choose our varieties specific to this area. If you go 20 miles south of here, you're in a whole different world."
Despite the brisk set of mornings, they both say the lake has treated them well so far this season.
Focusing today on seed corn, the two farmersare about halfway done with planting. Some commercial corn is even starting to emerge. Both say heat is needed to warm up both soil and temperatures.
"It's been like this for a couple of days where it's 45 to 50 degrees and really no sun. You don't have to go very far and they've been rained out several times," Tom says.
Planting Delays
Tom Parker might as well be talking about Scott Szczypiorski's operation. He's doing some work on the farm but he's not in the planter just yet.
"We had intentions of starting on Monday, April25. With this weather, we've held off," says Szczypiorski, who is just a few miles away from the lake.
It can cause a big impact on his growing season, with a May start date being quite common.
"It can even be cold the first two weeks of May," says Szczypiorski.
He says the area has too much moisture now. But he's hoping fields will dry down later this week. Besides the rain, he's holding off this year because he doesn't want to put his crop'sgermination process at risk.
"Planting is the most important time of the year. If you screw this up, you're done. You don't really get a better chance than the first time through," says Szczypiorski.
As cold temperatures breathe from the water to the field, farmers here know Mother Nature's plan can be altered by the mighty Great Lake.