The National Mango Board has a promotion season planned that highlights some key attributes of the mango, that it’s tropical and fun.
Valda Coryat, director of marketing of the Orlando, Fla.-based board, said the campaign is Go Mango! The Super Fun Superfruit.
“(This) is the year that we really roll out our brand transformation,” she said. “We have refreshed our branding and creative with sunnier colors, a more vibrant style and a brand personality that really brings fun to life.”
The board also plans to increase its consumer marketing with the continuation of its influencer program.
Coryat said the board has new creative campaigns, messaging, video and photography that brings out the flavor and nutrition of mangoes.
“We’re excited about the team of influencers and new creative,” she said. “For example, (actor) Wilmer Valderrama’s flirty mango post on Instagram breathed new life into ‘how to cut a mango.’”

We have refreshed our branding and creative with sunnier colors, a more vibrant style and a brand personality that really brings fun to life.

She said this winter, to demonstrate mango’s versatility, the board promoted “a beautiful mango rose that made Valentine’s Day extra special and fun.”
“We’re tapping into key social occasions where naturally consumers are always looking for something special or memorable,” Coryat said.
“We have invested more in digital and search to make mango both more discoverable and engaging. An example of this was the strong results we had during Super Bowl weekend, which showcased our Tropical Mango Guacamole recipe video.”
This summer, she said a new mango emoji will debut.
“We are also developing some great new video content that will put more of a spotlight on our farmers and how critical they are to delivering a delicious and nutritious product.”
Coryat said mangoes continue to grow demand in the U.S. Over the past 10 years, weekly retail volume has increased by 54%.