When the USDA released its Prospective Plantings Report last week, it was forecasting for soybean acres to top corn acres, something that hasn’t been seen since the 1980s. 

At this point, soybean acres are expected to outnumber corn by 1 million acres. If this pans out, farmers could expect 88 million acres of corn in the U.S. in 2018. 

Crunching the numbers, Jim McCormick, senior broker and market commentator with Allendale, Inc., thinks if the trendline yield falls three bushels behind last year’s average, the carryout could be between. 1.3 to 1.4 billion bushels.

"To put that in perspective, last couple of years we tapped out the market about 1.7 [billion bushels], so your economics look like if there’s any kind of a hiccup, there’s a very good potential you could se this corn push up to $4.50 [per bushel],” he said on AgDay.

There are a few market indicators to see how bullish the corn market will be. One of the first to watch, he says, is the funds.

“They lead this market—they really got excited about the corn market relatively the last few weeks, pushed their position about half of a record limit,” said McCormick.

Hear what he says about export sales and the corn market on AgDay above.