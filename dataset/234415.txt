Child labor case tied to polygamous group headed to court
Child labor case tied to polygamous group headed to court

The Associated Press

SALT LAKE CITY




SALT LAKE CITY (AP) — The U.S. Department of Labor is headed to court to press allegations that a company tied to a polygamous sect on the Utah-Arizona border changed its name and defied an order to stop using child labor.
A four-day evidence hearing begins Monday in the case against Paragon Contractors, which is accused of changing its name to Par 2 and hiring underage workers for construction jobs across the West in 2015 and 2016.
Federal prosecutors want the company found in contempt. Paragon disputes the allegations.
The company was previously ordered to pay $200,000 for back wages to children who picked pecans for long hours in the cold during a 2012 pecan harvest.
They're appealing that finding, arguing the children were glad to get a break from schoolwork to gather nuts for the needy.