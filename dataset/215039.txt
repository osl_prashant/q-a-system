AccuWeather reports a break from the waves of cold air hammering the Midwest and Northeast is on the way, but it will not be until next week.
While record warmth is in store for the West and parts of the Plains this week, waves of cold air from the polar region will intervene farther east through this weekend.
"Another significant blast of cold air with wind will rotate through from the Midwest to the Northeast from Saturday to Monday," according to AccuWeather Lead Long-Range Meteorologist Paul Pastelok.
Cold winds may make venturing out a little painful this weekend.
The best bet for a few days in a row with temperatures near or above average will be early next week for the North Central states and the middle of next week in the Northeastern states.

Temperatures are likely to reach the 40s to lower 50s F in Chicago and New York City for a few days next week.
The less harsh conditions expected will allow people who mind or cannot handle the cold to get outside as holiday shopping and outdoor decorating kick into high gear.
"The coldest air in the Northern Hemisphere, relative to average, is likely to be centered over parts of Europe and Asia during the first part of December, due to the anticipated position of the Polar Vortex," Pastelok said.
The Polar Vortex is a storm high in the atmosphere that typically hangs out near the North Pole.
When this storm is strong, it tends to keep frigid air contained around the Arctic Circle. However, when the storm weakens, it can become displaced and allow frigid air to plunge into the mid-latitudes in North America, Europe or Asia.
"There is still room for part of the Polar Vortex to stretch toward the Hudson Bay, Canada, area during the first half of December," Pastelok said.
If this occurs, then waves of cold air will resume in the North Central and Northeastern states during early December.

"However, these cold shots would likely be rather short in duration and not as potent as the blast coming in this weekend," Pastelok said. "One thing we are very confident of is above-average warmth for much of the West and southern Plains into the first half of December."
Temperatures from the Ohio Valley to the mid-Atlantic may not be too far from normal during the first part of December when averaging the approximate two-week period.
The air is still likely to be cold enough for brief episodes of lake-effect snow around the Great Lakes to the central and northern Appalachians during early December.
If cold air plunges again, the ingredients may be present for a storm to develop near or along the East Coast during the Nov. 30 - Dec. 1 time frame. Wintry weather could not be ruled out if all of the right factors come together.