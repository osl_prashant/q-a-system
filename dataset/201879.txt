The second-quarter updates on farmland values and farm financial conditions from three Federal Reserve Banks shows hints of stabilizing land values, but divergent regional trends also continue.
The diverging trends come from the continued weakness noted in Great Plains farmland values versus stabilizing to only slightly weaker values in the central and southern Corn Belts.
The Federal Reserve Bank of Chicago reports the value of good agricultural land in its district rose 1% compared to a year earlier. The gain is the first annual rise since mid-2014. While the district overall notched a gain, both Illinois and Indiana bankers reported slight declines of 3% and 1%, respectively. However, both states hinted at stabilization with Indiana noting a 2% rise during the second quarter versus the first quarter of 2017 and Illinois reported values remained unchanged in the second quarter compared to a quarter earlier.
           Central Corn Belt Farmland Values Post 1% Annual Gain

Iowa bankers report values rose 3% on an annual basis with values gaining 2% versus the first quarter of 2017. Wisconsin reported values rose 1% both on a quarterly and annual basis.
                     Central Corn Belt Values Mark 1% Quarterly Gain

The Federal Reserve Bank of St. Louis noted quality farmland decrease less than 1% annually in the second quarter while cash rents declined 1.8%. However, ranchland and pastureland rose 4.5% on an annual basis and rents improved 7.9%.

The Federal Reserve Bank of Kansas City reported continued weakness crop and ranchland values, but noted signs from and bankers that the pace of deteriorating ag credit conditions was decreasing.
"Farmland values continued to trend lower alongside the reductions in farm income and weaker credit conditions, but the changes have remained modest," the bank notes. "For the sixth consecutive quarter, the value of cropland and ranchland in the district decreased from a year ago. Similar to recent quarters, the value of irrigated cropland, nonirrigated cropland and ranchland decreased 7%, 5% and 4%, respectively.
             Further Decline in Central/Southern Plains Values