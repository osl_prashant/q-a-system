LENEXA, Kan. — Changing with the times is one reason that Kansas City-based potato and onion marketer Haun Potato has enjoyed 65 years as a company. 
Although Richard “Spud” Haun, founder of Kansas City’s Haun Potato Co., died last year at age 91, his son and longtime company president and owner Dave Haun continues the firm’s legacy. The firm serves customers all over the country, but Haun says the strength of his business is in the Midwest and Mid-South U.S.
 
For 2017, Haun said in early August the company was tracking conversion to new crop potato supplies in Idaho, Colorado and Minnesota, with harvest dates a little later than typical.
Both old and new crop potatoes will be available from Idaho, giving customers a choice.
 
“It will boil down to what the customers want and we will be able to service either new or old crop,” he said. The start of school will increase potato and onion demand, which can wilt a little with the summer heat, he said.
 
Locally grown
 
Kansas, with 4,200 acres planted to potatoes in 2016 according to the U.S. Department of Agriculture, has become a success story, Haun said. 
 
Even though southwest Kansas doesn’t have any packing sheds — the spuds are shipped to Colorado or Nebraska for packing — Haun said the deal has momentum.
 
“Over the past few years, they have grown quite a bit of potatoes, and it’s been a success,” he said.
 
Kansas supplies russets, golds and red potatoes, with earlier harvest than Minnesota and other northern growing regions.
 
Haun said Kansas may have taken some of the demand from north Texas potatoes, since the higher elevation start date for north Texas is about the same as Kansas. Kansas potatoes can tie nicely into local retail programs in the Midwest, Haun said.
 
Haun said the influence of contracting is growing in the potato and onion business.
 
“A contract deal saves the party that receives the product from staying out of the super high end of the market if the product is short,” he said. “It does eliminate a little bit of the bottom end, which is a good balance,” he said. “They might not make as much money, but also if you are flooded with too much product, it saves you from a real disaster,” he said.
 
Beyond facilitating the demands of contracts, Haun said that his firm also matches demand with products from regions that make the most sense. “Freight is such a big factor, and depending on what part of the country it is, you have to source areas closer to them.”