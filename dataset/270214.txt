What’s on special?
Everyone loves a bargain. I’ve even discussed the topic with my sweet, silver-haired mom. I teased her about a time she traveled 10 blocks out of her way to redeem a 50-cents-off coupon.
“It was an extra five minutes out of my way, and something I could stock up on,” she said. “Besides, it was a dollar-off coupon, on an on-sale item. So don’t give me any of your sass, boy.” 
Gulp.
Customers young and old appreciate a good buy.
Grocery store shoppers are also an interesting study when it comes to how they react to weekly ad specials, particularly in produce. So much planning goes into developing ads; so much care and attention goes to the details of the newspaper insert, or however the ad reaches the shopper (be it mailings or social media).
Yet with all this information at their disposal, the daily or weekly customer ventures into the produce department with “Most of what I need to buy is in my head” — as my chef-daughter likes to quip. 
We are creatures of habit, and tend to pretty much buy the same things each trip. 
However, we are also open to suggestion when it comes to purchasing food and particularly fresh produce. The majority of shopping lists I’ve swept up or fished out of shopping carts listed dry grocery specifics, but simply read “fruit” and “stuff for salad” under the produce heading.
That’s where a good produce operation can make an impact. So many of your shoppers don’t study the weekly ad insert. I’d venture to say most don’t base their shopping trip comparing you against your competition, with ads spread out on the kitchen table. So it’s up to you to call your ad items to their attention when they shop.  
Build your ad displays. Make them abundant. Keep the display clean, the produce fresh, culled and stocked, and they will shop.  

Ad items naturally follow what’s in season. So variables such as flavor, eye appeal, variety, selection and price are inherent. Looking to offset some of the loss-leader sting? Build comparable-sized displays of items that are equally in peak season, at regular price. Last (or next) week’s ad item, perhaps?
 
I’ll admit to debating with colleagues about what impact ads provide, given that customers are a mixed lot when it comes to perception of what produce is offered and at what price. There are even chains that don’t print ads at all and operate just fine.
To me it all comes down to one word: Value. 
Ad item or not, if you offer a produce item or category that is fresh, stocked in abundance and in some way excites the senses, customers will buy.
Armand Lobato works for the Idaho Potato Commission. His 40 years’ experience in the produce business span a range of foodservice and retail positions. E-mail him at lobatoarmand@gmail.com.