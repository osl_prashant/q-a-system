The Mexican government recently opened a proceeding in which grading standards are proposed for Mexican beef. Mexico’s Secretariat of Agriculture, Livestock, Rural Development, Fisheries and Food (SAGARPA) accepted public comments on the proposal through Dec. 19. The U.S. Meat Export Federation (USMEF) filed comments in this proceeding, raising concerns about how English grade names could be used interchangeably with Spanish names. Thad Lively, USMEF senior vice president for trade access, explains that this could create confusion in the marketplace and diminish the value that the U.S. beef industry derives from the USDA grading system. 
 



 
There are also significant differences that make interchangeable use of the English and Spanish grade names problematic, including differences in marbling scores and in the proposed procedures for determining carcass grades. 
 
USMEF’s comments in this proceeding are also available online: 
Spanish version 
English version