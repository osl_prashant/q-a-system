BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Thursday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeApr        12.25   12.38   12.19   12.35  Up   .14May                                12.46  Up   .12Jun        12.38   12.49   12.33   12.46  Up   .12Sep        12.80   12.89   12.78   12.87  Up   .09Dec                                14.13  Up   .06Feb        14.12   14.15   14.02   14.13  Up   .06Apr        14.34   14.39   14.28   14.35  Up   .05Jun        14.47   14.53   14.42   14.47  Up   .04Sep        14.72   14.79   14.68   14.72  Up   .03Dec                                15.32  Up   .02Feb        15.31   15.33   15.29   15.32  Up   .02Apr        15.26   15.28   15.26   15.28  Up   .04Jun        15.30   15.31   15.30   15.31  Up   .05Sep        15.53   15.53   15.53   15.53  Up   .05