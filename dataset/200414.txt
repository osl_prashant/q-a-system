Milk production in the United States during November totaled 17.1 billion pounds, up 2.4 percent from November 2015.Production per cow in the United States averaged 1,829 pounds for November, 39 pounds above November 2015.
The number of milk cows on farms in the United States was 9.34 million head, 17,000 head more than November 2015, and 4,000 head more than October 2016.
23 Major States: November Milk Production up 2.6 Percent

Milk production in the 23 major States during November totaled 16.1 billion pounds, up 2.6 percent from November 2015. October production, unrevised at 16.5 billion pounds, was up 2.7 percent from October 2015.
Production per cow in the 23 major States averaged 1,851 pounds for November, 40 pounds above November 2015. This is the highest production per cow for the month of November since the 23 State series began in 2003.
The number of milk cows on farms in the 23 major States was 8.67 million head, 33,000 head more than November 2015, and 4,000 head more than October 2016.