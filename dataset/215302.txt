Milk Production: Milk production in the 23 major states during October totaled 16.7 billion pounds, up 1.5% from October 2016. Production per cow for October averaged 1,917 pounds. The number of milk cows was 8.74 million head, 1,000 less than September 2017. September production was revised up 0.1% to 16.2 billion pounds. For milk production in the top 5 producing states, California was down 1.5% from October a year ago; Wisconsin up 2.3%; Idaho up 0.2%; New York down 0.1%; and Texas up 5.3%. Other states with large production increases were Arizona (6.6%), Utah (6.2%), Colorado (5.7%), Kansas (4.6%), Iowa (4.0%), South Dakota (4.3%), and New Mexico (4.0%). The states reporting a large decline in production compared to October 2016 were Oregon (-2.4%) and Washington (-0.5%).
Milk Price and Utilization: The Southeast Uniform milk price for October was $19.12, down $0.49 from September and $0.40 higher than October 2016. The Appalachian Uniform milk price was $18.59, down $0.47 from September and $0.49 higher than October 2016. September’s Class III price was $16.69, up $0.33 from September, and $1.87 higher than October a year ago. The Class IV price was down $1.01 from September to $14.85, and $1.19 higher than October 2016. The Class I Mover price for December is $16.88, up $0.47 from November. The milk/feed ratio for October was 2.45, even with September.
Southeast Class I utilization was 74.40%, down 2.16% from September, and 0.11% higher than October a year ago. The Uniform butterfat price was $2.7619, down 18.47 cents from last month and 58.37 cents higher than October 2016. The October Class I price was $20.24. November Class I price is $20.21. Appalachian Class I utilization was 71.23%, down 0.84% from September, and 0.63% higher than October a year ago. The Uniform butterfat price was $2.7563, down 18.41 cents from last month and 58.83 cents higher than October 2016. The October Class I price was $19.84.
Cheese Production and Stocks: Total cheese production in September was 1.01 billion pounds, up 2.7% from a year ago while butter production was down 0.3% to 135 million pounds. Nonfat dry milk (NDM) production was up 6.2% compared to a year ago with total production at 133 million pounds while skim milk powder (SMP) production was down 21.0% to 30.4 million pounds compared to a year ago. Total cheese stocks at the end of October were up 4% from October a year ago and down 3% from the previous month while butter stocks were down 14% from last month and down 4% from a year ago.
Springer Prices and Cow Slaughter: At Smiths Grove, Kentucky on October 31, supreme springers were $1,450 while US approved springers were $1,300 to $1,350. Dairy cow slaughter in October was 261,000 head, up 11,400 head from September and 23,800 more than October 2016.




Southeast Federal Order Prices




Month


Uniform Price $/cwt.


Class I Price $/cwt.


Class III Price $/cwt.


Class IV Price $/cwt.


Class I
% Utilization


Butterfat Price $/lb.




May 17


17.83


19.00


15.57


14.49


62.92


2.4133




Jun 17


18.70


19.11


16.44


15.89


66.25


2.5760




Jul 17


19.64


20.39


15.45


16.60


67.51


2.8681




Aug 17


20.02


20.52


16.57


16.61


77.22


3.0111




Sep 17


19.61


20.51


16.36


15.86


76.55


2.9466




Oct 17


19.12


20.24


16.69


14.85


74.40


2.7619




 




Appalachian Federal Order Prices




Month


Uniform Price $/cwt.


Class I Price $/cwt.


Class III Price $/cwt.


Class IV Price $/cwt.


Class I
% Utilization


Butterfat Price $/lb.




May 17


17.65


18.60


15.57


14.49


67.37


2.4142




Jun 17


18.37


18.71


16.44


15.89


63.56


2.5883




Jul 17


19.35


19.99


15.45


16.60


65.92


2.8735




Aug 17


19.53


20.12


16.57


16.61


71.62


3.0120




Sep 17


19.06


20.11


16.36


15.86


72.07


2.9404




Oct 17


18.59


19.84


16.69


14.85


71.23


2.7563




 
What is the Market Offering for Milk to be Sold in February?
Ex: It is Nov. 30 and Feb. Class III milk futures are trading at $14.52. Local Feb. basis estimate is +$3.00.




If February futures


=


16.00


14.50


13.00




and actual blend price


=


19.00


17.50


16.00




Sample Strategies


 


 


Realized Prices for Feb. Milk




1) Sold Futures

 

@


14.52

 

17.52


17.52


17.52




2) Bought Put


13.50


@


0.11

 

18.89


17.39


16.39




3) Bought Put


14.00


@


0.25

 

18.75


17.25


16.75




4) Bought Put


14.50


@


0.46

 

18.54


17.04


17.04




5) Synthetic Put

 
 
 
 
 
 
 



     Sold Futures

 

@


14.52

 
 
 
 



     Bought Call


15.50


@


0.14


 


17.88


17.38


17.38




 
What is the Market Offering for Milk to be Sold in March?
Ex: It is Nov. 30 and Mar. Class III milk futures are trading at $15.32. Local Mar. basis estimate is +$3.00.




If March futures


=


16.00


14.50


13.00




and actual blend price


=


19.00


17.50


16.00




Sample Strategies


 


 


Realized Prices for Mar. Milk




1) Sold Futures

 

@


14.57

 

17.57


17.57


17.57




2) Bought Put


13.50


@


0.17

 

18.83


17.33


16.33




3) Bought Put


14.00


@


0.52

 

18.68


17.18


16.68




4) Bought Put


14.50


@


0.53

 

18.47


16.97


16.97




5) Synthetic Put

 
 
 
 
 
 
 



     Sold Futures

 

@


14.57

 
 
 
 



     Bought Call


15.50


@


0.23


 


17.84


17.34


17.34