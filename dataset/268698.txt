Deadly disease spreads to more North Dakota bighorn herds
Deadly disease spreads to more North Dakota bighorn herds

By BLAKE NICHOLSONAssociated Press
The Associated Press

BISMARCK, N.D.




BISMARCK, N.D. (AP) — Deadly bacterial pneumonia among bighorn sheep in the North Dakota Badlands spread to three previously unaffected herds last year, resulting in the smallest estimated population in more than a decade.
State wildlife officials are still confident that this year's fall hunting season will be held, though possibly with fewer licenses.
Game and Fish Department biologists counted 265 bighorns in a population survey that began last fall and was completed in March with the recounting of lambs to see how many survived the winter. It's the lowest count since 2006, according to big game biologist Brett Wiedmann.
The number of adult sheep declined from 2016, and while the lamb count improved slightly, it was still well below the long-term average, he said.
"Fortunately, adult mortality was low in previously affected herds, and lamb survival improved as well, which could indicate those herds initially exposed to the deadly pathogens in 2014 are beginning to recover," Wiedmann said.
The outbreak of disease four years ago killed about three dozen sheep, leading Game and Fish to cancel the fall hunting season in 2015 for the first time in more than three decades. The agency reinstated hunting in 2016 after the deaths tapered off but reduced licenses in 2017 after a summer survey documented a significant drop in the number of rams, which hunters seek for their trophy horns.
Wiedmann said that with the spread of disease to the three new herds, all 15 herds in the Badlands population have the pathogen that can lead to pneumonia. He said the spread "will probably affect the number of licenses we have."
Bighorn hunting is popular in North Dakota, with thousands of people typically applying for fewer than 10 licenses. The licenses are distributed through a lottery, except for one that is auctioned off every year by the Midwest Chapter of the Wild Sheep Foundation to raise money for sheep management. Five licenses were issued last year, down from eight the previous year.
A decision on this year's hunting season will be made in September, after a summer survey of the sheep population.
"The next few years will be important in determining if the state's population shows signs of recovering from the disease outbreak, or if the pathogens are likely to persist and cause a long-term population decline," Wiedmann said.
___
Follow Blake Nicholson on Twitter at: http://twitter.com/NicholsonBlake