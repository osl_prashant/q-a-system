Feeding calves a higher plane of nutrition in the milk-feeding phase is becoming an increasingly popular practice in the U.S.
In addition to many studies showing improved stature growth, rate of gain and health of calves raised this way, some studies have also shown a milkproduction advantage later in life. Researchers at Virginia Tech completed a study examining the physiological reasons for why more nutrients might lead to more milk.
Researcher Adam Geiger says the study focused specifically on early life mammary tissue development. He and his colleagues also explored the effects of exogenous estrogen, which is one of the most important stimuli for mammary tissue development.
“We set out to determine whether feeding more nutrients in the preweaning period promotes mammary tissue growth, and whether it creates a mammary gland and mammary cells that are primed to respond more readily to estrogen,” Geiger explains.
They began the study with 36 Holstein heifer calves (all less than a week old) that were split into two feeding groups:

A traditional restricted milk replacer diet of 1 lb. of 20:20 milk replacer per day.
An enhanced milk replacer diet of 2.5 lb. of 28:25 milk replacer per day.

All calves were offered the same free-choice, 22% crude protein starter grain beginning at week five of the trial and were weaned from milk replacer at eight weeks. At weaning, six calves from each feeding group were euthanized and their mammary tissue analyzed. The remaining 24 calves were given either a placebo or an estrogen implant, creating four remaining study groups (conventional or enhanced diet, with or without estrogen). Those animals were euthanized and their mammary tissue evaluated two weeks after the estrogen/ placebo administration.
Mammary tissue development was assessed by taking measurements of the udder and teats; and measuring mass and tissue composition of the parenchyma and mammary fat pad. The team also measured total protein, DNA and fat in these structures, as well as mammary tissue ductal development.
The results confirmed feeding an enhanced diet in the preweaning period did, indeed, support greater mammary tissue development, which was further accelerated by estrogen supplementation postweaning. Specifically:

Enhanced-fed calves had more than five times more trimmed mammary tissue mass than those fed a conventional diet.
Providing estrogen after weaning increased mammary tissue weight in enhanced-fed calves, but had little effect on those fed a conventional diet.
Feeding an enhanced diet also increased the total protein, DNA and fat in the mammary fat pad and total protein and DNA in the parenchyma.
Development and complexity of mammary ducts and related tissue was minimal in the conventional group, more advanced in the enhanced-fed group, and was increased by estrogen in both.

After the study, the researchers analyzed the cellular mechanisms that changed based on diet and estrogen supplementation. They found mammary tissue cells in the enhanced-fed calves were better equipped to respond to estrogen, which explains why estrogen supplementation significantly increased mammary tissue mass in those animals.
“Our findings reinforced the idea that feeding more nutrients early in life helps develop more mature, complex mammary tissue that is better prepared to respond to the estrogen produced by the ovaries in growing heifers,” Geiger concludes. “This appears to be yet another benefit of feeding calves better early in life.”
 
Note: This story appears in the December 2017 magazine issue of Dairy Herd Management.