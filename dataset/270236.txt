BCfresh has hired Steve Young as vice president of sales.
Young has been with The Oppenheimer Group, Vancouver, British Columbia, since 2008, as a salesman, category manager for citrus, sales manager of the company’s Calgary office, and most recently sales director for Canada.
He officially joins BCfresh, a grower-shipper of potatoes and other vegetables, on April 23, according to a news release.
BCfresh President and CEO Murray Driediger said Young’s experience, leadership qualities and industry relationships made him an ideal candidate for the job.
“Steve’s experience with some of the industry’s top companies will serve him well in leading our sales efforts to the next level,” Driediger said in the release.
With Young joining the company, vice president of sales and marketing Brian Faulker moves to vice president of business development and marketing.
In 2014, the Canadian Produce Marketing Association award Young the Mary FitzGerald award, which recognizes younger industry leaders.
BCfresh recently celebrated its 25th anniversary and is moving into a new 55,000-square-foot distribution facility