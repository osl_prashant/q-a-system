BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Wednesday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeMar                                12.76  Up   .14Apr        12.64   12.83   12.58   12.76  Up   .14May                                12.97  Up   .08Jun        12.90   13.02   12.84   12.97  Up   .08Sep        13.27   13.37   13.24   13.33  Up   .06Dec                                14.23  Up   .02Feb        14.21   14.26   14.14   14.23  Up   .02Apr        14.39   14.39   14.30   14.38  Down .01Jun        14.49   14.52   14.42   14.49  Down .02Sep        14.79   14.80   14.71   14.77  Down .04Dec                                15.25  Down .06Feb        15.29   15.29   15.20   15.25  Down .06Apr        15.30   15.30   15.24   15.28  Down .04Jun        15.36   15.38   15.36   15.38  Down .03Sep                                15.63  Down .02