Four Seasons adds software
Ephrata, Pa.-based Four Seasons Produce Inc. is “investing heavily into new systems to give our team better tools to do their jobs and ultimately serve our customers better,” said Jason Hollinger, general manager.
The company recently launched new transportation management software, which “gives us one robust system to coordinate all aspects of transportation,” Hollinger said.
 
John Vena Inc. expands
Philadelphia-based John Vena Inc. expanded into an additional unit on the Philadelphia Wholesale Produce Market and now occupies about 30,000 square feet, including avocado and mango ripening rooms, plus a packing/production area, said Emily Kohlhas, sales and marketing coordinator.
The company also recently hired Joseph Gallagher Jr. as chief financial officer and Nora Schmidt as food safety and quality assurance manager.
Gallagher brought more than 30 years’ experience in accounting and strategic financial planning.
Schmidt has experience in the food/beverage and chemical industries.Vena’s packing facility continues to grow, Kohlhas said.
Vena now also offers transportation services, with tractor-trailer delivery, backhaul, consolidation and other logistic services, Kohlhas said.
 
M. Levin & Co. adds units
Philadelphia-based wholesaler M. Levin & Co Inc. has expanded its presence on the Philadelphia Wholesale Produce Market from four units to six, said Mark Levin, co-owner.
The addition includes new banana ripening rooms, which accommodate more than 400 pallets, Levin said.
The company also closed an old off-market warehouse and moved everything to its central location, Levin said.
The two new units accommodate a total of eight new rooms, each of which has a 48-pallet capacity, Levin said.
 
TMK works with food banks
Philadelphia-based TMK Produce is testing a logistics operation for a regional cooperative of food banks, called MARC, said Tom Kovacevich, president.
MARC is a regional cooperative that is a Feeding America concept being operated by Feeding PA and the member food banks, Kovacevich said.
“The idea is to consolidate donations of full load commodities that can then be shipped to smaller food banks as mixed loads,” he said.
“We have been operating with the MARC for about a month now, and I think the response has been very positive.”
 
Tom Curtis boosts distribution
Philadelphia-based distributor Tom Curtis Produce Inc. is working to expand its distribution radius, said Tom Curtis, president.
“We’re branching out into different areas, different parts of the country, reaching out a little farther,” he said.
“We’re going (delivering) to the Carolinas and past Pittsburgh, trying to reach a little deeper than what we were doing.”
The effort started in 2016, he said.