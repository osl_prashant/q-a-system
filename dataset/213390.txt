Bill Schuler, former CEO of Castellini Cos., receives the 2017 PMA Bob Carey Award as presenter Jan DeLyser of the California Avocado Commission looks on. Photo by Greg Johnson
NEW ORLEANS – Former Castellini Cos. CEO Bill Schuler took home the Robert Carey Leadership Award Oct. 19 at PMA Fresh Summit.
Last year’s honoree Jan DeLyser, vice president of marketing for the California Avocado Commission, presented the award to Schuler, saying he “embodies tenacious leadership.”
She said his peers recognized Schuler as a man of character and integrity who held many positions within Produce Marketing Association leadership, including being chairman of the board in 2010.
Schuler said he feels like he’s come full circle, as he presented the inaugural Carey award to Mike O’Brien of Monterey Mushrooms five years ago.
“To receive this today is very humbling,” Schuler said.
Schuler was CEO of Cincinnati-based Castellini until the start of this year, when he became a consultant and advisor at the company.