There are a lot of moving pieces in the trade situation between the U.S. and China. 

Earlier this month, the U.S. and China announced tariffs on more than $100 billion on goods between the two countries. Lists of items on both lists continued to grow, and soybeans are one agricultural product in which China could soon place a tariff.

If China does place a tariff on U.S. soybeans, this could put more pressure on South America to supply soybeans to the world’s second-largest economy.

Mark Freight, managing director of International Agribusiness Group, LLC., doesn’t think this will change the demand picture for China.

“Their crushers are screaming just as loudly as U.S. farmers not to have that tariff,” he said on AgDay. “There’s a short-term opportunity for them to buy more beans and maybe export a little meal, so margins are good.”

If China reduces their soybean imports from the U.S., Feight  thinks the U.S. could take more European business.

“It all offsets—we overlap that business anyway and it’s pretty easy to switch,” he said.

Hear what Feight says the acreage could look like in the future if this tariff goes into effect on AgDay above