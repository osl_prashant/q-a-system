Senate passes Kansas State Fair tax bill
Senate passes Kansas State Fair tax bill

The Associated Press

HUTCHINSON, Kan.




HUTCHINSON, Kan. (AP) — Kansas senators have unanimously passed a bill for state sales taxes collected during the Kansas State Fair to be used for capital improvements at the fair.
But The Hutchinson News reports that the bill only would allow for money to return to the fair if the event stays in Hutchinson.
The measure passed after a House bill was introduced that would require the State Fair Board to seek proposals from other cities interested in hosting the fair.
House bill sponsor Republican Rep. Don Schroeder of Hesston says the goal was to encourage talks between the city and the fair, where officials have expressed frustration with a hike in city stormwater fees.
Hutchinson has been home to the State Fair for more than a century.
___
Information from: The Hutchinson (Kan.) News, http://www.hutchnews.com