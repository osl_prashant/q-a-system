The international element of the pomegranate deal is shifting some with Peruvian fruit entering the country for the first time.
The U.S. Department of Agriculture ruled in favor of allowing Peruvian pomegranates into the country, effective in early August, provided pest management protocol is followed.
The additional imports will help round out the yearly supply of pomegranates to U.S. retailers.
Mike Forrest, president of Reedley, Calif.-based Youngstown Distributors Inc., said having pomegranates available more months out of the year is good for the market.
"People realize now that they are one of the healthiest fruits and having them available year-round in whole as well as arils is a benefit," he said.
Levon Ganajian, director of retail relations, Fresno, Calif.-based Trinity Fruit Sales, said Peru won't add any competition to California growers.
"There won't be an overlap," he said.
Ganajian said the quality out of Chile hasn't been as favorable, so he is excited to expand their Peruvian imports moving forward.
"We bring in a few pomegranates out of Chile, but more as a service to our customers who request that. We'll switch right to Peru when we're done with California, which allows us year-round offerings for the first time," Ganajian said.
Peru should hit markets slightly ahead of Chile at the end of the California season.
"It will depend on volumes to see if the industry gaps between California and Peru," Forrest said.
Tom Tjerandsen, manager at the Sonoma, Calif.-based Pomegranate Council, doesn't expect a lot of new volume at first, although reports have said more substantial volumes are expected.

Exports
On the export side, Tjerandsen said the industry is working to expend U.S. pomegranate exports.
"At the current time, about 40% of our harvest is exported with the principle destinations being Canada and Korea," Tjerandsen said.
According to Tjerandsen, overseas interest in U.S. pomegranates continues to grow.
"Our total domestic harvest volume is increasing so even though the percentage that is exported has remained the same, the total export volumes have been increasing too," he said.
Right now, efforts in underway to develop an approved protocol to ship to the Philippines and China.
"We're hoping to open those two new markets, and once we do, I think they will account for even more substantial volumes," Tjerandsen said.
Tjerandsen said it's important to work with retailers to understand the culture.
"Brazil is a rapidly growing market, and if you ask Brazilian retailers why people traditionally buy pomegranates, they'd say the surge was around New Year's because of a belief that carrying five arils in your wallet will bring luck," he said.
"It can be challenging to get people to expand beyond those traditions into new dishes and uses," Tjerandsen said.
Atomic Torosian, a partner in Fresno, Calif.-based Crown Jewels Marketing LLC, said the company ships a considerable number of pomegranates to Korea.
"That's the largest export market for us, followed by Japan and Australia and Mexico. We don't consider Canada as an export," Torosian said.