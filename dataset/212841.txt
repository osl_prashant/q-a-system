Potatoes from California’s Kern County face a possible gap in harvest because excessive winter rain created a gap in planting.“The rain did hamper our planting season for our potato crop which may create some gaps as we harvest. However, weather patterns from here moving forward can also change how those fields come off,” said Brett Dixon, president and sales manager of Bakersfield, Calif.-based Top Brass Marketing Inc.
At Top Brass, conventional red, yellow and white potato yields are expected to be lower than usual in May, but back to normal by June or July. 
The firm’s organic red and yellow potatoes began shipping the second week of April and will continue through July. Organic russets will begin at the end of May or early June and continue into August.
“Quality appears fantastic as the growing season has been very good,” Dixon said.
“Big L planted in early December but then because of the rain we had a gap in planting. It’s possible we may have a gap in harvest as well,” said Tom Drulias, owner of TD Produce Sales in Bakersfield.
Drulias said any potential gaps would occur in late May.
TD Produce handles the conventional potato sales for Big L Packers in Edison, Calif. Big L grows and packs white, red, yellow, fingerling and purple potatoes.
As of April 5, Drulias remained optimistic that the weather would warm up, allowing for a “good early start” on the white and red harvest.
The weather conditions combined with a reduction in the overall storage crop and about an 8% reduction in potato acreage across Kern County (in 2016 it was4,856 acres for fresh varieties, according to the Kern County Department of Agriculture) means potato prices are expected to be higher this year than last.
The markets look particularly strong for reds and yellows.
For the reds, the market is strong because they showed the highest drop in acreage. Red acreage is down approximately 250 acres, or about 12.5% from 2016.
The storage crop yellows “are cleaning up fast,” and there are “some quality issues with storage yellows as well,” Drulias said.
Edison, Calif.-based Johnston Farms expects potatoes to be available the last week of April or the first week of May, said Derek Vaughn, salesman.
The firm has planted white, red and yellow potatoes, with its total potato acreage down 7% from last year.
Vaughn said the rains “messed with planning for the potatoes,” but the two-week planting gap “was nothing major.”
There have been no pest or disease-related problems this season, and quality looks excellent, he said.
Bakersfield-based Grimmway Farms began the harvest on their Coachella Valley, Calif., crop on April 15, said Valorie Sherman, communications and engagements manager.
Production moves to Bakersfield in the spring and then to Lancaster, Calif., in the summer. This migration allows the firm to avoid planting gaps and potential harvest delays like the ones experienced across much of Kern County this season, Sherman said.
The company expects a steady supply of their red and gold King Pak potatoes as well as their proprietary Avalanche white potatoes through August.
“The quality of our crop looks great this year,” Sherman said.