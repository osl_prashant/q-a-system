The Drought Monitor has showed improvement for water conditions in California.Currently, the Golden State is in the least amount of drought for the past three years with 5.5% of the state having no signs of drought.

Despite those improvements 42% of the state is either in extreme or exceptional drought.