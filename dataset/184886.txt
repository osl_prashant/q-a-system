Anhydrous is $80.52 below year-ago pricing -- lower $4.47/st this week at $525.23.
Urea is $21.90 below the same time last year -- lower $3.65/st this week to $355.55.
UAN28% is $31.44 below year-ago -- lower 77 cents/st this week to $251.58.
UAN32% is priced $34.81 below last year -- higher 5 cents/st this week at $277.36.

Anhydrous was our downside leader in the nitrogen segment this week. Missouri fell $25.15 to lead NH3 declines as Michigan fell $16.10 and Nebraska shucked $8.37. Six states were unchanged as only Indiana moved higher, firming $1.06.
Urea was led lower by Missouri which fell $18.90 as South Dakota softened $7.84 and Michigan declined $6.25. Three states were unchanged as Indiana fell 92 cents per short ton and Minnesota softened 55 cents.
UAN28% was also lower as Nebraska softened $10.51, Michigan fell $2.387 and Iowa dropped 33 cents. Five states were unchanged as South Dakota firmed $2.36, and Minnesota and Indiana each gained a little more than a dollar.
UAN32% firmed all of a nickel on the week led by Indiana which added $5.30 and Iowa gained 48 cents. Eight states were unchanged as Nebraska fell $5.16 and Illinois softened 14 cents.
Had anhydrous not fallen as much as it did, the margins between our nitrogen products would have continued to tighten. But the urea/NH3 spread actually widened slightly as anhydrous outran urea to the downside. This week's wider margins may signal nitrogen is set to run aggressively lower. But we have to keep the season in mind, and while this week's lower prices may be a sign that spring demand has been satisfied, the will almost surely be farmers coming back to market for some replacement nitrogen in areas impacted by saturated field conditions. The resulting nitrogen loss may lead to a deferred nitrogen demand push, late in the season.
It is likely that all acreage decisions have been finalized, and we do not expect a major shift from beans to corn. Some drowned out corn fields may go to beans given the constraints of time, but we do not expect nitrogen demand for fresh corn acres. That will help keep a lid on demand-based price increases for late season nitrogen, but the risk that already planted corn acres that suffer nitrogen loss could amount a slight, short-lived demand bump.
For now, it does appear that nitrogen has topped for the time being, and some may choose this week's price point to book some nitrogen for fall. I do not recommend booking 100% of your expected fall needs at this time, nor am I willing to offer official Inputs Monitor advice. Having said that, a guy could do worse. Having said that, if your crop is or has been underwater and you anticipate needing extra UAN, book it at current prices to avoid supply shortfalls and higher prices.
December 2017 corn closed at $3.88 on Friday, May 5. That places expected new-crop revenue (eNCR) per acre based on Dec '17 futures at $614.92 with the eNCR17/NH3 spread at -89.69 with anhydrous ammonia priced at a discount to expected new-crop revenue. The spread widened 9.55 points on the week.





Nitrogen pricing by pound of N 5/10/17


Anhydrous $N/lb


Urea $N/lb


UAN28 $N/lb


UAN32 $N/lb



Midwest Average

$0.32 1/4


$0.39 1/2


$0.45 1/4


$0.43 1/2



Year-ago

$0.37 1/4


$0.41 3/4


$0.50 1/2


$0.48 1/2





The Margins by lb/N -- UAN32% is at a 1 1/4 cent premium to NH3. Urea is 2 1/4 cents above anhydrous ammonia; UAN28% solution is priced 1 cent above NH3. The margins continue to narrow.





Nitrogen


Expected Margin


Current Price by the Pound of N


Actual Margin This Week


Outstanding Spread




Anhydrous Ammonia (NH3)


0


32 1/4 cents


0


0




Urea


NH3 +5 cents


39 1/2 cents


+7 1/4 cents


+2 1/4 cents




UAN28%


NH3 +12 cents


45 1/4 cents


+13 cents


+1 cent




UAN32%


NH3 +10 cents


43 1/2 cents


+11 1/4 cents


+1 1/4 cents