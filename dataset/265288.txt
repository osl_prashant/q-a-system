BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Friday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2642  Up    82May         2533    2636    2511    2615  Up    80Jul         2557    2662    2537    2642  Up    82Sep         2569    2677    2551    2657  Up    83Dec         2571    2679    2553    2659  Up    82Mar         2558    2663    2540    2644  Up    81May         2546    2661    2546    2648  Up    79Jul         2600    2672    2600    2655  Up    77Sep         2612    2679    2612    2662  Up    76Dec         2643    2689    2643    2670  Up    75