A food safety group has been formed by leaders in controlled environment agriculture — primarily focused on companies identified as indoor, urban, vertical, rooftop and local operations.
The first meeting of the group is set for the June 25-27 United Fresh Produce Expo in Chicago, according to Paul Lightfoot, CEO of BrightFarms. Other organizing members include AeroFarms and Little Leaf Farms, with more companies expected to announce their participation, he said.
“We looked at the field salad organizations, the California Leafy Greens Marketing Agreement, and we think they have done a good job of bringing together competitors to focus on food safety as a non-competitive issue, where they all cooperate,” Lightfoot said.
While Lightfoot said controlled environment growers produce similar commodities to open field growers, they have a different production environment.
“We actually think that there is a structure for a safer supply chain with controlled environment (growing),” he said.
Growing in a controlled environment is also different, Lightfoot said.
“We thought it made sense to voluntarily all get together, and on a non-competitive basis, share best practices about how to grow safely in controlled environments, so the whole industry is safer and proactively attempt to head off any future problems that would be bad for consumers or the industry,” Lightfoot said.
Lightfoot said he will attend the meeting in Chicago and looks forward to helping to lead the direction of the new group.
He said BrightFarms and similar companies see promising days ahead.