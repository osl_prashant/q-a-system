Supplies of tomatoes, the No. 1 commodity imported from west Mexico through the Nogales, Ariz., port of entry, are plentiful this season. Perhaps too plentiful, distributors say.
On Feb. 7, f.o.b. prices of two-layer cartons of vine-ripes crossing through Nogales ranged from $6.64-7.37, depending on size, according to the U.S. Department of Agriculture. A year earlier, they were selling for $12.95-14.95.
"It's been a struggle," said Jimmy Munguia, sales manager for Del Campo Supreme Inc., Nogales. "It's been a tough road."
He attributed the rock-bottom prices to too much supply and too little demand.
It's hard to tell when or if the situation will improve, he said.
"We'll have to wait and see."
Meanwhile, quality of all those tomatoes has been "outstanding," he said, due largely to good growing conditions and stable weather.
"We haven't had temperature fluctuations this year like we have in past years," Munguia said.
Volume at Del Campo Supreme might be down slightly from last year, he said. The company should have tomatoes through May.
The firm ships tomatoes-on-the-vine, romas, grapes, vine-ripes and beefsteak tomatoes.
Tomatoes are not alone in their pricing woes, said Fried DeSchouwer, president of Vero Beach, Fla.-based Greenhouse Produce Co. LLC.
"Perfect growing conditions all around have given Mexico a flat vegetable market with very few or no items bringing good money," he said. "The tomato category has been a poor performer in general."
He doesn't see much relief once Florida's spring season kicks in and overlaps with the latter part of Mexico's winter season.
And volume likely will increase in March and April when Canadian Greenhouse shippers add more product to the supply chain, he said.
The tomato oversupply goes beyond a typical supply and demand situation, said Brian Bernauer, director of operations and sales for Calavo Growers Inc., Nogales.
Consumers now are faced with a growing array of tomatoes to choose from, including round, roma, cherry, beefsteak, snacking tomatoes and more.
"The category is so inundated with varieties that it gets clogged up even with the same volume as last year," he said.
Climate change may be another culprit.
"The warming trend has changed, so things are coming out earlier," Bernauer said. "It squeezes and changes historic and traditional windows."
The tomato glut started just before the holidays when good growing weather and high yields saw Florida growers ramping up their volume, he said.
The industry has not been able to catch up, even though distributors are shipping all the tomatoes they receive, he said.
That might change as a result of high winds that were reported in Florida in late January.
"Look for a break in overall supplies starting sometime the first couple of weeks of March," Bernauer said.
If conditions continue as they are, though, "It will be a struggle."