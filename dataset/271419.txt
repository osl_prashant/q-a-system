Trump likes coal, but that doesn't mean he's hostile to wind
Trump likes coal, but that doesn't mean he's hostile to wind

By STEVE LeBLANCAssociated Press
The Associated Press

BOSTON




BOSTON (AP) — President Donald Trump has courted coal miners and cast doubt on whether fossils fuel contribute to climate change, but that hasn't translated into hostility for renewable energy — particularly offshore wind.
Using federal offshore leases, wind power projects along the East Coast, including off the shores of Massachusetts, New Jersey, Connecticut, Virginia and New York, are pressing ahead with the goal of transforming the electric grid and providing energy to power millions of homes.
The administration is looking to renewable energy sources to help create "energy dominance" that will guarantee America is a leading global energy exporter and can't be held hostage by foreign energy-producing powers, Interior Secretary Ryan Zinke says — even as Trump's plan to expand offshore drilling has drawn harsh criticism from environmentalists and coastal state governors of both parties.
"On designated federal lands and off-shore, this means an equal opportunity for all sources of responsible energy development, from fossil fuels to the full range of renewables," Zinke said in a recent op-ed in The Boston Globe. "As we look to the future, wind energy — particularly offshore wind — will play a greater role in sustaining American energy dominance."
To help streamline the effort, Zinke wrote, he wants to give developers more flexibility by letting them postpone detailed design decisions until later in the planning process to let them take advantage of the latest technology.
West Coast states are also hoping to turn their offshore winds into energy. One planned project along the Northern California coast would create a 100-150 megawatt floating wind farm more than 20 miles offshore. Projects have also been eyed off Hawaii, in the Gulf of Mexico and even in the Great Lakes.
Renewable energy supporters are backing the development of offshore wind power — with a few caveats.
Offshore wind can help reduce carbon emissions, but it's critical to ensure they don't harm underwater ecosystems, especially the endangered North Atlantic right whale, said Sean Mahoney, executive vice president of the Conservation Law Foundation.
Long in the planning and discussion phase, the push for offshore wind power is picking up steam.
In New York, Democratic Gov. Andrew Cuomo has called for more projects and said the state will solicit in 2018 and in 2019 a combined total of at least 800 megawatts — with a long-term goal of developing 2.4 gigawatts of offshore wind by 2030, enough to power up to 1.2 million homes.
In New Jersey, the Danish offshore wind company Orsted, which is also working on projects in Massachusetts and Virginia, has set a goal of supplying enough energy for 1.5 million homes. Democratic Gov. Phil Murphy has said he wants New Jersey to generate 3,500 megawatts of offshore wind energy by 2030.
And in Massachusetts, Republican Gov. Charlie Baker is hoping to generate 800 megawatts of offshore wind power as a first step toward a goal of 1,600 megawatts. Three projects — Vineyard Wind, Bay State Wind and Deepwater Wind — are all vying to produce some or all of the 800 megawatts.
Vineyard Wind wants to build its wind farm on a 160,000-acre area south of the island of Martha's Vineyard and 14 miles from the nearest shore. The project — partly owned by Portland, Oregon, renewable energy developer Avangrid Renewables and Copenhagen Infrastructure Partners of Denmark — would consist of turbines spaced at least eight-tenths of a mile apart.
Bay State Wind, an initiative of Orsted and the utility Eversource, would be about 20 miles south of Martha's Vineyard and could power 500,000 homes. Orsted and Eversource are also hoping to build a 200-megawatt wind farm 65 miles off New London, Connecticut. In Virginia, Orsted is partnering with Dominion Energy on a wind project off Virginia Beach.
Deepwater Wind is also looking to build in federal waters southwest of Martha's Vineyard. The company, which also hope to provide 200 megawatts of wind power to Connecticut, can already claim bragging rights to having opened the nation's first offshore wind farm off Rhode Island's Block Island last year — a five-turbine project generating about 30 megawatts.
Even as the various projects and others move ahead, Zinke has announced that two additional areas off Massachusetts totaling nearly 390,000 acres are now available to be leased for commercial wind energy development.