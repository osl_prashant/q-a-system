BC-Business News Preview
BC-Business News Preview

The Associated Press



Among the stories Thursday from The Associated Press:
TOP STORIES:
FACEBOOK-CAMBRIDGE ANALYTICA — In the wake of a privacy scandal involving a Trump-connected data-mining firm, Facebook CEO Mark Zuckerberg embarked on a rare media mini-blitz in an attempt to take some of the public and political pressure off the social network. By Barbara Ortutay, Danica Kirka and Gregory Katz. SENT: 930 words, photos, video.
OF MUTUAL INTEREST-LURE OF BEATING THE MARKET — After years of falling short of index funds, stock-picking managers of mutual funds had a big pickup in performance last year, and most acquitted themselves well during the market's sell-off last month. But the majority still haven't been able to keep up with index funds over the long term. Experts say investors' primary goal should be to invest in funds with low expenses, before even considering whether they track indexes or are trying to beat them. By Stan Choe. UPCOMING: 850 words by noon, photos.
HARVEY-SILENT SPILLS- — The toxic onslaught from the nation's petrochemical hub was largely overshadowed by the record-shattering deluge of Hurricane Harvey as residents and first responders struggled to save lives and property. But a half-year after floodwaters swamped America's fourth-largest city, the extent of the environmental assault is beginning to surface, though questions about the long-term consequences for human health remain unanswered. By Frank Bajak of AP and Lise Olsen of the Houston Chronicle. SENT: 3,700 words, with photos, video, and abridged version of 900 words, as well as a sidebar slugged Harvey-Silent Spills-Arkema.
REVEAL-JP MORGAN CHASE-REDLINING
America's biggest bank is benefiting from a loophole in the Community Reinvestment Act, a landmark law designed to fight racial discrimination in lending. An analysis by Reveal from The Center for Investigative Reporting found that JP Morgan Chase rarely lent to African Americans or Latinos in the Washington area, where a majority of residents are people of color. By Aaron Glantz and Emmanuel Martinez of Reveal. SENT: 1,280 words, photos.
MARKETS & ECONOMY:
FINANCIAL MARKETS — Stocks are opening lower as President Donald Trump is expected to impose trade sanctions on China. SENT: 150 words. UPCOMING: Will be updated through 5 p.m.
MORTGAE RATES— Freddie Mac reports on this week's average U.S. mortgage rates. By Marcy Gordon. UPCOMING: 130 words after release of report at 10 a.m. 300 words by 10:45 a.m.
FEDERAL RESERVE-POWELL'S PERFORMANCE — In his first news conference as head of the world's leading central bank, Jerome Powell avoided any professorial lectures. His replies were briefer than his predecessors'. He said nothing of himself personally. He projected the air of an experienced technocrat, more steeped in finance than the complexities of economic theory. If anyone was wondering how the new chairman of the Federal Reserve would differ from his two immediate predecessors, his exchange with reporters offered some clues. By Josh Boak. SENT: 740 words, photos. Story moved after midnight.
INDUSTRY:
TOYS R US-BID EFFORT — Toy company executive Isaac Larian and other investors have pledged a total of $200 million and hope to raise four times that amount in crowdfunding in a bid to save potentially more than half of the 735 Toys R Us stores that will go dark in bankruptcy proceedings. By Anne D'Innocenzio. SENT: 730 words, photos.
TRUMP-TRADE — Farmers, electronics retailers and other U.S. businesses are bracing for a backlash as President Donald Trump targets China for stealing American technology or pressuring U.S. companies to hand it over. By Paul Wiseman. SENT: 680 words, photos.
TRUMP-CHINA TRADE — The Chinese government vowed Thursday to take "all necessary measures" to defend the country's interests if President Donald Trump targets it for allegedly stealing American technology or pressuring U.S. companies to hand it over. SENT: 240 words, photos.
CHINA-Volkswagen — Volkswagen unveils an SUV lineup for China, adding to growing efforts by global automakers to design models specifically to appeal to local tastes in the biggest auto market by number of units sold.
GLOBAL TOURISM — The travel and tourism sector is set for a modest slowdown in 2018 as a result of higher oil prices and airfares, a year after it experienced its best year on record, according to a leading global industry body. By Pan Pylas. SENT: 370 words, photos.
WYNN-STOCK SALE — Former Wynn Resorts CEO Steve Wynn is selling about 4 million shares that he owns in the casino-operating company. SENT: 120 words, photos.
STARBUCKS-CRYSTAL BALL FRAPPUCINO — Starbucks, thirsty for some social media magic, is releasing its latest sugary concoction: the fortune-telling Crystal Ball Frappucino. SENT: 180 words, photos.
TECHNOLOGY & MEDIA:
AT&T-ANTITRUST TRIAL — The Trump administration is facing off against AT&T to block the telephone giant from absorbing Time Warner, in a case that could shape how consumers get — and how much they pay for — streaming TV and movies. SENT: 130 words, photos. UPCOMING: Will be updated.
YOUTUBE-FIREARM VIDEOS — YouTube has tightened its restrictions on firearms videos. The video-serving network owned by Google is banning videos that provide instructions on how to make a firearm, ammunition, high-capacity magazines, and accessories such as bump stocks and silencers. SENT: 110 words.
TURKEY-MEDIA-SALE — Turkey's largest media group said Thursday it is in talks with a business group close to President Recep Tayyip Erdogan for the sale of its outlets, a development that further curtails independent journalism in the country that has taken an increasingly authoritarian turn under his leadership. By Susan Fraser. SENT: 370 words, photos.
ISRAEL-GARBAGE TO GOLD — Israeli start-up UBQ says its innovative method of converting garbage into plastics, five years in the making, will revolutionize waste management worldwide and make landfills obsolete. SENT: 930 words, photos.
BOOKS-THERANOS — The publisher of an investigative book on blood testing startup Theranos has moved up the release date from October to this spring. SENT: 130 words.
WASHINGTON:
BUDGET BATTLE — Congressional leaders have finalized a sweeping $1.3 trillion budget bill that substantially boosts military and domestic spending but leaves behind young immigrant "Dreamers," deprives President Donald Trump some of his border wall money and takes only incremental steps to address gun violence. SENT: By Andrew Taylor and Lisa Mascaro. 1,080 words photos.
PERSONAL FINANCE:
NERDWALLET-HOME VIEWS— A house with a fabulous view can be hard for a homebuyer to resist. But seeing the mountains, water or city lights from the comfort of home comes at a price. The hazy part is figuring out what that added cost is — and whether it's worth it. By Marilyn Lewis, NerdWallet. UPCOMING: 810 words by 11 a.m, photo.
INTERNATIONAL:
GERMANY-ECONOMY— German business confidence has fallen amid growing concerns about international risks like the possibility of a trade war with the United States. SENT: 150 words.
BRITAIN-ECONOMY — The Bank of England kept its interest rates unchanged Thursday but appeared to issue another broad hint that they may rise again in May to get inflation back to target. By Pan Pylas. SENT: 580 words.