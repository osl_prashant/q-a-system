BelleHarvest adds to sales staff
BelleHarvest Sales Inc., Belding, Mich., has expanded its sales staff to help retailers better execute apple promotions, said Chris Sandwick, vice president of sales and marketing.
Rafferty Fynn, who has 20 years’ experience as a produce manager for Minneapolis-based Lunds & Byerly’s, is based in Minnesota and now serves BelleHarvest customers on the western side of Lake Michigan.
RJ Simmons will continue to manage Eastern retail accounts.
 
Jack Brown makes upgrades to lines
A total upgrade of the two sorting lines at Jack Brown Produce Inc., Sparta, Mich., should allow employees to pack more product in less time and enable the company to move greater volume of apples with fewer defects, said Tom Labbe, sales manager.
The company now is a pack-to-order shed, he said.
 
Apple committee supports tastings
The Lansing-based Michigan Apple Committee is working with the University of Michigan and Wayne State University on an apple-tasting program involving elementary and middle schools in Michigan, said Diane Smith, executive director for the apple committee.
“They go in with three or four different varieties, and the kids get to taste them and take (information) home to mom and dad telling them which variety they like the best,” Smith said.
Support materials are available in English, Spanish and Arabic.
 
North Bay moving to new building
North Bay Produce, Traverse City, Mich., plans to move to a new 15,000-square-foot headquarters next to its existing facility in mid-September, said Ken Korson, apple category manager.
The company also has installed a new software system designed to streamline interaction with growers, the packinghouse and customers.
The new system should speed up the process of entering and receiving orders.
 
Riveridge adds earlier varieties
Riveridge Produce Marketing Inc., Sparta, Mich., has pulled out some orchards in favor of earlier-maturing strains of gala and Honeycrisp apples, president Don Armock said.
The company’s volume should be “somewhat smaller” this year compared to its five-year average, but volume of gala and Honeycrisp varieties should be up.
The company also has built a new plant for its apple cider business and hired industry veteran Eric Rockfellow to run it, Armock said.
Riveridge now offers fuji as well as gala, Honeycrisp and blended cider flavors and plans to introduce a tart version next season to complement its sweet-style cider.