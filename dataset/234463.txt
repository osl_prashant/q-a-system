Report: Nearly half of parched Kansas wheat crop struggling
Report: Nearly half of parched Kansas wheat crop struggling

The Associated Press

WICHITA, Kan.




WICHITA, Kan. (AP) — A new government report estimates that nearly half of the winter wheat crop in Kansas is struggling for lack of adequate moisture.
The National Agricultural Statistics Service reported Monday that 49 percent of the wheat is in poor to very poor condition. It rated the remaining crop as 39 percent fair, 11 percent good and 1 percent excellent.
That assessment comes amid estimates that topsoil moisture supplies are running short to very short across 74 percent of Kansas. Subsoil moisture levels were faring only slightly better with 71 percent rated as short to very short.