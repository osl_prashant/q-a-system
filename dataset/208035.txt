Peach season has started in New Jersey, with growers reporting good volumes and high-quality fruit.The season runs from late June through mid-September.
“My crop is coming in very strong, I’ve started picking and public demand is healthy,” Santo John Maccherone, chairman of the New Jersey Peach Promotion Council and owner of Circle M Farms, Mullica Hill, said in a news release.
At Holtzhauser Farms in Mullica Hill, yellow Desiree and Early Star and white Spring Snow and Manon peaches are being picked, said Tom Holtzhauser, owner.
“I started picking June 20th, and customers are lining up to get my peaches,” he says, noting that a year ago, his crop was decimated by frost. “My hours are 8 a.m.-6 p.m., and even at 6 p.m., I have customers waiting.”
Holtzhauser grows 30 peach varieties and sells mainly retail from his farm.
Ron Thomas of Sunny Slope Farm said his crop is good.
“We have a full crop and so far, no thunderstorms have knocked fruit off the trees,” Thomas said in the release.
Melick Town Farms in Oldwick/Califon reported similar results.
“Our peaches look wonderful this season, as compared to last year when we had a thin crop,” owner John Melick said in the release.’ We’ll have more than enough for the entire season, through Labor Day and beyond.”
Gary Mount of Terhune Orchards in Princeton said his harvesters were picking yellow and white varieties.
“We have a terrific crop” he said in the release. “Our early yellows, Flamin Fury, are some of the best.”