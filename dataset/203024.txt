Manhattan, Kan.-based AIB International is offering Good Manufacturing Practices inspections against the revised Consolidated Standards for Inspection.
 
In 2016, AIB issued updates for five of the company's Consolidated Standards for Inspection, putting in place new provisions to account for trends in the global food chain, according to a news release.
 
The revised standards that took effect Jan. 1, 2017 include:
Prerequisite and food safety programs;
Beverage facilities;
Food distribution centers;
Food contact packaging manufacturing facilities; and
Nonfood contact packaging manufacturing facilities,
Stephanie Lopez, vice president of food safety services of the Americas for AIB International, said in the release that the updates will help food operators to minimize risk of food safety issues.