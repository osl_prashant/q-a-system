Rain hampers search for Indonesian landslide victims, 7 dead
Rain hampers search for Indonesian landslide victims, 7 dead

By AGUS BASUKIAssociated Press
The Associated Press

BREBES, Indonesia




BREBES, Indonesia (AP) — Heavy rains hampered the search Friday for victims of a landslide on the Indonesian island of Java as authorities raised the death toll to seven.
The chief of the disaster mitigation agency in Brebes, Eko Andalas, said a body was found Friday and one of the people injured in the disaster had died in a hospital.
He said the number of missing is 14, down from 18, due to a duplicated name and two people incorrectly reported as victims.
The search involving more than 500 rescuers including police, volunteers and soldiers was halted due to heavy rains.
Farmers were working in their rice paddies in Central Java's Brebes district Thursday morning when the soggy hillside above them collapsed under the weight of torrential rains.
Survivors described a sudden roar as the landslide was unleashed, sweeping trees and everything else in its path toward the terraced rice fields below.
Seasonal rains cause widespread flooding and landslides across much of Indonesia, an archipelago of more than 17,000 islands. Millions of people live in mountainous regions and on flood plains.
Degradation of land by conversion of it from natural forest to pulp wood and palm oil plantations can also be a factor in landslides.
But at a news conference in Jakarta, National Disaster Mitigation Agency spokesman Sutopo Purwo Nugroho said the landslide in Brebes was purely a natural disaster and not due to the hillside being part of a planation forest.
"It was caused by land movement following continuing torrential rain in the past two weeks," he said.
The steep slopes meant there was high potential for ground movement in wet conditions, Nugroho said.