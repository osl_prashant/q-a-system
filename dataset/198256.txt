A fire in southeasternKansas engulfed an estimated 40,000 acres on Sunday afternoon. The fire burned a swath of land 1.5 miles wide and 20 miles long in just a few hours.The fire started in the northeast corner of Cowley Co. Winds averaged 30 mph and pushed the fire north across Butler Co. and Greenwood Co. before stopping at U.S. Highway 400. Fire crews set backfires in pastures along the highway to help stop the blaze from jumping the heavily traveled highway. Traffic was diverted for several hours due to smoke and the effort to stop the fire.
The town of Beaumont came closest to danger as the flames reached heights of 20-30 ft.
Twenty-one local fire crews battled the fire for an estimated 12 hours and lost one truck to the blaze. There have been no reports of buildings or homes being destroyed by the fire, but at least one rancher lost a cow to the flames.
Fortunately the majority of grass burnt was not stocked with cattle at the time of the fire. Typically, this region of the Flint Hills is grazed during the summer by stocker cattle.
The cause of the fire has not been determined, but authorities believe it could have been sparked by a passing train.
Smoke from the fire was large enough to show up on local weather stations' radar in Wichita.

Pastures have been ablaze the past few weeks in Kansas, but very few of the fires have been controlled burns. Two other wildfires occurred near Beaumont the past few weeks and have burned thousands of additional acres.
At the end of February, arsonists were blamed for 14 fires started in Lyon Co. near Interstate 35.
Another large fire occurred Sunday west of Topeka after a brush pile burn got out of control. Interstate 70 had to be shut down while 15 fire crews and 100 firefighters put out the blaze.
The author's family ranch is located less than 10 miles from the Beaumont and his family members helped moved cattle to safety.