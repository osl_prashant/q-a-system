San Antonio, Texas-based NatureSweet Tomatoes is reprising its Homegrown Tomato Challenge this summer at a new Raley’s store in Rancho Cordova, Calif. 
 
The Aug. 26 event, which is free and open to the public, allows home gardeners to enter for the chance to win one of three $2,000 grand prizes for best tomato in small, medium or large categories. Six runners-up will win a $200 Raley’s gift card, according to a news release.
 
All entries will first be tested for brix levels, and finalist tomatoes will be tasted by judges including former Sacramento King Bobby Jackson; Produce Man Michael Marks, with the California Department of Public Health’ Raley’s produce merchant Michael Schutt; and the Sacramento Master Gardeners.
 
The event also features a cherry tomato cooking contest in which three finalists from an online entry contest will be selected to compete in a secret ingredient cook-off. The grand prize-winner will receive $2,000, and two runners-up will receive $200 Raley’s gift cards, according to the release.
 
“We’re really looking forward to reviving the Homegrown Tomato Challenge in Sacramento this year,” said Kathryn Ault, vice president of sales for NatureSweet.