Double Diamond Farms has a new 10-acre organic cucumber facility in Kingsville, Ontario.
Shipments from that location will begin in February, and the company plans to also grow and distribute organic tomatoes and bell peppers in the future.
“The demand for organic produce has increased exponentially, and we are proud to be able to respond to our customers and provide a forward-thinking and local solution,” CEO and president Chris Mastronardi said in the release. “Consumers are looking for fresh organic options and convenience. We are confident that our new range will meet these needs and set the stage for future organic growth.”
The company looks at organic production as important in the context of its goal to use practices friendly to the environment.
“Sustainable agriculture is very important to us at Double Diamond Farms,” Mastronardi said in the release. “We are always looking for new ways to preserve the environment; growing our own organic produce is the next step towards reducing our ecological footprint.”

Want to know more about organic produce? Register for The Packer’s inaugural Global Organic Produce Expo, Jan. 25-27, in Hollywood, Fla.