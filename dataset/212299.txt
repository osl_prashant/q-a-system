In their zeal to not lose online grocery sales to Amazon, U.S. retailers are offering curbside pickup and online ordering to their customers. 
 But those same retailers also are fighting a battle against the hard discounter Aldi and soon, with the just-arriving companion European discounter Lidl.
 
Aldi has just announced growth plans to expand to expand to 2,500 stores nationwide by the end of 2022.  With this growth, ALDI will be the third largest grocery store by count in the U.S. and serve 100 million customers per month, according to the  company.
 
Today, Aldi operates 1,600 U.S. stores in 35 states, serving about 40 million customers each month.
 
With Baby Boomers scrimping and young adults looking to save coin on their grocery bill, Aldi represents a serious competitive challenge to the U.S. grocery industry in the years ahead. Now just imagine if Aldi offered online shopping and home delivery.....
 
--
 
Mexico is still - by far - the biggest international supplier of tomatoes to the U.S. market, but Canadian marketers made up a little ground in the past year.
 
Tomato imports from Mexico alone account for 22% of total U.S. fresh vegetable imports, but that line item showed a big decline in the past year.
 
The U.S. Department of Agriculture said U.S. fresh tomato imports from Mexico were $1.72 billion in 12-month period from May 2016 through April 2017, a decline of 9% compared with the previous year.
 
Fresh tomato import volume from Mexico dipped 1% for the May through April period.
 
On the other hand, U.S. imports of Canadian fresh tomatoes were $276 million in the 12-month period ending in April. That is up 9% in value and 13% in volume, according to the USDA.
 
Imports of peppers from Mexico, at $918 million for the May through April period, were down 10% in value but up 13% in volume compared with the year before. Imports of Canadian peppers, at $266 million, were down 3% in value but up 16% in volume.
 
--
 
Rep. Jim Costa, D-Calif., spoke on the importance of agriculture and the fruit and vegetable industry in this floor speech in Congress. 
 
From his remarks:
 
But the economic contributions of California agriculture--American agriculture--do not end at our borders. In California, we produce half of the Nation’s fruits and vegetables, and we are the top milk producing State in the Nation. The men and women who own and work on these farms provide both nutritious food for our families and create thousands of jobs across the country.
 We know we must make sure that our farmers have the tools to do so, but our farmers need a reliable source of water, a legal and stable workforce, and access to export markets, in other words, fair trade agreements.
 
 
Amen and amen.