“Dairy Replacement Heifers: Transitioning from Weaning through First Lactation” will be the focus of the 32nd Discover ConferenceTM on Food Animal Agriculture, hosted by the American Dairy Science Association (ADSA). The event is scheduled for May 30 to June 1 at the Eaglewood Resort & Spa, Itasca, Ill.ADSA Discover Conferences are designed to expose participants to cutting-edge science, and encourage in-depth discussion among all in attendance. Speakers who will address topics focusing on the major stages of dairy heifer development include:
Jim Drackley, University of Illinois – “Biology of growth”
Kristy Daniels, Virginia Tech – “Weaned heifer nutrition as it relates to growth and future milk production”
Jaymelynn Farney, Kansas State University – “Stress biology and the growing heifer”
Steve Nickerson, University of Georgia – “Heifer mastitis prevention and control”
Mike Overton, Elanco Animal Health – “Impact of diseases on future production”
Trevor DeVries, University of Guelph – “Impacts of feeding and housing management on heifer behavior and wellbeing”
Dan Weigel, Zoetis – “Genomics to manage the herd”
Brad Nosbush, Nosbush Dairy – “Genomics plus phenotype to manage the herd”
Darin Mann, M&M Feedlot – “Custom heifer rearing”
Mike Van Amburgh, Cornell University, and Greg Bethard, G&R Dairy Consulting, Inc. – “Herd-level dynamics and management impact on production and profit”
ARPAS continuing education credits are available to attendees. Additional information and conference and housing registration details can be found here.