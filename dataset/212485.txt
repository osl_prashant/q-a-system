Produce industry groups said what they had to say when the proposed 2018 U.S. Department of Agriculture budget showed the elimination of funding for the Market Access Program and the Specialty Crop Block Grant Program. But we also know from this president that it’s merely the opening of the negotiation.
The produce industry can make strong cases for continuing to receive taxpayer dollars to support both programs.
MAP has shown to be successful and business-building over the years. The groups that receive the funding also provide their own funds, showing that their investment is right along side those of taxpayers.
President Trump has also said he’s troubled by the country’s trade policies, promising to bring more jobs to the U.S. and strengthen exports.
MAP does exactly this.
We expect produce leaders to make a persuasive argument.
The Specialty Crop Block Grant program, delivering $85 million per year in the 2014 farm bill, has the goal of making the industry more competitive.
The program provides money, through state departments of agriculture, for research and outreach on food safety, pest and disease issues and other industry priorities.
With big changes coming to growers on food safety rules, the industry can make the case that this is no time to pull away from the grant program.
The produce industry can get more strategic when it comes to proposed changes to the Supplemental Nutrition Assistance Program.
Of course this will get caught up on politics, but everyone recommends fresh fruits and vegetables be a strong part of any man, woman or child’s diet.
If there need to be cuts, they should be in places other than fruits and vegetables. Maybe produce could be a negotiating tool.
Cut elsewhere and raise produce. Consumers and politicians could both win.
Nutrition is important, but some calories are better than others, and fruit and vegetable marketers shouldn’t be too shy to say so when the budget is on the line.
Did The Packer get it right? Leave a comment and tell us your opinion.