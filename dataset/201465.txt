Cool temps and scattered rains across the Corn Belt as the calendar flipped to August and concerns China could retaliate against U.S. soybeans if a trade battle breaks out weighed heavily on soybeans. November soybean futures plunged through support around $9.85, which caused us to advise trimming downside risk on a greater portion of expected new-crop production. Corn futures were pulled lower by soybeans and the non-threatening weather, though the December contract held above key support at the June low. Wheat futures extended their pullback from the early July highs despite falling spring wheat crop ratings.
Pro Farmer Editor Brian Grete provides weekly highlights:



Live cattle futures firmed on technical-based buying and better-than-expected cash cattle prices. Lean hog futures worked higher as traders narrowed the big discount contracts hold to the cash hog index.
Notice: The PDF below is for the exclusive use of Farm Journal Pro users. No portion may be reproduced or shared.
Click here for this week's newsletter.