Grass-fed beef is not as environmentally-friendly as some proponents claim. That’s the conclusion of a new study from U.K.-based Food Climate Research Network (FCRN).
 
The study, titled “Grazed and Confused?” suggests the net contributions of grass- and grain-finished animals’ to GHG emissions shows that both are contributors to the problem. The grass-only diet does not offset their own emissions, or that of other animals, says Tara Garnett, an FCRN researcher. 
 
Writing in a blog for a UK-based news analysis site The Conversation, Garnett says, “We asked one question: what is the net climate impact of grass-fed ruminants, taking into account all greenhouse gas emissions and removals?
 
“We found that well-managed grazing in some contexts … can cause some carbon to be sequestered in soils. But, the maximum global potential (using generous assumptions) would offset only 20 percent to 60 percent of emissions from grazing cattle, 4 percent to 11 percent of total livestock emissions, and 0.6 percent to 1.6 percent of total annual greenhouse gas emissions,” Garnett wrote. “In other words, grazing livestock – even in a best-case scenario – are net contributors to the climate problem, as are all livestock.” 
 
The researchers acknowledge the growing demand for protein worldwide, but for grass-fed animals to meet that demand we “would have to massively expand grazing land into forest and intensify existing grassland through the use of nutrient inputs, which among other things, would cause devastating CO2 releases and increases in methane and nitrous oxide emissions." 
 
If projections for animal product consumption remain unchanged, livestock would equal one-third of the total emissions budget adopted under the Paris climate agreement.
 
"Increasing grass-fed ruminant numbers is, therefore, a self-defeating climate strategy," the report concluded. 
 
Garnett’s blog about the report can be read here:
https://theconversation.com/why-eating-grass-fed-beef-isnt-going-to-help-fight-climate-change-84237