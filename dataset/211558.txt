San Diego County is hosting two citrus meetings this week focusing on the Asian citrus psyllid and huanglongbing, also known as citrus greening.The first will be 3-5 p.m. Sept. 20 at the Farm Bureau Office in Escondido, Calif., and the second will be 1-3 p.m. Sept. 21 at the Farm and Home Advisors Office in San Diego, according to a news release.
The free events are open to growers, packers, transporters, gleaners and residential citrus growers.
Topics covered include:
 Living with a Pest Invasion: Asian Citrus Psyllid and HLB, presented by Sonia Rios, farm advisor for UCCE Riverside/San Diego Counties;
 Update on Area Wide Management and the San Diego Citrus Pest Control District, presented by Enrico Ferro, grower liaison; and
 Citrus regulations and HLB quarantine preparation, presented by Keith Okasaki, environmental scientist for the California Department of Food and Agriculture.
Following the presentations will be a questions and discussion session, according to the release.
To reserve a seat call (858) 614-7734 or (760) 752-4700.