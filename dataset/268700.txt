CPMA is targeting food safety by educating members on new regulatory changes and how to meet market demands.
“Supported by our new food safety specialist, Jeff Hall, our food safety forums were well attended in Vancouver, Calgary, Toronto, Leamington and Montreal,” said CPMA’s outgoing chairman Rick Alcocer.
Another forum is scheduled for the Halifax, Nova Scotia, area after the association’s April 24-26 convention.
Alcocer said the forums educate the industry on the Safe Food for Canadians regulations, which come into effect this year, and on the Food Safety Modernization Act, which continues to roll out.
“While the two sets of regulations are aligned, there are still differences the industry must recognize or their products could be held back from market,” Alcocer said.
“Members should connect with Jeff at CPMA if they have any questions.”
To support food safety research in Canada, CPMA has committed $350,000 to date for a new CPMA Canadian Food Safety Fund.
Staff and volunteers have been framing the model over the past year, and Alcocer said the association has begun fundraising to create a sustainable program for ongoing research.
“As government regulates our sector more and more,” he said, “it’s vital that we have a science-based approach to dealing with food safety. The fund will support this approach.”