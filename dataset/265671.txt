Though its work is far from done, funding for the Produce Safety Alliance is scheduled to run out by September 2019.
The ticking clock has industry leaders worried about the future of grower produce safety training.
Given the task of helping to train and educate produce growers on food safety regulations, the alliance is a collaborative project among Cornell University, the U.S. Department of Agriculture and the Food and Drug Administration.
“We feel like we can make do with funding we have through the end of September 2019, but after that we don’t have enough funding to sustain us,” said Betsy Bihn, director of the Geneva, N.Y.-based alliance.
The current cooperative agreement runs from 2014-19, with a funding infusion of $3.5 million in 2014.
The group has stretched that amount to last until September 2019 by curtailing some of the initial objectives of the group, including redirecting funds set aside for outreach to the media.
Part of the group’s travel expenses have been picked up by states hosting training events that the Produce Safety Alliance staff participated in, Bihn said.
The lack of certainty over future funding of the alliance is troubling, said Jennifer McEntire, vice president of food safety and technology for the United Fresh Produce Association.
“What concerns me the most is that within the produce safety rule, there is a clear firm requirement for training to the PSA curriculum,” she said.
“It kind of requires that the PSA exists, and I think if there are questions around how PSA will be financially sustained, I think those questions need to be answered sooner rather than later because we are just now getting into compliance with the produce safety rule.”
Growers will need to continue to be trained, McEntire said, so the industry needs to be make sure that the PSA will be around.
Bob Ehart, a senior policy and science adviser at the National Association of State Departments of Agriculture, said that group also is concerned about the future of the alliance after 2019.
If the alliance can’t find funding enough to continue, some growers may find it hard to understand what they must do to comply with the rule, Bihn said.
By September of 2019, smaller growers won’t yet be subject the produce safety rule. The largest produce farms were subject to the produce safety rule in January this year, with the next group of growers subject in January 2019 and the smallest group of growers covered in January 2020.
“Smaller or medium-sized growers are not the guys with a ton of resources to dedicate to this,” Bihn said. “They are the ones that need help.”
Even growers not subject to the produce safety rule need assistance, she said.
“We are worried about all growers, because there is real value in having domestic growers of fruits and vegetables to supply their communities, their states, their local retailers and their local restaurants,” she said. “That is valuable.”
 
Track record
Since September 2016, more than 14,000 growers have been trained with the Produce Safety Alliance courses, having hosted or co-hosted 385 domestic grower training events. Forty-seven states have hosted alliance training for growers and the goal is to reach 50 states by June.
The alliance also has worked to have a produce safety trainer in every state.
Four regional extension associates employed with the alliance devoted up to 80% of their time traveling and conducting produce safety training.
The alliance has also hosted 44 “train the trainer” produce safety classes in the U.S. and five international “train the trainer” courses.
The group has developed curriculum and other produce safety resources, including developing the PSA website.
Bihn said in mid-March there is discussion of some additional funding from FDA, but nothing has been finalized and the amount that has been talked about isn’t enough to sustain the group.
The FDA is operating on a continuing budget resolution and cannot commit to additional funds.
Supporters are looking at the possibility of finding funding from the upcoming farm bill.
“I’m very concerned because I think we can show a track record of efficient and effective use of the funds we have been given to do the tasks we have been given,” Bihn said.
“I would say we have done more than the tasks we were tasked with doing,” she said.
The alliance has been very responsive to requests from growers, requests from trainers and requests from FDA to add to the information and service the PSA has delivered.
The group has organized three soil summits and one water summit related to produce safety, all of which were not initially part of the group’s objectives in its cooperative agreement.
“When it became evident that there needed to be conversations, we stepped up to the plate to organize the events,” she said.
Bihn said she believes the group may need about $1.7 million per year to sustain itself, provide some funds for travel and cover overhead expenses.
The option to raise fees for the group’s produce safety training courses runs counter to its mandate, she said.
“One of the concerns that has been expressed by the industry is making the training program affordable,” Bihn said.
The FDA, she said, wanted PSA training to be as affordable as possible, and that has been the approach the alliance has taken.
The alliance has also devoted staff resources to deliver educational materials, such as a database of antimicrobial pesticides used in water and their active ingredients.
“I guess we could charge for all of that, but that would be a really difficult way to fund us moving forward, to try to monetize all that.”
 
Staffing concern
Bihn said that the experienced produce safety staff at the Produce Safety Alliance — now numbering about seven full-time positions in addition to Bihn — could be compelled to look for other job opportunities if funding is not secured by early next year.
“It’s a Catch-22 that we are concerned about funding, because there are really good people on the team who would make really good people on other teams,” she said.
“I would hate to disband and then somebody come to me and say, ‘Oh, we have the funding,’ because by the time you disband and the team finds other jobs, the deal is done at that point,” Bihn said.
McEntire of United Fresh said that whether the alliance is funded with the farm bill or through the FDA makes no difference.
“From my perspective, I don’t care,” she said. “I just want PSA to exist.”