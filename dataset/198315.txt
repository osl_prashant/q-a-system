Editor's note: The following article appeared in the January 2016 issue of Dairy Herd Management.Mastitis. In the quest to become more efficient and productive, it has always been a hindrance to U.S. dairy producers.
Estimates vary, but Steve Nickerson, University of Georgia professor of Animal and Dairy Science, recently published data showing $2 billion is spent on mastitis as a result of treatment costs, discarded milk for drug withholding, lost milk production, veterinary services, lost milk premiums and reduced cull values.

Steve Nickerson, University of Georgia professor of Animal and Dairy Science
Dairies are under continuous pressure to provide a quality product, and most have successfully complied with the federal bulk tank somatic cell count (SCC) standard of 750,000 cells per milliliter (cell/mL), instituted in 1986. In keeping with global marketing standards, many industry experts predict the limit could be lowered again in the near future, to 400,000 cell/mL.
At the same time, producers face the ongoing challenge of producing milk profitably, in an environment of ever-volatile input and milk prices. Profitability often is achieved through higher per-cow production.
Now, managing mastitis amidst these challenges has a new wrinkle. Mounting concern about antibiotic resistance in human medicine is causing antibiotic mastitis therapy to be looked at more critically. Increasingly, more aggressive preventative strategies are being sought to prevent mastitis infections from happening at all.

We've come a long way
"The focus of mastitis research has definitely shifted," said veteran mastitis researcher Lorraine Sordillo, Professor and Meadow Brook Chair of Farm Animal Health at Michigan State University's College of Veterinary Medicine. "When I began researching mastitis 30 years ago, we concentrated heavily on epidemiology and microbiology. Now we are placing much greater emphasis on immunology and enhancing the cow's natural defenses to minimize mastitis infections."

Lorraine Sordillo, Professor and Meadow Brook Chair of Farm Animal Health at Michigan State University's College of Veterinary Medicine

Sordillo said she was impressed with the tremendous progress U.S. dairy producers have made in managing mastitis in their herds. She and Nickerson attribute much of that success to adherence to the "5-Point Plan for Mastitis Management," issued decades ago by the National Mastitis Council.
The hallmarks of the 5-Point Plan are (1) teat disinfection; (2) dry-cow antibiotic therapy; (3) use of functionally adequate milking machines; (4) antibiotic therapy for clinical mastitis infections, and (5) culling of chronic cows. Nickerson added mastitis prevalence and SCC levels have been reduced via herd surveillance and recordkeeping, environmental sanitation, strategic culling, vaccination, teat sealants, herd biosecurity, dietary supplementation and mastitis control in bred heifers.

Immunity is where it's at
Today's mastitis research focuses heavily on enhancing cows' immune systems to help them ward off and mitigate invasive pathogens. Commercially available mastitis vaccines all are bacterins, meaning they help the cow's immune system recognize the core structure of the target bacteria, and generally are more effective at helping cows resolve new infections rather than preventing them.
At least 137 organisms cause mastitis (Watts 1988), so it is not likely vaccines can be developed for all of them. But Sordillo is excited about the prospects for mastitis vaccine technology nonetheless. "The mammary gland is unique in that you can vaccinate it separately, targeting unique cell populations to trigger an immune response," she said. Sub-unit vaccines, which target specific peptides that contribute to disease progression, are the focus of current research. Sordillo also said a need exists for fresh thinking in development of the adjuvants that serve as the carrier for vaccine delivery.
Enhancing immunity through nutrition is Sordillo's primary area of research today. "Optimal immunity is derived from optimal nutrition," she said. "Particularly in times of stress and negative energy balance, the cow's immune system is starved for the micronutrients it needs to fully function." She said one negative health event - whether mastitis, metritis or another condition - often leads to more health issues, plus suppresses fertility and/or milk production. "If the immune system is busy healing the uterus, for example, it lets down its guard in other areas, like the mammary gland."
Sordillo's research has shown dietary supplements with trace minerals and vitamins can have immune-modulatory effects on the mammary system, and Nickerson thinks the use of such nutritional supplements, many of which also contain yeast, will expand. "We believe supplemental yeast acts as a probiotic, supporting rumen microflora and digestion, particularly in early lactation," he said.
Using genomics to breed for disease resistance also holds tremendous promise, but Sordillo cautioned such progress can't happen overnight. "The trick will be to determine the optimal immune phenotype for which to select," she said. "It is a complicated assignment, and in the process of selecting for mastitis immunity, we run the risk of negatively impacting other immune channels."
Finally, Sordillo was intrigued by the potential of discovering ways to accelerate involution at dry-off and optimizing host defenses, which could help see cows through a period in which they are highly prone to new infections.

The future of antibiotics
While the researchers agree antibiotic therapy always will be part of the mastitis offense, its role will change. "Through regulation and our own proactive efforts, I think we will be seeing increased veterinary involvement, and more emphasis on susceptibility testing in the future," Sordillo said. "Prophylactic antibiotic use, such as whole-herd dry-cow therapy, probably will not continue as we know it today."
And while change is always difficult, Nickerson sees many positives resulting from less antibiotic use beyond addressing concerns about resistance. "If we can reduce new mastitis infections, and successfully equip the cow to use her own defenses to manage those that do occur, it's a victory for animal welfare, drug residue risk, milk quality, production and profitability and consumer confidence," he said. "It is in our best interest as an industry to keep working in that direction."


Changes in milking and milk quality
In the past 18 years, the average bulk-tank somatic cell count on U.S. dairies has dropped from 300,000 cells/mL to 200,000 cells/mL.
That was one of many findings in the National Animal Health Monitoring Systems (NAHMS) Dairy 2014 study, a USDA-funded project that has included major studies of U.S. dairy operations in 1996, 2002, 2007 and, most recently, 2014. Other results from the 2014 study include:
‚Ä¢ 86% of U.S. dairy cows are milked in parlors, compared to 54.9% in the original 1996 study.
‚Ä¢ 3X milking has nearly doubled. In 1996, just 5.8% of U.S. dairy herds milked three times a day. In 2014, 10.2% of herds did so.
‚Ä¢ Use of gloves during milking has nearly doubled since it was first tracked in 2002. At that time, 48.7% of cows were milked with handlers using gloves. In 2014, 87.9% of cows were milked by gloved workers.
‚Ä¢ Forestripping before milking has increased from 44.5% of operations in 2002, to 71.5% in 2014.

Immune-stimulating additives explored
One of the new frontiers in mastitis prevention is the development of feed additives to support the immune system.
An animal's immune system is its body's tool to discern between foreign substances and the body's own molecules. When it recognizes invading organisms, the immune system mounts a response to eliminate the foreign organism. However, even if the immune system is successful in doing this, unfortunate side effects can be inflammation and damage to surrounding tissue.
Considerable research has been dedicated to the development of micronutrient supplementation that promote a more effective and sustained immunity to infectious agents without the risk of toxicity or tissue damage (Sordillo and Streicher 2002).
One such commercially available additive was evaluated by researchers at the University of Georgia. The dietary supplement containing B-complex vitamins and yeast extract was fed daily to 40 prefresh heifers from five months of age until calving. Their health and milk production then were compared to 40 untreated control heifers. The researchers' findings:
‚Ä¢ From 5 to 20 months of age, supplemented heifers had higher systemic levels of the molecule L-selectin, which is a measure of the ability of white blood cells to be mobilized from the blood stream and attack invasive organisms.
‚Ä¢ White blood cells collected from heifers in the treatment group were more active in engulfing two important mastitis-causing bacteria, E. coli and Staph. aureus, after just 30 days of feeding the supplement.
‚Ä¢ At Day 3 of lactation, mastitis incidence for the supplemented group was 11%, compared to 20% for the untreated controls.
‚Ä¢ Somatic cell count (SCC) at three days post-freshening was 221,000 cells/mL for the treated group, versus 535,000 cells/mL for the untreated controls.
‚Ä¢ Milk production at freshening was not significantly different between the two groups, but the supplemented heifers showed a growing production advantage as lactation progressed. By five weeks in milk, the treated group produced 7.0 pounds per day more than the untreated controls.
Researchers concluded dietary supplementation with immune-supporting additives shows promise in preventing mastitis infections and promoting udder health and milk production. They predicted as more research and product options become available, immune-supporting additives may become a standard recommendation in dairy nutrition.
Source: Journal of Animal Science Vol. 90, Suppl. 3/
Journal of Dairy Science. Vol. 95, Suppl. 2, Abstract 220

California tightens the reins on livestock antibiotics
Legislation recently passed in California (SB 27) places the strictest government standards in the country on antibiotic use in livestock production. Starting Jan. 1, 2018, the new regulations will:
‚Ä¢ Ban the use of medically important antibiotics (MIAs) for growth promotion in livestock.
‚Ä¢ Eliminate over-the-counter access to MIAs for food-animal health.
‚Ä¢ Require prescription by a licensed veterinarian or veterinary feed directive (VFD) for all MIAs used in livestock.
‚Ä¢ Restrict the routine use of MIAs for disease prevention. The bill's language states MIAs cannot be used in a "regular pattern." The bill does not specifically define the characteristics of a "regular pattern," and it is unclear where dry-cow therapy fits into the definition.
‚Ä¢ Require the California Department of Food and Agriculture (CDFA) to develop a monitoring program to gather information on antibiotic sales and use in livestock production.
SB 27 is more restrictive than the latest federal standards issued by the U.S Food and Drug Administration (FDA). The FDA's voluntary plan (GFI 213) goes into effect Jan. 1, 2017, and addresses antibiotic use for growth promotion, but not disease prevention. FDA guidelines also do not include an evaluation component.
Sources: Cornerstone Capital Group, the Pew Charitable Trusts


National Mastitis Council 55th Annual Meeting
The National Mastitis Council (NMC) continues to promote research and education in the quest to globally reduce mastitis incidence and enhance milk quality. The 55th Annual Meeting of NMC will be held Jan. 31 to Feb. 2 in Glendale, Ariz. The schedule includes:
‚Ä¢ Farm tours - A preconference day of touring on Jan. 30 will include visits to four dairy farms using various techniques and styles of equipment to successfully manage milk quality and udder health.
‚Ä¢ Short courses - Ten limited-enrollment short courses will be held throughout the conference. Topics include milk bacteriology, dairy advocacy, implementation of on-farm protocols and milking equipment sanitation - among others.
Open discussion groups - Open dialogue will be facilitated on milking equipment, residue avoidance and international dairy production. Attendees may suggest additional topics in advance.
‚Ä¢ Technology transfer session - Scientific posters on mastitis and milk-quality research topics will be on display, with authors/researchers present for discussion.
‚Ä¢ Featured symposium - "Implementing, sustaining and communicating change: Putting theory into practice" is the theme for a symposium scheduled for Feb. 2.
Additional details and registration information can be found at http://meeting.nmconline.org.

The role of genetic selection in improving mastitis resistance
More than 6,000 genes have been identified that regulate the cattle immune system. When making breeding decisions, it is much more challenging to de-select for a complex disease like mastitis, compared to a disease caused by a single, easily identified, recessive gene.
 That's not to say, though, that progress has not been made in improving cattle immunity and selecting for greater udder health via genetics. U.S. cattle breeders have successfully selected for physical traits - like udder conformation and teat placement - that help reduce exposure to mastitis pathogens.
Choosing sires with low Somatic Cell Scores (SCS) also has helped create some progress in udder health, although the heritability of mastitis resistance using this method is relatively low, estimated at 0.2 to 10% (Koeck et al. 2012, Parker Gaddis et al. 2012).
In addition, the following, emerging genetic tools are being developed and/or used in the U.S.:
1) High Immune Response (HIR) technology. The host immune response is a phenotype determined by the interaction of the immune response genotype with the environment. HIR technology has been used to identify the ability of cows, calves and sires to mount immune responses to disease challenges. HIR genes are more heritable than selecting for SCS, estimated at 25% to 35% (Thompson-Crispi et al. 2012). High immune-responding cows have been found to have an increased response to commercial E. coli J5 vaccination, and higher antibody levels in their colostrum. (Wagter et al. 2000, Fleming 2014).
2) Genomic technology. Genomics rely on breeding decisions based on genomic estimated breeding values (GEBV), which are calculated using the joint effects of single nucleotide polymorphism (SNP) markers across the entire genome. Breeding based on genomics has significantly increased the rate of genetic progress in U.S. dairy cattle. As additional genomic data is gathered, more strategic breeding decisions can be made. Recently, enough data has been generated to validate immune responsiveness in some dairy sires. The next step will be to increase the accuracy of selection for mastitis and immune response. This will require creating a reference population large enough to accurately identify both genotypes and phenotypes that are predictive of immune response activity against mastitis organisms.
Genetic management of mastitis resistance likely will continue to evolve. Using selection indices based on data from a broad base of genes appears to be the most effective approach to breeding for mastitis immunity.
Source: Frontiers in Immunology 2014; 5:493

Mastitis on U.S. dairy herds
According to data and a cost estimation model from the National Animal Health Monitoring System (NAHMS) Dairy 2014 study*:
¬? Operations with at least one case of mastitis: 100%
¬? Total cows affected by mastitis: 24.1%
¬? Average cost of treating a single case of mastitis: $42.05**
¬? Outcomes of mastitis cases: 
- 72.9% recovered
- 24.0% sold
- 3.1% died
*Data from U.S. dairy herds for the year 2013

**Included cost of fatalities; early culling, milk-yield loss; and costs for treatment, labor and replacement animals.