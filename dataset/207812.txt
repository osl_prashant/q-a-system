Glennville, Ga.-based Bland Farms more than fits the bill as an example of a vertically integrated operation that is involved with importing produce from Mexico, growing vegetables in the U.S. and selling produce into Canada. 
With renegotiation of the North American Free Trade Agreement about to get underway, Bland Farm leaders recently had the chance to show leading North American ag officials about the importance of trade between the three countries.
 
Secretary of Agriculture Sonny Perdue, Canadian Minister of Agriculture and Agri-Food Lawrence MacAulay and Mexican Secretary of Agriculture Jose Calzada visited Bland Farms June 20.
 
“I think Secretary (Perdue) was looking for an integrated operation that involves Canada and Mexico on a daily basis,” said Troy Bland, chief operating officer for Bland Farms. “We have a very strong import program from Mexico as well as a cross-dock facility in south Texas and we also sell quite a bit of onions and some sweet potatoes to Canada.”
 
He said the visit helped ag leaders see how the three countries are important to the Georgia marketer.
 
“It was one of the most significant moments in the history of Bland Farms and a day I am proud to say I took part in,” he said in a news release.
 
Perdue tweeted on June 20 that Bland Farms produces about 30% of all Vidalia onions. Like other sweet onion growers, Bland Farms has a year-round program and sources from other countries when Vidalias are not in season. The company also exports to Mexico.
 
The Trump administration appears to be listening to farmers and ag groups calling for continued duty-free trade between the three countries.
 
The U.S. Trade Representative recently released U.S. objectives for NAFTA negotiations, and the top bullet point on the list was to “ maintain existing reciprocal duty-free market access for agricultural goods.”