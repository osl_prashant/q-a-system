University of Aarhus researchers say results from a recently conducted feeding trial for newly weaned piglets have some surprisingly positive results, due mostly to the nature of the feedstuffs being tested.
In addition to fish meal and extruded soybean meal, the study looked at two formulations of starfish meal.

Yes, you read that correctly -- the scientists fed starfish meal, combined with extruded soybean meal, in two of four diets.

For this project, 96 pigs were housed individually beginning one week after weaning, and assigned to one of the four diets for a 14-day period.

What happened?

In short, a higher level of starfish meal had a somewhat negative effect on performance while the lower level starfish meal resulted in equal performance to pigs on the control diets.

Want to know more? See the research article on the ScienceDirect website here. For history on Aarhus researchers using starfish and other seafood in pig diets, see information on the University's website from 2013.