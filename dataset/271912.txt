Salmonella linked to Reno-area smoke shop's herbal product
Salmonella linked to Reno-area smoke shop's herbal product

The Associated Press

RENO, Nev.




RENO, Nev. (AP) — A Reno-area smoke shop has pulled a herbal supplement from its shelves after health officials linked a case of food poisoning to the ingredient kratom.
The health district confirmed Tuesday one Washoe County case of salmonella has been linked to a kratom product tied to more than 100 cases of food-borne illness this year in 38 states including Nevada.
The Center for Disease Control and Prevention says kratom is a plant native to Southeast Asia that is consumed for its stimulant effects and as an opioid substitute.
The U.S. Food and Drug Administration ordered a recall earlier this month of products that tested positive for salmonella at Triangle Pharmanaturals in Las Vegas.
Washoe County officials are urging residents to report locations that are selling kratom product under labels that include Thang, Kakuam, Thom, Ketom and Biak.