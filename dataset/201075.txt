Farm Diesel

Farm diesel is a penny lower on the week.
Our farm diesel/heating oil spread continues to indicate slightly lower near-term diesel price action.
Two states posted higher prices this week as 4 states were unchanged.
Distillate supplies firmed 0.3 million barrels last week and fell to 700,000 barrels below the year-ago peg.
This week's farm diesel price action follows the seasonal price path lower.

 
Propane

Propane softened 5 cents per gallon this week, and no state posted a higher LP price.
Four of the 12 states in our survey are unchanged this week.
Propane supplies firmed 2.357 million barrels on the week
As with farm diesel, this week's price action follows the seasonal price path --all be it at higher levels -- and confirms our expectations that the summer low will come in above the same time last year.






Fuel


6/5/17


6/12/17


Week-over Change


Current Week

Fuel



Farm Diesel


$1.97


$1.97


-1 cent


$1.96

Farm Diesel



LP


$1.24


$1.26


-5 cents


$1.21

LP