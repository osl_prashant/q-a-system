Buying grain to feed livestock can be expensive, but there are ways to protect your risk and reduce market volatility. This year feed buyers should be able to benefit from buying grain during a seasonal market low, says Bob Utterback, president and owner of Utterback Marketing.
Utterback believes the time to start buying grain is now during the height of harvest.
“I think we are in the optimum seasonal time period for a feed buyer to start accumulating aggressive inventories,” Utterback says.
He expects similar grain market conditions compared with 2016, barring a significant weather event in the global market. More risk will likely exist in the May, June and July futures markets.
Grain markets depend on supply and demand. The markets can do three things: go sideways, up or down.
“Your market bias is a function of your position in the market. As a feed buyer your biggest enemy is getting too bearish or for a grain seller getting too bullish,” Utterback says.
The odds are still one out of three for what the market will do going either up, down or sideways.
Seasonal markets happen at periods like harvest where supply out paces demand, typically occurring from August to November. Supply increases dropping the local cash price basis to futures. Historically this fall period is the time corn prices are at the low for the year.
This year the markets aren’t seeing as wide of fluctuations because supply looks like it shouldn’t go down and many corn farmers will try holding onto grain in the bin for a rally.
“We’re not trying to outguess the market, we’re trying to reduce variability,” Utterback says of buying grain early.
Locking in prices helps stabilize the cost structure. When doing this it allows producers to focus on production.
“The more you can stabilize your cost structure the better off you are during periods of tight margins,” Utterback says.