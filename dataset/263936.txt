Grains and livestock lower
Grains and livestock lower

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were lower Monday in early trading on the Chicago Board of Trade.
Wheat for March delivery declined 12.20 cents at $4.56 a bushel; March corn was off 6.60 cents at $3.77 bushel; March oats fell 4.60 cents at $2.3940 a bushel while March soybeans lost 14.20 cents at $10.3120 a bushel.
Beef and pork were lower on the Chicago Mercantile Exchange.
April live cattle fell .62 cent at $1.2073 a pound; March feeder cattle lost 1.02 cents at $1.3938 a pound; April lean hogs was off 1.23 cents at $.6435 a pound.