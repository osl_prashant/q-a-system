California tree fruit growers say they’ll be starting a little later this year than last year, returning to a more normal kickoff date.
The past couple of years, Kirschenman Enterprises Inc., Arvin, Calif., has started picking its spring/summer stone fruit program of nectarines, peaches and apricots “incredibly early,” said Wayde Kirschenman, owner and president.
This year, he predicted in mid-April that he would pick his first fruit around April 24.
Other than a bit of hail damage in March, growing conditions were conducive to a good crop.
“Quality looks pretty good this year,” he said, with “normal” size fruit.
Although excessive rainfall this year caused some damage to Kirschenman’s carrot and potato crops, fruit trees were not affected.
“For the most part, the rain was good because we’ve been in a drought situation,” he said.
Kingsburg, Calif.-based Valhalla Sales & Marketing Co., which focuses on its plum and apricot programs, will start picking in mid-May, five to seven days later than last year, which was “exceptionally early” because of temperatures in the 90-degree range in January followed by a warm spring, said owner David Stone.
Growers are happy with what they’ve seen so far this season, he said.
“It looks like a normal set,” Stone said. “We’re pretty optimistic. I think it’s a nice crop.”
The state has had a “tremendous amount of rain” this winter, he said, but the closer summer gets, the less the likelihood of bad weather.
“So far, we’ve dodged a bullet with our fruit, and we seem to be in pretty good shape,” he said.
After several years of drought, California growers should have ample supplies of water this year, Stone said. And the record rainfall should help with the sizing of the stone fruit.
Valhalla expects to kick off its plum deal between May 5 and May 10, which will be followed by apricots about five days later.
He said a March freeze in the Southeast could result in greater demand for California product this summer.
In April he said he already was getting inquiries about the coming crop.
“Everybody seems to be excited about the upcoming season,” he said. “We’re off to a good start.”
The Flavor Tree Fruit Co. LLC, Hanford, Calif., will have a “very good crop” of its popular Verry Cherry plum hybrid, said president Maurice Cameron.
With the fruit still green and only about three-quarters of an inch long, it was too early to predict the quality in mid-April, but he said, “We’ve got quite a bit of fruit out there.”
In fact, Flavor Tree was thinning out the crop to ensure a high brix rating.
“In order to guarantee the high brix, we have to remove a lot of the product so the . . . nutrients get shared with fewer pieces of fruit,” he said.
“We’re targeting a 21-brix product,” Cameron said.
The company also will have it high-brix green plum called the Emerald Beaut starting around mid-August.
Flavor Tree exports most of its Emerald Beauts, which have a brix level of 18 or 19, and ships some to upscale retailers.
The company also ships a handful of yellow peaches and yellow and white peaches and nectarines.
Bari Produce LLC, Madera, Calif., will start its apricot program in mid-May, said
Justin Bedwell, president.
“Everything is a little later this year,” he said.
Although the trees got plenty of rain, they did not have plenty of chill hours, he said.
“The trees did not get the dormancy period they needed,” Bedwell said.
But even with a start date later than last year, the crop should be back on its normal timing schedule, he said.
If no further rain turns up that affects the fruit quality, this year’s fruit should be good eating quality and good size, he said.
“Our fingers are crossed that it will be a good year.”