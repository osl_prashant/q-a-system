With industry leaders counting on equality with U.S. and Canadian produce safety rules, the government of Canada is taking public comments on food safety regulations.
 
Interested parties can review the Safe Food for Canadians Act regulations and comment on the Canadian Food Inspection Agency website through April 21, according to a news release.
 
Under the regulations, food businesses in Canada must put in place preventive controls to identify and manage food safety risks, according to the release. The rules were developed from the 2012 act.
 
Other parts of the regulations will allow the government of Canada to reduce the time it takes to remove unsafe foods from the marketplace.
 
Harmonization of the Canadian food safety regulations with U.S. rules has been a goal for the industry, said Jim Gorny, vice president for food safety and technology for the Newark, Del.-based Produce Marketing Association. 
 
"There has been a lot of talk in the administration about trade, between Canada and Mexico particularly with regards to the North American Free Trade Agreement," he said. "We will certainly be looking for the Safe For Canadians Act and what we are really hoping for is some harmonization with the Food Safety Modernization Act in the U.S."
 
Non-tariff barriers related to food safety regulations would be a drag on business, he said.
 
"The most important thing for our guys is to make sure there is one set of rules that are science-based and risk-based," he said. "That's really the key."