With spring planting already underway in the South, and Midwest farmers itching to roll their planters, Farm Journal Pulse wanted to see what level of insurance did farmers sign-up for to cover this year’s corn crop.
Nearly 900 farmers responded to the question: What level of insurance for your corn crop did you buy in 2018? 85% of those who responded to the unscientific survey say they will have some level of crop insurance. 15% said they opted out of coverage.  The highest percentage of respondents, 28%, is selecting 80% of crop protection.
 

 
To participate in the Farm Journal Pulse, text the world PULSE to 31313.