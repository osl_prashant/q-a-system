The December Dairy Cattle Reproduction Council (DCRC) webinar will address the use of genomics in commercial dairy farms. It has been re-scheduled to Dec. 8 and will begin at 1 p.m. Central Time. Francisco Peñagaricano, genetics and genomics assistant professor at the University of Florida, Gainesville, Fla., will present the webinar in Spanish.
During the “Use of Genomics in Commercial Dairy Farms” webinar, Peñagaricano will discuss:

Basic concepts of dairy cattle selection
Effective use of genomics for sire selection
Effective use of genomics for replacement heifer selection
Implications of using dairy cattle genomic data

To register for this webinar, go to: www.dcrcouncil.org/webinars  and follow the prompts. As the webinar approaches, you will receive an e-mail with information on how to log in to participate. If you are a DCRC member and cannot attend the live program, you may access the webinar at dcrcouncil.org.
This is the last of the 2017 webinar series of information-packed forums. These highly regarded educational programs offer attendees from across the United States and around the world access to high-quality information and interaction with industry experts from the comfort of their farm or office.
For more information, e-mail Pablo Pinedo, DCRC education committee chair, at Pablo.Pinedo@colostate.edu or e-mail DCRC at kristym@dcrcouncil.org.
The Dairy Cattle Reproduction Council is focused on bringing together all sectors of the dairy industry – producers, consultants, academia and allied industry professionals – for improved reproductive performance. DCRC provides an unprecedented opportunity for all groups to work together to take dairy cattle reproduction to the next level.