The arrival of fall means trees changing colors, football games, tailgates and apple cider. It also means farmers will be harvesting crops. While driving, you may encounter farm equipment—a combine or tractor pulling an implement. This equipment must operate on highways to get from farm to field or field to field. Farm equipment may be wider than one lane, or in some cases wider than the road, and travels at slow speeds, typically 10 to 15 mph.
If you approach a piece of wide farm equipment, slow down and be patient. The farmer understands your trip may be delayed, so they will pull off the road at the first available safe location to allow you to pass. Road shoulders may be narrow, steep, wet or soft, so they may not be able to pull over immediately.
Just as motorists are allowed to drive their vehicles on public roadways, farmers are legally allowed to operate farm equipment on these same roads. Caution, courtesy and patience are necessary to ensure the safety of motorists and operators of slow-moving farm equipment.
Motorists and farmers have an obligation to share the road safely.
Safety tips for motorists following or approaching farm equipment

Pass with caution if a farmer has pulled off the road to allow you to pass, or if they cannot pull off and you feel you must pass.
Be watchful of motor vehicles behind you that may also try to pass.
Do not pass if you must enter the oncoming traffic lane unless you can see clearly ahead of you and the vehicle you will pass.
Do not pass if there are curves or hills ahead that may block your view of the view of oncoming vehicles.
Do not pass in a designated “No Passing Zone” or within 100 feet of any intersection, railroad crossing or bridge.
Do not assume that a farm vehicle that pulls to the right side of the road is going to let you pass. Due to the size of some farm implements, the farmer must use wide left-hand turns. If you are unsure, check for turn signals or operator hand signals. Also, check the left side of the road for driveways, gates or any place a farm vehicle might turn into.
Do not assume the farmer can see you or knows you are there if you are following. Most operators are regularly checking traffic behind them and newer farm equipment is will equipped with mirrors. That said, the farmers must spend most of the time looking ahead to keep equipment safely on the road and watch for oncoming traffic.

Safety tips for farmers taking wide equipment on rural roads

Michigan law requires a slow moving vehicle (SMV) emblem and additional reflectors on any implement of husbandry, farm tractor and special mobile equipment. It is required on every vehicle that has a maximum speed potential of 25 mph operated on public highways. The Michigan Farmer’s Transportation Guidebook contains additional information on the requirement for operating farm vehicles on public roads.
Always keep the SMV emblem clean with the point of the triangle up. Replace emblem when it fades, normally every 2 to 3 years.
Turn on your lights, but turn off the rear spotlights when going onto the road. Rear spotlights can look like headlights.
Use pilot vehicles, one in front and one in back, if you are going a long distance or traveling on a heavily traveled road. Pilot vehicles should be well marked with brightly colored flags or appropriate lighting.
Install mirrors on equipment to enable you to be aware of motorists around you. Be careful where mirrors are placed so not to obstruct vision.

Caution, courtesy, patience and attention to the safety tips for motorist and farmers will help ensure the safety of motorists and the operators of slow-moving farm equipment on our rural roads during this fall harvest season.