Greener Fields Together, a sustainability initiative from foodservice giant Pro*Act, is launching a program to reduce waste across the supply chain.
The program was developed in partnership with consultants Measure to Improve LLC, according to a news release.
Ten Pro*Act suppliers have signed the Waste Reduction Pledge, focusing on reducing landfill waste.
In the program, participants work together to make verifiable sustainability improvements, with an ultimate goal of zero waste at their own companies and across the supply chain.
The initiative will reduce waste, save money, reduce harm to the environment and improve use of resources, according to the release.
Participating companies are:

Church Brothers Farms, Salinas, Calif.;
Coastal Fresh Farms Inc., Moore Park, Calif.;
HMC Farms, Kingsburg, Calif.;
Ippolito International, Salinas;
J.C. Watson Packing Co., Parma, Idaho;
Mann Packing Co. Inc., Salinas;
Mission Produce Inc., Oxnard, Calif.;
NewStar Fresh Foods, Mexicali, Mexico;
Sunkist Growers, Terra Bella, Calif.; and
Taylor Farms, Salinas.

“We are proud to work with our national farm partners that are leading the industry in incorporating sustainable practices into their operations, demonstrating a commitment to responsibly supplying their communities for generations to come,” Joe Cimino, vice president of procurement at Pro*Act, said the release.
The Environmental Protection Agency estimates that less than 35% of the 258 million tons of waste generated in the U.S. in 2014 was recycled or composted.
“At Ippolito, we believe sustainability is not a destination, but an investment in our continuous improvement. It becomes a culture as opposed to words on paper,” Dan Canales, vice president of sales, marketing, and processing at Ippolito International, said in the release.
Some of the benefits anticipated with the program include meeting buyers’ sustainability expectations, cost savings, supporting food safety programs, educating employees, and reducing environmental impacts, according to the release.