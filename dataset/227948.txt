As lawmakers put pen to paper on the new Farm Bill, some recent reports provide a snapshot of the country’s beleaguered farm economy.
The U.S. Department of Agriculture recently released projections that farm incomes will continue to fall in 2018 and will hit their lowest point in the past dozen years.
The estimated 6.7% decline in farm profits from last year is largely attributed to increased costs for oil, labor, and other farm inputs.
“Farmers and ranchers continue to operate on very thin margins,” a Nebraska Farm Bureau economist told the Omaha World Herald of the report.
And those thin margins extend well beyond just Nebraska.
A new economic report from the Federal Reserve tells of low prices nationwide, hurricanes, drought in the South and extreme cold in the Midwest – plus, an uptick in farmers seeking loan extensions because of an inability to pay.
Nathan Kauffman, an economist with the Federal Reserve Bank of Kansas City, spoke earlier this month to the crop insurance industry’s annual meeting.
“The U.S. farm economy remains in a prolonged downturn,” he said, “and commodity prices are likely to remain low in the near-term.”
Kauffman said the stress has made farm liquidity “a growing concern.”
Creighton University publishes a monthly report on the rural economy. This Mainstreet Index included some sobering news in January. The report identified rising farm loan defaults as the biggest challenge facing the rural economy in 2018. It also showed falling land prices and equipment sales amid the pressure caused by low commodity prices.
Creighton also measures rural bankers’ confidence in the economy for the next six months. Confidence dropped 5 points in January.
“Concerns about trade, especially current NAFTA negotiations, and low agriculture commodity prices continue to restrain bankers’ economic outlook,” explained Ernie Goss, who oversees the Mainstreet Index project.
“Approximately 71.2 percent of bankers projected that any interruption or abolition of NAFTA would have a negative impact on their area.”
“In summation: Things aren’t good right now in ag country – whether you farm on the East Coast, the West Coast, or in between,” said the authors of Farm Policy Facts in a recent release. “The farm policy critics who are actively lobbying Congress to leave farmers with fewer tools to weather the storm should be ashamed.”