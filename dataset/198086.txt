Traders don't expect Friday's Supply & Demand and Crop Production Reports to reflect any significant changes from last month, but given the large short position funds hold, it has given them some encouragement to cover short positions. According to pre-report expectations compiled by Reuters, traders look for USDA to trim old-crop corn carryover by around 8 million bu., trim old-crop soybean carryover by 2 million bu. and raise old-crop wheat carryover by around 2 million bushels. Meanwhile, traders expect USDA to trim new-crop corn carryover by around 25 million bu., to raise new-crop soybean carryover by 5 million bu. and trim new-crop wheat carryover by 3 million bu. from last month.




 


2016-17


2017-18






Commodity


Avg.


Range


USDA May


Avg.


Range


USDA May




in billion bushels




Corn


2.287


2.215-2.360


2.295


2.085


1.985-2.200


2.110




Soybeans


0.433


0.400-0.458


0.435


0.485


0.435-0.612


0.480




Wheat


1.161


1.144-1.200


1.159


0.911


0.860-0.995


0.914




Meanwhile, traders look for USDA to trim the size of the all winter wheat crop by around 7 million bu. from last month, with slight declines in the HRW and SRW crops offsetting a small increase in the white winter crop. Traders look for USDA to lower the size of the all wheat crop by 5 million bu. from last month to 1.815 billion bushels.




2017 Wheat Production


Avg.


Range


USDA
			May 




in billion bushels




All wheat


1.815


1.795-1.833


1.820




All winter


1.239


1.207-1.292


1.246




HRW


0.731


0.704-0.783


0.737




SRW


0.295


0.290-0.300


0.297




White winter


0.214


0.208-0.222


0.212




USDA will release its report on Friday at 11:00 a.m., CT.