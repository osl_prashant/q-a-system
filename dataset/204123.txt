AGCO Corporation announces it is bringing to market new White Planters 9800VE Series planters equipped with SpeedTube seed tubes from Precision Planting. This enables the planters to operate accurately at "nearly double traditional operating speeds," according to Larry Kuster, AGCO senior product specialist.
That in turn gives farmers a little more flexibility during the critical planting phase of the season, Kuster notes.
"To achieve the best yields in corn, producers focus on getting as many acres as possible planted during that very narrow five-to-ten-day 'optimum planting window,'" he says. "As more and more producers strive to cover more acres in less time and manage inputs on a prescription basis, AGCO is delivering the equipment, technology and crop production expertise our customers need."
AGCO conducted field tests on the 9800VE Series planter, concluding it can produce consistent speed spacing, whether planting at speeds below 4 mph or above 9 mph. The SpeedTube component controls the seed from meter to furrow to limit the so-called "ricochet affect" when planting at higher speeds.
Other features of note include:
vSet seed seed meter and vDrive electronic drive system
optional automated DeltaForce hydraulic downforce
fully integrated 20/20 SeedSense monitoring
FieldView data collection (available as a factory-installed option)
For more information, visit www.white-planters.com.