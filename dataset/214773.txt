Chicago Mercantile Exchange live cattle futures on Wednesday settled higher after short-covering helped recoup some of the market’s recent cash price related losses, traders said.
A few investors adjusted positions before the U.S. Department of Agriculture’s monthly Cattle-On-Feed report on Friday.
December live cattle finished 0.700 cent per pound higher at 120.200 cents, and February ended up 0.600 cent to 125.750 cents.
Wednesday morning’s Fed Cattle Exchange resulted in $119 to $119.25 per cwt sales versus $124 there last week.
Some packers in the U.S. Plains responded by paying $119 per cwt for slaughter-ready, or cash, cattle. That was down as much as $6 from the week before.
Wednesday’s market gains were an adjustment to selling in recent weeks that brought futures down from a high of 127.875 cents per pound on Nov. 2, fueled by higher prices at that time, a trader said.
“It’s just two steps down and one up as we hit a little bit of support today,” said U.S. Commodities analyst Don Roose.
Cash prices will in part depend on beef demand during the year-end holidays when turkey and ham are heavily promoted, he said.
Processors are also less inclined to pay more for cattle given their slipping margins and the Thanksgiving holiday-shortened workweek.
Short-covering, technical buying and live cattle futures advances lifted CME feeder cattle.
November feeder cattle closed up 0.375 cent per pound to 158.000 cents.
Mostly Soft Hog Futures Close
CME lean hogs ended mostly weak. Some investors sold deferred months and bought December, partly because of its discount to the exchange’s hog index for Dec. 13 at 66.68 cents.
Wednesday’s lower cash and wholesale pork prices further weighed on deferred contracts, traders said.
December hogs ended 1.150 cents per pound higher at 61.125 cents. February closed down 0.050 cent to 67.450 cents, and April finished 0.800 cent lower at 71.625 cents.
Cash prices were pressured for a fourth straight session because of packers competing less for hogs as plants prepare to shutdown over the upcoming holiday, analysts and traders said.
They said retailers are buying less pork after nearly filling Thanksgiving and Christmas ham orders.
“Grocery purchases of ham and turkey are pretty much done. Now the question is how much of it will consumers buy,” Roose said.