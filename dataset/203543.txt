<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


SUN VALLEY, Idaho - Fresh from capturing media coverage in New York City, the Big Idaho Potato Truck is back for another year - and beyond.
Frank Muir, president and CEO of the Eagle-based Idaho Potato Commission, presented the commission's annual business plan Sept. 1 at the Idaho Grower Shippers Association convention in Sun Valley, stressing the truck would be back.
The truck's journey began in 2012 as a way to celebrate the 75th Anniversary of the Idaho Potato Commission but its public relations value was quickly evident. Besides the wow factor of a six-ton potato, Muir said consumers connect with the message of nutritious, heart-healthy Idaho potatoes and the truck's mission to help small charities with the commission's A Big Helping Program.
While some have wondered when the popular campaign would end, Muir said the commission's leaders believe in the power of the Big Idaho Potato Truck and want to continue it.
"We plan to keep running the Big Idaho Potato Truck until the wheels fall off," Muir said.
This past season, the truck visited 27 states and traveled through 1,000 cities, logging more than 28,000 miles. The Big Idaho Potato Truck attended a number of parades, including the St. Patrick's Day parade in Chicago.
The most recent tour ended Aug. 24 with the truck on a barge in New York City's Hudson River and New York Harbor.
"We surprised everybody and it really stole the show in New York City," he said. The Big Idaho Potato Truck passed by the Statute of Liberty, the Freedom Tower, the Brooklyn Bridge and other sites. The truck's appearance created a New York media stir - 50 million impressions and counting, Muir said.
A new television ad featuring the Big Idaho Potato truck will air on Nov. 10, avoiding the clutter of advertising during the U.S. presidential race.
The truck has been invited to escort the 70-foot national Christmas tree from a forest in Idaho to the nation's capital.
"We're really thrilled to be part of that," he said. "We're excited to see how it turns out."
The television commercial campaign will then skip the holiday period but air into the spring, Muir said.
The commercial, shown at the Idaho Grower Shippers Association convention, answers the question of whether Idaho potato grower Mark Coombs finally finds the truck - or not. Past commercials feature Coombs driving a truck or flying a plane in search of the truck, just missing it.