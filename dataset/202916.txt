My daughter was telling me about a youtube video (was it fake news?) the other day that cast some doubt on the reading prowess of President Trump.
 
The video presenter, as I took it, made the case that Trump is the master of plain speaking not as a result of well-considered communications savvy but instead as an extension of his limited vocabulary. Like millions of his fellow Americans, the former reality TV star prefers the video screen over the written word.
 
If Donald Trump has thin skin - as it seems to be - these potshots at his abilities are likely to be terrible for his psyche but rich fodder for his Twitter retorts. 
 
In any case, reading may not be a "thing" for President Trump, but many of us do enjoy it.
 
I recently received a review copy of the  book The Georgia Peach, written by William Thomas Okie and published by the Cambridge University Press.
 
The front cover image is a W.T. Pearson crate label,  I'm about three chapters into the book and it is most definitely not a narrowly focused commodity narrative, but rather a thoughtful treatment of the entire context of the peach in southern culture. From the second chapter, a few words about the peach in the 19th century:
 
"And this was the orcharded South prior to the 1850s; made by the pits, sown by Spaniards, scattered by Indians and growing up promiscuously along fencerows and roadsides. For planters, these orchards were self-effacing servants of King Cotton. For slaves, the orchards were part of the liminal landscape they increasingly called their own; the fruit became also a source of celebration, offering the barest moments of hilarity amid lives stolen for their masters' profit."
 
The commercial ascent of the peach in Georgia will be covered in upcoming chapters. A good read for anyone connected to the peach and tree fruit industry, to be sure. More on the book in a later blog....