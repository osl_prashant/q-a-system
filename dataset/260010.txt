Grains mostly higher and livestock mixed
Grains mostly higher and livestock mixed

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mostly higher Tuesday in early trading on the Chicago Board of Trade.
Wheat for March delivery advanced 11 cents at $4.9440 a bushel; March corn was up 6.40 cents at $3.88 bushel; May oats was off .20 cent at $2.6440 a bushel while March soybeans gained 10.20 cents at $10.4140 a bushel.
Beef was lower and pork was higher on the Chicago Mercantile Exchange.
April live cattle fell .90 cent at $1.2118 a pound; March feeder cattle lost .85 cent at $1.4130 a pound; April lean hogs was up 1 cent at $.6808 a pound.