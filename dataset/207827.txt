The National Organics Standards Board has scheduled a web conference to discuss hydroponics in organic food production — a hot subject from the board’s past two meetings. Both times, the board tabled a decision to gather more information on the topic.
 
 
The NOSB will not vote on a recommendation during the conference, which will be 1-3 p.m. Aug. 14 Eastern. The issue, on whether any non-soil-based growing method (or growing in containers as well) should be allowed in organic production, would affect hydroponic greenhouse growers who sell organic produce.
 
 
A transcript will be available about two weeks after the event, according to a news release. The web conference can be accessed here.
 
 
The NOSB is a federal advisory committee established by the Organic Foods Production Act of 1990 and administered through the U.S. Department of Agriculture’s Agricultural Marketing Service. The group recommends whether substances should be allowed or prohibited in organic production, handling, and processing and advises the USDA on other aspects of organic regulations.