Film examines Dolores Huerta from jazz to 'Si, Se Puede'
Film examines Dolores Huerta from jazz to 'Si, Se Puede'

By RUSSELL CONTRERASAssociated Press
The Associated Press

ALBUQUERQUE, N.M.




ALBUQUERQUE, N.M. (AP) — Dolores Huerta, the social activist who formed a farmworkers union with Cesar Chavez and whose "Si, Se Puede" chant inspired Barack Obama's 2008 presidential campaign's "Yes We Can" slogan, is the subject of a new PBS documentary.
The film "Dolores" examines the life of the New Mexico-born Mexican-American reformer from her time as a tireless United Farm Workers leader and campaign volunteer for Sen. Robert Kennedy's 1968 presidential run. It also dives into her work fighting against toxic pesticides and later her transformation into an immigrant rights advocate.
But it also looks at history's attempt to erase her.
Director Peter Bratt says the project began after executive producer and guitarist Carlos Santana heard about Huerta's love of jazz and dreams of becoming a dancer.
"Dolores" is scheduled to air on most PBS stations Tuesday through the series "Independent Lens."