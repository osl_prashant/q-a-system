BC-US--Cotton, US
BC-US--Cotton, US

The Associated Press

New York




New York (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Thursday:
(50,000 lbs.; cents per lb.)
COTTON NO.2Open    High    Low    Settle     Chg.May       82.71   83.16   82.56   82.97  Up    .23Jul       82.41   82.96   82.27   82.82  Up    .21Aug                               78.59  Up    .27Oct       79.90   80.66   79.90   80.66  Up   1.90Oct                               78.59  Up    .27Dec       78.32   78.74   78.19   78.59  Up    .27Dec                               78.55  Up    .13Mar       78.47   78.67   78.36   78.55  Up    .13May       78.60   78.67   78.45   78.54  Up    .05Jul       78.43   78.43   78.35   78.35  Up    .01Aug                               73.81  Up    .13Oct                               75.44  Up    .02Oct                               73.81  Up    .13Dec       73.51   73.88   73.51   73.81  Up    .13Dec                               74.11  Up    .14Mar                               74.11  Up    .14May                               74.53  Up    .14Jul                               74.61  Up    .14Aug                               72.95  Up    .14Oct                               74.18  Up    .14Oct                               72.95  Up    .14Dec                               72.95  Up    .14Dec                               72.99  Up    .14Mar                               72.99  Up    .14