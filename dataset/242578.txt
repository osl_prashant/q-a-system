Marketing lobsters, harvesting crabs on agenda at fish forum
Marketing lobsters, harvesting crabs on agenda at fish forum

The Associated Press

ROCKPORT, Maine




ROCKPORT, Maine (AP) — The social event of the year in the world of Maine fishing is coming to a close in Rockport.
Saturday is the final day of the Maine Fishermen's Forum for 2018. The event began on Thursday at the Samoset Resort. It takes place every year and is a trade show for the commercial fishing industry that also includes numerous seminars, a banquet and other events.
Saturday's events will touch on everything from how to effectively market Maine lobster to how to prioritize the goals of the New England herring fishery.
One seminar, "Developing a Viable Green Crab Fishery in New England," focuses on finding a use for a pesky invasive animal that jeopardizes shellfish.