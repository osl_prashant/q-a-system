AP-MI--Michigan News Digest 1:30 pm, MI
AP-MI--Michigan News Digest 1:30 pm, MI

The Associated Press



Good afternoon! Here's a look at how AP's general news coverage is shaping up in Michigan at 1:30 p.m. Questions about today's coverage plans are welcome, and should be directed to the AP-Detroit bureau at 800-642-4125 or 313-259-0650 or apmichigan@ap.org. Corey Williams is on the desk. AP-Michigan News Editor Roger Schneider can be reached at 313-259-0650 or rschneider@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date. All times are Eastern.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
TOP STORIES:
CENTRAL MICHIGAN UNIVERSITY-SHOOTING
MOUNT PLEASANT, Mich. — Administrative and other offices will be open Monday at Central Michigan University after a 19-year-old student allegedly killed his parents in his campus dormitory. The school is on "spring break all of this week so classes will resume as scheduled" on March 12, university spokeswoman Heather Smith said Sunday. "Other university operations will resume as normal (Monday) morning." The campus was on lockdown Friday and officials canceled events and activities that evening after Eric Davis Sr. and Diva Davis were shot to death that morning in their son's fourth-floor dorm room in Campbell Hall. University police Chief Bill Yeagley said they were helping James Eric Davis Jr. pack up for spring break. Davis Jr. fled the dorm after the shooting and was arrested Saturday morning following an intensive manhunt. He faces murder and gun charges. SENT: 250 words; photos and video.
With:
— CENTRAL MICHIGAN UNIVERSITY-SHOOTING-THE LATEST.
TEXAS PRIMARY-TRUMP
NEW BRAUNFELS, Texas — The Trump effect on Republican campaigns on full display in Texas' first-in-the-nation primary. Texas votes Tuesday. President Donald Trump recently tweeted his endorsement of Texas' entire slate of Republican statewide officeholders, down to the obscure job of railroad commissioner. His tweets followed weeks of Republican candidates showing there's no such thing as cozying too close to Trump, regardless of his rough edges or low approval ratings nationwide. By Paul J. Weber. SENT: 715 words, photos.
AROUND THE STATE:
WRONGLY CONVICTED-STRUGGLE
DETROIT — A Michigan man whose case helped inspire a state law that gives money to the wrongfully convicted is now fighting to win compensation. David Gavitt spent 26 years in prison for the deaths of his wife and two daughters before a prosecutor agreed the arson evidence behind his conviction wasn't credible. He would qualify for more than $1 million under a Michigan law that took effect last year. Yet the state is resisting his request for money and questioning whether he's really innocent. By Ed White. SENT: 670 words, photos.
TOURNIQUET KITS
GRAND RAPIDS, Mich. — A Michigan health care organization is pushing to make tourniquet kits more widely available in public spaces. The Grand Rapids Press reports that Spectrum Health in Grand Rapids is participating in the Department of Homeland Security's "Stop the Bleed" campaign. Spectrum Health Butterworth hospital is offering low-cost or free training sessions for companies and groups on how to properly use a tourniquet. The sessions typically last 60 to 90 minutes. SENT: 220 words.
EXCHANGE-CEMETERY HISTORY
FLINT, Mich. —University of Michigan-Flint students spent half of their semester in Old Calvary Catholic Cemetery. They collected data such as birth and death dates, stone type and conditions of headstones before pulling them from the ground and preserving them. Their work came as part of a cemetery preservation and interpretation class, taught by Thomas Henthorn. The class was created with the goal of combing historic study and civic engagement. By Zahra Ahmad, The Flint Journal. SENT IN ADVANCE: 1,063 words.
EXCHANGE-RING MYSTERY SOLVED
ROYAL OAK, Mich. —A man has been reunited with the Michigan high school class ring he gave his wife 66 years ago just weeks after her death. Eighty-five-year-old Ronald F. Birou of Bonita Springs, Florida learned the news from his daughter after The Royal Oak Historical Society Museum posted a pictured of the ring in the history mystery. A Royal Oak man found the ring with a metal detector buried on the grounds of a parochial school in neighboring Birmingham. By Mike Mcconnell, The Daily Tribune. SENT IN ADVANCE: 577 words.
IN BRIEF:
— AMPHIBIAN CONSERVATION-BELLE ISLE: The Belle Isle Nature Center in Detroit is celebrating daylight saving time with a program on amphibian awareness and conservation.
— ENERGY DEPARTMENT GRANTS-MICHIGAN: Three Michigan firms will receive federal Energy research and development grants totaling about $450,000.
SPORTS:
BKC--T25-B10-MICHIGAN-PURDUE
NEW YORK — No. 15 Michigan tries to repeat as Big Ten Tournament champions against No. 8 Purdue. The third-seeded Boilermakers beat fifth-seeded Michigan twice during the regular season by a combined five points.
BKC--T25-TOP 25 REWIND
DURHAM, N.C. — No.9 North Carolina's loss at No. 5 Duke gave the Tar Heels a tougher road in the Atlantic Coast Conference Tournament. That rivalry game was part of a slate of games to wrap up regular-season play in several top leagues, including the Big East, the Big 12, the Pac-12 and the Southeastern Conference. The week in the AP Top 25 included plenty of uncertainty for No. 19 Arizona, while the Big Ten wrapped up an early tournament. By Aaron Beard. UPCOMING: 600 words and photos by 3 p.m.
HKN--RED WINGS-WILD
ST. PAUL, Minn. — The Minnesota Wild, who are 19-2-5 in their last 26 home games since Nov. 14, host the rebuilding Detroit Red Wings. By Tyler Mason. Game starts at 7 p.m.
HKW-US--US OPPORTUNITY
ANNAPOLIS, Md. — The Olympic champion U.S. women's hockey players are trying to extend their gold-medal fame beyond the afterglow of their victory tour. Players who are taking their gold medals on television and around the NHL know they need to do more to help their positive momentum continue. SENT: 860 words, photos.
BKH--MICHIGAN BOYS BASKETBALL HOW FARED
How The Associated Press ranked high school boys basketball teams fared this past week. SENT: 500 words.
___
If you have stories of regional or statewide interest, please email them to apmichigan@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.