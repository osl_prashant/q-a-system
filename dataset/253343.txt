On Thursday, the USDA its World Agricultural Supply and Demand Estimates (WASDE) report, but it’s likely to be overshadowed later in the month by the March Prospective plantings Report on March 29.

The USDA increased its forecast of domestic soybean stocks to 555 million bushels based on the expectation of lower exports due to higher exports from Brazil.

Corn demand ticked higher as the USDA lowered 2017-18 corn stocks to 2.1 billion bushels.

Pro Farmer editor Brian Grete has reaction from the report on AgDay above.