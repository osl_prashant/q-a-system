Grains and livestock mixed
Grains and livestock mixed

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were lower Wednesday in early trading on the Chicago Board of Trade.
Wheat for March delivery lost 3.60 cents at $4.4940 a bushel; March corn was up .80 cent at $3.7440 bushel; March oats fell 3.40 cents at $2.3220 a bushel while March soybeans gained 5.60 cents at $10.3260 a bushel.
Beef was mixed pork was lower on the Chicago Mercantile Exchange.
April live cattle fell 1.22 cents at $1.1858 a pound; March feeder cattle rose .43 cent at $1.3793 a pound; April lean hogs was off .05 cent at $.6323 a pound.