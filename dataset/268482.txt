BC-Wheat KX
BC-Wheat KX

The Associated Press

CHICAGO




CHICAGO (AP) — Winter Wheat futures on the Chicago Board of Trade Wed:
OpenHighLowSettleChg.WHEAT5,000 bu minimum; cents per bushelMay521½522¾509¼516¾—5Jul541½541½528¼535¾—4¾Sep557½559½547½555—4½Dec583583¼571579¼—3¾Mar597597587¾594¼—2¾May604½604½592½601¾—2Jul605¼605½595¾605½—1½Sep612½—1Dec624¾625½620625½—1Mar625631625631—1May631—1Jul624¼—¾Est. sales 75,330.  Tue.'s sales 87,198Tue.'s open int 272,176