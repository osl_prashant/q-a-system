After crops are grown and harvested it’s critical the grain makes it to the end buyers—whether they’re two miles down the road or 2,000. When rivers, roads and other forms of infrastructure are incapacitated it can rob farmers of marketing opportunities.
Whether it’s Mother Nature, or the need for infrastructure improvement, grain must keep moving for the U.S. to avoid losing customers to South America and other competitors. The Soy Transportation Coalition (STC) is challenging the Trump administration to think about the farmer while making infrastructure decisions.
Agriculture and rural programs account for 25% of total appropriations in the White House’s recent infrastructure plan. Eligible programs include roads, bridges, inland waterway ports, broadband, power and electric, water resources and others. The plan states 80% of state funds will be provided to state governors, while the other 20% of funds will be reserved for grants.

“During this pivotal time in which the White House and Congress are developing a strategy for improving our multi-modal transportation system, it is critical the farmer perspective has a seat at the table,” says Gerry Hayden, STC chairman. The organization created the “Top 10 Most Wanted List” for the Trump administration to consider. The list includes:

Maintenance and rehabilitation of locks and dams with priority devoted to reliability of lock and dams along inland waterways
Dredge the lower Mississippi River between Baton Rouge to the Gulf of Mexico to 50’
Maintain the Columbia River shipping channel from Portland to the Pacific Ocean at no less than 43’
Allow six-axle, 91,000 lb. semis on the interstate highway system
Increase the federal tax on gasoline and diesel by 10 cents a gallon—index the tax to inflation and ensure rural areas receive funding from the fuel tax increase
More predictability and reliability of funding for locks and dams in the inland waterway system
Block grants to replace the 20 most critical rural bridges
Provide grants for states to load-test rural bridges to diagnose which bridges are sufficient or deficient
Full use of the Harbor Maintenance Trust Fund for port improvement projects
Multi-year or permanent extension of short-line railroad tax credit

One of the biggest priority issues for the Soy Transportation Coalition is dams and locks on inland waterways. “You’re only as strong as your weakest link,” says Mike Steenhoek, director of the coalition. “If a bridge is down you may have a 10 mile detour—but if you have just one lock go out of commission, then the whole system is not operational.”
He says if money were available new locks would be preferred, but ultimately ensuring reliability of this system is critical. Other suggested changes might be easier to implement and cost less money, such as allowing 91,000 lb. semis on interstates.
Farmer noses might turn at one of their suggestions—a 10-cent tax increase at the pump. “We naturally recoil at the prospect of giving more money to the government, but the cost of building and maintaining roads and bridges goes up,” Steenhoek says. “That would provide more money for roads and bridges.”
Obviously, there is no guarantee the administration will meet these requests as there is much left to be decided. No matter what, transportation issues are sure to affect how farmers compete on a global scale.