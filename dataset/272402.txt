Maine group to use grant to explore Japanese scallop farming
Maine group to use grant to explore Japanese scallop farming

The Associated Press

PORTLAND, Maine




PORTLAND, Maine (AP) — A Maine firm is set to receive a $300,000 grant to work on a scallop farming technique that has helped the valuable shellfish grow faster in Japan.
Coastal Enterprises Inc. will receive the money via The Foundation for Food and Agriculture Research, which is a nonprofit corporation set up by the farm bill in 2014. Democratic Rep. Chellie Pingree of Maine says the money will help Coastal research the farming of sea scallops in "groundbreaking new ways."
Coastal will investigate the economic viability of the Japanese technique, which Pingree says has also been shown to grow larger, meatier scallops.
The money is part of a round of grants from The Foundation for Food and Agriculture Research that will be matched by five companies, an industry group and three universities.