Global dairy giant Fonterra Co-operative Group Ltd, expects to pay its farmer shareholders more in the coming season, but the increase will offer little relief as the forecast remains below estimated break-even levels.
The world's largest dairy exporter said it would pay NZ$4.25 ($2.86) per kilogram of milk solids in the 2016-17 season, up from NZ$3.90 in the season just ending.

Chairman John Wilson said Fonterra's forecast took into account factors including the high New Zealand dollar exchange rate, supply volumes from other major dairy regions, global inventory levels, and the economic outlook of major dairy importers.

Four economists polled by Reuters had expected Fonterra to pay between NZ$4.50 and NZ$5.00 a kilogram, but New Zealand's peak rural industry body said the forecast was in line with its estimates.

"It might be a slight improvement over the previous two years, but in effect it will be another year of treading water financially for most farmers," said Andrew Hoggard, dairy industry chairman for lobby group Federated Farmers.

"Even though it will be better it is still not going to be a year where you can catch up on deferred maintenance and things like that."

Until recently, dairy was the backbone of New Zealand's economy, representing around 25 percent of exports. But prices have tumbled by more than half since early 2014, hurt by China's economic slowdown and global oversupply.

Weak dairy prices have put significant pressure on New Zealand farmers. More than 85 percent of dairy farmers were already estimated to be running at a loss.

The forecast marks the third year of low payouts and remains below an estimated break-even level of around NZ$5.28.

"We are expecting global dairy pricing to gradually improve over the season as farmers globally reduce production in response to ongoing low milk prices, however we continue to urge caution with on-farm budgets," said Fonterra's Wilson.

Looking ahead, Chief Executive Theo Spierings said the long term fundamentals for global dairy were positive, with demand expected to increase by two to three per cent a year due to the growing world population, increasing middle classes in Asia, urbanization and favorable demographics.

Fonterra and Australian rival Murray Goulburn are facing an Australian regulatory probe over recent cuts to milk payouts.

The Australian Competition and Consumer Commission is examining the timing and notice of price cuts to Australian dairy farmers, exposing the companies to potential fines of A$1.1 million ($790,000) if found guilty of misleading or unconscionable conduct.

($1 = 1.3912 Australian dollars)