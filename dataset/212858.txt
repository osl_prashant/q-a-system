Organic food sales in the U.S. reached $40 billion annually in 2016, according to a state-by-state report in March from the Organic Trade Association.The report says organic items now are in kitchens of more than 80% of U.S. households and account for about 5% of total food sales in the country.
Fruits and vegetables continue to be a pace-setter in the organic category, produce marketers say.
“We see the growth of organics in recent years as a key indicator for what consumers want moving forward, and that is why we have focused on expanding our line of organic products like the organic version of Mann’s Broccoli Cole Slaw, which just celebrated its 25th anniversary at retail,” said Jacob Shafer, spokesman with Salinas, Calif.-based Mann Packing Co.
Growers are responding to growing demand, said Steve Lutz, senior strategist with Wenatchee, Wash.-based fruit grower-shipper CMI Orchards.
“Washington State Tree Fruit (Association) reports organic Washington apple production is up by 41% over last season,” Lutz said.
Organic apples and pears make up about 9% of the entire industry crop, he said.
“At CMI, we recognized early on the massive potential for organic foods and began transitioning a large percentage of our orchards to organics,” Lutz said.
He said 19.3% of CMI’s production was organic apples and pears and the company anticipates more than a quarter of its crop will be transitioned to organics by 2020.
“We believe our commitment to organic production truly sets us apart, and we are very well positioned for the expected continued growth in organic consumption,” Lutz said.
CMI markets numerous organic apple and pear varieties under the Daisy Girl banner.
The organic category’s success includes vegetables, said Doug Classen, sales manager with Salinas, Calif.-based vegetable grower-shipper The Nunes Co.
“There’s steady, incremental growth,” Classen said.
Growth also embraces specialties, said Robert Schueller, director of public relations for Los Angeles-based World Variety Produce, which markets under the Melissa’s brand.
Schueller said a number of his company’s organic items have seen double-digit growth in the last year, including ginger, at 40%; strawberries, 25%; mangoes, 15%; Brussels sprouts, 12%; and baby yams, 10%.
“The consumption of organics continues to set records year after year as consumers embrace it as a lifestyle,” Schueller said.
One reason for the growth is accessibility, Schueller said.
“You find it in most retailers across the country,” he said.
Stores once thought carrying organics could help them stand out among competitors. Now, the category is becoming a mainstay in mainstream retail produce departments, Schueller said.
Organics no longer are just a trend among shoppers, said Bil Goldfield, director of corporate communications for Dole Food Co., Westlake Village, Calif.
“U.S. organic sales posted a new record of $43.3 billion in 2015, up 11% over the prior year and more than three times the overall food growth of 3%,” Goldfield said, citing OTA numbers.
Dole is seeing its “strongest growth in demand” for organic bananas and pineapples, but it also keeps expanding its organic lineup, Goldfield said.
“Adding to our long-established business in organic fruits, Dole is answering the increased demand for organic packaged salads with our expanded Dole Organic Salad line, launched in 2016, featuring six organic salad mixes including three new varieties and two all-new organic salad kits,” Goldfield said.
Gary Wishnatzki, owner of Plant City, Fla.-based Wish Farms, which grows and ships organic and conventional berries, describes the organic category as having “methodical growth.”
“A lot of merchandising has changed, where consumers are more likely to buy organic,” he said.
There still are hurdles the category has to clear, Wishnatzki said.
“It will continue to go up, but there are some limitations,” he said.
“It’s difficult to take a field and transition it because of the costs of doing so. There’s no market, really, for transitional product, so it’s difficult to transition because you get diminished yields without the bump in price.”
Otherwise, there’s no telling how far organics can go, said Chris Ford, Salinas-based organics category manager for the Vancouver, British Columbia-based Oppenheimer Group.
“The sky’s the limit in terms of growth,” he said.