Wisconsin farm turns to Assaf sheep, genetics to make cheese
Wisconsin farm turns to Assaf sheep, genetics to make cheese

The Associated Press

JUDA, Wis.




JUDA, Wis. (AP) — A Wisconsin cheese-making company is in the midst of a $2 million project to establish a flock of 1,500 sheep known for their high quality milk production.
The Wisconsin State Journal reports that Ms. J and Co. farm officials hope the Assaf sheep will be a gateway to higher profit margins, deeper flavors and more variety. The sheep are a synthetic breed created in Israel in the 1950s. The breed is known for its higher quality and higher milk production.
Jeff Wideman, lead cheesemaker at Maple Leaf Cheese, and his business partner Shirley Knox are working with Mariana Marques de Almeida, a senior animal scientist, agricultural engineer and an expert in Assaf sheep genetics. Almeida has spent nearly seven years working with Assaf sheep and making cheese.
___
Information from: Wisconsin State Journal, http://www.madison.com/wsj