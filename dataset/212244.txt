The New Jersey Department of Agriculture says its Farm to School program’s No. 1 product is knowledge, since it connects schools across the state to growers to source more than 100 types of Jersey Fresh-labeled produce.The program is designed to work with New Jersey’s hundreds of school districts to ensure fresh, local farm products are brought into school foodservice programs, said Beth Freehan, the program’s coordinator.
Kids experience New Jersey agriculture, rather than just learning about it, Freehan said.
Knowledge is the most valuable asset of the program, Freehan said, with lessons come through sensory experience in gardens, as well as tasting the final product in the cafeteria.
Recipes crafted with New Jersey-based fruits and vegetables proliferate in school kitchens, and students gain an understanding of the steps involved in food preparation, from field to table, Freehan said.
For produce suppliers, Farm to School is an investment in the future, she said.
“Teaching children about fresh fruits and vegetables is important to have the next generation of consumers develop a love of eating fresh fruits and vegetables,” she said. “It’s important to tell the parents at home they tried something and to go out and try it.”
Parents marvel at the results, Freehan said.
“It’s anecdotal, but we know parents are coming in and saying, ‘I never thought I’d get my child to eat this,’” Freehan said. “It increases children’s awareness.”
Farm to School has room to grow, Freehan said.
“New Jersey has 772 school districts, and it’s hard to say how many participate,” she said. “Some participate to varying degrees.”
A yearly survey indicates increased participation, Freehan said.
The Jersey Farm to School program is inviting produce growers and distributors to a conference it plans to host in 2018. A date has not been set, but the event likely will be in March, Freehan said.
“We’ll probably be having some farmers and distributors as panelists sharing sales info and how they engage in schools and how they fit into the buying patterns and budgets in schools,” she said.
A similar conference in 2016 had some grower and distributor participation, but it drew mainly schools and foodservice operators.
Organizers want to see increased participation from the supplier side, Freehan said.
Local growers occasionally visit schools or host students for tours of their operations. Schools receive educational materials that can be incorporated into lesson plans, and some schools start their own vegetable gardens.
“Our role is to reach the stakeholders in those programs and point them in the direction of funding and encourage them to purchase locally and start school garden education programs,” Freehan said. “It’s a very multi-pronged effort.”
Suppliers say they like the program and appreciate its objective.
“I think it’s a great way to get your product to a place to appreciate it,” said David Sheppard, owner of Jersey Legacy Farms LLC, an organic vegetable grower in Cedarville, N.J.
“All those programs are great. I think it not only helps us — it helps educate kids about what’s out there.”
The only potential glitch to participation may be timing, Sheppard said.
“Early spring, you can do a good program or, in the fall, you could get into it, but we’re out of production for the majority of the school year,” he said.
“It’s a nice way to get local school systems to support the local economy, doing that.”