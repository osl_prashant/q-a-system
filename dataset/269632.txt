Top EU court: Poland broke law by logging in pristine forest
Top EU court: Poland broke law by logging in pristine forest

By MONIKA SCISLOWSKAAssociated Press
The Associated Press

WARSAW, Poland




WARSAW, Poland (AP) — The European Union's top court ruled on Tuesday that Poland violated environmental laws with its massive felling of trees in one of Europe's last pristine forests.
The ruling by the European Court of Justice said that, in increasing logging in the Bialowieza Forest in 2016-17, Poland failed to fulfil its obligations under EU directives to protect natural sites of special importance.
Poland's environment minister at the time, Jan Szyszko, argued that felling the trees was necessary to fight the spread of bark beetle infestation. Heavy machines were used in the process, causing additional damage to the forest.
Poland's conservative government is involved in a number of disputes with the EU, including one over changes to the judicial system — an argument that has led Brussels to trigger a process that could lead to punitive measures against Warsaw.
In the forest dispute, the court said bark beetle infestation did not justify the scale of the logging, while Poland failed to ensure the safety of birds and other species in the forest. No fines were imposed because the machines have been removed and the excessive logging has stopped.
Environmentalists say the large-scale felling of trees in Bialowieza, which straddles Poland's eastern border with Belarus and is a UNESCO World Heritage site, destroyed rare animal habitats and plants in violation of EU regulations. They held protests at the site and brought the case before the EU court.
The chief executive of the ClientEarth environmental organization, James Thornton, said the ruling was a "huge victory for all defenders of Bialowieza Forest, hundreds of people who were heavily engaged in saving this unique, ancient woodland from unthinkable destruction."
Philippe Lamberts, co-president of the Greens group in the European Parliament, said Poland's government has "repeatedly undermined rule of law."
"I hope this ruling will at last convince the Polish government that they need to change course," he said.
In January, Poland replaced its environment minister and stopped the logging. The new minister, Henryk Kowalczyk, has said Warsaw will respect the EU court's ruling and will seek better ways of protecting the forest.
The forest covers tens of thousands of hectares (hundreds of thousands of acres) in Poland and Belarus, and is home to hundreds of animal and plant species, including bison, lynx, moss and lichen.
Its younger parts have been traditionally used to produce timber, a source of income for local residents.
The European court ordered Poland to pay court costs. It didn't specify the amount.