When they pick up a bunch of bananas, most consumers look for fruit that is beyond the solid green stage but not yet to the point where it’s starting to “speckle,” said Rick Feighery, vice president of sales for Philadelphia-based Procacci Bros. Sales Corp.
Other shoppers, however, may want green bananas that can ripen slowly over several days, and still others may prefer overripe fruit for baking banana bread.
Ensuring that retail customers have the right stage fruit to meet consumers’ needs starts with the ripening process.
While major supermarkets usually have their own ripening rooms, smaller chains and independent stores often rely on a handful of companies like Proccaci Bros. to ripen their fruit.
Procacci Bros. takes its role as a “boutique ripener” very seriously, Feighery said. The process is not something to be rushed.
“We’re not juicing it up for quick sale,” he said.
The longer the ripening process lasts, the longer the shelf life will be, he said.
Banana color guides typically show seven or eight stages.
“We try to go from green to a 3 over a four-day period,” Feighery said. “It’s a slow process.”
Feighery believes ripening fruit for seven-day-a-week delivery is the way to go.
“We don’t want a customer to buy a week’s worth of bananas at a time,” he said. “We want them to buy them every single day.”
Retailers who buy bananas every day will increase their yields, see diminished shrink — if any — and should have a range of colors on the shelf for shoppers to choose from, he said.
 
Organic method
Although the process for ripening organic bananas is basically the same as that for ripening conventional ones, organic fruit may require a bit more attention, said Tom Yachtis, lead ripener for Four Seasons Produce Inc., Ephrata, Pa., which ripens organically grown Fair Trade bananas for Oke USA Fruit Co., West Bridgewater, Mass., as well as fruit from other suppliers.
Every pallet can contain bananas from six or seven sources that were grown, harvested and packed under different conditions, he said.
“They all have different stories behind them,” Yachtis said, yet they all must be ripened to meet the customer’s standards.
“You have to keep an eye on them,” he said, which may mean adjusting temperatures and moving fruit from one ripening room to another.
Four Seasons has six ripening rooms, which can handle 21 pallets, and nine double rooms, which accommodate 42 pallets.
Yachtis estimates that more than one-third of the company’s bananas are organically grown, and they’re available in several pack sizes and configurations.
Simplicity is the key to a successful ripening process at Procacci Bros., Feighery said.
“It’s a very basic forced-air ethylene system,” he said. “I think the way we do it is pretty efficient.”
The fruit is checked at least three times a day to see how it is reacting to the ripening process, said Carlos Rodriguez, director of sales.
Procacci Bros. has about 20 ripening rooms that can accommodate 16 pallets per room.
Since all the fruit is in one layer and not racked, he said, “We can look at every individual pallet at any given time.”
The goal is for the bananas to be fully ripe within about two days after they’re delivered.
Bananas are ripened to order, but Rodriguez said communication between the ripener and the customer is critical.
Customers must be educated about the different ripening stages, he said. If buyer and seller aren’t on the same page, it can be costly.
“If it’s too ripe for a customer, they don’t want it,” he said.
But once the ripener determines the customer’s expectation, he said, the ripening process “is pretty much like clockwork.”