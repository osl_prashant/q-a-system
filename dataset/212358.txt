Almost $25,000 was raised during the seventh annual Blueberry Gleaning Festival for Redlands Christian Migrant Association and FreeD.O.M. Clinic. 
The event was May 21 at Spring Valley Farms, Umatilla, Fla. Guests paid $10 for every bucket of berries they picked, and donations went to the charities.
 
RCMA, Immokalee, Fla., is a nonprofit organization running 67 childcare centers across the state. The centers accommodate the needs of farmworker families. Ocala’s FreeD.O.M. Clinic provides medical and dental care to farmworkers, according to the release.
 
“It was a great day that allowed us to give back to RCMA and the clinic for the vital services they provide to our farmworkers,” Chuck Allison, co-owner of Spring Valley Farms, said in the release. “We are passionate about providing a great work environment for these workers.”