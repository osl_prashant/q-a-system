Produce retailers recognize the value of food grown close to home, and they have expanded their local offerings.
For entrepreneurs Dave Rand and Ryan Kimura, that was a starting point.
The two started Local Foods, a wholesale distribution company that has its own retail outlet on Chicago’s North Side in 2015 based on local residents’ hunger for local and sustainable food, particularly produce and meat.
There were some hurdles to clear, they said. For one thing, locally grown produce isn’t universally available — not at all some months — in Chicago.
“We are a destination store for Chicago consumers that are looking for the best food to put on their plate,” Rand Kimura said.
The operators place a heavy emphasis on locally grown foods and even sell MightyVine tomatoes grown by a partner company’s 15-acre greenhouse in the Chicago area.
Local Foods is an attempt to be local, even out of season, Rand said.
“Think farmers market,” he said.
It’s not fool-proof, since many produce items are seasonal, but Local Foods works to carry as many local items as possible from nearby growers, Kimura said.
“We traditionally range somewhere around 50-65 (stock-keeping units) for most of the year,” Kimura said.
He said that number doesn’t dip much during the winter months, thanks to suppliers such as Burlington, Wis.-based River Valley Ranch and Brooklyn, N.Y.-based Gotham Greens, which has a greenhouse in Chicago.
“We have a lot of exciting super specialty and micro-seasonal items, too, that many of our customers love — locally grown, stem-on ginger, local chestnuts, scoop your own cranberries,” Kimura said.
“These are the kinds of things that are even hard to find at the farmers market.”
But Local Foods isn’t all local, Kimura said.
“We started the store by carrying only local produce and what we heard was that our customers were looking beyond that,” he said.

“Their habits included buying produce from around the world and country, so we reacted and added more from the U.S., except avocados.”
In the past two years, Local Foods has discovered its customers see the company as a supplier that can vet sources for quality, Kimura said.
He mentioned avocados as an example.
“We work carefully to select suppliers that can provide delicious varieties that are also organic. Often, we source the same variety of avocado used by Rick Bayless and his Frontera team,” Kimura said.
However, it’s still Local Foods, and that is a strong undercurrent of the business, Kimura said.
“Because of our distribution business, we take a much more partner-oriented approach, especially with produce suppliers,” he said.
“We work with farmers to give them specs for the demand that we are hearing in the marketplace. We spend a lot of time working and finding new innovative produce suppliers.”
One example is the Roof Crop, which Kimura described as “amazing, high-quality greens that are hearty and delicious that you really won’t find in any other store.”
Local produce may be seasonal, but seasons are growing longer, Kimura said.
“I think it’s a bit of a fallacy to think that local produce is only around during the summer,” he said.
“Farmers in the Midwest have been doing an incredible job of extending their seasons through things like hoop houses and more indoor growing.”