Discount supermarket Aldi has been in the U.S. for decades but continues to see growth with a unique value/quality mix. Well, it was unique until this summer, when German rival Lidl opened its first stores in the U.S.
Lidl’s stores opened in three southeastern states to fanfare, then questions from critics, and then company retorts and support.
Both chains expect to continue to grow in 2018.
 
Nov. 6 – Shoppers like Lidl despite retail experts’ opinions
By Pamela Riemenschneider
A recent survey of Lidl U.S. shoppers finds they have a better opinion of the German discounter than the retail experts.
The “U.S. Consumers Rethink Grocery” report by Oliver Wyman surveyed shoppers in Lidl markets before and after they first shopped Lidl and found they were “impressed,” particularly with the Arlington, Va.-based retailer’s fresh produce offerings.
Two-thirds of the shoppers surveyed said they think “the freshness and quality of produce and bakery is great,” according to the survey. Only 27% said they are “disappointed” with the store and “don’t understand what the hype is about.”
 
Oct. 16 – After initial surge, Lidl sees drop
By Pamela Riemenschneider
Reports that traffic took a nosedive after the first wave of Lidl stores in the U.S. have analysts speculating whether the German discounter has the right format.
According to inMarket research, Lidl’s store traffic in nine Virginia, North Carolina and South Carolina markets suffered a steep drop from the retailer’s entry in June.
According to the numbers shared with The Wall Street Journal, Lidl drew 11% of consumer visits to traditional grocers. That number dropped to less than 8% by August.
The research firm isn’t the only one with doubts Lidl is the right fit. Neil Stern, partner at McMillanDoolittle, penned a scathing report for Forbes in late September pointing out some big flaws in Lidl’s strategy.
Stern said the stores were “too large, too overly-engineered and too costly to operate,” with up to 11 full-size checkout lanes and 25,000-square-foot total store footprints. Aldi and Trader Joe’s with their tighter footprints feel “exciting” by comparison.
 
Aug. 21 – Aldi sends message with grocery delivery
By Ashley Nickle
Aldi has partnered with Instacart to test grocery delivery in several markets, and analysts see it as a signal that Aldi is upping its game.
“It gives you an idea that their customer base and the company has evolved significantly in the last three to five years,” said Scott Mushkin, managing director for Wolfe Research. “Aldi competes more and more with Wal-Mart, more and more with Kroger. Those companies are offering pickup and delivery, and so I think Aldi did this as a way to say, ‘We can do that too if that’s what you want,’” Mushkin said.
 
July 10 – Lidl, Aldi prices equal
By Jim Offner
German retailer Lidl’s first foray into the U.S. brings it on par in pricing with fellow German discount retail chain Aldi and below other major chains, including Walmart, a recent study by Deutsche Bank shows.
Deutsche Bank analysts visited three of 10 stores Lidl opened in June — in Greenville, Kinston and Sanford, N.C. — to compare prices there against those at Walmart, Kroger and Aldi.
“We came away impressed with Lidl’s relatively upscale in-store experience, low prices, and unique product assortment,” analysts Paul Trussell and Shane Higgins said in the report.
Lidl, which has more than 10,000 stores in 28 countries, now has 10 stores in Virginia, North Carolina and South Carolina.
The Deutsche Bank analysts said pricing was “about in-line” with Aldi. Lidl prices also came in at 10% below the Walmart Neighborhood Market in the group’s first study and 5.7% below the Walmart Supercenter in its second study — 8.1% below, when excluding national brands. 
 
June 19 – Aldi aims for 2,500 stores by 2022
By Ashley Nickle
Aldi has committed to spend $3.4 billion to increase its U.S. presence to 2,500 stores by 2022.
The move will make Batavia, Ill.-based Aldi the third-largest grocer in the U.S. by store count and will enable it to serve 100 million customers per month, according to a news release.
This investment is in addition to Aldi’s earlier plans to spend $1.6 billion remodeling 1,300 stores by 2020.
The company announced the news just days before fellow hard discounter Lidl was scheduled to open its first stores in the U.S.
 
June 5 – Lidl to open nine stores on June 15
By Ashley Nickle
The first nine U.S. locations of Lidl are scheduled to open June 15 with four-day celebrations.
The stores are in South Carolina, Virginia and North Carolina, according to a news release. The nine grand openings include opportunities to sample products, receive free reusable bags, and the chance to win $100 Lidl gift cards.
“We cannot wait to open our first U.S. stores and introduce customers to grocery shopping refreshed, retooled and rethought to make life better,” Brendan Proctor, president and CEO of Lidl U.S., said in the release.  
Lidl lists among its attributes its layout — 20,000 square feet with only six aisles — as well as its private label selection. Organic produce items will be among the offerings.
Lidl plans to have up to 100 stores on the East Coast by the summer of 2018, according to the company. P