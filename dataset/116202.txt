Farm diesel softened 2 cents per gallon this week to a regional average of $1.94.





This week our farm diesel/heating oil futures spread is below our line in the sand at 0.30 which signals mild upside risk for retail diesel.





This week propane softened 8 cents per gallon.