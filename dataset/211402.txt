The first shipment of U.S. beef to China under a new trade deal went airborne on Wednesday, a Nebraska meat company said, just two days after Washington finalized details to resume exports, ending a 14-year ban.Greater Omaha Packing Co said it shipped beef by plane to China from Nebraska, a top U.S. beef producing state, to meet strong demand.
"They want it right away," Chief Executive Officer Henry Davis said about Chinese consumers.
Beijing banned U.S. beef imports in 2003 after a U.S. scare over mad cow disease, but last month agreed to allow U.S. shipments by mid July as part of a broader trade deal.
Talks moved quickly, and U.S. officials said on Monday they had finalized requirements for exports.
China is the world's fastest growing beef market, according to the U.S. Department of Agriculture, and its imports increased to $2.5 billion last year from $275 million in 2012.
To win business, Greater Omaha Packing has hired bilingual salespeople from China, Davis said. He added that the company had received hundreds of phone calls in recent months about sales to China from potential customers and distributors.
To make the first shipment, the company, which exports to other countries, affixed labels in English and Chinese on every box of beef on the flight, Davis said.
"We'd never done Chinese before," he said.
So far, only Greater Omaha Packing and Tyson Foods Inc, the biggest U.S. meat company, have processing plants approved by the USDA to ship beef to China.
Tyson did not immediately respond to a request for comment.
On Tuesday, Cargill Inc, another major beef processor, said that only a small percentage of the total current U.S. cattle supply would qualify for exports to China under the terms of the new trade agreement.
The deal requires U.S. producers to track the birthplace of cattle born in the United States that are destined for export to China and take other steps.
Some U.S. producers still view the market as lucrative, given China's expanding middle class.
The U.S. Meat Export Federation, a trade group, said this week that China's import requirements will add costs for producers.
However, CEO Philip Seng said "China holds exciting potential for the U.S. beef industry and for buyers in the market who have waited a very long time for the return of high-quality U.S. beef."