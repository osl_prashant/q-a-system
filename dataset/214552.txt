Baloian Farms shipped green bell peppers out of the Coachella Valley in October and plans to start red bells in mid-November, says sales manager Jeremy Lane. Volume should be similar to last year, he says, and quality looks good. (Photo courtesy Baloian Farms)

(Corrected) There’s good news for pepper lovers from growers in the California desert this fall and winter. Pepper supplies are plentiful and quality is good, they say.
Coachella, Calif.-based Peter Rabbit Farms started shipping green bell peppers Oct. 19, about a week later than usual, said John Burton, general manager of sales and cooler.
Red peppers started Nov. 2.
Shipments of red and green bell peppers should continue through December, barring a freeze or frost, he said.
“We had an extremely good growing season,” Burton said, though an unusually warm late summer slowed the plants down a bit and caused some delays.
“Some of the first flowers didn’t set as good as we had hoped,” he said, but conditions gradually improved.
“The quality of the plants and the peppers look extremely good,” he said in late October, but the green peppers may be slightly darker than last fall because they were on the plant for a longer time.
Pepper acreage at Peter Rabbit Farms is similar to last year, he said, though red pepper acreage may be up slightly.
Yields should be at least as good as last year, and red peppers appear to be larger than those picked last season.
There was speculation that demand could increase this season because of hurricane-caused shortages in other growing areas.
If that does happen, Peter Rabbit Farms will go as long as possible, Burton said. But it’s not unusual for the desert to experience cold weather in mid-December.
“We can’t do much about a freeze,” he said. “A freeze does the pepper deal in pretty quick.”
Baloian Farms, Fresno, Calif., is shipping green bell peppers out of the Coachella Valley and planned to start red bells in mid-November, said sales manager Jeremy Lane.
Volume should be similar to last year, he said, and quality looks good.
“We’re very pleased with the color and condition,” he said Oct. 31.
Pasha Marketing LLC, Mecca, Calif., started its green bell pepper program Oct. 23 and will continue until the first week of December, said Franz De Klotz, vice president of marketing.
Red peppers were expected to start in early November and continue until the first freeze hits.
Demand for bell peppers has been steady, De Klotz said, and he anticipated additional nation-wide demand because of the bad weather in the East earlier in the fall.
Coachella-based Prime Time International no longer grows hothouse bell peppers during the fall in the Coachella Valley, said Mike Aiton, who retired Nov. 1 as the firm’s director of marketing.
“We’ve moved all of our (hothouse) production during this time period to Mexico,” he said.
“We had a change of heart over the winter, based on the competitive nature in Mexico, and opted not to play the game this year,” Aiton said.
But the company will have plenty of peppers, with harvest of field-grown peppers currently underway in the Coachella Valley, along with its Mexican production. 
“We’ll have a full line of peppers and plenty of them, loading in Nogales (Ariz.) and San Diego,” Aiton said. “We’re still doing peppers. It’s just that sourcing has changed a little bit for us.”
The company should have hothouse and field-grown red, yellow, orange and green bell peppers through April 1, he said. Mini sweet peppers also will be available.
Prime Time also will have round and roma tomatoes from Mexico and asparagus all winter long, Aiton said.
“We have a pretty full lineup now,” he said. “We’re expecting to have (a) big winter after a rather disappointing one last year.”
 
Editor's note: An earlier version of the story incorrectly described Prime Time International's fall pepper programs. The company has a large field-grown pepper program in the Coachella Valley, in addition to its hothouse pepper production in Mexico.