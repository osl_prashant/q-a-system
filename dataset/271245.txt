Oral electrolytes play an indisputably important role in helping restore scouring and dehydrated calves to health and productivity.But is one formulation as good as the next? North Carolina State University researcher Geof Smith, DVM, MS, PhD, ACVIM, and his colleagues have taken an in-depth look at the content and metabolic mechanisms of an array of commercial oral electrolyte solutions to find out.
Smith, who is associate professor in the Department of Population Health and Pathobiology at the NC State College of Veterinary Medicine, says he wanted to ultimately improve the outcomes of calves in need of oral electrolyte therapy. “We can and do tell producers to feed electrolytes to scouring calves, but are we providing them with the best possible information regarding what products to use and how to feed them?” he asks.
Guidelines for best products
Smith says the purpose of oral electrolyte therapy is to correct dehydration, correct acid-base and electrolyte abnormalities, and provide nutritional support. He describes the following characteristics of an “ideal” oral electrolyte product:
Supplies sufficient sodium to correct extracellular fluid deficits.
Provides neutral amino acids (glycine, alanine or glutamine) that facilitate intestinal absorption of sodium and water.
Provides an alkalinizing agent (acetate, propionate, bicarbonate) to correct metabolic acidosis.
Provides sufficient energy (glucose) to correct hypoglycemia.
Facilitates a healthy gastrointestinal environment (doesn’t encourage microbial growth and/or encourages healing of damaged intestinal epithelium).
Provides an excess of strong cations (sodium, potassium, calcium, magnesium) relative to the concentration of strong anions (chloride, bicarbonate, D-lactate, organic acids).
To evaluate commercial oral electrolyte products, Smith advises following the recommended levels in Table 1. He notes that osmolality levels within the recommended range should be selected based on the individual calf’s milk-based diet. In general, he does not subscribe to the “rest the gut” theory that suggests scouring calves be taken off of milk-based diets and fed strictly electrolytes. “Oral electrolyte solutions — even those labeled ‘high energy’ — do not contain enough energy to sustain calves,” Smith says. “There is no proven benefit to withholding milk, and this strategy only will exacerbate further the animal’s state of negative energy balance.”
He further specifies that if a calf is depressed and refuses to nurse, one (and only one) feeding of milk can be withheld, and, in such a case, a high-osmolality (hypertonic) electrolyte product (osmolality between 400 and 600 mOsm/L) is required because it will provide higher energy levels to help replace lost milk. When milk is retained in the diet, particularly in beef calves or auto-fed calves that consume milk frequently, a lower-osmolality (isotonic) electrolyte (near 300 mOsm/L) is recommended. Smith says an osmolality of 600 mOsm/L is the greatest concentration at which intestinal villi can process the solution, so electrolytes with higher osmolality levels are not recommended.
Smith also discourages the use of oral electrolyte products containing psyllium as added fiber. “While it may produce a desirable change in the physical characteristics of feces, psyllium actually reduces calves’ energy levels and slows their recovery,” he says. Rice-based electrolyte products, he adds, are becoming a popular choice in human medicine. However, Smith cautions that such products cause severe diarrhea in calves because they lack a maltase enzyme needed to metabolize the glucose polymers they contain.
The importance of alkalinizing agents
Smith says it is absolutely essential that any oral electrolyte solution used in calves have an alkalinizing agent to correct metabolic acidosis. The three most common alkalinizing agents are bicarbonate, acetate and propionate.
Bicarbonate is the alkalinizing agent used almost exclusively in commercial oral electrolyte solutions in the United States, while acetate- and propionate-containing products are more widely available in Europe and Canada. Smith says that while all three components have similar alkalinizing ability, he prefers acetate and propionate over bicarbonate for a number of reasons:
Acetate and propionate produce energy when metabolized; bicarbonate does not.
Acetate and propionate stimulate sodium and water absorption in the intestine.
Acetate and propionate inhibit the growth of Salmonella and other bacteria.
Bicarbonate alkalinizes the abomasums.
The researcher notes that the higher pH levels created in the abomasum by bicarbonate increases the likelihood that bacteria such as E. coli and Salmonella will remain viable and reach the small intestine, thus potentially cultivating an environment for more detrimental pathogen growth and greater intensity and/or duration of scours. As an illustration of this phenomenon, Smith points to the fact that enterotoxigenic E. coli (ETEC) diarrhea has been repeatedly induced in calves older than 3 days of age, by feeding bicarbonate along with E. coli inoculation. ETEC diarrhea in its naturally occurring state typically only is observed in calves 1 to 3 days of age.
In a recent trial, Smith and his colleagues compared pH levels in the abomasa of calves fed (a) one of two bicarbonate-containing electrolytes; (b) an electrolyte containing acetate; and (c) milk replacer alone. They found that abomasal pH readings of 5.5 or higher occurred significantly more frequently in the calves fed bicarbonate, compared to calves fed the acetate-containing product or no electrolyte. Specifically, those percentages were 35.6 and 43.8 percent, respectively, for the two bicarbonate groups; 9.5 percent for the acetate group; and 20.1 percent for the milk-replacer-only group. These results reinforced Smith’s preference for acetate as the preferred alkalinizing agent in oral electrolyte solutions.
“As we learn more about the mechanisms of how oral electrolytes work, we can help more calves survive and recover more quickly,” Smith concludes. “It’s important to understand what oral electrolytes can and cannot do, and to use them accordingly.”
Table 1. Recommended levels for contents and characteristics of commercial oral electrolyte solutions (Smith 2014).

Ingredient or   characteristic


Recommended level


Sodium


90   to 130 mM/L


Potassium


10   to 30 mM/L


Chloride


40   to 80 mM/L


Strong   ion difference (SID)


60   to 80 mEq/L


Osmolality


            300 to 600 mOsm/L