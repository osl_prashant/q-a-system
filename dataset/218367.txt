Cover crops have the potential to boost soil microbial activity, which could lead to long-term benefits. In addition, keeping cover on the ground could help choke out troublesome weeds.
“Our crops are healthy, but we’re starving our soil,” says Tim Recker, farmer in Iowa. “I’ve changed my operation to worry more about what’s going on in the top 6” of soil. I’m doing cover crops and less tillage.”
In its fifth annual cover crop survey, the Sustainable Agriculture Research and Education (SARE) program and the Conservation Technology Information Center (CTIC) reached 2,102 farmers to gain information about cover crop use. Farmers reported from across the U.S. with 88% having used cover crops before.
Key findings from the survey revealed:

Cereal rye is the No. 1 choice for cover crops, followed by oats and radish.
65% of users planted mixes of cover crops.
Yield boosts were seen in corn (2.3 bu. per acre) and soybeans (2.1 bu. per acre) after cover crops.
For the first time there was enough wheat data to conclude a 1.9-bu.-per-acre increase following cover crops.
75% of cover crops were seeded by farmers themselves.
73% of cover crops were planted after harvest and 27% were interseeded.
Planting into cover crops just before termination is on the rise at 39%.
66% of respondents said weed control was improved after cereal rye.

Non-cover crop users shared their concerns about time, labor, lack of economic return and fear that the crop won’t terminate properly and become a weed. The survey also showed those same farmers are more likely to try the practice when commodity prices are higher.

The survey also revealed 86% of respondents saw soil health benefits in the first year. “I’m the last person who I thought would ever do less tillage and use cover crops,” Recker says. “But it all ties in (to good soil health).”