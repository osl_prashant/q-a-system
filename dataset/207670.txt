Any industry leader remotely interested in fresh produce pricing at any level should be a heavy user of the USDA’s Market News Service Custom Average Tool.
 
The web tool has a new look to it, and I spent a few minutes checking it out this morning.
 
Here is a brief description of what it can do, from the USDA Market News:
 
The SCMN Custom Average Tool (CAT) offers users a way to condense the highly detailed information provided in Specialty Crops Market News reports. The tool allows users to determine average prices for any commodity available in Terminal Market, Shipping Point, and Retail reports from our entire dataset, using a variety of customizable filters. The CAT gives users the flexibility to narrow the average price selection down to a very granular range by selecting a specific location, package, unit, variety etc., or by broadening the average price range and selecting all or multiple locations, packages, varieties and other values in filter options. This tool empowers users with the capability to determine what averages are most meaningful to their applications.
?
Additionally, the CAT includes “snapshot” averages based on a sub-set of the most commonly requested filter options for each report type to give users average prices at a higher summary level. It also offers a graphing feature which visually displays custom average prices on a dashboard based on the date range and filters options chosen.
 
Custom Average Prices and Graphs
Terminal Market Averages
Provides custom average price information for terminal market data on a weekly, monthly, quarterly or annual time period
Shipping Point Averages
Provides custom average price information for shipping point data on a weekly, monthly, quarterly, seasonal or annual time period
Retail Averages
Provides custom average price information for retail data on a weekly, monthly, quarterly or annual time period
Shipping Point/Movement Graphs
Provides custom average price information compared to movement for shipping points on a weekly or monthly time period
 
 
Snapshot Average Prices and Graphs
Snapshot Average
Provides quick snapshots of average prices for terminal market, shipping point or retail data viewable on a weekly, monthly or annual basis.
 
 
TK: This morning, I looked at the Washington cherry f.o.b. season at shipping point.  The graph showed the up and down story of this season’s pricing.
 
While commodity specificity would seem to the goal of the Custom Average Tool, there also is an option to include all commodities in the calculation. That screen shows, with a quick glance, the overall tone - inflationary or deflationary - of price changes to all fresh produce commodites by week.  All in all, a gee-whiz web tool from USDA that is worth bookmarking for regular reference.