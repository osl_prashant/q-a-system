OKLAHOMA CITY (AP) - An Oklahoma sheriff said Friday that authorities are investigating the possibility of arson in connection with one of two large wildfires burning in Oklahoma, a blaze that's resulted in the death of one person.
Authorities believe the fire began April 12 near Vici, about 110 miles (180 kilometers) northwest of Oklahoma City, and later merged with a fire that began the same day about 20 miles (32 kilometers) away near Leedey. The combined blaze is known as the Rhea Fire and has burned about 452 square miles (1,171 sq. kilometers).
Dewey County Sheriff Clay Sander said it is possible the fire near Vici was actually just an offshoot of the other blaze, but arson also can't be ruled out. He has called in investigators from the Arkansas Department of Food, Forestry and Agriculture to determine whether the blaze was intentionally set.
"There is an individual suspect and that person has interviewed," but no arrest has been made, Sander said.
Shawna Hartman, a spokeswoman with the state agency, confirmed that the fire is under investigation. "Any time that a fatality happens in a fire, it is investigated to see if it was intentionally set," she added.
Sander has said the fire is blamed for the death of a woman found inside her burned vehicle. Arson is a felony in Oklahoma, and the death of a person as a result of arson could lead to a murder charge under state law.
Firefighters continued battling the Rhea Fire on Friday and had it 25 percent contained.
A big blaze known as 34 Complex fire also began April 12 near Woodward and has burned 97 square miles (251 sq. kilometers), a slightly smaller area than previously estimated due to a mapping error, according to Hartman. The 34 Complex fire is 60 percent contained.
Firefighters have made progress in containing the fires as temperatures and winds have dropped and the humidity has risen, and the current National Weather Service forecast calls for up to a half-inch of rain in the area by Sunday.
"The improved weather conditions (are) really going to help out the firefighting efforts," Hartman said.
 
Copyright 2018, Associated Press