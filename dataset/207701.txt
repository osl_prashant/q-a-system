Ten years ago in July, the Sacramento-based California Leafy Greens Marketing Agreement conducted its first official on-farm food safety audit.
“We’re marking that decade with kind of an enhanced focus on food safety in the industry this year,” said Scott Horsfall, CEO.
“We think it is significant that for 10 years the leafy green industry has been focused on food safety and focused on putting a set of safety standards in place on the farms.”
LGMA continues its work by sending out government inspectors to verify that the produce industry is instituting the required food safety practices, he said.
“I’ve always felt that the audits themselves are a means to an end,” Horsfall said.
“The end is really changing the culture of food safety on the farm. On all of these farms now, food safety is really the No. 1 priority (and) they take great pride in what they do.”
He said in the last 10 years, the LGMA had seen a dramatic decrease in citations. In 2016, there were fewer than 400 citations and about 500 audits, Horsfall said.
“The citations are usually for very minor things, but they are cited and need to be fixed,” he said.
Still, Horsfall sees room for improvement, particularly in worker practices.
“Anytime you have people involved, you have people who need to be constantly trained, and people make mistakes,” he said.
As Horsfall looks to this year, he pointed out that the organization has expanded its “LGMA Tech” that involves a training program and workshops at a low cost to the industry.
Also, LGMA is playing a role in helping the industry comply with the new food safety regulations under the Food Safety Modernization Act, Horsfall said.