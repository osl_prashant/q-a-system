Canada's new Liberal government on Friday extended by one year rules put in place in 2014 to speed movement of grain by rail, after an unprecedented backup of grain on the Western Prairies that year.The former Conservative government took drastic steps to ease the backlog, including giving U.S. railways greater access to Canadian shipments and requiring minimum grain shipments by the country's two main railways, Canadian National Railway Co and Canadian Pacific Railway Ltd.
Those provisions were due to expire on Aug. 1, 2016. But the Liberal government, in office since November, said it needed more time to consider them.
Postponing the decision ensures that commodity shippers and railways can plan for the next year under predictable conditions, Transport Minister Marc Garneau and Agriculture Minister Lawrence MacAulay said in a joint statement.
Farmers and grain handlers rely on railways to move grain vast distances from western farms to ports and North American buyers.
The delay means that expanded use of interswitching - the transfer of cars from one railway's line to the line of another railway - remains in place for now, giving U.S.-based BNSF Railway Co further opportunity to handle Canadian shipments.
That provision has proved to be "an effective tool to provide additional competition" among railways, said Wade Sobkowich, executive director of Western Grain Elevator Association, whose members include Richardson International and Cargill Ltd .
Canadian National and Canadian Pacific said they were disappointed by Ottawa's delay.
The measures were "never justified," said CN spokesman Mark Hallman. The grain backlog was due to factors outside railways' control, such as a huge harvest and extreme weather, he said.
"The grain supply chain has never been more efficient," added Canadian Pacific spokesman Martin Cej.
BNSF spokesman Michael Trevino declined to comment.
Actual minimum requirements for grain movement were previously lifted, but the government's authority to impose them again was due to end this summer.
A provision that allows the government to order a railway to compensate a shipper for failure to provide adequate service also remains in place.
Bigger changes for Canada's grain transportation system may be in store.
In February, a study for the Canadian government said it should phase out over seven years its cap on the revenue railways can earn transporting grain.