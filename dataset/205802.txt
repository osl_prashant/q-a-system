Crop calls
Corn: Fractionally to 1 cent higher
Soybeans: 2 to 4 cents higher
Wheat: 2 to 5 cents higher
Corn and soybean futures benefited from light short-covering yesterday in reaction to lower-than-expected Midwest Crop Tour results from Illinois. However, some scouts in Iowa will have wet feet as they trek through Iowa this morning, which limited buying overnight. News the U.S. Commerce Department has imposed steep duties on imports of Argentine biodiesel is also supportive for the soy complex. Wheat futures benefited from short-covering given the oversold condition of the market.
 
Livestock calls
Cattle: Lower
Hogs: Lower
Livestock futures are expected to be lower this morning amid bearish supply expectations. Additional pressure in cattle futures is expected from lower cash cattle trade. Light-volume trade at $2 to $4 lower was reported yesterday in the Central Plains, but cattle are at a discount to the cash market, which could lessen the impact on futures today. Meanwhile, pork cutout values continue to slide, led by a decline in pork bellies as buying for BLT season ends.