State statisticians report the condition of the winter wheat crops in Kansas, Oklahoma and Texas declined slightly last week. State reports note that some rain was seen in areas of the states, but they were not generous enough to stabilize crop conditions. While statisticians kept crop rated "good" to "excellent" in Kansas unchanged last week at 38%, they rate 25% in "poor" to "very poor" shape -- a one-point increase from last week.
In Oklahoma, 37% is rated in top shape, which is a drop of three points from last week. In Texas 34% is rated in top shape, which is unchanged from last week, but 23% is rated in "poor" to "very poor" shape, which is an increase of two points from last week.
USDA will begin to release its regular Crop Progress Report series on Monday, April 3. Traders expect the condition of the winter wheat crop to have declined from when the crop entered dormancy in late November. For now, traders aren't overly concerned about crop ratings as private forecasters say there are better chances of rains for the region through the week.



State
Very Poor
Poor
Fair 
Good 
Excellent



Percent



Kansas -
			03/27/17

7


18


37


36


2



03/20/17

7


17


38


36


2



 


Oklahoma -
			03/20/17

5


14


44


35


2



03/13/17

5


12


43


38


2



 


Texas - 03/20/17

5


18


43


29


5



03/13/17

4


17


45


29


5



 



 
Here are some of the key observations in the state monthly summaries:
Kansas: For the week ending March 26, 2017, temperatures averaged nine to ten degrees above normal across most of the State, according to the USDA’s National Agricultural Statistics Service. Large portions of Kansas received measurable rainfall, however much of the State remained drier than normal. Spring tillage was active and planting was underway in southern counties. There were 5.9 days suitable for fieldwork. Topsoil moisture rated 26 percent very short, 42 short, 31 adequate, and 1 surplus. Subsoil moisture rated 20 percent very short, 41 short, 39 adequate, and 0 surplus.
Winter wheat condition rated 7 percent very poor, 18 poor, 37 fair, 36 good, and 2 excellent. Winter wheat jointed was 17 percent, behind 29 last year, and near the five - year average of 18.
Oklahoma: Although the spring storms brought some rainfall to the Southeastern and East Central districts, drought conditions continued to advance across the state last week. According to the OCS Mesonet, the biggest drought increase was experienced across Eastern and North West Oklahoma. Statewide temperatures averaged in the mid 60’s. Drought conditions were rated 81 percent moderate, up 7 points from last week and 46 percent severe, up 5 points from last week. Topsoil and subsoil moisture conditions were rated mostly short to adequate. There were 6.6 days suitable for fieldwork.
Winter wheat jointing reached 47 percent, down 1 point from normal.
Texas: Warm, windy conditions were experienced in many areas of the state last week. Strong winds caused damage to some crops in the Trans Pecos. Precipitation in the Northeastern part of the state fluctuated between a quarter of an inch and 2.5 inches. Most areas of the state recorded less than a quarter of an inch of rainfall. There were 6.4 days suitable for fieldwork.
Winter wheat was rated 72 percent fair to good. Some wheat fields in the Southern Low Plains showed signs of moisture stress, but the dry conditions extended to most of the Northern parts of the state, the Edwards Plateau and South Texas.