The Philippines, one of the world's biggest rice buyers, could start exporting the grain within the next six years, the country's agriculture minister said on Friday."Rice could be a potential export commodity provided we lower the cost of production and increase productivity," Emmanuel Piñol told reporters.
The Southeast Asian nation is the world's No.3 importer of the grain, with rice accounting for about a quarter of the expenditure of its poor.
Frequent typhoons usually hit the country's rice production, forcing it to import any shortfall to feed its 100 million people.
Piñol also said the government aimed to boost its stocks of grains, including rice, to six months' worth of consumption at the end of President Rodrigo Duterte's six-year term that began on Thursday.
The current rice inventory buffer is 30 days during the lean harvest season from July to October.
The Philippines imports more than a million tonnes of rice a year, mostly from Thailand and Vietnam, although the new administration is aiming to make the country self-sufficient within two years.
Piñol reiterated that there is no need to import rice for now given ample supply.
Some 500,000 tonnes of rice which the state's National Food Authority bought from Vietnam and Thailand arrived in the first quarter.
Former President Benigno Aquino has given the agency standby authority to import an additional 500,000 tonnes if needed, although the current administration has said it will review those plans.