The latest list of U.S. Department of Agriculture Food Insecurity Nutrition Initiativeawarded $17 million to help Supplemental Nutrition Assistance Programparticipants buy more produce.That’s great.
My beef with it?
Many of the awards are tied up with requiring participants to shop at farmers markets.
Don’t get me wrong. I love local produce, but many times a farmers market is not the most economical, most accessible location for people to buy fresh produce.
There are a few programs that have made it into retail, and I did notice some of the grant recipients are retail pilots, but the majority of them are still tied to farmers markets and produce stands. Let’s not lose focus on what we’re trying to do here. We’re trying to get people to eat more fruits and vegetables.
The farmers market isn’t always the right venue, especially for time-strapped low income shoppers.

I was lucky enough to visit a pilot of the Double Up Food Bucks program in Detroit at a SpartanNash affiliate store a few years ago. It was the first retail pilot for the Double of Food Bucks program. The Ann Arbor, Mich.-based Fair Food Network started its Double Up program at farmers markets, and still does a fair bit of work with those organizations, but they also recognize some of the greatest benefit of the programs can be realized at supermarkets.
I was doubly pleased to see that Oran Hesterman’s Fair Food Network received one of this year’s largest grants -- $3.5 million for “Innovating technology and expanding geographies for double up healthy food initiatives.”
The grant says:
 
If funded, this project will benefit more than 172,000 food insecure individuals by scaling a highly innovative and efficient form of the successful Double Up Healthy Food Incentive program. The project is fully aligned with USDA FINI Program goal and priorities. 

It’s focused on three core innovations:
Widespread implementation in grocery stores;
High-tech e-incentive card systems; and
Interoperability across retail locations, allowing SNAP users more choice in how they earn and use incentives.

The Fair Food Network hopes to have 16 participating communities in three states in 113 retail locations, with a reach to 172,000 SNAP participants buying an estimated $3.6 million in fresh produce.
Double Up Food Bucks also is tied to local produce, and the program plans to track local purchases in Michigan with retailers committing to purchasing a minimum of 15% of their produce from Michigan growers.
I love to see this concept expand, and wish them well.