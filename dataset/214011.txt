Canada’s biggest cheesemaker Saputo Inc has agreed to pay up to $490 million for debt-ridden Murray Goulburn Co-operative, becoming Australia’s top milk producer with its second major acquisition in the country.
The deal rescues the maker of Devondale milk and cheese after a disastrous foray into China, where hoped-for sales never eventuated. When funds to repay Murray Goulburn’s debt are included, the total deal value is worth around A$1.3 billion ($990 million).
“MG has reached a position where as an independent company, its debt was simply too high,” Murray Goulburn Chairman John Spark said in a statement pledging the board’s unanimous support for the buyout.
“The transaction represents the best available outcome.”
Saputo will gain Melbourne-based Murray Goulburn’s production facilities in addition to the Warrnambool Cheese and Butter (WCB) factories and brands it bought in two bites in 2014 and 2017 for roughly A$500 million.
The Canadian firm will now command just over half of Australia’s milk powder market, adding Murray Goulburn’s 42 percent share to WCB’s 9.8 percent, according to data from IBISWorld. Murray Goulburn is also the country’s third-biggest producer of milk and cream.
“Saputo will achieve substantial synergies from merging MG with WCB and its existing global operations,” Morgans analyst Belinda Moore said in note to clients.
But Moore also noted that the deal was “extremely disappointing” for Murray Goulburn share and unitholders, who will receive in most cases materially less than what they originally paid.
“Sell on-market today. No reason to be here,” she recommended.
China Curdle
With the A$638 million deal, Saputo will buy Murray Goulburn’s operating assets and liabilities, although not the co-op’s equity directly.
That will allow it to avoid exposure to several shareholder lawsuits, and regulatory investigations hanging over Murray Goulburn that concern potential breaches of disclosure rules as well as the way it slashed milk payout prices in 2016.
The acquisition price is equivalent to between A$1.10 and A$1.15 per share, Murray Goulburn said, little more than half its A$2.10 listing price in 2015.
Although its units leapt 14 percent in early trade to 95 cents, they lost those gains to trade down 1 percent by late afternoon.
The deal still needs to be cleared by the Australian Competition and Consumer Commission as well as the Foreign Investment Review Board.
Other suitors for Murray Goulburn had included Australia’s Bega Cheese Ltd, Italy’s Parmalat and China’s Inner Mongolia Yili Industrial Group Co Ltd.
Proceeds from the buyout would be handed to Murray Goulburn’s farmer shareholders in the form of higher milk payouts, as well as paying down debt, the Australian firm said.
Hoping to capitalize on the popularity of Australian produce in China, Murray Goulburn went public in 2015, using the funds to expand aggressively in Asia.
But sales fell far below expectations, swinging the company to a loss, angering its farmer-owners and ultimately forcing it to quit the strategy and shutter three production plants in May.