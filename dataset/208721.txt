Chicago Mercantile Exchange live cattle futures settled mixed on Thursday after an up-and-down session, supported by futures' discounts to preliminary cash prices but pressured by profit-taking, traders said.June closed up 0.175 cent per pound at 131.375 cents, and August down 0.150 cent at 124.025 cents.
On Thursday, a small group of animals at the Fed Cattle Exchange fetched $136.50 to $137.00 per cwt versus last week's average top price of $132.17.
About 6,000 head of market-ready, or cash, cattle on Wednesday in the northern U.S. Plains brought $135 per cwt, compared to roughly $137 there a week earlier.
Investors expect packers to pay more for supplies this week given their impressive profits and good wholesale beef demand ahead of Father's Day meal preparations, said traders and analysts.
But beef demand typically subsides this time of year when grocers tend to promote more pork and chicken for the summer grilling season, said analsyts and traders.
Profit-taking, technical selling and sporadic live cattle futures selling pressured CME feeder cattle contracts.
August feeders ended 0.850 cent per pound lower at 153.950 cents. 
Higher Hog Contracts
Thursday's strong cash and wholesale pork prices, along with fund buying, rallied CME lean hogs, said traders. July futures led gainers after investors bought that contract and sold June futures before it expires on June 14, they said.
June closed up 0.400 cent per pound at 82.025 cents. Most actively-traded July ended 1.125 cents at 82.200 cents, and above the 10-day moving average of 81.568 cents.
Packers need supplies, which are tightening seasonally, while trying to take advantage of their highly-profitable margins and brisk pork demand, a trader said.