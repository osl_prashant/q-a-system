Chicago Mercantile Exchange live cattle tumbled to a six-month low on Monday as a glut of animals at U.S. feedlots fueled concerns about plentiful beef supplies into the fall, traders said.
The U.S. Department of Agriculture reported after Friday’s market close that ranchers had placed 7.3 percent more cattle in U.S. feedlots in February compared with a year ago, well above trade estimates for a 4.2 percent gain. March 1 feedlot supplies were up 8.8 percent from a year earlier, also above expectations.
“We’ve had a few years of the rebuilding process since we decimated the herd size. There’s just a ton of production and feedlots are full,” said Tim Hackbarth, risk management specialist at Top Third Ag Marketing in Chicago.
Packer margins remain profitable and the cattle slaughter is likely to remain large, despite a holiday-shortened work week this week due to the Easter holiday, he said.
CME April live cattle closed 0.875 cent per pound lower at 115.175 cents, and June ended 0.900 cent lower at 105.300 cents.
Packer margins narrowed to $46.00 per head on Monday, from $59.45 on Friday and $54.70 a week ago, according to Denver-based livestock marketing advisory service HedgersEdge.com LLC.
Wholesale boxed beef prices for choice cuts shed 56 cents to $222.53 per cwt on Monday while select cuts fell 94 cents to $215.46.
Feeder cattle futures tracked live cattle contracts lower, with March ending down 0.775 cent per pound at 134.925 cents and April off 1.600 cents at 134.500 cents.
Hogs Retreat
CME lean hogs advanced in early trading in a bargain-buying bounce from Friday’s sharp declines, but ended mostly lower as that support faded amid large supplies of hogs.
Traders also remain concerned about a slowdown in export demand on simmering trade tensions between the United States and China.
April hogs closed down 0.400 cent per pound at 58.025 cents, May finished up 0.25 cent at 65.250 cents, and actively traded June was down 0.050 cent at 74.100 cents.
Hog traders are looking to Thursday’s quarterly USDA hogs and pigs report which is expected to show a 3.1 percent year-over-year increase in the U.S. hog inventory on March 1.