Mastronardi West hired John Killeen as director of business development at its Salinas, Calif., office. Killeen is an industry veteran with a background in produce sales and operations. He began his career at Dole Fresh Fruit Co. as a fresh fruit district manager.
He was at Dole for about 10 years and has worked at Tanimura &Antle, Muranaka Farm Inc., and two stints at NewStar Fresh Foods LLC.
Killeen comes to Mastronardi West from his previous job at NewStar as vice president of sales.
“I’m thrilled to join Mastronardi West,” Killeen said.“The company is renowned for such high quality produce and innovation, and I’m very excited to be a part of the team.” 
“John comes to us with a wealth of experience and knowledge in the industry, and we’re pleased to have him join our growing team,” said CEO Paul Mastronardi.
Mastronardi West is a division of Mastronardi Produce, based in Kingsville, Ontario.