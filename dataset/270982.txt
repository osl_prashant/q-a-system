California berry grower-shipper G&M Farms Inc. is marketing its own fruit and has a new brand.The Fresno-based company introduced its Farm to Table Berries label and began handling its own sales in December, said Greg Willems, owner and president.
Founded in 1999, G&M Farms ships blueberries from April to September and blackberries in June and July from Central California.
“We realized that the only way our business could be sustainable is if we controlled it from field to store,” Willems said.
To handle sales, Willems brought on board his best friend since childhood, Ross Pantoja, and another sales veteran.
Pantoja said the company is focused on selling directly to customers rather than relying on brokers. While G&M is currently focused on retail, it’s open to foodservice business, he said.
G&M ships throughout the U.S. and Canada and to overseas markets, Pantoja said.
When Willems founded G&M, he started with 7 acres of blueberries. He added blackberry production nine years ago, and now has more than 500 acres of total berry production.
Because of labor concerns, Willems said he plans to maintain his current levels of production.
Willems, who grew up in Kingsburg, Calif., where his father grew stone fruit and grapes, got the idea to plant blueberries in the San Joaquin Valley from one of his professors at Fresno State University, where he majored in plant science.
In addition to planting his own fruit, beginning in 2001 Willems worked as a plant representative for Silverton, Ore.-based Oregon Blueberry Farms and Nursery.
Willems said he’s traveled the world looking for the best blueberries. G&M also has worked with some of the top breeders on varietal development.