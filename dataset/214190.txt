The United Fresh Start Foundation will take a big share of the spotlight with the United Fresh Produce Association’s winter leadership meeting in January 2018.
“With the growing importance of the United Fresh Start Foundation’s work to increase children’s access to fresh produce, we wanted to create a new annual platform focused on the impact we’re having in communities across the country, to recognize our donors and supporters, and to work together to advance this critical mission,” United Fresh chairman of the board Susan Reimer-Sifford, general manager of CC Kitchens for Castellini Companies, said in a news release.
The release said the United Fresh Start Foundation Annual Conference is set for Jan. 16-18 at the Rancho Bernardo Inn in San Diego.
The event will feature a new half-day program focused on growing access to fresh fruits and vegetables. Other items on the agenda are;

All of United’s traditional volunteer leadership board, council and committee meetings; 
The foundation’s annual fundraising golf tournament;
A new silent/live auction; and 
The annual gala dinner recognizing the recipient of United’s 2018 Lifetime Achievement Award.

“As chairman of the foundation’s board of trustees, I’m excited to add this third leg to United Fresh events through the year – complementing United’s Washington Conference focused on public policy issues, and the annual convention and expos focused on expanding the fresh foods marketplace and technology innovations,” Phil Muir, president and CEO, Muir Copper Canyon Farms, said in the release. “This annual platform will bring the industry together to focus on how we can fundamentally change the way the next generation connects with fresh produce from their earliest years to become lifelong consumers.”
Beginning Nov. 6, registration for the entire event is open to all United Fresh members for $200, with a non-member rate of $400. Participation in the United Fresh Start Foundation Golf Tournament is set at $300 per player.