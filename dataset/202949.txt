The Little Potato Co. , Edmonton, Canada, has chosen Linkfresh Inc. to provide its enterprise resource planning system.
 
The 25-user Microsoft Dynamics Nav Linkfresh ERP solution will give Little Potato Co. capabilities that include integrated solutions for forecasting, finance, purchasing, sales, grading and production, according to a news release.
 
"The Linkfresh ERP solution will allow us to eliminate inefficient manual processes in our operations, such as double data entry, as well as providing real-time information for greater insight into customer service levels and financial performance," Matthew Havertape, vice president of finance for Little Potato Co. and project lead on the changeover, said in the release.
 
The Little Potato Co. develops, grows and markets creamer potato varieties, according to the release.
 
"We are thrilled to welcome The Little Potato Co. as our latest customer, and are excited to help them deploy the latest ERP technology to consolidate their market leading position," Robert Frost, founding director of Linkfresh Inc., said in the release.