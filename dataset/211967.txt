The California table grape harvest, projected at 111.4 million 19-pound boxes, continues through the summer. 
Last season, California grape growers shipped 108.9 million 19-pound box equivalents, about 40% of which was shipped from May through August, according to the California Grape Commission, Fresno. By the end of August, 60% of the overall crop had been shipped in 2016, but the season will last into January.
 
More than 85 varieties are grown in California, according to a news release from the commission. Leading varieties are scarlet royal, autumn king, flame seedless, crimson seedless and sugraone. About 93% of the state’s grapes are seedless.
 
According to the release, the U.S. Department of Agriculture reports that 36% of the 2016 crop was exported, mostly to Canada, Mexico, China, the Philippines, Taiwan and Japan.