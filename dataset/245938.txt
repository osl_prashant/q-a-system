The Agricultural Retailers Association (ARA) finalized its list of public policy priorities on Feb. 7 during the Winter Board of Directors and Committee Meetings held in Washington, D.C.
ARA’s policy priorities center on the farm bill, rural infrastructure and transportation, Chemical Facility Anti-Terrorism Standards (CFATS) reauthorization, continued regulatory reform and a pro-growth economic agenda for rural America.
FARM BILL

Support a farm bill protecting crop insurance safety nets, which are vital for farmers.
Support improving certification process for technical service providers, and include 4R nutrient management in Natural Resources Conservation Service stewardship program.
Support the National Agricultural Statistics Service Conservation Effects Assessment Project.
Maintain current acreage for conservation programs, and protect working lands.

RURAL INFRASTRUCTURE AND TRANSPORTATION

Support funding for rural broadband and transportation infrastructure; lessen regulatory burdens.
Support pilot program to lower the federal age to receive a commercial driver’s license to 18 years old for short-haul drivers moving farm supplies or agricultural commodities to increase the count of available drivers.
Support electronic logging device exemption for all agricultural operations.

CFATS REAUTHORIZATION

Support short-term (four years to six years) reauthorization—December 2018 is the current expiration—that recognizes industry-led security measures such as ResponsibleAg and provides consistent and clear federal definitions for the terms ammonium nitrate and ammonium nitrate mixtures.
Maintain notice-and-comment rulemaking requirements of the APA for any changes to chemicals of interest found in CFATS. Appendix A.

REGULATORY REFORM FOR AGRICULTURAL RETAILERS

Support legislation that protects users from National Pollutant Discharge Elimination System permit’s duplicative requirements and legal liability.
Support codification of the process safety management definition of “retail facilities” using the “50%” rule.

PRO-GROWTH ECONOMIC AGENDA
FOR RURAL AMERICA

Support a fix to the tax reform package reinstating the domestic production activities deduction, also known as the Section 199 deduction.
Support domestic energy production and fair-trade agreements, such as NAFTA.
Support a new, usable guest worker visa program that works for all of American agriculture.

For more background on policy priorities and the ARA position, visit www.ARADC.org/priorities.