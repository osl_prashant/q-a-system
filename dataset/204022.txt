Pro Farmer Editor Brian Grete has been leading the eastern leg of the 2017 Farm Journal Midwest Crop Tour where he has seen a bit of everything when it comes to crop conditions. U.S. Farm Report’s Tyne Morgan caught up with him in Bloomington, Illinois and asked him about the best and the worst of what he’s seen so far.
Some highlights:

Biggest surprise: The extreme variability.  “It’s been off the charts in the corn yields in particular.”
Biggest disappointment: Soybean pod counts.  “They have potential. There’s a lot of little pods, but they’re going to need some time.”
Most impressed with: Grain length on corn.  It is much greater than anticipated, although it is still an immature crop.

 



 
See Brian Grete’s complete interview with Tyne Morgan of AgDay in the video player above.