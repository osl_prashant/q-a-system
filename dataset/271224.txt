A recent online blog in the Atlanta Journal-Constitution about a Georgia farmer who was attached by his rabid cow makes light of the incident, but rabies in livestock is no joke.The author, George Mathis, says after the attack the farmer was able to, “blast the unnamed and unarmed cow three times with his shotgun.” He goes on to say that the cow was euthanized several days later.
This is the disturbing part. A rabid animal that potentially has much more human contact than wildlife is definitely a concern for transmission of rabies to humans.
It’s always wise to put rabies on a differential list for cattle. Below are the clinical signs of rabies in cattle that veterinarians and producers need to keep in mind. Make sure to stress to producers that if they suspect rabies in an animal, to call a veterinarian immediately and not wait to “see how it comes out.”
Clinical signs of rabies in cattleCattle with “furious” rabies can be dangerous, attacking and pursuing humans and other animals. Cattle with “dumb” or paralytic rabies have minimal behavior changes, but progress into paralysis.
Clinical signs of rabies can be varied in cattle and other animals. Some of the more common clinical signs include:
Sudden change in behavior
Progressive paralysis
Ataxia
Abrupt cessation of lactation in dairy animals
Hypersensitivity/alertness
Abnormal bellowing
Paralysis of the throat
Drooling
Head extension
Bloat
Choking behavior
Read more about cattle rabies here.