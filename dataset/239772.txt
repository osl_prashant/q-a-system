Calamari on the menu as feds maintain US squid fishing quota
Calamari on the menu as feds maintain US squid fishing quota

The Associated Press

PORTLAND, Maine




PORTLAND, Maine (AP) — Federal fishing regulators are keeping the quota for commercial squid fishermen about the same under new fishing rules that take effect soon.
U.S. fishermen harvest shortfin and longfin squid in the Atlantic Ocean. The squid are used as food, such as calamari.
The National Oceanic and Atmospheric Administration says it's keeping the quota for shortfin squid the same and increasing the longfin squid quota by 2 percent. The new rules are effective on April 2.
The squid have been brought to shore from Maine to North Carolina over the years, and the fishery is based mostly in Rhode Island. Fishermen harvested more than 14.7 million pounds of shortfin squid and nearly 40 million pounds of longfin squid in 2016.