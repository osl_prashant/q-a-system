Grains mostly lower, livestock mixed
Grains mostly lower, livestock mixed

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mostly lower Thursday in early trading on the Chicago Board of Trade.
Wheat for March delivery was fell 0.10 cents at $4.4930 a bushel; March corn was rose .40 cent at $3.6640 bushel; March oats was off .60 cent at $2.6040 a bushel while January soybeans lost 1.60 cents at $10.3540 a bushel.
Beef was mixed and pork was higher on the Chicago Mercantile Exchange.
February live cattle was up .45 cent at $1.2850 a pound; March feeder cattle fell .62 cent at $1.4628 a pound; February lean hogs rose .38 cent at $.7018 a pound.