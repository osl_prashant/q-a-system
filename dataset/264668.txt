BC-Merc Early
BC-Merc Early

The Associated Press

CHICAGO




CHICAGO (AP) — Early trading on the Chicago Mercantile Exchange Wed:
Open  High  Low  Last   Chg.  CATTLE                                  40,000 lbs.; cents per lb.                Apr      119.60 119.75 118.75 119.05   —.52Jun      109.00 109.12 108.10 108.45   —.55Aug      106.75 106.85 105.82 106.12   —.58Oct      109.52 109.62 108.85 109.12   —.33Dec      113.22 113.30 112.55 112.77   —.33Feb      114.97 114.97 114.22 114.45   —.50Apr      115.15 115.15 114.42 114.65   —.37Jun      108.85 108.90 108.52 108.72   —.33Est. sales 31,903.  Tue.'s sales 81,549 Tue.'s open int 356,462                FEEDER CATTLE                        50,000 lbs.; cents per lb.                Mar      138.15 138.37 137.50 138.00   +.23Apr      137.82 138.17 137.17 137.92   +.47May      138.30 138.75 137.82 138.50   +.25Aug      144.10 144.10 143.02 143.62        Sep      145.42 145.42 144.47 144.92   —.15Oct      145.75 145.77 144.72 145.12   —.25Nov      145.40 145.70 144.85 145.02   —.35Jan      141.75 141.75 141.07 141.07   —.35Est. sales 5,176.  Tue.'s sales 18,708  Tue.'s open int 52,567,  up 952        HOGS,LEAN                                     40,000 lbs.; cents per lb.                Apr       63.60  63.60  63.10  63.40   +.15May       70.30  70.30  69.90  70.27   +.07Jun       76.70  76.80  76.15  76.45   —.30Jul       77.97  77.97  77.20  77.40   —.50Aug       78.25  78.37  77.57  77.77   —.48Oct       67.70  67.80  67.17  67.30   —.42Dec       62.67  62.70  62.20  62.22   —.38Feb       66.10  66.10  66.10  66.10   —.22Apr       69.70  69.77  69.67  69.77   —.33May       75.07  75.07  75.07  75.07   —.93Jun       78.57  78.57  78.57  78.57   —.48Est. sales 11,868.  Tue.'s sales 42,490 Tue.'s open int 234,265,  up 5,094      PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.