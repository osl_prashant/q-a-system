Farmers and applicators looking to cover more acres with dry fertilizer might want to check out the new AB485 air boom option for the John Deere F4365 high-capacity nutrient applicator.The new 70-foot stainless steel air boom feature adds greater precision to dry product applications, according to Doug Felter, product marketing manager. 

 

In a released statement, Felter says the air boom “combines application speed and accuracy with the comfort and power built into the F4365, thanks to the John Deere PowerTech PSS 9L engine and IVT transmission.”
The new boom feature allows for fertilizer application across a 70-ft. swath at speeds up to 30 mph, and at maximum rates up to 1,200 lbs. per acre at 10 mph.
The AB485 Air Boom option comes factory-installed on new F4365 machines for model year 2018. For more information on the air boom option for the F4365 High-Capacity Nutrient Applicator, farmers and applicators can contact their local John Deere dealer or visit JohnDeere.com/ag.