On January 25, Paul Neiffer, CPA and principal at accountancy CliftonLarsonAllen and author of The Farm CPA blog will present a breakout session at the 2018 Top Producer Seminar on how to plan whether tax reform happens or not.
Tax reform is creating a lot of uncertainty for farmers. On January 25, Paul Neiffer, CPA and principal at accountancy CliftonLarsonAllen and author of The Farm CPA blog will present a breakout session at the 2018 Top Producer Seminar on how to plan whether tax reform happens or not.
[Register for the 2018 Top Producer Seminar now!]
The House and Senate are expected to vote on a tax reform plan Dec. 18. Regardless, there are several tax tips that remain the same regardless of policy. Here are three of Neiffer’s top tips.
Keep Accurate Records This should go without saying, but your accountant can only work with the information you provide. If your records are incomplete, you could end up paying more taxes than necessary.
Pay Your Kids “If you are a schedule F farmer with children under age 18, make sure to pay them what they really earned this year,” Neiffer advises. “Children with no other income can earn about $6,000 this year tax-free (some states may require a little bit of tax) and the wages paid are completely deductible and even better, no payroll taxes are owed.” He says the child can take those earnings and contribute to a Roth IRA account. If your child puts $5,000 into a Roth IRA at age 17 and lets it compound for 48 years until age 65, it will grow to about $80,000.
Sell Some Grain Consider selling some grain on a deferred payment contract. “This gives you flexibility after year-end if you need to bring income into 2018,” Neiffer says.
Neiffer will help seminar attendees dissect tax planning during the 10:10 a.m. and 11:30 a.m. breakout sessions on Thursday.






 




Attend 2018 Top Producer Seminar
When: Jan. 23–26

Where: Hilton Chicago Hotel Downtown, 720 S. Michigan Ave.

What: Welcome reception on Tuesday, Jan. 24 with Trust In Food Symposium attendees; business education and Top Producer of the Year banquet on Wednesday, Jan. 25; “U.S. Farm Report” taping and business education with lunch keynote on Thursday, Jan. 26; and sponsor breakfast followed by two mainstage presentations on the global ag economy and event wrap-up at 10:30 a.m. on Friday, Jan. 27

More Info: To register for the seminar or for additional information, visit TopProducerSeminar.com.