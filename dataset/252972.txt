Indonesia seizes illegally logged wood from Papua
Indonesia seizes illegally logged wood from Papua

By STEPHEN WRIGHTAssociated Press
The Associated Press

JAKARTA, Indonesia




JAKARTA, Indonesia (AP) — Indonesian authorities foiled the shipment of 21 containers of prized ironwood from Papua, highlighting what environmentalists say is a rampant illegal trade in the country's easternmost region.
The Forestry and Environment Ministry's law enforcement agency said the wood from the Kaimana tropical forest was processed and ready for shipment to Surabaya when seized. The city has a major port and is a center of Indonesia's wood furniture manufacturing and export industry on the island of Java.
Greenpeace Indonesia said the confiscation, which was made Tuesday and announced Thursday, is small compared with long-standing smuggling from Papua where no "big actors" have been prosecuted. The region has Indonesia's largest remaining tropical forests and is seen by logging and palm oil companies as a new frontier for exploitation after the stripping of most of Java, Sumatra and Borneo of natural forests.
Indonesia was admitted in 2016 to an EU arrangement that makes it easier for Indonesian wood producers to export to the bloc if they've been certified by Indonesia's new Timber Verification and Legality System, known by its local acronym SVLK.
Some environmental and civil society groups have said the system, meant to provide certainty about the origin of wood, could easily become a conduit for illegal timber from a country where tropical forests have been cut down at an epic rate.
The ministry said investigations by police and its staff in Papua revealed a scheme for transporting and processing the wood and then shipping it once the desired quantity had been accumulated in warehouses at a West Papua port. Apparently falsified documents were to be used for the transport of the illegal wood.
It said police have arrested a suspect, who is a director of one of the companies involved, who could face up to 15 years in prison and a fine of up to $140,000.
Ironwood is prized for its beauty and strength and is used as flooring and in joinery.
Greenpeace Indonesia forests campaigner Charles Tawaru said weak supervision is one factor why the illegal timber trade continues to flourish in Papua and a significant amount of wood is shipped out without verification.
On the ground, oversight through the timber verification and legality system had weakened because of the absorption of the district forestry service into the larger provincial apparatus, he said.
Earlier this week, the U.K. ambassador to Indonesia, Moazzam Malik, said the SVLK system had provided a major boost for Indonesian wood exporters.
"The SVLK standard that Indonesia created, actually with an enormous U.K. investment over a 15-year timeframe, is something that's given Indonesian wood exporters access to the European market on a no restrictions basis," Malik said at a Jakarta Foreign Correspondents Club event.
"A world first, really a very major boost and that industry is responding really well," he said.