January exports of U.S. beef and pork were modestly higher than a year ago, but export value slipped for both products, according to data released by USDA and compiled by the U.S. Meat Export Federation (USMEF).Beef exports increased 3 percent from a year ago to 82,301 metric tons (mt), but value was down 13 percent to $438.1 million. Exports to most Asian markets, which were impacted early last year by the West Coast port labor impasse, increased in January, but these gains were largely offset by lower volumes shipped to Western Hemisphere markets and the Middle East. January exports accounted for 12 percent of total beef production and 9 percent for muscle cuts only (steady with January 2015). Export value per head of fed slaughter was $239.88, down 11 percent from a year ago.
Pork exports increased 4 percent from a year ago to 167,010 mt, but value fell 11 percent to $404.7 million. Exports to China were up significantly from last year's low volumes, reflecting recent reinstatement of several U.S. plants and continued strong demand for imported pork in China. Volumes also increased for Central and South America, the Caribbean and Oceania. January exports accounted for 22 percent of total pork production and 19 percent for muscle cuts only (up from 21 and 17 percent respectively last year). Export value per head slaughtered was $41.53, down 11 percent from a year ago.
Beef exports shows signs of rebound in Japan; Korea, Taiwan remain strong
Beef exports to Japan were the largest in six months at 16,762 mt, up 21 percent from a year ago, while export value edged 2 percent higher to $93.2 million. Exports to South Korea and Taiwan, which were bright spots for U.S. beef in 2015, were also above year-ago levels. Korea took 11,263 mt (+59 percent) valued at $67.2 million (+17 percent). Export volume to Taiwan was 2,890 mt (+35 percent) valued at $24.1 million (+3 percent). Led by a strong month in the Philippines, Vietnam and Indonesia, exports to the ASEAN region increased 71 percent in volume (1,638 mt) and 9 percent in value ($9.7 million). Exports to Hong Kong were up 19 percent (10,254 mt), although value declined by 16 percent ($58.4 million).
"Although it is encouraging to see beef exports to the Asian markets performing above year-ago levels, these results are a reminder of how disruptive the West Coast situation was for our industry," said USMEF President and CEO Philip Seng. "While we still face a tariff gap in Japan compared to Australian beef, Australia's recent slowdown in production presents an opportunity to reclaim market share - an opportunity the U.S. industry is pursuing very aggressively. U.S. beef is also capitalizing on the tight domestic supplies in Korea, making strides in both the retail and foodservice sectors."
Beef exports to Mexico were severely challenged in recent months by the weakening peso, and January exports were the lowest since May 2013 at 15,247 mt (-25 percent). Export value dropped 35 percent to $68.8 million. Exports were also significantly lower to Canada (9,144 mt, -11 percent, valued at $54.8 million, -26 percent) and Egypt (volume down 11 percent to 7,367 mt, value down 23 percent to $9.9 million). Central and South America were the bright spots in the Western Hemisphere, driven by growth to Chile (913 mt, +30 percent) and Guatemala (404 mt, +13 percent).
Pork highlights include China, Honduras, Dominican Republic
U.S. pork exports to China/Hong Kong maintained the stronger pace established in October, with January volume up 84 percent from a year ago to 32,609 mt and value increasing 50 percent to $64.2 million. Import data for China and Hong Kong show January was another record-breaking month, with combined volume from all suppliers reaching 224,077 mt, up 29 percent from last year.
"Having more pork plants and more product eligible for China is absolutely critical," Seng explained. "Last year China, Korea and Mexico were the major destinations with an increased need for imported pork. The U.S. industry capitalized on two of those situations, but the EU reaped most of the benefits in China. It's important that U.S. pork competes more vigorously in China in 2016."
Led by strong exports to Honduras and Guatemala, January pork exports to Central and South America increased 6 percent from a year ago in volume (8,970 mt) but fell 13 percent in value ($20.9 million). Exports to Honduras performed especially well, reaching 1,966 mt (+73 percent) valued at $3.5 million (+35 percent). This helped offset lower exports to Colombia.
Following a record year in 2015, pork exports to the Dominican Republic continued to shine in January, increasing 51 percent in volume (2,210 mt) and 26 percent in value ($4.5 million).
The recent rebound continued for pork exports to Oceania, with volumes to both Australia and New Zealand up sharply from the low totals posted in January 2015. Exports to the region more than doubled in volume (5,764 mt, +105 percent) and increased 36 percent in value to $15.2 million.
January exports slowed to leading markets Mexico and Japan. Following a record month in December and the fourth consecutive record year for Mexico, January volume was down 7 percent to 55,042 mt, while value fell 24 percent to $85.8 million. In leading value destination Japan, volume was down 14 percent to 29,835 mt and value declined 13 percent to $113.5 million.
Pork exports to Korea performed very well in 2015, but slowed in the second half. That trend continued in January, as exports fell 20 percent in volume (12,192 mt) and 41 percent in value ($30.5 million).
Lamb export value down despite sharp jump in volume
January exports of U.S. lamb were 35 percent above last year's low level at 971 mt, though value declined 13 percent to just over $1.5 million. Exports increased to Mexico and Canada, while Bermuda - once a top destination for U.S. lamb - also took its first significant volume in some time.
Complete January export totals for U.S. beef, pork and lamb are available from USMEF'sstatistics webpage.
More From AgDay:

<!--

    function delvePlayerCallback(playerId, eventName, data) {
        var id = "limelight_player_351368";
        if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
            DelvePlayer.registerPlayer(id);
        }
        switch (eventName) {
            case 'onPlayerLoad':
                var ad_url = 'http://oasc14008.247realmedia.com/RealMedia/ads/adstream_sx.ads/agweb.com/multimedia/prerolls/agwebradio/@x30';
                var encoded_ad_url = encodeURIComponent(ad_url);
                var encoded_ad_call = 'url=' + encoded_ad_url;
                DelvePlayer.doSetAd('preroll', 'Vast', encoded_ad_call);
                break;
        }
    }

//-->


<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->

Want more video news? Watch it on AgDay.