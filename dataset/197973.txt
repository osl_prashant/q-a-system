Crop calls
Corn: 1 to 2 cents higher
Soybeans: 1 to 3 cents higher
Wheat: 1 to 3 cents higher
Grain and soybean futures benefited from short-covering overnight and concerns about excessive rains across areas of the Corn Belt. Another round of showers is moving across the eastern Corn Belt this morning, meaning it would take several dry days before producers could return to the fields -- and there's more rain in the near-term forecast. Too-wet conditions on the Southern Plains also raises concerns about damage to the winter wheat crop. Meanwhile, this morning's weekly export sales data failed to impress the market. Soybean and wheat sales were within expectations and corn sales fell short of expectations. However, USDA also just announced an unknown destination has purchased 115,400 MT of old-crop U.S. corn.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Livestock futures are expected to see a choppy start as traders even positions ahead of the Memorial Day weekend, as well as prepare for tomorrow's Cattle on Feed Report. The report is expected to show On Feed at 100.8%, Placements at 106.8% and Marketings at 101.8% of year-ago levels. This week's cash cattle trade has begun roughly $2.50 lower than last week, but futures are trading well below the cash market. Meanwhile, the cash hog market has taken on a mixed tone this week amid reduced packer demand.