Farm Diesel

Farm diesel is unchanged on the week at $2.00.
Our appetite for risk is exhausted and we have advised subscribers fill remaining harvest farm diesel needs.
Distillate supplies were up 0.7 million barrels last week, now 5.6 million barrels below the year-ago peg.
Our farm diesel/heating oil futures spread remains at 0.38 which is a strong indication of pending near-term farm diesel price support.
Click here to view our official Inputs Monitor Farm Diesel advice.

Propane

Propane is also unchanged at $1.18 per gallon this week.
Nine of the states in our survey are unchanged this week.
Propane supplies firmed 1.380 million barrels on the week but remain 24.957 million barrels below year-ago.
Prepare to book harvest and winter propane needs very soon.

PERSPECTIVE -- As with our weekly fertilizer analysis, our fuels analysis has been light this week thanks to my duties with Crop Tour and this week's Farm Progress Show. So to keep you up to speed, you should be 100% filled on farm diesel for harvest. The impacts of Hurricane Harvey will likely soon be noted in higher fuels prices. Crude oil prices may fall as refineries are shut in and Texas tea backs up. But those backed up supplies will mean a slowdown in refining activity. That puts farm diesel at risk for a spike.
Again, get current to 100% filled on farm diesel for harvest.
On the propane side, we had viewed LP as less at risk from Hurricane Harvey than diesel, but this week, Iowa Secretary of Agriculture Bill Northey advised we proceed with caution on propane saying, "It is encouraging that propane inventories in the Midwest are within the five-year average and have steadily increased over the summer. But it is important for users to be prepared. Ensuring adequate supplies on hand now can help avoid any possible unforeseen spikes in demand later this year."
The communique went on with the following...
"Actions that farmers and other propane users can take now in order to prepare for this fall and winter include:
• Making sure propane supplies for grain drying, livestock facilities, homes and machine sheds were full going into the fall season.
• Take advantage of early buy/booking programs
• Consider expanding on-site capacity at facilities and homes
• Communicate early and regularly with propane suppliers"
We believe this to be sound advice and we expect to book propane at the beginning of next week. Get in touch with your preferred propane supplier to check availability and current bids. Be prepared to pull the trigger after the holiday weekend.





Fuel


8/14/17


8/21/17


Week-over Change


Current Week

Fuel



Farm Diesel


$1.94


$2.00


Unchanged


$2.00

Farm Diesel



LP


$1.16


$1.18


Unchanged


$1.18

LP