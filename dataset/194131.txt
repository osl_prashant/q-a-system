The tables show the differences we see for 2016-17 usage and lay out three scenarios — poor, average and excellent weather — for the 2017-18 marketing year.




Corn 

 


 
 

2015-16
			USDA 


2016-17
			USDA 


2016-17
			PF/Doane 


2017-18 USDA


2017-18
			PF/Doane



 Planted (mil. acres)

88.0


94.0


94.0


90.9


90.9



 Harvested (mil. acres)

80.8


86.7


86.7


83.5


83.1



 Yield (bu./acre)

168.4


174.6


174.6


169.5


166.0



 
 

million bushels



Beginning stocks

1,731


1,737


2,295


2,370


2,320



Production

13,602


15,148


15,148


14,158


13,795



Imports

68


55


55


50


45



Total Supply

15,401


16,940


16,940


16,573


16,160



 
 

 


 


 


 


 



Feed & Residual

5,113


5,425


5,450


5,450


5,525



Food, Seed, Industrial

6,650


6,920


6,920


7,000


6,940



   Ethanol for Fuel*

5,224


5,450


5,450


5,500


5,450



Total Domestic Use

11,763


12,345


12,370


12,450


12,465



Exports

1,901


2,225


2,250


1,850


1,825



Total Use

13,664 


14,570 


14,620 


14,300 


14,290



 
 

 


 


 


 


 



Carryover

1,737


2,370


2,320


2,273


1,870



Stocks-to-Use Ratio

12.7%


16.3%


15.9%


15.9%


13.1%



Proj. avg. price per bu.

$3.61


$3.35


$3.35


$3.30


$3.60




* 'Ethanol for Fuel' is included in the Food, Seed, Industrial total. 

 








Soybeans

 


 
 

2015-16
			USDA 


2016-17
			USDA 


2016-17
			PF/Doane 


2017-2018 USDA 


2017-18
			PF/Doane



 Planted (mil. acres)

82.7


83.4


83.4


89.5


90.0



 Harvested (mil. acres)

81.7


82.7


82.7


88.7


89.2



 Yield (bu./acre)

48.0


52.1


52.1


49.4


49.3



 
 

million bushels



Beginning stocks

191


197


197


370


380



Production

3,926


4,307


4,307


4,381


4,400



Imports

24


25


25


25


25



Total Supply

4,140


4,528


4,528


4,776


4,805



 
 

 


 


 


 


 



Crush

1,886


1,890


1,890


1,940


1,925



Exports

1,942


2,150


2,150


2,225


2,175



Seed

97


104


104


101


102



Residual

18


14


4


35


33



Total Use

3,944


4,158


4,148


4,301


4,235



 
 

 


 


 


 


 



Carryover

197


370


380


475


570



Stocks-to-Use Ratio

5.0%


8.9%


9.2%


11.0%


13.5%



Proj. avg. price per bu.

$8.95


$9.50


$9.50


$9.30


$9.00




 








Wheat

 


 
 

2015-16
			USDA 


2016-17
			USDA 


2016-17
			PF/Doane 


2017-2018 USDA 


2017-18
			PF/Doane



 Planted (mil. acres)

55.0


50.2


50.2


45.7


45.7



 Harvested (mil. acres)

47.3


43.9


43.9


38.1


37.9



 Yield (bu./acre)

43.6


52.6


52.6


45.6


45.6



 
 

million bushels



Beginning stocks

752


976


976


1,184


1,184



Production

2,062


2,310


2,310


1,739


1,729



Imports

113


118


118


150


145



Total Supply

2,927


3,403


3,402


3,074


3,058



 
 

 


 


 


 


 



Food

957


949


949


950


950



Seed

67


61


61


66


68



Feed

149


154


154


150


145



Total Domestic Use

1,174


1,164


1,164


1,166


1,163



Exports

778


1,055


1,055


975


985



Total Use

1,951


2,219


2,219


2,141


2,148



 
 

 


 


 


 


 



Carryover

976


1,184


1,184


933


910



Stocks-to-Use Ratio

50.0%


53.4%


53.4%


43.6%


42.4%



Proj. avg. price per bu.

$4.89


$3.89


$3.89


$4.80


$4.80




 

 








Cotton

 


 
 

2015-16
			USDA 


2016-17
			USDA 


2016-17
			PF/Doane 


2017-18
			USDA 


2017-18
			PF/Doane



 Planted (mil. acres)

8.58


10.07


10.07


12.06


12.60



 Harvested (mil. acres)

8.07


9.51


9.51


11.18


11.60



 Yield (lb./acre)

766


867


867


816


886



 
 

million bales



Beginning stocks

3.65


3.80


3.80


2.80


2.80



Production

12.89


17.17


17.17


20.55


21.40



Imports

0.03


0.01


0.01


0.01


0.01



Total Supply

16.57


20.98


20.98


22.36


24.20



 
 

 


 


 


 


 



Domestic use

3.45


3.25


3.25


3.35


3.40



Exports

9.15


14.91


14.92


14.20


14.50



Total Use

12.60


18.17


18.17


17.55


17.90



 
 

 


 


 


 


 



Carryover

3.80


2.80


2.80


5.80 


6.25



Stocks-to-Use Ratio

30.2%


15.4%


15.4%


33.0%


34.9%



Proj. avg. price per bu. (cents/lb.)

61.20


68.00


68.00


61.00


60.00