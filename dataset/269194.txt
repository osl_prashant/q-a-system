BC-NM--New Mexico News Digest, NM
BC-NM--New Mexico News Digest, NM

The Associated Press



Good afternoon. Here's a look at how AP's general news coverage is shaping up today in New Mexico. Questions about today's coverage plans are welcome, and should be directed to 505-822-9022 or apalbuquerque@ap.org
This information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date. All times are Mountain.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
TOP STORIES
FILM INDUSTRY-GOVERNOR'S RACE
SANTA FE, N.M. — Officials in New Mexico's film and television industry say a new governor may help boost business for film production in the state. SENT: 490 words.
TRUMP-BORDER WALL-RIO GRANDE
ROMA, Texas — Stymied by Congress and the courts, President Donald Trump has struggled to make good on his signature campaign promises to build a wall and stop migrants. But there is at least one place where his vision is becoming reality: the sinuous lower Rio Grande Valley, scene of more unauthorized crossings than any other stretch between the Pacific Ocean and the Gulf of Mexico. By Nomaan Merchant and John L. Mone. SENT: 1,100 words, photos.
ALSO:
— WILDFIRES-NEW MEXICO-THE LATEST: A top-level management team on Saturday assumed command over crews battling two nearby wildfires after high winds Friday caused the fires to grow to a combined 8 square miles (2,103 hectares) on part of the Cibola National Forest in northwestern New Mexico.
— UNM HOSPITALS-CEO: A series of town halls are planned over the coming weeks as one of the state's largest hospital systems seeks a new chief executive officer.
— POWER PLANT-SILO FAILURE: The investigation into the cause of a structural failure of a coal silo at a power plant in northwestern New Mexico is ongoing, but the plant's operator says repairs are underway.
— PECAN TARIFFS: New Mexico's two U.S. senators are concerned that tariffs implemented by the Chinese government on dozens of American product lines, including pecans, could hurt growers in the state.
— BIKE RACE-TRAFFIC: The state Department of Transportation advises drivers in southwestern New Mexico to be on the watch for cyclists and to expect intermittent road closures in the coming week due to a five-day cycling event.
--------
SPORTS:
MMA--UFC-BASE-BRAWL
The fights seemed more at home inside an octagon than a diamond. Baseball showed some moxie this week with a pair of bench-clearing melees that had players swinging and settling scores. A UFC fighter offers some tips to the baseball brawlers. By Dan Gelston. SENT: 750 words, photos.
BBN--ROCKIES-NATIONALS
WASHINGTON — The Rockies send Jon Gray to the mound while Max Scherzer starts for the Nationals. UPCOMNG: 650 words, photos. Game starts at 1:05 p.m. EDT.
___
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.