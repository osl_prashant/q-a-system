Forty years ago in the lobby of the Peabody Hotel in Memphis, Tenn., two regional associations agreed to join together and form what has become the National Alliance of Independent Crop Consultants (NAICC). The NAICC held its annual convention Jan. 13 to Jan. 20, and keeping with one of its traditions—a well-attended conference—82% of its membership attended.
“In 1978, people got together and shared their vision to address the problems they saw at that time,” says outgoing NAICC president Steve Hoffman. “Those were state and federal regulations, lack of understanding between environmental groups and a voice in Washington, D.C. We have a lot of the same issues today. The potential to solve any problem in agriculture today exists when NAICC members come together.” 
Accomplishments Of A Term. In the past year with Hoffman’s leadership, NAICC focused its efforts on three areas: addressing governmental regulations, building NAICC membership and pride and advocating for agriculture.
Hoffman commended NAICC members for their role in the government’s rollback of regulations on neonics and synthetic pyrethroids. He encourages members to continue to be active and make comments on regulatory websites. Additionally, the white paper “Application of U.S. EPA GLP Terminology for Selected Studies on Genetically Engineered Crops” was approved by the EPA. Hoffman says this success means everyone has agreed to the terms and can be on the same page to eliminate confusion.
“It’s the framework of the NAICC that allows for this common interest to get done,” he says. “And it’s still very important as we see monarch butterflies might be listed as an endangered species, and the biggest topic in policy in 2018 may be WPS,” the worker protection standard.
Last year, Hoffman started a campaign called “NAICC Pride.” It provides NAICC decals in the registration packets for all members to help showcase their membership on their vehicles and at their offices.
In the past year, NAICC has added 74 new members. In all, its membership is composed of independent researchers, consultants and quality assurance professionals.
NAICC Names Consultants Of The Year

NAICC Recognizes Charter Members

Service to NAICC Award Presented

NAICC Presents Service to Agriculture Award
Technology As A Tool. At the 2018 annual conference, which had 723 attendees and set a new attendance record, the keynote and general session speakers focused on tools for communicating about agriculture and technology.



In the keynote address, Robert Saik, founder of Agri-Trend, challenged attendees to continue advocating for agriculture, technology and the role both play in providing for people on this planet.
“I believe the nonscience movement is eroding the principles we have,” Saik says. “Why should we allow people to rip technology out of our hands? There are big-picture technology decisions made based on reactionary,” he says. On this topic, he assigns the acronym FUD: Fear, Uncertainty, Doubt.
“There is $2.5 billion spent annually to create FUD via 300 organizations,” he says. “And GMO isn’t an ingredient. It’s a breeding process. We need to stop using the term GMO—start saying genetically engineered. Would you rather drive on a modified bridge or an engineered bridge?” he says.
To take action on the initiative for advocacy, two sessions focused on training with communications.
Amy Hays from the Noble Research Institute provided perspective on how to communicate with people from different generations. She says generational intelligence helps us understand how people in any generation learn and make decisions. “This isn’t about their age; it’s about how they operate,” Hays says.
Timothy Pastoor built on Hays’ generational intelligence with his ABC approach—awareness, bridge, content—to communication.
First, awareness allows you to assess the situation and whether the person with which you are engaging is of a similar position or an adversarial position. Once the person’s position is identified, you can use a statement to bridge into something that is more helpful. And for content, he admits that as a scientist, he starts to talk about facts, but once you learn more about the person and how to address his or her question, you’ll generate a more positive experience.
Timely Topics. NAICC programming also featured sessions on crop protection technologies, big data, new products and regulations.
During a panel discussion on dicamba, Kevin Bradley from the University of Missouri and consultants shared their takeaways from in-field experiences and how to move forward into 2018. Another session focused on spray drift, how adjuvants play a role in effective application passes and ongoing research into new nozzle designs.
With boots on the ground and walking fields side-by-side with farmers, as Hoffman shares with NAICC members, “What you do really matters.”