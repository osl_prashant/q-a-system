The newest revolution in medication delivery systems has hit the farm gate at the 2018 Iowa Pork Congress.
Anna McGeehan, Swine Marketing Manager at Neogen Corporation and David Edwards, CEO and founder of automed® discuss what you need to know about this unparalleled automatic system that is ergonomic, highly accurate at top speeds and records dosing data at the time of injection.
For more information about automed visit: http://animalsafety.neogen.com/en/automed
Sponsored by Neogen