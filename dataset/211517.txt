Potato harvest has hit full stride in Colorado’s San Luis Valley, and at least one grower-shipper thinks the 2017 season will be a good one.“The stars seem to be aligning pretty good for a decent market this year,” said Jed Ellithorpe, marketing director for Center, Colo.-based Aspen Produce LLC.
“It certainly won’t be a wild, high market, but at least we’ll have some profit on the farms this year.”
The “stars” Ellithorpe referred to include a high quality crop, strong pricing, the continued emergence of organic potatoes in the market and a “buy local” program at the retail level.
“Prices are really good at the moment, and I expect them to stay there with the major reduction in acres in fresh supply in Idaho, which puts Colorado in a great position,” he said Sept. 11.
As of Sept. 18, prices for 50-pound cartons of russet potatoes from the San Luis Valley were $12 for size 40-90s, with 100s at $10, according to the U.S. Department of Agriculture. Year-ago prices were mostly $10 for size 40-70s and mostly $9 for size 80-100s.
Jim Ehrlich, executive director for the Monte Vista, Colo.-based Colorado Potato Administrative Committee, said the state had planted 51,900 acres of potatoes, an increase of about 800 acres from 2016.
The average acre yield is 390 cwt., he said.
“I talked with one grower who was about to harvest, and their tests came back at slightly more than 500 sacks per acre,” Ehrlich said.
 
Rise of organics
Ellithorpe said organic plantings had added 1,500 to 2,000 acres in 2017.
“We definitely have a movement to do more than (what we’re doing),” Ehrlich said, even with the the three years it takes to transition land from conventional to organic production.
“The demand for organics is there.”
Ellithorpe agreed.
“The game changer for the organics is going to be Whole Foods,” he said.
“They seem to be really aggressive on their pricing, and I think that will keep the demand for organics pretty high.”
Although his company cut back some organic acres, Les Alderete, general manager for Center-based Skyline Potato Co., said about 15% of his crop is organic.
Jamey Higham, CEO and president for Monte Vista, Colo.-based Farm Fresh Direct of America, said his organic acreage accounts for about 10% of his production.
Aside from organics, this year retailers are using a new potato label promoting Colorado-grown product, Ellithorpe said.
“I’m curious to see if consumers are willing to convert to that and support local produce or if they are price-conscientious,” he said.
“Retailers are showing an interest in promoting locally grown, but the data will say if it’s a consumer demand thing or not.” 
 
Cautious optimism
Other growers, speaking before the harvest got underway in earnest, were more cautiously optimistic about this year’s San Luis potato crop.
Sheldon Rockey, partner with Center-based Rockey Farms, began harvesting fingerlings after Labor Day.
“The weather started off good, then it cooled off and was cloudy in July with a lot of rain,” he said. “It’s going to be average again, but it’s hard to say until we get into harvest.”
Rockey said his company grows 25 varieties of potatoes, but it’s all for certified seed lots.  
Alderete of Skyline Potato said on Aug. 29 that quality looked good with decent size in the digs they had done.
“It should be a normal crop,” he said.
“I don’t think you’re going to see any huge yields because we had two or three weeks of overcast, and it delayed things a couple of weeks.”
Skyline grows norkotah, canela and centennial russet varieties among others on the conventional side. 
The company grows red and russet organic potatoes, with the season running from October to April.
“The color and quality on the reds look good,” Alderete said.
Farm Fresh Direct of America began harvesting russets and yellows in mid-August, Higham said.
“The quality looks great, but the sizing is not as large as we’d like it to be,” he said.
“I think the sizing across the board is smaller, but we’re not unhappy with it.”
He said companies won’t know a lot until their crops are harvested and in storage.
“It’s never as good or bad as you think it is,” Higham said