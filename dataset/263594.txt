National editor Tom Karst and staff writer Ashley Nickle talk about the latest 90-day delay granted to produce shippers regarding the electronic logging devices mandate and discuss how blueberries fared in some recent cold weather.
MORE: UPDATED: Produce haulers get another 90-day electronic logging waiver
MORE: Cold clips Georgia blueberries