The Georgia Organics Conference is celebrating its 20th anniversary. 
"It started out as less of a conference, and more of a meeting of the minds," said James Carr, communications coordinator for the Feb. 17-18 event at the Georgia International Convention Center in Atlanta.
Organizers expect 1,000 attendees, a far cry from the "forty or so" people Carr said attended the first show. 
"Twenty years ago it was a very small gathering. It started as a team of farmers, it now includes chefs, educators, producers, consumers and everyone in the middle," he said. 
Author Barbara Brown Taylor will be a keynote speaker at the Georgia Organics Conference and Expo. Taylor runs Indian Ridge Organic Farm, Clarksville, Tenn., with her husband and has written three memoirs about their life on the farm. 
The convention feature a Farmers Feast, with local, organic food, the awarding of the Land Steward Award and the Barbara Petit Pollinator Award, and a fruit tree market.
The conference is Feb. 17-18. Registration is available at http://conference.georgiaorganics.org/ .