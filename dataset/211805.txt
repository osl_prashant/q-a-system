As Hurricane Harvey relief efforts began to gain traction, some retail stores were reopening to big business in Houston Aug. 29 after four days of storms and flooding wracked the city. 
On Aug. 28, 87 Walmart locations and distribution centers at least 30 H-E-B stores had closed because of flooding caused by Hurricane Harvey. Many of those were reopening by Aug. 29, though many had limited hours of operation.
 
The Houston Food Bank remained closed as of Aug. 29 because flood waters made the facility inaccessible, but the group is asking for donations on its website to help feed people displaced by the storm.
 
H-E-B informed consumers of stores that were open on its Facebook page, with most stores operating from 9 a.m. to 3 p.m.
 
Wal-Mart had a dedicated page on its website about store status and hurricane relief.
 
A spokesperson in the produce department at a Whole Foods Market on 2955 Kirby, Houston, Texas, said lines were out the door at the store. The store was scheduled to close at 5 p.m., she said.
 
Supermarkets find overwhelming demand when they open, said John Kreger, director of sourcing and industry partnership with the north Houston Montgomery County Food Bank, with shoppers snapping up water, bread and shelf-stable items. The Montgomery County Food Bank is distributing hygiene kits and food to a major shelter in Montgomery County, he said.
 
Apples and oranges are among the food being delivered to storm victims, he said. “I’m been very pleased in the way the produce industry has reached out and offered their support, so it has been nice to get those phone calls and e-mails,” Kreger said. The food bank is looking for apples, citrus and other commodities that can be eaten readily.
 
“A lot of damage was done and relief efforts will go on for months, but getting people out of harm’s way is the first step,” Kreger said.
 
Glenmont-based New York Apple Sales and five of its growers in the Hudson Valley are donating a truckload of fresh apples to the San Antonio Food Bank this week, said Michael Harwood, sales representative for the company. The load, with transportation costs covered half by a freight company and half by New York Apple Sales, is expected to leave for Texas Aug. 31. Harwood said the company checked with the Houston Food Bank but high waters prevented scheduling a delivery there.
 
“We were inspired by all H-E-B and all they do for their customers,” he said. New York Apple Sales is a longtime supplier of H-E-B and wanted to support the hurricane relief efforts, he said.
 
Other relief efforts announced by the food and produce industry include:
Wal-Mart and the Wal-Mart Foundation pledge at least $1 million to hurricane relief;
H-E-B said it will donate $100,000 toward Hurricane Harvey relief efforts;
Supermarket partners Ralphs Grocery Co. and Food 4 Less Stores said they will accept donations from customers through September 9 to help victims. In addition, parent company, The Kroger Co., has made a $100,000 commitment to the Houston Food Bank. For every social share of the #KrogerCares post, the release said the Kroger Co. Foundation will donate $5 to the Food Bank, up to $100,000, to supply operational support and meals to families affected by the floods. 
The “Hurricane Harvey Produce Industry Relief Fund” was started by Brent Erenwert, president of Brothers Produce, Houston;
Bebo Distributing, Pharr, Texas, is collecting food and clothing supplies trucking them to relief efforts in Corpus Christi and Houston; 
West Des Moines-based Hy-Vee Inc. said it is accepting donations, matching donations from August 30 to September 30, up to $100,000. Funds will be provided to the American Red Cross to help with relief efforts, according to a news release;  and
The Fresh Produce & Floral Council’s board of directors has authorized a $10,000 donation to the American Red Cross relief effort for Hurricane Harvey, according to a news release.