How you and your employees manage your parlor, including your milking herd, can have a profound impact on the profit that you can achieve through your parlor.
Cows that are handled calmly, and prepped well will respond positively in the parlor in both milk production and animal behavior, enhancing the milking experience for both the cows and employees.
Through this program, dairy farm owners and managers, and dairy industry professionals will learn about the latest research and strategies to improve parlor performance with employee education and animal behavior in mind.
Topics include the impact of milking practices on bimodal and over milking, developing milking protocols, employee training, animal handling, parlor efficiency, holding pen management, mastitis identification, on-farm milk culturing, 2X vs 3X milking, and how these impact profit.
Presenters: Ron Erskine, Paola Bacigalupo, Marianne Buza, Faith Cullens, Phil Durst, Kathy Lee, Martin Mangual and Stan Moore
Dates and Locations:

January 23 -  St. Johns (Agro-Liquid, 3055 M-21)
January 24 - Hillsdale (Hillsdale County MSU Extension, 20 Care Drive)
February 13 - Hamilton (The Trestle Stop, 3366 M-40)
February 14 - Falmouth (Falmouth Community Center, 219 E Prosper Rd)
February 20 - Bad Axe (Franklin Inn, 1070 E. Huron Avenue)
February 21 - West Branch (Forward Conference Center, 2980 Cook Road)

All programs run from 9:30 a.m. - 3 p.m. with registration beginning at 9 a.m.
View the full registration details at https://events.anr.msu.edu/ParlorProfit18/ 
Registration Fee: $35 for the first person, $15 for each additional person from the same farm (includes lunch and educational materials).
For more details or questions about registration, please contact Gaye Rider at 231-533-8818 or riderga@anr.msu.edu or your area MSU Extension Dairy Educator.