$2125 PER ACRE Alfalfa County, OK
June 8: 160 acres east of Aline. 157.7 acres tillable with 124.2 acre wheat base and 30 bushel PLC yield. Irrigated cropland with 10 water wells, 6 currently in service, and 5 wire perimeter fence. Includes Reinke 8 tower irrigation system. Soil types include Eda Sand, Devol fine sandy loam and Carmen & Ruella soils. Jeff Crissup, Wiggins Auctioneers LLC, Enid, OK 580-233-3066.

The inclusion of a functioning irrigation system added value to this tract. The following is a similar tract with fewer tillable acres, but includes wooded hunting and recreation areas along with a slightly higher yield rating.

 
$2493.75 PER ACRE Garfield County, OK
June 7: 160 acres 1 mile south of Carrier, OK. 106.2 acres tillable with a 104.4 acre wheat base and PLC yield of 34 bushels per acre. The balance of the land is in grass pasture, timber lined creeks with pond development potential. Vicki Wiggins Allenm, Wiggins Auctioneers LLC, Enid, OK 580-233-3066.

Our final land sale brings us back to Alfalfa County, OK for another 160 acre tract of farmland. The higher yield rating is offset partially by grass pasture and by trees and creek not noted to be suitable for hunting and recreation, but, rather, for livestock. That may account for the sharply lower price per acre compared to the above which included hunting and recreation wooded areas in the seller's description.

 
$1550 PER ACRE Alfalfa County, OK
June 2: 160 acres south of Cherokee, OK. 103.1 tillable acres with 101.3 acre wheat base and a PLC yield of 36 bushels per acre. Soil types include Reinach very fine sandy loam, and Yahola fine sandy loam. The balance is in grass pasture, scattered trees and creek. Perry Wiggins, Wiggins Auctioneers LLC, Enid, OK 580-233-3066.
 
Our first featured land sale was for a very straightforward tract of irrigated cropland, not advertised to contain trees, creek or other recreational opportunities. Our second sale "has it all" with highly productive cropland and wooded recreational land. Our third land sale demanded 37% less despite having a similar number of tillable acres. In the case of these three sales, high quality land with wooded hunting and recreation areas attached demanded a premium which outstripped even high quality irrigated farmland without trees, creek or recreational areas.
All three were from around the same geographic area, contained 160 total acres and included at least 100 acres of tillable farmground. From these three sales, it appears Oklahoma buyers value farmland attached to wooded hunting areas above land that includes existing irrigation infrastructure and a high productivity rating.