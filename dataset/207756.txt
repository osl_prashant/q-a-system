They are still trying to change drop “Fresh” from the Fresh Fruit and Vegetable Program. 
House Resolution 3402 would amend the fresh fruit and vegetable program under the Richard B. Russell National School Lunch Act to include canned, dried, frozen, or pureed fruits and vegetables.
 
The bill was introduced in late July by Rep. Bruce Poliquin, R-Maine, The bill’s cosponsors include Rep. Kurt Schrader, D-Ore, Rep. Ron Kind, D-Wis., Rep. Jaime Herrera Beutler, R-Wash., Rep. John Garamendi, D-Calif., Rep. Timothy Walz, D-Minn., Rep. Todd Rokita, R-Ind., Rep. Luke Messer, R-Ind., Rep. David Valadao, R-Calif.
 
In a news release from his office, Poliquin called the legislation the Fruit and Vegetable Access for Children Act, a bill the release says “will give our children greater access to healthy fruits and vegetables in their school cafeterias.”
 
The release said the bill, which has four Democratic and four Republican original cosponsors, will allow schools to have the option to use the money from U.S. Department of Agriculture’s (USDA) Fresh Fruit and Vegetable Program (FFVP) to purchase additional forms of fruits and vegetables—fresh, frozen, canned, pureed, and dried—expanding the number of nutritious food options for schoolchildren.
 
“Our children should have greater access and more nutritious choices when it comes to the foods they eat at their school cafeterias,” Poliquin said in the release. “As a parent, I believe it’s very important for our kids to have the opportunity to eat healthy fruits and vegetables, such as Maine wild blueberries, all school year round. I’m proud to join with my Democratic and Republican colleagues in pushing this bipartisan legislation forward.”
 
While those who argue for inclusion of processed fruits and vegetables cite the flexibility it will give schools, the purpose of the program is to expose kids to fresh fruits and vegetables. Don’t get sidetracked giving kids frozen okra and prunes.
 
Since starting as a modest pilot program in four states in the 2002 farm bill, the Fresh Fruit and Vegetable Program has since grown to serve kids in all 50 states, the District of Columbia, Guam, Puerto Rico and the Virgin Islands. Funding has climbed to more than $170 million annually.
 
Let’s hope industry lobbyists - now missing the determined passion of Lorelei DiSogra from their arsenal  - can again beat back this familiar attack on the Fresh Fruit and Vegetable Program. There is no reason to change what has been a very successful fresh formula.