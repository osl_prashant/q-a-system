Consumers will see more options in the Kiss melon line on store shelves this season, as Savor Fresh Farms adds production of the Snow Kiss melon.
The company is also growing more Sugar Kiss melons, according to a news release, and organic melons in the line.
The company grows hundreds of melons in test trials each year, as well as experiments with different growing conditions to find varieties will deliver flavors consumers want, according to a news release.
Those trials led to the addition of the Snow Kiss Melon, which has white exterior and flesh.
“We found Snow Kiss’ flavor to be sweet, but not overpowering,” Savor Fresh Farms President Milas Russell III said in the release. “Texture at first bite is semi-crisp with a subtle crunch, but the melon soon dissolves on the palate, providing a delectable eating experience.”
Consumers have requested organic Kiss melons, but the company wanted “to make sure that all the right components were in place,” Russell said in the release.
The company has a direct line to consumer preferences, allowing Savor Fresh Farms to adjust variety selection and growing processes, according to the release.
“Consumer demand for unique flavor experiences is driving growth in many produce categories and with the Kiss Melon line we are striving to add depth to our customers’ melon categories,” Patrinka Crammone, retail program director, said in the release.
Kiss melon harvest begins the first week of May and lasts through October in the U.S., and transitions to Australia over the winter.