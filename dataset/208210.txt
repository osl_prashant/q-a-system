On their Seymour, Ill., corn and soybean farm, the Klein family works each day to make sure their operation is running efficiently while keeping their goals in mind. With numerous family members and employees, it can sometimes be tricky.“We have our areas of expertise, but the biggest challenge is communication,” says Deb Klein, matriarch of the family. “We make decisions on the run but don’t always communicate them back with the group; it’s not contentious, there’s just so much going on.”
 
One of the biggest challenges of working with family is you know them as family first—all their flaws, history and tendencies. With that in mind, it’s often difficult to treat them as a professional colleague. 
Set communication ground rules.
First understand you need to draw a clear line between work and home life. When it’s time to talk work, walk work, but when it’s time to be with family keep work separate.
“Strive to keep it professional—talk to them like you would other people,” says Carolyn Rodenberg with Alternatives to Conflict, LLC. She even advises leaving family titles such as “mom” and “dad” at home and opting for first names instead. “It’s tough but it’s amazing what it does—it’s no longer parent-child, it’s a business relationship.”
Define and respect farm roles.
Clearly defined roles help with daily efficiency and success in the Klein operation. For example, Deb handles bookwork, Joe makes most agronomy decisions and Paul handles the operational and logistics side.
“Particularly with family it’s easy for a lot of things to be taken for granted and to assume people are on the same page as you,” says Rena Striegel, president of Transition Point Business Advisors. “First make sure you have a structure for holding people accountable for their responsibilities. Determine who has ownership of what part of the operation and let them own it. Don’t undermine their authority.”
Once roles and communication strategies have been defined, check in with family regularly—at least once a week is best, according to experts.
Work toward a common goal.
“Moving toward a common goal gives you traction,” Striegel says. “You can be confident each family member’s decisions will be strategic toward reaching that goal. Consider quarterly meetings with the whole farming family to set and evaluate short- and long-term goals for the farm.”
Have a plan for your meetings. At quarterly meetings focus on the big goals, and weekly discuss short-term goals.
 “Bring an agenda, set a time limit and make it a routine,” Rodenberg adds. One meet can help avoid misunderstandings.