The Greeley, Colo.-based National Onion Association recently completed a fall promotion focusing on recipe ideas, said Kim Reddin, spokeswoman for the association.
“We did a feature page with Family Features that featured the hot consumer recipe trends, which is sheet pan suppers,” Reddin said.
The idea is in line with a consumer trend to roast combinations of vegetables, Reddin said.
“We developed three recipes with the Idaho-Eastern Oregon Onion Committee, and they were all sheet pan recipes,” she said.
Family Features is an editorial syndicate that provides content for newspapers, magazines and websites.
“It’s a great way to distribute (usage information),” Reddin said.
“These feature pages are created specifically for the cover of a food section of a newspaper, both online and in print.”
The association also had a radio media tour with Kate Winslow and Guy Ambrosino, authors of “Onions Etcetera: The Essential Allium Cookbook.”
The group also is completing a kit for retail dietitians, which will be ready in January, Reddin said.
“The toolkit really is geared to provide dietitians with the toolbox for them to use when they’re meeting with somebody with dietary needs or a medical necessity and they have to change the way they shop and eat,” Reddin said.
“This provides them with resources when they’re talking with consumers.”