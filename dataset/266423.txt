BC-IA--Iowa News Digest 1:30 pm, IA
BC-IA--Iowa News Digest 1:30 pm, IA

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Iowa. Questions about coverage plans are welcome, and should be directed to the Des Moines Bureau at 515-243-3281 or apdesmoines@ap.org. Iowa News Editor Scott McFetridge can also be reached at 515-243-3281 or smcfetridge@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
TOP STORIES:
CROP REPORT
DES MOINES — Corn has been dethroned as the king of crops as farmers report they intend to plant more soybeans than corn for the first time in 35 years. By David Pitt. SENT: 130 words. UPCOMING: 450 words by 3 p.m.
STATE GOVERNMENT AND POLITICS:
XGR--MENTAL HEALTH-IOWA
DES MOINES — A new law is expanding access to mental health services across Iowa. By Scott Stewart. SENT: 130 words. UPCOMING: 400 words.
AROUND THE STATE:
OLD STUDENT LOANS — An Iowa agency is making a new effort to recover more than $9.4 million in defaulted private student loans, including some that date back to the 1990s. SENT: 130 words. UPCOMING: 275 words.
IN BRIEF:
FATAL SHOCK-CEDAR RAPIDS — Authorities say a man died after suffering an electric shock at a Cedar Rapids mobile home park.
TEEN GIRL DIES — Court records say the adoptive grandmother of a central Iowa teenage girl found starved to death has reached a plea deal.
GIRL DROWNS-SIOUX FALLS — A 2-year-old review of Falls Park that Sioux Falls officials used to defend safety protocol at the park where a 5-year-old girl drowned on March 18 was actually a training exercise.
BABY DEATH-CHARGES — An Ankeny man whose baby died after becoming wedged between a mattress and a wall has been given two years of probation.
PRISON GUARD-HEAD BUTTED — Officials say an inmate head-butted a guard at the Iowa State Penitentiary in Fort Madison.
SPORTS:
BKC--DRAKE-DEVRIES
DES MOINES — Drake has hired longtime Creighton assistant Darian DeVries as its basketball coach. By Luke Meredith. SENT: 300 words.
___
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.