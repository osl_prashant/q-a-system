In the midst of rumors and anti-GMO rhetoric, one researcher is striving to use genetic modification to improve crop health as well as potentially save consumer lives. Aspergillus, which creates carcinogenic aflatoxin, can now be controlled through genetic modification.Aflatoxins are found in corn, peanuts, cottonseed, milk, walnuts, pistachios and Brazil nuts. In the U.S., farmers can be turned away for having as few as 20 parts per billion of the toxin, or about 40 highly contaminated kernels in a bushel of corn, according to the University of Kentucky. One part per million is the equivalent of one second in 32 years. It doesn’t take much to be turned away.
“This [the new GMO corn] will make a difference in the U.S., but it will make the biggest difference in the undeveloped world,” says Monica Schmidt, University of Arizona researcher. “There they don’t test corn [for aflatoxins] and about 4.5 billion people consume the toxin.”



Aspergillus Ear and Kernel Rot




People in undeveloped countries are 16 to 32 times more likely to develop cancer, namely liver cancer, she adds. In addition, aflatoxins can stunt children’s growth and damage immune systems. The new GMO corn shuts down the toxin and doesn’t allow it to accumulate in the corn kernel.
Using RNAi technology, Schmidt and her team silence the aflatoxin genes in aspergillus fungi in the corn’s kernels. The ear could still show the greenish, powdery fungal spores on the surface; however, aflatoxin should not enter the kernels. 
“We’re looking forward to field testing,” Schmidt says. “For the past year and a half we’ve tested with controls from 1,000 to 100,000 parts per billion in greenhouses.”
The next steps for this potential product are tricky. The team in Arizona can keep testing on a small scale, but if they want to see results on more acres or see this product commercialized they’ll have to partner with someone who has deep pockets. This is a GMO product, so there are a number of regulatory hurdles the product must cross, and that takes time and millions of dollars.
“We’re seeking partners now,” Schmidt says. “If the regulatory pathway stays the same as it is now and funding becomes available we could realistically see this in corn in 10 to 20 years.”
It’s easy to get this gene into existing or new products—it’s a single insertion, she adds. So if and when the product does get approved it’ll be simple for U.S. and foreign corn hybrids to take advantage of the trait.
While this product can have positive financial impact in the U.S., Schmidt is most excited for what it will do in undeveloped countries. “It can save lives, I’d love to see it go to Africa,” she says.