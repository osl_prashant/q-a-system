New Hampshire-based Windham Packaging LLC has filed a complaint with the International Trade Commission, alleging that several produce marketers violated its patents with imported produce in micro-perforated film packaging.
Windham Packaging is asking the commission to halt the imports of the products, and “impose a bond” on those items during a 60-day review period.
Windham Packaging names these companies in the complaint:

Alpine Fresh Inc., Miami, Fla.;
Apio Inc., Guadalupe, Calif.;
B&G Foods North America, Parsippany, N.J.;
Glory Foods Inc., Columbus, Ohio;
Mann Packing Co. Inc., Salinas, Calif.; and 
Taylor Farms California, Salinas.

According to Windham Packaging’s website, the company’s packaging materials are micro-perforated using a carbon dioxide laser. The website said owner Elizabeth Varriano Marston holds patents on Registered Laser Micro-perforation, and the process is licensed to plastic converters throughout the U.S.
“Our modified atmosphere packages for fresh produce is customized by varying micro-perforation size and number, taking into consideration respiration rate, package weight, volume, and anticipated distribution temperatures,” according to the website. Proper selection and placement of micro-perforations, according to the website, encourages development of an optimum atmosphere inside the package, controlling oxygen, carbon dioxide, and moisture levels to extend product life.
The ITC said it would consider comments from the companies named in the complaint, other interested parties and members of the public through Nov. 21, after which the ITC will issue a decision.
The legal counsel for Windham Packaging would not comment. Representatives for the companies said they were not prepared to comment or did not return messages.