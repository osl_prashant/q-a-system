Public hearing to be held over South Dakota turkey barn
Public hearing to be held over South Dakota turkey barn

The Associated Press

IPSWICH, S.D.




IPSWICH, S.D. (AP) — A public hearing will be held after residents expressed opposition to the site of a proposed 5,000-hen turkey barn.
The Edmunds County Commission approved permits for Farm Holdings S.D. 12's three turkey barns in February. County Commissioner Tim Thomas said only the barn located 2 miles (3 kilometers) southeast of Ipswich has been met with strong opposition.
Annette Jones, county planning and zoning officer, told Aberdeen American News that 61 residents have signed a petition calling for county commissioners to retract Farm Holdings' building permit. Farm Holdings S.D. 12 is a limited liability company owned by Hendrix Genetics.
Some opposing residents worry about odor and reduced property values.
Thomas hopes residents and the company can reach a compromise during Monday's public hearing.
Matt McCready, a spokesman for Hendrix Genetics, said the barn would house about 5,000 hens while the hens lay eggs.
"With this new design (of the barns) we are able to control the internal environment to minimize the odors that could have been produced," he said.
"We are building breeder facilities not commercial facilities. These facilities are shower-in, shower-out so they are operated at a very secure level of bio-security," McCready said.
He also said the barns will benefit the local economy through more business and employment opportunities.
The public hearing will be held at 9 a.m. Monday at the Edmunds County Courthouse in Ipswich.
___
Information from: Aberdeen American News, http://www.aberdeennews.com