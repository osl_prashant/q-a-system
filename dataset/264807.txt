New Mexico regulators OK massive wind farms near Texas
New Mexico regulators OK massive wind farms near Texas

The Associated Press

SANTA FE, N.M.




SANTA FE, N.M. (AP) — New Mexico regulators have approved a plan to build two massive wind farms along the Texas-New Mexico border.
The New Mexico Public Regulation Commission voted unanimously Wednesday in favor of a $1.6 billion project that Xcel Energy says will add 1,230 megawatts of wind energy to the regional generating mix.
The utility is still awaiting approval from Texas regulators.
Xcel officials say the proposed wind farms would take advantage of what has become the least expensive generating resource in the region to reduce fuel costs and ultimately save customers money.
Xcel anticipates average monthly fuel savings to be about $2 for a typical residential customer beginning in 2021 once the wind farms are operational.
Wind turbines already dot the plains, from central New Mexico to the Texas Panhandle. Texas leads the nation when it comes to installed wind-power capacity.