What’s old is new in Detroit. 
The Detroit Produce Terminal, for example, a 168,000-square-foot facility at 7201 W. Fort St., has been the center of the city’s produce business since 1928.
 
The market, which has seven vendors, compares well with any of the most modern produce terminals in the U.S., said Jeff Abrash, owner of wholesaler Andrews Bros. Inc. and longtime market association president.
 
“We own it together and jointly operate it,” he said.
 
“We’ve hired a third-party property manager to execute our daily operations, and that has been a good move. We have professional management on-site every day, and it’s been a positive and has enabled us to focus on our core businesses.”
 
It’s the best way to run the facility, Abrash said.
“It’s best letting people who are skilled in building management and maintenance take care of the details,” he said.
 
While some major metropolitan clusters have seen their produce terminals disappear, Detroit’s remains strong and vital, Abrash said.
 
“We belong in that group of the exception,” he said.
 
“This market is 100%-plus occupied and is very competitive. We attract customers, whether retail or foodservice, to the market. It’s a great place to operate. We take a lot of pride in the terminal. It’s an effective source of supply.”
 
Maintenance is regular and rigorous, Abrash said.
 
“For 88 (years old), it’s serving its purpose,” he said. “We are essentially debt-free, which is great.”
 
The number of vendors doing business on the market has dwindled over the years, but there may never have been as much product or services available there as today, Abrash said.
 
“We’re at seven companies operating here, so consolidation has occurred just like at the retail level,” he said. 
 
“I would say there are seven very strong companies operating here.”
 
R.A.M. Produce Distributors, which also operates on the market, is doing very well there, said Michael Badalament, in sales.
 
“It’s very viable,” he said. “Overall, it’s been very good. It makes it more opportunistic for people to come here and get everything in one place.”
 
Retail support has been consistent, Badalament said.
 
“I think a lot of it happens to be we’ve got some super-great chain stores here and some talented, independent people who do an extremely good job and it all goes in,” he said. 
 
“The competition certainly helps you to maintain and make sure you keep stuff fresh and have good quality.”