USDA's Cattle on Feed Report showed the April 1 feedlot inventory at 10.904 million head, a modest 82,000 head more than the average pre-report estimate implied and up 0.5% from year-ago. March placements at 2.102 million head were stronger than anticipated at 11.1% above year-ago. Marketings last month at 1.914 million head were virtually in line with expectations and topped year-ago by 9.6%.




Cattle on Feed


USDA


Avg. trade estimate


Range 




 
% of year-ago levels
 



On Feed

100


99.7


98.7-100.5



Placements

111


106.5


101.0-109.5



Marketings

110


109.4


108.1-110.0




USDA's quarterly feedlot inventory breakdown shows steers down 1.8% from year-ago, while the number of heifers on feed are up 5.2%. That continues to suggest the herd expansion phase has run its course as more females are moving into the slaughter mix.
A weight breakdown of March placements shows only the lightweight category (under 600 lbs.) down from year-ago. And that was a modest 0.6% decline. Placements of 6-weights were up 7.3%, 7-weights were up 26.5%, 8-weights were up 8.3%, 9-weights were up 5.7% and heavyweights (1,000-plus lbs.) were up 11.7% from year-ago.
March placements topped the high end of the average pre-report estimate range. Therefore, far-deferred live cattle futures could face some corrective selling Monday. However, the data still indicates feedlots are current, which will limit seller interest in nearby futures.