Phosphates firmed on the week as potash softened.

DAP $1.85 above year-ago pricing -- higher $3.12/st on the week to $445.63/st.
MAP $10.52 above year-ago -- higher 66 cents/st this week to $454.14/st.
Potash $27.27 above year-ago -- lower $2.68/st this week to $328.47/st.


DAP led gains in the P&K segment this week led by Nebraska, which added $28.27 and Ohio gained $6.75. Four states were unchanged as Kansas fell $17.90 to lead declines as Indiana dropped 73 cents.
MAP was firmer as well led by Nebraska, which gained $14.02 as Michigan firmed $10.45 and Illinois added $5.47. Three states were unchanged as Kansas fell $10.40 and Minnesota softened $9.43.
Potash was lower on the week with Kansas off $21.60, Wisconsin down $9.60 ad Minnesota falling $7.84. Two states were unchanged as Michigan firmed $6.68 and Illinois firmed $2.83.
Wholesale prices were a mixed bag during the week ended October 13. Prices at US terminals softened slightly as phosphate out of Morocco firmed a few bucks. Meanwhile, wholesale potash values firmed at all reporting locations. Demand will likely take a pause as farmers are busy harvesting this week. That may weigh on prices at the retail level next week, but P&K prices typically do not fully bottom until after the first of the year.
As we said with nitrogen, and have been saying for a few weeks now, there are deals to be had on P&K around the Midwest. While according to our data and calculations phosphate is mildly overpriced compared to nitrogen, it is still just off the lowest prices we have recorded in 5-years. Compared to expected new-crop revenue, P&K should be considered a value, but, again, we expect price pressure, especially on phosphate after the first of the year.
To be specific, DAP for less than $410 per short ton or MAP below $425 and potash around $300 should be considered a good deal and if you can find such prices in your area, consider topping off your fall or spring needs. While we do expect prices to fall through the offseason, we do not expect DAP/MAP to mark fresh lows. So check with your preferred local retailer and see if your local supply situation is weighing on prices where you are. If you catch a bid at or below the levels above, take a moment and capture the value to guard against any unknowns that might prop P&K prices up.


By the Pound -- The following is an updated table of P&K pricing by the pound as reported to your Inputs Monitor for the week ended October 13, 2017.
DAP is priced at 46 1/2 cents/lbP2O5; MAP at 43 cents/lbP2O5; Potash is at 27 3/4 cents/lbK2O.





P&K pricing by the pound -- 10/19/2017


DAP $P/lb


MAP $P/lb


Potash $K/lb

 



Average


$0.46 1/2


$0.43


$0.27 3/4

Average



Year-ago


$0.46 1/4


$0.41 3/4


$0.25

Year-ago