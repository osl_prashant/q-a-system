$1.5B settlement in suit over Syngenta modified corn seed
$1.5B settlement in suit over Syngenta modified corn seed

By MARGARET STAFFORDAssociated Press
The Associated Press

KANSAS CITY, Mo.




KANSAS CITY, Mo. (AP) — A $1.5 billion settlement has been reached in a class-action lawsuit covering tens of thousands of farmers, grain-handling facilities and ethanol plants that sued Swiss agribusiness giant Syngenta over its introduction of a genetically engineered corn seed.
Lawsuits in state and federal courts challenged Syngenta's decision to introduce its modified Viptera and Duracade corn seed strains to the U.S. market for the 2011 growing season before having approval for import by China in 2014. The plaintiffs said Syngenta's decision cut off access to the large Chinese corn market and caused price drops for several years.
The settlement must be approved by a federal judge in Kansas. It does not include exporters such as Cargill and ADM that are also suing Syngenta.