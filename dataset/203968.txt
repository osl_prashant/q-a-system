Snacking is a booming trend for kids as much as for adults, and it is encouraging fresh produce marketers to develop new products geared toward the younger generation.
Skyrocketing demand for quick, easy snacks that can be eaten throughout the day is fueling innovation in convenience and value-added produce packs for children.
The produce industry is adapting quickly to the trends, making it likely that profits will improve in coming years, Elizabeth Pivonka, president and CEO of the Hockessin, Del.-based Produce for Better Health Foundation said in May during a presentation for the Southeast Produce Council.
Other opportunities, Pivonka said, lie in retail cross-promotion with new and growing partners, such as the burgeoning yogurt, pizza and Mexican food markets.
The yogurt category, for instance, grew 12.5% from 2004-14, and it plays into another trend in the report - the desire consumers have to include fruit as part of their breakfast or snacks.
"We need to reposition fruits and vegetables, and positioning must differentiate in health as meeting broader needs for the occasion, such as breakfast," Pivonka said.
"Convenience, convenience, convenience and speed are what we're hearing we need to promote besides health."
Research shows fresh produce trending upward while other fruit and vegetable categories struggle for even meager consumption growth.
The number of projected fruit eatings, excluding juice, is projected to rise 9.2% from 2014-19. Fruit eatings rose 18.9% from 2004-14. However, total fruit eatings, including juice, is projected to rise only 3.8% from 2014-19 and actually rose 6.5% from 2004-14.
Meanwhile, fresh vegetable eatings are expected to rise 7.5% by 2019 (compared to 2014 figures) after rising 14.5% from 2004-14. The total vegetable category, however, is projected to continue its 4.3% growth pace through 2019.
PBH predicts fruit-craving younger consumers will be the best bet to drive produce category growth in coming years.
Research suggests the only notable consumption gains by demographic groups will come in fruit from consumers under the age of 18.
PBH projects slight gains in fruit consumption from men and women age 18-34, and declines in fruit consumption for all those 35 and over, as well as declines in vegetable eating for Americans of all ages.
School foodservice sales could prove to be especially lucrative for the fresh produce industry, Pivonka said.
"Pricing strength is in-home, so we need to make sure we're focusing in-home, but foodservice is the big opportunity for growth," she said.
Quoting research compiled by the NPD Group factoring into the PBH 2015 State of the Plate report, Pivonka noted the average in-home meal costs $2.24 per person while the average restaurant meal costs about three times that much, $6.96 each, making foodservice a profitable category.