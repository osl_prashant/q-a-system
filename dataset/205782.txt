Wednesday morning, the grain and soybean complexes were uniformly higher. Dan Hueber, author of “The Hueber Report” blog on AgWeb.com and a guest on the Aug. 23 episode of Market Rally Radio, couldn’t believe his eyes.
“Granted, since the initial jolt of optimistic trade, we have surrendered most if not all the gains but deference to current myth, these markets can trade positive, albeit for short periods right now,” he says.
The dominant influence on the grain markets this week is the news from the Farm Journal Midwest Crop Tour, he says.
“Of course, at this point, they are little more than bits and pieces but when something is starving for nutrition, this case in the form of fresh news, it is grateful to lap up even the tiniest crumbs that fall from the table,” Hueber says. 
As crop scouts release their findings for Illinois and preliminary findings for Iowa, Hueber expects a few surprises.
“Illinois has a lot of problem spots and Iowa’s crops are not uniform,” he says. “It is hard to envision we’ll match last year’s yields.”
Listen to Hueber on Market Rally

The final numbers from the Tour will be released Thursday and Pro Farmer will release their yield estimates, which factor in Tour findings as well as other perspectives, on Friday.
“Then the trade can stew about them until markets reopen on Sunday night,” Hueber says. “The markets are marking time until we have something new to talk about.”
Reports from the Field
Also appearing on today’s Market Rally Radio were Crop Tour hosts, Brian Grete and Chip Flory.
Grete, Farm Journal’s Pro Farmer editor, is on the eastern leg of the tour. In Indiana, he says, signs of the rough spring farmers there experienced were evidence in their crop samples.
“The variability is extreme in Indiana, but not as extreme as what we saw in Ohio,” he says.
Listen to Grete on Market Rally

In Illinois, Grete says, his route found less variability as they traveled West.
“We’re pulling decent samples, but probably not what farmers wanted when they put the seed in the ground this spring,” he says. “Illinois is not as good as it was last year.”
On the western leg, Flory, Farm Journal Pro editorial director, saw a wide range of weed issues in Nebraska.

“It varied from a complete mess to pretty good weed control,” he says.
As his team crossed into western Iowa, drought issues were prevalent and they recorded an extremely wide of corn yield estimates.