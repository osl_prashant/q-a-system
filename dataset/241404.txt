Amazon Produce Network earns Fair Trade status
Vineland, N.J.-based Amazon Produce Network earned its Fair Trade Certified designation in January.
“That’s exciting for us because we just believe in the program and the opportunities, the better lifestyle it’s providing for the workers on the farms that are making this investment,” said partner Greg Golden.
“That’s something that’s new that we’re proud of.”
The company’s largest supplier in Mexico earned the certification a few months earlier, and another one of Amazon’s growers is also working to receive the distinction.
 
Ciruli Bros. expects promotable volumes
Rio Rico, Ariz.-based Ciruli Bros. expects volume to be similar to 2017 or higher.
“Sizing looks to be running a little bit smaller than last year, but that’s good for promotable volume in fruit by the each,” partner Chris Ciruli said in early February, with the ataulfo season getting started.
“Quality looks to be a little bit cleaner than last year because obviously one of the things that affects your quality is how much rain you have, and there hasn’t been rain, so that creates a smooth and clean fruit.”
He expected round mangoes to start becoming available from Mexico in late February, with the deal picking up in March.
 
Robinson Fresh seeing good demand
Eden Prairie, Minn.-based Robinson Fresh reported continued high interest in mangoes.
“Demand through January of 2018 was very strong in the U.S. markets,” said James Watson, who works in sourcing. “For the majority of the month, demand exceeded supply for mangoes, (primarily) exported from Peru until the Mexican mango market is up and running.”
Robinson Fresh began shipping ataulfos the last week of January and hadens the following week.
“The honey mangoes are showing fair quality,” Watson said. “This is typical for the early crop fruit out of Mexico as it normally has some cosmetic defects like scarring at the beginning of the season. The quality is expected to improve in the next few weeks.
“The haden mangoes are showing very good quality, including a good blush or color to the fruit,” Watson said.
He noted that while Robinson Fresh handles mostly red varieties, ataulfos have been growing in popularity.
 
Coast Tropical adds sales staff
Los Angeles-based Coast Tropical has added to its sales department.
Brenda Covarrubia, who most recently worked for Los Angeles-based The Wonderful Co., recently joined the staff.
In addition, Maricela Velasquez has returned after working for Oxnard, Calif.-based Freska Produce International.
 
Consumer education still a focus for Crespo Organic
Rio Rico, Ariz.-based RCF Distributors sees mango demand continuing to rise.
Nissa Pierson, who manages sales and marketing for the Crespo Organic program, said the company will continue its consumer education on how to prepare and use mangoes.
It does social media contests and display contests for retailers as well as mango cutting demonstrations and other events, Pierson said.
“There’s a lot of inquiry about what to do with (mangoes), how to cut them, etc.,” Pierson said.
“So this is kind of one of the places we thrive.”
 
Organics going strong for Freska
Oxnard, Calif.-based Freska Produce International continues to see interest in mangoes grow.
“Demand after the holidays has been excellent,” said co-founder and managing member Gary Clevenger.
“This has also affected the organic (segment) as well — every year for the past four years organics has doubled in volume for us, and we expect the same again this year.”
The company expects good volume this season, starting with ataulfos. Freska began shipping in late January.
Sales manager Tom Hall will join the National Mango Board in March.
 
Grupo Palenque expects large volumes
Mango producer and exporter Grupo Agricola El Palenque SA de CV expects good volume and quality this season.
The company exported about 2 million boxes in 2017 and projects it will ship a similar amount of fruit this year.
“We are harvesting ataulfo mangoes of all sizes, especially the large ones (12s, 14s and 16s),” said chief marketing officer Alejandro Arcos.
“Quality is looking great at our orchards as well. Our percentage of (No. 2s) in comparison with last year has decreased significantly.”
The company plans to launch two products in 2018. One is a 6-kilogram carton box, which was designed for shipments to Europe and Asia. The other product is still in development, so details are limited, but it is targeted for younger consumers, Arcos said.
 
J&C Tropicals adds to staff
Luis Vega recently joined the sales department of Miami-based J&C Tropicals.
His previous experience includes stints at two floral companies, Galleria Farms and Esmeralda Farms.
J&C also added Alejandro Castano to its procurement department in September.
Castano had most recently worked at Santiago de Chile, according to a news release.
 
National Mango Board hires marketers
The Orlando, Fla.-based National Mango Board has made two more additions its marketing staff.
Marissa Khan joined in December as a marketing manager, and Michelle Larkin joined in December as a marketing specialist.
Tammy Wiard, retail program manager, came on board in November. She succeeded Wendy McManus, who had been with the board for 11 years.
 
Vision Import Group buys out partner
Hackensack, N.J.-based Vision Import Group recently bought out its Los Angeles-based partner Vision Produce Co.
The companies are pursuing different directions and may collaborate on certain projects in the future, said partner Ronnie Cohen.
Vision Import Group and Vision Produce Co. had been together since 2008.
“The companies have enjoyed a partnership of 10 years and have decided on independent strategies looking to the future,” the companies said in a joint statement. “This change will create two powerful best-in-class companies with new independent focuses.”
 
Melissa’s expecting higher volumes
Los Angeles-based World Variety Produce, which markets under the Melissa’s brand, expects to ship more Mexican mangoes this year.
Volume should be up 5% to 10%, said director of public relations Robert Schueller. Gains in organic may be even larger.
The company started shipping ataulfos in mid-January. Tommy atkins mangoes will be available March through November, with peak volumes available May through September.