California Citrus Mutual vice president Bob Blakely has retired.
Blakely had worked for the organization since 2004 and served on the board for nearly a decade before he joined the staff.
Prior to his tenure there, Blakely was a grower and professional farm manager.
“It didn’t take long for Bob to show his value,” California Citrus Mutual president Joel Nelsen said in a notice to members. “Statistical analysis, regulatory advocacy, industry coordination, actual farming knowledge and the ability to articulate to government officials, the media and the industry became a regular feature of his daily job.
“There’s no doubt in my mind that his value will never be truly acknowledged, but in the past 25 years the ROI from his work ethic and successes is readily visible to me and the boards of directors he worked with,” Nelsen said in the notice.
The organization logged some significant accomplishments in the years Blakely was involved on the board or on staff.
Among those most notable for Blakely were the establishment of the California Citrus Growers Association, the adoption of a modified maturity standard for navel oranges, and the creation of the California Citrus Pest and Disease Prevention Program.
Blakely explained why each milestone was meaningful.
The California Citrus Growers Association helped stabilize the market for California citrus, making it more likely growers would receive a good return for their fruit. The California standard for navel maturity allowed growers to send better-tasting fruit to the market earlier in the season, an improvement over the earlier standard of an 8-to-1 sugar-to-acid ratio that did not always correlate to flavor.
The California Citrus Pest and Disease Prevention Program has been an instrument through which the industry has been proactive about battling the Asian citrus psyllid and the crippling huanglongbing disease that it can carry.
In his retirement, Blakely and his wife Sharon plan to do volunteer work and travel.
Blakely said what he will miss most about day-to-day involvement in the industry is the people.