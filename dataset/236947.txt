LSU, state look for canker-resistant satsuma varieties
LSU, state look for canker-resistant satsuma varieties

The Associated Press

BATON ROUGE, La.




BATON ROUGE, La. (AP) — The Louisiana Department of Agriculture and Forestry and the LSU AgCenter are beginning a two-year study to find out which varieties of satsuma trees are most resistant to the dangerous disease called citrus canker .
The bacteria damage all kinds of citrus from kumquats to grapefruit, causing a gradual decline until the tree stops producing any fruit.
The agriculture department says LSU's analysis of samples collected for the department's citrus pest survey found that some satsuma are resistant, but not which.
AgCenter scientist Raj Singh says the study will look at five cultivated varieties of satsuma — or, as scientists call them, "cultivars."
They'll keep an eye on healthy trees in areas with known infections, to see whether some show infections sooner than others.