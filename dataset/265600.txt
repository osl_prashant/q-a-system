Ricketts unites farm, business groups behind tax package
Ricketts unites farm, business groups behind tax package

The Associated Press

LINCOLN, Neb.




LINCOLN, Neb. (AP) — Nebraska Gov. Pete Ricketts is uniting major farm and business groups behind his tax package, despite continued uncertainty over its prospects in the Legislature.
Ricketts argued Monday that the bill offers the property tax relief that many farm, ranch and home owners have demanded.
The Nebraska Farm Bureau says that if the measure passes in this year's session, it will withdraw its support from a campaign to place a property tax measure on the November general-election ballot.
The Nebraska Farm Bureau has been a major supporter of the campaign. Ricketts has said the ballot measure would cause major disruptions in state government, requiring large cuts or massive tax increases.
Sen. Paul Schumacher, of Columbus, a critic of the bill, says it would "spend money we don't have."