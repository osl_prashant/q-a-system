Mystery remains for South Dakota rancher's poisoned cattle
Mystery remains for South Dakota rancher's poisoned cattle

The Associated Press

HOWARD, S.D.




HOWARD, S.D. (AP) — The case of an eastern South Dakota rancher whose 44 cattle were mysteriously poisoned and killed on Halloween remains unsolved.
The state Department of Criminal Investigation hasn't been able to track down cattle-killer.
Bernard Donahue tells KELO-TV that his family had been keeping 65 cattle on a rented pasture in Miner County when someone poisoned the water tank last October. By the next day, 44 of their cattle were dead, including a bull.
Donahue says the tank smelled like rotten eggs or ammonia. He says water samples were taken for testing, but there hasn't been confirmation of what chemical was used. A veterinarian called to the scene says an ammonia source can dissolve and disappear in water.
County Sheriff Lanny Klinkhammer is confident the killer will be caught eventually.
___
Information from: KELO-TV, http://www.keloland.com