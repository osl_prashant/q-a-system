The internet and social media are introducing a generation to the world of tropical produce.
"Everything's a click away," said Carlos Rodriguez, director of tropical sales for Procacci Bros. Sales Corp., Philadelphia.
"People see new products, and they're intrigued," Rodriguez said. "Two seconds later they've Googled it and found 12 recipes."
Young people are the most adventurous eaters, he said, curious about and willing to experiment with new fruits and vegetables.
"Every day is an education," he said. "Somebody mentions turmeric, and it's now the hottest thing we have, with Hispanic customers who've never used it before asking for 40-pound cases a week because they read it's wonderful."
"We've got customers calling to say I want this and we don't even know what this stuff is," said Rick Feighery, Procacci's vice president sales.
"Our resource tool used to be the older guys in the office," Feighery said. "Now it's Google."
Consumers are definitely looking for information on produce they may not be familiar with, said Mary Ostlund, marketing director for Brooks Tropicals Inc., Homestead, Fla.
"In July, 4,500 people landed on our Caribbean Red papaya how-to pages," Ostlund said.
Mobile devices alone made up over a third of Brooks' website traffic in July, she said. Consumers can text "tropicals" "papayas" or "starfruit" to 41-411 for instant access.Ostlund said frequent tips, recipes and how-to's on social media also pull in the consumer, but the posts must be relevant.
"Don't highlight a holiday unless you've got a great tie-in," she said. "For example, you'll want to wish everyone a Happy Columbus Day, but only post if you can tie it in with fruit the explorer may have seen on his journeys."Brooks' "no-recipe recipes" are another winner on social media, Ostlund said.
"We're offering ways for consumers to enjoy tropical fruits and vegetables without having to follow any instructions," she said. "Better yet, no-recipe recipes are the ones you can pull together while you're in the store, stymied over what to make for dinner."
Brooks' dragon fruit no-recipe page is one post getting lots of hits, she said, with ideas such as tossing chunks in a melon salad or topping a steak hot off the grill with chopped dragon fruit.