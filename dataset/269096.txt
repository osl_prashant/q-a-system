Lucas Strom, who runs a century-old family farm in rural Illinois, canceled an order to buy a new $71,000 grain storage bin last month - after the seller raised the price 5 percent in a day.
The reason: steel prices jumped right after U.S. President Donald Trump announced tariffs.
Throughout U.S. farm country, where Trump has enjoyed strong support, tariffs on steel and aluminum imports are boosting costs for equipment and infrastructure and causing some farmers and agricultural firms to scrap purchases and expansion plans, according to Reuters’ interviews with farmers, manufacturers, construction firms and food shippers.
The impact of rising steel prices on agriculture illustrates the unintended and unpredictable consequences of aggressive protectionism in a global economy. And the blow comes as farmers fear a more direct hit from retaliatory tariffs threatened by China on crops such as sorghum and soybeans, the most valuable U.S. agricultural export.
A&P Grain Systems in Maple Park, Illinois - the seller of the storage bin Strom wanted to buy with a neighboring farmer - raised its price two days after Trump announced aluminum and steel tariffs on March 1 to protect U.S. producers of the metals. Strom and his neighbor backed out.
“Would that price destroy us? No,” Strom said. “But these days, you have to be smart about your expenses.”
The metals tariffs also hitting makers and sellers of farm equipment, from smaller firms like A&P Grain to global giants such as Deere & Co. and Caterpillar Inc. Such firms are struggling with whether and how to pass along their higher raw materials costs to farmers who are already reeling from low commodity prices amid a global grains glut.
The world’s two largest economies have threatened each other with tariffs on tens of billions of dollars of goods recent weeks.
Trump imposed tariffs of 25 percent on steel and 10 percent on aluminum in a move mainly aimed at curbing imports from China. He has since temporarily excluded the European Union and six other allies from the duties and given them until May 1 to negotiate permanent exemptions.
A&P Grain President Dave Altepeter said the steel used in their bins is made in the United States, but domestic steel prices also have soared because of the tariffs.
U.S. steel mills typically adjust their prices once a year, normally in the first quarter, Altepeter said. But this year, those prices have jumped four times, he said.
The price of steel used in A&P’s grain bins has jumped about 20 percent since January 1.
“Any time there’s any type of negative talk that affects the steel mill, they’ve raised the price,” said Altepeter.
Last year, about 95,000 tons of steel was shipped to the agriculture industry, compared to the 14 million tons for the U.S. auto industry, according to the American Iron and Steel Institute, an industry group.
Other factors had been driving up steel prices before the recent trade disputes, including an improving global economy and accelerating manufacturing and construction, particularly in the U.S.
The White House referred questions from Reuters to the U.S. Department of Agriculture, which did not respond to a request for comment. Trump and Agriculture Secretary Sonny Perdue have vowed the U.S. government will protect farmers from China’s tariffs, but not explained how.
U.S. farmers can ill-afford any loss of sales. Farm income has dropped by more than half since 2013, following years of massive harvests that have depressed prices for staples such as corn and soybeans.
U.S. competitors Brazil, Argentina and Russia have all raised grain output in recent years, eating into the U.S. share of global markets. Mexico imported ten times more corn from Brazil last year and is set to buy even more in 2018 on worries that renegotiations of the NAFTA trade pact could disrupt their U.S. supplies.
Strom said he has also pushed back plans to build a new metal storage building to house his planter and the combine head he uses for harvesting corn and soybeans. Other farmers, food producers and beer makers have scrambled to finalize deals for steel-based equipment before prices climb more.
CONSTRUCTION POSTPONED
In Riverton, Illinois, farmer Allen Entwistle said he postponed construction of a new $800,000 storage system for grain after AGCO Corp’s GSI unit increased prices by 15 percent.
Entwistle, who voted for Trump, will instead store corn in bags on the ground.
“President Trump keeps telling us he’s going to get a better deal,” Entwistle said. “When are we gonna make it better?”
AGCO said Trump’s tariffs will raise its costs and make price hikes to customers unavoidable.
“As the entire grain storage industry has weathered increased steel prices, AGCO and GSI are constantly looking for new ways to maximize efficiency and minimize the impact to customers,” said spokeswoman Kelli Cook.
Other companies, including Deere and Caterpillar, are also facing pain from rising steel prices, which account for about 10 percent of equipment manufacturers’ direct costs.
Deere CEO Samuel Allen told Reuters last month the company will have to absorb the price increase and cut costs elsewhere. China’s threatened tariffs on U.S. crops could hurt the company even more by undermining demand from farmers, he said.
“This has a huge effect on livelihood of the farmer right now, and at the same time it has a huge impact on manufacturers,” said Dennis Slater, president at the Association of Equipment Manufacturers, an industry group.
U.S. net farm income is forecast to drop to $59.5 billion in 2018 dollars, down from $64.9 billion in 2017, an 8.3 percent decline, according to the USDA.
TARIFF ‘DOOM-AND-GLOOM’
In Sheffield, Iowa, Sukup Manufacturing has seen steel prices soar 40 percent since November, said Brent Hansen, the company’s commercial accounts manager.
The maker of grain bins and pre-manufactured steel buildings has encouraged customers to buy quickly before prices jump more. But some have already postponed projects, Hansen said.
“That’s obviously a big price increase for an industry that’s a little bit doom-and-gloom over tariffs,” Hansen said.
Sukup used to give customers up to two months to consider its bids for projects. Now, it allows just a week in some cases because of volatile steel prices, Hansen said.
Prices have jumped by 25 percent for thermal insulated panels that keep food cold – which can use either steel, aluminum or both, said Glenn Todd, owner of Todd Construction Services. The company has built food processing and storage facilities for Bumble Bee Seafoods and poultry company Foster Farms.
Richard Adkins, director of sales at Discovery Designs Refrigeration in Mukwonago, Wisconsin, thought his company wouldn’t have to worry about Trump’s tariffs. Most of the metal they use to design industrial refrigeration systems comes from Canada and Mexico, he said, and the president has exempted both countries from the levies.
It didn’t matter. Price-hike notices from vendors landed in Adkins’ mailbox days after Trump announced the duties.
“There’s this knee-jerk reaction,” Adkins said. “We’re quoting prices for projects that won’t be awarded for another six or eight months, and no one wants to be hung out to dry.”