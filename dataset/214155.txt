Mucci Farms and Southern Specialties earned top honors at the Produce Marketing Association’s Fresh Summit for Best Packaging Promo and Best Overall Product Promo, respectively.
The packaging award “recognizes packaging that generates interest, appeal and differentiation on the store shelf,” according to a news release from PMA. Exhibiting companies with products in the Fresh Summit Fresh Ideas Showcase were eligible for the honor.
Mucci’s Veggies to Go line of healthy snack packs for children was noted for its convenient, on-the-go size for easy stashing in school bags. The recyclable containers come with EZ Snap closures allowing each compartment to be snapped closed, according to the release.
Southern Specialties received the overall product category award with its Fresh Ideas Showcase shelf, which included the company’s Sweet Carmine Cherry tomatoes, hand-peeled fava beans and recipe-ready specialties. Those products which have items ready for meals, and home cooks just add protein and seasonings.
The awards were judged by a panel of retail experts from Ahold USA Inc., Grocery Outlet Inc., The Packer, Tops Markets Inc. and Wal-Mart, according to the release.
Judge Ron McCormick, senior director of global sourcing for Wal-Mart Stores Inc., said in the release that the contestants demonstrated that “product packaging helps sell products, and is critical to our industry’s success.”
Three Fresh Summit exhibitors also took home Best of Show honors:

Ripe, a fresh juice company, received the Best of Show for in-line booth;
Zespri Kiwifruit was the Best of Show island booth winner; and
Obrigado, a coconut water company, was Best First-Time Exhibitor.