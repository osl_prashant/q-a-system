Farmers deciding whether to continue apple promotion program
Farmers deciding whether to continue apple promotion program

The Associated Press

LANSING, Mich.




LANSING, Mich. (AP) — Michigan apple farmers are voting on whether to continue an advertising and promotion program for their industry.
The state Department of Agriculture and Rural Development mailed ballots to eligible producers, who have until March 30 to return them.
The program is intended to boost Michigan apple production through market development, education and research.
It's led by a panel consisting of seven producers appointed by the governor.
The program was last approved in 2013 and by law must be renewed every five years.
More than 50 percent of the producer votes cast must support the program for it to continue.