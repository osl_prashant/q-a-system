If labor issues are keeping produce industry executives awake at night, technology can work as a sedative, according to Bob Whitaker and Lisa Montanaro, who will talk about how companies can “work smarter” at the Produce Marketing Association’s Tech Knowledge event.Trying to be more efficient is nothing new, but it may never have been as crucial as it is today, said Whitaker, PMA’s chief science and technology officer, who emcees the event May 4-5 in Monterey, Calif.
“Produce and floral are manual labor-intensive, so the ready availability of workers has always been a  premium for our industry,” he said.
The concept of employing technology isn’t just for big companies, either, said Montenaro, a productivity consultant and owner of Davis, Calif.-based Lisa Montanaro Global Enterprises LLC.
Montanaro’s organization focuses on helping clients become more efficient.
“(A company’s) size doesn’t matter as much as people think,” she said. “Some are scalable and adaptable, but the benefits are still there.”
Smaller firms may even have a bit of an edge, as they adapt strategies that larger operations had to “iron out,” Montanaro said.
Labor costs are rising as the available pool of workers shrinks, Whitaker said.
“More recently, factors have been playing into crops not being able to be harvested,” he said.
An unclear U.S. immigration policy and stiffer competition for help on both sides of the border have exacerbated the situation, which heightens the need for companies to make optimum use of the workers they have, Whitaker said.
“(The U.S. Department of Agriculture) estimates up to 40% of certain products can be assigned to labor costs, and I’d suggest it could be higher in value-added products,” he said. “Now, there’s competition for that labor,” he said.
There are ways growers and processors can “work smarter,” Whitaker said.
“If you can look at solutions that allow you to reduce the amount of labor you need or more efficiently use the labor you have, you can improve the amount of product you can produce and the price you can produce it,” he said.
Automation and mechanization could be part of the solution, he said.
“You can eliminate weeding and thinning crews by using a lettuce bot from Blue River Technology and get rid of 20- or 30-person labor crews that do it by hand, you can look at the cost of that technology and make it pay off going forward, as labor becomes more scarce and minimum wages increase,” he said.
Digital sorting machines have become common, which illustrate the point, Whitaker said.
“Replacement of labor is really a continuation of what we’ve been doing for 30 years,” he said.
Harvesting technology is just one piece of the puzzle. Sensors and drones can deliver data accurately and quickly, Whitaker said.
Automation should not be seen as a direct replacement of workers. It’s more about “using technology to help us collect and analyze data and predict outcomes and put us in a situation where we can prevent a line shutdown or harvest issues,” he said.
Companies should have a productivity audit, Montanaro said.
“It doesn’t have to be negative,” she said. “It’s an opportunity to focus on productivity challenges, find missing structures and improve on them. Industry has to be careful not to over-focus on their weaknesses. Focus on what’s working, not just what’s not working.”