Carpinteria, Calif.-based Hollandia Produce, which markets under the Pete’s Living Greens brand, plans to showcase its recently launched endive and escarole Living Strips at the Produce Marketing Association’s Fresh Summit, Oct. 19-21 in New Orleans. 
Living Strips’ Home Harvest Packs have 100% recyclable plastic packaging that allows consumers to easily harvest leaves by cutting or pulling them from the top of the package, leaving the roots intact below, according to a news release.
 
Endive and escarole Living Strips are contenders for a PMA Fresh Summit Best Packaging Promo award.
 
“Our focus will always revolve around providing consumers with the top-quality fresh and sustainable produce that fits into their busy lifestyles,” Pete Overgaag, vice president of innovation and corporate strategy, said in the release.
 
Living Strips also come in baby romaine, spring mix, sweet butter blend, Italian mix and upland cress, and are available at California retailers including Stater Bros., Safeway, Von’s, Albertson’s and Bristol Farms.