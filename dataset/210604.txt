Often in agriculture, external factors, such as weather and market prices, are beyond a farmer’s control. Financial difficulties may arise due to lack of production and decreased market prices resulting in stress experienced by farm families. When difficulties exist, it is important to focus upon maintaining open communication within the farm operation.When information is not shared among family members and employees, increased tension and worrying may occur. Farm employees may begin to worry if they might lose their job, and rumors may be shared. Maintaining open communication and seeking social support can help producers get through difficult times.
Establishing Open Communication
To establish open communication, farmers and ranchers may find it helpful to hold regular operational meetings. If others are involved in brainstorming ideas and providing solutions, a sense of togetherness and shared goal can relieve stress. Effective team meetings can provide structure and promote positive decision-making, focus on strengths of employees, establish trust and cohesion among the team, increase productivity, and help effectively plan for current and future tasks1.
The following include several tips in establishing effective operation meetings1:
Establish ground rules (e.g. one person speaks at a time)
Include everyone – make sure everyone feels heard
Set a specific date/time to meet; establish a regular meeting schedule
Develop an agenda
Alternate who leads the meeting (if appropriate to your operation)
Document minutes from the meetings (share this role)
Establish a process for decision making and well as resolving conflict (e.g. voting)
Integrate fun into the meeting
Provide a summary of the meeting – ask for any unfinished business
End on a positive note
If you are a farmer or rancher, it is also important for you to share your thoughts and feelings with those that support you. Social support has been found to decrease symptoms of depression2. Go into town for coffee, engage in a hobby, or talk to a friend or family member about your worries. Unfortunately, the weather cannot be controlled; however, with social support, you can process your worries and stress with others who care about you.
Other Resources
Brain health: The impact of chronic stress
Farmers have stress also
Thriving in today’s times: Stress and the family
Recognizing Symptoms of Stress During Farming Challenges
A Five-Step Approach to Alleviating Farm Stress
References:
Wilkinson, J., & Sykes, L. (2011). A guide to communication for farm families. Canberra MC ACT: Grains Research & Development Corporation.
Bjornestad, A., & Brown, L. (in progress). The relationship between social support and depression symptoms in farmers and ranchers.