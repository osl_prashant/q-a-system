The locally grown movement continues to boost sales for growers. Of course, what you consider "local" depends on who you ask.
"For sweet potatoes, local could be better categorized as regionally correct," said Eric Beck, marketing director for Wada Farms Marketing Group, Idaho Falls, Idaho.
"Now, how you define regionally correct is up for interpretation. For the most part, if you split the U.S. in half with Texas being the halfway point, everything east of this line does better with East Coast supplies; and conversely, west of this line is best serviced with West Coast supplies. Having the most apt regionally/locally sourced product on the shelf will appeal to the end consumer."
Black Gold Farms is based in Grand Forks, N.D., but its sweet potatoes are grown in Louisiana, said Leah Brakke, director of new business development.
"Locally grown promotions are important for any produce category," she said. "We are lucky because we only grow in Louisiana. We tout that as much as we can. We've been able to support our Louisiana retailers with locally grown materials, and we say Louisiana grown on our packaging. Some of our product stays in state, but we also do a lot out of state."
Rene Simon, director of the Louisiana Sweet Potato Commission, Baton Rouge, said that Louisiana label has appeal beyond the Pelican State.
"We try to focus on regional markets where we might have a small advantage," he said. "In places like Mobile, Ala., and Dallas there are a lot of Louisiana natives. We can offer something with a Louisiana flavor that reminds them of home."
The same is true for California, said Duane Hutton, manager of Yagi Bros. Produce Inc., Livingston, Calif.
"California is a big state, so a large portion of our product is sold in state," he said. "We have seen retailers promote the locally grown aspect."
However, Hutton said "California grown" has promotional appeal in other states because of the state's reputation for quality.
Autumn Campbell, sales for Matthews Ridgeview Farms, Wynne, Ark., said a portion of the company's volume stays in that state.
"Locally branding is always good because people like the feeling of knowing they are buying local and close to the farm," she said.
Demand for local isn't limited to retail settings.
"Consumers are trying to make a closer connection to the farm," said Charlotte Vick, partner in Vick Family Farms, Wilson, N.C. "Chefs are serving more local products, and often time the sweet potato is a part of their menu."