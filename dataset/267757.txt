Greenhouse vegetables are an integral part of produce department sales and give consumers ever-expanding options in variety and packaging choices.
According to 2015 research by Roberta Cook, with the University of California-Davis, on “Fresh Tomato Production and Marketing Trends in the North American Market,” combined hothouse round and tomatoes on the vine represent 36% of total category quantity and 39% of total category dollar sales.
And considering the estimated sales of roma and snacking tomatoes grown in hothouses, Cook said in her report the hothouse share of the total tomato category should be more than half of total volume and sales.
 
Market advantage
Greenhouse marketers have created a distinctive presence in the market, said Anthony Totta, founder/consultant at Lee’s Summit, Mo.-based FreshXperts LLC.
“One of the things that makes the industry unique to some of the other fresh produce market categories is that the growers have put themselves in a position where they sell delivered instead of f.o.b.,” Totta said.
“Most of their business is done delivered and they make the logistics really easy and kind of seamless.”
Besides that, greenhouse produce has found a great market reception, Totta said.
Greenhouse vegetables generally sell well at retail, with less shrink, he said.
“Retailers are making good money selling greenhouse vegetables,” he said.
Matt Mandel, vice president of operations for Rio Rico, Ariz.-based SunFed said greenhouse and protected culture vegetables are protected from the threats faced by open-field crops, giving it both higher quality and less exposure to pests.
“Because they are less exposed to pest threats, greenhouse vegetables typically have fewer pesticides applied to them, which is an appeal to many consumers,” he said.
With new greenhouse tomato and vegetable varieties coming to market on a regular basis, consumers are intrigued about their quality and flavor profiles, said Chris Veillon, chief marketing officer for Pure Hothouse Foods, Leamington, Ontario.
“Additionally, understanding how greenhouse produce is grown and the sustainable process growers take to grow their vegetables is becoming more and more interesting to consumers,” Veillon said.
From packs of blended grape tomatoes to new varieties of snack sized tomatoes, the continual rollout of new packaging and new varieties has helped fuel the growth of greenhouse vegetables, Totta said.
Many types of greenhouse vegetables add to sales and don’t displace other field-grown items, said Craig Carlson, owner of Chicago-based Produce Consulting LLC.
With distinctive packaging or wrapping, greenhouse vegetable items are typically priced at a premium and viewed as an incremental item.
The expansion in the availability of organic greenhouse vegetables also has spurred growth, Totta said.
Retailers can drive more sales by specifically calling out the “greenhouse grown” nature of the vegetables and explaining what it means.
Aaron Quon, executive greenhouse category director for the Vancouver, British Columbia-based Oppenheimer Group, said helping consumers understand the difference between produce grown in a greenhouse compared with an unprotected environment can lead to more sales, and he also said consumers want to hear grower stories on point-of-sale materials or through social media.
Consistency in size, quality and flavor are important attributes of greenhouse vegetables for consumers, said Harold Paivarinta, senior director of sales and business development for Red Sun Farms, Kingsville, Ontario.
“In addition, the commodities produced are from non-GMO seeds and for some individuals that is a key component to the products they buy,” he said.
 
Future challenges
Carlson said the rising urban greenhouses and vertical farming operations using LED lighting near metropolitan centers face challenges to match supply with demand.
“Now they are growing a lot of greens, but they are also diversifying themselves and growing a lot of other items,” he said.
“The problem with some of the greenhouses is that they grow so much of that leafy greens, but where it is all going to go?” he said. “You have to be able to find a home for all of it.”
Finding a way for urban greenhouses to diversify beyond mixed greens and microgreens so that the additional items are competitive with the broader market is the challenge, he said.
That being said, Carlson said that technology of growing with LED lights is improving and retailers and foodservice operators value locally sourced products.
“I think it is a draw to have it local,” he said.
“Foodservice want to have a steady program and steady prices 52 weeks per years, that way chefs can put it on the menu as locally grown,” Carlson said.
“The same thing with retailers — they can call it locally grown and use it as part of their program.”