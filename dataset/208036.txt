Author and graffiti artist Erik Wahl will give the keynote address at Supervalu’s fourth annual National Expo in St. Paul, Minn.
 
Wahl will speak July 25 about unlocking creativity and innovation in business, according to a news release.
 
The event also will feature Grammy Award-winning singer-songwriter Colbie Caillat at a July 26 fundraiser for Feeding America, according to the release.
 
The July 25-27 expo will give retail attendees insights to industry trends, according to the release, with more than a dozen workshops offered on topics that include understanding the importance of natural, organic, specialty and ethnic food solutions. Other workshops focus on maximizing sales potential in retail grocery departments and tips on staking out a competitive strategy in the grocery business, according to the release.
 
More than 300 vendors will occupy 130,000 square feet of exhibit space, according to the release.
 
On July 27, Supervalu will present Master Marketer Awards to recognize merchandising, marketing and community relations efforts from its independent retailers over the past year.