BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Thursday:
Open  High  Low  Settle   Chg.ORANGE JUICE                          15,000 lbs.; cents per lb.                May     140.90 141.30 139.85 141.10        Jun                          141.55   +.35Jul     140.90 141.55 140.00 141.55   +.35Aug                          141.70   +.30Sep     141.35 141.70 141.30 141.70   +.30Nov     142.00 142.30 141.90 142.30   +.15Jan                          142.95   +.10Feb                          143.45   +.05Mar                          143.45   +.05May                          144.20   +.05Jul                          144.30   +.05Sep                          144.40   +.05Nov                          144.50   +.05Jan                          144.60   +.05Feb                          144.70   +.05Mar                          144.70   +.05May                          144.80   +.05Jul                          144.90   +.05Sep                          145.00   +.05Nov                          145.10   +.05Jan                          145.20   +.05Est. sales 471.  Wed.'s sales 805      Wed.'s open int 13,128