(UPDATED, Oct. 26) In a close vote, the House Judiciary Committee passed an amended Agricultural Guestworker Act Oct. 25.
Also known as the AG Act, the bill would replace the H-2A guest worker program under the Department of Labor with what is called an H-2C program overseen by the U.S. Department of Agriculture.
All committee Democrats and two committee Republicans — Louie Gohmert, R-Texas, and Steve King, R-Iowa — voted against the bill in 17-16 vote.
Under the AG Act, 450,000 visas will be available each year for H-2C workers. Returning H-2A workers and previously unauthorized farmworkers who participate legally in the new program won’t count toward the annual visa limit, according to a summary of the bill.
“It’s time for an ag worker program that both respects our nation’s immigration laws and keeps American agriculture competitive, House Agriculture Committee chairman Michael Conaway, R-Texas, said in a statement. He said Goodlatte’s bill reduces regulations and will provide a flexible program to meet the needs of both specialty crop producers and dairy operations.
“I look forward to working with the ag community and Chairman Goodlatte to help shepherd this legislation through the House,” Conaway said in his statement.

Produce industry leaders have been supportive of Goodlatte’s guest worker legislation.
Frank Gasperini, president and CEO of the National Council of Agricultural Employers, said the bill was amended by the committee to include a “touch back” provision for currently undocumented workers. That means current undocumented workers would have to return to Mexico before they could participate in the H-2C program.
That change is problematic, Gasperini said. It is not known when the full House will take up the bill, especially since taxes are a top priority.
The touch back provision will make it difficult for undocumented workers, especially those with families living in the U.S., to risk going back to Mexico.
Gasperini said the industry can’t afford to lose those workers.
“We hope for a chance to work and improve it on the House floor,” he said. “We are still thankful for Chairman Goodlatte to continue to work on this, and we are hopeful that we can come out with something that works in the end.”
Western Growers President and CEO Tom Nassif issued a statement supporting Goodlatte’s role in introducing guest worker legislation, but emphasized it needs work before passage in House and Senate.
“During the committee process, several changes were made to the legislation that would make this bill unworkable, particularly when coupled with expedited mandatory E-verify,” Nassif said in the statement. “We must ensure practical and reasonable solutions are achieved for both the current and future workforce that American farmers depend on in order for our industry to remain competitive.”
Nassif did not specify what revisions are troubling. The changes include:

Six months after approval, H-2C becomes officially enacted, but undocumented workers in the interim would not be protected from enforcement;
The original cap of 500,000 dropped to 450,000;
H-2C workers would have to have health insurance;
The Department of Homeland Security’s responsibilities under the H-2A program remain with DHS; and
Green cards will not be set aside for experienced agricultural workers.

Gasperini said agriculture industry leaders are also concerned about going from an uncapped H-2A program to a H-2C program at will be capped at 450,000 guest workers.
If the bill does pass the House and Senate, building the capacity at the USDA to handle such a large program is also a concern, Gasperini said.
“There is going to be some learning curve and they have got a build an organization to handle it and make it work and serve agriculture,” he said.
In an Oct. 26 statement, United Fresh Produce Association congratulated Goodlatte for passage of the Agriculture Workforce Act (AG Act) out of the House Judiciary committee.
“However, several amendments to the bill that were passed pose significant impediments to securing an adequate guestworker program, “ the statement said, adding that the group will seek improvements in the legislation.
“ Agriculture must be assured that our current workforce has a path forward toward legal stability and security and we have a workable future agriculture guestworker program,” United Fresh said.