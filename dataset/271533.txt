The Latest: Environmental groups oppose Enbridge ruling
The Latest: Environmental groups oppose Enbridge ruling

The Associated Press

MINNEAPOLIS




MINNEAPOLIS (AP) — The Latest on Enbridge Energy's proposed Line 3 replacement (all times local):
6:20 p.m.
Environmental and tribal groups are criticizing an administrative law judge's recommendation that Minnesota regulators should approve Enbridge Energy's proposal for replacing its aging Line 3 crude oil pipeline if it follows the existing route rather than the company's preferred route.
They oppose Enbridge building the project, regardless of what route it takes.
The director of the Sierra Club's Minnesota chapter, Margaret Levin, says the Public Utilities Commission should listen to thousands of Minnesota residents who have marched, submitted comments and testified against Line 3, and reject it once and for all.
Tara Houska, national campaigns director of Honor the Earth, says tribes have made it "crystal clear" that a new line is not acceptable.
Greenpeace USA campaigner Rachel Rye Butler says tar sands pipelines carry too much environmental and economic risk.
___
5:15 p.m.
An administrative law judge says Minnesota regulators should approve Enbridge Energy's proposal for replacing its aging Line 3 crude oil pipeline only if it follows the existing route rather than the company's preferred route.  
Administrative Law Judge Ann O'Reilly recommended Monday that the Public Utilities Commission choose the existing route, which avoids sensitive areas in the Mississippi River headwaters region where American Indians harvest wild rice and hold treaty rights. The proposal has drawn opposition because the line would carry Canadian tar sands crude.  
The commission is expected to make its final decision in June.  
Line 3 was built in the 1960s. Alberta-based Enbridge says a replacement is needed to ensure reliable deliveries of crude to Midwestern refineries. It has said proposed route alternatives are unworkable.
___
9 a.m.
An administrative law judge is due to recommend whether Minnesota regulators should approve Enbridge Energy's proposal for replacing its aging Line 3 crude oil pipeline across northern Minnesota.
The proposal has drawn strong opposition because the line would carry Canadian tar sands crude across environmentally sensitive areas in the Mississippi River headwaters region where American Indians harvest wild rice and hold treaty rights.
Enbridge, based in Calgary, Alberta, says the project is necessary to ensure the reliable delivery of crude to Midwestern refineries. The existing line, which was built in the 1960s, can run at only half its original capacity.
Administrative Law Judge Ann O'Reilly's recommendations, expected Monday, on whether the line is needed and routing, should guide the Minnesota Public Utilities Commission when it announces its decision in June.