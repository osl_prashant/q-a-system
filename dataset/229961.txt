BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Friday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMar                              121.00  Up    .10Mar      119.80  121.00  118.75  119.45  Down  .15May                              123.15  Up    .15May      120.90  122.80  120.60  121.00  Up    .10Jul      123.20  124.80  122.70  123.15  Up    .15Sep      125.25  127.00  125.05  125.35  Up    .10Dec      128.70  130.35  128.45  128.65  Up    .05Mar      131.80  133.35  131.80  131.95  unchMay      134.50  134.60  133.95  133.95  Down  .05Jul      136.25  136.35  135.75  135.75  Down  .05Sep      137.90  137.95  137.40  137.40  Down  .10Dec      140.40  140.45  139.90  139.90  Down  .05Mar      142.90  142.90  142.45  142.45  Down  .05May                              144.20  Down  .05Jul                              145.90  unchSep      147.80  147.80  147.50  147.50  unchDec                              149.80  Up    .15