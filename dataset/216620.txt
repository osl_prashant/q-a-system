New Zealand dairy co-operative Fonterra said on Thursday it expected lower milk collection for the 2017/18 season, hurt by dry weather sweeping the country.
Fonterra said in a statement it was reducing its milk collection forecast for the season to 1,525 million kilograms of milk solids (kgMS), from a June forecast of 1,575 million kgMS.
It also said its dairy exports from New Zealand fell 7 percent in October, facing pressure from export growth from the European Union.
Fonterra’s exports from the United States also took a hit, falling 10 percent. Australian exports, however, remained resilient compared with last year.
Earlier this month, Fonterra reduced its forecast payout to farmers for the current season by NZ$0.35 to NZ$6.40, highlighting the downward pressure the step-up in European Union production was creating on global dairy prices.
However, with strong global demand and dry weather conditions in New Zealand likely to curb its production, the outlook was not considered dire.
On Thursday, the company added that milk production in November in New Zealand and Australia rose 4 percent and 7 percent respectively.