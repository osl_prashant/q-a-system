The outlook for the summer and fall supply of mushrooms is strong, and promotional volumes will be available, barring any unforeseen production issue, said Daniel Rahn, project manager with the Avondale, Pa.-based American Mushroom Institute.“The industry does not anticipate any supply interruptions and expects quality to remain high for the second half of the year,” Rahn said.
Mushroom production continues to increase because of a strong industry focus on the uses and benefits of fresh mushrooms, Rahn said.
“The various successes of association and grower marketing programs have resulted in sales and production growth across many segments and varieties for both organic and conventionally grown mushrooms, benefiting the entire fresh mushroom industry,” he said.
The strongest growth areas include browns, organics and specialty mushrooms, Rahn said.
 
Labor concerns
One concern — common across the produce industry — is the availability of workers for a labor-intensive crop, Rahn said.
“Growers and shippers continue to face the challenges of labor availability and increased production costs, which are the driving forces in managing productivity,” he said.
As has occurred with other commodities, worker shortages have led to unharvested mushroom crops, Rahn said.
“Labor shortages can disrupt harvesting schedules and result in lost production, which is especially critical around the holiday season when sales volumes increase,” he said.
“Growers in all regions say it is becoming more and more difficult to find and retain labor, driving up wages.”
Labor probably is the chief concern, said Vince Versagli, sales director with Kennett Square, Pa.-based South Mill Mushroom Sales and Kaolin Mushroom Farms Inc.
“Production has been good, especially through this second quarter of the year. I don’t see any production issues that would impact supply, as happened in the third quarter of last year,” he said.
Growers are dealing with other rising costs, as well, Rahn said, listing energy, regulatory compliance and raw materials among the concerns.
 
Summer doldrums
All told, production is stable, said Kevin Donovan, sales manager with Kennett Square-based Phillips Mushroom Farms.
“Our summertime production isn’t nearly what we would like it to be, but that’s normal for summertime production,” Donovan said. “There’s no problems — it’s just tougher to grow in the summertime."
A year ago, the industry was voicing more concern about supplies for the fall and winter holiday marketing season.
Lower-than-anticipated mushroom supplies — 15% to 20% lower, by some estimates — had caused some worries among marketers as Thanksgiving and Christmas approached in 2016.
“It’s better than it was,” said Gale Ferranto, president of Landenberg, Pa.-based Buona Foods Inc.
But there is some concern, she said.
“Our prediction here is that we’re going to see some shortage in the market,” Ferranto said. “The growing conditions in summer are just compromised in hot weather.”
Ed Wuensch, salesman with Gonzales, Texas-based Kitchen Pride Mushroom Farms Inc., agreed.
“Production has remained strong so far this summer, but supply will begin to tighten up as we head into the fall,” he said.
The forecast for Watsonville, Calif.-based Monterey Mushrooms Inc. was brighter, thanks to a number of production facilities across North America, said Mike O’Brien, vice president of sales & marketing.
“The crop outlook for Monterey Mushrooms is outstanding, since we have 10 farms strategically located around the United States and Mexico,” he said. “We make our own compost, so we grow mushrooms end to end.”
Late summer and early fall is a “soft market” for Olympia, Wash.-based Ostrom Mushroom Farms, said Fletcher Street, sales and marketing director.
“In the Pacific Northwest, we get stone fruits and the tomatoes that we haven’t seen all winter, and everybody gets excited, so it’s a little softer than I would say is normal,” she said.