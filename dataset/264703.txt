Wisconsin researchers aim to help cranberry industry
Wisconsin researchers aim to help cranberry industry

By BARRY ADAMSWisconsin State Journal
The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — Cranberries are so entrenched in Wisconsin that there are significant research efforts under way in the state to support the industry.
Last year, the Wisconsin Cranberry Research & Education Foundation, founded in 1999, purchased about 155 acres of land near Black River Falls in Jackson County to create a $1.5 million cranberry research station supported by the U.S. Department of Agriculture.
The facility, scheduled to be completed later this year, will include about 30 acres of cranberry beds where researchers can develop and refine growing practices.
Meanwhile, at UW-Madison, research is happening on multiple fronts.
Juan Zalapa, a USDA scientist, studies the genetics and genome sequencing of cranberries to improve production systems and the quality of the fruit.
Over in computer and electrical engineering, professors Susan Hagness and John Booske have developed a prototype device that counts cranberries before they are harvested by analyzing microwaves bounced off the ground and through the plants.
The research of Amaya Atucha, an assistant professor and Gottschalk Chair for cranberry research in the university's horticulture department, focuses on how cranberry plants are able to withstand subfreezing temperatures during winter, as well as strategies to reduce the impact of frost and winter stress in cranberry plants.
She's also studying cranberry flower development and seasonal patterns of root growth, which could help farmers better manage irrigation, fertilizer and other input costs.
"A lot of the work we do is understanding how the plant works," Atucha told the Wisconsin State Journal .
She recently hosted the board of the Wisconsin State Cranberry Growers Association at her lab.
"Once we know that, then we can target production practices, and we probably can produce cranberries with less resources," Atucha said.
___
Information from: Wisconsin State Journal, http://www.madison.com/wsj


An AP Member Exchange shared by the Wisconsin State Journal.