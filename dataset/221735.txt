The February Rural Mainstreet Index (RMI) reveals a slight improvement for farmers’ financial conditions—increasing to the highest level since May 2014. The monthly survey of bank CEOs in a 10-state Midwest region increased to 54.8 in February. In January, the index was 46.8, which was below the growth-neutral rating of 50.
“Given that fewer than one in four, or 23.9%, of bankers reported economic growth in their area, the solid February reading surprised me,” says Ernie Goss, who chairs Creighton’s Heider College of Business and leads the RMI. “However, weak agriculture commodity prices continue to weigh on the rural economy.”
Bank CEOs were asked how their organizations were dealing with low farm income. Around half said they are increasing collateral requirements, while around 20% said they have rejected a higher percent of loan applications. Around 12% said they have reduced the average size of farm loans. Meanwhile, the remaining third reported no change in their farm lending practices.
February’s farmland index rose to 46.3 from 42.2 in January. This is the highest reading since July 2014, but it is the 51st straight month the index has fallen below growth neutral 50. 
 
Bank CEO Poll: 
How would you describe the economy in your area?

Recession: 2%
Modest Downturn: 38%
No Growth: 36%
Modest Upturn: 19%
Strong Growth: 5%