The US Department of Labor Occupational Safety and Health Administration (OSHA)has made changes to its Injury and Illness Reporting and Recordkeeping requirements in the past two years. In the webinarOSHA Recording Requirement ChangesMary Bauer, Eau Claire Area OSHA Compliance Assistance Specialist, andCheryl Skjolaas, Agricultural Safety and Health Specialist, University of Wisconsin - Madison/Extension, share in the webinar the following topics:Am I required to report an injury or illness to OSHA? Understanding OSHA coverage related to Appropriations Act.
What is a recordable injury or illness and what is first aid?
Severe Accident and Fatality Reportingto OSHA (effective Jan., 1, 2015)
Understanding OSHA Recordkeeping Requirements
What are the different forms?
Example farm injuries and illnesses and how you would record
How long do I keep the files?
New for 2017Electronic Reporting
Informing Employees of their Rights and Anti-retaliation Policy (effective, Nov. 1, 2016)
Incentive Programs or Drug Testing - How they can deter employees form reporting injuries and illnesses and things to consider
Entering the OSHA Log and incident information Electronically and Size of Business Requirements (starting Jan. 1, 2017)
New Penalty Structure(effective, August 1, 2016)
Review of Current Emphasis Programs including2016-2017 Wisconsin Dairy LEPcontinuation
Publications
Resources
Questions and Answers
The OSHA Reporting and Recordkeeping Requirements are federal requirements for agricultural operations with more than 10 employees. For more information on agriculture safety, please visitUW-Center for Agricultural Safety & Healthor extension specialistCheryl Skjolaas.