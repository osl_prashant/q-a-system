In wake of the Thanksgiving Holiday, which, for many, included at least part of Friday of last week, many of our reporting retailers appear to have been out of the office as the vast majority of our retail prices are unchanged. I expect, when we get a full week under our belts, we will get a better indication of fertilizer pricing around the Midwest. We take this week's results with a grain of salt, since many locations went unreported, but overall, most price changes were small by the short ton.

Fertilizer prices were mixed with potash and UAN28% leading declines. 
Our Nutrient Composite Index firmed 1.16 points to 484.53 compared to last year's 492.67. 

Nitrogen

Anhydrous, UAN32% and urea all firmed this week with urea topping the list of gainers across the entire fertilizer segment.
UAN28% fell 35 cents per short ton as 32% added $1.39.
Urea's recent price strength and NH3's rangebound price action, UAN solutions are priced this week below the midpoint between NH3 and our indexed urea figure. This may be a clue that future UAN price movements will be influenced more by anhydrous than by urea.
It must be pointed out that the last time we saw such disparity between anhydrous and urea, with urea at a sharp premium to NH3, anhydrous and UAN solutions eventually followed urea's trend higher until urea topped and retraced what was gained. That took place in winter 2014, and we noted another, less sanguine example of urea leading nitrogen to the upside last winter, beginning about at this time. We do not believe urea's premium to anhydrous will have the same impact as it has historically since most suppliers report strong on-hand supplies, and ample U.S. NH3 production.

Phosphate

Phosphate prices were higher on the week although only by a few cents per short ton.
The DAP/MAP spread narrowed to 5.13 which is still a good 10 points more narrow than the historical norm.
Wholesale DAP and MAP prices firmed at all offshore reporting terminals, but added just $1 at NOLA.
We have not mentioned it in a while, but DAP and MAP still hold a very strong premium to anhydrous ammonia, but since phosphates are more dependent on imported supplies, the disparity is likely to continue. Interestingly, our indexed phosphate price figures are in the ballpark with our indexed urea figure.

Potash 

Potash softened this week on a decline in Kansas prices.
Wholesale potash prices were higher at NIOLA this week, but lower in Brazil, Asia and in the Cornbelt.
With phosphate and urea at a strong premium to anhydrous ammonia, potash has run basically sideways for a few weeks, placing vitamin K very near the midpoint of anhydrous ammonia and indexed urea prices.

Corn Futures 

December 2018 corn futures closed Friday, November 24 at $3.87 putting expected new-crop revenue (eNCR) at $613.22 per acre -- unchanged on the week.
With our Nutrient Composite Index (NCI) at 484.67 this week, the eNCR/NCI spread narrowed 1.16 points and now stands at -128.69.





Fertilizer


11/13/17


11/20/17


Change


Current Week

Fertilizer



Anhydrous


$409.12


$407.10


+73 cents


$407.84

Anhydrous



DAP


$437.25


$440.18


+24 cents


$440.41

DAP



MAP


$447.55


$445.45


+9 cents


$445.54

MAP



Potash



$325.51




$325.82




-66 cents




$325.16


Potash



UAN28


$208.73


$214.56


-35 cents


$214.21

UAN28



UAN32


$235.05


$223.90


+$1.39


$225.29

UAN32



Urea


$339.84


$351.21


+$4.03


$355.24

Urea



Composite


482.26


483.37


+1.16


484.53

Composite