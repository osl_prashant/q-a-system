Discover the value of products on your farm faster by combining efforts with farmers across the U.S., that’s what FarmLogs promises with their new “FarmLogs’ Research Network.” The group plans to give farmers access to benchmarking information they say will help inform their input decisions.
“We’re starting an on-farm research network to make it easy for growers to participate in trials that prove real ROI on different practices and products,” says Jesse Vollmar, FarmLogs CEO. “This is different than other benchmarking tools on the market because we're designing specific protocols that accurately answer the most important questions, and we're validating the results to ensure the protocol is followed.”
Contrary to other products on the market, FarmLogs isn’t crowdsourcing information. Instead, they’re setting up organized trials for “apples to apples” comparison. “It’s benchmarking on steroids,” Vollmar adds.
Adding specific protocol means farmers can evaluate a tested product or practice’s performance with more accuracy, the company says. Without protocol, testing could look like this: Say you’re testing the efficacy of a herbicide, and you spray it on time, but your neighbor is a week late—you’ll have different results. When those results are combined to make an average it doesn’t create an accurate portrayal of the product.
“Misleading information can cost a lot of money,” Vollmar adds.
Testing a new product or practicing with FarmLogs’ Research Network requires a few steps:

You must be involved in FarmLogs—either the free membership or the paid. Currently the company does not plan to charge for access to this benchmarking information.
Talk to a representative from the company and explain what you’re interested in learning more about, and they’ll find other interested farmers to involve in testing.
FarmLogs agronomy experts will set up specific testing protocol each trial’s operator needs to follow.
Interested farmers sign up for the trials in which they would like to participate.
Agronomists work with farmers to place the test in the best location within a farm and field to avoid unintended bias to create statistically valid information.
Throughout the season, agronomists work with trial operations to ensure protocol is followed and watch for possible issues.
Results are recorded at the end of the season and added into the larger data pool to develop conclusive information from the test.

“It’s controlled and better data,” Vollmar says. “If a farmer wanted to do this on their own it would take two to three years. With the network you get it in one.”