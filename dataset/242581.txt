Milk co-op mailing highlights suicide risk for dairy farmers
Milk co-op mailing highlights suicide risk for dairy farmers

By LISA RATHKEAssociated Press
The Associated Press

MONTPELIER, Vt.




MONTPELIER, Vt. (AP) — Accompanying the routine payments and price forecasts sent to some Northeast dairy farmers last month were a list of mental health services and the number of a suicide prevention hotline.
The Agri-Mark dairy cooperative got the resources out to its 1,000 farmers in New England and New York following the suicide of a member farmer in January, and one the year before.
"I know there's a number of farmers out there that are under such tremendous stress that we're worried about that same thing happening," said Bob Wellington, an economist for Agri-Mar Inc., which owns Cabot Creamery.
Farmers are facing their fourth year of payments well below their cost of production, due in part to a national and global oversupply of milk, he said.
Prices paid to farmers hit an average of $24 per hundred pounds of milk in 2014, the highest price since at least 2000. They quickly dropped to an average of about $17 per hundred pounds of milk in 2015, $16 in 2016 and $17 last year.
"They're really getting frustrated, getting concerned and in some cases they're getting almost desperate about how they're going to pay their bills, how they're going to support their families," Wellington said.
The farm prices have no direct correlation with what consumers pay for milk.
According to a 2016 Centers for Disease Control and Prevention report , people working in farming, fishing and forestry had the highest rate of suicide.
Agri-Mark is also creating its own assistance program for its members and isn't alone in its concern.
"Financial duress magnifies some of the personal issues like depression, or anxiety, or something else that's underlying can be triggered by a severe economic downturn in the dairy industry," said Ed Staehr, executive director of NY FarmNet, a free confidential service for New York farmers that offers personal and financial consulting at their homes. Vermont has a similar program called Farm First.
Minnesota has set up a free, 24-hour confidential help line as stress, anxiety, depression, financial burdens, and other mental and emotional problems continue to affect farmers and other rural residents. It also provides workshops for people who work with and serve farmers, such as bankers and suppliers, to understand mental health.
Since the department started promoting the help line in early October through December, 39 calls came in including from a few suicidal farmers and others who called about fights with a spouse, anxiety or physical manifestations of stress, like not sleeping, said Meg Moynihan, a dairy farmer and adviser for the Minnesota Agriculture Department.
"At this time of year, people are doing their taxes and starting to go in for operating loans, and it's kind of terrifying to say, 'Am I going to be able to farm again for a year?'" she said.
Calls have poured in to FarmNet in New York since the end of December. The service is seeing more severe cases in recent months, with farmers realizing they're stressed out. Some lenders are not letting certain farmers have a line of credit, so FarmNet is encouraging farmers to work with their suppliers.
Social worker Erica Lewbner's load has jumped from one new case a month to at least one new case a week, she said.
"Farmers are very proud," said Lewbner, of Marietta, New York. "They typically do not reach out for help."
Beth Kennett, of Liberty Hill Farm in Rochester, Vermont, said she and her family make decisions every day to keep the farm going, something they want to do for future generations. In addition to dairy farming, they offer farm vacations to visitors.
"Faith and fortitude will get us through this difficult time," she said.
Although gut-wrenching, the Agri-Mark letter sparked conversations among farmers and farm families and words of support from the community, she said.
"I think it really initiated conversations on multiple levels within the family individually, but within the farm community, as well," she said, "recognizing that we all are family and we really have to reach out and care for each other."