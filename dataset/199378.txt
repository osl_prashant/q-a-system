Total Recipe Time: HIGH 5 to 6 hours or on LOW 8 to 9 hoursMakes 6 servings
Ingredients
1 beef Shoulder Roast Boneless (2-1/2 pounds)
2 cups chopped onions
1 can (14-1/2 ounces) diced tomatoes with green peppers and onions, undrained
1 cup frozen hash brown potatoes (cubes)
1 cup beef broth
1 tablespoon minced garlic
1 teaspoon dried thyme leaves
1/2 teaspoon salt
1/4 teaspoon pepper
2 cups broccoli slaw
1/2 cup frozen peas


Instructions
Cut beef roast into 12 equal pieces. Place in 4-1/2 to 5-1/2-quart slow cooker. Add onions, tomatoes, potatoes, broth, garlic, thyme, salt and pepper. Cover and cook on HIGH 5 to 6 hours or on LOW 8 to 9 hours or until beef is fork-tender. (No stirring is necessary during cooking.)
Stir in broccoli slaw; continue cooking, covered, 30 minutes or until broccoli slaw is crisp-tender. Turn off slow cooker. Stir in peas; let stand, covered, 5 minutes.