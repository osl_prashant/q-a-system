Nogales, Ariz.-based organic grower-shipper Wholesum Harvest has promoted two employees. 
Steve LeFevre, who has been with the company for more than 17 years, has been promoted to vice president of business development, according to a news release. He was previously sales manager.
Kristina Luna, who started at Wholesum in 2012 as sales coordinator, is now sales manager, according to the release.