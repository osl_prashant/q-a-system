Republican senators say bill to benefit mining company dead
Republican senators say bill to benefit mining company dead

By SCOTT BAUERAssociated Press
The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — Two influential Republican senators said Wednesday a proposal the Assembly passed quickly that would allow a Georgia mining company to destroy wetlands containing rare hardwoods in western Wisconsin is dead.
Republican Sen. Alberta Darling, co-chairwoman of the powerful budget-writing committee, and Sen. Rob Cowles, chairman of the Senate's Natural Resources Committee, both told The Associated Press the votes aren't there to pass the bill exempting Meteor Timber from environmental regulations for its $70 million frac sand processing plan.
Cowles called the bill "ridiculous" and "outrageous" because it would allow Meteor Timber to proceed with its project even as an appeal to its state-issued permit was pending.
"I believe the votes are there to stop it," Cowles said. "I've talked to enough people who understand what this means."
Meteor Timber is an Atlanta-based company that's planning a $70 million plant to process industrial sand near Interstate 94 in Monroe County and ship it to Texas. Environmental groups and others oppose the project, saying Meteor's plans to destroy a 16-acre wetland that includes a 13-acre rare white pine and red maple swamp would cause irreparable harm.
To make up for destroying those wetlands, the company's plans include restoring more than 630 acres of other land, including wetlands, near the 750-acre project site in Millston. Project supporters say it will be an economic boon and job creator for the area.
The Department of Natural Resources in May granted a permit for the project. The Ho-Chunk Nation and Clean Wisconsin appealed, arguing that Meteor's wetland mitigation plan won't make up for the loss of the rare pine-red maple swamp.
Last month, a state administrative law judge heard five days of testimony on the appeal but has yet to rule. The bill pending in the Legislature would allow the project to proceed even as the permit appeal is pending.
The Assembly in February added the provision to a largely technical bill before passing it in the middle of the night. But that bill died in the Senate after Cowles voiced his opposition. Cowles said he objected to the company circumventing the permit process by seeking a law change.
"If you eliminate the need for a permit, where will this end?" Cowles said.
Last week, the Assembly used a stealth move that caught Democrats off guard to revive the bill by adding the Meteor provisions to an unrelated Darling proposal to help ex-convicts get jobs if they aren't a threat to public safety.
Darling said she was disappointed that the addition of the amendment was going to result in her underlying bill not passing.
"I know people in my caucus who are very opposed to that amendment," Darling said. "That will kill my bill."
Senate Majority Leader Scott Fitzgerald refused to say Wednesday that the bill has no chance. He said it is "not dead," but he didn't know yet if there are enough votes to pass it in the Senate. Republicans have an 18-14 majority, meaning if more than one Republican opposes it and Democrats remain unified against it, the measure will not have enough votes to pass.
"I haven't done a nose count on the bill at all," Fitzgerald said.
The Senate plans to meet next week to vote on a bill that would restrict when the governor could call special elections, but could also take up other proposals. Fitzgerald said he did not know yet what other bills the Senate may take up.
___
Follow Scott Bauer on Twitter: https://twitter.com/sbauerAP