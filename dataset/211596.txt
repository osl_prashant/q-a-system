The Chilean Citrus Committee recently partnered with U.S. Asian supermarket chain 99 Ranch Market’s Culinary Adventure Program to teach kids proper food safety and food prep techniques. 
In a Chilean Citrus cooking class, kids learned to make a Chilean mandarin jelly roll with orange dipping sauce and received an apron and Chilean fruit activity books, according to a news release.
 
The citrus committee has sponsored more than 100 kids cooking classes at Southern California retailers, according to the release.
 
“Kids cooking classes are a wonderful opportunity for us to get kids excited about and engaged with our fruit,” Karen Brux, managing director for the Chilean Fresh Fruit Association, said in the release.
 
99 Ranch Market is the largest Asian supermarket chain in the U.S., with 46 stores on the West and East coast. The company recently began offering the Tawa Culinary Adventure program for kids in four locations and it has proved very popular, according to the release.
 
“Eating fruits like Chilean mandarins provides numerous health benefits. We’re happy to be teaching our little chefs how to cooks dishes that are not only tasty, but also incredibly healthy,” a 99 Ranch spokesman said in the release.
 
Chilean citrus continues through October. By the week of Aug. 28, the country had shipped more than 52,000 tons of mandarins to the U.S., an 84% increase from the same time last year, according to the release.