Corn 

 


 
 

2015-16
			USDA 


2016-17
			USDA 


2016-17
			PF/Doane 


2017-18 USDA


2017-18
			PF/Doane



 Planted (mil. acres)

88.0


94.0


94.0


90.0


90.0



 Harvested (mil. acres)

80.8


86.7


86.7


82.4


82.4



 Yield (bu./acre)

168.4


174.6


174.6


170.7


171.0



 
 

million bushels



Beginning stocks

1,731


1,737


2,295


2,295


2,295



Production

13,602


15,148


15,148


14,065


14,090



Imports

67


55


55


50


45



Total Supply

15,401


16,940


16,940


16,410 


16,430



 
 

 


 


 


 


 



Feed & Residual

5,123


5,500


5,475


5,425


5,600



Food, Seed, Industrial

6,643


6,920


6,920


7,000


6,940



   Ethanol for Fuel*

5,224


5,450


5,450


5,500


5,465



Total Domestic Use

11,766


12,420


12,395


12,425


12,490



Exports

1,898


2,225


2,250


1,875


1,825



Total Use

13,664 


14,645 


14,645 


14,300 


14,315 



 
 

 


 


 


 


 



Carryover

1,737


2,295


2,295


2,110 


2,115



Stocks-to-Use Ratio

12.7%


15.7%


15.7%


14.8%


14.8%



Proj. avg. price per bu.

$3.61


$3.40


$3.40


$3.40


$3.45




* 'Ethanol for Fuel' is included in the Food, Seed, Industrial total. 

 








Soybeans

 


 
 

2015-16
			USDA 


2016-17
			USDA 


2016-17
			PF/Doane 


2017-2018 USDA 


2017-18
			PF/Doane



 Planted (mil. acres)

82.7


83.4


83.4


89.5


89.5



 Harvested (mil. acres)

81.7


82.7


82.7


88.6


88.6



 Yield (bu./acre)

48.0


52.1


52.1


48.0


47.7



 
 

million bushels



Beginning stocks

191


197


196.7


435


445



Production

3,926


4,307


4,306.7


4,255


4225



Imports

25


24


25


25


20



Total Supply

4,140


4,528


4,528


4,715 


4,690



 
 

 


 


 


 


 



Crush

1,886


1,925


1,900


1,950


1,930



Exports

1,936


2,050


2,075


2150


2,100



Seed

97


104


104


101


102



Residual

25


14


4.4


34


33



Total Use

3,944


4,093


4,083


4,235 


4,165



 
 

 


 


 


 


 



Carryover

196.7


435


445


480 


525



Stocks-to-Use Ratio

5.0%


10.6%


10.9%


11.3%


12.6%



Proj. avg. price per bu.

$8.95


$9.55


$9.55


$9.30


$9.00




 








Wheat

 


 
 

2015-16
			USDA 


2016-17
			USDA 


2016-17
			PF/Doane 


2017-2018 USDA 


2017-18
			PF/Doane



 Planted (mil. acres)

55.0


50.2


50.2


46.1


46.1



 Harvested (mil. acres)

47.3


43.9


43.9


38.5


38.6



 Yield (bu./acre)

43.6


52.6


52.6


47.2


47.3



 
 

million bushels



Beginning stocks

752


976


976


1,159


1,140



Production

2,062


2,310


2,310


1,820


1,824



Imports

113


115


115


125


120



Total Supply

2,927


3,400


3,400


3,105 


3,084



 
 

 


 


 


 


 



Food

957


955


955


955


958



Seed

67


61


61


66


63



Feed

152


190


204


170


195



Total Domestic Use

1,176


1,206


1,220


1,191


1,216



Exports

775


1,035


1,040


1,000


985



Total Use

1,952


2,241


2,260


2,191


2,201



 
 

 


 


 


 


 



Carryover

976


1,159


1,140


914 


883



Stocks-to-Use Ratio

50.0%


51.7%


50.4%


41.7%


40.1%



Proj. avg. price per bu.

$4.89


$3.90


$3.90


$4.25


$4.35




 

 








Cotton

 


 
 

2015-16
			USDA 


2016-17
			USDA 


2016-17
			PF/Doane 


2017-18
			USDA 


2017-18
			PF/Doane



 Planted (mil. acres)

8.58


10.07


10.07


12.23


12.23



 Harvested (mil. acres)

8.07


9.52


9.51


11.38


11.23



 Yield (lb./acre)

766


869


867


810


820



 
 

million bales



Beginning stocks

3.65


3.80


3.80


3.20


2.90



Production

12.89


17.17


17.17


19.20


19.19



Imports

0.03


0.01


0.01


0.01


0.01



Total Supply

16.57


20.98


20.98


22.41 


22.10



 
 

 


 


 


 


 



Domestic use

3.45


3.30


3.30


3.40


3.40



Exports

9.15


14.50


14.80


14.00


14.00



Total Use

12.60


17.80


18.10


17.40 


17.40



 
 

 


 


 


 


 



Carryover

3.80


3.20


2.90


5.00 


4.70



Stocks-to-Use Ratio

30.2%


18.0%


16.0%


28.7%


27.0%



Proj. avg. price per bu. (cents/lb.)

61.20


69.00


69.00


64.00


65.00