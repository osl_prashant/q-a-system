(CORRECTED) TORONTO — The demand for fresh-cut fruit and vegetables continues to grow as Toronto restaurants look for ways to pare labor costs and millennials demand fresh, healthy convenience at home.
“With the minimum wage increase we’re seeing lots of demand for value-added products as chefs and restaurateurs look for back-of-house solutions to optimize labor efficiency,” said Ezio Bondi, business development manager for Bondi Produce, which launched its fresh-cut New Toronto Food Co. last year.
At Gambles Produce, meanwhile, packaged and fresh-cut produce under the company’s Go Fresh brand has been growing at a rapid rate, said Tom Kioussis, vice president of sales and marketing.
“The customization of packs provides our clients with tailor-made solutions,” Kioussis said.
“For instance, we’re now pre-portioning cut vegetables in 6-ounce packs for foodservice to eliminate the prep work and weighing of portions in the back.” 
Noel Brigido, vice president Mississauga-based Freshline Foods Ltd., said spiralized vegetables such as sweet potatoes and zucchini, along with riced vegetables, continue to lead his fresh-cut pack.
The value-added processor is also bringing back concepts that may have been ahead of their time when first launched.
“In the next 30 to 60 days we’re introducing a line of Steam Its — vegetable mixes in a micro-safe tray covered with breathable film that’s ready in less than five minutes,” Brigodo said.
“We’re also doing Roasters for the oven, different vegetable blends, including spiralized vegetables, in ready-to-roast aluminum containers.”
He sees these quick, healthy side dishes as one way retailers can catch the wave created by the boom in meal kit companies, led in Canada by Chefs Plate.
Pre-portioned meals also fulfil millennials’ desire for convenience with little waste.
“I think young people have time to cook but they don’t want to shop for 40 ingredients,” Brigido said.
Along with its regular 30-minute chef-inspired meals, Chefs Plate introduced 15-minute meals last month with great success.
“The new kits are more popular than we thought they’d be,” said co-founder Jamie Shea, “and they’re healthier than take-out.”
Recent 15-minute options included Thai peanut lime noodle soup with chopped baby bok choy, diced sweet potato and trimmed snap peas.
A staff of five full-time chefs develops 14 new recipes each week for the company’s land, sea and increasingly popular vegetarian options, such as chipotle cauliflower chickpea tacos with guacamole.
Shea said the company purchases fresh-cut vegetables from local vendors and sources them directly from local farms and greenhouses to process in its fulfillment centers across Canada. 
Note on correction: The original version of this story incorrectly indicated the number of new recipes developed by Chefs Plate chefs each week.