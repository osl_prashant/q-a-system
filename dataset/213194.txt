Measuring Farm IncomeTwo methods of measuring farm income are used and reported on by the United States Department of Agriculture (USDA) and other reporting sources, Net Cash Farm Income and Net Farm Income. While the two use very similar words, the formulas and use of them is different, and should be understood when reading reports regarding one or the other to discuss and understand the current farm income situation.
Net Cash Farm IncomeNet cash farm income (NCFI) is calculated by USDA by adding crop receipts to livestock receipts to show total receipts from farming, add in government payments and other cash income to total establish gross cash income. Then subtract the total gross cash expense, including interest expense.
Net cash farm income is a financial indicator and can be used by individual producers to monitor profitability of their operations. More on utilizing the net cash farm income ratio can be found in Financial Measures for South Dakota Farms.
Net Farm IncomeNet farm income (NFI) utilizes the same data used to determine gross cash income. Then, an accrual adjustment is made to indicate net inventory changes. Gross cash expense is a total of expenses, depreciation and requirements for hired labor. Subtracting gross cash income from the gross cash expenses utilizing this information provided the NFI. Net farm income is utilized in macro-economic evaluation of the farming sector.
2017 Farm Sector Income Forecast
In February USDA Economic Research Service (ERS) release their 2017 Farm Sector Income Forecast. Nationally the net cash farm income (NCFI), is forecasted to total $93.5 billion, a $1.6 billion (1.8 percent) increase from 2016 totals. These totals cover all agricultural sectors across the nation.
On the other hand, net farm income (NFI) is forecast to decline by 8.7 percent to $62.3 billion. The variation in these similar sounding formulas is due to the change in crop inventory held on the farm. There is a forecasted $8.2 billion in the sale of crop inventories, which will increase the gross cash income in both formulas, however the change in inventories will also be reflected in the NFI accrual adjustment.
For comparison, the national NCFI and NFI levels peaked above $120 billion in 2013 (Figure 1) with the record corn, soybean, feeder cattle and live cattle prices (Figure 2, Figure 3).
South Dakota Perspective
NFCI & NFIAs mentioned above, both NFCI and NFI are reported for all agricultural sectors, many of which do not affect South Dakota. Nationally, cash receipts for most of South Dakota’s main sectors are expected to decrease, compared to 2016 levels.
Corn-receipts decrease 0.8 percent. The percentage decrease is relatively small due to the offsetting forecast of increased bushels sold and the declining price.
Wheat-receipts decrease 16.6 percent, due to a combined reduction in price and quantity sold.
Soybeans-receipts increase 1.1 percent amid a decrease in quantity sold.
Cattle and calves-receipts decrease 6.7 percent due to price decline.
Hogs-receipts decrease 4.7 percent due to price decline.
Milk-increase 13.7 percent due to increased production sold and increase in prices.
ExpensesExpenses were also forecast for 2017, compared to 2016. Overall the total gross expense number is expected to be similar to 2016 levels, with variation in line item changes.
Livestock and poultry purchases-decrease due to the reported decrease reflected in the cash receipts above, mainly due to the decrease in feeder cattle prices.
Fuel and oil- decrease 13.2 percent due to an increase in diesel fuel price.
Seed, Pesticide and Fertilizer- decrease 5.4 percent due to fewer acres being planted.
 Labor-increase 5.5 percent amid wage rate increases.
Interest-increase 15 percent due to higher debt loads being carried and an increase in the interest rate.
Net rent expense (rent adjusted for any income the owner may receive in the form of government payments or share agreements)-2.7 percent increase from higher rental rates.
Monitor & Analysis
 
Throughout 2017-2019, it will be more important than ever for South Dakota producers to have a good understanding of their operations overall financial standing. The decreases in receipts forecasted, without major decreases in expense categories for the main commodities produced indicate short income years are in the near future.



 
Disclaimer: The information in this article is believed to be reliable and correct. However, no guarantee or warranty is provided for its accuracy or completeness. This information is provided exclusively for educational purposes and any action or inaction or decisions made as the result of reading this material is solely the responsibility of readers. The author(s) and South Dakota State University disclaim any responsibility for loss associated with the use of this information.