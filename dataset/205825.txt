Crop calls
Corn: Fractionally to 1 cent lower
Soybeans: 1 to 2 cents lower
Wheat: 1 to 4 cents higher
Corn and soybean futures were weaker overnight as another round of showers was brewing across the western Corn Belt, although more scattered in coverage than recent events. Corn futures are working on weekly losses and are oversold, which should limit pressure to end the week. Meanwhile, soybeans are working on weekly gains due to positive demand news and disappointing overall pod counts from the Midwest Crop Tour. Wheat futures were lifted overnight by outside markets, with the dollar weaker and crude oil firmer.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Livestock futures are expected to see a mixed tone this morning on a combination of short-covering and a bearish mindset. Traders are also focused on evening positions ahead of the weekend and this afternoon's Cattle on Feed Report. Futures are trading at a discount to their respective cash markets, but given building supplies, traders are comfortable with some discount priced in, which limits buying interest.