On Thursday, President Trump is hosting a meeting at the Whitehouse with the renewable fuels and oil industries to get a direction of the Renewable Fuel Standard (RFS).

This week at Commodity Classic in Anaheim, California, Perdue pledged his support for the RFS, and said Trump “stands with corn farmers, and he stands with biofuel farmers and stands for the RFS.”

Bob Dinneen, president and CEO of the Renewable Fuels Association, said the message from Washington on Tuesday was a matter of them “kick[ing] the can down the road.”

“Our advocates are in there fighting for rural America,” he told Chip Flory on AgDay.

Dinneen thinks there’s a “false premise” out there that refinery jobs and ethanol jobs can’t coexist. He believes both have the ability to “thrive.” Dinneen told Flory he expects this conversation to last throughout the summer.

Hear his full comments on AgDay above.