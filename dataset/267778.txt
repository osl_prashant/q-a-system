Immigration raid takes 97 into custody at Tennessee plant
Immigration raid takes 97 into custody at Tennessee plant

By SHEILA BURKEAssociated Press
The Associated Press

NASHVILLE, Tenn.




NASHVILLE, Tenn. (AP) — Civil rights activists say a dramatic immigration raid at a Tennessee meat processing plant may be the biggest employment crackdown under President Donald Trump's administration.
U.S. Immigration and Customs Enforcement spokeswoman Tammy Spicer says in a Friday statement that the agency took 97 people into custody Thursday after executing a search warrant at Southeastern Provision, a meat processing plant in Bean Station in eastern Tennessee.
The National Immigration Law Center says it is believed to be the largest single workforce raid under the Trump administration. A federal immigration official declined to confirm whether it was the largest.
A statement from immigration officials says 11 people were arrested on criminal charges, 54 others have been placed in detention and 32 have been released from custody.
A total of 21 people were arrested after immigration agents raided 7-11 stores nationwide in January.