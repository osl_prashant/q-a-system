Lower ocean freight and fruit costs combined with higher retail prices for bananas and other fresh produce helped boost profits by 35% for Fresh Del Monte Produce in 2016.
 
While net sales were marginally lower for the year and the quarter, the company reported Nov. 21 that net income and gross profit for the year and the quarter were sharply higher.
 
The Coral Gables, Fla.-based Del Monte said that full year 2016 net income was $225.1 million, up 261% compared with 2015.  Gross profit for 2016 was $461.4 million, 35% higher compared with the gross profit of $342.3 million in 2015, to a news release.
 
The better performance was attributed to higher gross profit in all of the company's business segments, helped by lower ocean freight and fruit costs, according to the release.
 
In addition, the company reported higher selling prices for some fresh produce items and banana business segments.
 
"Our positive performance during the fourth quarter and excellent results for the full year demonstrate our ongoing efforts to drive diversification across all of our businesses," Mohammad Abu-Ghazaleh, chairman and CEO, said in the release. "Throughout the year, we remained focused on reinforcing our global business platform with a series of strategic initiatives that enhanced our higher-margin portfolio of products and businesses, achieving strong results and growth in our fresh-cut product line with new facilities globally, product line extensions and expanded production areas."
 
The company also added foodservice sales, he said in the release.
 
"Looking forward, we see tremendous opportunities for growth, while remaining focused on the bottom line and increasing shareholder value for the long-term," Abu-Ghazaleh said in the release.
 
With net banana sales of $1.81 billion in 2016, the company reported that bananas accounted for 45% of sales and 35% of profits.
 
"Other fresh produce" accounted for $1.85 billion in net sales in 2016, according to the release. That category accounted for 46% of total 2016 sales and 51% of profit for the year, according to the release.
 
Fresh Del Monte's "prepared food" segment represented $374 million in sales, according to the release. That equaled 9% of total sales and 14% of profits, according to the company.
 
For 2016, Fresh Del Monte reported 55% of its sales occurred in North America, with 17% of sales in Europe, 14% in the Middle East and 12% in Asia.