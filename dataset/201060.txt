Fertilizer prices were mixed... sort of. All fertilizers were lower with the exception of potash which firmed 2 cents on the week.
Our Nutrient Composite Index softened 4.58 points on the week, now 51.29 points below the same week last year at 525.33.

Nitrogen

Anhydrous ammonia led declines in the nitrogen segment by a ratio of two to one compared to the rest of the segment.
This week's downside rush in anhydrous ammonia has widened our margins in the N segment. Sine the other three nitrogen products in our survey are lower this week, NH3 appears to be a weight on the entire segment.
UAN has been on our radar in light of early season nitrogen loss and the amount of replanting going on across corn country. This week, only Kansas UAN32% is higher this week, suggesting either more than ample UAN supplies or the end of peak demand for UAN.

 
Phosphate

Phosphates were lower with MAP retracing last week's climb and then some as DAP fell another few dimes.
The DAP/MAP spread is at 13.99 this week, which is at the low end of our range of expected margins.
Phosphates remain overpriced compared to the rest of the fertilizer segment. Since nitrogen is lower across the board this week, we may see DAP/MAP continue to soften since nitrogen appears unwilling to right the balance between itself and phosphate..

Potash 

Potash was our only gainer this week but added just 2 cents per short ton.
Just a few states firmed during the week.
We recommend a hand-to-mouth approach on potash for now, as with the rest of the fertilizers in our survey, but potash fundamentals, more than any other fertilizer, will confine vitamin K to sideways price action for the time being.

Corn Futures 

December 2017 corn futures closed Friday, June 16 at $4.02 putting expected new-crop revenue (eNCR) at $638.64 per acre -- lower $6.77 on the week.
With our Nutrient Composite Index (NCI) at 525.33 this week, the eNCR/NCI spread widened 26.90 points and now stands at -113.31. This means one acre of expected new-crop revenue is priced at a 113.31 premium to our Nutrient Composite Index.





Fertilizer


6/5/17


6/12/17


Week-over Change


Current Week

Fertilizer



Anhydrous


$518.21


$515.53


-$7.69


$576.84

Anhydrous



DAP


$447.74


$447.38


-23 cents


$447.14

DAP



MAP


$463.65


$466.87


-$5.75


$461.13

MAP



Potash


$333.88


$333.12


+2 cents


$333.14

Potash



UAN28


$252.25


$249.30


-$3.20


$246.11

UAN28



UAN32


$273.21


$273.80


-$2.54


$271.26

UAN32



Urea


$346.53


$343.12


-$3.54


$339.58

Urea



Composite


531.40


529.91


-4.58


525.33

Composite