Wildfires have burned wide swaths of the western U.S. the past week forcing evacuations of communities, cattle to move pastures and highway closures.Fires are currently active in nine states throughout the West, according to the National Interagency Fire Center. Here is a breakdown of the acreage burnt so far in active wildfires reported by the National Interagency Fire Center since Aug. 28:
Arizona, 1 wildfire, burning 48,443 acres
California, 22 wildfires, burning 354,316 acres
Colorado, 1 wildfire, burning 1,405 acres
Idaho, 19 wildfires, burning 248,141 acres
Montana, 26 wildfires, burning 544,583 acres
Nevada, 7 wildfires, burning 111,379 acres
Oregon, 9 wildfires, burning 146,418 acres
Utah, 1 wildfire, burning 5,097 acres
Wyoming, 2 wildfires, burning 4,766 acres
For those active fires reported on since Aug. 28 it amounts to 1,464,548 acres actively burnt or burning.

Earlier in the summer there were large wildfires that went across Montana, like the Lodgepole Complex which at its last reporting burned 270,723 acres in the eastern portion of the state.
Another series of wildfires popped up around Labor Day weekend in eastern Montana where many cattle ranches are located. The Sartin Draw Fire has burned nearly 100,000 acres after igniting on Aug. 30, and thanks to work of fire fighters it is at 95% containment in just six days.
Two fires near Sheridan, Wyo. have merged along the Wyoming and Montana state borders burning more than 90,000 acres. The fire is being called the Battle Complex and was formed by the 73,285-acre Brush Flat fire and the 17,160-acre Tidwell fire. None of those fires is mentioned on National Interagency Fire Center website, but the organizations map show the burn area split almost evenly between Montana and Wyoming.
The Battle Complex fires went across land the Padlock Ranch Co. grazes. According to the ranch’s Facebook page no livestock, people or homes were lost to the fire.

Interstate 84 closed in Oregon after the Eagle Creek Fire began to spread. The highway is closed to traffic for more than 80 miles from Troutdale to The Dalles, Ore. The Eagle Creek Fire has burned 10,000 acres and it is expected to be contained by Sept. 30.


The La Tuna fire made national headlines over the weekend after it burned 7,000 acres near Los Angeles, Calif. The fire forced 700 people to evacuate suburbs like Glendale and Burbank. It is reported to be the largest wildfire in Los Angeles County’s history.
Drought has caused many of the wildfires in Montana where 24.55% of the state is experiencing exceptional drought, the worst classification drought by the National Drought Mitigation Center. Nearly all of the state is in moderate drought at 90.2%. Other states in the West are starting to see drier conditions with the entire state of Washington classified as abnormally dry and neighboring Oregon at 77.67% abnormally dry. Idaho has 56.52% of the state classified as abnormally dry.
North Dakota and South Dakota are both continuing to endure drought conditions, but have not experienced wildfires this summer. Both states have improved slightly since the last report. South Dakota is reporting 68.85% moderate drought and North Dakota has 65.84% moderate drought as of Aug. 29.