The U.S. Department of Agriculture (USDA) released its expectations for 2018 and beyond at its annual outlook forum in Washington D.C. earlier this week.
The Agricultural Outlook Forum is the USDA’s largest annual meeting, and upwards of 2,000 people attend.
USDA expects corn and soybeans will both hit about 90 million acres this year, but soybean acres could edge out corn.

Both crops are a bit lower than last year’s acreage, and USDA economists believe this could be because increasing demand from China. 
AgDay national reporter Betsy Jibben reports on USDA’s chief economist Robert Johansson breaking down his projections on price.