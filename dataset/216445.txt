Even though the drive to reduce the use of antibiotics has had a greater impact in the poultry, pork and beef industries than it has had in dairy, significant pressure remains to use less antibiotics in milk production.
In addition to the outside social pressures from consumers to use less antibiotics, there might be economic reasons for treating fewer animals. But that depends on how producers look at the expense, says Pam Ruegg, DVM, milk quality specialist at the University of Wisconsin-Madison.
“All treatment is losing money, it just depends on how much you want to lose,” Ruegg says. “But money spent on prevention is an investment.”
The two ways to reduce antibiotic use is to change how animals are treated for mastitis and dry cow therapy, Ruegg says. To alter the process for deciding when animals should be treated, Ruegg recommends using the three D’s.
1. Detection
Using somatic cell count (SCC) data and cow history, detect when the cow was infected. “Information about how long the SCC has been high helps us guess at the type of bacteria creating the infection,” Ruegg says. Gram positive infections are usually subclinical for a long period, and antibiotics might be useful. Gram negative infections are usually subclinical for a shorter period of time and most don’t benefit from antibiotic therapy. “This isn’t a perfect method, but it gives us a clue,” she says.
Detecting when cows are infected requires accurately recording clinical cases and severity scores, Ruegg says. “Chronic cows have a low probability of being cured by antibiotics,” she says.
2. Diagnosis
Determine if the clinical signs you see are signs of an infection or just inflammation. The only way to know what’s really going on in the udder is by culturing milk samples from the affected cow. Ruegg estimates about one third of the culture results will come back as no growth. “
In most clinical cases the cow will take care of the infection,” she says. “A no growth result from a clinical incident is an indication that the inflammation was successful in containing the infection.”
The remaining culture results will normally be split evenly between gram positive and gram negative bacteria. Ruegg says 85% to 90% of gram negative infections will be back to normal by day seven. Gram positive cases are usually the ones that need treatment.
3. Decision
With the data—culture results, SCC scores, cow history—can you justify antibiotic use on this cow? That question can be asked of lactating cows and those that are near dry off.
Ruegg says antibiotics are rarely useful for cows with these histories:

One or more previous diagnoses of refractory pathogens.
Greater than three previous treatments for clinical mastitis.
Greater than four months of SCC scores higher than 200,000 cells/mL.
Chronic clinical or subclinical mastitis in previous lactations.

If cows do not meet these criteria, and a culture shows the associated infection would be cured by antibiotic therapy, then decide which antibiotic to use. For a chart that shows antibiotics available on the market, their efficacy and dosage visit www.DairyHerd.com/ mastitisantibiotics.
The same question can be asked of dry cow therapy, the goal of which, according to Ruegg, is to either cure cows with subclinical infected quarters or prevent new infections from occurring during a high-risk period.
In protocols relating to selective dry cow therapy, Ruegg explains the objective is to use antibiotics only on cows that have evidence of current intramammary infections and to use a teat sealant in healthy cows to prevent new infections during the high risk period after dry off.
“Selective dry cow programs are not simply stopping the use of dry cow therapy,” Ruegg says. She suggests answering two questions to help determine if your herd is ready for selective dry cow therapy:
Q. Has your herd controlled subclinical mastitis?
Review your herd’s bulk tank SCC history. “Herds with bulk tank SCC greater than 250,000 cells/mL shouldn’t even consider selective dry cow therapy,” Ruegg advises, noting those levels are an indicator a considerable portion of cows are affected with subclinical mastitis.
Q Do you have the ability to adequately monitor mastitis?
Do you:

Record and review monthly individual cow SCC values?
Forestrip to identify clinical mastitis?
Keep clinical mastitis records?

If your answer to these two questions is yes, then selective dry cow therapy might be appropriate for your herd.
Still, decisions have to be made on an individual cow basis. Ruegg offers a decision tree to help producers make those individual cow decisions. (You can access the flowchart at www.DairyHerd.com/SDCTflowchart.)
Antibiotic use will continue to be heavily scrutinized on all livestock operations, including dairies. There are opportunities for all dairy producers to practice judicious antibiotic use in both lactating and dry cow therapies.
 
Note: This story appears in the December 2017 magazine issue of Dairy Herd Management.