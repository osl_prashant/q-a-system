California growers are expected to produce 1.7 million 40-pound boxes of fresh apples this year, an increase from 1.5 million boxes last year, said Alex Ott, executive director of the Clovis-based California Apple Commission.           
“California has finally received rain, which is a great relief to growers statewide,” he said.
“As a result, California apple growers are expecting a good year with good quality and more apples.”
Although the state provides only 1% of all U.S.-grown apples, California is the sixth-largest apple producer in the U.S. and the third-largest exporter, he said.
Ott said California is a niche market for apples.
“We pick, pack and ship apples and do not store apples,” he said.
Greene & Hemly Inc., Courtland, Calif., started packing organic apples south of Fresno the last week of July, said owner Doug Hemly.
The Linden area got started the first week of August.
The season started off on schedule with galas, he said, adding that quality “looks good.”
By late August, Greene & Hemly should be shipping braeburns, fujis and a few early granny smith apples, he said. Galas should be finished by that time.
The company will finish the season with Pink Lady and granny smith in late November or early December.
Hemly expects to see a slight increase in volume at his company this season.
“We have a little better set than last year,” he said. “The thinning went well, so we have a nice distribution of sizes.”
He said there were no reports of pests or diseases.
“The crop looks good,” he said.
Although record rainfall fell throughout the state last winter and early spring, Hemly said there “seems to be no adverse effect” on apples from the downpours.
Lodi, Calif.-based Rivermaid Trading Co. started picking July 28, said Kyle Persky, sales manager.
The start date was historically on schedule, but later than the past couple of years, which had an earlier start than usual, he said.
The company grows the gala variety exclusively to complement its pear crop, Persky said.
Although picking was scheduled to end in August, the company expects to ship apples until mid-September.
Persky’s goal is to beat Washington apples to market, he said.
In early August, the season seemed to be progressing nicely.
“Prices are high, and demand is very good,” he said.
On Aug. 3, gala f.o.b. prices on tray packs of size 72s, 80s and 88s were mostly $42.90-46.90, according to the U.S. Department of Agriculture. Size 100s, 113s and 125s ranged from $36.90-42.95.
Primavera Marketing Inc., Linden, Calif., ships four varieties of apples — gala, fuji, granny smith and Pink Lady, said Rich Sambado, sales manager.
The company started picking galas July 24 and planned to start fujis on Aug. 21, granny smiths on Aug. 28 and Pink Lady apples on Oct. 16.
“The California apple deal really is a niche deal starting in late July, with a lot of galas, and we generally try to be done by late November or early December,” he said.
Galas, which should run until late August, “got off to a good start,” he said, with good color and normal sizing.
Volume at Primavera Marketing should be similar to last year, Sambado said. The company now packs about 20% of its crop in bags, ranging from 3 pounds for mainstream markets to 6 pounds primarily for club stores.