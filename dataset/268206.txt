Michigan dairy farm shutters amid industry oversupply
Michigan dairy farm shutters amid industry oversupply

The Associated Press

ZEELAND TOWNSHIP, Mich.




ZEELAND TOWNSHIP, Mich. (AP) — A young Michigan dairy farmer has auctioned off his herd of 230 milking cows after being unable to absorb losses brought about by an oversupply of milk in the state.
The Grand Rapids Press reports that Daybreak Dairy in Ottawa County has shuttered after 32-year-old dairy farmer Nate Elzinga and his family couldn't keep up with maintenance costs for the herd.
Elzinga says they weren't getting paid enough for their milk.
Michigan's herd is growing faster than the demand because of its temperate climate and abundance of water and pastures. Cows are also producing more milk than ever due to improved genetics and nutrition programs.
Michigan farmers are making between $1 and $2 less per hundred pounds of milk than farmers in other parts of the Midwest.
___
Information from: The Grand Rapids Press, http://www.mlive.com/grand-rapids