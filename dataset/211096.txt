Chicago Mercantile Exchange live cattle futures on Wednesday reversed some of Tuesday's advances, led by fund liquidation and expectations for softer prices for slaughter-ready, or cash, cattle this week, traders said."We obviously still see liquidation by funds that are still long the market," West Oak Commodities analyst Tom Tippens said.
Futures remained undervalued, or at a discount, to last week's cash prices which minimized losses for the August and October live cattle contracts, traders and analysts said.
August ended down 0.575 cent per pound to 109.475 cents, and October closed 0.725 cent lower at 108.325 cents.
There were no sales reported at Wednesday morning's Fed Cattle Exchange auction.
Packer bids for cash cattle in the U.S. Plains stood at $110 per cwt against $115 asking prices from sellers, said feedlot sources. Cash cattle last week brought $114 to $116 per cwt.
Market bears contend packers will pay less for cattle than last week because of seasonally slow wholesale beef demand and ample near-term supplies, with increased numbers ahead.
However, Tippens believes beef prices are close to bottoming out as retailers buy product at prices that have come down from their spring highs.
And he said lighter year-over-year cattle weights suggests ranchers and feedlots are on time, or current, in sending their animals to market to avoid lower prices as supplies grow later.
"I think we're going to be okay once we can get through some of this influence from the funds," said Tippens.
Fund liquidation, live cattle futures selling and as much as $10 per cwt lower cash feeder cattle prices erased CME feeder cattle's sharp gains on Tuesday.
August feeders closed 3.100 cents per pound lower, down 2.15 percent, at 142.250 cents. 
Hogs Step Backwards
CME lean hogs retreated from recent highs on profit-taking, following softer cash prices and sharply lower wholesale pork values, or cutout, said traders.
They said live cattle market losses discouraged lean hog futures buyers.
October ended 1.750 cents per pound lower at 68.775 cents, and December finished 1.225 cents lower at 63.475 cents.
Packers have plenty of hogs at their disposal as supplies build seasonally, a trader said. The pork cutout fell more than $2 per cwt on Wednesday, largely due to the seasonal pullback in pork belly prices, he said.