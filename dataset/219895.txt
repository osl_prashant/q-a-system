Add legumes to grazing pastures to improve cow performance, soil health and forage production, says Patrick Davis, University of Missouri Extension livestock specialist in Cedar County.
Now is the time to frost-seed legumes into pastures, Davis says. Frost seeding is a method of broadcasting seeds over frozen pastures. This no-till method works seeds into the soil as it freezes and thaws during the transition between winter and spring. You also can let cattle trample the ground after seeding to work seeds into the soil.
This method can improve pastures, add nitrogen to the soil and increase the quality of livestock forage, he says. Frost seeding requires less fuel and equipment than drilling.
Drilling or no-till drilling is another option that may lead to better seed-to-soil contact. Davis says the producer needs to know proper seeding rates, dates and depths to achieve a good stand. Davis recommends contacting your local extension agronomy specialist for advice or downloading the MU Extension publication "Seeding Rates, Dates and Depths for Common Missouri Forages" at extension.missouri.edu/p/G4652.
Davis prefers legumes to grasses for improvement of grazing pastures.
Much of southwestern Missouri's grass base is fescue. Legumes interseed well into a fescue base, he says. Legumes produce a higher quality forage than grasses because of the lower stem-to-leaf ratio. The lower stem ratio results in less neutral detergent fiber and more protein.
Legumes also make better year-round grazing, Davis says. He recommends red clover, white clover and lespedeza in southwestern Missouri. These legumes produce better in late spring and summer, when fescue does not grow or performs poorly. They result in improved forage intake, and cattle perform better with potential for higher profits.
Legumes also dilute the consumption of toxic fescue. This results in healthier cattle.
Do a soil test before incorporating legumes into fescue pastures, Davis says. Make sure pH and mineral levels are adequate for legumes to grow. Clover legumes require a pH of 6. Lespedeza can tolerate a 5.5 pH.
Stand management is key to legume persistence, he says. New legume plants need time to grow without competition from grass canopies. Rotational grazing systems prevent overgrazing. Davis recommends a four- to five-week rest period after grazing young legume plants.
Cattle bloat is a concern when grazing high-protein, highly digestive legumes. White clover is the most likely legume to cause bloat. Hold the forage base at 30 percent legume to reduce likelihood of bloat. Cattle experience less bloating with lespedeza and red clover, Davis says.
He recommends the following to reduce bloat:

Restrict grazing of legume fields at first to allow cattle time to adjust to legumes.
Provide dry hay to cattle to reduce legume intake before turning cattle out to legume pastures.
Provide poloxalene to cattle through bloat blocks or other means of supplementation.

Incorporating legumes should prevent cattle from poor performance in summer, a condition known as summer slump. It should reduce symptoms of fescue toxicosis during late spring and summer, and increase forage production, quality and cattle carrying capacity.
For more information on incorporating legumes in your fescue pasture, contact your local MU Extension livestock specialist or agronomist.