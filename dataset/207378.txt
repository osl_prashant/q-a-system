The topic of livestock production’s impact on antibiotic resistance has been debated for the past few years. Retailers are marketing more products as being “antibiotic-free” and government programs like the Veterinary Feed Directive (VFD) have stepped in to curb antibiotic use.During a conference for cattle producers and veterinarians hosted by Merck Animal Health in Kansas City, Kan., several speakers outlined the debate on antibiotic use in livestock production.
To say that all meat is antibiotic free is not necessarily true, says Brain Lubbers, DVM and director of the microbiology laboratory at Kansas State University.
“When we use antibiotics we follow the labeled withdrawal time,” Lubbers says of livestock producers.
Pharmaceutical companies spend a lot of time and money researching antibiotic withdrawals to determine when they are safe, meeting government specifications. “Safe and zero are two different terms,” Lubbers adds.
Pharmacokinetics is a branch of pharmacology focused on the movement of drugs within the body. The amount of antibiotics present in the body will rise quickly as it is absorbed in the blood. The principles of pharmacokinetics also apply to the movement of antibiotics out of the body through blood being processed in the kidneys and liver, eventually extracted via urine. Overtime through those processes the amount of antibiotics present in the body will go down exponentially.
The graph below from ResearchGate helps explain the process:
 

The longer the withdrawal period, the less antibiotic that would be present. For instance, if a calf were treated for pneumonia at 30 days old it would have possibly no antibiotics present when slaughtered 18 months later.
“That is very hard to explain to a consumer,” Lubbers says of explaining antibiotic withdrawal. “I think what we should say is commercially raised livestock treated with an antibiotic have been deemed safe.”
More than six months into VFD implementation, animal health record keeping has become more important for livestock producers than ever. Not only should producers keep track of the animals they are treating, they should also keep records on vaccinations, nutrition and other health events says Tim Parks, DVM and technical service veterinarian for Merck.
“We can’t measure the outcome if we don’t keep a record,” Parks says.
Merck has an app to help cattle producers keep track of cattle doctored for illness called Cattle Treatment Manager. There is an app for both veterinarians and pen riders.
Parks points out that antibiotics aren’t the only factor to cure an animal. Livestock need to have proper nutrition, handling and housing to help limit stress. By limiting stress it aids the immune system.
Vaccination programs also help set the immune system up for success by reducing the chance of infection.
“If we do not have an immune system behind an antibiotic we’re not going to see the response we want,” Parks says.
Regulations look like they will continue coming for livestock production following the implementation of the VFD, but they aren’t the only hindrance to future antibiotic use.
“People have decided the way to effect agriculture is not through regulations, it is through the retail supply,” Lubbers says.
Food retailers like Subway, Chipotle, Panera and McDonald’s have all made moves to include “antibiotic-free” items on their menus. It creates a faster route to changing production methods than passing a law. Policy is dictated by who is in the current administration, so regulations could change. Making the rules within a company removes the backlog of government.
“We can talk about regulations and how burdensome they are, but I’m a lot more worried about what retailers can do,” Lubbers says.
Down the road Lubbers could see more on-farm data being taken for antibiotic use, rather than the current system of sales data being pulled from pharmaceutical companies.
There is an opportunity to use a regulation or retailer implemented data program to benefit producers. Through confidential sharing of data individual farms might be able to communicate with one another to identify areas of improvement or success on their operation.
Going forward judicious use of antibiotics, while keeping better track of treatments will help livestock producers in the long-run.
“What I’m doing today with antibiotics will potentially affect my ability to use them tomorrow on my operation,” Lubbers says.