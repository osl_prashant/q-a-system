BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Fri. Mar 09, 2018          USDA Market NewsDaily California EggsMARCH 09, 2018Benchmark prices are unchanged. Asking prices for next week are 3 centshigher for Jumbo, 21 cents higher for Extra Large, 24 cents higher forLarge, and 5 cents lower for Medium and Small. Trade sentiment is fullysteady. Demand ranges moderate to good. Offerings and supplies are light tomoderate. Market activity is moderate. Small benchmark price $1.70.CALIFORNIA:Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                254EXTRA LARGE          249LARGE                243MEDIUM               190SOUTHERN CALIFORNIA:PRICES TO RETAILERS, SALES TO VOLUME BUYERS, USDA GRADE AA AND GRADE AA,WHITE EGGS IN CARTONS, DELIVERED STORE DOOR,CENTS PER DOZEN.RANGEJUMBO                241-253EXTRA LARGE          237-244LARGE                231-238MEDIUM               178-185Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 09-Mar-18 11:16 AM E MP