Soybean futures reacted sharply to USDA’s October crop reports that featured a virtually unchanged crop estimate and deeper cut to carryover than was expected. November soybean futures reached their highest level since the beginning of August, touching $10.00 Friday morning. The strength in soybeans pulled corn futures off their fresh contract lows, despite a bigger-than-anticipated crop estimate and unexpected rise in projected 2017-18 carryover. Wheat futures slumped amid what is perceived to be favorable weather for winter wheat crop development and USDA’s bigger-than-expected increase in 2017-18 carryover.
Pro Farmer Editor Brian Grete provides weekly highlights:



Cattle futures firmed amid strength in the cash cattle market. But with futures already at a premium to cash trade, buying was limited. Lean hog futures maintained a premium to the cash index amid signs the cash market has posted a seasonal bottom.
Notice: The PDF below is for the exclusive use of Farm Journal Pro users. No portion may be reproduced or shared.
Click here for this week's newsletter.