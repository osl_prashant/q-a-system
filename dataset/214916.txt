Boskovich Farms adds leeks
Oxnard, Calif.-based Boskovich Farms is offering organic leeks and romaine hearts from Mexico for the first time, starting in November and December, said Darrel Byer, vice president of sales and marketing.
“We’ll have decent volume,” he said of the items, which Boskovich will market under its B Organic label. “We’ve got a number of locations where we produce in Mexico.”
Boskovich now has 25-30 organic items, and the program is growing, Byer said.
“Summertime is a little tough, but the program’s been growing every year I’ve been here, and it continues to grow,” Byer said. “Just baby steps. It’s definitely growing every year.”
 
Canada association creates report
In July the Ottawa-based Canada Organic Trade Association completed its first-ever report on the “state of organics” across the country, said Jill Guerra, research and special projects coordinator.
The report looked at four areas — provincial regulations, market development, support for increasing organic production and “organic-specific data,” Guerra said.
The association will compile a new report yearly and will track changes “in terms of government support for organic,” Guerra said.
The association also will issue an updated market report in November, Guerra said, noting that the last — and first — such comprehensive overview of Canada’s organics market was compiled in 2013.
 
First Fruits sees Opals grow
Yakima, Wash.-based First Fruits Marketing of Washington LLC has increased its organic apple production by 5% to 7% in 2017, with more coming next year, said Matt Miles, organic program coordinator.
“We’re actually in a state where we have quite a bit of fruit in transition,” he said. “We’ll have more organic grannies next year and more organic Opals next year.”
Organic volume of First Fruits’ proprietary Opal apple variety is expected to double in 2018 — to 250,000 cartons, from this year’s 125,000, Miles said.
“That’s exciting — we’ve been planting a lot of Opal,” he said. “Demand for organic Opal has exceeded our supply as long as we’ve been supplying.”
Opals are grown by Broetje Orchards in Washington and distributed exclusively by First Fruits Marketing.
 
Frieda’s Inc. builds program
Los Alamitos, Calif.-based Frieda’s Inc. is building on its organic program, said Alex Jackson Berkley, senior account manager.
“We’ve really developed an organic squash and ginger program over the last six months to provide these consistently through their seasons,” Jackson said. “Organic ginger will become demanded year-round.”
 
Homegrown builds stone fruit program
Porterville, Calif.-based Homegrown Organic Farms is building its organic stone fruit program, said Stephen Paul, category director for fall commodities.
“All of our commodities in stone fruit are growing quite a bit, probably upwards of 20% across the board over the next five years,” he said.
Homegrown’s Asian pear volume is forecast to double in that time frame, Paul said.
“We’ve already seen (growth) this year. It goes pretty much from Aug. 1 to maybe mid-December,” he said.
Homegrown also has built a year-round organic blueberry program, Paul said.
“Homegrown is becoming more of a year-round full-service organic company,” he said.
Homegrown Organic Farms now grows citrus, stone fruit and blueberries on more than 3,000 acres, Paul said. Production is primarily in California, although the company also has growers elswhere, including blueberries from Oregon.
“It’s pretty substantial,” he said. “Because we are a grower-centric company, we’re seeing growers liking and migrating to what Homegrown’s program is all about.”
 
Mann Packing expands line
Salinas, Calif.-based Mann Packing Co. has expanded its organic line, with the addition of green beans, cauliflower florets and an Organic Super Blend this year, said Jacob Shafer, senior marketing and communications specialist.
The three new products join Mann’s existing line of organic products, which includes broccoli florets, broccoli and carrots, a vegetable medley, broccoli coleslaw and a vegetable snacking tray, Shafer said, noting that all products are available to retailers across the U.S. and Canada.
 
NatureFresh to grow more peppers
In October, Leamington, Ontario-based NatureFresh Farms signed an agreement with Netherlands-based Van der Knaap Group to grow peppers organically using a patented sustainable organic cultivation system.
NatureFresh said the deal supports its plan to increase its organic pepper lineup from 9 to 15 acres in 2018, with a goal of 30 acres of organics by 2019.
Van der Knaap, a group of companies focused on rooting and growing media, has been working with their partners to develop a cultivation system for growing organically above the soil.
“There is a growing consumer demand for more organic produce generally and organically grown peppers are in strong demand,” said Ray Wowryk, director of business development with NatureFresh Farms.
“All retailers continue to expand organic offerings within the department,” he said. “As the product become more visible at mainstream retailers, consumers are reacting. This will continue to draw more demand for organic peppers in the future.”
 
Oppy prepares brand for fruit
Vancouver, British Columbia-based The Oppenheimer Group, which has exclusive global marketing rights to the Envy, Jazz and the Pacific Rose apple varieties, will make the items available in 2-pound pouch bags starting this year, said Chris Ford, Oppy’s organic category director.
The company also is going to launch a yet-to-be-determined company brand for apples and pears early in 2018, Ford said.
The label will be available to growers in Chile, New Zealand, Argentina and Washington state, Ford said.
“The continuity of branding is important for the consumer and for the grower,” he said. “That’s very exciting to have the branding.”
The new brand likely will start with the beginning of Oppy’s import season in February, out of Argentina, Ford said.
Oppy’s three premium apple varieties will have distinct branding, he said.
 
Sage Fruit sees big jump
Yakima, Wash.-based Sage Fruit Co. will see a jump in its organic fruit volume in the coming year, said Chuck Sinks, president of sales and marketing.
Sinks said the company had a little over 100,000 cartons of organic fruit last year, but expects 1.3 million cartons this year after a lot of orchards completed the three-year transition period.
“We’ve got organic bins for the retailers to use and we got these organic pouch bags that retailers seem to love.”
 
Stemilt aims for online shoppers
Wenatchee, Wash.-based Stemilt Growers LLC recently completed “a tremendous program” on organic peaches and nectarines for customers across North America, said Roger Pepperl, marketing director.
“It is the first large-scale operation of organic stone fruit in Washington state and is flourishing today with new plantings and varieties,” he said.
Also, Stemilt now has a 4-pound stand-up pouch bag of apples for its Fresh Blenders organic apple program, aimed at the juicer/smoothie market and also to online shoppers, Pepperl said.
Stemilt also has a 3-pound Lil Snapper organic apple lineup that targets parents and kids. It is also geared to work for online ordering systems, including home delivery and store pick-up, Peperl said.
“It is a great way to sell 3 pounds of organic apples to the organic shopper, who is the largest purchaser of fresh produce in your store,” he said. 
 
Tanimura & Antle builds program
Salinas, Calif.-based Tanimura & Antle is building on an organic program it launched this year, said Chris Glynn, organics director, and John McKeon, senior manager of organic compliance.
“We started our organics program with Artisan Romaine, then expanded to include our Artisan Red Onions and Romaine Hearts, and soon we’ll be adding celery, celery hearts, broccoli and cauliflower to the program,” they said in an e-mail.
Organics will be about 8% to 10% of T&A’s total production this year, with projected growth of about 5% to 10% next year, they said.
 
Urban Organics to open facility
St. Paul, Minn.-based Urban Organics Pentair Group LLC has nearly completed work on its new 90,000-square-foot production facility in St. Paul, said Dave Haider, president and founder.
“Once we’re at full capacity at the end of year, we’re going to focus on what our next markets are,” said Haider, whose company grows organic leafy greens, lettuce, kale, Swiss chard and herbs. Urban Organics packages various blends in 5-ounce clamshells with a suggested retail price of $3.99.
Most of the company’s annual volume of 750,000 to 1 million pounds of produce currently is consumed in Minnesota, but it has plans to expand its distribution to neighboring states in 2018 and beyond, Haider said.
The new facility already is at about 40% production capacity, Haider said.
 
Wish Farms to offer more berries
Plant City, Fla.-based Wish Farms has more organic berries coming to the marketplace, said Gary Wishnatzki, CEO.
“Our strawberry deal is probably going to be a little bigger — maybe up 10% from last year; we have a little more organic acreage,” he said.
Wish Farms also will have organic blueberries available earlier than usual in 2018, Wishnatzki said.
“We’re expecting February Florida organic blueberries off a new farm we have,” he said. “How much, we don’t know. We’re still early to know.”
The early blueberries will be available mostly in the eastern U.S., Wishnatzki said.
Organics comprise about 10% of Wish Farms’ total strawberry production and about 5% of blueberries, Wishnatzki said.
“In blueberries, it has been small, but we’re going to begin to change that,” he said. “If we’re successful with this new venture, it should be more, because we’ll expand it.”