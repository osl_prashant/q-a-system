BC-Deep South News Digest
BC-Deep South News Digest

The Associated Press



Good afternoon! Here's a look at how AP's news coverage is shaping up today in the Deep South. Questions about today's coverage plans are welcome and should be directed to:
The Atlanta AP Bureau at 404-522-8971 or apatlanta@ap.org
The Montgomery AP Bureau at 334-262-5947 or apalabama@ap.org
The New Orleans AP Bureau at 504-523-3931 or nrle@ap.org
The Jackson AP Bureau at 601-948-5897 or jkme@ap.org
Deep South Editor Jim Van Anglen can be reached at 1-800-821-3737 or jvananglen@ap.org. Administrative Correspondent Rebecca Santana can be reached at 504-523-3931 or rsantana@ap.org. A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
GEORGIA (All times Eastern):
TOP STORIES:
MLK50-WITNESSES
MEMPHIS, Tenn. — Clara Ester's eyes were fixed on the Rev. Martin Luther King Jr. as he stood on the concrete balcony of the Lorraine Motel. King was in Memphis to support a sanitation workers' strike, and Ester, a college student, had been marching alongside the strikers as they sought better pay and working conditions. She and some friends had gone to the motel for a catfish dinner when she saw King chatting happily, not far from where she stood. By Adrian Sainz and Kristin M. Hall. SENT: 1307 words, photos.
BOOKS-HARPER LEE-LETTERS
NEW YORK — Around the same time "To Kill a Mockingbird" made Harper Lee a best-selling author and Pulitzer Prize winner, she was still fighting for creative control. "I must say it's increasingly difficult for magazine articles to be written any other way than a magazine editor standing over your shoulder telling you what to write. You know how well that sets with me," the Monroeville, Alabama, native wrote to her New York friend Harold Caufield (affectionately referred to as "Darling Aitch"). The 1961 letter — the year after the book was published — told of Esquire's turning down a piece she had been asked to write. By Hillel Italie. SENT: 912 words, photo.
MARIJUANA OPIODS
NEW YORK — Can legalizing marijuana fight the problem of opioid addiction and fatal overdoses? Two new studies in the debate suggest it may. Pot can relieve chronic pain in adults, so advocates for liberalizing marijuana laws have proposed it as a lower-risk alternative to opioids. But some research suggests marijuana may encourage opioid use, and so might make the epidemic worse. By Malcolm Ritter. SENT: 622 words, photo.
MLK50 INTERNATIONAL LEGACY
KAMPALA, Uganda — The name of the Rev. Martin Luther King Jr. can be found across Africa on streets, schools, even a bridge in Burkina Faso. It's a measure of the influence of the American civil rights leader who was assassinated 50 years ago after speaking out against injustices at home and abroad. Africa's push for independence from colonialism, which mirrored King's movement for racial equality in America, attracted his support. By Rodney Muhumuza. SENT: 142 words, photo.
MEMBER EXCHANGE:
MLK SUPPORTER
DALLAS — Peter Johnson stared out the plane's window and watched the fires burning below in Washington, D.C., the nation's capital convulsed by riots, the anger below reflecting the turmoil inside himself. The Dallas Morning News reports it was April 4, 1968. The Rev. Martin Luther King Jr. had been assassinated in Memphis, Tennessee, and Johnson had taken the first flight home to Louisiana. By David Tarrant, The Dallas Morning News. SENT: 3247 words, photos.
IN BRIEF:
— WRECK-JUDGE KILLED — Authorities say a judge is dead following a wreck in central Georgia.
— SCHOOL THREAT-ARREST — Police in Georgia say a woman who admitted posting a video depicting a shooting threat to an elementary school has been arrested
— DRIVE-BY SHOOTING — A 3-year-old has been killed in a drive-by shooting in Georgia
— NIGHTCLUB SHOOTING — Authorities say a man is dead after a shooting outside of a Georgia nightclub
SPORTS:
BBN--NATIONALS-BRAVES
ATLANTA — The Washington Nationals, off to a 3-0 start, open a series against the Atlanta Braves on Monday night. Washington's Tanner Roark will face Atlanta's Sean Newcomb. By Charles Odum. UPCOMING: 650 words, photos. Game starts at 7:35 p.m.
GLF--MASTERS
AUGUSTA, Ga. — The smallest field for the majors was lacking golf's biggest star. Tiger Woods was due at the Masters on Monday afternoon, his first time to Augusta National with golf clubs since 2015. By Golf Writer Doug Ferguson. UPCOMING: 700 words, photos by 6 p.m. EDT.
With:
— GLF--MASTERS-NOTEBOOK. By Doug Ferguson. UPCOMING: 700 words, photos by 7 p.m. EDT.
— Sidebars on merit
GLF--MASTERS-BRYAN
AUGUSTA, Ga. — Wesley Bryan makes his Masters debut in the city where he lives by walking away from the game. Finding himself in a rut, Bryan hasn't played a PGA Tour event since late February as he tries to rekindle some passion. By Mark Long. UPCOMING: 600 words, photos by 5 p.m. EDT.
ALABAMA (All Times Central)
TOP STORIES:
CORRUPTION CHARGES
MONTGOMERY, Ala. — An Alabama legislator and a lobbyist who once chaired the Alabama Republican Party were arrested Monday on conspiracy charges related to payments made to another lawmaker to advance an insurance bill, prosecutors announced. By Kim Chandler. SENT: 370 words.
ARBY'S HARASSMENT
MONTGOMERY, Ala. — The U.S. Equal Employment Opportunity Commission is suing the owner of an Alabama fast food restaurant over allegations of sexual harassment, according to a lawsuit filed Friday. The federal lawsuit alleges that a team leader at an Arby's in Atmore, Alabama, pressured several young female employees to have sex with him, used crass language while commenting on their appearance, and described the sexual acts he wanted to perform on them. The behavior lasted from May to August 2016, according to the lawsuit. By Mallory Moench. SENT: 232 words.
BOOKS-HARPER LEE-LETTERS
NEW YORK — Around the same time "To Kill a Mockingbird" made Harper Lee a best-selling author and Pulitzer Prize winner, she was still fighting for creative control. "I must say it's increasingly difficult for magazine articles to be written any other way than a magazine editor standing over your shoulder telling you what to write. You know how well that sets with me," the Monroeville, Alabama, native wrote to her New York friend Harold Caufield (affectionately referred to as "Darling Aitch"). The 1961 letter — the year after the book was published — told of Esquire's turning down a piece she had been asked to write. By Hillel Italie. SENT: 912 words, photo.
MEMBER EXCHANGE:
MLK SUPPORTER
DALLAS — Peter Johnson stared out the plane's window and watched the fires burning below in Washington, D.C., the nation's capital convulsed by riots, the anger below reflecting the turmoil inside himself. The Dallas Morning News reports it was April 4, 1968. The Rev. Martin Luther King Jr. had been assassinated in Memphis, Tennessee, and Johnson had taken the first flight home to Louisiana. By David Tarrant, The Dallas Morning News. SENT: 3247 words, photos.
IN BRIEF:
— TEEN CONCERT-FIVE SHOT — Police say five teenagers were shot and two others were trampled at what was advertised as a concert for high school students on Easter night in Mobile, Alabama.
— APPLE CEO-CIVIL RIGHTS AWARD — Apple CEO Tim Cook is being honored by a civil rights group in his home state of Alabama.
— MOORE LAWSUIT — Montgomery Circuit Judge Ronan Shaul last week denied Roy Moore's request to dismiss the lawsuit filed by Leigh Corfman
— BEEF RECALL — A North Texas company has recalled nearly 4 tons of raw beef wrongly produced and packaged without federal inspection
— INFANT INJURED — Police in Alabama say a 2-month-old child suffered severe head trauma and a woman has been arrested
LOUISIANA (All Times Central)
TOP STORIES:
SCOUT CAMP KILLING
BATON ROUGE, La. — Louisiana prosecutors aren't seeking the death penalty against a suspect charged with killing three men and wounding a fourth in a string of shootings last year. By Michael Kunzelman. SENT: 534 words.
XGR-LOUISIANA ROADS
BATON ROUGE — Lawmakers decide whether they will support Gov. John Bel Edwards' plan to use a federal borrowing tool to construct $600 million in Louisiana interstate projects. The roadwork has been sought for decades to relieve traffic headaches, but some Republican lawmakers question if the federal financing plan would lock up dollars for years that might be better spent elsewhere. The biggest-ticket project is a $350 million widening of Interstate 10 from the Mississippi River Bridge to Interstate 12 in Baton Rouge, a daily chokepoint also considered one of Louisiana's most dangerous stretches of roadway. Also slated for financing are projects in Bossier City and the New Orleans area. The Joint Legislative Committee on the Budget debates the plan Monday afternoon. By Melinda Deslatte. Filed by 6 p.m.
MEMBER EXCHANGE:
MLK SUPPORTER
DALLAS — Peter Johnson stared out the plane's window and watched the fires burning below in Washington, D.C., the nation's capital convulsed by riots, the anger below reflecting the turmoil inside himself. The Dallas Morning News reports it was April 4, 1968. The Rev. Martin Luther King Jr. had been assassinated in Memphis, Tennessee, and Johnson had taken the first flight home to Louisiana. By David Tarrant, The Dallas Morning News. SENT: 3247 words, photos.
IN BRIEF:
— BEEF RECALL — A North Texas company has recalled nearly 4 tons of raw beef wrongly produced and packaged without federal inspection
— FATAL SHOOTING-DEPUTY WOUNDED — Authorities have identified a vehicle burglary suspect killed in a gunfire exchange with deputies in Louisiana
— CARD SKIMMING CONSPIRING — Prosecutors in Louisiana say three Cuban nationals have admitted that they agreed together and with others to obtain credit and debit card numbers that had been skimmed from gas pumps
— FATAL ARMED ROBBERY — Police say a teenager was shot and killed during an attempted robbery in Louisiana
MISSISSIPPI (All Times Central)
TOP STORIES:
MISSISSIPPI AGRICULTURE COMMISSIONER
JACKSON, Miss. — The new Mississippi agriculture commissioner says diversity of race and culture makes the state stronger. Former state Rep. Andy Gipson of Braxton was sworn in Monday as the state's eighth commissioner of agriculture and commerce. By Emily Wagster Pettus. SENT: 138 words. Will be updated.
IN BRIEF:
— BANK ACQUISITION-THE FIRST — A Mississippi bank has completed its purchase of a bank in Florida's capital city.
— BEEF RECALL — A North Texas company has recalled nearly 4 tons of raw beef wrongly produced and packaged without federal inspection
— MINISTER JAILED-SEXUAL ASSAULT — A Methodist minister accused of having sex with two teenage girls has been arrested in Mississippi
SPORTS:
BKW--T25-MISSISSIPPI ST-WRAPUP
The brilliant careers of four Mississippi State seniors ended with a heartbreaking last-second loss to Notre Dame in the NCAA Tournament championship game on Sunday. It was the second straight season the Bulldogs lost in the title game. By David Brandt. UPCOMING: 500 words by 3 p.m.
___
If you have stories of regional or statewide interest, please email them to
The Atlanta AP Bureau: apatlanta@ap.org
The Montgomery AP Bureau: apalabama@ap.org
The New Orleans AP Bureau: nrle@ap.org
The Jackson AP Bureau: jkme@ap.org
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.