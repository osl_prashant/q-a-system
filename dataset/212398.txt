Frank Gasperini, president and CEO of the National Council of Agricultural Employers, Washington, D.C., plans to retire at the end of the year. 
Gasperini was hired by the group in 2008, having previously served as director of government relations for RISE, a part of CropLife America.
 
The NCAE website  said the group will be taking applications on Gasperini’s replacement through July 31.
 
Gasperini said June 2 the group hopes to hire a replacement perhaps sometime between October and November, which will allow a couple of months of overlap so Gasperini can help orient his successor.
 
Gasperini said he will likely remain in the Washington, D.C., region and continue to manage, on a part-time basis, the Agricultural Safety and Health Council of America.
 
Gasperini, who will be 66 at the end of this year, said he looks forward to fishing and other outdoor pursuits in his retirement.