BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Friday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2547  Down  10May         2536    2546    2514    2522  Down  14Jul         2557    2567    2541    2547  Down  10Sep         2565    2578    2553    2559  Down  10Dec         2557    2574    2551    2557  Down   6Mar         2543    2556    2537    2542  Down   5May         2545    2558    2540    2545  Down   4Jul         2547    2565    2546    2552  Down   5Sep         2558    2564    2554    2559  Down   6Dec         2573    2585    2569    2571  Down   8