High temperatures, low humidity and heavy winds have Oklahoma’s wildfire threat at its highest point in a decade, according to the National Weather Service.

On Tuesday, temperatures soared into the 90s in parts of western Oklahoma, creating the highest wildfire threat that the state has seen in a decade. Wind gusts were calculated at 40 mph, and the humidity levels were less than 10 percent.

Since the Rhea fire started last week, two people have died from western Oklahoma and more than 400,000 acres. Hundreds of miles of fencing has been destroyed, and many cattle, livestock and wildlife have been killed in the blaze.

Several towns in western Oklahoma have been forced to evacuate, and according to Mike Karlin, fire chief of Weatherford, Oklahoma, firefighters are working “to get some things secured” due to the wind changes, low humidity and increased wind speeds.

The National Weather Service is expecting an increased chance of seeing rain and thunderstorms Thursday night and Friday, and strong to severe storms are possible in southwestern Oklahoma and western north Texas on Friday.