“While the challenges are great, so are the opportunities.”
–Bill Ford, executive chairman of Ford Motor Company and the great-grandson of company founder, Henry Ford
This is a great quote to ponder in light of the current landscape of farm country. Even with razor-thin profit margins, you will have opportunities to succeed with your farm operation—and perhaps even expand.
“Even in today's environment of tight margins, many farms are exploring expansion options,” write Michael Langemeier and Michael Boehlje in a recent farmdoc daily piece. “When exploring these options, it is important to address key questions pertaining to the farm's strategy.”
At the 2016 Purdue Top Farmer conference, I heard Boehlje list 10 questions to ask before growing your farm. Read the full list here.
Today, let’s consider one of the questions: How will expansion impact my current operation? 
Any business expansion or diversification will affect your farm’s financial standing—to the upside or downside. Consider how renting more acres or adding a new venture will influence farm's balance sheet, income statement and cash flow statement. Will it improve efficiencies and lower costs? Will it provide better asset utilization? 
But also, will it divert your leadership’s attention? Beyond zeroing in on your financial and business assets, consider your soft assets.
“Most people worry too much about their capital assets and not enough about their management and staffing,” Boehlje says. “Too many business grow and acquire physical assets and then think about how they will run it.”
How will adding acres or partnering with another farm impact your managerial capacity? Assess your current management skills and those skills required by a specific growth option, Langemeier and Boehlje advise.
Ask yourself the following questions, provided by Langemeier and Boehlje: 

Will adding the new venture increase the managerial requirements beyond the current capacity of the business? 
Are additional managerial skills needed to be successful in operating the new venture? 
Can time be allocated by the current management team to "get up to speed" in the new venture? 
Will the acquisition increase the complexity of the current business? 
Will managerial resources need to be diverted from the current business to the new venture, thus putting increased pressure on the current management team? 
Will the farm and the new venture have a relatively separate management structure, or will the new venture be integrated from both a managerial and operational perspective with the current business?

“Each expansion option should be evaluated in terms of its impact on the farm's financial statements, financial performance, and managerial attention and oversight,” Langemeier and Boehlje write. “Attractive expansion options should both improve the farm's long-term financial performance, and be a good strategic fit for the farm's owners and managers.”
You can use Purdue’s online tool called Farm Growth: Venture Analysis and Business Models to analyze decisions you make based on these questions.
Read the full piece by Langemeier and Boehlje: How Will Expansion Impact My Current Operation?