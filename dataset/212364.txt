A new how-to guide has been developed by the Produce Marketing Association to help produce growers and shippers know what to do when Food and Drug Administration inspectors arrive. 
The association’s food safety team partnered with the international food and drug law firm Keller & Heckman to produce the “Food Regulatory Inspection Manual,” according to a news release from the Newark, Del.-based association.
 
The manual provides an employee training reference for produce industry farms and food facilities, according to the release.
 
Including checklists to help produce companies prepare for an FDA inspection, the document is designed for both executives and food safety managers, according to the release.
 
“The key to a successful inspection is preparation — and the time to prepare is right now, before FDA knocks,” Leslie Krasny, author of the manual and attorney with Keller & Heckman, said in the release.
 
The PMA’s new “Food Regulatory Inspection Manual” can be downloaded at the PMA website.
 
PMA also will host a web seminar featuring Krasny at 2p.m.-3p.m. Easter time June 8.
 
Registration for the free web seminar is available online. The seminar also will be recorded and posted online for on-demand access, according to the release.
 
“We know our industry has many questions about FSMA,” PMA vice president of food safety and technology Jim Gorny said in the release. “Our team has been working hard to provide as much clarity as possible, starting with a wealth of resources on pma.com that are available whenever you are,”