The National Weather Service's (NWS) extended outlook calls for above-normal temps to linger the remainder of the growing season. In its outlook for August, it says the best chance for warm conditions in across the Dakotas, where current soil moisture values rank below the 10th percentile. However, the precip outlook for next month is less certain, with above-normal precip expected across the Four-Corner states, but equal chances for normal, below- or above-normal precip across the remainder of the country.
The outlook for above-normal temps across the Corn Belt is especially concerning given the current warning for "excessive" heat to linger into the weekend across the central Corn Belt during pollination. The flip of the calendar will put more focus on the soybean crop as it begins to flower and fill pods.
NWS outlooks for August:




 




 




The August through October temperature outlook indicates increased chances of above-normal conditions across the contiguous U.S., although probabilities are reduced from parts of the Southern Plains to the central Mississippi Valley, states the NWS. The highest probability for above-normal temps are forecast for parts of the northern Rockies and Intermountain West, the Gulf Coast, the Atlantic Coast and the Northeast.
Meanwhile, the Four-Corners States are expected to see above-normal precip continue, spreqading into the Southern Plains, Arkansas and Louisiana. But once again, the precip outlook is more uncertain for the remainder of the Plains and Corn Belt, with chances equal for normal, below- or above-normal precip.
NWS outlooks for August through October:




 




 




Below, we compare the Seasonal Drought Outlook to the current Drought Monitor:




 




 




The latest National Drought Monitor reflects the spread of drought in Minnesota, Iowa and Illinois. Iowa now has 58.4% of its area covered by drought (47.5% last week), with 22.2% covered by moderate (D1) drought (16.2% last week). Illinois has 42.5% covered by drought, up from 16.2% last week. Drought conditions also worsened in Nebraska, where 74.4% is covered by some form of drought and 24.8% is covered by D1, up from 15.8% last week.

The seasonal drought outlook reflects some shrinking of the drought in Iowa and removal from Illinois, but drought will maintain its stronghold on a large area of the Northern Plains. Forecasters say the summer-into-fall period is "notoriously difficult to predict due to variable summertime convection and the wild card Atlantic hurricane season." Additionally, it states, "There was little guidance in the longer-term statistical and dynamical precipitation models as they were limited in their probability, areal coverage and consistency, therefore much of this [forecast] was based upon climatology, current conditions and short-term forecasts, along with any slight tilts in the one- and three-month precipitation and temperature outlooks."