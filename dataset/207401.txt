The USDA’s National Animal Health Monitoring System (NAHMS) has been providing benchmarks to the beef industry for nearly 25 years.  Since the first cow-calf study conducted in 1993, the NAHMS program has been describing health and management on beef operations across the nation.  Over the years the NAHMS program has conducted four studies of the cow-calf segment of the beef industry and three studies of the feedlot segment.   In addition to providing benchmarks for producers and industry organizations the NAHMS program has helped educate future producers and veterinarians about how beef production occurs in the US and the challenges to animal health.  Further, the information has been useful in setting research agendas for both private and public institutions to focus animal health research in the areas of greatest need. Past beef reports and other information about the NAHMS program is available at http://www.aphis.usda.gov/nahms.  
The NAHMS program is preparing to update the information on health and management on cow-calf operations by conducting a new study to begin in the fall of 2017.  This will mark the first time in 10 years that NAHMS has taken an in-depth look at the U.S. beef cow-calf industry. This new study will provide an opportunity to see how the cow-calf industry has changed during this 10-year period.
Past study revealed key management trends
Some of the highlights of the previous beef cow-calf study conducted in 2007 and 2008 included:
·         One frequent recommendation of preconditioning programs is to hold calves for a period of time after weaning to allow them to become accustomed to drinking out of water tanks and eating out of feeders. Another recommendation is to avoid combining the stresses of weaning, mixing, and transportation. In 2007, about one-half of beef cow-calf operations (49.8 percent) that sold calves for purposes other than breeding sold them immediately at the time of weaning.
·         In 2007, six of 10 operations (60.6 percent) did not vaccinate beef calves for respiratory disease from birth to sale, and 30.9 percent of calves were on these operations.
·         A higher percentage of operations in the Central region (67.9 percent) and the West region (56.4 percent) vaccinated any cattle or calves against BVD compared with operations in the Southeast region (28.9 percent).
·         About four of 10 operations (41.0 percent) vaccinated any cattle or calves against BVD in 2007. The percentage of operations that vaccinated any cattle or calves against BVD varied by herd size, ranging from 28.6 percent of operations with one to 49 beef cows to 80.5 percent of operations with 200 or more beef cows.
·         For cows sold for purposes other than breeding in 2007, 33.0 percent were sold due to pregnancy status (open or aborted), and 32.1 percent were sold because of age or bad teeth.
·         A higher percentage of unweaned calves (7.2 percent) and replacement heifers (6.0 percent) were treated at least once with oral or injectable antibiotics for any diseases or disorders than mature cows (1.9 percent).
·         About one-half of all operations (50.8 percent) consulted a veterinarian for some reason during the previous 12 months. Interaction with a veterinarian was more common among herds with 200 or more cows (82.2 percent) compared with operations with fewer than 50 cows (43.2 percent). It will be interesting to see if the percentage of operations consulting a veterinarian in the previous 12 months has changed due to the new VFD rules that went into effect on January 1, 2017.
·         About one-half of operations (54.5 percent) had no set breeding season. However, only 34.1 percent of the cows were on operations with no set breeding season, indicating that this practice is more common on smaller operations.
·         A higher percentage of operations in the West and Central regions (92.3 and 84.2 percent, respectively) castrated any bull calves born in 2007 before selling them than did operations in the South Central and East regions (43.9 and 46.0 percent, respectively).
·         Most operations observed heifers and cows regularly during calving (92.7 and 89.0 percent, respectively). The majority of cows and heifers (95.0 percent) required no assistance at calving. Two-thirds of calves (65.8 percent) born in 2007 were born in the first four months of the year.
·         During the first six months of 2008, nearly all calves born to beef cows and beef heifers were born alive (97.9 and 94.2 percent, respectively).
·         The percentage of calves born that had or were expected to have horns decreased from 27.8 percent in 1997 to 12.4 percent in 2007. This drop in the percentage of non-polled calves may be due to changes in breed utilization or the implementation of Beef Quality Assurance (BQA) guidelines that recommend the reduction of horned calves.
·         Calves born dead accounted for nearly one-half of all calves (44.5 percent) that died prior to weaning during the first six months of 2008. Only 14.0 percent of calf loss or death occurred three weeks or more following birth but prior to weaning. About one in four calf losses (28.0 percent) occurred more than 24 hours but less than three weeks following birth.
·         Only 12.3 percent of operations had not heard of BVDV, and 64 percent of operations were fairly knowledgeable or knew some basics about the virus.
·         Only 4.2 percent of producers had done any testing for persistent infection with BVDV in the past three years.
·         Of the 205 operations that submitted ear-notch samples for BVDV testing, only 8.8 percent had at least one persistently infected animal identified. Among the 44,150 ear notch samples tested, only 0.12 percent were positive for persistent infection with BVDV.
New study to update trends, document changes since 2007
The Beef 2017 study will invite a random sample of cow-calf producers from 24 states to voluntarily participate in the study.  First contacts with those selected to participate will begin in October by personnel representing the National Agricultural Statistics Service.  Information will be collected in an interview lasting approximately one hour.   Producers will be asked about their willingness to continue in the study and allow a second interview to occur sometime in the January to April timeframe.  A second interview about one hour in length will collect more detailed information on health, productivity and management of cattle in the herd. 
Producers who participate in both interviews will be eligible to conduct testing on their entire calf crop for persistent infection with the BVD virus and will also be eligible to submit a forage sample for nutrient analysis.
Importantly, all participation in the Beef 2017 study is voluntary and the data from the individual producers is confidential and reported only in aggregate summary form. The key objectives of the Beef 2017 study were set through polling of industry stakeholders and in conjunction with beef industry leaders and include;
 1. Describe trends in beef cow–calf health and management practices, specifically
• Cow health and longevity,
• Calf health,
• Reproductive efficiency,
• Selection methods for herd improvement including tests of genetic merit, and
• Biosecurity
2. Describe management practices and producer beliefs related to
• Animal welfare,
• Emergency preparedness,
• Environmental stewardship, and
• Record keeping and animal identification practices.
3. Describe antimicrobial use practices (stewardship) and determine the prevalence and antimicrobial resistance patterns of potential food-safety pathogens.
• Types and reasons for use of antimicrobial drugs by animal type
• Stewardship
·         Use of alternatives for disease control
·         Use of Beef Quality Assurance principles
·         Veterinarian-client-patient relationship
·         Information sources
• Enteric organism antimicrobial resistance assessments (e.g., Salmonella, E. coli, Enterococcus)
For comparison and identification of trends, most of the questions in the 2017 survey are similar to those asked in 2007. The survey will also cover some new ground, with questions regarding the use of remote-delivery darts for treating cattle, additional questions on nutrition and antibiotic use, including use of chlortetracycline in minerals, veterinarian-client-patient relationships (VCPR) and preconditioning practices.