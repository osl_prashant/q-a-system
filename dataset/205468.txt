Many producers in Kansas could benefit by using starter fertilizer when planting corn. Starter fertilizer is simply the placement of some fertilizer, usually nitrogen (N) and phosphorus (P), near the seed -- which "jump starts" growth in the spring. It is not unusual for a producer to see an early season growth response to starter fertilizer application. But whether that increase in early growth translates to an economic yield response is not a sure thing in Kansas. How the crop responds to starter fertilizer depends on soil fertility levels, tillage system, soil temperature, and N placement method. Phosphorus source is not an important factor.Soil fertility levels
The lower the fertility level, the greater the chance of an economic response to starter fertilizers. A routine soil test will reveal available P and potassium (K) levels. If soils test low or very low in P, below 20 ppm, there is a very good chance that producers will obtain an economic yield response to applying a starter fertilizer containing P, even in some low-yield environments. If the soil test shows a medium level of P, 20-30 ppm, it's still possible to obtain a yield response to P fertilizer. But the yield response will not occur as frequently, and may not be large enough to cover the full cost of the practice. If the soil test is high, above 30 ppm, economic responses to starter P fertilizers are rare. The chances of an economic return at high P soil test levels are greatest when planting corn early in cold, wet soils. In general, the same principles apply with K. If soil tests are low, below 130 ppm, the chances of a response to K in starter are good. The lower the soil test level, the greater the odds of a response.
All of the recommended P and/or K does not need to be applied as starter. If the soil test recommendation calls for high rates of P and K in order to build up or maintain soil test levels, producers will often get better results by splitting the application between a starter and a preplant broadcast application. As a general rule, starter fertilizer should be limited to the first 20-30 pounds of P or K per acre, with the balance being broadcast for best responses.
Phosphorus source
Does the type of phosphorus used as a starter make any difference? In particular, what about the ratio of orthophosphate to polyphosphate in the fertilizer product? This has been a concern for many producers.
Liquid 10-34-0 is composed of a mixture of ammonium polyphosphates and ammonium orthophosphates. The dissolved ammonium orthophosphate molecules are identical to those found in dry MAP (e.g. 11-52-0) and/or DAP (e.g. 18-46-0). Ammonium polyphosphates are simply chains of orthophosphate molecules, formed by removing a molecule of water, and are quickly converted by soil enzymes back to individual orthophosphates identical to those provided by MAP and/or DAP.
Polyphosphates were not developed by the fluid fertilizer industry because of agronomic performance issues. Instead, polyphosphates were developed to improve the storage characteristics of fluid phosphate products (and other fertilizers made from them) and to increase the analysis of liquid phosphate fertilizers. Ammonium polyphosphate is equal in agronomic performance to ammonium orthophosphates when applied at the same P2O5 rates in a similar manner. And liquid phosphate products are equal in agronomic performance to dry phosphate products if applied at equal P2O5 rates in a similar manner. When polyphosphate is added to soil, a process called hydrolysis breaks down the polyphosphate chains into orthophosphates. The concern of many people is the length of time it takes for this process to occur. Previous studies indicate that although it may take a few days to complete the hydrolysis process, the majority is completed in 48 hours. As a result, phosphorus in soil solution will typically be similar from either source shortly after application.
Tillage system
No-till corn will almost always respond to a starter fertilizer that includes N - along with other needed nutrients - regardless of soil fertility levels or yield environment. This is especially so when preplant N is applied as deep-banded anhydrous ammonia or UAN, or where most of the N is sidedressed in-season. That's because no-till soils are almost always colder and wetter at corn planting time than soils that have been tilled, and N mineralization from organic matter tends to be slower at the start of the season in no-till environments.
In general, no-till corn is less likely to respond to an N starter if more than 50 pounds of N was broadcast prior to or shortly after planting.
In reduced-till systems, the situation becomes less clear. The planting/germination zone in strip-till or ridge-till corn is typically not as cold and wet as no-till, despite the high levels of crop residue between rows. Still, N and P starter fertilizer is often beneficial for corn planted in reduced-till conditions, especially where soil test levels are very low, or low, and where the yield environment is high. As with no-till, reduced-till corn is also less likely to respond to an N starter if more than 50 pounds of N was broadcast prior to or shortly after planting.
Conventional- or clean-tilled corn is unlikely to give an economic response to an N and P starter unless the P soil test is low.
Starter fertilizer placement 
Producers should be very cautious about applying starter fertilizer that includes N and/or K, or some micronutrients such as boron, in direct seed contact. It is best to have some soil separation between the starter fertilizer and the seed. The safest placement methods for starter fertilizer are either:
-- A subsurface-band application 2 to 3 inches to the side and 2 to 3 inches below the seed, or
-- A surface dribble-band application 2 to 3 inches to the side of the seed row at planting time, especially in conventional tillage or where farmers are using row cleaners or trash movers in no-till.
If producers apply starter fertilizer with the corn seed, they run an increased risk of seed injury when applying more than 6 to 8 pounds per acre of N and K combined in direct seed contact on a 30-inch row spacing. Nitrogen and K fertilizer can result in salt injury at high application rates if seed is in contact with the fertilizer. Furthermore, if the N source is urea or UAN, in-furrow application is not recommended regardless of fertilizer rate. Urea converts to ammonia, which is very toxic to seedlings and can significantly reduce final stands. 
Work several years ago at the North Central Kansas Irrigation Experiment Field near Scandia illustrates some of these points (Table 1). In this research, former Agronomist-In-Charge Barney Gordon compared in-furrow, 2x2, and surface band placement of different starter fertilizer rates in a multi-year study on irrigated corn. Excellent responses from up to 30 pounds of N combined with 15 pounds of P were obtained with the both the 2x2 and surface-band placement. In-furrow placement however, was not nearly as effective. This was due to stand reduction from salt injury to the germinating seedlings, likely due to the high application rate of N plus K in furrow, indicating the importance of monitoring the N+K rates for in furrow application. Where no starter, or the 2x2 and surface band placement, was used, final stands were approximately 30-31,000 plants per acre. However, with the 5-15-5 in furrow treatment, the final stand was approximately 25,000. The final stand was just over 20,000 with the in-furrow 60-15-5 treatment.


Table 1. Effect of Starter Fertilizer Placement on Corn Yield at 
North Central Irrigation Experiment Field


Yield (bu/acre)


Fertilizer
Applied (lbs)


In-Furrow
Placement


2x2 Band
Placement


Surface Band
Placement


Check: 159 bu


--


--


--


5-15-5


172


194


190


15-15-5


177


197


198


30-15-5


174


216


212


45-15-5


171


215


213


60-15-15


163


214


213


Average


171


207


205