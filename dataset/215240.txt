Farmers hoping for any sign of an economic bright spot for 2017 got a modest one yesterday. Net farm income is expected to inch up a slight 3% over 2016 and reach $63.2 billion this year, according to the U.S. Department of Agriculture-Economic Research Service (USDA-ERS) November Farm Income Report.
The small improvement is the first farmers have seen in the last several years but is still less than half the amount farmers saw in 2013, when net farm income reached a record $129 billion.
Carrie Litkowski, a senior economist for USDA-ERS, says the 2017 gains primarily came as the result of strong export demand and higher prices in the livestock sector for beef, dairy, pork and poultry. The USDA cites a couple of examples: cotton and cattle prices have helped farmers in the Southern Plains, while dairy farmers have seen strong performance in the Northeast.
At the other end of the spectrum, low prices for corn and soybeans continue to hamper farmers’ earnings in the Northern Plains and the Midwest. Litkowski says the depressed prices are a result of oversupply, lower sales and lower government payouts.
Despite the slight economic improvement for agriculture this year, median income from farming alone is expected to be a negative $1,093 in 2017. However, USDA says the median income for farm households at $68,000 is higher than the U.S. overall, thanks to off-farm employment.
Here are additional summary notes from the USDA November report:

Direct Government farm program payments—those made “directly” by the U.S. Government to farmers and ranchers such as Price Loss Coverage, Agricultural Risk Coverage, and conservation program payments—are forecast to decline $1.8 billion (13.8 percent) in 2017 to $11.2 billion. 


Federal Crop Insurance Corporation indemnities—payments made by private insurance companies to farm operators for their insured crop losses—are forecast to rise in 2017 by $1.1 billion, or 25.1 percent, to $5.4 billion.


Total production expenses are forecast to increase $5.3 billion (1.5 percent) in 2017 to $355.8 billion after falling year-over-year in both 2015 and 2016. Inflation-adjusted total production expenses are forecast to be relatively unchanged from 2016. In nominal terms, interest expenses are forecast up $2.1 billion (12.3 percent) and hired labor expenses up $1.1 billion (4.1 percent). Expenses for fuels and oils are forecast up by $1.7 billion (13.8 percent) after 2 years of decline. In contrast, expenses for fertilizer are forecast to drop $1.0 billion (4.7 percent) and feed to drop $1.9 billion (3.4 percent)