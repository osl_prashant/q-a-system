As part of its ongoing commitment to the practice of veterinary medicine, Merck Animal Health is pleased to support 18 exemplary bovine veterinary students in their educational endeavors. The company presented each student with an American Association of Bovine Practitioners (AABP) Bovine Veterinary Student Recognition Award, as well as a $5,000 scholarship, at the 2017 AABP Annual Conference.“These recipients are capable of not only providing quality cattle care, but also of serving as the driving force behind important advancements in the field of veterinary medicine in the years to come,” said Rick Sibbel, D.V.M., Executive Director of Food Animal Technical Services, Merck Animal Health. “We are pleased to support such outstanding students who will, in time, make a lasting impact on our industry.”
The following students received the 2017 AABP Bovine Veterinary Student Recognition Award:
·         Maxwell Beal, Kansas State University
·         Ben Bennett, Kansas State University
·         Justin Casares, Texas A&M University
·         Taylor Crandall, Kansas State University
·         Alex Gander, University of Wisconsin
·         Marissa Horton, Cornell University
·         Cade Luckett, Texas A&M University
·         Tara Lynch, Oklahoma State University
·         Rachel O’Leary, University of Wisconsin
·         Jenetta Porter, Iowa State University
·         Delaine Quaresma, University of California-Davis
·         Morgan Randall, University of Wisconsin
·         Cassandra Rice, Iowa State University
·         Travis Roberts, Ross University
·         Brian Schnell, University of Wisconsin
·         Kristy Shaw, The Ohio State University
·         Lauren Thompson, Texas A&M University
·         Kendra Wells, University of Wisconsin
"Support of veterinary students who are interested in bovine practice is a critical part of the mission of AABP," said AABP Executive Vice President Fred Gingrich, D.V.M. "This year, as we celebrate our 50th annual conference, we recognize the exceptional quality of bovine veterinary students, which is reflected in these 18 award recipients. We thank Merck Animal Health for its generous support of this scholarship program that honors these outstanding future colleagues.”
Veterinary students in their second or third year of school are eligible for the award, which Merck Animal Health has sponsored since 2004. Recipients are selected based on academic achievement, career goals, work experience and interest in veterinary medicine.