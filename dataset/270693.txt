Federal funding OK'd for Dona Ana County wastewater systems
Federal funding OK'd for Dona Ana County wastewater systems

The Associated Press

LAS CRUCES, N.M.




LAS CRUCES, N.M. (AP) — Members of New Mexico's congressional delegation say the federal government has agreed to provide more than $14 million to help renovate wastewater systems serving two unincorporated communities in Dona Ana County.
Sens. Tom Udall and Martin Heinrich on Friday announced that the U.S. Department of Agriculture has awarded an $8 million grant and approved a loan of $6.2 million to improve systems for Mesquite and Brazito.
The senators' announcement says the funding provided to the Lower Rio Grande Public Water Works Authority will pay for connections to eliminate the use of hundreds of septic tanks and reduce the possibility of groundwater pollution and contamination.
The two communities have 1,088 residential and 20 commercial users.