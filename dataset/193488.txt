DAP and potash were higher on the week as MAP softened slightly.

DAP $37.64 below year-ago pricing -- higher 56 cents/st on the week to $448.04/st.
MAP $32.45 below year-ago -- lower 41 cents/st this week to $467.89/st.
Potash $18.21 below year-ago -- higher 14 cents/st this week to $335.55/st.


DAP was our upside leader in the P&K segment led by Indiana which gained $6.95 as Illinois firmed 49 cents and Michigan, which added 31 cents per short ton. Eight states were unchanged as Iowa fell $1.58.
Potash firmed very slightly led by a $1.79 increase in Iowa, North Dakota added 70 cents and Indiana gained 47 cents per short ton. Seven states were unchanged as Illinois declined $1.29 per short ton.
MAP softened this week led by Illinois which fell $5.28 per short ton. Nine states were unchanged as Iowa firmed 43 cents and North Dakota added 30 cents.
Potash supplies around the world remain portly. In fact, at a conference in New York this week, an industry expert placed the world oversupply at 16.6 million tons. Despite the supply overhang and price-negative fundamentals, there are some producers actually entering the potash production space. Click here to read our article titled "Potash Producers Optimistic Despite Price-negative Fundamentals" that details some of the new projects and how investors justify building new potash production facilities in the current climate of depressed retail prices.
Phosphate prices posted very slight price moves which likely indicates demand for P&K is falling quickly as preplant applications near completion. It is unclear where prices will go from here, but the DAP/MAP spread is back to expected levels. Since MAP fell this week after working very hard over the past few weeks to rewiden the spread, we may take this as an indication of near-term price pressure. Potash is far too oversupplied to firm into the offseason, but if importers view U.S. products in surplus as a value buy, the added export demand could influence prices higher.
At this point, we expect sideways action in both P&K for the time being. As I said above, the decline in MAP pricing does seem to indicate a lower price heading into summer and that would align with the seasonal price path.


By the Pound -- The following is an updated table of P&K pricing by the pound as reported to your Inputs Monitor for the week ended May 12, 2017.
DAP is priced at 46 3/4 cents/lbP2O5; MAP at 43 3/4 cents/lbP2O5; Potash is at 28 1/4 cents/lbK2O.





P&K pricing by the pound -- 5/18/2017


DAP $P/lb


MAP $P/lb


Potash $K/lb

 



Average


$0.46 3/4


$0.43 3/4


$0.28 1/4

Average



Year-ago


$0.50 3/4


$0.47 1/4


$0.29 1/4

Year-ago