(Bloomberg) -- On Wednesday, U.S. farmers got the news they had been fearing for months. China announced plans for retaliatory tariffs against American soybeans, the nation’s No. 2 crop. As growers digested the move, their concerns ranged from the immediate drop in Chicago futures to the potential strain on long-term relationships between U.S. exporters and Chinese buyers.
The following are comments from farmers from across the Midwest:
Chris Gould, Illinois
“We were optimistic about some modest profits in 2018, and they are going to be much more modest, or there won’t be profits,” said Chris Gould, who grows soybeans and grains near Maple Park, Illinois.
As Chicago soybean futures fell as much as 5.3 percent on Wednesday, Gould expressed concern that an extended decline could wipe out expected returns for this season. He also said he’s worried that tariffs will mean China ends up building stronger trade relationships with Brazil and Argentina, which could reduce imports from the U.S. in the long term.
April Hemmes, Iowa
On Tuesday, Iowa farmer April Hemmes contracted the sale of about 25 percent of the soybeans she will produce this year. A March 29 U.S. government report had forecast smaller soy acres than analysts were expecting and sparked a temporary rally for prices. Hemmes was quick to take advantage of the gain.
The “what if” surrounding China, along with large U.S. inventories, prompted her to lock in prices, she said. In total, more than 40 percent of her soybean crop is contracted, compared with 20 percent at this time last year, she said.
“Let’s just hope this is a war of words and none of these tariffs are ever enacted,” said Hemmes, a fourth-generation grower who farms soy and corn on 900 acres near Hampton, Iowa. “These words and actions cost farmers real money.”
Bob Worth, Minnesota
Soybeans are the biggest crop on the 2,200 acres that Bob Worth farms with his son in Lincoln County, Minnesota, with the rest seeded to corn and spring wheat. He doesn’t plan to change his acreage mix, as much of this season’s harvest is already forward-sold.
The news of the tariffs is “disheartening” as any trade hiccups will be an added damper to the difficult economic conditions that farmers face, Worth said. “I hope calm minds work this out quickly and we move forward.”
Monte Peterson, North Dakota farmer
Traditionally a wheat-heavy region, North Dakota has become one of the leading soybean states and much of its production is shipped to China. Monte Peterson splits his fields near Valley City, North Dakota, 50-50 between corn and soybeans, though he had planned to boost soy acres this year. The tariffs mean he may reevaluate his acreage plans for the coming season, he said. While many countries have boosted purchases of U.S. soybeans, it would take time to offset the loss of a such a major market.
Robert Stobaugh, Arkansas
Robert Stobaugh is nearly ready for planting on the 6,000 acres he farms with his brother and nephew in Conway and Pope counties -- with about two-thirds devoted to soybeans and the rest to corn and rice. After harvest, most of the oilseed in his area gets loaded on barges for New Orleans export terminals, with China as a likely buyer.
“We can negotiate all day long, so long as by the time these crops come off the field, we aren’t selling into a further depressed market,” he said. “Those things are so far out of our hands as producers that it makes us feel extremely vulnerable.”
 
Copyright 2018, Bloomberg