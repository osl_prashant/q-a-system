After a record-breaking year in 2017, U.S. beef and pork exports are starting 2018 with strength.

According to the U.S. Meat Export Federation (USMEF), beef export volume in January was uo 9 percent year-over-year with value surging 21 percent.

Pork exports remained steady as well. Export value increased 7 percent.

Joe Schuele, communications director for USMEF, says beef and pork demand to Mexico and Canada is strong.

Hear Schuele’s discuss the beef and pork export numbers on AgDay above.