Meet Tom Jenny, a Wisconsin cheesemaker who spent 51 years honing his skills.
“I just fell in love with it, and decided I like taking milk and turning it into something a person could eat, a good wholesome food, and I just kind of stayed with it,” says Jenny who has spent the last 13 years of his career at Carr Valley Cheese.
Jenny says the friends he made along have made the years of dedication worth it.
“The cheese industry isn’t that big so you have a lot of friends, and I could call any of my friends up right now and go up and see them for a day and I could talk all day with them so, I like the camaraderie,” he says.
Watch his full story on AgDay’s “In the Country” below.