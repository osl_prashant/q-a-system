The Hogs and Pigs report released by USDA on Friday showed the June 1 inventory of all hogs and pigs to be the highest ever since estimates began more than 50 years ago. That means prices will be tight for the remainder of this year, according to livestock economists who commented on the report in a National Pork Board teleconference following the report's release.All hogs and pigs were estimated at 68.4 million head on June 1, up 2% from a year ago and up 1% from a month ago. This ties in with the breeding inventory which was up 1% from last year but down slightly from the previous quarter.
The March-May pig crop is estimated to be the largest for that time period since 1971. Sows farrowing during this period totaled 2.9 million head, up 1% from 2015, according to the report.
"The main reasons for the bigger numbers are a larger pig crop and larger farrowings," said Dan Vaught, senior economist with Doane and Pro Farmer, St. Louis, Mo. "The intentions figures were simply too low."
Ron Plain, professor emeritus at the University of Missouri, said the trade was expecting the numbers to be even to 1% higher, "but we ended up between 1-2%. The report was more bearish than expected but not a huge miss."
He noted the report showed the biggest inventory to date, which means hog slaughter will likely be a new record as well. However, with new plants coming on line next year, he doesn't see a negative on the market for too long.
On the other hand, Joe Kerns, president of Kerns & Associates in Ames, Iowa, sees some real concern with the report.
"Stuffing A Lot of Sausage""Don't forget last year when we were in a non-packer-constraint time and we were down to $48 per cwt. The larger plants aren't due to come online until spring of 2017 so we'll have lower prices through the end of this year. With 2.5 million pigs killed this fall, that's stuffing some sausage."
He's very worried about prices in the fourth quarter based on the number of animals coming to market during that timeframe.
He's also concerned about the decline in the sow herd, particularly in Iowa, saying the discrepancies in the number of breeding animals and talk of expansion doesn't seem to be showing up in this report.
"If USDA is wrong, they are consistent," Kerns said. "It's the best survey we've got. Expansion for plants coming on line doesn't seem to be in line with these numbers."
Plain pointed out that the national data comes in more accurately than any one state's data, noting there is more variability at the state level.
"The track record has been pretty good on these pig reports, especially relative to pig numbers and the slaughter report."
Still Money to be MadeCurrent futures markets give producers a head start relative to USDA's own data, Kerns said. The futures markets still hold opportunities for producers to profit from the market.
Plain agrees: "Based on Iowa State University calculations, producers made a little money last year and it looks like they'll make a little money this year, based on what the futures market is offering."
Vaught said the market was acting fairly normal until the last few days.
"Realistically demand in recent weeks in the market is banking on demand being strong in late summer. It's based on the Chinese being aggressive in their purchasing. It's not quite as bearish if we're looking at no improvement in demand. Building price protection is a good idea."
Anxiously Awaiting Expanded Packing Capacity"The industry has been on the cusp of some cutbacks in overall population but a couple things have changed to that make that scenario less likely, including new packing plants coming on line," Vaught said. "You had a decent rally in the cash market recently and futures topping 90 cents this spring."
History is likely to repeat itself in terms of expansion, believes Plain.
"The industry grows 1 to 1.5% per year, so you're going to be setting records on the number of hogs," Plain said. "Sometimes the industry likes to grow too fast and historically, we see red ink when that happens. Normally there's a lag between when the red ink shows up and when we see a cutback."
"If you follow the money, you're going to find your garden of bliss as far as predictions are concerned," Kerns said.
Producers are well-healed with 2014 profits. Even with limited returns in the near future, producers have been plowing money back in to their operations.
Price PredictionsUsing the Western Corn Belt market, Kerns predicts third quarter prices to average $80, with a deep dive in the fourth quarter to about $52 ("with a lot of basis in there - I'm very concerned for the fourth quarter," he said).
First quarter prices will hover around $68, he said, with second quarter prices raising to $73 to $75.
Plain suggests $77 carcass basis for the third quarter using the Iowa-Minnesota market; $60 to $63 for the fourth quarter and a range from $62 to $65 for first quarter 2017.
Using the CME Index, Vaught predicts and $80 average for the third quarter and prices around $60 to $61 for the fourth quarter. He, too, feels the market will bounce up to an average of $75 in the second quarter of 2017.
Watch the Ag Day TV video clip below for more infomration on both the Hogs and Pigs and Cattle on Feed reports:
Exports drive hogs, cattle on feed inventories


<!--
LimelightPlayerUtil.initEmbed('limelight_player_606547');
//-->

Want more video news? Watch it on MyFarmTV.