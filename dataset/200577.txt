Universities are ranked by many different factors, from student-professor ratios to tuition costs. For many of these options, CollegeRank.net ranks the best, and worst, of universities across the country. So as university farms have become increasingly prevalent, the website recently released its 2017 list of the best college farms in America.College Rank sorted the top farms based on student integration in sustainability efforts and training, community programming and courses taught on the farm, beyond the classroom. The focus on sustainability and organic farming lead to a concentration of the Top 35 falling along the coasts, instead of in the primary farming areas of the U.S.
If your college isn't on this list, tell us why you think it should be included.
America's Best College Farms

<!--*/

.embed-container {position: relative; padding-bottom: 80%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}
/*-->*/



Click on the pins in the map to see how each college farm stacks up, images, and a link to the school. (Bridget Beran/Farm Journal)
1. Warren Wilson College - Swannanoa, NC
2. College of the Ozarks - Point Lookout, MO
3. Deep Springs College - Big Pine, CA
4. Hampshire College - Amherst, MA
5. University of California Davis - Davis, CA
6. Evergreen State College - Olympia, WA
7. Butte College - Oroville, CA
8. Michigan State University - East Lansing, MI
9. Yale University - New Haven, CT
10. Berea College - Berea, KY
11. California State University - Chico - Chico, CA
12. University of New Hampshire - Durham, NH
13. Oberlin College - Oberlin, OH
14. University of Vermont - Burlington, VT
15. Green Mountain College - Poultney, VT
16. Goshen College - Goshen, IN
17. Clemson University - Clemson, SC
18. Pomona College Farm - Claremont, CA
19. University of Minnesota Duluth - Duluth, MN
20. Ferrum College - Ferrum, VA
21. Cornell University- Ithaca, NY
22. University of Maine - Orono, ME
23. Berry College - Mt Berry, GA
24. University of Virginia - Charlottesville, VA
25. Prescott University - Prescott, AZ
26. Washington State University - Pullman, WA
27. Universityof Idaho - Moscow, ID
28. Sterling College - Sterling, VT
29. Penn StateUniversity - State College, PA
30. Earlham College - Richmond, IN
31. Duke University - Durham, NC
32. Western Washington University - Bellingham, WA
33. California Polytechnic State University - San Luis Obispo, CA
34. College of the Atlantic - Bar Harbor, ME
35. Dartmouth College - Hanover, NH