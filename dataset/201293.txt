When USDA's weekly crop condition ratings are plugged into Pro Farmer's weighted (by production) Crop Condition Index (CCI; 0 to 500 point scale, with 500 representing perfect), it shows the corn crop dipped by 4.22 points and is now 25.31 points lower than year-ago. Meanwhile, the soybean rating dipped by 3.17 points and is 22.82 points lower than year-ago.
Corn and soybean ratings improved in Illinois, but declined in Indiana and Iowa.




 
Pro Farmer Crop Condition Index





Corn




This week



Last week



Year-ago


 


Soybeans




This week



Last week



Year-ago





Colorado (1.03%*)


3.68


3.64


3.82


Arkansas *(3.78%)


14.39


14.13


14.11




Illinois (15.40%)


56.38


56.22


61.30


Illinois (13.85%)


50.70


50.42


53.74




Indiana (6.64%)


22.05


22.11


27.10


Indiana (7.41%)


25.05


25.20


28.76




Iowa (17.72%)


66.80


68.58


67.19


Iowa
			(13.35%)


48.07


48.87


51.61




Kansas (4.29%)


15.37


15.54


14.75


Kansas (3.96%)


14.14


14.38


13.41




Kentucky (1.57%)


6.20


6.24


6.30


Kentucky (2.14%)


8.17


8.21


8.52




Michigan (2.35%)


8.69


8.64


8.91


Louisiana (1.59%)


6.22


6.19


6.40




Minnesota (9.66%)


37.58


37.87


37.28


Michigan (2.38%)


8.84


8.51


8.56




Missouri (3.81%)


14.21


14.13


13.81


Minnesota (8.82%)


33.35


33.62


33.29




Nebraska (11.62%)


42.08


42.90


46.32


Mississippi (2.38%)


9.27


8.96


9.11




N. Carolina (0.71%)


2.85


2.83


2.76


Missouri (5.86%)


21.33


21.15


21.11




N. Dakota (2.70%)


8.52


8.81


9.64


Nebraska (7.46%)


26.42


26.95


29.29




Ohio (3.80%)


13.50


13.35


15.54


N. Carolina (1.50%)


5.78


5.77


5.95




Pennsylvania (0.98%)


4.18


4.03


4.15


N. Dakota (5.24%)


16.20


17.04


17.76




S. Dakota (5.62%)


15.67


16.90


20.54


Ohio
			(6.14%)


20.95


21.62


23.21




Tennessee (0.89%)


3.87


3.86


3.51


S. Dakota (5.93%)


16.85


17.68


21.19




Texas (2.06%)


7.97


7.72


7.30


Tennessee (1.86%)


1.00


1.00


1.00




Wisconsin (3.61%)


13.39


13.57


14.22


Wisconsin (2.29%)


8.61


8.71


8.54




Corn total


363.05


367.27


388.36


Soybean total


349.41


352.58


372.23





* denotes percentage of total national corn crop production.
Iowa: Hot, dry weather continued across the state with a few reports of notable precipitation during the week ending July 16, 2017, according to the USDA, National Agricultural Statistics Service. Statewide there were 5.8 days suitable for fieldwork. Activities for the week included hauling grain, applying herbicides, cultivating, and haying. Topsoil moisture levels rated 18 percent very short, 33 percent short, 48 percent adequate and 1 percent surplus.
Over 85 percent of south central and southeast Iowas topsoil falls into the short to very short moisture level categories, while 90 percent of northeast Iowas topsoil falls into the adequate to surplus categories. Subsoil moisture levels rated 13 percent very short, 29 percent short, 57 percent adequate and 1 percent surplus. Thirty-seven percent of Iowas corn crop has reached the silking stage, 5 days behind last year and 2 days behind the 5-year average.
Corn conditions deteriorated slightly to 1 percent very poor, 5 percent poor, 23 percent fair, 58 percent good, and 13 percent excellent. A little over half of the soybean crop was blooming, with eleven percent of soybeans setting pods which is equal to the average. Soybean condition also fell to 2 percent very poor, 8 percent poor, 27 percent fair, 54 percent good, and 9 percent excellent.
Illinois: Much needed rain was reported across the state. There were 5.4 days suitable for fieldwork during the week ending July 16. Statewide, the average temperature was 76.5 degrees, 0.2 degrees above normal. Precipitation averaged 1.30 inches, 0.53 inches above normal. Topsoil moisture supply was rated at 6 percent very short, 28 percent short, 63 percent adequate, and 3 percent surplus. Subsoil moisture supply was rated at 5 percent very short, 28 percent short, 66 percent adequate, and 1 percent surplus. Corn silking jumped to 63 percent, compared to 68 percent for the 5 - year average.
Corn condition was rated at 2 percent very poor, 7 percent poor, 29 percent fair, 47 percent good, and 15 percent excellent. Soybeans blooming reached 56 percent, compared with 57 percent last year. Soybeans setting pods was at 17 percent, compared to 14 percent last year. Soybean condition was rated 2 percent very poor, 8 percent poor, 23 percent fair, 56 percent good, and 11 percent excellent.
Indiana: Rain early in the week continued to impede field work, according to Greg Matli, Indiana State Statistician for the USDAs National Agricultural Statistics Service. Warm, humid, stormy weather was followed by cooler drier weather at the end of the week. Storms in the central portion of the state brought severe winds that knocked down trees and caused property damage. The statewide average temperature was 75.1 degrees, 0.1 degrees above normal. Statewide precipitation was 1.76 inches, above average by 0.83 inches. There were 3.5 days available for fieldwork for the week ending July 16, down 0.8 days from the previous week. Regionally, corn was 27% silked in the North, 39% in Central, and 63% in the South.
Corn rated in good to excellent condition was 54% in the North, 40% in Central, and 50% in the South. Soybeans were 44% blooming in the North, 54% in Central, and 52% in the South. Soybeans rated in good to excellent condition were 53% in the North, 42% in Central, and 54% in the South.
Minnesota: Precipitation was variable during the week ending in July 16, 2017, with 5.6 days suitable for fieldwork overall according to USDAs National Agricultural Statistics Service. The week left some farmers in hope for more rain and others content with the amount they received. Field activities for the week included hay making, and spraying. Topsoil moisture supplies decreased slightly from the previous week, rating 2 percent very short, 23 percent short, 72 percent adequate and 3 percent surplus. Subsoil moisture supplies also decreased slightly, rating 1 percent very short, 14 percent short, 81 percent adequate and 4 percent surplus.
Corn was 20 percent silked, a week behind last year, and five days behind the five year average. Corn crop condition rated 79 percent good to excellent. Forty-eight percent of the soybean crop were blooming, 4 days behind average, with 10 percent of soybeans setting pods. Soybean condition rated 72 percent good to excellent.