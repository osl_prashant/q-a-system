Finding a path from Florida’s citrus industry’s rapid decline to a future with promise for growers is what Harold Browning and other citrus researchers in the Sunshine State are looking for.
While genetically engineered solutions may be five to 10 years away, researchers are working on solutions to citrus greening disease that can span the gap between then and now.
“The 60 million or so trees out there now have to be the economic bridge from where we sit with an industry that is heavily in decline to the ability to replant when new trees become available,” said Browning, chief operating officer of the Citrus Research and Development Foundation.
The foundation is heavily involved both in short- and longer-term research for solutions to citrus greening, also known as Huanglongbing (HLB). He said short-term solutions may be the most critical part of the picture right now, he said.
Florida grapefruit acreage has declined from about 108,000 acres in 2001 to 33,800 acres in 2016-17, according to the U.S. Department of Agriculture. Orange acreage in Florida has dropped from 605,000 acres in 2000-01 to 367,500 acres in 2016-17.
Even if a viable solution arrives, growers won’t be able to take out all their trees at once and then replant because of cash flow and expense issues. 
“If we fail to keep these trees alive, there is going to be a significant challenge to getting back into the game for growers that can’t afford to replant,” Browning said.
 
Short-term fixes
Browning said growers are implementing and tweaking tools they have used in the past. 
Growers report some success in manipulating timing and materials to fertilize citrus trees already suffering decline from citrus greening.
“The idea behind this is to try to optimize the ability of the plant’s roots to have continuous access to nutrients, and there is a lot of encouragement coming from that,” Browning said.
While it is not a cure to HLB, growers have had success managing tree health.
Researchers have recommended suppressing Asian citrus psyllids — which spread the disease — primarily with chemicals, but the amount of unmanaged citrus groves and the pressure on grower budgets has dimmed the appeal of pysllid control, he said.
Growers have reported improved yields of up to 15% with the use of the bactericides streptomycin and oxytetracycline, which have been approved by authorities for emergency use.
 
Longer term
Breeding programs at the University of Florida and the USDA Agricultural Research Service have done extensive work in comparing the performance of naturally disease-tolerant citrus rootstocks and plants.
To be able to give growers a strong recommendation for any given location, Browning said that more research is required.
Researchers are looking for trees in commercial groves that show unusual resistance to the greening disease, but so far have not found any promising results. Researchers also are looking for citrus varieties from around the world that might be truly resistant to citrus greening. Again, that search has been unsuccessful.
Researchers are looking at everything from gene editing to taking genes from other organisms and putting them in citrus varieties in efforts to provide resistance to HLB in citrus, he said.
Over the next few years, more products from biotechnology will be evaluated in greenhouses and limited field trials to eventually deliver plants resistant to the disease.
In April, the USDA said it would prepare an Environmental Impact Statement that will evaluate the environmental effects that may result from the potential approval of a permit application from Southern Gardens Citrus Nursery LLC, for the environmental release of genetically engineered citrus tristeza virus (CTV). 
Southern Gardens’ purpose for requesting the permit is to use the virus as a biological control agent to help manage citrus greening disease.