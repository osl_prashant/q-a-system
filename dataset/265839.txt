Preclosing
Preclosing

The Associated Press

CHICAGO




CHICAGO (AP) — Futures trading on the Chicago Board of Trade Tue:
OpenHighLowLastChg.WHEAT5,000 bu minimum; cents per bushelMay454¼457½449450—4¼Jul471½474466¼467¼—4¼Sep487¾490½483484—3¾Dec508¾511¾504½505½—3½Mar524526¾521521½—4¼May533½536530530—4¾Jul540½540½536¼536¼—5Est. sales 82,161.  Mon.'s sales 112,676Mon.'s open int 493,893,  up 9,573CORN5,000 bu minimum; cents per bushelMay374376372¼373—1Jul382½384½380¾381¼—1¼Sep389390¾387½388¼—¾Dec396¾398½395¼396—¾Mar403¾405¾402¾403¼—¾May410½410¾408408¼—1Jul414½415412412¼—1¼Sep404404402½402½—½Dec406½408¼405405—1½Mar415½417¼415½416¾+1½May419419419419—½Dec412½414412412—½Est. sales 187,162.  Mon.'s sales 340,300Mon.'s open int 1,850,911,  up 17,117OATS5,000 bu minimum; cents per bushelMay227¾228¾225½226¾+¼Jul236½236½233234—1¼Dec252252252252Est. sales 453.  Mon.'s sales 593Mon.'s open int 6,084,  up 82SOYBEANS5,000 bu minimum; cents per bushelMay10271031¼10191019¾—5¾Jul1037¾10421029¾1030½—6Aug1040¼1044¼1032¼1032¾—6¼Sep1031¼103410241024¼—6Nov1026¼1029¾1019½1020—5½Jan102910331023¼1023¼—5½Mar1026¾10311020¾1020¾—6May10281031¼1021½1021½—5¾Jul10301030¼1026¼1026¼—3¾Nov996¼997½993993—2¾Est. sales 135,536.  Mon.'s sales 188,586Mon.'s open int 870,134,  up 2,897