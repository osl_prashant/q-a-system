The Argentinean Blueberry Committee continues to promote its fruit domestically and internationally.
Those activities ramped up in October, the month of highest production for the country, according to a news release.
“We are still working on the broadcasting of this superfruit which, due to its functionality, can adapt to the current consumption needs and, as well, provides several benefits to health,” committee president Federico Bayá said in the release.
International marketing of Argentinean blueberries this month has included exhibiting at Fresh Summit in New Orleans and at Fruit Attraction in Madrid.
The hashtag for the broader campaign is #TasteTheDifference, as the committee asserts that its blueberries are sweeter than others.
The organization will also be exhibiting at Aliment.Ar, a food-centric tradeshow in Buenos Aires, in November.
The Argentinean Blueberry Committee will promote the fruit throughout the country with a focus on production zones, according to the release.
“Our goal is to reach the whole country with our campaign,” Jorge Pazos, leader of the committee’s domestic market commission, said in the release. “That is why the committee is working jointly with the regional institutions in order to make this fruit available for more Argentine consumers.”