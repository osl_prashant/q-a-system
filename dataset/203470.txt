Titan Farms donated 36,000 pounds of peaches to North Carolina Second Harvest food banks through its fourth annual Peaches with a Purpose program.
The program was launched in 2013 with the goal of feeding the underserved and raising awareness of the food scarcity problems, according to a news release.
Harris Teeter, Titan Farms' retail partner, assisted with the donation by contributing a percentage of all peaches purchased in August at the company's North Carolina locations.
Salem Produce, LLC provided assistance to the program as well by transporting the donated product.
The 36,000 pounds of peaches was split evenly between the Second Harvest locations in Winston-Salem and Charlotte, which, together, provide food assistance aid to 1,100 programs in 37 counties of North Carolina.
Kay Carter, CEO of Second Harvest Food Bank of Metrolina in Charlotte, appreciated the efforts of Titan Farms and Harris Teeter.
"Produce is an item that those living in poverty can least afford but need as part of a healthy diet. We are very grateful for this support," she said in the release.