It’s the reason USDA enumerators return to corn and soybean objective yield plots after growers harvest the field. They literally crawl on hands and knees, counting kernels and soybeans left in the field. It’s called “harvest loss.”
Until harvest loss is included, USDA enumerators measure biological yield potential. That’s the same thing we do on the Farm Journal Midwest Crop Tour, without the return trip to count kernels and beans on the ground. But biological yield potential is a long ways from “in the bin.”
Normally by this time, about 40% of the crop is still in the field. This year, closer to 60% of the crop is in the field. Where harvest is slowest and harvest conditions are most difficult, about half of the expected U.S. corn crop is still waiting for harvest.
After soggy weather rain delays gave way to a narrow window of excellent harvest last week, growers in the central and western Corn Belt were still two to three weeks behind the normal harvest pace. And that’s when the wind kicked up. Prioritizing fields by stalk quality and standability is always important, but growers that took the time to make a list this year will undoubtedly be rewarded.
And even then, this year’s harvest conditions will likely leave some yield in the field. How much? Hard to tell at this point. Will it matter to prices? Only if USDA reflects the loss in either the November Crop Production Report (not likely) or the January Annual Production Summary (maybe).
Here's what some of our Market Rally Radio listeners had to say about their fall weather yield loss on Twitter:
 

@ChipFlory Corn blown down in Renville County in Southern MN. Lots of fun to combine.... #MRRChip pic.twitter.com/q4gGNHkzMV
— Noak (@15noak) October 26, 2017


@MRRChip seeing weather stressed corn dropping ears, nothing horrible but it all adds up
— Martin Danner (@martin_danner) October 26, 2017


Poor weather, tough beans, one crescent pliers loss #MRRChip
— Tanner Deering (@DeeringTanner) October 26, 2017


Today’s struggle.....

Watch your corn fall over from the wind or fight with a tarp in 40+ mph winds #MRRChip
— misunderstood (@dubyadirt) October 26, 2017


@MRRChip few ears dropped, corn leaves headed your way. Listen to the audio on video pic.twitter.com/cTOWGUlVi8
— Randy Schwab (@SchwabRandy) October 26, 2017


@MRRChip
Wind is breaking off ears of corn. Counted 30 in one row on quarter mile rows. South Central Nebraska
— Micah Schutte (@MicahScmic7) October 26, 2017


@ChipFlory Lots of corn left in the field with snow coming late tonight and tomorrow in southern MN. Should be interesting #MRRChip
— Noak (@15noak) October 26, 2017


Being 2-3 weeks behind schedule from rain and now severe wind...harvest loses are becoming significant. #mrrchip #harvest17
— Randy Uhrmacher (@Cornfrmr) October 26, 2017