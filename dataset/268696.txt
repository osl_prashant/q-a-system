Grain industry injuries range from grain bin explosions and entrapment to respiratory diseases. Most incidents can be avoided however through proper maintenance, procedures and training.
John Lee, safety, health and environmental services director for the Grain and Feed Association of Illinois, says over all, the grain elevators he visits work hard to operate safely.
“They’re aware of the hazards,” says Lee. “It’s the ‘it won’t happen to me’ mentality that is the problem.”
Although Purdue University’s grain dust explosions reports show a steady decline over the past decade, the 2017 research reported seven grain dust explosions in the U.S., two more than in 2016. The number of fatalities also rose in 2017 from three to five.
Kingsley Ambrose, lead author of the report, is optimistic about the future of grain safety.
“It’s good to see that trends on these overall reported incidents are going down,” says Ambrose. “The negative is that the incidents are still happening. Through safety as a whole, from training to types of sensors we use, down the road we can probably bring down these incidents to zero.”
With grain dust explosions, the main factors at play are outlined in what Purdue calls the grain dust explosion pentagon. By eliminating just one of the five factors, an incident can usually be avoided.
The factors include: grain dust, dispersion, confinement, oxygen and ignition.
“Four of them are almost unavoidable,” says Ambrose. “The easiest factor to control is the ignition source. If you can prevent that, we can avoid an explosion.”
To avoid an ignition source, Ambrose says preventative maintenance is important. Scheduling regular maintenance of equipment and keeping a careful watch on electrical systems and wiring can help this. Cleanliness and avoiding dust accumulation is also vital to prevent ignition.
Grain safety issues don’t stop at grain dust explosions. Grain entrapment and respiratory diseases are also of serious concern to ag retailers.
However, all three of these issues can be further prevented through following procedures and proper training.
“Our grain bin procedure is 16 steps starting with identifying hazards before you enter,” says Daniel Gaither, safety manager for Premier Cooperative. “Before anyone enters a bin at Premier, an email has to be sent to me. That’s been helpful so I know where to visit and make sure they know how to follow the rules.”
Outside of company procedures, organizations such as National Fire Protection Association (NFPA), Occupational Safety and Health Administration (OSHA) and National Institute for Occupational Safety and Health (NIOSH) also create standards for grain safety.
For example, OSHA’s standard on respirators includes instructions on when a respirator is required and under what conditions a specific respirator should be used.
Respirators are used to protect from particles that are so small they can be breathed in and cause respiratory damage including chronic and acute lung disease. See sidebar for more information.
Frequent training can also increase employee safety.
Premier hosts grain bin entry training twice a year, a general policy training once a year and a speaker on safety once a year.
“I also show videos on accidents and injuries that have occurred from improper handling,” says Gaither. “We try to make it hands on and mix it up every year.”
Training that shows the hazard of things inside the bin is critical says Lee. This includes things like grain flow, bridging, pyramids and atmospheric issues.
Gaither shares this sentiment.
“Grain safety is so important, because we’ve had injuries in the past at Premier, and no one wants to see that ever again,” says Gaither. “My main goal is to make sure every employee makes it home safe at night.”

Five Myths You Might Believe About Respirators
Respirators keep those who work around dust and other particulate matter from inhaling damaging materials. The dust found in grain elevators can cause diseases like chronic and acute lung disease. To keep yourself and others safe, learn these five common myths about respirators from John Lee, safety, health and environmental services director for the Grain and Feed Association of Illinois.
Myth: Any respirator will do.
Truth: A respirator should be approved by National Institute for Occupational Safety and Health, have two straps rather than one and should be specific to the conditions you are working in. For example, respirators are made differently for grain dust from barley and oats than they are for grain dust from corn and soybeans.
Myth: Going into an elevator once without a respirator won’t hurt me.
Truth: Some sicknesses, like Organic Dust Toxic Syndrome, can occur after just one heavy exposure to dust contaminated with fungi, mycotoxins, endotoxins, glucans or bacteria. Symptoms are flu-like and include aches, pains, headache and lethargic feelings.
Myth: If I wear a respirator, I will not get sick.
Truth: NIOSH approved respirators keep out 95 percent of respirable debris. Unfortunately, you can still get sick from the other 5 percent. Limit your exposure when possible.
Myth: Respirators are one size fits all.
Truth: The type of respirator you can wear depends partly on the fit. Not everyone can wear the same type because of differences in face shape and size.
Myth: I can wear a respirator with facial hair.
Truth: It depends. If your company requires the mask, then you can’t have a beard. However, if it is a voluntary use, then facial hair is acceptable. Additionally, if the mask is greater than an N95, you can’t have facial hair, because you can’t have a beard and do a fit test.