“If you had been alive in America in 1877, chances are 50-50 you would have lived on a farm,” reads the dust jacket of “100 Years of Farm Journal.” The first issue of Farm Journal, one of the oldest farm magazines in the country was March 1877, and the “100 Years” book was distributed in 1977. The company and magazine is now 140 years old.
The magazine was started by Wilmer Atkinson, described as “a crusading Quaker gentleman.”
“It is full of snap and ginger and hits the nail squarely on the head,” Adkinson said of his fledging magazine. That’s what Farm Journal Media editors continue to strive for, yet today.
That 100-year history provides a glimpse of rural life as it was “during the Civil War, in the proper Victorian era, and in the “golden age” of agriculture, just prior to World War 1. The first editor of Farm Journal experienced it all and didn’t spare his opinions or his advice.”

Farm Journal has chronicled it all: wars, the great depression, and the amazing advancements in science and technology that serve as the foundation of agriculture today.
“The importance of the farm was never so clear cut as it is today,” the book stated. “The effects of his work are felt in the farthest corners of the world.”
It was true in 1877, true in 1977, and even truer today, 140 years later.
Editor’s Note: Surprisingly, copies of the “100 Years of Farm Journal,” are available on Amazon. Click here for more information.