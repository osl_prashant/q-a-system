Thriving Utah goat farm provides halal meat for refugees
Thriving Utah goat farm provides halal meat for refugees

The Associated Press

SALT LAKE CITY




SALT LAKE CITY (AP) — A goat farm started several years ago by three African refugee communities to create a source of affordable goat meat is thriving with more than 100 births and a slew of volunteers.
Refugee Ismael Mohamed said the ranch provides a source of affordable, high-quality meat for the more than 7,000 refugees from Somali Bajuni, Burundi and Somali Bantu who live along the Wasatch Front, The Salt Lake Tribune reported Tuesday.
Mohamed said the protein that the refugees were accustomed to eating in their homeland was difficult or expensive to find in Utah, mainly because it must be processed in the proper halal manner, required by those of the Muslim faith.
"By ourselves, we couldn't do this," Mohamed said. "Where we are is because of the volunteers."
Each new member of the herd is welcomed into the world by a volunteer "kid sitter" who — just like a maternity ward nurse — keeps watch over the pregnant does as they prepare to deliver.
The farm started in 2013 with 40 goats. By 2017, the herd had grown to about 250. Once the kidding season ends in 2018, it will be nearing 500.
The refugees pay an employee — a fellow refugee with animal husbandry experience — to maintain the herd during the week, while refugee families pitch in on the weekends to ensure the animals have food and water, Mohamad said.
Processing the meat and selling it to other refugees bring in income, as do grazing fees, Mohamed said. Large landowners rent portions of the herd to clear overgrowth and noxious weeds and, in the process, prevent field fires. Profits go into a scholarship fund.
___
Information from: The Salt Lake Tribune, http://www.sltrib.com