Along with the Naturally Imperfects, the Misfits and other ugly but still delicious fruits and vegetables being bagged and sold across North America comes a new brand from Quebec.“Hors la Loi” (Outlaws), or Rebels in English, launched in the province’s Metro stores on June 1.
The irreverent brand, with its bright colors and cartoon figures, is the brainchild of Joël Lalancette, better known as co-owner of La Frissonnante greenhouse strawberries; Marie Gosselin, president of marketing firm Mur Conseil; and produce veteran Jean-Francois Beauregard, known for his decades of buying experience and good relationships with Quebec growers.
After working on the project for more than a year, the trio and three silent partners have launched seven Stock-Keeping Units of Rebels, comprising of potatoes, carrots, peppers, onions and apples in 2- to 5-pound bags, as well as Quebec greenhouse cucumbers and tomatoes in bulk that don’t meet the No. 1 grade.
Gosselin said the products will be sold year-round at prices 30% cheaper than No. 1 produce, and she doesn’t see any problem getting enough supply.
“Nature is nature,” she said, “It’s not always perfect.”
Depending on the product and the season, she said 5% to 25% of fresh produce is currently being rejected because it doesn’t meet the Canada No. 1 standard.
“Some are thrown away or sold for a low price,” she said.
While the program aims to use as much Quebec produce as possible, Gosselin said they’re sourcing carrots from the U.S. and Ontario until the local crop becomes available.
The name and packaging aim to play up the charm and nutrition content of the “ugly but tasty” produce rather than portray it as discards, she said, while giving all consumers, no matter what their budget, the chance to add more fresh produce to their grocery cart.
She said the brand also resonates with young people who care about not wasting food.
“There’s a new generation coming on the market that cares about food waste and we have to address their needs,” she said.
The group plans to expand Rebels to Ontario soon and consider a U.S. launch next year.