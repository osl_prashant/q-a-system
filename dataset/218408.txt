Just as negotiators made their way to Montreal, Canada for round six of talks to renegotiate the North American Free Trade Agreement (NAFTA), a new study was released outlining the cost of withdrawal for the 10 states that benefit the most from the trade deal. Farmers For Free Trade, a non-profit group comprised of several commodity groups including the National Pork Producers council, released a new study called The NAFTA Withdrawal Tax. In it, the group outlines how much life without NAFTA would cost farmers and the economy.
“Just as farmers and ranchers have been among the biggest beneficiaries of NAFTA, they’d also be the ones to feel the most pain if America withdraws from the pact,” say Farmers for Free Trade Co-Chairs, Senators Max Baucus and Richard Lugar. "That’s because NAFTA withdrawal would result in a massive tax on the products American farmers grow and produce."
Without NAFTA, cheese would face a tariff up to 45%. Using 2016 cheese exports to Mexico totaling $170 million, the tariffs would add up to $76.5 million in additional duties without NAFTA. Similarly, without NAFTA, beef would be subject to a 25% tariff. In 2016, the U.S. shipped $867 million of beef to Mexico. With a tariff in place, that would be $217 million in additional duties. Chicken and pork would also become far more expensive, with tariffs costing $653 million and $130 million respectively.
“It’s not only farmers who would pay the price for NAFTA withdrawal,” explains Baucus. “Rural communities, which run on the engine of the farm economy would suffer too. If farmers have less in their pockets, they spend less at local stores, restaurants, and contribute less to local schools and community organizations. Jobs throughout the agricultural production cycle that depend on trade suffer, including growers, harvesters, processors, packagers, as well as grain elevator operators, railroad workers,  and truck drivers. The simple truth is that rural communities don't work well when their economic engine is hurting."
Here are the top 10 sates depending on Mexican Exports:
#1 MISSOURI: 51% of all agricultural exports to Mexico
#2 NEW MEXICO: 45% of all agricultural exports to Mexico
#3 SOUTH DAKOTA: 39% of all agricultural exports to Mexico
#4 TEXAS: 37% of all agricultural exports to Mexico
#5 NEBRASKA: 36% of all agricultural exports to Mexico
#6 IOWA: 35% of all agricultural exports to Mexico
#7 KANSAS: 28% of all agricultural exports to Mexico
#8 ARKANSAS: 27% of all agricultural exports to Mexico
#9 NORTH DAKOTA: 25% of all agricultural exports to Mexico
#10 MINNESOTA: 24% of all agricultural exports to Mexico