(CORRECTED) Buckeye, Ariz.-based Duncan Family Farms has established operations in New York’s Monroe County. 
New York Governor Andrew Cuomo announced that Duncan Farms, a multi-region grower of organic baby leaf items and other greens, has partnered with agribusiness Newstead Ranch for its operations in Brockport, N.Y., according to a news release.
 
“The economic momentum in the Finger Lakes is clear — and with this exciting announcement, it continues strong,” Cuomo said in the release. “By expanding in Monroe County, Duncan Family Farms will create jobs and encourage further economic activity — helping to continue to push the Finger Lakes further forward.” 
 
Duncan Family Farms, with existing organic acreage of 8,000 in Arizona and California, provides product for many bagged salad processors in the U.S., Canada and in the United Kingdom, according to the release and the company’s website. 
 
The company also grows several hundred acres of conventional cabbage and red potatoes and directly markets bunched root crops, bunched herbs and specialty bunched greens for retail and foodservice customers.
 
“We are excited to have the opportunity to expand into the beautiful Finger Lakes region of New York,” Caleb Ayers, general manager of Duncan Family Farms Northeast, said in the release. “Being able to bring new partnerships, commodities and innovation to an area that is steeped in American agricultural tradition is an absolute honor.”
 
Empire State Development has offered up to $865,000 in performance-based Excelsior Jobs Program tax credits to Duncan Family Farms, according to the release. The total project cost is projected at $11.8 million, and the release said the project will result in the creation of 50 new jobs.
 
Michael Kreher, a family partner in Newstead Ranch, based in New York, said in the release that the company is looking forward to working on the project with Duncan Family Farms. “It is particularly exciting to grow these crops closer to where people are eating them, providing a fresher product to consumers in our region,” he said in the release.
 
 
Note on Correction: The original version incorrectly identified the headquarters for Newstead Ranch.