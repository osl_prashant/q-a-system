Negotiators from Canada, Mexico and the United States are meeting for a second round of talks to renegotiate the North American Free Trade Agreement, amid threats by U.S. President Donald Trump to pull out of the deal.NAFTA, first implemented in 1994, eliminates most tariffs on trade between the United States, Canada and Mexico.
Critics say it has drawn jobs from the U.S. and Canada to Mexico, where workers are badly paid. Supporters say it has created U.S. jobs, and the loss of manufacturing from the United States has more to do with China than Mexico.
Key issues facing negotiators include:
Rules of Origin
NAFTA says in order for a good to be traded duty-free within the three countries, it must contain a certain percentage of North American content, which differs for various products. The rule of origin is most contentious in the auto industry; cars must contain at least 62.5 percent American, Canadian or Mexican content.
The United States wants to increase the content threshold for NAFTA goods in a bid to return manufacturing jobs to the United States, and the auto industry has conceded that the rules should be updated to account for auto components that did not exist when the original deal was signed.
Canada has said it is prepared to discuss some strengthening of rule of origin in the auto sector, but any change must apply equally to all three countries.
Mexico is willing to look at strengthening rules, but warns that going too far will make the region less competitive.
Dispute Resolution
The United States has sought to ditch the so-called Chapter 19 tool, under which binational panels hear complaints about illegal subsidies and dumping and then issue binding decisions.
The United States has frequently lost such cases since NAFTA came into effect in 1994, and the mechanism has hindered it from pursing anti-dumping and anti-subsidy cases against Canadian and Mexican companies. Washington also argues that Chapter 19 infringes on the sovereignty of its domestic laws.
Canada has said Chapter 19 can be updated, but said a dispute settlement mechanism is its “red line” and must be part of any updated NAFTA.
Mexico also says dispute settlement mechanisms are a vital part of the deal to give investors security.
Protecting Agriculture
U.S. negotiators are seeking to allow U.S. seasonal produce growers to file anti-dumping cases against Mexico. Seasonal fruit and vegetable growers in the southeastern United States have come under increasing pressure from year-round Mexican imports under NAFTA and are seeking the ability to pursue anti-subsidy and anti-dumping cases or seek temporary import quotas.
But U.S. retailers and food industry groups argue that American producers could be left open to retaliatory measures if more complaints were to be filed, for instance, against avocados, tomatoes and other produce imported from Mexico.
Supply Management
Quotas are a feature of NAFTA in several agricultural commodities including dairy and sugar, but Washington is seeking to eliminate non-tariff barriers to U.S. agricultural exports.
Most notably, U.S. President Donald Trump has called Canada’s restrictions on dairy imports a “disgrace.”
Although dairy was excluded from the original 1994 deal, the United States is seeking to eliminate non-tariff barriers to its agricultural exports.
Currency Manipulation
The United States is seeking a provision to deter currency manipulation. While Washington wants a mechanism to ensure the NAFTA countries avoid tinkering with exchange rates to gain a competitive advantage, neither Canada nor Mexico is on the U.S. Treasury’s currency manipulation watch list.
Critics say the U.S. demand is an attempt to get currency manipulation into a global trade agreement to establish a precedent with other trading partners, including China.
Government Procurement
The United States is pushing for governments in Canada and Mexico to open up their tender processes to U.S.-made products but at the same time is defending existing “Buy American” procurement laws.
The Buy American provisions have blocked the use of Canadian steel to build U.S. bridges, and Canada is pushing for a freer market for government procurement.
Mexico says it expects government procurement, already included in NAFTA, to be part of the renegotiation.
Investor-State Dispute Settlement
The United States has proposed minor tweaking of the NAFTA Chapter 11 provisions, designed to ensure that firms that invest abroad receive “fair and equitable” treatment by foreign governments.
As with Chapter 19, opponents of the provisions argue they infringe on sovereignty which benefits multinational corporations. Canada wants to update the mechanism to allow governments to regulate in the interest of the environment or labor, as in the Comprehensive Economic and Trade Agreement that Canada recently negotiated with the European Union.