Think Iowa farmland is pricey? Cast your eyes to Europe for a little perspective. Here are the countries with the highest farmland prices in European Union member states, according to a new report (featuring 2016 data) from Eurostat.

Netherlands
Italy
Luxembourg
United Kingdom
Ireland
Denmark
Slovenia
Spain
Greece
Slovakia

The Netherlands’ per-hectare price of farmland for 2016 was 63,000 euros.
To help with the math, the current exchange rate for euros to U.S. dollars is 1 to 1.24. So, 63,000 euros equals $78,031. And, 1 hectare equals about 2.5 acres. That means 1 acre of land in the Netherlands is worth around $31,200.

Source: Eurostat
The price of farmland in Europe depends on several factors, according to Eurostat. They are:

national laws
climate
soil quality, slope, drainage etc.
market forces of supply and demand, including the influence of foreign ownership rules

In almost all regions, buying arable land was more expensive than permanent grassland. Additionally, irrigated arable land was more expensive than non-irrigated.
The strongest growth in arable land prices between 2011 and 2016 was in the Czech Republic (a three-fold increase), Lithuania, Estonia, Latvia and Hungary (a two-fold increase). 
The highest farmland rent price was also in the Netherlands. The average per-year rental rate is 791 euros per hectare. The cheapest per-hectare annual rent rate—46 euros—is in Latvia.

Source: Eurostat