Oakes Farms in Immokalee, Fla., saw product looking good even in the wake of Hurricane Irma, which blew through the state in early September but is still affecting the season for many companies. Photo courtesy Oakes Farms.
Florida vegetable growers anticipated strong quality in the aftermath of Hurricane Irma.
A few issues popped up in the wake of the storm, but most companies reported in mid-November that quality was already very high and that it would only get better in the following weeks.
Jon Browder, sales manager for Belle Glade, Fla.-based Pioneer Growers, had high expectations for corn, green beans, radish and leafy items.
“Quality’s still going to be very nice when we do get started because we did replant, so none of the plantings went through the actual storm,” Browder said. “It was just flooded out (and) then we replanted new seed, but everything quality-wise should be very nice for December through the winter.”
Brett Bergmann, a partner in South Bay, Fla.-based Branch: A Family of Farms, gave a similar report on corn and green beans.
“So far the quality that we’ve seen has been excellent,” Bergmann said.
Steve Veneziano, vice president of sales and operations for Oakes Farms in Immokalee, reported the vast majority of product looked great.
“I would say 90% of it is looking 10 out of 10, 10% of it maybe 6 or 7 out of 10,” Veneziano said in mid-November. “For the most part (we’re) extremely happy with all commodities.”
Calvert Cullen, president of Cheriton, Va.-based Northampton Growers Produce Sales, also said quality would be solid once the season moved into full swing.
“Once we get past this little window, we expect quality and volume to be like normal, to be good,” Cullen said at the time. “We’ve just got to get past this next couple weeks.”
Jim Monteith, sales manager for Myakka City, Fla.-based Utopia Packing, also conveyed that quantity rather than quality has been the issue post-Irma.
“On these early plantings the pepper didn’t quite size up the way it should because of the weather, peaking on extra-large and large versus having any real big jumbos,” Monteith said in mid-November. “Hopefully we’re going to see that change once we get into the plantings that were put in the ground after the storm.”