Recruiting employees for your farm has never been tougher and it will get worse, says long-time headhunter Dan Simmons of Oceanview, Del. But if you can figure out how to bridge the generational divide, your business will thrive as we approach the 2020s.
The generation in which you came of age frames your point of view. Baby boomers communicate differently than Gen Xers (email versus text). Gen Xers will hold different values about a job than later millennials (life balance versus passion to make a difference). Gen Zers (ages 2 to 20) are more influenced by social media than anything else.
Unless you can speak to  the “hot buttons” that fire up the talented, creative potential employees you hope to attract, you will continue to suffer what Simmons calls “employment dysfunction” – a malady of national scope.
Tips to Becoming the Workplace of Choice
To excel in labor management in the present time and culture, try these 10 tactics:

Write job descriptions that clearly detail the work to be done.
Create formal training, onboarding and retraining programs.
Articulate your farm’s management style and goals and share them online via website, Facebook and YouTube.
Provide much more frequent performance feedback – 30 seconds here and there pays off in spades.
Share why you’re in business so staff can find meaning and purpose in the work they do.
Encourage employees to bring their fresh ideas to the table.
Worry a little less about offering health insurance and retirement.
Worry a little more about providing steps on a career path instead of offering just a job.
Plan for turnover, expecting few workers to stay more than three years.
Find out what the competition is paying for comparable positions and beat that price.

Be Brave
Does this vision make you squirm? Simmons’ 14 years of experience recruiting workers for ag businesses that support farms reveal how many ways employers aren’t on the same wavelength with today’s job-seekers. Waste no time testing out some new labor-friendly tactics you’ve never tried – or even imagined – before.