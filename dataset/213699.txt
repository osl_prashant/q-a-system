Not all flood control structures are created equal. Here a pond, there a pond, everywhere a pond, pond… or at least that was how it seemed for this young soil conservationist working in Woodbury County back in the mid-‘80s. One site seemed as good as the next for flood control, and we built a lot of ponds. Basically, there were three requirements. First, we had to secure consent from the landowner to build the pond. Second, we needed a positive cost-benefit ratio. Third, we needed to obtain adequate erosion control in the upstream watershed. The formula was pretty simple.


 


For those acquainted with the history of flood control and NRCS/SCS watershed programs, you will recognize Woodbury County, Iowa as the heart of the Little Sioux Watershed. And you will probably recognize the Little Sioux Watershed was the granddaddy of all NRCS/SCS watersheds approved nationwide for flood control. (Or, at least that is what I was told as a young conservationist – and I might be biased since I worked there.)


 


If we are going to return to the days where we can use small watershed structures to reduce flooding, we must do a better job at using precision conservation to identify the most cost effective sites. (NRCS Photo)


 


I know that it has been a bunch of years, but I recall while working in the Little Sioux Watershed, the placement of flood control structures seemed to happen in a very random way. But, we built a lot of small flood control structures. Those were the days. The Flood Control Action of 1944 (or PL-534) authorized the Secretary of Agriculture to install watershed improvement measures to reduce flood, sedimentation, and erosion damages. The Little Sioux Watershed was one of 11 watersheds chosen to receive special funding over decades of time.


 


Since then, it seems the public support for building small watershed structures for flood control has all but evaporated. NRCS’s two major flood reduction programs, PL 534 and PL 566, have been all but abandoned. Sure, there may be a couple watersheds building flood control practices, but nothing like the past. I’m told one of the primary reasons for losing this funding for flood control is that it has become more difficult to demonstrate the cost-benefit ratio.


 


Looking back on my days in Woodbury County, it seems we did things backwards. If we could have systematically analyzed the watershed and sought out the best locations for flood control structures, I believe the Little Sioux Watershed could have achieved far better flood control results with a better cost-benefit ratio. After identifying the most effective locations for flood control structures, we could have systematically approached those landowners and attempted to secure easements to build dams.


 


The two things that have a significant impact on the ability of a pond to reduce flooding are the size of the temporary pool and the size of the pipe. The larger the temporary pool, the more water a pond can store before it is released through the pipe. The smaller the pipe, the slower water is released.


 


So you are probably asking, “It sounds simple enough. Then why isn’t this being done?” Actually, only in the past few years has the technology been available to complete this analysis. LiDAR data has allowed us to develop specialized software like PondBuilder where a pond estimate can be completed in 10 minutes. Now, a conservation planner can easily plan 30 pond locations in one day. With one week of work, a conservation planner can site 150 structures. With another week of work, the conservation planner can rate the 150 sites and determine the best locations of ponds for reducing downstream flooding.


Agren’s PondBuilder was used to plan 20 different ponds in a watershed in Jasper County, Iowa. The ponds were then ranked for the amount of storage, in acre-feet, of the temporary pool. Some of the sites are considerably more affective than others for flood control.


 


Agren’s PondBuilder was used to plan 20 different ponds in a watershed in Jasper County, Iowa. The ponds were then ranked for the amount of storage, in acre-feet, of the temporary pool. Some of the sites are considerably more effective than others for flood control.


 


Precision conservation is putting the right practice, in the right place, to maximize environmental benefits. We can do a better job placing ponds to reduce flooding, but in order to do so we need to use software that provides us with more information in less time