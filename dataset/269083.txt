Grain mixed and livestock higher
Grain mixed and livestock higher

The Associated Press



Wheat for May fell 8.50 cents at 4.7250 a bushel; May corn was down 2.50 cents at 3.8625 a bushel; May oats was off 3.25 cents at $2.3425 a bushel; while May soybeans lost 6.50 cents at $10.5425 a bushel.
Beef was higher and pork was lower on the Chicago Mercantile Exchange. April live cattle was up .80 cent at $1.1655 a pound; April feeder cattle rose .50 cent at 1.3937 a pound; while April lean hogs fell .45 cent at $.5385 a pound.