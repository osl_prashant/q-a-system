BC-APFN-Money & Markets Digest
BC-APFN-Money & Markets Digest

The Associated Press



Money & Markets modules for Wednesday, March 21
TODAY
The Federal Reserve is widely expected to raise interest rates again; the National Association of Realtors reports on sales of previously occupied homes; and food maker General Mills reports its latest quarterly results.
SPOTLIGHT
McDonald's is taking steps to cut the greenhouse gases the company emits in its restaurants and in the production of its packaging and beef in its Big Macs and other burgers.
CENTERPIECE
Is Stitch Fix still fashionable?
Stitch Fix, the styling service that sends outfits to subscribers' doorsteps, posted disappointing quarterly earnings last week. But the company is still in vogue with some Wall Street analysts.
STORY STOCKS
Oracle (ORCL)
SandRidge Energy (SD)
Facebook (FB)
BlackBerry (BB)
Children's Place (PLCE)
Ashland Global Holdings (ASH)
CSRA (CSRA)
Sempra Energy (SRE)
FUND FOCUS
Akre Focus (AKREX)
This fund is "decisive and differentiated," Morningstar says, noting that its "Silver" analyst rating is merited in part because of its ability to seize upon market opportunities.
For questions about Money & Markets modules, please contact Seth Sutel (212-621-1618). For technical support: contact Todd Balog (816-654-1096). After 6 p.m., contact the AP Business News desk (800-845-8450, ext. 1680) for content questions; 1-800-3AP-STOX for technical support and 212-621-1905 for graphics help. The Money & Markets digest can also be found at www.markets.ap.org.