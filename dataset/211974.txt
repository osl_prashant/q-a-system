The United Fresh Insights for Foodservice summer report shares new data on avocados, potatoes, tropical fruit and other commodities. 
The report, developed in partnership with market research firm Datassential and sponsored by Tanimura & Antle, is free to United Fresh Members and $50 to non-members at www.unitedfresh.org. 
 
The 26-page paper features “In Season” and “On the Horizon” updates, according to a news release. The report’s “In Season” section covers the cold press juicing trend, tropical fruits, endive and other commodities.
 
Looking forward to the winter season, the report’s “On the Horizon” looks at avocados, potatoes and the use of winter produce items for topping pizzas, according to the release.
 
Avocados have seen double digit gains in all sectors of foodservice over the past four years, according to the report.
 
The use of avocados in quick-service restaurants jumped 24% in the past four years, compared with 15% growth over the same period for fast casual. Mid-scale restaurants saw 19% growth in use of avocados over four years, with 14% for casual and 12% for fine dining. The report also describes use of avocados by cuisine, from Mexican, American, French and steakhouse formats.
 
“The winter season provides a prime opportunity to feature produce in menus,” Jeff Oberman, vice president of trade relations for United Fresh, said in the release. “Planning for menu promotions start months in advance, and produce companies should begin reaching out now to foodservice distributors and operators to develop exciting menu items that will capitalize on the coming holiday season and special events like the Super Bowl.”
 
The quarterly report’s “Top Chain Trends” spotlights menu items featuring sweet corn, onions and blueberries, according to the release. 
 
Also in the report is an interview with 2017 Produce Excellence in Foodservice Award winner chef Matt Smith, executive chef of the Sheraton Columbus Hotel at Capitol Square.