UN reports see a lonelier planet with fewer plants, animals
UN reports see a lonelier planet with fewer plants, animals

BY SETH BORENSTEINAP Science Writer
The Associated Press

WASHINGTON




WASHINGTON (AP) — Earth is losing plants, animals and clean water at a dramatic rate, according to four new United Nations scientific reports on biodiversity.
Scientists meeting in Colombia issued four regional reports Friday on how well animal and plants are doing in the Americas; Europe and Central Asia; Africa; and the Asia-Pacific area.
Their conclusion after three years of study: Nowhere is doing well.
The work was about more than just critters, said study team chairman Robert Watson. It is about keeping Earth livable for humans, because we rely on biodiversity for food, clean water and public health, the prominent British and U.S. scientist said.
"This is undermining well-being across the planet, threatening us long-term on food and water," Watson said in an interview.
What's happening is a side effect of the world getting wealthier and more crowded with people, Watson said. Humans need more food, more clean water, more energy and more land. And the way society has tried to achieve that has cut down on biodiversity, he said.
Crucial habitat has been cut apart, alien species have invaded places, chemicals have hurt plants and animals, wetlands and mangroves that clean up pollution are disappearing, and the world's waters are overfished, he said.
Man-made climate change is getting worse, and global warming will soon hurt biodiversity as much as all the other problems combined, Watson said.
The reports project that, if current trends continue:
— By the year 2050 the Americas will have 15 percent fewer plants and animals than now.
— In Asia, there will be no fish stocks for commercial fishing by 2048.
— More than a quarter of the species that live only in Europe are threatened now.
— Africa could lose half of some bird and mammal species by 2100.
The outlook is bleak if society doesn't change, but it still can, Watson said.
"Some species are threatened with extinctions. Others, just pure numbers will go down," Watson said. "It will be a lonelier place relative to our natural world. It's a moral issue. Do we humans have right to make them go extinct."