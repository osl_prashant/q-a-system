President-elect Donald Trump will name first-term Republican U.S. Representative Ryan Zinke of Montana, a former Navy SEAL commander, as his interior secretary, according to media reports on Tuesday.A senior transition official who spoke to Reuters described Zinke, 55, as a leading candidate for the job.
Zinke will be nominated to head the Interior Department, which employs more than 70,000 people across the United States and oversees more than 20 percent of federal land, including national parks like Yellowstone and Yosemite.
Trump's decision to pick Zinke was first reported by Politico. The Washington Post and the Wall Street Journal also said Zinke had been tapped for the post.
Zinke is a proponent of keeping public lands under federal ownership, putting him at odds with some in his Republican Party who are more favorable to privatization or placing them under the control of states.
It remains unclear where Zinke would stand on opening up more federal lands to increased drilling and mining, something Trump promised he would do as president.
Trump's official energy platform calls for opening "onshore and offshore leasing on federal lands, eliminate moratorium on coal leasing, and open shale energy deposits."
A Trump aide told Reuters last week that another Republican U.S. representative, Cathy McMorris Rodgers of Washington state, had been picked for the post. On Tuesday, a source close to the congresswoman said she had never been offered the job.
Politico reported earlier on Tuesday that Trump had expanded his search to include Zinke and Republican U.S. Representative Raul Labrador of Idaho as well as McMorris Rodgers.
Zinke was an early Trump supporter, backing the New York businessman for president in May. His nomination must be confirmed by the Republican-controlled Senate.
"Congressman Zinke is a strong advocate for American energy independence, and he supports an all-encompassing energy policy that includes renewables, fossil fuels and alternative energy," Trump spokesman Jason Miller said before a meeting on Monday between Zinke and Trump at Trump Tower in New York.
Zinke, a member of the House of Representatives subcommittee on natural resources, has voted for legislation that would weaken environmental safeguards on public land.
But unlike other candidates who were on the short list for the interior secretary job, Zinke opposes the transfer of public lands to the states, a position that echoes Trump's.
Public Lands
Trump has said he does not think public land should be turned over to the states and should be protected.
"I don't like the idea because I want to keep the lands great, and you don't know what the state is going to do," Trump said in an interview with Field & Stream magazine in January.
Trump said that putting states in control of public land would make it easier to sell it off for energy or commercial development. He thinks the federal government needs to focus on conservation.
"I mean, are they going to sell (states) if they get into a little bit of trouble? I don't think it's something that should be sold," he said. "We have to be great stewards of this land. This is magnificent land. And we have to be great stewards of this land."
In July, Zinke resigned as a delegate to the Republican nominating convention because the party platform called for transferring public lands to the states.
"What I saw was a platform that was more divisive than uniting," Zinke told the Billings Gazette. "At this point, I think it's better to show leadership."
Public land comprises more than 30 percent of Montana, according to the Montana Wilderness Association.
The League of Conservation Voters, which ranks lawmakers on their environmental record, gave Zinke an extremely low lifetime score of 3 percent.
The Wilderness Society, a leading conservation group, said it was concerned by Zinke's support for logging, drilling and mining on public lands.
"His overall record and the backdrop of Cabinet nominations with close ties to the fossil fuel industry cause us grave concern," Wilderness Society President Jamie Williams said in a statement.
The Interior Department also oversees the Bureau of Indian Affairs and handles tribal policy.
Under Obama, the department played a big role in efforts to curb the effects of climate change by limiting fossil fuel development in some areas.
Outgoing Interior Secretary Sally Jewell banned coal mining on public lands, canceled leases for drilling on the Arctic and Atlantic coasts, expanded wildlife protection and cracked down on methane emissions.