Last US rubber band maker in Arkansas fights competition
Last US rubber band maker in Arkansas fights competition

By ROLLY HOYTKTHV-TV
The Associated Press

HOT SPRINGS, Ark.




HOT SPRINGS, Ark. (AP) — When it comes to free trade in Arkansas, it's usually Bentonville-based retail giant Walmart at the center of the discussion.
This time it's a slightly smaller company ready fight its way through a trade war. Alliance Rubber Co. of Hot Springs has gone to Washington in a battle over rubber band dumping.
The third generation president of the family-owned company says this is the first time they have had to raise a fuss, but Bonnie Spencer-Swayze says low-cost rubber bands made in three Asian countries have forced her hand.
"We want to go back to full employment," Spencer-Swayze says as she walks across the factory floor, addressing every employee by name. "We want to go from 176 people where we are today back to 250 great Arkansans."

                KTHV-TV reports that like most companies that make products in America, foreign competitors are tough. That's true for even the only maker of rubber bands left in the United States. Alliance has accounts with Walmart and Office Depot, but when Staples ended a deal to sell Alliance products, the company decided to cry foul.
"We have been up against some pretty stiff competition coming out of Thailand, China and Sri Lanka really for the last 20 years or so," said Jason Risner, the director of business strategy for Alliance. He testified before the International Trade Commission at a hearing Feb. 23. "(Those countries) have government subsidies that allow them to sell at a price to the United States that's cheaper than what they sell in their own countries."
Lawyers for Alliance presented numbers indicating exporters from China sold products here 27.27 percent below fair value. Exports from Sri Lanka came here for between 56.54 percent and 133.13 percent. Thailand rubber bands sold here with margins ranging from 28.92 to 78.36 percent.
American-based importers Frank Winne & Son, Inc. and Schermerhorn Bros. Co. pay those low prices. The foreign companies allegedly get subsidies from their governments to cover the loss. The ITC heard enough at the hearing to call for an investigation. The Department of Commerce will also look into the matter.
"We believe the numbers put forth by (Alliance) are fabrications used for the purpose of getting this process started," said Liz Levinson, an attorney with Fox Rothschild who represented Winne & Son at the hearing. "We think after our suppliers are investigated any dumping margins won't be found or will be much smaller than was put forth."
If the investigations find evidence of product dumping, the Commerce Department would impose duties to eliminate price margins. Levinson says that would be good for Alliance but would lead to hundreds of job losses across the wider industry.
Alliance has the largest American flag in Arkansas displayed along US-270 near its factory. Red, white and blue is everywhere in the plant and there is a garden dedicated to veterans on the property.
Spencer-Swayze and her family are clearly ready for a fight, but she insists this isn't about winning.
"We have the greatest workforce in America here in Hot Springs," she said. "They are salt of the earth, head-working people and we just want a level playing field."
___
Information from: KTHV-TV, http://www.kthv.com/


An AP Member Exchange shared by KTHV-TV.