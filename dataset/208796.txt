Fund buying and short-covering boosted Chicago Mercantile Exchange lean hogs, said traders.They said Friday morning's unexpected cash price retreat capped May hog gains.
May closed up 0.075 cent per pound to 66.975 cents. Most-actively traded June ended 1.175 cents higher at 74.000 cents.
Friday morning's average cash hog price in Iowa/Minnesota was at $54.92 per cwt, down 61 cents from Thursday, the USDA said.
Packers cut cash hog bids after shoring up inventories for Saturday's slaughter, a trader said. Earlier this week, some processors paid more for hogs, whose numbers start to decline this time of year, he said.
"Packers may again bid up for hogs next week if they can charge retailers more for pork," the trader said.
USDA quoted Friday morning's average wholesale pork price up 2 cents per cwt from Wednesday to $73.67.
Fund buying again moves CME live cattle to 1-year high
CME live cattle on Friday notched a one-year high for a third straight day, with strength from fund buying during the month's final trading session, traders said.
They said brisk wholesale beef demand and deferred-month futures' discounts to this week's cash prices helped drive contracts to fresh highs.
Funds were partly behind Friday's market gains, said Oak Investment Group President Joe Ocrant. But most of the buying was because of the large discount between June futures and cash, which appeared even greater after April expired, he said.
April live cattle, which expired at noon CDT (1700 GMT), closed 2.400 cents per pound higher at 138.000 cents and posted a new high of 138.900 cents. Most actively traded June ended 2.500 cents higher at 124.025 cents and marked a new high of 124.225 cents.
CME live cattle on Monday will revert to its 3.000-cent daily price limit after failing to settle up the 4.500-cent expanded limit on Friday.
Processors this week bought market-ready, or cash, cattle at $135 to $140 per cwt, up from $130 to $133 last week.
Friday morning's average wholesale beef price jumped $1.85 per cwt to $221.00 from Thursday. Select cuts climbed $1.24 to $207.92, the U.S. Department of Agriculture said.
Sharply higher live cattle futures pushed CME feeder cattle to new contract highs and a 7-1/2 month top.
May feeder cattle ended 2.975 cents per pound higher at 149.550 cents and made a new high of 149.850 cents. Feeder cattle will resume their 4.500-cent limit on Monday after not settling up Friday's 6.750-cents expanded limit.