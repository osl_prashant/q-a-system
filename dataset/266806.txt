A cool, wet planting season pushed Texas onions a few weeks later than usual, but grower-shippers aren’t bothered by the delay.
“We’re about two weeks late getting started,” said Don Ed Holmes, president of The Onion House LLC, Weslaco, Texas. “We normally get started the 20th or 25th of March, but we’re looking like the first week of April.”
Texas onions typically plant in mid-October, but didn’t get into the ground until late October and early November, he said in late March.
“That may actually work out well for us,” he said. “Mexico has probably another three weeks before we finish, and the storage onions will clear up.”
Tommy Wilkins, director of sales and business development for Grow Farms Texas, said the company’s onion harvest also is a little behind.
“Mexico seems late, and the market is a little soft, so we’re better off not coming in right now anyway,” he said. “Quality looks great on them right now. We’re anxious to start our local Texas onion program.”
Winter greens slogged through a wet and cold season this year, said Bret Erickson, director of business development for J&D Produce, Edinburg, including the Rio Grande Valley’s second snowfall in the past 100 years.
“We did suffer a little bit of frost damage here and there, but overall we got through it successfully,” he said.
“Cold, wet conditions slowed down harvest and slowed down our crop, but that’s the nature of the business, especially here in Texas where we can have some wild weather swings.”
Everything looks good looking ahead, Erickson said.
“Our onions and melons are looking exceptional, and if we can avoid any major weather events, we’re looking forward to great yields and quality for both of those programs.”
Wilkins said Grow Farms expects to start harvesting mini seedless watermelons the last week in April and seedless watermelons and honeydews the first week in May.
For shippers coming out of the Rio Grande Valley, trucking continues to be a challenge, said Dante Galeazzi, president and CEO of the Texas International Produce Association, Mission.
Galeazzi said in late March availability was tight again.
“Working in advance with growers will be critical to ensure forecasting needs and transportation is arranged ahead of time,” he said.
“Many more buyers are starting to look at Texas for more product, but more importantly ... they’re loading from Texas for a much wider period of than they traditionally did.”