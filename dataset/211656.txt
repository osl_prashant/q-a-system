Crystal Valley in growth mode
Miami-based Crystal Valley Foods’ May acquisition of Team Produce International Inc. has strengthened the company for the Argentina blueberry deal and in numerous other ways, said Richard Doyle, director of business development for retail, who moved to Crystal Valley in the deal.
 
“We’re going strong — more salespeople, more contacts,” he said. “It’s going to be an exceptional year, with Crystal Valley moving forward with the Argentina blueberry program.”
 
Through the acquisition, Crystal Valley now has expanded entry points in the U.S., Crystal Valley president Jay Rodriguez said in a news release.
 
“As a pioneer in asparagus and baby vegetable imports, we plan on continued growth by focusing on innovation and growing new and unique specialty products,” he said. 
 
“The acquisition of Team is just one step of many in order to expand our presence and better serve our customers.”
 
All of Team Produce’s employees, including sales and procurement, joined Crystal Valley and are based in its Miami facility.
 
 
Naturipe Farms promotes staff
Salinas, Calif.-based Naturipe Farms has promoted Brian Vertrees to director of business development-West.
 
In this new role, Vertrees focuses on providing additional leadership, guidance and direction for Naturipe’s West Coast blueberries and all organic berry programs.
 
“His vast industry knowledge and experience with berries makes him the perfect fit for this position,” Jim Roberts, Naturipe’s vice president of sale-East, said in a news release. “He is well-respected and a trusted individual in our industry.”
 
Vertrees joined Naturipe Farms in 2011, originally hired as an account manager and then promoted to senior account manager in April 2014. 
 
“Brian has shown a great passion for the berry business and has a proven track record of connecting our growers to our customers to maximize sales of high quality Naturipe berries, and in his new role this will allow him to expand this even further,” Roberts said.
 
 
Fresh Results LLC plants new varieties
Fort Lauderdale, Fla.-based Fresh Results LLC is planting its own proprietary blueberry varieties, along with other licensed varieties, in Argentina and elsewhere, said Eric Crawford, president.
 
“It’s about quality control both in-country and also in the U.S.,” he said. “It’s about the amount of time we can harvest and deliver to customer and make arrival to the U.S.” 
 
All of the new plantings are producing hardy and flavorful berries, which will benefit customers, Crawford said.
 
“Genetics is the key to all our futures in this industry. If you don’t have proprietary genetics, you won’t be able to compete in the next three to five years, and we have our own proprietary varieties that we’re planting in South America and North America and also in South Africa,” he said. 
 
“We’ve gone out of the lab and in the field, so we’re about a year away from having a little bit of fruit and two away from having good supplies.”
 
In Argentina, the varieties are being grown in the Tucaman and Concordia districts, primarily in Concordia, Crawford said.
 
Crawford said the company has not yet named its own new varieties.