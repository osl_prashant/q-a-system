Brothers Produce expects to diversify
Brent Erenwert, president of the Houston division of Brothers Produce, expects the company to continue to diversify in 2017.
Five years ago, about 95% of the company's business came from foodservice, Erenwert said. Now the breakdown is closer to 65% foodservice, 20% schools and 15% retail.
Erenwert aims to continue to grow the retail sector, with a goal of moving toward a 55-25-20 mix of foodservice, schools and retail.
"The way that I service these retailers, I go find the holes in their model," Erenwert said. "I'm not going to go replace a grower or somebody in their system, but I can go find where I might be able to squeeze in, and they see the value that I bring them, and then that brings us more into the fold with them. We've been very successful kind of pecking at those little holes in the models that show our value."
Another occupation for Erenwert has been increasing the presence of the company's brand. Brothers is currently rolling out new designs for its trucks, with each vehicle getting a unique look, from pomegranates to carrots to cherries to mandarins.
"Maybe somebody will see one of those trucks and it creates some new accounts," Erenwert said. "They've got like a 'wow' factor. Every truck's going to be different."
One area of growth for Brother Produce is its storage division, Houston Cold Storage. The 30,000 square feet currently allotted for that purpose is full, and Erenwert said the company is looking to expand to about 50,000 feet of cold storage this year.
Brothers Produce has locations in Houston, Dallas and Austin.
 
Touchstone and Associates streamlines
San Antonio-based Touchstone and Associates has streamlined and is basically a two-man operation now, owner Trey Touchstone said in an e-mail.
Robert Ingram, the company's primary onion and potato salesman, was promoted in the past year to vice president.
 
Willson Davis Co. offers apples again
San Antonio-based Willson Davis Co. has started offering apples again.
Johnston Williams, the son of owner Sid Williams, has been with the company about a year and has spearheaded the movement to get back into the category.
"We're bringing a lot of apples from Washington now, and also off the East Coast, Michigan, Missouri, and doing real well with it," Sid Williams said.
Johnston Williams said the company sells a lot of smaller red delicious apples for schools along with some granny smiths, golden delicious and galas. He said he hasn't moved many newer varieties yet, with the exception of a load of Smitten apples, which are marketed in North America by Wenatchee, Wash.-based Pegasus Premier Fruit.
Willson Davis Co. sells most of its product to wholesalers who specialize in foodservice.
 
W.G. Frazier & Son looks for steady growth
Mark Frazier, vice president of San Antonio-based W.G. Frazier & Son, said business is still growing.
"We're trying to expand, just like always, but we try to do it slowly and methodically, so that we don't lose any of the current customers we have, and we want to be able to maintain the level of service that we've always given," Frazier said.
Frazier noted the common concerns about competing against Mexico, where costs are lower, and against California companies that have bought up smaller Texas operations.
He also observed that transportation hurdles, including fuel prices and particularly newly required e-logs for trucks, made the year tough.
"That's been a big deal for this year," Frazier said. "It's been quite taxing on the truck companies this year for sure.
"I don't think it'll last very long," Frazier said. "I think they'll see at that point how terrible of a system that is, and the worst thing is we've got a driver shortage already, and once the receivers see how bad e-logs are, they're going to want teams on everything, and then we'll have even a larger driver shortage."
Frazier said he and others are hoping the system won't stick around.
"You can run less miles in a week and you've still got the same insurance and truck payments and fuel costs, for the most part, so it's just going to drive prices up and it's just going to kill productivity and it's going to make it worse service for more money," Frazier said.
 
Central Produce Co. supports local farmers
Mike Stupak, owner at Waco, Texas-based Central Produce Co., said economic conditions made this year difficult for the company.
"I'd say business is actually down," Stupak said. "From the week after Thanksgiving, the restaurants and the business in Waco ... it has been extremely quiet."
With schools starting back up, including Baylor University, however, Stupak expected business would pick back up.
Along with the wholesale part of the operation, Stupak runs a storefront from which individuals can buy product. At the store Stupak sells produce brought to him by area growers.
"During the spring time, I have a couple local farmers that will bring me in homegrown tomatoes," Stupak said. "The restaurants don't use them, but I sell it more inside the store ... I do like to support the local farmers in the area ... always have homegrown squash, yellow and zucchini, and tomatoes and cucumbers."
Central Produce Co. is still a small family business, Stupak said, and service is a point of pride.
"It's knowing your customer, like if they're real old and they really have a hard time getting out of the car, I'll go outside and see what they want and go get it and bring it out to them," Stupak said. "It's just how you take care of your customers."