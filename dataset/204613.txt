European politicians advised on Wednesday that the herbicide glyphosate should only be approved for another seven years, rather than the 15 proposed by the EU executive, and should not be used by the general public.Environmental campaigners have demanded a ban on glyphosate, which is used in products such as Monsanto's Roundup, on the grounds it can cause cancer, though EU and U.N. scientists disagree on whether there is a link.
The European Commission has proposed glyphosate be approved for 15 years when an existing license expires at the end of June.
Wednesday's European Parliament motion supported renewal for seven years and urged a ban on non-professional use, as well as in and around public parks and playgrounds.
Angelique Delahaye, a French member of the European People's Party, the main center-right group in the parliament, said many people were concerned but farmers needed glyphosate.
"The agricultural sector depends highly on it and it is absolutely necessary to find solutions to replace it before totally forbidding it," she said.
Wednesday's motion is not binding, but can influence member states so far undecided on whether to approve glyphosate's use.
Member states were initially expected to extend approval in March, but EU sources, speaking on condition of anonymity, said there was not enough support to reach a majority decision so the vote was deferred.
The European Food Safety Authority, which advises EU policymakers, issued an opinion in November that glyphosate was unlikely to cause cancer.
But the World Health Organization's International Agency for Research on Cancer (IARC) has classified glyphosate as "probably carcinogenic to humans."
A closed-door EU committee on pesticides is due to meet next on May 18-19, though a different committee could debate the issue earlier.
Among big EU member states, France has voiced opposition to glyphosate, while Britain and Germany are said to back its use.
The Green Party, which wants a ban, said European governments and the European Commission could not ignore the concerns raised by the European parliament.
"This is a shot across the bow of the Commission and it must now work with EU governments to address these concerns," Green food safety and public health spokesman Bart Staes said.
The Glyphosate Task Force, set up to represent the industry, said in an emailed statement that glyphosate was "a key tool for the control of weeds and the protection of crop yields."
"An informed debate cannot be achieved through scare-mongering or the promotion of misinformation and unsubstantiated claims," it said.