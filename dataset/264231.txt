Report: More than half of Kansas winter wheat in poor shape
Report: More than half of Kansas winter wheat in poor shape

The Associated Press

WICHITA, Kan.




WICHITA, Kan. (AP) — The latest government crop report estimates that more than half of the winter wheat crop in Kansas is in poor or very poor condition.
The National Agricultural Statistics Service reported Monday that 17 percent of the Kansas wheat is in very poor condition with another 38 percent is rated as poor. About 34 percent is rated as fair with just 10 percent in good and 1 percent in excellent condition.
That assessment comes at the same time that topsoil moisture supplies were rated as short or very short across 81 percent of the state.
Their report covers crop conditions for the week ending on Sunday.