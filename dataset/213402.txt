It’s been said over and over again that food trends start with foodservice — we’re the creative ones. We have the culinary creatives on our side and our ears to the ground picking up signals from the consumer and sharing innovative ideas with the rest of the supply chain, right? 
Remember the veg-centric trend that gives vegetables a starring role in restaurants by cooking them in unique ways from braising to barbecuing to roasting to other techniques we often see applied to proteins? 
The idea spawned out of a think tank that the Produce Marketing Association and the National Restaurant Association organized in 2010. And while foodservice is doing its part to make eating fruits and vegetables en vogue, the majority of produce is consumed at home.
In fact, according to the U.S. Department of Agriculture’s Economic Research Service, only 14% of fruit consumption and 32% percent of vegetable consumption occurs away from home.
More companies are getting in the game of making eating produce at home easy, convenient and tasty. It’s driving innovation in everything from value-added produce to frozen offerings. 
In fact, IRI data suggests that traditional retailers are showing growth in 19 of the top 25 vegetable segments. “And it’s difficult to track penetration or growth of vegetables and fruits through new channels like deli, convenience and home delivery,” said Jamie Strachan, CEO of Growers Express, the Salinas company that goes to market under the Green Giant Fresh brand. 

With all this excitement happening, we must be making a difference in sparking produce consumption, right? 

Growers Express’ Cauliflower Crumbles are in high demand, and Strachan is following up with a new line of vegetable noodle dishes. 
Plant-based protein companies, like Beyond Meat, are also rapidly sparking innovation. Their featured product, Beyond Burger, is said to satisfy like a beef burger, with 20 grams of protein. 
And if retail and foodservice were not enough options for consumers, an entirely new type of channel has emerged in recent years: meal kits, which deliver all the ingredients needed for recipes to be cooked at home. 
Blue Apron, for example, delivers 8 million meals per month to home chefs across the U.S. In the short term, the market is predicted to hit $4 billion — though traditional retail will not stand still and watch sales erode. 
With all this excitement happening, we must be making a difference in sparking produce consumption, right? 
Unfortunately, produce consumption — despite the innovation and a plethora of new ways to eat your greens — is relatively flat.
 
Produce grower-shippers would be wise to stay up to date on trends in retail and foodservice. To my foodservice colleagues, if we are to remain in the driver’s seat of trends, we better keep one eye on our retail counterparts and emerging players. They are doing some good work. 
Tim York is CEO of Salinas, Calif.-based Markon Cooperative. Centerplate is a monthly column on “what’s now and next” for foodservice and the implications for produce. E-mail him at timy@markon.com.
What's your take? Leave a comment and tell us your opinion.