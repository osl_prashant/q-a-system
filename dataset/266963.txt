Uncertain times for Oregon wheat industry
Uncertain times for Oregon wheat industry

By GEORGE PLAVENCapital Press
The Associated Press

SALEM, Ore.




SALEM, Ore. (AP) — The beginning of spring ushers a flurry of activity on the vast, rolling wheat fields of Umatilla and Morrow counties in northeast Oregon.
Farmers drive large sprayer rigs over still-green plants to control weeds and pests, while crossing their fingers for make-or-break rainstorms that can turn otherwise average yields into a bumper crop.
This year, however, a new layer of uncertainty has emerged for the Northwest wheat industry. Since the U.S. pulled out of the Trans-Pacific Partnership, or TPP, growers are worried about maintaining strong relationships with longtime foreign customers in countries such as Japan, which signed on to a revised version of the trade deal March 8 in Santiago, Chile, along with 10 other nations.
The vast majority of Oregon wheat — between 85 and 90 percent — is exported, with 21 percent of export sales to Japan. That amounts to $60 million, at current prices from Portland grain terminals.
While the price of soft white wheat has rebounded modestly from its sub-$5 per bushel low last year, Japanese flour mills estimate higher tariffs on American wheat could negatively impact market share by more than half, from 3 million metric tons to less than 1.4 million metric tons. And once that market share is gone, it can be difficult to recapture.
Matt Wood, who farms several thousand acres of dryland wheat and cattle pasture near the small town of Helix, said farmers are wary of the unpredictability. But the economic consequences extend even further.
"The community you draw business support is ever shrinking," Wood said. "That's a real concern."
Ripple effect
What has happened in Helix — population 181 — is what continues to happen all over rural America, Wood said.
Wood took over the lease on his family's farm in 1993. Since then, he said the town has lost its grocery store, hardware store and the post office was forced to cut back hours. The Helix Market & Pub nearly suffered the same fate until Wood and four other guys bought the place in 2006 just to keep it open — it was cheaper than paying their tabs, he quipped.
The Watering Hole Consortium, as they called themselves, has since passed the pub along to Anna Doherty, who has kept it running.
"The concern we all had was if that place closes, it ain't opening again," Wood said.
Helix has historically depended on agriculture, yet Wood estimates that half his neighbors are no longer farming. The return on investment is no better than 2 percent, he figures. So, instead of having 10 guys farming 2,000 acres, he said two guys are now farming 10,000 acres.
That observation is backed up by data from the USDA. A recent report from the agency's Economic Research Service shows that farm production has been trending toward consolidation over the last three decades. By 2012, 36 percent of all cropland was on farms with at least 2,000 acres, up from 15 percent in 1987.
Consolidation means fewer people to invest in the community, Wood said.
"It's just been a shift in the economy," he said.
Eric Orem, a wheat farmer north of Lexington, has seen something similar. Orem serves on the board of directors for Morrow County Grain Growers, the local farmers' cooperative. Between drought-stunted yields and increasing trade uncertainty, he said the co-op is experiencing some lean years in product and equipment sales.
In turn, the co-op —while still profitable — has less money to support community-based organizations like FFA or Little League Baseball.
"It's a ripple effect, for sure," Orem said. "It's definitely a reflection of what's happening on the farms."
Exports crucial
Exports remain crucial to wheat growers to turn a profit. Umatilla and Morrow counties rank first and second, respectively, in statewide production as of the most recent 2012 Census of Agriculture. Combined, they total roughly 395,000 wheat acres.
With the U.S. out of the latest TPP agreement, state and national wheat industry groups sent a letter to Trade Representative Robert Lighthizer, urging President Donald Trump to reconsider.
"The President has promised to negotiate great new deals," the letter reads. "American agriculture now counts on that promise and American wheat farmers — facing a calamity they would be hard-pressed to overcome — now depend on it."
Wood said the last thing the wheat industry needs is to jeopardize any kind of international trade. Orem said that, while he does not believe U.S. wheat will ever truly lose the Japanese market, the latest developments are "troubling," and "disheartening."
Blake Rowe, CEO of the Oregon Wheat Growers League and Oregon Wheat Commission, echoed the farmers' sentiments. In addition to TPP, Rowe said the organization is keeping a close eye on NAFTA negotiations, and possible retaliation against U.S. agriculture based on new steel and aluminum import tariffs.
"It's not a good time," Rowe said. "I think folks are being cautious, not knowing where this is going to end up."
Dryland wheat is the still the major agricultural crop for many communities in the dry, arid climate of Eastern Oregon. Without access to irrigation water, there are not many viable alternatives.
"I'm not really sure how communities will absorb those hits," Rowe said.
Bruce Sorte, community economist for Oregon State University Extension in Eastern Oregon, said wheat isn't the only commodity that may be affected. Fruit also depends on exports, along with processed potatoes and onions.
"Those exports are just critical to the folks out there," Sorte said. "How these things play out, you never can tell."
Job diversification
Steve Chrisman, economic development and airport manager for the city of Pendleton, said the local job base has grown more diverse over the last couple of decades, which has helped insulate the community against impacts to a single business — namely agriculture.
"If wheat's not going to do well, it's not good for the city, but it doesn't cripple it, either," Chrisman said.
Chrisman pointed to Wildhorse Resort & Casino, Keystone RV and Interpath Laboratory as examples of large employers outside the farm sector. Another recent development that has him excited is the development of the Pendleton Unmanned Aerial Systems Range, which he said is beginning to add full-time permanent jobs.
Companies and defense contractors have flocked to the range, which has been the site of several high-profile drone launches including the Project Vahana air taxi and ArcticShark, which will be used to gather sophisticated climate data in the Arctic atmosphere over Alaska.
The range has brought anywhere from 20 to 50 people into Pendleton at all times during the last six months, Chrisman said, eating out in local restaurants and staying in local hotels. And Chrisman believes this is only the tip of the iceberg.
"The potential for growth here is pretty exciting," he said. "I think we've made huge strides in the last two years."
Pat Beard, who manages the Pendleton Convention Center, added summer tourism as another economic driver for the city, with Pendleton Bike Week, Pendleton Whisky Music Fest and, of course, the world-famous Pendleton Round-Up.
Beard said he knows, living in a very agriculture-oriented area, the price of commodities will always affect the community. But as the economy diversifies, it is not as devastating a blow as it would have been 50 years ago.
"While it's challenging times, it's part of the lifestyle," Beard said.
That lifestyle is what keeps farmers like Wood and Orem working through the tough times.
"It's what we do. It's what we know," Wood said.
Orem said he is still optimistic for the future, and believes the battle-tested wheat economy will ultimately prevail.
"We've been through tough times before," Orem said. "We'll figure these trade deals out. It's a bump in the road right now."
___
Information from: Capital Press, http://www.capitalpress.com/washington