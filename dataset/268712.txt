House approves letting farmers carry loaded rifles in fields
House approves letting farmers carry loaded rifles in fields

By HOLLY RAMERAssociated Press
The Associated Press

CONCORD, N.H.




CONCORD, N.H. (AP) — Farmers would be allowed to carry loaded rifles across their fields under a bill passed Thursday by the New Hampshire House.
The bill seeks to modify a current law prohibiting the carrying of loaded rifles or shotguns in motor vehicles, snowmobiles or aircraft whether stationary or moving, and aboard boats in motion. The bill passed by the House would limit the prohibition to vehicles in motion and would create an exemption for farmers protecting their crops and livestock.
The bill was opposed by the state Fish and Game Department and police, and opponents argued it was confusing and needed further study.
"In committee we heard many things. One of the things I heard was that if you're a farmer, and you see a varmint, you don't have enough time to slow your vehicle down, stop it and load your gun because that would make a noise, and then it would be gone. I was sort of worried about that a little bit," said Rep. Jonathan Manley, D-Bennington.
Manley said he was concerned that the bill would apply to both farmers and their "agents," and said he disagreed with allowing rifles to be fired from aircraft on the ground.
"This is something we have for many years said was an unsafe thing to do, and we've not passed many bills that sought to do the same kinds of things," he said.
Supporters said it had been well vetted and addressed all concerns brought forward by lawmakers.
Rep. James Spillane, R-Deerfield, said the bill was necessary to allow farmers  "to take care of predators, varmints, things that critically decimate their crops or their livestock." And he clarified that "agents" would mean employees of a farm, not "anyone on the street."
The bill now goes back to the Senate, which had passed a different version.