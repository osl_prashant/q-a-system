The right or wrong seed choice sets your yield and profit potential from the get-go. While it might seem like old hat, remember the basic nuances and more technical deails before locking in your seed choice.
First, match field’s characteristics with hybrid or variety characteristics. After finding a few seed options that fit the field, find what works for your price point.
“In my opinion there are three key focus areas: first crop rotation, second cultural practices and third soil type and drainage, when picking seed for each field,” says Scott Stein, Monsanto U.S./Canada corn portfolio lead.
Also take into account maturity needs, irrigation, fertilizer programs, field history of insect and disease, harvest timing and yield expectations for that field.
Each hybrid or variety will have areas where it excels—maybe you have a well-drained field with adequate fertility levels that could use a racehorse hybrid that sacrifices some defensive characteristics to pack on bushels. Maybe a field has wet spots and a history of Northern Corn Leaf Blight—in that case you’ll want to look for something with a better defensive package. 
Start with this checklist to focus seed choices on the options that best fit the agronomic needs of your fields:

Narrow down maturity. Corn needs to reach black layer a minimum of two weeks before a killing frost.
Establish a yield goal. Use historical yield data to determine what each field can produce and select seed with that potential.
Plan your defense. Don’t neglect to consider disease and insect history for each field and select defensive traits accordingly.
Diversify your lineup. Compare seed information to make sure characteristics don’t match too closely and check hybrid and variety numbers to ensure you don’t unknowingly plant the same thing twice. See page 62 for more details.

Compare hybrid and variety characteristics to your production practices. “Taking factors such as soil, climate and management into account might add another 15 bu. or 20 bu. per acre, compared with picking hybrids based on general plot performance,” says Farm Journal Field Agronomist Ken Ferrie.
Make it right by your checkbook, too. While it might not be your first choice, buying early and in bulk could lead to significant savings.
“Right now the majority of seed companies offer an early pay cash discount—it’s highest early in the season and decreases the closer you get to spring,” says Colin Steen, head of Golden Harvest of the west commercial unit. “Ask about financing options, as you can often finance seed early and, without spending any money out of pocket, still take advantage of a discount.”
Early cash discounts typically start around 10% or a little higher and decrease each month as spring nears. One of the reasons companies offer early cash discounts is because that allows them to get a better idea of what hybrids and varieties are in high demand and sort out logistics.
Seed companies often offer volume discounts, too, rewarding customers who buy more. The level of discount varies by company, so ask your seed rep for more information and try to negotiate better prices.
“Keep in mind seed availability,” adds Brent Stauffacher, Mycogen Seeds corn marketing leader. “The earlier you make a decision the more likely you’ll get the seed you want.”
This year seed companies say supplies are in good shape, but remember the most popular corn and soybean products aren’t unlimited so you should lock in those choices early.
Carefully think through your seed decision again this year. Spend money on fields with high yield potential, and consider saving dollars on fields with less output. Your seed choices set the stage for next season—make it a good one.