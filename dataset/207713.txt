Just in time to celebrate California’s garlic harvest, McDonald’s restaurants in the Bay Area are offering Gilroy Garlic Fries.
 
To promote the menu item, Greater Bay Area McDonald’s hosted a tour of Christopher Ranch, supplier of the garlic for Gilroy Garlic Fries, according to a news release.
 
The tour group included food bloggers, owner-operators and some corporate McDonald’s staff, the release said.
 
The tour featured a visit to a Christopher Ranch garlic field near Gilroy, where Christopher Ranch president Bill Christopher talked about growing and harvesting fresh garlic.
 
The tour then headed to Christopher Ranch headquarters to see grading, peeling and packing facilities.
 
Ken Christopher, third generation grower and vice president, headed up the tour, while founder Don Christopher was on hand to answer questions about the company, garlic and the Gilroy Garlic Festival, which he co-founded in 1979, according to the release.
 
At the end of the tour, the group headed to a Gilroy McDonald’s for a demonstration of how Gilroy Garlic Fires are prepared, followed with a tasting.