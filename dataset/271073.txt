Idaho and New York are both known for its dairies, and both states are seeing their share of successes and challenges these days.“We’re in a fun competition between New York and Idaho. We’ll see how it goes over the next couple of years,” says Tony VanderHulst, president of the Idaho Dairymen’s Association.
Idaho Dairy Growth
In Idaho, production and growth that began years ago. According to the Idaho Dairymen’s Association, as of March 2016 there were more than 500 dairies with nearly 579,000 milk cows within the state. The number of dairies have decreased or consolidated during the past ten years, but cow numbers have risen.
“You’ll see some internal growth. When it comes to new dairies coming in, though, it won’t be as many as people thought,” says VanderHulst.
That’s internal growth as of late. Nearly 100 of those dairies have 2,000 cows. “Production has doubled since 2000,” VanderHulst adds.
That includes nearly two dozen processors. “Chobani came into town a few years back," he says. "That’s helped us."
Consolidation in New York
New York is also up there. According to USDA,  the state had roughly 620,000 milk cows as of January 2016.
The Empire State's dairy industry is also seeing consolidation.  “Farms have grown in size. Smaller farmers have left the industry because the next generation isn’t coming home. Those farms have been acquired,” says Tonya Van Slyke, executive director of the Northeast Dairy Producers Association.
“In general, I would say the number of family farms has decreased. Cow numbers have probably stayed about the same and average herd size has gone up a little bit,” says Kendra Lamb, a dairy farmer in Oakfield, N.Y.
That’s putting a flush of milk on the market, pushing the limits for processing. “We’ve become really efficient in milk production, and our farms are doing a great job," Van Slyke says. However, "we don’t always have a home for our milk. That’s an issue."
Yogurt Boom
The yogurt industry became a home for much of that milk during the boom years in both states. Chobani alone brought hundreds of jobs to New York when the company bought an idled cheese plant in 2005 to start producing Greek yogurt.
“The yogurt boom here the last decade has been great for soaking up our milk. Of course we have a problem with some of them moving their production out of state," says John Mueller, a dairy farmer in Clifton Springs, N.Y. "Now we have too much milk in New York,” 
Mueller is referring to Chobani’s production sites and expansion in Idaho. One facility in Twin Falls is now the largest in the world, and it’s going to get even larger. “It helped us out tremendously. It’s a new home for milk,” says Idaho Dairymen's Association's VanderHulst.
Chobani has brought jobs and growth to the dairy industry within both states. Dairies say it’s helping the industry, but still, the yogurt boom in New York isn’t quite what it used to be. “When this yogurt boom went on, they said how they needed 225,000 more cows," says Jeff Mulligan, a dairy farmer in Avon, N.Y. "Now we don't have any more cows, and we’re making more milk than we need."
Still, dairy producers hope the growth will continue with success, profits and prices.
“We have the climate. We have the water. We have a lot of things going for us here, but it’s a matter of making the best of it,” says Mulligan.
The yogurt companies say they’re working on new and different products to innovate Greek yogurt and help create new demand for their dairy products. 
 

<!--
LimelightPlayerUtil.initEmbed('limelight_player_769774');
//-->

Want more video news? Watch it on MyFarmTV.