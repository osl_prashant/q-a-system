Idaho Falls, Idaho-based Potandon Produce LLC has updated its Green Giant Vidalia onion carton. 
The redesign comes a season after Potandon updated its consumer bag, according to a news release.
 
Supply is seasonally increasing from Vidalia.
 
Through May, Potandon will ship onions from the field, according to the release, and ship from storage starting June 1. The crop profile is running 30% mediums and 70% jumbos, according to the release.
 
“We’re seeing yields increased over last season in the 15% to 20% range and there are plenty of onions available from the Vidalia region,” Ralph Schwartz, vice president of sales for Potandon, said in the release. “The increased yields are the direct result of an exceptionally mild growing season, with no major weather events.”
 
Ray Farms, Glennville, Ga., supplies a majority of the Vidalia onions to Potandon.