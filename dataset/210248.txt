Honeybear Brands is expanding distribution of its proprietary Pazazz apple, and the sweet-tart variety will have a coming out party at the 2017 Produce Marketing Association’s Fresh Summit show.
The variety is in its fourth year of commercial release and production is beginning to hit big volumes, said Don Roper, vice president of sales and marketing with Elgin, Minn.-based Wescott Orchard & Agri Products Inc. and its Honeybear Brands marketing arm.

“We will be fully nationwide with the product,” he said.
Availability is expected from November through April. The apple has been received with great success by select retailers the last three years.
“It is exciting to that now we have a lot of volume to push it out even further,” he said.
With packing expecting to begin at the end of October, volume is expected to almost triple compared with last year, he said.
Fresh volume is expected close to 250,000 cartons this year, with long-term potential targeted at about 2 million cartons. 
Roper said that there four growing regions for Pazazz — Minnesota/Wisconsin, New York, Nova Scotia and Washington state.
With Honeycrisp as one of its parents, the Pazazz variety has a crunchy texture, Roper said, possessing an intense burst of both sweet and tart.
“It’s a brand new flavor spectrum — it is a wow flavor spectrum when you bite into it,” he said.
The variety features good availability for local demand for each of the growing regions and strong volume out of Washington to cover retail needs across the country.
The Pazazz apple is marketed by Honeybear Brands, with growing packing partners including Sun Orchard Apples in New York and Van Meekeren Farms in Nova Scotia. Honeybear Brands grows and pack the apple in Minnesota, Wisconsin and in Washington, Roper said. 
Besides sampling the Pazazz variety at Fresh Summit, Honeybear Brands also will bring eight to twelve new varieties currently under development to the show for sampling. “You will get a sneak peek of some of the new varieties we are working on into the future.”