WASHINGTON, D.C. — Brushing off protesters who briefly interrupted his remarks, Sen. Jeff Flake, R-Ariz., called for bipartisan immigration legislation, expanded trade agreements and tax reform in a Sept. 19 general session speech at the United Fresh Produce Association Washington Conference.Tom Stenzel, president and CEO of the United Fresh Produce Association, said later that two protesters had registered for the event using a fictitious produce company name. They were escorted from the room after one of them stood and shouted at Flake “Why are you a climate change denier, sir?”
After the disruption, Flake was unfazed and smiling. He continued his recollections of his childhood in Snowflake, Arizona, and his experience working on his family’s farm and ranch alongside migrant workers in the 1960s.
One Mexican migrant was Manuel Chaidez, who was 16 when he started at the Flake family ranch. He worked for the family for 24 years, eventually marrying and raising six children in Arizona.
“My father would often hire high school buddies of mine to help for a bit and they would last for a day, but Manuel stayed,” he said.
Flake said Manuel and workers like him must be accounted for in immigration policy.
“We must always remember that we need those whose only credential is a strong back and a willingness to work, and realize the American dream,” he said.
Flake said he has never been able to look at a group of people like Manuel Chaidez and others and see a “criminal class.”
“We have got to find a way to humanely and rationally deal with those who came here, particularly those who were brought here as children,” he said.
Congress must address the Deferred Action for Childhood Arrivals program by March or it will expire by order of President Trump. The program allows hundreds of thousands of immigrants brought to America as children to live and work openly.
Flake expressed hope that a fix of the “dreamer” issue could be packaged with other badly needed immigration reforms, including a redo of guest worker programs.
“I have often said that I’ll take immigration reform comprehensive, I’ll take it piece-meal, I’ll take it however we can get it,” he said.
Speaking about the North American Free Agreement, Flake said the agreement was positive for the U.S., Canada and Mexico now and hopefully will be improved after current negotiations to modernize the deal.
He called for expansion of free trade agreements.
“We have got to be actively negotiating bilateral and multilateral trade agreements,” he said. “I’m very concerned about the TPP (Trans Pacific Partnership); I think it was a mistake for us to withdraw that will haunt us for decades.”
Tax reform needs to be addressed by the end of the year, Flake said, with efforts expected to lower the corporate tax rate and make taxes simpler and fairer.
The debt and deficit spending is a serious worry, Flake said, with $600 billion added annually to the nation’s debt. That will eventually cause interest rates to go up and debt service costs will explode.
Just as with immigration, solving the debt problem will require an effective bipartisan effort.
“We have to sit down together and share political risk,” Flake said.
The inability for Republicans and Democrats to work together is a big concern, he said.
“We have got to come to a point where you can look across the aisle and see people with different political views and not see the enemy.”