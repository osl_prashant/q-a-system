Western Growers is offering its members a four-part web series that summarizes the Center for Produce Safety’s Research Symposium in June.
 
At the two-day symposium, more than 30 research projects were presented. Western Growers has organized the conference sessions and food safety findings into four recap web seminars, according to a news release.
 
Hank Giclas, senior vice president of strategic planning, science and technology at Western Growers, and Susan Leaman, vice president at iDecisionSciences, helped create the series.
Registration is available online.