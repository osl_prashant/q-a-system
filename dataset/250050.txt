Grain lower and livestock mixed
Grain lower and livestock mixed

The Associated Press



Wheat for March lost 9 cents at 4.93 a bushel; March corn was off .50 cent at 3.7925 a bushel; March oats fell 2.50 cents at $2.5950 a bushel; while March soybeans declined 9.50 cents at $10.55 a bushel.
Beef was mixed and pork was lower the Chicago Mercantile Exchange April live cattle was up .15 cent at $1.2297 a pound; March feeder cattle fell .53 cent at $1.4377 a pound; while April lean hogs lost .40 cent at $.6780 a pound.