Wilson Produce has completed the three-year transition to organic production on its 150-acre farm in Sinaloa, Mexico.
“Our use of cover crops, companion crops and compost have all enhanced the quality of our soil, the quality of our products, and have made this challenging transition possible,” James Martin, director of sustainability, said in a news release. “An important milestone in our sustainability strategy has been achieved today.”
The Nogales, Ariz.-based company noted that it began moving away from conventional growing practices about seven years ago.
“We are ecstatic,” co-CEO Alicia Martin said in the release. “The whole team from production to sales has come together to make this possible.”
The farm received its certification in December, and the company will have another growing region certified organic this summer.
Wilson Produce has 120 acres in Baja California, Mexico, that will complete the three-year transition in June.