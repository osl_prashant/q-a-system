Promising a new era in consumer awareness of the U.S. Department of Agriculture organic seal, backers of the proposed organic research and promotion order say it could raise more than $30 million a year for research, technical services, consumer education and promotion of the organic brand. 
 
Consumer education and promotion efforts under the proposed order likely focus on making sure shoppers know what the green and white USDA organic label represents, said Laura Batcha, CEO and executive director of the OTA.
 
She said USDA guidelines allow for comparison but not disparagement with conventional product.
 
"The major emphasis is about delivering the message about the attributes that are embedded in that seal," she said.
 
Education and promotion efforts will focus on what suppliers must do to earn the seal, she said.
 
Features of the proposed program:
Organic producers and handlers with gross sales in excess of $250,000 for the previous marketing year of certified organic agricultural commodities would pay an assessment of one-tenth of 1% of net organic sales, according to a summary of the proposal. 
The program's board would be made up of 50% producers and 50% handlers;
Producers covered by another federal marketing order - for example, avocadoes - could decide whether to support the organic order or the Hass Avocado Board.
It would treat all organic products like a commodity instead of a singular product or category like dairy, tomatoes or grain; 
Producers will select regional representatives through direct balloting;
Everyone assessed will have a direct vote - there is no bloc voting;
Assessments would be made throughout the value chain: producers, handlers, processors, retailers;
Growers and handlers with gross organic revenue below $250,000 are exempt; 
Half to 75% of the funds would go to research;
One-quarter of the assessment from producers would be required to be used for local and regional research; 
All of the research, inventions and innovations would remain in the public domain; and
A referendum is required every seven years to decide whether to continue the program.