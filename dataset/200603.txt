Interesting news from the supermarket deli: Publix has eliminated the longstanding practice of offering courtesy slices of deli meat for customers placing an order at the store's service counter.Like many other grocery chains, customers at Publix could get a slice of deli meat "on the house," while the employee in the deli runs the slicer to test the thickness of the meat.
For now, the new policy only applies to stores in the central part of Florida west of Polk County, according to the Orlando Sentinel.
For now, it's a test, but for folks who don't live in the Sunshine State, this is a move that could eventually affect a whole lot of people. As noted in the article, Publix operates almost 400 stores in Central Florida, the area from Daytona to Orlando to Tampa, and controls 43% of grocery sales in that sector, as reported by industry tracker Shelby Publishing.
In that region, Publix's total store count exceeds the number of Wal-Mart, Winn-Dixie and SuperTarget stores combined.
My immediate reaction to the announcement was the same one I'm certain most people will have upon being told by a store employee, "Sorry, we don't offer samples anymore."
What a cheapskate move.
The Real ReasonsBut that's not the corporate spin. As reported in the Sentinel, Publix spokesman Dwaine Stevens said there's been "some confusion" about the new policy.
Stevens said that Publix stopped offering free meat slices because stores are now offering a "cheese of the week" sample instead. He said that deli employees will no longer offer a piece of meat automatically, but customers will be still be given a slice if they request it.
"We are piloting a change in a few dozen delis in central and southwest Florida to create a more natural exchange between our deli clerks and our customers," he said.
Sure Dwaine. This has nothing to do with giving away what amounts to tons of product annually. It's all about creating "natural exchanges," whatever those are.
However, another Publix spokesman, Bryan West, took the issue head-on, insisting that the no-more-freebies policy was not a cost-cutting move.
To even reference the phrase "cost-cutting move" gives the game away. It is all about the costs.
Not convinced? Consider another initiative underway at Publix headquarters.
As reported in several media outlets, Publix is running a PR campaign to pressure the Florida legislature to allow liquor sales in supermarkets. The rules currently restrict hard liquor sales to "liquor stores."
What an outrage.
But it's not about profits. Oh, no. According to Publix's public statements, it's all about people, not profits.
"This year comes a decision on whether to let grocery stores, drugstores and big box stores ‚Äî like Wal-Mart and Target ‚Äî sell liquor alongside beer and wine," Publix's news release stated. "The change is long overdue. It makes no sense that consumers can buy beer and wine in one store, but must walk outside and into a separate store to buy distilled spirits."
Heck, yeah! Walk outside to another store for the shopping cart full of booze we need to buy along with our deli meat? It's un-American!
Especially in Florida, where that sunshine can be absolutely brutal.
For purposes of comparison, that same argument was used in Washington state a few years ago to convince voters to repeal a state law that limited liquor sale to state-run stores.
We're not interested in profits, the coalition of superstores and supermarkets funding Initiative 1183 proclaimed. This is about consumer choice. About freedom and liberty!
The Yes On Initiative 1183 proponents hammered away at the notion that once the greedy, profit-obsessed State Liquor Control Board got out of the business of selling alcohol, free market competition would drive down prices to the point that winos on street corners could start brown bagging bottles of Crown Royale, instead of Thunderbird.
Guess what? The initiative passed. And prices went up, not down, as the big retailers toasted their windfall with, well, something other than Mad Dog 20/20.
When companies stop giving things away, it's all about cost-cutting. And when they urge legislators to bend the rules to support "consumer choice," it's not about choice ‚Äî it's about profits.
And you can take that to the bank.
The opinions expressed in this commentary are those of Dan Murphy, a veteran journalist and columnist.