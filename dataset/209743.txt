Crop calls
Corn: Fractionally to 1 cent lower
Soybeans: 1 to 3 cents lower
Wheat: 1 to 2 cents higher

Corn and soybean futures faced light followthrough weakness following yesterday's losses as traders anticipate ample supplies to weaken basis levels. However, harvest is running behind the five-year averages and rains in the forecast will cause some delays. Traders also reacted to news that INTL FCStone has raised its corn and soybean yield pegs to 169.2 bu. per acre and 49.9 bu. per acre, respectively. Meanwhile, wheat futures benefited from light short-covering overnight. But buying in wheat was limited by a firmer tone in the U.S. dollar index.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Following yesterday's low-range close, cattle are vulnerable to followthrough pressure, but a more cautious tone is expected as traders wait on cash signals. This week's showlist is larger in Nebraska and Texas, but about steady in Colorado and Kansas compared to last week and the beef market got off to a lackluster start. Meanwhile, hog futures are called mixed on a combination of profit-taking and followthrough buying following yesterday's sharp gains. This week's hog slaughter is estimated to be close to last week's 2.534-million-head level as packers are enjoying highly profitable margins.