Fresh Link Produce adds staff
Fresh Link Produce LLC has expanded its sales staff.
Shannon Aliotti joined the Lake Park, Ga.-based grower-shipper in February, and Steven Johnson, sales manager, returned to the company in April.
Aliotti is a saleswoman.
She previously worked in sales in the tomato division at Ippolito International LP, Salinas, Calif., for three years. She also has worked at Pro*Act, Monterey, Calif., and Taylor Farms of California, Salinas.
Johnson was in sales at Lake Park's South Georgia Produce Inc., for 16 years.
"They will help expand our sales quite a bit," said Steve Sterling, Fresh Link's general manager.
Fresh Link sells bell peppers, squash, cucumbers, eggplant, green beans and carrots.
 
J&J partners with Georgia vegetable grower
J&J Family of Farms Inc. expects to increase its supply of vegetables after forming a partnership with a south Georgia grower-shipper.
Manor, Ga.-based Moore Farms grows and ships bell peppers, eggplant, cucumbers, specialty peppers and hard squash to customers throughout the East Coast.
The relationship increases Moore Farms' production and Loxahatchee, Fla.-based J&J's sales and marketing capabilities, according to a news release.
As part of the transaction, Moore Farms will become a shareholder in J&J Holdings, producing an alignment and partnership in all aspects of J&J's business.
 
Ken Corbett Farms boosts chili acreage
Ken Corbett Farms LLC has increased its fall specialty pepper acreage.
Last spring, the Lake Park, Ga.-based grower-shipper began growing specialty peppers.
Though Eric Bolesta, sales manager, declined to state acreage, this fall the company plans to increase acreage by 10%.
"We have grown some in the past, but we're growing more acres and a few more varieties," he said. "We have had great success with them and we are receiving good demand."
Ken Corbett also grows bell peppers, cucumbers, squash and eggplant.
 
Generation Farms adds sorting equipment
Generation Farms has installed color and laser sorting machinery on its green bean line.
The Lake Park, Ga.-based grower-shipper added the machinery to help maintain quality, particularly during the fall, when the vegetable is hard to grow, said Jamie Brannen, general manager of produce.
The Tomra equipment features a color sorter with laser technology, he said.
"It can actually see inside the beans, which is something you can't do manually," Brannen said. "We are hoping it gives us a better pack of beans that will hold up better."
Generation Farms uses a similar sorter on its carrots.
Generation Farms installed the machinery in March.
 
South Georgia Produce increases acreage
South Georgia Produce Inc., Lake Park, Ga., has increased acreage for cabbage, green beans, squash and specialty peppers.
This fall, the grower-shipper is planting 20% more acres of hot peppers, 15% more acres of green beans, 10% more acres of squash and increased acreage by a small amount on cabbage, said Brandi Hobby, a saleswoman.
The company added five more squash varieties and increased its variety lineup on cabbage, she said.
Typically, South Georgia doesn't grow red or savoy cabbage but planted some this season following customer requests, Hobby said.
"One reason we upped our specialty pepper acreage is to give our buyers a better variety of vegetables to pickup at one packinghouse," she said.
South Georgia Produce also grows and ships bell peppers, cucumbers and eggplant.