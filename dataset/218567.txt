In our previous commentary, we talked about risk management and the five steps to help you maximize risk management for yourself and your advisors. This brings us to an important question: Is your operation mining value out of your advisory relationships? And are you mining the value out of your risk management practices and relationships?
For us, value is easily measured—in dollars and “sense,” in insurance that protects against market shifts, and in long term and consistent gains. Clients who utilize their risk management teams effectively continue to experience significant operational growth and profitability.
Producers should expect some of the following measurable outcomes as a result of good advisory relationships:

Advising that translates to decision-making and execution
Operational capacity to act flexibly and assertively in a timely fashion when opportunity presents itself
Conversations that focus on where to go next as opposed to defensive postmortems of: who, how, and why things are right or wrong
Risk management programs that provide “insurance” against the downside of market volatility
Enhanced profitability consistently proven over time as a result of advantageous positioning opportunities
Margin management plans that don’t compromise cash flow
Alignment of your marketing plan with financial capacity and plans for operational growth
Internal expertise and capacity to co-create solutions as a result of the partnership, especially the development of the operation’s financial team and bankers
Immersion, understanding of, and solutions for other operational areas of risk, because these areas impact your risk management strategy: packer contracts, expansion plans, production problems, strategic initiatives, lender relationships, procurement, access to stakeholders who advance the operation

Do your risk management practices line up with these metrics?  If not, it’s probably time to dig deeper and determine the “why” and “how” that compromises your operational culture. Advisory relationships require both parties to contribute in a way that creates value.
You might ask yourself: What are the members of my advisory team contributing and how do I measure that those contributions?
In our next commentary, we’ll take a look at the behaviors and beliefs that influence your risk management decisions.
Editor’s Note: Karen Kerns is CEO of Kerns & Associates, a risk management firm serving agriculture in Ames, Iowa. The company employs a holistic approach to agricultural risk management, operating as an extension of clients' operations to ultimately enhance their customers’ profitability. For more information, call 515.268.8888, or go to http://www.kerns-associates.com/