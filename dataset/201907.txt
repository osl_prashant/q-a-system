Click here for related charts.

CORN COMMENTS
The national average corn basis firmed 7 1/4 cents from last week to 16 3/4 cents below September futures. The national average cash corn price declined 12 1/2 cents to $3.35 3/4.
Basis is much weaker than the three-year average, which steady with futures for this week.
 
SOYBEAN COMMENTS
The national average soybean basis firmed 2 3/4 cents from last week to stand 14 cents below November futures. The national average cash price plunged 48 3/4 cents from last week to $9.07 3/4.
Basis is well below the three-year average, which is 79 cents above futures for this week.
 
WHEAT COMMENTS
The national average soft red winter (SRW) wheat basis softened 3/4 cent from last week to 4 3/4 cents below September futures. The average cash price declined 41 cents from last week at $4.14 1/2. The national average hard red winter (HRW) wheat basis firmed 2 1/2 cent from last week to 71 1/4 cents below September futures. The average cash price declined 41 3/4 cents from last week to $3.48 1/4.
SRW basis is firmer than the three-year average of 26 cents under futures. HRW basis is much weaker than the three-year average, which is 52 cents under futures for this week.
 
CATTLE COMMENTS
Cash cattle trade averaged $115.17 last week, down $2.13 from the previous week. Only very light trade has been reported so far this week at lower levels. Last year at this time, the cash price was $118.35.
Choice boxed beef prices dropped $4.15 from last week at $197.51. Last year at this time, Choice boxed beef prices were $201.75.
 
HOG COMMENTS
The average lean hog carcass bid softened $1.59 over the past week to $84.03. Last year's lean carcass price on this date was $67.17.
The pork cutout value declined $3.14 from last week to $91.17.  Last year at this time, the average cutout price stood at $67.17.