When USDA's weekly crop condition ratings are plugged into Pro Farmer's weighted (by production) Crop Condition Index (CCI; 0 to 500 point scale, with 500 representing perfect), it shows the corn crop stands at 367.42 points, down 3.25 points from last week and 19.41 points lower than year-ago.
The initial soybean CCI of 360.57 is 14.17 points lower than year-ago.




 
Pro Farmer Crop Condition Index





Corn




This week



Last week



Year-ago


 


Soybeans




This week



Last week



Year-ago





Colorado (1.03%*)


4.13


4.07


3.91


Arkansas *(3.78%)


14.39


NA


14.11




Illinois (15.40%)


53.76


54.68


59.90


Illinois (13.85%)


50.14


NA


53.46




Indiana (6.64%)


21.51


21.85


26.74


Indiana (7.41%)


25.49


NA


28.91




Iowa (17.72%)


68.22


68.93


66.35


Iowa
			(13.35%)


50.34


NA


51.61




Kansas (4.29%)


15.63


15.24


14.82


Kansas (3.96%)


14.30


NA


13.82




Kentucky (1.57%)


6.23


6.10


6.20


Kentucky (2.14%)


8.15


NA


8.66




Michigan (2.35%)


8.87


8.83


9.49


Louisiana (1.59%)


6.17


NA


6.45




Minnesota (9.66%)


37.48


37.29


37.00


Michigan (2.38%)


8.96


NA


9.21




Missouri (3.81%)


13.60


13.67


13.66


Minnesota (8.82%)


34.24


NA


33.29




Nebraska (11.62%)


44.99


44.87


45.62


Mississippi (2.38%)


9.01


NA


9.07




N. Carolina (0.71%)


2.73


2.76


2.84


Missouri (5.86%)


21.21


NA


20.88




N. Dakota (2.70%)


9.43


9.78


9.86


Nebraska (7.46%)


28.29


NA


29.29




Ohio (3.80%)


13.16


13.12


15.88


N. Carolina (1.50%)


5.59


NA


5.81




Pennsylvania (0.98%)


3.88


3.81


4.12


N. Dakota (5.24%)


18.14


NA


18.32




S. Dakota (5.62%)


18.37


20.22


21.52


Ohio
			(6.14%)


22.30


NA


23.72




Tennessee (0.89%)


3.64


3.62


3.68


S. Dakota (5.93%)


19.52


NA


22.00




Texas (2.06%)


7.92


7.90


7.59


Tennessee (1.86%)


1.00


NA


1.00




Wisconsin (3.61%)


13.53


13.42


13.95


Wisconsin (2.29%)


8.82


NA


8.35




Corn total


367.42


370.68


386.83


Soybean total


360.57


NA


374.74





* denotes percentage of total national corn crop production.
Iowa: Dry and hot conditions helped crop development and fieldwork progress during the week ending June 11, 2017, according to the USDA, National Agricultural Statistics Service. Statewide there were 6.8 days suitable for fieldwork, the highest number of days suitable so far this year. However, Iowa could use rain in the next week, as some crops are showing signs of stress due to the dry conditions. Activities for the week included cutting and baling hay, spraying herbicides and side-dressing corn with nitrogen, and planting and re-planting. Topsoil moisture levels rated 7 percent very short, 33 percent short, 59 percent adequate and 1 percent surplus. Southeastern Iowa reported the lowest levels of topsoil moisture with 56 percent rated short to very short. Subsoil moisture levels rated 2 percent very short, 16 percent short, 79 percent adequate and 3 percent surplus.
Ninety-six percent of Iowas corn crop has emerged, one week behind last year. Seventy-seven percent of the corn crop was rated in good to excellent condition. Soybean planting reached 98 percent complete, over 2 weeks ahead of the 5-year average. Soybean emergence reached 85 percent, 2 days behind last year but 4 days ahead of average. Soybean condition rated 73 percent good to excellent.
Illinois: Warm weather and limited rain assisted farmers in the early wheat harvest. There were 6.9 days suitable for fieldwork during the week ending June 11. Statewide, the average temperature was 70.9 degrees, 0.9 degrees above normal. Precipitation averaged 0.15 inches, 0.76 inches below normal. Topsoil moisture supply was rated at 3 percent very short, 38 percent short, 56 percent adequate, and 3 percent surplus. Subsoil moisture supply was rated at 1 percent very short, 18 percent short, 77 percent adequate, and 4 percent surplus. Corn emerged was at 96 percent, compared with 97 percent for the 5-year average.
Corn condition was rated at 3 percent very poor, 11 percent poor, 28 percent fair, 50 percent good, and 8 percent excellent. Soybean planting was 93 percent complete, compared with the 5-year average of 88 percent. Soybeans emerged was at 78 percent. Soybean condition was rated 2 percent very poor, 9 percent poor, 23 percent fair, 57 percent good, and 9 percent excellent. Winter wheat harvest was 24 percent complete, compared to the 5-year average of 11 percent.
Indiana: Drier conditions led to a busy week for farmers, according to Greg Matli, Indiana State Statistician for the USDAs National Agricultural Statistics Service. Growers were able to wrap up planting, and apply fertilizer and weed control. However, some working under the driest conditions postponed those activities to wait for needed rain. The statewide average temperature was 69.0 degrees, 0.3 degrees below normal. Statewide precipitation was 0.12 inches, below average by 0.90 inches. There were 6.7 days available for fieldwork for the week ending June 11, up 1.5 days from the previous week.
Corn was 86% emerged in the North, 85% in Central, and 87% in the South. Soybeans were 91% planted in the North, 92% in Central, and 85% in the South. Soybeans were 70% emerged in the North, 68% in Central, and 63% in the South.
Minnesota: Minnesota farmers took advantage of the 6.1 days suitable for field work to nearly complete planting during the week ending June 11, 2017 according to USDAs National Agricultural Statistics Service. Continued warm and dry conditions advanced crop development statewide. Crop reporters suggest that rain is needed to benefit crops planted on lighter soils. Field activities for the week included planting, spraying herbicides, and cutting hay. Topsoil moisture supplies were rated 2 percent very short, 17 percent short, 78 percent adequate and 3 percent surplus. Subsoil moisture supplies were rated 1 percent very short, 6 percent short, 87 percent adequate and 6 percent surplus. Nearly all of Minnesotas corn has emerged. Corn condition improved slightly to 78 percent good to excellent. Soybean planting was 99 percent complete. Soybean emergence, at 87 percent statewide, remained ahead of the 5-year average. Soybean condition improved to 78 percent good to excellent.