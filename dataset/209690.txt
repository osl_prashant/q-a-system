The Food and Drug Administration has too often failed to take needed action against food facilities with significant inspection violations, according to a new report from the Office of Inspector General for the Department of Health and Human Services.

The 39-page report, “Challenges remain in FDA’s inspections of domestic food facilities,” said the FDA should:

Improve how the agency handles inspections to ensure better use of resources;
Take action against facilities with significant violations;
Improve the timeliness of its actions so that facilities do not continue to operate under harmful conditions; and 
Conduct timely follow-up inspections to ensure significant violations are corrected.

The FDA identified 21,086 high-risk facilities requiring an inspection from 2011 to 2015.
FDA also identified 61,010 non-high-risk food facilities requiring an inspection from 2011 to 2017. The agency said those inspection goals will be met, according to the report. The FDA did not disclose the proportion of fresh produce facilities among those inspected.

According to the report, the FDA is on track to meet the Food Safety Modernization Act inspection mandates. The number of food facilities that FDA inspected since the FSMA was passed decreased from about 19,000 facilities in 2011 to about 16,000 facilities in 2015. The FDA inspected 19% of all food facilities in 2016, down from 20% last year and 25% in 2011.
Lack of action
The OIG said FDA did not always take action when it uncovered significant inspection violations — those found during inspections classified as “Official Action Indicated.” When the FDA took action, it mostly relied on facilities to voluntarily correct the violations.

The report said FDA uncovered significant inspection violations in 1% to 2% of facilities inspected each year, with 1,245 facilities having significant inspection violations from 2011 to 2015. Type of foods produced at those facilities were not disclosed.
“Moreover, FDA’s actions were not always timely nor did they always result in the correction of these violations,” according to the report.
The FDA took no advisory or enforcement action in response to 22% of the significant inspection violations from 2011 to 2015.
“FDA consistently failed to conduct timely follow-up inspections to ensure that facilities corrected significant inspection violations,” according to the report.
For nearly half of the significant inspection violations, the FDA did not conduct a follow-up inspection within a year, and for 17% of significant inspection violations, the agency did not conduct a follow-up inspection at all.
The FDA initiated judicial actions such as seizures or injunctions in response to 4% of the significant inspection violations and the agency began administrative actions such as the detention of food products or suspension of a facility’s registration in response to only 1% of the significant inspection violations.
The report said FDA agreed with all four recommendations. The agency is developing a system that will track activities or information relating to each specific inspection violation to ensure all violations are corrected for all facilities that receive “Official Action Indicated” classifications.