There are some chores you can't do from horseback. From hauling feed to corralling feeders, there are some good opportunities to upgrade farm machinery at auctions and dealers across the country.Farm Journal's Machinery Pete columnist, Greg Peterson, tracks auction prices and analyzes machinery trends. "Since May 2016, monthly searches for "Livestock Equipment" on MachineryPete.com have increased 40% month over month," he says.
From mid-November to mid-December, searches of livestock equipment were up 26%‚Äîtop categories were feed wagons (35%), grinder mixers (26%) and feed bins (21%).
In October, Peterson said prices for used tractors in the 120 hp to 200 hp range were holding steady, a trend that will continue into 2017.
For example, this past summer a one-owner 1998 John Deere 7410 two-wheel drive tractor (120 hp) with 7,718 hours and a loader sold for $51,500 at a farm auction in southeast Minnesota. A solid price for a two-wheel drive.¬? e trend seems to hold for all brands.
The trend seems to hold for all brands. On Aug. 20, 2016, a 2010 Kubota M135X with 880 hours and a loader sold for $45,500 at a consignment auction in northwest Michigan.
Smaller tractors are also showing up on the block regularly, says Rick Vacha with Ritchie Auction Services. "I was anticipating that market falling off as cattle prices declined, but it has turned out to be not a soft as I originally anticipated," he says.

Sales of balers and other hay tools are a bit softer than the tractor market. Vacha says there is plenty of newer equipment on dealer lots, mainly due to a production surge when cattle prices peaked.
"¬?That drove new orders for balers up tremendously," he says. "Used baler prices have fallen a little bit, just because we're trying to get through the huge supply that's out there. ¬?
The key is to look at the accessory package. "When prices are good, producers will spend money for new innovations such as wrappers or automation," Vacha says. "Right now, producers might buy a new product, but it'll have the older technology."
Changes in the construction market is putting new life‚Äîand higher prices‚Äîon used skid steers. Com-pared to feeding with tractors, skid steers can move quicker, have two- or three-speed transmissions, large load capacities and better accessories.
"Skid steers are being used for more than livestock jobs," Vacha says. "With pallet accessories, skid steers move seed and bulk fertilizer boxes."
Searches on MachineryPete.com were up 12% for skid steers from mid-November to mid-December, Peterson adds. "Much like used 10- to 15-year-old tractors, there are many older skid steers, holding or increasing in value."

This past year, a 1999 Case 75XT skid steer with 1,215 one-owner hours sold for a record price of $20,000 on March 5, 2016. The previous record price was $18,950, from a Feb. 2, 2000, dealer auction in Minnesota‚Äî16 years ago. It only had 200 hours on it. So the Case 75XT at the March 2016 auction was 16 years older, had 1,000 more hours and still went for $1,150 more.
As feed price and availability shifts, grinder mixers are in demand. Peterson points to a March 12, 2016, farm auction in north-central Indiana, where an older model Gehl 100 grinder mixer sold for $4,600. The sale tied for the fourth highest auction price in nearly nine years. At least until results came in from a farm auction in northeast Missouri on the same day‚Äîa Gehl 100 sold for $5,900.
With a cautious outlook for cattle prices, carefully evaluate capital equipment purchases. "If you have the capital to invest in equipment, it's still a good time to buy," Vacha says.
To stay up-to-date on used machinery auctions, visit www.MachineryPete.com


Note: This story appears in the January 2017 issue of Drovers.