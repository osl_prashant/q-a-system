3-D printers are used to make toys, tools, gadgets, and even replacement parts for farm equipment.

They can even be build to print a 3-D replica of a patient's aorta! This technology allows experts to predict how specific people would react to each type of valve.

Barb Consiglio of the Ohio State Wexner Medical Center shows how these parts are saving lives on AgDay above.

EDIT: An earlier version of the story implied these parts are replacements, but they're replicas.