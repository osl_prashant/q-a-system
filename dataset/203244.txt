Produce operators will receive training on managing product recalls in a "Recall Ready Training Workshop" offered by the United Fresh Produce Association.
 
Set for April 10-11, the workshop will be at the headquarters of Darden Restaurants in Orlando, Fla., according to a news release.
 
Registration and information about the event is available online.
 
According to the release, the event will cover:
Understanding the fundamentals of a product recall;
Knowing rights and responsibilities;
Knowing the role of the FDA and how to work with it; and
Discovering strategies for limiting liability and managing customer expectations.
The release said trainers for the two-day workshop are expected to include experts on food safety, FDA regulations, crisis communications and recall insurance options.