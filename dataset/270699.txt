SUNY Cobleskill hosts 37th annual fundraising cattle show
SUNY Cobleskill hosts 37th annual fundraising cattle show

The Associated Press

COBLESKILL, N.Y.




COBLESKILL, N.Y. (AP) — Outstanding cows, calves and heifers will be strutting their stuff at the 37th annual Dairy Fashions Sale sponsored by SUNY Cobleskill.
The auction at the Schoharie County Fairgrounds on Saturday raises funds for scholarships and events for the college's Dairy Cattle Club. Organizers say it draws thousands of people who compete to purchase cattle of all six major dairy breeds consigned by nearly 80 farms.
Last year, the event grossed $240,000, with its highest seller a Red Avalanche cow consigned by Four-Hills Farm of Vermont that sold for $6,000.
To meet sale criteria, cattle must have desirable qualities such as deep pedigrees, high production and success in the show ring.