Ugly delicacy? Industry touts weird looking Monkfish
Ugly delicacy? Industry touts weird looking Monkfish

By PATRICK WHITTLEAssociated Press
The Associated Press

PORTLAND, Maine




PORTLAND, Maine (AP) — Members of the fishing industry, regulators and environmentalists are trying to convince U.S. consumers to eat more of a particularly weird looking fish.
Monkfish have been commercially fished for years, but recent analyses by the federal government show the monster-like bottom dweller can withstand more fishing pressure. However, U.S. fishermen often fall short of their quota for the fish.
Fishermen say a lack of markets and convoluted fishing regulations make it difficult to catch the full quota. Nevertheless, the U.S. government is upping their limit for monkfish.
Monkfish is often more affordable than some other kinds of domestic seafood. Monkfish tails typically sell for about $7 per pound at New England fish markets where popular items such as lobsters and flounder sell for $10 per pound or more.