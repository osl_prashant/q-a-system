Anhydrous is $75.57 below year-ago pricing -- lower $9.89/st this week at $475.02.
Urea is $18.81 below the same time last year -- lower $2.92/st this week to $325.51.
UAN28% is $37.78 below year-ago -- lower $4.36/st this week to $235.46.
UAN32% is priced $41.38 below last year -- lower $1.23/st this week at $256.51.

Anhydrous ammonia led declines in the nitrogen segment this week with Missouri falling $68.79 to lead declines. Nebraska fell $31.12 by the short ton as Kansas fell $17.60. Five states were unchanged as Only Illinois posted gains, up $7.93 on the week.
UAN28% was led lower by Missouri which softened $17.89 as Wisconsin dropped $16.15 and Nebraska shucked $12.98. Five states were unchanged as Illinois and Kansas each gained a dime per short ton.
Urea fell under the weight of declines in Kansas which fell $16.00 per short ton. Missouri softened $6.94 as Wisconsin fell $4.67 and Nebraska dropped $3.89. Four states were unchanged as Michigan was the only state to post a higher price, up 6 cents by the short ton.
UAN32% was led lower by Nebraska which fell $5.30 as Kansas softened $4.53. Six states were unchanged as Illinois fell $2.35 and Indiana softened $1.36.
Nitrogen prices continue to trend lower as the summer progresses. This week's nearly $10 decline in the regional average anhydrous price, however, widened our nitrogen margins slightly as NH3 outran declines in urea and UAN. That may suggest anhydrous has overdone it to the downside. But with NH3 priced at $475 regionally, we should be able to see a trend reversal coming from a mile away.
Of note this week, Kansas anhydrous fell below $400 per short ton, marking a new overall low for the life of the Monitor. Presently, seven of the twelve state we survey sport a 4-handle. Four states are still priced above $500, all in the eastern Belt. Michigan is at $549.47, Indiana is at $530.76, Illinois NH3 is priced at an average of $519.22 and Ohio is priced at $509.38 per short ton.
While we are firm believers that low prices cure low prices, supply side additions to U.S. production capacity will help limit upside risk in nitrogen, possibly for the next few years, given that world supplies remain ample to fill in any gaps. The market could be setting itself up for a shock if natural gas availability wanes or some other unforeseen production problem arises. For the time being, we will continue to wait for nitrogen to forge a confirmed price floor before we book for fall 2o17 or spring 2018.
December 2018 corn closed at $4.17 on Friday, July 28. That places expected new-crop revenue (eNCR) per acre based on Dec '18 futures at $664.05 with the eNCR17/NH3 spread at -189.03 with anhydrous ammonia priced at a discount to expected new-crop revenue. The spread widened 6.50 points on the week.





Nitrogen pricing by pound of N 8/2/17


Anhydrous $N/lb


Urea $N/lb


UAN28 $N/lb


UAN32 $N/lb



Midwest Average

$0.29 1/4 


$0.36 1/2


$0.42


$0.40 



Year-ago

$0.34 


$0.38 1/2


$0.48 3/4


$0.46 1/2





The Margins by lb/N -- UAN32% is at a 3/4 cent premium to NH3. Urea is 2 1/4 cents above anhydrous ammonia; UAN28% solution is priced 3/4 cent above NH3.





Nitrogen


Expected Margin


Current Price by the Pound of N


Actual Margin This Week


Outstanding Spread




Anhydrous Ammonia (NH3)


0


29 1/4 cents


0


0




Urea


NH3 +5 cents


36 1/2 cents


+7 1/4 cents


+2 1/4 cents




UAN28%


NH3 +12 cents


42 cents


+12 3/4 cents


+3/4 cent




UAN32%


NH3 +10 cents


40 cents


+10 3/4 cents


+3/4 cent