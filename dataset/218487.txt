Consumers who shop at farmers markets and other direct-to-consumer outlets buy more fruits and vegetables overall than those who don’t.
That’s one conclusion of a new study from the U.S. Department of Agriculture’s Economic Research Service called “The Relationship Between Patronizing Direct-to-Consumer Outlets and a Household’s Demand for Fruits and Vegetables.”
“I think it is interesting that the relationship seems to cut both ways,” study author Hayden Stewart said Jan. 25. “Households that like fruits and vegetables are more likely to go to direct-to-consumer outlets and going direct to consumer outlets can increase your overall level of spending on fruits and vegetables.”
He said the study shows that consumers who shopped at farmers’ markets and other direct-to-consumer outlets don’t spend less at supermarkets.
Spending more at direct-to-consumer outlets doesn’t seem to translate into spending less at traditional outlets like supermarkets, he said.
 
Small but mighty
The number of consumers going to farmers’ markets, farm stands and other direct-to-consumer outlets is fairly small, according to the study.
The USDA’s National Household Food Acquisition and Purchase Survey (FoodAPS), shows that among 4,826 FoodAPS households reporting purchases over a one-week period, 231 (5%) bought food from a farmers market or other direct-to-consumer outlets.
Fruits and vegetables were the most frequently purchased at those outlets, with about three in four consumers shopping at direct-to-consumer outlets reporting fresh produce purchases.
The results show:

Among 170 FoodAPS households that bought fruits and vegetables at direct-to-consumer outlets, weekly produce spending averaged $28.36.
For 3,388 FoodAPS households that also bought fruits and vegetables but did not go to direct-to-consumer outlets, weekly produce spending averaged $16.53.
Average fruit and vegetable spending across all 4,826 FoodAPS households was $12.60, including those who bought nothing.

Most Americans don’t often go to direct-to-consumer outlets.
The probability to buy fruits and vegetables at direct-to-consumer outlets in a given week averaged 3.5% across all FoodAPS households.
The study’s takeaway? Encouraging consumers to shop at farmers’ markets and similar outlets more often could boost overall spending on fruits and vegetables.