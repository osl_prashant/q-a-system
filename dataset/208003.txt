Salinas, Calif.-based Church Brothers Farms has brought back Ernst van Eeghen to become the vice president of business development. 
From 2007 to 2014, van Eeghen was the director of marketing and product development for Church Brothers. After Church Brothers, he was director of marketing at Twang Partners, San Antonio. He joined Germains Seed Technology, Gilroy, Calif., in early 2016, as director of sales and marketing.
 
In his new role at Church Brothers, van Eeghen will build business relationships with new and existing customers in the U.S. and Canada, according to a news release. Working in the company’s Salinas office, van Eeghen will report to Jeff Church, vice president of sales.
 
“I’m thrilled with the opportunity to rejoin this great company and it feels like coming home,” van Eeghen said in the release. “I’m looking forward to driving the business forward under the leadership and vision of Brian and Jeff Church.”
 
Church Brothers also hired Mark Zahnlecker and Sebastian Pagano for general sales roles, according to the release. Chuck Church, son of Steve Church, has moved from the marketing department to the sales department and is handling a range of accounts in his new role. 
 
“As we continue to grow our company, we have to grow our talent,” Jeff Church, vice president of sales, said in the release. “We’re very excited to strengthen our sales staff with these new hires as we move into the second half of the year.”