BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Tuesday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMar                              121.25  Up    .15Mar      120.10  120.10  119.95  119.95  Up    .75May                              123.45  Up    .10May      121.20  122.30  120.40  121.25  Up    .15Jul      123.40  124.55  122.70  123.45  Up    .10Sep      125.60  126.80  125.00  125.70  Up    .10Dec      129.00  130.00  128.40  129.00  Up    .05Mar      132.45  133.50  131.80  132.40  Up    .10May      134.50  134.75  134.40  134.50  Up    .10Jul      136.35  136.35  136.30  136.30  Up    .05Sep      138.00  138.05  138.00  138.00  Up    .05Dec      140.55  140.60  140.55  140.60  Up    .10Mar      143.05  143.15  143.05  143.15  Up    .10May                              144.90  Up    .10Jul                              146.60  Up    .10Sep      147.85  148.20  147.85  148.20  Up    .10Dec                              150.60  Up    .10