Supply chain solutions provider Linkfresh Inc. announced two new hires in the business development area.
Erich Colberg is now the company's business development executive for North America, and Felix Morales is the newest Linkfresh pre-sales consultant, according to a news release.
Colberg worked at JustFoodERP with food manufacturing customers and Microsoft Dynamics technology, Intuit, a QuickBooks reseller, and he also owned his own logistics company.
Morales has a background in accountancy but also has experience working with Microsoft Dynamics. He has served in several consultancy and business development roles with Tectura Corporation and KCP Dynamics.
Linkfresh CEO Robert Frost welcomed Colberg and Morales to the company.
"Their knowledge of ERP and Microsoft Dynamics coupled with their industry backgrounds will be a huge addition to the skill sets of the team," said Frost in the release.