The list of retailers and foodservice operators eliminating romaine lettuce from shelves and menus continues to grow, even though no recall has been issued and U.S. health agencies have not definitely linked romaine to an E. coli outbreak.
The outbreak was first reported by the Centers for Disease Control and Prevention on Dec. 28 as being “closely related genetically” to an outbreak in Canada that’s been linked to romaine. The CDC said it was conducting whole genome sequencing tests that would prove — or disprove — that link, but it has not issued an official update since then.
Consumer Reports advised consumers to avoid romaine on Jan. 3
The list of companies pulling romaine continues to grow:

Wendy’s
Foodservice supplier Compass Group
School foodservice supplier Chartwells
University of Michigan foodservice
Canadian restaurants, including Jungle Jim’s Eateries, Swiss Chalet, and Boston Pizza International

Canadian retailer Sobey’s has returned romaine back to its shelves, according to Guy Milette, vice president at Montreal-based wholesaler Courchesne Larose.
Milette said the Public Health Agency of Canada’s Dec. 11 advice to consumers to avoid romaine put distributors like his company in a bad situation where they couldn’t return the lettuce to growers/suppliers because no recall had been issued.
Milette said he fielded numerous calls about the safety of romaine, and consumers and businesses were under the false impression there was a recall.
 Courchesne Larose racked up losses of $20,000-$25,000, and was forced to throw romaine products away in the weeks following the Canadian health agency’s notice. In the past few weeks, however, demand has picked up,” Milette said.
“We’re back to selling 4,000, 5,000 cases a week,” he said Jan. 8. “It looks like people are getting fed up with (the health agency’s advice) since there was no recall.”
Health agencies in both countries have linked a death in Canada and one in California and a total of 48 illnesses, although that hasn’t been updated since Dec. 28.
U.S. fresh produce industry groups, led by the United Fresh Produce Association, have been working with governmental agencies, including the CDC, with a goal of an official declaration the outbreak is over.
The industry’s main talking point — that the last U.S. case was reported Dec. 8, making it unlikely the outbreak is ongoing — appears to resonate with FDA Commissioner Scott Gottlieb, who tweeted Jan. 9 that the “source of these cases likely is no longer on the market.”