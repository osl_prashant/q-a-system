This market update is a  PorkBusiness weekly column reporting trends in weaner pig.  All information contained in this update is for the week ended December 22, 2017.

Looking at hog sales in June 2018 using July 2018 futures the weaner breakeven was $77.44, up $4.38 for the week. Feed costs were up $0.85 per head. July futures increased $1.65 compared to last week’s July futures used for the crush and historical basis is improved from last week by $0.90 per cwt. Breakeven prices are based on closing futures prices on December 22, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was above average last week with 36,497 head being reported which is 105% of the 52-week average. Cash prices were $67.17, up $3.94 from a week ago. The low to high range was $51.00 -  $73.00. Formula-priced weaners were down $1.93 last week at $44.88.
Cash-traded feeder pig reported volume was below average with 18,700 head reported. Cash feeder pig reported prices were $77.64, up $4.27 per head from the previous week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through December 22, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential increased $0.44 this week to a projected gain of $10.27 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.