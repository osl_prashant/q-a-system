Employees were back at work June 7 after a one-day unsanctioned strike over wages at Salinas, Calif.-based Taylor Farms, but the issue of wages in the Salinas Valley apparently isn’t settled.An official with Teamsters Local 890, who participated in negotiations during the impasse, indicated that other grower-shippers in the Salinas Valley could expect to hear from the union in the near future.
“We’ll be talking to other companies,” said Crescencio Diaz, union representative who participated in talks for about 2,000 workers involved in the walkout.
Diaz said his local represents an estimated 5,000 “ag-related jobs.”
“Living in Salinas is getting more expensive, and people are in survival mode,” he said June 7. “It’s very difficult to stay with wages that represent the cost of living we had 10 years ago. We’re telling companies they need to remember it’s important to retain these working people. If rents keep going up and up and out, how many families can you get into a house? I hope employers understand that. We all need each other.”
Diaz said the walkout lasted one day, although earlier media reports said it had occurred June 5-6.
The union did not sanction the strike, but Local 890 leaders moved in and handled negotiations, Diaz said.
“The union couldn’t say much. We were shocked, too,” Diaz said. “But the company said, ‘Let’s fix this,’ and we went back to work.”
Employees returned to work when the company agreed to a $1.50 per-hour pay increase, with an additional $1 an hour coming Jan. 1, Diaz said.
Taylor Farms would not comment specifically on the matter, but released a statement from Mark Borman, company president, June 6.
“At Taylor Farms, our employees are our greatest assets. For the past 20 years, we have an enjoyed a wonderful partnership with our workforce. As the labor market continues to evolve, we remain committed to offering our employees some of the most competitive wages and benefits in the industry,” according to the statement.
“While our Salinas employees are under a union-negotiated contract for another year, we have implemented additional wage increases.”
On June 7, Borman issued a follow-up statement:
“Taylor Farms has reached a path forward with our Salinas facility employees and have resumed full operations. We remain committed to offering our employees the most competitive wages and benefits in the industry.”
Western Growers and other industry associations declined to comment on the walkout or its wider implications, including the Salinas-based Grower-Shipper Association of Central California.
A number of other grower-shippers in the Salinas Valley declined to comment on the strike, and on potential effect on their own operations.