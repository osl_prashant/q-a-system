Organic avocados account for a relatively small part of U.S. avocado sales, but the category continues to grow, and prices remain at a premium, grower-shippers say.
Organic volume has been growing slightly faster than volume of conventional fruit, said Rob Wedin, vice president of sales and marketing for Calavo Growers Inc., Santa Paula, Calif. However, sales have been significantly higher because of the price differential, which he said has been increasing in recent years.
Up to 80% of Calavo's avocados are grown in Mexico, but the company's California program has shown significant increases recently, he said.
About 5% of the avocados Calavo sells are organic, Wedin said, and that number continues to grow.
Similarly, Dana Thomas, president of Index Fresh Inc., Riverside, Calif., said about 5% of that company's avocados are organically grown in California, Mexico and Peru.
Although a variety of customers order organic fruit, most of the growth is driven by mainstream retailers who are increasing their organic offerings, Thomas said.
"Avocados over the last 10 years have gone from being an item to being a category," he said.
Most major retailers merchandise the fruit in displays of conventional bags and various sizes of loose fruit along with organic product.
Prices for organic avocados remain at a premium because production costs are high because of the need for extensive hand work in the groves and fewer options for fertilization, he said.
Giumarra Agricom International, Escondido, Calif., is a major supplier of organic avocados, said Gary Caloroso, marketing director.
"We're very big into organic avocados," he said, and organic volume increases every year.
Demand for organic fruit is surging, he said, "so we try to find ways to increase the supply."
Most of the growth is among mainstream retailers.
Retailers are able to charge a premium for organic fruit over conventional, he said.
"There should be a difference because there are more costs associated with growing organically," Caloroso said.
Mission Produce Inc., Oxnard, Calif., also is trying to cope with ever-expanding demand for organic avocados, said Robb Bertels, vice president of sales and marketing,
"We're focusing on trying to source additional organic fruit," he said.
Demand is higher than availability, Bertels said.
"We're looking for additional growers who may have organic fruit or trying to find additional sources for it just to keep up with demand," he said. "We're working hard to try to get the supply up to match the demand. It's a constant battle."
Henry Avocado Corp., Escondido, Calif., has a large organic avocado program, which it launched in 2010, said president Phil Henry.
Most of the firm's organic fruit is grown in Mexico and California, with a smaller amount coming from other sources.
Organic business is up at Henry Avocado.
"It's been growing dramatically," Henry said. "For our company, the amount of organic fruit that we offer has grown every year since 2010."
The company has converted a number of conventional groves in California to organic, he said.
Organic prices started out lower than usual this season for Mexican avocados, he said, but eventually edged upward, along with prices of conventional fruit.
As of late July, organic prices were higher than usual, as were prices of conventional product, he said.
Organic avocados - both bagged and bulk - have become additional stock-keeping units that many retailers ask for, along with conventional bagged and bulk product, Bertels said.
They give shoppers additional options, he said.
Increased demand for organic avocados shows no signs of slowing, Wedin said.
Most retailers today demand organic produce, he said, and since avocados are a popular produce item, increased demand "goes hand in hand."
"When almost all the retailers in the U.S. want to have an organic display, it creates a demand-exceeds-supply situation until supplies catch up," he said. "They certainly have not caught up at this point."