Grain mostly higher and livestock higher
Grain mostly higher and livestock higher

The Associated Press



Wheat for May advanced 13.75 cents at 4.8625 a bushel; May corn was up 5.25 cents at 3.8650 a bushel; May oats fell 1.50 cents at $2.2275 a bushel; while May soybeans gained 5.25 cents at $10.2750 a bushel.
Beef and pork were higher on the Chicago Mercantile Exchange. April live cattle was up .82 cent at $1.2192 a pound; April feeder cattle rose .75 cent at 1.3985 a pound; while May lean hogs gained 1.10 cent at $.6855 a pound.