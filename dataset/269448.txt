It’s a struggling wheat crop in the Plains adding to the anxiety of weak prices for Kansas wheat farmers. 
 
“We've had a number of conditions on our wheat crop this year that’s been difficult for it to get through,” said Justin Knopf, a farmer in Saline County, Kansas. 
 
The Kansas farmer watches wheat get put to the test every year, but 2018 is pushing wheat’s limits.
 
“It's a resilient crop, but there are a lot of things outside of our control that's adding to the stress of raising a profitable crop this year,” said Knopf. 
 
U.S. Department of Agriculture’s (USDA) latest crop progress report is painting a grim picture for Kansas wheat this year with 13 percent rated good to excellent, and 44 percent is considered poor to very poor. Nationally, it's the second worst crop since records began in 1986. 
 
The weather unleashed another round of possibly fatal weather last weekend, when a freeze blanketed the state. Below-freezing temperatures also hit Kansas in early April, but Knopf thinks that freeze wasn’t detrimental to his crop. 
 
“Easter weekend 2007 we had snow and then freezing temperatures for about four or five nights, and we lost our entire wheat crop that year,” Knopf remembered. “Our field average that year was under 10 bushels per acre.”
 
Wheat specialists are looking to determine how much damage is out there, but immaturity in central and northern Kansas is a key factor in protecting the crop from extreme cold. 
 
“It would have been a devastating freeze had the crop had normal maturity this time of year,” said Arlan Suderman, chief commodities economist with INTL FCStone. 
 
The freezing temperatures gripped some areas harder than others, and maturity of the crop varied. 
 
“There were parts of the state- like Hill City in northwest Kansas- where we measured five or six degrees Fahrenheit,” said Romulo Lollato, Kansas State Wheat and Forages Extension Specialist. “In other parts of the state, like south central Kansas where the crop is a little bit further along than in northwest Kansas, we measured 17 to 19 Fahrenheit or so.”
 
Lollato says while the extent of the damage is still being determined, the father south you travel in the state, the further along the crop is right now. That’s why he thinks the southeast portion of the state is at the highest risk of injury.
 
“We were below that 24 [degrees] threshold for that part of the state for about six hours or so,” said Lollato. “I think that can hurt—we can see some of the primary tillers get slaughtered off in that part of the state.” 
 
The bigger risk of damage may sit in Oklahoma right now, as concerned farmers cancelled new crop wheat contracts in fear that a portion of the crop is lost. 
 
"One percent of Oklahoma was headed already, more of it was in the boot stage where we'd look at 24 degrees for two hours as being a critical temperature,” said Suderman. “Temperatures dropped down in the low 20s in many of those areas of Oklahoma, so we believe there was quite a bit of damage in Oklahoma.” 
 
Oklahoma State’s small grains specialist David Marburger says scars are starting to show up. 
 
“We are starting now to see some injury symptoms, such as leaf tip burning,” he said. 
 
He says it’s still too early for most farmers to depict the extent of damage of the recent freeze in Oklahoma.
 
“It's the hardest thing to do, but it's best to wait a week - if it's still cool, maybe a little bit longer than a week - and then go out and assess what type of injury did we get on this crop,” Marburger said. 
 
It’s a delayed crop that's already struggling as drought worsens across both Oklahoma and Kansas. The latest U.S. drought monitor shows while 58 percent of Oklahoma is seeing drought, exceptional drought now covers 18 percent of the state. In Kansas, more than 97 percent is covered with dry conditions and nearly 60 percent is considered severe or worse.
 
Knopf hasn’t seen rain since fall. 
 
“After those rains finished in October, it stopped raining ever since,” he said. “We've had the driest winter on record for much of Kansas." 
 
It's that disappointing story playing out for wheat growers across the plains. 
 
“The drought has still taken away a lot of the yield potential, and that's going to hurt the crops ability to come back from this spring freeze damage,” said Suderman. 
 
“We're going to be in a real dicey situation as we head into warmer temperatures - we're going to use up that small amount of soil moisture we have at the surface pretty rapidly,” said Knopf. “The ongoing drought is real significant as we move further into spring.”
 
If rains swept the state today, Lollato says some of the state's crop has already been damaged enough that moisture would save the crop. 
 
“If we have some rain soon, we can still have a decent crop, but in parts of the state - like southwest Kansas—I believe we already hurt our yield potential,” he said. 
 
It's that potential also lost in parts of Oklahoma. 
 
“You've got a third of the state that's in despicable, desperate conditions,” said Kim Anderson, Oklahoma State University Grain Marketing Specialist. “They're looking at yields 10 bushels per acre.”
 
Anderson says if there's a silver lining in all of this, it's the possible protein of the wheat this year. 
 
“I think quality is more important than yield this year,” said Anderson. “You’re going to be paid on the bushels produced; however, if we produce a crop with relatively low protein and low test weight, we're going to have prices below $4. If we can average with high protein and test weight, we're going to have prices up around $5 to $5.25 because the market needs the wheat.”
 
However, Lollato warns that if the market is flooded with high-protein wheat, he questions if producers will actually get paid for that protein. 
 
“The last two years they were because the majority of the state had low protein, so whoever managed for that protein were getting a premium,” he said. “If everyone has a high protein, may be god for the market, but not for the producers.”
 
Whether it's from yield or protein, wheat producers like Knopf say they need higher prices than what they're faced with today. 
 
“Right now we're hovering right around break even prices, going on both sides of that,” he said. 
 
Suderman says the recipe for higher wheat prices would be with two key events.
 
“One would be a problem in another major wheat producing area of the world that competes with our milling wheat here in the Plains,” he said. “The other factor would be the continued strength in corn. The two feed off each other so if we have strength in corn, that could allow world wheat prices to drift higher, as well”
 
As wheat prices struggle to reach break-even, it’s starting to look like a disappointing finish to the final chapter of what's been a trying and challenging year.