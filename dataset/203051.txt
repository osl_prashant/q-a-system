President Donald's Trump Oval Office pen is getting quite a workout. 
 
The latest of his seven executive orders was signed Jan. 30,  called "Presidential Executive Order on Reducing Regulation and Controlling Regulatory Costs." 
 
In remarks recorded by C-SPAN at the signing in the Oval Office, Trump said it will be the largest cut "by far" of government regulations of small business.  "If you have a regulation you want, the only way it has a chance is to knock out two regulations for every new regulation," Trump said.
 
"There will be regulation, there will be control but it will be normalized control where you can open your business and expand your business easily," Trump said in his remarks.
 
"It is important that for every one new regulation issued, at least two prior regulations be identified for elimination, and that the cost of planned regulations be prudently managed and controlled through a budgeting process," the order said. 
 
The executive order states that for fiscal year 2017 - October 2016 through September 2017 - the heads of all agencies are directed that the total incremental cost of all new regulations, including repealed regulations to be finalized this year, shall be no greater than zero. The order asked the agencies to account for any costs associated with new regulations, "to the extent permitted by law, be offset by the elimination of existing costs associated with at least two prior regulations."
 
It is hard to know what this executive order will mean, if anything. 
 
Most importantly for Trump, the executive order fulfilled a promise he made on the campaign trail.
 
What it means to the future of regulations from USDA, FDA and other agencies is still murky. Does this only apply to small businesses? Will it create agency roadblocks to needed regulations?
 
Will it stall FDA's work on food safety rules?
 
Trump's can sign executive orders with the best of the presidents, but he shouldn't yet be hanging up any signs reading "Mission Accomplished."
 
--
 
Brazil has been in the news for a new traceability program for fresh produce.
 
Explained at the website for the Brazilian Association of Supermarkets (translated by Google Chrome):
 
The Food Traceability and Monitoring Program, RAMA, is a fruit and vegetable tracking and monitoring program developed by ABRAS (Brazilian Association of Supermarkets) and its State Associations. 
The RAMA Program is voluntary and promotes good agricultural practices, following the global trends of the retail sector in the attention to food safety offered to its consumers. 
The RAMA Program addresses a growing need for supply chain monitoring, with pesticide residues in fruit and vegetables as a starting point...
 
 
To go further with the translated page would strain the boundaries of Google Translate, but the idea of an association of supermarkets collectively putting in place a traceability program for their suppliers sounds like a good strategy. How various parts of the supply chain (and consumers?) would have access to the data on pesticide residues will be the challenge in the implementation of such a plan.