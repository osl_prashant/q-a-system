Corn Planted Acreage Down 2 Percent from 2017
Soybean Acreage Down 1 Percent
All Wheat Acreage Up 3 Percent
All Cotton Acreage Up 7 Percent
Corn planted area for all purposes in 2018 is estimated at 88.0 million acres, down 2 percent or 2.14 million acres from last year. Compared with last year, planted acreage is expected to be down or unchanged in 33 of the 48 estimating States.

Soybean planted area for 2018 is estimated at 89.0 million acres, down 1 percent from last year. Compared with last year, planted acreage intentions are down or unchanged in 20 of the 31 estimating States.

All wheat planted area for 2018 is estimated at 47.3 million acres, up 3 percent from 2017. This represents the second lowest all wheat planted area on record since records began in 1919. The 2018 winter wheat planted area, at 32.7 million acres, is up slightly from both last year and the previous estimate. Of this total, about 23.2 million acres are Hard Red Winter, 5.85 million acres are Soft Red Winter, and 3.64 million acres are White Winter. Area planted to other spring wheat for 2018 is estimated at 12.6 million acres, up 15 percent from 2017. Of this total, about 12.1 million acres are Hard Red Spring wheat. Durum planted area for 2018 is estimated at 2.00 million acres, down 13 percent from the previous year.
All cotton planted area for 2018 is estimated at 13.5 million acres, 7 percent above last year. Upland area is estimated at 13.2 million acres, up 7 percent from 2017. American Pima area is estimated at 262,000 acres, up 4 percent from 2017.
 
Read the full report.