Lobster shell disease nudges up slightly off of Maine
Lobster shell disease nudges up slightly off of Maine

By PATRICK WHITTLEAssociated Press
The Associated Press

PORTLAND, Maine




PORTLAND, Maine (AP) — A disease that disfigures lobsters has ticked up slightly in Maine in the last couple of years, but authorities and scientists say it's not time to sound the alarm.
The disease, often called epizootic shell disease, is a bacterial infection that makes lobsters impossible to sell as food, eating away at their shells and sometimes killing them. The Maine Department of Marine Resources said researchers found the disease in about 1 percent of lobsters last year.
Circa the early- and mid-2000s, they almost never found it in Maine. But overall prevalence of the disease remains low, especially compared to southern New England waters, where it's in the 20 percent to 30 percent range, the department said.
Scientists who study the fishery, such as microbiologist Deborah Bouchard of the University of Maine, said it remains important to monitor for the disease, which appears to correlate with warming temperatures.
"People have been investigating shell disease," Bouchard said. "I don't know if we can even call it emerging yet."
The amount of shell disease observed in Maine grew from very little circa 2005 to about a half a percent in 2012, and three of the past five years have been around 1 percent. Those numbers are so low that they don't raise alarms, but it's worth noting that the two highest recently years of shell disease — 2013 and last year — followed the two warmest years, the marine department said on its website.
Scientists are still trying to figure out exactly what causes shell disease, and one possibility is that warm water temperature stresses lobsters and taxes their immune systems, making them more susceptible. That would help explain the high incidence of shell disease in the warmer waters of Buzzards Bay, off Massachusetts, and Long Island Sound, where the lobster fishery has collapsed.
Maine's lobster fishery remains strong, with fishermen catching more than 100 million pounds of lobster for several years in a row. The crustaceans have been readily available to customers domestically as well as in emerging Asian markets.
The quality of lobsters' shells is especially important for distributors that ship to Europe and Asia, said Bill Bruns, operations manager of The Lobster Co. of Arundel.
"Our motto still sticks — we arrive alive," he said.
The Gulf of Maine is warming faster than most of the world's oceans, so increased vigilance about possible growth of shell disease remains very important, said Justin Ries, a marine biogeochemist with Northeastern University. The low percentage of Maine lobster with shell disease could be temporary, he said.
"My concern wouldn't be on that number. My concern would be what's going on in southern New England," he said. "What's happening down there is foretelling what's happening up here."