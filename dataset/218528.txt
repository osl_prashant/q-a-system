There is uncertainty in agriculture when it comes to policy. A new farm bill needs to be completed this year and there are multiple trade deals up for renegotiation. 
Will farm country possibly see some answers during an election year? 
AgDay national reporter Betsy Jibben talks with Jim Wiesemeyer, Farm Journal policy analyst about what could play out in Washington.