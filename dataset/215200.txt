Berry People, a new organic berry distributor in Hollister, Calif., made its first shipments this month.
The company sources blueberries, raspberries, blackberries and strawberries from Mexico, Chile, California and the Pacific Northwest for a year-round supply. The berries will be sold under the Berry People label.
The company was formed by two growers in Mexico, Francisco Ortiz and Gerardo Escalera, and U.S. produce industry veteran Jerald Downs, who most recently worked as the global fruit manager for San Juan Batista, Calif.-based Earthbound Farm.
“We decided after years of working together to form this new business as an organic berry and avocado distribution platform with the idea to grow with both third-party and internal production over the next seven- to 10-year horizon,” Downs said.
Ortiz, the chairman of Berry People, grows organic blueberries and raspberries. He is growing new varieties Jupiter and Atlas, which are available currently in limited volumes, with much more volume coming in the next few years.
He also grows the biloxi variety with good size and flavor, Downs said. Ortiz also grows avocados, which will be marketed under the Avo People label.
Escalera, who grows blackberries, is also exploring new varieties, currently focused on a successor to the industry staple tupi.
The company sources the rest of its offering from third-party growers.
Downs, the president of Berry People, has spent his entire career in the industry, from starting in a Fred Meyer produce department, to buying for Safeway, to running the now-defunct shipper Market 52. He described navigating the 2012 bankruptcy of that company as a difficult and humbling experience.
Downs said that since his return to produce he has received support from many growers with whom he worked earlier, including his new partners.

Want to know more about organic produce? Register for The Packer’s inaugural Global Organic Produce Expo, Jan. 25-27, in Hollywood, Fla.