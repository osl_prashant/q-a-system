California is expected to produce 70 million cartons of navel oranges in the coming season, according to a report from the California Department of Food and Agriculture and the U.S. Department of Agriculture. 
In its Sept. 12 initial forecast, which included conventional and organic navels and specialty varieties such as cara caras and blood oranges, the CDFA predicted a 2017-18 utilized production volume of 68 million 40-pound carton equivalents from 115,000 acres in the Central Valley, down from 75.6 million cartons produced on 120,000 acres last season.
 
CDFA predicts the average fruit set per tree to be 273, down from 384 last season. The average fruit diameter on Sept. 1 was 2.34 inches, up from 2.21 last year.
 
California’s Central Valley includes Fresno, Tulare, Kings and Kern counties.
 
Predictions are based on the annual Navel Orange Objective Measurement Survey, which examined fruit from 540 orange groves.