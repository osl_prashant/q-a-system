BC-Cotton
BC-Cotton

The Associated Press

NEW YORK




NEW YORK (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Thursday:
OpenHighLowSettleChg.COTTON 250,000 lbs.; cents per lb.May82.6683.2181.7782.15—.44Jul82.9483.3282.0382.40—.39Sep77.95—.05Oct78.96—.18Nov77.95—.05Dec77.8878.0077.5977.95—.05Jan78.13—.06Mar78.0178.1577.8078.13—.06May78.0278.2377.9278.21—.05Jul78.1678.2278.0078.20—.08Sep73.75+.22Oct75.82+.07Nov73.75+.22Dec73.7573.9873.5973.75+.22Jan73.84+.22Mar73.84+.22May74.39+.18Jul74.47+.15Sep72.87+.09Oct74.04+.13Nov72.87+.09Dec72.87+.09Est. sales 21,225.  Wed.'s sales 23,587Wed.'s open int 273,204,  up 837