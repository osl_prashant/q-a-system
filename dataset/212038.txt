The Corrugated Packaging Alliance, Itasca, Ill., issued its third Life Cycle Analysis report.“The LCA results demonstrate the efforts our mills and converting facilities are making to reduce the industry’s supply chain impact on the environment,” Dennis Colley, alliance executive director, said in a news release.
According to the release, the study, conducted by the National Council for Air and Stream Improvement, looked at the effects of a 2014 corrugated product on environmental impact and inventory indicators.
Indicators used for testing included:
global warming potential,
smog,
ozone and fossil fuel depletion,
water use and consumption; and
renewable and non-renewable energy demand.
The study showed a 35% decrease in greenhouse gas emissions from 2006-14, a decrease in fossil fuel use and less methane emissions, as well as an increase of 17.5% of old corrugated container recovery.
“Sustainable forest procurement, along with the high old corrugated containers recovery rate, provide for a well-balanced system of fiber and supports the sustainability of our industry’s products,” Colley said in the release.