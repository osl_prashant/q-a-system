While the name would imply the farm bill has much to do with farming, 80 percent of the bill involves food and food stamps.

On Wednesday, House Agriculture Committee Chairman Michael Conaway announced he’s delaying the release of a draft bill to find bipartisan support for a law that takes aim at government food programs.

According to a report from Bloomberg, Rep. Collin Peterson (D-Minn.) says additional work requirements for recipients of the Supplemental Nutrition Assistance Program (SNAP) would pull any support from Democrats in the overall bill.

Rep. Rodney Davis (R-Ill.), serves on the ag committee and says Republicans are willing to face that debate.

“There’s always going to be a fight over issues like some more stringent work requirements,” he said.

Chairman Conaway says his committee would send a farm bill to the house floor for a vote by the end of march, but that delay will put the timeline at risk.

Senate Agriculture Committee Chair Pat Roberts says it’s crucial to get the farm bill done in 2018 because a tariff fight may trigger retaliation that would hit farmers.

Hear Roberts’ thoughts on AgDay above.