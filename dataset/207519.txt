Most live cattle contracts at the Chicago Mercantile Exchange gained, led by technical buying and futures' discounts to initial cash prices, said traders. They said positioning as the holiday and the end of the quarter draw near contributed to advances. 
June, which will expire on Friday, closed down 0.050 cent per pound to 120.200 cents. Most actively traded August ended 0.875 cent higher at 116.500 cents, and above the 10-day moving average of 116.050 cents. October finished 1.325 cents higher at 114.975 cents. 
On Thursday, a small number of market-ready, or cash, cattle in the U.S. Plains brought $119 to $120 per cwt, compared with $118 to $123 last week, said feedlot sources. 
Packing plant holiday shutdowns and seasonally slumping wholesale beef demand weakened most cash returns so far this week, said traders and analysts. 
Market bulls believe impressive packer profits and fewer animals for sale than last week might underpin prices for unsold cattle. 
Technical buying, buy stops and live cattle futures advances lifted CME feeder contracts for a second straight session. 
August feeders ended 0.775 cent per pound higher at 147.175 cents.
CME Hog Futures Climb Ahead of USDA Report
CME lean hog futures settled higher on Thursday, and the July contract notched fresh highs, supported by their discounts to cash prices and fund buying prior to the U.S. Department of Agriculture's neutral quarterly hog report, said traders. 
The USDA's report showed a record number of hogs for the March-May period, but the results were within analysts' forecasts. 
July ended 1.550 cents per pound higher at 89.475 cents, and hit a fresh high of 89.650 cents. August finished 1.275 cents higher at 80.750 cents, and above the 20-day moving average of 80.378 cents. 
Hog futures received an added boost from surprising cash price strength, despite plants scheduled to close during the U.S. Fourth of July holiday. 
Solid profits for packers motivated them to line up hogs for what could be a big post-Saturday holiday slaughter, a trader said. 
Retailers may buy small amounts of pork until they determine how much of it moved during the holiday, he said.