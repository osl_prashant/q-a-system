The U.S. Drought Monitor has been released, and while it’s still dry in parts of the southeastern U.S., there have been improvements from rain and snowfall this week.

Despite this precipitation, 40 percent of the contiguous United States is experiencing drought or dryness. Parts of the Texas and Oklahoma Panhandles and Kansas are experiencing D3 or extreme drought, as well as the Four Corners region and east central Missouri.

On the other end of the spectrum, parts of Arkansas, Indiana, Michigan and Ohio are dealing with flooding. Rainfall coupled with melting piles of snow and frozen ground caused roads and farmland to flood.

Watch drone footage from Bill Bauer of Coldwater, Michigan of the flooding on AgDay above.