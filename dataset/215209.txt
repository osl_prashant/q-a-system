Raymond "Bob" Rowland, is doing his part to eradicate porcine reproductive and respiratory syndrome (PRRS) at Kansas State University. It is estimated that the virus costs the U.S. pork industry more than $600 million in losses every year.

“In his latest study, Rowland, professor of diagnostic medicine and pathobiology in the College of Veterinary Medicine, has created a way to protect offspring from the PRRS virus during pregnancy,” a news release from KSU said. “He has found that mothers without the CD163 protein are resistant to the PRRS virus and give birth to healthy, normal piglets. The work appears in Nature's Scientific Reports.”

"We have created a protective shell against the PRRS virus during the reproductive phase of production," Rowland said in the release. Offspring don’t become infected during pregnancy, hence healthy piglets are born, which means during this particular phase of production, the disease can’t exist.
The PRRS virus causes disease in two forms: a respiratory form that weakens young pigs' ability to breathe and a more severe reproductive form that causes mass deaths in pigs during late pregnancy.

"The reproductive form not only has a tremendous economic impact, but also a psychological impact on people who work with pigs," Rowland said in the news report. He has spent more than 20 years studying the PRRS virus, and said, "When we look at ways to control this disease, it really begins with reproduction. We want to keep this disease out of the reproductive process and we have found a way to do that."

Rowland collaborated with Randall Prather, a professor at the University of Missouri, and a team to develop PRRS-resistant pigs. PORKBusiness.com reported on this breakthrough in Dec., 2015. Using CRISPR/Cas9 technology, the researchers found that pigs without the CD163 protein showed no signs or evidence of being infected with the PRRS virus. CD163 is the receptor for the virus.
The research has the potential to save pig farmers millions of dollars, Rowland said.
However, it’s not a silver bullet. Even though offspring can be born without the virus, they may still be susceptible to the disease later in life.

"This is one tool that we can use," Rowland explains. "It doesn't mean that we can give up on vaccines or diagnostics, but it does create more opportunities for other tools to become more effective. Because this pig is born healthy, it will respond better to a vaccine or a diagnostic test. We are enhancing other aspects of disease control as well."