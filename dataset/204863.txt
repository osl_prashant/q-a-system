In November 2016, USDA forecast that for a third year in a row, U.S. farmers' net farm income is going to decline.To put that forecast into perspective, we asked four ASFMRA farm managers for their thoughts about 2017 farm profitability and the outlook for rents. These farm managers believe their strategies and farm operating budgets moving forward will keep 2017 cash rents steady overall for landowners.

Dennis Reyman, AFM, ARA, Stalcup Ag Service Inc.,Storm Lake, Iowa
"In our region, corn set record or near-record yields, and soybean yields were even better than 2015. Although incomes for 2016 aren't as high as previous years, cash rent tenants who shopped for best prices on inputs and did a good job marketing grain were likely to turn a profit."
Reyman recommends benchmarking input costs as a good starting point for forecasting farm profitability in a depressed economy.
"Review and shop expenses," he says. "If an expense projects a good ROI, there's no sense cutting it."
Reyman notes that fertilizer costs are down from a year ago, and seed costs can be negotiated for the first time in a long while.

David Englund, AFM,Farmers National Company,Omaha, Neb.
"We see farmers closely scrutinizing seed prices for 2017," says Englund, who works with landowners throughout the U.S. "Farmers look to see if any traits can be eliminated to reduce costs while still achieving the desired yields. For example, if triple-stack seed was used in 2016, and corn is planted on soybean ground in 2017, maybe the expense of a corn rootworm trait can be saved.
"Corn Belt yields were excellent the past three years," he says. "We also maximize fertilizer dollars by grid sampling and variable-rate application. We want to make sure that fertilizer withdrawals are replenished so that corn yields in 2017 aren't adversely affected. It helps that fertilizer is forecast to be down 5% to 15% this year."
Englund notes that pesticide prices also look steady. However, real estate taxes continue to be high this year compared with projected farm income.

Howard Halderman, AFM, Halderman Farm Management, Wabash, Ind.
His business heavily focuses on the eastern Corn Belt (Indiana, Ohio and Michigan). Based on a December 2016 survey of his 650 farm managers in 19 states, Halderman projects that 2016 yields in the eastern Corn Belt, combined with pricing upticks since harvest, will result in rents remaining steady in 2017.
His company uses cash "flex" rents for most landowner clients.
"The flex lease uses a base cash rent with some opportunity for upside profit, which helps smooth negotiations," he says. "Our farm managers report that while base cash rent is down slightly this year, they also negotiated down the base gross revenue."

Michael Downey, AFM, AAC,Hertz Farm Management Inc., Mount Vernon, Iowa
"I think, overall, farm operator and ag lender attitudes are more positive today than they were a year ago," he says. "When we focus on revenue per acre, the combination of strong yields, plus better-than-expected commodity prices, and the government ARC payment made in the fall, I suspect many of our farms in eastern Iowa may have higher net income for 2016 than in 2015."
Downey says that in his area rents are down 5% to 10% for 2017.
"We work with landowners to help understand a farm operator budget and how much they have to offer for rent after expenses and a profit factor," Downey says. "We aim for a fair, win-win outcome for landowners and tenants over the long run."