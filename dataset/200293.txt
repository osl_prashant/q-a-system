European and U.S. government and trade officials say they have been lobbying hard against a draft Chinese regulation on food imports, worried it would hamper billions of dollars of shipments to the world's No.2 economy of everything from pasta to coffee and biscuits.The rule, part of a drive by China to boost oversight of its sprawling food supply chain, requires all food imports to carry health certificates from October next year, even if the product is deemed low-risk.
That would be far stricter than in Europe and the United States, where certification is typically only needed for perishable goods such as dairy and meat products.
The regulation, announced in April, would potentially create a major logistical headache and boost costs for global suppliers that operate in China such as Hershey Co, Kraft Heinz Co, Mondelez International Inc and Unilever Plc.
"(The draft rule) could bring large segments of foreign food imports to a grinding halt," Germany's ambassador to China, Michael Clauss, told Reuters.
"The draft has clearly crossed the line between protecting the consumer toward outright protectionism of the domestic producer."
Chinese products are not required to meet the same standards.
China's Administration of Quality Supervision, Inspection and Quarantine (AQSIQ), the agency that oversees the safety of all imports, did not respond to requests for comment from Reuters.
China is already embroiled in disputes with the United States and Europe over metal and agricultural trade, while U.S. President-elect Donald Trump has indicated he would take a tougher approach on China, threatening import tariffs on some products.
European officials and others have held high-level talks with Chinese officials urging them to modify the rules, with talks intensifying in recent months.
Jerome Lepeintre, minister counselor for health and food safety at the European Union delegation in Beijing, said that China's government had already agreed to "revise" the proposed food safety rule. But exporting nations have been keeping up pressure on authorities in Beijing as they fret that those touted changes may be little more than cosmetic.
A Big Slice
Under the proposal, authorities in exporting countries would be required to certify the safety of every product being shipped to China, issuing a certificate designed by AQSIQ to accompany exports.
Global confectionery firm Hershey Co said it had shared comments with the Chinese government regarding the implementation of food safety rules.
Mondelez declined to comment, while Kraft Heinz Co, Unilever, Nestle and Danone did not respond to requests for comment.
China's imports of food and beverages have rapidly increased in recent years, driven by rising disposable incomes, concerns about the safety of domestically made food and the growing taste for foreign foodstuffs.
By some estimates, China will be the world's top consumer of imported food by 2018.
Of its $45.9 billion in food imports in 2015, the European Union accounted for the largest share, according to AQSIQ's website.