Scholars working to archive historic Butte fashion
Scholars working to archive historic Butte fashion

By ANNIE PENTILLAThe Montana Standard
The Associated Press

BUTTE, Mont.




BUTTE, Mont. (AP) — It's no mystery that Butte is a town rich in mining history. But two scholars from Brigham Young University-Idaho say they're uncovering a form of Butte history that has literally been hanging in the closet for years.
Since January, JoAnn Peters and Jane Sheetz have been traveling to Butte on the weekends meticulously archiving historical clothing and textiles housed in the Copper King Mansion. It all started, Peters said, when she and her husband stayed at the mansion's bed and breakfast and saw some of the historic clothing on display.
So far, the two scholars have catalogued over 2,500 items, some dating as far back as the Victorian and Edwardian eras and hailing everywhere from Butte manufactures of old to manufactures in places as far away as Paris and South America.
The diversity in the Copper King's collection, Peters and Sheetz said, is reflective of Butte's cosmopolitan past, a time when the city boasted a population of close to 100,000 and attracted immigrants from all over the world who hoped to build a life based on copper.
Erin Sigl, manager and co-owner of the Copper King, inherited the historic mansion in the 1990s. She currently lives at the mansion with her husband Pat.
Built in the 1880s, the mansion was once home to William A. Clark and boasts a cozy 34 rooms.
The mansion has been in Sigl's family since the early 1950s when her grandmother, Ann Cote, purchased the home. Sigl grew up in the mansion, moving in with her mother Ann Cote Smith in 1958.
"It was a wonderful place to live," Sigl said in a 2001 interview, adding that her mother and grandmother spent their lives going to garage sales, estate sales, and antique stores buying pieces for the home in an attempt to recapture the grandeur of the Victorian era.
Sigl's mother is also responsible for amassing the Copper King's clothing and textile collection, whose oldest pieces date to as far back as the 1840s (as far as Peters and Sheetz can tell) and contain more recent pieces, ranging from the 1920s and '30s to contemporary times.
Sigl said her mother was a fan of the theater and collected many of the pieces to be worn by actors. Many of the dresses were Sigl's mother's, and a few of them were her own, including a silver, satiny dress that Sigl said was one of her prom dresses.
For years the clothing was tucked away in the mansion's ballroom and closets, some hanging on racks and others folded into boxes.
Friends have asked to borrow some of the items for Halloween over the years, but Sigl said she always turned them down, not wanting the dresses and hats her mother painstakingly collected to get damaged.
"I have a great deal of respect for what my mother did in ways of historic preservation," said Sigl.
Sigl said she knew her collection was special — the pieces are part of her family memories — but she never imagined anyone would take an interest in it the way that Peters and Sheetz have.
"It's been a great reward for saving all these things," said Sigl. "Somebody wants to just come and take pictures of them all and catalogue them and tell us what years they were from and all those wonderful things."
Peters, an adjunct professor at BYU-Idaho in Rexburg, where she teaches in the apparel entrepreneurship program, says the collection for her has been a gold mine — or a copper mine, rather.
In addition to cataloging 730 items of apparel, Peters and Sheetz have archived footwear, hats, and accessories along with over 600 home decor items, including drapes, doilies, tablecloths, and even the mansion's lampshades.
Some of those items the two plan to display April 3 through 13 in an exhibit at BYU's David O. McKay Library in conjunction with the an April 7 student and faculty fashion show. In addition, Peters said, students are working toward creating an online archive of the Copper King collection, and Peters hopes to write a book documenting Butte's fashion history.
Peters and Sheetz (who is an intern for the project) said archiving the mansion's thousands of pieces has been invaluable to them from an academic perspective.
They said they've learned a lot about sewing and construction techniques of the past, when designers and seamstresses didn't have elastic or zippers and worked solely with natural fibers like cotton and silk. In fact, Peters said, she plans to incorporate some of those techniques into a piece she's working on for the upcoming fashion show.
"The dress that I'm designing for the fashion show was inspired by a dress I saw here at the Copper King. The sleeve is totally different than anything I've ever seen," said Peters.
Peters said her favorite piece in the collection is a 1920s to '30s sapphire dress, while Sigl said she's drawn to a bag-sleeve coat, which features billowy sleeves that hang from the arms, giving a kind of wing effect.
Another notable dress in the collection is a lacy red and black gown with a train. The three women said W. A. Clark's second wife Anna LaChapelle bought the lace for the dress from Paris, and the dress was worn by her friend, whom they referred to as "Mrs. Layton."
As for Sheetz, she said one of her favorite dresses is a 1900-to-1910 white and purple day gown.
"It's made with a silk tulle and velvet bodice. It's just an incredible piece," said Sheetz.
Ornate by today's standards, the dress was intended for casual occasions, Sheetz said, adding that it's an item that speaks to the formality and grandeur of the Victorian and Edwardian eras — a time when clothing possessed an attention to detail that Sheetz said she's drawn to.
The Montana Standard asked if anything is lost in an era where clothing is inexpensive and mass produced.
"Individuality," said Sheetz, not missing a beat.
"I would guess, just because there's no tags on this dress, no one else had a dress that looked like this," said Sheetz. "It expresses your style, it expresses your tastes, and.it expresses your level of morality."
"I prefer the politeness of society that they had and the respect for women and family," she said.
Some might see Sheetz and Peters project as obscure, but they say their hours of research and archival work have been worthwhile, noting that Butte's past is spoken through the clothing worn by the generations of families who have called the Mining City home. They hope the project will draw more attention to Butte's history and the region's contribution to fashion, both historic and modern.
"(Fashion is) not just happening in California and New York — it is happening in Montana and Idaho too," said Peters.
___
Information from: The Montana Standard, http://www.mtstandard.com