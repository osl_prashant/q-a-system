Time is running out to nominate a produce manager for the 2018 United Fresh Retail Produce Manager Awards Program.
Sponsored by Dole, the program honors produce managers who put in the hard work to boost sales and encourage consumers to eat more fresh fruits and vegetables.
The 2018 awards program nominations are due by Jan. 24.