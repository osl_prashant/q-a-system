Industry consolidation is causing the U.S. Department of Agriculture to propose reducing the number of members on the South Texas Onion Committee.
The USDA said the proposed amendment to Marketing Order 959, would reduce the size of the committee from 34 to 26 members. The USDA said the change would remove one voting producer and handler member, and one alternate producer and handler member from each of the two districts.
The proposed amendment was unanimously recommended by the committee in June.
The USDA said the reduction is necessary to reflect industry consolidation and decreasing numbers of onion producers and handlers over the past 15 years. In that time, there has been a 31% decrease in the number of onion producers and a 34% decrease in the number of handlers in the production area.
Many seats on the committee remain vacant, the USDA said, because of lack of nominees.
There are about 60 onion producers covered by the marketing order and about 30 handlers.
According to committee data, total shipments of south Texas onions were approximately 3 million 50-pound equivalents for the 2015-16 season, with a total 2015-16 crop value estimated at $37 million.
The deadline for comments on the proposal is April 30.