Led by double-digit growth in Canada and Mexico, U.S. fresh potato exports soared 17% in value last year.
U.S. Department of Agriculture statistics reveal 2017 U.S. fresh potato exports totaled $238.8 million, up 17% from a year ago.
Volume of fresh potatoes exported in 2017 totaled 544,624 metric tons, up 11% from 2016.
 
if("undefined"==typeof window.datawrapper)window.datawrapper={};window.datawrapper["XmXyE"]={},window.datawrapper["XmXyE"].embedDeltas={"100":695,"200":506,"300":458,"400":417,"500":417,"700":400,"800":386,"900":386,"1000":386},window.datawrapper["XmXyE"].iframe=document.getElementById("datawrapper-chart-XmXyE"),window.datawrapper["XmXyE"].iframe.style.height=window.datawrapper["XmXyE"].embedDeltas[Math.min(1e3,Math.max(100*Math.floor(window.datawrapper["XmXyE"].iframe.offsetWidth/100),100))]+"px",window.addEventListener("message",function(a){if("undefined"!=typeof a.data["datawrapper-height"])for(var b in a.data["datawrapper-height"])if("XmXyE"==b)window.datawrapper["XmXyE"].iframe.style.height=a.data["datawrapper-height"][b]+"px"});
 
Canada was the top-ranked export market for U.S. fresh potatoes, taking $101 million, up 15% from 2016.
Volume of U.S. fresh potatoes sold to Canada totaled 263,426 metric tons, up 9%.
Sales to Mexico were $41.8 million in 2017, up 14% from 2016. Volume of U.S. fresh potato exports to Mexico totaled 93,350 metric tons, up 2% from 2016.
Sales to other markets, with gains/loss compared with a year ago:

Taiwan, $22.2 million (19%);
Japan, $18.47 million (-5%);
Philippines, $11.9 million (80%);
South Korea, $8.9 million (160%); and
Malaysia, $6.5 million (19%).