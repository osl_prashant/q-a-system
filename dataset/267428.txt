Dallas activist Peter Johnson recalls early days backing MLK
Dallas activist Peter Johnson recalls early days backing MLK

By DAVID TARRANTThe Dallas Morning News
The Associated Press

DALLAS




DALLAS (AP) — Peter Johnson stared out the plane's window and watched the fires burning below in Washington, D.C., the nation's capital convulsed by riots, the anger below reflecting the turmoil inside himself.
The Dallas Morning News reports it was April 4, 1968. The Rev. Martin Luther King Jr. had been assassinated in Memphis, Tennessee, and Johnson had taken the first flight home to Louisiana.
Johnson had worked for King all over the South on voter drives since he was a college student. He had followed the civil rights leader on the bloody 1965 protest march through Alabama, from Selma to Montgomery. He'd met several casualties of that march, including Viola Liuzzo, a white mother of five from Detroit, shot and killed by the Ku Klux Klan while driving marchers home.
Johnson had come close to getting killed himself. In Birmingham, Alabama, police attacked him and other marchers with fire hoses blasting at levels high enough to strip bark off a tree. Just 23, his body carried scars from other run-ins with police or angry mobs.
Sitting in the plane, he worried about the safety of his family and his friends at Southern University, the historically black college in Baton Rouge.
He worried about himself. Without King, where would he be?
For Johnson, devoted to King's principle of nonviolence, the days and weeks ahead would be a test like no other.  He could give in to the rage all around him. Or do what his parents wanted. Walk away. Get a degree. Get married. Settle down.
Whatever decision he made would set the course of the rest of his life.
The memories remain vivid as Johnson prepared to return to Memphis this past week to commemorate the 50th anniversary of King's assassination. Many who accompanied him are longtime friends and activists. Some have known Johnson since he arrived in Dallas in 1969 with no intention of staying beyond a few months.
Five decades later, Johnson is the dean of civil rights activists in Dallas, dedicated to passing on the hard-won lessons of the civil rights movement to a new generation of students.
Lesson No. 1: Don't give up.
Lesson No. 2: Bring a toothbrush
Born in 1945, in Plaquemine, Louisiana, 20 miles south of Baton Rouge, the middle of five children, Johnson grew up in the post-World War II civil rights movement. His father was president of the local NAACP. His grandfather had been involved in civil rights activities since the 1930s.
His father was a deacon at Plymouth Rock Baptist Church, nicknamed Freedom Rock because of its central role in the civil rights movement in Louisiana. Peter was president of the NAACP youth chapters.
The Johnsons knew Andrew Young, later to become mayor of Atlanta and ambassador to the United Nations. A New Orleans native, Young had moved to Atlanta to work with King.  The Johnsons knew King, too, and King's father, the pastor of Ebenezer Baptist Church in Atlanta. But Peter's biggest influence growing up was his pastor, the late Rev. Jetson Davis, whose sermons demanded civil rights and who led from the front.
"It was Jetson who challenged me to take the whole question of ministry seriously," Johnson said. "So I couldn't help but be influenced by his concept of service."
Plymouth Rock became the scene of a bloody battle on Sept. 1, 1963, when police tried to stop hundreds of demonstrators protesting police brutality.  State troopers attacked them with tear gas, cattle prods and billy clubs. The demonstrators sought refuge inside the church. But mounted police wearing gas masks raced inside, tossing tear gas into pews, smashing windows and wrecking the sanctuary.
Johnson, 18 at the time, had just returned from the famous March on Washington, where King had made his "I Have a Dream" speech with its promise of an America with all races guaranteed equal rights. Back in Louisiana, Johnson was swept up in mass arrests.  "I still have nightmares about that day," he said.
As a student at Southern, Johnson joined the Congress of Racial Equality, a nonviolent, civil rights group co-founded by James Farmer, a Texan. Johnson eventually left school to work full time for the Southern Christian Leadership Conference (SCLC),  the civil rights organization founded by King.
He had grown up in a family and a church that adhered to nonviolence as a matter of spiritual principles. In the civil rights movement, he learned how to employ those principles to achieve results.
He recruited other young leaders to the cause and to go through the SCLC's training at a campus donated by the Quakers. "You learned how to mobilize, how to organize your block, how to do voter registration," Johnson said. "And how to stay alive."
Johnson had a fiery personality. "We had to mellow him a little bit," recalled Andrew Young, then executive director of the SCLC.  "A fire that burns too quickly burns out," he said. "If you lose your temper, you're going to lose your head. Don't get mad, get smart."
The last part was key. Johnson traveled the South joining voter registration drives that attracted violent responses. Live to fight another day was the motto of organizers.  "I was always hiding," he said. "I was always trying to figure out how to make it through the night."
But he enjoyed himself, too, forming lifelong friendships with others in the movement. During SCLC retreats and workshops with King, Johnson played basketball and pool with the civil rights leader.
In August 1967, Johnson was taking part in a 100-mile civil rights march across Louisiana when he received one of the most brutal beatings of his life. He got into an argument with a state trooper over the demonstrators' right to walk on the sidewalks and streets. Johnson looked back, urging those behind to keep walking when someone yelled "Duck!" Johnson turned forward just as the state trooper plunged the buttstock of his rifle into Johnson's face, shattering his teeth and cracking his jaw.
Dr. Walter Young, Andrew Young's brother, had just finished dental school in Louisiana. A medical volunteer for the march, Young patched Johnson up. He had suffered severe nerve damage in his jaw. "It took 40 years before I could get my mouth fixed completely," Johnson said.
Young, who still practices dentistry in Atlanta, said Johnson never faltered, then or since. "He's kept the movement alive to carry Dr. King's dream forward, never given up."
From its inception, the civil rights movement struggled over the question of nonviolence — whether it could truly bring change. But King's murder plunged the movement into turmoil.  When Johnson flew back to Baton Rouge after King's assassination, "the situation was precarious," he said.
Johnson headed from one hot spot to the next trying to calm people down. He argued for nonviolence and was accused of cowardice. "People who knew me knew I wasn't a coward," he said.
His argument was simple and practical: If you get into a fight with the police, "even if you win, what do you win?  The chance to fight with an even bigger militia? The National Guard? The Feds? It's never-ending. It's a fight you can't win."
Violence would harm the black community the most. "If you do this, you're going to destroy this campus. They're going to burn this community down," he said.
Only in private did he allow himself to give in to his despair. "I couldn't stop crying," he said.  "They were extremely painful days for me. They were dark days too."
Today, Johnson teaches a class on the civil rights movement to a new generation of students at the University of North Texas-Dallas. He has the look of an elder statesman, a lined brow above his silver mustache and deep-brown eyes that look directly at a person. He draws on 50 years of personal experience, much of it in Dallas. His stories bring the class to life more powerfully than any textbook.
There is no flash, no pretense. He wears a black shirt and blue jeans, a jean-jacket and a pair of white sneakers. The only adornment is a medallion hanging from his neck, a gift from an old friend. It is engraved with the Serenity Prayer.
At ease standing before the class, he tells stories about his early days in Dallas, pausing only when a hand is raised.
Johnson arrived in Dallas in late 1969 for an SCLC mission that wasn't expected to take long. The organization had produced a documentary, "King: A Filmed Record — Montgomery to Memphis." The group wanted theaters across the country to show the film and donate the proceeds to support King's mission and his family, including his widow and four young children.
There hadn't been any problems lining up theaters around the country. But in Dallas, Johnson ran into roadblocks.
"Dallas was one of the places somewhat resistant to racial change," Andrew Young recalled. "I remember there was a great deal of anxiety about desegregation at the time."
Theater owners joined by white civic leaders told him they wanted no part of the film. They considered King a communist and thought the movie would stir up problems in the black community.
"They said the movie would be bad for black-white relationships," Johnson said. "It was a nightmare."
Dallas prided itself on avoiding the racial turmoil of other big cities. The Dallas Citizens Council, composed of white business leaders, collaborated with selected black ministers to circumvent activists in the black community. In Johnson's mind, the Dallas way was just as pernicious as other forms of racism, dribbling out concessions only when it suited the white leadership.
"I was really angry with the power structure here," said Johnson, who found himself almost immediately branded an "outside agitator" by both whites and conservative black leaders.
Johnson was finally able to get the King film shown in Dallas. SCLC supporters in Hollywood put pressure on local theater owners, while Johnson arranged for city business leaders to preview a trailer of the film, which calmed them down.
Johnson was supposed to leave Dallas, but he didn't. He found himself pulled into a fight that pitted poor, black homeowners against the most powerful people in Dallas. It was the kind of lopsided dispute that rarely received any attention.
Soon after Johnson came to Dallas, the Fair Park Homeowners Association approached him, pleading for help. Their small, shotgun homes, which they had worked hard to purchase, had been condemned by the city to make way for an expansion of Fair Park. They wanted a fair price for the homes and believed the city's offers were too low.
The Fair Park saga proved to be the epitome of "how to smartly use nonviolence," Johnson told his UNT-Dallas class.  The mayor, Erik Jonsson, refused to meet with the Fair Park residents, including Elsie Faye Heggins and Al Lipscomb, who would later win City Council seats.
Johnson devised a way to change the mayor's mind. He announced plans to disrupt the Cotton Bowl Parade on New Year's Day 1970. The national media was in town for the Cotton Bowl Classic, Notre Dame vs. the University of Texas.
It had been just seven years since the assassination of President John F. Kennedy. "Dallas business leaders were extremely sensitive to the way the country saw the city after the assassination," said Johnson, and he would turn that sensitivity for leverage.
On New Year's Eve, Johnson and dozens of allies gathered in the basement of Mount Olive Lutheran Church near Fair Park. The city sent emissaries, including black ministers, who recommended Johnson call off the protest and agree to a cooling-off period. Dallas police, warning of bomb threats, showed up in riot gear and demanded the protesters leave the church.
"My deal with the police was, if you want us out of the church, you'll have to drag us out," Johnson said. "I didn't think they'd do that with the national press around."
Johnson figured they might end up in jail and brought a toothbrush. Some of the protesters were ready for something more, a confrontation that night or the next morning. But Johnson would have none of it. "My strategy was to force the city to make a decision before that happened," he said. "And God was with us that evening."
About 11:30 p.m., Mayor Jonsson called to say he would meet with Johnson that night in the mayor's office. Johnson insisted that the mayor speak directly with the Fair Park homeowners.  Just before the meeting, Johnson urged the group to get the mayor to give them some public sign of good faith.
The next morning Johnson turned on the television to watch the Cotton Bowl parade. Leading the parade was a Cadillac convertible carrying the mayor. And sitting right next to him was J.B. Jackson Jr., one of the Fair Park leaders.
In a news story, the mayor conceded the homeowners had a point: "Maybe the city has made some wrong approaches to the problem. ... I'll try to work it out and do my best to see that what (they) get is equitable."
Johnson had forced the mayor to meet with leaders chosen by the black community. "He would have to deal with black leaders the white establishment had not chosen: Elsie Faye Heggins, Al Lipscomb and J.B. Jackson," Johnson said.
"So I knew that was a nonviolent victory."
Johnson had not just found a cause to champion in Dallas, he'd found a home. After King's assassination, many of his followers dispersed around the United States, Young said.
"Jesse Jackson went to Chicago. I ended up staying in Atlanta. And Peter found a place in Dallas. All of us were young, enthusiastic and somewhat ambitious," Young said. "We each ended up finding a place to plant the seeds of freedom."
After Fair Park, Johnson turned his focus to economic justice and opportunity as part of the SCLC's Operation Breadbasket. In the early 1970s, he led boycotts of grocery chains to force their South Dallas stores to improve sanitary conditions, lower prices and provide jobs to African-Americans.
A lay minister and often called "Reverend Johnson," he used a mixture of spiritual zeal and hard-nosed street tactics — his conversation punctuated by profanities — to get his point across.
Sister Patricia Ridgley, a Catholic nun and Dallas native, was one of those who joined Johnson picketing the grocery stores. In her mid-20s at the time, Sister Patricia had done mission work in Africa. She began attending Saturday morning gatherings at the local SCLC's office in South Dallas, meetings that were part religious revival and part street politics.
"There was singing, preaching by Peter and then planning the action for that day, week or month," she said.
Johnson opened the nun's eyes to the needs of her own community and changed her life.  "I thought I knew my city," she said. "There were whole facets of Dallas I didn't know," she said. "I didn't know how much of our city suffered from economic problems. I attribute that part of my journey to Peter's preaching about the needs of the community.
"He taught me to read the world with the Scriptures in one hand and the newspaper in the other. That's how disciples are to live and find their marching orders."
So many people misunderstood Operation Breadbasket to be a food-handout program, but Johnson learned that hunger was a big problem among low-income families. He decided to go on a hunger strike on the steps of the old City Hall building to call attention to the problem.
His fast lasted 18 days, ending after the City Council approved a community anti-hunger effort called Operation Assist. Johnson has called the hunger strike one of the most important efforts of his life.
Johnson started to settle down in the mid-70s. He and the SCLC parted ways as the organization's national relevance faded. He married his wife, Dolores. They have two children and five grandchildren. Faced with raising a family, Johnson took a step back from civil rights issues to focus on earning a steady paycheck. He worked in a construction company with old friends from South Louisiana. He was also the administrator of a South Dallas hospital for a few years.
Never as happy as when he was an activist, he eventually came back to that work.
"Peter sacrificed a regular career for activism," said John Fullinwider, a longtime Dallas activist and co-founder of Mothers Against Police Brutality. "He has been economically poor. Along with being fearless, he had dedication and a sense of sacrifice," he said. "Those two qualities allow you to get a lot of done."
As the song goes, he gets by with a little help from his friends. Johnson's been driving a borrowed truck of late because his ancient Mitsubishi, with over 300,000 miles on the odometer, needs work and won't pass inspection.  "There's so much smoke coming out of the tailpipe, it's probably burning more oil than gas," said longtime friend Robert Pitre, who owns Skyline Ranch near the UNT-Dallas campus.
"He ain't trying to make no money," Pitre said of Johnson.  "He could have sold out to the power structure and walked away from his people. Material values and money are not his deal."
"He's a Christian and he believes one day things will change," said Pitre, who said he remains less optimistic.
People have to change, not just laws, Pitre said. "I don't see a lot of changes in people. We're going to have to have more respect for one another."
One thing that Pitre and Johnson agree on is that change will have to come now from the younger generation.
At UNT-Dallas, Johnson is in the classroom twice a week teaching civil rights and the Dallas social justice movement with Walt Borges, a political science professor.
"Peter is telling his students we need to pass the torch. You must pick it up and move it along," Borges said.  "He's purposely not telling them in which direction to go. That's up to them."
Johnson tells the students unvarnished stories, describing the cost of hundreds of years of racial discrimination and the harm suffered by African-Americans. His message, like King's, remains steadfastly nonviolent.
Of all the things King taught Johnson, the most important lesson — the one he most wants his students to learn — is forgiveness.
"It's so important to me that the next generation understand the concept of forgiveness, and why nonviolence is important," he said.  "To be able to forgive people who slap you and hit you and spit on you and hit you with billy clubs has a disarming impact. The ability to forgive this person changes his idea about who you are," he said.
"It's the most significant lesson I learned from Martin Luther King, Ralph Abernathy and Andrew Young," he said.  "And as my years have evolved, I realize how valuable forgiveness is," he said.
"I'm convinced it's why I'm alive today."
___
Information from: The Dallas Morning News, http://www.dallasnews.com


This is an AP Member Exchange shared by The Dallas Morning News