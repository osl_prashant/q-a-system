Editor's Note: The article that follows is part of the 13th edition of The Packer 25 Profiles in Leadership. These reports offer some insight into what drives successful people in produce. Please congratulate these industry members when you see them and tell them to keep up the good work.

Michael Castagnetto grew up in Salinas, Calif., part of a family with strong ties to the produce industry, so it’s no surprise to him to find himself in a produce career.
Castagnetto is vice president of global sourcing for Eden Prairie, Minn.-based Robinson Fresh, the produce division of C.H. Robinson Worldwide Inc., but he got his start in the industry as a produce trader for America West after graduating from Saint Mary’s College of California.
A merger between America West and FoodSource was followed by several years of increasing leadership responsibilities for Castagnetto, including managing new projects and ventures, when Robinson Fresh acquired the company in 2005.
Since that time, Castagnetto has worked in nearly every area of the business, from business development to operational and divisional leadership, he said. 
Castagnetto seeks to be a transformational leader, bringing energy to get things moving and growing.
“I believe that leadership is a responsibility and privilege to inspire and drive from the front,” he said. “As leaders we set the tone of innovation and energy. We are responsible to provide a consistent and effective culture, identity and strategy.”
And he’s living up to his ideals, said Jim Lemke, Robinson Fresh president.
“Mike is a tireless and consistent leader that exudes an enormous amount of business acumen from financial expertise to strategic industry knowledge,” Lemke said.
Lemke said Castagnetto excels at implementing change to improve the business.
“His desire is to move quickly and not dwell on something that shows a clear need for change,” Lemke said. “He is a competitor and wants to win, yet doesn’t let competition get in the way of always doing the right thing and ensuring everyone walks away feeling good about decisions.”  
Collecting and analyzing data is essential in successfully managing a global supply chain, and it’s Castagnetto’s job to stay on top of the data that Robinson Fresh’s customers rely on for their business decisions.
“He is very fact-based and he is continuously studying the moving elements of the business,” said Jose Rossignoli, general manager of tropicals for Robinson Fresh.
Castagnetto’s leadership extends to the broader produce industry as he serves on the United Fresh Produce Association board of directors.
He played rugby in college and serves on the board for Saint Mary’s College of California rugby team.
Castagnetto has also coached high school and youth football for the past 20 years, he said, and coaches his son’s sixth grade team.
His coaching mentality comes through in his approach to leadership at Robinson Fresh.
“I believe we need to challenge our team to do more than they thought was possible and to focus on how we find a solution to an opportunity," Castagnetto said. and not accepting the reasons why it might be difficult to achieve.”