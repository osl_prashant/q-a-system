The Produce for Better Health Foundation has chosen four groups of students as finalists in its Formula 5 Student Marketing Competition.
In the competition, marketing students from U.S. colleges create a marketing pitch designed to increase consumption of fruits and vegetables, with each team given access to a produce industry leader as a mentor.
“This is a wonderful experience both for the four student groups and the produce industry,” said PBH Formula 5 committee chairman Greg Johnson, editor of The Packer, in a news release. “We get to see and hear the ways these college students would market fruit and vegetable products, and they get to meet and talk to those of us who do it every day.”
A panel of judges reviews each submission to choose the finalists based on criteria including business proposition, financials, marketing tactics and the viability of the product/marketing proposal, according to the release.
The finalists compete at PBH’s Annual Conference: The Consumer Connection April 6 in Scottsdale, Ariz., pitching their proposals to industry leaders.
This year’s finalists are bringing the following pitches to the PBH conference:

Arizona State University: Moderna Moringa Greens;
Concordia University, St. Paul, Minn.: Vibes Premium Coconut Chips;
University of North Dakota: Broc-Tons; and
University of Wisconsin, Whitewater: Odyssey Greek WOAHgurt.

“We are delighted to sponsor this program and inspire the next generation of marketers,” said PBH’s president & CEO, Wendy Reinhardt Kapsak, in the release. “This group of finalists has done a wonderful job proposing new and innovative ideas to incorporate more fruit and vegetables on the plates of consumers.”
Information on the marketing competition is available at www.pbhfoundation.org. An e-mail of intent to submit a completed application is due by Sept. 30.