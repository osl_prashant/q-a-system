Touting success in securing tax reform and cutting regulations in his first year in office, President Donald Trump vowed progress on the nation’s infrastructure and immigration policy during his first State of the Union Address, but he never mentioned NAFTA by name.
“Just as I promised the American People from this podium 11 months ago, we enacted the biggest tax cuts and reform in American history,” Trump said in the Jan. 30 speech. Tax reform cut the business tax rate from 35% to 21%, and small companies can deduct 20% of their business income.
Since the November 2016 election, Trump said the U.S. has added 2.4 million new jobs and the stock market has added $8 trillion in value. Unemployment claims are the lowest in 45 years and he said African-American and Hispanic-American unemployment rates are at historic lows.
Trump said his administration has eliminated more regulations in its first year than any administration in history. Trump said the U.S. has “turned the page on decades of unfair trade deals.”
Making no mention of the North American Free Trade Agreement, Trump said the U.S. expects trading relationships to be “fair and reciprocal.”
Jim Bair, president and CEO of the U.S. Apple Association, said in a statement that he was pleased with Trump’s appreciation of the importance of trade.
“The one thing that has accomplished the most for free and fair trade in apples, and most of agriculture, is the North American Free Trade Agreement,” Bair said, noting that since the agreement was signed, U.S. apple exports to Mexico have quadrupled, and exports to Canada have doubled.
Bair said U.S. Apple is working with the White House, the U.S. Trade Representative’s Office and other federal agencies to preserve NAFTA trading relations.
 
Infrastructure and immigration
Trump said he is asking Republicans and Democrats to produce a bill that generates at least $1.5 trillion for new infrastructure investment in the U.S.
On immigration policy, Trump said he will put forward “immigration policies that focus on the best interests of American workers and American families.”
Trump said that in the next few weeks, the House and Senate will be voting on an immigration reform package. He said the White House is working on a bipartisan approach to immigration that includes four pillars:
> A path to citizenship for 1.8 million illegal immigrants who were brought here by their parents;
> Border security provisions that include a wall on and more hiring of immigration agents;
> Merit-based immigration rules that admit people who Trump said are “skilled, who want to work, who will contribute to our society”; and
> Ending chain migration, limiting sponsorship to spouses and minor children.
“For over 30 years, Washington has tried and failed to solve this problem,” Trump said. “This Congress can be the one that finally makes it happen.”
Rep Joe Kennedy, D-Mass., said in the Democratic response to the State of the Union that the Trump administration is bent on slashing the safety net for Americans.
“We are bombarded with one false choice after another: Coal miners or single moms. Rural communities or inner cities. The coast or the heartland.”
Kennedy said the Democrats won’t be bullied.
“We choose an economy strong enough to boast record stock prices and brave enough to admit that top CEOs making 300 times the average worker is not right,” Kennedy said in his prepared remarks.
 
Ag reaction
Agricultural interests were supportive of Trump’s message to move on infrastructure and immigration, as long as those priorities accounted for their needs.
“The bold package of immigration reform measures he put on the table tonight should prime the pump for overdue action, and we encourage Congress to take action in a timely manner,” America Farm Bureau Federation president Zippy Duvall said in a statement. “While we must do more to secure our borders, the fact remains that our farmers and ranchers need access to agriculture labor they can depend on.”
Duvall said agriculture must be part of President Trump’s proposal for merit-based immigration.