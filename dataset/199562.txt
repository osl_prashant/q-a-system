It's never too early to be focused on the next potential forage stressor - Summer. In the Southeast we are no stranger to the long, hot, dog-days of summer and the impacts we see on forage availability throughout the grazing season. We are fortunate that much of our area has some very hardy, prolific, perennial forage options, but in the most extreme drought even bermudagrass and bahiagrass have difficulty surviving for the long haul. In order to protect our perennial forages for long-term stand life, there are times that we should evaluate alternative forage options to allow our permanent stands to rest and recover.The first key to drought management is preparedness and having the right tools ready when needed. Planting summer annual forage grasses is yet another "tool in our toolbox" to help extend the grazing season and provide forage during environmental stressors.
Why use summer annual forage grasses?
When looking for a short term, temporary pasture alternatives, summer annuals are an excellent choice. They can provide "fast forage" when forage is limited. They are known for being relatively quick and easy to establish, with quick emergence, and high productivity for a short amount of time. Best of all, summer annual forage grasses are very drought and heat tolerant and provide relatively high forage quality in comparison to some of the other forage options at the same time of year.
When to consider summer annual forage grasses?
When referring to our forage management plans, we do not recommend annually relying on summer annual forage grasses, rather relying on them as a "tool in the toolbox" ready to be used in the right situation. How do we know when to consider summer annual forage grasses? Refer to the following table for more:

When to consider summer annual forage grasses:


Use:


Situation:


As an Emergency Forage Crop


When Forage Supplies are Low and Quick Growth is Needed


In Double-Cropping System


Providing a Temporary Cover


When "Salvage-Crops" are Needed


To Make Use of Fertilizer Applied to Summer Row Crops that Have Failed


In Annual Rotation Forage System


Typically with Crabgrass

Is it economical to use summer annual forage grasses? 
As any Ag Economist would tell you - It's Depends. There are a variety of things to consider when justifying the use of summer annual grasses. Each situation is different and not all situations justify planting summer annual forage grasses. Start by asking yourself a few questions like:
What are the forage needs?
Do I have a surplus or lack of forage supply?
What are the forage needs of my livestock?

What class of animal am I feeding?
Dairy: Can justify the use of summer annuals
Beef Cow/Calf: can justify in an emergency forage situation
Beef Stocker: highly dependent on the price of cattle
Equine: dependent on forage need and other forage production options available - Avoid Sorghum Species.
What are the current cattle and/or milk prices?
Remember: Land preparation, seed, and fertilizer are needed annually. These costs tend to be relatively high and should be evaluated strongly when considering the use of summer annual forage grasses.
When do I plant summer annual forage? 
Spring planting is recommended but planting can be variable. Summer annual forage grasses can be planted later than most crops and still produce a decent yield within about six weeks. As with all planting date recommendations, planting near optimum time and environmental conditions will provide the best overall results.

Crop


Planting Dates


Soils


Seeding Depth (inches)


Seeding Rate (lb/A) Pure Live Seed 














Broadcast


Drilled 


Crabgrass


Late Feb - May.


Well-drained;pH 5.6-7.5


1/4 -1/2


4-6


4-6


Pearl Millet


May 1 - Aug. 1


Well-drained, fertile;pH 5.6-6.5


1/2 - 1 1/2


25-30


10-15


Sorghum (Forage)


Apr 1-Aug 1


Well- drained;pH 5.6-6.5


1-2


20-25


15-20


Sorgum-Sudan Hybrid


Apr 1- Aug. 15


Well-drained, productive;pH 5.6-6.5


1/2-1


20-25


15-20


Sudangrass


May 1 - Aug. 1


Light sandy to heavy clay;pH 5.6-6.5


1/2-1


30-40


20-25


Note: Presented pH values are a range and may not represent the ideal pH of planting.


*The above information is for recommendation purposes only, planting dates, rates, etc. may be adjusted according to specific situation and location.All species recommendations may not be suitable to your specific area and are for information purposes only.

While we may be enjoying greener pastures today, let's not forget to be prepared for what comes tomorrow - as we are always only one day away from a drought.