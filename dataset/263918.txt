Nominees sought for 2018 Vermont Dairy Farm of the Year
Nominees sought for 2018 Vermont Dairy Farm of the Year

The Associated Press

BURLINGTON, Vt.




BURLINGTON, Vt. (AP) — One dairy farm in Vermont will receive top honors based on factors like conservation practices and milk quality.
The University of Vermont Extension and the Vermont Dairy Industry Association are looking for nominations for this year's Vermont Dairy Farm of the Year. Each year since 1961 the award honors one outstanding dairy farm for overall excellence.
The deadline for nominations from the public is April 27.
A judging committee made up of past winners reviews the nominees based on their farm management, innovative practices, crops, pasture quality and promotion of the dairy industry.
The winner will be announced this summer.