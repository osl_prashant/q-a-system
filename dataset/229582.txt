With a priority of social sustainability, Fowler, Calif.-based Bee Sweet Citrus offers a wellness program to full- and part-time employees and their families.
The program gives access to health insurance and savings plans as well as medical and dental care, according to a news release.
Registered dietitians and nutritionists also visit Bee Sweet’s packing facility twice a month to help with meal planning and basic health services, according to the release.
“Our company is only as strong as our workforce,” Jasmine Reynozo, human resources manager, said in the release. 
“We want to make sure that our employees and their families know that their health and well-being are of the utmost importance to the Bee Sweet Citrus family.”