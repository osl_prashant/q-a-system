The Latest: Runoff set to challenge GOP US Rep. Sessions
The Latest: Runoff set to challenge GOP US Rep. Sessions

The Associated Press

AUSTIN, Texas




AUSTIN, Texas (AP) — The Latest on primary day in Texas (all times CST):
12:55 a.m.
A former U.S. Department of Agriculture official will join a former NFL linebacker and civil rights attorney in the May runoff to face U.S. Rep. Pete Sessions.
Lillian Salerno and Collin Allred advanced Tuesday to a May 22 runoff for the Democratic nomination in a Dallas-area U.S. House district.
Sessions was first elected to Congress in 2002 and his district includes Dallas enclaves north of downtown, such as upscale Highland Park and Richardson.
Hillary Clinton got more votes than Donald Trump in Sessions' district in 2016, even as no Democrat challenged Sessions. With renewed Democratic energy this year, a seven-way field of Democrats emerged to try to challenge him this time.
___
12:45 a.m.
A Houston Democrat has advanced to the runoff for a Houston congressional seat despite facing attacks from the top echelons of her own party.
Laura Moser advanced Tuesday to a May 22 runoff against attorney Lizzie Pannill Fletcher.
Moser moved from Washington to her native Texas and joined the seven-way Democratic primary to try and unseat longtime U.S. Rep. John Culberson.
Culberson's district is a key target for national Democrats trying to win back the U.S. House in November. In a sign of the seat's importance, the Democratic Congressional Campaign Committee took the unusual step of slamming Moser, accusing her of expressing "outright disgust for life in Texas."
Hillary Clinton beat Donald Trump in Culberson's district in 2016, making Democrats hopeful he's vulnerable.
12:40 a.m.
A retired Navy SEAL will join a Texas state representative in the Republican primary runoff in May to replace outgoing U.S. Rep. Ted Poe.
Dan Crenshaw served in Afghanistan and lost his right eye in a battlefield explosion. He'll face State Rep. Kevin Roberts, who has served one term in the state Legislature.
Republican mega-donor Kathaleen Wall did not advance to the runoff despite pumping close to $6 million of her own money into her campaign for District 2 in the Houston area. Her husband is the founder of Houston-based tech company Texas Memory Systems.
Wall also had Texas Gov. Greg Abbott's endorsement.
___
12:35 a.m.
For all the talk of renewed Democratic energy heading into the 2018 midterms, Texas Republicans have set a new benchmark for turnout in a midterm election.
More than 1.5 million people voted Tuesday in the Republican primary for U.S. Senate, won by incumbent Ted Cruz. That beats the previous record of 1.48 million in 2010, during former President Barack Obama's first term.
The 2010 election was a massive wave for Republicans, who took control of the U.S. House.
Texas remains a deeply conservative state that hasn't elected a Democrat statewide since 1994.
At least 1 million people voted in Tuesday's Democratic primary for Senate.
___
12 a.m.
A businessman will face a former chief of staff to U.S. Sen. Ted Cruz in the runoff for the Republican nomination to replace U.S. Rep. Lamar Smith in a hotly contested San Antonio district.
Matt McCall moved on Tuesday to the May 22 runoff. He will face former Cruz staffer Chip Roy.
More than 15 Republicans entered the race to replace Smith, who announced in November he would retire from a seat he's held since 1987. Smith is currently the chairman of the U.S. House Science, Space and Technology Committee and is a noted skeptic of climate change.
He is one of eight Texas congressmen not seeking re-election this year.
___
11:45 p.m.
Texas state Rep. Kevin Roberts has advanced to the Republican primary runoff to replace outgoing U.S. Rep. Ted Poe.
Roberts has served one term in the state Legislature. His opponent in the May runoff was not immediately clear as votes were still being counted.
Poe is stepping down after six terms in Congress. He's one of eight Texas congressmen not seeking re-election this year.
___
11:40 p.m.
The seven-way Democratic primary in Dallas for the right to challenge longtime U.S. Rep. Pete Sessions is headed to a runoff.
Leading the pack is Colin Allred, a former linebacker for the NFL's Tennessee Titans and a civil rights attorney. Allred's opponent in the May runoff wasn't immediately clear as votes were still being counted.
The winner will face Sessions in November. Sessions was first elected to Congress in 2002 and his district includes Dallas enclaves north of downtown, such as upscale Highland Park and Richardson.
Hillary Clinton got more votes than Donald Trump in Sessions' district in 2016, even as no Democrat challenged Sessions. That sparked this cycle's crowded field of Democrats who believe Sessions is vulnerable.
___
11:35 p.m.
Attorney Lizzie Pannill Fletcher has advanced to a May runoff for the Democratic nomination to face longtime U.S. Rep. John Culberson in a Houston-area district.
Fletcher moved on Tuesday night to the runoff. Her opponent was not immediately clear as votes were still being counted.
Culberson's district is a key target for national Democrats trying to win back the U.S. House in November. In a sign of its importance, the Democratic Congressional Campaign Committee took the unusual step of slamming one of Fletcher's opponents, Laura Moser. A DCCC spokeswoman accused Moser of previously expressing "outright disgust for life in Texas," in hopes of driving voters to others running in the primary.
Fletcher says she became the first female partner at her law firm.
___
11:25 p.m.
The race to replace a Texas congressman who's not seeking re-election amid allegations of sexual harassment is headed to a primary runoff.
Republicans Bech Bruun and Michael Cloud advanced Tuesday from a five-candidate field in the South Texas congressional district that currently represented by U.S. Rep. Blake Farenthold.
Farenthold announced in December he wouldn't seek re-election amid pressure from fellow Republicans. He did so while denying 3-year-old accusations that he'd subjected a former aide to sexually suggestive comments.
Bruun is the former chairman of the Texas Water Development Board and was endorsed by his old boss, former Texas Gov. Rick Perry.
Cloud is a past chairman of the Victoria County Republican Party.
___
11:15 p.m.
A lesbian Iraq war veteran vying to become the first Asian-American in Congress from Texas will need to win a primary runoff election if she's going to challenge incumbent Republican Rep. Will Hurd.
Gina Ortiz Jones advanced Tuesday to a second round of voting May 22. It was not immediately clear who she would face.
Hurd's territory sprawls from San Antonio to nearly El Paso, encompassing 800-plus miles of the U.S.-Mexico border and is one of Texas' few swing districts.
When Hurd won re-election in 2016, his victory marked the first time the district hadn't flipped between parties since 2010. He's one of just two black Republicans in the House.
___
11:05 p.m.
Texas is poised to send its first Latina to Congress after State Sen. Sylvia Garcia has won the Democratic nomination to replace retiring U.S. Rep. Gene Green of Houston.
Garcia easily beat six little-known candidates on Tuesday, including health care executive Tahir Javed, who vowed to spend his own money lavishly to compete in the race. She immediately becomes the overwhelming favorite to win November's general election.
The district includes swaths of north and east Houston and is nearly 80 percent Hispanic but has been represented since 1992 by Green, who speaks minimal Spanish. That's a key reason why Houston has the country's largest Hispanic population without a Latino representative in Congress.
In El Paso, former county judge Veronica Escobar also won the Democratic nomination to replace U.S. Rep. Beto O'Rourke.
___
11 p.m.
A former chief of staff to U.S. Sen. Ted Cruz has advanced to the runoff for the Republican nomination to replace U.S. Rep. Lamar Smith in a hotly contested San Antonio district.
Chip Roy moved on Tuesday to the May 22 runoff. It was not immediately clear who he'd face.
More than 15 Republicans entered the race to replace Smith, who announced in November he would retire from a seat he's held since 1987. Smith is currently the chairman of the U.S. House Science, Space and Technology Committee and is a noted skeptic of climate change.
He is one of eight Texas congressmen not seeking re-election this year.
___
10:50 p.m.
A Republican strategist backed by Gov. Greg Abbott is headed to a primary runoff in her bid to replace outgoing Texas congressman Jeb Hensarling.
Bunni Pounds on Tuesday advanced to a May runoff against state Rep. Lance Gooden. Eight Republicans had jumped in the race after Hensarling announced last year that he was retiring after 16 years in Congress.
Hensarling was the powerful chairman of the House Financial Services Committee. He's one of eight Texas congressmen not seeking re-election this year.
Pounds is a GOP fundraiser who has never held public office. She and Kathaleen Wall, a Republican mega-donor in Houston, were the only congressional candidates that Abbott endorsed in the primary.
Gooden has served in the Texas Legislature since 2011.
___
10:40 p.m.
The packed Republican primary to replace retiring U.S. Rep. Joe Barton is headed to a runoff.
None of the 11 candidates won a majority Tuesday, prompting a second round of voting May 22.
The best known is Ronald Wright, an ex-Barton aide who was a county tax assessor-collector who added "In God We Trust" to tax bills and payment envelopes. Also advancing Tuesday was ex-Navy combat pilot Jake Ellzey.
Barton is leaving after almost 30 years in Congress following the appearance online of a nude photo of him. A Republican activist subsequently revealed suggestive Facebook messages that the then-married congressmen sent her in 2012.
The district includes suburban Dallas and east Fort Worth and is probably safely Republican, though five Democrats competed for their party's nomination to face the GOP runoff winner.
___
10:30 p.m.
Agriculture Commissioner Sid Miller, known for his provocative social media posts about Democrats and Muslims, has survived a bitter Republican primary in his bid for re-election.
Miller on Tuesday topped veteran Austin lobbyist and conservative podcaster Trey Blocker, and Jim Hogan, who ran for agriculture commissioner as a Democrat in 2014.
Miller will be the favorite to win in November. He's perhaps best-known for a social media presence that has frequently stirred controversy.
He once tweeted a derogatory term to refer to Hillary Clinton, shared a Facebook post advocating bombing the "Muslim world" and has retweeted misleading reports claiming to be factual news.
Miller also used taxpayer funds in 2015 to travel to Oklahoma for a "Jesus shot" meant to alleviate all pain. He later reimbursed the state.
___
10:20 p.m.
Democratic turnout in the Texas primary has hit its highest level in a midterm election since 2002, a reflection of the nationwide momentum for liberal candidates opposing President Donald Trump.
At least 700,000 people voted in the Democratic primary election for U.S. Senate won Tuesday by U.S. Rep. Beto O'Rourke. That surpasses the last three midterms. Just over 1 million people voted in the Democratic primary in 2002.
Texas Democrats haven't won a statewide race since 2002, and O'Rourke and other Democrats running statewide remain heavy underdogs. Tuesday's Democratic numbers are still well behind Republican turnout figures.
But O'Rourke has recently outraised U.S. Sen. Ted Cruz, and there is at least one Democratic candidate in every congressional district — 36 of them — for the first time since 1992.
___
10 p.m.
Former county judge Veronica Escobar has won a crowded Democratic primary to succeed U.S. Rep. Beto O'Rourke in El Paso.
Escobar beat five other hopefuls Tuesday and immediately becomes the favorite in November's general election.
O'Rourke is giving up his seat as he mounts a longshot campaign against incumbent Republican U.S. Sen. Ted Cruz.
Texas has never elected a Hispanic woman to Congress but that's poised to change. In addition to Escobar, state Sen. Sylvia Garcia is the heavy favorite to replace retiring Democratic U.S. Rep. Gene Green in Houston.
Escobar was among the best-known and top-funded of the primary candidates — though she drew some criticism because her husband is federal immigration judge Michael Pleters, who was confirmed last August by the Trump administration.
___
9:50 p.m.
Texas Land Commissioner George P. Bush has won the Republican nomination to keep his post, topping a primary challenger from the right.
Bush, whose grandfather and uncle were president and whose father was Florida's governor, beat his predecessor as land commissioner, Jerry Patterson, Tuesday. The 41-year-old Bush immediately becomes the overwhelming favorite for re-election in November.
Bush began the year with $3.4 million in campaign funds compared to Patterson's roughly $100,000. He won despite campaigning lightly, spending much of his time visiting parts of Texas hit by Hurricane Harvey after his agency was tapped to lead the state's housing recovery efforts.
Patterson criticized Bush for bungling the post-Harvey rebuild and a makeover of the Alamo in San Antonio. Bush was endorsed by President Donald Trump and his son, Donald Jr.
___
9:35 p.m.
The first Hispanic female sheriff in Texas is headed to a Democratic runoff against the son of a former governor in their longshot bid to unseat Republican Gov. Greg Abbott.
Former Dallas County Sheriff Lupe Valdez and Houston investor Andrew White advanced Tuesday from a field of nine largely unknown Democratic primary candidates. The runoff election is May 22.
Texas Democrats haven't won a statewide race since 1994. The party's rising stars in Texas all passed on running for governor after Democrat Wendy Davis lost by 20 points in 2014.
White is the son of former Texas Gov. Mark White, who served one term in the 1980s and died last year. Andrew White is a self-described "conservative Democrat" who has alienated progressives over his personal opposition to abortion.
Valdez would be the first Hispanic and openly gay governor in Texas history.
9:10 p.m.
U.S. Sen. Ted Cruz's newest ad targeting his Democratic challenger is a country jingle that warns, "If you're going to run in Texas, you can't be liberal, man."
Cruz tweeted the 60-second radio ad Tuesday night, shortly after he and U.S. Rep. Beto O'Rourke advanced to a closely watched November matchup.
The ad says O'Rourke "wants to take our guns" and calls him "liberal Robert," referring to his given first name, and says he "changed his name to Beto."
Cruz also uses a nickname for his given name, Rafael Edward Cruz.
O'Rourke is raising more money than Cruz so far but remains very unlikely to win in November. Texas has not elected a Democrat statewide since 1994.
___
9:05 p.m.
Texas state Sen. Van Taylor has won the Republican nomination to replace outgoing U.S. Rep. Sam Johnson.
The 45-year-old Taylor has been a state lawmaker since 2010. The businessman is a former Marine and Iraq War veteran who was first elected to the state House and has been in the state Senate since 2015.
Taylor has been endorsed for Congress by Gov. Greg Abbott and U.S. Sen. Ted Cruz.
Johnson is one of eight Texas congressmen not seeking re-election this year. He had held the seat since 1991 in the solidly Republican Dallas-area district..
___
8:55 p.m.
Republican Gov. Greg Abbott is striking a cautious tone despite sailing to victory against little-known challengers in Texas' first-in-the-nation primary.
Abbott easily clinched his party's nomination Tuesday and has a campaign war chest worth around $43 million, more than any gubernatorial candidate nationwide.
It's not yet clear who Texas' Democratic nominee will be, but Abbott is nonetheless heavily favored in November.
Still, Democrats more than doubled their early voter participation in the primary when compared to the Texas' last midterm election in 2014. They are optimistic despite not winning a Texas statewide office since 1994.
In a statement, Abbott declared "We cannot afford to take Texas for granted," adding "This is a fight for our future and it begins now."
___
8:15 p.m.
Republican Texas Attorney General Ken Paxton has clinched his party's nomination for re-election despite being indicted on felony securities fraud charges.
Paxton was unopposed in Tuesday's first-in-the-nation Texas primary. He is the overwhelming favorite in November since a Democrat hasn't won statewide office in Texas since 1994, the country's longest political losing streak.
Paxton is facing a much-delayed trial for allegedly defrauding wealthy investors in a technology startup. The accusations cover actions before Paxton took office in 2015.
Despite that negative attention, Paxton wasn't challenged in the Republican primary — even as other top members of his party were.
__
8 p.m.
A closely watched matchup is set between U.S. Sen. Ted Cruz and his Democratic challenger, U.S. Rep. Beto O'Rourke.
Cruz and O'Rourke clinched their parties' nominations Tuesday. O'Rourke is raising more money than Cruz so far but remains very unlikely to win in November.
Gov. Greg Abbott also clinched the Republican nomination for governor and enters the general election as a heavy favorite against whoever emerges from the Democratic primary.
Polling places in El Paso and far West Texas closed at 8 p.m. CST, an hour later than the rest of the state.
No Democrat has won a statewide race in Texas since 1994.
___
7 p.m.
The polls have closed in almost all of Texas for the nation's first primaries of 2018.
Only polling places in El Paso and far West Texas will remain open for another hour, until 8 p.m. CST. People in line at the 7 p.m. CST closing time will still be able to vote.
Democrats hope to break a losing streak in statewide races that dates back to 1994. Many Republicans say they want to show their steadfast support for President Donald Trump.
Long lines were reported in Texas' major cities and at the University of Texas in Austin, where hundreds of students waited to vote at a campus library.
Civil rights groups said they received reports of problems at eight polling places in Harris County, which encompasses Houston and some of its suburbs.
___
5:45 p.m.
A line stretching three quarters of the way around a campus library is waiting to cast last-minute ballots at the University of Texas.
Hundreds of students with books and backpacks, some eating and others with laptops open, waited Tuesday evening to cast their ballots in the nation's first primary election in 2018. Polls in most of Texas will close at 7 p.m., though polling places are required to accommodate anyone in line at that time.
Democrats saw participation in their primary surge in early voting, but turnout for the election as a whole remains to be seen.
Democrats are optimistic that backlash against President Donald Trump will lift their candidates, though the party hasn't won statewide office in Texas since 1994.
___
4 p.m.
The Texas secretary of state says the Democratic and Republican parties pay for polling locations during the state's primary elections, meaning that ballots for both don't necessarily have to be offered at all locations.
There were reports during Texas' first-in-the-nation primary Tuesday that two polling sites in Houston didn't have Democratic ballots.
Secretary of State spokesman Sam Taylor says parties sometimes opt to consolidate precincts or otherwise forgo having joint polling stations with their counterparts. That can mean that voters wanting to cast ballots for the other party in certain areas have to head to another location.
Texas has a semi-open primary, meaning voters can choose whether to vote Democratic or Republican. The problem doesn't apply to the general election since no party choice is made.
___
1:15 p.m.
A congressman who has launched a longshot bid to unseat Republican Sen. Ted Cruz is generating excitement for Democrats voting in the Texas primary election.
Voting Tuesday morning in Dallas, 64-year-old Bonnie Kobilansky said she thinks Beto O'Rourke is "a good guy" and was excited to cast her vote for him.
She says she wants to see change in the government, adding "that starts at the local level, the state level."
Twenty-seven-year-old Katie Newsome, a United Methodist pastor, says she's excited for the "freshness" and "vision" O'Rourke brings. She says she wants to see change both in the U.S. and in Texas, too.
She says she'd love to see Texas "turn blue." She says, "I don't know if that will happen but that would be exciting."
O'Rourke has generated national buzz in his challenge to Cruz. Neither faces serious primary challengers but O'Rourke recently has outraised Cruz and the senator has warned conservatives against complacency.
___
12:30 p.m.
Some Republicans voting in the Texas Republican primary say they're concerned about proposals calling for restrictions on firearms sales.
Jynelle Mikula, who voted in the GOP primary Tuesday at a Houston elementary school, says assault-style weapons and bump stocks shouldn't be sold to the public. But she's concerned the debate in the aftermath of the deadly shooting at a Florida high school could lead to the confiscation of weapons from law-abiding citizens such as herself.
Robert Coghlan, voting at the same school Tuesday, says the ongoing gun debate nationally also has him concerned. He says "we're kind of on the road to ban all guns."
Another Republican voter, Rosa Magaña, says the answer to gun violence should be educational outreach and not weapons bans.
___
11:45 a.m.
Some Houston-area voters say they've encountered problems at the polls that include one site opening more than an hour late, prompting some people to leave.
Teneshia Hudspeth with the elections division of the Harris County clerk's office said Tuesday that a last-minute change in staffing led to a delay in opening a Katy polling site.
She says local Democratic officials made a late change to the party's election judge who monitors that site.
Hudspeth says she wasn't aware of claims that two sites didn't have ballots for Democratic voters. She says some polling sites may only be for one party to vote, so a voter enrolled in an opposing party may appear expecting to vote only to be told they must go elsewhere.
___
11:15 a.m.
President Donald Trump isn't on the ballot in the Texas primary, but he's on the minds of voters.
One woman voting in Dallas says she's a "strong" Republican who supports Trump's agenda. Sixty-year-old Laura Smith said Tuesday that she backs Trump because he has "guts," isn't afraid and is a strong leader.
Smith, who works in a dentist's office, approves of the president's handling of immigration, job creation and tough approach to North Korea. She adds that she's open to new restrictions on people seeking to purchase firearms.
But Democrat Bonnie Kobilansky says she's alarmed by Trump's actions.
Kobilansky, a nurse practitioner, wants "to see a complete change in the top of the government."
She adds that she's heartened by the number of women running for office, explaining that political leaders need "common sense and practical knowledge — women have that."
___
8 a.m.
Early turnout has been light at some polling stations in Texas, which is holding the country's first midterm primary.
Texas' primary on Tuesday follows a relatively busy early-voting period.
Democratic early voting across Texas' 15 most-populous counties, the only figures available, more than doubled that of the last non-presidential cycle in 2014. Meanwhile, the number of Republican early ballots cast increased only slightly.
Total Democratic early votes exceeded Republican ones roughly 465,000 to 420,000, though those figures combined accounted for less than 9 percent of the state's total registered voters.
Polls close at 7 p.m. Tuesday, except for some far West Texas locations, such as El Paso, where they close at 8 p.m. Central Standard Time.
___
11:10 p.m.
Texas Democrats have turned out in force ahead of their state's first-in-the-nation primary election Tuesday, even though their party remains a longshot to win much.
Democratic early voting across Texas' most-populous counties was more than double that of the last non-presidential cycle in 2014.
But Democrats haven't won a statewide office in Texas since 1994, and that losing streak should continue this year.
A record six Texas Republicans and two Democrats are leaving Congress. Many of the open seats feature so many candidates from both parties that most primary races won't have anyone winning a majority of Tuesday's votes, ensuring a second round of voting May 22.
Democrats also hope to flip three other Republican congressional districts, but those races may need runoffs to decide who the party's nominee will be.