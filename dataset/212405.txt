Talk of freezes in the East and blustery rain in the West will be well in the past as volumes kick into gear for the Fourth of July.
Avocado prices remain high, and will likely remain so through July 4th, but supplies will remain healthy from California and Peru, even as Mexico’s season winds down, marketers say.
“Just kind of looking forward with what we expect supplies to be, it’s going to be fairly steady but not an overwhelming amount of fruit available,” said Robb Bertels, vice president of marketing with Oxnard, Calif.-based Mission Produce Inc. 
Volume across the U.S. will continue to trend in the low- to mid-40 million-pound a week range, Bertels said.
“I don’t see any big peaks coming,” he said. “Demand will pick up and supply doesn’t have horsepower to keep up. Expect prices to strengthen as we get closer to the Fourth.”
Avocado prices have remained “relatively high,” and likely will stay so through the holiday, said Phil Henry, president of Escondido, Calif.-based Henry Avocado Corp.
“It doesn’t seem like there will be any major changes,” he said.
Rob Wedin, vice president of sales and marketing with Santa Paula, Calif.-based avocado gower-shipper Calavo Growers Inc., said prices could rise as the holiday approaches.
“We could see a relaxation in next couple of weeks, but as we get closer to Fourth of July, we’ll see some minor strengthening of prices, which is pretty normal,” he said.
Homestead, Fla.-based Brooks Tropicals is seeing increased volumes of Florida avocados, said Bill Brindle, vice president of sales.
“This year’s crop Is sizing up heavier than the last two years,” he said.
 

Berry supplies look strong
July 4th arrives just as New Jersey’s blueberry season is peaking, so supplies should be no problem, said Bob Von Rohr, marketing and customer relations manager at Glassboro, N.J.-based Sunny Valley International Inc.
“It will be a peak week,  no problem with quantity, Von Rohr said. “It’s one of our bigger weeks for promoting blueberries, with retailers looking for red, white and blue (for displays).”
A plentiful supply of strawberries will be available to cover the “red” part, said Cindy Jewell, marketing director with Watsonville, Calif.-based berry grower-shipper California Giant Inc.
“You can expect plenty of fruit. It’s one of those years that everything’s going to be in alignment with weather, quality and flavor,” she said.
Cal Giant’s blueberry, raspberry and blackberry crops should be ready for the Fourth, as well, she said.
“Strawberries coming into peak the last two weeks in June and will stay that way through mid-July, but you’ve also got blackberries and raspberries coming in, with blueberries and cherries coming in around July 4,” she said.
 

Watermelon crop shapes up
Quality and quantity won’t be a problem in the watermelon market for the Fourth of July, said Mark Arney, executive director of the Winter Springs, Fla.-based National Watermelon Promotion Board.
“I’d say if you called me a month ago, I’d be a little more concerned because there had been some cold weather in central Florida to further north and colder into Georgia, and some guys there had to replant,” he said. “But it doesn’t appear now that it’s as bad as people thought. I was in a field last Friday in Lakeland, Fla., and things were looking really good. We tested some, and the brix was great.”
Volume should be adequate as the holiday approaches, Arney said.
“Of course, we just don’t know; weather is always a wild card,” he said.
 

Sweet corn supplies build
Sweet corn growers said they’d be ready for the holiday.
“Should be standard issue, with good supplies out of Georgia,” said Jason Bedsole, sales manager with Duda Farm Fresh Foods Inc. in Wellington, Fla.
Calvert Cullen, owner of Cheriton, Va.-based Northampton Growers Produce Sales, said his crews will be harvesting sweet corn in Moultrie, Ga., until around July 4 and was getting set to start in North Carolina.
“We expect good supplies, as long as the weather doesn’t (interfere),” he said.
Northampton also was harvesting squash, cabbage and green beans in Moultrie, as well as bell peppers, hot peppers and eggplant in North Carolina.
Gilroy, Calif.-based Uesugi Farms is expecting promotable volumes of sweet corn in time for the July 4th holiday, said Pete Aiello, general manager.
“It’s a little far out there to anticipate what kind of markets we’re going to see, but the Fourth is always a popular pull for sweet corn, obviously, so the demand will be there and we will definitely have supply,” he said.
The crop was looking good June 1, he said.
“A couple of early plantings got a little messed up due to our (wet) weather in late winter and early spring, but we did need it. Certainly, by the Fourth we’re going to have some nice fields and good harvests.”
Oak Grove, Va.-based Parker Farms will actively promote its sweet corn coming out of Georgia for the holiday and will have enough of its Virginia crop to help supplement supplies, said Sean McFadden, business development director there.
“We have a lot of corn in Georgia that we will be supporting ads with,” he said.