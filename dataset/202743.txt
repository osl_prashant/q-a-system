Michigan blueberry growers have been watching the weather closely, hoping warm days don't lead to early blooming and later losses due to freezing.
Growing areas experienced unusually high temperatures, including days in the 60s, for at least seven days leading up to Feb. 22.
Barry Winkel, a partner in Benton Harbor, Mich.-based Greg Orchards and Produce Inc., expressed some trepidation.
"We're really concerned," Winkel said. "The buds are starting to swell, especially on peaches and blueberries.
"We're all kind of on pins and needles," Winkel said.
There were reasons for optimism, however.
On Feb. 22, the warm weather was expected to move out by the end of the week, and while the days had been warm, the nights had been sufficiently cold to slow any bud growth - with the exception of the last few nights.
Winkel said growers have seen similar situations in the past and not lost much crop, and he said that if the weather cooled back down the week of Feb. 27 the buds should still be able to handle temperatures in the high teens.
That tolerance is key since the normal frost-free date for southwest Michigan is May 15, Winkel said.
Eric Crawford, president at Sunrise, Fla.-based Fresh Results LLC, said he has heard about the warm weather from multiple regions.
"Growers from Michigan all the way to south Georgia are extremely concerned about the advanced stage of the production," Crawford said.
Mark Longstroth, a Michigan State University Extension educator with a focus on blueberries, said Feb. 22 that he hadn't seen much swelling of buds so far.
"I expected to see more movement than I've seen, so that's good," Longstroth said.
Buds normally start to swell in early to mid-March, so the crop could be a few weeks ahead of schedule, but Longstroth said it is too early to tell.
Larry Ensfield, CEO of Grand Junction, Mich.-based MBG Marketing, said he doesn't expect higher temperatures to lead to crop loss.
"There has not been much movement, if any, in the bushes, and buds are remaining tight," Ensfield said in an e-mail. "We have lost our snow cover and ground frost in most of the region, but Lake Michigan remains cold and is pushing temperatures colder earlier into the night."
Steve Spiech, president of Paw Paw, Mich.-based Spiech Farms LLC, said he had seen a little bud swelling on blueberries but not much.
"It is a little concerning," Spiech said. "It could go any way ... If it stays warm and they bud, then we're sunk."
The recent conditions are markedly different from those of the warm spell of 2012 that brought on Michigan crops early and resulted in devastating losses across the state. That season had two weeks of temperatures around 80 during the days and 60 during the nights.
For the most part, temperatures during the recent warm spell have dropped low enough overnight - around 40 degrees or lower - to limit early bud growth.