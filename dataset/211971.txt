The Food and Drug Administration’s two-year sampling program for sprouts produced both enforcement actions — prompting six product recalls and helping stop an outbreak of listeriosis — and long-term insights on the prevalence of pathogens on sprout seeds and finished product. 
The FDA’s fiscal year 2014-16 Microbiological Sampling Assignment Summary Report: Sprouts collected and tested sprouts as part of a “proactive and preventive approach” to help protect food safety, according to the report. Samples were tested for salmonella, Listeria monocytogenes and E. coli O157:H7.
 
Collecting 825 samples of domestic product — virtually all fresh sprouts eaten in the U.S. are grown in the U.S. — the sampling program tested sprout seeds, fresh sprouts and spent irrigation water, according to the summary.
FDA’s sprout sampling program found:
Salmonella was found in 0.21% of the sprouts, 2.35% of seeds and 0.54% of used irrigation water;
Listeria monocytogenes was in 1.28% of the tested sprouts, similar to the percent found in seeds and used irrigation water;
No sprouts/spent irrigation water tested positive for E. coli O157:H7. Seeds weren’t tested for that strain due to limitations associated with the test method;
Samples with pathogens were from a small number of companies, with positives at eight (8.5%) of the 94 sprouting operations tested;
 
According to the summary, many firms still have progress to make in sprout safety.
 
“The fact that the agency found multiple positive samples at some of these operations underscores the need for sprouting operations to comply with the agency’s Produce Safety Regulation (published November 2015), which seeks to prevent outbreaks of foodborne illness and improve sprout safety,” according to the report.
 
During the testing process, the FDA worked with firms that owned or released the sprouts/seeds to conduct recalls or to have the product destroyed, and then followed up with inspections. 
The sampling program helped stop a listeriosis outbreak before it spread beyond a few cases, and it also prompted six recalls.
 
While no large-scale testing of sprouts is planned, the FDA will determine if more sampling is needed. Criteria include whether a firm has a history of uncorrected contamination and observations from food safety inspectors who visit the facilities, according to the report.
 
“The presence of harmful bacteria in the commodity remains a concern to the agency in view of the history of reported outbreaks associated with sprouts, including five in 2016, and the fact that this assignment resulted in six product recalls,” according to the report.