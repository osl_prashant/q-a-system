This market update is a PorkBusiness weekly column reporting trends in weaner pig.  All information contained in this update is for the week ended January 12, 2018.

Looking at hog sales in July 2018 using July 2018 futures the weaner breakeven was $78.97, down $0.82 for the week. Feed costs were down $0.61 per head. July futures decreased $0.70 compared to last week’s July futures used for the crush and historical basis is unchanged from last week. Breakeven prices are based on closing futures prices on January 12, 2018. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was below average last week with 32,072 head being reported. This is 96% of the 52-week average. Cash prices were $74.91, up $5.17 from a week ago. The low to high range was $55.00 - $80.00. Formula-priced weaners were up $3.37 this week at $47.76.
Cash-traded feeder pig reported volume was below average with 4,815 head reported. Cash feeder pig reported prices were $82.20, up $4.64 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through January 12, 2018.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential decreased $5.99 this week to a projected gain of $4.06 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.