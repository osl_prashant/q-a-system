Dole Fresh Vegetables recently settled two civil suits related to the 2015-16 listeria outbreak tied to a Dole processing facility in Springfield, Ohio.Terms of the settlements were not disclosed in court documents, and a Dole spokesman said the company had no comment to offer at this time.
An April 24 court document noted the settlement of a March 2016 suit filed against Dole by Constance Georgostathis on behalf of her mother, Angeliki “Kiki” Christofield. She contracted listeriosis after eating a Dole salad and later became comatose.
Christofield is no longer in a coma but lives with her daughter now, said attorney Bill Marler, who handled the case. Before the illness Christofield lived by herself.
In its answer to the complaint, Dole said it was not responsible for the incident, according to court documents.
Marler confirmed the case has been settled but declined to provide further information.
A March 30 court document noted the settlement of a July 2016 suit filed by David DiStefano, whose mother, Ellen DiStefano, contracted listeriosis after eating a Dole salad and later died after suffering injuries in a fall attributed to symptoms of the illness.
Dole said in court documents that it was not responsible for the incident.
Attorney Natalia Steele, who represented DiStefano in the case, declined to comment on the settlement.
Centers for Disease Control & Prevention reported 19 people in nine states were sickened in the outbreak, including one man from Michigan who died as a result of listeriosis.
Marler is representing the family of that man and is in conversation with Dole about the matter, he said.
The outbreak also affected people in Canada, according to Public Health Agency of Canada, sickening 14 individuals in five provinces.
Those people became sick between May 2015 and February 2016; the illnesses in the U.S. were also reported during that time span.
In Canada, three people who contracted listeriosis died, though the agency said in its investigation summary that it is unknown whether listeria contributed to the cause of those deaths.