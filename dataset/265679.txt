BC-USDA-Natl Sheep
BC-USDA-Natl Sheep

The Associated Press



SA—LS850San Angelo, TX    Mon Mar 26, 2018    USDA Market NewsNational Sheep Summary for Monday, March 26, 2018No test on slaughter lambs in Virginia.Slaughter Lambs:  Choice and Prime 2-3:Virginia:      no test.Slaughter Lambs:  Choice and Prime 1-2:Virginia:      no test.Slaughter Ewes:Virginia:      Good 2-4 no test.Feeder Lambs:     Medium and Large 1-2:Virginia:      no test.Replacement Ewes:  Medium and Large 1-2:Virginia:      no test.Sheep and lamb slaughter under federal inspection for the week to date9,000 compared with 9,000 last week and 11,000 for the same period lastyear.Source:  USDA Market News Service, San Angelo, TexasRebecca Sauder 325-653-1778www.ams.usda.gov/mnreports/SA—LS850.txtwww.ams.usda.gov/LSMarketNews1600 rs