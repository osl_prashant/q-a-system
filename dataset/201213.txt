Crop calls 
Corn: 2 to 4 cents higher
Soybeans: 2 to 4 cents higher
Winter wheat: 1 to 2 cents lower
Spring wheat: 15 to 20 cents higher
Spring wheat futures saw two-sided trade overnight, but early losses gave way to buying, giving bulls the upper hand heading into daytime trade. The recovery spilled into the corn and soybean markets, where traders are also concerned about the weather. While rains have benefited areas of the eastern Corn Belt this week, the western and northwestern areas of the Belt are in need of precip... and there's little to no rain in the forecast for the region the next 10 days. Winter wheat futures ended the overnight session under pressure due to a rise in farmer selling.
 
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Cattle futures are expected to see more back-and-forth price action today as traders even positions ahead of the weekend. Cash cattle trade is mostly wrapped up for the week, with trade occurring at steady to $2 lower levels. Pressure on cattle futures should be limited by the discount futures hold to the cash market. Hog futures are also expected to see a mixed start to the day, with pressure limited by expectations packers are still working on securing next week's supplies. But traders are becoming more cautious about extending long positions as they expect a seasonal downturn in the pork market to pressure the cash hog market in the near-term.