Baby boomers and their offspring propelled steady increases of sales in the organic produce category, but marketers look to millennials to drive sales even higher.All that is needed is a bit more time, marketers say.
“I think it’s a big opportunity,” Roger Pepperl, marketing director with Wenatchee, Wash.-based Stemilt Growers LLC, said of millennials’ interest in organics.
Organics have caught millennials’ interest, and that’s a start, Pepperl said.
“They seem to be very attracted to buying organic product, but their shopping frequency is a little less,” Pepperl said.
Time will change that, Pepperl said.
“Too often, I hear, millennials are 22-year-old kids are going in to buy one thing or another, then leaving. But now, they’re getting married, having kids,” he said.
That means, millennials have produce marketers’ and retailers’ attention, Pepperl said.
“The millennial population is starting to become true consumers and we have to figure out how to market to those people,” he said.
One way is to recognize younger consumers’ devotion to certain labels, Pepperl said.
“It’s very important to segment those organic items,” he said. “They’re highly brand-conscious. Their loyalty stays with the brands. Having a branded product like Stemilt is a very big advantage with the millennials.”
Millennials represent the organic category’s “greatest hope,” said Scott Mabs, CEO of Porterville, Calif.-based Homegrown Organic Farms.
“I think a lot of millennials have been raised with the influence of what organic is — just like millennials were the first kids who were raised with digital devices and iPhones, organic is a part of that.”
Millennials take to organics more readily than their elders, said Bill Hahlbohm, owner of Oceanside, Calif.-based Sundance Organics.
“Way more so than older people,” he said.
Education is the key there, Hahlbohm said.
“They learn about it in school and on their own,” he said. “The industry has done a miraculous thing, putting salad bars in schools.”
Emissaries like chef Alice Waters have been an ally to the organic category, Hahlbohm said.
“She is the most famous chef in the world. George Bush sent her to Russia to represent American cooking. She has written a lot of books, and she’s been a customer of mine for 20 years,” he said.
Waters was instrumental in putting salad bars in four Bay Area schools, and taught kids how to grow organic vegetables, Hahlbohm said.
“She taught them how to cook. Now, many have become chefs and growers,” he said.
That’s the kind of education that will help organics continue to gain followers, he said.
For now, though, millennials are mostly potential organic consumers, said Robert Schueller, spokesman for Los Angeles-based World Variety Produce, which markets under the Melissa’s brand.
“Millennials will be a strong demographic. However, it is early in the marketplace because they’re younger,” he said.
“They’re being more supported by Gen-X and baby boomers, who fueled the growth there. They will have the buying power.”
For the present, millennials seem primarily to be influencers, Schueller said.
“They tend to inspire the baby boomers and Gen-X to buy organic offerings in the marketplace there,” he said. “Their influence is very great in the marketplace as an influential factor. They’re just not the buying factor right now.”