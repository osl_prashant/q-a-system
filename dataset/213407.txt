No single company seems to drive potato sales out of the Red River Valley, but collectively the region is a strong supplier for retail and foodservice buyers.
“The tolerance for error or not delivering what you said you’d deliver is lower and lower, so you just do what you say you’re going to do and do it well,” said Eric Halverson, executive vice president of technology with Grand Forks, N.D.-based Black Gold Farms.
It also helps to be a major supplier of red potatoes, said Randy Boushey, president and CEO of East Grand Forks, Minn.-based A&L Potato Co.
“I think one of the things that has helped the red potato industry, you got some big boys in Idaho promoting more yellows and reds and it’s opened doors for markets that historically have been shut for us,” he said.
“Wal-Mart is promoting reds, and it puts it into more consumers’ hands. The consumers have acquired a taste for potatoes and enjoy the change.”
The Red River Valley has ceded the russet potato category to other growing regions, and growers in the valley focus on what they do best, said Ted Kreis, marketing director for the East Grand Forks-based Northern Plains Potato Growers Association.
“The fact that we raise just yellow and red potatoes and no russets for the fresh industry probably makes us unique,” he said.
“Skagit Valley (Wash.), as far as I know, are the only other regional pack that grow colored potatoes. They’re known for their red potatoes also. That’s been a favorable thing for us. Reds are increasing. Yellows are increasing at faster pace than reds even.”
Quality is a factor in growing foodservice and retail sales out of the Red River Valley, said Kevin Olson, salesman with Becker, Minn.-based Ben Holmes Potato.
“We do pack private-label stuff for people, and more and more people want quality. Price tends to be an issue, but you can’t send ugly spuds,” he said.
Bryan Folson, president of East Grand Forks-based Folson Farms, agreed.
“We have a nice quality potato to get to at a fair price, compared to some places,” he said.
The Red River Valley also enjoys a logistics advantage, Folson said.
“Part of that is we’re more centrally located,” he said.
“We’re not way on the West Coast and our transportation is more reasonable than, say, Washington state. That helps us.”
Potatoes are a staple, and it helps to offer retailers top-quality reds, said Dave Moquist, president of O.C. Schulz & Sons Inc. in Crystal, N.D.
“I think it seems like the retailers need to keep featuring potatoes in front of the consumer,” he said.
“People in general love potatoes, and, for us, it’s making sure at least they’re featured in (retailer) ads.”
Much of the potato volume shipped out of the Red River Valley is packed under private labels, which is a plus for retail sales but doesn’t help build recognition for the Valley as a major source of high-quality spuds, said Paul Dolan, president of Associated Potato Growers Inc. in Grand Forks.
“To be honest, in retail, we lose a lot of our recognition because we’re packing so much in private labels, and we need to figure out a way to get the word out there that they’re Red River Valley potatoes, not Wal-Mart potatoes or whoever it is,” he said.
“A lot times, they just advertise ‘red and yellow potatoes.’”