KANSAS CITY, Mo. — Dole shared smoothie samples and recipe cards at its “refueling station” at a “Cars 3” tour event May 24.Dole is a sponsor of the tour, which is scheduled to make 27 stops through June 25. The movie comes out June 16.
Lisa Cabello, who works with the special events division for Disney, said the activities of the tour, including a sneak preview of the movie and photo ops with the characters, have drawn at least 2,000 people in each city the tour has visited.
At the event, Dole offered Pineapple Pit-Stop Smoothies with pineapple, baby spinach, banana, almond milk and cinnamon. The other recipe on the card Dole handed out — Lightning Berry Smoothie — had green tea, blueberries, raspberries, banana and pineapple.
The smoothies were inspired, respectively, by “Cars 3” characters Cruz Ramirez and Lightning McQueen, according to the card.
Dole also promotes its fruit and vegetables through its Get Up and Grow tour of retailers. It stopped at about 400 stores around the country last year.
The “Cars 3” tour with Disney is a complementary approach, said Bil Goldfield, director of Dole corporate communications.
“It’s a different scale,” Goldfield said. “You’re throwing a wider net here.”
The Kansas City audience was composed almost entirely of families.
“(We’re) trying to help parents win that battle with getting kids to eat right,” Goldfield said.
Dole has been featuring Disney characters since last fall, with “Moana” characters on bananas and pineapples. “Beauty and The Beast” characters have been featured on bananas, pineapples and some leafy items.

“Cars 3” will be promoted on bananas, pineapples, cauliflower, celery, berries and several lettuce and romaine offerings through the end of September.
Those products began arriving at retailers the week of May 22, Goldfield said.
Dole can see the positive effect of the partnership with Dole in its growing digital footprint.
“We know that we’re reaching that audience,” Goldfield said.