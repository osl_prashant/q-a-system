Citrus grower-shippers in California, Texas and Florida have seen good demand for citrus.
California is mainly shipping oranges, mandarins and lemons, Texas mainly oranges and grapefruit, and Florida mainly grapefruit and juice oranges.
Marketers expected strong quality to drive California citrus sales.
Los Angeles-based The Wonderful Co. and Pasadena, Calif.-based Sun Pacific also noted the industry will have a smaller navel orange crop and a smaller lemon crop, so supplies will tighten toward the end of the season.
The U.S. Department of Agriculture estimated Jan. 12 that California will produce 35 million boxes of navel oranges, down 11% from the 2016-17 season. The department forecast lemon production to remain steady at 20.5 million boxes.
“Overall, the season has been free from any major harmful factors,” said Adam Cooper, vice president of marketing for Wonderful.
“Except for lighter crop availability in some cases, the season for all varieties has been smooth.”
In Texas, grapefruit has received a good amount of interest after Hurricane Irma significantly reduced volume from Florida. The USDA estimated Florida will produce 4.65 million boxes of grapefruit, down more than 40% from the 2016-17 season.
Trent Bishop, sales manager for Mission, Texas-based Lone Star Citrus Growers, said the company has seen regular customers doing more volume and has also fielded inquiries from companies that would be new customers. Supplying Europe will be a large part of the season, Bishop said.
Texas Citrus Mutual president Dale Murden noted the USDA lowered its Texas grapefruit estimate in its Jan. 12 report, from 5.3 million boxes to 4.1 million boxes, but he had received good reports on the crop.
“Quality has been exceptional,” Murden said, adding that recent low temperatures were not low enough to negatively affect fruit and that fruit fly pressure has not been as bad as last season.
“Nature was good to us so far this year,” Murden said.
Shipments are ahead of estimates, with about 56% of the overall crop remaining, compared to 68% at the same time in 2017.
Texas International Produce Association president Dante Galeazzi said production should stay strong through spring.
“Volume has been a little lighter this year compared to average years, but the quality has been outstanding,” Galeazzi said.
“Sizing has been heavy to larger fruit, but it’s trending back to middle sizes. The market for Texas fruit has been very strong this year and is likely to remain that way through the end of the season.”
Florida grower-shippers have had a tough go, with Irma estimated to have caused at least $760 million in losses to the citrus industry there.
GT Parris, commodity manager for Vero Beach, Fla.-based Seald Sweet, said volume was down 40% to 55% depending on grove location.
Quality has been a struggle, he said, because the winds of the hurricane beat up fruit and also weakened trees.
“Marketing Florida has been a struggle and the hardest we’ve experienced,” Parris said. “Supplying volume for big ads is difficult due to inconsistent supply.”  
Programs in Mexico and Morocco have allowed Seald Sweet to fill the gaps, and the company has been running imported fruit in its Florida packinghouse to keep jobs there despite the serious decrease in volume from the state.
For the most part, Florida is focusing on grapefruit and juice oranges, Parris said.
“They still are the best in terms of juice content and flavor, and because of this there is still a demand from loyal Florida citrus customers,” he said.
Seald Sweet is also packing honey tangerines, tangos and royal mandarins.
Oviedo, Fla.-based Duda Farm Fresh Foods also reported a strong market. John Holford, commodity manager for Florida citrus, said juice oranges have benefited from some of the recent cold nights, which help the fruit color up and bring up the sugar content.
Irma reduced volumes of all Florida citrus commodities for Duda. The company reported its orange volume is estimated to be down 29%, grapefruit down 65% and tangerines and mandarins down 80%.
Its grapefruit season, which usually goes into March, ended in early January.
Duda has an import program as well, including clementines from Morocco.