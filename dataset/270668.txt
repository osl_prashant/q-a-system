CDC expands warning in E. coli outbreak from Arizona lettuce
CDC expands warning in E. coli outbreak from Arizona lettuce

By TERRY TANGAssociated Press
The Associated Press

PHOENIX




PHOENIX (AP) — The U.S. Centers for Disease expanded its warning Friday surrounding a multistate E. coli outbreak tied to tainted romaine lettuce from Arizona, which has now sickened more than 50 people.
The agency said information from new cases of illness prompted them to caution against eating any forms of romaine lettuce that may have come from Yuma. Previously, CDC officials had only warned against chopped romaine by itself or as part of salads and salad mixes. But they are now extending the risk to heads or hearts of romaine lettuce.
People at an Alaska correctional facility recently reported feeling ill after eating from whole heads of romaine lettuce. The vegetable was traced to lettuce harvested in the Yuma region, according to the CDC.
So far, the outbreak has infected 53 people in 16 states. At least 31 have been hospitalized, including five with kidney failure. No deaths have been reported.
Symptoms of E. coli infection include diarrhea, severe stomach cramps and vomiting.
The CDC's updated advisory said consumers nationwide should not buy or eat romaine lettuce from a grocery store or restaurant unless they can get confirmation that it did not come from Yuma. Restaurants and retailers are also being warned not to serve or sell romaine lettuce from the area.
According to the Produce Marketing Association, romaine grown in coastal and Central California, Florida and central Mexico is not at risk.
Yuma is roughly 185 miles (298 kilometers) southwest of Phoenix and close to the California border. The region, referred to as the country's "winter vegetable capital," is known for its agriculture and often revels in it with events like a lettuce festival.
Steve Alameda, president of the Yuma Fresh Vegetable Association which represents local growers, said the outbreak has weighed heavily on him and other farmers.
"We want to know what happened," Alameda said. "We can't afford to lose consumer confidence. It's heartbreaking to us. We take this very personally."
Growers in Yuma typically plant romaine lettuce between September and January. During the peak of the harvest season, which runs from mid-November until the beginning of April, the Yuma region supplies most of the romaine sold in the U.S., according to Alameda. The outbreak came as the harvest of romaine was already near its end.
While Alameda has not met with anyone from the CDC, he is reviewing his own business. He is going over food safety practices and auditing operations in the farming fields.