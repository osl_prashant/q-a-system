Farmer cuts off snowmobile trail after cow shot near route
Farmer cuts off snowmobile trail after cow shot near route

The Associated Press

MADISON, Maine




MADISON, Maine (AP) — A Maine farmer who says his pregnant cow was shot and killed near a snowmobile route on his property will no longer let the public ride on the trail.
The Morning Sentinel reports that Clayton Tibbetts had permitted a local snowmobile club to maintain a route through his farm in Madison until the cow was found dead last week. Tibbetts says he doesn't know if a snowmobiler killed the animal, but the carcass was found about 30 feet away from the trail.
The secretary of the snowmobile club says the group is considering hosting a fundraiser to compensate Tibbetts for the loss of the animal.
The shooting is being investigated by the county sheriff's office and state game wardens.
___
Information from: Morning Sentinel, http://www.onlinesentinel.com/