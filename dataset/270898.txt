Participants of the Eat Brighter! campaign say the Sesame Street tie-ins continue to be popular with consumers and retailers.The Eat Brighter! campaign, a partnership between Sesame Workshop, Partnership for a Healthier America and the Produce Marketing Association, has continued to grow since launching in 2014.
Kathy Means, vice president of industry relations for PMA, said that 57 supplier companies are now licensed to use the Sesame Street characters in packaging, retailing and a number of other ways.
“We are continuing to add suppliers, and consumer feedback has been really positive,” Means said.
Behind the numbers are stories of parents trying to encourage their children to eat more fruits and vegetables, she said.
In addition to the campaign’s marketability to children familiar with the characters, Means said that participants in the program have benefited from consumer nostalgia for Sesame Street among adults and millennial moms who grew up with the classic characters.

Campaign future

Eat Brighter! is scheduled to run through 2018, and meetings between the partners are upcoming to determine a possible extension.
Companies such as Classic Harvest, a California-based citrus and grape supplier, would be glad to see the program continue, president Linda Cunningham said.
“For us, it’s been excellent and gives us national name-brand recognition. The packaging is very professional, unique and looks great in the stores.”
Cunningham said parents have reported that it was exciting to have something bright and colorful attracting their children to healthy produce.
Means said that the program has been particularly important for small and medium-sized suppliers who are often priced out of expensive character marketing.
Central American Produce, Pompano Beach, Fla., created their own imagery — working under guidance from Eat Brighter! and PMA — of Cookie Monster holding mangoes. The company used the images in stores and on their website and communications materials.
“When we first heard about the program, we were immediately intrigued,” said Shannon Barthel, marketing director for Central American Produce.
The company tested the program starting with product stickers on mangoes and saw “a notable sales lift — feedback was extremely positive,” Barthel said.
The cost of the program is also very affordable, she said.