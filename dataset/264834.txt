As of Feb. 1, 2018, the USDA’s Cattle on Feed report showed there are 11.6 million head cattle and calves on feed, 8 percent higher than Feb. 2017 levels.

The cattle herd has been growing with help from cheap feed, and Joe Vaclavik, president of Standard Grain, believes a tipping point could soon be on the way.

The next two weeks, he told U.S. Farm Report host Tyne Morgan, will be “tight” in cash cattle.

“The futures are telling you that things are going to loosen up after that, and once we get into June, July, August, we’re really going to have a much better supply base to work with and we’re going to be dealing with much lower prices,” he said.

This story isn’t anything unique. Producers have heard this before, and prices have improved.

“Is this the time where this wall of beef comes back to bite us?” said Vaclavik. “That’s the million-dollar question. The futures say yes, but it’s not guaranteed.”

Hear his full comments on the cattle market on AgDay above.