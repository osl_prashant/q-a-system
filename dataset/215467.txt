Is twine or net wrap good feed? Obviously not, but it can cause health problems if animals eat too much of it. 

Feeding hay is work. To lighten the work load feeding hay, we often take short cuts and leave some twine or net wrap on the bales. Whether we want them to or not, animals eat some of that twine.

A few years ago I shared with you information I had received from Dr. Dee Griffin, veterinarian at the Great Plains Veterinary Education Center in Clay Center, about the potential for twine to accumulate in the rumen of cattle and cause obstruction. Recent research at North Dakota State University has confirmed this risk and provided further information on what happens to twine when cattle eat it.

In a series of experiments, the North Dakota research first showed that neither plastic net wrap nor biodegradable twine get digested by rumen microbes. Sisal twine, however, does get digested, although quite a bit more slowly than hay.

In another study net wrap was included in the ration fed to steers for an extended period of time. Then, 14 days before the steers were harvested the net wrap was removed from the feed to learn if the net wrap eaten earlier might get cleared out of the rumen and digestive system. Turns out it was still in the rumen even after 14 days. 

So what should you do? First, remember that it doesn’t appear to be a health concern very often. Cows are obviously more at risk than feedlot animals. So, it might be wise to remove as much twine, especially plastic twine, as can be removed easily from bales before feeding. Twine in ground hay may be less of a problem since more of it is likely to pass completely through the animal.

Think about how shortcuts and work-reducing actions you take this winter might affect your animals. Then act accordingly.