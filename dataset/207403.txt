Two pioneer cattle feeders were inducted into the Cattle Feeders Hall of Fame during a ceremony Tuesday night in Denver.Earl Brookover, who opened Brookover Feedyard in 1951 at Garden City, KS, and Jeff Biegert, Shickley, Neb., who launched Midwest PMS – Biegert Feeds, and who maintains ownership in two Nebraska feedyards, are the latest to be recognized by their peers.
 


Earl Brookover
Jeff Biegert
 
The event also recognized two people for their careers in the cattle feeding industry.
Eulogio Dimas, Southwest Feeders, LLC, Hayes Center, Neb., received the Arturo Armendariz Distinguished Service Award, which recognizes feedyard employees who go above and beyond the call of duty to help improve the cattle-feeding industry and the beef we provide to American families.
Dee Griffin, DVM, received the Industry Leadership Award, given to distinguished individuals who have demonstrated outstanding leadership, provided exemplary service and have made significant contributions to the advancement of the cattle-feeding industry.


Eulogio Dimas
Dee Griffin
Individual profiles of the four honorees to the 2017 Cattle Feeders Hall of Fame will appear on Drovers.com in the coming days.
The founding partners of the Cattle Feeders Hall of Fame are: Merck Animal Health, Osborn & Barr and Drovers.