The amount of rain that California has experienced this  winter has opened up a window of opportunity this year for international markets, said Miles Fraser-Jones, director of global business development with Iselin, N.J.-based Seven Seas Global Produce Network, part of the Tom Lange Family of Cos.
Early finish

Early reports indicate that navel and mandarin supply in California will finish earlier than in previous years.
South African citrus shipments to the U.S. primarily remain in the East Coast and Midwest markets since the bulk of the shipments arrive at ports near Philadelphia, said Tom Cowan, sales manager with Wonderful Citrus division DNE World Fruit, Fort Pierce, Fla.
However, a few vessels are expected to ship to the Port of Houston this summer, he said.

Competitors

Chile is the biggest competitor to South African clementines and navels, Cowan said. Chile also has the advantage of shipping to three U.S. ports: Philadelphia, Miami and Long Beach.
Peru ships to both Philadelphia and Long Beach ports. Peru’s shipments are mainly clementines and minneolas, with very few navels, Cowan said.
“Peru is already in the market and Peru will continue to grow, and I believe it will be the biggest competitor,” Fraser-Jones said.
Australian citrus primarily stays on the West Coast since their imports arrive at Long Beach.
“Australia will be present on the West Coast but the domestic availability has taken market share from them,” Fraser-Jones said.
Uruguay is the newest Southern Hemisphere country to receive access to the U.S. market. Their volumes to the U.S. are currently less than those of Chile or South Africa.
“Uruguay continues to ship to the U.S. but they have had mixed quality over the past couple of years,” Fraser-Jones said.
Suhanra Conradie, CEO of Summer Citrus from South Africa, Citrusdal, said “our export volumes have grown 25% in the last five years, while demand for summer citrus from South Africa has continued to increase during the months that U.S. citrus is not grown.