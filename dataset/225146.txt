Earl’s Organic Produce has hired Jonathan Kitchens as a fruit buyer.
Kitchen most recently was produce buyer and coordinator for Good Earth Natural Foods in Marin County for the past four years, according to a news release.
His first job at the San Francisco Wholesale Produce Market was for Bay Area Organic Express Inc., aka The BOX.
Kitchens graduated from Montana State University with a degree in architecture, according to the release.
Earl’s Organic Produce is on the San Francisco Wholesale Produce Market, and carries only organic fruits and vegetables.