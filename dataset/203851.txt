The Washington apple crop is expected to be a big crop by historical standards, but industry leaders say an early harvest start, great quality and consumer driven changes to the variety mix are reasons to believe demand will be strong.
The Washington State Tree Fruit Association in early August forecast a crop of 132.9 million cartons, which is 15% over 2015's 115 million box crop, but still down 6% from the 2014 record crop of 141.8 million boxes.
 
With supplies from last season nearly cleaned up, there is no "hangover" effect from 2015 Washington apples in storage, said Jon DeVaney, president of the Washington Tree Fruit Association.
 
The 2016 crop will have larger apples than a year ago, but quality and color are expected to be very good, said Brianna Shales, communications manager for Stemilt Growers LLC, Wenathcee.
 
The mix of apple varieties is tuned to consumer tastes, industry leaders said in August.
 
Even with a big crop year, Todd Fryhover, president of the Washington Apple Commission, Wenatchee, said the volume of red delicious apples this year will total 33 million cartons, down 10 million cartons from the record 2014 crop. Golden delicious volume is also down, he said. Those older varieties are giving way to more popular varieties, he said.
 
The popular gala variety will total close to 30 million cartons, close to a record crop, High colored fuji and honeycrisp apples â€‘ also consumer favorites - also are on the increase, he said.
 
For fruit quality and color, Fryhover said the 2016 apple crop looks great so far.
 
"The manifest is wide, with plenty of big fruit and smaller fruit," he said.