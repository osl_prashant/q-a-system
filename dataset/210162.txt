The United Fresh Produce Association is offering two workshops on the Foreign Supplier Verification Program in coming weeks.
One is set for Oct. 23-24 in West Palm Beach, Fla. and the other is slated for Dec, 6-7 in San Antonio, Texas, according to a news release.
The courses will provide participants with the knowledge to implement the requirements of the Foreign Supplier Verification Programs for importers of food. The release said courses are designed for U.S. based importers and others interested in compliance with the rule.
The courses will be taught by Foreign Supplier Verification Program lead instructor Jennifer McEntire, vice president of food safety and technology for the United Fresh Produce Association.
Information and registration for the courses is available online.