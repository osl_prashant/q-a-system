Churches still have stained glass from famed artist Tiffany
Churches still have stained glass from famed artist Tiffany

By CAROL MOTSINGERThe Cincinnati Enquirer
The Associated Press

CINCINNATI




CINCINNATI (AP) — This is secret treasure.
These masterpieces are unlabeled and unexplained, yet they're still part of a collection by one of the most recognizable names in American art.
This is the Louis Comfort Tiffany exhibit that is not on display at the Taft Museum or in the Cincinnati Art Museum today.
It's in the Church of the Advent in Walnut Hills, where five Tiffany windows illuminate the sanctuary.
And there are even more at First Unitarian Church and Mount Auburn Presbyterian and Calvary Episcopal Church.
That means that technically, there is a million-dollar art cache tucked away in Cincinnati's places of worship.
Yet the windows themselves are not hidden or hushed.
They may not be identified as Tiffany creations, but they are instantly understood as special.
There's a soft grace that makes a window a Tiffany. It is almost an unseen thing. It is, instead, felt by the faithful each Sunday.
It's not unlike that unseen thing that makes a table an altar, a thought a prayer.
There are some seen things, of course, that make Tiffany windows extraordinary.
Amy Dehan can point them out.
It has partly to do with the materials, says the Cincinnati Art Museum curator. Tiffany somehow kept inventing glass. (He held three glass patents by 1880.)
His company created an unprecedented amount of varieties, too.  (His vision did not fall too far from the family tree. His father was the blue-box Tiffany. Their companies were separate entities.)
Some 200 artisans created 5,000 different colors and textures in the younger's New York studio.
They also applied these pieces in unprecedented ways.
They layered sheets to blend color or create perspective or mute light. The result of that care is that signature complexity and sensitive beauty, Dehan said.
Still, these choices were not superficial. They were always in service of the design, she said.
Tiffany ultimately aimed to tell stories using light and color.
Glass was his vocabulary and it was a language religious groups recognized.
These windows have always been didactic, using pictures to teach those who couldn't read the Bible. Stained-glass windows have illustrated Biblical lessons in churches since the 12th century, after all.
"The richness of color, the sensitivity and the beauty of the windows make the space all the more poignant," Dehan said.
Tiffany's rise — and fall — mirrors the Protestant revival in this country. (The Cincinnati churches home to his work were each built in the 19th century.)
Tiffany recognized this growth as a new business opportunity. He produced pamphlets for church leadership. He ran advertisements for these windows, too.
His message? If you want the droves of new converts to join your congregation instead of the others, you need to invest in Tiffany's brand of elite, refined beauty.
Tiffany had already made his name by decorating the lavish homes of the titans of industry — the Carnegies, the Vanderbilts, the Mellons — with his lamps and windows and decorative vases.
He even renovated the White House.
Tiffany's church pitch worked. Really worked.
We know it did even though there isn't an official count of his religious window commissions.
Only records from 1893, 1897 and 1910 exist today. But that partial list reprint fills more than 20 pages in Alastair Duncan's 1980 tome, "Tiffany Windows."
And each of those windows was expensive, even back then.
Take a window that cost $3,500 in 1900. That's the equivalent of $95,000 now. (And just one of his lamps might go for millions on the auction block today).
The Church of the Advent was once home to the type who could afford a Tiffany window.
This was during the era before Walnut Hills was Walnut Hills, actually, when Kemper Lane was known as the Walnut Farm.
The patrons of the five Tiffany glass windows there hail from names we know, our street-name families, the clans behind the commercial enterprises that crowned Cincinnati the Queen City in the mid-19th century.
The windows are in honor of a Longworth, a Stettinius, an Olmstead, a Bates and a Baldwin.
The Church of the Advent, however, began modestly.
In a Walnut Hills parlor, to be exact. A Mrs. Moffett founded the congregation at her home in 1855.
She didn't want to travel every week down that cursed hill to the nearest service in the city basin, so the story goes.
And her son was a preacher, so why should she have to?
By 1859, the church was too large for a living room. They bought a lot from Rev. James Kemper and constructed a frame church in 1859.
And expansions continued on that land for next two decades. In the 1870s, the new streetcar brought new people and more businesses to the now-neighborhood.
The church was widened and heightened for the last time in 1884.
These are the walls where the Tiffany windows now stand.
Three daughters erected the first Tiffany window there in honor of their parents, Samuel R. Bates and Hannah Grandin Bates, just six years after the church expansion.
It depicts the Angel of Purity draped in scarlet robes, flanked by pink blossoms and lush leaves.
The Church of the Advent was still the only Episcopal church on the hill then, says Barbara Haven, a church archivist who has attended services there since the 1960s.
So it was the closest church to those Longsworths and Bateses and their other wealthy neighbors in what we now call Hyde Park.
These elite families were also drawn to the church because of its beloved leader, Haven noted.
Rev. Peter Tinsley was rector from 1869 to 1902.
He came to Walnut Hills after serving as a chaplain in the Confederate Army. He was at Gettysburg, and he was at Appomattox for the surrender.
Tinsley's service continued in Cincinnati.
He rode horseback from house to house to spread the Good Word. He'd return later with hymnals and prayer books.
After his 1908 death, the Taylor family erected the Nativity window behind the altar.
During a baptism recently, JoAnn Morse's gaze wandered up toward the trio of Tiffany windows overhead.
Morse, another church archivist, stood next to a young couple and their infant, the newest member of the Church of the Advent.
But Morse's mind visited the past members, the ones memorialized in those 100-year-old Tiffany pieces showcasing the Holy Family.
This baptized child is the first here in a while.
Church attendance there has been in decline for 100 years. First, an Episcopal church opened in Hyde Park so many Church of the Advent's prominent families began to stay closer to home for Sunday service in the 20th century.
Later, others just left the church, any church, altogether.
America's church-building slowed everywhere then.
Yet four of the Church of the Advent windows were installed in the 1920s.
By then, Tiffany was in his 70s. He rarely supervised the window construction those days.
In 1924, he shut off his studio furnaces. For the next seven years, artists used old, leftover pieces of glass for new works.
That makes the 1920 Church of the Advent windows some of the last of their kind.
By 1932, the glass company filed for bankruptcy. The next year, Louis Comfort Tiffany died.
In the decades since, many of Tiffany's church windows have been lost as 19th-century churches close around the country.
His radiant works, again highly valued on the art market, have been sold to alleviate church debt.
The two windows on display with the Cincinnati Art Museum were actually saved.
Originally installed at the St. Michael and All Angels Church in Avondale, leadership gifted them to the permanent collection when the church was turned into a community center.
Others haven't been as lucky. A few have been stolen or destroyed during demolition.
And they can never be replaced.
So with each year, Cincinnati's church window Tiffany collection becomes rarer and rarer.
JoAnn Morse can only think of the future when she examines her favorite Church of the Advent window.
In the corner of the sanctuary, lilies flank a glorious angel. The sun rises behind the angel's crown, her glowing wings, her flowing robes.
She is the Angel of Resurrection.
On these three panels, it is always Easter. It is always a new day.
This glass guardian leads Morse, she says. The window is a reminder of her responsibility after she leaves service every Sunday morning.
The window is, in fact, illumination.
It tells her that whatever she owns, the material and the immaterial, is to be given away.
The window itself, she explains, was the most generous gift given in honor of a sister loved and lost in 1920.
Her name — Alice Gradin Bates — appears in the panels below the angel's feet.
There's another name in the right corner.
It's much harder to see.
It is not even half the size of the other block text.
It appears to be written by hand.
"Louis C. Tiffany."
___
Online: https://cin.ci/2pRit5C
___
Information from: The Cincinnati Enquirer, http://www.enquirer.com