There is a lot of discussion lately about grain storage and quality. Now, you can tap into a four-part webinar series focused on stored grain IPM in the north-central region. Here are the topics and links to the YouTube videos.Part 1: pest insect biology and identification, and basics of IPM
Part 2: pest prevention
Part 3: IPM for stored corn
Part 4: fumigation-decisive control, when needed
Featured speakers include entomology experts like Dr. Tom Phillips (Kansas State), Linda Mason (Purdue), and Frank Arthur (USDA-ARS). I encourage you to take some time and watch the videos to better understand stored pest IPM. It's afree webinar series!
Granary weevil. Photo by Clemson University, www.ipmimages.org.
Indian meal moth adult and larvae. Photo by Clemson University, www.ipmimages.org.