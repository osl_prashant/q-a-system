The year is off to a good start for avocado lovers, growers and importers, and, if last year is any indication, fall could be even better.
From January through May, more than 1 billion pounds of avocados were sold in the U.S. That's up from 883 million pounds for the same period last year, according to the Hass Avocado Board, Mission Viejo, Calif.
In all, volume rose 16% in 2015 compared to 2014, but for fall, volume grew 21% - from 575 million pounds for September through December 2014 to 696 million pounds for the fall of 2015.
The increase was driven by an increase in supplies from Mexico, which had its biggest year ever, said Emiliano Escobedo, HAB executive director.
Mexico was by far the largest source of avocados in 2015, shipping 1.7 billion pounds to the U.S.
California was next, with 262 million pounds, followed by Peru with 100 million and Chile with 20 million pounds. Other countries shipped almost 4 million pounds.
"Consumption is increasing in the U.S. at a very fast pace," Escobedo said. "Almost every year we are breaking records."
Total volume in the U.S. for 2015 was 2.14 billion pounds, an increase of 15% over the 1.85 billion pounds in 2014.
The increase was the result of higher demand, increased consumption and new plantings in various parts of the world, he said.
At the same time, prices are dropping.
From January to May, the average retail price of an avocado in the U.S. dropped to 91 cents, down from $1.03 in the same period in 2015, Escobedo said.
Prices were lowest in the West and Southeast.
In late July, the U.S. Department of Agriculture was reporting the following f.o.b. prices on two-layer cartons of avocados:
Mexico - mostly $58.25-60.25 for sizes 40s and 48s
Peru (through Philadelphia) - $50-52 for size 40s and $46-50 for size 48s
California - mostly $60.25-62.25 for size 40s and $57.25-59.25 for size 48s
Florida - $6.50-8.50 for all sizes
Fruit from most growing areas was smaller than usual this summer, said Robb Bertels, vice president of sales and marketing for Mission Produce Inc., Oxnard, Calif.
An El NiÃ±o condition resulted in a hotter-than-usual growing season in Peru, which prevented the fruit from sizing up, he said, and California had a large crop, so there was more fruit on the trees, which typically keeps sizing in check.
Rob Wedin, vice president of sales and marketing for Calavo Growers Inc., Santa Paula, Calif., said a late rain in Mexico and lack of rain in California help stifle fruit size from both those areas.
As a result, demand for size 48s and larger has been very high this summer, he said.
Some California fruit should be available through August. Then the fall crop from Mexico starts ramping up, Bertels said.
Marketing boards from California, Mexico, Peru and Chile have done a good job promoting avocados and have helped boost consumption and demand, said Gary Caloroso, marketing director for Giumarra Agricom International, Escondido, Calif.
The Hass Avocado board also has helped spark avocado sales through its research projects, he said.
"As those research studies come out (showing) the nutritional benefits of avocados, that's a real help to the industry," he said.
In Florida, where Homestead-based Brooks Tropicals Inc. grows its green skin Slimcados, "everything looks good," Mary Ostlund, director of marketing, said as the first month of harvesting kicked off in July.
The season started about a month later than usual because of a wet winter, she said, and fruit was a bit smaller than usual.
New Limeco LLC, Princeton, Fla., also started shipping green skins in July, and early response was positive, said Eddie Caram, general manager.
"There is good demand for Florida avocados," he said, especially throughout the East Coast.
"It's a bigger, different piece of fruit" than the more prevalent hass variety, he said.
They should be available until March.