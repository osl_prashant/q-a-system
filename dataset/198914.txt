A U.S. national security panel has cleared ChemChina's $43 billion takeover of Swiss pesticides and seeds group Syngenta, the companies said, boosting chances that the largest foreign acquisition ever by a Chinese company will go through.
The decision removes significant uncertainty over the takeover of the world's largest pesticides maker after the two companies agreed a deal in February.

Syngenta did not disclose whether it had made concessions to secure approval but indicated that any such steps would not have a significant impact on its business.

Syngenta shares jumped 11.2 percent by 0900 GMT to 423.4 Swiss francs ($439.7). ChemChina's $465 per share cash offer values the company at around 448 francs at current exchange rates plus a special five-franc dividend.

Kepler Cheuvreux analyst Christian Faitz called the step a "major milestone for the deal", adding in a note to clients that

"approval removes a major potential hurdle and should come as a relief to Syngenta shareholders". Kepler Cheuvreux rates Syngenta shares a "Buy". Reuters reported earlier on Monday that the acquisition was in the final stages of being cleared by a U.S. panel that scrutinizes deals for national security implications.

"China National Chemical Corporation (ChemChina) and Syngenta today announced that the companies have received clearance on their proposed transaction from the Committee on Foreign Investment in the United States(CFIUS)," a joint statement released by Syngenta said.

The statement made no mention of any concessions required to win clearance.

"We are not disclosing the details of the agreement with CFIUS to respect the confidentiality of the process," a Syngenta spokesman said by email in response to a Reuters query. "Any mitigation measures are not material to Syngenta's business."

Syngenta reiterated that is expected the deal to be finalised by the end of the year.

It said closing the transaction was subject to "anti-trust review by numerous regulators around the world and other customary closing conditions. Both companies are working closely with the regulatory agencies involved and discussions remain constructive."

RIPPLES ACROSS SECTOR

The CFIUS review is being watched closely by Monsanto Co, the world's largest seed company, which is deliberating whether it should sell itself to Germany's Bayer AG. Syngenta last year turned down offers to be acquired by Monsanto.

The deal comes as China looks to secure food supplies for its population.

Syngenta is a key player in the market for pesticides and seeds. It has facilities in North Carolina, as well a presence in California, Delaware, Iowa and Minnesota among other states.

Syngenta's share price has significantly lagged ChemChina's offer amid concerns that the deal would get through CFIUS. Syngenta derives about a quarter of its sales from North America.

Several U.S. lawmakers wrote to Treasury Secretary Jacob Lew this year asking for CFIUS to subject the deal to additional scrutiny over its impact on domestic food security. The U.S. Department of Agriculture also joined the CFIUS review, Reuters previously reported.

Syngenta had said this year it would make a voluntary filing with CFIUS "even though no obvious national security concerns were identified during due diligence".

With a growing number of Chinese companies looking to acquire U.S. peers, CFIUS had emerged as a significant risk for such deals, particularly those with potential cyber security implications.

For example, in February, state-backed Chinese firm Unisplendor Corp scrapped a $3.78 billion investment in Western Digital Corp after CFIUS said it would investigate the transaction.