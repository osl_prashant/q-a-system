Crop calls
Corn: 1 to 2 cents higher
Soybeans: 6 to 8 cents higher
Wheat: 1 to 4 cents higher
Soybean futures were the upside leader overnight due to short-covering. Key this morning will be if traders view early gains as a selling opportunity or if traders continue to cover short positions given its oversold condition. Traders are also keeping an eye on the weather in Argentina, as forecasters are warning flooding could be seen over the weekend in Buenos Aires. The U.S. dollar index saw two-sided trade overnight.
 
Livestock calls
Cattle: Lower
Hogs: Lower
Cash cattle trade picked up in Nebraska at $120 yesterday, which is $4 to $8 below where it traded last week and signals lower trade is likely in Kansas and Texas. This also narrows the discount nearby futures hold to cash. Meanwhile, hog futures are called lower in reaction to a deteriorating cash hog market. Pressure should be limited by futures moving into oversold territory according to the nine-day Relative Strength Index.