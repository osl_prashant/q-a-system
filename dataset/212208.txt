CHICAGO — Sure enough, the news that greenhouse giant Village Farms plans to expand into growing medical cannabis in British Columbia raised a lot of eyebrows and questions. 
I had a chance to talk with Village Farms CEO Michael DeGiglio at United Fresh’s annual conference to get a better perspective on the company’s business decision to go this direction.
This is certainly not a decision taken lightly.
 
Village Farms entered a 50/50 agreement with Emerald Health Therapeutics to grow 25 acres of greenhouse cannabis in Delta, British Columbia, estimated to yield more than 75,000 kilograms (165,000 pounds) of product per year upon completion of full licensing and greenhouse conversion, which is expected in late 2018.
 
DeGiglio said June 14 that the cannabis production won’t come at the expense of Village Farms’ established tomato and cucumber production, grown in 240 acres of greenhouses.
We weren’t able to speak to DeGiglio last week for the story, so we relied on comments from a conference call and news release.
 
He said in Chicago that there are a few areas he’d like to clear up.
 
First, medical marijuana is legal in Canada, but it’s not legal recreationally. Second, it’s not federally legal in the U.S., although it is legal in a few states for recreational use, so Village Farms has no intention of entering that market.
 
He said Village Farms sees a dynamic change coming in the greenhouse industry, as production and distribution is becoming more localized in North America. Nearly three-quarters of Canadian greenhouse vegetables are exported to the U.S., and nearly all of Mexico’s are sent to U.S. buyers. Growth in vegetables is slowing, and the company needs to grow, he said.
 
“It doesn’t make sense to build more greenhouse (vegetable) production in Canada,” he said.
 
DeGiglio also points out that the company plans to grow cannabis indica, which is low in THC and used for medical purposes, not cannabis sativa, which is high in THC and more often used for recreational use. In other words, getting high.
 
The word marijuana is used for both, but they are distinct plants, he said.
 
In the week since the announcement, DeGiglio has heard positives and negatives from customers, though he wouldn’t get into details.
 
Is this a good business move? It’s not my place or The Packer’s to say. We will go back to our guiding principle: The market will decide.
 
The Packer’s coverage used the term “joint venture” for the agreement, which was also used by Village Farms in its announcement because it’s a clever double entendre.
 
But I’m no Seth Rogen. I’m not about to make a career on tired pot jokes.
 
This is a serious business move, and one many of us knew was inevitable by fruit and vegetable growers because they are entrepreneurs and the best farmers in the world. And marijuana has proven so far to be very profitable, whether legal, semi-legal or flat illegal.
 
Why not a produce company?
 
Want the rest of the story? Find it here: Joint venture: Village Farms plans to grow marijuana
 
Greg Johnson is The Packer’s editor. E-mail him at gjohnson@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.