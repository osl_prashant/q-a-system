Zoetis has a new, Food and Drug Administration (FDA)-approved label claim for SPECTRAMAST LC(ceftiofur hydrochloride) Sterile Suspension for treatment of diagnosed subclinical mastitis infections.Already approved for the treatment of clinical mastitis infections caused by a broad spectrum of pathogens, SPECTRAMAST LC now can help treat diagnosed subclinical mastitis infections before lactating cows exhibit physical symptoms of the disease.
Subclinical infections compromise milk quality, rob profitsLeft untreated, subclinical mastitis infections can be extremely costly to a herd, in terms of:
Lost milk production throughout lactation1
Increased risk of clinical mastitis2-5
Lost milk-quality premiums
Increased treatment costs
Risk of mastitis recurrence
Cost of early culls from the herds1
Reduced reproductive efficiency2-5
New data demonstrates the high cost of subclinical mastitis. The study examined 164,423 individual Holstein cow lactation records for the presence of high first-test somatic cell counts or recorded clinical mastitis cases in the first 60 days of lactation. These records, based on 22 herds in the western United States, revealed that cases of subclinical mastitis translated into:
1,583 pounds of lost milk production per cow through 210 days of lactation, for a cost of $285 in lost milk yield per cow (based on $18/cwt)6
2.5 times greater likelihood that the infected animal would develop a case of clinical mastitis by 60 days in milk6
Three times greater likelihood that cows with subclinical mastitis infections would be culled in the first 60 days of lactation6
17 additional days open (not pregnant)6
Add to these factors the additional cost of milk production losses and treatment of cases that become clinical, and it is clear that subclinical infections are a silent but significant threat to a dairy’s profitability.
Committed to offering on-label solutions for mastitis treatment successZoetis understands the changing needs of today’s dairy industry, and it is committed to providing flexible, comprehensive on-label solutions.
“This label extension is a continuation of Zoetis’ commitment to providing the industry with comprehensive mastitis management solutions. We’ll continue to invest in new, practical on-label solutions and support research that advances mastitis management protocols so veterinarians and producers can use them with confidence. This gives producers peace of mind, because they know they are making the best decision for their dairy.
For more than 30 years, Zoetis has been delivering innovations to help producers and veterinarians manage mastitis and produce quality milk.
Early treatment with SPECTRAMAST LC pre-empts mastitis infectionsCows with a high somatic cell count — in excess of 200,000 cells/mL — may indicate a subclinical mastitis infection. Coagulase-negative Staphylococci (CNS) and Streptococci are two major bacteria responsible for subclinical infections.7,8 SPECTRAMAST LC is approved for the treatment of diagnosed subclinical mastitis caused by both of these organisms. SPECTRAMAST LC also offers a convenient once-per-day dosing.
Treating mastitis cases at the subclinical level improves the likelihood that infections will resolve before they become clinical, thus minimizing milk production losses and quality premium reductions, Kirkpatrick said. SPECTRAMAST LC has a unique, flexible label that allows for treatment for two days, or up to eight days to achieve a full bacteriological cure.
In Zoetis field studies involving more than 700 cows, those with subclinical mastitis infections treated with SPECTRAMAST LC achieved bacteriological and clinical cure rates of 44%, compared with 4% for untreated controls.9 The results also showed reduced somatic cell counts at 15 days after treatment.
Developing treatment protocols for subclinical mastitisMonitoring individual cow somatic cell counts through monthly Dairy Herd Improvement (DHI) testing is an important tool for tracking early lactation udder health. I advise monitoring DHI or milk conductivity records to identify cows with somatic cell counts greater than 200,000 cells/mL. I also recommend using the California Mastitis Test (CMT) to evaluate the quarters of cows with high somatic cell counts and collecting milk samples from each infected quarter to help identify the mastitis-causing pathogen.
Work with your herd veterinarian to establish on-label treatment protocols based on common pathogens on your dairy operation to provide the best chance for a complete bacteriological cure. 
SPECTRAMAST LC is the only mastitis treatment for lactating cows that is backed by the Residue Free Guarantee.TM If you use SPECTRAMAST LC according to its label indications and observe its milk and meat withdrawal times, Zoetis guarantees you will not experience a violative residue. Visit AvoidResidues.com for more information on the Residue Free Guarantee.
Learn more about SPECTRAMAST LC at DairyWellness.com or by contacting your veterinarian or Zoetis representative.
 1Hertl JA, Schukken YH, Bar D, et al. The effect of recurrent episodes of clinical mastitis caused by gram-positive and gram-negative bacteria and other organisms on mortality and culling in Holstein dairy cows. J Dairy Sci 2011;94(10):4863-4877.
2 Chebel R. Mastitis effects on reproduction, in Proceedings. NMC Regional Meeting 2007;43-55.
3 Hertl JA, Gröhn YT, Leach JD, et al. Effects of clinical mastitis caused by gram-positive and gram-negative bacteria and other organisms on the probability of conception in New York State Holstein dairy cows. J Dairy Sci 2010;93(4):1551-1560.
4 Hertl JA, Schukken YH, Welcome FL, Tauer LW, Gröhn YT. Effects of pathogen-specific clinical mastitis on probability of conception in Holstein dairy cows. J Dairy Sci 2014;97(11):6942-6954.
5 Lavon Y, Ezra E, Leitner G, Wolfenson D. Association of conception rate with pattern and level of somatic cell count elevation relative to time of insemination in dairy cows. J Dairy Sci 2011;94(9):4538-4545.
6 Kirkpatrick MA, Olson JD. Somatic Cell Counts at First Test: More than a Number, in Proceedings. NMC Annu Meet 2015;53-56.
7 Pantoja, JCF, et al. Dynamics of somatic cell counts and intramammary infections across the dry period. Prev Vet Med 2009:90(1-2):43-54.
8 Taponen S, Pyörälä S. Coagulase-negative staphylococci as cause of bovine mastitis—Not so different from Staphylococcus aureus? Vet Microbiol 2009:134(1–2):29-36.
9 Data on file, Study Report No. A131C-US-12-127, Zoetis Inc.