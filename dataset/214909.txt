Blogger Gluten-Free Palate created a Nacho Potato Power Bowl as part of Side Delights Steamables' #PotatoPowerBowl recipe challenge.
Fresh Solutions Network’s latest recipe challenge for its Side Delights Steamables was Potato Power Bowls.
The company asked bloggers to create one-bowl meals with any form of potatoes as the base ingredient, with layers of protein, vegetables and whatever else they wanted to include, according to a news release.
The challenge, promoted with the hashtag #PotatoPowerBowl, played off 2017’s popular food trend of bowl-based meals, according to the release.
Top recipes included a Savory Potato Breakfast Bowl, a Twice Baked Potato Bowl, and a Nacho Potato Power Bowl, according to the release.
Blogger recipes linked back to the Side Delights website and social media posts provided additional coverage, according to the release.
“Side Delights Steamables are triple-washed, microwaveable in the bag and ready to eat in 8 minutes — which is exactly the convenience that millennials and other busy shoppers are seeking,” Kathleen Triou, president and CEO of Fresh Solutions Network LLC, said in the release.
“This program leverages a popular trend to show food bloggers, home cooks and food influencers one more way to enjoy Side Delights Steamables, and encourages them to share their recipes through social media channels, driving shoppers into the store.”