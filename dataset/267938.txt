BC-Merc Early
BC-Merc Early

The Associated Press

CHICAGO




CHICAGO (AP) — Early trading on the Chicago Mercantile Exchange Mon:
Open  High  Low  Last   Chg.  CATTLE                                  40,000 lbs.; cents per lb.                Apr      112.97 113.97 112.85 113.12   +.90Jun      103.02 104.60 102.80 103.55  +1.23Aug      103.40 104.75 103.25 103.80  +1.03Oct      107.25 108.47 107.10 107.70  +1.00Dec      111.75 112.67 111.55 111.90   +.75Feb      112.92 113.90 112.92 113.42   +.87Apr      114.20 114.62 114.17 114.17   +.85Jun      108.80 108.90 108.57 108.80  +1.15Est. sales 17,875.  Fri.'s sales 66,824  Fri.'s open int 348,284                 FEEDER CATTLE                        50,000 lbs.; cents per lb.                Apr      136.00 138.02 135.65 136.30   +.98May      136.20 138.57 135.95 136.77  +1.15Aug      142.00 144.12 141.90 143.00  +1.55Sep      143.00 144.75 143.00 143.55  +1.25Oct      142.62 145.12 142.62 143.70  +1.08Nov      143.12 145.00 143.12 143.57   +.97Jan      140.90 140.97 139.67 140.25  +1.40Est. sales 5,131.  Fri.'s sales 21,070   Fri.'s open int 49,758                  HOGS,LEAN                                     40,000 lbs.; cents per lb.                Apr       52.25  52.72  52.00  52.35   +.23May       64.47  65.17  64.47  65.00   +.53Jun       72.97  73.50  72.70  73.32   +.05Jul       75.20  75.77  75.05  75.60   +.15Aug       75.72  76.67  75.72  76.50   +.35Oct       64.35  64.92  64.30  64.77   +.22Dec       59.07  59.60  59.05  59.50   +.30Feb       63.17  63.20  63.15  63.15   +.03Est. sales 7,615.  Fri.'s sales 47,559   Fri.'s open int 241,818,  up 1,470      PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.