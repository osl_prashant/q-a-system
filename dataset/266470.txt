Missouri Senate changes foster care rules after boy's death
Missouri Senate changes foster care rules after boy's death

The Associated Press

JEFFERSON CITY, Mo.




JEFFERSON CITY, Mo. (AP) — The Missouri Senate has approved a measure to make it easier for state agencies to share information about potential child abuse.
The measure, passed Thursday in a 31-0 vote, was partially in response to the death of Adrian Jones, a Kansas boy killed by his father and stepmother in 2015 and whose body was fed to pigs. Jones' family moved between Kansas and Missouri, and lawmakers said increased communication between states, as well as agencies within the states, could help spot similar abuse.
The proposal makes several other changes to the foster care system, including extending the amount of time the Department of Social Services is required to retain abuse records.
The bill now heads to the House.
___
The bill is SB 850