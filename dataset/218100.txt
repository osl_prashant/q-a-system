The U.S. Department of Agriculture has lowered it's milk price projection for 2018 by $0.80/cwt in the latest World Agricultural Supply and Demand Estimates.
The milk production estimate for 2017 is reduced on recent data. For 2018, the milk production estimate is reduced on slower anticipated growth in the dairy cow herd combined with continued slow growth in milk per cow.
Fat basis imports for 2017 are reduced on slower butter imports, but exports are raised on solid global demand for U.S. butter and other dairy products.
Skim-solids basis imports are reduced modestly while exports are raised on strong demand for skim milk powder and several other products.
For 2018, the fat basis import forecast is reduced on slowing demand for butter products, while the export forecast is raised on expected robust foreign demand for U.S. fat-containing products.
On a skim-solids basis, the 2018 import forecast is reduced on weak demand for U.S. milk protein concentrates. The 2018 skim-solids basis export forecast is raised reflecting stronger demand for a number of products.
Dairy product prices for 2017 are adjusted for December data. For 2018, all dairy product prices are reduced on slowing domestic demand and global competition.
The Class III and Class IV price forecasts for 2018 are reduced on lower product prices. The all milk price is lowered to $15.80 to $16.60 per cwt for 2018.