Potato marketers must help retailers close the sale, one retail expert believes.
 
America is facing a new era of unprecedented change that is putting new pressures on retailers, said John Rand, senior vice president of retail insights for Kantar Retail, Boston.
 
Rand spoke at a Jan. 5 breakout session on the fresh market at the 2017 Potato Expo in San Francisco.
 
Retailers and marketers need better signage, better fixtures and new ways to speak to shoppers at the point of decision, Rand said. 
 
"Static signs are good but I think we need to have more holistic conversations (with consumers) about where products come from, what their purpose is, how to use it," he said. 
 
Retail changes
 
Grocery stores are adjusting to big changes in how they relate to consumers, he said.
 
Consumers surveyed last year indicated that the average number of retailers they visited during the previous four-week period declined from 12.4 in 2007 to 9.4 in 2016. 
 
However, millennial consumers, Hispanics and households with kids added retailer visits compared with 2015, according to Kantar.
 
Grocery stores such as HEB and Kroger have more than 70% of their sales in groceries, while Wal-Mart has about 44% of its sales in groceries and Target just has a little more than 20% of their sales in groceries, Rand said.
 
Only about 2% of Amazon's sales are food, but they are working to increase that total.
 
Rand said the consumer of today is far different from the 1950s housewife. "Chances are our shoppers are not the traditional mom at home, wearing an apron," he said. "(America) hasn't done that for 40 years, but we are still marketing to that person sometimes, and that's a mistake,"
 
Guys make about 35% of grocery store trips, and about half the people cooking meals are guys, he said.
 
"Everything we thought we knew about the world we knew is going through some kind of change," he said. 
 
From what people earn, what they believe, where they shop and how they get information, everything is changing, Rand said. For the first time in history, five purchasing generations are alive and well.
 
"It's a very different world out there, but a lot of grocery stores open for business are looking just as they did in 1968," he said. "That's a problem."
 
More consumers are moving to metropolitan areas, and the retail sites available in cities are tighter for grocery stores. That creates a dense population, full of consumers who want a lot of things at the same time, he said. Those shoppers also lack knowledge of farming.
 
"You have an awful lot of people who have never seen anything grow, and they have no idea how potatoes came into existence," he said.
 
Because of that, Rand said marketers have to tell the story of potatoes to consumers who have never understood anything about potatoes.
 
What's more, the space in smaller stores to build displays is limited. E-commerce also is beginning to disrupt food sales, he said.
 
Going forward, brick and mortar retailers will put a greater emphasis in the store environment on the sensory experience, such as the texture, taste, smell, color and appearance of fresh produce.
 
Retailer differentiation
 
Most retailers are now trying to differentiate themselves in some way. Much more than center store categories, fresh produce and other perishables are an important way they can stand out, he said.
 
For example, Albertson's is setting up regional buying offices to respond to local needs.
 
"Having more buying offices is complex and difficult, but it is the only way they can stay local," he said. 
 
Supercenters and mass market retailers will suffer in the years ahead, Rand said. "The Wal-Mart era, at least as we know it, is gone," he said. While Wal-Mart won't go away, Rand said they will focus their expansion on neighborhood grocery stores and e-commerce.
 
Retailers that could grow store count significantly include Aldi, Lidl and dollar stores, he said.
 
Grocery stores are competing for the same dollar with club stores, drug stores, supercenters, e-commerce and discounters, he said.
 
"So when we think about (potato) product lines, we have to get away from thinking about the big pile of 10-pound bags that worked really well in grocery stores and think what belongs at CVS," he said. 
 
Rand said marketers should consider how to serve retailers who are trying to create their own niche.
 
Marketers must work with retailers on a message to consumers that makes them want to come back to retailer's store, to cultivate loyalty. Retailers want products that speak to their shoppers, Rand said.
 
"Retailers aren't in the business of selling potatoes, they are in the business of generating more shoppers trips," he said.
 
Local appeal
 
Local relevance will be a big point of difference in coming years, he said. The ability of stores to say that a produce item came from a local farmer is powerful and helps support the retailer's brand image, he said. 
 
One Kantar survey last year found that 21% of consumers who say a regional grocer is their primary store say they have purchased local food there. That compares with 12% of shoppers who purchased local food at a primary grocery store that is part of a national chain.
 
Transparency and authenticity also matter, Rand said.
 
The attributes of fresh, local, organic, natural and healthy food items will characterize between 25% and 50% of all products that enter the market in the next few years, and potatoes can fit in that category, he said.
 
Nearly seven in 10 shoppers believe fresh is very important and 17% of consumers prefer organic or natural products, he said.
 
Rand said potatoes are one of the produce category's most magical products, with a powerful story to tell. 
 
"You have a great message here, but I'm not sure it is being heard," he said. Potato marketers are competing with other food marketers who are selling less healthy products, but who are spending more time in telling consumers about them.
 
Rand said successful marketing campaigns create an identity, differentiation, a visual reference and focus on the essence of the brand promise.
 
"It is not enough for produce to show up, it needs to talk," he said. "Retailers need you to close the sale - they can't do it themselves."