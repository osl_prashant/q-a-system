Fresh goji berries will be offered by Morro Bay, Calif.-based Shanley Farms beginning in May. 
Dried goji berries are known as a superfood in the U.S., but owner Jim Shanley said he believes the firm is the only one in the U.S. growing and marketing fresh goji berries.
 
Shanley said the stems are left on to prevent damage to the fruit when harvested. Last year, the company harvested berries from about 400 plants on a half acre last year, selling to Whole Foods and other high-end customers. About 12 additional acres were planted last year, and now the firm has 12.5 acres of organic goji berries, he said.
 
The goji berries are blooming right now, with harvest expected to begin in May and spill into June. An “echo” occurs in the fall. Shanley said the bushes planted last year are expected to have better supplies in the fall compared with this spring.
 
Total volume this year might be close to 40 to 50 pallets, he said. The fresh berries are sold in four-ounce clamshells, he said.
 
The goji berry may be slightly more fragile than blueberries or raspberries, Shanley said.
 
Last year, regular retail pricing of the goji berry was mostly $7.99 to $8.99 per 4-ounce carton. Wholesale pricing last year was at $5 per 4-ounce clamshell, Shanley said.
 
Megan Shanley, marketing manager for Shanley Farms — also marketers of avocados, passion fruit and finger limes — said last year’s harvest allowed the firm to learn how to pick the berry to maximize its shelf life.