BC-Wheat KX
BC-Wheat KX

The Associated Press

CHICAGO




CHICAGO (AP) — Winter Wheat futures on the Chicago Board of Trade Mon:
Open  High  Low  Settle   Chg.WHEAT                                   5,000 bu minimum; cents per bushel    May      514¼  528¾  511    522¾  +16  Jul      533    547¾  529¾  541¾  +16¼Sep      551    565¾  548½  560¼  +16  Dec      574¼  587¾  570¼  582¾  +15¾Mar      587¾  600¼  584½  596    +15¼May      590    606¼  590    603    +13¼Jul      596¾  609    594    607    +12  Sep      610¼  620    609    615    +11  Dec      615    636    615    627½  +11¼Mar                           633    +11¼May                           633    +11¼Jul      620    625    620    625    +12  Est. sales 77,969.  Fri.'s sales 68,804  Fri.'s open int 281,082