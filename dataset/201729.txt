Resource Categories RevisitedPart 2 will cover the remaining 3 categories of the ranch inventory. Financial, human and physical resources. As mentioned inPart 1, a ranch inventory should include 4 categories of resources available to the ranch operation:
Natural.
Financial.
Human.
Physical.
Financial Inventory
A financial inventory should include:
Any cash, checking and savings accounts used by the ranch.
Any brokerage accounts utilized by the ranch.
Any debts owed by the ranch to banks or other lenders along with the amounts owed, interest rates, and time remaining on loans.
Any lease agreements for land or livestock.
Any legal or trust agreements should also be included so all members of the ranch operation are on the same page.
Knowing the financial condition of the ranch is critical when negotiation new operating notes, new loans for diversification, or possibly making room for a son or daughter returning to the ranch.
Several computer programs, smartphone apps, and paper record booklets are available to complete a financial inventory of the ranch. No matter which method or technology is used, a financial inventory is critical when determining how well positioned the ranch is to manage risk.
Financial Resources
A thorough financial inventory should allow a ranch manager to complete a balance sheet, income statement, and cash flow projection for the ranch. Please refer toImportance of Keeping Records for Filing Taxes on the Ranch/FarmandFinancial Record Keeping Software: A comparison of Quicken and QuickBooksfor resources available for organizing financial records. Please refer Iowa State University Extension'sFarm Financial Statementsfor spreadsheet examples for completing balance sheets, income statements, and cash flow statements.
Human Resources
An inventory of the human resources of a ranch should include any person who works on the ranch (Figure 1&Figure 2) or who works for the ranch in a support role.
Family, full and part-time employees.
Custom hired operators.
Neighbors and friends.
Volunteer Fire and Ambulance Departments.
Banker, attorney, FSA, NRCS, veterinarians.
Fig. 1.My wife, Tracy, and step-daughter, Raesha, working on our family's ranch.
Fig. 2.Myself and Raesha fixing fence on our family's ranch.

Other Considerations
Don't forget about your local volunteer fire and ambulance departments (Figure 3&Figure 4) that will respond to your ranch in case of an emergency. A simple spreadsheet or notebook can be used for the human resources inventory (Table 1).
Fig. 3.Gregory Volunteer Ambulance Service.Photo by Matt Vanderlinden.

Fig. 4.Gregory Volunteer Fire Department.Photo by Matt Vanderlinden.

Physical Resources
Physical resources will include: Equipment, livestock and crop resources. List all equipment utilized by the ranch along with the model, size, age and condition. Specify if the piece of equipment is owned, leased, or borrowed. Placing a value on the equipment can be done two different ways. We can use the market value or cost value. Please refer toEnd-of-Year Accounting: Just what do you have?for more information on which value is recommended for each different physical resource as recommended by theFarm Financial Standards Council.
Depreciation Example
For example, if we want to determine the cost value of a tractor using a straight-line depreciation method we take:
(Original Cost - Salvage value) / Yrs. of Useful Life = Annual Depreciation
Example of tractor: ($120,000-$30,000)/10 = $9,000)
Now we multiply $9,000 by the years of accumulated use and then subtract from original cost of $120,000 to calculate the cost value.
If our tractor is 4-years-old: $9,000 x 4 = $36,000 in accumulated depreciation.
Original cost - accumulated depreciation = Cost value. ($120,000-$36,000 = $84,000).
Our tractor would have a cost value (book value) of $84,000 (Table 1). This value can then be used in our balance sheet.
Additional Considerations
All livestock and crops produced on the ranch need to be included in the inventory. Fair market values can be used for stored crops or raised livestock. Purchased breeding stock can use fair market value or cost value. Please refer to theseuseful toolsto assist with livestock and crop inventories.

Conclusion & Putting it Together
As mentioned in theStrategic and Scenario Planning in Ranching Manual, a ranch operation first needs to "know where it is" before figuring out "where it's going." Completing an in depth ranch inventory is a critical first step in the strategic planning process. Once the inventory is complete, a ranch manager can determine what the strengths, weaknesses, opportunities and threats (SWOT) are to the ranch and thus effectively manage through challenging times. Upcoming installments will cover the SWOT analysis along with establishing a vision for the ranch.
References:
Gates, R.N., B.H. Dunn, J. Davis, A. Arenzo, M. Beutler. 2007. Strategic and Scenario Planning in Ranching: Managing Risk in Dynamic Times. Manual No. EC924. South Dakota State University, King Ranch Institute for Ranch Management, Texas A&M University-Kingsville.
Johnson, J., B. Bennett, S. Beavers, B. Duckworth, W. Polk, B. Thompson. 2005. Developing Business Plans for Agricultural Producers. Department of Agricultural Economics, Texas Cooperative Extension, Texas A&M University.