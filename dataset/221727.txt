The major potato states had roughly the same amount of product in storage Feb. 1 as they did at the same point in 2017.
Potato stocks were 202.55 million cwt. at the start of this month, just barely down from last year, when the stocks were 203.10 million cwt.
Potatoes in storage account for 51% of the production by fall storage states, 1% more than 2017, according to a Feb. 15 report by the U.S. Department of Agriculture.
Potato disappearance is down 3% to 197 million cwt., and season-to-date shrink has also fallen, down 5% from 2017 to 15.4 million cwt.
The 13 key fall crop producing states listed in the report account for 91% of all potato production.