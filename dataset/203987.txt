Following are final Farm Journal Midwest Crop Tour results from South Dakota: 




South Dakota Corn




2017 District


Ear Count in 60 ft of Row


Grain Length
			(inches) 


Kernel Rows Around


Row Spacing
			(inches) 


Yield
			(per bu.)


Samples




SD5


41.00


6.00


17.00


30.00


69.70


1




SD6


88.78


6.90


15.67


29.70


161.86


27




SD9


82.70


6.75


15.26


30.09


141.52


46



S Dak. Average

84.35


6.79


15.43


29.95


147.97


74 




3-year avg. by district


Ear Count in 60 ft of Row


Grain Length
			(inches) 


Kernel Rows Around


Row Spacing
			(inches) 


Yield
			(per bu.)


Samples



SD 5

74.50


4.78


13.40


30.00


79.74


1



SD 6

86.30


6.82


16.41


29.44


164.39


23



SD 9

83.82


6.63


16.05


29.41


154.03


44



SD Average

84.62


6.68


16.14


29.42


156.14


68




 




South Dakota Soybeans




2017 District


Pod Count in
			3 feet


Soil Moisture


Growth Stage


Row Spacing
			(inches) 


Pod Count in
			3 X 3 Square


Samples




SD5


330.00


5.00


5.00


15.00


792.00


1




SD6


674.36


4.07


4.81


27.30


895.95


27




SD9


674.94


3.73


4.77


27.51


904.22


44



S Dak. Average

669.93


3.88


4.79


27.26


899.56


72 




3-year avg. by district


Pod Count in
			3 feet


Soil Moisture


Growth Stage


Row Spacing
			(inches) 


Pod Count in
			3 X 3 Square


Samples



SD 5

712.95


4.00


4.75


30.00


855.54


1



SD 6

796.27


4.05


4.85


28.46


1023.69


22



SD 9

778.91


4.11


4.65


27.59


1035.35


42



SD Average

783.71


4.10


4.72


27.94


1027.80


65