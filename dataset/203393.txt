Good weather and strong U.S. demand has growers and shippers of Peruvian onions looking forward to a strong season for their product.
"Initially, we are really excited about what we see - size profile and, especially, flavor profile," said Marty Kamer, president of Greencastle, Pa.-based Keystone Fruit Marketing Inc.
Volume likely will be "consistent" with recent seasons, he said.
"The early sweet onion crop in Peru is coming in nicely," Kamer said. "Adequate volume and size of onions for our core business. Demand and high quality sweet onions from Peru (are) expected to be excellent throughout fall and winter selling season."
A "favorable growing season" should lead to high-quality onions in a good range of sizes, including mediums, jumbos and colossal, said John Shuman, president/sales director of Shuman Produce Inc. in Reidsville, Ga.
"We are poised for a great shipping year with high-quality product for our consumers," Shuman said.
Everything points to a successful Peruvian season, said Nelly Yunta, vice president of customized brokers with Jacksonville, Fla.-based Crowley Maritime Corp.
"This season looks very good and should run through December," she said. "According to our customers, the weather has not (adversely) affected the crops."
Ralph Diaz, export and import sales manager with Sunbury, Pa.-based Karpinski Trucking & Produce, got a first-hand look at the crop on a trip to Peru in mid-August.
"It looked pretty good," he said.
Maybe too good, in one respect, Diaz noted.
"It's concerning in the simple fact that we thought Vidalia would be over with and has another two weeks -- looks like that's going to slow down the process a little," he said.
Diaz said the onions he saw in the Ica growing region of Peru looked "sharp - fairly clean with good sizing."
He said he anticipates strong production volume this year.
"They're on schedule to have a pretty decent crop," he said.
Peru onions appeared to be entering a "satisfactory" market, Diaz said.
"The Peruvians are always hoping for a $20-to-$28 market; we'd love to see a $20 market, but we're expecting more like $19."
As of Aug. 26, according to the U.S. Department of Agriculture, 40-pound cartons of jumbo granex sweet onions from the Vidalia District of Georgia were priced at $24. A year earlier, the same product was $27-30.
"The market looks pretty good," said Delbert Bland, owner of Glennville, Ga.-based Bland Farms LLC. "There's a lot of demand for Peruvian onions because we're finishing Vidalia (by the end of August) and we've already started receiving Peruvian onions."
The timing of the two deals works out well, said Walt Dasher, co-owner of Glennville, Ga.-based G&R Farms.
"Our core Peru customers are also our Vidalia customers," he said.
If there is any concern with the Peruvian deal it generally would be too many large onions, Bland said.
"You have a tendency of Peruvian onions to get too big," he said.
Bland Farms grows its own product in Peru, and it works to mitigate those concerns, Bland said.
"We plant onions a little thicker each year to try to compensate for the oversupply of the larger ones," he said.
The ideal balance would be 15% mediums, 15% colossal and 70% jumbos, Bland noted.
"It works out pretty good for us," he said.
Lyons, Ga.-based L.G. Herndon Farms Inc. had received its first few loads of Peruvian onions by Aug. 23, said John Williams, sales director.
"They look fresh and good, clean skin - one of the best onions so far," he said.
A similar report came from Savannah, Ga.-based Triple A Produce and Oso Sweet Onion Co.
"For initial supplies, we have very nice quality," said Mark Breimeister, sales director.