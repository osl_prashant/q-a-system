Easing alongside his farmland on the afternoon of June 7, Charles Johnson slammed his brakes and jolted his truck into park when he saw the dicamba cobra heads. Even from his cab and across the turn row, he could see the telltale hooded cupping of stretched soybean leaves indicative of dicamba presence. Johnson was surrounded by the aftereffects of off-target movement: “I’ve got roughly 500 acres of dicamba damage, but I’m not alone by any means. This is popping up everywhere. Everywhere.”
After a disastrous 2016, which saw hundreds of thousands of acres of U.S. farmland affected by off-target dicamba movement, 2017 was touted as a year of labels, best management and spot-on application. Yet, early reports of dicamba drift pouring in across Arkansas and Mississippi starting in May and continuing through mid-June make the echoes of 2016 difficult to ignore.
Despite an absence of concrete data on affected acreage, the concerns of producers and weed scientists are mounting. Pared down, the damage is alarming and there are plenty of passes left in the spray season. Reacting to the current number of alleged dicamba misuse complaints, Arkansas’ Pesticide Committee, a special committee of the Arkansas State Plant Board (ASPB), passed a motion June 16 to recommend adoption of an emergency regulation to immediately ban in-crop dicamba product use at a scheduled meeting on June 20. If passed by the full ASPB, the measure will need approval from Gov. Hutchinson, according to Adriane Barnes, communications director for the Arkansas Agriculture Department (AAD).
One County, 20,000 Acres
Tom Barber waded through a sea of soybean fields hammered by dicamba incidents in 2016, and the University of Arkansas (UA) weed scientist shouldered heavy off-target concerns prior to 2017. “I’ve had numerous calls over the last two weeks and the volume of incidents has already surpassed 2016,” he says. “However, we’ve got millions of acres to spray this year and that has something to do with it. Most producers are trying their best to be careful, but soybeans are incredibly sensitive and it doesn’t take much to cup the leaves.”
As of June 20, the AAD had received 135 dicamba misuse complaints across 17 counties. (Arkansas banned Monsanto’s Xtendimax dicamba formulations for the 2017 crop year, but gave a green light to BASF’s Engenia product line.) “Our plant board is very busy right now and they are getting many field reports and working a lot of extra hours to handle a higher than normal volume of calls, says Barnes.




 


Early reports of dicamba damage pouring in across Arkansas and Mississippi starting in May and continuing through mid-June make the echoes of 2016 difficult to ignore.


© Chris Benentt







Phillips County rubs against the Mississippi River in eastern Arkansas and is an early season epicenter of off-target dicamba movement, evidenced by 20,000 acres hit with drift or volatility, according to Robert Goodson, UA Extension agent. Goodson’ cell phone has rung with calls from worried growers since May, and the pace of reports has quickened. (Phillips County farmland affected by off-target dicamba movement in 2016 tallied roughly 2,000 acres for the entire crop year.)
The affected ground in Phillips County ranges from 40-acre plots to a 2,000-acre expanse blanketing four separate farming operations. Ominously, Goodson insists many of the countywide drift incidents involve applications with strict adherence to label specifications: spraying done right. “Some guys are doing it absolutely right by the label and management and still ending up with dicamba on a neighbor’s crops through volatility,” he says.
Widening Footprint
Across the river from Phillips County, Johnson farms in Lula, Miss., and believes inversion was the dicamba vehicle of delivery on his acreage. Johnson says the application source was relatively distant. “The dicamba must have gotten up and moved. The guy who sprayed left a good buffer of three-quarters of a mile in one spot and 1.5 miles in another spot,” Johnson explains. “I’m not against the technology because I’ve seen how it knocks out pigweed. Next year, I may have to go with a dicamba-tolerant soybean.”
Regarding yield issues, Johnson is forced to play a guessing game because most of his affected acres were in the reproductive stage and subject to significant yield loss. (120 acres were newly planted and not highly susceptible to yield loss.) “I don’t know what’s going to happen because I don’t know the rate that got on my beans. Yield loss is coming, but I don’t know how much,” he explains.
As of June 6, the Mississippi Department of Agriculture and Commerce is investigating 26 complaints of suspected off-target movement of dicamba, compared with 13 cases in 2016, according to Paige Manning, director of marketing and public relations for the Mississippi Department of Agriculture and Commerce.
In 2016, Mississippi escaped the intensity of off-target dicamba movement that occurred in Arkansas, Missouri and Tennessee. However, Jason Bond, a weed scientist with Mississippi State University, has been fielding calls regarding dicamba issues in 2017 since late May, and he has recorded drift and volatility on a broad scale. (In 2016, he didn’t see any significant injury from dicamba in Mississippi until June 29.)




 


“Some guys are doing it absolutely right by label and management and still ending up with dicamba on a neighbor’s crops through volatility,” Goodson says.


© Chris Benentt







The Mississippi Delta is a mix of Xtend and Engenia technologies (cotton and soybeans) in the heart of the Palmer amaranth war. Bond says affected acreage is adding up quickly as the scale grows: “We know Arkansas has 20,000-plus acres hit in one county. It’s likely a couple of Mississippi counties have affected acres in that same ballpark, and we’ve had whole farms hit. With affected acres on that scale, this is a really big deal.”
Bond has seen examples of straight line drift, but he’s also noted instances of injury due to movement during times conducive for temperature inversions. In particular, one 120-acre field in the central Delta was bordered by trees on all four sides, yet was entirely blanketed with dicamba damage. “The only way it could get there was from straight up,” he explains. “With that pattern, it is likely a temperature inversion was responsible for the off-target movement.”
Bond is increasingly frustrated due to correct procedures followed in many of the errant applications. He acknowledges there are likely some illegal applications of dicamba in 2017, but says he’s recording damage in cases where all rules and regulations were followed. “Some of it is bizarre. There are growers who managed applications entirely by the book and still hurt a neighbor’s soybeans,” Bond notes.
“Sometimes I can’t offer a grower any explanation. I’m seeing growers who wanted to do it right and so they followed every line of the label. It still got away from them,” he says.
Shoulder to shoulder with Bond, Mississippi State University rice specialist and soil fertility agronomist Bobby Golden has walked dicamba-damaged Delta fields this year. From Yazoo City to Tunica to the bluff hills at Greenwood, Golden says the off-target incidents are numerous: “I’ve never been involved with anything of this scale. From physical drift to inversion, dicamba-injured soybeans are showing up in so many places.”
In agreement with Bond, Golden says many growers are making applications correctly, yet off-target incidents are occurring. “There’s always a learning curve with a new technology, but in a lot of these cases, there was nothing a farmer could have done differently,” he says. “From talking with producers, the application was on the money, but it went off the crop.”
“What’s really scary, considering soybean sensitivity to dicamba? If you’re the guy making an application properly and your neighbor still gets drifted on, I’m not even certain it was from you. It could have been another farmer down the road that wasn’t following procedure,” Golden explains.
Safety in Mizzou, Tennessee?
Despite widespread damage in 2016, Missouri has fared well so far this crop season. However, Kevin Bradley, a University of Missouri weed scientist, is withholding judgement and remains cautious because Arkansas and Mississippi are weeks ahead in crop stage. In 2016, Bradley’s first dicamba-related call didn’t come until June 22, and it triggered a daily flood of calls that didn’t stop until the end of July.
“Right now I’m just glad we’re not winning the dicamba race this year,” he says. “We don’t have the problem yet that I’m hearing about in neighboring states. It’s just too early to be certain about anything.”
Larry Steckel, a weed scientist with the University of Tennessee, says the Tennessee Department of Agriculture has only received a handful of dicamba-related complaints in 2017. “Growers are working hard to spray the right way. Things may blow up, but I haven’t seen anything significant yet,” he says. “I’ve only gotten a few phone calls so far, but last year they just started rolling in all at once.”




 


Early dicamba damage is alarming, particularly with plenty of passes left in the spray season.


© Chris Benentt


 




In 2016, Steckel was swamped with dicamba calls that began the last 10 days of June. The related incidents kept him in fields recording off-target acreage damage for the next five weeks. “I looked at 30,000 acres of drift myself last year,” he says. “Certainly, hearing what’s going on in the states below makes me worried.”
Bag of Unknowns?
Back in Mississippi, Golden’s phone rings as he heads to another farm to survey more dicamba damage: “Right now nobody knows exactly what is going on and we aren’t able yet to follow the clues with most cases all presenting different ones,” he says. “However, we do know one thing for sure: There is a lot of dicamba ending up in places it shouldn’t be.”
“This is all a big bag of unknowns,” Johnson echoes. “All I know is you wake up and go out to a field and there it is: Dicamba is on your beans.”