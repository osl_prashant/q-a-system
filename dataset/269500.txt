A semi hauling 1,900 pigs tipped over on state Route 20 in Iron County, Utah, Friday afternoon. The driver was apparently going too fast for the corner as it approached the Interstate exchange. The load shifted, forcing the semi to roll over onto the passenger side, reports Sgt. Eric Prescott of the Utah Highway Patrol.
Reports say some of the 1,874 pigs on the trailer died in the accident. Several people arrived on scene to help corral pigs that escaped when the semi tipped over.

“Their biggest challenge was getting the pigs that were loose corralled and then offloaded onto another vehicle,” Prescott told the St. George News.
The driver only suffered minor bumps and bruises.
Read more from StGeorgeNews.com.