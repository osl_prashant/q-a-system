Polygamous town contractor must pay $200K child-labor case
Polygamous town contractor must pay $200K child-labor case

By BRADY McCOMBSAssociated Press
The Associated Press

SALT LAKE CITY




SALT LAKE CITY (AP) — A Utah contractor with ties to a polygamous group must pay $200,000 in back wages to children who were forced to pick pecans for long hours in the cold without pay, after a U.S. appeals court upheld a ruling on this week.
But the company doesn't have to hire independent attorney to monitor the business for five years, the 10th Circuit Court of Appeals said in a ruling issued Tuesday. That watchdog was ordered by a lower court judge to deter the company from using child labor again, but the appeals court judges decided that exceeded the court's authority.
The decision marks a partial victory for both sides in the long-running case that centers on a 2012 harvest in the twin cities of Hildale, Utah and Colorado City, Arizona. The community is the home base of the Fundamentalist Church of Jesus Christ of Latter-Day Saints, an offshoot of mainstream Mormonism that practices polygamy.
The Department of Labor, which accused Paragon of violating a 2007 agreement by putting nearly 200 children to work picking pecans for long hours in the cold without pay, didn't return an email seeking comment.
Attorneys for Paragon, which argued the kids were volunteering with their families to pick up fallen nuts for the needy, didn't return emails and phone calls seeking comment either.
The Salt Lake Tribune first reported the ruling, which came four months after judges in the Denver-based appeals court hear arguments from both sides.
In the ruling, the judges reject the argument that the kids were volunteering. They pointed to testimony in the case from children and parents who said church leaders pressured members to send kids to work the harvest under threat of discipline if they didn't.
Prosecutors contend the company has deep ties to a polygamous sect led by Warren Jeffs, who was under pressure to make money for its leaders when it used 1,400 unpaid workers, including 175 children.
Jeffs is currently serving a life sentence in Texas after being convicted of sexually assaulting girls he considered brides. Now group leaders don't have a spokesman or contact where they can be reached.
The appeals court overruled the appointment of the special master, saying the penalty invoked by U.S. District Judge Tena Campbell exceeded the court's authority. The move needed to be based on more than just the possibility that Paragon might put children to work again, the ruling said.
"The district court was understandably frustrated with what it saw as repeated and willful violations by Paragon," the judges said. "But no evidence existed regarding Paragon's employment of children at the time of the district court's sanction."
It's unclear how the ruling will impact a separate but related case in which the US government accuses Paragon Contractors of changing its company name to Par 2 and in 2015 and 2016 again putting children to work on construction jobs.
Par 2 denies the allegations, saying it's a totally separate company.