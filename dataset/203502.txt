<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


SUN VALLEY, Idaho - Change is coming to Washington D.C., but John Keeling of the National Potato Council believes the potato industry will be prepared no matter which political party holds the upper hand after November elections.
 
Keeling, executive vice president and CEO of the National Potato Council, Washington, D.C., spoke about politics, nutrition and trade issues Sept. 1 at the Idaho Grower Shippers Association convention in Sun Valley.
 
After reviewing recent developments in the presidential campaign, Keeling said he believes the potato and the NPC will be successful no matter who gets elected.
 
"I've seen a lot of changes happen in Washington and we get a new set (of changes) about every four years, and the only thing we do is go to work and start making new friends and start pushing to where we can go," he said. 
 
Some doors will open and others will close, he said, but the NPC will continue to be effective because of it support of lawmakers from both parties.
 
"We are one of the very few agriculture groups in the country that has any kind of balance between Democrats and Republicans," he said. While most potato growers are Republican-leaning, Keeling said the NPC isn't locked up with one side of the aisle or the other.
 
Looking ahead to the 2018 farm bill, Keeling told The Packer that the historical model of nutrition programs being paired with farm programs in the farm bill may not hold.
 
"The last farm bill had to split the nutrition title from the farm programs in the House to get it passed, and you will have that dynamic again," he said.
 
Keeling said that farm bill lobbyists are used to building a coalition between nutrition program supporters and farm program supporters to pass the bill.
 
"But will that traditional coalition exist and what will it take to pass a farm bill?" Keeling asked, noting there are fewer and fewer lawmakers representing rural districts in Congress.
 
Because of the uncertainty, Keeling said agriculture groups are looking to work together earlier to evaluate the situation and be ready to move forward.
 
Speaking to potato nutrition, Keeling said the industry has made great progress in making the case for potatoes in schools lunches and the Women Infants and Children feeding programs.
 
"People started to see what the facts were about the nutrition of potatoes and decisions were being made more on the nutritional quality of the food than they are on the form of the food," he said. 
 
Keeling also said the NPC and other industry groups continue to work at removing obstacles to trade with Mexico and some Asian markets.
 
U.S. potato exports to all of Mexico was opened in May of 2014, but a federal judge in Mexico later shut down U.S. imports to the interior of Mexico only weeks later.
 
Several U.S. potato grower shippers are now involved in a lawsuit in Mexico to remove phytosanitary barriers to trade. Keeling said the president of Mexico has been trying to resolve the issue. 
 
"Good news is that we have success in some of the cases and we have the resources to continue to be present in those cases," he said. Keeling said that while potato interests in Mexico may continue to fight against the imports of U.S. potatoes, he believes the U.S. can prevail in court cases.
 
"We should never forget that we got that market open, and I don't think in the world we live you can put that cat back in the bag," Keeling said. "It will take us some time and we will have to fight for it, but I think we will get that market," he said.
 
Having invested 14 years in trying to expand U.S. potato trade to Mexico, Keeling said the fight is "personal." "We will make it happen."