About five years ago, management at Riverside, Calif.-based Index Fresh Inc., along with some of the company's producers, was discussing ways the company's field department could attract new growers to accommodate rising demand.
Then they had a brainstorm.
"We came to the realization that if we could help our current base of growers farm better and produce more fruit, that we would achieve the goal of more fruit for Index Fresh as well as help the farmers who had been loyal to us be more profitable," said Giovanni Cavaletto, vice president of sourcing.
The germ of an idea blossomed into a full-fledged grower outreach program that has served the company well as its annual production swelled by 30% over the past five years.
 

Seminars

Core to the program is a series of quarterly seminars the company conducts for its growers in three California avocado-growing areas - San Luis Obispo, Oxnard and Fallbrook.
"We identify a topic important to avocado farming, invite two or three experts in that topic to speak, and then we go on a road show," Cavaletto said.
Many of the topics, which have included pest management, fertilizers, irrigation, farm labor, crop management and pollination, are suggested by growers in response to company surveys.
Together, the seminars attract nearly 200 participants, including farm owners, operators, managers and other employees.
"The number keeps getting bigger," Cavaletto said.
The seminars, some of which are conducted in Spanish, are recorded on DVDs and posted on the Index Fresh website.
"We're the only ones doing that level of cultural outreach to help put the best information into the hands of our growers," he said.
"It's one of our biggest successes in the last five years."
Index Fresh believes that in order for its California growers to compete on the world stage, they need the best farming information available, Cavaletto said.
The company has hosted speakers from Chile, Israel and New Zealand as well as speakers from the University of California, state and local water authorities, labor specialists and pollination experts.
"We really created a name for ourselves in the grower community," he said.
In addition to the seminars, Index Fresh communicates with its 300 growers via newsletters and an annual marketing meeting, where the company highlights supply and demand opportunities that affect the California crop.
"Good old-fashioned field work" also is part of the grower outreach.
Members of the Index Fresh field department visit with growers, walk their groves with them, look at the state of their trees and work with them to develop harvest strategies and crop estimates.
Cavaletto said he is especially proud of the leaf analysis program the company has developed for the fall.
"It gives a read on what nutrients the tree needs, and growers use those to tailor a fertilizer program for the coming year," he said.
Adding value to growers' operations is the top priority of the field team members.
"We're not showing up to take time away from them," Cavaletto said.
"We're showing up with information about the market or cultural issues. We want growers to look forward to what we have to say."