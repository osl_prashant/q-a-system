After a rough week for the grains complex, the markets are eager for good news. Will the USDA reports released this Thursday, August 10, provide that much needed boost to morale?
Historically, the August reports tend to send the corn and soybean markets up or down significantly, according to Brian Basting with Advance Trading. That’s most likely because this Crop Production report is the first time USDA looks at corn and soybean yields since their initial projections at the beginning of the year.
 

In January, USDA projected corn yield to ring in at 170.7 billion bushels. Analysts agree Thursday’s yield number will likely come in well below USDA’s trendline.

 
“I think we will be in the mid-160 range,” says Pro Farmer’s Brian Grete. “My gut says it’s probably going to be 165-166.”
Jerry Gulke of the Gulke group agrees. “We expect USDA to report corn yields somewhere near the Informa Economics estimate of 166,” he says.
Gulke doesn’t expect soybean yield to change too much. “A surprise would be if they lowered bean yield more than a bushel,” he says.
Some analysts don’t think USDA’s August yield estimates are super accurate.
“If you just look at the methodology USDA uses, the August is based on plant populations,” Grete says. “Moving forward, that’s when they start to take into account grain length, ear weights and that type of stuff. You won’t have an accurate yield picture until you peel the husks back.”
USDA is also releasing the August World Agricultural Stocks and Demand Estimates (WASDE) report on Thursday. It’s critical for U.S. producers that demand stay high in order for markets to remain afloat.
“What we don’t want to see is to have them lower the yields in corn and lower demand,” Gulke says. “If they lower the demand side such that it overshadows any loss in production, that’s not going to set well with traders.”
USDA has the potential to lower demand because of the huge South American crops earlier this year. Gulke expects soybean demand to remain pretty flat because of delivery problems in South America.
“My hope is that they cut the yield and my fear is that they cut demand as well,” he says.
It’s still unclear what the market will do Thursday, but you can take action. Gulke says it’s not too soon to be marketing some grain.
 “We’ve started selling some grain, but I think it’s too early to be 100% sold in grains yet,” he says.