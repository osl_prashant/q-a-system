Drought and dryness is continuing to expand across the Great Plains and southwestern states. In the latest Drought Monitor, roughly 57 percent of the U.S. is experiencing some form of drought.

The biggest changes are happening in the southern Plains states of New Mexico, Texas, Oklahoma, Colorado and Kansas. The dryness and colder weather are causing some problems for wheat producers.

“For all the promise we saw the end of August, early September, it really hasn’t happened that way,” said Dr. Derrell Peel, Oklahoma State University department of agricultural economics. “We really don’t have a lot of good wheat pasture right now.”