The application of HACCP (hazard analysis critical control points) principles has been critical for the success of many industries. Our dairies, with the multitude of processes that occur every day, are perfect candidates to benefit from the HACCP concept to gain better control of feeding practices.Feeding practices can have a tremendous impact on herd health and production. Every day, we fight the risk of introducing mycotoxins and pathogens from spoiled feeds, we struggle to maintain a consistent level of dry matter in our wet feeds, we struggle to have feed in front of the cows at all times and we often find it challenging to have new feed in the bunks at the same time every day.
In short, there are many steps that if skipped or done improperly can have a direct negative effect on cow health.
Most dairies are already practicing some form of quality control. At most operations, however, it is an informal program. The application of HACCP principles can enable dairies to have a more formal and effective method of controlling feeding, which can have a positive impact on herd health and production.
The following eight steps are required to design and implement an HACCP plan for feeding practices:
1. Identify the hazard
Cows are not receiving the prescribed diet.
2. Identify the critical control points (CCP)
Point A. Feed Availability 
1. Understand how to read feed bunks.
2. Measuring pounds of refusals might not be indicative of feed availability for all cows in a pen. Research indicates individual cows have a tendency to use the same set of 10 to 20 stanchions daily and are reluctant to eat in other sections of feed bunk.
Point B. Feeding Times 
1. Keep track of feed management software feed delivery times.
Point C. Consistent TMR nutrient profile 
1. Milk urea nitrogen (MUN’s) can be a good measure of consistency.
2. Measure NIR TMR sampling to ensure correct nutrient profile.
Point D. TMR Dry Matter 
1. Use Koster tester to measure dry matter.
Point E. TMR Temperature 
1. Use s long probe thermometer for measuring TMR temperature.
2. If TMR is heating up, investigate which feeds and management issues could be causing it.
3. Establish minimums and a maximums for CCPs 
A. Feed availability: We want no more than five stanchions on a row to be without feed less than one hour before the next feeding.
B. Feeding times: We want cows to be fed within 10 minutes of the previous day’s feeding times.
C. TMR Nutrient Profile: MUN’s within two to three points of weekly average target (8mg/dL to 11 mg/dL); NIR nutrient profile kept within established ranges.
D. TMR Dry Matter: Within 48% to 52% dry matter. E. TMR Temperature: We want the temperature range to remain between ambient temperature and up to 10°F higher.
4. Establish critical limits for each CCP
A. Feed availability: More than five stanchions on a row without feed.
B. Feeding Times: More than five minutes early or more than five minutes late.
C. TMR Nutrient Profile: MUN’s more than three points from average weekly target (less than 8 mg/dL to greater than 11 mg/dL).
D. TMR Dry Matter: under 48% or over 52%.
E. TMR Temp: more than 10°F from ambient temp.
5. Establish monitoring procedures
Who, how and when will each CCP be measured?
6. Establish corrective actions
A. Be prepared to take corrective action quickly.
1. Who: Designate a responsible person.
2. When: If we are outside critical limits.
3. What: Define what action is to be taken if outside critical limits.
B. Example: If TMR is too dry, we add water; if there is a large fluctuation of MUN’s, we have a checklist:
1. Did we run out of an ingredient?
2. Did we have feeding errors or change silage piles?
7. Establish record keeping procedures
Keep records to show the system is in control.
8. Establish verification procedure
One person should be responsible to check at regular intervals whether or not CCPs are met.
These guidelines are only examples and need to be adjusted in the context of each dairy’s management, diet design and so forth.
So be sure to check with your nutritionist, veterinarian or management consultant to see how they can help you structure a quality control program for feeding that uses the HACCP principles.
Controlling variation at our dairies is key to achieving better herd health and production. 
 
Note: This story appeared in the September 2017 issue of Dairy Herd Management.