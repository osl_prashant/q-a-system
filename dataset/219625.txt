A new rule from the U.S. Department of Agriculture gives guidance on how growers can preserve their Perishable Agricultural Commodities Act trust rights when selling to agents or commission merchants.
PACA’s statutory trust provision, put in place by Congress in 1984, moves suppliers of fresh produce to the top of the creditors’ list in the event a company declares bankruptcy.
Preserving grower trust claims
In the final rule, the USDA noted that recent court decisions have invalidated the trust claims of unpaid growers against their growers’ agent because those growers did not file a trust notice directly with the growers’ agent.
The agency said that some courts have ruled that while the growers’ agent is required to preserve the growers’ trust benefits with other buyers of the produce, the grower has the responsibility to preserve its trust benefits with the grower’s agent.
The USDA said the final rule makes it clear that fresh produce growers must preserve their trust benefits against their agents. Growers’ agents sell and distribute produce for or on behalf of growers and may provide such services as financing, planting, harvesting, grading, packing, labor, seed, and containers, according to the USDA.
The USDA said growers are currently not required to obtain a PACA license. If they do, however, they can include the statutory trust language on their billing documents.
Without a PACA license, growers would need to send the trust notice separate from the invoice.
The final rule also clarifies that if a grower has not received a final accounting from a commission merchant or growers’ agent, the grower is only required to “provide information in sufficient detail to identify the transaction subject to the trust.”
Welcome news
Craig Stokes, PACA lawyer with Stokes Law Office, San Antonio, Texas, said the final rule was welcome news for growers who sell to agents on consignment.
“The rule confirms what I had told growers for a long time and that is the rule recognizes you can’t put information down that you don’t know,” he said, noting that growers often don’t know what price they will receive for their product.
“What is nice now is that we will be able to take this rule and show it to a judge and say, ‘The (USDA) agrees you don’t have to supply information you don’t possess,’” he said.
Stokes said growers can issue a trust notice and not put in the amount owed if they don’t know it. “Growers can always get a PACA license and simply issue a consignment invoice and then put the PACA trust legend at the bottom of the invoice,” he said.
If a grower doesn’t have a PACA license, Stokes said the grower must issue a trust notice in a separate document.
Stokes said it makes sense for growers to invest in a PACA license.
“For $995 a year, you can obtain the license and issue consignment invoices and put the trust legend at the bottom of the invoice,” he said. “You send the invoices out and you are under the trust.”
Matt McInerney, senior executive vice president of Western Growers, said the association was supportive of the USDA’s rulemaking on PACA though it had hoped the agency would create another approach on trust protection for growers.
He said the rule reaffirms that unlicensed growers, in order to preserve their trust rights, do in fact have to file paper intent to preserve their trust benefits. McInerney said Western Growers had been hopeful that another way would have been created to allow growers to automatically enjoy PACA trust protection.