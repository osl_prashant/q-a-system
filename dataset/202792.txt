The World Avocado Organization is setting its sights on expanding avocado demand in Europe and the United Kingdom.
 
Holding its first-ever membership meeting in Berlin Germany on Feb. 7, the group's primary mission this year is to promote the consumption of avocados in the European Union and United Kingdom, according to a news release. More than 120 avocado growers, exporters, and importers gathered for the meeting, according to the release.
 
"For our inaugural membership meeting, we were truly inspired by the level of energy and engagement displayed by our members," organization chairman Jimmy Bosworth said in the release. "The WAO's goal is to jointly promote the growth of avocado markets in Europe and the United Kingdom and we look forward to continuing to work hand-in-hand with all of our partners to make that dream a reality."
 
Together, the EU and the UK are the second-largest market in the world for imported avocados, according to the release, consuming more than 800 million pounds of avocados in 2016.
 
"This meeting was a very important first step in bringing together some of the varied stakeholders in the avocado market," organization vice chairman Zac Bard, said in the release. "Under the WAO's unified banner, avocado producers, exporters, and importers from around the world will work together to promote avocados for the benefit of all WAO members and European and British avocado consumers."
 
The marketing effort is unique, World Avocado Organization CEO Xavier Equihua said in the release. 
 
"Never before has a multi-country organization implemented a generic marketing program of this kind and scale across the EU and U.K.," he said in the release. "We are extremely excited to see it play out over the course of 2017 and beyond."