We are blessed that the vast majority of America’s ranches are still family enterprises. Passing ranching heritage to the next generation is a privilege, but it comes with the challenge of careful planning to ensure a smooth and sustainable transition. 
Lawyers start to sound like broken records with our constant harping on estate planning. But we keep nagging because we’ve witnessed the wrecks that occur when families fail to plan for the future. Estate planning is more critical for ranches now than ever because our operations have become more sophisticated and more valuable.
 
At the same time, succession planning for ranchers has become more complex, which adds to the natural impulse to procrastinate on estate planning. There are lots of reasons folks avoid succession planning, but there are a few excuses we see over and over in our practice. Here’s a list of the most common excuses as well as our responses, which will hopefully motivate you to take action:
 
1. Planning is too expensive.  Estate taxes and probate litigation can take a huge bite out of your assets, but both can be mitigated with a good plan. The price of creating a good plan is almost always lower than the eventual cost to your loved ones if you don’t have a plan.  
 
2. It takes too much time. Setting up an estate plan does take some time and effort, but your family will come out ahead if you give careful attention to the future now. In addition, it is not unusual for good estate planning to lead to better business planning and higher profits as you get a fresh perspective on your finances and operations.
 
3. My family will fight. The near-term discomfort for your family that might come with estate planning pales in comparison to the fights that break out in families when a loved one dies with no plan in place. When someone dies without a plan, it often results in protracted litigation. Litigation is expensive—and your kids are still fighting. 
 
4. I already have a will. Estate plans should be reviewed and updated periodically to ensure they still reflect your current operations. If you own your land, you are likely significantly wealthier now than you were 10 years ago, which means your tax exposure has changed. Additionally, in most states a simple will does not avoid the costly and time-consuming probate process upon your death.  
 
5. I don’t want to talk about dying. Death is inevitable, but it is also our nature to ignore or avoid that reality. Like everything else on your ranch, unpleasant tasks are often critical for success. For your family’s sake, you should steel yourself to plan for the one certainty with which every person must contend.
 
Good estate planning doesn’t have to be prohibitively expensive or painful. Scrap those excuses and contact an attorney you trust to get an estate plan in place as soon 
as possible.  
 
Brent Haden and his wife, Connie, are founders of Haden & Haden law firm in Columbia, Mo. He was raised on a Missouri farm and attended the University of Missouri and Harvard Law School. They live in rural Boone County, Mo., with three sons. You can contact him at: brent@hadenlaw.com.