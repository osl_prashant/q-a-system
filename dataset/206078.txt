Crop calls 
Corn: 1 to 2 cents lower
Soybeans: 4 to 6 cents lower
Wheat: 1 to 4 cents lower

The crop markets faced pressure from weekend harvest progress, with additional pressure coming from a firmer tone in the dollar index. While some areas of the western Corn Belt are in line for showers today and tomorrow, the National Weather Service forecast for Sept. 30 through Oct. 4 calls for below-normal precip across the Corn Belt, which will ramp-up harvest. Drier conditions are also expected across the Central and Southern Plains, which are not favorable for winter wheat establishment. But wheat is currently in a follower's role to the corn market and focused on ample global supplies.
 
Livestock calls
Cattle: Lower
Hogs: Lower
Following last week's strong gains, cattle futures are vulnerable to profit-taking as well as Friday's bearish Cattle on Feed Report. The report showed On Feed and Placements above expectations. Additionally, Friday's Cold Storage Report showed beef stocks topping expectations. Pork stocks rose from the previous month, but were basically in line with expectations. Traders in the hog market are more focused on weakening cash fundamentals. Hog slaughter last week topped 2.5 million head and dressed hog weights rose by one pound from the previous week to signal marketings are backed up.