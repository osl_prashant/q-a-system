Families received food bags with turkeys, produce and other Thanksgiving meal items. Photos courtesy Baldor Specialty Foods.

The weekend of Nov. 18-19, New York-based Baldor Specialty Foods distributed turkeys, produce and other Thanksgiving meal components to more than 700 Bronx families in need.
The meals were handed out at The Point, a community group dedicated to revitalizing the Hunts Point area, according to a company news release. 

Baldor volunteers and local officials helped assemble and distribute the food bags to a line of people wrapping around three city blocks, according to the release.
Companies partnering in the donation:

Tanimura & Antle
Red Jacket Orchards
Krasdale
Bolthouse Farms
Satur Farms