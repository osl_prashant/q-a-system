Officials say vandalism caused North Carolina oil spill
Officials say vandalism caused North Carolina oil spill

The Associated Press

ROBERSONVILLE, N.C.




ROBERSONVILLE, N.C. (AP) — Officials in North Carolina say vandalism is to blame for an oil spill at a closed meat packing plant which has impacted a creek and a swamp.
Local media cite a report from the state Division of Environmental Quality which says a valve on the 5,000-gallon storage tank at the former Robersonville Meat Packing Co. in Robersonville. The division said the spill was discovered on Sunday and involves #6 fuel oil used in furnaces.
A Martin County official says 1,500 gallons have contaminated the site, and a division report says a creek and a swamp are impacted. Town manager Libby Jenkins says crews have kept the spill from getting into the sewage plant.
Jenkins says the facility used to be owned by Perdue, but ceased operations in 2003.