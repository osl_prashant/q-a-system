Get a behind the scenes view of shipping cattle from the Youngmeyer pens in the southern Flint Hills of Kansas (Note: scroll down below for video). 
The summer grazing season has passed across much of the Flint Hills in Kansas and stocker cattle are entering feedlots.
My family has been custom grazing cattle for four generations in Greenwood County. We took in more than 5,000 head of steers and heifers from several different customers this year.
Overall gains were better than expected ranging from 2.1 to 2.85 lb. per day across the native pastures we grazed this year. The grazing season ran from 90-110 days depending on the cattle.
Cattle were shipped to feedlots in Kansas, Nebraska and Oklahoma this year, while some sold direct at sale barns.
Family and friends helped ship cattle for more than a month this summer after the grazing season ended. We started shipping the third week of July and ended on the third week of August.
This year we were able to rent another ranch in neighboring Elk County. My father had ran cows with my grandfather on a set of pastures called the Youngmeyer when he was just getting introduced to the Flint Hills in the 1970s. Almost 40 years later we were able to lease the ranch again, this time running stocker cattle on the pastures that are now preserved through the Kansas Land Trust.
Here is a behind the scenes video from shipping cattle at the Youngmeyer pens this summer:

To see how shipping is done on our home ranch read the following photo essays I’ve put together:
Ranch Life: Shipping Cattle in the Flint Hills of Kansas
15 Images from Shipping Cattle in the Flint Hills