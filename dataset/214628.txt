Marisela, Amanda and Dick Peixoto, owner of Lakeside Organic Gardens, hosted one of the stops for Agri-Culture's 21st Annual Progressive Dinner.
Agri-Culture recently celebrated the fall harvest during its 21st Annual Progressive Dinner.
The educational organization was created to educate people on California’s Santa Cruz County and Pajaro Valley agriculture.
The theme for this year’s dinner was “Distill, Cool, History,” according to a news release, and it raised nearly $40,000.
During the progressive dinner, attendees went to three agriculture related locations for each course, the release said. The first stop was at Blume/Whiskey Hill Farms for appetizers, dinner was at Lakeside Organic Gardens’ cooling facility and dessert was at Naturipe Berry Growers.
Attendees received a bag and box of produce from 14 local companies.