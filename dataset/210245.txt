Red River Valley potato grower-shippers in North Dakota and Minnesota anticipate plenty of spuds on the market for the upcoming season.
That’s a departure from a rain-soaked 2016 season, they say.
“This year, generally, it’s a much better growing season than last year,” said Eric Halverson, executive vice president of technology with Grand Forks, N.D.-based Black Gold Farms.
“We had all the rain last year, and it’s been much drier this year, so it should be a much better crop.”
There wasn’t much moisture to work with during the 2017 growing season, but the abundant rainfall from 2016 had created good planting conditions.
A couple of timely downpours in mid-September helped the digging get started in a timely fashion, Halverson said.
“Things are looking like we’re going to get into some pretty fair harvest conditions,” he said.
The Red River Valley potato generally runs for about six weeks in September and October, with supplies ideally lasting through spring.
Some growers had to wait for rain to enable them to start digging.
East Grand Forks, Minn.-based A&L Potato Co. was one of them, said Randy Boushey, president.
“We had rain on June 25 and it never rained again until last week,” he said Sept. 20. “We’ve got an OK crop.”
That latter downpour opened the fields for harvest, Boushey said.
All told, 2017 is looking much better than 2016, when persistent rain cut about 30% of the valley’s crop, said Chuck Gunnerson, president of the East Grand Forks-based Northern Plains Potato Growers Association.
The Red River Valley includes about 80,000 acres in North Dakota and 45,000 in Minnesota, according to the association. Potato volume for the fresh market typically totals about 7 million cwt., said Ted Kreis, the association’s marketing director.
Harvest was on time for Hoople, N.D.-based H&S FreshPak, a new operation created when Hoople-based Hall’s G4 and Crystal, N.D.-based O.C. Schulz & Sons Inc. purchased Hoople-based Northern Valley Growers.
“It turned out to be a good year to get (the acquisition), with the volume we’re getting this year,” said TJ Hall, president of H&S FreshPak.
Yields were down a bit this year because of the dry conditions, said Chad Heimbuch, president of Cogswell, N.D.-based Heimbuch Potatoes.
Some well-timed rain enabled an early start of digging for Heimbuch, he said.
“We started harvesting Aug. 4 and ran everything through the wash plant,” he said. “Quality is good.”
The valley’s potatoes were arriving during a good market, said Kevin Olson, salesman with Becker, Minn.-based Ben Holmes Potato.
“Prices are strong and quality was good to start,” he said.
 
Pricing
As of Oct. 4, 50-pound cartons of grade U.S. One round red potatoes from the Red River Valley were $11-12 for size A and $18-20 for size B, according to the U.S. Department of Agriculture. Fifty-pound cartons of creamers, 3/4 to 1 5/8 inches, were $25-30.
Prices a year ago were slightly higher for size A — 50-pound cartons were $13-15 — and nearly the same for size B at $19-20. Creamers were the same price, at $25-30.
The contrast in growing conditions between 2016 and 2017 brought some trade-offs, said Bryan Folson, president of East Grand Forks-based Folson Farms.
“The quality is pretty decent — color’s nice and deep and red,” he said.
Steve Tweten, president and CEO of Buxton, N.D.-based NoKota Packers, agreed the potatoes look better this year.
“Beautiful color,” he said.
Volume this season will be up considerably over 2016 at O.C. Schulz & Sons, said Dave Moquist, president.
“It was a tough growing season last year — we had less than half a crop,” he said.
If there were any problems on the horizon for this year, it likely was truck availability, said Paul Dolan, president of Grand Forks-based Associated Potato Growers Inc.
“Transportation right now is a bearcat. It’s tough to get trucks,” he said, noting that recovery from hurricanes in Texas and Florida had tapped into the trucking system. P