Bee Sweet Citrus mandarins support cause
Fowler, Calif.-based Bee Sweet Citrus, which has donated $150,000 to the New York-based Breast Cancer Research Foundation, is going into its third year of support of the organization, said Monique Bienvenue, communications director of the grower-shipper.
Bee Sweet uses the pink ribbon on its 3-pound bag and 5-pound box of Sweethearts-branded mandarins.
"I know packaging will be the same, but as far as our marketing team is concerned, we're definitely going to build more momentum than last year," Bienvenue said. "We pushed awareness during October, but this year, we're actually going to push it up starting in September."
 
Bland Farms furthers its fall tradition
Glennville, Ga.-based Bland Farms will bring its total donations to breast cancer research since 2010 to $135,000 this year, said Allanah Finnan, marketing specialist.
"Starting in September, we switch to all pink packaging," she said. "All sweet onion bags will have pink wrapping on them," she said.
Even the Bland Farms logo will be pink for the duration, as has been custom since the program got involved in the cause in 2010, Finnan said.
 
D'Arrigo Bros. firms up plans
D'Arrigo Bros. Co. of California, Salinas, reports it has raised more than $1.25 million to date for breast cancer research.
In mid-August, the company still was planning a strategy for participation in this year's Breast Cancer Awareness Month campaign, said Claudia Villalobos, marketing and culinary manager.
"We have the foundation and logo and name 'proud supporter of' on every Andy Boy bag and commodity box," she said. "We are looking at doing something to rally around the mission of goals of Breast Cancer Research Foundation and get more press around it and getting it out to the industry at large."
Every September, D'Arrigo Bros. ties large pink ribbons on trees around Spreckels, Calif., Villalobos said.
"The great thing is, people coming up the 101 (U.S. Highway 101, which runs along the California's western coast and connects San Francisco and Los Angeles), go through town to bypass the traffic and see that and try to see what it's about," Villalobos said. "It kind of piques people's interest."
 
GloriAnn/Five Crowns split fundraising 
GloriAnn Farms, Tracy, Calif., and marketing arm Five Crowns Marketing, Brawley, Calif., passed the $40,000 mark in total donations to charities this year, including $21,000 for the National Breast Cancer Foundation, said Daren Van Dyke, sales and marketing director at Five Crowns.
The rest went to the Intrepid Fallen Heroes program, Van Dyke said.
The companies made a donation to the Intrepid Fallen Heroes Fund for every case of packaged sweet corn sold to participating retailers during August and do the same for the National Breast Cancer Foundation during Beast Cancer Awareness Month in October, Van Dyke said.
"Our value-added side is what has the chance to speak directly to the customer, where bulk can't," he said, discussing the focus on packaged sweet corn. "This is our one little niche, and because it's been so successful you're not talking a huge volume item we've had, but the last three years, we've been a bronze- or silver-level supporter of the foundation. For that, it's been very effective."
 
National Mango Board funds research
The Orlando, Fla.-based National Mango Board recently had some of its breast cancer-related research findings presented at the 2016 Experimental Biology Conference on April 2-6 in San Diego, said Angela Serna, communications manager.
"The National Mango Board does not currently fund industry outreach programs regarding breast cancer awareness. However, the board deems breast cancer an important area to research and has funded various studies to determine how mangos could aid in combating the disease," Serna said.
The research, from Susan Talcott and Stephen Talcott of Texas A&M University, showed that even low concentrations of mango polyphenols caused cell death in cancer cells, although not in noncancer, normal cells, according to a news release.
"Overall, mango was more effective the younger the woman and the more aggressive the tumor type," the board said in the release.
 
NatureSweet backs Susan G. Komen
San Antonio-based NatureSweet has announced it is working with the Susan G. Komen organization for the fifth straight year, said Lori Castillo, NatureSweet's senior brand manager.
"In support of the partnership and to raise awareness for Breast Cancer Awareness Month, NatureSweet will run a promotional in-store program Sept. 26-Nov. 5, featuring NatureSweet Cherubs tomatoes," Castillo said.
In connection with its 2016-17 program, NatureSweet Tomatoes will donate to Susan G. Komen $100,000, "regardless of sales," Castillo said.
 
Robinson Fresh drives research donations
Eden Prairie, Minn.-based Robinson Fresh, an offshoot of C.H. Robinson Inc., has raised and donated about $1.36 million for breast cancer awareness and research, said Josh Knox, general manager of Robinson Fresh.
"At this point, we are continuing to work with our customers on this campaign and will continue to drive donations to breast cancer research," he said.
 
Sendik's donations reach $500,000 
Milwaukee-based Sendik's Foood Markets has reached a milestone of $500,000 in donations to Glendale, Wis.-based ABCD: After Breast Cancer Diagnosis, a one-to-one breast cancer patient support organization, Sendik's announced in June.
Sendik's reached that mark after having presented its 2016 donation of $113,500, which the retailer said was its largest annual gift to ABCD to date.
For the last 10 years, Sendik's Food Markets has sponsored fundraising campaigns that involve Sendik's employees, the community and corporate giving, according to a company statement.
This year, the grocer created a three month charitable promotion from February through April. The program began with information and sales of the Sendik's Real Food Magazine for $1.99, with all proceeds going to ABCD. Also in February, Sendik's shoppers who donated $1 at the register received chocolate hearts in appreciation.
In March, Sendik's shoppers rounded up their total at the cash register to benefit ABCD.
In April, funds were raised through Sendik's donation of $1 with each purchase of select ice cream, and the sales of an ABCD key chain generated additional funds.
"Sendik's creativity and commitment to supporting ABCD's mission positively impacts breast cancer patients at times when they need it most," ABCD executive director Ginny Finn said in a news release.
Based in Milwaukee, ABCD provides free, personalized, one-to-one support to all those affected by breast cancer - patients, families and friends. Trained and professionally supported volunteer survivors and co-survivors help those dealing with breast cancer, from the newly diagnosed to those in treatment and every point afterwards.
Funds raised from Sendik's programs go toward funding ABCD's distinctive, free peer support efforts, including its Helpline, signature Mentor Match service and Mentor Survivorship support activities.
ABCD is one of four charity partners to whom Sendik's has donated a total of $1.6 million over the last five years, said Nick Bandoch, Sendik's communications manager.
 
Vick Family Farms aids local programs
Wilson, N.C.-based sweet potato grower-shipper Vick Family Farms is entering a fourth year of its Pink Pure Gold label for breast cancer awareness, said Charlotte Vick, a partner.
Pink Pure Gold cartons of sweet potatoes sold during the month of October, with a portion of the sales donated to Wilmed Healthcare Foundation's Breast Cancer Awareness Program in Wilson, N.C., Vick said.
Funds that the company raises and donates to the cause stays in the local community and provides funds for screening and patient support, Vick said.
"We are not a nationally known brand and don't necessarily raise millions to donate each year, but we've been able to make some sizable donations thanks to our customers who help us every year by carrying the Pink Pure Gold brand," she said. "We hope to continue to grow the label and in the future add 3-pound pink bags.