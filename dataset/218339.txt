There will, of course, be another attempt to end the government shutdown on Jan. 22.
According to the Associated Press, one potential proposal would extend the government funding by three weeks in return for a commitment from the Republican leadership in the Senate to address immigration policy.
Will Democrats or Republicans buckle in the budget talks? 
The average person may blame the whole lot of politicians.
For their part, politicians are taking pains to limit the fallout.
On June 21, U.S. Sen. Bill Nelson, D-Fla., issued a statement about one reason why he voted against the Republicans’ short-term spending bill Friday night (Jan. 19)
From Nelson’s statement:
“Another reason I voted against the Continuing Resolution Friday night was that it omitted the desperately needed hurricane disaster assistance to help Florida recover. Especially hurt is Florida’s citrus industry, which lost most of its oranges in the storm. They are now teetering on the brink of bankruptcy unless we can get them some help immediately.”
At some point, there will be no reason to extend this shutdown any longer. The sooner, the better.
---
I asked a question to the LinkedIn Fresh Produce Industry Discussion Group about implications of the truck shortage and the electronic logging device mandate.
The question was:
How are high truck rates/shortage of trucks affecting the “normal” movement of produce?
Are recent conditions preventing produce from moving to market? What are implications for suppliers and retailers/foodservice operators?’
Some responses so far:
GM Higher costs due to E-log have conferred an advantage to FL growers vs Mexican produce in major NE markets
AA It has had a major impact on pricing. The range in pricing from region to region has definitely increased the farther you move the fruit from its import destination.
DP I don’t feel the rates are high compared with the other rising costs of doing business in the transportation business. I do believe the E-logs have reduced the available truck capacity by 20-25 %. I do see that the shippers are going to improve loading time-at-dock and product consolidation for the effective movement of shipments due to e-logs.
JL This is just another incremental change along complex supply chains. The answer for fresh produce is to change the business model. When the collective cost of fresh produce is far higher than the cost of growing it the time has come to make changes. Schools and colleges all teach what distribution companies have known for a long time - the final mile is always the most costly. In the case of fresh produce that is often due to the disproportionately large amount of single-use transit only packaging needed to prevent crushing. Some address the headline issue with CEA or Vertical Farming - but this can only small savings. The answer is to change the business model and grow smaller quantities to order at the point of need. Our patents and trademarks are all about how we can make this work.
VM I am seeing countless shorted product due to the trucking situation throughout the entire value-added veg side as well as the value-added organic salads and conventional salads. I am an Optimist I am sure the trucking industry will figure it out learn to get their efficiencies worked out with the growers and get us back to or close to full service on the delivery supply side.
Check out the discussion and add to it this week....