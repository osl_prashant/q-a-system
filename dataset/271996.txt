Baby eel fishermen on track to catch quota after short years
Baby eel fishermen on track to catch quota after short years

The Associated Press

PORTLAND, Maine




PORTLAND, Maine (AP) — Maine's baby eel fishermen are on track to catch the entirety of their quota for the valuable fish after falling short in recent years.
The eels are sold to Asian aquaculture companies, and are part of the worldwide supply chain for Japanese food. Maine's the only U.S. state with a significant fishery for the eels, called elvers, and regulators limit them to 9,688 pounds per year.
The Maine Department of Marine Resources says fishermen have caught more than 4,150 pounds of elvers through Monday. That puts them in position to catch the whole quota this season, which ends in June.
Maine elver fishermen fell just short of quota in 2016 and 2017, and caught 5,259 pounds in 2015. They are averaging a record of $2,585 per pound at the dock.