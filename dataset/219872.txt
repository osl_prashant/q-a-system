Got milk?
If so, you may be among the majority of consumers who throw that milk out once the date on the carton or jug label has passed.
But Ohio State University researchers say not so fast — that pasteurized milk is still good to drink past its sell-by date.
Scientists in the College of Food, Agricultural, and Environmental Sciences (CFAES) say that arbitrary date labels on food contribute to significant food waste because the date labels serve only as an indicator of shelf life, which relates more to food quality than safety.
Brian Roe, a CFAES professor of agricultural economics, co-authored a new study examining consumer behavior regarding date labeling on milk containers. The goal of the research is to help consumers reduce food waste through improved food labeling systems and consumer education.
The study, which will appear in the June 2018 edition of Food Quality and Preference Journal, surveyed 88 consumers who were asked to sniff half-gallon jugs of milk that were 15, 25, 30 and 40 days past the date they were bottled. Some milk samples were dated and some were not dated.
The study found that 64 percent of respondents said they would throw the milk out that had a date label, while only 45.8 percent of respondents said they would throw the same milk out if they didn’t know the date label of the milk.

“Date labeling doesn’t tell you when a food will spoil,” said Roe, who also leads the Ohio State Food Waste Collaborative, a collection of researchers, practitioners and students working together to promote the reduction and redirection of food waste.
“Consumers often view dates as if they indicated health or safety, but those dates are really just about the quality of a product determined by manufacturers,” Roe said. “There’s a difference between quality and safety.
“Pasteurized milk is safe past the sell-by date unless it has been cross-contaminated. While it may not taste as good — it can go sour and have flavors that people don’t like and may make them feel nausea — but it isn’t going to make them sick.”
Roe said the study focused on milk because it is one of the most wasted food products in the United States, representing 12 percent of consumer food waste by weight. And past research suggests the date label is a critical reason why milk is discarded, he said.
“Innovations in date labels and explaining what the date labels mean will allow more consumers to save money by keeping milk longer and reducing food waste, which has social implications as well,” Roe said. “It’s very resource intensive to produce milk — from the land needed to grow feed for the cows, to the water used for cows to produce the milk, to the energy that goes into housing cows and to processing and transporting the milk.
“Not to mention the retailers, who spend a lot of time managing the milk case at the grocery store as well.”
Confusion regarding food label dates leads to significant food waste nationwide, with the average American household spending more than $2,000 annually on wasted food, according to a study by the Natural Resources Defense Council.
So what do the date labels on food mean?
According to the U.S. Department of Agriculture, the:

“Best if used by/before” date indicates when a product will be of best flavor or quality. It is not a purchase or a safety date.
“Sell-by” date tells the store how long to display the product for sale for inventory management. It is not a safety date.
“Use-by” date is the last date recommended for the use of the product while at peak quality. It is not a safety date except when used on infant formula.

“If we make changes to the date labeling, we have to make sure the regulatory system understands how the changes will impact their regulations,” said Dennis R. Heldman, a CFAES professor of food engineering, a member of the Food Waste Collaborative and a study co-author.
Heldman is also studying the effect on consumers of an indicator that would be attached to containers of perishable foods to monitor their shelf life. The indicator would gradually change color during storage and distribution of a food or beverage. So a change in color, say, from blue to red, would tell consumers that the product has reached the end of its shelf life.
“Using this method, consumers can be confident as to when the product should and shouldn’t be consumed,” he said.