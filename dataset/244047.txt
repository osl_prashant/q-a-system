Woman tells story of slave who taught Jack Daniels whiskey
Woman tells story of slave who taught Jack Daniels whiskey

By JESSICA BLISSThe Tennessean
The Associated Press

LYNCHBURG, Tenn.




LYNCHBURG, Tenn. (AP) — All Fawn Weaver could imagine was the sound of a gun being cocked over and over again.
On their second night in Tennessee's whiskey capital, Weaver and her husband, Keith, stood at the edge of a 10-foot hole that looked petrifyingly like a freshly dug grave.
It was after sunset. The field around them was tree lined and dark.
The man who led them there was a self-declared redneck.
"We were sure we were going to be shot or buried alive," Weaver recalls.
But as Weaver soon discovered, nothing in this small Southern town was exactly as it seemed.
She had come to Lynchburg to research the roots of a former slave named Nathan "Nearest" Green, the man who some believed had taught the famous Jack Daniel to make whiskey more than 150 years ago.
Weaver's husband never wanted to set foot in the place, objecting as a black man to visiting any town with the word "lynch" in its name. He preferred Paris for his wife's 40th birthday.
But Weaver, an intrepid entrepreneur, investor and New York Times best-selling author from Los Angeles, had a mission — and a research trip to Lynchburg, the home of world-renowned Jack Daniel Distillery, was her celebratory request.
So the couple flew across the country, ready to explore the back roads and back issues in a potentially backward town.
What they uncovered was an unexpected friendship between a black slave on loan to a wealthy preacher and a white teenager who worked at that preacher's grocery store — a relationship that launched a multibillion-dollar whiskey company and contradicted what many assume about race relations in the South.
It was only by chance that they were there at all.
It began with a trip to Singapore and an article in the International New York Times.
Overseas on a business trip in June two years ago, Weaver unfolded the Times and was met with a surprising headline.
Jack Daniel's Embraces a Hidden Ingredient: Help From a Slave
The story revealed a complicated and long-concealed tale.
For more than a century, Daniel's distilling prowess had been credited to a man named Dan Call, the local preacher and grocer understood to have employed a young Daniel on his farm and taught the boy everything he knew about whiskey.
But last year, the company came forward with a possible new twist — Daniel didn't learn to distill from Call, it said, but from a man named Nathan "Nearest" Green, one of Call's slaves.
A man who would become Daniel's teacher and mentor and friend.
Weaver read the details and gasped. The slaves of the segregated South were generally treated as property, not people — objects transferred like deeds upon a white owner's death.
Green's unlikely story, built on oral history and a tenuous trail of archives, disputed that.
"This story will never be accepted unless it is indisputable," Weaver thought, wanting to know more.
She Googled Green's name. Her search returned nothing but a newly created Wikipedia page referencing the book "Jack Daniel's Legacy." She ordered it.
And she read it voraciously, tagging every page where Green and his sons were mentioned. There was more than 50.
For a book published in 1967, that was significant.
The '60s marked the contentious apex of the civil rights era. Sits-ins. Selma. Assassinations.
And yet, here was a biography that candidly detailed a relationship between "Little Jack" and "Uncle Nearest" — at a most unlikely time.
"To have another family, a black family, mentioned that many times is insane," Weaver says now. "That's how I knew the story was a little more special."
And so the journey began. The Weavers booked a cross-country flight to Tennessee and rented a place to stay near the Lynchburg town square.
They kept the reason for their trip a secret, telling their family only that they were going on a "whiskey tasting" in the South.
When Weaver first entered the unfamiliar community to research Green and forever alter the story of Jack Daniel Distillery, she did it with no fear. No worries about racism, no feeling of danger.
A vibrant and relentless businesswoman with bright hazel eyes and a loud, joyful laugh, Weaver has always focused on finding the positive.
"I'm not a fearful person like that," she says, navigating her sporty white BMW down the tree-lined two-lane roads of Lynchburg. "The way I look at it is if someone is racist it's their issue, not mine."
What Weaver began to discover was that Lynchburg, at the time of Jack Daniel and Nearest Green, was perhaps a more progressive place than most — surrounded by the South's racial tension, but in some ways, less susceptible to it.
Without question, there were troubled stories in its past. In 1903, one newspaper reported a story about "a mob that wanted to lynch a negro" and assailed the Lynchburg jail. The sheriff fired on the mob, killed one man and captured three others.
The mob, however, reached its victim and shot him to death in jail.
But that type of event did not define the town. In the decades before desegregation, whites and blacks worked, lived and played together, according to Green's descendants and Weaver's research.
"Lynchburg is a unique town, no doubt," says Debbie Staples, Green's great-great-granddaughter. "Were there things that happened? We're talking about the South. Did I face racial discrimination or racial tension? No."
And, Staples adds, "I don't think my grandmother did either."
Staples grew up in Lynchburg in the '60s, raised by her grandmother, her Mammie. She would often sit on the front porch at Mammie's modest white home on the corner of Elm Street. From there, one could see out to the hollers and up to where the Jack Daniel's warehouses stood. It was a place where the town gathered to gossip.
Mammie would recline in her rocking chair, looking out at the small valleys between the hills, and tell stories.
"My granddaddy made the whiskey for Jack Daniel," Mammie would say.
"Oh, yeah?" her grandchildren would respond skeptically. "OK."
But maybe this place, this town, did have something different about it.
There were only two businesses in town — the pharmacy and a place called the Coffee Cup — that followed Jim Crow laws, forcing black patrons to enter through the back. The Green descendants would often go downtown to get a snack at the cafe or an ice cream without any trouble, Staples' brother Jeff Vance recalls.
They didn't go into some restaurants or the swimming pool. "It wasn't like they said we couldn't come," Staples says. "We just knew."
But down the road from Mammie's house, at the home of a white family where Staples' Auntie worked, all the grandchildren — white and black — sat and ate dinner at the same table together.
"We were like family," Staples says.
And that, it seemed, was like what Nearest Green was to Jack Daniel, too.
Nearly two years after her first research trip to Lynchburg, Weaver stands in a room at Dan Call's farm — the place where Daniel and Green first began making whiskey together — and marvels at its contents.
Photographs of Green's descendants, well-dressed men with bow ties and ladies in brimmed hats and lace, cover the walls.
Dozens of handwritten letters between Daniel's descendants lay on the tables, their contents detailing the comings and goings of "Uncle Jack."
In one corner of the wood-paneled room hangs the complete family tree of Nathan "Nearest" Green.
And on the mantel above the fireplace stand bottles of Tennessee's newest brand of whiskey, Uncle Nearest — a company created by the Weavers to honor the original master distiller.
Perhaps surprisingly, the community and Jack Daniel's have embraced it. The distillery has recognized Green as its first master distiller and incorporated mentions of Green in all its tours, consulting Weaver's research.
And though there is no known photo of Green, one of his son now hangs on the master distiller wall of fame.
Weaver never expected it to be this way. When she first came to Lynchburg, she fully believed that she would tell one version of Jack Daniel's history and the company would tell another. Instead, they have worked hand in hand.
"I think it's really important," Eddy says of Jack Daniel's inclusion of Green. "And I think it's a wonderful story.
"It's bigger than Jack Daniel's and it's bigger than Lynchburg, Tennessee. At a time when there might be divides, it's a story in a very difficult time of two people coming together and color not being as much a part of that relationship as we might have expected it."
Two legacies — not just one — now emerge.
Weaver's love for Daniel, for Green and for the town where they made history has kept her in Tennessee much longer than she planned.
Lynchburg is now home.
She and her husband live in the tiny carriage home next to the historic Tolley House, where several generations of Jack Daniel's distillers once resided and which they now own.
Keith, who is an executive vice president at Sony Pictures Entertainment, travels back to L.A. for work. Fawn, who loves horses almost as much as history, throws on her jeans and farm boots and revels in country life.
Neighbors know them by name. They check in when strangers visit. They repair a fence post when it's broken.
"I am embarrassed, in a way, to share I had a bias against some towns in the South," Keith Weaver says. "What you may perceive in terms of a person or a place may not be true.
"The humanity that has been uncovered through this story and the relationship ultimately between Nearest and Jack can be inspiring."
Fawn Weaver feels that, too. As she drives through town, she points out the home of Green's descendants. She slows to look at the land purchased to create the Nearest Green Memorial Park, which will be nestled right next to Mammie Green's house.
And then she stops at the cemetery where Green is buried.
It's less than a football field away from Daniel's final resting place, a massive headstone where tourists from all around the world leave flowers and shot glasses. But, until recently, Green's gravesite was obscured, secluded in the segregated part of the cemetery.
In December, as part of the rededication of Highview Cemetery orchestrated by Weaver and the Nearest Green Foundation, a new cemetery entrance was built and a towering stone memorial was erected for Green.
"Now you can see Nearest from Jack," Weaver says with a smile.
Weaver isn't finished spreading Green's story just yet. There is the whiskey, a pending book, maybe a movie, and next up, a soon-to-be-built distillery in Shelbyville.
The Nearest Green Distillery will stand on the 270-acre Sand Creek Farms, a historic Tennessee walking horse farm and event center. In time, they plan a 3,500-barrel rickhouse, a gift shop and a tasting room.
The 600-seat show arena on the site will be reimagined and double as a private concert venue.
When Weaver talks about the next stage, making her way through the barns and petting the elegant walking horses that will remain on the property, she glows.
Then, standing in the performance ring of the show barn, Weaver reflects on all that has happened in the last two years.
She recalls that fearful experience when she and her husband first arrived — the panic that she might be buried alive — and she laughs.
It was the middle of their second long day of research. The Weavers were getting dinner at a local barbecue restaurant and making conversation with the owner, Chuck Baker.
Muscular and bald, Baker had an intimidating build but a friendly presence. Knowing they were new to town, he invited the couple for drinks after he finished work.
Keith Weaver was cautious, but Fawn saw it as a research opportunity.
Being from L.A., they expected to meet Baker at a bar or another spot in town. Instead, they found themselves following Baker's truck down unfamiliar Tennessee back roads behind the distillery as the sun set.
Twenty minutes later they were pulling down a long dirt road driveway to Baker's house. After hanging out on his front porch with some beers, Baker said, "Hey, I want to show you something."
He went out back, to a place beyond the house lights, and stopped in front of a freshly dug pit. That's when terror truly set in.
"For the first time in my life, I was certain I was going to die," Weaver says. "This was my final resting place."
But, as Weaver would uncover many more times in the next two years, the situation was not as it seemed.
Baker had dug the pit for a photo shoot of his pigs, which were featured in the Jack Daniel's Tennessee Squire calendar.
"A photographer had climbed into the pit and taken pictures of the pigs looking down over the side," Weaver says with a boisterous laugh.
Baker is now one of the couple's closest friends in Lynchburg.
As Weaver has learned, people can often surprise — in positive ways.
Jack Daniel's certainly has.
"We have the enormous responsibility of raising up one legend without harming the legacy of another," Weaver says. "Because I have no doubt if Nearest was here he would be raising a glass to Jack and Jack would be raising a glass to Nearest."
___
Information from: The Tennessean, http://www.tennessean.com