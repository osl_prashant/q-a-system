Chicago Mercantile Exchange live cattle contracts on Wednesday settled up the 3-cents per pound daily price limit, driven by fund buying and futures' discounts to stronger-than-expected cash prices, said traders.Funds that follow the Standard & Poor's Goldman Sachs Commodity Index at times sold, or "rolled", August futures and simultaneously bought October on the third of five days of the Goldman Roll process.
August and October closed limit up at 117.875 and 117.825 cents, respectively. They finished above their respective 20-day and 40-day moving averages of 115.889 and 116.152 cents.
CME live cattle's trading limit will expand to 4.500 cents on Thursday.
On Wednesday a small number of market-ready, or cash, cattle in Texas and Kansas brought $120 per cwt, up as much as $3 from a week ago, said feedlot sources. They said remaining cattle sellers are holding out for more money.
Animals at Wednesday morning's Fed Cattle Exchange sold from $117.25 to $118.75 per cwt versus last week's $117.75 average.
Cash prices appear to be bottoming out with beef now being more competitively-priced to pork, said JRS Consulting owner Jack Salzsieder.
The unexpected cash price bounce and seasonally slumping wholesale beef values trimmed packer profits, although they still remain high historically, a trader said.
CME feeder cattle settled up the 4.500-cent price limit, led by technical buying and live cattle futures' limit-up move.
August feeders ended limit up at 151.750 cents per pound. Feeder cattle's limit will increase to 6.750 cents on Thursday. 
Firmer Nearby Hog Contracts
Front-month CME lean hogs gained, helped by higher wholesale pork values and futures' discounts to the exchange's hog index for July 10 at 92.59 cents, said traders.
Deferred months slipped after investors sold those contracts and bought nearby futures with the view that cheaper feed might encourage farmers to increase production.
July, which will expire on Monday, ended up 0.425 cent per pound to 92.575 cents. Most actively-traded August finished up 0.375 cent to 82.625 cents.
Retailers and restaurants purchased pork to accommodate weekend barbecues, which also comes at a time of year when bacon-lettuce-tomato sandwiches are popular, a trader said citing sharply higher rib costs and record-high pork belly prices.
He said Wednesday's USDA weekly hog weight data suggests hogs may have backed up on farms over the U.S. Fourth of July holiday.