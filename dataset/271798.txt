In the first sale of the year, April 20, the 150 Show-Me-Select replacement heifers averaged $1,555 per head. The bred heifers will calve this fall.
The Farmington Livestock Auction in Missouri is the newest in a statewide network of guaranteed heifer sales. This was the first of four spring sales across the state of SMS heifers.
"It was a rough sale," said David Patterson, beef reproduction specialist, University of Missouri Extension. "The crowd was small."
The area was hit by drought last fall and winter. "Most herd owners are out of hay," Patterson said. "With this late spring, grass isn't growing in their pastures."
There was good news. "Buyers got bargains," he said.
Show-Me-Select sales attract national attention for providing calving-ease heifers that cut death loss at birth. Also, a growing number of heifers are bred by fixed-time artificial insemination (FTAI). All heifers in a herd can be bred on one day. That shortens calving seasons, reducing labor.
With AI breeding, the top proven sires in a breed can be used. Even small cow herds gain improved genetics.
The top average price of $1,860 on five heifers went to Turner Farms, Belgrade. Sale high of $2,000 went to a Turner AI-bred registered Angus. She carries a bull calf.
Tom O'Loughlin, Oak Ridge, was volume buyer, taking home 42 heifers. David Golhofer, Salem, bought 22 heifers. "Both Tom and David were new buyers," said Kendra Graham, MU Extension livestock specialist, Farmington.
"The Farmington sale gives a place for producers in east-central Missouri to sell Show-Me-Select heifers," Graham said. SMS producers enroll in a yearlong education course to improve their herds.
Patterson noted heifer quality improved since the start at Farmington three years ago. "That always happens," he added. Consignors and buyers learn the value of quality.
Buyers learn they can pay more for SMS bred heifers. "Repeat buyers make a sale," Patterson said.
"Six of the 14 consignors in the sale were new to Show-Me-Select," Graham said. "The quality was good but buyers were cautious."
Bidders were also new. "Half the buyers were first-timers," she said.
Patterson heard concern from farmers about the impact of trade tariffs on beef exports. "There's real concern about the farm economy."
All SMS heifers are guaranteed pregnant. They are pregnancy-checked at least twice, the second time within 30 days of selling.
The sale catalog gives estimated calving times. Ultrasound pregnancy tests can show sex of the calf.
The other sales and extension coordinators are:


May 4, 7 p.m., Fruitland (Mo.) Livestock Sales, Erin Larimore, 573-243-3581.


May 18, 7 p.m., Joplin Regional Stockyards, Eldon Cole, 417-466-3102.


June 2, 6 p.m., F&T Livestock Market, Palmyra, Daniel Mallory, 573-985-3911.


All SMS heifers are checked on arrival at the sale barn by Missouri Department of Agriculture graders. Any not meeting standards are sent home.
"I hope the sale continues to grow and attract new consignors and buyers,' Graham said. Cow-calf producers can enroll in Show-Me-Select with their regional extension livestock specialist. Information on Show-Me-Select and sales is at agebb.missouri.edu/select.
Heifers must meet standards for reproductive soundness, pelvis size and vaccinations. Sires in the program meet high calving-ease scores on EPDs (expected progeny differences).
Most replacements go back into their home herds. Regional sales market extra heifers. Producers learn SMS cows stay in their herds longer. That gives extra heifers to sell, a new profit source.
Better genetics improve not only heifers but also steermates. The right sires produce quality beef. More calves grade USDA prime, bringing premium prices at packing plants.
Missouri has the second-highest number of cows in the nation.