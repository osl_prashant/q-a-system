BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Mon.Fri.No. 2 Soft wheat4.81¾4.80¼No. 1 Yellow soybeans10.1610.14¼No. 2 Yellow Corn3.78¾e3.78½eNo. 2 Yellow Corn3.84¾p3.84½p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted