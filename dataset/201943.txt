The past few days have been a whirlwind for The Maschhoffs.On Wednesday, PORK Network made the company, one of the nation's largest pork producers, aware of undercover video footage alleging animal abuse at one of its breeding facilities in Nebraska. The video was released by the activist group Animal Legal Defense Fund.
Upon viewing the footage, The Maschhoffs wasted no time in responding to the situation to ensure all standards of animal care are being met across their network of sow farms in the state.
"We are better than this. The Maschhoffs has zero tolerance for any abuse or mistreatment," Josh Flint with The Maschhoffs told PORK Network. "This is not in line with who The Maschhoffs are. We will ensure it's corrected."
He added, "We owe it to our animals and our customers."
The Maschhoffs President Bradley Wolter said, "As animal caregivers with a long-standing history of excellent animal welfare, we are appalled by the level of animal care depicted in the video at this sow farm. We are aggressively implementing improvements that will help to ensure excellent animal care every day and on every farm, and prove our ongoing commitment to the responsible and humane care of our animals."
More specifically, The Maschhoffs have:
Terminated the farm manager at the sow farm where the footage was shot;
Conducted a series of independent audits at its Nebraska sow farms;
Immediately initiated extensive re-training on proper animal care and handling practices on farms through the Maschhoffs network with a specific focus on Nebraska facilities;
Launched intense, internal communications for all employees to fully understand their obligations to report instances of animal abuse and neglect; and
Completed an exhaustive review of the video and its animal welfare records from the facility to identify issues and critical improvements needed.
Click here to read more about The Maschhoffs' action.
Hormel Foods Corp. has also suspended the company as a supplier until certified third-party auditors complete their own investigation. The Neb. Attorney General Doug Peterson and Ill. Attorney General Lisa Madigan have also launched their own investigation into the allegations.
See, "The Maschhoffs target of abuse allegations"