New farm bill but same wish lists






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






New farm bill hearings are like past years, with farm group lobbyists and others saying they want more funding and to protect crop insurance. Meanwhile, USDA Secretary-nominee Sonny Perdue just might get a Senate confirmation vote before the Senate departs for Easter.
Perdue nomination vote today at Senate panel. The Senate Agriculture Committee plans to vote today on the nomination of former Georgia Governor Sonny Perdue for USDA Secretary. The committee said the nomination vote will occur near the Senate chamber following a Senate floor vote, but the timing will depend on the still-to-be announced Senate schedule.
Only a few Democrats are expected to vote against Perdue during today’s panel vote; all Republicans are expected to support him.
Senate floor vote soon? Senate Agriculture Chairman Pat Roberts (R-Kan.) said he hopes to get a floor vote on Perdue before Congress breaks for its Easter recess at the end of next week. Perdue told lawmakers that, if confirmed, he would get to work quickly on trade and immigration issues facing agriculture.
Highlights of House hearing on commodity programs and new farm bill. 

Corn, soybean, wheat, barley and sorghum grower groups told a House Agriculture subcommittee that the 2018 Farm Bill should tweak price and revenue programs to provide bigger payments in times of low prices. Witnesses also said the bill should revise the programs for more timely payments after a crop year.
American Soybean Association President Ron Moore said farm and nutrition programs should remain in the 2018 Farm Bill because critics calling for separate bills want “to defeat rather than pass a new farm bill.” Moore said an agriculture-anti-hunger alliance generates rural-urban support for farm bill passage.
A joint statement signed by grain and oilseed groups as well as the American Farm Bureau Federation and National Farmers Union said the Agricultural Risk Coverage (ARC) and Price Loss Coverage (PLC) safety nets should be preserved and made “more effective and fairer to all farmers.”
David Schemm, president of the National Association of Wheat Growers, said that allowing farmers to choose between ARC and PLC “is a critical component” of the farm bill.



Comments: We never get too excited about early action on a new farm bill. Reason: so many things can and usually do change. Like the last farm bill. We remember when the Senate’s proposed farm bill had only a $50,000 pay cap for the farmer safety nets of ARC and PLC… how would you corn and soybean growers getting all those ARC payouts feel about that now? The pay cap language was significantly changed by the House Ag Committee farm bill proposal.
We also find it interesting that some farm groups are again asking for a “one-time” update of acreage bases. Recall there was an option to update via the 2014 Farm Bill. This could eventually be a World Trade Organization challenge.
We want to know how significant generic base language is going to change because that is clearly the way funding is going to be found to finally get cotton an adequate safety net, which they do not have now relative to the 2014 Farm Bill.
We also want to know how a way will be found to deal with a big price tag for getting an effective dairy safety net. That does not mean it can’t happen… we’ve heard of some end-arounds under discussion. Dairy usually gets what it wants… unless their program is shaved a lot in the final days of the farm bill debate, like the last farm bill process.
Also, if higher reference prices are ahead, what are the potential budget costs for King Corn? Each one-cent payout is a whopper with all those base acres.
Which brings us to this observation: With all the hearings ahead of the 2013, whoops, 2014 Farm Bill, why did so few point out that the cotton and dairy “safety” nets would not work out? Those that did were not listened to. You will read a lot about 2014 Farm Bill options and analysis, but remember how the current farm bill worked or didn’t work. 






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.