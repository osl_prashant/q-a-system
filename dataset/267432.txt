Dallas-based family's gardening company still growing
Dallas-based family's gardening company still growing

By CHERYL HALLThe Dallas Morning News
The Associated Press

DALLAS




DALLAS (AP) — The Ruibals joke that they love folks with brown thumbs because they make the best repeat customers.

                The Dallas Morning News reports in actuality, the family would much rather turn those big digits into green ones — just as it has been doing for more than three decades in the Dallas Farmers Market District.
In 1984, a dead-broke Mike Ruibal (pronounced ROO'-bal) came to Big D and began selling bedding plants with his brother, Ed, from a pickup in the parking lot behind the sheds.
A good week that first year was clearing $250 after paying their $30 daily rent for their double-deep, 10 by 20 parking space, any hired help and restocking.
Today, Mike and his wife, Linda, own Ruibal's Plants of Texas, which, with the help of their two eldest sons, will bring in nearly $20 million in sales this year.
Ed Ruibal founded Landscape Systems of Texas in 1983. Ed and his brood now operate a seven-acre garden center and 10,000-square-foot gift shop in Keller.
This time of year, Ruibal's flagship nursery, which covers two city blocks along South Pearl Expressway, is like a mini-arboretum.
If the weather cooperates, Ruibal's will sell nearly $1 million in bedding, hanging basket, succulents, cacti and accent plants in the next three weeks at its four locations, which include  Lemmon Avenue near Love Field, Lakewood near White Rock Lake and Rosemeade on the Dallas border with Carrollton.
The seasonal gardening fury will continue through July Fourth, when Texas' brutal heat sets in. Business will pick up again with ornamental cabbage and kale, chrysanthemums, pansies and pumpkins in the early fall.
Mike Ruibal and his clan have fended off the onslaught of big-box interlopers by catering to both homeowners and landscapers, growing their own Texas-hardy plants on 79 acres of nearby farms in South Dallas, and being a destination for local gardening knowledge.
Thinking about planting periwinkles right now? You'll have to wait a few weeks before you can buy them at Ruibal's. A wet, cool April will have you replanting them. Plant petunias instead.
"We're saving people from themselves," says eldest son Mark.
Mark, 47, runs the company's marketing and retail operations, while Matt, 44, directs growing at the company's two farms in South Dallas — the largest of which has 130 greenhouses and covers 65 acres within the city limits.
Mark and Matt say their separate-but-equal roles avoid day-to-day brotherly head-butting.
"There's 11.4 miles of buffer zone," says Mark, referring to the distance from downtown to the main farm just south of where Interstate 20 turns into Interstate 635.
Maverick patriarch Mike, 66, is a fly-by boss who's a bit of a flamethrower.
"I run my part. Mark runs his part," says Matt. "And Dad sets fires for me and Mark to put out."
"Mom's the one who keeps everything together," says Mark. "She keeps track of Dad, which is no small job."
Matriarch Linda, 65, loves her role as cat herder.
"Mike tends to shoot from the hip. If he sees something, and it looks right, he does it," says his wife of 47 years. "The CPAs, the lawyers are like, 'You can't do that. You need to plan this.' And he'll say, 'No, we're doing it.'
"It drives everyone crazy. Then we all catch up and put it in order."
Mike and Linda were high school sweethearts in Fort Morgan, a tiny rural town on the Colorado eastern plains, where his father was a junk hauler and landscaper and raised a few pigs.
That's where Mike learned to hustle.
Mike's dad would bring scrap metal into the junkyard. Mike would take and hide this new inventory while his dad wasn't looking, and then sell it back to him a few days later.
"He was always looking for an edge," says Linda. "Still does."
Mike hated school, so instead of studying, he made money as a pool shark and playing drums in rock 'n' roll and country bands. He was so good at shooting pool that he competed successfully in Vegas tournaments.
But when Mark came along before Mike and Linda graduated from high school, he decided he needed more stability to raise a family and began selling shoes.
In 1973, when Mark was 3 and Matt was a baby, Mike uprooted the family to Wyoming to sell men's clothing.
"We moved to Laramie in December. How stupid is that?" Linda laughs.
Seven years later, Mike opened his own store, selling trendy Botany 500 and Britannia suits, blue jeans and polo shirts.
The family of six lived high on the hill until the oil bust, when Mike lost everything and declared bankruptcy.
In 1984, Mike followed brother Ed to Dallas, where Ed had gotten a job with a landscape company in Las Colinas and was selling landscape castoffs at the Farmers Market. That July, Mike rounded up Linda and their four kids and moved them here.
"Moving to Dallas in July? Another baptism by fire," Linda says.
Mike stocked his ever-increasing Farmers Market presence by making three round trips to East Texas several times a week to pick up flats of colorful annuals. He'd leave his house in Seagoville at 6 a.m. to be in Jacksonville or New Summerfield when the nurseries opened. He'd drop off the final batch of flats around midnight to sell the next day.
"I had plants stuffed on the dashboard, on the seats, everywhere I could put them."
As the business grew, demand outstripped dependable East Texas supply.
In 1990, Mike drove by a 14-acre farm that had been lying fallow for years and noticed a for-sale sign.
He called and learned that the seller would take $68,000 for it, but the deal had to be completed in three days.
Mike's bankruptcy had trashed his credit rating, so he called one of his biggest customers who'd promised to help out Mike if he ever needed it. The client immediately gave Mike a loan with an open-ended payback.
Growing their own plants dramatically reduced costs for the Ruibals and made it possible to compete for larger wholesale landscaping business. That helps to even out the seasons.
During the fall, about 60 percent of its sales are to wholesale landscapers. In the spring, it flips to 65 percent retail.
Ruibal's trucks in about 30 percent of its inventory — mostly tropicals and locally sourced plants in California and Florida.
Ruibal's opened its second location near White Rock Lake in 2006.
Then came Love Field in 2011, which draws retail customers from the Park Cities and wholesale landscapers in the warehouse district. It's the second-highest grossing store by far, Mark says. Ruibal's bought Rosemeade Market in 2013, which also sells natural foods.
Landscaper Noel Pullam goes to the downtown Ruibal's almost every day.
"They're good about getting deliveries out for me," he says. "They make my life easy, because there's so many times when I need things last minute."
And because the family grows its own plants, it knows what's going to die or thrive, Pullam says. So if he comes across a plant that's interesting, he'll ask the Ruibals to grow it for him. "Some of the things I've requested have become mainstays several years later as everybody in the landscape industry realizes that they're a good product."
It's not only clients who have rewarded Ruibal's with loyalty.
Mike hired his first employee, Chon Piedra, 34 years ago to help with the Farmers Market stall while he was making runs to East Texas to buy bedding plants. Piedra is a buyer for the company today.
"People start here, and they don't seem to leave," says Mike.
___
Information from: The Dallas Morning News, http://www.dallasnews.com


This is an AP Weekend Member Exchange shared by The Dallas Morning News