For 2017, the national average for cash rents on cropland is $136 per acre, which shows no change from the 2016 national average. So what states have the highest cash rents in 2017?Here are the top 10:
State
Cropland Cash Rent ($/acre)
California
$325
Hawaii
$282
Arizona
$250
Iowa
$231
Illinois
$218
Washington
$198
Indiana
$195
Nebraska
$194
Minnesota
$166
Idaho
$160
 
Curious about the other end of the spectrum? The lowest state-average cash rents are in these 10 states:
State
Cropland Cash Rent ($/acre)
New York
$61
Virginia
$60
Alabama
$58
Wyoming
$56.50
New Hampshire
$54
South Carolina
$46.50
West Virginia
$42
Texas
$40.50
Oklahoma
$33
Montana
$31.50
                                           
Don’t see your state? Here’s a full data set on 2017 state-average cropland cash rents from USDA.