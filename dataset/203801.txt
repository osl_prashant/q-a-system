Chilean avocado growers are expected to produce more fruit this season, but volume exported to the U.S. should be about the same as last year, said Karen Brux, managing director of the San Carlos, Calif.-based Chilean Avocado Importers Association.
Total volume harvested during the 2016-17 season is expected to grow from 180,000 metric tons to 200,000 tons partly because of "some healthy rainfall" following a period of drought.
Last year, about half of the country's fruit remained in Chile, and 90,000 tons were exported. About 11,000 tons arrived in the U.S. for a few retail accounts, she said.
"We will continue to work with a few key retail chains and develop targeted marketing programs to support their sales of Chilean avocados," Brux said. 
"If market conditions are supportive, there could potentially be opportunities to expand our U.S. program," she said.
Most of Chile's exports last year went to Europe because of strong market conditions there.
Dana Thomas, president of Index Fresh Inc., Riverside, Calif., said he does not expect to see many Chilean avocados this season, since most of the fruit is destined for Europe or is already committed to specific U.S. customers.
Fruit that does arrive should be good quality, though.
He said that quality always is good during peak season from the various growing areas.
"If you're marketing fruit that's in season, it's good fruit," he said. "The market doesn't take poor-quality fruit."
There's no benefit to growers to shipping poor-quality fruit, he said.
Although most Chilean avocados are available during the fall and winter, some fruit can arrive as early as July, said Gary Caloroso, marketing director for Giumarra Agricom International, Escondido, Calif.
Chilean growers, which have strong markets domestically and in Europe, consider a number of factors before deciding how much fruit to ship to the U.S., he said.
Those factors include the currency exchange rate, market conditions in the U.S. and volume shipping from Mexico.
Chilean avocados have performed well in the U.S., and quality has been good, he said, but shipments typically are tight because of high demand from other markets.
Chilean growers don't ship as many avocados to the U.S. today as they did in the past, agreed Phil Henry, president of Henry Avocado Corp., Escondido.
"The demand for hass avocados has continued to grow in many parts of the world, including South America, Europe, Japan and now China, as well," he said, and Chile has stepped up to help fill that demand, taking pressure off certain other countries.
But Chile typically plans to ship much of its fruit to Europe, Henry said, and changing those plans can be a challenge.
"Even if they chose to change some of their shipments, it still could take a while to get here," he said.
A "small, but growing volume" of Chile's avocados were exported to China last year, Brux said.
In 2015, Chile exported 5,300 tons to China, and in 2016-17, the number is expected to increase, she said.
"The Chilean Hass Avocado Committee is also investing in a larger marketing program in China, with plans for in-store promotions, social media programs and cooking activities," she said.
Harvesting of small volumes began in Chile around July 11, and growers expected the harvest to be in full swing by mid-August.
"We're continuing to monitor the orchards and will only harvest when our avocados reach a degree of maturity at which they'll ripen appropriately and deliver a great eating experience for the American consumer," Brux said.

Shipments should continue through March.