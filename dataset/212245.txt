After watching greenhouse tomatoes and creamer potatoes move from commodity to cool thanks to great flavor and marketing, Quinton Woods thinks carrots are next.“Everyone sees carrots as the cheap option on the shelf and retailers love promoting them,” said Woods, sales manager for Gwillimdale Farms in Bradford, Ontario, which has just completed a company-wide rebranding.
“Last summer’s consumer research told us that shoppers aren’t concerned about price,” he said, “but they do want their carrots to be sweet, clean and crisp.”
Gwillimdale’s new bag plays up the carrots’ attributes, he said.
“Consumers don’t want traditional carrots,” Woods said.
“With all the different nationalities in Toronto in particular, there’s more pressure every year for new offerings in the category.”
“Our goal is to stand out on the shelf and get consumers coming back for the taste and quality,” he said.
 

Nantes carrots grow

Gwillimdale is one of several Ontario farms growing Nantes carrots, which have gained popularity, especially at farmers markets.
Another Holland Marsh grower, Riga Farms, launched a 1-pound Nantes retail pouch in January.
Bradford, Ontario-based Hillside Gardens grows Nantes in Georgia for the Quebec market, said sales and marketing associate Steven Kamenar.
Paul Smith, co-owner of Queensville, Ontario-based Smith Gardens in the Holland Marsh, agrees Nantes are attractive, tasty and grow quickly for the early market, but he said they require twice as much land to get the same tonnage as the 500 acres of regular carrots he sells by the tractor trailer load.
Jason Stallaert, president of Nature’s Finest in Pain Court, Ontario, an hour east of Detroit, said the 1-pound tray packs of Nantes carrots he’s produced for the past two years are “growing slightly” every year.
“We grow a lot of cello carrots as Nantes to get a better, sweeter-tasting carrot,” Stallaert said.
Nature’s Finest also over-winters 60 acres of carrots under straw — an expensive process — for sale in April, May and sometimes into June.
“Consumers want year-round local crops, so we’ll be extending our local storage and doing more winter-overs (plans are for 100 acres) to try and close that gap,” he said.
In the meantime, Nature’s Finest is in the midst of its own market research to figure out how to make carrots more appealing to kids and millennials.
“We’re thinking along the lines of making it easier for consumers to eat carrots,” Stallaert said.
“They don’t want to peel carrots anymore but at the same time it has to be cost-effective — at the end of the day it’s still a carrot.”