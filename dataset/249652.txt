Pamela Riemenschneider recounts some of the most interesting moments at Southern Exposure, from the keynote speech by Whole Foods co-founder John Mackey to the coming analytics innovation by Kroger.
If you're short on time, feel free to skip straight to the highlights:
0:37 Get the scoop on retailer attendance at the show.
1:40 Hear what Mackey had to say about the integration with Amazon.
4:19 Get the early word on Kroger's plan to introduce a service that combines nutrition advice and analytics.
For more on that panel with retailers, check out Pamela's report for Produce Retailer.