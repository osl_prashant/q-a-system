If there’s a one–two punch that can result in a knockout blow to soybean yields, it’s likely to come from the damaging duo of sudden death syndrome (SDS) and soybean cyst nematode (SCN). In severe cases, the two can easily contribute to between 50% and 70% yield loss in soybeans.
In 2015, 2016 and again this year, the Farm Journal Test Plots program evaluated the efficacy of ILeVo seed treatment to prevent yield losses to SDS and SCN. Farm Journal Field Agronomists Ken Ferrie and Missy Bauer considered yield data and return on investment (ROI) of the treatment, which is offered by Bayer CropScience.
 
When ILeVo is paired with a fungicide plus an insecticide, three modes of action can be delivered to the seed to protect it from foliar and root-rot phases of SDS, Bauer says. Plus, ILeVo addresses all plant-parasitic nematode species in the seed zone, including SCN, which, when present, can exacerbate SDS.
In 2015, Bauer evaluated the increase or decrease in soybean yield and net income results from ILeVo plus a fungicide/insecticide combination seed treatment versus a fungicide/insecticide combination seed treatment alone. That year, the study consisted of two plots planted in Indiana in mid-May with six replications.
In 2016, Bauer repeated the study with similar protocols and data collection. She planted two test plots on April 25 in Steuben County, Ind. One plot had four replications and the second had six.
In each case, the ILeVo plus a fungicide/insecticide seed treatment combination outperformed the fungicide/insecticide-alone seed treatment in yield and ROI. The yield increase, averaged between 2015 and 2016, was 3.32 bu. per acre. The increase in net dollars, again averaged for the two years, was $18.57 per acre.
Bauer was somewhat surprised to see the significant positive difference between the two seed treatments.
“Where we ran the plots, we didn’t have any visual signs of SDS in either 2015 or 2016, and there were only low levels of SCN present,” she explains, noting her team pulled benchmark soil samples early each year and again late-season for comparison.
With the low levels of disease and pest pressure, Bauer is now trying to pinpoint the factors that specifically contributed to the yield increases she observed during those two years.
“Certainly, we’re seeing some improvement in the [quality of the] stands, but I think most of the yield increase is coming from the stay-green effect,” she says. The concept is that healthier soybean plants stay greener longer, which supports pod development and bean fill.
For the 2017 season, Bauer decided to do more in-depth analysis in the plots. During the growing season she observed the lower nodes on plants in the study appeared closer together in the ILeVo plus fungicide/insecticide-seed-treated plots versus where only the fungicide/insecticide treatment was used. “We may be ending up with more total nodes on the plant and, therefore, more pods or seeds,” she notes. Bauer plans to have the answers to these questions once she crunches 2017 data.
For farmers making their seed treatment selection for 2018, Bauer says her data indicates the ROI with ILeVo warrants consideration. “It’s another tool in your toolbox that offers protection against two big pests, and your chances of having a positive result that pays off even with little or no pressure is impressive,” she adds.
This year, Ferrie also conducted a study he hopes will shed additional light on ILeVo’s performance before farmers go to the field next spring.
“What I believe ILeVo could do is help farmers in central Illinois go back to planting soybeans earlier in April like they did before SDS became a problem here,” he says.
Given the price of the treatment ($10 to $13), Ferrie suggests growers try it on one-third of their soybean seed and evaluate the results. “Historically, early planted beans are usually our highest-yielding beans and most at risk to disease and pests, so that’s the seed I’d put it on,” he advises.
 








The ILeVo plus a fungicide/insecticide seed treatment combination outperformed the fungicide/insecticide-alone seed treatment in yield and return on investment in 2015 and 2016. A follow-up story to address the 2017 results will be published later this winter.


© Farm Journal Test Plots








 

Thank You to Our Test Plot Partners
Bayer CropScience, Case IH, New Holland, Unverferth, AirScout, Trimble, Can-Am, Willibey Brothers, Simington Farms, Finegan Farms, North Concord Farms, Bob Miner, R&D Bracy Farms Pioneer Seed and Shipe’s Seed Service

Farm Journal Test Plots Pledge
You can count on our test plots to be conducted on real farms with real equipment using a high-touch set of protocols. The information will be completely independent and actionable. Our hands will always be in the dirt researching the production practices and technology that are best for you.
To learn more, visit www.FarmJournal.com/testplots
?