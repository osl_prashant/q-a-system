The U.S. Apple Association has a winner for its 2018 Munch Madness Tournament, and it might sound familiar. The Envy apple variety took the top spot for the second year in a row.
The March promotion earned more than 30,000 website views on AppleVsApple.com, 3,000 votes and hundreds of social media followers, according to a news release.
“For its third year, Munch Madness provided consumers a fresh and creative opportunity to learn more about apples, all while connecting the roster of our top 16 varieties by sales to everyone’s favorite spring pastime of filling out college basketball brackets,” Tracy Grondine, director of consumer health and media relations at USApple, said in the release.
The Munch Madness contest took the 16 highest-selling apple varieties in the U.S., ranked them and created a seeded online bracket where participant voting decided which apple varieties moved forward in each matchup.
The four weekly rounds were The Vitamin-C Sixteen, The Edible Eight, The Fiber Four and The National Chomp-ionship, with the online bracket providing information on each variety’s flavor profile, apple health benefits and recipe ideas.
Below are the 16 Munch Madness varieties, by seed. The first round had the 1 seed gala facing the 16 seed Pacific Rose, the 2 seed Honeycrisp facing the 15 seed jonagold, and so on:

gala
Honeycrisp
fuji
granny smith
red delicious
Pink Lady
golden delicious
Ambrosia
mcintosh
Jazz
Envy
braeburn
SweeTango
empire
jonagold
Pacific Rose

Envy beat out Pink Lady, braeburn, Jazz and finally fuji to take the win.
“We’re thrilled Envy has once again been chosen by U.S. consumers as their favorite apple,” Darren Drury, executive general manager of T&G Global, which owns the trademark for Envy, said in the release. “We’re continuing to increase global production of this beautifully sweet and crunchy premium apple to keep up with the growing demand.”