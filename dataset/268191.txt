If successful pest control constitutes a run scored, then the four bases you have to touch are time, equipment, people and products. To ensure timely applications for every customer, plan ahead with your in-house team as well as farmers.
With your own offering of scouting services, products and application services, it’s important to ensure you and your customers start the season with a clear understanding of who does what in-season.
One key component for success is communication, which spans across your staff and your customers.
“A common threat to efficiency in the application season is sometimes a lack of communication between the sales team and the applicators,” says Farm Journal Associate Field Agronomist Missy Bauer. “It’s important to share details, so when a sales contact knows how a farmer likes thing done, that is shared with the applicator, so everyone ensures the customer is happy.”
Don’t let customer complaints post-application give you the biggest lessons from the season. Click here for more information on how to make every application count. 
With customers, timeliness in communicating the details from the field drives success. Bauer says it’s important to track planting and emergence dates as well as measure weed size accurately.
“When a detail is in doubt with a customer, call them,” she recommends. “It’s not a safe bet to guess.”
Farm Journal Field Agronomist Ken Ferrie advises farmers to have a pest management day planner—a key person to calculate and assess all of the farm’s application needs.
The duties for the pest management day planner are straight-
forward. Ferrie suggests asking the following 10 questions with farmers. 
“If a farm’s pest management plan doesn’t contain the answers to these questions, then help them find the information needed, and plug it into the plan,” he advises.
 
1. Have I accounted for planned and unplanned applications?  Start by identifying whether sufficient labor hours will be available during crunch times when scouting, spraying and other tasks must happen simultaneously. Next, review a farm’s implementation plan for managing all threats.
Pest control plans fall naturally into two groups: planned treatments, such as weed control, that occur every year and reactive applications for insects and diseases that show up only occasionally.
 
2. Do I have enough resources to apply planned treatments in a timely manner? “A farmer’s plan must include who will apply each treatment,’” Ferrie says. “This could include a member of their pest control team, a retailer or a contractor. A farmer should have their day planner show enough time and people available on the days when they will need to make the applications.”
One common bottleneck occurs when no-till farmers use burndown herbicides in the fall, winter and early spring. Often, the equipment is available, but the farm’s employees are busy with other work, such as harvest in the fall. “If the day planner reveals there’s no driver for the farm’s applicator, they’re going to have to delegate to a retailer or custom applicator,” Ferrie says.
 
3. Can my on-farm team, retailer or custom applicator meet my application needs at planting time? Because of evolving planting practices, this is not something you can take for granted.
“More growers have gone to early-planted soybeans,” Ferrie says. “If they are planting two crops at the same time, they might find their farm short on sprayer operators.”
This may require a change in operation for some retailers who previously wouldn’t apply soybean herbicides early in the season—when they’re busy with corn and not wanting to shift equipment and drivers between crops.
“Some farmers may even want both crops sprayed on the same day,” Ferrie adds.
As more farmers running multiple planters and planting soybeans before corn add other wrinkles, J.W. Thomas, field sales agronomist with Landus Cooperative, Perry, Iowa, says these challenges can be managed with communication.
“It’s about communicating early about the plan. Talking about plans in December rather than April eases the springtime pressure,” Thomas says. “It’s about staying up to date with the farmer’s operation.”
If you forgot or haven’t discussed a plan, then do it now, Ferrie says.
 
4. How many acres can my team spray per day? “Farmers should know the limitations of their team,” Ferrie says.
“How many acres a day can their people and equipment treat?” he continues. “The clock might not be the limiting factor. Some pesticides, such as postemergence herbicides, can only be applied during daylight hours for maximum effectiveness. To determine a farm’s capacity, multiply the acres per hour their equipment can cover by the hours per day they will be able to spray.”
 
5. Have I allowed for the effect of weather? “Realistically evaluate how many days will work for pesticide applications,” Ferrie says. “If a farmer needs 14 days to cover all of their acres, they probably won’t get that many days for application without rain or wind.”
Consider how many days per week an applicator can typically operate.
“The easiest thing to plan for is weed control,” Thomas says. “You know you’ll have weeds, and you know the history of the fields with weed pressure. It’s the easiest to plan for but the hardest to execute because you are up against the weather—constantly.”
Factor in the window of control for each pest, especially weeds.
“If a farmer needs 14 days to treat all their acres with a postemergence herbicide, they can only spray a product during daylight, weeds are growing ½" to 1" per day, the height limit is 5" and days are lost because of wet and windy weather, allowing 14 days for that application is not realistic,” Ferrie says. “With that scenario, the farmer needs to hire out some spraying or change products, perhaps switching to more pre-emergence herbicides.”
 
6. Have I prepared backup plans?  “It’s about having a plan but being able to adapt when the weather throws something at you,” Thomas says.
To plan for the worst and hope for the best, Ferrie shares this example of a self-applied farmer: Under Plan A, things go well, and an on-farm team applies the herbicide. Under Plan B, a retailer or contractor takes up some of the slack. Under Plan C, a retailer, or possibly more than one, does all the spraying.
“It’s important for farmers to talk to their retailer about their roles in Plan B and Plan C, so they can also plan,” Ferrie says.
 
7. Is my plan complete? “Every team member, on- and off-farm, must be able to look at a farm’s plan and understand what pre-emergence products will be needed as well as the rates, carriers, label restrictions and buffer requirements, if any,” Ferrie says. “When the time comes to treat, they can just look at the plan, get on with the job and not be delayed searching for more information. The products should already be stored on the farm or at the retailer.”
 
8. Have I addressed problems from previous years? “The best way to execute on any plan is to scout,” Thomas says. “This is an especially important investment in time. Specifically with weeds, you’ve got to stay on top of them.”
Assess a farmer’s scouting needs and resources.
“Scouts can alert us to weeds, and we know how fast the weeds will grow based on heat units,” Ferrie says. “So we should be able to apply planned herbicides on time.”
If farmers always have difficulty treating weeds before they get too big, then Ferrie suggests that farmers analyze their teams. Are scouts finding weeds in time? Do they need more applicator operators or bigger machines? Are custom applicators unable to reach their fields on time?
“Occasional problems caused by weather are to be expected, but if timely application is always a problem, farmers need to change something,” he says.
9. Am I ready for the unexpected? Reactive treatments involve pests that don’t occur every year, but you and farmers must always be prepared for them.
Thomas reports that in 2017 his team helped farmers treat for corn rootworm in corn for the first time in three years.
“Most of these problems arise when nature tips the balance in favor of the pest,” Ferrie says.
Some reactive situations become somewhat routine if you pay attention to environmental conditions.
“For example, spider mites occur during long dry periods. With cutworms, moths must fly in, find habitat and get the right amount of heat units to develop. There’s no excuse to be caught by surprise,” Ferrie says.
Pests like these might only become a problem every three years to five years, Ferrie says, but you still need a contingency plan on file. If scouts find a pest, then they can consult the farm’s plan to see what products have been selected and how they will implement the attack.
“With reactive situations, you usually see evidence the pest is building up,” Ferrie adds. “That gives operators time to get their sprayers ready.”
But some threats are not so predictable. Important resources for farmers include scouting reports from seed companies, retailers and land-grant universities in their own areas and surrounding states. Another tool may be a farm’s own  sticky traps and pheromone traps.
“Japanese beetles come from nowhere,” Ferrie says. “Some diseases and insects, such as armyworms, migrate from nearby fields. Rootworms might show up in corn if pumpkins, which are a host crop, are nearby.”
He says it’s key for farmers to know what crops are planted in nearby fields. 
“If the neighbor’s field of fully
traited corn goes down because of resistant rootworms, you can expect those resistant rootworms to spill over into the next field. This also will happen with resistant weeds.”
 
10. Who will make treatment decisions? “Timeliness and control are sacrificed and the value of a pest boss and his management team is lost if the pest boss is not empowered to pull the trigger,” Ferrie says. “People not involved with the pest management team—including farm managers and landowners—cannot be involved in treatment decisions.”
When it comes time to make treatment decisions, there’s no room for emotion. Ferrie reminds farmers to make treatment decisions from actual pest numbers and scientific thresholds. “The plan set in place will enable farmers to do that.”