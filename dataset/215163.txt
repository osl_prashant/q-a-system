More consumers are going tropical in the fall and winter, with the National Mango Board reporting the fruit has seen a 50% rise in sales during that time in the past three years.
That increase in interest is expected to last through the new year, according to the mango board, which is encouraging retailers to promote the fruit in the winter months. The board has point-of-sale materials and mango recipes to entice consumers.
“The days where mangos are confined to tropical displays in stores are gone and for good reasons,” Valda Corvat, director of marketing for the National Mango Board, said in the release. “Research show us that mangoes are an impulse purchase, therefore we encourage all retailers to place mangoes in their mainstream displays to benefit for recipe inspiration.”
The board’s tips for retail success include:

Build secondary displays
Carry multiple sizes and varieties;
Educate consumers about ripening and preparing mangoes;
Group fruit by size, variety and ripeness level; and
Maintain high-traffic shelf space throughout the year.

POS materials are free to retailers on the mango board’s website.