October 10, is considered World Mental Health Day. As American's farmers deal with harvest delays, USDA reports and stagnant prices taking a moment for mental health may be time well spent.  Brian Splitt with Allendale Inc. tells AgDay host Clinton Griffiths that this year, it's more important than ever to mentally prepare for business challenges.
"It's been a tough marketing year," says Splitt. 
He says if producers are honest with themselves and factor in their total cost of production including seed, fertilizer, machinery as well as living expenses and saving for things like college, the numbers are disappointing.
"On average if you're assuming 200-bushel corn in Illinois and Iowa those guys needed $4.02 cash," says Splitt.  "We didn't even get close to that."
That's why this former Marine says it's going to take mental toughness in the months ahead.
"I learned from being in the military and going to war you have to keep your mindset positive," says Splitt.  "Your attitude can be the difference between feeling like you're in an ordeal and feeling like you're on an adventure."
He says feeling like you're on an adventure can help keep your mind sharp and focused on the goals ahead.
"If you keep the right mood and a positive attitude, I think you're going to be more prone to recognize opportunities," says Splitt. "When you get down in the dumps and complacent or feel like there's no hope, then you are not going to see the bright spots out there."
He says those fleeting bright spots can be the moments that make or break your farm financially.
"You've got to just continue to pay attention and stay engaged," says Splitt.