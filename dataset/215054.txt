Make fall fertilizer decisions with these factors in mind
As producers look to the 2018 growing season, fertilizer application is top of mind. Anhydrous is cheaper than it has been in the past couple of years, so many farmers are considering fall application.
Iowa producer and Ag View Solutions consultant, Chris Barron, has developed an NH3 calculator to help determine if fall application is appropriate based on five criteria:
• Right Yield Goal. Identify your yield goal. Will fall application of nitrogen help you achieve it?
• Right Timing. What’s the right timing for this anhydrous application? It might be better to apply in the spring because of equipment availability or weather.
• Right Product. Is anhydrous the right nitrogen choice to make? In some cases, a different form of nitrogen might be a better option.
• Right Placement. Determine whether your equipment can get the anhydrous in the right spot for maximum crop benefits, based on your soil conditions.
• Right Rate. Have you considered the rate you will need to apply if you choose a fall application compared to spring? “If you’re putting a fall application on, you’re probably going to put on more than if you applied it in the spring,” Barron says.
Think about all of the factors and scenarios that could affect your fertilizer choice, Barron says. How much might that price go up? Will you use a nitrogen stabilizer in the fall and how much will application costs be?
Producers can use Barron’s tool for free by simply emailing cbarron@agviewsolutions.com or calling 319-533-5703. 
 
Click here to watch the Margin Minute video now to learn more.