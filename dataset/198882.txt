U.S. wheat futures firmed on Tuesday, with a falling dollar spurring a mild round of short-covering in the beaten-down market, traders said.Corn futures also edged higher, with technical buying underpinning the market.
Soybeans weakened, drawing back from sharp gains on Monday as investors locked in profits. Rain in eastern parts of the U.S. Midwest bolstered already robust harvest expectations.
All three commodities traded both sides of unchanged during Tuesday's session.
The benchmark Chicago Board of Trade September soft red winter wheat futures contract settled up 1-1/2 cents at $4.23-1/2 per bushel. K.C. hard red winter wheat posted modest declines, while MGEX spring wheat was mixed.
The dollar fell to a seven-week low on Tuesday, dipping below 100 yen for the first time since June as traders reined in U.S. interest rate hike bets following dovish comments from a Federal Reserve policymaker.
A weak dollar makes U.S. commodities a save-haven bet for investors looking for a hedge against inflation. It also helps boost overseas demand for U.S. supplies, but export sales of wheat remained light due to ample global supplies.
CBOT November soybean futures dropped 2 cents to $10.07-1/4 a bushel.
Good demand for the oilseed, both domestically and internationally, kept the declines in check.
The U.S.AgricultureDepartment said on Tuesday that private exporters reported the sale of 119,000 tonnes of soybeans to China for delivery in the 2016/17 marketing year. The announcement confirmed a deal traders had been buzzing about on Monday.
CBOT December corn futures were up 1/4 cent at $3.37-1/4 a bushel.
Lingering doubts about the size of the upcoming U.S. corn harvest despite signs of little stress on the crop provided further support.
"Market chatter suggests there is plenty of skepticism over whether the record U.S. corn yields forecast by the U.S.AgricultureDepartment will come to fruition," said Tobin Gorey, director of agricultural strategy at Commonwealth Bank of Australia.
The USDA on Monday afternoon said 74 percent of the corn crop was in good-to-excellent condition, the same as a week ago and above last year's 69 percent.
U.S. soybeans were rated 72 percent good to excellent, steady from a week ago and above last year's 63 percent. (Additional reporting by Naveen Thukral in Singapore and Gus Trompiz in Paris; Editing by Steve Orlofsky and Dan Grebler)