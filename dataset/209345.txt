Milk Production: Milk production in the 23 major states during June totaled 16.9 billion pounds, up 1.7% from June 2016. Production per cow for May averaged 1,939 pounds. The number of milk cows was 8.73 million head, up 4,000 head from May 2017. May production was revised up 0.1% to 17.8 billion pounds. For milk production in the top 5 producing states, California was down 2.1% from June a year ago; Wisconsin up 0.2%; Idaho up 1.9%; New York up 0.4%; and Texas up 15.0%. Other states with large production increases were New Mexico (9.8%), Colorado (8.1%), Utah (6.1%), Kansas (4.1%), Arizona (3.8%), and Illinois (3.8%). The states reporting a large decline in production compared to June 2016 were Oregon (-3.2%), Virginia (-1.4%) and Washington (-1.3%).Milk Price and Utilization: The Southeast Uniform milk price for June was $18.70, up $0.87 from May and $2.53 higher than June 2016. The Appalachian Uniform milk price was $18.37, up $0.72 from May and $2.44 higher than June 2016. June’s Class III price was $16.44, up $0.87 from May, and $3.22 higher than June a year ago. The Class IV price was up $1.40 from May to $15.89, and $1.05 higher than June 2016. The Class I Mover price for August is $16.72, up $0.13 from July. The milk/feed ratio for June was 2.32, 0.11 higher than May.
Southeast Class I utilization was 66.25%, up 3.33% from May, and 2.45% lower than June a year ago. The Uniform butterfat price was $2.5760, up 16.27 cents from last month and 5.68 cents higher than June 2016. The June Class I price was $19.11. July Class I price is $20.39. Appalachian Class I utilization was 63.56%, down 3.81% from May, and 1.28% lower than June a year ago. The Uniform butterfat price was $2.5883, up 17.41 cents from last month and 20.72 cents higher than June 2016. The June Class I price was $18.71.
Cheese Production and Stocks: Total cheese production in May was 1.05 billion pounds, up 4.0% from a year ago while butter production was down 1.5% to 164 million pounds. Nonfat dry milk (NDM) production was up 1.2% compared to a year ago with total production at 168 million pounds while skim milk powder (SMP) production was up slightly to 51.0 million pounds compared to a year ago. Total cheese stocks at the end of June were up 5% from June a year ago and down 1% from the previous month while butter stocks were down 1% from last month and down 5% from a year ago.
Springer Prices and Cow Slaughter: At Smiths Grove, Kentucky on July 25, supreme springers sold for $1,400 to $1,550, down $25 from last month while US approved springers were $1,300 to $1,375, down $13 compared to a month ago. Dairy cow slaughter in June was 236,700 head, down 500 head from May and 13,000 more than June 2016.

Southeast Federal Order Prices


Month


Uniform Price $/cwt.


Class I Price $/cwt.


Class III Price $/cwt.


Class IV Price $/cwt.


Class I
% Utilization


Butterfat Price $/lb.


Jan 17


20.27


21.25


16.77


16.19


69.76


2.4696


Feb 17


19.36


20.53


16.88


15.59


68.48


2.4765


Mar 17


19.15


20.70


15.81


14.32


65.23


2.4416


Apr 17


17.97


19.85


15.22


14.01


59.97


2.3992


May 17


17.83


19.00


15.57


14.49


62.92


2.4133


Jun 17


18.70


19.11


16.44


15.89


66.25


2.5760

 

 
Appalachian Federal Order Prices


Month


Uniform Price $/cwt.


Class I Price $/cwt.


Class III Price $/cwt.


Class IV Price $/cwt.


Class I
% Utilization


Butterfat Price $/lb.


Jan 17


20.06


20.85


16.77


16.19


72.88


2.4699


Feb 17


19.09


20.13


16.88


15.59


71.16


2.4766


Mar 17


19.06


20.30


15.81


14.32


69.75


2.4420


Apr 17


17.77


19.45


15.22


14.01


63.06


2.4008


May 17


17.65


18.60


15.57


14.49


67.37


2.4142


Jun 17


18.37


18.71


16.44


15.89


63.56


2.5883

 
 
What is the Market Offering for Milk to be Sold October?
Ex: It is Jul. 31 and Oct. Class III milk futures are trading at $17.33. Local Oct. basis estimate is +$3.00.

If October futures


=


18.75


17.25


15.75


and actual blend price


=


21.75


20.25


18.75


Sample Strategies


 


 


Realized Prices for Oct. Milk


1) Sold Futures

 

@


17.33

 

20.33


20.33


20.33


2) Bought Put


16.25


@


0.17

 

21.58


20.08


19.08


3) Bought Put


16.75


@


0.31

 

21.44


19.94


19.44


4) Bought Put


17.25


@


0.53

 

21.22


19.72


19.72


5) Synthetic Put

 
 
 
 
 
 
 

     Sold Futures

 

@


17.33

 
 
 
 

     Bought Call


18.25


@


0.20


 


20.63


20.13


20.13

 
 
What is the Market Offering for Milk to be Sold in November?
Ex: It is Jul. 31 and Nov. Class III milk futures are trading at $17.34. Local Nov. basis estimate is +$3.00.

If Nov. futures


=


18.75


17.25


15.75


and actual blend price


=


21.75


20.25


18.75


Sample Strategies


 


 


Realized Prices for Nov. Milk


1) Sold Futures

 

@


17.34

 

20.34


20.34


20.34


2) Bought Put


16.25


@


0.22

 

21.53


20.03


19.03


3) Bought Put


16.75


@


0.38

 

21.37


19.87


19.37


4) Bought Put


17.25


@


0.60

 

21.15


19.65


19.65


5) Synthetic Put

 
 
 
 
 
 
 

     Sold Futures

 

@


17.34

 
 
 
 

     Bought Call


18.25


@


0.26


 


20.58


20.08


20.08