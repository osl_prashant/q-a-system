Plant parasitic nematodes are microscopic worm-like organisms that require water to survive and are sensitive to high temperature. Only living nematodes can be extracted from roots. Through the years we have had many samples submitted to the Purdue Nematology Laboratory with little consistency in the quality of the samples. We have discussed proper sampling procedures at every opportunity but not exclusively until now. Sampling might appear trivial but we believe proper sampling is the most crucial step for correct diagnosis. Because we continue to receive improper samples we address this issue again via this article. Even though procedures for sampling among the most important plant parasitic nematodes are similar, there are differences based on the host and the type of nematodes we are trying to recover.
Corn Parasitic Nematodes: There are three major groups of nematodes that parasitize corn.

Endo-parasites (e.g., Lesion nematodes): These nematodes mostly feed within corn roots. Plant roots along with surrounding soil must be submitted to recover these types of nematodes. A proper soil sample consists of about one quart of sub-samples taken to a depth of 6-8 inches directly from the root zone of affected corn plants Dig up the stunted plants and place with adhering soil and roots in a plastic bag. Attach a label to the outside of the bag. On the label, give sufficient information to identify the sample. Root and soil samples should not become dry or be exposed to high temperature. The best time to sample for these nematodes is mid-season when most of the nematodes have migrated to the inside of the roots. These nematodes continue to feed throughout the growing season. They can be found in all kinds of soil types.
Ecto-parasites (e.g., Needle nematodes): These types of nematodes feed from outside of the young roots. The sampling procedure is the same as described above for Lesion nematodes. But, Needle nematode is mostly a problem in sandy soil and can be found early in the season (4-6 weeks after germination). Often they disappear when the soil temperature rises above 80 degrees.
Semi endo-parasites (e.g., Lance nematode): These nematodes can feed from inside or outside of the roots. The sampling procedure is the same as that described above for endo-parasites. Lance nematodes feed throughout the season, have no soil type preference and can parasitize corn or soybean.

Soybean Parasitic Nematodes: Lesion and Lance nematodes parasitize soybean too but Needle nematode does not. The sampling procedure for these nematodes in soybean is similar to the one described for corn. The most economically important nematode affecting soybean is the Soybean Cyst Nematode (SCN). The SCN distribution, as for most of the plant parasitic nematodes, is in patches. So it is very important to take many sub-samples to increase possibility of hitting the concentrated areas. A soil probe or a small trawl should be used to collect the sub-samples. Most of these nematodes are within 6-8 inches of the soil. One sample for every 10 acres is ideal. A quart of soil is sufficient and no root samples are required for SCN. Samples can be taken anytime.
Recommended optimum sampling type and time for major plant parasitic nematodes in Indiana.



Host
Target Nematode
Sample Type
Optimum Time to Sample





Corn


Needle


Soil and roots


June-mid July




Corn


Lesion, Lance


Soil and roots


Late June-Late August




Soybean


Lesion, Lance


Soil and roots


Late June-Late August




Soybean


SCN


Soil


Anytime




Turf


All


Soil and roots


June and September




Melons


Root knot


Roots


At harvest




Mint


Lesion


Roots and soil


Late June-September




Mint


Needle


Roots and soil


Late Spring or early fall




Mint


Root knot


Roots


Fall