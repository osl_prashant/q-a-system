Kansas State University’s 2018 Swine Profitability Conference is planned for Tuesday, Feb. 6. This year’s event will be hosted at the Stanley Stout Center in Manhattan, and focuses on the business side of pork production.
“Attendees will have a chance to learn ideas for improving their businesses from leading producers, veterinarians and economists,” says Mike Tokach, K-State Department of Animal Sciences & Industry distinguished professor and swine extension specialist. “This conference is designed to help producers stay competitive in today’s swine industry.”
The conference will feature speakers from an array of swine-related businesses, including:

Doug MacDougald, DVM, Southwest Vets, speaking on managing pig health with minimal antibiotic use in commercial pig production
 Ben Woolley and Ben Keeble, Sunterra Farms, who will address opportunities and pitfalls of producing antibiotic-free pork
Terry Nelson, Husky Hogs, sharing his story of rebuilding after a catastrophe
Gray Louis, Seaboard Foods, will focus on future trends impacting the swine industry
Lisa Tokach, DVM, Abilene Animal Health, will provide life lessons learned while practicing with Steve Henry, DVM, who recently retired after more than 40 years in swine practice

The conference begins with coffee and donuts at 9:15 a.m. and the program starts at 9:30 a.m. Lunch is included in the conference, which will end at 3 p.m.
Pre-registration is $25 per participant and due by Jan. 30. Attendees can register at the door for $50 per participant. More information, including online registration, is available at KSUSwine.org or contact Lois Schreiner at 785-532-1267 or lschrein@ksu.edu.