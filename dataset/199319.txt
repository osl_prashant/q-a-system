Over the decades, cattle farmers learned to work around toxins in fescue grass in their pastures."We called it 'managing fescue,'" says Craig Roberts, extension forage specialist. "It should be called 'coping with a bad grass.'"
Toxic fescue cuts gains, lowers birth rates, reduces milk, causes fescue foot and more, says Roberts, with University of Missouri Extension, Columbia. "All of that can be coped with, somewhat. Now there's a cure."
Toxic Kentucky 31 tall fescue can be replaced with nontoxic novel-endophyte fescues. Pasture renovation kills every sprig of toxic grass and replaces it with nontoxic fescue.
Coping methods must be applied every year toxic fescue is grazed. Renovated pastures grow greater gains year after year.
Farmers put off renovation since it takes a year of management to make the switch, Roberts says. "However, once it is done, it's done."
"Payoff on renovation goes on for years. It makes more profits and gives farmers peace of mind," he says. "No one wants to pick up a cow with the front loader to bury. Cows die from fescue foot."
The Alliance for Grassland Renewal promotes renovation. The national group formed in Missouri four years ago. This year, an Alliance school goes out of state into Oklahoma.
Adding legumes to grass pastures is a proven practice that adds nutrition and gains. On toxic pastures, legumes dilute the intake of K-31 fescue. That lowers losses of gains from toxin.
However, legumes in novel-endophyte fescue pastures add gains and a lot more, Roberts says. "On the new fescue pastures, it's not about coping with losses."
Legumes add energy, protein and minerals, which improves grazing. Legumes kick in when cool-season grasses start summer slump. Legumes extend grazing seasons.
Clovers provide tender, easy-to-digest leaves. Lespedeza, another legume, gives growth into the hot part of grazing season when fescue fades. Lespedeza grows in July and August.
An easy practice used by grass farmers is frost-seeding legumes. They broadcast the small, hard seeds over grass pastures. Freezing and thawing work seeds into the soil. Sprouts appear when spring temperatures rise.
Legumes in novel-endophyte pastures add pounds of gain to grazing cattle. On K-31 fescue, that added weight offsets weight lost to toxin. Repeated studies show that toxic fescue takes away seven-tenths pound of gain a day. In their growing season, clovers add four-tenths pound a day.
Legumes add another bonus. Root nodules fix nitrogen from the air. That fertility boosts growth of nearby grass. In a good year, fixed nitrogen replaces 25 pounds of fertilizer.
"Free nitrogen cuts costs, a good business practice," Roberts says. "Profits come from both adding income and cutting costs."
Any way you look at it, renovating fescue pastures is a good long-term investment, he says.
The Alliance schools explain how the coping methods are converted into income.
The first school will be March 28 in Welch, Okla. In following days the group moves to Mount Vernon, Columbia and Linneus. Those Missouri schools will be on research farms of the MU College of Agriculture, Food and Natural Resources.
Details on sites, agenda and registration are at grasslandrenewal.org. Advance sign-up is required. Space is limited.
A downside of new fescues: Cattle like them so much they can overgraze. Managers must take cattle off to give pastures a rest. That prolongs stand survival.
Toxic fescue prevents cattle from eating too much. That's good for grass, not for grazing cattle.