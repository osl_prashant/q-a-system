Wallace Wiley Martin, a longtime grower of onion transplants at Dixondale Farms in Carrizo Springs, Texas has died.
Martin, 93, died Jan. 25.
Martin was one of the growers assisting Texas A&M University in developing the yellow granex onions, and the popular 1015Y Super Sweet, according to his obituary.
Martin, an orphan at 14, joined the Army Air Corps at 18 and was involved in 50 missions over France and Germany as navigator of a B-17 bomber during WWII. When his plane was shot down, the French Resistance hid Martin from German soldiers.
After the war, Martin met and married Sissy McClendon, later moving to Carrizo Springs to work with his father-in-law at Dixondale Farms in 1948. In the late 1960s, according the company’s online history, Dixondale Farms focused on onion transplants. In 1982, Martin’s son-in-law, Bruce Frasier, joined the company, where he is now president.
Martin is a recipient of the Texas Vegetable Association Lifetime Achievement Award and was inducted into the 2016 Texas International Produce Hall of Fame.
Survivors include a son, Bill Martin, and daughters Marilu Martin, Nancy Martin and Jeanie Martin Frasier.
Donations may be made to the First United Methodist Church, P.O. Box 301, Carrizo Springs Texas, 78834.