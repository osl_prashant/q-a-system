Agriculture groups are asking for follow-through on big ticket items mentioned in President Donald Trump’s State of the Union message to Congress Tuesday evening.
“It’s time to reform these outdated immigration rules and finally bring our immigration system into the 21st century,” the President said Tuesday night.
 



 
Other big ticket items include trade, regulations, infrastructure and tax reform, reports AgDay host Clinton Griffiths.
Each of these topics “continues the theme of renewing rural America,” says Zippy Duvall, American Farm Bureau president.
National Farmers Union urged President Trump to “follow through” with infrastructure promises to rural America.
Reaction to the speech was mostly positive from attendees at the 2018 Cattle Industry Convention and NCBA trade show.
“We are always looking for a more definitive answer from him on trade, but the fact of the matter is he’s in the middle of negotiations. And we know that when you are in the middle of negotiations, you don’t play all of your cards, especially in a speech to Congress,” said Colin Woodall, NCBA senior vice president of government affairs, in an interview with Betsy Jibben, AgDay national reporter.
Positive feedback from the latest round of NAFTA negotiations is making farmers hopeful for a quick resolution.