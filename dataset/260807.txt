The multiyear dip in North Dakota farmland prices has seemed to level off. For 2017, the average price for cropland is $2,377 per acre—a mere 3% drop from 2016 levels, according to the 2017 North Dakota Agricultural 
Land Price & Cash Rent Survey by the North Dakota Chapter of the American Society of Farm Managers and Rural Appraisers (ASFMRA).
Farmland values in the state peaked in 2013 at $2,953 per acre, and have been on a steady decrease since. The small drop in values for 2017 is an
improvement to the nearly 10% annual declines seen in 2015 and 2016. 
“This development in pricing likely has established a firm floor on land values,” says Dwight Hofland, a member of the North Dakota Chapter of ASFMRA and senior land manager with Pifer’s Auction & Realty. 
Pasture land and marginal land values have improved, alongside quality crop land. This is mostly due to buyer demand, Hofland says. 
“Investors and farmers with strong balance sheets continue to purchase land to add to their portfolios,” he says. “This group of buyers is well positioned to provide market strength as we are starting to see land inventories tighten. As ranchers look to add land to sustain their cattle herds and feed supply, they have expanded their trade regions adding additional pressure to local land markets and rental rates.”

Multiple market influences in the near-term could support or sink land values in North Dakota, Hofland says. They include: 
• continued drought conditions in the upper Midwest.
• increasing interest rates.
• tight profit margins in the agricultural industry.
• increasing global demand.
• trade relationships.
 
Strong Rental Rates. Despite anemic commodity markets through most of 2017, the high demand for quality land in North Dakota is supporting rental rates, Hofland notes. “We are seeing a tightening of available rentable acres, which in turn holds rental rates at arbitrarily high levels,” he says.
In the Red River Valley, the average for the high end of the cash-rental-rate range is $205 per acre. In the Southeast, the average is $135 per acre; in the Northeast the average is $115 per acre. The average high end of the cash rental range in the Northwest/North Central and Southwest is $75 and $65, respectively.
Hofland says even with depressed commodity markets, cropland rental rates will continue to remain steady this year.