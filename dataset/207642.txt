I noticed that one of my older blog posts about Subway’s promotion of “National Eat Your Vegetables Day”  is getting a little traffic on the web site in the past few weeks.
 
Sometimes it is hard to say why a particular story finds traction long after it is posted. Another example is a short story I wrote in 2013 is headlined “Mexico dominates U.S. fresh produce imports.” 
 
Perhaps because of NAFTA related Google searches, that story continues to see steady “hits” by visitors to The Packer website, and occasional inexplicable spikes in traffic.  There are numerous examples of this, of course, and the column “The great green pepper ‘mango’ mystery” by The Packer’s Amelia Freidline has received steady views long after it was published online.  It is a delightful column and the internet obviously agrees.
 
Back to Subway. Why the recent interest in Subway and vegetables? There has been news about Subway’s “fresh-forward” redesign in recent weeks.
 
Find The Packer’s coverage of the story, by Ashley Nickle, here. and the news release from Subway here.
 
There is no doubt that many Subway stores need a refresh since it seems more than a few are attached at the hip with remote gas stations.  One of the aspects of the redesign is a display showing whole peppers, onions, cucumbers, and tomatoes. The display, it can be deduced, communicates that the veggies are sliced fresh daily, a valuable go to market value that consumers may be unaware of.
 
Subway and fresh vegetables - may the two search terms always lead the internet to The Packer.