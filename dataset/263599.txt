Southern Indiana REMC getting $74M loan for infrastructure
Southern Indiana REMC getting $74M loan for infrastructure

The Associated Press

BROWNSTOWN, Ind.




BROWNSTOWN, Ind. (AP) — A southern Indiana rural electric membership corporation is receiving a federal loan to upgrade its infrastructure.
The U.S. Department of Agriculture announced this week that Jackson County REMC in Brownstown will receive a $74 million loan to build 84 miles of line and improve 32 miles to provide reliable, affordable electricity to 20,000 residential and business consumers. The loan includes $59 million for smart grid technologies.
The counties the loan will serve include Bartholomew, Brown, Clark, Jackson, Jefferson, Jennings, Lawrence, Monroe, Scott and Washington.
The loan is part of $276 million the federal agency is investing in rural electric infrastructure. Jackson County REMC was the only Indiana recipient announced.