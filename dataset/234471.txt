Distilleries could follow in brewery footsteps
Distilleries could follow in brewery footsteps

By JORDAN GRICEConnecticut Post
The Associated Press

BRIDGEPORT, Conn.




BRIDGEPORT, Conn. (AP) — Like many entrepreneurs, Robert Schulten said expanding his Asylum Distillery business has always been part of his plans.
It's been two years since the Fairfield resident opened his distillery at in Bridgeport, and his most recent plans include opening a second facility among the growing restaurant scene in downtown Bridgeport, introducing a speakeasy-style establishment where customers could enjoy a few in-house drinks, much like a brewery or winery.
"We could be a destination to help the downtown vibe," Schulten said. "Downtown right now is up and coming, and so I think it's exciting. We love the old buildings down there. This is fine here, but it's really manufacturing. We would really create something that is more of a tasting room, but a little bit nicer."
But unlike wineries and breweries, state law prevents distilleries from selling their products for consumption on site.
It's only been two years since distilleries won permission to manufacture, store, bottle and sell their liquor for off-site consumption. The most customers could expect to taste at a distillery would be a 2-ounce sample before heading home.
While that was a big win for local spirits manufacturers, Schulten said the next step would be to get the law updated once again.
Local officials have backed up his efforts, and as the 2018 legislative session has gotten underway, Bridgeport's delegation and Mayor Joe Ganim wrote to the heads of the General Law Committee urging their support of the distillery proposal.
"In addition to the demand for locally produced wine and beer, so too is the demand growing nationally for regionally distilled spirits sourced from local grains such as corn, wheat, rye, barley and other agricultural crops," the letter said. "Connecticut is in prime position to take advantage of this growing market trend."
Breweries' popularity has boomed in recent years, with roughly 60 locations around the state making Connecticut a destination for the growing cadre of craft beer drinkers. The same could be said for wineries, where about 40 locations have led to tourist attractions like the Connecticut Wine Trail.
According to state Rep. Christopher Rosario, D-Bridgeport, the prospects of job growth in the city has made getting the law changed among the delegation's priorities for the session.
"At the end of the day we are looking to support a Bridgeport business that has been creating jobs and has revitalized a part of our neighborhood that was kind of dormant," Rosario said, "and if this allows them to expand and create a bigger share in the marketplace, then they might be able to hire more folks and really put Bridgeport on the map when it comes to distilleries and the spirits market."
Av Harris, a Ganim adviser, said putting distilleries on par with brewers and wineries would have residual benefits to the statewide economy.
"This isn't just about a distiller who will sell a glass of vodka or bourbon or whatever at their distillery — this is about economic development," he said. "This is about locally produced, really high-quality spirits that also rely on other parts of the Connecticut economy."
The possible benefits haven't been lost on other markets, according to Carroll Hughes, executive director of the Connecticut Package Store Association.
As distillers sample out and serve their products to customers, Hughes said that will also drive business for package stores that have been gradually increasing their demand for locally made spirits.
"We think if they sell by the glass it will encourage people to go to package stores to buy their products," Hughes said, stating that the consensus of the package store owners was that the shift in the law would be to their benefit.
"We strongly encourage our members to devote shelf space to Connecticut products because, obviously, the shelf space might sell better on another national product, but we have encouraged them to put Connecticut products on those shelves, and many of them have Connecticut sections now."
Many distillers like Schulten have also put an emphasis on using locally sourced produce and ingredients to make their variety of bourbon, whiskey and gin, contributing the local farm economy.
"When people look at it and look at the economic vitality of Connecticut, why would you be against it? I can't find a good reason," Hughes said.
For now, the proposal to allow on-site consumption at distilleries is attached to a more controversial bill aimed at eliminating minimum bottle prices, which has garnered opposition from package store owners.
According to Hughes, the distillery proposal could also appear in a separate bill that could come up for a public hearing early next month.
___
Online: http://bit.ly/2HI76UW