Iowa dairy producers have the opportunity to learn about emerging dairy industry issues at the 2018 Dairy Days hosted by Iowa State University Extension and Outreach specialists. The program is scheduled at seven eastern Iowa locations between Jan. 15 and Feb. 1.
“The ISU Extension and Outreach Dairy Team conducts this workshop to provide the latest research to Iowa’s dairy producers,” said Jennifer Bentley, ISU Extension and Outreach dairy specialist. “Our goal is to help producers make sound herd management decisions that are backed by current and relevant information.”
Bentley said the day-long program offers producers an opportunity to hear up-to-date information and provides time to talk with speakers about specific individual situations.
Topics covered at 2018 Dairy Days will include:

Double Cropping Small Grain and Sorghum Forages on Your Dairy
Alternative Forages for Dairy Cows - What else is there besides corn silage and alfalfa?
Transition Cow Success - Managing Pain, Weight and Milk
Dairy System Profit Performance Comparison in 2016
It’s More Than Just Checking the Markets - Powering Up Your Smartphone for Better Farm Management
Research Update - Speed Round
Common Feed Additives for Dairy Cows - What are they and what do they do?

Dairy Days will be offered at seven Iowa locations. Registration starts at 9:30 am and the program will conclude by 2:30 p.m. A $15 registration fee covers the noon meal and proceedings costs. Pre-registration is requested by the Friday before each event to reserve a meal. Vouchers for the event may be available at your local agri-service providers or veterinarian’s office.

Jan. 15 in Riceville
Jan. 16 in Waukon
Jan. 17 in Waverly
Jan. 29 in Bloomfield
Jan. 33 in Kalona
Jan. 31 in Ryan
Feb. 1 in Holy Cross

For more information contact your county ISU Extension and Outreach office or a dairy specialist - Jennifer Bentley at 563-382-2949 or Larry Tranel at 563-583-6496.