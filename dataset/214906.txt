The cattle herd in the U.S. is growing, and cash prices for fed and feeder cattle have exceeded expectations this fall as live and feeder cattle futures reached a new contract high to start November.

This could be a positive start to 2018, and some analysts are attributing the recent strength to the extremely long position held by the funds, but others say it’s the stock market rally.

Michelle Rook reports there are other underlying factors on AgDay above.