After another quarter of less-than-thrilling comps, Austin, Texas-based Whole Foods Market Inc. said it plans to focus on what its core "Whole Foodies" want.
The company said during its first quarter earnings call on Feb. 8 that it plans to close nine stores and open only eight in the next quarter. This is the first time the retailer has contracted its footprint since 2008.
"We will continue to grow, but no longer have a goal of 1,200-plus stores," said CEO John Mackey, during the call. The retailer stressed that more than 80 new stores are still in the pipeline.
Whole Foods plans to double down on the shopping experience for its core customers. That includes relocating and enhancing stores.
"In markets where our brand is well-established, relocating older, smaller stores to larger locations offering more parking and an elevated shopping experience offers low risk and high returns," Mackey said.
These efforts are part of what the company says is an emphasis on its core customers, its "Whole Foodies," Mackey called them.
"We know that our core customers represent our largest customer segment and account for a majority of our sales," he said, during the call. "They are dedicated to the high-quality, fresh, healthy foods and transparency that we offer."
Efforts to market to these customers already are visible, particularly in the latest ad campaign, "Eat Real Food."
Even though it's focusing on those core customers, the company says strategic price investments will continue. One analyst questioned if these core customers are as price sensitive. Mackey said Whole Foods doesn't want to make these customers feel taken advantage-of.
"We are very conscious of pricing and, in some cases, we have a particularly strong quality advantage, the gap might be a little larger," he said. "But if we're selling the exact same items and exactly the same brands, we feel like we really need to be competitive on those prices."
Whole Foods also engaged Dunnhumby Ltd. for category management efforts, and is pushing forward with its category management, which also includes a lower inventory and order-to-shelf project
"We've transformed the inventory levels we have in the back room, focusing on never-outs - key items that we need to have in stock at all times," said Ken Meyer, executive vice president of operations, during the call.
Meyer said this strategy has helped with shrink.
"What happens in our perishable departments when we reduce inventory in the back room - it's handled less," he said. "When you take away handling it improves the reduction in shrink in stores."