Doubling previous informal estimates, a new study says Arizona’s leafy greens industry delivers $2 billion in annual sales.
The study, by researchers at the University of Arizona’s Department of Agricultural and Resource Economics, estimated a sales contribution of $2 billion for the Arizona leafy greens industry.
“We examined the whole value chain, including on-farm and post-harvest activities to understand the broad scope of the industry’s contribution to the Arizona economy,” Ashley Kerna Bickel, key researcher and contributor to the report, said in a news release.
Called “Arizona Leafy Greens: Economic Contributions of the Industry Cluster,” the study examined 2015 agricultural cash receipts for on-farm production and post-harvest activities, according to the release.
The release said the report was funded by the Arizona Leafy Greens Food Safety Committee. Authors included Kerna Bickel, Dari Duval and George Frisvold. The study is available online.
For purposes of the study, the leafy greens industry was defined to include on-farm activities and also cooling, cutting, washing, packing, processing, storing and shipping.

In addition to the $2 billion sales figure, the study found:

Arizona is the No. 2 producer of lettuce (iceberg, leaf and romaine) nationally;
The state’s Yuma County ranks second among U.S. counties in harvested lettuce and spinach acreage;
From late November to mid-March, Arizona supplies 80% of the nation’s lettuce, with an average of 1 billion pounds of lettuce shipped per month;
Leafy greens have accounted for an average of 17% of the state’s total agricultural receipts each year since 2010;
Nearly 27,000 individuals were employed either directly or indirectly by the Arizona leafy greens industry in 2015, with 16.9 million hired labor hours needed for on-farm operations alone; and 
The leafy greens industry’s total contribution to Arizona’s gross state product was nearly $1.2 billion in 2015.