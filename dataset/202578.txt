The Produce Marketing Association's May 4-5 Tech Knowledge conference in Monterey, Calif. will focus on labor challenges that can be solved or helped through technology.
 
"One of the keys to solving your business's labor challenges is to work smarter,"  PMA Chief Science & Technology Officer Bob Whitaker said in a news release. "We are going to teach attendees to look at working smarter as a holistic innovation process, from how to evaluate available technologies and decide what's right for your business, to how to assess and fine tune those solutions to get the best results for your business."
 
The conference will explore developments in artificial intelligence and robotics, according to the release.
 
Speakers will include Whitaker, Minos Athanassiadis of Fresh Link Group,  efficiencies expert Lisa Montanaro and crop discovery and development authority John Purcell.
 
Information and registration is available at the PMA website www.pma.com/events/tech-knowledge. 
 
The 2017 Tech Knowledge conference also will unveil the first winner of its new Science & Technology Circle of Excellence award, according to the release.
 
The Newark, Del.-based Produce Marketing Association is taking applications until March 24 for the award.
 
The new award aims to recognize "those who are solving business challenges by applying advances in science and/or technology to create better products, processes or customer value," according to a news release.
 
"Science and technology are playing an ever-important role in our industry, particularly to help us solve labor challenges, work smarter and create new business opportunities," Whitaker said in the release. "By recognizing innovators in this area, we hope to inspire our industry to follow their lead â€“ that's why the award is named 'Circle of Excellence'."
 
A panel, including PMA staff science and technology subject matter experts and industry executives, will select the 2017 award winner, according to the release.
 
For more information and to apply for the Science & Technology Circle of Excellence Award, visit PMA's website.