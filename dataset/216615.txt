2017 will be remembered as one of the most destructive and costliest years for natural disasters in U.S. history.
We've witnessed three major hurricanes in this country - Harvey, Irma and Maria. Those are easy to remember due to their scope and size.
 
But, do you remember the tornado outbreak in early March that hit the midwest? Or how about that April and May flooding in Missouri and Arkansas? And, there was the lingering drought that turned Montana into kindling and North Dakota fields and pastures into a parched land.

All of those are "billion dollar disasters" as measured by the National Oceanic and Atmospheric Administration (NOAA). While numbers are still being tallied, there are at least 17 billion dollar natural disasters in the u-s this year.
Several of those major disasters involved wildfires this year and some are still burning.
"We're in near record territory," says USDA meteorologist Brad Rippey. "We've seen more than 9.5 million acres burned nationally and only two other years in recent history have topped that number.
Rippey says two years ago in 2015 roughly 10.1 million acres burned and in 2006 the season ended with 9.9 million chared acres. 
California alone is breaking its own state records for the year. AccuWeather.com is reporting the state's wildfire season will cost $180 billion. The California Department of Forestry and Fire protection says 115 separate fires, each at least 300 acres in size, rolled through the state this year.

Some of the largest damage came in October in 'wine country" and now, the Thomas fire is the latest bring devastation to two counties just outside of Los Angeles.
Elsewhere, wildfires torched more than 1 million acres in Montana in 2017. All Montana counties were approved for emergency haying and grazing through the CRP program. 53 Of the state's 56 counties have been declared either primary or contiguous disaster areas.  As a result, Montana is still plagued with drought and the latest Drought Monitor says roughly three-fourths of the state is abnormally dry, more than half is considered moderate drought while over 30 percent is ranked a severe drought.  
In South Dakota, more than 54,000 acres of land in Custer State Park burned just two weeks ago. It was started by a downed power line. Park rangers are asking people to help neighboring farmers and ranchers who lost grazing lands and hay. The park is known for its large herd of bison, burros and elk.  Some animals got caught in the fire. Due to their injuries, several had to be euthanized.

Unfortunately, a similar scenario played out across the southern plains last March when wildfires erupted in Texas, Oklahoma, Kansas and Colorado. More than 1.5 million acres burned in three days' time. Thousands of head of cattle were caught in fast-moving flames and local veterinarians were pressed into action to put-down many of severely injured animals. 
Watch the entire wildfire special on AgDay TV online starting December 27th.