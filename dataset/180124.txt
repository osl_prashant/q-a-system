Statistics Canada released its Canadian grain stocks as of March 31 report this morning, pulling from a March 16 to 31 survey of 11,600 Canadian farms. Farmers were asked to report the amounts of grain, oilseeds and special crops in on-farm storage. Data on commercial stocks of western major crops originate from the Canadian Grain Commission.
As expected, as of March 31, 2017, total stocks of canola, soybeans and oats were lower compared with the same date in 2016. Meanwhile, total stocks of wheat, corn for grain and barley were up.

Wheat
Total wheat stocks were at 16.6 million tonnes (MMT) as of March 31, 2017, up 15.5% from the same day a year earlier, but came in well below the average of trade expectations (18.3 MMT) and was in fact below the lowest of pre-report trade estimates.
Interesting...a slightly positive response in Minneapolis spring wheat futures since the report release...moving from narrowly mixed to now 3 cents higher.
Canola
As of March 31, total canola stocks were down 23.3% from the same day a year earlier to 6.6 million tonnes...which falls about in line with trade ideas. This decrease resulted from a 28.0% decline in on-farm stocks to 5.0 million tonnes. On-farm stocks in Saskatchewan were down 32.1% to 2.3 million tonnes, while farm stocks in Alberta fell 24.5% to 1.8 million tonnes. Commercial stocks also contributed to the overall decrease as they were down 3.4% to 1.6 million tonnes.
A positive response from the canola market this morning...currently up $4 to $5/tonne.
Soybeans
Soybean stocks decreased 4.2% to 1.9 million tonnes as of March 31, despite a 1.4% increase in production in 2016. On-farm stocks were down 19.6% to 930 000 tonnes, mainly driven by Ontario, where they fell 33.8% to 460 000 tonnes. Commercial stocks increased 18.6% to 925 000 tonnes.
Barley and oats
Total barley stocks increased 22.6% to 4.6 million tonnes as of March 31, following a 6.8% gain in production in 2016...and is slightly above trade expectations...though not enough to have market impact. Farm stocks grew 24.2% compared with the same day a year earlier, to 4.3 million tonnes. The commercial stock levels increased 4.6% to 317 100 tonnes.
After an 8.2% year-over-year decline in oat production in 2016, total oat stocks decreased 8.5% to 1.7 million tonnes compared with March 31, 2016. Both farm stocks (-6.8%) and commercial stocks (-19.1%) contributed to the overall decrease.