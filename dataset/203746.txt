Mushroom volumes were up in the 2015-16 season, but the value of the crop was down slightly, according to the U.S. Department of Agriculture.
About 946 million pounds of mushrooms were produced in 2015-16, up 2% from last season, according to an annual mushrooms report from the USDA's National Agricultural Statistics Service.
The value of this season's crop, at $1.19 billion, was down less than 1% from 2014-15.
The average price for mushrooms in 2015-16, $1.26 per pound, was two cents lower than the season before.
About 346 producers grew mushrooms in the U.S. in 2015-16, 12 fewer than the year before.
Agaricus mushroom volumes in 2015-16 totaled 922 million pounds, 2% more than the season before. Pennsylvania accounted for 64% of total volumes and California 12%.
The agaricus crop was valued at $1.1 billion, down 2% from 2014-15.
About 165 million pounds of portabello, crimini and other brown mushrooms were produced this season, 3% more than last season.
The specialty mushroom category, which includes shiitakes, oysters and other varieties, registered the biggest value gain by percentage in 2015-16.
Specialty sales rose 30% this season to $95 million. The average price, $3.94 per pound was up 40%.