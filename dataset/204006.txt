Not only are the markets keeping a close eye on the Farm Journal Midwest Crop Tour - Machinery Pete is as well.

When the USDA released crop reports ahead of the tour, he said it was a kick in the gut to some. He thinks farmers could be seeing price differences around the country based on crop conditions.

In some places, prices were soft while strong in others.