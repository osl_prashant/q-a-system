Fertilizer prices were mixed on the week with anhydrous and urea softening as all others firmed slightly.
Our Nutrient Composite Index firmed 0.19 points on the week, now 50.62 points below the same week last year at 515.25.

Nitrogen

Urea led overall declines in the fertilizer segment this week.
This week's firmness in nitrogen was led by northern states and Nebraska.
On an indexed basis, UAN28% is our most expensive form of nitrogen this week.

 
 
 
Phosphate

Phosphates were higher with DAP gaining nearly twice that of MAP.
This week's price action is a return to the "see-saw" type action that indicates the market is looking for direction.
The DAP/MAP spread is at the narrow end of our expectations at 14.75.
This week's mixed nitrogen price action pulled some direction out of retail prices, and phosphate took that as the go-ahead to firm slightly.

Potash 

Potash prices continued higher this week.
Mild strength in potash alongside a decline in NH3 prices widened potash's premium to NH3 slightly.
Despite a slightly firmer price this week, we still feel no urgency to book Vitamin K ahead of fall applications.

Corn Futures 

December 2018 corn futures closed Friday, July 14 at $4.13 putting expected new-crop revenue (eNCR) at $657.27 per acre -- lower $11.86 per acre on the week.
With our Nutrient Composite Index (NCI) at 515.25 this week, the eNCR/NCI spread narrowed 12.05 points and now stands at -142.02. This means one acre of expected new-crop revenue is priced at a $142.02 premium to our Nutrient Composite Index.





Fertilizer


7/3/17


7/10/17


Week-over Change


Current Week

Fertilizer



Anhydrous


$489.63


$488.80


-$1.10

$487.69
Anhydrous



DAP


$445.10


$445.05


+$1.79

$446.84
DAP



MAP


$461.30


$460.72


+87 cents

$461.59
MAP



Potash


$333.82


$333.87


+62 cents

$334.49
Potash



UAN28


$238.07


$238.10


+99 cents

$239.09
UAN28



UAN32


$260.52


$258.28


+$1.27

$259.55
UAN32



Urea


$334.69


$333.57


-$4.18

$329.39
Urea



Composite


516.13


515.06


+0.19

515.25
Composite