Editor's Note: The article that follows is part of the 13th edition of The Packer 25 Profiles in Leadership. These reports offer some insight into what drives successful people in produce. Please congratulate these industry members when you see them and tell them to keep up the good work.

David Lake began his journey in the produce industry while in college.
The president and CEO of Los Angeles-based 4Earth Farms even married into a produce family: His wife and business partner Deborah’s grandfather was Norman “Buzz” Bolstad, a pioneer in the Los Angeles wholesale produce industry and the first president of the Fresh Produce & Floral Council.
After graduating, Lake went to work for his father-in-law, David Bolstad, owner of Western Mixer.
“Both Buzz and David were great mentors,” he said. “I learned early that relationships are built on trust and communication.”
He also learned to surround himself with smart, hardworking people and to let them thrive while growing the business.
“Produce is a team sport, and I am fortunate to have a terrific, intelligent and loyal team,” Lake said.
Lake founded MCL Distributing Inc. in 1993 with his business partner Fred McConnell, who retired 15 years ago.
“Easily, the person who had the biggest impact on my career was my mentor and business partner Fred McConnell,” Lake said. “Fred taught me some valuable lessons that are still an integral part of 4Earth Farms today.
“Fred taught me that all decisions begin with the customer’s needs first. That continues to be a guiding principle at 4Earth today. Second, keep it simple. It’s just produce ... do not overcomplicate it.”
After 15 years of doing business as MCL Distributing, the company became MCL Fresh Inc. to better reflect the company’s mission.
“When you grow and have growing relationships all over the world, have a year-round citrus program, house a state-of-the-art packing facility and sell hundreds of specialty, organic and conventional fruits and vegetables, you can hardly be called a distributor,” Lake said.
The company launched its 4Earth label in 2004 and rebranded as 4Earth Farms 10 years later.
Lake said it has been exciting and rewarding to watch the organic sector grow and mature.
“I think the biggest opportunities going forward are the same ones that helped us get to where we are today," he said. "These are availability, quality and telling our story.
“Supply and quality gaps discourage sales, so we have worked hard to develop geographic diversity and a network of farm programs to eliminate gaps,” Lake said.
Like many consumers, organic shoppers want to know where their food comes from and how it is being grown, and Lake said each farm has a compelling story to tell.
“Consumers often say they prefer local produce, but we like to talk about ‘locale’ produce,” Lake said. “To maximize quality, flavor and farm efficiencies, often the best areas to grow sustainably are not local to most markets.”
Lake also said he sees potential growth in foodservice.
“Due to historically inconsistent availability and the challenges of managing multiple recipe ingredients, organic produce has been a challenge for foodservice,” he said. “However, as quality, availability and a wider range of organic items continue to become available, I see unlimited opportunity in the foodservice sector.”