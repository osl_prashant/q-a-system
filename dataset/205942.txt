Following is a snapshot of key data from this morning's USDA Crop Production and Supply & Demand Reports:




 


Yield -- in bu. per acre





 

USDA
 


Avg.


Range


USDA Aug.




Corn


169.9


168.2


166.7-170.9


169.5




Soybeans


49.9


48.8


47.1-49.8


49.4




 




 


Production -- in billion bu.





 

USDA


Avg.


Range


USDA Aug.




Corn


14.184


14.035


13.878-14.249


14.153




Soybeans


4.431


4.328


4.179-4.417


4.381




 
 




 


2016-17


2017-18






U.S. Carry


USDA


Avg.


Range


USDA
			Aug.


USDA


Avg.


Range


USDA
			Aug.




in billion bushels (cotton million bales)




Corn


2.350


2.358


2.320-2.380


2.370


2.335


2.170


1.898-2.420


2.273




Soybeans


0.345


0.365


0.331-0.390


0.370


0.475


0.444


0.375-0.524


0.475




Wheat


--


--


--


1.184


0.933


0.920


0.875-0.943


0.933




Cotton 


2.75


--


--


2.80


6.00


5.68


5.30-6.15


5.80




 




Global Carryover


2017-18
September


2017-18
August


2016-17
September


2016-17
August




Million Metric Tons
 





Corn

202.47


200.87


226.96


228.61



Wheat 

263.14


264.69


255.83


258.56



Soybeans 

97.53


97.78


95.96


96.98



Meal 

12.88


13.06


13.32


13.40



Soyoil 

3.63


3.62


3.66


3.65



Cotton (mil. bales)

92.54


90.09


89.57


89.99




 




Global crop in MMT


2017-18
September


2017-18
August


2016-17
September


2016-17
August




Wheat



U.S.

47.33


47.33


62.86


62.86



Australia

22.50


23.50


33.50


35.11



Canada

26.50


26.50


31.70


31.70



EU

148.87


149.56


145.43


145.70



China

130.0


130.0


128.85


128.85



FSU-12

137.27


133.77


130.74


130.54




Corn



U.S.

360.30


359.50


384.78


384.78



Argentina

42.0


40.0


41.0


41.0



Brazil

95.0


95.0


98.50


98.50



EU

59.39


60.01


61.28


60.71



Mexico

26.0


25.0


27.4


27.4



China

215.0


215.0


219.55


219.55



FSU-12

47.75


49.30


47.36


47.36




Soybeans



U.S.

120.59


119.23


117.21


117.21



Argentina

57.0


57.0


57.8


57.8



Brazil

107.0


107.0


114.0


114.0



China

14.0


14.0


12.9


12.9




Cotton (mil. bales)



U.S.

21.76


20.55


17.17


17.17



Brazil

7.50


7.00


7.00


6.80



India

30.00


29.0


27.00


27.00



China

24.5


24.5


22.75


22.75



Pakistan

9.15


9.15


7.7


7.7




Pre-report expectations compiled by Reuters.