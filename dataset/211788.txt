The volume and value of all U.S. potato exports — including a 9% increase in fresh potatoes — hit record numbers in fiscal year 2017.Potatoes USA reported the gains from July 2016-June 2017 on Aug. 29. Sales hit $1.76 billion and volume reached 71.84 million cwt. at their fresh weight equivalent, according to a news release from Potatoes USA.
According to the release, potato exports were:
Fresh — 491,716 metric tons, up 9%;
Potato chips — 52,103 metric tons, up 5%;
Frozen — 1.03 million metric tons, up 3%;
Dehydrated — 122,063 metric tons, down 10%
Frozen and dehydrated potatoes account for 60% and 24% of the U.S. potato exports; fresh potatoes have 15% of the export market, according to the news release. Seed potatoes showed a 97% increase, according to the group, but those numbers aren't definite because fresh potatoes are often misclassified as seed potatoes.
Japan is the largest export market for U.S. potatoes, followed closely by Canada. A total of 680,264 metric tons went to Japan in the past fiscal year, according to Potatoes USA, and 635,463 metric tons of fresh and processed potatoes were shipped to Canada. Mexico is in third place, with 527,464 metric tons of potatoes sent from the U.S. Fresh potatoes are still restricted to a 26-kilometer zone in Mexico.
Potatoes USA is the marketing and promotion board for commercial potato growers. It sees growth opportunities for U.S. exporters, according to the release, even as the strong U.S. dollar and competition from the European Union challenge growers.
“However, prospects still look good for U.S. exports as the dollar has weakened over the past six months and U.S. processors are expanding capacity while ongoing efforts could increase access for U.S. fresh potatoes to a number of markets,” according to the release.