The Crop Trust, which runs a so-called doomsday seed vault in the Arctic, secured a doubling of its core funds on Friday and urged the private sector to do more to safeguard commercial food production.Friday's pledges totaling about $150 million were mainly from governments, including the United States, Germany and Australia, and will lift the organization's endowment fund to $300 million, it said in a statement after a pledging meeting in Washington.
Marie Haga, executive director of the Crop Trust, said that she hoped to attract more private sector investment to the fund, which works to preserve millions of varieties of seeds against threats such as climate change and disease.
"We don't have a well-functioning global system for conservation of coffee," Haga told Reuters, referring to the type of projects that require funding.
"If you're dependent on coffee for your business, it's a good idea to conserve coffee. If you're dependent on citrus for your business, it's a good idea to make sure that farmers have the right varieties to use when they produce lemons and oranges."
The Crop Trust has started to cooperate with coffee companies, she said.
"This is an approach I want to take crop by crop," Haga added. 
She also said that the Crop Trust is working with Deutsche Bank on initiatives including a bond linked to food security.
Pledges on Friday for the Crop Diversity Endowment Fund included $60 million over five years from the United States, $27 million from Germany and $4 million from Australia, the trust said.
The trust's main global store is its doomsday vault in the Norwegian Arctic archipelago of Svalbard. The vault is a deep, frozen store against cataclysms such as nuclear war and has capacity to store 4.5 million varieties of crops, from wheat to coconuts.
It also runs other gene banks storing seeds around the world. 
"Seeds are miracles. But it does not take a miracle to safeguard this material," Haga said. 
The fund's long-term goal is to raise $850 million for the endowment fund.