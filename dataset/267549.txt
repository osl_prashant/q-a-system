AP-MT--Montana News Digest, MT
AP-MT--Montana News Digest, MT

The Associated Press



Montana at 6 p.m.
The desk can be reached at 406-442-7440. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
UPCOMING TOMORROW:
SCHOOL CHOICE
HELENA — The Montana Supreme Court is being asked Friday to decide whether a state tax-credit program discriminates against religious schools in a school-choice case that has drawn attention from educational groups across the nation.
TOP STORIES TODAY:
TRADE WAR-MONTANA BEEF
BILLINGS — A brewing trade war between China and the U.S. comes at an inopportune time for Montana ranchers, who are seeking to close a multi-year deal to export up to $200 million of beef through China's largest online retailer, industry representatives said Thursday. The export agreement between the Montana Stockgrowers Association and Chinese e-commerce giant JD.com was touted as a landmark agreement when it was announced in November, soon after the lifting of longstanding restrictions on Chinese imports of U.S. beef. By Matthew Brown. SENT: 640 words, photos.
PRIVATE PRISON-MONTANA
HELENA — Montana and CoreCivic are at an impasse in negotiations a little more than a year before the private prison company's contract to run Crossroads Correctional Center expires and the state would have to buy the 550-bed Shelby prison. Montana Department of Corrections director Reginald Michael and Gov. Steve Bullock's budget director, Dan Villa, traveled to Nashville, Tennessee, this week for talks with CoreCivic. By Matt Volz. SENT: 500 words.
OF MONTANA INTEREST:
TRADE WAR-POLITICAL FALLOUT
The international trade dispute playing out in Washington poses a direct threat to the economies of small communities in red and blue states, from California's central valley through Minnesota's plains and across Missouri, Indiana and into Ohio. Republican Donald Trump's trade policies are also exposing an unexpected political vulnerability in what was supposed to be the Republican Party's strongest region: rural America. By Nick Geranios, Steve Peoples and Steve Karnowski. UPCOMING: 800 words by 5 p.m.
INTERIOR-METHANE RULE
WASHINGTON — An on-again, off-again effort to restrict harmful methane emissions on federal lands is off — again. A federal judge in Wyoming has halted the clean-air rule indefinitely, saying it "makes little sense" to force oil and gas companies to comply with the Obama-era rule when the Trump administration has moved to roll back the 2016 regulation. By Matthew Daly. SENT: 440 words, photos.
DEFENSE CONTRACTOR-KICKBACK SCHEME
SANTA FE, N.M. — The former president of a New Mexico-based defense contractor has been sentenced to more than three years in prison for his role in a scheme that solicited and accepted kickbacks for wartime reconstruction projects in Iraq. Neal Kasper of Fairfield, Montana, was sentenced Tuesday in federal court in Santa Fe after pleading guilty in February 2016 to charges of wire fraud and conspiracy to solicit and accept kickbacks. SENT: 290 words.
IN BRIEF:
— TRUMP-BORDER-MONTANA — Montana Gov. Steve Bullock says he'll never deploy National Guard troops "based simply on the whim of the President's morning Twitter habit."
— SPERRY CHALET — The National Park Service says public comments favor plans to rebuild the Sperry Chalet dormitory using its rock walls where they stand after the building in Glacier National Park was gutted by a fire last August.
— SCHOOLS SUPERINTENDENT-JUNEAU — The Seattle School Board has selected Montana's former superintendent of public instruction Denise Juneau as the next superintendent of Seattle Public Schools
SPORTS IN BRIEF:
— BKC-MONTANA-RORIE-DRAFT — Montana junior guard Ahmad Rorie has announced his intention to declare for the NBA draft.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Montana and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.