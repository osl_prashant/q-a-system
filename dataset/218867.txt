The Vanguard International Group, Issaquah, Wash., has launched a new website at vanguardfresh.com.
The site is fully responsive and has interactive tools such as grower maps, customizeable and sharable produce availability data, vidoes, news updates and announcements, according to a news release.
“Our new website is a continuation of our vision of where we will take Vanguard,” CEO Craig Stauffer said in the release.  
“We want to provide content that aids everyone, from the growing stage to fresh fruits and vegetables on shelves for customers; a page that they will bookmark and return to often that makes their daily tasks more efficient.” 
The site URL is vanguardteam.com in China, according to the release.