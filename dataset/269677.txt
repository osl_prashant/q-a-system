Iceland company to resume commercial hunting of fin whales
Iceland company to resume commercial hunting of fin whales

By EGILL BJARNASONAssociated Press
The Associated Press

REYKJAVIK, Iceland




REYKJAVIK, Iceland (AP) — A whaling company in Iceland said Tuesday it is preparing its fleet to bring commercial hunting of fin whales back to the Nordic island nation after a two-year freeze.
Whaling company Hvalur hf (Whale Inc.) said it is readying two vessels for the 100-day summer whaling season. Fin whale hunting stopped in Iceland after the 2015 hunt, when Japanese authorities refused to import Iceland's catch because of unmet health code requirements.
Fin whales are the world's second-largest whales after blue whales, and Iceland is the only country where the marine mammals can be hunted commercially.
The fin whale population is considered critically low outside the Central North Atlantic region surrounding Iceland. The latest counts from 2015 put the region's population at 40,000, the highest on record, Gisli Vikingsson, head of whale research at Iceland's Marine and Freshwater Research Institute, said.
The institute is responsible for recommending a quota for sustainable whale hunting. Since 2009, when the Icelandic government resumed permitting whale hunting after a 20-year pause, the quota has been around 160 animals annually.
"The common misconception is that we are allowing an endangered species to be hunted," Vikingsson said "But it is only in the southern hemisphere that the fin whale population is critical."
For Iceland, a small island nation of about 340,000 people, the whaling industry has long drawn criticism from Western governments and international NGOs.
German activists from Greenpeace once boarded a freighter ship in Hamburg to prevent it from leaving for Japan with a cargo of Icelandic fin whale meat.
In 2014, the United States government outlined a number of actions it planned to take against Iceland because of whaling. No measures were imposed since fin whale hunting stopped a year later for commercial reasons, with Japan being a vital market for the Icelandic industry.
Whale Inc. manager Kristjan Loftsson told The Associated Press the company is working with Japanese officials on methods to fulfill the Asian country's standards for fresh meat imports. How many fin whales its crews catch starting in June depends mostly on the weather, he said.
"We believe the red tape is settled now — at least we'll take our chances," Loftsson said.