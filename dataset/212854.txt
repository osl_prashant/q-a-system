Produce marketers generally agree organics have no scientifically proven nutritional advantage over conventional counterparts, but consumer perceptions are a different matter.“It is interesting that, according to recent research, putting an organic label on food products makes it seem healthier,” said Jacob Shafer, marketing and communications specialist with Salinas, Calif.-based Mann Packing Co.
Organic foods have a healthy image in general, so a “health halo” surrounding the category could help drive sales for organic products outside of produce, Shafer said.
“But generally, consumers know that (conventional and organic) produce is healthy,” he said.
Westlake Village, Calif.-based Dole Food Co. is building an organics-focused website, but the company long has marketed its products as healthy, said Bil Goldfield, director of corporate communications.
“The ‘health halo’ exists for all fresh produce, and certainly for organic, but what the organic consumer is looking for the most is authenticity and transparency in their food choices,” he said.
Consumer demand for organics is growing at a rapid pace, and perception about the category’s nutritional cachet is driving much of that demand, said Steve Lutz, senior strategist for CMI Orchards, Wenatchee, Wash.
“It is clear that organic shoppers are inherently invested in their health and well-being and truly believe in the health benefits of organic produce,” Lutz said.
“To them, eating organic foods is a way of life and is something they feel good about.”
CMI offers organics under its Daisy Girl brand.
Bill Hahlbohm, owner of Oceanside, Calif.-based Sundance Organics, postulates that organic produce does offer health benefits that conventional lacks.
“There’s some very strong scientific research now, pretty substantial proof that naturally grown and organic is healthier for you,” he said.
Others, who market conventional and organic, disagree.
“Nutrition isn’t a primary reason for organics,” said Robert Schueller, director of public relations for Los Angeles-based World Variety Produce, which markets under the Melissa’s brand and offers 300-400 organic items in a product line that exceeds 1,000 items.
“If you do your research, organics has not proven to be healthier, although that’s a fable that continues to pop up. Though there is this perception that organics are healthier, there’s no concrete info. It will continue to spin in the marketplace. It’s not a factor.”
Plant City, Fla.-based berry grower-shipper Wish Farms has conventional and organic product, but nutrition doesn’t factor into the business model there, said Gary Wishnatzki, owner.
“The truth is, we’re not really looking to push organics over conventionally grown. We’re just giving consumers a choice,” Wishnatzki said.
“The chains we sell to certainly appreciate the choice.”
Perception about nutritional value may play a role in organic sales success, but sustainability is more important, said Mayra Velazquez de Leon, CEO of San Diego-based Organics Unlimited Inc.
“People are learning more about what organic is and this is more about healthier produce and ag practices that make your food healthier with no chemical pesticides, and this does not change the nutritional value,” she said.
“We get a healthier product with a sustainable structure. It’s also about social responsibility — no pesticides, a smaller carbon footprint. It puts everything together.”
Growing without chemicals would seem to provide organics a healthy boost, said Chris Ford, Salinas, Calif.-based organics category manager for Vancouver, British Columbia-based The Oppenheimer Group.
“I feel it is legitimate, because you’re farming without chemicals,” he said.
“I know there’s mixed data and it depends on who you ask — I can’t see how that couldn’t.”
Roger Pepperl, marketing director with Wenatchee, Wash.-based Stemilt Growers LLC, urges caution when talking about organic produce’s nutritional claims.
“You have to be very careful,” he said. “We just talked about some of the home runs we have out there selling 10% to 20% of their fruit organically, but you can’t (criticize) the 80% that’s not.”