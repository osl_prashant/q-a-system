Fresh foods, including fresh produce, play a big role in driving consumers to supermarkets.
The United Fresh Produce Association’s FreshFacts on Retail Report for the third quarter of 2017 reveals an 0.8% increase in household trips to purchase fresh foods  in the last year.
That compares with a 0.3% decrease in trips to purchase all groceries.
The average retail basket size for shoppers purchasing fresh produce is $63, compared with the average basket size of $41 with the purchase of any item, according to the report.
The top attribute that shoppers associate with their last shopping occasion was fresh produce, according to the report.
“As retailers prepare for the movement of packaged food sales online, many are turning to the perimeter as their point of differentiation to bring shoppers into the store,” according to the report.
For the third quarter of 2017 — the 13 weeks ending Sept. 30 — FreshFacts on Retail reports that average fresh produce sales grew 2.5% compared with the same quarter last year. That was well above the growth rates of deli, bakery and seafood of 07.%, 0.6% and 1.2%, respectively. The sales growth of fresh produce only trailed the meat department’s rise of 2.6% for the quarter.
The FreshFacts on Retail report, produced in partnership with Nielsen Fresh and direction from the United Fresh Retail-Foodservice Board of Directors, is sponsored by Del Monte Fresh Produce.
The third quarter report explores how innovation in the produce department is driving consumer demand for alternatives to pasta and rice, according to a news release.
“Health and convenience are both key factors in the rise of new products in the produce department,” Jeff Oberman, United Fresh vice president of trade relations and the foodservice board’s liaison, said in the release. “From kits to veggie noodles, we expect continued growth and expansion of fresh-cut and value-added products, both in private label and branded produce.”
Other highlights of the report include:

An in-depth look at produce innovation, focusing on veggie noodles and cauliflower rice;
Salad kits are now the top-selling type of packaged salad;
Organic sales saw 7.6% growth in the third quarter with more organic items sold at retail; and
Sales and volume trends for top ten fruit and top ten vegetable items at retail.

The complete report can be downloaded free for United Fresh members ($50 for non-members) at the United Fresh website .