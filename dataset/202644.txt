Cincinnati-based Kroger reported a nearly 7% gain in sales for the company's fiscal year ending Jan. 28. 
 
The chain reported its 12th consecutive year of market share gain and said identical store sales - excluding fuel - were up 1% for the year, according to a news release.
 
Excluding fuel, total sales increased 6.7% in 2016 compared to 2015, according to the release, with mergers with Roundy's and ModernHEALTH contributing to the growth.
 
"In 2016, Kroger grew market share, increased tonnage and hired more than 12,000 new store associates," chairman and CEO Rodney McMullen said in the release. "For 2017 and beyond, we will continue delivering for our customers while also setting the company up for our next phase of growth and customer-first innovation."
 
The company reported it added more than 420 ClickList locations for 640 online ordering service locations in 2016.
 
Kroger projects identical supermarket sales, excluding fuel, to range from flat to 1% growth for 2017, according to the release.