Castroville, Calif.-based artichoke grower Ocean Mist Farms has a new blog, From the Heart. 
Publishing new content once a week, From the Heart features seasonal recipes, company updates, community involvement news and highlights from the field, according to a news release.
 
Content contributors include Ocean Mist Farms employees, chef Adrienne Saldivar-Meier and blog contributor Kelsey Preciado from Little Bits of Real Food.
 
“The blog is a way for us to connect to our readers on a more personal level sharing timely content about our both our company and products that they’ve asked us for,” Diana McClean, director of marketing for Ocean Mist, said in the release. “Our intent is to provide a space where creativity, storytelling, and inspiration become part of a daily interaction with people who want to know more about eating fresh.” 
 
“I’m thrilled to contribute fun, healthy and most importantly delicious recipes and ideas to the From the Heart blog by Ocean Mist Farms,” Preciado said in the release. “Ocean Mist Farms products and company values are aligned with everything Little Bits of Real Food stands for, which makes this partnership that much more rewarding.”