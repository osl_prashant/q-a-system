On the Big Island of Hawaii a string of cattle killings has been going on for the past few years and according to local ranchers the case is still not solved.
Based on interviews with ranchers by West Hawaii Today, it appears that more than a dozen cattle have been killed this past year near the community of Naalehu, Hawaii. Ranchers in the southern region of the Big Island say the killings don’t appear to be for meat, but rather someone wanting to get a thrill.
Cattle-owner Armando Rodriguez had a heifer killed on Nov. 19 by an apparent gunshot wound. He reported it to local authorities, something that a number of other ranchers have not done because it is nearly impossible to catch the killer or killers.
The Hawaii Police Department has only had three reported cases of animal cruelty in the area and one case of livestock theft since January 2017.
“Currently, there are no leads and the singular incident remains under investigation,” says Ka‘u police Captain Kenneth Quiocho. “The district of Ka‘u has not had a crime spree associated with this isolated incident.”
Rancher Guy Galimba says he has lost eight or nine cattle in the past year in what he believes are related incidents. Only twice did the animals have any meat harvested.
“It just gets frustrating,” Galimba says. “Nothing gets done other than a report. (The police) can’t really go and find who it is. You basically gotta catch them doing it.”
Other ranchers interviewed by West Hawaii Today echoed those sentiments saying that the cases go unsolved so there is little reason to report when cattle are killed.
A number of ranchers wanted to remain anonymous for fear of retaliation from the cattle killers. In one instance an anonymous rancher says he reported the first cattle death on his ranch and it has been ongoing for years. This year alone he lost two cattle.
Phil Becker has been ranching near Naalehu for two decades and has not lost any cattle, but he says the situation is disturbing.
“It has gotten worse, much worse, in I’d say the last three or four years,” Becker says. “I don’t know if it’s just for the fun of it or retaliation or what the purpose is.”
Losing two cattle this past year to shootings Jerry Benevides believes the problem is primarily isolated to the Kaalaiki Road.
“That road connects Naalehu to Pahala, and that’s pretty much how the outlaws and the poachers get back and forth,” Benevides says.
Police are asking cattle producers to do a better job of reporting crimes, even if it doesn’t result in immediate action, that way authorities get a clearer view of what is going on in the community and can make needed action.
While these killings do appear to be acts of animal cruelty, it is interesting to point out that wild cattle do roam in Hawaii and have been hunted in the past. Lotteries for wild cattle hunting permits are hosted periodically with the last one occurring in 2014.
However, feral cattle are currently protected in Hawaii and may not be hunted unless authorized by the Division of Forestry and Wildlife, like the lottery system implemented three years ago.
Below view a map of the area where cattle are being killed on ranches in Hawaii: