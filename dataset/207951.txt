Atomic Torosian, managing partner of Fresno, Calif.-based Crown Jewels Produce, died July 7 at the age of 67.Torosian was introduced to agriculture early, growing up on a farm in the San Joaquin Valley and loading trucks with produce for 95 cents an hour as a 14-year-old, and he remained in the industry throughout his life.
“Produce was truly his passion,” said longtime friend Keith Johnson, who works in business development for Fowler Packing.
Atomic started Crown Jewels Marketing & Distribution LLC — later renamed Crown Jewels Produce — with Rob Mathias about 20 years ago. The company markets a variety of items, including grapes, Mexican vegetables, apples and pomegranates.
Earlier in his career, Atomic worked as a produce buyer for A&P. He also worked at Tri Produce and had his own venture, Sales King.
“He had a tireless work ethic,” said his brother Ted Torosian, an owner of Custom Produce Sales. “He’d just really outwork anybody.
“His big thing was he built relationships between the growers and the buyers,” Torosian said. “I think that’s what set him apart was just building relationships.”
Johnson, who was working for Kroger when he first met Atomic and who knew him more than 30 years, gave a similar account.
“He was really a student of his growers and his customers,” Johnson said. “He really was connected with people.”
Johnson described Atomic as one of the best salesmen in produce because of those connections. Growers knew he would do everything he could for them even as he had to do the same for buyers, Johnson said.
Ted Torosian noted that several people have told him Atomic was “the LeBron James of the produce industry.”
“He was a professional’s professional,” said Doug Hemly, president of Greene & Hemly, who had known Atomic more than 20 years. “He was a good benchmark for how you go about your business.”
Johnson said Atomic’s lasting contributions include wise counsel — both professional and personal — given to others in the industry.
“He really guided and mentored and taught so many people in the industry,” Johnson said, noting that he could name 10 such people off the top of his head but estimating that there are probably 100.
“When Atomic spoke, everybody listened,” Johnson said. “He was respected by all.
“He touched a lot of people’s lives in the produce industry,” Johnson said.
Atomic is survived by his wife of 47 years, Charlene, their son Wyatt, and siblings Arlene, Max, Tara, Ted and Christine.
A memorial service was July 14 at Peoples Church in Fresno.
In lieu of flowers, donations may be made to Fresno State in support of student athlete scholarships.