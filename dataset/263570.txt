Grain mostly lower and livestock lower
Grain mostly lower and livestock lower

The Associated Press



Wheat for May fell 10 cents at 4.7875 a bushel; May corn fell 2 cents at 3.8675 a bushel; May oats was off 4.85 cents at $2.5100 a bushel; while May soybeans rose 8.50 cents at $10.4075 a bushel.
Beef and pork were lower on the Chicago Mercantile Exchange. April live cattle was off 1.15 cents at $1.2185 a pound; March feeder cattle lost 1.43 cent at $1.4062 a pound; while April lean hogs fell 1.15 cents at $.6572 a pound.