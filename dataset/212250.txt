New Zealand’s apple season is well underway, and high-quality fruit is arriving at U.S. ports, though in lower volumes than originally predicted for some varieties, marketers say.“This season, the crop is going to be well under (New Zealand’s industry’s normal estimate),” said David Nelley, the Vancouver, British Columbia-based Oppenheimer Group’s apple and pear category director.
Nelley said the general “gut feel” is that the crop could be 15% less than what had been originally forecast for all apples.
On the other hand, volumes of Jazz and Envy club varieties that Oppy markets under license from New Zealand-based T&G Global will increase this season, Nelley said.
Jazz and Envy represent more than 60% of the apples Oppenheimer imports from New Zealand, he said.
“We’re looking at a situation where specialty apples supplement the apples from Washington that sold out, so the timing is perfect,” Nelley said.
Oppy expects to import about 1 million boxes of apples and pears from New Zealand this year, Nelley said.
Growing conditions have allowed fruit to size up well, said Jason Bushong, division manager of Giumarra Wenatchee, the Wenatchee, Wash.-based division of the Los Angeles-based Giumarra Cos.
“Fruit size will be good for the U.S. market,” he said.
Giumarra will have more volumes of its newer apple varieties, including the Lemonade — a medium-size, golden-yellow apple with an orange blush — and the Diva, a “densely sweet” deep-red apple with a good crunch.
Meanwhile, Coast to Coast Growers Cooperative — a group that includes Yakima, Wash.-based Borton Fruit, Wenatchee-based Oneonta Starr Ranch Growers and Glenmont, N.Y.-based New York Apple Sales — is promoting the fourth season of its Koru club variety, a bicolored hybrid of the fuji and braeburn varieties.
“(Koru) is on a path to ramp up faster than any other controlled variety,” said Bruce Turner, national salesman with Oneonta.
Coast to Coast Growers will import about 150,000 cartons of Korus from New Zealand this season, about double last year’s volume, he said.
Turner said Koru was the fastest-growing apple variety at retail in the U.S., citing a 235% increase, according to Nielsen Fresh Facts.
The co-op also is bringing in Honeycrisp apples from New Zealand, Turner said. “We’re breaking new ground with it (in New Zealand),” he said.
New Zealand will play an increasingly key role in the group’s ability to supply Honeycrisps to customers, Turner said.
“Although we are one of the largest producers of Honeycrisp apples in the U.S., we can’t quite get to year-round supply,” he said. “Consumer demand for this incredible variety doesn’t drop off in the summer months and, to meet that demand, we began planting Honeycrisp in New Zealand.”
There have been some weather issues in New Zealand this year, but the season, which generally runs from mid-April through mid-August, continues apace, Turner said.
“It’s caused a few problems with the crop — basically a little less color and maybe slightly lower yields and packouts, but it’s not going to have any issues at the retail or consumer level,” he said.
 The co-op also will answer summertime demand for organic apples with the help of New Zealand supplies, Turner said.
“2017 marks our second season of importing from New Zealand, offering 40-pound tray-pack cartons of organic royal gala, fuji, granny smith, braeburn and Pink Lady,” he said.
Wenatchee-based CMI Orchards reported volumes of New Zealand-grown Kiku and Kanzi club varieties are down 20% from initial projections.
“We hope to have imports of both Kiku and Kanzi available through August, but with the fantastic eating quality, this season I would bet we are out sooner rather than later,” said Robb Myers, director of domestic sales.
“A shorter crop and challenging weather in New Zealand indicates that even though we were asking for increases our actual supply of import apples will decline,” Steve Lutz, senior strategist with CMI Orchards. The 22 million carton crop is now estimated at 19.5 million, he said.
Lake City, Minn.-based Courtier Pepin Heights was moving its final shipments of SweeTango apples from New Zealand through its system by early June, said Dennis Courtier, owner and president.
“(With) volumes, we’re always hoping for more, but it was still substantially better than the last couple of years, and the fruit quality was brilliant,” he said.
The season for the “super-premium” variety began in late March, he said.
“We expect that volume will continue to increase,” Courtier said.