Cranberry crops could face major tariff on exports
Cranberry crops could face major tariff on exports

The Associated Press

BOSTON




BOSTON (AP) — The top agricultural food crop in the state could face a new business hurdle for international customers.

                The Boston Globe reports Massachusetts cranberries could face a 25 percent tariff on exports to Europe.
The continent is the top consumer of cranberry exports from the Bay State, which produces 15 percent of the world's cranberries.
The Trump administration's move to impose tariffs on steel and aluminum imports could cause the EU to assess its taxes on incoming U.S. goods.
The state's cranberry industry was valued at almost $70 million in 2016, according to a Globe analysis of government data. The Boston Business Journal first reported the issue earlier this month.
Members of the Congressional Cranberry Caucus are warning of the impact of potential tariffs on the fruit.
___
Information from: The Boston Globe, http://www.bostonglobe.com