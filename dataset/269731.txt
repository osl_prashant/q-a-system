The Animal Agriculture Alliance is gearing up for its 2018 Stakeholders Summit, scheduled for May 3-4 in Arlington, Virginia. Here are 18 reasons why organizers feel you should be there.
1. Learn how to protect your roots
Get inspired to be proud of your past and become forward thinking on how to grow in the future.

2. Be one of the first to hear results from research on antibiotics and animal welfare
Dr. Randall Singer, professor of veterinary medicine at the University of Minnesota, will share the findings of a recent study examining "No Antibiotics Ever" animal production and the effects it could have on animal welfare.

3. Find out what extreme actions animal rights activists are taking
Jason Roesler from Fur Commission USA and Nicole Drumhiller, PhD, from the School of Security and Global Studies at American Public University System will give their presentation “Radical Animal Rights Extremism: Assessing the Nature of the Threat” so we know more about those who threaten our way of life.

4. Make a bid at the Silent Auction
Browse over 35 items at this year’s Silent Auction - there’s something for everyone! Take home the jewelry, a case of bacon or maybe the Star Wars signed posters.

5. Hear Mark Gale talk about Consumer Perspectives on Food Labels
Mark Gale, CEO and partner at Charleston|Orwig, will discuss what average Americans think of food labels and how they buy their food.

6. Ask questions about postmodern animal ag at a panel discussion
“Postmodern Animal Ag Begins Now” will be moderated by Chuck Jolley, president of Jolley & Associates. Hear from Danielle Nierenberg from Food Tank; Janet Riley from North American Meat Institute; and Dallas Hockman from National Pork Producers Council.

7. Hear Dr. Allison Van Eenennaam dissect animal ag in the past and now
Alison Van Eenennaam, PhD is a cooperative extension specialist in animal genomics and biotechnology at University of California Davis. She will ask "Were Those the Days?" and discuss animal agriculture- past and present.

8. Meet people across all sectors 
Meet professionals in every sector of agriculture, from aquaculture to dairy, and from the feed industry to restaurant and retail. The Summit is the premier event connecting industry stakeholders across all sectors of agriculture!

9. Dr. Jayson Lusk will discuss how consumer choice can shape our market
Jayson Lusk, PhD will discuss "The Future of Consumer Choice." He is a professor and head of the department of agricultural economics at Purdue University.

10. Hear about Agriculture's Roots in Washington
USDA’s Under Secretary for Trade and Foreign Agricultural Affairs, Ted McKinney, joins us to talk about agriculture in the Capital.

11. Meet the Alliance Staff
Meet the awesome ladies that run the Alliance and learn how they could help you!

12. Learn why it is important to tell our story
Jenny Splitter, a food, science and health writer; Tamara Hinton of Story Partners and Phil Brasher from AgriPulse will tell us how “Sharing Your Roots” is an important piece of animal agriculture.

13. Learn how to respond to threats toward animal agriculture
Panel moderator Dallas Hockman from National Pork Producers Council will ask Scott Sobel, senior vice president of crisis and litigation communications at kglobal, Jamie Jonker, PhD, from National Milk Producers Federation and Brian Humphreys of Ohio Pork Producers Council questions about “Responding to Activist Tactics: Lessons Learned.”

14. Network with others in your field
With nearly 300 attendees you can make valuable connections with people across your industry.

15. Hear why plant based diets seem to be on the rise
Registered dietitian nutritionists Leah McGrath and Amy Myrdal Miller will discuss how the term ‘plant-based’ became popular and how we, in animal agriculture, can promote a balanced diet.

16. Learn how animal agriculture is affecting the environment, and why most people get it wrong
Frank Mitloehner, PhD, professor and air quality extension specialist at University of California, Davis, will discuss the facts and fiction of animal ag and the environment and will debunk myths about animal ag’s environmental impact

17. Learn what the Animal Ag Alliance does
Become familiar with the Animal Ag Alliance and all we do to connect industry stakeholders, engage with key influencers and protect the agriculture industry.

18.Hear how our resilience has kept agriculture alive
Tyne Morgan, the host of U.S. Farm Report and the 2018 Summit moderator, will close out the Summit with a session on how “Resilience Reigns in Agriculture.”

Don't take our word for it: 95% of 2017 attendees rated the Summit as good or great and 100% say it was worth their time and money. Online registration closes May 1: click here to register.