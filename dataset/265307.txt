BC-Cash Prices, 1st Ld-Writethru,0342
BC-Cash Prices, 1st Ld-Writethru,0342

The Associated Press

NEW YORK




NEW YORK (AP) — Wholesale cash prices Friday
Fri.        Thu.
F
Foods

Broilers national comp wtd av            .9904       .9904
Eggs large white NY Doz.                  2.79        2.74
Flour hard winter KC cwt                 15.15       15.00
Cheddar Cheese Chi. 40 block per lb.    2.1650      2.1650
Coffee parana ex-dock NY per lb.        1.1953      1.1998
Coffee medlin ex-dock NY per lb.        1.3877      1.3894
Cocoa beans Ivory Coast $ metric ton      2752        2752
Cocoa butter African styl $ met ton       7426        7426
Hogs Iowa/Minn barrows & gilts wtd av    52.72       53.93
Feeder cattle 500-550 lb Okl av cwt     171.00      171.00
Pork loins 13-19 lb FOB Omaha av cwt     87.56       85.01
Grains
Corn No. 2 yellow Chi processor bid     3.57¼       3.56
Soybeans No. 1 yellow                  10.03¼      10.04¾
Soybean Meal Cen Ill 48pct protein-ton 372.50      369.00
Wheat No. 2  Chi soft                   4.51¼       4.44½
Wheat N. 1 dk  14pc-pro Mpls.           7.47¾       7.38
Oats No. 2 heavy or Better              2.41¼       2.43¾
Fats & Oils
Corn oil crude wet/dry mill Chi. lb.    .31           .31
Soybean oil crude Decatur lb.           .30¼         .30¾
Metals
Aluminum per lb LME                    0.9339        0.9327
Antimony in warehouse per ton            8700          8700
Copper Cathode full plate              3.0600        3.0276
Gold Handy & Harman                    1346.60      1329.15
Silver Handy & Harman                   16.593       16.400
Lead per metric ton LME                2378.00      2377.00
Molybdenum per metric ton LME           15,500       15,500
Platinum per troy oz. Handy & Harman    954.00       950.00
Platinum Merc spot per troy oz.         948.40       949.10
Zinc (HG) delivered per lb.             1.4618        1.4623
Textiles, Fibers and Miscellaneous
Cotton 1-1-16 in. strict low middling    78.63       78.86
Raw Products
Coal Central Appalachia $ per short ton   63.00       63.00
Natural Gas  Henry Hub, $ per mmbtu       2.62        2.70
b-bid a-asked
n-Nominal
r-revised
n.q.-not quoted<29
n.a.-not available