The beef export market was down overall this past year after a strong dollar had countries looking to trade elsewhere. The strong dollar brought a flood of cheaper beef into the U.S., too.Beef exports were down 12 percent for the year compared to a 20 percent rise in imports.
Australia sent a lot of beef to the U.S. this past year as the country continues to cull cattle from drought impacted areas, says Craig VanDyke with Top Third Ag Marketing. Australia's beef herd has reached its lowest numbers since 1990.
"Do we start to see those imports slow up a little bit? Obviously, we're going to keep an eye on exchange rates," Vandyke says. "That's been the talk of the town."
Beef imports have been opened back up from South America to the U.S. It hasn't resulted in much trade yet withBrazil only accountingfor 4 percent of beef imports into the U.S. last year.
VanDyke notes those imports from Brazil are pre-cooked beef. It isn't fresh beef likesteaks, or even hamburger grind.
Change might be coming down the road though as the dollar distances itself from foreign currency like the Brazilian Real.
Beef imports to the U.S. could potentially shift from Australia to South American countries like Brazil and Argentina.
Despite those import factors, exports shouldn't continue to drop.
Exports are holding steady now, says Derrell Peel, an economist for Oklahoma State University Extension.
"They haven't really increased and probably won't increase a lot in 2016," Peel says of the export market.
Peel expects beef imports will be down significantly for the year, helping to improve the overall trade picture.

Watch VanDyke's interview with AgDay:

<!--

    function delvePlayerCallback(playerId, eventName, data) {
        var id = "limelight_player_351368";
        if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
            DelvePlayer.registerPlayer(id);
        }

        switch (eventName) {
            case 'onPlayerLoad':
                var ad_url = 'http://oasc14008.247realmedia.com/RealMedia/ads/adstream_sx.ads/agweb.com/multimedia/prerolls/agwebradio/@x30';
                var encoded_ad_url = encodeURIComponent(ad_url);
                var encoded_ad_call = 'url='   encoded_ad_url;
                DelvePlayer.doSetAd('preroll', 'Vast', encoded_ad_call);
                break;
        }
    }

//-->



<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->
Watch Peel's interview with AgDay:

<!--

    function delvePlayerCallback(playerId, eventName, data) {
        var id = "limelight_player_351368";
        if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
            DelvePlayer.registerPlayer(id);
        }

        switch (eventName) {
            case 'onPlayerLoad':
                var ad_url = 'http://oasc14008.247realmedia.com/RealMedia/ads/adstream_sx.ads/agweb.com/multimedia/prerolls/agwebradio/@x30';
                var encoded_ad_url = encodeURIComponent(ad_url);
                var encoded_ad_call = 'url='   encoded_ad_url;
                DelvePlayer.doSetAd('preroll', 'Vast', encoded_ad_call);
                break;
        }
    }

//-->


<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->