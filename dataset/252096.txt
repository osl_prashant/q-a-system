Grain mostly higher and livestock mixed
Grain mostly higher and livestock mixed

The Associated Press



Wheat for March up 1.75 cents at 4.9475 a bushel; March corn was up 6.50 cents at 3.8575 a bushel; March oats gained 1.25 cents at $2.6075 a bushel; while March soybeans declined 1.25 cents at $10.5375 a bushel.
Beef was lower and pork was higher at the Chicago Mercantile Exchange April live cattle was off 1.20 cents at $1.2177 a pound; March feeder cattle fell 2.02 cents at $1.4175 a pound; while April lean hogs rose .27 cent at $.6807 a pound.