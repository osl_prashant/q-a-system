Trade worries sink US stocks; China pork duty hits Tyson
Trade worries sink US stocks; China pork duty hits Tyson

By MARLEY JAYAP Markets Writer
The Associated Press

NEW YORK




NEW YORK (AP) — U.S. stocks are tumbling Monday after China raised import duties on U.S. pork, apples and other products. Tyson Foods is among the biggest losers on Wall Street. Investors are also dumping some of their recent favorites, including retailers like Amazon and technology companies such as Microsoft.
Health insurer Humana is jumping on continued reports Walmart might buy the company or announce a new partnership with it.
KEEPING SCORE: The Standard & Poor's 500 index skidded 58 points, or 2.2 percent, to 2,582 as of 11:38 a.m. Eastern time. The Dow Jones industrial average lost 442 points, or 1.8 percent, to 23,660. The Nasdaq composite slumped 190 points, or 2.7 percent, to 6,872. The Russell 2000 index of smaller-company stocks fell 30 points, or 2 percent, to 1,498.
U.S. markets were closed Friday for the Good Friday holiday. Before that, the S&P 500 rose 2 percent last week in choppy trading. The benchmark index lost 1.2 percent in the first quarter of 2018 following nine straight quarters of gains.
TRADE FEARS: China raised import duties on a $3 billion list of U.S. goods in response to a new U.S. tariffs on imported steel and aluminum. Meat producer Tyson Foods slumped $4.02, or 5.5 percent, to $69.17 while Hormel lost 70 cents, or 2.1 percent, to $33.62.
A bigger dispute looms over Trump's approval of possible higher duties on Chinese goods. Investors are worried that increasing tensions over trade could slow down global commerce and hurt corporate profits. China's latest step is just one point of contention between China and Washington, Europe and Japan over a state-led economic model they complain hampers market access, protects Chinese companies and subsidizes exports in violation of Beijing's free-trade commitments. Meanwhile the U.S., Canada and Mexico continue to hold talks about potential changes to NAFTA.
The price of gold climbed 1.1 percent to$1,341.70 an ounce and silver jumped 2.1 percent to $16.61 an ounce as some investors took money out of stocks and looked for safer investments.
WALMART GOES SHOPPING? Health insurer Humana rose following continued reports Walmart could buy the company or create a new partnership with it. The Wall Street Journal reported on the possible deal last week. Humana is a major provider of Medicare Advantage coverage for people age 65 and older. Humana gained $13.08, or 5.2 percent, to $282.73 and Walmart slid $2.86, or 3.2 percent, to $86.11.
Walmart has declined to comment on what would be just the latest major deal in health care: drugstore chain and pharmacy benefits manager CVS agreed to buy health insurer Aetna for $69 billion, while another insurer, Cigna, agreed to pay $52 billion for pharmacy benefit manager Express Scripts this month.
TESLA SLOWS: Tesla stock declined after the electric car maker said Friday that the vehicle in a fatal crash last week in California was operating on Autopilot mode, making it the latest accident to involve a semi-autonomous vehicle. The company said the driver did not have his hands on the steering wheel for six seconds before the crash and did not act to prevent the car from hitting a concrete lane divider. Earlier this month, a self-driving Volvo SUV being tested by ride-hailing service Uber struck and killed a pedestrian in Arizona.
The National Transportation Safety Board said it is "unhappy" Tesla released information about the crash while the NTSB is continues its own investigation.
Tesla fell $16.11, or 6.1 percent, to $250.02. Nvidia, a chipmaker that reportedly stopped its own work on products for semi-autonomous cars after the recent incidents, lost $7.31, or 3.2 percent, to $224.28.
PRIME TARGET: Amazon fell another $6922, or 4.8 percent, to $1,378.12. After peaking at almost $1,600 a share last month, Amazon has slumped recently as investors took a more cautious approach to stocks. The online retailer was also repeatedly criticized by President Donald Trump last week over its shipping deals with the U.S. Postal Service. The White House doesn't appear to be pursuing specific policies that would harm Amazon's business, and much of Trump's criticism has come after unfavorable reporting in The Washington Post, which is owned by Amazon founder Jeff Bezos but is a separate company from Amazon.
Despite its recent losses, Amazon stock is still up about 18 percent in 2018. It wasn't the only market favorite to fall out of favor Monday. Microsoft dropped $2.15, or 2.4 percent, to $89.12 and Google's parent company, Alphabet, shed $29.76, or 2.9 percent, to $1,007.38. Boeing slid $5.30, or 1.6 percent, to $322.58.
BONDS: Bond prices fell. The yield on the 10-year Treasury note rose to 2.74 percent from 2.74 percent after a sharp decline last week.
ENERGY: Benchmark U.S. crude lost $1.83, or 2.8 percent, to $63.12 a barrel in New York. Brent crude, used to price international oils, slid $1.13, or 1.6 percent, to $68.21 a barrel in London.
CURRENCIES: The dollar declined to 106.08 yen from 106.50 yen. The euro dipped to $1.2304 from $1.2306.
OVERSEAS: Trading in France, Germany and Britain was closed for Easter. Japan's benchmark Nikkei 225 lost 0.3 percent and South Korea's Kospi fell almost 0.1 percent. The Hang Seng in Hong Kong was closed as well.
____
AP Markets Writer Marley Jay can be reached at http://twitter.com/MarleyJayAP . His work can be found at https://apnews.com/search/marley%20jay