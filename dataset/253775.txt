Columbine Vineyards president and CEO Martin Caratan has retired.
Chief operating officer Matt Surber will assume leadership of the Delano, Calif.-based company, which was acquired by investment group Agriculture Capital in early March. 
Surber retains his COO title and reports to Agriculture Capital, according to spokesman Wood Turner. Surber has worked for Columbine for many years.
Agriculture Capital has been working to build its AC Foods business into a year-round operation. Before acquiring Columbine, the group was managing about 12,000 acres of permanent crops, including citrus and blueberries.
Its assets include packinghouses in Dinuba, Calif., Lindsay, Calif., Tulare County, Calif., Sublimity, Ore., and Australia.
Columbine Vineyards adds almost 7,000 acres of farm land, a cold storage facility and other assets.
“We believe the farms and facilities of Columbine will become an iconic part of our business and are looking forward to working with Columbine’s customers to meet the growing demand for healthy, delicious food,” Tom Avinelis, managing director of Agriculture Capital, said in a news release.
Caratan also stated his excitement about the acquisition in the release.
“Columbine Vineyards has been my family’s pride and joy for over 90 years, and we are delighted to be entrusting our legacy to Agriculture Capital and to the AC Foods family,” Caratan said. “We were drawn to AC because of the extraordinary amount of care they bring to the land and to the communities where they operate.
“We share with them a deep commitment to the protection and conservation of natural resources and to supporting the people who make the production of food in the Central Valley possible,” Caratan said. “I can speak for all of the Caratans when I say we look forward to AC’s plans and are eager to support them as they move forward.”
Previous acquisitions by Agriculture Capital include Lindsay-based Suntreat Packing & Shipping Co. in late 2015.