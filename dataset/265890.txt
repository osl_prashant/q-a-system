AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:15 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
TOP STORIES:
COLORADO BALLOT RULING
DENVER — A federal judge on Thursday struck down a key element of Colorado's Constitution that made it tougher for citizens to get proposed amendments onto the ballot. U.S. District Judge William J. Martinez ruled that requiring voter signatures for proposed constitutional amendments to be collected from all 35 state Senate districts violates the "one person, one vote" principle contained in the Equal Protection Clause of the U.S. Constitution because voter populations vary among Senate districts. By James Anderson. SENT: 450 words, photo.
PUBLIC EMPLOYEE PENSIONS-COLORADO
DENVER — Colorado's Republican-led Senate have passed a bill to stabilize a state pension program whose billions in unfunded liabilities have led to one state credit downgrade, but the rescue package is certain to be changed in the Democrat-led House. One of the top legislative priorities of 2018, the bill exposed stark differences between Democrats who argue it's unfair that retirees and public employees bear the brunt of stabilization and Republicans who say further delay could lead to insolvency. By James Anderson. SENT: 400 words.
OF COLORADO INTEREST:
XGR-PRISONS-METOO
ANNAPOLIS, Md. — Maryland's Legislature is one of the latest to catch a wave of measures in state legislatures, as well as action by state corrections officials to address the growing recognition about a lack of access to feminine hygiene products in prison. In Colorado last year, lawmakers included a provision in the state's budget bill to provide incarcerated women with free access to tampons. By Brian Witte. SENT: 870 words, photo.
FIRE DANGER-NEW MEXICO
ALBUQUERQUE, N.M. — New Mexico is dry, the spring winds are already in full force and there's plenty of grass that's ready to burn, resulting in what authorities said Tuesday is the perfect recipe for a potentially severe fire season. Dozens of state and federal land managers along with officials from New Mexico's largest city and surrounding communities gathered in Albuquerque to issue a warning to residents around the arid state. By Susan Montoya Bryan. SENT: 500 words, photos.
IN BRIEF:
— TOWNHOMES BURNED — Colorado Springs fire officials say three people were taken to a hospital after a blaze that destroyed or damaged more than 20 townhomes.
— GREAT LAKES-NO FLIGHTS — Great Lakes Airlines, which operated small turboprop planes between destinations in the West and Midwest, has suspended flying.
— MISTAKEN ESTATE SALE — A Colorado woman says her house was ransacked by people who mistakenly thought it was the site of an estate sale.
— FATAL PLANE CRASH — The Oklahoma Highway Patrol says two men were killed when the plane they were flying from Texas to Oklahoma crashed in central Oklahoma.
— ESCAPED INMATE — An inmate who escaped from a prison in southern Colorado has been found.
— LAND MANAGEMENT-GRAZING PROJECTS — Federal land managers have designated 11 demonstration projects in six Western states in a bid to create more flexibility for grazing livestock on public range.
— ROCKY FLATS REFUGE OPENING — The U.S. Fish and Wildlife Service plans on opening the Rocky Flats Wildlife Refuge to the public this summer following multiple public forums and a lawsuit that tried to keep the refuge from opening at all.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for May fell 5.25 cents at 4.49 a bushel; May corn was unchanged at 3.74 a bushel; May oats was off .50 cent at $2.26 a bushel; while May soybeans lost 6 cents at $10.1950 a bushel.
SPORTS:
NUGGETS-RAPTORS
TORONTO — The Denver Nuggets play the Raptors in Toronto. (Game starts at 5:30 p.m. MT)
SPORTS IN BRIEF:
— OLY-US TEAM-WHITE HOUSE — The U.S. Olympic team has been invited to the White House for a visit April 27, leaving the 244 members about four weeks to decide if they'll attend.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.