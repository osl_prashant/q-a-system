CHICAGO — Just a few years ago, Ready Pac Foods was in trouble.The Irwindale, Calif.-based company — known for its innovation over the past several decades — was on the brink of bankruptcy.
“We’d lost our operational edge,” said CEO Tony Sarsam during his keynote address at the United Fresh Produce Association’s FreshMKT conference. “We had no capability to innovate.”
Sarsam, who joined the company in the midst of its issues in 2013, said fundamental changes were in order.
“We had to focus on our people, we had to be technically sound, and we had to have an adaptable strategy,” he said. “We began investing in our people, in training, in pay and all the elements to get people back on the path.
“In 18 months, we had put more money into wages than the entire EBITA we had generated but we believed if we commit to our folks, our folks will pay us back,” Sarsam said June 13.
Armed with a new mission, “Giving people the freedom to eat healthier,” Ready Pac focused on four key consumer trends:
Healthy eating
Convenience
Indulgence
Value
“That mission has given us a lot of ways to expand,” he said.
And that expansion, announced on Wednesday, is the company’s new Fresh Prep’d line, which kicks off this fall with soup and wrap kits, which Sarsam said bring “real freshness and real texture and a great eating experience.”
Sarsam stressed to the audience that innovators don’t have to be the “hero inventors” but they do have to come up with more than incremental changes. He used Seattle-based Amazon as an example.
“Best I can tell they’ve invented nothing, but they’re one of the most innovative companies,” he said. “You need an execution machine — no great hero inventions. They’re a sound operator and anticipate what the consumer wants and get it to the market a little earlier than everyone else.”