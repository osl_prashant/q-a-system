The late winter market for Brussels sprouts has been characterized by excellent quality and high yields, but an overabundance of product from increased planting and yields has driven the market ever downward, grower-shippers say.
"Brussels sprouts deals the last two to three years have been probably in the range of $25-40, and last year's margin was even $60, but this year's average has been $15-25," said Jason Lathos, commodity manager for Salinas, Calif.-based Church Brothers Farms.
Brussels sprouts used to be more of a specialty crop provided by just a few shippers, he said. Now they've become more of a hardware item offered by multiple shippers.
"Five years ago, you probably had five guys (selling sprouts), and now you have 25 to 30 guys offering them," Lathos said.
He added that 100% of his late February crop was coming from Mexico.
Bob Montgomery, sales manager for Guadalupe, Calif.-based Beachside Produce LLC, said the market is very depressed.
"Mostly on a 25-pound carton loose medium, the price has been $12-14, which is just a terrible market," he said.
Don Hobson, vice president of sales and marketing for Oxnard, Calif.-based Boskovich Farms Inc., said he didn't see the market changing until warmer weather at the end of March and early April.
"We should see the market picking back up, but right now it's a great time for promotions because there's plenty of supply," he said.
"There's never been any product in central Mexico, and now we're seeing a lot of Brussels sprouts grown in central Mexico crossing into Texas, and we've never seen that before."
Ocean Mist Farms, Castroville, Calif., has gone the promotion route to move Brussels sprouts, said John Shaw, Brussels sprouts commodity manager.
"This increase in volume has also created opportunity, (and) we were able to promote both bulk and our Season and Steam Brussels sprouts throughout the winter with our retail partners," he said in an e-mail.
Jamie Strachan, CEO of Salinas, Calif.-based Growers Express/Green Giant Fresh, said in an e-mail that Brussels sprouts were continuing to gain popularity, keeping them on-trend with consumers and foodservice operators.
"Our complete line of sprouts products make it easy for them to be used in a variety of dishes and cooking applications," he said. "Retailers have seen their Brussels category, and their bottom lines, grow with Green Giant Fresh Brussels sprouts."
Still, oversupply of Brussels sprouts remains a problem.
Henry Dill, sales manager for Salinas, Calif.-based Pacific International Marketing, said there's more acreage and more people who have seen the kinds of profits (others) have been able to make in the commodity in the last two or three years, and they have added that to their program.
Meanwhile, existing Brussels sprouts growers also have expanded their acreage.
"As a result of that, it's a perfect storm," Dill said.