Several Ontario potato growers are focusing on organics this year, while the demand for new conventional varieties continues to grow.Bill Nightingale Jr., president of Nightingale Farms in La Salette, Ontario, has partnered with neighboring grower Aaron Crombez to grow and pack 70 acres of certified organic potatoes under his Norfolk Organics label.
“During our local season, about 80% of Ontario’s organic potatoes come from British Columbia and Prince Edward Island,” Nightingale said.
He plans to start harvesting in July and hopes to have organics until January, or whenever supplies run out.
“It gives us something to do in late fall and winter to put more pallets on the truck,” he said.
Trevor Downey, owner of Shelburne, Ontario-based Downey Potato Farms, an hour north of Toronto, is excited about his new organic farm within Rock Hill Park.
He plans to increase his organic acreage by 25% this year for the Downey Farms Organic and Loblaw’s PC Organic label.
“Organics is a small percentage of our business and it’s challenging, but our retail customers are asking for it, people are conscious about what they eat, we’ve got the right soil and we’re close to market,” he said.
In conventional potatoes, Downey sees increased demand for yellows. Along with Yukon Gem, he’s growing varieties such as Satina and Colomba.
 

Specialty spuds

He’s also excited about Ripple, a creamer-sized fingerling with a rough surface for the PC brand.
Downey plans to continue growing pink-skinned Sweet Cerises for his Bistro Fresh label.
Burlington-based EarthFresh Foods Inc., meanwhile, is conducting trials of new varieties for its NutriSpud line, which debuted last fall and quickly sold out.
“We’re looking for varieties that look good, taste good and offer a healthy attribute,” said field manager Len Brackenbury.
Yellow-fleshed Carisma, the first NutriSpud, has a lower glycemic response, meaning it does not cause the rapid spike in blood sugar that normally comes from eating carbohydrate-rich foods, said marketing manager Stephanie Cutaia.
“It’s a specialty item and the price reflects that, but after the media release last fall we had all kinds of people asking where they could buy it,” Brackenbury said.
As part of its regular line-up, he said EarthFresh has increased its acreage on the white-fleshed Whitney variety after several years of trials. He said sales of specialty potatoes such as creamers and fingerlings continue to increase.