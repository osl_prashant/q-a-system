Fonterra Co-operative Group Ltd said on Tuesday that milk production in its home market New Zealand dipped in February due to unfavorable weather.
Production volumes in New Zealand were down 2 percent in February as pasture quality was affected by “difficult weather conditions”, the company said in a statement.
Meanwhile, Fonterra’s Australia milk production rose 2 percent in February as weather conditions, especially in South East Australia, were favourable.
February’s production figures could impact New Zealand’s fortnightly milk auction, due on April 4.
In the last auction on March 21, prices dropped for the third consecutive time, as production continued its slow pickup from weaker levels earlier in the season.
Last month, the firm reported a 5 percent fall in January’s milk production on account of dry weather.