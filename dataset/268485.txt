BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Wednesday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMay                              120.25  Up    .70May      117.85  118.35  116.50  118.15  Up    .55Jul                              122.30  Up    .65Jul      119.70  120.45  118.60  120.25  Up    .70Sep      121.70  122.50  120.70  122.30  Up    .65Dec      125.30  125.75  124.15  125.60  Up    .60Mar      128.50  129.30  127.80  129.10  Up    .60May      130.75  131.65  130.25  131.45  Up    .55Jul      133.10  133.65  132.55  133.60  Up    .55Sep      135.15  135.55  134.55  135.50  Up    .55Dec      138.10  138.30  137.75  138.30  Up    .50Mar      140.90  141.05  140.50  141.05  Up    .45May      142.45  142.85  142.25  142.85  Up    .45Jul      144.15  144.60  143.90  144.60  Up    .40Sep      145.75  146.30  145.50  146.30  Up    .40Dec      148.15  148.80  147.90  148.80  Up    .35Mar                              150.65  Up    .35