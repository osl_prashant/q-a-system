Spring is here and forages are starting to take off before the first hay cutting begins around much of the country. Producers are making harvest plans, but they may want to reevaluate their strategy to better optimize forage quality.
Nearly every hay or silage acre that farmers have available is capable of growing dairy quality forage if managed and harvested timely, says Joe Lawrence, dairy forage systems specialist at Cornell University.
“I would encourage folks to start think of every acre and cutting as a different opportunity,” Lawrence says.
Working around the weather is a concern for many producers. Hay cutting and baling require forage growers to often work between rains.
Lawrence suggests producers try managing for the weather rather than letting the weather manage them.
“There's a lot of one and two day opportunities or a day or two following just a trace of rain,” Lawrence says. “We may not be willing to pull the trigger and mow hay if two days later, there's a bunch more rain forecast.”
Wide swathing, raking or bagging haylage could all be ways to help speed up harvest and fit it into a narrower window.
“There’s a lot of opportunities where we’re using one of these more intensive systems,” Lawrence says.
The first cutting on a field often sets the stage for how the rest of the harvest season will go.
“First cutting is a really good opportunity for some high quality feed,” Lawrence says.
Lawrence thinks it is better for producers to focus the early season cuttings on getting quality forage from all the acres rather than pinpointing particular fields for high lactation or dry cow feed. Pre-determining that a field is already for heifer or dry cow consumption because you think it has lower forage quality is a lost opportunity to produce high quality forage.
Should enough high quality forage be harvested in the first or possibly second cuttings, any remaining cuttings can be focused on yield.
For more on making the most of your forage program watch the following Cornell ProDairy Webinar: 

 
Note: This story ran in the April 2018 magazine issue of Dairy Herd Management.