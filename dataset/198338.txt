Have you ever wondered about the difference between a manager and a leader?Jennifer Hill has. Having held senior leadership positions with some top global financial institutions and in her current role as CFO for Global Banking and Global Markets with Bank of America, she's learned a thing or two about what leadership is - and isn't.
How does she define leadership? "When I think about the qualities I admire most in leaders, three words come to mind," she shared. "They are responsibility, accountability, and decisiveness."
Jennifer also stresses that we shouldn't confuse leadership with management. "There is a big difference," said Jennifer, who knows this from experience. "I have been led by some great people and I'd been micromanaged by some horrible people," she admitted. Here are five lessons on leading, not managing, that Jennifer learned from the best leaders she has worked with.

1. Listen more and speak less.
Jennifer pointed out that being a good listener and hearing the thoughts and inspirations of others can help you, even when you're the one in charge. And, she added "People feel good when they feel heard."

2. Consensus is good but‚... direction and decisiveness create action.
Leading by consensus will rally a team around a common goal, but according to Jennifer, that alone is not enough to be a leader and create change. "Consensus is good but direction and decisiveness create action," she emphasized. "You must guide consensus."

3. Anyone can identify a problem. A leader is part of the solution.
"Leaders don't sit around and complain about what's wrong," Jennifer said. They drive solutions. "They figure out how to make it right," she added.

4. Apologize publicly and gloat privately.
True leaders don't just praise publicly and criticize privately. They are also humble enough to apologize publicly and gloat privately.
"You don't see leaders bragging about their success," said Jennifer. "They talk about their team and their team's contributions. And if they feel really good about themselves, they do it at home or with a close friend but not publicly." A leader will apologize and take accountability their actions.

5. Give the hard message.
"It's very easy to praise somebody. It's easy to give somebody a raise or a good review." It can be much harder, however, to have the tough conversations.
While admitting that this might sound like a harsh thing to say, Jennifer pointed out that not everyone does an outstanding job all the time. "You've got to tell them, and then you've got to move on," she acknowledged.

The takeaway
Her parting words were filled with encouragement to strive to be a leader and not just a manager. "Learn to inspire and motivate as opposed to dictate," she said in closing. "At the end of the day, good leaders often don't do the work but they make you feel like they did."

Author: Jo Miller, Women's Leadership Coaching Inc.