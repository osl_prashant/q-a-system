Virginia farm worker becomes buried in soybeans, dies
Virginia farm worker becomes buried in soybeans, dies

The Associated Press

AMELIA COURTHOUSE, Va.




AMELIA COURTHOUSE, Va. (AP) — Authorities say a Virginia farm worker was killed when he became buried in soybeans.
The Richmond Times-Dispatch cites an Amelia County Sheriff's Office release that says 30-year-old Dustin Lee Arthur was pronounced dead at the scene Tuesday evening.
The sheriff's office said Arthur was wearing a safety harness and was tethered to the grain bin at the time. Amelia County's director of emergency management, Kent Emerson, says rescuers had to create a way to safely enter the bin before digging to find Arthur, who was found deceased.
Emerson said Arthur was killed as farm workers were unloading the grain bin into a container truck. Authorities said Arthur had been trying to free the flow of beans.
___
Information from: Richmond Times-Dispatch, http://www.richmond.com