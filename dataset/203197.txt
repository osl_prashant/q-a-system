Branded apples are entering the peak season for distribution and sales.
 
National supermarket scan data results show February and March are particularly key months for nabbing incremental sales with branded apples, said Steve Lutz, vice president of marketing for Wenatchee, Wash.-based CMI Orchards. 
 
That is a win for both suppliers and retailers, he said. 
 
"In the fall there are a tremendous number of local, regional apples and other niche items that are out there that are only in the stores for a limited amount of time and that makes it difficult to get shelf presence with branded apples," he said Jan. 17. "So as we turn the corner on the new year what we see and the data confirms is those local and regional varieties dry up and successful retailers pivot to jump on distribution and promotion of branded apples," he said. 
 
Typically, local apples tend to carry lower price points, so when that volume is replaced by higher priced branded apples - often priced near $2 per pound - category dollars will grow.
 
"That's where we see the lift, because those branded apples go from about 5% to 10% of the category sales once we get into the new year," he said. 
Lutz said mid-January to mid-April is key for branded apple sales.
 
"On a number of these varieties, the strongest sales of the entire year occur in February and March," he said. "Retailers are looking for new items to promote and they are willing to get behind them and consumers are looking for new flavors, so everything comes into alignment and it works."
 
In general, Lutz said legacy varieties like red delicious, golden delicious and mcIntosh have been giving up shelf space as new branded varieties gain attention. 
 
Robb Myers, CMI's vice president of sales, said in a news release that supplies of the company's Ambrosia, Kiku and Kanzi branded apples in larger sizes will support late winter retail promotions. There are strong selling opportunities for larger sizes in Envy and Pacific Rose apple varieties, Myers said in the release. Two-pound pouch bags are a good way for retailers to introduce consumers to branded apples, he said.
 
CMI Orchards is the exclusive U.S. grower of Ambrosia and co-marketer of U.S. grown Kiku, Kanzi, Jazz, Envy and Pacific Rose,