Though the H-2A guest worker program has helped soften the crisis, farm labor remains a top issues for Santa Maria growers.
“Our big three issues are labor, water and land use,” said Claire Wineman, president of the Grower Shipper Association of Santa Barbara & San Luis Obispo Counties.
The association’s members tend to grow cool season vegetables including broccoli, lettuce, celery and cauliflower and strawberries, raspberries, blackberries and blueberries.
As for farm labor, Wineman said the issue continues to be a priority issue for 2018.
“We continue to struggle with the availability of labor,” she said. “People have been increasingly looking for the H-2A agriculture guest worker visa program to help alleviate (farm labor).
“We have seen an increase in the utilization of the H-2A program, and that has helped somewhat with the labor shortage,” she said, noting the program is often used in the July through September period.
Even so, growers have reported labor shortages are costing them money.
“I think we are still short,” Wineman said.
“There are some people that are OK, who have just enough, but there are other members who are reporting shortages.”
In the fall of 2017, members reported at 16% shortage for farm labor for vegetables and a 19% shortage for berries. That is down slightly from 2016, when shortages of 22% in vegetables and 26% in berries were reported.
In 2016, Wineman said association members reported short labor conditions resulted in a $7 million loss in potential gross revenue, down from a reported $13 million loss in 2015.
“It is unclear where the changes were due to the H-2A guest worker program helping to alleviate the labor shortage or other factors,” she said.
 
Water issue
Water volume and quantity is always top of mind for association growers, Wineman said.
“Last year was a very helpful rain year in terms of the amount and timing,” she said.
This year hasn’t been as abundant, though Wineman said rain and unseasonable cold stymied March production of berries.
By 2020, growers are expected to face regulations related to expanded water reporting requirements for nitrogen fertilizer applied and nitrogen removed. Growers will face increased down-water quality trend monitoring.
Growers also face increased pesticide regulations in the ag-urban environment, particularly related to the limitations on pesticide use near schools and daycare centers, Wineman said.
Growers also are dealing with the phase-in of a higher minimum wage and a phase-out of overtime exemptions for agriculture.
“Availability and cost of labor will be a challenge,” she said.