David Shepherd’s 1951 John Deere R has a lot of memories attached to it. 

His father, Lowe, farmed with the tractor in Helena, Oklahoma for several years, and initially David was fearful of the tractor when it was brought to the field.

Watch Tractor Tales every weekend on U.S. Farm Report.