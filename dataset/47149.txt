USDA Weekly Export Sales Report
			Week Ended March 23, 2017




Corn



Actual Sales (in MT)

Combined: 841,900
			2016-17: 716,900
			2017-18: 125,000



Trade Expectations (MT)
1,000,000-1,500,000


Weekly Sales Details
Net sales of 716,900 MT for 2016/2017 were down 47 percent from the previous week and 28 percent from the prior 4-week average. Increases were reported for Peru (205,800 MT, including 155,000 MT switched from unknown destinations), Mexico (189,300 MT, including decreases of 5,200 MT), Japan (180,000 MT, including 56,200 MT switched from unknown destinations and decreases of 17,600 MT), Colombia (149,300 MT, including 140,500 MT switched from unknown destinations and decreases of 8,600 MT), and South Korea (133,000 MT, including 500 MT switched from Taiwan and decreases of 3,400 MT). Reductions were reported for unknown destinations (273,600 MT) and Canada (10,700 MT). For 2017/2018, net sales of 125,000 MT were reported for Mexico (78,200 MT), Nicaragua (30,000 MT), and unknown destinations (17,100 MT). Reductions were reported for Japan (300 MT). 


Weekly Export Details
Exports of 1,402,800 MT were up 2 percent from the previous week, but down 4 percent from the prior 4-week average. The primary destinations were Colombia (398,200 MT), Mexico (228,000 MT), South Korea (191,200 MT), Taiwan (187,800 MT), and Japan (155,600 MT).


Comments and Performance Indicators
Sales fell short of expectations. Export commitments for 2016-17 are running 50% ahead of year-ago compared to 51% ahead the week prior. USDA projects exports in 2016-17 at 2.225 billion bu., up 17.2% from the previous marketing year.



Wheat



Actual Sales (in MT)

Combined: 627,600
			2016-17: 464,100
			2017-18: 163,500



Trade Expectations (MT)
300,000-650,000 


Weekly Sales Details
Net sales of 464,100 metric tons for delivery in marketing year 2016/2017 were up 11 percent from the previous week and 33 percent from the prior 4-week average. Increases were reported for Saudi Arabia (120,000 MT), the Philippines (118,400 MT, including 57,000 MT switched from unknown destinations and decreases of 1,000 MT), Mexico (73,400 MT, including decreases of 1,600 MT), Indonesia (70,000 MT), and Taiwan (48,500 MT, including 42,000 MT switched from unknown destinations). Reductions were reported for unknown destinations (53,800 MT) and Honduras (2,000 MT). For 2017/2018, net sales of 163,500 MT were reported primarily for unknown destinations (86,300 MT), Thailand (39,000 MT), and Nigeria (22,000 MT). Reductions were reported for Mexico (9,500 MT). 


Weekly Export Details
Exports of 495,400 MT were down 24 percent from the previous week and 11 percent and from the prior 4-week average. The destinations were primarily the Philippines (107,400 MT), Morocco (91,200 MT), Mexico (82,800 MT), Nigeria (55,000 MT), and Taiwan (46,300 MT).


Comments and Performance Indicators
Sales came in at the upper end of expectations. Export commitments for 2016-17 are running 37% ahead of year-ago versus 37% ahead the week prior. USDA projects exports in 2016-17 at 1.025 billion bu., up 32.3% from the previous marketing year.



Soybeans



Actual Sales (in MT)

Combined: 996,600
			2016-17: 681,000
			2017-18: 315,600



Trade Expectations (MT)
450,000-850,000 


Weekly Sales Details
Net sales of 681,000 MT for 2016/2017 were up 15 percent from the previous week and 42 percent from the prior 4-week average. Increases were reported for Germany (208,900 MT), Mexico (172,700 MT, including decreases of 1,300 MT), China (89,200 MT, including 80,000 MT switched from unknown destinations), Indonesia (72,700 MT, including 50,000 MT switched from unknown destinations), and Egypt (66,000 MT). Reductions were reported for unknown destinations (99,000 MT) and Singapore (300 MT). For 2017/2018, net sales of 315,600 MT were reported for China (183,000 MT), Pakistan (66,000 MT), unknown destinations (66,000 MT), and Japan (600 MT). 


Weekly Export Details
Exports of 951,500 MT were up noticeably from the previous week and 19 percent from the prior 4-week average. The destinations were primarily China (479,700 MT), Germany (208,900 MT), Indonesia (73,700 MT), Japan (59,700 MT), and Mexico (51,500 MT). 


Comments and Performance Indicators
Sales were stronger than expected. Export commitments for 2016-17 are running 25% ahead of year-ago, compared to 24% ahead the previous week. USDA projects exports in 2016-17 at 2.025 billion bu., up 4.6% from year-ago.



Soymeal



Actual Sales (in MT)
Combined: 161,600
			2016-17: 162,600
			2017-18: -1,000


Trade Expectations (MT)

100,000-400,000 



Weekly Sales Details
Net sales of 162,600 MT for 2016/2017 were up 28 percent from the previous week, but down 32 percent from the prior 4-week average. Increases were reported for Venezuela (37,500 MT), Spain (34,900 MT, including 32,200 MT switched from Italy), the Philippines (27,900 MT), Colombia (25,500 MT, including decreases of 3,400 MT), and Mexico (21,100 MT, including decreases of 100 MT). Reductions were reported for Italy (32,200 MT) and Morocco (400 MT). For 2017/2018, net sales reductions of 1,000 MT were reported for Nicaragua. 


Weekly Export Details
Exports of 430,000 MT--a marketing-year high--were up 31 percent from the previous week and 41 percent from the prior 4-week average. The destinations were primarily Colombia (132,700 MT), the Philippines (98,000 MT), Mexico (42,500 MT), Saudi Arabia (35,500 MT), and Spain (34,900 MT). 


Comments and Performance Indicators
Sales were near the lower end of expectations. Export commitments for 2016-17 are running 2% ahead of year-ago compared with 3% ahead of year-ago the week prior. USDA projects exports in 2016-17 to be down 3.0% from the previous marketing year.



Soyoil



Actual Sales (in MT)

2016-17: 12,500



Trade Expectations (MT)
0-40,000


Weekly Sales Details
Net sales of 12,500 MT for 2016/2017 were down 18 percent from the previous week, but up noticeably from the prior 4-week average. Increases were reported for Colombia (4,500 MT), Mexico (4,300 MT, including decreases of 2,700 MT), South Korea (3,000 MT), and the Dominican Republic (500 MT). Reductions were reported for Morocco (100 MT). 


Weekly Export Details
Exports of 22,300 MT were down 27 percent from the previous week and 3 percent from the prior 4-week average. The destinations were primarily Morocco (15,400 MT), Mexico (5,500 MT), Canada (400 MT), and the Netherlands (400 MT). 


Comments and Performance Indicators
Sales were within expectations. Export commitments for the 2016-17 marketing year are running 3% ahead of year-ago, compared to 4% ahead last week. USDA projects exports in 2016-17 to be up 0.4% from the previous year.



Cotton



Actual Sales (in RB)
Combined: 476,600
			2016-17: 392,300
			2017-18: 84,300


Weekly Sales Details
Net upland sales of 392,300 RB for 2016/2017 were up 20 percent from the previous week and 14 percent from the prior 4-week average. Increases were reported for Turkey (94,800 RB, including decreases of 3,300 RB), Vietnam (94,300 RB, including 8,000 RB switched from South Korea, 1,700 RB, switched from Pakistan and decreases of 2,300 RB), India (84,100 RB, including decreases of 5,600 RB), China (32,600 RB, including 400 RB switched from Indonesia), and Bangladesh (25,100 RB). Reductions were reported for El Salvador (2,100 RB) and South Korea (2,000 RB). For 2017/2018, net sales of 84,300 RB were reported primarily for Indonesia (35,600 RB), Pakistan (19,800 RB), and China (13,200 RB). Reductions were reported for South Korea (4,000 RB). 


Weekly Export Details
Exports of 394,000 RB were up 4 percent from the previous week and 1 percent from the prior 4-week average. The primary destinations were Vietnam (91,600 RB), China (81,600 RB), Turkey (51,300 RB), Pakistan (29,000 RB), and Indonesia (22,000 RB). 


Comments and Performance Indicators
Export commitments for 2016-17 are 71% ahead of year-ago, compared to 67% ahead the previous week. USDA projects exports to be up 31.0% from the previous year at 16.5 million bales.



Beef



Actual Sales (in MT)

2017: 10,800



Weekly Sales Details
Net sales of 10,800 MT reported for 2017 were down 26 percent from the previous week and 31 percent from the prior 4-week average. Increases were reported for South Korea (2,900 MT, including decreases of 200 MT), Hong Kong (1,700 MT, including decreases of 100 MT), Japan (1,700 MT), Taiwan (1,600 MT), and Mexico (1,100 MT, including decreases of 100 MT). 


Weekly Export Details
Exports of 14,500 MT were up 11 percent from the previous week and 8 percent from the prior 4-week average. The primary destinations were Japan (5,100 MT), South Korea (2,800 MT), Hong Kong (1,900 MT), Mexico (1,500 MT), and Canada (1,400 MT). 


Comments and Performance Indicators
Weekly export sales compare to 14,600 MT the week prior. USDA projects exports in 2017 to be up 6.9% from last year's total.



Pork



Actual Sales (in MT)
2017: 21,100


Weekly Sales Details
Net sales of 21,100 MT reported for 2017 were down 30 percent from the previous week, but up 8 percent from the prior 4-week average. Increases were reported for Mexico (10,400 MT), South Korea (3,000 MT), Japan (2,300 MT), Canada (1,300 MT), and Australia (900 MT).


Weekly Export Details
Exports of 21,700 MT were down 9 percent from the previous week and 7 percent from the prior 4-week average. The destinations were primarily Mexico (7,700 MT), Japan (4,100 MT), South Korea (2,800 MT), China (2,100 MT), and Canada (1,300 MT). 


Comments and Performance Indicators

Export sales compare to a total of 30,600 MT the prior week. USDA projects exports in 2017 to be 8.4% above last year's total.