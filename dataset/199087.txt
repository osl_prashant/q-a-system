I wanted to make you aware of an upcoming leadership change here at APHIS. Dr. John Clifford, who has served for the past 12 years as Chief Veterinary Officer and Deputy Administrator for Veterinary Services is transitioning from this role to become the Chief Trade Advisor for Veterinary Services National Import Export Services staff beginning March 1st.
Maintaining existing and expanding new markets for trade is essential for today's livestock producers. This change will allow Dr. Clifford to focus more on global trade issues at a time when international work has become ever more crucial to our mission.

In the wake of last year's highly pathogenic avian influenza outbreak and our eradication of the virus, Dr. Clifford spent several weeks traveling overseas and meeting with numerous agricultural officials across Asia with the goal of addressing trade restrictions on behalf of the poultry industry. Those discussions helped maintain poultry trade with several Asian countries.

In his role as Deputy Administrator, Dr. Clifford has also been actively engaged with the World Organization for Animal Health or the OIE, which is the international body responsible for improving animal health worldwide. This organization helps to ensure transparency regarding countries' disease statuses and that countries adhere to the latest science when establishing trade restrictions due to animal disease. As Chief Trade Advisor, Dr. Clifford will continue to liaise with the OIE on behalf of U.S. producers.  

Dr. Clifford has been with APHIS for more than 30 years in both field and headquarters positions. In this time, he has established relationships across all animal sectors and been tirelessly dedicated to safeguarding U.S. animal health. As Administrator, I am thankful we will continue to benefit from Dr. Clifford's expertise in an arena where we have much at stake, and I will announce Dr. Clifford's successor closer to his transition date in March.

Sincerely,

Kevin Shea,APHIS Administrator