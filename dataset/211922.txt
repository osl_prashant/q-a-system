Sonic Drive-In will provide just the boom The Blend needs, mushroom suppliers say.In June 2017, the hamburger chain became first major quick-service restaurant to announce plans to test a mushroom-ground beef blended burger.
The company will be marketing its new Sonic Slinger not as a healthier burger but simply as its juiciest yet.
The Slinger, which is 25% to 30% mushroom and weighs about 3 ounces, will be tried out in multiple markets.
“We are excited to see Sonic take the Blend to the (quick-service) sector as a next major step in consumer adoption,” Bart Minor, president and CEO of the Mushroom Council, said when Sonic unveiled its plans for The Blend.
It’s the latest step for one of the most important innovations in the mushroom business, said Daniel Rahn, project manager with the Avondale, Pa.-based American Mushroom Institute.
“Forecasts are for The Blend to continue to grow in popularity, propelling fresh mushroom sales growth.”
Sonic is a key step for growth, said Vince Versagli, sales director with Kennett Square, Pa.-based South Mill Mushroom Sales and Kaolin Mushroom Farms Inc.
“Assuming a successful result, other chains will certainly feel the need to add a blended burger to their menu,” he said. “That will be huge in terms of volume.”
Chefs, the initial adopters of The Blend, continue to see its sustainability and flavor benefits, with more menu mentions each year, Rahn said.
“In addition to gaining momentum in the foodservice segment, we are starting to see increased interest in The Blend from the retail side, especially in terms of cross-merchandising opportunities,” he said.
Consumers generally are becoming “more adventurous” in their food choices, and the versatility of mushrooms and The Blend fits well with that trend, Rahn said.
“Mushrooms and the Blend are being introduced to a new generation of mushroom customers,” he said, noting that 23% of the largest 150 school districts in the country are now ordering mushrooms.
Rahn also said “farm to school” purchases of mushrooms in 2016-17 increased 43% over the previous year.
“There are at least seven processors selling over 20 blended products to the school market, including burgers, meatballs and taco meat blends,” Rahn said.
Blendability is keeping mushrooms top of mind in foodservice, said Mike O’Brien, vice president of sales and marketing with Watsonville, Calif.-based Monterey Mushrooms Inc.
“This effort communicates to consumers about how blending fresh mushrooms with ground meat — beef, pork or poultry — can enhance flavor and substantially reduce fat, sodium and calories,” O’Brien said.
It works at retail, as well, said Jane Rhyno, director of sales and marketing with Leamington, Ontario-based Highline Mushrooms.
“Consumers are looking for ways to eat better without giving up flavor and the foods they enjoy. In fact, approximately 100 million transitional meat consumers — those looking to reduce red meat — are looking for an improved alternative to their diet.