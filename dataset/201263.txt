USDA Weekly Export Sales Report
			Week Ended July 6, 2017





Corn



Actual Sales (in MT)

Combined: 440,700
			2016-17: 161,000
			2017-18: 279,700



Trade Expectations (MT)
350,000-700,000


Weekly Sales Details
Net sales of 161,000 MT for 2016-17 were up 15% from the previous week, but down 59% from the prior four-week average.  Increases were reported for Japan (112,200 MT, including 92,200 MT switched from unknown destinations), Spain (94,500 MT, including 65,000 MT switched from unknown destinations), Mexico (87,700 MT), Saudi Arabia (49,700 MT, including 47,000 MT switched from unknown destinations), and Peru (35,700 MT, including 32,500 MT switched from unknown destinations).  Reductions were reported for unknown destinations (321,400 MT), El Salvador (12,300 MT), and the French West Indies (6,900 MT).  For 2017-18, net sales of 279,700 MT were reported primarily for Mexico (269,200 MT).


Weekly Export Details
Exports of 880,500 MT were down 21% from the previous week and 19% from the prior four-week average.  The primary destinations were Mexico (246,200 MT), Taiwan (98,700 MT), Spain (94,500 MT), Japan (92,200 MT), and Colombia (52,800 MT).


Comments and Performance Indicators
Sales were in line with expectations. Export commitments for 2016-17 are running 16% ahead of year-ago compared to 18% ahead the week prior. USDA projects exports in 2016-17 at 2.225 billion bu., up 17.0% from the previous marketing year.




Wheat



Actual Sales (in MT)

2017-18: 357,700



Trade Expectations (MT)
300,000-500,000 


Weekly Sales Details
Net sales of 357,700 metric tons were reported for delivery in marketing year 2017-18.  Increases were for Japan (111,700 MT), the Philippines (59,100 MT, including 30,000 MT switched from unknown destinations), Malaysia (56,000 MT, switched from unknown destinations), Nigeria (49,900 MT, including 36,000 MT switched from unknown destinations), Mexico (31,100 MT, including decreases of 100 MT), and Venezuela (30,000 MT).  Reductions were reported for unknown destinations (40,000 MT), El Salvador (4,000 MT), and Honduras (2,900 MT).  


Weekly Export Details
Exports of 475,300 MT were reported to Egypt (115,100 MT), Mexico (106,100 MT), Algeria (85,100 MT), Peru (41,800 MT), and Nigeria (38,800 MT).


Comments and Performance Indicators
Sales were aligned with expectations. Export commitments for 2017-18 are running steady with year-ago versus 1% behind the week prior. USDA projects exports in 2017-18 at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual Sales (in MT)

Combined: 683,000
			2016-17: 228,000
			2017-18: 455,000



Trade Expectations (MT)
250,000-600,000 


Weekly Sales Details
Net sales of 228,000 MT for 2016-17 were down 38% from the previous week and 19% from the prior four-week average.  Increases were reported for Egypt (90,400 MT, including 30,000 MT switched from unknown destinations), Indonesia (86,000 MT, including 60,000 MT switched from unknown destinations), the Netherlands (70,500 MT, including 66,000 MT switched from unknown destinations), China (67,100 MT), and South Korea (25,300 MT, including 25,000 MT switched from unknown destinations). Reductions were reported for unknown destinations (154,000 MT), Mexico (28,400 MT), and Bangladesh (5,100 MT).  For 2017-18, net sales of 455,000 MT were reported for Pakistan (195,000 MT), China (129,000 MT), and Mexico (111,000 MT). 


Weekly Export Details

Exports of 407,400 MT were up 46% from the previous week and 16% from the prior four-week average.  The destinations were primarily Indonesia (84,600 MT), the Netherlands (70,500 MT), Mexico (59,400 MT), Bangladesh (57,900 MT), and Egypt (30,400 MT). 
 



Comments and Performance Indicators
Sales were a bit stronger than expected. Export commitments for 2016-17 are running 16% ahead of year-ago, compared to 16% ahead the previous week. USDA projects exports in 2016-17 at 2.100 billion bu., up 8.1% from year-ago.




Soymeal



Actual Sales (in MT)
Combined: 140,700
			2016-17: 3,700
			2017-18: 137,000


Trade Expectations (MT)

25,000-250,000 



Weekly Sales Details
Net sales of 3,700 MT for 2016-17--a marketing-year low--were down 92% from the previous week and 96% from the prior four-week average.  Increases were reported for Colombia (16,300 MT), Mexico (9,200 MT), Panama (8,100 MT, including 5,500 MT switched from unknown destinations and 1,700 MT switched from Colombia), Honduras (6,100 MT, switched from unknown destinations), and Canada (5,400 MT).  Reductions were reported for the Dominican Republic (27,900 MT), unknown destinations (11,500 MT), Ecuador (6,000 MT), and the French West Indies (4,000 MT).  For 2017-18, net sales of 137,000 MT were reported for Mexico (92,500 MT), the Dominican Republic (58,100 MT), and Canada (1,400 MT), were partially offset by reductions for Ecuador (15,000 MT).


Weekly Export Details
Exports of 128,800 MT were down 19% from the previous week and 27% from the prior four-week average.  The destinations were primarily the Dominican Republic (32,000 MT), Mexico (21,300 MT), Honduras (20,500 MT), Colombia (17,000 MT), and Canada (9,800 MT). 


Comments and Performance Indicators
Sales were in line with expectations. Export commitments for 2016-17 are running near steady with year-ago compared with 1% ahead of year-ago the week prior. USDA projects exports in 2016-17 to be down 0.5% from the previous marketing year.




Soyoil



Actual Sales (in MT)

2016-17: 16,200



Trade Expectations (MT)
5,000-25,000


Weekly Sales Details
Net sales of 16,200 MT for 2016-17 were up noticeably from the previous week and 3% from the prior four-week average.  Increases were reported for South Korea (13,000 MT), Mexico (2,600 MT), and the Dominican Republic (400 MT).  


Weekly Export Details
 Exports of 7,400 MT were up 50% from the previous week, but down 46% and from the prior four-week average.  The destinations were primarily Colombia (4,300 MT), Mexico (2,400 MT), and Canada (600 MT).


Comments and Performance Indicators
Sales were within expectations. Export commitments for the 2016-17 marketing year are running 9% behind year-ago, compared to 6% behind year-ago last week. USDA projects exports in 2016-17 to be up 7.0% from the previous year.




Cotton



Actual Sales (in RB)
Combined: 165,600
			2016-17: 13,000
			2017-18: 152,600


Weekly Sales Details
Net upland sales of 13,000 RB for 2016-17--a marketing-year low--were down 93% from the previous week and from the prior four-week average.  Increases were reported for Mexico (5,300 RB), Turkey (3,300 RB), Vietnam (3,100 RB, including 2,400 RB switched from South Korea), India (2,800 RB), and Thailand (1,200 RB, including 400 RB switched from Japan).  Reductions were reported for South Korea (2,600 RB), China (2,000 RB), and Japan (1,300 RB).   For 2017-18, net sales of 152,600 RB were reported primarily for China (62,100 RB), Bangladesh (25,100 RB), Vietnam (21,100 RB), and Turkey (21,100 RB).


Weekly Export Details
Exports of 195,300 RB were down 35% from the previous week and 25% from the prior four-week average.  The primary destinations were Vietnam (48,100 RB), Turkey (42,500 RB), Mexico (16,400 RB), China (16,300 RB), and India (15,800 RB). 


Comments and Performance Indicators
Export commitments for 2016-17 are 63% ahead of year-ago, compared to 64% ahead the previous week. USDA projects exports to be up 58.5% from the previous year at 14.500 million bales.




Beef



Actual Sales (in MT)

2017: 12,500



Weekly Sales Details
Net sales of 12,500 MT reported for 2017 were down 27% from the previous week and 7% from the prior four-week average.  Increases were reported for Japan (5,000 MT, including decreases of 1,900 MT), South Korea (2,300 MT, including decreases of 300 MT), Hong Kong (1,800 MT, including decreases of 100 MT), Mexico (1,500 MT, including decreases of 100 MT), and Canada (1,000 MT).  Reductions were reported for Egypt (400 MT).  


Weekly Export Details
Exports of 11,700 MT were down 22% from the previous week and 20% from the prior four-week average.  The primary destinations were Japan (4,900 MT), South Korea (1,900 MT), Hong Kong (1,400 MT), Mexico (1,300 MT), and Canada (800 MT). 


Comments and Performance Indicators
Weekly export sales compare to 17,000 MT the week prior. USDA projects exports in 2017 to be up 10.2% from last year's total.




Pork



Actual Sales (in MT)
2017: 9,700


Weekly Sales Details
Net sales of 9,700 MT reported for 2017--a marketing-year low--were down 27% from the previous week and 49% from the prior four-week average.  Increases were reported for Japan (6,000 MT), Mexico (1,500 MT), Canada (1,000 MT), Colombia (300 MT), and China (200 MT). 


Weekly Export Details
Exports of 14,900 MT were down 14% from the previous week and 24% from the prior four-week average.  The destinations were primarily Mexico (5,200 MT), Japan (2,500 MT), South Korea (1,700 MT), Canada (1,400 MT), and Hong Kong (1,100 MT). 


Comments and Performance Indicators

Export sales compare to a total of 13,200 MT the prior week. USDA projects exports in 2017 to be 9.7% above last year's total.