Buoyed by big supply of red delicious and gala varieties, March 1 fresh apple holdings of 69.7 million (42-pound) cartons were up 12% higher than last year and 8% more than the five-year average, according to the U.S. Apple Association.
 
Washington state accounted for 62.7 million cartons on March 1, or about 90% of total fresh supply, according to the report.
 
Red delicious fresh inventories on March 1 totaled 24.4 million cartons, up 33% from 18.4 million cartons in 2016 but down 16% from 29 million cartons from March 2015.
 
Gala variety fresh apple inventories on March 1 were 12.9 million cartons, up 36% from 9.5 million cartons a year ago and about the same as two years ago, according to the report.
 
Other varieties, with comparison to March 1 last year:
fuji: 7.34 million cartons, up 12%;
golden delicious: 4.34 million, down 14%;
cripps pink/Pink Lady: 3.364 million cartons, up 11%;
granny smith: 6.58 million cartons, down 30%; and
Honeycrisp: 2.28 million cartons, up 2%.