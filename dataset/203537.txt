A couple of programs introduced last year that should enhance the importation process for Argentina blueberries seem to be working well, despite some anticipated hiccups.
The Automated Commercial Environment single-window program from U.S. Customs and Border Protection already has been adopted by Customs and several other government agencies involved in importing Argentina blueberries.
When fully implemented, the system will make information from nearly 50 agencies accessible through a single site.
At the same time, a pilot program that will allow importers to bring Argentina blueberries directly to ports in Miami and Georgia seems to be working well and may be expanded to include other items.
In the past, the berries were delivered to ports in the Northeast, where they were subjected to a cold treatment to protect against the Mediterranean fruit fly and then trucked to Florida or Georgia.
Under the new program, the cold treatment is applied while the berries are in transit.
Nelly Yunta, vice president at Crowley Maritime Corp. Inc. in Miami, predicted a year ago that there would be some glitches when the ACE program was implemented early this year, and she was right.
Entries were delayed and there were some slowdowns, but the program has been steadily improving, she said.
"Customs is working very diligently helping the trade go through the transition," she said in late August. "Now things are going smoothly."
Not all agencies are on the system, but the three major ones involved in the importation of Argentinean blueberries – the U.S. Department of Agriculture, the Food and Drug Administration and Customs and Border Protection – have signed on.
"They have made a huge improvement from when we started," Yunta said. "All of our entries are filed through ACE."
"ACE is in full swing," said Frank Ramos, president and CEO at Miami-based The Perishable Specialist Inc. "The transition already has taken place."
Like Yunta, he said the transition hasn't been without its flaws.
"They're working out some kinks," he said. "But we're proactive, and so is Customs, and they continue to do updates to try to get the program to what they want it to eventually be."
ACE continues to be "a work in progress," he said, but he remains optimistic.
"Once they work out all the kinks and the program is running to full capacity, it will streamline the entire process, where all the government agencies will be able to work out of the same program."
Meanwhile, Yunta said that Crowley Maritime Corp. has long been a proponent of the program that allows Argentina blueberries to come directly into Miami, and now that the pilot program is in effect, she would like to see it expanded to include other fruits and vegetables.
The company supports a proposed rule that would allow USDA to change regulations more easily, which could make it easier to ship cold-treatment products through Florida ports.
"We are commenting on the proposed rule to support the change," she said.
The company also would like to enable the cold-treatment process to be applied after product arrives at the port rather than having to be done in transit.
"We put a lot of effort into mitigating potential risks for domestic agriculture," Yunta said.
So far, the program has been working.
"We haven't had any incidents, so USDA is more confident that we can make it work without any problem," she said.
The changes would make importing products cheaper and faster and help enhance freshness, she said.