President again mentions dairy trade issue with Canada






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






Details on renegotiation of the North American Free Trade (NAFTA) agreement will come between now and the first week of May, President Donald Trump pledged today.
"We’ll be reporting back sometime over the next two weeks as to NAFTA and what we’re going to do about it," Trump said in the Oval Office after signing an executive memorandum focused on steel imports, according to a pool report.
Trump again noted his dismay over U.S. dairy trade with Canada. He said he had not planned to break from prepared remarks to touch on dairy but "the fact is, NAFTA, whether it’s Mexico or Canada, is a disaster for our country."
“We’re going to have to get to the negotiating table with Canada very, very quickly,” Trump said.
Trump did not elaborate on NAFTA proposals ahead. One possibility is that the White House could formally notify Congress of its intent to reopen NAFTA, a step that is statutorily required under Trade Promotion Authority (TPA/fast-track) legislation before talks can begin. A draft of the notification was circulated on Capitol Hill late last month (link), but the administration is waiting for Robert Lighthizer to be confirmed as US Trade Representative (USTR) before sending a final version.
Lighthizer's confirmation has been held up amid debate over whether he needs a waiver, which would require bicameral approval, because of past work representing foreign governments. Lighthizer will likely need to get the waiver besides undergoing a Senate Finance Committee vote and a full Senate vote before he can be confirmed. Some Democratic members are also pushing aid to miners as a requirement before letting Lighthizer get a nomination vote.
Congress returns from its two-week Easter recess next week – the Senate returns April 24 and the House April 25.


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.