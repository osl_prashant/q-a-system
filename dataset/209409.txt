Farmers in the northern Plains are well aware of the dry conditions, and now they have to resort to drastic measures in order to deal with eroding conditions that are destroying crops and pastures.Russell Wagner, a farmer from Kidder County in North Dakota cut 400 acres of Conservation Reserve Program (CRP) land for feed, but it’s not nearly enough forage to get his cattle through the winter.
Other livestock producers are cutting poor quality, small grain crops like oats, barley and hard red spring wheat for hay.
“It will feed a cow—it will be cut down and turned into some sort of nutrition for the animals,” said Wagner.
He feels more than 50 percent of North Dakota’s CRP land should have been opened for haying because the need for feed is so great and the quality of CRP hay is marginal.
“Unfortunately, CRP isn’t typically the highest quality because it has a lot of old, dead material in it,” said Penny Nester, the extension agent for Kidder County. “It’s not going to be a very high feed value; however, it’s great filler.”
Clay McPeak, another farmer in Kidder County, is cutting 900 acres of CRP acres. He said it’s so dry, he’s cutting and baling the same day.
“Some of it’s a bale an acre, some of it’s a little more or it’s a less,” said McPeak.
Nester says farmers may soon have more than the quantity and quality of CRP hay to worry about.
“If we don’t get rain this year, we don’t get a good snowfall this year, we could have drought next year and that just escalates the issues and problems associated with that,” she said.
Many farmers have carryover hay from last year that will help get them through the coming winter, but after those reserves are used up, the need for moisture will become even more critical for the 2018 growing season.