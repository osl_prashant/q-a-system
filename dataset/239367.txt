Alma Nursery installs optical sorters
Brandon Wade, plant manager for Alma Nursery and Berry Farms, Alma, Ga., said the firm has added optical sorting equipment from BBC Technologies to its blueberry packing operation.
The line has partially eliminated the need for human inspection of the fruit, he said.
“We are pushing to jump on board with the technology early,” he said. “We are a seasonal ag business and it is hard for us to fill that two- to three-month window with all of our labor needs.”
The technology is able to detect and sort according to color, fruit defects and fruit condition/softness. As machine harvest technology becomes more widely used in future years, Wade said it is important to be able to sort fruit, since machine harvesting is less precise than hand picking.
 
MORE: Alpine Fresh sees blueberry volume jump
 
Florida association seeks to inform
The Florida blueberry industry is experiencing growth and is working to keep the momentum going, said Brittany Lee, president of the Florida Blueberry Association and vice president of Florida Blue Farms, Orange Heights.
From just a few hundred acres in the 1980s, Lee said the industry now boasts more than 7,000 acres.
Growers, spanning from Arcadia to Gainesville and west of Tallahassee, may tally 400 or so. There may be about 35 marketers in the state, Lee said.
The association is working on getting some funding to develop an accurate crop forecasting model, she said. Development of an accurate forecasting model will help better inform retailers and consumers about the state’s crop size and when to expect it.
“Consumers and retailers prefer Florida fruit over other blueberries and if we are able to educate (them) when we are in season and how much we will have, we will experience higher market share and (more) shelf space.”
 
Florida Classic sees volume bump
Florida Classic Growers expects a boost in blueberry volume.
Al Finch, president of Florida Classic Growers, the Dundee, Fla., marketing arm of the Dundee Citrus Growers Association, said the company anticipates volume to be up about 25% this year.
“We are continuing to expand our Florida blueberry volume and we are marketing all of our Florida blueberries under our Florida Classic label,” Finch said.
“The main marketing window will be from mid-March to the end of April.”
Peak volume is expected during April, he said.
 
Georgia growers expect rebound
About half to two-thirds of Georgia blueberry crop is expected to be marketed fresh, said Brandon Wade, president of the Georgia Blueberry Growers Association and plant manager for Alma Nursery and Berry Farms, Alma, Ga.
There is not an official crop estimate for the Georgia crop but it may be in the 100-million to 105-million-pound range for the combined fresh/processed output, he said.
Last year’s crop was on track to be a record breaker — around 115 million pounds or so — when a mid-March freeze cut output to only 45 million pounds (fresh and processed).
Georgia will see substantial harvest volume by April 15, he said, with earliest volume possible by April 1. Harvest of fresh market berries will slide the end of June, with processing berries continuing into July.
About 5% of the state’s acreage — estimated near 26,000 acres — is devoted to organic berries, he said, with increased area every year.
 
J&B Blueberry offers more 1-pounders
J&B Blueberry Farms will increase availability of 1-pound clamshells, said Joe Cornelius, president of the Manor, Ga.-based firm.
“This year we are going to (pack) a few more 1-pound clamshells, and think the industry needs to go to the 1-pound clamshell,” Cornelius said, noting that the larger pack can move a bigger crop.
The company commonly packs the pint package, and also offers 18-ounce and 2-pound clamshells.
 
Naturipe Farms increases volume
Crop volume is expected to be stronger for Naturipe Farms LLC, Salinas, Calif.
Blueberry crops in Florida, Georgia and North Carolina look good, said Brian Bocock, vice president of sales.
More Naturipe proprietary varieties that improve the texture and flavor of blueberries are being planted every year, he said.
“We have a nice stable of proprietary varieties going into the ground that we are having good luck with,” he said.
 
Southern Hill Farms reports heavy crop
David Hill, owner of Southern Hill Farms, Clermont, Fla., said the firm’s volume is expected to start in a light way by the end of March, with peak volume by the first or second week of April.
The firm’s top variety is emerald, he said. The firm will offer 6-ounce, pint, 18-ounce and possibly 2-pound clamshells for the box stores.
 
Sun Belle gives volume a boost
Schiller Park, Ill.-based Sun Belle Inc. expects bigger volume of blueberries in Florida, Georgia, Mississippi and North Carolina this year, according to company founder and president Janice Honigberg.
The firm’s blueberry volume from Florida, Georgia and North Carolina could be 20% to 30%, and Mississippi may see 40% to 50% more volume after a short crop a year ago, she said.
Sun Belle markets conventional and organic fruit, she said.
The company’s biggest volume is from Florida and Georgia, she said, with volume starting out of central Florida, then moving to the Gainesville area and then into southern Georgia.
Sun Belle begins with a 6-ounce pack and then offers the pint pack and 18-ounce clamshell.
 
MORE: Wish Farms adds organic blueberry volume