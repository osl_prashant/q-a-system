Vilmorin-Mikado is the official name of the of the vegetable seed company, a year after France-based Vilmorin SA merged with Japan-based Mikado.
Rodolphe Millet, CEO of Vilmorin-Mikado, said in a news release that the name reflects both partners’ common values and strengths.
The company has offices in 12 countries and sells dozens of vegetable seeds, including broccoli, cauliflower, greens, herbs, lettuce and watermelons.
A spokeswoman said the company has clients in Coastal California, Arizona and the Imperial Valley, as well as South and Central America and Europe.