Alsum Farms packs pouches
Friesland, Wis.-based Alsum Farms & Produce Inc., Friesland, Wis., recently introduced a line of 24-ounce Gourmet Potato pouch packs as specialty potato offerings, said Christine Lindner, national sales and marketing representative.
"The pouch provides a convenient grab-and-go concept for busy shoppers, helps extend product shelf life and prevents contamination from consumer handling," she said.
The pouch offers a merchandising advantage, since it sits upright on the shelf, Lindner said.
Each pouch features a recipe on the front and the background of grower Larry Alsum on the back.
The pouch offering comes in four choices: Gourmet Blend fingerlings, a petite blend, petite gold and 3 pounds of gold B-size.
 
Black Gold Farms Redventures on
Grand Forks, N.D.-based Black Gold Farms has updated marketing materials for 2017, said Keith Groven, fresh sales manager.
"At PMA (the 2016 Produce Marketing Association's Fresh Summit), we launched our Redventure program, which is a push to explore and discover different recipes and ways of eating potatoes," Groven said.
"We'll see that program kind of be the message for all our programs this year."
Black Gold grows fresh potatoes on about 2,000 acres in Texas, Missouri, Indiana, Minnesota and North Dakota, Groven said.
 
Northern Plains boosts yellows
The East Grand Forks, Minn.-based Northern Plains Potato Growers Association has increased its yellow-flesh volume "to be able to supply our customers with yellows for a longer period of time," said Ted Kreis, marketing and communications director.
The association also continues to focus on its food safety initiative, Kreis said.
 
Pleasant Valley offers reds, yellows
Aberdeen, Idaho-based Pleasant Valley Potato Inc. had red and yellow potatoes available for "the first time in many years" for 2016-17, said Ryan Wahlen, sales manager.
Pleasant Valley also expanded its fingerling program, Wahlen said.
"The reds are gone, but we have quite a few yellows to market and they look great," he said.
"Our issue is trying to find time to pack them, honestly."
 
Potandon increases bag options
Idaho Falls, Idaho-based Potandon Produce has updated its websites, potandon.com and klondikebrands.com, said Ralph Schwartz, vice president of sales.
"They've been through a total refresh, and we added a lot more content and made them mobile-friendly," he said.
Potandon also has launched a limited line of Green Giant potatoes in "light-blocker" packaging, Schwartz said.
The company debuted the bags at the Produce Marketing Association's 2016 Fresh Summit, Schwartz said.
The company was still evaluating the new product in mid-February, he said.
"It's been positive so far," he said.
Potandon also has rolled out two stock-keeping units of a 2-pound pouch bag of mini potatoes in red and yellow varieties, Schwartz said.
"We launched with one customer on the West Coast and one on the East Coast," he said.
"We're looking to ways to increase that. The results of the early tests have been very positive and we're increasing distribution on it this year."
 
Potatoes USA hires three to staff
The past year has been one of major changes for Denver-based Potatoes USA, starting with an organizational name change, said Ross Johnson, global marketing manager.
"It had a couple of different reasons. One of the biggest is we promote potatoes and the old name was more focused on the board, rather than the product," he said.
The new name also has "more resonance" with consumers worldwide, he said.
"They know they're getting potatoes from the U.S.," Johnson said.
Johnson is one of several newcomers to arrive at Potatoes USA in the past year, having come to the organization from the consumer packaged goods industry, most recently as account manager for ConAgra in Tampa, Fla.
Jill Rittenberg now is Potatoes USA's global marketing manager for ingredient. She is a newcomer to the produce industry.
Rachael Lynch now is a nutritionist with Potatoes USA, overseeing its nutrition and school foodservice programs.
 
Robinson Fresh adds fingerlings
Robinson Fresh, the fresh produce division of Eden Prairie, Minn.-based C.H. Robinson Worldwide Inc., now offers organic fingerling potatoes, said Lara Grossman, organics customer group manager for sourcing.
Robinson Fresh offers 1.5-pound bags of specialties, such as Russian bananas, Ruby Crescent and French under its Tomorrow's Organics label and has a 5-pound bag of fingerling potatoes with a recipe on the package, Grossman said.
 
RPE launches microwave cups
Bancroft, Wis.-based Russet Potato Exchange Inc. has launched a value-added product - Tasteful Selections Take & Shake cups, said Randy Shell, vice president of business development.
"Take & Shake cups pack fresh bite-sized potatoes and the perfect amount of seasoning into a microwavable cup," he said.
RPE says the "snack-sized" products are available in three flavors:

Chipotle BBQ;
Chili Lime; and
Black Pepper, Rosemary & Thyme.

"These cups will satisfy cravings and provide a healthier option to grab-and-go eating," Shell said.
The Take & Shake line will expand this spring with the anticipated launch of a fourth flavor, he said.
Wada Farms builds digital presence
Idaho Falls, Idaho-based Wada Farms Marketing Group is looking to increase its digital and social media presence in business-to-business and business-to-consumer audiences, said Eric Beck, marketing director.
"We feel in doing so, it will help us keep a pulse on the industry," he said.
"Our increased focus in understanding the data and trends will be an important part of our future strategies."
 
WPVGA Spudmobile keeps rolling
The Antigo-based Wisconsin Potato & Vegetable Growers Association awarded a Fat Bob Harley Davidson motorcycle to Trig's in Rhinelander, Wis., for "the best and most creative" display for Wisconsin retailers, said Dana Rady, the association's director of promotion, communication and consumer education.
The contest ran in October during Wisconsin's Potatopalooza month, Rady said.
The association also continues to take its Spudmobile to various events promoting Wisconsin's potato production, Rady said.
The association has hired a part-time driver, Doug Foemmel, to help community relations coordinator Jim Zdroik, who also is full-time driver of the Spudmobile.
"The two have done a great job of showcasing our industry to the public and are working well with WPVGA's many other efforts," Rady said.