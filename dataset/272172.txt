For Trump lookalike, crops mean more than social media fame
For Trump lookalike, crops mean more than social media fame

By ARITZ PARRAAssociated Press
The Associated Press

MADRID




MADRID (AP) — A woman in Spain has found unexpected fame on social media after many found she bore a striking resemblance to U.S. President Donald Trump.
A journalist reporting on farming in northwestern Spain posted on Instagram a picture of Dolores Leis dressed in farm clothing with a hoe over her shoulder, prompting thousands of responses.
The 64-year-old has since been asked to comment on pressing U.S. policy and international issues — though she has shown more concern for a moth plague threatening her potato crops.
"I say that it must be because of the color of the hair," Leis told the La Voz de Galicia newspaper Tuesday
She is different to Trump on one issue though — she doesn't use a mobile phone and has little interest in online chatter.
Leis, who appears standing in the middle of her farming plot, her frowning face looking away from the camera and blond hair held by a diadem, has many fans now.
"Can we replace Trump with this hard working lady?" one responder on Instagram asked.
Others, who called Leis "Trump's Galician sister," made an online call to research the president's family roots in the Costa da Morte, or Death Coast, the rocky shore in northwestern Spain with a long history of shipwrecks.
A Galician native who has lived in the same town since she married her husband four decades ago, Leis works at home and at her farm, where the reporter found her last week planting potatoes.
Leis told the newspaper she has not felt overwhelmed by sudden fame because, without a smartphone, the online buzz is easy to ignore.
"I look at everything that my daughters show me, but it never stung my curiosity to have one (phone)," she said.
___
A previous version of this story has been corrected to show that the woman's age is 64, not 70.