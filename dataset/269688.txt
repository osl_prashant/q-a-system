BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Tuesday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2704  unchMay         2692    2750    2664    2728  Up    26Jul                                 2730  Up     1Jul         2693    2727    2684    2704  unchSep         2722    2751    2709    2730  Up     1Dec         2725    2749    2710    2728  Down   2Mar         2712    2735    2697    2711  Down   6May         2734    2735    2696    2708  Down  10Jul         2737    2738    2701    2709  Down  12Sep         2740    2740    2703    2714  Down  11Dec         2746    2746    2711    2720  Down   9Mar                                 2729  Down   9