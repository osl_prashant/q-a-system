Onion grower-shipper Shuman Produce Inc., Reidsville, Ga., has hired Amanda Lott as marketing coordinator.
“With Amanda joining our team, we will continue to bring fresh and innovative programs the industry has come to expect from Shuman Produce Inc.,”  president John Shuman said in a news release.
She lives in Baxley, Ga.
“When I graduated from college with an English degree, working in the sweet onion industry was the last thing I ever imagined,” Lott said in the release.