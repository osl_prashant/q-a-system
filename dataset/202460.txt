Virtually every retail produce department features the accents of red, yellow and brown potatoes.
Potato marketers say shoppers and retail merchandisers alike appreciate the color palette potatoes bring to the produce department. Marketers credit the variety of their product as just one segue to sales.
"There's no magic bullet in marketing," said Keith Groven, fresh sales manager with Grand Forks, N.D.-based Black Gold Farms.
Potatoes are ubiquitous, but they still require aggressive marketing strategies, Groven said.
"We hear potatoes called a commodity at times. It's a staple item, but it's all about having good quality at a fair value," he said.
Equally important is having "the structure and organization" that facilitates sales to retail customers, Groven said.
"Easy process, good price and being dependable.... (I)t's about doing what you say you're going to do," he said.
Potatoes are a staple, but an effective marketing approach can build sales further, said Dana Rady, director of promotion, communication and consumer education with the Antigo-based Wisconsin Potato & Vegetable Growers Association.
"For Wisconsin, WPVGA has focused on education about our industry and buying local in the Midwest," she said.
That assures consumers a "fresher product at a lower carbon footprint" while supporting their local economy, Rady said.
"Wisconsin growers have a lot to offer and are very conscientious when it comes to growing their vegetables in a way that also conserves Mother Nature's resources," Rady said. "Consumers want to know about this and the processes used to produce their food."
Through various promotional efforts, the association is accomplishing that goal of education, which leads to purchases, partnerships and an overall awareness of the importance of supporting local communities, Rady said.
It's also important to promote and market potatoes across the entire category, said Randy Shell, vice president of business development with Russet Potato Exchange Inc. in Bancroft, Wis.
"New varieties and specialty potatoes are in their early stages and need more retail and consumer focus," he said.
Success comes in educating the end consumer that potatoes are part of a balanced diet, said Eric Beck, marketing director with Idaho Falls, Idaho-based Wada Farms Marketing Group.
"Think about it: Up until recently, potatoes haven't had the best reputation being categorized as a healthy eating option," he said.
Promotional efforts of the Idaho Potato Commission, Potatoes USA and others have helped to change consumer thinking along those lines, Beck said.
"Well-educated consumers can yield more consistent consumption," he said.
It's also important to monitor consumer trends in marketing potatoes, Beck said.
"We are quickly becoming a society where small, convenient and ready-to-go are all synonymous," he said.
Grower-shippers are adjusting their product portfolios to keep pace, in some cases, moving away from the familiar 5- and 10-pound bags, Beck said.
"Consumers aren't necessarily eating less potatoes nowadays but have become more responsible when it comes to food waste," Beck said.
Finding varieties that yield good aesthetics also is critical, Beck said.
"Most consumers are eating with their eyes nowadays, and if a potato doesn't look appealing more than likely it will not be a hot selling item," he said.
Value-added products also now are part of the marketing mix, said Christine Lindner, national sales representative with Friesland, Wis.-based Alsum Farms & Produce Inc.
"The value-added category will continue to experience growth in the future," she said, noting that consumers are factoring convenience into some purchases.
Consumers can get excited about a staple item, said Ross Johnson, global marketing manager with Denver-based Potatoes USA, which, this year, is focusing on potatoes as a fuel to "power athletic performance."
"We're taking a much more proactive approach with regards to our marketing messaging to the consumer to tell them why it's good to eat potatoes, because it will power their performance, as opposed to saying they're not really that bad for you," Johnson said.