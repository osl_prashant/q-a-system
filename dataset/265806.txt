Water flows reduced at Idaho dam ahead of fish release
Water flows reduced at Idaho dam ahead of fish release

The Associated Press

LEWISTON, Idaho




LEWISTON, Idaho (AP) — The Army Corps of Engineers has reduced water flows from a northern Idaho dam to reduce dissolved gas levels that can affect juvenile fish.
The Lewiston Tribune reports the water flows exiting Dworshak Dam were reduced Monday and will remain low for a week as nearby hatcheries release salmon and steelhead trout.
Dworshak Hatchery assistant manager Adam Izbicki says high dissolved gas levels can cause some health issues for fish. The water that plunges hundreds of feet from the dam's spillway into the north fork of the Clearwater River creates high levels of dissolved gas.
Izbicki says the fish will be released into the Clearwater River instead of its north fork in an effort to help shield the fish from high gas levels.
___
Information from: Lewiston Tribune, http://www.lmtribune.com