Salinas, Calif.-based Taylor Farms awarded 12 first-time winners and 22 renewal recipients a total of $170,000 at the company’s annual Scholarship Luncheon.All of the recipients are children of full-time Taylor Farms employees. Recipients receive $5,000 with a guaranteed renewal of $5,000 for each year they remain in an undergraduate or graduate program at a university.
“An excellent education is the foundation of a free and fair society in America,” Bruce Taylor, chairman and CEO of Taylor Farms, said in a news release.
The Taylor Farms Scholarship Program, formally developed in 2012, has been awarding scholarships since 2008. The selection committee consists of 12 Taylor Farms’ employees, and has awarded $710,000 in scholarships over the past 11 years.
“Taylor Farms is committed to seeing me through to the end and, because a significant amount of financial burden has been lifted, I can focus on maximizing my education,” Adela Contreras, 2016 TF Scholarship Recipient and University of California-Davis undergraduate student, said in the release.
The 2017 first-time recipients honored at the June 16 luncheon are:
Betzibeth Valencia, Arizona State University;
Brandy Martinez, University of Arizona;
Crystal Hernandez Marquez, California State University-Monterey Bay;
Crystal Osuna Cecilio, California State University-Monterey Bay;
Daisy Solorzano, University of California-Davis;
Diana Cervantes-Morales, California State University-Monterey Bay;
Ismael Lopez, San Jose State University;
Leticia Isabel Cardenas, San Diego State University;
Maria Fernanda Garcia, University of Arizona;
Paulina Suguey Hernandez, University of California-Davis;
Viridiana Macias Contreras, Humboldt State University; and
Zeltzin Donaji Gonzalez Lopez, San Francisco State University.