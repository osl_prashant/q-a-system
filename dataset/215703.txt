In the restaurant business — the upscale sector, not the establishments that promote 24-hour drive-thru service — nothing is more important than being “hot.”
Hot, as in new, trendy, cutting-edge. If you’re not developing new flavors, some novel cooking method or a fusion of previously unrelated cuisines, critics simply aren’t interested.
That’s why the National Restaurant Association’s annual survey focuses on novelty, as in “What's Hot in 2018.”
To quote from Hudson Riehle, NRA’s senior vice president of research, “Local, vegetable-forward, and ethnic-inspired menu items will reign supreme on menus in the upcoming year. Guests are implementing these trends in their own lifestyles and want to see them reflected in the food they eat at restaurants.”
I don’t know about you, but vegetable-forward menus certainly reign supreme in my household.
Not!
Among the other (alleged) “hot” new trends are a few that seem a little outside the mainstream, such as donuts with liqueur fillings, artisan pickles and “healthful” kids’ meals.
Good luck with that last one.
However, I found another trend NRA identified to be far more plausible, and far more favorable to the industry. In fact, according to the association’s press release, the No. 1 trend next year will be “new cuts of meat, eg, shoulder tender, oyster steak, Vegas Strip Steak and the Merlot cut.
A Niche Market — For Now
The survey was conducted among more than 700 chefs who belong to the American Culinary Federation, so I consider the new-cuts-of-meat-being-a-big-deal to be the real deal.
Of course, some of these trendy cuts aren’t really all that new. The Vegas Strip Steak, for example, was developed three years ago at Oklahoma State University’s Robert M. Kerr Food & Agricultural Products Center by Jacob Nelson, a value-added meat processing specialist. The steak is cut from the chuck according to Nelson is not as highly marbled as, say, a ribeye steak, but it rivals many of the traditional steak cuts in tenderness.
And there’s no shortage of flavor with beef from the chuck muscles.
As for the others on the No. 1 new trend list, collectively, they represent a very positive trend that may well impact how beef is butchered — maybe even help generate a resurgence in the very art of butchering itself.
That’s because cuts like the Flatiron, the Vegas Strip and the oyster steak require time, effort and skill to produce, and although they offer restaurateurs good value per pound, those cuts are rarely available in the supermarket meat case because the extra labor required to cut, trim and package them doesn’t fit with the model of a high-volume boxed beef production line.
But like the growth of high-end boutique beers that don’t necessarily appeal to the mass market, these “new” cuts of beef are sold as niche products, but ones that may well offer opportunities for small-scale meat shops to emulate the craft breweries that have remade the beer industry.
I love the idea that new center-of-the-plate choices top the chef’s 2018 hit list.
I’m even more excited that along with novel menu items, the chefs in NRA’s survey indicated that they intend to explore further the concept of what’s been called “hyper-local dining and imbibing.” That would include not only such features as on-site beer brewing — nothing new there — but more “house-made” items unique to the region, to the local area and even to the restaurant itself.
What better way to jump on that bandwagon than to source locally grown beef or pork butchered into unique cuts that diners can’t find in a grocery store?
Now that’s something I can definitely consider implementing into my lifestyle.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.