When raising livestock, our responsibility to care for an animal does not end when its life ends.
Properly and disposing of dead animals requires both legal and ethical considerations, according to J. Craig Williams, Dairy Team Educator with Penn State University Extension; and Elizabeth Santini, veterinarian with the Pennsylvania Department of Agriculture.
“Improper animal disposal can result in environmental issues and pose a risk to animal and public health,” said Santini. Williams added, “Neighbor issues and a poor public perception of animal agriculture also are at risk.”
Animal carcasses need to be disposed of quickly. In Pennsylvania, the two experts note the timeframe is 48 hours. They outline the four legal ways to manage on-farm mortalities, and the pros and cons of each:

Rendering – Convenient with minimal labor required, but may be expensive and pose biosecurity risks. Rendering services need to be licensed, usually by the state department of agriculture. Be sure to place waiting carcasses away from the main animal housing area, and out of public view.
Burial – Poses the greatest number of environmental, public health and safety concerns. Burial sites should be carefully selected:


A good distance (100-300 feet, depending on state and local regulations) from water sources such as streams, ponds and wetlands).
Covered within a minimum of two feet of soil within 48 hours.
At least 200 feet from wells or sinkholes.
At least 200 feet from property lines.
Away from public view.


Burning – Convenient, but may be expensive and cause odors. Open-air burning is not a legal method of dead-livestock disposal. Legal burning requires a commercial incineration unit, the best of which have a burner above the animal chamber, or are fitted with a flue after-burner to help eliminate smoke and odor. Farms usually are exempt from air-quality regulations, but check with your local municipality to be sure.
Composting – The most convenient, affordable, and environmentally friendly means of animal disposal. Plus it produces a useful nutrient source that can be utilized on the farm. Composting requires the combination of a high-carbon source and a nitrogen source, which is the animal. Commonly used carbon sources include sawdust, wood chips, dry bedded pack, leaves, municipal yard waste, and corn silage.

The carbon source should be 50-60% moisture. Squeezing a handful should produce a loosely clumped ball and a damp hand. Composting sites should be located 200-300 feet from a water source, out of public sites, and protected by visual screens such as a tree line.
A composted animal should be surrounded by 18-24 inches of carbon source, which should prevent any issues with rodents, vultures, scavenger animals, flies or odors. Periodic turning of the pile every 6-10 weeks will add oxygen and accelerate animal decomposition. Compost may be ready to use as fertilizer as early as three months later for small animals like calves, but may take up to 10 months for adult cows.
Follow this link to view Williams and Santini’s comments on video. Additional information on livestock composting from Penn State Extension can be found here.