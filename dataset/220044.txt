Anything is possible in Future World, and that’s the beauty of it for agtech startups and venture capital investors.
As I’ve talked to strawberry growers for this week’s Southern California strawberry report, many have played up the prospect of automated harvest.
It isn’t a reality yet, but the expectation is that it will be within a few years. 
“When we look at the task of harvesting strawberries, the technology is possible — probably not ready for the fields this year or next year — but the technology is developing rapidly,” Carolyn O’Donnell, communications director for the California Strawberry Commission, Watsonville, told me. 
That alluring hope doesn’t lessen the need to find labor here and now, and many growers have been leaning more heavily on the H-2A guest worker program in recent years to secure labor.
Long term, some wonder if growing strawberries indoors with artificial heat and light, closer to metropolitan areas, will emerge as a significant trend.
 
Vertical farms, conveyor belt farming, strawberries springing from Detroit warehouses — how quickly will this vision bear fruit, so to speak?
Current technology favors the outdoor farms in California, said Dan Crowley, vice president of sales for Watonsville-based Well-Pict Berries.
“(Indoor farming) looks wonderful as an idea, but right now it doesn’t make economic sense,” he said. 
Perhaps a more natural next step would be the increased use of hoops to extend the growing season for berries.
I also asked members of the LinkedIn Fresh Produce Industry Discussion Group about some of the future trends for strawberries. We had 17 people comment on the topic, with (mostly) optimistic visions of the future. Here are just a few of the thoughts shared:
JP: The biggest change in the upcoming year is going to be robotics and mechanical harvesting. Second, expanding use of tunnels and greenhouses will improve quality and supply. Third, I expect that we will continue to see some producing areas growing (and some) areas shrinking.
GS: I see line packing to accommodate the trend of fresh pick up and delivery. That could open the door for vertical/greenhouse growers locating close to the DC’s of the retailers.
JP: Berries are the darling category yet Dole shut down to make its financials better, and another recent $50 million bankruptcy. Have berries become the retailers’ dream and growers’ nightmare?
RV: Automatic retractable roof greenhouses and cooling houses help growers extend the season, grow larger fruit with a higher brix ... and make it easier to produce organically.
KR: I can see pulp punnets with resealable films coming to play a part sometime, taste profiles and better variety developments with yields that are commercially viable still important. More early season glass house availability.
As for me, I think the future always takes a bit longer to arrive than seems reasonable. The same holds true for produce trends such as harvest automation and vertical greenhouses next to retail distribution centers. The future will continue to look a lot like today, until it doesn’t.
Tom Karst is The Packer's national editor. E-mail him at tkarst@farmjournal.com.