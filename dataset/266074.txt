Moving into springtime, one might think beef and dairy cows would be on the same page when it comes to nutritional needs, but our nutrition experts say that’s not the case. 
Beef Cows Prepare for Rebreeding

Colostrum is produced in the last five weeks of gestation. In the Midwest, some farms will be calving into April, so colostrum is still being produced. Therefore, one focus is to check your cow’s body condition scores and determine if they need maintained or improved.  
“For those producers whose cows calved in February or March, their cows are in the first stage of production,” says Francis Fluharty, research professor at The Ohio State University. “Those cows are in peak lactation and therefore have peak nutritional requirements. They are still on a harvested feed, usually hay. They are undergoing uterine involution and starting to cycle again to prepare for the next breeding cycle and hopefully conceive 80 to 85 days after calving. The cow’s nutritional requirements are really high; she needs both energy and digestible protein to rebuild.”
Maximize Forage Digestibility
A lot of beef cows lose weight during the winter due to being fed harvested forage, so it’s important to maximize forage digestibility. Particle size can have significant impact on digestibility. 
“Most particles leaving the rumen that are being broken down and moving on down the digestive tract are around 1/20th [of an] inch,” he says. “One of the biggest advantages in the last few years is chop-cut bales. If those beef cows start chewing a piece of hay that is 5" in length, they have much less work to get the digestible nutrients out of the forage.”
Monitor Grass Magnesium 
Also, maintaining beef quality is really important. Cows are back on grass, but in the East, some areas are deficient in magnesium. High-potassium fertilization can tie up magnesium, and it’s more of an issue in first grass growth, where you’re going to get warm temperatures, rainfall and rapid forage growth. 
Dairy Producers Focus on Economics

Dr. Gonzalo Ferreira, dairy management extension specialist at Virginia Tech, says the big concern for dairy producers has less to do with the change of seasons and more to do with economics. 
“Milk prices are really low and have been since 2015. While we can’t change milk prices, I have been looking at the impact of the diet on a producer’s feeding cost,” says Ferreira. “Forage, specifically silage, is cheaper than concentrate, and it’s very likely going to be cheaper than hay. Increasing the inclusion of forages can make the diet cheaper, helping to obtain a better margin and creating a greater income over feed cost.”
A key to this strategy is to ensure an adequate forage supply is available or to plan for an adequate supply in the next growing season. To do this, farmers should perform a forage budget and inventory to determine how much silage they have and outline how much silage they will need to reach the next harvesting season. 
“The last thing you want to happen is to increase the forage in the diets, and then suddenly run out of silage inventory and need to purchase silage, which would be counterproductive,” Ferreira says. “The other important strategy is to reduce silage shrinkage so you capture the most out of your available crop.”
In a recently published study in the Journal of Dairy Science, high-forage diets had 65% forage and yielded 3.9% milk fat, while low-forage diets contained 45% forage and yielded 3.5% milk fat. 
“Given that, typically, milk components are a determinant of the milk price, a higher milk fat concentration should result in a higher milk price,” Ferreira concludes. “Plus, a high-forage diet costs less, so overall profitability for the dairy should improve.”
 
Sponsored by Lallemand Animal Nutrition