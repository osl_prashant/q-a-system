We all love to complain about high prices.
Whether it’s the price-per-gallon at the pump while filling up the family vehicle, or staring in disbelief at the register total at the supermarket checkout counter, the price of commodities is always way too expensive.
Then there’s Switzerland.
That Alpine bastion of neutrality nestled amidst its much larger neighbors of Italy, Austria, Germany, and France boasts some of the most picturesque scenery on Earth.
And some of the highest meat prices in the world.
A typical roast of beef in a Swiss supermarket goes for the equivalent of about $50 a kilogram. That’s more than $22 bucks a pound, which equates to what you’d pay at a fancy restaurant for a steak expertly prepared and served on fine china.
According to the 2017 Meat Price Index published by Caterwings, a UK-based online marketplace of catering and foodservice operators, Switzerland’s average meat price point is about 150% higher than the world average.
Or is it?
That’s because the Meat Price Index data accounts not just for price, but calculates affordability, as well.
Although the price of meat varies significantly from country to country, there is an even greater disparity in its affordability. The index creates its ranking by cross-referencing each nation’s minimum wage and then calculating the number of hours a person has to work to buy each type of meat, thus providing comparative affordability country-by-country.
Take Switzerland and its $22-bucks-a-pound beef. On the basis of cost, the Swiss pay the world’s highest meat prices, while Ukraine has the lowest prices, according to a report by Bloomberg.
That’s deceptive, however, because price alone doesn’t track affordability.
Despite the low prices of meat in countries such as India, for example, someone earning minimum wage there must work nearly a whole week to buy a piece of meat, while a person in Norway would need to work less than one hour at that country’s minimum wage to afford the same amount of product.
In Indonesia, beef costs just $9 a pound. But Indonesians have to work for 23.6 hours to purchase a kilogram of beef, versus less than 3 hours of work on average in Switzerland for the same amount.
So, how about fish, which nutritionists are always carping about eating more frequently? You don’t want to live in Egypt and switch to the seafood diet. In that country, a pound of white fish requires more than 20 hours of work, compared to only one hour of work in Sweden, despite the price per pound being much higher there.
(Although to be fair, one would expect that fish of any sort would tend to be more readily available, and thus cheaper, in Scandinavia than the Middle East).
And what about poultry? According to Caterwings’ Meat Price Index, Indonesians have to put in 7.3 hours of work to purchase a kilo of chicken. Even in the U.S. states with the lowest minimum wage, that would equate to forking over $24 a pound for a package of boneless breasts.
Smugglers Central
Yet despite the leveling factor of affordability, price disparity remains a thorny problem internationally, especially in Western Europe. Switzerland’s citizens on average consume a respectable 112 lbs. of meat per person per year, and that appetite helps sustain a thriving underground trade in black market meat smuggled across the border.
In January, according to Bloomberg, Swiss customs officials captured a 41-year-old man who had illegally brought in a total of 27 tons of lamb, 18 tons of beef, 12 tons of chicken and 11 tons of pork into the country over the last 15 years.
The bust isn’t an isolated occurrence.
“Discoveries are quite frequent; they’ve increased over the years,” Alain Husson, head of the criminal section at the Geneva regional customs office, told Bloomberg. “The strength of the [Swiss] franc is a factor, as is the fact that meat is a lot cheaper in France.”
Switzerland is not part of the EU, and its currency, which is preferred by investors “sheltering” their money in Swiss banks, has risen more than 50% against the euro in the last decade.
Smugglers thus make money by purchasing meat and other goods that are less expensive in neighboring EU countries, then selling them for higher prices on the black market in Switzerland.
For all the relativity of a index that calculates true affordability, in the end, the price you pay is really what matters most.
Which is why it’s always too high.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.