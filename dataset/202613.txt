For growers of California spring vegetables, the question isn't whether there will be supply gaps and possible quality issues this season. It's how bad will these challenges be.
Record rainfall throughout the state has, at the very least, put a dent in the serious drought that plagued California for the past several years, but it also wreaked havoc with planting and harvesting schedules and could have a varying impact on quality.
"I don't think there's any way of getting around the potential gaps coming up," said Russ Widerburg, sales manager for Boskovich Farms Inc., Oxnard, Calif.
Everything grown in the coastal regions will be affected, he said, including lettuce, broccoli, celery, bunched items, mixed lettuces and cauliflower.
"We're anticipating a little bit of a rollercoaster ride in the spring," he said. "We haven't been able to get in and prep ground and get our regular plantings in."
Adding to rain delays in California could be a gap caused by warm weather in Arizona, where crops may finish early.
"(Arizona) didn't get much of a cold streak," Widerburg said. "They are probably a couple of weeks ahead on their normal growing cycle."
The gaps likely will result in stronger markets for suppliers and higher prices at retail.
Already, broccoli bunched 14s that had an f.o.b. price of mostly $5-6.50 in late February 2016 were selling for mostly $15.45-16.50 late this February, according to the U.S. Department of Agriculture.
And 24-count cartons of romaine that sold for mostly $7-8.55 a year ago were mostly $15.50-16.95 Feb. 28.
"I've never seen it this bad in Salinas as far as getting stuff into the ground," said Mark Adamek, general manager for romaine and mixed leaf production for Tanimura & Antle Inc.
Once something is planted, he said, "it gets buried and rained on so hard that we're not sure it will come out of the ground."
The company was trying to plant romaine hearts and some of its artisan items in late February.
"With heavy rainfall, it's hard to establish a stand," he said.
Salinas-based The Nunes Co. Inc. was transitioning broccoli from Yuma, Ariz., to Salinas a little earlier than usual this season because of the rain, said Doug Clausen, vice president of sales.
"Nothing's been normal this winter season," he said.
The company will wind down its Yuma operation "during the next couple of weeks," he said Feb. 27.
"Supplies are going to remain very, very light over the course of the next month," he said.
Leafy vegetables will start transitioning in late March from Yuma to Huron and Salinas, he said.
Five Crowns Marketing, Brawley, Calif., will have spring asparagus out of Bakersfield, Calif., said partner Bill Colace.
"We have started up, and we're real pleased with the quality," he said.
"As long as we get some good weather, we feel like we'll have good quality asparagus for both April and May."
The company also will start shipping sweet corn out of Brawley on schedule April 1 and from Coachella a couple of weeks later.
But because of rain-related planting delays, there likely will be a gap in sweet corn during the first two weeks of June, he said.
In late February, Salinas-based D'Arrigo Bros. Co. of California was winding down its desert production of Andy Boy Broccoli Rabe, romaine hearts and fennel and transitioning back to Salinas.
"Within the next few weeks, we'll have everything up here," said Claudia Pizarro-Villalobos, marketing and culinary manager.
"It's definitely an earlier transition this year," she said.
"We're trying to do our best to ensure the quality and consistency of supply."
It's the goal of Salinas-based Coastline Family Farms "to be as consistent as possible" this spring, said salesman Mark McBride.
Farmers must take a number of steps in a timely fashion to ensure a quality crop, he said.
"If the weather prevents that, it becomes anybody's guess as to what the outcome of the crop is going to be â€“ if it's going to suffer or not."
Coastline offers a full line of vegetables that includes iceberg lettuce, cauliflower, celery, leafy vegetables and romaine hearts.
"Everybody's commodities will be affected going forward," McBride said, in terms of the harvest, timing, size and condition.
He was unsure whether supplies will be sufficient to handle demand.
"It's going to be very interesting and very challenging," he said.