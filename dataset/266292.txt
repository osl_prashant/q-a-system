AP-Deep South News Digest
AP-Deep South News Digest

The Associated Press



Good evening! Here's a look at how AP's news coverage is shaping up today in the Deep South. Questions about today's coverage plans are welcome and should be directed to:
The Atlanta AP Bureau at 404-522-8971 or apatlanta@ap.org
The Montgomery AP Bureau at 334-262-5947 or apalabama@ap.org
The New Orleans AP Bureau at 504-523-3931 or nrle@ap.org
The Jackson AP Bureau at 601-948-5897 or jkme@ap.org
Deep South Editor Jim Van Anglen can be reached at 1-800-821-3737 or jvananglen@ap.org. Administrative Correspondent Rebecca Santana can be reached at 504-523-3931 or rsantana@ap.org. A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
GEORGIA (All times Eastern):
TOP STORIES:
ZELL MILLER-REMEMBERED
ATLANTA — At a funeral that began with the voice of Ray Charles singing "Georgia On My Mind" and ended with taps echoing through the ornate Georgia Capitol rotunda, former Gov. Zell Miller was celebrated Wednesday as a beloved leader who helped bring the state into a more modern era. Gov. Nathan Deal became emotional as he thanked Miller's family at the former U.S. senator and two-term Georgia governor's state funeral. By Jeff Martin. SENT: 540 words.
AP Photos GAJB104-0328180942, GAJB101-0328181127, GAJB105-0328181132, GAATJ310-0228181028, GAATJ311-0228181029, GAATJ313-0228181019, GAATJ307-0228181012, GAATJ305-0228181004, GAATJ303-0228181005.
BALTIMORE-CYBERATTACK
BALTIMORE — Baltimore officials have confirmed a ransomware attack temporarily hobbled the city's 911 dispatch system over the weekend. Earlier this week, the mayor's office didn't comment on the specific nature of the hack. But on late Wednesday afternoon, Baltimore's chief information officer announced that "ransomware perpetrators" were behind the cyberattack.  SENT: 138 words.
DNR-WETLANDS DECISION
MADISON, Wis. — Two influential Republican senators said Wednesday that a proposal the Assembly passed quickly that would allow a Georgia mining company to destroy wetlands containing rare hardwoods in western Wisconsin is dead. Republican Sen. Alberta Darling, co-chairwoman of the powerful budget-writing committee, and Sen. Rob Cowles, chairman of the Senate's Natural Resources Committee, both told The Associated Press the votes aren't there to pass the bill exempting Meteor Timber from environmental regulations for its $70 million frac sand processing plan. By Scott Bauer.  SENT: 704 words.
EQIFAX-CEO
ATLANTA — Equifax tapped longtime financial industry executive Mark Begor as its new permanent CEO, the company said Wednesday, as Equifax continues to try to recover from fallout surrounding the company's massive data breach. The 59-year-old Begor will take over from Paulino do Rego Barros Jr., who became interim CEO in September when Richard Smith stepped down from the post. Smith's departure followed those of two other high-ranking executives who left in the wake of the hack, which exploited a software flaw that Equifax didn't fix to expose Social Security numbers, birthdates and other personal data that provide the keys to identify theft. By Ken Sweet. SENT: 522 words, photo.
TRAVEL-MLK50
MEMPHIS, Tenn. — Fifty years ago, the Rev. Martin Luther King Jr. was killed while standing on the balcony of the Lorraine Motel in Memphis, Tennessee. The civil rights leader's shocking murder on April 4, 1968, marked one of the most significant moments in U.S. history. The city's role in the civil rights movement and King's death has long made it a destination for anyone interested in King's legacy. Museums, churches and even iconic Beale Street tell the story of King's final days here. By Adrian Sainz. SENT: 925 words, photos.
WOMEN IN POLITICS
ATLANTA — Women running for governor in Wisconsin and Maryland breastfeed their infants in their campaign videos. Another in Georgia tells voters that "the folks who have held the office of governor don't look like me." The record number of women expected to run for office this year are already breaking barriers, upending traditional campaigning by emphasizing their gender as they introduce themselves to an electorate they hope is eager for change. By Christina A. Cassidy. SENT: 1004 words, photos.
IN BRIEF:
— DEADLY HOME INVASION — A Georgia elementary school teacher has been charged with murder in the 2016 gang-related slaying of two children in 2016.
— POLICE OFFICER-RAPE — A Georgia police officer accused of raping a woman has been fired.
— GEORGIA SCHOOL THREATS — A 17-year-old in Georgia has been accused of threatening his private Catholic school.
— SON KILLED — Police say a man shot and killed his estranged son in an Atlanta suburb.
SPORTS:
BKN--HAWKS-TIMBERWOLVES
MINNEAPOLIS — After a brutal loss to lowly Memphis, the Minnesota Timberwolves try to regroup and hang on to their spot in the playoffs when they host Atlanta. The Hawks wrap up a six-game road trip, with nine losses in their last 10 games. By Brian Hall. UPCOMING: 700 words.
BKH--MCDONALD'S GAME-COLLEGE CORRUPTION
ATLANTA — Some of the nation's top recruits gathered in Atlanta to play in the McDonald's All-American game say they've kept a close eye on the FBI investigation that's rocked college basketball this season. Three Duke signees and one Kansas signee have used a network of family, coaches and friends to avoid unwanted contact from anyone who could possibly put their careers in jeopardy.  By George Henry. SENT: 600 words, photos
BBN--PHILLIES-BRAVES OPENER
ATLANTA — The Philadelphia Phillies are eager to get started on a new, winning era. For the Atlanta Braves, opening day still feels like it's a few weeks away. By Paul Newberry. SENT: 600 words, photos
ALABAMA (All Times Central)
TOP STORIES:
XGR-GAY LEGISLATOR RETIRING
MONTGOMERY, Ala. — Alabama's first openly gay legislator received a standing ovation as she bid farewell to the House of Representatives ahead of the last day of the legislative session Wednesday. Democratic Rep. Patricia Todd of Birmingham will not seek re-election after serving 12 years. Todd said on the House floor Tuesday evening that her life had changed forever when she joined the Alabama legislature as its first openly gay member in 2006. By Mallory Moench. SENT: 664 words.
AP Photos ALHUT201-0629130902.
XGR-SESSION END
MONTGOMERY, Ala. — Alabama lawmakers are heading into what could be the last day of the 2018 legislative session. Senate President Pro Tem Del Marsh said lawmakers will end the session Wednesday "if we get the work done." Senators are expected to debate the state's education budget, tax bills that fund Medicaid and a proposal to exempt economic developers from the state law that governs lobbyists. SENT: 130 words. Will be updated.
TRAVEL-MLK50
MEMPHIS, Tenn. — Fifty years ago, the Rev. Martin Luther King Jr. was killed while standing on the balcony of the Lorraine Motel in Memphis, Tennessee. The civil rights leader's shocking murder on April 4, 1968, marked one of the most significant moments in U.S. history. The city's role in the civil rights movement and King's death has long made it a destination for anyone interested in King's legacy. Museums, churches and even iconic Beale Street tell the story of King's final days here. By Adrian Sainz. SENT: 925 words, photos.
SHARPTON'S BROTHER-MURDER
DOTHAN, Ala. — The Rev. Al Sharpton's half brother faces a capital murder charge in Alabama, and he's challenging that charge in court. Dothan Police Chief Steve Parrish said the Rev. Kenneth Glasgow drove 26-year-old Jamie Townes around Sunday to look for his stolen car before Townes allegedly shot 23-year-old Breunia Jennings, believing she was the thief. In Alabama, a person aiding or abetting a criminal act is equally liable. SENT: 309 words.
XGR-RACIAL PROFILING
MONTGOMERY, Ala. — Alabama lawmakers could end the legislative session with debate to collect data about race and traffic stops, but it is uncertain if the measure has the votes to pass. The House of Representatives could vote Wednesday on the bill that has become a contentious issue in the session's closing days. SENT: 135 words. Will be updated.
IN BRIEF:
— MISSING MAN — Two friends of a man who was missing since February have been charged with murder after his body was found buried in the yard of an Alabama home.
— XGR-ETHICS BILL — A divided Alabama Senate delayed a vote on a proposal to exempt economic developers from the rules governing lobbyists.
— PHARMACY OWNER CHARGED — The owner of a compounding pharmacy who pleaded guilty to plotting to defraud a federal health insurance program out of more than $10 million has been sentenced to five years in prison.
— DRUNK TEACHER-ALABAMA — Authorities say an Alabama high school teacher was jailed for being drunk in class.
IN SPORTS:
FBC--ALABAMA-DAMIEN'S DECISION
TUSCALOOSA, Ala. — Damien Harris bucked the trend of Alabama's recent starting tailbacks, who follow up big seasons with an early departure for the NFL. Instead, Harris opted to stick around for his senior season and has a chance to become the first Crimson Tide rusher with three 1,000-yard seasons. By John Zenor. SENT: 648 words.
BKC--FINAL FOUR-TOP PLAYER VS TOP PICK
RALEIGH, N.C. — Duke's Marvin Bagley III, Arizona's Deandre Ayton and Oklahoma's Trae Young are certain high NBA draft picks, one of them could easily be the top overall selection. Still, that doesn't make any of them the best player in college basketball. By Aaron Beard. SENT: 981 words.
AP Photos NY151-0309180005, NY154-0303182241, NY155-0311180440, NY152-0315181922.
BKC--AUBURN-GREENE-PEARL
AUBURN, Ala. — Auburn basketball coach Bruce Pearl has received a vote of confidence from the Tigers' new athletic director. Allen Greene told WJOX-FM in Birmingham on Wednesday that it's "absolutely" his intention to stand by Pearl barring further developments from the FBI or NCAA. SENT: 166 words.
AP Photos CAGB120-0318181729.
LOUISIANA (All Times Central)
TOP STORIES:
XGR-LOUISIANA BUDGET
BATON ROUGE, La. — Gov. John Bel Edwards wants to spend two-thirds of a $123 million Louisiana surplus on road and bridge work, college building repairs and local construction projects, a lengthy list of small-dollar items that would spread the money across the state. The Democratic governor released his proposal for divvying up the cash Wednesday. Lawmakers will determine how to use the dollars left over from the last budget year in their current legislative session. By Melinda Deslatte. SENT: 387 words.
AP Photos RPMD201-0328181538, RPMD203-0328181521, RPMD202-0328181538, RPMD204-0328181536.
XGR-NURSING HOME CAMERAS
BATON ROUGE, La. — When she visited her mother in a Louisiana nursing home, Lucie Titus found her with a black eye and severe back pain that the Alzheimer's patient couldn't explain. Titus questioned the Slidell nursing home's staff and she said they couldn't explain what caused the injuries. Shortly thereafter, a compression fracture was discovered. Titus asked to install a video camera system in her mother's room and was rebuffed by home administrators. By Melinda Deslatte. SENT: 529 words.
RENOVATION PROJECT
SHREVEPORT, La. — Renovations spurred by the need to be compliant with the Americans with Disabilities Act will close a Louisiana venue for a year. The RiverView Theater and Hall in Shreveport will be closed from June 2018 to June 2019, as it gets a much-needed update and face-lift, The Times of Shreveport reported Tuesday. The renovations were spurred by a federal mandate that states the RiverView Theater and Hall must be compliant with ADA, which prohibits discrimination against individuals with disabilities in all areas of public life. SENT: 332 words.
ROSEAU CANE SCALE QUARANTINE
BATON ROUGE, La. — Louisiana has ordered an emergency quarantine to try to slow the spread of a tiny invasive insect that destroys roseau cane, a plant that holds together delicate wetland soil. Roseau cane scale has damaged more than 100,000 acres (40,400 hectares) of wetlands. It threatens the health and welfare of Louisiana's wetlands, and possibly sorghum, sugarcane and rice industries, according to the emergency declaration. SENT: 244 words.
XGR-HUNTING AND FISHING LICENSES
BATON ROUGE, La. — An overhaul of Louisiana's recreational hunting and fishing licensing system that would pull more money out of some people's pockets edged forward Wednesday in the Legislature. With a 12-3 vote, the House natural resources committee sent the 40-page bill to the House floor for debate. By Melinda Deslatte. SENT: 465 words.
HEALTH CARE FRAUD
NEW ORLEANS — A 63-year-old New Orleans woman has been sentenced to two years and eight months in prison for her part in a $3.2 million Medicare fraud and kickback scheme. Federal prosecutors say U.S. District Judge Kurt Engelhardt also ordered Sandra Parkman to pay $277,000 in restitution. SENT: 141 words. Will be Updated.
IN BRIEF:
— XGR-HAZING PENALITIES — People who file civil lawsuits in Louisiana when someone dies from hazing could be in line for higher penalty payments, under a bill that won Senate passage.
— XGR-LAWMAKER REMOVED ON STRETCHER — A Louisiana lawmaker who was rushed from a committee room last week on a stretcher is back at the state Capitol.
— SULPHUR MAYOR — A man who served in the Louisiana House of Representatives since 2007 beat a two-term incumbent in a mayoral election.
— WOMAN ELECTED MAYOR — A Louisiana city has elected a woman to be its mayor for the first time.
— DISASTER FRAUD PLEA — A 30-year-old Louisiana woman has pleaded guilty to one count of disaster relief fraud in federal court in New Orleans.
— CONFEDERATE MONUMENTS-LOUISIANA — A federal judge has ruled that four members of a local governing board in Louisiana don't have to submit to questions from lawyers fighting the removal of a Confederate monument from courthouse grounds.
— CHILD RAPE — A Louisiana man has pleaded guilty to forcibly raping a 12-year-old girl in 2013.
MISSISSIPPI (All Times Central)
TOP STORIES:
XGR-MISSISSIPPI LEGISLATURE
JACKSON, Miss. — Mississippi lawmakers concluded their 2018 regular session Wednesday after killing one last proposal on transportation funding, highlighting their inability to agree on that issue. The House and the Senate adjourned the three-month session, the third of the four-year term. The 2019 session will take place in the run-up to statewide elections in which all 122 representatives and all 52 senators will be on the ballot, as well as the governor, lieutenant governor and other statewide officials. By Jeff Amy. SENT: 490 words.
SENATE-MISSISSIPPI
JACKSON, Miss. — A U.S. Senate candidate in Mississippi said Wednesday that he's filing an Internal Revenue Service complaint against a group that complained to the Federal Election Commission about him. Republican Chris McDaniel said he believes the Campaign Legal Center is violating its IRS status as a nonprofit group by engaging in political activity. By Emily Wagster Pettus. SENT: 496 words.
AP Photos MSRS105-0315181051, MSRS116-0321181214, GAJJ201-0110181937, JX301-0814171313.
TRAVEL-MLK50
MEMPHIS, Tenn. — Fifty years ago, the Rev. Martin Luther King Jr. was killed while standing on the balcony of the Lorraine Motel in Memphis, Tennessee. The civil rights leader's shocking murder on April 4, 1968, marked one of the most significant moments in U.S. history. The city's role in the civil rights movement and King's death has long made it a destination for anyone interested in King's legacy. Museums, churches and even iconic Beale Street tell the story of King's final days here. By Adrian Sainz. SENT: 925 words, photos.
IN BRIEF:
— MISSING WALLABY-MISSISSIPPI — A southern Mississippi pet wallaby has found his way back home after being missing for over two weeks.
— LOADED GUN-BACKPACK — Authorities say the discovery of a loaded handgun in a 5-year-old student's backpack may result in criminal charges for any adult who put it there.
— XGR-TERRORISTIC THREAT BILL — Mississippi lawmakers have killed a bill that would have set a prison sentence of up to 10 years for making terroristic threats.
— POLICE SHOOTINGS-IDENTIFICATION —The mayor of Mississippi's capital city has created a task force to explore releasing the names of officers involved in shooting deaths by police.
— ABORTION-MISSISSIPPI — A federal judge in Mississippi has extended his temporary block on the most restrictive abortion law in the United States. By Emily Wagster Pettus.
— XGR-CONFIRMATIONS — State senators are confirming Mississippi Gov. Phil Bryant's choices to sit on state boards and to run state agencies.
— JACKSON ZOO — The board of directors for the Jackson Zoological Society has approved a recommendation to find a more optimal location for the animals and their visitors within the Mississippi city.
— MISSISSIPPI-ISRAEL INVESTMENTS — A new Mississippi law will allow the state treasury to invest up to $20 million in bonds issued by Israel.
SPORTS:
BKC--MISSISSIPPI ST-WRAPUP
Mississippi State basketball took a big jump forward during coach Ben Howland's third season, giving the proud program much-needed hope after several frustrating seasons. The Bulldogs finished with a 25-12 record, advancing to the semifinals of the NIT before losing 75-60 to Penn State on Tuesday . Mississippi State had hoped to be a part of the NCAA Tournament field, but struggled in the regular season's final weeks to fall out of contention for an at-large bid. By David Brandt. SENT: 516 words.
AP Photos MSG132-0327182211, MSG139-0327182311, MSG135-0327182334, MSG140-0327182227.
___
If you have stories of regional or statewide interest, please email them to
The Atlanta AP Bureau: apatlanta@ap.org
The Montgomery AP Bureau: apalabama@ap.org
The New Orleans AP Bureau: nrle@ap.org
The Jackson AP Bureau: jkme@ap.org
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.