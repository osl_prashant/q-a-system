CarbAmericas boosts sales staff
Fort Lauderdale, Fla.-based CarbAmericas Inc. is adding to its Peruvian asparagus program, internally and externally.
“We have a couple of new growers that are coming online, so hopefully our volume will pick up a little,” said Jeff Friedman, president.
CarbAmericas also has added to its sales staff, having hired Darrell Genthner, Friedman said.
“He specializes in mangoes but will also start selling asparagus,” Friedman said.
Genthner, a 48-year industry veteran, came to the company after 14 years  with citrus grower-shipper Noble Worldwide.
 
Crystal Valley adds marketing director
Miami-based Crystal Valley Foods, a year-round asparagus supplier, is looking at ways to expand its program, said Jay Rodriguez, president.
The company will continue to invest in its grower-partner relationships, its bagging and value-added options and its white asparagus supplies, he said.
In a personnel move, Crystal Valley hired Katiana Valdes as its marketing director in May.
Valdes had held numerous positions within the marketing department at Coral Gables, Fla.-based Del Monte Fresh Produce, most recently as senior marketing manager.
Also in May, the company finalized its acquisition of Doral, Fla.-based Team Produce International.
 
Robinson Fresh increases volume
Eden Prairie, Minn.-based Robinson Fresh, a subsidiary of C.H. Robinson Worldwide Inc., is growing its asparagus program, said Alan Guttmann, general manager of asparagus.
“We’re moving more volume, and Mexico is becoming a much more important source for a greater part of the year,” he said.
“Our work with Green Giant Fresh is also ramping up as customers are consistently asking for this globally recognized brand.”
 
Tambo Sur packs more 1-pounders
Coconut Creek, Fla.-based Tambo Sur will be packing more Peruvian asparagus in 1-pound bags this year — an expansion of a program the company started in 2016, said Fabian Zarate, sales manager.
“We’ve always done a 2-pounder, but now we have a couple of chains asking for the 1-pounder, so we’re ramping it up this year.”
 
West Coast Fresh hires manager
Peter Warren is the new business development manager at Boston-based West Coast Fresh. Warren was import director for Pompano Beach, Fla.-based Ayco Farms, directing that company’s Peruvian asparagus deal and handling business-development functions.
“I am happy to report that we have just completed our first year of marketing Peruvian asparagus, and the consensus is that it was very clearly a resounding success,” Warren said July 18.
“Consistent year-round continuity of supply is important to our valued client base. Looking ahead, we have many new and exciting projects around both packaging and vertical integration from farm to distribution.”