Millions of New Yorkers simultaneously ate locally grown apples during the fifth annual Big Apple Crunch.
Big Apple Crunch, sponsored by the New York Apple Association and hosted by FarmOn! Foundation on Oct. 19, raised awareness in support of New York farms and agriculture, according to a news release. Participants of the event bit into an apple at the same time.
Participants of the simultaneous crunch had the chance to win a FarmOn! Victory Garden for a local school, the release said.
NYAA will fund the garden and other educational opportunities through a $1,000 scholarship to the winner’s choice school.
To win the scholarship, participants had to post a tagged apple crunch video to social media, the release said. Posts received by midnight on Oct. 20 were entered into the raffle.
The association set a goal to break last year’s participation record of two million people, the release said, which broke 2015’s record of one million New Yorkers.
The exact number of participants is not yet available.