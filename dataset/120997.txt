Corn




The national average corn basis firmed a penny from last week to 7 1/4 cents below May futures. The national average cash corn price declined 3 1/2 cent from last week to $3.54 1/2.
Basis is weaker than the three-year average, which is 2 1/4 cents above futures for this week.












Soybeans





The national average soybean basis firmed 6 cent from last week to stand 22 3/4 cents below May futures. The national average cash price improved 17 cents from last week at $9.27 1/2.
Basis is softer than the three-year average, which is 2 cents above futures for this week.












Wheat





The national average soft red winter (SRW) wheat basis firmed 1 1/4 cents from last week to 16 cents below May futures. The average cash price declined 11 cents from last week to $4.03. The national average hard red winter (HRW) wheat basis firmed 2 3/4 cent from last week to 87 3/4 cents below May futures. The average cash price declined 10 1/4 cents from last week to $3.29.







SRW basis is firmer than the three-year average of 20 cents under futures. HRW basis is much weaker than the three-year average, which is 40 cents under futures for this week.










Cattle




 




Cash cattle trade averaged $128.01 last week, up $3.68 from the previous week. Cash cattle trade has begun at $130 to $132. Last year at this time, the cash price was $133.90.
Choice boxed beef prices have firmed $5.80 from last week at $215.17.  Last year at this time, Choice boxed beef prices were $222.27.











Hogs





 




The average lean hog carcass bid dropped $2.76 over the past week to $61.89. Last year's lean carcass price on this date was $67.02.
The pork cutout value dropped 2 cents from last week to $74.55. Last year at this time, the average cutout price stood at $80.18.