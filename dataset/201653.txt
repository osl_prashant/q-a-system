After a year of paralyzing El Nino-induced drought, Zimbabwe's farmers have been relieved to receive substantial rain in recent weeks, with normal to above-normal rainfall predicted for the new growing season.But for a share of poor farmers, the rains are not enough to get them back in the fields. After losing their draft animals to the drought, they cannot plow their land to sow new crops.
"My cattle survived the drought but they do not have the strength to pull a plough. They all look like skeletons," said Everson Manatse, a small-scale farmer at Village I in the Mpudzi resettlement area, in eastern Zimbabwe's Manicaland province.
Farmers across the country who normally rely on ox-drawn ploughs to till their fields fear they will have to plow by hand this year - and harvest little at the end of the season.
In this part of Zimbabwe farmers have five-hectare (12-acre) plots, but without animals to draw the ploughs, many have reduced the area under crops this season.
"We are always hungry (and) cannot use hoes to plough on a bigger area," Manatse said. "We will only be able to plough small pieces of land."
"We don't have farming inputs like fertilizers and seed," he added. "We have used all the money we had to buy food."
The Zimbabwean government is helping some farmers with supplies under its Targeted Command Agriculture and Presidential Input Schemes. But the two programs reach a relatively small number of farmers.
Up to 2,000 are expected to receive agricultural inputs such as seeds and fertilizer under the Targeted Command Agriculture. The Zimbabwean press reported that the country's Vice President Emmerson Mnangagwa said the plan aims to ensure the country's food self-sufficiency.
Plowing with Hoes
Lloyd Munguma, a farmer from Chimhenga area, a few kilometers south of the city of Mutare, said the lack of cattle for ploughing had led many small-scale farmers in the area to give up almost completely on sowing crops.
"Very few farmers still have cattle. Most farmers have now turned to zero tillage", or planting in unplowed land, Munguma said in an interview with the Thomson Reuters Foundation.
He said some farmers had turned to working together, using hoes, to plow fields for planting, "but that still will not help much".
According to the state-owned newspaper The Sunday Mail, Zimbabwe lost at least 25,000 cattle during the first half of 2016.
The Zimbabwe Vulnerability Assessment Committee (ZimVAC), a consortium of government bodies, United Nations agencies and non-governmental organizations, said in its 2016 report that Manicaland province recorded the highest number of drought-related cattle deaths, followed by Masvingo and Matabeleland South.
Some areas in Manicaland have benefited from a $12 million initiative by the United States government to save the country's livestock by helping farmers market their cattle and introducing drought-tolerant fodder and low-cost feed formulations using locally available resources.
The report warns that an estimated 4.1 million people will be food insecure during the peak of the lean season from January to March 2017.
However, even with these challenges, Munguma's hopes have risen with the new rainfall. "In terms of rainfall, this season promises to be better than the previous one," he said.
In Mpudzi, Manatse hopes that the rains will improve grazing, helping strengthen his and his neighbors' cattle.
"But it will take us a long time to restock and fully recover from this drought," he said.