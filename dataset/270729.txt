The 2016 Produce Retailer of the Year is headed to sunny Southern California.  
Paul Kneeland, who led the fresh merchandising at Ahold Delhaize’s millennial-focused start-up banner Fresh Formats LLC, starts a new job as senior director of produce and floral for Gelson’s Markets, Encino, Calif., May 1. 
 
“I like their strategy: the high end go-to-market strategy,” Kneeland says. “From a quality aspect, a customer service aspect, and from their positioning in the market, it’s a great company.” 
 
Gelson’s, which opened in 1951, has been growing rapidly the past few years. It was acquired by the private investment firm TPG in 2014, and the company picked up half a dozen former Haggen stores in late 2015.

 
“We are excited to have Paul Kneeland join the Gelson’s team as he has a wealth of both produce and floral experience,” said Gelson's CEO Rob McDougall. “Just last year he was awarded the prestigious Produce Retailer of the Year, indicative of his keen eye for quality and flavor, which has always been a cornerstone of Gelson’s. With our growth strategy, store investments and focus on having the best service and highest quality, I’m confident he’ll fit right in with our culture and continue our ongoing quest to meet and exceed our customers’ expectations.”
 
Currently in 25 locations in the Los Angeles and San Diego markets, it continues to expand and improve existing locations. 
 
“They’re a growing company, and remodeling stores,” Kneeland says. “I’m looking forward to that.” 
 
Kneeland, 52, started his career in Boston at Roche Bros., where he spent 26 years before joining Kings Super Markets in Parsippany, N.J., in 2007.
 
He left Kings in 2015 to lead the launch of Fresh Formats for Netherlands-based Ahold Delhaize, eventually opening several small format, urban stores under the bfresh banner.