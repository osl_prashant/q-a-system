Salinas, Calif.-based Taylor Farms has add two new flavors to its Stir Fry Kit meal options.
The kits are customizable and cook in five minutes, according to a news release. The new flavors are:

Pad Thai with Brussels sprouts, kale, bok choy, carrots, broccoli, green cabbage and snap peas or snow peas with a Pad Thai sauce; and 
Coconut Curry with Brussels sprouts, kale, broccoli, carrots, red cabbage and snap peas or snow peas with coconut curry sauce.

Existing flavors are:

Teriyaki with sprouts, broccoli, carrots, kale, snow peas, red cabbage, and broccoli with a teriyaki sauce;
Ginger Garlic with bok choy, Brussels sprouts, broccoli, carrots, kale, snow peas, and green cabbage with a ginger garlic sauce;
Sesame Chili with bok choy, green cabbage, snow peas, and broccoli with a sesame chili sauce; and 
Mandarin Orange with Brussels sprouts, broccoli, carrots, kale, and snow peas with a mandarin orange sauce.

“Stir Fry kits continue to be a popular choice for consumers looking for healthy, customizable dinner options,” Sydney Burlison, product manager of Taylor Farms Retail, said in the release. “Expanding the flavor options creates additional offerings for all palates and preferences.”
Taylor Farms will exhibit at the Produce Marketing Association’s Fresh Summit expo Oct. 20-21 at booth No. 745.