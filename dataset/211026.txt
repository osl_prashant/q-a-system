USDA Weekly Grain Export Inspections
			Week Ended October 12, 2017




Corn



Actual (MT)
322,672


Expectations (MT)

600,000-800,000



Comments:

Inspections fell 259,575 MT from the previous week and the tally was below expectations. Inspections are running 50.0% behind year-ago versus 49.3% behind the week prior. USDA's 2017-18 export forecast of 1.850 billion bu. is down 19.3% from the previous marketing year.




Wheat



Actual (MT)
322,860


Expectations (MT)
325,000-525,000


Comments:

Inspections fell 28,825 MT from the previous week and the tally was just shy of expectations. Inspections are running 3.8% behind year-ago versus 2.6% behind year-ago last week. USDA's export forecast for 2017-18 is at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual (MT)
1,770,324


Expectations (MMT)
1,100,000-1,400,000 


Comments:
Export inspections fell 279,957 MT from the previous week, and the tally was much stronger than expected. Inspections for 2017-18 are running 7.6% behind year-ago versus 3.7% ahead of year-ago last week. USDA's 2017-18 export forecast is at 2.250 billion bu., up 3.5% from year-ago.