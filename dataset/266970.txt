Two years ago the U.S. had its lowest hay harvest in more than a century. This year is slated to be second lowest hay acreage harvest since 1906.
In the latest Prospective Plantings report from the U.S. Department of Agriculture, an estimated 53,726,000 acres of hay are projected to be harvested in 2018. This is a decrease of 58,000 acres from 2017, or a 0.1% drop in acreage.
The 2018 acreage projection is 245,000 acres more than the total acreage harvested in 2016, an increase of 0.4%. The 2016 hay harvest was the lowest hay harvest since 1906, according to USDA data.
States to see the largest increase in percentage of acreage harvested include:

New Hampshire 17.0%
Ohio 13.2%
Minnesota 8.7%
Nevada 8.3%
Montana 8%

USDA indicates that hay growers in “Montana, North Dakota and South Dakota are optimistic about harvesting more acres than last year to replenish reduced stocks resulting from a dry 2017 production cycle.”
Meanwhile, the states with largest decreases in acreage by percentage include:

Pennsylvania -11.6%
Indiana -8.6%
Oregon -8.2%
South Carolina -7.7%
Florida -6.7%

USDA expects all time harvest acreage lows in California, Connecticut, Illinois, Rhode Island and Wisconsin, as well.
The report did not indicate any specific planting acreages for forage like alfalfa or corn silage. However, last year 6,702,880 acres of alfalfa and 2,603,780 acres of corn silage were harvested in 2017.
In terms of tonnage, 55,068,000 tons of alfalfa hay were harvested in 2017, an average of 3.32 tons per acre. Corn silage yielded 128,356,000 tons in 2017, an average of 19.9 tons per acre.