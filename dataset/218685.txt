Tim York, CEO of Markon Cooperative, Salinas, Calif., is the new chairman of the Alliance for Food and Farming.
He succeeds Chris Zanobini, executive director of the California Pear Advisory Board. York officially started his two-year term during the alliance’s annual board meeting Jan. 30. Zanobini remains on the group’s executive committee, according to a news release.
“Tim’s commitment to the AFF this past year has been remarkable,” Zanobini said in the release “His perspectives and insights in his role as vice-chair have served the organization well.
Rick Tomlinson, president of the California Strawberry Commission and secretary/treasurer for the Alliance for Food and Farming, is also the group’s vice-chairman.
York said the alliance’s message continues to change to counter consumer misinformation/misunderstanding of the industry.
“This evolution continues in 2018 with some strong new tactics and strategies approved by the board during its recent meeting,” York said in the release.
The alliance's goal is to remove fear as a barrier to produce consumption, including responses to the Environmental Working Group's annual Dirty Dozen list.