Together with Mycogen Seeds, FFA students will have the opportunity to see what it’s like in the life of a corn seed sales rep. In honor of its 60th anniversary of partnership with FFA, Mycogen Seeds is providing a set number of blue-and-gold corn seed bags for students to sell.
Each bag sold will provide dollars to the local chapter, state association and national organization. “It’s providing a way for local chapters with FFA to learn, engage and fundraise,” says Zach Ferguson, Mycogen Seeds corn product manager. “The idea is rooted in developing young leaders in ag.”
In addition to dollars raised for the organization, the students will learn more about agronomy, selling and intrapersonal skills needed to work in a sales role. It’s a school-year-round program that has students to ride along with retailers or Mycogen Seeds representatives on sales calls and agronomic check-ins.
“We’ve developed local teams, powered by the local ag retailer and local Mycogen Seed rep and agronomist, that works with the FFA advisor to put together the avenue in which the students learn,” Ferguson says. The company is just getting started with the program and has seven chapters participating this year. It plans to expand the program in years to come.
The seven chapters and retailers involved this year are:

Aurora FFA Chapter, Aurora, Nebraska—Aurora Cooperative, Aurora, Nebraska
York FFA Chapter, York, Nebraska—Central Valley Ag Cooperative, York, Nebraska
North Shelby FFA Chapter, Shelbyville, Missouri—Vortex Seeds, Leonard, Missouri
Centralia FFA Chapter, Centralia, Missouri—MFA Incorporated, Centralia, Missouri
Bureau Valley FFA Chapter, Manlius, Illinois—CPS, Sheffield, Illinois
South Newton FFA Chapter, Kentland, Indiana—Ceres Solutions, Rensselaer, Indiana
Eastern Hancock FFA Chapter, Charlottesville, Indiana—Harvestland, Wilkinson, Indiana

“These students will learn basic agronomics—seed 101, growth stages—ag basics and ag selling basics though a training course,” Ferguson says. The company launched the initiative today with the start of National FFA Convention.