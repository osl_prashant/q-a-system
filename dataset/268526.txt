BC-WI--Wisconsin Weekend Exchange Digest, WI
BC-WI--Wisconsin Weekend Exchange Digest, WI

The Associated Press



Here are the Wisconsin AP Member Exchange Features for  April 14-16:
FOR SATURDAY-SUNDAY:
EXCHANGE-TEEN HIP-HOP DUO
MADISON, Wis. — Krystyn "Kstar" Jones and Ryaah "Wild Child" Wyatt are two 17-year-old girls from Madison's south side making laid-back, playful hip-hop and R&B jams laced with positivity and party vibes. The duo, who performs under the name Trend-N-Topic, won first place at the talent competition Take the Stage: Atlanta last year. For both, music played a central role in their childhoods. The two say they share a mission of making music that's positive and empowering. By Erik Lorenzsonn, The Capital Times. SENT IN ADVANCE: 793 words, photo.
EXCHANGE-CAFE-BUSINESS INTERN
SHEBOYGAN, Wis. — Holly Pospichal had a rough childhood, but now she is a part of a team of business interns at Jake's Café, a space where creative entrepreneurs and freelancers can rent rooms and offices to house their businesses. The business internship, the product of a partnership between Jake's Café and Lakeshore Technical College, gives Pospichal and others the opportunity to practice the skills they are learning in their business classes with real companies that work out of Jake's. By Marina Affo, Sheboygan Press. SENT IN ADVANCE: 733 words, photos.
FOR MONDAY:
EXCHANGE-PRAIRIE CHICKENS
RUDOLPH, Wis. — On April 14, the Paul J. Olson Wildlife Area will be front and center for the second annual Wisconsin Prairie Chicken Festival. The area is made up of scattered parcels ranging from 40 to 860 acres in western Portage and eastern Wood counties. The habitat, which includes canary, timothy, brome and quack grass, along with willow brush and spirea, provides prime areas for prairie chickens to mate, roost, nest and draw onlookers. By Barry Adams, Wisconsin State Journal. SENT IN ADVANCE: 1,296 words, photos.
EXCHANGE-BEANIE MAKER
ELMWOOD PARK, Wis. — Army veteran Nick Loomis figured out how to be a married man and a father, relearned how to walk and ride his Harley, and became a business owner.  In the past couple years, he found inspiration in his ear and on a Harley. Last September, he and his wife acquired a patent for their beanies and started selling them soon after. In less than six months, they'd sold more than 1,000 hats, fulfilling orders from mechanics, construction workers and the Los Angeles Police Department. By Adam Rogan, The Journal Times. SENT IN ADVANCE: 1,169 words, photos.
The AP, Milwaukee