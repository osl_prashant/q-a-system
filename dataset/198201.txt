When veterinary students from around the country assembled at Ohio State University for the 2016 Food Animal Medicine Student Symposium, one of the initial sessions covered bovine pulmonary hypertension (BPH), also known as brisket disease or high-mountain disease.With no mountains in sight, students might have wondered about the topic, but Colorado State University veterinarian Tim Holt quickly noted that the heritable disease, which can cause calf death loss of 20 percent or greater, increasingly occurs in cattle at elevations as low as 3,000 feet above sea level. Also, bulls raised in Ohio or other Midwestern states often ship to ranches at higher elevations and feeder cattle from the region could easily find themselves in feedyards at 4,000 or 5,000 feet of elevation.
Hold described one ranch, about 50 miles north of Fort Collins, Colo., and at around 5,000 feet of elevation, where calf death loss reached 27% for a $100,000 loss to the owner. Necropsy examinations confirmed brisket disease, which traditionally has been considered a risk for cattle at 7,000 feet or higher.
Brisket disease, Holt says, is characterize by:
¬? Alveolar hypoxia
¬? Pulmonary vasoconstriction or shunting
¬? Pulmonary remodeling and hypertension
¬? Right ventricular hypertrophy and dilation
¬? Right heart congestive heart failure
Necropsy findings include brisket edema, fluid and adhesion in the chest cavity, aneurism pending pulmonary hypertension and an enlarged pulmonary artery relative to the aorta.
While some treatments such as using diuretics can temporarily relieve the condition, cattle diagnosed with BPH generally should be moved to lower elevations, Holt says.
Confounding factors in diagnosing BPH include:
¬? Viral respiratory diseases such as IBR, BVD, PI3 or BRSV
¬? Bacterial respiratory diseases including those caused by Mannheimia haemolytica, Pasteurellamultocida, Mycoplasma, Haemophilus or E. coli.
¬? Internal parasites
¬? Extended cold temperatures
¬? COPD, asthma, fog fever or other chronic illnesses
During the symposium, Holt also conducted a hands-on workshop for students at a nearby farm, where they conducted pulmonary arterial pressure (PAP) tests on several Holstein steers. The test involves inserting a large-diameter needle into the animal's jugular vein, and feeding a catheter back through the right atrium and right ventricle into the pulmonary artery. A pressure transducer attached to the catheter measures the pulmonary arterial pressure.
After Holt demonstrated, the students each took turns inserting the needle and catheter and taking pressure readings. Among other practical tips, Holt showed students that, while holding the animal's head to the side, pulling up on its ear helps expose the jugular vein.
The test measures systolic and diastolic pressure, and uses the mean of the two for a PAP score. Holt says the stress of testing can elevate systolic pressure, while diastolic pressure tends to drop, and the PAP score remains unaffected.
A PAP score between 45 and 48 mmHg suggests moderate to high risk, while a score above 48 indicates high risk of BPH and brisket disease, Holt says.
Ideally, cattle are PAP tested after spending at least three weeks at high elevation, but tests at lower elevation can predict the highest-risk cattle.
With practice, modern equipment and reasonably cooperative cattle, Holt says veterinarians can complete each PAP test in two to three minutes. He adds that producers in mountain areas increasingly are testing replacement heifers as well as bulls. Young animals with marginal PAP scores, he says, probably should be sold or moved to lower elevations, as scores tend to move higher as the animal ages.
For more information on BPH and PAP testing, read this bulletin from Colorado State University.