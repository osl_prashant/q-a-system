Naturipe Farms LLC has entered into a long-term avocado deal with one of its Peruvian blueberry suppliers.Talsa, a grower that belongs to Grupo Rocio SA,  is packing and shipping Peruvian avocados under the Naturipe brand this year.
Grupo Rocio is a longtime partner with Naturipe for South American blueberries, said Fortunato Martinez, general manager of Naturipe Avocado Farms.
“They were already a blueberry supplier; they had avocados, and it seemed like a logical extension,” Martinez said.
Ulises Quevedo, Grupo Rocio’s CEO, declined to comment.
Martinez said the agreement is a “long-term” one.
“We expect to produce a decent amount our first year and continue to grow strong in the coming years,” he said.
Talsa is “technically” the Peruvian partner-grower involved with Naturipe Avocado Farms, said Kyla Oberman, Naturipe’s marketing director.
Oberman, Naturipe’s marketing director, said Naturipe plans to officially announce the new product availability through the partnership at the annual United Fresh Produce Association trade show and expo June 14 in Chicago.
“We are always looking for new opportunities to grow our business, and at the same time offer our customers a year-round supply of healthy, premium products,” Oberman said. “Avocados made perfect sense for Naturipe.
“Our avocado growers complement our company in that they’ve built their business on the same principles and values as we have: a commitment to being the best growers in the world, loving what we grow and focusing on building collaborative relationships.”