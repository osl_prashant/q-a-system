Calf and yearling markets continued to recover from the mid-October wreck when prices declined 13% in a two-week period. Late-November and early-December auction prices were 7% higher on yearlings and 10% higher on calves, compared with pre-Thanksgiving markets. Unseasonably warm weather through November improved cattle gains on fall pastures, before mid-December storms brought harsh temperatures to the Plains.


Note: This story appears in the January 2017 issue of Drovers.