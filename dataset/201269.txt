Farmers in Missouri can again purchase and use new dicamba formulations for over-the-top use in soybeans and cotton—with a few additional requirements.
­The Missouri Department of Agriculture (MDA) has approved of Special Local Need label for each herbicide, Engenia, FeXapan and Xtendimax. MDA says the labels provide special provisions and safeguards including:

Wind Speed – do not apply at wind speeds greater than 10 mph. Applicators must measure and record wind speed and wind direction for each field prior to application.
Application Timing – do not apply before 9:00 a.m. and do not apply after 3:00 p.m.
Certified Applicator – All applications of Engenia, FeXapan and Xtendimax must be made by a properly licensed Missouri certified private applicator or certified commercial applicator, certified noncommercial applicator or certified public operator.
Dicamba Notice of Application Form – Certified applicators must complete an online web-based form “Dicamba Notice of Application” prior to the actual application. The Dicamba Notice of Application Form is posted on the Missouri Department of Agriculture’s website at: www.Agriculture.Mo.Gov/dicamba/notice/.
Recordkeeping Requirements – Certified private applicators, certified noncommercial applicators and certified public operators must keep and maintain a record of use for each application of Engenia, FeXapan and Xtendimax herbicide.

“From the moment the stop sale and use order went into effect, we’ve been working to get these weed control products back into the hands of our farmers,” says Director of Agriculture Chris Chinn in a recent press release. “BASF, Monsanto and DuPont came to the table and agreed to additional safeguards for product use in response to issues we’ve faced this growing season.”