The following commentary does not necessarily reflect the views of AgPro or Farm Journal Media. The opinions expressed below are the author's own.The 4th of July is my second-most favorite holiday of the year--second only to Thanksgiving. Like that holiday, this one causes me to reflect and be grateful. I love this country and what she stands for. There are many times I'm disgusted with politicians and by the behavior of my fellow citizens, but at the end of the day I'm an American through-and-through and proud to be one. As you spend time this weekend with your family and friends, I hope you take a few minutes to reflect on the many blessings we all enjoy that were purchased by the commitment and shed blood of our fellow countrymen. I hope you'll also enjoy a couple of laughs from my tasteless humor, exemplified by the following examples thanks to Scott Jensen at GlobalFlare. Here's wishing you the happiest 4th of July. May freedom ring. -Rhonda Brooks
 
#1 We know how to landscape
 

 
#2 We dominate at sports

 
#3 Freedom rings

 
#4 We love animals

 
#5 We win at measuring things and space travel

#6 Freedom

 
#7 Our style

 
#8 American food

 
#9 Our grandmas

 
#10 Our Grandpas

 
#11 Our generosity

 
#12 Our persistence