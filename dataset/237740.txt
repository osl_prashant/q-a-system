BC-Cotton
BC-Cotton

The Associated Press

NEW YORK




NEW YORK (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Wednesday:
Open  High  Low  Settle   Chg.COTTON 2                                50,000 lbs.; cents per lb.                Mar      81.75  81.75  81.50  81.65   —.11May      82.27  83.05  81.80  82.93   +.68Jul      82.79  83.36  82.32  83.29   +.50Sep                           77.11   +.06Oct                           78.30   —.17Nov                           77.11   +.06Dec      77.00  77.50  76.77  77.11   +.06Jan                           77.26   +.03Mar      77.15  77.63  76.96  77.26   +.03May      76.95  77.05  76.87  77.01        Jul      76.80  76.80  76.60  76.70   —.09Sep                           71.82   —.13Oct                           74.53   —.09Nov                           71.82   —.13Dec      71.95  71.97  71.75  71.82   —.13Jan                           72.20   —.13Mar                           72.20   —.13May                           73.01   —.13Jul                           73.24   —.13Sep                           72.65   —.38Oct                           72.93   —.26Nov                           72.65   —.38Dec                           72.65   —.38Est. sales 30,779.  Tue.'s sales 24,356 Tue.'s open int 259,538,  up 198