Chicago Mercantile Exchange live cattle futures were weakened on Tuesday by uncertainty over how much packers will pay for cattle this week and whether the seasonal lull in wholesale beef demand has ended, traders said.Futures remained at a steep discount, or undervalued, compared with this week's expected prices for market-ready or cash cattle, which minimized market losses.
August ended down 0.275 cent per pound at 112.475 cents, and October closed 0.525 cent lower at 110.600 cents.
Demand for beef remains low, the outlook for U.S. beef exports to China is still a mystery and cattle supplies are plentiful, Oak Investment Group President Joe Ocrant said, citing the bearish market for live cattle futures.
"Right now I'm trading it (futures) gingerly," said Ocrant.
Market bears believe tepid beef demand and increased cattle numbers ahead might discourage packers from bidding up for supplies this week.
Bullish traders said easing but still-profitable packer margins and currently tight cattle supplies in parts of the U.S. Plains might mitigate potential cash cattle price losses.
A week ago packers paid $116 to $119 per cwt for slaughter-ready cattle.
Investors await Wednesday's Fed Cattle Exchange sale for cash price direction. A week ago, animals moved at $116 per cwt.
Sell stops and softer live cattle futures pressured CME feeder cattle for a third straight session.
August feeders ended down 0.225 cent per pound at 146.025 cents. 
Hogs End Mostly Firmer
CME lean hog futures for the most part were supported by their discounts to the exchange's hog index for Aug. 4 at 86.10 cents, traders said.
They said profit-taking, along with Tuesday morning's weaker cash and wholesale pork prices, capped market advances.
August, which expires on Aug. 14, closed up 0.125 cent per pound to 83.525 cents. Most actively traded October ended 0.375 cent lower at 67.750 cents, and December finished up 0.050 cent to 62.550 cents.
Cash prices suffered as supplies grow seasonally, a trader said.
The pork cutout value fell, led by pork belly prices that dropped below $200 per cwt for the first time since late June - likely confirming the market has passed the summer top, he said.
Retailers balked at paying more for pork after packing plants that were closed on Monday for a floater holiday resumed normal operations on Tuesday.
Packers typically give employees time off in August in exchange for work during winter holidays.