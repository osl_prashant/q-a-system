New officers and directors nominated to serve on the 2018-19 United Fresh Produce Association have been announced.
United Fresh immediate past chairman Tony Freytag, executive vice president for Crunch Pak, Cashmere, Wash., announced the list of officers and directors nominated to serve on the board.
The new board and officers will be installed at the group’s April 17 meeting in Santa Fe, N.M, according to a news release.
“Our organization thrives on the strength and diversity of the leadership on the United Fresh board of directors,” president and CEO Tom Stenzel said in the release. “This slate of new board members, along with those returning to the board, will provide a strong representation and leadership of our industry.”
Board executives will be:

Chairwoman: Cindy Jewell, vice president of marketing at California Giant Berry Farms, Watsonville, Calif.
Chairman-elect: Greg Corrigan, senior director of produce and floral for Raley’s Family of Fine Stores, Sacramento, Calif.
Secretary/treasurer: Danny Dumas, vice president North American sales and product management of Del Monte Fresh Produce, N.A. Inc., Coral Gables, Fla.
Immediate past chairwoman: Susan Reimer-Sifford, Castellini Group of Cos., Newport, Ky., will step down as chairwoman and remain on the board.

Incoming board members with two-year terms are:

Mason Arnold, founder of Cece’s Veggie Noodle Co., Austin, Texas;
Luca Ascari, vice president of sales for ABL SPA, Cavezzo, Modena, Italy;
John Gates, president of Lancaster Foods Inc., Jessup, Md;
Richard Gonzales, vice president of global produce sourcing for Walmart Stores Inc., Bentonville, Ark.;
Eric Halverson, chief executive officer of Black Gold Farms, Grand Forks, N.D.;
Jeff Huckaby, president of Grimmway Enterprises Inc., Arvin, Calif.;
John Newell, CEO of Windset Farms, Delta, British Columbia;
Ramon Paz-Vega, chairman of Avocados From Mexico, Irving, Texas; and
Heather Shavey, assistant vice president/general merchandise manager at Costco Wholesale, Issaquah, Wash.

John Jackson, CEO of Beachside Produce LLC, Guadalupe, Calif., will be the chairman of the Grower-Shipper segment board.
Officers of the association’s market segment boards and councils will continue their service. They are:

Chairman of the Fresh-Cut Processor Board is Brian Jenny, vice president and general manager of value-added fresh at Naturipe Farms LLC, Sugar Hill, Ga.;
Vice chairwoman of the Fresh-Cut Processor Board is Raina Nelson, senior vice president of supply chain for Renaissance Food Group, Rancho Cordova, Calif.;
Chairwoman of the Wholesaler Distributor Board is Jackie Caplan Wiggins, vice president and CEO of Frieda’s Inc., Los Alamitos, Calif.;
Vice chairman of the Wholesaler Distributor Board is Tom Brugato, president and CEO of Pacific Coast Fruit Co., Portland, Ore.;
Chairman of the Retail-Foodservice Board is Shannon Mikulskis, program lead of distribution for Chick-fil-A Inc., Atlanta;
Vice chairman of Retail-Foodservice Board is Jeff Cady, director of produce and floral for Tops Friendly Markets, Buffalo, N.Y.;
Chairman of the Finance and Business Management Council is Jason Pounds, treasurer/secretary at Hardie’s Fresh Foods, Dallas;
Chairman of the Food Safety & Technology Council is Drew McDonald, vice president of quality and food safety at Taylor Farms Inc./Taylor Fresh Foods, Salinas, Calif.;
Chairman of the Government Relations Council is Charles Wingard, director of field operations for Walter P. Rawl & Sons Inc., Pelion, S.C.;
Chairman of the Produce Marketing and Merchandising Council is Mark Munger, vice president of sales and marketing at 4Earth Farms, Los Angeles;
Chairman of the Supply Chain Logistics Council is Jeff Moore, vice president of sales-Midwest region for The Tom Lange Co. Inc., St. Louis; and
Chairman of the United Fresh Start Foundation is Phil Muir, president and CEO of Muir Copper Canyon Farms, Salt Lake City.