Greenhouse and field trials have Arkansas weed scientists looking for answers
It only takes three generations for a demon seed to produce a flower of fire. In a greenhouse setting in 2015, Palmer amaranth developed full-blown dicamba resistance. As the herbicide dominoes fall, weed resistance is forever around the corner and strong management requires multiple effective modes of action.

Jason Norsworthy transferred virgin Palmer from a soybean field to a greenhouse and sprayed the first two generations with dicamba at sublethal doses. After he selected the survivors and grew them out, the third generation was resistant to a full label rate of dicamba (0.5 lb. acid equivalent per acre). Even though this resistance was recorded in an artificial environment, the research confirms herbicide resistance can develop in just three years if the same weed population is exposed to sublethal chemical doses.

Norsworthy, an Extension weed scientist with the University of Arkansas, ensured a timely application by spraying a low dose of dicamba on 1.5" to 2" Palmer that provided good but partial control on the first generation. (It killed most of the plants.) The experiment mirrored potential coverage or calibration issues often encountered in the field. The Palmer survivors crossed and produced seed. Norsworthy slightly increased the dicamba dosage for the second generation and once again killed most of the Palmer after a spray application.

The process was repeated once more for the third generation of Palmer, except the application rate was boosted to the commercially labeled field rate. This time a quarter of the plants survived the full dicamba rate.

“Under greenhouse conditions, we shifted the tolerance of pigweed to dicamba about three-fold in only three generations,” Norsworthy says.

The speedy development of resistance isn’t unique to dicamba and can be demonstrated in other herbicides, including 2,4-D. (In 2010, Norsworthy showed basically the same results with glyphosate and Palmer.) However, the third-generation dicamba findings are particularly relevant considering current tank mix and buffer prohibitions on XtendiMax, Engenia and FeXapan.
Dicamba-tolerant crop systems bring a unique resistance dynamic to farmland. After weighing multiple factors, many producers are shifting entire farms to dicamba-tolerant soybeans and cotton. Questions over boom cleaning, separate spray rigs, drift concerns and overall efficiency boil down to money and time. With a farmer possibly facing thousands of acres in need of attention in a tight window, speed and efficiency become paramount. The bare truth: Monoculture is far simpler.

However, simplicity plays into the waiting hands of weed resistance. Smoking fields with glyphosate was once the ultimate in efficiency, but all silver bullets lose their sheen. Economics and practicality work against weed resistance management. 

Norsworthy and weed science colleagues Bob Scott and Tom Barber have trials across Arkansas, and recent results from soybean fields in the northeast part of the state raise questions. Norsworthy and Barber note reduced dicamba efficacy on PPO-resistant Palmer. Generally, dicamba is highly effective against 3" to 4"-tall Palmer, but the PPO-resistant populations are showing a lower level of control. “We’re not saying we have dicamba-resistant pigweed in these fields. We don’t fully understand what we’re seeing and are investigating the data,” Norsworthy says.

Barber was surprised by the diminished level of Palmer control in Marion (Crittenden County), but he hasn’t seen the same results in his research in nearby Marianna and Newport (Lee and Jackson counties), where dicamba efficiency remains strong. However, Marianna and Newport don’t have documented PPO-resistant Palmer.

“We’re not saying this is dicamba resistance in northeast Arkansas, but what we’re seeing is a decrease in overall efficacy of dicamba,” Barber says. 

In Marion, the dicamba is affecting Palmer and causing injury, but the plants are able to recover, even after repeat applications. “If we get two years of data saying the same thing, it’ll be an issue of worry,” Barber says.

At the Marion plots in 2016, Barber tested Roundup Ready, LibertyLink and Xtend soybeans. He looked at 27 pre-emergent options to determine the best at-planting combination to control PPO-resistant Palmer. (If plants are PPO resistant, Valor is out of the running.) After each pre-emergent combination, the research team came back 28 days later and sprayed Roundup with Flexstar in the Roundup Ready system, Liberty in the LibertyLink system and dicamba in the Xtend system.
The Roundup Ready system offered minimal control with Palmer already resistant to glyphosate and PPOs. The LibertyLink system was fairly clean after two applications. However, after two dicamba applications, the Xtend system was less effective than the LibertyLink system. 








 


Field research by weed scientists in Arkansas found reduced dicamba efficacy on PPO-resistant Palmer.











I-40 essentially splits Arkansas east to west and serves as the Mason-Dixon of PPO-resistance for weed scientists. Palmer growing north of I-40 has a 50% chance of PPO-resistance, and Barber believes the percentage is set to rise. The northeast Arkansas corridor is a hotbed of PPO-resistance, and if more fields respond to dicamba in the same manner observed in Crittenden County, Barber fears it will translate to more herbicide applications and more potential for off-target movement.

In the general area of PPO resistance (Arkansas, Mississippi, Missouri and Tennessee), a solid weed control program relies on two pre-emergent residuals to tackle PPO resistant Palmer. Barber recorded optimal results from combinations of Metribuzin and Zidua, or Metribuzin and Dual Magnum. 

“Those are our recommendations in 2017. If we’re not robust at planting with pre-emerge herbicides, whether in the Xtend or Liberty system, we’ll be behind the eight ball once pigweeds start to break,” Barber says.

Scott confirms the robust nature of PPO-resistant Palmer in northeast Arkansas and says dicamba, Liberty and 2,4-D choline weren’t entirely effective at three farm trial sites this past year. Yet, at Scott’s research farm in nearby Newport, where Palmer is merely glyphosate-resistant and ALS-resistant, the same chemicals were highly effective. 
“I think there will be surprises waiting if growers only use dicamba to kill pigweed in 2017,” he adds. 

The absolute necessity of multiple modes of chemical action is very important in 2017, Norsworthy adds. “Choosing two effective modes of action is required, and I emphasize ‘effective.’ Otherwise, a grower is simply not doing enough to mitigate the risks of resistance and the weeds will get worse,” he says.

With an increasingly hostile roster of resistant weeds, crops are under constant waves of assault that necessitate a diversified response. The days of polite recommendations to mix modes of action have given way to outright demand: Multiple, effective modes of chemistry are a farming absolute.