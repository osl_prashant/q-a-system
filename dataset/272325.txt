Nebraska men launch malthouse to supply local breweries
Nebraska men launch malthouse to supply local breweries

By PAUL HAMMELOmaha World-Herald
The Associated Press

OMAHA, Neb.




OMAHA, Neb. (AP) — Like the pioneers of old, Zach Davy and his business partner, Bill Hall, are stepping into virgin territory in Nebraska.
And they are discovering that in plowing new ground, you hit a few stumps.
In an out-of-the-way warehouse off Interstate 80 in Omaha, the partners are establishing a malthouse, a place where grains of barley are transformed into the toasted crop that gives beer its, well, fine malty taste.
They started in their basement, sprouting barley in plastic tubs and drying it in a makeshift kiln fashioned out of parts from a clothes dryer.
For a while, with only one electrical outlet to work with, they had to decide: "Do we dry our clothes or kiln our barley?"
But the barley boys have grown their malting homestead, mostly using their own ingenuity, some old farm equipment and a lot of spare parts.
From an initial 6 acres of barley grown on Davy's family farm near Lynch, Nebraska, they now have nine farmers who are tilling 140 acres of both winter and spring barley for them.
Their new malting kiln, a huge, steel-lined wooden box designed by Hall, got its first trial run a week ago. It can process 1,000 pounds of barley at a time, a tenfold increase over their initial clothes-dryer prototype. And a rattling contraption called a "debearder" that they built has proved ideal for shaking off the sprouts from their malted barley during the final stage of cleaning the finished product.
A handful of craft breweries in Omaha and Lincoln are ready to use their malt, and two guys who met at an Omaha church gathering think their malting startup might just turn into something.
The goal, for now: grow into a regional, "craft" malt supplier, and perhaps help develop a malt with a unique flavor, spiced by the soil of Nebraska.
"It's been a journey, but we're pretty excited about getting things moving," Davy told the Omaha World-Herald .
They aren't the only ones trying to establish a barley malting beachhead in Nebraska. Blue Blood Brewing in Lincoln has an offshoot called Nebraska Malt, which has 10 growers tilling varieties like two-row, pale and pils barley.
But Missouri Valley Malt, as the Davy-Hall endeavor is called, was the first to provide malt for an "all-Nebraska ingredient" beer, a brew called Nebraska Native, which debuted at Boiler Brewing Company of Lincoln in January 2017.
It was quite a feat. While the state now has more than 50 craft breweries, with several hopyards sprouting across the state, growing barley and malting it has been an also-ran.
For Davy and Hall, it started with a lot of research. Then, in 2016, the effort progressed to clearing trees from an old pasture along the Missouri River in Boyd County.
Davy, who grew up a doctor's son in Norfolk, had to become acquainted with things like a plow, a planter and a stump grinder.
His first attempt at plowing, using borrowed equipment in soil that was too wet clogged the implement to a halt every few feet.
"It was literally just inch by inch," Davy said.
He persisted, just like the rugged pioneers. The field dried out and they planted their first crop.
Now they're leaning more on others to grow the barley while focusing on the three-step process of malting: steeping, sprouting and drying. They have traveled from North Dakota to North Carolina is search of knowledge, and are now working with a barley specialist at the University of Nebraska-Lincoln. While barley has been grown in the state, growing it for beer is all new.
"There's been a lot of talking with other maltsters to understand the nature of the beast," Davy said. "It's a combination of art and science."
In their warehouse, they produce malt the old-fashioned way, using equipment they built, modified and adapted. A heavy, wooden rake they made looks like something out of the Middle Ages. "You can't buy them off the shelves at Menards," he said.
The rakes turn the barley as it germinates on a concrete floor, helping ensure that it sprouts uniformly. The barley eventually is dried and toasted, and the sprouts shaken off. Flavors and colors can be adjusted using heat and the length of kilning. In the end, the malted barley provides the sugars that become alcohol during fermentation.
Davy worked as a financial analyst for Conagra before the jump to malting and another, software startup. Hall, a native of Omaha, still works as a mechanical engineer when he's not at the malthouse. Both are 30, super psyched and glad to be part of an industry that brings people together and produces an end product valued for its craftsmanship and nuance.
Idaho, Minnesota, Montana, North Dakota and Washington may produce the vast majority of the barley in the U.S. now, but Davy and Hall want to put Nebraska on the map.
"We'd like to start offering things you can't get anyplace else in the brewing world," Davy said.
A few days in Plattsmouth last week led me down Main Street to a place my Uncle Carl always raved about: Mom's Cafe.
It's a place Norman Rockwell would have loved. Stools that swivel surround the front counter. The walls are covered with old 7UP and Pepsi-Cola signs, and there's a handwritten listing of the pies of the day and a catchy motto: "Home on a Plate." And, my favorite, there's a World-Herald box right by the front door.
The food reminds people of home, said waitress Kathy Sutton, who has been hustling tables at Mom's for 26 years.
She's a newbie. Co-owner Mary Harrison has been behind the cafe's grill for 27 years.
My uncle loved their breakfasts, but I'll recommend their tasty ground beef casserole, Mom's Favorite No. 1, served over German noodles called spaetzle. Their homemade chicken fried chicken was a crispy notch above the norm, too.
The only problem, according to two customers as they ducked out the front door, is too much food.
That, they said, is one of the great things about Mom's.
Lost in the hustle of activity during the Nebraska Legislature's recent session was a resolution honoring Lincoln architect Tamara Eagle Bull.
Eagle Bull, who co-founded her architectural firm three decades ago, was the nation's first female, Native American architect. Women are a distinct minority in the field, comprising less than 20 percent of all licensed architects.
She recently received a national award from the American Institute of Architects for her social responsibility and activism, including her advocacy for culturally relevant design.
Eagle Bull grew up on South Dakota's Pine Ridge Indian Reservation. Among her work: three concepts for a long-desired memorial at the site of the Wounded Knee Massacre, which currently is marked only by an often-vandalized sign. The site is considered sacred by the Oglala Sioux. Funding for the project is being pursued.
___
Information from: Omaha World-Herald, http://www.omaha.com


An AP Member Exchange shared by the Omaha World-Herald.