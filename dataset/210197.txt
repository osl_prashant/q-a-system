Instacart delivery is now available for fresh groceries from 376 Costco locations.
Chief financial officer Richard Galanti announced the news during the company’s fourth-quarter earnings call Oct. 5.
Fresh items can be delivered the same day through Instacart, and two-day delivery for dry items is available through Costco.
The company has been working for a while to make its website better for e-commerce.
“As discussed over the past few quarters, much of our efforts over the past year focused on improving the functionality of our site,” Galanti said.
“We improved search, streamlined the checkout process, improved our members’ ability to track their orders, and automated much of the returns process, and we also improved our online merchandising efforts by adding high-end and well-known brand names.”
Galanti said the company, which is a major organic player, has not seen any effect from the Whole Foods-Amazon merger and the lowering of some prices in those stores.
E-commerce brought in $4.6 billion for Costco in fiscal 2017, an increase of 15%.
The company has e-commerce in the U.S., Canada, U.K., Mexico, South Korea and Taiwan and plans to add more countries within 18 months.