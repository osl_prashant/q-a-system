University of Richmond using goats for landscape management
University of Richmond using goats for landscape management

The Associated Press

RICHMOND, Va.




RICHMOND, Va. (AP) — The University of Richmond is planning to use a herd of goats as part of a landscape management project.
The university said in a news release Monday that the goats will help clear invasive species from part of the campus as part of an effort to create an "eco-corridor." They will reside in a contained area on the outskirts of the university's campus for twelve weeks.
Rob Andrejewski is the university's director of sustainability. He says incorporating goat browsing into its invasive plant removal process will reduce the need for herbicide and gas-powered equipment.
According to the news release, the University of Virginia, William and Mary and the James River Park System have all successfully used the animals as a mechanism for invasive species removal.