Plan to limit fishing guides on Madison River rejected
Plan to limit fishing guides on Madison River rejected

The Associated Press

HELENA, Mont.




HELENA, Mont. (AP) — The Montana Fish and Wildlife Commission has rejected a plan to limit commercial fishing guides on the popular and crowded Madison River.
The commission unanimously voted against the proposal Thursday after several outfitters spoke out against it.
The plan proposed by Fish, Wildlife and Parks would have capped the number of commercial fishing guides and barred guides from certain stretches of the river on different days.
The outfitters and guides generally agreed something needs to be done to address the river crowding. But they questioned the plan's focus on only them, when private, non-commercial anglers make up 87 percent of the users annually.
FWP director Martha Williams says she heard the comments loud and clear, and that the department would seek more input and come up with a revised plan.