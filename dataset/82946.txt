Our Nutrient Composite Index fell 0.71 points this week to 536.60.





Anhydrous ammonia softened 26 cents per short ton to $521.06.





DAP fell $4.69 to $452.07.
			MAP firmed 14 cents to $459.17.





UAN28% firmed 3 cents to $248.79.
			UAN32% gained 17 cents to $280.98.





Urea softened 85 cents per short ton to $362.19.





Potash firmed $1.23 to $333.66.