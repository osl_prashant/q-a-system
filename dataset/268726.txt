(Bloomberg) — President Donald Trump told lawmakers he is considering rejoining the Asia-Pacific trade pact he withdrew from shortly after taking office as he expressed confidence the U.S. is headed toward resolving trade conflicts without economic disruption.
A week after escalating trade tensions with his threat to impose tariffs on an additional $100 billion in Chinese products, Trump said Thursday the two countries ultimately may end up levying no new tariffs on each other.
“Now we’re really negotiating and I think they’re going to treat us really fairly,” Trump said during a White House meeting with Republican governors and lawmakers from farm states, “I think they want to.”
The remarks sent another conciliatory signal the administration has been sending this week on a trade war of words that’s rattled markets. Trump also indicated that talks are progressing toward successful renegotiation of the North American Free Trade Agreement.
After reporters left the room, Trump told the lawmakers he deputized economic adviser Larry Kudlow and U.S. Trade Representative Robert Lighthizer to explore re-entering the Trans-Pacific trade accord. Senator Ben Sasse told reporters of Trump’s commitment on TPP, and two White House officials who spoke on condition of anonymity confirmed the statement.
“He multiple times reaffirmed the point that TPP might be easier to join now,” said Sasse, a Nebraska Republican who participated in the meeting with Trump.
The GOP political leaders are especially concerned about the impact trade retaliation from China could have on U.S. agriculture, which relies heavily on exports to China. The Asian nation said listed U.S. soybeans and pork among its prime targets for proposed retaliatory duties.
 
Why Pacific Trade Deal Is a Big Deal With U.S. or Not: Quick Take
During his first week in office, Trump withdrew the U.S. from the Trans-Pacific Partnership Agreement. The pact, which was conceived as a counterweight to China’s dominance in the region, had been negotiated under the Obama administration but never approved by Congress.
The 11 remaining nations, representing 13 percent of global output including Japan and Canada, finalized a revised version of the trade pact last month, renaming it the Comprehensive and Progressive Agreement for Trans-Pacific Partnership or CPTPP.
Trump suggested in February he could be open to rejoining the trade bloc during a news conference with Malcolm Turnbull, the prime minister of Australia, which is in the CPTPP.
One of the White House officials said that while the president prefers negotiating bilateral trade deals, a multilateral deal with the TPP countries would counter Chinese competition and would be faster than negotiating one-on-one with each of the 11 other nations.
 
Open China
In his remarks on Thursday, Trump cited a speech by Chinese President Xi Jinping that the U.S. president interprets as a signal China is about to open its markets to more U.S. goods. “He’s going to get rid of a lot of taxes and tariffs,” Trump said of Xi.
Xi pledged a “new phase of opening up” Tuesday in a keynote address to the Boao Forum for Asia. While the speech offered little new policy and made no mention of Trump, Xi affirmed or expanded on proposals to increase imports, lower foreign-ownership limits on manufacturing and expand protection to intellectual property -- all issues central to the U.S. president’s trade complaints.
Trump clearly regarded the remarks as conciliatory, and said again Thursday that it was a “good speech.”
“Very thankful for President Xi of China’s kind words on tariffs and automobile barriers,” Trump said on Twitter on Tuesday. “Also, his enlightenment on intellectual property and technology transfers. We will make great progress together!”
 
Talks Stopped
Talks between the world’s biggest economies broke down last week after the Trump administration demanded steps to curtail China’s support for high-technology industries, a person familiar with the situation said. The U.S. has accused China of unfairly subsidizing targeted sectors and forcing foreign companies to transfer technology in areas like robotics, aerospace and artificial intelligence.
The U.S. hasn’t announced a date it intends to impose tariffs on an initial list of $50 billion in Chinese goods. The administration is also expected to release a separate list for the additional proposed tariffs covering $100 billion in imports from China.
On talks with Canada and Mexico to revamp Nafta, Trump said negotiations are “coming along great,” though he added there’s no timeline on reaching a deal.
An agreement could be completed within a few weeks or five months, he said, adding “I don’t care.” The U.S. will come up with a good deal and “agriculture will be taken care of,” Trump said.
 
©2018 Bloomberg L.P.