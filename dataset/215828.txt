The Public Health Agency of Canada has linked an E. coli outbreak to romaine lettuce.
Twenty-one people have fallen ill, and 10 have been hospitalized, according to a public health notice on the agency website.
The notice from the Public Health Agency of Canada said no product recalls are associated with the outbreak, and it did not say at which retail or foodservice locations the lettuce was purchased.
Thirteen E. coli infections have been reported in Newfoundland and Labrador, five in New Brunswick and three in Quebec. The group includes 15 women and six men. They range in age from 5 to 72.
Many of them reported eating romaine lettuce before they became sick, according to the notice. The Canadian Food Inspection Agency is working with health officials to identify the source of the lettuce.
Most of those ill reported symptom onsets between Nov. 16 and Nov. 22, with a few more illnesses reported Nov. 25-28.