Top Acres Supreme Wizard-ET, owned by Wayne E. Sliker, St. Paris, Ohio, finished on top in the International Brown Swiss Show at World Dairy Expo on Thursday, October 5. Wizard was first awarded top honors in the Component Merit Class, then Senior Champion Female, and finally, Grand Champion Female. She and Sliker also took home the $1,000 Udder Comfort Grand Champion Cash Award. Meanwhile, Reserve Grand Champion honors went to Cutting Edge Thunder Faye, the first place Senior Three-Year-Old Cow, owned by Ken Main and Peter Vail of Copake, N.Y.
Official judge Keith Topp, Botkins, Ohio and associate judge Dean Dohle, Halfway, Mo. placed 317 animals.

Show results and placings are as follows:
Spring Heifer Calf:
1. Silver Top Allstar Trick, leased by Shelby Besani, owned by Silver Top Swiss, Morrisville, Vt.
2. Hills Valley Lucky Raylinn, Hill's Valley Farm, LLC, Cattaraugus, N.Y. 
Winter Heifer Calf:
1. Wind Mill Carter Snow Angel, E Kueffner, T Packard and M & L Hellenbrand, Boonsboro, Md.
2. Siegerts Power Merry, Kelvin Webster, Laingsburg, Mich. 
Fall Heifer Calf:
1. Fairdale Hill Braiden Shey-ET, Leslie & Linda Bruchey, Westminster, Md.
2. Round Hill Talant Wilona-ET, Emily Heffner and Nathan Golenberg, Hagerstown, Md. 
Summer Yearling Heifer:
1. Latimore Braiden Twinkie, Lindsey Rucks, Okeechobee, Fld.
2. Onword Lawman Vanilla, Carter Kruse, Dyersville, Iowa 
Spring Yearling Heifer:
1. Glen Islay Biver Fairytale, Glamourview-Iager & Walton, Walkersville, Md.
2. West Winds Wonder Fancy, Milleanne Mullinix, Monrovia, Md. 
Winter Yearling Heifer:
1. R Hart BJ 25, Leslie & Linda Bruchey, Westminster, Md.
2. Cutting Edge B Trail-ETV, Laurie A. Winkelman, Watertown, Wis. 
Fall Yearling Heifer:
1. Top Acres Braiden Wiza-ET, Lindsey Rucks, Okeechobee, Fld.
2. Siegerts Braiden Sheena, Coltin, Morgan, Claytin & Mady Wingert, Harmony, Minn. 
Junior Champion Female:
Top Acres Braiden Wiza-ET, Lindsey Rucks, Okeechobee, Fld. 
Reserve Junior Champion Female:
Fairdale Hill Braiden Shey-ET, Leslie & Linda Bruchey, Westminster, Md. 
Junior Best Three Females:
1. Ken Main and Peter Vail, Copake, N.Y.
2. Pit-Crew Genetics, Cambridge, Minn. 
Premier Breeder and Premier Exhibitor of Heifer Show:
Ken Main and Peter Vail, Copake, NY 
Yearling Heifer in Milk:
1. Brown Heaven Biver Frenchfries, Brown Heaven, Verchères, Qué.
2. McCall Braiden Windy-ET, Ken Main and Peter Vail, Copake, N.Y. 
Junior Two-Year-Old Cow:
1. IE Twinkle-Hill Bush Jengas, John & Dee Winkelman, Watertown, Wis.
2. New View TP Tessa, Lucas Ayars, Mechanicsburg, Ohio 
Senior Two-Year-Old Cow:
1. TCC Durham Dolly-OCS, Bethany Bauer, Hayfield, Minn.
2. Red Brae La Jongleur Showy, Elise Bleck, Muscoda, Wis.
Junior Three-Year-Old Cow:
1. Cutting Edge B Sonny, Ken Main and Peter Vail, Copake, N.Y.
2. VB HP Bush Minty, Jeremy & Kelsi Mayer, Monroe, Wis. 
Senior Three-Year-Old Cow:
1. Cutting Edge Thunder Faye, Ken Main and Peter Vail, Copake, N.Y.
2. Peach Kist Total Tango-ET, Lindsey Rucks, Okeechobee, Fld. 
Intermediate Champion Female:
Cutting Edge Thunder Faye, Ken Main and Peter Vail, Copake, N.Y. 
Reserve Intermediate Champion Female:
Peach Kist Total Tango-ET, Lindsey Rucks, Okeechobee, Fld. 
Four-Year-Old Cow:
1. Top Acres Garbro S Wish-ET, Larry & Jennifer Meyer, Chilton, Wis.
2. Cutting Edge T Delilah, Kyle Barton, Ancramdale, N.Y. 
Five-Year-Old Cow:
1. R N R LeBron Emily, Ronald & Renee Michalovich, Lakeville, Ohio
2. Whitland Trump Snowup-ET, Justin Whitney, Fort Ann, N.Y. 
Aged Cow, Six-Year-Old & Over:
1. Kar-Linn Wondermnt Reese-ET, Kar-Linn Swiss and Jeff Brown, Jackson Center, Ohio
2. Random Luck B Tea Rose, Allison Thompson, Darlington, Wis. 
Component Merit:
1. Top Acres Supreme Wizard-ET, Wayne E. Sliker, St. Paris, Ohio
2. Jonlee Secret Langwathby, Dalton, Dillon & Breanne Freeman, Bremen, Ind. 
Best Three Females:
1. Ken Main and Peter Vail, Copake, N.Y.
2. Ronald & Renee Michalovich, Lakeville, Ohio 
Produce of Dam:
1. Random Luck-Richard Thompson, Darlington, Wis.
2. Armbruster Bros. Farms, Inc./Red Brae Farms, Muscoda, Wis. 
Total Performance Winner:
Jonlee Secret Langwathby, Dalton, Dillon & Breanne Freeman, Bremen, Ind. 
 
Nasco International Type & Production Award:
Jonlee Secret Langwathby, Dalton, Dillon & Breanne Freeman, Bremen, Ind. 
 
Senior Champion Female:
Top Acres Supreme Wizard-ET, Wayne E. Sliker, St. Paris, Ohio 
Reserve Senior Champion Female:
R N R LeBron Emily, Ronald & Renee Michalovich, Lakeville, Ohio 
Grand Champion Female:
Top Acres Supreme Wizard-ET, Wayne E. Sliker, St. Paris, Ohio 
Reserve Grand Champion Female:
Cutting Edge Thunder Faye, Ken Main and Peter Vail, Copake, N.Y. 
State Herd:
1. Wisconsin Brown Swiss Breeders
2. Ohio Brown Swiss Breeders 
Premier Breeder and Premier Exhibitor:
Ken Main and Peter Vail, Copake N.Y. 
Premier Sire:
Blessing Jex Braiden-ET 
For over five decades, the global dairy industry has been meeting in Madison, Wis. for World Dairy Expo. Crowds of nearly 75,000 people from more than 100 countries attended the annual event in 2016. WDE will return Oct. 3-7, 2017 as attendees and exhibitors are encouraged to “Discover New Dairy Worlds.” Visit worlddairyexpo.com or follow us on Facebook and Twitter (@WDExpo or #WDE2017) for more information.