The U.S. Department of Agriculture’s National Institute of Food and Agriculture (NIFA) has announced grants to help fight citrus greening disease.
The funding has been made available by the emergency Citrus Disease Research and Extension Program, authorized as part of the 2014 Farm Bill, according to a news release.
“The need to advance research and extension to develop management strategies for huanglongbing (citrus greening disease) has reached a critical juncture,” NIFA director Sonny Ramaswamy said in the release. “Severe damage to the Florida citrus crop from 2017 hurricanes further exacerbates the pressure on the industry and the need for new strategies to address the disease.”
The USDA said Florida’s citrus industry has lost nearly half of its $1.5 billion on-tree fruit value in just 10 years due to citrus greening disease.
The release said the Citrus Disease Research and Extension Program supports research and extension activities to fight citrus diseases and pests, including citrus greening, and the Asian citrus psyllid — the vector for the disease.
Grants include a University of Florida project that will develop an Asian citrus psyllid detection systems, hydroponics and sensor-based irrigation, integrated pest management with biocontrol, and economic analysis to support citrus trees grown under protective screen structures. 
The release said grants also include an Agriculture Research Service project that will develop citrus fruiting hybrids that are tolerant or resistant to citrus greening disease.
Five grants totaling nearly $17 million were awarded for fiscal year 2017:

University of Arizona, $3.8 million;
University of Florida, $3.6 million;
University of Florida, $3.5 million;
University of Florida, $2.95 million; and
 Agricultural Research Service, $2.9 million.

Additional project details can be found on the USDA’s National Institute of Food and Agriculture website.