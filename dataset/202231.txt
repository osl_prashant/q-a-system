U.S. corn and soybean exports are soaring, thanks to South American weather woes that are hurting crops in Brazil and Argentina, analysts say.USDA figures show corn exports are up 70%above the 5-year average, while soybean exports have risen 27%higher, Allendale, Inc., noted on Friday.
The figures included weekly sales of2,601,867 MT (2,060,847 current plus 541,020 new 2017/18). This exceeded trade expectations that hoveredbetween 1.6 and2.1 million, Allendale says.
South American weather woes have pushed up U.S. grain exports, according to Mike Zuzolo, of Global Commodity Analytics in Atchison, Kan.
"The U.S. has taken the vast majority of the marketshare away from Brazil, especially in corn," he says."Brazil's corn stocks are so low, they are even importing higher levels of U.S. ethanol."
Although China is probably not taking much U.S. corn, Zuzolo says other Asian countries that normally buy from South America are turning to U.S. corn stocks to meet their demand, especially South Korea and Japan.



<!--
LimelightPlayerUtil.initEmbed('limelight_player_606547');
//-->

Want more video news? Watch it on MyFarmTV.