Nutrition is a major building block of the mushroom industry’s marketing efforts, said Amy Wood, spokeswoman for the Redwood Shores, Calif.-based Mushroom Council.“The Mushroom Council is actively working with influencers such as retail dietitians and sports nutrition experts to educate them about mushroom nutrition, in order to build consumer awareness and understanding about the many health benefits of mushrooms,” she said.
The council points out that mushrooms are a source of an under-consumed nutrient: vitamin D.
One of the key marketing initiatives for Watsonville, Calif.-based Monterey Mushrooms Inc. focuses on the product’s vitamin D content, said Mike O’Brien, vice president of sales and marketing.
“They are the only produce item with natural vitamin D, important not only for bones, but essential for a healthy immune system,” he said.
Mushrooms also provide B vitamins such as niacin, riboflavin, pantothenic acid, and minerals, including copper and selenium, which functions as an antioxidant.
Marketers are working with retailers to ensure health-minded consumers are getting the message about mushrooms, said Joe Caldwell, president of Temple, Pa.-based grower-shipper Giorgio Fresh Co.
“Research and sales trends tell us that the health benefits of mushrooms are a prime motivation to purchase the product,” he said.
“We work closely with our retailers to give them the knowledge and tools to promote the multiple health benefits of mushrooms."
The term “blendetarian” is gaining use in the mushroom business’ health lexicon, said Fred Recchiuti, marketing director with Basciani Mushroom Farms.
“That’s what we’re trying to do with the ‘blendetarian’ website and the articles that are being written in Better Health, Men’s Health,” he said.
“There’s a lot of articles being written about the health benefits.”
Many of those stories focus on the health benefits of The Blend, said Ed Wuensch, salesman with Gonzales, Texas-based Kitchen Pride Mushroom Farms Inc.
“We have to keep telling the story of all the benefits, health and otherwise, that the blend offers,” he said. “We as individual farms, and as individuals need to continue to support the Mushroom Council’s efforts however we can — especially through engagement on social media.”
Kennett Square-based Oakshire Mushroom Farm, which markets under the Dole brand, promotes the nutritional assets of its mushrooms on its packages, said Brian Kiniry, president.
“I think one of the things we’ve been doing for I guess the last five years is try to promote some of that on our Dole label — potassium, selenium,” he said. Packaging is a key conduit for nutritional informational, agreed Fletcher Street, sales and marketing director with Olympia, Wash.-based Ostrom Mushroom Farms.
“That’s the most cost-effective way,” she said.