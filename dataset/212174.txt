Los Alamitos, Calif.-based Frieda’s Inc. is gearing up for summer with McCormick & Co.’s “5 Hot Ideas for Summer.”McCormick & Co., Sparks Md., released its 2017 Flavor Forecast, which includes new grilling trends using potatoes, Korean barbeque flavors and a wide range of glazes.
“These grilling trends resonate with shoppers seeking new food experiences for summer grilling, and they can go to the produce department for it,” Alex Jackson Berkley of Frieda’s Specialty Produce, said in a news release. “Retailers can get creative with in-store displays and signage, suggesting new ways to use produce for summer grilling,” Berkley said.
A few of Frieda’s merchandising suggestions include:
Adding peppers with differing heat levels from mild to hot;
Displaying assorted baby and fingerling potatoes with russets and Okinawan sweet potatoes with orange sweet potatoes; and
Adding to an Asian vegetable display with nappa cabbage, daikon and Shanghai bok choy.