BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Thursday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMar      120.00  120.10  117.65  117.65  Down 2.30May                              120.95  Down 2.25May      121.20  121.50  117.50  118.75  Down 2.30Jul      123.30  123.55  119.75  120.95  Down 2.25Sep      125.60  125.70  122.00  123.20  Down 2.15Dec      128.90  129.00  125.45  126.60  Down 2.10Mar      132.50  132.50  129.00  130.15  Down 2.05May      135.00  135.00  131.70  132.50  Down 2.00Jul      138.00  138.00  133.75  134.60  Down 1.90Sep      139.80  139.80  136.00  136.55  Down 1.85Dec      140.85  141.00  139.10  139.30  Down 1.70Mar                              141.95  Down 1.70May                              143.70  Down 1.70Jul                              145.40  Down 1.70Sep                              147.00  Down 1.70Dec                              149.40  Down 1.70