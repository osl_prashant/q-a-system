Freedom-loving cow no longer roaming with bison in Poland
Freedom-loving cow no longer roaming with bison in Poland

The Associated Press

WARSAW, Poland




WARSAW, Poland (AP) — A Polish scientist says a cow that drew international headlines while roaming free much of the winter with a herd of bison in eastern Poland has apparently been captured and removed from the herd.
The reddish brown, freedom-loving cow had been spotted in January following bison across fields bordering the Bialowieza Forest.
Rafal Kowalczyk, a bison expert with the Polish Academy of Sciences who photographed the unusual sight, said Wednesday that he knows from witnesses that the cow was immobilized and captured some weeks ago, apparently by a farmer.
Previously, Kowalczyk had described the case as exceptional but risky. Mating would have been dangerous to the cow if she gave birth to a large hybrid calf, and also bad for the gene pool of Poland's endangered bison population.