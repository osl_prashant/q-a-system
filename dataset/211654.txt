Marketers say retailers look for the consumer sweet spot in packaging for Argentinean blueberries. 
 
Price and convenience are two prime factors in pack sizes.
 
“We may start to see more bigger packs, rather than small packs,” said Luciano Fiszman, blueberry category manager with Los Angeles-based Gourmet Trading Co. 
 
“Argentina, years ago, was a region where 4.4-ounce was the predominant format. This is now a special pack, with 6-ounce being the main pack style.” 
 
Pints likely will be popping up on more store shelves, Fiszman said.
 
“We will see more pints than in years past, probably,” he said. 
 
Eighteen-ounce packs also are available, but such a big pack often depends on the condition of the fruit, Fiszman said.
 
“We are the specialist on this big pack, as we learned how to pack and deliver a good box of 18-ounce quality fruit,” he said.
 
Watsonville, Calif.-based California Giant Berry Farms is planning for mostly 6-ounce and pint packs for its blueberries from Argentina, said Cindy Jewell, marketing director.
 
A pack size often depends on the retail customer, or where it is located, said Richard Doyle, director of business development for retail at Miami-based Crystal Valley Foods.
 
“Retailers prefer pints, except for California. They only stick to 6-ouncers,” he said. “They’re set in their ways.”
 
Club stores tend to prefer bigger packaging, Doyle said.
 
“They want at least pints, if not 2 pounds,” he said.
 
Pricing often determines pack size, said Eric Crawford, president of Fort Lauderdale, Fla.-based Fresh Results LLC, with small packs more common when prices are high. 
 
“As volume picks up and the market goes down, the larger packs will increase.”
 
Club stores start typically with 18-ounce packages, Crawford said.
 
Most Argentinean production will move in 6- or 18-ounce packages, Crawford said.
 
The key is to know and be able to react to what a retail customer wants, said Tom Richardson, vice president of global development at Vernon, Calif., Giumarra International Berry, a division of Los Angeles-based The Giumarra Cos.
 
“All the retailers have different ideas these days, but in general, what we have seen is that retailers today are usually interested in carrying two pack types, and out of Argentina the last several years the two that seem to work well in this window are a 6-ounce and an 18-ounce,” he said.