Riveridge Produce Marketing, Sparta, Mich., added asparagus three years ago and expects to see a good payoff from the decision this year.
"I would guess that we'll do three times as much as last season this year," said Don Armock, president of Riveridge, who declined to comment on acreage.
"We shipped to 26 states last season, from New England to the Rockies."
The asparagus is snapped by hand, rather than cut with a knife at or below ground level, which affects the consumer experience, Armock said.
"The consumer only gets the tenderest part of the asparagus, so there is very minimal or no waste."
In addition to updating its packaging this year to make it more colorful and attractive, Riveridge is expanding its packaging offerings.
The asparagus will still be offered in 11- and 28-pound cases of 1-pound bundles, as well as new 12-ounce microwave pouches.
"The pouches are a convenient new product," said Trish Taylor, account manager for Riveridge Produce Marketing. "Instead of having to wash and trim the asparagus, you just trim the corner and microwave it for a few minutes."
Harvest will begin the first of May and run through the end of June.