AP Photos: The faces of Cuba's tobacco industry
AP Photos: The faces of Cuba's tobacco industry

By RAMON ESPINOSAAssociated Press
The Associated Press

SAN LUIS, Cuba




SAN LUIS, Cuba (AP) — One is a weathered tobacco picker who harvests bright-green leaves in the fields of Pinar del Rio province. Another is a Havana resident who selects the best leaves with elaborately manicured hands, her delicate nails decorated with acrylic swoops of color and tiny beads.
They are among the many faces of Cuba's tobacco industry, which is a rare bright spot in the island's struggling economy. Increased yields and better quality tobacco made 2017 one of the best ever for Cuban farmers and cigar producers. The country saw more than $500 million in revenue, 12 percent more than the previous year, according to government and company officials.
The process starts in the tobacco fields of western Cuba, where pickers like Jorge Luis Leon Becerra harvest the leaves and move them by oxen to the high-ceiling drying house, where female workers thread them and hang them for the first in a series of drying and curing sessions. Among the latter is Delma Mendivez Martinez, who began working at the Martinez tobacco farm just over six decades ago, when she was 14.
The farm is run by Roberto Armas Valdes, who took control when his father-in-law died five years ago. He says running a tobacco farm "is a very hard and complicated job but this year has been very good for tobacco."
After the private Martinez farm, the tobacco goes to a state-run warehouse in the town of San Luis, where workers like Juan Hernando Regalado Rosales shake the leaves to dry them further as soon as they arrive. Afterward, Luis Miguel Vergara and other "dryers" put the leaves in a dark room for more drying that last anywhere from 45 to 90 days.
From San Luis, the tobacco is sent to Havana, where experts like Milagros Suarez Tamayo choose the best colors and textures for specific lines of cigars, which are rolled and packaged at the La Corona factory.
The cigars that leave La Corona include the famed San Cristobal, Montecristo and Romeo and Juliet Churchill lines, which can sell for upward of $700 a box in stores and clubs around the world.