Kansas groups push school finance constitutional amendment
Kansas groups push school finance constitutional amendment

The Associated Press

TOPEKA, Kan.




TOPEKA, Kan. (AP) — A coalition of business and agricultural groups is proposing a constitutional amendment that would give the Kansas Legislature sole authority to determine adequate funding for the state's schools.
The push comes as lawmakers work to respond to a Kansas Supreme Court ruling that the state continues to underfund its schools after years of litigation and court rulings. The state Constitution requires Kansas to provide a suitable education for every child.
The Lawrence Journal-World reports a group called the Kansas Coalition for Fair Funding says the state needs to take politics out of the education funding controversy and allow Kansas residents to resolve the issue.
The coalition includes associations representing oil and gas, contractors, livestock, convenience stores, along with the Kansas Chamber of Commerce.
___
Information from: Lawrence (Kan.) Journal-World, http://www.ljworld.com