Traders are calling the latest cattle on feed report bearish. Placements in feedlots were 18% higher last year.
Cattle on feed came in even at 10.6 million head.
The U.S. Department of Agriculture also reported a record amount of beef in freezers or cold storage last week.


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.