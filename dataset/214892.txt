Crop calls 
Corn: Steady to 1 cent lower
Soybeans:  3 to 4 cents lower
Wheat: 2 to 4 cents lower
Grain and soybean futures were weaker overnight following Friday’s gains. The softer overnight trade is expected to spill over to the start of daytime trade. A lack of supportive news limits traders’ willingness to be buyers. Historically, trading volume is thin during this holiday-shortened week.
Limited rainfall in drying areas of Argentina over the weekend should help limit selling in the soybean market, though those concerns won’t be enough to offset improving weather in Brazil. Dry areas of central and northeastern Brazil received the best rains of the young growing season last week. More rains are in the near-term outlook for central and northeastern Brazil.
Selling in corn and wheat futures should be limited by the aggressive fund short positions in those markets. Funds are record-short corn.
 
Livestock calls
Cattle: Lower
Hogs: Lower
Cattle futures are expected to face pressure from Friday’s Cattle on Feed Report in which placements topped the average pre-report trade estimate. However, futures were lower ahead of the report, so heavy pressure is not anticipated. Once the report data is factored into the market, focus will turn to cash cattle trade for the week. Typically, packers like to do their buying ahead of Thanksgiving, but there are years when cash negotiations during this holiday-interrupted week have spilled into Friday.
Hog futures are likely to face pressure from the cash market, which is expected to be weaker. But with December futures at a sizable discount to the cash index, selling pressure should be limited in the lead contract. Hog futures may also feel spillover from the cattle market, especially if selling pressure on cattle is heavier than anticipated.