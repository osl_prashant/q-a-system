Milkman accused of sneaking drugs, phones into Ohio prison
Milkman accused of sneaking drugs, phones into Ohio prison

By ANDREW WELSH-HUGGINSAssociated Press
The Associated Press

COLUMBUS, Ohio




COLUMBUS, Ohio (AP) — A longtime milkman delivered more than what the labels on his packages promised, smuggling marijuana, tobacco and cellphones into an Ohio prison hidden inside milk cartons, a prosecutor said.
Ray Adams was in contact with an inmate at Lebanon Correctional Facility who facilitated the deliveries and set up payments, according to Warren County Prosecutor David Fornshell.
Adams, an employee of Martins Ferry-based United Dairy Inc., made thousands of dollars sneaking the items in over time, Fornshell said
On Jan. 8, authorities searched the nearly 30,000 half-pint milk cartons Adams was delivering that day and found contraband, including 12 cellphones, in 30 of them.
Adams, 50, has not yet entered a plea to charges of conveying drugs and cellphones and remains free on bond. His attorney hasn't returned messages seeking comment.
The family-owned United Dairy would never tolerate such activity and Adams was fired immediately, said human resources director Doug Longenette.
"It's just a sad situation all the way around," he said Wednesday. "We hope our employees would think before they did something like that."
Prisons spokeswoman JoEllen Smith declined to comment because of the pending criminal charges against Adams.
Ohio began relying on outside companies to deliver milk to prisons after selling off its dairy cows in 2016. Reducing contraband was one of several factors the prisons director cited at the time in support of the move.
"The department anticipates that phasing out prison farming operations will also minimize the opportunities for passing illegal contraband into our prisons," according to an April 2016 fact sheet from the Department of Rehabilitation and Correction.
Adams has been a driver with United Dairy for 14 years and a prison milk deliveryman for the past two years, Fornshell said.
Investigators believe the contraband scheme was dreamed up last August.
Before making deliveries, Adams would meet a contact at a nearby highway gas station and receive substitute milk cartons containing the contraband before entering the prison, Fornshell said. An inmate took it from there.
"It's not being brought in just to be randomly passed out to whoever — 'Hey it's your lucky day, you're one of 30 winners today,'" Fornshell said.
"Somebody on the inside had to be looking for the milk cartons coming in, knowing how they were going to be marked, knowing what day they were coming in," the prosecutor said.
Fornshell said the investigation was ongoing and there could be additional charges.
___
Andrew Welsh-Huggins can be reached on Twitter at https://twitter.com/awhcolumbus.