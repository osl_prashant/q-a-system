Retailers who want to take their soil nutrient knowledge and agronomic management skills to a higher level of expertise have an exciting opportunity to participate in a free Nutrient Masters program in early June. The two, one-day events are being hosted by Calcium Products and New Leader in partnership with AgPro magazine, and are scheduled for the following dates and locations:Tuesday, June 6                Lincoln, Nebraska     
Wednesday, June 7        Altoona, Iowa 
Click here to view the event agendas and to register!  
Topics and speakers for the Nutrient Masters' events include:
Precision pH Management -- Andrew Hoiberg, Calcium Products vice president, research & development
Steps to Radically Improve the Way Dry Nutrients are Applied -- Craig Fenstermaker, New Leader product manager/specialist
What to Evaluate Mid-Season in Your Customers’ Corn Fields & Management Practices that Maximize Yield Components of Soybeans -- Missy Bauer, Farm Journal associate field agronomist
Retailers will have time during each program to interact with the speakers and have any individual questions addressed. CCA credits are available for both events, and pre-registration is required.