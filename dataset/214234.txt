Editor's Note: The article that follows is part of the 13th edition of The Packer 25 Profiles in Leadership. These reports offer some insight into what drives successful people in produce. Please congratulate these industry members when you see them and tell them to keep up the good work.

“Waste” is a four-letter word to Michael Muzyk. So he’s doing something about it.
Baldor Specialty Foods, Bronx, N.Y., is diverting all organic waste generated in its fresh-cut plant from landfill as part of its SparCs food waste initiative.
The company processes more than 1 million pounds of produce weekly and was discarding an “unacceptable amount” of usable food scraps, said Muzyk, the company’s president.
SparCs — “scraps” spelled backward — is a waste-prevention strategy developed by Baldor’s sustainability director Thomas McQuillan.
Muzyk said the idea was inspired by an owner of a pop-up restaurant in New York that made use of fruit and vegetable trimmings.
“Somewhere along the line, he heard of us, toured our facility and realized our pineapple skins are as safe as the pineapple,” Muzyk said. “He asked if we could supply him with a couple dozen ingredients and his restaurant was a complete success.”
The experience got Muzyk — himself a graduate of the Culinary School of America and a longtime cook — to thinking.
“The No. 1 contributor to landfills in the U.S. is you and me,” he said.
Invention often being second nature to a chef, Muzyk has applied that philosophy to his business.
“I depend a lot on how I learned a lot in the kitchen — it’s an absolute chain of command in the kitchen, and it brings the sense of urgency from the restaurant to the produce business," he said.
Muzyk hired McQuillan as sustainability director and relied on him “to figure out what to do.”
One of the results was the SparCs initiative.
That kind of thinking has permeated Baldor since Muzyk arrived at the company 21 years ago.
“Bringing that sense of urgency from my culinary background to the table got people on board with me,” he said.
Muzyk also aims to anticipate growth areas. The company is expanding into a larger plant, no easy task in New York.
“Today, we’re done with Phase 1 and 20% done with Phase 2," Muzyk said. "Had I waited, it would be too late.”
Muzyk also took Baldor — which traditionally had been a foodservice-only supplier — into the retail realm.
Bryan Patmore, a 16-year Baldor employee who now runs retail sales, said Muzyk’s gift for “seeing around the corner” is crucial to the company’s growth.
“They decided they needed to go in the direction of retail to balance out the portfolio,” Patmore said. “He empowered me. We had done foodservice cuts for high-end restaurants and hotels. He said, ‘How can we apply this to retail side? Take that and run with it.’”