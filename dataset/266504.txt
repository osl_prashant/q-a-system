AP-MN--Minnesota News Digest 6 pm, MN
AP-MN--Minnesota News Digest 6 pm, MN

The Associated Press



Here's a look at AP's general news coverage in Minnesota. Questions about coverage plans go to News Editor Doug Glass at 612-332-2727 or dglass@ap.org. Jeff Baenen is on the desk, to be followed by Gretchen Ehlke at 6 a.m.
This information is not for publication or broadcast, and these plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
AROUND THE STATE:
CROP REPORT
DES MOINES, Iowa — Corn has been dethroned as the king of crops as farmers report they intend to plant more soybeans than corn for the first time in 35 years. The U.S. Department of Agriculture says in its annual prospective planting report released Thursday that farmers intend to plant 89 million acres (36 million hectares) in soybeans and 88 million acres (35.6 million hectares) in corn. By David Pitt. SENT: 590 words, photos.
With:
CROP REPORT-MINNESOTA
BOAT LIABILITY INSURANCE
ST. PAUL, Minn. — A Twin Cities woman who lost her lower left leg in an accident on the family's boat is advocating for a change in state law after she learning she was excluded from her husband's insurance policy on the boat. SENT: 340 words, photo.
IN BRIEF:
OLD ABDUCTION-MINNESOTA, INDUSTRIAL EXPLOSION-MINNESOTA, FATAL OD-CHARGES, MINNESOTA LEGISLATURE-LEGAL FEES, CENSUS-CITIZENSHIP-MINNESOTA, SCHOOL SAFETY, SUPERINTENDENT-EXPOSURE CHARGES, PARTY BUS KILLING, SOLAR JOBS-FLORIDA
SPORTS:
TWINS-ORIOLES
BALTIMORE — Center fielder Adam Jones plays in his 11th consecutive opening day for the Baltimore Orioles, and right-handed starter Jake Odorizzi makes his debut with the Minnesota Twins when the teams launch their 2018 season at Camden Yards. By David Ginsburg. UPCOMING: 650 words, photos. Game starts at 2:05 p.m. CT.
STARS-WILD
ST. PAUL, Minn. — The Minnesota Wild try to move closer to a spot in the Western Conference playoffs, while the Dallas Stars are barely hanging on in the race. The two Central Division rivals meet for the first of two games in three days. By Dave Campbell.
MINNESOTA-MOTZKO
MINNEAPOLIS — Bob Motzko is introduced as the new men's hockey coach at Minnesota, where he's been tasked with restoring the program's place among the sport's elite programs. By Dave Campbell. UPCOMING: 500 words, photos.
IN BRIEF:
VIKINGS-SHERELS
___
If you have stories of regional or statewide interest, please email them to apminneapolis@ap.org. If you have photos of regional or statewide interest, please send them to the AP in New York via FTP or email (statephotos@ap.org). Be sure to call to follow up on emailed photos: 800-845-8450, ext. 1900. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.