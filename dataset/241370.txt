Milk Production: Milk production in the 23 major states during January totaled 17.3 billion pounds, up 1.8% from January 2017. Production per cow for January averaged 1,979 pounds. The number of milk cows was 8.74 million head, up 4,000 head from December 2017. December production was revised up 0.1% to 17.0 billion pounds. For milk production in the top 5 producing states, California was up 2.2% from January a year ago; Wisconsin up 0.4%; Idaho up 2.1%; New York down 3.3%; and Texas up 5.8%. Other states with large production increases were Colorado (10.4%), Utah (8.0%), New Mexico (5.0%), Iowa (3.7%), and Kansas (3.4%). The states reporting a large decline in production compared to January 2017 were Vermont (-2.1) and Florida (-1.3%).
Milk Price and Utilization: The Southeast Uniform milk price for January was $18.00, down $1.04 from December and $2.27 lower than January 2017. The Appalachian Uniform milk price was $17.53, down $0.84 from December and $2.53 lower than January 2017. January’s Class III price was $14.00, down $1.44 from December, and $2.77 lower than January a year ago. The Class IV price was down $0.38 from December to $13.13, and $3.06 lower than January 2017. The Class I Mover price for March is $13.36, down $0.89 from February. The milk/feed ratio for January was 2.19, 0.19 lower than December.
Southeast Class I utilization was 71.54%, up 1.25% from December, and 1.78% higher than January a year ago. The Uniform butterfat price was $2.4823, down 4.46 cents from last month and 1.27 cents higher than January 2016. The January Class I price was $19.24. February Class I price is $18.05. Appalachian Class I utilization was 71.13%, up 5.54% from December, and 1.75% lower than January a year ago. The Uniform butterfat price was $2.4816, down 4.29 cents from last month and 1.17 cents higher than January 2017. The January Class I price was $18.84.
Cheese Production and Stocks: Total cheese production in December was 1.09 billion pounds, up 2.6% from a year ago while butter production was up 4.2% to 170 million pounds. Nonfat dry milk (NDM) production was up 5.4% compared to a year ago with total production at 163 million pounds while skim milk powder (SMP) production was down 8.8% to 49.9 million pounds compared to a year ago. Total cheese stocks at the end of January were up 7% from January year ago and down slightly from the previous month while butter stocks were up 33% from last month and up 1% from a year ago.
Springer Prices and Cow Slaughter: At Smiths Grove, Kentucky on February 27, supreme springers were not tested while US approved springers were not tested. Dairy cow slaughter in January was 289,800 head, up 42,500 head from December and 20,800 more than January 2017.




Southeast Federal Order Prices




Month


Uniform Price $/cwt.


Class I Price $/cwt.


Class III Price $/cwt.


Class IV Price $/cwt.


Class I
% Utilization


Butterfat Price $/lb.




Aug 17


20.02


20.52


16.57


16.61


77.22


3.0111




Sep 17


19.61


20.51


16.36


15.86


76.55


2.9466




Oct 17


19.12


20.24


16.69


14.85


74.40


2.7619




Nov 17


19.07


20.21


16.88


13.99


74.89


2.6246




Dec 17


19.04


20.68


15.44


13.51


70.29


2.5269




Jan 18


18.00


19.24


14.00


13.13


71.54


2.4823




 




Appalachian Federal Order Prices




Month


Uniform Price $/cwt.


Class I Price $/cwt.


Class III Price $/cwt.


Class IV Price $/cwt.


Class I
% Utilization


Butterfat Price $/lb.




Aug 17


19.53


20.12


16.57


16.61


71.62


3.0120




Sep 17


19.06


20.11


16.36


15.86


72.07


2.9404




Oct 17


18.59


19.84


16.69


14.85


71.23


2.7563




Nov 17


18.63


19.81


16.88


13.99


73.95


2.6227




Dec 17


18.37


20.28


15.44


13.51


65.59


2.5245




Jan 18


17.53


18.84


14.00


13.13


71.13


2.4816




 
What is the Market Offering for Milk to be Sold in May?
Ex: It is Feb. 28 and May. Class III milk futures are trading at $14.23. Local May. basis estimate is +$3.00.




If May futures


=


15.75


14.25


12.75




and actual blend price


=


18.75


17.25


15.75




Sample Strategies


 


 


Realized Prices for May Milk




1) Sold Futures

 

@


14.23

 

17.23


17.23


17.23




2) Bought Put


13.23


@


0.09

 

18.66


17.16


16.16




3) Bought Put


13.75


@


0.21

 

18.54


17.04


16.54




4) Bought Put


14.25


@


0.41

 

18.34


16.84


16.84




5) Synthetic Put

 
 
 
 
 
 
 



     Sold Futures

 

@


14.23

 
 
 
 



     Bought Call


15.25


@


0.13


 


17.60


17.10


17.10




 
What is the Market Offering for Milk to be Sold in June?
Ex: It is Feb. 28 and Jun. Class III milk futures are trading at $14.77. Local Jun. basis estimate is +$3.00.




If June futures


=


16.25


14.75


13.25




and actual blend price


=


19.25


17.75


16.25




Sample Strategies


 


 


Realized Prices for Jun. Milk




1) Sold Futures

 

@


14.77

 

17.77


17.77


17.02




2) Bought Put


13.75


@


0.15

 

19.10


17.60


16.60




3) Bought Put


14.25


@


0.29

 

18.96


17.46


16.96




4) Bought Put


14.75


@


0.51

 

18.74


17.24


17.24




5) Synthetic Put

 
 
 
 
 
 
 



     Sold Futures

 

@


14.77

 
 
 
 



     Bought Call


15.75


@


0.20


 


18.07


17.57


17.57