Spring calving in North Dakota reaches one-third complete
Spring calving in North Dakota reaches one-third complete

The Associated Press

FARGO, N.D.




FARGO, N.D. (AP) — Spring calving in North Dakota is one-third complete.
The federal Agriculture Department says in its monthly crop report that cattle and calf conditions are rated 80 percent good to excellent. Cattle and calf death loss is mostly average to light.
Eighty-eight percent of the state's winter wheat crop is rated fair, good or excellent.
Soil moisture remains a concern, however, with only about half of the subsoil and topsoil moisture supplies rated adequate to surplus.