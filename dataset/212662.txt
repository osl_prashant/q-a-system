Our top stories for the week of May 6 - May 12 were:Avocado prices are soaring;
Analysts examine possibility of Whole Foods sale and Whole Foods doubles down on investment in price; and
USDA declares Georgia, Florida disaster areas after freeze.