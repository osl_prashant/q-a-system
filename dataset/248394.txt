College awarded federal grant to study Lake Superior bay
College awarded federal grant to study Lake Superior bay

The Associated Press

BAY MILLS, Mich.




BAY MILLS, Mich. (AP) — Bay Mills Community College has received a federal grant to study biodiversity and pollution levels in Lake Superior's Waishkey Bay.
The bay is an important recreational and cultural resource for members of the Bay Mills Indian Community and tourists.
The U.S. Department of Agriculture awarded the college $216,000 for the study. One part will involve taking samples to analyze levels of pharmaceuticals, personal care products, pesticides and micro-plastics in the water.
Another will provide training and surveys to inventory the bay's freshwater mussels, which are susceptible to pollution and can provide information about water quality.
Lake Superior State University's Environmental Analysis Lab and Wayne State University's Lumigen Instrument Center will perform chemical analysis on samples collected.
Bay Mills Indian Community's Biological Services Department will help with training and sample collection.