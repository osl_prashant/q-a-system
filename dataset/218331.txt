The last thing your pesticides or fertilizers touch is the nozzle tip—and the wrong nozzle or a worn nozzle could lead to lost dollars and poor performance.
“Nozzle selection is more critical than it was a few years ago with new herbicides—getting the right nozzle is essential to avoid drift,” explains Tim Stuenkel, Tee Jet global marketing communications manager.
Before jumping in the sprayer this year, make sure you’re using the right nozzles for the job and each and every nozzle is in peak working condition.
“Remember, it’s not a one-size-fits-all solution—sprayers have nozzle bodies that hold three to five nozzles for a reason,” says Will Smart, president of Greenleaf Technology. “If you’re going to buy a half-million dollar sprayer and expensive chemicals you want a nozzle that will provide maximum effectiveness.”
Pick the right nozzle for your specific conditions and product.
“Getting the right nozzle tip is the heart of accurate applications,” says Dave Mulder, John Deere product marketing manager. “First, consider what you’re applying and what your goals are from an efficacy standpoint. Also what does the product label require—some more universal chemicals may provide variability but your target pests, diseases and label rate guidelines matter as well as crop type in choosing the best nozzle.”
Follow these steps from Virginia Tech and nozzle experts to avoid confusion and select the right nozzle.
Read the label. Taking time to check the label keeps you legal from a stewardship standpoint and can narrow down nozzle options, rate capacity, droplet size and spacing.
“Select the proper type or design of nozzle, which is dictated by the mode of action on the products you’ll use,” says Bob Wolf, owner of Wolf Consulting and Research LLC. “The first two things to check for are if you’re seeking drift control like we are with dicamba or more coverage like what you need with glufosinate.”
Define operating conditions. “Applicators need to know what speed they plan on running—we like a range (say 5 mph to 10 mph) versus just saying 10 mph because speed varies,” says Steve Fischer, inside sales rep and precision expert with Fertilizer Dealer Supply of Boonville, Mo. “Also know how many gallons per acre you want to put on.”
Find your nozzle spacing and spray volume (keep within label requirements), which is especially critical to know because it influences drift, coverage, droplet size, acres per tank and product efficacy. From there you’ll need to figure operating pressure and an acceptable droplet size. Many of these decisions will be influenced by the pesticide.
Calculate nozzle discharge. To determine the correct orifice size, consider spray volume, spacing and travel speed. Virginia Tech uses the following equation:
Nozzle discharge (gal. per minute) = (travel speed x nozzle spacing x spray volume) ÷ 5940
where: travel speed = miles per hour
nozzle spacing = inches
spray volume = gal. per acre
Today there are a number of apps or websites you can use to enter this information and determine your nozzle options—if you want to skip the math.
Study options in the nozzle catalog. Review spray patterns (such as flat-fan, cone or flood) to determine what’s best for the job. In some cases, you’ll have several nozzle options. If that’s the case, try to find mid-range discharge rates so the nozzle tips don’t shut off at lower speeds or underapply at faster speeds. This also allows for a range to fine-tune. Greater operating pressure creates smaller droplets, which increases drift potential. If discharge rates aren’t in the catalog you can calculate them with this equation:
psi1 = psi2 x (gpm1/gpm2)2
where: 1= the desired condition
2= the known catalog specifications
Once you narrow down the nozzle size, make sure you select the right spray pattern and droplet size. Bear in mind these decisions play a critical role in drift management.
“For example, at Wilger, the 05 style tip has five droplet sizes to go with it,” says Craig Bartel, sales manager at Wilger in Tennessee. “You can adjust your tip style based on the chemicals going through it—that way even if your sprayer needs size 05 tips you can get what you need to spray fungicide or dicamba, which require different tips.”
Droplet sizes vary from fine to coarse. You’ll get better coverage with fine sprays but be more prone to drift. Coarse droplets reduce your risk of drift and could potentially penetrate the canopy better because of their size and weight.
It’s not just droplet size that comes into play. You need to know what spray pattern will work best for the pesticide or fertilizer you’re applying. Patterns range from flat-fan, the most commonly used for 80% of applications, to more specialized fan patterns, flood and cone-type patterns.
“Application pattern is related to how it covers the plant,” says Dale Kleinschmidt, parts and service location manager for Heartland Ag in Marshall, Mo. “Some liquid fertilizers, for example, recommend a cone for better coverage than a fan spread. Each tip style has its own use.”
Chances are there isn’t one type of nozzle that works for all application jobs on your farm—especially if you’re using the sprayer for a variety of chemicals or fertilizers. Maximize your sprayer and chemical’s performance by using the optimal tip for each application.
“Everyone seems to be getting new tips for dicamba—that’s because the coverage and flow rate are different,” Fischer says. “It’s different than what they used for Liberty, which recommended a better coverage tip than that of Roundup. Most people just try to keep to two or three tips and only change when forced to—that’s not always the best strategy.”
Make sure you have nozzle tips that fit all of your needs—even if that means buying an additional set.
Avoid Excessive Nozzle Wear and Clogged Nozzles
The chemicals, tank mix and ground you run across affect how your nozzles wear and their propensity to clog. Regular evaluations should help you catch any major issues.
“Most chemicals aren’t all that bad. Where you could run into trouble is with dry powder AMS, which really eats things up quickly—it’s like running gritty sand through sprayers and spray tips,” says Steve Fischer, inside sales rep and precision expert at Fertilizer Dealer Supply.
“You can’t use AMS with dicamba, but you use it with Roundup. Atrazine and other gummy chemicals can cause issues with clogging and getting stuck in booms periodically.”
Clogs are easy to recognize, you’ll visually see them blocking spray, even with rate controllers.
“One reason you have to check nozzle spray patterns on conventional sprayers is that rate controllers measure flow rate right before the boom splits to the nozzles—so even if a nozzle is plugged or worn, the sprayer still sees the right gallon per acre,” says Bob Wolf, owner of Wolf Consulting and Research LLC. “We did a test with six blocked nozzles on a sprayer, and the rate control still thought it was right when in some cases nozzles were putting out twice as much while plugged nozzles put out zero.”
Take time to check for wear. With rate-control systems you could still have an accurate rate across sections, but individual nozzles could be off.
“You might not blow your chemical bill because of rate controllers,” says Will Smart, president of Greenleaf technology. “Check for wear that affects the spray pattern because streaks and nonuniform coverage could make the application ineffective.”
As little as 15% overspray could result in $182,000 in extra cost, according to Spraying Systems Company. Their calculations are for a system with a flow of 100 gal. per minute, water cost at $2.75 per 1,000 gal. and chemical cost of $1 per gallon diluted 10:1 on a sprayer operating 2,080 hours annually.
“You want to check spray tips every season at a minimum,” says Tim Stuenkel, Tee Jet global marketing manager. “A midseason check could be beneficial, too, depending on how many acres you run.”
When checking tips, Stuenkel recommends the following steps:

use water to run the check
bring the boom up to typical operating pressure
based on the capacity of the tip, have a container marked in ounces to collect water from the tip
collect water for 30 to 60 seconds
check to see if the flow rate is within range of the manufacturer’s recommendations, which is found in catalogs

Equipment manufacturers also offer calibration tools to check nozzle flow rates, as an alternative.
“Be sure to check multiple tips along the boom for differences in wear,” Stuenkel adds. “Replace nozzles when they are 10% over nominal flow rate.”
Even if flow rate is within range, you’ll still need to observe the spray pattern because wear can degrade the pattern from what you need.
“Most of the time it changes your flow rate, but there is also a chance it’ll affect droplet size and pattern—but that’s often not as big of a change,” says Craig Bartel, Wilger sales manager.




Learn more about each nozzle spray pattern and when and how Virginia Tech experts recommend they’re used.
© Source: Virginia Tech