(CORRECTED) While the U.S. Department of Agriculture won't issue a seal or label for the program, marketers will be able to claim their products are "certified transitional" under a new USDA program designed to help growers make the change from conventional to organically grown.
Any form of the word "organic" will not be allowed on packaging or advertising.
Some fresh marketers express skepticism about the program, and organic industry observers said grain producers may be most interested in using it.
USDA has announced it is accepting applications from organic accredited certifying agents who may be interested in certifying agricultural products as "transitional."
The agency said accredited certifying agents should submit their applications by Feb. 28 for what is being called the National Certified Transitional Program.
USDA's Agricultural Marketing Service Administrator Elanor Starmer said in a news release the program will help more growers making the transition to organic agriculture.
In the program, AMS will use a standard for transitional agricultural products that was developed by the Organic Trade Association, according to the release.
The agency expects to review applications by March.

Industry reservations

Marketing transitional produce to consumers may not have a lot of value, said Chuck Zeutenhorst, general manager of Yakima, Wash.-based FirstFruits Marketing of Washington.
"It could potentially water down (produce) that is fully organic," he said. "We know what organic is in this country, and in our mind, for it to be organic, it has to be fully organic."
Zeutenhorst said there is a cost associated with getting product to the point it can be certified organic, but the value of organic farming is to have fully organic produce. Transitional produce is marketed by FirstFruits as conventional fruit now.
"The value for organic is not when (orchards) are in transition, it is when they are fully accredited as organic fruit," he said. "Transitional is merely the process to get to organic, it is not a smart methodology to create additional value."
For the limited ground going into organic potatoes, the idea of transitional certification may not be helpful, said Dominic Carnazzo, program manager for Wada Farms, Idaho Falls, Idaho.
Some growers leave fields fallow before their ground is certified organic, while other transitional acreage may have a three- to four-year rotation that includes a combination of leaving the ground unplanted or planting grain crops.
Once land is certified organic, the land can be used for potatoes and also be rotated to organic alfalfa, sorghum, greens or other crops.
"The way that we have done it the last couple of years is to go through the necessary steps to gain organic ground and put potatoes right into organic without the need for a transitional label."
Roger Pepperl, director of marketing for Wenatchee, Wash.-based Stemilt Growers, said the company has sold some tree fruit as non-certified transitional fruit.
Several years ago, the company successfully marketed its stone fruit as Artisan Natural during the two years it was in transition to organic.
The fruit was labeled Artisan Organic after the orchards were certified.
"We've had several times in our lines where there are markets for these types of products, but the problem is that as long as there is suitable supply of organic, there is not going to be any market for the transitional (fruit)," he said.
Some new apple varieties could potentially be marketed as transitional, he said. Traditionally, new apple plantings are farmed conventionally for three years to help them build tree structure and then growers can take them organic over the next three years.
"So there is an opportunity for one or two years in a transition program for that fruit, and that may become more a vehicle in the future," he said.
For retailers appealing to organic consumers, Pepperl said there may be a niche for marketing under-supplied organic fruit varieties as certified transitional.
In general, Pepperl said the industry is starting to see the need for bigger retail displays for the rising supply of organic fruit.
For most conventional supermarkets, the abundance of key organic commodities that provide ample promotion opportunities will limit the appeal of certified transitional produce, he said. Minimal ad support for transitional produce makes it difficult to market produce as transitional, he said.

Program details 

The concept for federal oversight of a certified program for transitional acreage has been under development for close to two years, said Nate Lewis, farm policy director for the Organic Trade Association.
The USDA program doesn't address labeling at the consumer level, he said.
Lewis said the fear that the marketing of certified transitional product would siphon demand away from organic is "speculative" at this point.
In response to that concern, however, one of the aspects of the program is a limit to the number of times the same piece of land can get into the program.
Instead of flip-flopping in and out of transitional, the program puts a limit of two times the same piece of land can be certified transitional.
"We definitely acknowledge that threat is there and real, and we have tried to develop backstops to prevent that from happening," he said.
There won't be a consumer-facing seal associated with the program, and Lewis said the OTA will work with retailers, suppliers and certifiers to adopt a market-driven solution to how transitional products should be represented and labeled.
"What we recommended is that individual certifiers can develop their own seal for transitional products but the seal has to be distinct from the one used for organic product and cannot appear on the principal display panel of a product," he said.
Certifiers are expected to be able to enroll their transitional clients this year, which means that some certified transitional grain crops could be harvested this year, Lewis said.
Lewis said OTA believes the certification program will encourage longer-term forward contracting models between buyers and farmers, where buyers will pay extra for transitional crops in order to have guaranteed production once the farm is certified organic.
The certification program will help assure buyers that growers will actually make the transition to organic successfully, he said.
The appeal of the program has mostly been from corn and soybean growers.
"We haven't heard the same level of eagerness from the produce industry," he said. Grain growers face more significant economic barriers in transition years compared to fruit and vegetable growers, he said.
Note on correction: The original article's first sentence was incorrect in what wording could be used in the program.