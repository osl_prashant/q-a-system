Dallas-based DMA Solutions Inc. is offering produce companies the opportunity for a free marketing consulting session, according to a news release.  
Sessions are available leading up to and during the Produce Marketing Association’s Fresh Summit, Oct. 19-21 in New Orleans.
 
Spots are available to help with planning for Fresh Summit, and DMA president and CEO Dan’l Mackey Almy; vice president Megan Zweig; account directors Marci Allen and Mackenzie Wortham; and communications director Beth Atkinson will be available at Fresh Summit to help companies plan for their 2018 marketing campaigns, according to the release.
 
“To get results and use marketing as a lead generator for sales, it takes a unique mindset and more resources than ever before, which is why our team is offering free consultations,” Mackey Almy said in the release.
 
“During these discussions we intend to uncover challenges and provide solutions that are effective and approachable for fresh produce marketers.”
 
To request a consulting session, complete this form.