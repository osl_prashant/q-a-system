Livestock owners increasing their herds must grow demand for meat. Current herd size expands meat supplies and domestic meat consumption to record levels.
With more supply, expect lower prices, University of Missouri economist Scott Brown told the Womack Agricultural Outlook Conference in Columbia.
But that didn't happen as forecast in 2017.
Growing beef demand worked last year. As supply increased, prices continued up. Livestock prices helped total farm income.
"As long as demand outpaces supply, you're going to like being in the livestock business," Brown said. "However, growing supplies can be troubling."
Once you start increasing a cow herd, it takes a long time to turn that around. Beef producers respond quickly to higher prices, adding more cows. They respond slower to lower price signals.
For now, growth continues in all major meat sectors.
To maintain prices, demand is critical, Brown repeated to his audience.
Brown said a basic shift among customers brought the change in demand.
"Taste matters. Consumers found that in beef." It was not so much in pork and chicken.
For beef producers, it is not just steaks. Hamburger has an upscale trend going. Burgers are found in more than fast-food joints, as upscale dining includes hamburgers.
Ground beef prices have gone up 54 percent since 2010. In the same time, pork chop prices barely budged upward.
For pork producers, bacon kept hog prices up.
For poultry producers, chicken wings helped prices.
"Pork producers know they must put flavor back in those lean loins," Brown said.
Looking at feed grain baselines in the 2018 U.S. Baseline Outlook from the MU Food and Agricultural Policy Research Institute (FAPRI), Brown saw bright spots for livestock. Crops also outgrow population growth. That can mean lower feed costs, which helps livestock profit margins.
Exports remain vital to strong livestock prices, Brown said. Livestock producers grow more meat than U.S. consumers eat. World demographics help U.S. producers. Populations grow faster in other countries than in the United States.
As incomes grow in other countries, the first expenditures are for better food. That includes protein in meat grown by U.S. livestock farmers.
China finally allowed the import of U.S. beef. For now, China's consumption tops 10 pounds of beef per person per year. That compares with almost 80 pounds for U.S. consumers.
The export trade potential remains high.
However, baseline projections can change in a day, Brown said. It could happen if the North American Free Trade Agreement (NAFTA) fails.
Or change could be a growing threat of drought. "The U.S. Drought Monitor looks a lot like pre-2012," Brown said. (A severe drought hit in 2012.)
For now, U.S. consumers search for more taste in their meat. That drives prices upward.
For FAPRI's 72-page 2018 U.S. Baseline Outlook, go to fapri.missouri.edu. FAPRI is part of the College of Agriculture, Food and Natural Resources at the University of Missouri, Columbia.