Last month, the USDA released its annual acreage estimate, predicting a 50/50 split between corn and soybean acres at 90 million each.

Some analysts don’t think it’s out of the question for soybeans to steal another 1 to 2 million acres from corn, but that story is changing.

The latest World Agricultural Supply and Demand Estimates from the USDA show that domestic soybean ending stocks have been raised to 555 million bushels because of lower than expected exports, and corn ending stocks have lowered to 2.1 billion bushels.

The positive story in soybeans comes from production problems in South America. For Mike North, president of Commodity Risk Management Group, there could be more of both, especially after the “swing in the last couple of years towards more soybeans.”

“This might be the window to open us back up to more corn and invite some more acres back in,” he said on AgDay.

Hear his full comments and how weather is playing a big role in planting decisions on AgDay above.