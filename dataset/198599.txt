Canada may delay implementing new restrictions on the use of foreign workers as farmers and meat processors warn of severe labor shortages in one of the world's biggest agricultural exporters, the employment minister said.Employers have reported difficulty finding workers despite a 7.1 percent unemployment rate. Many Canadians resist jobs involving manual labor and rural living, a trend that has driven up labor costs and hurt productivity.
Employment Minister MaryAnn Mihychuk told Reuters the Liberal government may delay the July 1 starting date for changes made by the previous Conservative administration, when low-skilled foreign workers can account for no more than 10 percent of an employer's workforce. That is down from 20 percent currently and 30 percent in 2014.
Mihychuk said that timeline may be too ambitious.
"Ten percent has obviously caused a lot of disruption," she said.
The move to 10 percent is part of a "progressive strangulation of the livestock industry" in rural areas, said Ron Davidson, spokesman for Canadian Meat Council, whose members include Cargill Ltd and JBS Canada.
At Sunterra Group, the changes mean some workers would have to leave even though its Albertapork-processing plant runs at only 70 percent capacity, said president Ray Price.
"They're so happy to be here and they've come from difficult environments," he said. "To see them start to go home makes everybody feel sad."
Even though the government may extend the deadline, Mihychuk said employers should try harder to hire Canadian aboriginals, women and youth.
The Liberals loosened foreign-worker restrictions in March for seafood plants. Mihychuk said then she was not convinced other sectors needed the same help.
But other parts of the program are also creating worker turnover.
Highline Mushrooms lost 73 harvesters last spring, mostly because of a government rule change in 2011 that foreigners who work in Canada for four years must then return home for at least four years.
It meant that last year, many Highline mushroom harvesters, including 40 percent of harvesters on one Ontario farm, headed back to Jamaica, Guatemala and Honduras. Some of the crop went to waste because there were not enough workers to pick.
"We take up to a year to train workers and then we know they will have to leave. When someone works for you for four years, they're part of your family," said Highline director of human resources Susan McBride.
The gap between domestic workers and jobs on Canadian farms was 59,000 positions in 2014, of which foreign workers filled three quarters, according to Canadian Agricultural Human Resource Council. The gap will expand to 114,000 jobs by 2025, the council said, adding that vacancies currently cost farms C$1.5 billion ($1.19 billion) in lost sales and production.
Legislator Jason Kenney, who handled the file when the Conservatives held power, said foreign workers' stay should be temporary. Since it is not easy to recruit Canadians for many jobs, he said employers should consider boosting wages and benefits.
Saskatchewan grain farmer Kenton Possberg employs six workers from Europe and South Africa, and restarts his search every time a worker times out. He said many farms already pay more than double minimum wage of C$10.50, but bigger paychecks don't overcome disadvantages such as remote locations.
"If we can't find a Canadian to occupy that role, I don't know why we have to go through (hiring) all over again every year."
David Tharle, a beekeeper from Alberta, where numerous oilfield workers are unemployed, said a former employee recently rebuffed his offer to return, saying he would rather serve fast food.
Maple Leaf Foods Inc is short workers at its Brandon, Manitobapork-processing plant and wants Ottawa to make it easier for foreign workers to become citizens, said Susan Yaeger, senior manager of human resources.
The current system allows some employers to abuse their leverage by keeping workers short-term, said Stan Raper, national coordinator of Agricultural Workers Alliance.
"It's not a humane system," he said.
($1 = 1.2630 Canadian dollars) (With additional reporting by Andrea Hopkins in Toronto; Editing by David Gregorio)