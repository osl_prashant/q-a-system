It's a gray, blah January in much of the country (it sure is as I type this here at Packer Mission Control in the Kansas City area), but spring is right around the corner.
 
Spring, of course, brings many good things: better weather, women in shorts, lamb for Easter.
 
Sadly, a less welcome spring tradition also looms: the Environmental Working Group's annual Dirty Dozen list.
 
The release of EWG's Dirty Dozen, which purports to identify the 12 vegetables and fruits with the most pesticide residues, is the kind of fear-mongering click bait tailor-made to generate lots of media coverage - much of it superficial and inaccurate.
 
One of the produce industry's most compelling critiques of EWG's annual hatchet job is that reports calling into question the safety of fruits and vegetables could cause consumers to doubt the safety of eating fresh produce - with collateral damage affecting the entire category and not just the 12 items on the list.
 
Unfortunately, research suggests those fears have validity.
 
According to a report in The Washington Post, researchers at the Illinois Institute of Technology in Chicago surveyed more than 500 low-income shoppers about organic and conventional fresh produce.
 
The survey found that specifically naming the "Dirty Dozen" made shoppers less likely to buy any fresh produce at all.
 
That smoking gun confirms what groups like the Alliance for Food and Farming have warned against for years.
 
When asked about organic produce, researchers found 61% of participants said they felt the media encouraged them to buy organic fruits and vegetables, which shoppers looking to (or needing to) save a buck can't afford.
 
According to the article, there's a premium of up to 47% more for organic produce.
 
The report says Davis Carl Winter, vice chairman of Food Science and Technology at the University of California, found the Dirty Dozen list lacking scientific credibility.
 
"Foods on the Dirty Dozen list pose no risks to consumers due to the extremely low levels of pesticides actually detected on those foods," Winter told the Post.
 
While pesticide residues may be present, their presence is not enough to cause harm, he said. 
 
Plus EWG's methodology erroneously treats all pesticides as being equally toxic, further undermining its validity.
 
Sadly, these key distinctions don't get much if any airing in a quickie TV news segment promoted along the lines of "Are fruits and vegetables a threat to your family? Tune in at 10."
 
"Actual exposure levels are typically millions of times lower than those that are of health concern," Winter said in the report.
 
Winter continued: "The methodology used to rank produce items on the Dirty Dozen list was seriously flawed as it failed to consider the three most important factors used in authentic risk assessments - the amounts of pesticides found, the amounts of the foods consumed, and the toxicity of the pesticides."
 
It's good to see the record being set straight.
 
As we at The Packer editorialized in response to last April's Dirty Dozen, consumers should follow the Food and Drug Administration's guidelines to thoroughly wash fruits and vegetables - whether conventional or organic - before enjoying them.
 
Fred Wilkinson is managing editor of The Packer. E-mail him at fwilkinson@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.