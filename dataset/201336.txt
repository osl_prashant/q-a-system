Traders rebuilt some premium into corn and soybean futures this week as temps soared and crop ratings declined again, though weekly gains were pared at week's end as rains fell across the north-central Corn Belt and were forecast to move into the eastern Belt over the weekend. Without a dominant weather pattern, traders’ attitudes will continue to shift with changes in the forecast and price action is likely to remain choppy as traders determine how much — if any — weather premium is needed. Winter wheat futures pulled back and are signaling a short-term top is in place. Spring wheat futures firmed, but are well below their early July peak. Focus next week will be on results of the Wheat Quality Council HRS tour.
Pro Farmer Editor Brian Grete provides weekly highlights:



Cattle futures faded slightly in choppy trade ahead of the Cattle on Feed and Cattle Inventory Reports. Hog futures modestly narrowed their big discount to the cash index.
Click here for this week's newsletter.