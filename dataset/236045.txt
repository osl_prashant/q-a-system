NASHVILLE — Watermelon grower-shipper Melon 1 has expanded its partnership with CHEP.
Announced at the annual convention of the National Watermelon Association Feb. 23 in Nashville, Melon 1 will now include on-site pallet storage and international shipments using CHEP pallets.
“This decision shows what kind of a company we think we want to be, and CHEP shares those values,” said Rachel Syngo, director of business development at Melon 1.
She said using and reusing shared pallets reduces waste and carbon emissions, and that fits into the company’s sustainability goals. 
Syngo said nine of the company’s locations will store CHEP pallets. Melon 1, headquartered at the Brooklyn, N.Y., terminal market, has 20 packinghouses and distribution centers in the U.S.
Suzanne Lindsay-Walker, director of sustainability for CHEP parent company Brambles, said CHEP is proud to work with so many family farmers and be able to expand their sustainability goals.