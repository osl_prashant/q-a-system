The National Mango Board will be meeting with industry members to provide mango growers, exporters, importers and handlers with the latest mango information through the summer and fall.These meetings will take place with the top five exporting countries and at the Fairchild International Mango Festival, according to a news release
The extension workshops and outreach meetings enhance communication between the board’s staff and industry members and showcase tools, research, and resources.
The schedule is:
•Los Mochis, Sinaloa, Mexico, May 31–June 1;
•Piura, Peru, at the XIII International Mango Forum, June 8-9;
•Fairchild International Mango Festival,  Miami, July 1-2;
•Guayaquil, Ecuador, July 27-28;
•Petrolina, Brazil, Aug. 9-10;
•Mango Industry Reception at PMA’s Fresh Summit, New Orleans, Oct. 20;
Piura, Peru (Peruvian Mango Growers and Exporters Assn.), Nov. 9-10; and
•Puerto Vallarta, Mexico, Nov. 30–Dec. 1.