Kubota Unveils its “Best Utility Vehicle Yet”
 
Kubota Tractor Corporation enhances its current market standards with the introduction of the RTV-X1120 models, Kubota’s “most well-equipped utility vehicle at the best price among its competition,” the company says. It is designed for commercial customers who use these machines for heavy duty work every day, the company says. The Kubota RTV-X1120 combines “Kubota quality with enhanced power, torque and performance at a starting price point of $13,999 MSRP.”

“The biggest surprise for the marketplace is the technology offered at a competitive price point. The new RTV-X1120 is a well-equipped machine at a highly competitive price point,” says Roger Gifford, Kubota product manager, utility vehicles. “We are extremely excited about adding these models to our lineup. We are confident that commercial customers will appreciate the six new model options to consider when purchasing a worksite utility vehicle.”

Boost in Power, Torque and Transport Speeds
The RTV-X1120 features a 24.8 gross horsepower diesel engine. The 3-cylinder liquid-cooled diesel engine has a “reputation for providing extra acceleration and climbing power,” the company says. “The engine and VHT-X transmission provide a top speed of 29 mph as well as plenty of hill-climbing power. With the increased speed comes an increased dynamic load, so the RTV-X1120 comes equipped with three-point seatbelts for safety.”
The new RTV-X1120 models will be available at Kubota dealerships in February 2018. For more information visit KubotaUSA.com.
 
TechMix Global Launches New Swine Drying Agent Product 
 
TechMix Global announces the introduction of Dual Dry, an innovative drying agent to support newborn and pre-weaned pigs, the company reports.
“Dual Dry is specifically formulated to dry the environment and the pig itself,” the company says. “The dark color of Dual Dry promotes absorption of radiant heat, which can affect the young pigs’ core body temperature. Applying Dual Dry to mats absorbs moisture, creating a better environment for the caretaker, as well as the baby pig. It produces an environment that reduces the potential for microbial growth and related risk of scouring events.”
Dual Dry is twice as absorbent as other drying agents available, leading to a 30-50% reduction in product usage during day one processing, the company says.
Dual Dry is part of TechMix’s nursery pig program called 3E, which is a protocol for nurturing and developing weaned pigs established by the company’s swine technical consultants. The 3E program centers around three core fundamentals that help ensure pigs thrive through weaning and beyond; environment (keeping them warm and dry), enrichment (providing nutrients through water), and encouragement (palatable supplements that encourage dry feed intake). Baby pigs have limited capability for thermoregulation and the act of drying with Dual Dry warms them immediately after birth reducing chilling and wasted energy, the company says.
For more information, visit: http://www.techmixglobal.com/dual-dry.