#load the hugging face bert model
import numpy as np
import os
import joblib
from nltk.stem.snowball import SnowballStemmer
from transformers import pipeline
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity


def get_top_k_results(filenames_path,nlp, similarity, query,  k = 1):

    top_k = k #select k files based on similarity
    top_file_idx = []
    for i in range(top_k):
        idx = np.argmax(similarity)
        similarity[idx] = -np.inf
        top_file_idx.append(idx)

    for idx in top_file_idx:
        file1 = open(filenames_path[idx],"r+")  
        content = file1.read()
        file1.close()

        result = nlp(question=query, context=content)
        print('Filename:', filenames[idx])
        print(result['answer'])
        for lines in content.splitlines():
            if result['answer'] in lines:
                print(lines)
                break


nlp = pipeline("question-answering")
print('Pipeline loaded....')

filenames = os.listdir('/content/dataset/') #path of dataset
filenames_path = [ '/content/dataset/' + i for i in filenames] # getting an iterable for paths of documents
print('Documents Found....')


stemmer = SnowballStemmer(language='english')
vectorizer = joblib.load('/content/vectroizer.pkl') #load tfidf vectorizer and vectors
X = joblib.load('/content/document_vectors.pkl')
print('Vectorizer loaded....')



flag = 'y'

while flag == 'y':

    query = input('Enter the Query ?')
    query_stem = stemmer.stem(query)
    file2 = open("query.txt","w")
    file2.write(query)
    file2.close()

    query_vector = vectorizer.transform(['/content/query.txt'])
    similarity = cosine_similarity(X, query_vector)

    get_top_k_results(filenames_path,nlp, similarity, query,  k = 1)

    flag = str(input('Another Query? (y/n)'))