The Center for Growing Talent by PMA is investing deeper into young talent with a new November event for college seniors.
The center affiliated with Produce Marketing Association will host an Immersion Academy “Rooted to Succeed” conference for 100 seniors, representing 10 universities, who are studying a variety of majors from marketing to food safety. 
The academy will be Nov. 7-9 in Dallas.
 

Center board chairman Leonard Batti, vice president of business development for Taylor Farms, said the Immersion Academy is a step beyond an earlier program, Career Pathways.
He said that was more for creating awareness for the produce industry, and this is more about preparing students for a career in the industry.
Center vice president of talent portfolio Alicia Calhoun said the program will cover all travel and registration costs for the students. 

She said the event will start with a produce industry overview then focus on the student’s area of emphasis. It will also work on the students’ interview skills, and then have actual interviews with companies representing the entire produce supply chain.
Calhoun said more than 20 companies have already signed up for the program, and Taylor Farms CEO Bruce Taylor will be in the education program.
Immersion Academy expands on PMA’s successful Career Pathways student attraction programs. Created by Jay Pack and PMA, the original Pack Family Career Pathways program brought the first group of students to Fresh Summit in 2004. 
More details on Immersion Academy can be found at www.growingtalentbypma.org.