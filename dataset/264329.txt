Talks of another biofuel summit are continuing to ripple across Washington D.C.

According to Jim Weisemeyer, political analyst for Farm Journal, five Midwestern Republicans are sending a letter to President Trump to ask to not consider changes that could harm the Renewable Fuel Standard (RFS).

Last week during an exclusive interview, AgDay host Clinton Griffiths spoke with EPA Administrator Scott Pruitt about putting a cap on Renewable Identification Numbers (RINs) and Secretary Sonny Perdue about help solving the transparency issue.