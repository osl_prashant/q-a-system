Washington state phases out Atlantic salmon farming
Washington state phases out Atlantic salmon farming

Associated Press
The Associated Press

SEATTLE




SEATTLE (AP) — Washington state is phasing out marine farming of Atlantic salmon and other nonnative fish under legislation signed by Gov. Jay Inslee.
Inslee signed the measure Thursday but vetoed a section of the bill he said wasn't needed to implement the bill.
Net pens have operated in state waters for several decades but the practice came under heavy criticism after thousands of nonnative fish escaped into waterways last summer.
Inslee has said the risks, while low, are unacceptable given how much money and effort the state spends to restore wild Pacific salmon runs.
The bill sponsored by Rep. Kristine Lytton would end state leases and permits for operations that grow nonnative finfish in state waters when current leases expire in 2022.
It targets Canada's Cooke Aquaculture Pacific, which the state fined $332,000 for the Aug. 19 net pen collapse. Cooke, the largest producer of farmed Atlantic salmon in the U.S., currently holds two leases with the state.