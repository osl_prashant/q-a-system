BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Fri.Thu.No. 2 Soft wheat4.58¾4.69¾No. 1 Yellow soybeans10.24½10.15¾No. 2 Yellow Corn3.62¾e3.66¾eNo. 2 Yellow Corn3.76¾p3.80¾p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted