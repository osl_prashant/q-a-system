Nominations for the 2018 Michigan State University (MSU) Dairy Farmer of the Year Award are currently being accepted by the MSU Department of Animal Science. The MSU Dairy Farmer of the Year Award is the highest honor bestowed by the Animal Science Department to individuals in the dairy industry. It is presented annually to a dairy farmer to recognize the recipients’ ongoing efforts at managing an outstanding dairy operation and strengthening the Michigan dairy industry and the surrounding communities. Recipients are respected by their peers, neighbors and industry as outstanding herd managers and dairy leaders at the local, state and/or national levels.Award nominees may be individuals, partnerships or multi-generation family partnerships. A team made up of faculty members and MSU Extension educators select the honoree on the basis of commitment to their operation and contributions to the dairy industry. The 2018 award will be presented at the Michigan Dairy Industry Awards Banquet on Feb. 9 at the Great Lakes Regional Dairy Conference in Mt. Pleasant. The winner’s portrait will be added to the display featuring previous winners at the MSU Pavilion for Agriculture and Livestock Education.
Nomination forms for the MSU Dairy Farmer of the Year Award are available at the MSU Animal Science website awards section. The deadline to submit nominations is Nov 1. Nominations can be submitted by producers, family members, professionals, agri-business firms or other individuals.
The MSU Department of Animal Science has been recognizing outstanding Michigan dairy farmers since 1958.
Recent honorees are listed below:
2017: Jim and Jack Winkel
2016: Hank Choate
2015: Mike Rasmussen
2014: Geert and Gertie van den Goor
2013: Bruce and Jennifer Lewis
2012: James Reid
2011: The Crandalls- Larry, Gloria, Brad, Monica, Mark and Sara
2010: Earl and Diane Horning
Michigan is fortunate to have progressive dairy producers who have grown the industry by 40 percent over the past 16 years and increased total milk production by 91 percent making Michigan the fifth leading dairy state. At the same time, Michigan has become known for producing high quality milk. Michigan State University is pleased to work with Michigan dairy producers and professionals to solve problems and improve economic performance in sustainable ways. MSU AgBio Research and Extension work with dairy producers and professionals daily throughout the state.
For more information, contact Ted Ferris ferris@msu.edu or visit http://www.msue.msu.edu