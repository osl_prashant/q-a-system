With the new year, beef producers are anxious for the 2018 calf crop. In anticipation of calving season, Kansas State University Animal Sciences and Industry and K-State Research and Extension are planning a series of calving schools in January.
The program will outline the normal processes of calving. A.J. Tarpoff, K-State extension beef veterinarian, explains the goals of the event are to increase knowledge and practical skills, and to increase the number of live calves born if they need assistance.
The schools will also share tips on when and how to intervene to assist the cow and how those times may be different when dealing with young heifers. Presenters will also demonstrate proper use of calving equipment on life-size scale.
“This is an excellent opportunity to ask questions and review the calving process,” Tarpoff adds. “We will discuss timelines on when to access calving cows, and when to call for help if things are not going well.”
Several of the meetings will also cover topics such as cow nutrition during the winter months, and managing cull cows.
Meetings scheduled include:

Wednesday, Jan. 3, 6 p.m. CST, Cloud County Fairgrounds, Concordia, Kansas; RSVP to Washington County Extension Office at 785-325-2121.


Tuesday, Jan. 4, 6 p.m. CST, Oswego Community Center, Oswego, Kansas; RSVP to Cherokee County Extension Office at 620-724-8233 or Wildcat District Extension Office at 620-429-3849.


Tuesday, Jan. 9, 10 a.m. CST, Kansas Wetlands Education Center, Great Bend, Kansas; RSVP to Cottonwood Extension Office at 620-793-1910.


Thursday, Jan. 11, Noon MST, Morton County Civic Center, Elkhart, Kansas; RSVP to Crystal Bashford at 620-697-2558.


Tuesday, Jan. 16, 6:30 p.m., Norton County 4-H Building, Norton, Kansas; RSVP to Twin Creeks Extension at 785-877-5755 (Norton), 785-675-3268 (Hoxie) or 785-475-8121 (Oberlin). Program will cover opportunities for marketing cull cows.


Thursday, Jan. 18, 6 p.m., Alta Vista Baptist Church, 402 Main St., Alta Vista, Kansas; RSVP to Wabaunsee Extension at 785-765-3821.

More information about the Calving Schools is available at KSUBeef.org.