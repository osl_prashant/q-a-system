Western Growers is marking its first decade of its transportation program and celebrating the anniversary by helping feed America's hungry through a Giving Day.
On Aug. 24, the Western Growers Transportation Program is scheduled to turn 10 years old.
On that day, the Irvine, Calif.-based association plans to donate to food banks through a partnership with C.H. Robinson Worldwide Inc., Feeding America and other Western Growers Transportation Program-licensed associations, according to a news release.
The association encourages grower-shippers to contribute to the campaign though financial or product donations.
The Eden Prairie, Minn.-based C.H. Robinson is storing and transporting donations to Feeding America locations, which distribute them to cities throughout the U.S.
C.H. Robinson has already received many contributions from Western Growers' members and associate members throughout the country.
The donations include single pallets to half truckloads of food.
These organizations have contributed:
California Fresh Fruit Association;
Florida Fruit & Vegetable Association;
Fresh Produce Association of the Americas;
Georgia Fruit & Vegetable Growers Association;
Idaho Grower Shippers Association;
National Onion Association; and
Texas International Produce Association;
"Within the industry, we already know how generous and caring farmers are," Western Growers officials said in the release. "Help us show others around the country what we already know about your generosity!"
For more information and to contribute to the program, visit http://bit.ly/2aZ6uLM.