As the corn belt gears up for the push through planting season, many are experiencing widespread rain.
While you’re out of the field, here are some stories to catch up on.
Many are viewing 2018 as a pivotal year for online ag retail. Farmers have never had more options to buy crop protection and other ag inputs online.
With building momentum, more data will be collected via drone this growing season. And, professionals in the industry are getting better at generating value from that data. Read more about how crop consultants are earning business and protecting yields.
Monsanto has announced its investment with a partner for gene editing. Pairwise will work with Monsanto exclusively on corn, soybeans, wheat, cotton and canola crops.
In a three-year, multimillion dollar deal, Granular will provide Planet Labs satellite imagery to users.
Bayer and Gingko Bioworks have named their joint venture—JoynBio, which as its first focus will manufacture microbial products using synthetic biology aiming to reduce the amount of fertilizer crops need.
The game rules haven’t changed. Soil have to be at 50 degrees or above for the first 48 hours after corn is planted to ensure good germination.
Many in the industry are fearing Monarchs may be classified as endangered species. A new report takes aim at dicamba use, saying it leads to habit destruction for the closely watched pollinator.
Studies in the Farm Journal Test Plots have shown how new tools are helping keep nitrates in the field, and out of tile drainage. At the same time, many states are eyeing tighter regulations, such as Minnesota that seemingly has fall-applied nitrogen in its cross hairs.
Researchers at Kansas State University have found a genetic weak link in glyphosate resistance, and they are hoping to learn more in how to exploit it.
While soybeans and cotton are forecasted to gain acreage in 2018, a world record holder for corn yields say he is eyeing 800 bushel.