Florida citrus deals were slowing down in mid-March. Spring citrus consists mostly of grapefruit, tangerines and valencia oranges.
Winter weather was uneventful for citrus. Although the Florida grapefruit season ended early, "tangerines should be good until mid-April," said Al Finch, president of Florida Classic Growers, the marketing arm of the Dundee Growers Association in Dundee, Fla.
"We should see a very good supply of valencias through May."
Jones Potato Farm, Parrish, Fla., grows and sells valencias and grapefruit for fresh and juice markets.
"It's hard for us to compete with California," president Alan Jones said, as Florida weather patterns are not as conducive to growing a "pretty" bagged orange.


HLB's effect

A major challenge for Florida citrus growers continue to be citrus greening, also known as huanglongbing.
"HLB is an ongoing and formidable foe," Jones said.
"It's not so much a question of how to get rid of it, but rather how do we learn to live with it - to manage it."
As reported many times in The Packer, HLB is the most devastating citrus disease worldwide.
It was first detected in Florida in 2005 and has since affected all of Florida's citrus-producing areas, leading to a 75% decline in Florida's $9 billion citrus industry.
Since 2009, the USDA has invested more than $400 million to address citrus greening, including more than $57 million through the Citrus Disease Research and Extension Program since 2014.
Past projects have included research by the University of Florida to help HLB-affected groves recover fruit production, and work at the University of California to develop strategies for creating citrus rootstocks that are immune to HLB.