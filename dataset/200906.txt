As we approach the end of 2016, we want to look back on the top 10 articles from Bovine Vet this year. Read the number fivebelow.
Drovers CattleNetworkrecently received an inquiry from a college student conducting research for a public-health class project. She sent a list of questions regarding the relative merits of finishing cattle on grass versus grain-based rations. Her questions, while somewhat biased, reflect common misperceptions of grain feeing and the kinds of question consumers are asking. For that reason, we have adapted the questions and answers into this article, to serve as possible "talking points" for our readers as you encounter similar questions from the public.
We'll be posting the 13 questions, along with our answers, over the next couple weeks. Here is question 5:
Student
Biologically, how does corn speed up the cattle growth cycle?
Drovers CattleNetwork
Grain is high in starch, which is a good source of dietary energy. Forage, on the other hand is high in fiber and much lower in energy. Cattle, as ruminant animals, can digest cellulose or fiber, converting forage to beef. However, even on a high-quality forage diet, a steer will gain about two pounds per day, compared with about three to four pounds per day on a typical feedlot diet, due to the higher calories in the grain component.
See question 4 from this serieshere.