China’s COFCO Meat Holdings Ltd said on Wednesday that profit more than halved in 2017 as Chinese hog prices slumped, but that it would speed up expansion of its pig production by moving into contract farming.
The company, a subsidiary of China’s state-owned grains-to-property conglomerate COFCO, reported net profit of 444.8 million yuan ($71 million) for the year, down from 951.9 million yuan in 2016. Revenue was 6.96 billion yuan, up 5.2 percent.
Most of the firm’s profit comes from raising pigs, prices of which came under pressure last year as farmers sold off herds to comply with tighter environmental rules.
Large farms that can meet the tough new standards have been expanding rapidly, however. COFCO said it would continue to expand too, despite hog prices recently hitting multi-year lows.
“Although the price factor is unfavourable, the accelerating trend of transformation and upgrading of the pork industry has not been changed,” it said in a stock exchange filing.
The company said it would use contract farmers to help raise its hog output more rapidly, in addition to its own integrated farms.
Last year it produced 2.2 million pigs, a jump of 30 percent on the year. It did not give targets for this year’s production.
It also said it would construct fresh pork factories in North and Central China to “match upstream and downstream production capacity”.
Chinese pig farmers have been moving north as government policy encourages more livestock production closer to grain-growing areas. Processing operations have traditionally been more concentrated in the south, near large population centres.
COFCO currently has three meat processing bases in Jiangsu, Hubei and Guangdong, and is trying to boost revenues from higher value branded pork sold under the Joycome and Maverick brands.
Branded pork sales by weight increased by 65 percent to 33,000 tonnes. Revenue from branded products grew to account for 15.6 percent of the total, up from 13.2 percent a year earlier.
“Although the hog price was weaker during the year, the price of mid- and high-end branded pork product remained high,” the company said.
Revenues from COFCO’s meat import business, which accounts for more than a quarter of the total, fell 5.8 percent to 1.9 billion yuan as lower Chinese hog prices made importing fresh pork less attractive.
Sales from imported beef jumped 46 percent to 815 million yuan as demand among consumers surged.
COFCO Meat shares were down about 3 percent by 0745 GMT. ($1 = 6.2789 Chinese yuan)