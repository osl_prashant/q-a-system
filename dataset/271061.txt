Fats in rations are an important component of any ration. They serve as an energy source for maintenance, milk production, body weight gain and performance.Fats can be defined as compounds that have a high content of long-chain fatty acids (FA) including triglycerides, phospholipids, non-esterified FA, and salts of long-chain FA. Long-chain FA are the richest in energy compared to other fatty acids.
Historically, fat content in feeds has been determined by ether extraction.
In addition to fat, ether may also solubilize plant pigments, esters, and aldehydes. For this reason, the result is called "crude fat" and is reported as Fat, % on analysis reports.
Recently, there has been more interest in total fatty acid (TFA) analysis.
The TFA procedure is specific for fat and eliminates potential contamination from the aforementioned compounds. As such, TFA is a more accurate measure of the true fat content of the feed.
Breaking it down
What do all of these abbreviations stand for and what do they mean nutritionally?
RUFAL - Rumen Unsaturated Fatty Acid Load - This represents the sum of C18:1 oleic, C18:2 linoleic, and C18:3 linolenic fatty acids. These unsaturated FAs have been shown when present in high levels to negatively impact rumen function including decreases in intake, fiber digestion, and potentially milk fat concentration.
Saturated Fatty Acids - These are FAs that have no carbon double bonds, or those that are listed on reports as number:0, like C18:0.
MUFA - Monounsaturated Fatty Acids - These are FAs that have one carbon double bond, or those that are listed on a report as number:1, like C18:1.
PUFA - Polyunsaturated Fatty Acids - These are FAs that have more than one carbon double bond, or those that are listed on a report as number:2 or larger, like C18:2 and C18:3.
Putting it to work
We are interested in looking at measures of FAs versus Crude Fat so we can better monitor energy intakes and the potential for impacting rumen health.
While there is not an established cut point, the current recommendation is to not exceed 3.5% of the total ration dry matter as RUFAL.
Forages make up a large percentage of most lactating dairy rations. In the Forage Lab we have seen corn silage to have the highest RUFAL concentration. The greatest difference between Crude fat and TFA was seen in haylages, 1.97 %. Check with your ration software provider to determine if you should be using Crude Fat or TFA for building diets.