U.S Agriculture Secretary Sonny Perdue said on Friday he hoped farm business with Mexico would not suffer due to President Donald Trump's drive to get a better deal for manufacturers.His remarks came before U.S.-Mexico talks to revamp the 23-year-old North American Free Trade Agreement (NAFTA) were due to begin on Aug. 16 in Washington.
After holding talks with his Mexican counterpart Jose Calzada during his first official trip to Mexico, Perdue said NAFTA had caused problems for some sectors, but not others.
"We understand that the agriculture sectors both in the U.S. and in Mexico have benefited tremendously under the rules of NAFTA. We know, frankly, that U.S. manufacturing has not," he said. "How we reconcile those two will remain to be seen."
In the picturesque colonial-era city of Merida, Perdue said he shared Trump's desire to help U.S. manufacturers.
"We hope we can do it without diminishing the beneficial impact that NAFTA has had on the agricultural sector," he said.
To ward off threats by Trump against its manufacturers, Mexico is looking to diversify away from major U.S. imports such as yellow corn.
But Perdue said U.S. suppliers still had an edge.
"I think while they may talk about (that), that they may act as if they're having conversations, we have a corner store location for their supply," said Perdue, touting competitive and logistical advantages of the U.S. agricultural sector.
Mexico has long relied on U.S. grain suppliers for its growing livestock sector.
"Our goal is, first of all, to do no harm in agriculture," Perdue said of the NAFTA talks.
NAFTA gradually eliminated nearly all tariffs for Mexican, U.S. and Canadian goods and services. But Trump has vowed to dump it if he cannot secure more benefits for the United States, labeling it "the worst trade deal ever" during the campaign.
Mexico says it hopes it to benefit from a new deal.
"We hope we can have better exchange of information to make trade much more fluid and efficient," Mexican agriculture minister Calzada said alongside Perdue. "We also hope that synergies will continue to be generated to do a better job in customs so that there are less obstacles to trade."
The ministers visited a major pork producer on Thursday, and were set to visit port facilities on Friday, to highlight the complementary nature of U.S-Mexico agriculture trade.