Kingsville, Ontario-based Mucci Farms has finished phase two of its 36-acre strawberry-growing greenhouse operation, and phase three is underway with production expected to begin next fall.
Mucci Farms markets the fruit as Smuccies Sweet Strawberries.
Joe Spano, vice president of sales and marketing, said the company has seen steady growth and demand for the berries since partnering with Dutch growers Ton Bastiaansen and Joost van Oers in January.
“Overwhelming is the best way to describe how our Smuccies are being received,” Spano said in a news release.
Mucci Farms plans a ribbon cutting ceremony Oct. 26 to celebrate the first harvest from phase two of the expansion, according to the release.
Phase three will include lit culture technology, allowing the company to grow strawberries in Kingsville during the winter, according to the release.
“As with all of our new greenhouses, the new 24 acres of strawberries will also include the use of diffused glass, which reduces stress on the plants by providing even sunlight,” Bert Mucci, CEO, said in the release.  
“We will continue to use high pressure fogging systems to cool down the greenhouse in the hotter months and also install the swing gutter system, which (allows) for the amount of maximum plants per square meter.”
At the Produce Marketing Association’s Fresh Summit in New Orleans, Mucci Farms won the Kids Choice Award in the Sensory Experience Contest for its Smuccies Layered Strawberry and Mango Dip, according to the release.