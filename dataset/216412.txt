Cutting corporate, business and individual taxes, the first major overhaul of U.S. tax laws since 1986 is being welcomed by the farm and retail interests. 
In a re-vote made necessary by a procedural error, the House passed $1.5 trillion tax package Dec. 20, following Senate action the night before. Republicans in Congress delivered a big win President Donald Trump just before Christmas.
“Stocks and the economy have a long way to go after the Tax Cut Bill is totally understood and appreciated in scope and size,” Trump said in a Tweet Dec. 19.
One retail association immediately expressed enthusiasm for the package.
“Independent grocers across the country can feel more optimistic now that a once-in-a-generation tax reform bill has finally passed Congress and is on its way to the President’s desk for his signature.” Peter Larkin, president and CEO of the National Grocers Association, said in a news release. 
“For years, independent supermarket operators have tried to keep pace with a rapidly-changing marketplace while operating in an industry with high effective tax rates on just one to two percent profit margins,” he said. 
“This new weight lifted off their shoulders will allow stores to invest more in their companies, employees, and communities.”
The American Farm Bureau Federation also found good things in the legislation.
“Farm Bureau believes the tax reform bill that passed Congress today will result in lower taxes for most farmers and ranchers, so we are pleased with the outcome of the year and a half long process to reform the tax code,” said Patricia Wolff, senior director of Congressional relations for the American Farm Bureau Federation.
Wolff said Dec. 20 that the vast majority of farmers pay taxes under the individual tax code. She said the bill reduces tax rates and expands the brackets for pass-through business income. “The fact that the rates are coming down will reduce the tax level on farm profits,” Wolff said. 
For farms that are corporations, the corporate tax rates are being reduced from a top rate of 35% down to 21%.
“Lower tax rates are good news for farm businesses,” she said.
Despite early opposition, Wolff said the tax bill maintained tax code provisions that farmers rate as very valuable, including the ability to deduct the cost of business inputs. 
With the new bill, farmers will immediately be able to deduct more expenses than allowed in the current tax code. 
The bill also preserves the deduction for property taxes paid on farm land, the interest deduction and continues cash accounting.
There is a new deduction for pass-through businesses, allowing growers to take a deduction of 20% of net profit on their farms and operations. 
“It means that farmers and growers will be able to protect up to 20% of their profits from taxation,” she said. 
The estate tax exemption is doubled to $11 million per person.
The estate tax change and other individual tax codes changes do have expiration dates after 2025, so she said Farm Bureau said that will be a concern.
“It is seven years out, so that is a ways away, but the closer we get to that expiration, the more uncertainty that creates for farm management, so Farm Bureau will be working to make the lower rates and the business deduction and the estate tax exemption permanent.”