BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Wed. Apr 04, 2018          USDA Market NewsDaily California EggsPrices are unchanged. Trade sentiment is sharply lower. Retail demand islight to fairly good. Warehouse buying interest is light. Offerings aremoderate to heavy. Supplies are light to moderate. Market activity is slowto moderate.Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                301EXTRA LARGE          337LARGE                335MEDIUM               210Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 04-Apr-18 12:08 PM E MP