Research confirms paths of drift mitigation
Bigger isn’t better, but in this case it’s a whole lot safer. As the spray application industry trends to coarser droplets that offer less coverage but mitigate drift, new field data offers valuable insights into the efficacy of spray tips and hoods. The trial results are particularly timely with three new dicamba herbicides available this year: Engenia, FeXapan and XtendiMax.

In 2015 and 2016, researchers tested the drift prevention efficacy of Redball hooded booms in combination with a variety of droplet sizes. Certainly, hoods deter off-target movement, but to what degree? Greg Kruger, a weed scientist and application specialist with the University of Nebraska, and Dan Reynolds, a weed scientist with Mississippi State University, provided answers with a series of glyphosate-dye applications in soybeans.

Before making comparative 600' passes with hooded and open booms, small mylar cards were placed downwind at intervals between 7' and 192'. Larger mylar cards were placed 240', 292' and 341' downwind. The deposition cards picked up drift in the form of dye-laced glyphosate to mark off-target movement. Tip pressure and rig speed were varied to produce fine, medium, very coarse and ultra coarse droplets. Glyphosate was applied at 10 gal. per acre with an 8-row boom in wind speeds ranging 7.5 to 9.6 mph.

During two years of testing, Kruger and Reynolds found fine spray and medium spray drift is highly reduced by hood presence. Coarse spray drift is also reduced but at a lesser degree. 

For all droplet sizes (fine, medium, very coarse and ultra coarse), the hooded sprayer decreased the amount of particle drift on the deposition cards. However, as droplet size increased, the drift variation between the hooded and open boom decreased, particularly noticeable with coarse droplets.
“There’s a trade-off between target movement and coverage. Dicamba herbicides require coarse sprays, and hooded sprayers can definitely mitigate off-target movement,” Kruger says.

Spray tips were the single biggest factor affecting the drift research. “Far and away, the biggest tool in preventing drip is a proper tip,” Reynolds says. “A hood isn’t a necessity, but it’s an additional means to prevent drift and it provides another level of mitigation, particularly in fields of concern.”

Every tool capable of helping to prevent drift is valuable, but nothing substitutes for common sense.

“A hooded sprayer definitely helps, especially with fine to medium sprays, but it’s not a silver bullet,” Reynolds says. “My biggest concern is a producer who thinks he can put on a hood and spray in heavy wind without regard to what is downwind.”

Kruger and Reynolds will continue the drift study in 2017 by testing dicamba sprayed beneath a hood. 








 


Compared with other particle sizes, ultra coarse droplets reduced drift by nearly 85%—to about 15% relative drift at 7' with an open boom and less than 5% drift with a hood.