(UPDATED COVERAGE, Feb. 23) Wendy Reinhardt Kapsak will succeed Elizabeth Pivonka as president and CEO of the Produce for Better Health Foundation.
 
Pivonka retired Feb. 17 after 26 years at PBH, including 20 years as president and CEO. Reinhardt Kapsak, a registered dietician, joins the Hockessin, Del.-based organization Feb. 21
 

"I could not be more excited to merge my experience in both the food and agriculture sector to promote fruit and vegetable consumption on behalf of the industry," Reinhardt Kapsak said Feb. 23. 
 
Reinhardt Kapsak previously worked for 12 years at the International Food Information Council. In her most recent job, Reinhardt Kapsak was global lead for Food, Nutrition and Health Partnerships at the Monsanto Co. 
 
At the International Food Information Council, she worked with PBH to promote fruit and vegetable consumption in the context of federal dietary guidance recommendations, she said. "That's where my interaction with PBH actually started was promoting PBH and their mission in coordination with dietary guidance recommendations."
 
At Monsanto, Reinhardt Kapsak become familiar with how grower/shippers bring their produce to retail and foodservice consumers. "The experience at Monsanto allowed me to connect the seed to the shopping cart," she said. 
 
Reinhardt Kapsak will be based in St. Louis, she said, though PBH headquarters will remain in Delaware.
 
She said her main goal for the first 90 days as PBH president and CEO is to listen to the staff, stakeholders and the board and understand how to deliver exceptional value to the industry and move the consumption message forward.
 

"Wendy has extensive connections in the food and nutrition community as well as in agriculture, combined with great enthusiasm and a vision for the future," Scott Owens, PBH chairman of the board and vice president of sales for Wonderful Citrus, said in a news release. "We welcome Wendy to PBH and are looking forward to introducing her to everyone at the upcoming PBH annual conference in Scottsdale on April 5-7."
 

Reinhardt Kapsak has attended the PBH annual conference for several years, she said. "I look forward to seeing old friends and seeing new colleagues and beginning the process of getting their feedback on how we can continue to carry the mission forward."