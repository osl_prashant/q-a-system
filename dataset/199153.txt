Currently AABP has one guideline and three position statements that are open for member comment. AABP members can log onto the website and post comments to these documents through May 31, 2016. Once logged on to the AABP website, you can find links for the guidelines/position statements on the home page, or you can use the direct links below (must be logged on to the website).Guidelines available for comment are:
¬? Drug Use for Bovine Practice
Find it in the members section of the AABP website. 
Position statements available for comment are:
¬? AABP Principles of Animal Welfare
¬? Tail Docking
¬? Aminoglycoside Use in Cattle
Find them in the resources section of the AABP website.
Can't remember your login information? Go to www.aabp.org and on the right hand side, click on "Click here to have your login information emailed to you".