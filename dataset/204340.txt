The United States is the largest exporter of food in the world, and the U.S. Agricultural Safety and Health Centers believe that the people who produce our food deserve to go home uninjured each day.The U.S. Ag Centers join agricultural associations, corporations, students, producers and their employees in marking the celebration known as National Agriculture Day, March 15.
"National Agriculture Day serves as a reminder that agriculture is part of us all," said Scott Heiberger, spokesman for the U.S. Ag Centers. "And we want to highlight how important the health and wellbeing of people in agriculture is to our society."
The U.S. Ag Centers are promoting the theme, "Celebrating Safe and Healthy Ag Workers," on this 43nd anniversary of National Agriculture Day. #AgDay2016 #farmsafety #USAGCenters
The Centers are funded by the National Institute for Occupational Safety and Health (NIOSH). Visit the Centers' YouTube channel for new content and fresh ideas about how to stay safe while working in agriculture, forestry and fishing. Nearly 90 videos cover topics such as: personal protective equipment, needlestick prevention, livestock safety, chainsaw safety, tractor and machinery safety, child development, emergency response, grain safety, pesticide safety, heat illness prevention and more.
National Agriculture Day is being hosted by the Agriculture Council of America (ACA). National Ag Day is celebrated in classrooms and communities across the country.
The National Ag Day program encourages every American to:
Understand how food and fiber products are produced.
Appreciate the role agriculture plays in providing safe, abundant and affordable products.
Value the essential role of agriculture in maintaining a strong economy.
Acknowledge and consider career opportunities in the agriculture, food and fiber industry.
For the latest information on National Agriculture Day, go to www.agday.org