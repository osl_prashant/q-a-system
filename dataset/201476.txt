USDA Weekly Grain Export Inspections
			Week Ended August 3, 2017




Corn



Actual (MT)
979,006


Expectations (MT)

700,000-1,050,000



Comments:

Inspections were down slightly from the previous week, but the tally was at the upper end of expectations. Inspections for 2016-17 are up 29.1% from year-ago, compared to 31.4% ahead the previous week. USDA's 2016-17 export forecast of 2.225 billion bu. is up 17.0% above the previous marketing year.




Wheat



Actual (MT)
586,149


Expectations (MT)
400,000-625,000 


Comments:

Inspections rose slightly from the week prior and the figure was within expectations. Inspections are running 16.8% ahead of year-ago early in the marketing year versus 15.0% ahead last week. USDA's export forecast for 2017-18 is at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual (MT)
685,697


Expectations (MMT)
250,000-550,000 


Comments:
Export inspections were up 209,500 MT from the previous week, and exceeded expectations. Inspections for 2016-17 are running 17.0% ahead of year-ago, compared to 16.2% ahead the previous week. USDA's 2016-17 export forecast is at 2.100 billion bu., up 8.1% from year-ago.