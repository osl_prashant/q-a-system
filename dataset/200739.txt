When President Donald Trump handed TransCanada Pipeline Co. a permit for its Keystone XL pipeline last month, he said the company could now build the long-delayed and divisive project "with efficiency and with speed."But Trump and the firm will have to get through Nebraska farmer Art Tanderup first, along with about 90 other landowners in the path of the pipeline.
They are mostly farmers and ranchers, making a last stand against the pipeline - the fate of which now rests with an obscure state regulatory board, the Nebraska Public Service Commission.
The group is fine-tuning an economic argument it hopes will resonate better in this politically conservative state than the environmental concerns that dominated the successful push to block Keystone under former President Barack Obama.
Backed by conservation groups, the Nebraska opponents plan to cast the project as a threat to prime farming and grazing lands - vital to Nebraska's economy - and a foreign company's attempt to seize American private property.
They contend the pipeline will provide mainly temporary jobs that will vanish once construction ends, and limited tax revenues that will decline over time.
They face a considerable challenge. Supporters of the pipeline as economic development include Republican Governor, Pete Ricketts, most of the state's senators, its labor unions and chamber of commerce.
"It's depressing to start again after Obama rejected the pipeline two years ago, but we need keep our coalition energized and strong," said Tanderup, who grows rye, corn and soybeans on his 160-acre property.
Now Tanderup and others are gearing up for another round of battle - on a decidedly more local stage, but with potentially international impact on energy firms and consumers.
The latest Keystone XL showdown underscores the increasingly well-organized and diverse resistance to pipelines nationwide, which now stretches well beyond the environmental movement.
Last year, North Dakota's Standing Rock Sioux, a Native American tribe, galvanized national opposition to the Energy Transfer Partners Dakota Access Pipeline. Another ETP pipeline in Louisiana has drawn protests from flood protection advocates and commercial fishermen.
The Keystone XL pipeline would cut through Tanderup's family farm, near the two-story farmhouse built in the 1920s by his wife Helen's grandfather.
The Tanderups have plastered the walls with aerial photos of three "#NoKXL" crop art installations they staged from 2014 to 2016. Faded signs around the farm still advertise the concert Willie Nelson and Neil Young played here in 2014 to raise money for the protests.
The stakes for the energy industry are high as the Keystone XL combatants focus on Nebraska, especially for Canadian producers that have struggled for decades to move more of that nation's landlocked oil reserves to market. Keystone offers a path to get heavy crude from the Canada oil sands to refiners on the U.S. Gulf Coast equipped to handle it.
TransCanada has route approval in all of the U.S. states the line will cross except Nebraska, where the company says it has been unable to negotiate easements with landowners on about 9 percent of the 300-mile crossing.
So the dispute now falls to Nebraska's five-member utility commission, an elected board with independent authority over TransCanada's proposed route.
The commission has scheduled a public hearing in May, along with a week of testimony by pipeline supporters and opponents in August. Members face a deadline set by state law to take a vote by November.
"Tens of Thousands" of Jobs
TransCanada has said on its website that the pipeline would create "tens of thousands" of jobs and tens of millions in tax dollars for the three states it would cross - Montana, South Dakota and Nebraska.
TransCanada declined to comment in response to Reuters inquiries seeking a more precise number and description of the jobs, including the proportion of them that are temporary - for construction - versus permanent.
Trump has been more specific, saying the project would create 28,000 U.S. jobs. But a 2014 State Department study predicted just 3,900 construction jobs and 35 permanent jobs.
Asked about the discrepancy, White House spokeswoman Kelly Love did not explain where Trump came up with his 28,000 figure, but pointed out that the State Department study also estimates that the pipeline would indirectly create thousands of additional jobs.
The study indicates those jobs would be temporary, including some 16,100 at firms with contracts for goods and services during construction, and another 26,000, depending on how workers from the original jobs spend their wages.
TransCanada estimates that state taxes on the pipeline and pumping stations would total $55.6 million across the three states during the first year.
The firm will pay property taxes on the pumping stations along the route, but not the land. It would pay a different - and lower - "personal property" tax on the pipeline itself, said Brian Jorde, a partner in the Omaha-based law firm Domina Law Group, which represents the opposition.
The personal property taxes, he said, would decline over a seven-year period and eventually disappear.
Trump: 'I'll Call Nebraska'
The Nebraska utilities commission faces tremendous political pressure from well beyond the state it regulates.
"The commissioners know it is game time, and everybody is looking," said Jane Kleeb, Nebraska's Democratic party chair and head of the conservation group Bold Alliance, which is coordinating resistance from the landowners, Native American tribes and environmental groups.
The alliance plans to target the commissioners and their electoral districts with town halls, letter-writing campaigns, and billboards.
During the televised ceremony where Trump awarded the federal permit for the pipeline, he promised to weigh in on the Nebraska debate.
"Nebraska? I'll call Nebraska," he said after TransCanada Chief Executive Russell Girling said the company faced opposition there.
Love, the White House spokeswoman, said she did not know if Trump had called Nebraska officials.
The commission members - one Democrat and four Republicans - have ties to a wide range of conflicting interests in the debate, making it difficult to predict their decision.
According to state filings, one of the commissioners, Democrat Crystal Rhoades, is a member of the Sierra Club - an environmental group opposing the pipeline.
Another, Republican Rod Johnson, has a long history of campaign donations from oil and gas firms.
The others are Republicans with ties to the farming and ranching sectors - including one member that raises cattle in an area near where the pipeline would cross.
All five members declined requests for comment.
Prepping the Witness
TransCanada has been trying since 2008 to build the 1,100-mile line - from Hardisty, Alberta to Steele City, Nebraska, where it would connect to a network feeding the Midwest and Gulf Coast refining regions. The firm had its federal permit application rejected in 2015 by the Obama administration.
Opponents want the pipeline, if not rejected outright, to be re-routed well away from Nebraska's Sandhills region, named for its sandy soil, which overlies one of the largest freshwater aquifers in the United States.
The Ogallala aquifer supplies large-scale crop irrigation and cattle-watering operations.
"It all comes down to water," said Terry Steskal, whose family farm lies in the pipeline's path.
Steskal dug his boot into the ground on his property, kicking up sand to demonstrate his biggest concern about the pipeline. If the pipeline leaks, oil can easily seep through the region's porous soil into the water, which lies near the surface.
TransCanada spokesman Terry Cunha said the company has a good environmental record with its existing Keystone pipeline network in Nebraska, which runs east of the proposed Keystone XL.
The company, however, has reported at least two big pipeline spills in other states since 2011, including some 400 barrels of oil spilled in South Dakota last year.
The Domina Law Group is helping the opposition by preparing the landowners, including the Tanderups and Steskals, for the August hearings, much as they would prepare witnesses for trial.
If the route is approved, Jorde said the firm plans to file legal challenges, potentially challenging TransCanada's right to use eminent domain law to seize property.
Eminent domain allows for the government to expropriate private land in the public interest. But Jorde said he thinks TransCanada would struggle to meet that threshold in Nebraska.
"Some temporary jobs and some taxes is not enough to win the public interest argument," he said.