The Latest: Republicans disagree on fate of mining bill
The Latest: Republicans disagree on fate of mining bill

The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — The Latest on fate of bill benefiting mining company (all times local):
9:15 a.m.
Republican senators are disagreeing over whether a bill that would allow a Georgia mining company to destroy a rare wetlands in western Wisconsin still has a chance of passing.
Senate Majority Leader Scott Fitzgerald tells The Associated Press on Wednesday that the bill is "not dead," but he doesn't know yet if there are enough votes to pass it. Republicans have an 18-14 majority.
His comments contradict those made by fellow Republican Sen. Alberta Darling, who says because of opposition from GOP senators the measure is dead.
The Assembly last week attached an amendment benefiting the Meteor Timber company onto an unrelated bill of Darling's. She says several senators voiced opposition to the amendment and "that will kill my bill."
Meteor Timber wants to build a sand-processing plant to serve the frac sand industry near Interstate 94 in Monroe County.
___
8:41 a.m.
A key Republican says a proposal that would clear the way for a Georgia company to build a $70 million frac sand processing plant in western Wisconsin is dead because of opposition from several state senators.
Sen. Alberta Darling tells The Associated Press on Wednesday that the measure the Assembly passed last week that would allow Meteor Timber to destroy a rare hardwood wetland won't pass the Senate.
The provision benefiting Meteor was attached to an unrelated Darling bill in a move by Assembly Republicans in the closing minutes of their last session day that caught Democrats off guard.
Darling says after a "very, very intense" discussion among Republican senators, it was clear there was too much opposition to pass the bill with the Meteor amendment. Darling says she is disappointed, but the bill is dead.