Form of avian flu found at northeast Texas chicken farm
Form of avian flu found at northeast Texas chicken farm

The Associated Press

SULPHUR SPRINGS, Texas




SULPHUR SPRINGS, Texas (AP) — Texas and federal agriculture authorities are testing birds and poultry in a northeast Texas county after inspectors detected a low-pathogenic form of avian flu at a poultry farm.
The U.S. Department of Agriculture says the H7N1 influenza was found last week during routine surveillance of a commercial breeder's flock of about 24,000 chickens in Hopkins County, about 75 miles (120 kilometers) northeast of Dallas.
Authorities now are testing other poultry within about 6 miles (10 kilometers) of the Hopkins County farm.
Federal officials last week confirmed a similar occurrence at a turkey farm in southwestern Missouri but said it poses no risk to the food chain.
The low-pathogenic flu is different from the high-pathogenic virus that resulted in the loss of nearly 50 million birds in the Midwest in 2015.