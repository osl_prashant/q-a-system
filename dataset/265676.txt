Pint-sized wisecracks paid off for 11 winners of Stemilt Growers’ Snappiest Lil Snapper contest, which asked parents to share funny stories and videos of their kids for a chance to win prizes.
“We reached out to parents through their favorite social channels and asked them to share funny stories about their favorite topic — their kids,” said Brianna Shales, communications manager for Wenatchee, Wash.-based Stemilt, in a news release. 
“This Snappiest Lil Snapper contest helped us build brand recognition through an easy, fun contest that parents were eager to participate in.”
Stemilt chose 11 winners to receive prize packs that included a bag of Lil Snappers Piñata apples, a bento lunch box, an apple corer and a Visa gift card, according to the release. The grand prize-winner’s Visa gift card was $1,000.

Stemilt created a highlight reel of the winning stories and videos and plans to share it on its social media channels, according to the release.
“The video highlight reel is a fun way to finish this contest, which was held to reach both regular Lil Snappers shoppers and those not as familiar with the brand,” Shales said in the release. 
“The contest was the central part of a large effort to boost brand awareness and marketplace availability of these kid-sized fruits.”