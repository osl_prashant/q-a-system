Christmas is a time of reflection, and a time of giving and receiving – whether we deserve and understand the gifts or not. It’s called grace.
This Christmas, we’d like to thank the farmers of this nation, who generously bless us all – friends and strangers alike – with the fruits of their labor.
Farmers’ fields and pastures are why we have great open spaces in this nation and the picturesque countryside most citizens can enjoy with a short drive from their homes.
Because of farmers’ efficiency, we spend less of our incomes on food than citizens of any other nation. And in return, we have the most abundant and safest food supply in the history of mankind.
Unlike most other countries, America has never known widespread hunger thanks to the consistency of our farmers and ranchers. Year in and year out – in good times and bad – they start each season with a seed of hope and a hearty resolve, and it flowers from there.
It’s incredible, actually. Farmers toil in one of the riskiest enterprises on the planet, just to feed and clothe the rest of us.
They compete with foreign treasuries, trade barriers, and highly volatile markets. They never know when Mother Nature will strike and destroy a season’s worth of labor. But even when they lose a crop to freeze or a flood, they are back out the next season to try again.
And their work does more than just put food on our tables and conserve our landscape. It makes our economy hum.
More than 20 percent of the U.S. economy is directly or indirectly tied to the food and agriculture sectors. That’s 43 million jobs, $1.9 trillion in wages, and $894 billion in taxes.
Yet somehow, the policy that underpins it all represents less than 1% of federal spending.
We are truly blessed.
Thank God for farmers, who continue to grace us all.