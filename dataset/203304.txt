Howard Roeder, the former president of Dole Fresh Vegetables, has joined Vineland, N.J.-based Safeway Group as the president of Sunnyside Farms.
Roeder will be involved in all aspects of the business, from sales and marketing to operations and finance, said Safeway executive vice president and owner Sam Tedesco.
Roeder's previous stints at Dole and Irwindale, Calif.-based Ready Pac Foods garnered respect from Safeway, which had worked with him in various capacities for years.
"We have a lot of trust in Howard," Tedesco said.
Safeway has grown substantially since its creation in 2011, and the expectation is that Roeder will help the company continue to grow strategically, Tedesco said.
Roeder's first day with Safeway was Jan. 3.