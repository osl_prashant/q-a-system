Nebraska Extension and Colorado State University Extension will be hosting a series of winter meetings for cattle producers in ten locations across western Nebraska and eastern Colorado. The program is designed to help producers evaluate management practices that could improve their bottom line. Extension Specialists and Educators will discuss a variety of topics including nutritional management to prepare for the breeding season, grazing management principles, implant strategies, lameness in cattle, fly research updates, methods to increase productivity of spring calving systems and more.

Dates and locations include:

• December 13th - Loup City, NE, Community Center, 10:00 a.m. to 2:00 p.m. CT, (308) 745-1518
• January 10th – Arthur, NE, Arthur County Fairgrounds, 5:30 p.m. to 9:30 p.m. MT, (308) 284-6051
• January 11th – Minden, NE, Kearney County Fairgrounds, 10:00 a.m. to 2:00 p.m. CT, (308) 425-6277
• January 11th – Oxford, NE, Mulligan’s Restaurant, 5:30 p.m. to 8:30 p.m. CT, (308) 268-3105
• January 15th – Imperial, NE, Mid Plains Community College Campus, 5:30 p.m. to 9:00 p.m. MT, (308) 882-4731 
• January 16th – Curtis, NE, NCTA Education Center, 5:00 p.m. to 9:00 p.m. CT, (308) 367-4424
• January 17th – Ogallala, NE, Open Range Restaurant, 5:30 p.m. to 9:00 p.m. MT, (308) 284-6051
• January 23rd – Wray, CO, Wray Community Hall, 10:00 a.m. to 3:00 p.m. MT, (719) 346-5571
• January 24th – Culbertson, NE, Hitchcock County Fairgrounds, 5:00 p.m. to 8:30 p.m. CT, (308) 345-3390
• January 25th – Sterling, CO, Sterling Livestock Commission, 1:00 p.m. to 7 p.m. MT, (719) 346-5571

Please register at the local Extension office one week prior to the event for a meal count. Cost to attend varies based on location.