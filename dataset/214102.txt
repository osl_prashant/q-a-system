Words and their perceived meanings can spell boom or doom for marketers.
Navigating word choice, deciding on what to stress when describing a product, draws on the art of inspiration and the science of data.
As the Bees Gees sang, “It’s only words, and words are all I have to take your heart away.”
Words and consumers associations with those words will play a big role in the success of the Arctic apple, just now coming into retail stores in the Midwest in the form of 10-ounce packages of sliced Arctic golden delicious apples. Story, here.
While the company’s packaging encourages consumers to find out more about the biotechnology behind the non-browning apples’ development through a quick-response code, phone number and website, Arctics won’t be labeled as biotech or genetically modified.
Okanagan founder and president Neal Carter recently told me that the company was not trying to differentiate itself by price. He left unsaid the fact that the non-browning Arctic also wouldn’t stake its success on its biotech roots.
“We are trying to differentiate ourself by quality, great taste and preservative free,” he said.
I think Carter is on the right track. Stressing “preservative free” may be a winning message for sliced apples. The notion that the non-browning feature can lead to less food waste also may be worth exploring, if indeed that is the case.
A recent study from the University of Florida found that consumers are confused between foods labeled as “organic” and “non-genetically modified.” In fact, the researchers found that some consumers view the two labels as synonymous.
The study, authored by Brandon McFadden and Jayson Lusk, found that consumers in the survey were willing to pay 35 cents more per pound of apples for those labeled “non-GMO Project” and 40 cents more per pound for those labeled “USDA Organic.”
That finding raises questions.
If a “Non-GMO” label is worth nearly as much to consumers as “organic,” then why wouldn’t every producer simply use that “non” label, which is likely far cheaper to put in place? How can the organic industry raise its perceived value to consumers and draw a line between “organic” and “non-GMO”?
University of Florida researchers also wanted to know how QR codes affect choices for foods labeled as containing GM ingredients.
Study results showed consumers are willing to pay more for genetically modified food if the information is provided by a QR code.
If Arctic apples do find a toehold in the market place, I expect more apple marketers will turn to “non-GMO” to define their apples.
It’s “only” words, but words are all the industry has to win consumers’ hearts.
Tom Karst is The Packer’s national editor. E-mail him at tkarst@farmjournal.com.
What's your take? Leave a comment and tell us your opinion.