The last sale set records for six fall sales of Show-Me-Select heifer replacements. The average price per head was $2,118 on 224 head of bred heifers at F&T Livestock Auction, Palmyra, Dec. 9.
The top individual price of $3,200 per head went to a pen of four Angus cross AI-bred heifers. They came from Keithley-Jackson Farm of Frankford. Ed Jackson has consigned for 21 years. That's from the start of the University of Missouri Extension Show-Me-Select (SMS) replacement heifer program.
"You are seeing super reputations here," said auctioneer Brian Curless at the sale by northeastern Missouri producers. All are enrolled in SMS.
"Some 80 percent of the heifers went to repeat buyers," says Daniel Mallory, New London, regional MU Extension livestock specialist.
"Many of our consignors have been involved 21 years," Mallory says. "They develop reputations."
The largest consignor, with 49 head, was Terry Mudd of Twin Hill Stock Farm, Silex. He has been in 19 years. His average on heifers beat the sale average.
Dave Patterson, MU beef reproduction specialist, says, "Those who buy heifers come back and they bid more. They know what they are getting."
This year, MU Extension sale coordinators saw more new buyers across the state coming to the sales.
The Palmyra sale also had first-time sellers. Brian Evans of Vandalia sold 15 head at an average of $2,140, beating the sale average. "That's unusual for a first-timer," Mallory says.
But Evans' heifers are different. He built a herd of 200 beef cows by buying SMS heifers over the years. His herd has a genetic head start. Until now, he's been a regular buyer. He likes switching to selling.
A young consignor was high-school student Kirby Latimer of Hunnewell. He consigned four head, with a jump start by having cows coming from heifers bought from SMS consignor Dan Moore, Kahoka.
Bidding picked up when pen lots of heifers from Gene and Kim Dryden of Hannibal entered the ring. Their first lot of registered Angus brought $2,400 per head. Next lot went for $2,550 and ultimately a lot hit $2,700 per head.
After the sale, Kim recalled she did not like their first try at timed artificial insemination. It didn't work well. But they learned they could use top sires in the breed with AI. Now they go away pleased.
In all sales throughout the state, heifers with stacked genetics do well. Now heifers rank Tier I or Tier II. Those at the top rank are out of proven sires bred to proven sires.
AI-bred heifers consistently bring premium prices over those for bull-bred heifers. A newer upper class is Show-Me Plus heifers. Those have genomic tests based on DNA. The heifers can be ranked in a breed based on a dollar index.
For all sales, the catalog printed on sale day contains data that is worth premium payments.
Producers wanting to make quality beef, increasingly grading USDA prime, can enroll in Show-Me-Select through their local MU Extension center.
Only heifers wearing ear tags with the trademarked MU SMS logo can be sold as "Show-Me-Select."
"The main economic gain comes to those who build their home-farm herds on MU heifer protocols," Patterson says.
The Show-Me-Select heifer program will be part of the Missouri Cattlemen's Association annual meeting in Columbia, Jan. 5.
Increasingly, out-of-state buyers come to Missouri heifer sales. At Palmyra, 28 of 81 lots went out of state. Those were from Iowa, Illinois and Indiana. Over the years they've gone to 20 states, Patterson says.
Details on the sales and SMS rules are posted at agebb.missouri.edu/select.