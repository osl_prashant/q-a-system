Greenhouse vegetable grower-shipper NatureFresh's education center-on-wheels is on the road for another year.
Leamington, Ontario-based NatureFresh's mobile Greenhouse Education Center, introduced in 2015, is once again making the rounds of schools and retailers in eastern North America, according to a news release.
The center helps educate consumers about how and where their greenhouse tomatoes, bell peppers and cucumbers are grown, Chris Veillon, NatureFresh's marketing director, said in the release.
"Using the Greenhouse Education Center as a living example of how we grow, we have a unique opportunity to connect with consumers," Veillon said. "Our teams spend as much time as needed with the consumer to explain how we grow and the various benefits of eating greenhouse grown tomatoes, bell peppers, and cucumbers."
NatureFresh is tracking the progress of the center on its social media channels under the hashtag #GreenInTheCity.
New at NatureFresh, the company has partnered with Windsor, Ontario-based Walkerville Collegiate to create an informational video explaining greenhouse vegetable growing.
The video has been popular in schools, Veillon said.