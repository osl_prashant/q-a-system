Gone are the days of mainstream grocers bypassing organic produce.
For example, Issaquah, Wash.-based Costco Wholesale claimed to be the top organics grocer in 2015, when it reported $4 billion in organic sales.
They are not alone among traditional retailers, organic suppliers say.
"Most major mainstream retailers are now offering a full selection of staple items in organics, along with their conventional produce - 'twin lining,' said Bruce Turner, national salesman for Wenatchee, Wash.-based Oneonta Starr Ranch Growers.
Category managers are experimenting with integrated vs. separate merchandising of the organics and analyzing the results to optimize sales and profit, Turner said.
He also noted that, according to an emerging trend, when price points are similar between organic and conventional retailers are offering only the organic item.
"Traditional retailers used to have margin challenges due to the high shrink in organics, but as sales, tonnage and consumption have continued the double-digit growth, retailers will continue to allocate more square footage to organic produce," Turner said.
That's common business sense, said Megan Schulz, communications director for Los Angeles-based Giumarra Cos.
"Consumer education and demand for organic produce is increasing. We believe retailers will respond accordingly," Schulz said.
Other marketers voice a similar sentiment.
"As long as consumer demand for organic keeps growing, we would expect that retailers will continue to expand the shelf space dedicated to it," said Samantha Cabaluna, managing director of brand communications for San Juan Bautista, Calif.-based Earthbound Farm, which is owned by WhiteWave Foods Co., Denver.
Salinas, Calif.-based organic and conventional grower-shipper The Nunes Co. is seeing its organic sales to retail customers increase, said Doug Classen, sales manager.
"We have a full line of all our mainstay conventional items, plus a full line of all the bunching items and continue to see the retail side of the business wanting to expand that category," Classen said. "They've been coming at us to service that part of the business, as well."
The way a retailer approaches organic still can vary from chain to chain or even store to store, said Earl Herrick, owner, president and founder of Earl's Organic Produce, a San Francisco wholesaler.
"You have some retailers that their mission is if it's grown and organic, we want it and the more different the better. Obviously, it takes a fair amount of retail space to do that," he said.
Some retailers might offer organics merely as a lure, Herrick said.
"A lot of times you'll trade off making money on an item but you're attracting a clientele that will buy a more expensive bottle of wine or cut of meat," he said.
Savvy retailers know how much organic product their clientele will support and stock accordingly, Herrick said.
"If you have a strong retailer that has a finger on the pulse of the community, they're going to follow demands that are there," he said.
Organic shelf space seems limited only by supplies available, said Rachel Mehdi, organics category manager for Vancouver, British Columbia-based The Oppenheimer Group.
"With this being said, ongoing research and development will surely make this barrier smaller as our knowledge and technology improves," she said.
Shelf space is competitive in every department of the store, but ultimately the items that end up on the shelf - and stay there - are the ones that sell, Mehdi said.
"If organic produce continues to experience double-digit sales growth, it's likely more organic products will find their way to the shelves and in some cases, replace their conventional counterparts," she said.
On the other hand, organic produce suppliers must be careful not to saturate the retail marketplace with product, said Scott Mabs, CEO of Porterville, Calif.-based Homegrown Organic Farms.
Growers and retailers can partner in controlling growth of the category.
"When you grow too fast and that supply-demand curve gets out of whack, it throws the economics of the deal out the window," he said.
For the moment, demand outpaces supply, said Tom Deardorff, president of Oxnard, Calif.-based Deardorff Family Farms.
"We (think) it's going to continue to grow at a steady pace," he said.
Roger Pepperl, marketing director at Wenatchee, Wash.-based Stemilt Growers LLC, said the pace likely would be more dramatic.
"I really think that the entire produce section will convert to 50% within 25 years or less," he said.
Pricing will play a role in the process, said Addie Pobst, organic integrity and logistics coordinator for Mount Vernon, Wash.-based Viva Tierra Organic Inc.
"We'll take it all, but, I guess, at a certain point, we'll get to a place where there's really true parity between organic pricing and conventional pricing, and when we get there it will be up to the consumer to decide, and the retailer would respond to that," she said.
Produce department managers have boosted organic sales by placing products side by side with conventional instead of relegating them to some dark corner, said Gary Wishnatzki, owner of Wish Farms in Plant City, Fla.
"Consumers that go to the organic section are more hard-core organic buyers, whereas shoppers that are crossing over will pick up the organic option if it's right beside the conventional," he said.