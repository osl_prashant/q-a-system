For the first time in 21 years, the American Soybean Association (ASA) has a new face to lead the organization.

On Monday, the ASA announced Ryan Findlay will serve as the CEO, after his predecessor Stephen Censky was confirmed by the U.S. Senate as Deputy Secretary of Agriculture.

As for an ag background, Findlay is from the thumb of Michigan in Caro, and his family farms row crops. He obtained a political science degree from Western Michigan University and an MBA from Northwood University in Midland, Mich.

The last four years, Findlay worked at Syngenta and freedom-to-operate issues that impacr farmers. For seven years, he worked with the Michigan Farm Bureau on two farm bills, international trade, climate change, and regulatory issues.

“Ryan’s background growing up on a farm and working for the Michigan Farm Bureau and for Syngenta give him the right combination of life and work experience that will serve ASA well in the coming years,” said ASA President John Heisdorffer.

“I look forward to building on Steve Censky’s strong legacy as ASA and its state affiliates continue to lead the public debate on key policy issues including farm risk management, international trade and rural infrastructure,” said Findlay.