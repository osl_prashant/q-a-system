(CORRECTED) Dole Food Co. is adding to its chopped salad line with a Greek Chopped Salad Kit.
The new salad is shipping nationwide, according to a news release, growing the Dole chopped kit line to 10, and the overall Dole salad kit offering to 17.
The Greek salad kit has bite-sized romaine lettuce, feta cheese, sweet onion pita chips, herb oregano seasoning and Dole’s Greek vinaigrette dressing.
“The new Dole Chopped Greek Kit represents the next step in two simultaneous consumer salad trends: the growth of Greek and the overwhelming public preference for chopped,” Dole director of marketing Lisa Overman said in the release. “It also allows retail produce managers to build-out their Dole Chopped Salads set with a proven, on-trend flavor that has grown 52% in annual sales from 2010 to 2017.”
The suggested retail price for the Greek salad kit is $3.99, the same as other Dole kits, according to the release.
The Dole chopped salad kit line also includes:

Southwest
Sesame Asian
BBQ Ranch
Sunflower Crunch
Caesar

Note on correction: The article originally had an incorrect suggested retail price.