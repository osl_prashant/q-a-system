Central American Produce has developed a new consumer web page, www.capcofarms.com.In collaboration with various industry sources, the page offers extensive information on how to select and use products the company grows.
The page also has information regarding the company culture and business philosophy. 
“We are aware that a lot of people want to know who is growing their food and what is important to them; we also want people to know how much we love what we do,” Central American Produce President Michael Warren said in a news release.
The page also shares details on the Eat Brighter! Sesame Street character campaign, in which Central American Produce participates. 
“Our goal is to help people lead healthier lives, and believe that this web page helps do just that,” Warren said.
Central American Produce has been in business for more than 40 years.