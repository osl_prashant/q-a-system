What's fresh at McDonald's? The beef in some burgers
What's fresh at McDonald's? The beef in some burgers

By JOSEPH PISANI and TERESA CRAWFORD
The Associated Press

NEW YORK




NEW YORK (AP) — The Quarter Pounder is getting a fresh makeover.
McDonald's said Tuesday that it is serving Quarter Pounders with fresh beef rather than frozen patties at about a quarter of its U.S. restaurants, a switch it first announced about a year ago as it works to appeal to customers who want fresher foods.  It will roll out fresh beef Quarter Pounders to most of its 14,000 U.S. restaurants by May.
The fast-food giant, which has relied on frozen patties since the 1970s, said workers will cook up the fresh beef on a grill when the burger is ordered.
"The result is a hotter, juicier, great tasting burger," said Chris Kempczinski, who oversees McDonald's Corp.'s restaurants in the U.S.
Its pricier "Signature Crafted" burgers, stuffed with guacamole or bacon, will also be made with fresh beef since they use the same sized patty as the Quarter Pounder. The Big Mac and its other burgers, however, will still be made with frozen beef.
Fresh beef has always been used by rival Wendy's, which aired a Super Bowl commercial last month criticizing the "flash frozen" beef at McDonald's. A Wendy's Co. representative gave a frosty response Tuesday, saying that "it's awesome" that McDonald's "is recommitting to using frozen beef on the majority of its hamburgers."
McDonald's, however, has signaled that it may use fresh beef in more burgers. Earlier this year, the Oak Brook, Illinois-based company confirmed that it was testing a fresh beef burger that used a patty that was slightly smaller than the one in the Quarter Pounder, but larger than the one its hamburgers and cheeseburgers.
The change at McDonald's is the latest as it seeks to shed its junk food image. It has removed artificial preservatives from Chicken McNuggets, and made other tweaks, including replacing the apple juice in Happy Meals with one with less sugar.
"Fresh in the mind of the consumer really has a better-for-you connotation," said David Henkes, a senior principal at Technomic, a food industry market research firm. "It certainly has a perception that it's better than frozen."
The company tested the fresh beef Quarter Pounder for about two years in Dallas and Tulsa, Oklahoma. Eight more cities are serving it now, including some restaurants in Atlanta, Miami and Salt Lake City. It'll come to Denver, Houston and other cities over the next month before the nationwide rollout.
McDonald's said the switch is a major change for the company, and has said the rollout takes time because employees need to be trained to safely handle fresh beef and to cook the patties only when ordered.
___
Crawford reported from Oak Brook, Illinois.