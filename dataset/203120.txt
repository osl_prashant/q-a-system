(CORRECTED) There's a glut of grapes coming out of Chile and, combined with fruit from Peru and what is left of the California crop, prices are going down.
Importers are saying prices likely will stay down, at least for the short term.
Even a spate of rainy weather in the Chile's northern growing region hasn't done much to slow down shipments, but it has affected some fruit quality, said Hernan Garces, salesman with Moorestown, N.J.-based Forever Fresh LLC, which represents some Chilean growers.
"They got some rain, so I think the situation is going to get even worse, because you have Peru with excellent quality and you've got Chile coming in with quality that's not going to be good at all, so I think the trend is going to be prices going down and a big gap (in quality) between Peruvian and Chilean stuff."
Of course, there could be exceptions, as far as quality goes, Garces said, but not as far as low prices that could be nearly half of a normal of about $40 a box.
As of Jan. 24, according to the U.S. Department of Agriculture, 18-pound containers of bagged flame seedless grapes from Chile were priced at $20-22 for extra-large; $16-20, large; and $13-15, medium.
A year ago, the same product was $38-40, extra large; $36-38, large; and $34-36, medium.
"It's going to be a tough season for grapes and not going to get any better at least for flames," Garces said.
According to the USDA, as of Jan. 21, 2,355 40,000-pound units of Chilean grapes had come into the Port of Philadelphia, compared to 2,002 units at the same time a year ago.
Karen Brux, director of the San Carlos, Calif.-based Chilean Fresh Fruit Association, said from Nov. 28-Jan. 1, Chile had shipped 138% more grapes to the U.S. than it did a year earlier.

In that period, Chile shipped more than 7.75 million boxes of grapes, where last year, it was 3.26 million.


"It came on strong and came on early," Brux said.


In the new year, weekly shipments have been similar or less than same time last year, taking some pressure off the market, she said.


Brux emphasized it's a prime opportunity for retail table grape promotions, due to the available volumes.


"The table grape market is, indeed, in some disarray with heavy volumes in the storages all along the Delaware River," said Mark Greenberg, president and CEO of Capespan North America, Saint-Laurent, Quebec.

The problem is a function of a few factors, Greenberg said.
"First, California grape producers were able to continue shipping red seedless grapes to their North American retail customers past Christmas and even into January; at the same time, though, the Chile was knocking on the door with a table grape crop that is running much earlier than originally anticipated."
Peru also had been in the market with its crimsons since early November, Greenberg noted.
Chile was early with its crop, too, and has shipped "a lot of fruit" into the U.S. since the end of December, Greenberg said. Retailers, meanwhile, did not switch to imported sources until mid-January.
"By that time, inventory levels had climbed substantially," he said.
Retailers have, since then, made the switch to imported table grapes, Greenberg said.
"But prices on flames from Chile reflect the fact that there remains an inventory that needs to move," he said.
The situation will soon start to reverse itself.
"Flame loadings from Chile have declined and, indeed, the last flames will likely leave Chile this week," Greenberg said Jan. 24.
Inventories are likely to be more under control by the second week of February, and an anticipated slight gap between the end of the Chilean flames and the start of the crimsons will cause prices to bounce back to normal levels, Greenberg said.
"The possible scenario includes a shortage of late table grapes from Chile with higher than normal pricing of crimsons come late-March and into April," he said.
California also has been shipping small quantities of fruit in January, although much of that product typically is bound for export, according to the Fresno-based California Table Grape Commission.
A year ago, California sent its final shipments Jan. 25.
"The same could be true this season, as well, although shipping small quantities into early February is not uncommon," commission spokesman Jeff Klitz said.
Note on correction: The article originally contained incorrect numbers on imported grapes.