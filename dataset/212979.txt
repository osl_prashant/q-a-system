Repair or replace? It can be an agonizing question to ask about farm equipment – especially amid tightened farm budgets.According to Eric Madsen, a senior financial services executive with AgStar Financial Services, it’s absolutely okay to scrutinize decisions surrounding whether to repair or replace farm equipment.
“Just because a piece of equipment needs to be repaired doesn’t mean we automatically send it in to be fixed,” he writes for AgStar’s blog. “It’s important to look back at each individual equipment item on our balance sheet and really ask whether the repair costs have gotten to the point where replacement is justified.”
To that end, farmers should ask the following three questions, according to Madsen:
“Could we adjust our equipment management strategy to become more productive or more efficient at a lower cost?”
“Could more work be done on-farm to become more profitable than outsourcing labor or specialized services?”
“If we have a large equipment line that has a great deal of debt to run our cropping enterprise, do we need to evaluate outsourcing and/or reducing our equipment line to make the operation more cash efficient?”
Read more from AgWeb.