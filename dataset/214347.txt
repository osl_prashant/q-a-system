Dairy cows selectively consume their rations, generally sorting longer particles in favor of finer particles. Feed sorting decreases fiber intake while increasing the consumption of grains and co-products. It also creates instances where cows eat different rations throughout the day. 
Are Your Cows Sorting?
In 2010, researchers from University of Minnesota evaluated ration change over time in 50 Minnesota freestall barns. At each farm, samples were collected from rations fed to high-producing cows. One sample was collected immediately after the TMR was delivered, three additional samples were collected every two to three hours after feed delivery, and the last sample was taken from the accumulated weigh-backs.
Researchers evaluated particle size in the TMR samples using a threesieve Penn State Particle Separator. On average, the researchers found a noticeable change in the percentage of material retained in the top screen from the initial TMR to the weigh-backs showing cows were selecting against long particle size. In addition, fiber content—percent of neutral detergent fiber (NDF)— of the TMR increased throughout the day.
Similar results were obtained in a Canadian survey including 22 freestall herds. On average, the refused ration was higher in the percentage of long particles recovered in the top screen (19.8% versus 33.1%) and physically effective NDF (17% versus 24.5% dry matter) than the average offered ration.
Effects of Sorting on Milk Components
Feed sorting causes fluctuations in rumen fermentation patterns, and can result in reduced ruminal pH and episodes of subclinical ruminal acidosis. A recent study showed the association between sorting behavior and milk production. The researchers evaluated feeding behavior in 28 lactating Holstein cows individually housed in a tiestall barn at the University of Guelph.
Cows sorted against long particles and in favor of short and fine particles. On average, intake of the longest particles was 78%. Milk production of the group was 90.6 lb. per day with 3.81% and 3.30% protein. The authors found negative associations between feed sorting and milk composition. For every 10% increase in sorting against long particles:

Milk fat content decreased by 0.10 percentage units
Milk protein content dropped 0.04 percentage units

Because the average sorting against long particles in the group was 22%, milk fat was reduced by 0.22 percentage units or 0.2 lb. per cow per day due to sorting. Similarly, milk protein was reduced by 0.09 percentage units or 0.08 lb. per cow per day. Using values from September FMMO Advanced Component prices (fat $3.03 per pound and protein $1.54 per pound), the economic impact of sorting in this research herd was 72¢ per cow per day or $263 per year.
In conclusion, feed sorting is a common behavior of dairy cows that could produce health issues and economic losses in the herd.
Fernando Diaz, DVM, Ph.D. is a dairy nutrition and management consultant with Rosecrans Dairy Consulting LLC. You can reach him at fernando@jration.com
 
Note: This story appeared in the November issue of Dairy Herd Management.