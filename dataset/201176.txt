August 2017 natural gas opened today at $3.07 -- up 18 cents from last Friday.
Farm Diesel is 5 cents lower on the week at an average of $1.91 per gallon.
August 2017 WTI crude oil opened the day at $44.89 -- higher $2.09 on the week.
August 2017 Heating oil futures opened the day at $1.44 -- up 7 cents from our last report.
Propane is 1 cent lower at an average of $1.20 per gallon regionally.

Farm Diesel -- Farm diesel was led lower by South Dakota which fell 23 cents per gallon as Nebraska softened 19 cents and Indiana slipped a dime. Six states were unchanged as Missouri firmed 4 cents per gallon, our only state to post gains on the week.
With heating oil futures on the rise (see chart at right), buoyed by higher crude oil, we have begun to suspect farm diesel will soon confirm a summer low. In this week's The Energy Report, Phil Flynn posits, "Suddenly oil is turning around as it starts to pay attention to bullish news. With the quarter ending and signs that US oil production may be stalling out, the complexion on the recent bear run move has run its course. As everyone falls all over themselves to lower their crude price forecasts, it is probably a sign that this market will begin a significant turnaround." (click here to read the article)
While some still believe crude oil futures will sink to $40 per barrel, an exodus to the long side would quickly pull futures higher, and that is a real near-term possibility. We must also consider summer diving season which has the power to urge all transportation fuels higher.
To the bearish argument, distillate supplies are strong, above year-ago and above the five-year average. We know refinery throughput has been very strong heading in to summer driving season, bolstering national fuel stocks. Spring demand for fieldwork has come and gone with little support for retail diesel, and world crude oil supplies have seen little impact from OPEC's agreement to moderate production.
In an environment where the arguments of bears and bulls both seem plausible, it is prudent to consider managing farm diesel price risk. Since farm diesel tends to lag the futures markets by at least a week -- sometimes as much as 20 days -- we will hold off for now, but be prepared in the coming weeks to cover perhaps as much as half of your expected late summer/fall harvest farm diesel needs.


Distillate inventories reported by EIA softened 0.2 million barrels to 152.3 mmbbl. Stocks are currently 1.8 mmbbl above the same time last year.
The regionwide low currently lies at $1.73 in Nebraska. The Midwest high is at $2.16 in Illinois.





Farm Diesel 6/30/17


Three Weeks Ago


Previous Week


Change


Current Week

 



Iowa


$1.94


$1.91


-3 cents


$1.88

Iowa



Illinois


$2.19


$2.16


Unchanged


$2.16

Illinois



Indiana


$2.10


$2.01


-10 cents


$1.91

Indiana



Ohio


$1.93


$1.90


Unchanged


$1.90

Ohio



Michigan


$1.80


$1.88


Unchanged


$1.88

Michigan



Wisconsin


$1.90


$1.90


Unchanged


$1.90

Wisconsin



Minnesota


$2.04


$2.04


-10 cents


$1.94

Minnesota



North Dakota


$2.04


$2.03


Unchanged


$2.03

North Dakota



South Dakota


$1.98


$1.98


-23 cents


$1.75

South Dakota



Nebraska


$1.91


$1.92


-19 cents


$1.73

Nebraska



Kansas


$1.89


$1.88


Unchanged


$1.88

Kansas



Missouri


$1.91


$1.91


+4 cents


$1.95

Missouri



Midwest Average


$1.97


$1.96


-5 cents


$1.91

Midwest Average



Propane -- Propane prices softened led by Indiana, which fell 7 cents as Ohio fell 5 cents and Michigan and Minnesota each dropped a penny. Seven states were unchanged as Iowa firmed a penny per gallon, our only upward mover this week.
Propane prices have some work to do before we pull the trigger. The trouble is, we do not expect that work to be done. We do believe propane prices will continue lower, but we maintain our belief that the summer low will come in somewhere around $1.10-$1.15 rather than softening to last year's low of 96 cents per gallon. We noted last week that retailers are beginning to send bids to their retail customers. We recommend again this week that you go to the map on FarmJournalPro.com to see what your average local propane bid is. If the number you have been given by your retailer is above that, you may be able to leverage some bargaining power. If the bid your preferred retailer's bid is below that figure, you can be assured you are getting a good deal.
For now, we will wait to see what propane wants to do as supplies build seasonally. Stay patient, but check in with prices in your crop district on the map we provide to better gauge what others in your area are paying.


According to EIA, last week, national propane inventories firmed 3.918 million barrels -- now 23.622 million barrels below the same time last year at 58.451 million barrels.
The regionwide low is at $1.05 per gallon in Nebraska, and the regionwide high is in Missouri at $1.41.





LP 6/30/17


Three Weeks Ago


Previous Week


Change


Current Week

 



Iowa


$1.08


$1.07


+1 cent


$1.08

Iowa



Illinois


$1.39


$1.36


Unchanged


$1.36

Illinois



Indiana


$1.30


$1.23


-7 cents


$1.16

Indiana



Ohio


$1.25


$1.25


-5 cents


$1.20

Ohio



Michigan


$1.43


$1.20


-1 cent


$1.19

Michigan



Wisconsin


$1.43


$1.25


Unchanged


$1.25

Wisconsin



Minnesota


$1.16


$1.14


-1 cent


$1.13

Minnesota



North Dakota


$1.09


$1.09


Unchanged


$1.09

North Dakota



South Dakota


$1.09


$1.09


Unchanged


$1.09

South Dakota



Nebraska


$1.11


$1.05


Unchanged


$1.05

Nebraska



Kansas


$1.38


$1.34


Unchanged


$1.34

Kansas



Missouri


$1.41


$1.41


Unchanged


$1.41

Missouri



Midwest Average


$1.26


$1.21


-1 cent


$1.20

Midwest Average