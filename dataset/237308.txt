Protests planned as feds hold offshore drilling plan meeting
Protests planned as feds hold offshore drilling plan meeting

The Associated Press

PROVIDENCE, R.I.




PROVIDENCE, R.I. (AP) — Protesters are expected to gather in Providence as federal ocean regulators hold a public meeting on the Trump administration's plan to allow drilling off the coast.
The administration has proposed a vast expansion of offshore drilling that would open 90 percent of the nation's offshore reserves to development by private companies. The Bureau of Ocean Energy Management meeting is scheduled for 3 p.m. Wednesday at the Marriott Providence Downtown.
Opponents warn any drilling could hurt the fishing industry, tourism and the environment.
The environmental group Save the Bay plans to hold a Statehouse rally then a march to protest the plan. Democratic Gov. Gina Raimondo (ray-MAHN'-doh) plans to make remarks before the rally.
Another group plans to protest inside the meeting, which they call a "sham hearing."