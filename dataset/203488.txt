Heavy rains in Mexico have affected the appearance and movement of citrus exported to the U.S.
The rains have lightened the color of Mexican limes imported by River Edge, N.J.-based Vision Import Group, said Ronnie Cohen, the company's vice president of sales.
Significant volumes of fruit have a "blanched" look, as opposed to the darker green color customers like, Cohen said. The juice content and overall quality, however, have been fine, he said.
Still, U.S. consumers have gotten used to bright-colored fruit.
"We have to have perfection, like it was made in a machine," he said.
Volumes haven't been affected by the rains, Cohen said, with Vision Import reporting similar supplies as last season as of Sept. 6, Cohen said.
And demand also hasn't been affected, thanks to ever-increasing U.S. demand for limes.
"I don't think color hurts demand - it can only improve it," he said. "My suspicion is consumption is up significantly in the past 10 years, and f.o.b.s are up, too."
Supplies were tight at the beginning of September, said Bret Erickson, president and CEO of the Mission-based Texas International Produce Association. But that isn't atypical for that time of year.
"Prices are high, around $20 on all sizes right now and will probably head a little higher before they start to come back down," Erickson said. "Overall I think we've been pretty fortunate to have had a relatively quiet tropical season thus far."
In early September Vision Import Group was transitioning into new-crop limes in Mexico, which were much greener than the old-crop fruit, Cohen said. Fruit was small at the beginning of the deal.

Lemon movement affected
The rains significantly affected lemon volumes in the last three weeks of August for Vero Beach, Fla.-based Seald Sweet International, said Dave Brocksmith, the company's citrus manager.
In the seven days ending Aug. 18, 222 truckloads of Mexican limes crossed into the U.S., Brocksmith said. In a normal year, they would have gone up in the week following, as the Mexican deal ramped up - instead, they fell to 138 loads.
"That should never happen," he said. "They should be picking up steam. Now, I think we'll be playing catch-up for awhile."
As a result, the market was in a demand-exceeds position, Brocksmith said.
Big sizes, in particular, were hard to come by at the beginning of September, he said. Quality wasn't affected, however, Brocksmith said.
Volumes should, however, start to pick up again the week of Sept. 5, with at least 200 loads likely crossing, Brocksmith said.
River Edge's lemon volumes are up this season as of Sept. 5, Cohen said. Unlike limes, there were no quality or appearance issues on lemons thus far caused by the rains.
"There was a pretty good set, and the quality is excellent," he said. "I'd put it up against any California lemon there is."