(Bloomberg) -- Lovers of chalupas and crunch wraps have spoken: Taco Bell is now bigger than Burger King.
The Mexican-themed chain eclipsed its burger rival in U.S. sales last year, becoming the fourth-largest domestic restaurant brand, according to a preliminary report by research firm Technomic. McDonald’s Corp., Starbucks Corp. and Subway Restaurants held on to the top three spots.
Taco Bell’s systemwide sales -- the total sales of restaurants that carry the brand -- jumped 5 percent in the U.S. to about $9.8 billion in 2017. The company, owned by Yum! Brands Inc., has made inroads with indulgent fare, along with $1 items that appeal to budget-strapped millennials.
The ranking change also underscores the surging popularity of Mexican-inspired fare. Last year marked the first time that Taco Bell has overtaken Burger King, the data showed.
Though Burger King has fared better than many restaurants brands in recent years, it hasn’t kept pace with its biggest burger rivals -- McDonald’s and Wendy’s Co. -- or chains like Taco Bell. Its domestic sales rose just 1.5 percent in 2017, according to the Technomic report, which will be finalized in March.
Burger King faces a “resurgent McDonald’s,” David Henkes, senior principal at Technomic, said in an interview. Upscale burger chains, such as Shake Shack Inc., also are threatening its market share.
Taco Bell, meanwhile, has drawn customers with wacky new foods, including fried-chicken taco shells, and a marketing campaign dubbed Live Mas. In January, the chain introduced $1 nacho fries.
“They certainly continue to do pretty well, and bring out some interesting and new menu items,” Henkes said. “They’ve done a good job of connecting with the millennials and Gen Z.”
 
Copyright 2018, Bloomberg News