A hearing has been set for Jan. 24 to consider motions in the multi-million dollar Chapter 11 bankruptcy proceedings of Eclipse Berry Farms LLC and subsidiaries Rosalyn Farms LLC and Harvest Moon Strawberry Farms LLC.  (See related story here.)
The U.S. Bankruptcy Court for the Central District of California, Los Angeles Division, said in a court document filed Jan. 19 that the hearing will take place at 2 p.m. on Jan. 24 before the U.S. Bankruptcy Judge Barry Russell in Los Angeles.
The hearing will consider several motions, including:

Debtors’ motion for joint administration of related cases;
Debtors’ motion for approval of agreed use of cash collateral;
Debtors’ motion for authorization to pay certain health care benefits;
Debtors’ motion for authorization to pay key employee severance payments and contractual severance payments;
Debtors’ motion for assurance of payment for continued utility services and granting related relief: and 
Motion for establishing procedures for the assertion of PACA claims and certain related relief.

A copy of the motions, according to the court document, is available upon written request from debtors’ counsel Kevin Morse of Saul Ewing Arnstein & Lehr LLP, Chicago, (312) 876 7100, or kevin.morse@saul.com.
The court document said that by no later than Jan. 23, 10 a.m. Pacific, oppositions to the motions must be made in writing, filed with the court and served upon the debtors’ counsel and the Office of the U.S. Trustee. If no party responds, according to the document, the court may treat such inaction as consent to the reliefs requested in the motions.
The court said any reply to the oppositions must be made in writing and served to the opposing party no later than Jan. 24 at 10 a.m. Pacific. A judge’s hard copy of any reply filed must be provided to the court by Jan. 24 at noon, according to the document.
 
Winding down
In a separate document filed with the court, Eclipse Berry chief restructuring officer Robert Marcus said the first-day motions will provide an orderly transition of the debtors into the Chapter 11 cases and will ultimately permit the debtors to maximize recoveries for all parties. 
The firm was not shipping any berries as of Jan. 22.
Marcus said that as recently as 2016, Eclipse Berry Farms was one of the largest fresh strawberry shippers in the country, with gross revenues of about $230 million in 2016. At its peak, Eclipse Berry Farms employed more than 3,000 and had a 2,500-acre farming operation that stretched from Southern California to northern California. The firm did not own any of the property it farmed, according to court documents.
Marcus was appointed chief restructuring officer in September after Wells Fargo withdrew its lines of credit in late July and the first half financial results in 2017 revealed in $5-6 million in losses. 
Marcus said the firm completed the 2017 California harvest by mid-November in Santa Maria and by late November in Salinas. Berry operations in Mexico ceased shipments to the U.S. in mid-December. 
As of September 2017, Eclipse Berry Farms had already undertaken substantial efforts toward the 2018 crop season, planning 1,535 acres for 2018. Oxnard acreage was 99% completed by September, with 90% for Santa Maria and 30% completed for Salinas.
Many of the firm’s leases were taken over by West Coast Berry Farms and affiliate Superior Fruit LLC, according to the court document.
As of Jan. 19, Marcus said Eclipse Berry Farms only remains in possession of its Los Angeles headquarters lease and its Oxnard yard lease. The remaining unexpired agricultural leases not assumed by Superior were either turned over or taken over by their respective landlords, Marcus said in a court document.
Marcus said the company had only seven employees when it filed for bankruptcy protection.
With $12 million in cash on hand, claims against the company are more than $50 million. Marcus said there are five lawsuits pending against Eclipse Berry Farms for a variety of collection actions.
Marcus said in the court document that the Chapter 11 filing and first-day motions will “assure the downsizing and liquidation efforts will continue without interruption.”