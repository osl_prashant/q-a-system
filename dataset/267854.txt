Beef cattle feedlot settles with state in air pollution case
Beef cattle feedlot settles with state in air pollution case

The Associated Press

WALLULA, Wash.




WALLULA, Wash. (AP) — A company that operates a beef cattle feedlot in southeastern Washington has reached a settlement with the state over a charge of failing to manage air pollution.
Idaho-based J.R. Simplot Co. agreed to pay a $5,000 fine and spend at least $30,000 to put asphalt on a road to keep trucks from kicking up dry manure and dust at its beef cattle feedlot north of Wallula.
The Washington Department of Ecology initially fined the company $50,000 for allowing small particles into the air. The company unsuccessfully appealed the fine to the state Pollution Control Hearings Board.
"We're happy to reach this resolution," Simplot spokesman Josh Jordan said Friday. "The safety of the community where we work is extremely important to us, so if there are concerns, we want to address that. The road should help significantly."
State officials say the agreement also requires Simplot to update its dust-control plan and improve particle pollution prevention. That will require improved staff training, and using water to control dust from roadways and cattle pens.
State law requires feedlots to keep dust and emissions from blowing onto neighbors by taking reasonable precautions. The Department of Ecology said the company failed to take those precautions between April 1 and June 20 in 2015.
Simplot contended it took appropriate preventive measures to prevent the dust from escaping.
The feedlot has a capacity of 80,000 cattle.