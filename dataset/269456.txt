BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Mon. Apr 16, 2018          USDA Market NewsDaily California EggsBenchmark prices are 71 cents lower for Jumbo, 75 cents lower for ExtraLarge, 79 cents lower for Large and 37 cents lower for Medium and Small.Trade sentiment is steady. Offerings and supplies are moderate. Demand ismostly moderate. Market activity is slow to moderate. Small benchmark price$1.27.Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                212EXTRA LARGE          202LARGE                196MEDIUM               147Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 16-Apr-18 12:42 PM E MP