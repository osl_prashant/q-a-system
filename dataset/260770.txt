After stealing $1.6 billion each year from farmers, soybean cyst nematode’s (SCN) reign is coming to an end. The SCN Coalition is working with farmers to stop the menacing pest and regain an estimated 14 bu. per acre.
“SCN is still the No. 1 soybean yield-reducing pest in North America,” says Greg Tylka, nematologist at Iowa State University and one of the SCN Coalition leaders. “This new educational effort is working to bring [SCN resistance to soybean genetics] to the attention of farmers and convince them to more actively manage SCN.”
“We’ve used the same resistance mechanism for two decades; you wouldn’t do that with herbicides and not expect resistance,” says George Bird, nematologist at Michigan State University and an SCN Coalition leader. “We want to see other forms of resistance made available.”
To manage for this pest the coalition suggests the following:

Test your fields to know your SCN numbers.
Rotate to SCN-resistant varieties such as PI88788 or Peking.
Switch to non-host crops.
Use a seed treatment nematicide.

The SCN coalition is a collaboration of university scientists from 27 states and Ontario as well as Checkoff and industry partners working to provide recommendations. The coalition is encouraging the industry to actively study new forms of SCN management for the future.
For more information about the SCN Coalition and SCN resources, visit www.TheSCNcoalition.com.