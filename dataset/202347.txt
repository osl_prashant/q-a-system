Pork is still seeing high demands across the European Union. The Pig Site reported that the European pig slaughter market's atmosphere was remaining friendly.Prices quoted in Germany showed that the market is trending upward throughout Europe, though price quotes were unchanged in Spain, France and Ireland.

The recent price increase resulted in Denmark leading the price structure of the EU.