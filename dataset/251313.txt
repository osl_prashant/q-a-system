BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Thu. Mar 08, 2018          USDA Market NewsDaily California EggsPrices are unchanged. The undertone is fully steady. Retail and warehousedistributive demand is moderate to fairly good with movement into foodservice channels fairly good to good. Offerings and supplies remain lightto moderate. Market activity is moderate.Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                254EXTRA LARGE          249LARGE                243MEDIUM               190Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 08-Mar-18 11:02 AM E MP