The organic category continues to expand, with many California table grape growers experiencing significant increases in volume.
At Bakersfield, Calif.-based Anthony Vineyards, 80% of its San Joaquin Valley table grape crop now is organic, said partner Bob Bianco.
That figure eventually should reach 100%, he said.
The company started shipping organic grapes about 12 years ago when it acquired an organic ranch.
It took a while to figure out the organic deal and develop a customer base, but the company is “very committed to organic,” Bianco said, and the program “just gets bigger and bigger.”
Anthony Vineyards grows at least 12 varieties of grapes, and has an organic offering for each one.
Porterville, Calif.-based Homegrown Organic Farms has an organic grape program that is “in development and growing,” said Stephen Paul, category director of stone fruit, fall fruit and grapes.
The program kicks off in early to mid-July with scarlet royals and also includes Timco and crimson. It features the France Ranch label, Paul said.
Homegrown also ships some newer varieties, such as Ivory and Kelly.
“We’re trying to build the program with the newer varieties because the newer varieties are exceptionally good,” he said.
Some of the older varieties, like thompsons, crimsons and flames, are being pushed out, Paul said, and new ones that size well and offer lots of crunch and flavor are “taking over in the industry and spilling over into organic.”
About 15% of the grape acreage at Top Brass Marketing Inc., Bakersfield, Calif., is organic, said Brett Dixon, president.
“All categories have increased as our commitment to growth continues,” Dixon said.
“However, the organic category continues to grow in double digits per year to keep up with demand.”
Growing organically is a more complex process than growing conventionally, and yields typically are less, requiring growers to charge a premium for their fruit.
There may be times when prices on organic grapes are equivalent to those on conventional product, but that’s an exception, Dixon said.
“Every year you seem to learn new ways of increasing yields and using new organic supplements to treat the fields for pests,” he said.
Delano-based Sunview Marketing International is one of the largest grower-shippers of organic grapes, which are grown exclusively in California’s San Joaquin Valley, said salesman Charles Alter.
This year, the company is growing Rosa Seedless, scarlet royal, Magenta, Timco and crimson red varieties and sugraone, Stella Bella, princess, Great Green, thompson and autumn king green varieties.
Black grape offerings are summer royal and black seedless.
Sustainability seems to go hand in hand with organic growing.
Top Brass has five acres of solar panels to run its cold storage facilities and recently contracted to add Tesla batteries to enhance that renewable resource even more, Dixon said.
Homegrown Organic Farms monitors its suppliers growing’ and conservation practices, Paul said.