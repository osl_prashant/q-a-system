Every Monday afternoon, these planting progress maps are updated with the latest data. The maps are interactive and available for corn, soybeans and cotton. You can drill down by state or zoom to the areas of interest. Click for more:Corn Planting Progress Map
 
Soybean Planting Progress

 
Cotton Planting Progress Map