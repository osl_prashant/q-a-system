As harvest nears completion farmers, retail and state Extension agents have gotten a better idea of how many acres of soybeans were allegedly damaged by dicamba. Kevin Bradley, with the University of Missouri, compiled the number of reported complaints and estimated acres from state Extension agents.
The final numbers? Approximately 3.6 soybean million acres of damage and 2,708 dicamba-related injury complaints—both up from an Aug. 10 report of 3.1 million acres and 2,242 complaints.
 




 


Extension weed scientists provided their state's estimated acres of damage.


© Kevin Bradley







The Environmental Protection Agency (EPA) recently imposed new label requirements for Engenia, FeXapan and Xtendimax. Changes seek to reduce risk of injury to sensitive and specialty crops and include information about wind speed, tank clean out and changed the label to restricted use, among other changes. States are reviewing the changes and are considering if additional restrictions will be required. For example, Arkansas is close to passing a rule that would limit dicamba application to before April 15.
 




 


Extension weed scientists provided the number of formal complaints within their state.


© Kevin Bradley







Dicamba damage can happen due to a number of factors. Larry Steckel, University of Tennessee Extension weed scientist, breaks it down to “five big issues.” Several of these issues were considered in recent EPA label changes.

Tank contamination 
Following the label
Avoid inversions 
Old formulations 
Volatility in new formations

The product is under a 2-year conditional label provided by EPA. Results from 2017 indicate improvements must be made to avoid the same level of off-target damage and keep the product in farmer hands.