While the USDA lays out its nice and neat prospective acreage numbers in March, a lot can change. This planting season, farmers across the Corn Belt experienced wet weather that caused them to not only plant and replant, but replant the replant.
According to Jim McCormick, senior trading advisor and broker at Allendale, Inc., corn acres could be down 1 to 1.5 million acres.
“We do get some updated WASDE numbers on Friday—they won’t make any acreage adjustments there,” said McCormick. “These abandoned acres that we’re looking for, you’re going to see them show up, but will be more like the September, October WASDE.”
He told AgDay host Clinton Griffiths that the summer months lead to dry weather, and it tends to get the market excited, about 50 to 60 cents in excitement for corn.
“You’ll get corn back up toward the $4.40, $4.50 [range],” he said. “Hopefully that will put the beans back up toward $10.”
Watch McCormick's full comments on AgDay above.