At the recent Penn State Dairy Nutrition Workshop, Kenneth Kalscheur, a prominent dairy nutrition researcher at the U.S. Dairy Forage Research Center in Madison, Wisconsin, provided findings on the subject of “Canola Meal vs. Soybean Meal in Dairy Cow Diets.” The annual workshop provides training to feed-industry professionals, and this year’s focus concentrated on amino acid and protein nutrition for lactation.
“Canola meal provides a cost-favorable source of essential amino acids,” explained Kalscheur.
He pointed out that the U.S. Dairy Forage Research Center, in conjunction with the University of Wisconsin, has been involved in researching the feeding of canola meal as a protein source for lactating dairy cows for quite some time. This research is part of the Canola Agri-Science Cluster, with funding provided by Agriculture and Agri-Food Canada, the Canola Council of Canada, Alberta Canola, SaskCanola and the Manitoba Canola Growers.  
Many trials conducted with mid-lactation cows in the Wisconsin studies showed that substituting canola meal for soybean meal resulted in a 1.25- to 3.0-pound daily milk yield advantage. This is often accompanied by feed cost savings. Results from Kalscheur’s 2016 early-lactation study showed an even greater milk increase in early-lactation cows. The researcher noted that the efficiency of nutrient conversion to milk is improved with the inclusion of canola meal in diets for dairy cows. This often results in lower milk urea nitrogen. Kalscheur emphasized that the superior amino acid balance in canola meal supports its efficient utilization.
Overall Results for the Early-Lactation Feeding Trial Comparing Canola Meal to Soybean Meal




 


Soybean  Meal


Canola Meal




Dry-Matter Intake, lbs/day


55.0


55.9




Milk Yield, lbs/day


112.64


122.43




Energy-Corrected Milk


117.92


126.72




Feed Efficiency, milk/feed


2.17


2.27




Milk Components


 


 




   Fat, %


3.9


3.9




   Protein, %


2.9


2.8




   Fat, lbs/day


4.5


4.8




   Protein, lbs/day


3.2


3.4




   Milk Urea Nitrogen, mg/dL


11.5


10.9




These findings are important for dairy professionals seeking to improve feeding performance and lower feed costs, particularly in the Northeast and Wisconsin. Kalscheur and the Wisconsin researchers continue their leadership role in the application of canola meal in diets for dairy cows, with additional studies and demonstration trials currently in the planning stage.