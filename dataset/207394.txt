Cattle are killing the planet.And meat is killing humanity.
Both statements are absurd, but a surprising percentage of people give them both a lot of credence. Why? Because both determined activists and less-than-knowledgeable media have combined to amplify genuine concerns, in the case of the former, and falsify unproven allegations in the case of the latter. The result is a perception that raising livestock and eating meat is bad news all the way around.
Here’s another accusation that appears to be going mainstream: meat causes diabetes.
Despite the illogic of that statement — diabetes is a failure of the body to properly process sugar — a growing wave of research studies and media reports on those studies are implicating animal protein as the culprit.
I’ve lost track of how many times I’ve written about the flaws in such thinking, but here we go again.
This time, the source is the respected Cleveland Clinic, a medical establishment that certainly has credibility when it comes to cardiology, but less so when its publicity machine cranks out articles with titles like this: “How to Reduce Your Risk of Diabetes: Cut Back on Meat.”
“You probably know that eating too much sugar and fat increases your risk of developing type 2 diabetes,” the article began. “But research increasingly shows that a food you might not expect — meat — can dramatically raise your chances as well.”
First of all, red meat consumption has been declining significantly across the Western world for decades now, and during the exact same time period the incidence of diabetes among the same population has been soaring.
Those data alone undercut any possible cause-and-effect rationale between eating meat and developing diabetes.
Second, dietary studies are notoriously unreliable. With the rare exceptions of limited population studies where the subjects are in prison or some other type confinement, and the types and amounts of food can be strictly controlled, recall studies or dietary journals as a source of accurate information are anything but.
I defy you to write down what you had to eat for the last seven days. Within the first 30 seconds, you will be “guesstimating” meals, exaggerating quantities and simply forgetting about the snacks and treats that we sometimes don’t even realize we’re eating.
Disproving Assumptions
Third, and most importantly, all dietary studies suffer from the curse of what’s called “multivariate causation,” which means that virtually all disease arises from a complex of factors, not from pinpointing one specific reason.
Here’s a great example: Ask the average adult what causes (or aggravates) high blood pressure, and most will respond with a four-letter word: “Salt.”
Not so fast. Despite the prevalence of that assumption, a recent large-scale study in the American Journal of Hypertension — whose authors ought to know a thing or two about high blood pressure — examined nearly 9,000 adults in France and found that salt consumption was not associated with elevated systolic blood pressure in either men or women.
Why not? Because, as the authors wrote, the link between salt and blood pressure is “overstated” and “more complex than once believed.”
Yes — it is far more complex, and that’s the takeaway from every dietary study.
Here’s another widely believed assumption about diet and health: Soy has no saturated fat, and thus is a fantastic source of protein, right?
Not if you factor in the effect of what nutritionists call “phytoestrogens,” which can act upon human physiology just like endogenous estrogens. Eat too much soy, or drink too much soy milk, and some seriously unpleasant side effects can occur. Like growing breasts in men and worsening the course of breast cancer in women.
And here’s the clincher.
The Cleveland Clinic article was based on a recent study published in the Journal of the American Medical Association. That study analyzed the deaths of nearly 700,000 people in 2012 from heart disease, stroke and type 2 diabetes. Notice: we’re suddenly not focused strictly on diabetes alone any more.
The JAMA researchers found that nearly 50% of those deaths were related to “poor nutritional choices.” What does that mean? Not just eating meat but a complex of eating too few vegetables, not enough whole grains and way too much sugar-loaded foods and beverages.
In other words, the cause of death was “more complex” than the Cleveland Clinic would have you believe.
What’s more, the real risk was for people who’d already developed diabetes. For those folks, their risk of death increased if they consumed more processed meats — as part of a set of otherwise poor nutritional choices, the article failed to mention.
The JAMA study concluded that choosing whole grains, nuts, low-fat dairy, fish and poultry instead of red meat lowers a person’s risk of diabetes.
Wrong on all counts.
As previously explained, researchers cannot draw conclusions about certain foods causing certain diseases. The compounding factors of stress levels, amount of activity and exercise, genetic predispositions, drug use and even environmental pollution combine to make it virtually impossible to draw a straight line between what you eat and how you die.
Should we all be eating more vegetables, whole grains, fish and fruit? Of course.
Should we worry that a slice of deli meat, a strip of bacon or a serving of ground beef is going to saddle us with a crippling disease?
Not unless a whole lot of other variables are on the negative side of one’s lifestyle ledger.
 
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.