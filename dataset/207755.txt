Chilean grape shipments to North America rose 11% in the 2016-17 season, according to statistics from the Chilean Fresh Fruit Exporters Association.
 
The association said in a news release that exports to North America totaled 364,770 metric tons, up 11% compared with 2015-16. 
 
Confirming the rise in Chilean numbers, the U.S. Department of Agriculture reported that U.S. imports of Chilean grapes from October 2016 through May 2017 totaled 341,000 metric tons, up 10% from the previous year.
 
Buyers in North America received half of Chile’s total 2016-17 exports of just more than 730,000 metric tons. Chilean grape exports to all global destinations were up 4% in 2016-17, according to the release.
 
In 2016-17, the Far East received 23% of Chilean grape exports, with Europe taking 17% and other destinations accounting for 9% of exports, according to the release.
 
Red seedless grapes accounted for 37% of all Chilean grape exports, followed by red seeded grapes with 30% of total volume, white seedless grapes with 27% and black seedless grapes with 5% of exported volume.
 
The release said 293 companies exported Chilean table grapes in the 2016-17 season, which was 23 companies fewer than the previous season. 
 
The top five exporters, according to the release, were:

Subsole with 4.93% of the total exported volume;
Del Monte, 4.88%;
Rio King, 4.46%;
Exser, 4.34%; and 
Dole-Chile, 4.31%.