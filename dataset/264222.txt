BC-Cotton
BC-Cotton

The Associated Press

NEW YORK




NEW YORK (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Monday:
OpenHighLowSettleChg.COTTON 250,000 lbs.; cents per lb.May82.9982.9981.1981.23—1.62Jul82.9683.0881.4881.52—1.46Sep77.29—.99Oct78.30—1.08Nov77.29—.99Dec78.1378.2077.2677.29—.99Jan77.50—.97Mar78.1878.1877.4777.50—.97May78.2278.2277.6277.62—.86Jul78.2278.2277.6577.65—.77Sep73.15—.34Oct75.26—.56Nov73.15—.34Dec73.2573.2573.1573.15—.34Jan73.39—.34Mar73.39—.34May74.09—.34Jul74.20—.34Sep72.66—.34Oct73.79—.34Nov72.66—.34Dec72.66—.34Est. sales 26,851.  Fri.'s sales 17,017Fri.'s open int 272,558