A check with the www.regulations.gov website reveals that more than 2,400 people have commented on the organic research promotion and information order. Comments are due by March 20 for some parts of the proposal and April 19 for other elements, notably regarding the referendum and certified products.
 
The comments rolling into the USDA are diverse. With a script in hand, a woman from Phoenix writes:
 
"It is important for families like mine to be able to choose organic. That is why I am writing to let you know I support the creation of an organic check-off. I try to buy the most organic products I can for my family, because I know that when I do, I'm supporting an agricultural system that is good for our planet. 
I believe that the U.S. Department of Agriculture (USDA) Agricultural Marketing Service (AMS) proposed rule on an organic research and promotion order (AMS-SC-16-0112) is a terrific advancement that will lead to more organic food and farming. "
 
 
A organic operator writes:
 
"As a member of the organic sector I applaud AMS's work on the proposed rule, appreciate the challenges that come with designing a check-off program for an entire multi-commodity sector, and look forward to a final rule and industry vote so we can move forward toward creation of an organic check-off in an expedient manner.
I support the check-off mechanism as a way for organic farmers AND businesses to respond to research, education, promotion, and information challenges and act collectively to ensure a strong future for the organic industry.  I thank the Agricultural Marketing Service for the opportunity to comment, and I urge USDA to move to an industry vote as soon as possible."
 
 
A naysayer chimes curtly chimes in:
 
"Organic farmers already have a detailed regulatory procedure they follow. They do not need any more regulations." 
 
A grower from Oregon make his point briefly but decisively:
 
"I have made my living as an organic farmer for 39 years. I strongly oppose the organic checkoff program. It is not needed to promote demand for organic produce. It will unfairly charge growers for promotions they may not support."
 
Another grower is equally wary:
 
"Our farm spends a great deal of time and money conducting our own research into improving organic production methods in our area. Our local extension center also conducts relevant local research. I don't want to contribute to a fund that would duplicate those efforts. No checkoff, please."
 
 
In sheer numbers, the voices supporting the organic promotion order may win the day, so perhaps we will see a referendum take place to settle the question. So far, however, the organized enthusiasm for the organic check off program has not quieted the voices of dissent in the countryside.