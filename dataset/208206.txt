Expansion has been deliberate and thoughtful at Hanor. Establishing working relationships with genetic stakeholders and processors has been key to the company's success from conception.Early Beginnings
Hermann Kronseder and Hanns-Dieter Alhusen founded the pig production company, which eventually became known as Hanor, in 1978. Hermann, who founded the Krones AG mechanical engineering firm in Germany, also had an interest in animal agriculture. He and his friend Hanns, an immigrant from Germany, established farms in Rocky Mount N.C., expanding to 23,000 sows.
When Hanns died in 1995, a new partnership emerged involving Norman Kronseder (Hermann’s son), Baxter Gutknecht and Myrl Mortenson. Baxter and Myrl were contracted from PIC USA in 1996, to fill the urgent management void. After a short time, both left PIC to become managing partners at Hanor.

Genetics to Processing
With the retirement of Gutknecht, Mortenson was appointed president in January of 2016. He and the CFO, Carl Stoner, are directing the company during a growth target of 100,000 sows (2.6 million pigs), a second plant (Seaboard Triumph Foods) in Sioux City, Iowa and a new corporate office in the heart of Hanor production, Enid, Okla. They manage the company for Harald Kronseder, son of founder Hermann Kronseder, which includes Pederson’s all-natural pork plant. 
“Any expansion we’ve done has been deliberate but careful,” Boyd says. 
“The financial ability of the Kronseder family has to be mentioned because when it came to getting critical monies for that initial plant, they were able to provide the needed resources.” 
The farms continued to grow in North Carolina. There were six sow farms in addition to the one west of Chicago. It suddenly grew again when Oklahoma was added as another sow base, with 30,000 sows. The company also purchased from PIC the Spring Green, Wis. operations, including PIC’s genetic nucleus farm (called the Main Farm) and several multiplication farms. 
The growth spurt ended with a second base of about 30,000 sows joining another sow base of about 28,000. Combined with the genetic nucleus multiplication farms in Wisconsin, the total was approximately 65,000 sows. 
 
The next major step forward was to partner with Allied Pork Producers and become aligned with the Farmland Meat System. Hanor and Allied Pork Producers provided pigs under the name Triumph Group to Farmland. When Farmland was not able to continue, Hanor partnered with Seaboard.
“At first, we provided pigs to Seaboard but eventually, we pulled together four other producer groups and the Allied pork producer group to create a new plant called Triumph Foods,” Boyd says. “The Triumph Foods plant operated separately from the Seaboard plant but since Seaboard was the marketing arm for both companies, we had a contractual arrangement around certain procedures.” 
The Triumph Group in St. Joseph, Mo., was given birth as it is today in about 2006 and Boyd says, “It has been a tremendous investment for the owners.” 
The first plant in St. Joseph, Mo., harvests 21,000 pigs per day, and eventually, the second plant (which opened in Sioux City, Iowa this year)  will harvest about 21,000 pigs per day when fully operational. 
 
 

This article was featured in the September issue of Farm Journal's Pork. Read the full profile by viewing the digital edition. Adapted for web by Sara Brown.
 

 


Read more about Hanor: 

How Hanor Triumphs
Strategic Partnerships Helped Hanor Grow
On-Farm Research Guides Production at Hanor
Hanor Touts Genetic and Animal Management 
What Makes Hanor Different