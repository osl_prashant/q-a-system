Large volume of wood burns at Wyoming sawmill, no one hurt
Large volume of wood burns at Wyoming sawmill, no one hurt

The Associated Press

SARATOGA, Wyo.




SARATOGA, Wyo. (AP) — A fire has damaged a large amount of lumber at a sawmill in Wyoming.
Firefighters say they prevented flames from reaching the sawmill in the town of Saratoga but not before they damaged some 600 million board-feet (1.4 million cubic meters) of wood Friday.
The Rawlins Daily Times reports nobody was hurt and nearby commercial property was spared.
The Saratoga Forest Management sawmill is among the largest in Colorado and Wyoming and employs about 100 workers including loggers and truck drivers.
Local officials say state fire investigators will try to determine the cause.
___
Information from: Rawlins (Wyo.) Daily Times, http://www.rawlinstimes.com