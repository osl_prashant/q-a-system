Dan Mathieson has been appointed chief executive of Zespri. 
Effective immediately, the Zespri board of directors has appointed Mathieson — previously Zespri’s president of global sales and marketing — to the role, succeeding Lain Jager, according to a news release.
 
Zespri chairman Peter McBride said in a news release that Mathieson has an impressive track record over his 15 years with Zespri.
 
“I am confident he will lead Zespri well and deliver growth and increasing value for our industry and shareholders, building on the strong legacy that Lain Jager leaves behind,” McBride said in the release.
 
“In his time leading our sales and marketing operations and offshore markets, Dan has successfully delivered a demand-led strategy, growing mature markets and diversifying into new markets.”
Mathieson said in the release that Zespri is in growth mode.
 
“As chief executive I am looking forward to helping continue the momentum we have in our business as we work toward increasing global sales to $4.5 billion by 2025,” he said in the release. “Delivering Zespri’s strategy means an increasing focus on our international markets, as we continue to increase demand ahead of supply and expand our 12-month supply business.” Mathieson said he will split time between Zespri’s headquarters in New Zealand and the company’s sales and marketing hub in Singapore.
 
Zespri sold $2.26 billion of Zespri kiwifruit in 2016-17, according to the release, marketing kiwifruit in 59 countries. Zespri exports kiwifruit from New Zealand and also markets kiwifruit grown in Italy, France, Japan and South Korea, according to the release.