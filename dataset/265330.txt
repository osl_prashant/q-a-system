AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:15 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
UPCOMING TOMORROW:
STUDENT GUN PROTESTS-COLORADO
DENVER — Students and activists in Denver and other Colorado cities plan events or rallies Saturday in conjunction with the Washington, D.C., March For Our Lives.
TOP STORIES TODAY:
CHEMICAL WEAPONS-DESTRUCTION
DENVER — The U.S. Army wants to change the way it destroys part of its huge stockpile of obsolete chemical weapons in Colorado, but some people worry that could increase the chances of contamination escaping into the air. The Army's Pueblo Chemical Depot is eradicating 780,000 shells filled with thick liquid mustard agent — many of them dating to the Cold War — under an international treaty banning chemical weapons. By Dan Elliott. SENT: 730 words, photos.
STUDENT GUN PROTESTS-LIBERAL SUPPORT
DENVER — Before the shooting had even stopped, teenagers hiding at their Florida high school were talking about gun control. Within days, they had launched a crusade against gun violence — one that will result in a nationwide series of protests Saturday. In taking up the fight, the students have joined forces with liberal organizations that have been pushing for years for tighter gun laws. That's led to criticism that the youngsters' cause is less spontaneous than it seems and that they are being used as pawns. By Nicholas Riccardi. SENT: 1,020 words, photos.
AP POLL-GUNS IN AMERICA
Weeks after the mass shooting of high school students in Parkland, Florida, a new poll from The Associated Press-NORC Center for Public Affairs Research finds that Americans are overwhelmingly in favor of stricter gun laws, and increasingly likely to think making it harder to legally obtain a gun will result in fewer mass shootings. By Steve Peoples and Emily Swanson. SENT: 930 words, photo, video.
With:
AP POLL-GUNS IN AMERICA-METHOD — The Associated Press-NORC Center for Public Affairs Research poll on gun laws was conducted by NORC from March. 14-19. It is based on online and telephone interviews of 1,122 adults who are members of NORC's nationally representative AmeriSpeak Pane
IN BRIEF:
— FLAMING T-REX — The co-owner of a dinosaur-themed park in southern Colorado thinks an electrical malfunction caused a life-size animatronic Tyrannosaurus Rex to burst into flames. (With AP Photo and Video)
— FORT CARSON WILDFIRE — The last remnant of a wildfire that started on Fort Carson during a training exercise has been extinguished, but the Army still hasn't said whether soldiers were using live ammunition during the exercise.
— COLORADO POPULATION — U.S. Census Bureau data shows that Colorado population growth is extending beyond its Front Range.
— RIG COUNT — The number of rigs exploring for oil and natural gas in the U.S. increased by five this week to 995. That exceeds the 809 rigs that were active this time a year ago.
— PIKES PEAK RAILWAY — A Colorado Springs tour bus operator is planning to offer daily round-trip shuttles to the summit of Pikes Peak after a railway to the top has closed.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for May rose 4.50 cents at 4.6025 a bushel; May corn was up 1.25 cents at 3.7725 a bushel; May oats was off 2.50 cents at $2.2625 a bushel; while May soybeans was fell 1.50 cents at $10.2825 a bushel.
PHOTOS:
— CHEMICAL WEAPONS DESTRUCTON
— FLAMING T-REX
— STUDENT GUN PROTESTS-LIBERAL SUPPORT
SPORTS:
DENVER-TANNER'S EDGE
DENVER — The images that decorate the mask of Denver's senior goaltender are all too revealing. They've come to symbolize his affection for his school. His upbringing. His loyalty. And even to a degree the chip on his shoulder. By Pat Graham. SENT: 800 words, photos.
NUGGETS-WIZARDS
WASHINGTON — The Wizards return home to open a three-game home stand as they try to keep pace in a competitive Eastern Conference playoff race against the Denver Nuggets, who are on the outside looking in out West. By Stephen Whyno. UPCOMING: 600 words, photos. (Game started at 5 p.m. MT)
SPORTS IN BRIEF:
— HKO-US-ZITO — Bill Zito has been selected as the general manager for the United States men's national hockey team.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.