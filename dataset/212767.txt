It’s berry season, and California growers say ample supplies of all kinds of berries should be available for the spring/summer holidays, including Mother’s Day, Memorial Day and the Fourth of July.
All districts were picking strawberries by mid-April, said Craig Moriyama, director of berry operations for Naturipe Berry Growers, Salinas, Calif.
That includes Oxnard, Santa Maria and Salinas/Watsonville.
Peak season in Oxnard was winding down, however. Picking likely will wrap up there after Mother’s Day, May 14.
At the same time, volume out of Santa Maria was picking up, he said. That district should reach peak season around the first of May.
Meantime, Salinas/Watsonville got off to a late start — about a month later than last year — because of consistent rainfall all winter, he said.
Growers had to throw away a lot of fruit.
But by mid-April, things were looking up.
“The fruit is all set on the plants, and the plants are all growing really good because of all the water we’ve had,” he said.
Salinas/Watsonville typically peaks between the end of May and the end of June, depending on variety, Moriyama said.
Strawberry growers were in and out of their fields all winter long picking then stripping and cleaning up after the storms, said Jim Grabowski, merchandising manager for Watsonville-based Well-Pict Inc.
The company started picking in Watsonville in early April, a bit earlier than had been anticipated, probably due to tighter supplies in the southern districts than expected, Grabowski said.
He expected shipments to be up to speed by early May.
“By May 1, Watsonville should be producing fairly good numbers,” he said.
Picking also was “cranking up” in Santa Maria, and Oxnard was peaking in mid-April.
Volume out of Oxnard was not as great as it should be for that time year, he said, “but they’ll get there.”
“Quality has been good so far,” Grabowski added.
At California Giant Berry Farms in Watsonville, Cindy Jewell, vice president of marketing, said Santa Maria and Watsonville should be kicking into normal volume by May 1, with buyers shifting to those areas and out of Oxnard.
Varietal berries also should be in good supply this summer.
California Giant will begin its California blueberry program by early May and continue into June, Jewell said.
She was hopeful that the company’s California blackberries and raspberries would be available by Memorial Day, May 29.
She encouraged retailers to set up berry patch displays throughout the summer.
Naturipe got a late start on its raspberry and blackberry programs, Moriyama said, but raspberries were expected to be ready by late April in Santa Maria, and blackberries should be up and running from late May throughout the summer.
Well-Pict started its early-season raspberry program in mid-April, Grabowski said.
That program will continue into mid-July, then a second crop should come on a short time later and continue until November.
The company grows many of its raspberries under hoops, he said.