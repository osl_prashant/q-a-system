“We don’t track shrink in produce.”The cashier at the supermarket told me that a few weeks ago after I mentioned the produce bags were messed up and I dropped an avocado on the floor.
“Yikes, that’s an expensive item to shrink,” I said.
“Oh, we don’t track shrink in produce,” she said.
“We. Don’t. Track. Shrink. In. Produce.”
Insert record player scratching noise, eye-twitching, and my “say what?” look of incredulity.
The clerk (a cashier, not a produce clerk) clarified that the store doesn’t do inventory to track item-by-item what doesn’t get sold in produce like they do in center-store and other barcoded inventory, but I do honestly believe this person doesn’t know how produce tracks its shrink.
And that kind of blew my mind.
Of COURSE a good supermarket operator knows how many avocados (maybe not numbers, but surely volume), peaches, bags of salad, bananas, etc., came in the door versus what went through checkout.
If our frontline personnel don’t think they’re accountable for shrink item-by-item, how can we trust them to take their jobs seriously? Shrink is one of the most important numbers in produce and is managed very carefully.
The National Grocers Association, Washington, D.C., recently released its 2017 Independent Grocers Financial Survey that had some really interesting points about shrink and performance.
According to the report, total shrink in produce among independents is 6.2%, but there’s a big shift in numbers between small operators and those with 31+ stores.
Single-store operators reported 7.8% shrink in produce, those with two to 10 stores reported 8.1%, 11-30 stores reported 3.8% and those with 31-plus stores reported 2.9%. How do your numbers compare?
Hey, at least we’re not a bakery. Their shrink is 8.5%, the leader in all categories, but their gross margin was also the highest, at 42.63%, compared to 31.28% gross margin in produce.
Total store shrink was 3.2%, and total gross margin was 27.13%.
The survey had some pointers for retail, such as tracking produce shrink at retail, instead of at cost.
“Measuring shrink at cost is useful for accounting purposes, but measuring shrink at retail price helps highlight its importance to the bottom line and may prompt greater motivation for performance improvement,” the authors said. “Stores that use the retail method have lower-than-average shrink, at 2.4%.”
Shrink programs, and third party shrink audits, authors said, are especially beneficial in helping mitigate shrink in perishables and theft-related losses.
Do your cashiers know how you track shrink? Do your produce clerks? Do they think that jumbled up cull box just magically disappears into the landfill or food bank or wherever unsaleable product goes, and it doesn’t affect the store the same way a box of crackers marked on a spreadsheet might?
Take some time to talk to your least senior employees about shrink, how you track it in produce, and how those sales dollars affect the whole department. Those avocados I dropped? Say goodbye to at least $3 in sales. I’m sure I wasn’t the only one who dropped fruit that day, thanks to a faulty spool of produce bags right next to the avocado display.
We’re coming up on some of the highest shrink numbers of the year over the next couple of months. Get it in check now before it’s too late.

Pamela Riemenschneider is editor of Produce Retailer magazine. E-mail her at pamelar@farmjournal.com.

What's your take? Leave a comment and tell us your opinion.