BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Monday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeApr        12.56   12.77   12.41   12.42  Down .15May                                12.61  Down .15Jun        12.75   12.91   12.61   12.61  Down .15Sep        13.12   13.29   13.00   13.00  Down .14Dec                                14.05  Down .08Feb        14.11   14.31   14.05   14.05  Down .08Apr        14.27   14.47   14.22   14.22  Down .07Jun        14.39   14.59   14.34   14.34  Down .05Sep        14.63   14.82   14.60   14.60  Down .03Dec                                15.14  Down .03Feb        15.17   15.25   15.14   15.14  Down .03Apr        15.14   15.19   15.11   15.11  Down .04Jun        15.14   15.14   15.12   15.12  Down .05Sep        15.36   15.36   15.32   15.33  Down .07