Jakov Dulcich, founder of McFarland, Calif.-based J.P. Dulcich & Sons, was shot and killed April 11.
Sgt. David Hubbard with the Kern County Sheriff’s Department said Dulcich was driving on South Browning Road when an occupant of another vehicle fired into his SUV.
The two vehicles were taking up both lanes of the road, so a third vehicle traveling northbound pulled onto the shoulder to avoid a collison.
After the shooting, Dulcich’s SUV continued forward and hit the vehicle on the shoulder, Hubbard said. The vehicle from which shots were fired drove to the scene of the crash, and an occupant then got out and assaulted the driver of the car on the shoulder. The driver escaped into a vineyard, and the attacker fled.
Hubbard said the investigation is ongoing, with the department pursuing several leads.
Dulcich's company issued a statement in wake of the news.
“J. Dulcich & Sons and Sunlight International Sales confirm, with heavy hearts, the passing of our founder, Jakov Dulcich,” the company said. “The Dulcich family asks for privacy during this difficult time. We have no further comment at this time.”
Originally from Croatia, Dulcich has been involved in California agriculture since 1960, according to the company website. He purchased his first 80 acres of farmland in 1975 and over the next several decades added acreage — including a purchase of 2,600 acres in the Maricopa and Arvin areas in 2012 — and built several packinghouses.
The company has been known for its Pretty Lady table grape brand since its debut in 1995.