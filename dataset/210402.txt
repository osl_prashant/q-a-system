Tracy Moore grew impatient as she waited for a Quarter Pounder recently in the parking lot of a McDonald's restaurant in central Dallas.
The burger, made with fresh beef and billed as hotter and juicer than the original made from a frozen patty, is part of the company's effort to serve tastier food.
But after about four minutes, it was Moore who was steamed. Like other customers who'd ordered the new Quarter Pounder at the restaurant's drive-through, she was asked to pull into a parking space and wait.
"If it's going to be that long every time, I won't order it. I'd go elsewhere," said Moore, who hits the drive-through every morning for a Coke and dines frequently at the chain.
The tradeoff between time and taste looms large for McDonald's Corp as it works to win back business lost to rivals. The introduction of cooked-to-order, quarter-pound burgers made with fresh beef is part of the chain's attempt to improve food quality. Announced in March, the new sandwiches are already in selected test markets and are expected to be served in all U.S. stores by mid-2018.
But the success of the initiative may well hinge on satisfying important customers like Moore: speed-minded drive-through patrons who account for 70 percent of the firm's U.S. revenue.
An on-demand Quarter Pounder takes about a minute longer to land in a customer's hands than does the original sandwich, according to restaurant managers and analysts, even though fresh beef fries up faster than frozen patties. That's because grilling begins only after a patron orders. Traditional Quarter Pounders were often cooked up in batches ahead of time.
Every second counts in the fast-food business. McDonald's drive-through speeds already lag those of some major competitors, according to one widely watched survey. McDonald's does not share such data, but company representatives told Reuters earlier this year that service times have slowed.
Still, company executives are bullish on prospects for the popular Quarter Pounder, which accounts for about one-fourth of McDonald's U.S. burger sales. At an investor conference last month, Chief Executive Steve Easterbrook said the changeover has created fewer complications than expected and that restaurant operators are on board.
Some industry veterans, however, are skeptical. Richard Adams, a former Southern California McDonald's franchisee-turned-consultant, says convenience is paramount for the chain's patrons, who may go elsewhere if speed deteriorates.
"Any time the cooking process begins after the customer orders, the service time will be slower," Adams said.
The fresh-beef initiative comes as pressure builds on McDonald's kitchens.
Adams says restaurant crews already are juggling trickier menu items thanks to the recent national launch of McDonald's new "Signature Crafted" sandwich line, which allows customers to pick their own meat, buns and toppings. "Signature Crafted" quarter-pound burgers also will use fresh beef as it becomes available nationwide.
McDonald's cooks could be further strained by the chain's embrace of self-service kiosks and mobile ordering. The technology shaves ordering times, but can create new bottlenecks by swamping kitchens at peak hours, as companies such as Starbucks Corp have learned.
 
FRESH VERSUS FAST
The revamped Quarter Pounder is the latest move by Easterbrook to modernize the 60-year-old chain and reverse four straight years of traffic declines.
It's also a direct shot at Wendy's Co, Whataburger and In-N-Out. Those fresh-burger chains are among the fast-food rivals that McDonald's says have siphoned 500 million U.S. transactions from its stores since 2012.
Easterbrook's introduction of all-day breakfast in October 2015 was a big hit and has helped lift sales. The company's stock price is up more than 25 percent so far this year.
Analysts expect the fresh-beef push, along with moves to ditch artificial ingredients in popular items such as chicken nuggets, to bolster sales by addressing consumer demand for simpler, "cleaner" and fresher ingredients.
The Quarter Pounder makeover has won early support from analysts and McDonald's franchisees in the heart of cattle country, where the product has been tested for almost two years in about 400 stores in Oklahoma and Texas.
Three Dallas-area McDonald's managers who spoke with Reuters estimated the switch has improved their Quarter Pounder sales from 20 percent to 50 percent, albeit aided by advertising and coupons.
"We've been stealing customers from a Whataburger down the street," said Edgar Meza, a manager at a McDonald's restaurant in an upscale neighborhood in north Dallas. Officials at Texas-based Whataburger, a regional chain, declined to comment.
Some burger lovers are taking notice too.
"They're a little juicier," said Bob Riley, who was polishing off a Quarter Pounder at an outlet near Dallas' Deep Ellum neighborhood, his third McDonald's meal of the week.
"I think Wendy's woke them up," he said.
Joe Jasper, a former McDonald's executive who owns 20 restaurants in the Dallas-Fort Worth area, has been deeply involved in the effort. He described the new Quarter Pounder as "the best burger in our industry, but more importantly, (one delivered) at the speed of McDonald's."
Trouble is, the "speed of McDonald's" isn't as fast as that of many of its competitors.
The average service time at a McDonald's drive-through last year was 208.2 seconds, according to a study published by QSR magazine, an industry publication, using data from SeeLevel HX, an Atlanta-based business intelligence firm. That's well behind industry leader Wendy's at 169.1 seconds, according to the survey. Burger King, Dunkin' Donuts and KFC all beat McDonald's too.
McDonald's narrowed the gap with Wendy's by one-third from 2012 to 2016 by adding more drive-through lanes at some stores and by scrapping products such as "snack wraps," tortilla-wrapped sandwiches that proved time-consuming to prepare. Still, its average drive-through service time last year was almost 20 seconds slower than it was in 2012, according to SeeLevel HX data.
(For a look at drive-through speeds, see tmsnrt.rs/2sLWMqW)
Claudia Barcenas, assistant manager at a McDonald's off Dallas' Central Expressway, says her counter and drive-through staff inform patrons that fresh-beef Quarter Pounders can be delayed, particularly if the sandwiches are ordered well-done.
"We have to explain that it takes a bit longer. Perhaps a minute," Barcenas said.
Whether that's worth it for McDonald's customers remains to be seen as the experiment goes nationwide.
Juan Rodriguez waited on his lunch break for a fresh-beef Quarter Pounder at the drive-through of another Dallas McDonald's outlet about nine miles from Barcenas' store. At the three-minute mark, the 20-year-old was getting restless.
"If it's better, I don't mind waiting," Rodriguez said. "But if it tastes the same, then no."
(Editing by Marla Dickerson)