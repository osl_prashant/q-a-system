Arizona farmers, brewers fight drought with craft beer
Arizona farmers, brewers fight drought with craft beer

The Associated Press

CAMP VERDE, Ariz.




CAMP VERDE, Ariz. (AP) — A new conservation-minded operation to grow and process malt barley in north-central Arizona is intended to provide an in-state source of a key ingredient for craft beer brewers while reducing agricultural demand for Verde River water.
The Arizona Republic reports the state's first barley malt house should open in the Verde River Valley this month, supplying a key beer ingredient grown with water pulled from an overworked river that is crucial to metro Phoenix's water supply.
Sweet corn, alfalfa and other crops typically suck water from the Verde all through summer and, in years past, have drained miles of the river between a diversion ditch and the point downstream where the runoff returns to the river.
Hauser and Hauser Farms have converted .22 square miles (.57 square kilometers) of those crops to winter-planted barley.
___
Information from: The Arizona Republic, http://www.azcentral.com