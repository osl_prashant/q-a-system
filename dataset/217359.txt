Chicago Mercantile Exchange live cattle futures rallied about 1.5 percent on Tuesday on fund buying and stronger cash cattle and beef prices, traders said.
Frigid temperatures in U.S. Plains cattle producing areas also supported futures as such weather could slow down cattle weight gains, making them less available to packers.
Actively traded February live cattle settled 1.800 cents per pound higher at 123.350 cents, ending near its session high and at its loftiest level since Dec. 1.
“Cash trades late on Friday were a couple of dollars higher and futures are responding to that,” said Jeff French, broker with Top Third Ag Marketing.
“And the cold temperatures were reason enough not to sell the futures today,” he added.
Slaughter-ready cattle at U.S. Plains feedlot markets traded around $122 to $124 per cwt on Friday after futures markets had closed for the Christmas holiday weekend. Cattle had traded at $119 to $120 the previous week.
This week’s cash cattle sales could be $1 to $2 higher, traders said.
The choice wholesale beef price on Tuesday rose $2.24 per cwt to $205.14, while the select cutout jumped $3.59 to $197.57 per cwt, the U.S. Department of Agriculture said.
Feeder cattle futures rallied more than 2 percent on spillover strength from live cattle and on technical buying.
January feeders closed up 3.525 cents per pound at 149.525 cents while actively traded March ended up 4.200 cents at 146.875 cents. Both contracts left chart gaps as they rallied and breached their 100-day moving averages, and both closed near session highs.
Spot Hog Prices Slip
Lean hog futures ended mixed as traders unwound bull spreads and as nearby contracts shed some of their premium to cash markets, traders said.
CME February lean hogs closed 1.050 cents lower at 70.725 cents and April ended down 0.800 cent at 74.850 cents. The June 2018 through February 2019 contracts all closed higher.
Cash hogs in the closely followed Iowa and southern Minnesota market traded slightly higher on Tuesday, according to USDA data, but prices from $54.50 to $60.50 per cwt were well below spot futures.
Cold weather in the Midwest was less of a concern for hog producers as most animals are raised indoors and, thus, at less of a risk of slower weight gain. However, movement of pigs to packing plants is more difficult in cold weather.