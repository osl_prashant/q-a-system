Fertilizer prices were mixed on the week with UAN28% and potash firming just slightly.
Our Nutrient Composite Index softened 1.07 points on the week, now 56.81 points below the same week last year at 515.06.

Nitrogen

UAN32% led declines in the fertilizer segment this week.
This week's firmness in UAN28% was supported by gains in the eastern Belt and Minnesota, which may be the result of a temporary spike in sidedress demand.
On an indexed basis, urea is once again our most expensive form of nitrogen.

Phosphate

Phosphates were lower with MAP outpacing DAP declines 10 to 1.
This week's price action is about as expected since the DAP/MAP spread has normalized.
The DAP/MAP spread remains within expectations at 15.67.
Pressure on the nitrogen segment will continue to weigh on DAP and MAP in the coming weeks as much as it is able.

Potash 

Potash prices firmed just 4 cents this week.
Mild strength in potash alongside a decline in NH3 prices widened potash's premium to NH3 slightly.
Despite a slightly firmer price this week, we still feel no urgency to book Vitamin K ahead of fall applications.

Corn Futures 

December 2018 corn futures closed Friday, July 7 at $4.20 putting expected new-crop revenue (eNCR) at $669.13 per acre -- higher $47.44 per acre on the week.
July 1 marks the start of a new fertilizer year so we have changed our expected new-crop corn revenue (eNCR) formula to reflect December 2018 futures, which partially accounts for the jump in eNCR.
With our Nutrient Composite Index (NCI) at 515.06 this week, the eNCR/NCI spread widened 48.51 points and now stands at -154.07. This means one acre of expected new-crop revenue is priced at a $154.07 premium to our Nutrient Composite Index.





Fertilizer


6/26/17


7/3/17


Week-over Change


Current Week

Fertilizer



Anhydrous


$492.86


$489.63


-83 cents


$488.80

Anhydrous



DAP


$446.06


$445.10


-5 cents


$445.05

DAP



MAP


$461.25


$461.30


-58 cents


$460.72

MAP



Potash


$334.27


$333.82


+4 cents


$333.87

Potash



UAN28


$244.13


$238.07


+3 cents


$238.10

UAN28



UAN32


$261.39


$260.52


-$2.24


$258.28

UAN32



Urea


$335.50


$334.69


-$1.12


$333.57

Urea



Composite


519.08


516.13


-1.07


515.06

Composite