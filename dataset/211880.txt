South Texas produce importers and growers were preparing for heavy rains and strong winds over the weekend with expected landfall of Hurricane Harvey near Corpus Christi late Friday or early Saturday, according to the National Hurricane Center. 
According to an advisory from the center,  Harvey is expected to bring a life-threatening storm surge, rainfall and wind hazards to portions of the Texas coast. Storm surge flooding could reach between 6 and 12 feet above ground level at the coast between the north entrance of the Padre Island National Seashore and Sargent, just south of Galveston.
 
The Hurricane Center expected across the middle and upper Texas coast from heavy rainfall of 15 to 25 inches, with isolated amounts as much as 35 inches.
 
Calling conditions “the calm before the storm,” Dante Galeazzi, CEO and president of Mission, Texas-based Texas International Produce Association, said Aug. 28 that some towns along the coast are under mandatory evacuation orders.
 
“As you make it further inland, the threat is not quite as strong here in McAllen, but still there is the threat of localized flooding and power outages,” he said. Residents picked up sand bags and hit the stores hard for batteries, food and water, he said.
 
“If the storm hits tonight, the big concern is these bands of rain that will come through and drop a lot of rain in a short amount of time and that creates the localized flooding concern,” he said.
 
Galeazzi said most vegetable growers have not yet planted any crops. High winds could cause damage to the citrus crop, he said, but perhaps the biggest concern is the possibility of delays on the international bridges between Mexico and Texas. “If the wind gets too high in our area, the international bridges could close and that would affect imports,” he said. If that happens, loads could be shifted west to Laredo, he said.
 
Shippers in the region will likely close early today and encourage their customers to load before the storm hits late Friday.
 
Brent Erenwert, president of Brothers Produce, Houston, said the company had its biggest sales day of the year on Thursday, Aug. 24, as consumer and retail demand spiked for stock-up items such as apples, bananas, avocados, potatoes and grapes.
 
The impact of the storm is expected to last about five days, with potential for as much as 25 inches of rain in Houston.
 
“This city can handle that,” Erenwert said.
 
The company has generators if the power goes out, he said, and plans are to be operating Aug. 26 and Aug. 29.
 
As retail business surged in advance of the storm, school and foodservice businesses will fade, Erenwert said. Some Houston schools were closed Aug. 25, and residents will stay home when the storm rolls in rather than eat out.
 
Later, when the storm hits, loads of produce may not reach affected cities near the coast and be diverted to other markets such as Austin or Dallas.
 
“It could be a rollercoaster (market) on produce next week because of the storm,” he said the morning of Aug. 25.