Wetlands fight over western Wisconsin plan heads to Senate
Wetlands fight over western Wisconsin plan heads to Senate

The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — A state Senate committee is scheduled to take testimony on a bill that the Wisconsin Assembly amended to include environmental exemptions for a controversial $70 million sand processing development in western Wisconsin.
The bill before the Senate committee Wednesday does not include that provision meant to benefit Meteor Timber, but it could be added.
The hearing provides opponents to the move a chance to speak against it in hopes the Senate won't go along with the Assembly's change.
Meteor Timber is an Atlanta-based company that's planning a sand-processing plant to serve the frac sand industry near Interstate 94 in Monroe County. Environmental groups and others have been speaking out against the project.