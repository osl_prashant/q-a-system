Taylor Farms partnered with Yuma, Ariz.-based Labor of Love to thank harvesting crews for their service.
Nearly 30 workers were surprised with breakfast, thank you notes from Taylor Farms and $25 gift cards. Two raffle prizes were also randomly given out, according to a news release.
"At Taylor Farms we don't have employees, we have family members," said Christina Barnard, director of marketing at Taylor Farms, in the release.
Labor of Love was established to raise awareness about the important contributions of farm employees.
"We look forward to continuing to work with them as we shine a light on the industry's most important asset - the people," said Steve Alameda, president of the Yuma Fresh Vegetable Association, in the release.