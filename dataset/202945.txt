Democratic Congressional Reps. Jimmy Panetta and Zoe Lofgren are hosting an immigration town hall meeting in Salinas, Calif.
 
The meeting is set for Feb. 12, 1-3 p.m. at Hartnell Community College in Salinas, according to a news release.
 
Rep. Panetta, D-Calif., organized the town hall meeting to hear from California's Central Coast residents about concerns regarding President Donald Trump's executive orders on immigration, according to the release.
 
The release said the hearing will include legal experts and government officials to help explain the rights of immigrants.