In less than a week, the USDA will release its prospective plantings report. Corn has been seeing increased exports and more demand, putting the Dec. futures contract above the $4 mark.

These low prices are playing in corn’s favor, according to Bryan Doherty, vice president of brokerage solutions with Stewart-Peterson, because corn isn’t priced high enough to compete with soybeans.

He says part of the reason U.S. farmers in 2017 saw record yields was due to genetics, good farmers and good weather, but to continuously bank on that, it isn’t a strong bet. For example, he says if corn were to take a 10 bushel per acre tumble, that could be enough to ration supply.

“It doesn’t take much to change on the supply and demand table,” he said during Markets Now on U.S. Farm Report.

It’s no secret farmers love to plant corn, including Matt Bennett, owner of Bennett Consulting. However, he has seen more producers in his area of central Illinois earn more net income harvesting 75 bushel per acre soybeans than corn.

For Bennett, planting more soybeans should be an “easy discussion” for those farmers hurting for cash and have been making money on soybeans.

There’s a lot of speculation around what the acreage mix could be this year. While many farmers have already made their decisions, some are wondering if they could swap their acres.

Bennett doesn’t think there will be a lot of acres switching back to corn because of higher prices, and it won’t be until the corn markets see an additional price bump of 20 to 30 cents.

How likely is it to see that? Hear Doherty and Bennett discuss the possibility on U.S. Farm Report above.