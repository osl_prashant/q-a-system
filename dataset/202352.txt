Ready to deal with (yet another) serious problem of modern life, one that needs to be addressed right now?What are we talking about? Street crime? Traffic jams? The spread of the Zika virus?
Nice try.
The problem, I regret to inform you, is charcoal. Charcoal and chicken.
That's right. Charcoal grilling of chicken needs to end, and end today!
The issue has erupted in New York, where the City Council is planning to double the number of food-vendor permits. The city wants to put the clamps on a black market for such permits, in which a $200 license can be sold for as much as $25,000, according to a report in Crain's New York Business.
The proposed legislation would allow licensing of more than 600 additional street vendors every year for the next seven years, a total of 4,200 new permits. The New York Times reported that preference would be given to some 2,500 people on the city's waiting list for permits, with 35 allotted to veterans and disabled people.
Makes sense, right? Expand one of the entry points to the economy for more deserving people, and curtail a black market that can financially cripple people desperate enough to shell out thousands of dollars for a vendor's permit.
Problem is, New York health officials are warning that expanding the number of food carts could worsen what they claim is the "serious pollution" caused by charcoal grilling.
According to the Department of Health and Mental Hygiene, a single street cart grilling meat over charcoal creates as much particulate pollution as a diesel truck driving 3,500 miles from New York to Denver and back. Allegedly, 20% of New York City's particulate pollution comes from commercial charbroiling operations (including conventional restaurants).
"We know that New Yorkers care deeply about this issue," Deputy Health Commissioner Corinne Schiff testified at a hearing. "We frequently receive complaints from residents about smoke and odor coming from mobile food carts and trucks."
Love the Smell of Chicken in the Morning
And therein lies the problem. Not the danger of inhaling particulates from a street grill, but the nuisance of such operations.
Here's how The Gothamist website led off its story on the issue:
"It's a New York City right of passage to bear the Eau du Street Meat scent after walking through a plume of scorched chicken grease on a Midtown sidewalk."
That captures the controversy in a single sentence: What bothers people isn't some threat to their health, but the inconvenience of the smoke and the smells.
Consider second-hand smoke as a parallel. These days, most states not only prohibit smokers from puffing away inside a building, they're not even allowed to stand outside a doorway with a cigarette. In my state, for example, you need to stand 30 feet from an entryway or window if you plan to light up outdoors. Any night of the week, you can see people huddled in the rain around the corner from bars and restaurants, sucking down a quick smoke before they get totally soaked.
Is there a danger to one's health from walking into a building where a smoker is standing closer than 30 feet? Of course not. It's the aesthetics of smelling cigarette smoke that people object to, and likewise, it's the odor of grilled meat that causes New Yorkers to start squawking.
Truth is, most food vendors won't be affected if New York cracks down on charcoal grilling. Vendors who operate the food trucks found all over the city use propane-powered grills.
And of all the issues New Yorkers could rightly complain about (reckless cabbies, piles of trash on the sidewalk on garbage day, hustlers, muggers and pickpockets who make a living ripping people off) the smell of grilled chicken is pretty far down the list.
I used to live in Manhattan, and I guess it has to be chalked up to one of the incongruities of modern life that New Yorkers can step over a homeless person sleeping on a subway grate without even a momentary concern, but if a guy is cooking chicken on the sidewalk?
Something's gotta be done about that!

The opinions expressed in this commentary are those of Dan Murphy, a veteran journalist and columnist.