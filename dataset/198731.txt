According to the USDA's latest Crop Progress report, corn planting is 98% complete - making it inches away from reaching the finish line. At 90%, emerged corn isn't far behind.Attention now shifts to corn conditions.
Nationally, corn conditions improved from last week's report with 75% in good or better condition. This is slightly ahead of last year's report of 74%.
The best corn conditions in the country were reported in North Dakota and Wisconsin, with each reporting 86% of corn in good or excellent condition. Other states in the Northern Corn Belt are also faring quite well.
Not all states can say that though. Indiana, Kentucky, and Missouri each reported at least 6% of corn in poor or very poor condition - the worst in the country.
SoybeansThe 2016 soybean planting sprint is nearly over. At 83%, this year's pace is 6 percentage points above last year's report and 6 percentage pointsahead of the five-year average. Emerged soybeans are picking up the pace, reported at 65%.
Soybean conditions are faring better than reported in 2015 with 72% of soybeans in good or excellent condition.
Unsurprisingly, many of the same states to report higher percentages of corn in good or better conditions are reporting similar percentages of soybeans in these conditions. North Dakota (81%) and Wisconsin (82%) once again lead the pack.
Four states reported higher percentages of corn in poor or worse condition: Arkansas (9%), Illinois (6%), Mississippi (9%) and Missouri (6%).
Click here to read the USDA's full report.