U.S. farmers are off to a slow start on corn plantings and even though it is still very early in the 2017 season, the numbers may already imply that total corn acres could be less than the 89.996 million that the market currently expects.The U.S. Department of Agriculture placed corn planting progress at 6 percent in the week ended April 16, slightly below the pre-report trade guess of 8 percent. The figure stands behind both last year and the five-year average, which were 12 percent and 9 percent, respectively.
This year, the wet conditions in some areas of the upper Midwest, the forecast for a potentially wet spring, and the fact that the forecasts have leaned wetter for the last couple of weeks have all instilled in traders the idea that U.S. corn planting could be slow - which it has been to start.
Although technological advancements now allow farmers to plant nearly twice as fast as they could two decades ago, wet weather and soil conditions still impede the planting process, especially if the inclement weather is without frequent breaks.
Corn planting pace also affects soybean acreage and corn yield, though those impacts will become more prevalent next month. And even after all the corn is planted, the market might not be clued in to the realized acreage shifts right away, at least not by USDA's June estimates.
Progress and Corn Acres
Farmers cannot plant corn too late - much after May - because they are risking yield, quality and insurance guarantees by doing so. This is why delays sometimes lead to a reduction in intended corn acres.
Of the last 20 years, there were 12 years in which corn planting progress was behind the long-term average pace by April 16, which is the case this year. Final corn acres came in more than 1 percent below March intentions six out of those 12 years, and in only two of the 12 did final corn acres actually increase over March.

But this scenario was not always portrayed in the June acreage report. USDA placed corn acres more than 1 percent below March intentions in only one of the 12 slow early planting years.
And the up or down bias in the June report was much less clear amongst the 12 years - three of them had acres decisively rising while three were decisively falling, and the rest were mixed.
Progress and Soy Acres
Any delays in corn planting always gets analysts immediately thinking of the upside potential for soybean acres, especially this year given the acreage tug-of-war ensuing between the United States' two key crops.
On the other hand, excellent corn planting weather and fast progress might encourage farmers to get a little more corn in than they had anticipated, perhaps dipping into the land set aside for soybeans - some 89.482 million acres in 2017, according to USDA.
History shows that these scenarios do actually occur, but the extent to which fast corn planting hurts soybean acres is actually greater than the extent to which fast corn planting helps corn acres.
Based on the past 20 years of data, corn planting progress as of May 21 - during the final stages of the planting efforts - should provide clear indication on which direction U.S. soybean acres may be headed following the March planting intentions.

Similar to corn, June acreage does not always reflect these shifts, as the June report typically holds too much optimism for soybean acres under fast corn planting conditions.
The June report tends to capture the soybean upside well when corn planting is slow. Of the nine years where corn planting was slower than the 20-year average by May 21, the June report only twice reflected lower soybean acres than what had been printed in March - and this ended up being the case at the end of the season.
However, in the 11 years where corn planting progress was ahead of pace by the same date, June soybean acreage rose above March intentions five times, though the actual acres only landed higher than the March numbers twice. Estimates that were already on the downside in June often became even lower when the final figures were published.
Progress and Corn Yield
At this early stage, planting progress hardly provides any bearing on final corn yield. And the relationship with yield is just as muddy toward late May.

That is unless U.S. farmers have fallen well behind schedule and remain there late in the planting window - which historically introduces yield risk.
The 20-year average corn planting pace is 86 percent by May 21. Some of the worst yields in the last two decades were ultimately observed in years where progress was 75 percent or below on this date.

The exception to this was 2009, where the laggard May 21 planting progress of 73 percent was overridden by exceptional summer weather.
On the other hand, the case of 2012 proves that fast planting can sometimes mean absolutely nothing. Farmers were able to completely plant the corn crop by the third week in May that year - one of the fastest plantings on record - only to end up harvesting one of the worst corn crops in U.S. history due to historic drought and heat.