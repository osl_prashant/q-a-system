WENATCHEE, Wash. — Random-weight pouch bags dominate Northwest cherry sales, but fixed-weight clamshell packages showed considerable growth in 2016.
Nielsen scan data showed random-weight dark sweet cherries contributed the bulk of category sales at $670 million out of $815 million in total sales (82%), but fixed-weight cherries represented 26% of incremental cherry growth and 12% of cherry dollars.
“Pouch bags still dominated red cherry sales for sure,” said Mike Preacher, director of marketing and customer relations for Domex Superfresh Growers, Yakima, Wash.
However, he said fixed-weight clamshells have been driving incremental growth in cherry sales.
Pouch bags are typically shipped in the Euro carton containing 12 bags at 2.25 pounds per bag, said Bruce Turner, national salesman for Wenatchee, Wash.-based Oneonta Starr Ranch Growers.
“It has become the industry standard in a two- to three-year window,” he said.
Discount or value proposition retailers typically use clamshells, while most supermarkets use random-weight pouch bags, said Randy Abhold, vice president of sales and marketing for Rainier Fruit Co., Selah, Wash.
“All have done very well, and we believe that we will see more of the same,” he said.
Abhold said more retailers are looking for higher end or premium product in random-weight pouch bags over the past few years.
“You see more retailers looking for 10.5-row, 10-row cherries, and we don’t expect to see any decrease in that anticipated request.”
Consumers love the convenience of the pouch bag, said George Harter, vice president of marketing for Wenatchee, Wash.-based CMI Orchards.
“We don’t see (cherry) packaging changing a whole lot,” Harter said.
CMI offers a wide variety of packaging options, including a 6-ounce Happy Bee package that won a Produce Marketing Association innovation award in 2015.
Packaging is all about flexibility, said Bob Mast, president of CMI Orchards.
“Early in the deal when cherries are very expensive, and the f.o.b. prices are higher, some retailers will want to carry smaller package sizes and then convert to large package sizes when we get into volume and the pricing comes down,” he said.
Packing line automation allows a myriad of sizes for the pouch bag, Mast said. The 6-ounce pack is a convenience pack for snack centers by supermarket deli sections, convenience stores and retailers like Starbucks, Mast said.
“We’re not trying to get the consumption level down, we are trying to offer (retailers) a secondary display if they have a snack center,” he said.
The smaller 6-ounce pack adds to a pouch bag lineup that also includes a 1-pound, a 2-pound and a 2.25-pound package.
Clamshells for cherries are offered in 1-,2-,3- and 4-pound options, both for organic fruit and also for a colossal cherry program for jumbo fruit typically sold in 2-pound or 3-pound clamshells.
“The pouch bag is definitely the primary method, but clamshells are still a big part of it too, depending on customers and their preferences,” said Brianna Shales, communications manager for Stemilt Growers, Wenatchee.
Shales said the firm’s proprietary Skylar Rae variety is packed in a unique globe-shaped clamshell to help the cherry stand out from the crowd.
Mac Riggan, director of marketing for Chelan Fresh Marketing, Chelan, Wash., said clamshells are often preferred by club stores.
“If the crop is short they will take them in 3-pound packages to keep the price point down,” Riggan said.
“If the crop is long, they will take the 4-pound clamshell,” he said. “It’s a serious discussion early in the year.”
Chelan Fresh offers an 8-ounce package, a half dry pint to appeal to consumers who want a lower price point.
“With about 30% of people buying cherries, you wonder why the other 70% aren’t buying,” Riggan said.
Consumers who may not want to pick up a 2-pound plus random-weight pouch bag for $8 may give the smaller package a try.
“So if you can get an introductory product like that, almost anybody can spend $1.50,” he said.
Looking ahead, Riggan thinks packaging will be the basis of the most innovation for the cherry industry.