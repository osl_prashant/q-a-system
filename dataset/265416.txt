State asking feds to revise 1980s quotas for fluke
State asking feds to revise 1980s quotas for fluke

The Associated Press

ALBANY, N.Y.




ALBANY, N.Y. (AP) — State officials are asking the federal government to change its quotas to allow New York-based commercial fishing companies to haul in more fluke.
Gov. Andrew Cuomo and Attorney General Eric Schneiderman have filed a petition seeking revised allocations for fish also known as summer flounder.
The Democrats say the current quotas unfairly limit the New York fishers' allocation because they're based on outdated data from the 1980s. The New York officials say scientific studies have shown the summer flounder fishery has shifted north toward New York waters since then.
Cuomo and Schneiderman say the current quotas hurt New York's commercial fishers and seafood dealers. They say increasing the allotment for New York-based commercial fishing operations will allow more fish to be landed in ports closer to where they are caught.