Don't go there, boys and girls. Don't reopen the 2014 farm bill.
 
That's the underlying message of a letter sent by sent Feb. 21 to Senate and House leaders from a broad alliance of ag and farm groups, including the American Farm Bureau Federation, the United Produce Association, the National Sustainable Agriculture Coalition and Western Growers. From the letter, some excerpts:
 
"The undersigned organizations..... strongly urge you to reject calls for additional cuts to policies within the jurisdiction of the Senate Committee on Agriculture, Nutrition and Forestry or the House Committee on Agriculture," the letter said.
 
"We have all begun preparing for the 2018 Farm Bill and recognize that passing a bill with additional funding reductions would be extremely difficult, if not impossible. 
 
"Therefore, as the Senate and House Agriculture Committees begin preparing for the 2018 Farm Bill, it is imperative that the committees not be hamstrung by further budget or appropriations cuts to any farm bill program. Instead, we strongly encourage you to recognize the substantial savings already achieved, which far exceed expectations, and to provide the committees th opportunity to complete their work through regular order, without arbitrary budget cuts or caps. With the agriculture and rural economy struggling, households across the country struggling to meet their basic needs for nutrition, and farm income down 46 percent from only three years ago, it would be perilous to hinder development and passage of the 2018 Farm Bill with further cuts."
 
"We know the committees will once again face challenging budgetary and policy choices in the development of the 2018 Farm Bill. That is why it is so important you ensure the committee process for the farm bill can proceed with some budget flexibility."
 
 
 
Agvocates are worried congressional appropriators may use pending 2017 and 2018 appropriations bills to take mandatory money out of farm bill programs.
 
Don't go there, for real.
 
 
 
 
---
 
Just think of your business in ten years. The USDA just did.
 
USDA long term projections on fruit and vegetable production have recently been released. 
 
From the USDA:
 
"Over the next 10 years, U.S. fruit, nut, and vegetable production, measured by farm weight (in pounds of product), is projected to rise by an average of 0.6% per year. Expanded U.S. aggregate production is largely supported by gains in vegetables, which are projected to grow by an average of 0.7% per year to reach production of 141 billion pounds in 2026."
 
The growth in U.S. fruit and vegetable production appears to be lagging somewhat behind the expected rate of U.S. population gain over the next decade. The U.S. Census Bureau projects the annual rate of population growth from 2017 to 2027 ranges from a high of 0.81% in the next couple of years to a low of .72% by 2027. 
 

USDA projections of horticultural trade - including fruits and vegetables but also plants, nuts and other products - reflect a U.S. trade deficit of $15.7 billion in 2015 widening to $29.3 billion by 2026.
 

Left unsaid by the USDA are any predictions of fresh produce import growth. However, it appears the U.S. will need imports of fruits and vegetables to increase at a faster rate than the domestic growth rate to satisfy the demand from more American mouths to feed.