DAP, MAP and potash were all lower on the week.

DAP $42.04 below year-ago pricing -- lower 9 cents/st on the week to $447.48/st.
MAP $34.38 below year-ago -- lower $1.49/st this week to $468.31/st.
Potash $18.24 below year-ago -- lower 33 cents/st this week to $335.41/st.


MAP led declines this week led by Missouri, which fell $9.64 and Nebraska softened $4.51. Five states were unchanged as only Minnesota firmed, adding $1.95.
Potash was led lower by Missouri, which fell $6.15 and Iowa, which softened $3.57. Two states were unchanged as Indiana gained $2.10, South Dakota firmed $2.02 and Michigan added $1.13.
DAP fell on declines in Indiana to the tune of $4.13 as Minnesota softened $2.19 and Missouri fell 52 cents per short ton. Six states were unchanged as Indiana gained $4.13. Minnesota firmed $2.19 and Missouri added 52 cents.
Demand for preplant P&K has been exhausted and our fears that a more normalized DAP/MAP spread would accelerate the uptrend which began in January. This week's declines can hardly be called a downtrend just yet, but according to the three-year averages, phosphate does tend to peak in May.
North American producers are optimistic that phosphate prices will recover, but we take that enthusiasm with a grain of salt considering they have shareholders to encourage. Wholesale DAP and MAP are lower on the week and just slightly above last year's price at this time. Feedstocks including phosphate rock, ammonia and sulphur are sideways to lower, which will weigh on retail prices for future production.
There is also the premium phosphate holds to the rest of the fertilizer segment to consider. If we are correct in our analysis which suggests nitrogen has placed a seasonal top, and since nitrogen has softened this week, we may expect a form of spillover pressure on DAP and MAP. The two phosphate products have realigned their price spread to a more normal level, which could signal either a change of direction or an acceleration of the current trend. At this point, is appears phosphate prices will decline slightly in the coming weeks, but may resist correcting itself sharply lower to reflect price softness in the rest of the fertilizer space.


By the Pound -- The following is an updated table of P&K pricing by the pound as reported to your Inputs Monitor for the week ended May 5, 2017.
DAP is priced at 46 3/4 cents/lbP2O5; MAP at 43 3/4 cents/lbP2O5; Potash is at 28 1/4 cents/lbK2O.





P&K pricing by the pound -- 5/11/2017


DAP $P/lb


MAP $P/lb


Potash $K/lb

 



Average


$0.46 3/4


$0.43 3/4


$0.28 1/4

Average



Year-ago


$0.50 3/4


$0.47 1/4


$0.29 1/4

Year-ago