LENEXA, Kan. — Yes, there’s something different about The Packer this week.
 
You’re holding the all-new print version, and it’s got more of what you want. 
 
How do we know that? We asked!
 
Based on a reader survey we conducted late last year, we found that readers of The Packer would like to see more retail news, more new products, more consumer trends and more about pricing of fresh fruits and vegetables. 
 
We’ve expanded all these areas, as you’ll see on page A2, A3 and A10.
 
We’ve also incorporated our more modern logo that we’ve used for our digital products that better ties in with all our produce brands, including Produce Retailer magazine and the Produce Market Guide, and a new brand promise: The most trusted news in produce.
 
Along those lines, this print version will better tie into all the media we use at The Packer, from online to newsletters to video to social media and events. 
 
We know reader preferences are changing, and we’re committed to delivering the important produce industry news in whatever format you want it.
 
We hope you find this new version of The Packer to be more useful, more engaging and easier to read. 
 
We were proud to see that in our reader survey, The Packer is by far the most trusted media brand in the produce industry. 
 
We didn’t earn that overnight, and we take that responsibility very seriously. 
 
We not only want The Packer to be the media brand you have to read, but we also want it to be the one you love to read.