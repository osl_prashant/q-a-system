It may not yet have the prestige of Cuties or Halos mandarins, but the Super Nova variety launched in January by LoBue Bros. Inc., Lindsay, Calif., surpasses competitors when it comes to flavor and sweetness, at least if you ask Robert LoBue, general manager of LoBue Farms, the company's growing operation.
It was LoBue who named the variety, officially known as USDA 88-2, for LoBue Bros., and who has championed its cause since he discovered it at the University of California Lindcove research station several years ago.
The Super Nova, sold under the LoBue Citrus label, actually was bred by U.S. Department of Agriculture researcher Jack Hearn in Orlando, Fla., in 1966. It's a cross between the Nova and the Lee varieties.
Then called 6-13-44, the variety basically sputtered out in Florida, so Hearn sent budwood to the University of California-Riverside, where it was renamed USDA 88-2, and to Lindcove.
USDA 88-2 is larger than most mandarins - almost the size of an orange - and it has a distinctive bold flavor that comes from its Nova parentage, LoBue said.
It's also very fragrant and has a dark orange color.
Although both the Nova and the Lee are seeded varieties, "together, they are basically seedless," LoBue said.
Super Nova has a high brix rating - about 15% - and "tastes fantastic," he said.
"But it came to the party a little bit late," so now the company has set out to establish a niche for the variety and is trying to "get people to understand it and buy it."
Although it yields better in California than in Florida, it's still not a big producer.
"It's a great piece of fruit," LoBue said. "If we can get the price up just a little bit more, we can make a go of it and make it more commercially acceptable."
LoBue Bros. has 70 acres of the mandarins, though not all are in production yet.
A couple of other small growers also offer them, but not under the Super Nova name.
They're available for about six weeks starting in early January, but LoBue is hopeful that, as the trees mature, he'll be able to offer the Super Nova starting before Christmas.
To help the variety "gain traction," the company is focusing sales in Southern California for now, where it's available in a few chains.
By offering the product consistently in certain stores, LoBue hopes consumers will develop an affinity for it and return to buy more week after week.
"Were hoping for the best," he said.