The I-29 Moo University collaboration will be offering their Winter Workshop Series January 8-12, 2018. They encourage all dairy producers, students, stakeholders and industry personnel to attend.
The series focus is From Field to Bunk: Growing and Feeding Dairy Quality Forages. The workshop series will take place in five locations including: Mandan, North Dakota; Watertown, South Dakota; Pipestone, Minnesota; Orange City, Iowa and Norfolk, Nebraska.
Register by December 29, 2017. Registration is $50 per person and $25 for students. Late registration is late $65 and $30 for students. Late registration fees begin December 30. To register, visit the iGrow Events page.
Workshop Series Details

North Dakota workshop will be held January 8, 2018 in Mandan at the Baymont Inn & Suites (2611 Old Red Trail Northwest Mandan, North Dakota 58554);
South Dakota workshop will be held January 9, 2018 in Watertown at the Codington County Extension Complex (Kitchen Mtg. Room) (1910 West Kemp Avenue, Watertown, SD 57201);
Minnesota workshop will be held January 10, 2018 in Pipestone at the Pipestone Veterinary Services (1801 Forman Drive, Pipestone, MN 56164);
Iowa workshop will be held January 11, 2018 in Orange City at the Sioux County Extension Office (400 Central Ave. NW, Suite 700, Orange City, IA 51041); and
Nebraska workshop will be held January 12, 2018 in Norfolk at the Lifelong Learning Center at Northeast Community College (NECC), (601 East Benjamin Ave., Norfolk, NE 68701). 

Learning Objectives: To improve the sustainability of the dairy production system. Attendees can expect the following:

Learn to incorporate cover crops and new forage genetic lines into the forage production system for dairies.
Producers will increase their understanding of forages and cover crops in dairy rations.
Improve dairy and labor management skills in the areas of feeding management and safety protocols. 

Workshop Agenda 
9:30 a.m. - Registration & Refreshments
10 a.m. - New Forage Genetic Lines and how they Impact the Dairy Industry - Bruce Anderson, Professor of Agronomy, UNL Extension Forage Specialist

Learn how forage genetic improvements in corn silage, sorghums and cover crops can influence the soil health and dairy diet performance in your operation.

10:45 a.m. - Cover Crops - incorporating them into your Forage Production System - Sara Berg, SDSU Extension Agronomy Field Specialist

Learn about cover crop incorporation into your fields, rotation considerations and planting methods.

11:30 a.m. - Break
11:45 a.m. - Incorporating Cover Crops into Dairy Rations - James C. Paulson, Associate Professor Forage Specialist and Nutritionist

Incorporate the nuts and bolts of cover crops into your dairy farm, maximizing nutrition and profitability.

12:30 p.m.  - Lunch
 1:30  p.m. - Sponsor recognition
 2:00  p.m. - Silage Pile Safety training for you & your employees - Keith Bolsen, PhD, Professor Emeritus, Kansas State University, "The Silage Man,"  Nationally known speaker in silage production and safety practices.

Is your silage program safe? Silage safety practices and considerations to come home safely at the end of the day for you, your employees and family members.

 2:45 p.m. - Evaluating Dairy Diets from the Nutritionist, to the Employee, to the Cow.- Co-presented -  Fernando Diaz, DVM, PhD - Dairy Nutrition and Management Consultant - Rosecrans Dairy Consulting  & Tracey Erickson, SDSU Extension Dairy Field Specialist

Understand feeding inefficiencies as you deliver diets to your dairy herd.

3:30 pm - Evaluation & Adjourn
For more information, contact the I-29 Moo University Winter Workshop: From Field to Bunk, Program Committee Chairs; Tracey Erickson, SDSU Extension Dairy Field Specialist by email or 605.882.5140; or Kimberly J. Clark, UNL Extension Dairy Educator by email or 402.472.6065.