<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


LAS VEGAS - The independent retailers who make up the National Grocers Association feel well positioned to take on the challenges of modern food retailing.
Workshops and general sessions told success stories in sourcing locally, offering value-added food for time-starved consumers and creating online ordering and delivery systems to compete with larger and non-traditional rivals.
On the expo floor, the 30 companies in the Produce Marketing Association pavilion treated the event as more of an educational than sales opportunity.
Few produce buyers strolled through the aisles.
"We've seen some presidents of small chains," said Lance McMillan, director of North American sales for Sunkist Growers, Valencia, Calif. He said he had many productive meetings.
Juliemar Rosado, director of retail operations and international marketing for the National Watermelon Promotion Board, Winter Springs, Fla., said many independent retailers don't know how the board can help them.
"These meetings lets them know we have resources to help them sell more watermelons," she said.
Carrie Robertson, in marketing for Veg Fresh Farms, Corona, Calif., said it was the company's first time exhibiting at NGA, and she heard a lot of excitement for its new line of Handy Candy brand blueberries in resealable cups. (photos A4)
Whether produce, meat or bakery, fresh food continues to be the way to appeal to consumers, many workshop speakers said.
"You have to make something or be known for something to differentiate yourself," said Dan Koch, vice president of bakery-deli for Associated Wholesale Grocers.
Peter Larkin, CEO of NGA, said the event saw 3,400 attendees and a sold out expo floor. Next year the show will be back in Las Vegas, Feb. 11-14, but in 2019, Larkin said it needs larger space so it's moving to San Diego, Feb. 24-27.