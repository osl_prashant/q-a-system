There are many congressional leaders who aren’t supporting President Trump’s tariff increases on steel and aluminum, including House Speaker Paul Ryan.

Trump is adamant saying the U.S. is “not backing down,” however he is tying the NAFTA negotiations to the threat of steel tariffs.

Canada and Mexico won’t be immune to the tariffs on metal imports unless a “new and fair” free trade agreement is signed, according to the president.
 

We have large trade deficits with Mexico and Canada. NAFTA, which is under renegotiation right now, has been a bad deal for U.S.A. Massive relocation of companies & jobs. Tariffs on Steel and Aluminum will only come off if new & fair NAFTA agreement is signed. Also, Canada must..
— Donald J. Trump (@realDonaldTrump) March 5, 2018


...treat our farmers much better. Highly restrictive. Mexico must do much more on stopping drugs from pouring into the U.S. They have not done what needs to be done. Millions of people addicted and dying.
— Donald J. Trump (@realDonaldTrump) March 5, 2018


Speaker Ryan says the U.S. economy would suffer from proposed import taxes on metal, and he is “extremely worried about the consequences.”

The Association of Equipment Manufacturers (AEM) is also concerned about the impact the tariff will have on farm equipment, especially with our NAFTA partners.

“As we look at this discussion with China, Canada is 16 percent of the imports which come from Canada and another 9 percent from Mexico,” said Dennis Slater, president of AEM. “25 percent of that steel comes from our partners in NAFTA.”

Slater says 10 to 20 percent of the price of a machine will be the cost of steel. He says domestic steel manufacturers are raising rates and prices in anticipation of the impending tariff.

According to Trump administration officials, the effects of higher costs on U.S. products will be a small fraction of costs. One example that was given by White House trade advisor Peter Navarro was the aluminum tariff would increase the price of a six-pack of beer by 1.5 cents.

Watch the full story on AgDay above.