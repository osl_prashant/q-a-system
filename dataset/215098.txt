August Battaglia, doing business as QMP Sales, has satisfied a reparation order issued under the Perishable Agricultural Commodities Act, according to a U.S. Department of Agriculture news release.
The USDA sanctioned the Westmont, Ill., company in November 2015 for failing to pay a $3,080 reparation award to a Florida seller.
Having now paid the debt, the company can once again operate in the produce industry after getting a PACA license.
August Battaglia was listed as the sole proprietor of the business and may now be employed by or affiliated with any PACA licensee.