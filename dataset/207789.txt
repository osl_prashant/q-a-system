Lemon volumes are lighter coming out of Chile this year, although there are no quality issues apparent, marketers say.
“The Chilean lemon crop is also coming along just fine,” said Patrick Kelly, citrus commodity manager for Eden Prairie, Minn.-based Robinson Fresh.
Robinson Fresh was anticipating its first loads of lemons, barring any problems at U.S. ports, he said.
“The quality is looking good with light scar, but there will be some light green on these first loads,” Kelly said.
The size structure will be 115, 140, 95, 165, 75, Kelly said. He also said Chile’s lemon crop is 5% lighter than last year, “so let’s take advantage of fruit while we have the availability.”
Steve Woodyear-Smith, vice president of categories with the Vancouver, British Columbia-based Oppenheimer Group, said supplies of lemons won’t vary throughout the deal.
“I think navels and lemons are expected to stay pretty tight,” he said.
Nolan Quinn, commercial manager for Fresno, Calif.-based Summit Produce, offered a similar assessment.
“I think the overall navels and lemons will be similar,” he said.
The fruit’s quality is good, Quinn said.
It’s a relatively short deal, though, compared to other citrus items out of Chile, Quinn said.
“Not much time left on lemons. That’s kind of winding down — probably about two more weeks,” he said.
The mandarin season out of Chile, by contrast, lasts into October, he said.
Tony Liberto, import citrus manager with Reedley, Calif.-based Dayka & Hackett, described Chile’s current lemon deal as typical.
“Usually during the summer, there isn’t much fruit in the market, and the South American fruit gets here and prices go down as markets fill up. There is typically strong demand early,” he said.
“By mid-August, you start seeing the Mexican and California lemons. Our window’s not too long from Chile. It looks like price and movement is similar to the last couple of years.”
Weather issues have hampered the Chilean lemon crop, though not seriously, Liberto said.
“In the last four or five years, it’s been really dry this time of year, but they’ve gotten a lot of rain this year,” he said. “It’s actually a blessing because Chile gets the rain they need, even if it does affect the crop.”
 
Pricing
As of July 18, 17-kilogram cartons of fancy-graded lemons from Chile were $36-40 for sizes 75s and 95s; $34-38, 115s; $32-36, 140s; $30-34, 165s; and $28-30, 200s, according to the U.S. Department of Agriculture.
A year earlier, the same item was $28-32, size 75s; $34-36, 95s; $32-36, 115s; $32-34, 140s; $28-32, 165s; and $24-29, 200s.
Chile is expected to export about 250,000 pounds of lemons, navels, clementines and mandarins this year, said Karen Brux, managing director of the San Carlos, Calif.-based Chilean Fresh Fruit Association.