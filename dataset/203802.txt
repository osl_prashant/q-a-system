Most Idaho and eastern Oregon onion grower-shippers expect a similar mix of yellow, red and white onions this season.
Shay Myers, owner of Owyhee Produce, Nyssa, Ore., said the company is significantly increasing production of sweet yellow onions, which should ship through mid-winter.
"It's a true, tested, stickered sweet, and not a lot of growers in the area focus on sweets."
California has some sweet onions in late summer but is done shipping by September, Myers said, leaving buyers looking for another domestic alternative.
"There are a lot of people who want a domestic sweet."
Nyssa-based Snake River Produce ships a "fair amount" of long-day sweet onions, but the deal won't kick off until September, Tiffany Cruickshank, transportation manager and saleswoman for the Nyssa, Ore.-based company.
The company's volumes of long-days should be similar to last season, she said, and Snake River also expects a similar mix of yellows, reds and whites in 2016, with perhaps slightly higher acreage overall than in 2015.
Chris Woo, national sales manager for Idaho and Oregon for Murakami Produce, Ontario, Ore., said Murakami expects to ship about 80% yellows, 15% reds and 5% whites from the Treasure Valley this season.
The red onion category is seeing especially strong growth for Murakami, Woo said.
"Reds have really taken off. Our customers who buy yellows like to buy reds, too."
A big driver of demand for reds is increased foodservice use at places like Subway, Pizza Hut and salad bars, Woo said.
Paul Skeen, owner of Skeen Farms Inc., Nyssa, planted his usual mix of yellows and reds this season.
But industrywide, he thought yellow and red acreage would be up slightly in the Treasure Valley and white acreage about the same.
Skeen Farms stopped growing white onions about 20 years ago, Skeen said.
John Vlahandreas, onion sales manager for Wada Farms Marketing Group LLC, Idaho Falls, Idaho, is optimistic that Wada Farms' different Treasure Valley onion varieties will be well paced this season.
"I don't see everything coming off at once, like in some past years."
Wada Farms' varietal mix of yellows, reds and whites should be similar to last year, he said.
"It never changes that much."