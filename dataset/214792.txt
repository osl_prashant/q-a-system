Marketers say it’s important for retailers to promote product at retail as “Fresh from Chile” as millennials are driving sales and they are conscious of where their food comes from.
Eric Coty, executive director of South American imports with The Oppenheimer Group, Vancouver, British Columbia, said millennials and people of all generations can relate to stories of growers and their traditions and practices.
“It’s advantageous to remind shoppers that the imported fruit is fresh — especially items like apples, where ‘just picked’ is a key message that separates newly harvested Chilean fruit from domestic apples,” Coty said. “The eating experience differs enough that it should be pointed out for sure.”
Bil Goldfield, director of corporate communications for Dole Food Co., Westlake Village, Calif., suggested touting Chile to cause-conscious shoppers as a pioneer in Fair Trade and social responsibility.
Also, Goldfield said consumer trends continue to move towards convenience, portability and freshness.
“This is why we are in the process of developing innovative packaging that allows our customers to carry our products on-the-go, and enjoy the full benefits of enjoying our delicious fresh fruit in the most convenient way possible,” he said.
Los Angeles-based The Giumarra Cos.’ cherry program starts mid-November with product from Chile and Argentina and runs through January (sourced from New Zealand).
“Cherries are a great holiday item due to their rich red color and versatility in recipes,” said Megan Schulz, director of communications. “Traditionally, people think of using cherries for pies and desserts, but they are also great in beverages, salads and savory dishes.”
Giumarra will start its tree-ripened program for peaches and nectarines shipped by air during the second week of December. 
“This program is a good option for retailers who want early arrivals of stone fruit prior to vessel shipments,” Schulz said. “We will receive vessel fruit after the Christmas holiday.”
Chilean grapes will arrive on the East Coast in mid-December, Schulz said, with West Coast arrivals in late December.
As for packaging, more pack styles are available now than ever, Oppy’s Coty said.
Oppy is exploring the use of top-seal packaging that could eventually replace currently popular pouch bags. 
“Top-seal options reduce waste while giving the consumer a good look at the product inside,” Coty said. “Also, growers and marketers are finding success with product shipped in bulk and packed in the market — cherries, blueberries, citrus and grapes, for example.”