Earlier this week, Dan Murphy wrote a column called Valentine’s Day Advice, in which he shared how to cook the perfect steak for that special someone on Valentine’s Day.
I love a good steak as much as anyone, but being editor of Farm Journal’s PORK, of course I love to eat the product our brand represents.
One of my favorite meals centers around pork tenderloin with Béarnaise sauce. It’s delicious, and it couldn’t be easier to prepare.
I first discovered the recipe in a small pamphlet called, “Best Ever Pork Recipes,” from the National Pork Producers Council (before there was a National Pork Board). All the recipes in this little book were good, but the pork tenderloin was a standout.
So Easy
Pork is such a good buy right now. Why wouldn’t you want to fix a delicious, standout meal starring pork that’s as good as a steak, at far less cost? Very often pork tenderloin comes in a package of two, so it’s perfect when you’re hosting another couple.
Here’s what you do.
Heat a Dutch oven on medium with two tablespoons of olive oil and two tablespoons of butter. Toss in the pork tenderloins and brown on all sides. Add one-half cup water and one-half cup of red wine, and crush some black pepper over the top of the tenderloins. Cover and lower heat to simmer. Cook for 45 minutes.
During the last 5 minutes of cooking time, prepare the Béarnaise sauce. I discovered that Knorr makes a knock-out Béarnaise sauce mix that just requires butter and milk. Yes, it’s cheating if you’re into making everything from scratch but in my opinion, it tastes as good as the original.
Remove the tenderloins from the pan and let rest on a cutting board for at least 10 minutes. Cut into ½ to ¾ in. slices and serve with Béarnaise sauce over the top and on the side.
I like roasted asparagus or roasted Brussels sprouts with onion and bacon as sides, and maybe roasted baby potatoes.
Dessert? It’s Valentine’s Day! Go for chocolate mousse, or in my case, my husband, Lyle, loves homemade coconut cream pie.
I hope you and your “significant other” have a great day!