Young farmers are especially vulnerable to the ramifications of not having a succession plan. Just ask Shannon Ferrell, an ag law professor with Oklahoma State University Extension.
Ferrell’s brother, Adam, was in line to take over the family operation in western Oklahoma. Adam was 31, single and working to expand the family’s operation—until a farming accident took his life in 2015. He had no succession plan. “
We’re scrambling and paying off farm debts because a single guy didn’t have a plan,” Ferrell says.
Real Risk
Young farmers typically have higher debtto-asset ratios along with young dependent children, Ferrell says. This combination poses financial and personal risk. “Now is the time to get the ball rolling,” Ferrell advises. “You need to plan before the need is there.”
To get started, build a family and farm business profile and start discussions with stakeholders who would be affected in the event of your absence, advises Dick Wittman, a family business consultant and Idaho farmer. This eases the pressure because there isn’t a need to make immediate decisions.
Include biographical information such as family details, addresses, goals and personality profiles; business information such as organizational structure, tax returns, financial position and budgets; personal and business insurance coverage; and estate-planning files such as wills, trusts and land titles.
“Put all this information in a threering binder,” Wittman says. “When you can put this on paper, you can describe where you are today and define some preliminary ideas of goals for the future.” 
Dedicate Time
Take the initiative and do this business’s grunt work to prove to your family you are committed to developing a plan.
“This process can feel overwhelming,” says Lance Woodbury, a farm management consultant in Garden City, Kan. “Catching people at a place where they are ready to start working on this stuff is important.”
The correct place to kick start this conversation is during a family meeting. “Tell your family you want to have a meeting to talk about the future of the farm,” Ferrell says. Select a neutral location and pick a date that does not correspond with a holiday. You also might want to hire a facilitator to run the meeting.
Understand succession planning is a process. It is not a one-and-done task that can be completed in an afternoon.
“Make sure you circle back every year or six months,” Woodbury advises.
Use This Checklist to Plan for the Future
Young farmers should add these important tasks to their to-do list to avoid unnecessary stress and legal issues, says Shannon Ferrell, ag law professor with Oklahoma State University Extension.

Nominate a guardian for minor children. You should also designate funds for the guardian to raise your child. Trusts can be good tools for this.
Identify powers of attorney for the farm business and also for decisions related to health care.
Consider a life-insurance policy for yourself and your spouse. These funds can be used to satisfy operational debt. Additionally, life insurance becomes more expensive as you age, so purchasing a policy early can offer financial benefits.
Develop a contingency plan for every key person in your operation. If you lose an important player tomorrow, what do you do? Use clear and easy-to-follow instructions.
Create a list of all your online accounts and passwords. Also, list your professional advisers with contact information.
Ensure your farm records are current.
Journal what you do on a weekly, monthly and annual basis. This will be a huge asset to your family and team in the event you leave the business unexpectedly, regardless of the reason

Note: This story appeared in the November issue of Dairy Herd Management.