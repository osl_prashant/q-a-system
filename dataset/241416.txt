National editor Tom Karst and staff writer Ashley Nickle discuss the possible effects of upcoming tariffs, the latest in the Argentine lemon situation, and two companies that made news with avocados this week.
MORE: Trump’s new tariffs spark retaliation worries
MORE: Court rules for USDA, Argentina lemons
MORE: Red Sun Farms expands into avocados
MORE: Mission debuts ‘Emeralds in the Rough’ packaging