Debate stirs over 'America's Harvest Box,' food benefit plan
Debate stirs over 'America's Harvest Box,' food benefit plan

By JULIET LINDERMANAssociated Press
The Associated Press

WASHINGTON




WASHINGTON (AP) — Hawaii's food stamp administrator says he was stunned when he first heard that the U.S. Agriculture Department wanted to replace some cash benefits with a pre-assembled package of shelf-stable goods. That changed quickly to frustration, befuddlement and serious concern.
"This will wreak havoc on the states," said Pankaj Bhanot, who serves as director of Hawaii's Department of Human Services and is in charge of administering the state's Supplemental Nutrition Assistance Program, or SNAP, to roughly 165,000 residents scattered across a series of islands.
SNAP administrators across the country shared Bhanot's reservations about "America's Harvest Box," pitched by USDA officials as a way to cut costs and improve efficiency. Administrators say their programs already are efficient, allowing recipients to purchase whatever foods they want directly from retailers, which benefits families, retailers and local economies.
The proposal, unveiled last month in the Trump administration's 2019 budget, is part of an effort to reduce the cost of the SNAP program by roughly $213 billion over a 10-year period.
Brandon Lipps, administrator of the Food and Nutrition Service at USDA, said the idea was partially inspired by rapidly changing models for how people get their groceries. The USDA last year launched a pilot program that allows SNAP recipients to order provisions online using their EBT, or Electronic Benefit Transfer, cards, which function like debit cards but can only be used to purchase groceries.
He said in an interview that it was designed to streamline the process of getting healthy food into the hands of those who need it most. State administrators, he said, would be responsible for figuring out how to package and distribute the boxes themselves.
But SNAP administrators say the proposal is riddled with holes.
Bhanot had a broad list of questions, ranging from delivery of the boxes, especially during hurricanes, to ensuring that recipients were getting the right type of nutrition. "We'd have to ramp up staff. Where will the money come from?" he asked.
In Minnesota, Chuck Johnson, acting commissioner of the Department of Human Services, called the proposal "a significant step backward in our nation's effort to ensure all Americans have access to nutritious food." He said it would be a major burden on states, which would have to figure out how to deliver the food boxes.
Tom Hedderman, director of food and nutrition policy at the New York State Office of Temporary and Disability Assistance, said there are about 1.25 million SNAP recipients in his state who get more than $90 in benefits each month — the threshold that would trigger a food box. He criticized the proposal for its lack of detail and direction.
"It's clear in the proposal that they would dump the problem of logistics and cost back on to the states," he said.
Babs Roberts, who directs the community services division of the Washington Department of Social and Health Services, said a uniform system for distributing boxes simply wouldn't work in her state, where there are roughly 900,000 SNAP recipients. The cities are too dense for a delivery system to work, she said, while residents living in rural communities would likely have trouble traveling to a centralized location to pick up their box.
"I think it's going to be administratively burdensome," she said. "I don't know if it's any better than what we're doing now."
Sammy Guillory, deputy assistant secretary for the division of family support for the Louisiana Department of Children and Family Services, said he worries that if the proposal were approved, it would take years to iron out the kinks. In particular, Guillory said he is concerned that making such a drastic change from the current SNAP system would force employees to spend significant time and energy adjusting.
"Somehow our staff that determines eligibility would have to learn all these rules, our system would have to be reprogrammed. And that's not even taking into consideration the logistics of getting these boxes to families," he said, adding that more than 400,000 households receive SNAP, or about a quarter of Louisiana's population.
Rus Sykes, director of the American Public Human Services Association, an umbrella organization that includes the American Association of SNAP Directors, said administrators across the country were shocked by the proposal and are concerned that if it moves forward states will be forced into an impossible situation.
"They will not have the ability to administer this program this way," Sykes said, "and the states will be on the line for everything that goes wrong."