California cherry exports don’t rely heavily on China, and that is a good thing this year.
For 2018, China hiked the tariff rates on U.S. cherry imports by 15% in response to U.S. steel and aluminum tariffs on China.
During the April through June period that California is shipping cherries, U.S. cherry exports are spread to nearly 40 countries.
That diversity in export sales should help California marketers this year.
Though China is only the fourth leading market for U.S. cherry exports for the April through June period, it was one of the fastest growing markets.
“Time will tell what the new tariff in China does to us,” said Mike Jameson, director of sales and marketing for Linden, Calif.-based Morada Produce.
“It will affect us a little bit, but I don’t think it will be dramatic.” 
 
if("undefined"==typeof window.datawrapper)window.datawrapper={};window.datawrapper["lNQIX"]={},window.datawrapper["lNQIX"].embedDeltas={"100":769,"200":542,"300":491,"400":447,"500":430,"700":413,"800":413,"900":413,"1000":413},window.datawrapper["lNQIX"].iframe=document.getElementById("datawrapper-chart-lNQIX"),window.datawrapper["lNQIX"].iframe.style.height=window.datawrapper["lNQIX"].embedDeltas[Math.min(1e3,Math.max(100*Math.floor(window.datawrapper["lNQIX"].iframe.offsetWidth/100),100))]+"px",window.addEventListener("message",function(a){if("undefined"!=typeof a.data["datawrapper-height"])for(var b in a.data["datawrapper-height"])if("lNQIX"==b)window.datawrapper["lNQIX"].iframe.style.height=a.data["datawrapper-height"][b]+"px"});
The tariff on China is not going to affect everyone in the same way, said Matt Nowak, domestic and export account manager for Grower Direct Marketing LLC, Stockton, Calif.
“We might see a little dip in supply being shipped to China but I still think there will be good movement into that market no matter what the tariffs end up doing,” he said.
According to trade statistics from the U.S. Department of Agriculture, leading export destinations from cherry exports from April through June in 2017 were:

South Korea: 2017 value $107.5 million, down 11% from 2016. Volume: 1.41 million 18-pound cartons, down 8% from 2016.
Canada: 2017 value $78.43 million, up 51% from 2016. Volume: 1.7 million 18-pound cartons, up 75% from 2016.
Japan: 2017 value $41.13 million, down 24% compared with 2016. Volume: 587,277 18-pound cartons, down 22% from 2016.
China: 2017 value $32.6 million, up 133% from 2016. Volume: 498,177 18-pound cartons, up 147% from 2016.
Taiwan: 2017 value $16.5 million, up 121% from 2016. Volume: 271,944 cartons, up 163% from 2016.
Hong Kong: 2017 value $15.1 million, up 165% from 2016. Volume: 213,155 18 pound cartons, up 197% from 2016.
Australia: 2017 value $8.7 million, up 17% from 2016. Volume: 135,000 18 pound cartons, up 11% from 2016.