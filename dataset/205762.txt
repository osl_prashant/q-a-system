This year's winter wheat crop in the Dakotas is smaller than the previous year's.The Agriculture Department estimates South Dakota's crop at 1.1 million acres, down from 1.4 million acres.
North Dakota's crop is projected at 190,000 acres, down slightly from 200,000. Winter wheat is a minor crop in North Dakota, where spring wheat and durum wheat dominate.
Winter wheat is seeded and emerges in the fall, goes dormant over winter, begins growing again in the spring and is harvested in the summer.
Nationally, this year's crop is estimated at 36.6 million acres, down from 39.5 million.