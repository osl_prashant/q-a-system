Wally the pig thrives 1 year after escape to freedom
Wally the pig thrives 1 year after escape to freedom

The Associated Press

SIOUX FALLS, S.D.




SIOUX FALLS, S.D. (AP) — Wally the pig is living the life of fiction since making his leap to freedom from a cattle truck near Sioux Falls a year ago.
Wally went from being destined for the slaughterhouse to now holding an ambassador position for the sanctuary that rescued him, the Argus Leader reported.
"He has a really incredible story, and people identify with that," said Beth Berhow, the program manager for the SoulSpace Farm Sanctuary in Wisconsin.
Wally was held in a cattle truck headed to Sioux Falls last year when he forced open a gate and jumped, skidding along Interstate 90. A witness called 911, leading animal control officers to rescue Wally before a car could hit him.
The pig got his name from Andy Oestreich, the senior humane officer at the Sioux Falls Area Humane Society. SoulSpace Fonder Kara Breci adopted Wally after nobody claimed him.
Wally was initially standoffish during interactions with humans, according to sanctuary workers. But now the animal is trusting and responds to his name.
"He is very sociable," Berhow said. "He loves people. That happened really quickly."
Wally will headline a SoulSpace fundraiser April 21 in Minnesota. The pig's celebrity status has generated a documentary film and several stories in the Upper Midwest, helping SoulSpace raise money to protect castaway farm animals.
Berhow said the sanctuary is "just so grateful he made his way to us."
___
Information from: Argus Leader, http://www.argusleader.com