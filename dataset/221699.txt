TORONTO — Local produce importers began 2018 in an uproar over record-high freight rates, which they blame on the Dec. 18 implementation of electronic logs on trucks traveling in the U.S.
“It’s a disaster, a fiasco, a mess!” said one wholesaler after another at the Ontario Food Terminal on Feb. 1.
At the same time, many companies reported surprisingly strong produce sales in December and January, much better than last year. A slightly higher Canadian dollar also may have helped.
“Overall we’re very strong and will increase over last year despite the higher freight,” said Tony Fallico, president of F.G. Lister and Co.
Though freight rates have plateaued in recent weeks, Fallico said they’re probably still 25% higher than last year.
By the third week of January, lettuce was worth less than the freight to get it to Toronto, said Mimmo Franzone, produce director for Longo’s 34 supermarkets.
“The initial shock has worn off,” said Ted Cira, general manager sales and procurement for Dominion Citrus, “but customers still squawk when they walk in.”
Peter Streef, president of Streef Produce, blamed e-log rules for the “extortionate” freight rates.
“It’s an excuse to pass costs on to us and make us pay for all of it,” Streef said.
Transporting Mexican vegetables from Nogales, Ariz., to Toronto in January cost $7,000-9,800, he said, compared to $5,000 last year, and he’s been forced to hire teams of drivers, adding $2,000-4,000 more per load, to ensure product arrives on time.
“It takes a week to move a load from San Diego to Toronto,” he said, “and some of it is ridiculous — if a truck takes six hours to load the driver loses six hours of driving time.”
Vince Carpino, partner with Tomato King, said “everything takes way longer to get here, and it’s just going to get worse.” 
Larry Davidson, president of North American Produce Buyers Ltd., called the trucking situation “a fiasco,” but said the regulations are needed.
“No matter how much they inconvenience the industry, you have more confidence in the people behind the wheel, and higher rates allow drivers to make a better living,” Davidson said.
Vince Bruno, president of Italian Produce, said he understands the benefits of the new rules, “but the delays are hurting us, especially out of Florida and McAllen, Texas.”
Steve Bamford, CEO of Fresh Advancements, blames the new rules for tying up imports of everything from citrus to pomegranates at East Coast ports.
“Container orders are being released at one time,” Bamford said, “which means we receive nothing one week then three week’s worth of containers the next, which is negative for the market.”
The result, everyone agrees, will be higher prices passed on to the consumer.
“Tomatoes and a lot of Mexican commodities are two to three times the regular price,” said veteran foodservice buyer Joe Pettinelli, who’s facing a 10% increase on many items, from mushrooms to mixed salads.
Fallico said the f.o.b. on grapes is already over $30. “When you add additional freight and other charges, they’re up to $4.99 a pound,” he said. “Higher retails slow down sales at retail and wholesale.”
Though everyone blames e-logs for the hike in freight rates, John Corsetti, owner of Woodbridge-based Rally Logistics Inc., said it’s too early to tell exactly where the problem lies, and it will take a full year to truly gauge the effect of the new rules.
The busy spring deal will tell the tale, he said.
“If rates get crazy in the $9,000 range again in April then we’ll know it’s going to be a trend that keeps going.”