Increasingly, the mushroom business is all about diversity, said Jane Rhyno, sales and marketing director with Leamington, Ontario-based Highline Mushrooms.
"As Canada and the U.S. continue to become expanding multicultural countries, so do the cuisines to satisfy changing consumer palettes," she said.
Mushrooms fit that scenario ideally, she said.
"Mushrooms - more specifically, exotic mushrooms - play an important part in many ethnic dishes and, nowadays, you can find an assortment of exotic mushrooms at any Asian grocery retailer," Rhyno said.
The standard buttons and portabellas win new mushroom buyers, and then shoppers discover unfamiliar varieties and try them, said Bill St. John, sales director with Gonzales, Texas-based Kitchen Pride Mushroom Farms Inc.
"There will be a certain percentage of people that want to try that next level," he said.
There's a cost difference, particularly in foraged mushrooms, but shoppers wanting to try something special will find plenty among all the specialty varieties now readily available, St. John said.
"They won't use as many, but they'll tie them in and want to take it to that next level once in a while. That's going to increase the sales of mushrooms in general," St. John said.
With sales in the mushroom category steadily increasing - about 3% this year - specialty varieties are moving up, as well, said Bart Minor, president of the San Jose, Calif.-based Mushroom Council.
Stores that cater to the high-end consumer often will carry numerous specialty mushroom varieties, said Mike Basciani, partner in Avondale, Pa.-based Basciani Mushroom Farms.
"High-end stores, the variety is so wide, the mushroom section seems to be getting bigger and bigger," he said.
Restaurants are doing the same, Basciani said.
"Menus love to have a wild mix," he said.
So-called "wild mushrooms," such as chanterelles, matsutakes and porcinis are scarce and pricey, but there are plenty of "richly flavorful" choices among more than 2,000 varieties of edible mushrooms, said Mike O'Brien, vice president of sales and marketing with Watsonville, Calif.-based Monterey Mushrooms Inc.

"Now, several wild mushroom varieties are being cultivated, including oyster mushrooms and maitakes, enokis and shiitakes and (numerous) mushrooms that have been given proprietary trademarked names," O'Brien said.
"These mushrooms are reasonably priced and played around with in mushroom recipes all week."
O'Brien said it doesn't matter that such "wild" mushroom varieties are cultivated.
"They had the flavors and textures of wild mushrooms I have gathered in the Provencal countryside - and they don't require the meticulous cleaning that real wild mushrooms require," O'Brien said.
Monterey plans to aggressively develop its exotic mushroom program in 2017, O'Brien said.
"We want to be the one-source supplier of oysters, shiitakes, brown beech, white beech, maitake, king trumpet and enoki mushrooms," he said.
Monterey also now has updated packaging and offers an organic option.
Morels present "exciting promotional opportunities" for retail, as well as foodservice operators, said Kevin Delaney, vice president of sales and marketing with Avondale-based To-Jo Mushrooms Inc.
"The seasonality of wild mushrooms keeps the category fresh and always evolving," he said.
Exotics still comprise only a small slice of the mushroom business, but they have devoted fans, said Fletcher Street, sales and marketing director with Olympia, Wash.-based Ostrom Mushroom Farms.
"As the acceptance of conventional mushrooms increases to the average consumer, you see the creative chef or foodie home cook is looking for more variety," Street said.
An increase in available recipes showcasing exotics in magazines and food blogs increases the exposure exponentially, Street said.
"This category has nowhere to go but up," she said.