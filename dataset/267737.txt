BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Fri.Thu.No. 2 Soft wheat4.73¼4.65¾No. 1 Yellow soybeans10.03¾10.01¼No. 2 Yellow Corn3.71½e3.72½eNo. 2 Yellow Corn3.87½p3.88½p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted