It's white roses, peonies and foxgloves for UK royal wedding
It's white roses, peonies and foxgloves for UK royal wedding

The Associated Press

LONDON




LONDON (AP) — The royal wedding will take place among white garden roses, peonies and foxgloves, set off by branches of beech, birch and hornbeam.
Prince Harry and Meghan Markle have chosen a self-taught London floral designer for their nuptials. The couple said Sunday that Philippa Craddock will create the church flower arrangements for the May 19 wedding at St. George's Chapel in Windsor.
Her team will include florists from the chapel and from Buckingham Palace for the displays in the chapel and at St. George's Hall. The Royal Parks will also supply some plants taken from wildflower meadows.
Kensington Palace said the designs will reflect the wild, natural landscapes from which many of the plants will be drawn.
The flowers will be given to charities after the wedding.
___
Complete AP coverage of the royal wedding:

                https://apnews.com/tag/Royalweddings