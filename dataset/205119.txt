The Spectral Evolution leaf clip and ILM-105 light source are designed to provide a streamlined and effective approach to scanning vegetation (leaves, pine needles, etc.) when used with a Spectral Evolution field portable spectroradiometer. The leaf clip features:Comfortable spring grip handle
Light touch, tactile feel, pushbutton external trigger
3mm spot size for precise focus on sample area
Rugged, metal clad field swappable fiber optic cable
Built-in reflectance standard
Separate ILM-105 light source keeps heat away from the sample
The leaf clip is easy to use - squeeze the spring grip to open the leaf clip, use the built-in reflectance standard for your reference scan, then turn the swivel mount to the black side, insert your leaf sample and use the remote trigger to take your target scan. The white reference standard is disposable - replaced by applying a peel and stick white reference that is available in packs of ten.
The ILM-105 light source has been completely re-designed and streamlined for field use. It is mounted to your spectroradiometer by a rail and connected to the leaf clip by a power cable and rugged metal-clad fiber optic cable. The ILM-105 has a toggle switch for low/high intensity settings, so you control the light source. It keeps the heat away from your sample so it remains unharmed.
The leaf clip and ILM-105 can be used with the PSR+ and RS-3500 field spectroradiometers in a range of remote sensing applications including:
Ground truthing hyperspectral images
Chlorophyll and moisture analysis to identify healthy and stressed vegetation
Agricultural analysis to prevent over/under fertilization
Forest canopy studies
Plant species identification
Crop health including photosynthesis efficiency
For more information on the leaf clip and ILM-105, visit: http://www.spectralevolution.com/illumination_modules.html