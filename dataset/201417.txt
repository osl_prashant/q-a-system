Crop calls
Corn: 3 to 5 cents lower
Soybeans: 10 to 15 cents lower
Wheat: 3 to 6 cents lower

Very little rain fell on the Corn Belt over the weekend, but weather models added some showers to areas of the western Corn Belt Tuesday and Wednesday, with precip moving south and eastward Thursday and Friday. While some areas may miss out on this week's precip event, traders are not overly concerned given the forecast for mild temps, which limit stress on the crops. A firmer tone in the dollar and weakness in corn and soybean futures weighed on wheat futures overnight. Also this morning, USDA announced that Colombia has purchased 150,000 MT of new-crop U.S. corn.
Livestock calls
Cattle: Lower
Hogs: Lower
Bears have the advantage to start the week in the cattle and hog markets, but pressure should be limited by the discount futures hold to their respective cash markets. Traders are concerned about building beef supplies after last week's cattle slaughter topped year-ago by 8.1%. Additionally, dressed cattle weights rose once again to signal marketings are not current. Hog traders also fear supplies will be building, which is why they expect the cash market to $1 lower to start the week.