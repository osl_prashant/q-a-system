Fairtrade America has joined 1% for the Planet, a global group of more than 1,200 companies in 40 countries, as a nonprofit partner.
Members of the global organization contribute 1% of their revenue to environmental causes, according to a news release.
“We believe deeply that nonprofits, like Fairtrade America, play a critical role in solving the many challenges facing our planet,” Kate Williams, CEO of 1% for the Planet, said in the release. 
“Our core work is to grow the corporate support that enables these nonprofits to accomplish even more.” 
Through the partnership, Fairtrade America partners can count Fairtrade fees and donations toward their 1% committment, according to the release.