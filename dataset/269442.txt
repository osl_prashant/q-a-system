Farmers and ranchers are digging out after Winter Storm Xanto hit a wide swath of the Upper Midwest and the Great Lakes Wednesday through the weekend, bringing record-setting snow falls. The storm is currently working its way through the Northeast.
 

The only pair pin we have successfully made it to. The others are unreachable so far. ?? god help us all..... pic.twitter.com/C1DjJPutAj
— Matt Karo (@matt_karo) April 14, 2018

 
 

This was dad’s best investment I think.  Shocker he bought it the year me and Bryce were out of high school! #Xanto pic.twitter.com/fr7uYGcBb8
— Brady Teveldal (@BradyTeveldal) April 16, 2018

 
Blizzard conditions brought heavy snow, with reports of 25” near Bozeman, Mont.; 14.9” in Minneapolis/St. Paul; 20” near Okreek and Winner; 13.7” Sioux Falls, S.D.; 33” in Amherst, Wis., and 23.5” in Green Bay, Wis. The highest report came from Amherst, Wis., where 33” were recorded along with 5’ to 6’ drifts. See more storm totals from Weather.com.
 

Another heavy snowstorm upon us this morning. Hope our calves and fellow ranchers make it through yet another one. #puttothetest #calving18 pic.twitter.com/mI9U204LjW
— Plateau Cattle Co (@plateaucattleco) April 16, 2018

 

If next winter is anything like this winter.... thinking I’m going to cross everything on a musk ox bull. Getting ridiculous! #calving18 but it’s #winter19. #january87th pic.twitter.com/P8g1PVxZxA
— Tyler Lind (@Lindsanity34) April 11, 2018
 
The system dumped measurable snow as far south as Kansas, Iowa and Illinois. Ice and freezing rain was also reported in Minnesota, Michigan, Indiana and New York.
Blizzard warnings were issued for parts of six states: South Dakota, Colorado, Nebraska, Kansas, Iowa and Minnesota.
Most major interstates and roads in South Dakota and western Nebraska were closed due to the conditions on April 13. Interstate 70 in eastern Colorado and Interstate 80 in eastern Wyoming were also closed. 

 
 

Nothing like some nice hay in a blizzard. #newx #Xanto #nebraska #ranchlife #WinterStorm pic.twitter.com/uvCrhUl9jN
— Myra Richardson (@NEmyrich) April 14, 2018

 

As the wind continues to blow, we'd like to thank all of our employees and others in agriculture who braved the #blizzard elements to care for livestock. We'd also like to give a heartfelt thanks to all of the other industries and professions who have worked tirelessly! ?? pic.twitter.com/WPaiygg46q
— Adams Land & Cattle (@AdamsCattle) April 15, 2018

 

#WinterStormXanto is bringing heavy snow and strong winds to the Dakotas. Take a look at this video from Winner, #SouthDakota! The area is under a #blizzard warning right now. Further west, #RapidCity airport saw a record 8.8 inches of snow. pic.twitter.com/sfovan3ZUP
— AMHQ (@AMHQ) April 14, 2018