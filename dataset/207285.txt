Chicago Mercantile Exchange live cattle futures eased to their lowest level in nine months on Monday as the seasonal supply build-up pressured wholesale beef prices for a third consecutive session, traders said.They said bullish traders worried that plentiful cattle numbers might again result in lower prices for slaughter-ready, or cash, cattle later this week.
Bargain buyers limited market losses while being drawn to futures that remained bullishly undervalued, or discounted, to last week's cash prices.
August ended down 0.375 cent per pound at 106.000 cents, and October closed down 0.050 cent to 105.850 cents.
Grocers are buying beef at lower prices knowing more heavyweight cattle are now being processed, which tends to add more tonnage to the retail sector, a trader said.
"Packers are in a tough spot because they have to clear as much beef from their coolers as possible to make way for fresh meat," he said.
Last week cash cattle in the U.S. Plains brought $109 to $110 per cwt versus $114 to $116 a week earlier.
Market participants await Wednesday's Fed Cattle Exchange (FCE) sale of 1,067 animals. No cattle changed hands last week due to sufficient supplies and packer inventories.
On Tuesday the U.S. Department of Agriculture will issue the monthly cold storage report at 2 p.m. CDT (1900 GMT) that will include total July beef and pork inventories.
Two analysts surveyed by Reuters, on average, projected last month's total beef stocks at 416.1 million pounds and 561.4 million for pork.
Sell stops and weaker live cattle futures weighed on CME feeder cattle contracts.
August feeders closed 0.700 cent per pound lower at 139.800 cents. 
Hogs Repeat 4-month Low
Expectations for lower cash and wholesale pork prices, as supplies grow seasonally, sank CME lean hogs to a four-month low for a second straight session, said traders.
October ended 1.450 cents per pound lower at 64.675 cents, and December finished down 1.475 cents at 59.850 cents.
Besides already abundant supplies, packers will not need as many hogs as the Sept. 4 U.S. Labor Day holiday approaches, a trader said.
He said pork belly prices, part of overall wholesale pork values, have peaked seasonally ahead of Labor Day - the unofficial end of the summer bacon-lettuce-tomato sandwich season.