Chicago Mercantile Exchange live cattle futures closed sharply higher on Monday, with the August contract up the 3-cent per pound daily price limit, helped by buy stops and fund buying, said traders.They said Monday morning's wholesale beef price upswing and back-month futures' discounts to last week's cash prices offset initial selling tied to Friday's bearish U.S. Department of Agriculture monthly Cattle-On-Feed report.
June, which will expire on Friday, closed 2.275 cents per pound higher at 121.475 cents. Most actively traded August finished limit up at 118.275 cents, and topped its 10-day moving average of 116.960 cents.
CME live cattle's trading limit on Tuesday will be expanded to 4.500-cents following August's limit-down settlement on Monday.
Market participants await this week's sale of market-ready, or cash, cattle that a week ago brought $118 to $123 per cwt.
Packers are buying cattle for the U.S. July Fourth holiday-shortened workweek as beef demand tends to subside this time of year, said traders and analysts.
Wednesday's Fed Cattle Exchange sale of 2,554 animals could set the tone for remaining animals in the Plains. Last week, a small number of cattle at the exchange moved at $123 per cwt.
Buy stops, technical buying and live cattle futures advances drove nearby CME feeder cattle contracts up their 4.500-cent price limit. The limit will be expanded to 6.750 cents on Tuesday.
August feeders ended limit up at 149.450 cents per pound. 
Hogs Finish Firmer
CME lean hog futures' discount to the exchange's hog index for June 22 at 90.17 cents attracted buyers, said traders.
They said investors sold deferred months before Thursday's USDA quarterly hog report and bought July - driving it to a new high for the contract.
July ended up 1.725 cents per pound to 87.025 cents, and marked a new high of 87.725 cents. August closed up 0.025 cent to 78.675 cents.
Packers need fewer hogs before plants shut down during the holiday, which could reduce the amount of product available to retailers, a trader said.