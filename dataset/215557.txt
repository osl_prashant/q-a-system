Q.        Is it important to have enzymes in my forage inoculant?
 
A.         Yes! Enzymes are used in inoculants to help break sugar polymers into simple sugars. This provides "food" to the lactic acid bacteria (LAB), which, in turn, help drive the fermentation. Enzymes also can affect the digestibility of plant material.
Under optimum conditions, enzymes can work quickly — up to 20,000 times per second — to speed up the chemical process of fermentation. Most of the enzymes in forage inoculants break down complex materials into simpler materials and generate one molecule of water for each reaction.
Each type of enzyme contributes differently to the ensiling process. Depending on your specific needs, here are some common enzymes to look for in your forage inoculant:

Cellulase, which can lower neutral detergent fiber (NDF) and acid detergent fiber (ADF) in the forage and typically works best at low pH levels. A potential concern with cellulase is that the substrate it works on (cellulose) is the major component of the plant.
Protease breaks down proteins . This enzyme is less beneficial for protein crops like haylage, but could be useful in high-moisture corn (HMC). Look for research studies that validate the specific product/enzyme activity if claimed. There have been recent instances of basic research into the breakdown of zein proteins in HMC and corn silage being cited in support of completely unrelated enzyme activities.
Xylanase, which attacks part of NDF portion that to yield pentoses (5-carbon sugars), later helping drive the production of acetic acid (a yeast inhibitor).
Amylase, which breaks down starch into soluble sugars and is most suited to for legume and cereal forages. Some lactic bacteria can use starch directly and would not require the "support" of amylase.
Ferulic acid esterase, an enzyme produced by some strains of Lactobacillus buchneri, breaks down some of the links binding plant fiber and lignin.

If a product claims to contain enzymes, the label should clearly state guaranteed levels. It"s not enough to simply list some enzyme sources in the ingredients. Without guaranteed levels, you can assume that any enzyme activity present is limited at best. To be effective, a product must contain guaranteed levels that are validated effective by research studies.
I hope this information helps you in making future inoculant purchases.
 
Sincerely,
The Silage Dr.
Question about silage management? Ask the Silage Dr. on Twitter, Facebook or visit www.qualitysilage.com.