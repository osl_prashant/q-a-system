Canadian health officials have declared an E. coli outbreak is over and consumers can once again eat romaine lettuce with confidence, but U.S. agencies maintain not enough is known about a similar outbreak in this country to arrive at the same decision.
In a Jan. 10 update, the Centers for Disease Control and Prevention reported the “likely source of the outbreak in the United States appears to be leafy greens” — but declined to name romaine or other product because of the complicated investigation process.
To date, the outbreak has been linked to 24 illness (an increase of seven since the CDC’s original notice on Dec. 28) and a death in California. Of 13 ill people interviewed, all reported eating a type of leafy green, but “no common supplier, distributor, or retailer of leafy green has been identified.”
In fact, even though five ill people reported eating romaine lettuce, the CDC concluded “ill people in this outbreak were not more likely than healthy people to have eaten romaine lettuce.”
While the CDC’s whole genome sequencing showed the U.S. and Canadian E. coli strains were “closely related genetically,” that data alone is not sufficient enough to prove a link, according to the CDC notice.
Because no definite source has been found, the CDC is not advising consumers to avoid any leafy greens.
In Canada, efforts to track the source of the romaine that’s been linked to the outbreak have not yielded results, according to the Jan. 10 announcement by the Public Health Agency of Canada declaring the outbreak there over.
The most recent onset date for E. coli in Canada was Dec. 12.
“As a result, the outbreak appears to be over and the risk to Canadians has returned to low,” according to the notice. “The Canadian Food Inspection Agency has completed its food safety investigation. All samples tested were negative for E. coli O157.”
They agency did not say if those samples were of lettuce, or if CFIA tested potential grower/packer/processor links in the supply chain.
The last U.S. case was reported Dec. 12, but the CDC lists several reasons it is not calling the outbreak over:

There is a delay between when someone gets sick and when it is reported to the CDC;
For this specific E. coli strain, that can be up to three weeks; and
Holidays can increase the reporting delay.

The United Fresh Produce Association, working in connection with the other major industry groups in the U.S. and Canada, released its third member update on the outbreak in less than a week. The Jan. 10 notice again stressed that industry associations are working with government agencies to assist in the investigations. The alert noted that U.S. and Canadian health agencies agree the product is no longer in the marketplace, and that they have reported there is no reason to avoid eating romaine or any leafy green.