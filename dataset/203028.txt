Americold is opening a cold storage facility in East Point, Ga., primarily to serve Sprouts Farmers Market.
The warehousing and food industry logistics company purchased the 260,000-square-foot temperature-controlled site in the Atlanta suburb July 2016.
Sprouts plans to expand to more than 250 stores in 15 states in 2017, according to a news release.
"Americold is thrilled to be able to work with Sprouts as an anchor tenant in this facility and provide the additional, dedicated capacity needed as they continue to expand their healthy grocery operations from coast to coast," said Fred Boehler, president and CEO of Americold, in the release.
The Georgia site serves as a distribution center for Sprout's eastern operations. The site offers two temperature zones at 34 F and 55 F and will create more than 80 jobs, according to the release.
"The opening of our East Point site allows us to support Sprouts and other food retailers that are rapidly expanding throughout the Southeast," Boehler said in the release.
The Atlanta, Ga.-based Americold owns and operates more than 165 temperature-controlled storage facilities.
"Americold's decision to come here underscores East Point as a strategic location for the foodservices industry in metro Atlanta," said Maceo Rogers, director of East Point's Department of Economic Development, in the release.