SALINAS, Calif. — While the future is hard to predict, a retail panel at the third annual Forbes AgTech Summit said the long-running trend of organic produce and newer developments such as online shopping are certain to be part of meeting the needs of tomorrow’s consumers. 
Moderator Maggie McGrath, staff writer for Forbes Media, asked three panelist at the June 29 workshop about the biggest changes in the produce business since they began their careers.
 
Heather Shavey, assistant vice president and general merchandising manager of Issaquah, Wash.-based Costco Wholesale, said the strength of the demand for organic produce over the past 30 years is what stands out to her.
 
“I really thought at first it was going to be a passing trend, and now it has become a huge industry initiative and a huge focus for us at Costco, not only in the fresh category but throughout the business,” she said.
 
Packaged salad now represents a much larger share of the produce business than was the case 30 years ago, said Bruce Taylor, founder and CEO of Taylor Farm.
 
“In the old days you had a packaged salad, and people would buy the bag and add their ingredients to it,” Taylor said. “Now the consumer wants everything in the package to the point where five years ago, salads with dressings weren’t growing and now they want the dressing in the salad as well.
 
“The challenge is having product to satisfy the entire customer base,” he said.
 
Mike Teel, owner and CEO of Raley’s Inc., West Sacramento, said the retail market is more competitive than ever, with players like Costco taking top-tier customers and Wal-Mart competing for lower-income consumers. That puts a premium on making retail stores more relevant than ever to shoppers, he said, and Raley’s is seeking to be involved with the health and well-being of its consumers. 
 
Teel said Raley’s purpose is to improve the health of their consumers, and that means promoting fresh and healthy foods over less nutritious packaged goods. 
 
“It is because (retailers) take in millions and million and millions of dollars from the consumer packaged goods to put products on the shelves and at the registers and advertise those products, and those products are literally killing our customers over a long period of time,” Teel said.
 
Teel said Raley’s is taking a new approach.
 
“What we are doing is we say no, we are going to be driven by health decisions for our customers,” he said. “You can’t change overnight but suppliers like Taylor Farms are producing products that are healthy and health-driven so what we want to see is more of those products are coming online and we are educating customers in that direction,” he said. 
 
Future challenges
 
Teel said the shorter supply chain that online shopping and technology promises — direct consumer delivery of produce from the farm, for example - prompts the question of Raley’s should be closer to the point of production.
 
“We are going to have to figure out how to deal with this disruption over the next 20 years, and I think it brings us closer to the growers,” he said.
 
Taylor said the complexity of marketing demands a collaborative approach with retailers. With 240 stock-keeping units from Taylor Farms in Raley’s, for example, the buyer-seller dynamic is not transactional but based on relationships.
 
“There is so much you have to do to prepare to do business with each other and then maintain and build each others business,” he said.
 
The growing number of outlets for fresh market channels such as convenience stores and drug stores also adds complexity, he said.
 
“The desire for variety is there but you have to have the volume to make it worthwhile,” he said. “As you have more points of distribution, you have less velocity at each point, and each point becomes more difficult to manage.”
 
The need to automate labor on the farm and in the packinghouse is as great as ever, Taylor said.
 
“The reality is the second generation of immigrants don’t want those jobs,” he said. “It is not the shortage of labor, but the fact there is no labor, that no one is going to be available to pick lettuce,” he said.