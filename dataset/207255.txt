Chicago Mercantile Exchange live cattle settled higher on Thursday after short-covering and bargain buying rescued futures from a nine-month low earlier in the session, said traders.Technical buying and positioning before the U.S. Department of Agriculture's monthly Cattle-On-Feed report on Friday provided more market support.
August , which will expire on Aug. 31, ended up 0.300 cent per pound to 105.950 cents. Most actively-traded October finished 0.650 cent higher at 106.825 cents.
"Futures rallied but peeled back from highs because no one wants to get far ahead of Friday's report that could show bigger cattle numbers down the road," a trader said.
Investors await remaining slaughter-ready, or cash, cattle sales in the U.S. Plains that so far this week brought $106 to $107 per cwt. Last week those cattle fetched $109 to $110.
Plentiful cattle supplies further dragged down wholesale beef values, which tend to peak soon after Labor Day - the summer's final grilling holiday, said traders and analysts.
"There's enough beef, pork and chicken out there for supermarkets to choose from. Pretty soon, talk will turn to ham and turkey for the year-end holidays," another trader said.
Short-covering, live cattle futures turnaround and technical buying lifted most CME feeder cattle contracts from morning lows.
August feeders, which will expire on Aug. 31, closed down 0.025 cent per pound to 141.600 cents. Most actively-traded September closed up 0.275 cent to 142.175 cents, and October finished 0.700 cent higher at 142.400 cents. 
Hogs Up from 4-month Low
CME lean hogs settled moderately higher on buy stops and bargain buying, but not before initially sinking to a four-month low as burdensome supplies continued to undercut cash prices, said traders.
Nonetheless bullish investors were drawn to futures that remained undervalued compared with the exchange's hog index for Aug. 22 at 80.49 cents.
October ended up 0.225 cent per pound to 63.775 cents, and December finished 0.400 cent higher at 59.250 cents.
The seasonal supply bump, and packers needing fewer animals going into Labor Day plant closures, points to mostly weaker cash prices in the near term, a trader said.
Almost fall-like weather in the Midwest is causing hogs to grow sooner than expected - making them more accessible to processors at less money, said traders and analysts.