City worries about safety of Iowa plant's leftover stover
City worries about safety of Iowa plant's leftover stover

The Associated Press

NEVADA, Iowa




NEVADA, Iowa (AP) — Public safety officials are concerned about up to 500,000 bales of flammable stover that are stored across central Iowa and will be left over after a cellulosic ethanol plant is sold.
The newly merged DowDuPont is selling its $225 million cellulosic ethanol plant in Nevada because it no longer fits its strategic plan, leaving many residents asking what will happen to the remaining stover.
The plant's corn cobs, husks and stalks are a fire liability for the city and county, the Des Moines Register reported . The plant's stover is scattered around 23 storage sites in central Iowa. A few bales catching fire could turn into a major blaze, particularly at the plant's main site where there are about 200,000 bales within about a half mile.
"Our concern is who will be responsible for the bales once the plant is sold," said Ricardo Martinez, Nevada's public safety director.
"What would be my worst nightmare is if DowDuPont shuffles its hands and says, 'we're out of here,' and they walk away, and we still have the problem to deal with," he said.
Nevada firefighters faced two large stover fires two years ago. DuPont also has experienced between seven and eight lightning fires at stover storage areas, though some were believed to be arson.
"DuPont knows our concerns and has been working with us," Martinez said.
Ray Reynolds, the city's fire chief, believes a buyer will be interested in the stover, purchased under contracts with area farmers. Reynolds said that if a new plant owner doesn't want the stover, the company said the crop residue could be ground up and used for livestock bedding or covering landfills.
___
Information from: The Des Moines Register, http://www.desmoinesregister.com