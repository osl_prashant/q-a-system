South Dakota spring wheat planting finally getting underway
South Dakota spring wheat planting finally getting underway

The Associated Press

SIOUX FALLS, S.D.




SIOUX FALLS, S.D. (AP) — Planting of spring wheat is finally getting underway in South Dakota.
The federal Agriculture Department says in its weekly crop report that 2 percent of the crop is in the ground, well behind the five-year average of 50 percent. Last year at this time, nearly 75 percent of the spring wheat was seeded.
About 2 percent of the oats crop also is planted, behind the average pace of 54 percent.
The state's winter wheat crop remains mostly in fair-to-good condition.
Topsoil moisture supplies are rated 87 percent adequate to surplus, up from 82 percent. Subsoil moisture is 72 percent in that category, up from 63 percent.