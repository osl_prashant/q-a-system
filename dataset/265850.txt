BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Tuesday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2583  Down  73May         2623    2623    2534    2554  Down  75Jul         2650    2650    2564    2583  Down  73Sep         2662    2664    2581    2601  Down  71Dec         2666    2666    2585    2606  Down  69Mar         2652    2652    2574    2595  Down  65May         2640    2643    2578    2600  Down  63Jul         2630    2630    2593    2608  Down  62Sep         2636    2636    2600    2616  Down  62Dec         2651    2651    2604    2624  Down  62