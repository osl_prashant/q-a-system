Hog industry lawyers: Jury needn't hear other waste methods
Hog industry lawyers: Jury needn't hear other waste methods

By EMERY P. DALESIOAP Business Writer
The Associated Press

RALEIGH, N.C.




RALEIGH, N.C. (AP) — Attorneys defending the hog industry against federal lawsuits that claim spraying liquefied animal waste over farm fields has harmed their North Carolina neighbors don't want jurors to hear about alternative methods used to dispose of the waste elsewhere.
U.S. District Judge W. Earl Britt and attorneys on Tuesday discussed trial details ahead of the first of what could be dozens of nuisance cases that could impact profits and processes in the country's No. 2 pork-producing state. Britt called the trials starting next month involving more than 500 neighbors of industrial-scale hog operations a "massive" legal undertaking.
The predominant method of raising hogs involves closely confining hundreds of animals in warehouse-like buildings and frequently washing their wastes into collection cesspools. There, the waste is treated and the remaining liquids periodically sprayed out onto fields from movable towers.
Neighbors suing Murphy-Brown LLC, a subsidiary of Virginia-based pork giant Smithfield Foods, contend winds sometimes catch the falling droplets and coat their homes, cars and clothing with filth. The neighbors also contend the hog operations concentrated in a handful of eastern North Carolina counties create intense smells, noise and clouds of flies that intrude on how freely they can use their own property.
Lawyers defending Murphy-Brown said in a court filing that jurors shouldn't be told about alternatives to spraying for disposing animal urine and feces. For example, Midwest hog operations inject or disc hog waste directly into soil rather than spraying it into the air to settle on the ground.
Telling jurors about pork-production technologies used outside of North Carolina would confuse them by implying that the same methods could be used at Kinlaw Farm, the Bladen County hog operation being sued by 10 neighbors in the first trial, Murphy-Brown attorneys said. The farm located 10 miles (16 kilometers) east of a massive Smithfield Foods slaughterhouse raises hogs for Murphy-Brown but is not owned by the company.
"Alternative technologies outside North Carolina involve different types of farms, different climates, different topography, and different localities — among other things — all of which make this information of no consequence here," Raleigh-based attorney Mark Anderson wrote last week.
Farmers in Iowa — the top U.S. pork state — and the other large Midwestern pork states store the manure and urine in deep concrete pits beneath the hog houses. The resulting "slurry" is then pumped or trucked onto fields.
North Carolina operations raising roughly 9.3 million hogs tend to be much smaller than their Midwest counterparts and are clustered in low-lying, flood-prone counties with sandy soils and shallow aquifers.
Pork is a $2.3 billion industry in the Tar Heel State.
Next month's test case involving plaintiffs chosen by their lawyers is expected to last about six weeks. A second trial to follow weeks later involves a plaintiff household chosen by Murphy-Brown's lawyers. New trials then would start every month until the cases are decided, dismissed or settlements are reached.
___
Follow Emery P. Dalesio at http://twitter.com/emerydalesio. His work can be found at http://bigstory.ap.org/content/emery-p-dalesio .