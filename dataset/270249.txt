Fresh Ecuadorian cape gooseberries will be allowed U.S. market access under a new proposal from the U.S. Department of Agriculture.
Comments on the proposal will be accepted until June 18, according to the USDA.
Imports of the fruit — also called ground cherries, goldenberry and physalis — will be allowed from Ecuador under what the USDA calls a systems approach.
Those measures by growers, packers, and shippers are to protect against the introduction of the Mediterranean fruit fly in the U.S. The measures require establishing pest-free places of production and the labeling of boxes prior to shipping. The fruit would have to undergo approved cold treatment or irradiation to be cleared for shipment to the U.S.
U.S. import levels for fresh cape gooseberry fruit are not known, according to the USDA, because the fruit is combined in U.S. trade statistics with black, white, and red currants.
In 2015, the U.S. imported approximately 78.7 metric tons of gooseberries and currants valued at about $476,000. 
The U.S. does not produce fresh cape gooseberry fruit commercially, according to the USDA.