DAP, MAP and potash were mixed on the week.

DAP $25.07 below year-ago pricing -- lower $2.13/st on the week to $456.75/st.
MAP $36.54 below year-ago -- higher $1.42/st this week to $459.03/st.
Potash $26.12 below year-ago -- lower 34 cents/st this week to $332.43/st.


DAP posted the largest declines in the P&K segment this week led by an $18.00 per short ton decline in South Dakota. Nebraska softened $4.26 and Wisconsin shucked $3.09. Four states were unchanged as Kansas firmed $2.07 and Michigan gained just 86 cents.
MAP was higher on the week supported by Nebraska which firmed $13.04 as Illinois and Kansas each gained about $2.80. Three states were unchanged as Minnesota fell $4.60 and South Dakota dropped $2.35.
Potash fell this week led by declines in Kansas, North Dakota and Illinois which softened 39, 36 and 6 cents respectively. Two of the twelve states in our survey were unchanged as Nebraska firmed $2.60, Missouri gained $2.25 and Iowa added $1.77.
The spread between DAP and MAP prices averages roughly $15.00 per short ton. Right now that spread is at $2.28 with MAP at a premium to DAP. The charts indicate that the spread between DAP and MAP indicate little about upcoming price action. Our nitrogen commentary this week details clear evidence that the price relationship between urea and NH3 does indicate pending price action, but so far, we have not found the DAP/MAP spread to be an indicator of a change in the trend or any other price path.
But that does not render a phosphate spread analysis useless. It has been a rare occasion that DAP and MAP have spent as much time at or near parity as they have over the past few months. Our charts do indicate that MAP has been priced at a premium to DAP for the entirety of our weekly survey. This week, as DAP falls and MAP firms slightly, we speculate that the spread will unwind, and MAP will work toward that $15 premium above DAP. We have seen the spread narrow during the course of a move upward or downward and our research suggests the spread will reach its widest point before a change of direction. Perhaps the spreads do have a tale to tell after all.
If the above is correct, we could expect DAP and MAP to continue on their current price path until we reach a "terminal spread" between the two. That is, once the current move has run its course, MAP will overdo the spread to the upside, and phosphate products will correct lower together. The change of direction has always been accompanied by narrowing phosphate spreads. The most recent example came early last summer when the spread swelled to $20.39. At that point, the margins began to narrow as phosphate prices slid lower. We also note that it works for an upside move as well as demonstrated in January 2014 when the spread widened to $23.60 before springing higher and thinning the spread.
This suggests the current uptrend in DAP and MAP will continue until MAP gains a significant premium over DAP. In other words, our phosphate spread analysis is telling us DAP and MAP are headed higher with the greatest upside risk baked-in to MAP.
Potash fell back this week under the weight of bulging world supplies. Reports from Turkmenistan that potash powerhouse Belarus has helped build a new potash production facility with two more planned will mean the upside for potash will be limited for the foreseeable future. The new plants, and planned projects, are aimed specifically at concentrating on satisfying demand from India and China. That may mean China will no longer need to sign global import contracts, especially when we consider that China was able to postpone signing a potash contract last year by importing Russian potash via rail.
That will cause trouble for North American producers who desperately need world supplies to soften. Even without the new production facility in Turkmenistan, Russian and Belorussian producers have been very aggressively mining and producing finished potash as North American producers have been fighting to bolster prices by cutting production. Despite the curtailed North American production, laid off workers and boardroom semantics, potash supplies worldwide are just too big right now to expect a near-term potash price recovery.


By the Pound -- The following is an updated table of P&K pricing by the pound as reported to your Inputs Monitor for the week ended March 31, 2017.
DAP is priced at 47 1/2 cents/lbP2O5; MAP at 43 cents/lbP2O5; Potash is at 28 1/4 cents/lbK2O.





P&K pricing by the pound -- 3/29/2017


DAP $P/lb


MAP $P/lb


Potash $K/lb

 



Average


$0.47 1/2


$0.43


$0.28 1/4

Average



Year-ago


$0.50


$0.46 1/2


$0.29 1/2

Year-ago