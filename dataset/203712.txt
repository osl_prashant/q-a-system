A south Florida grower-shipper and wholesaler are partnering to supply the University of Miami's dining services tropical specialties for Compass Group North America's Imperfectly Delicious Produce program.
Miami's J&C Tropicals and wholesaler Freedom Fresh LLC are to participate in the Charlotte, N.C.-based Compass Group initiative that provides an outlet for secondary market produce.
The specialty produce from J&C, which grows and ships and imports tropical fruits, roots and vegetables, and distributed by Freedom Fresh, is scheduled to be used in the university's retail and residential dining operations.
The products will be promoted as part a sustainable food supply, according to a news release.
Because of skin blemishes, discoloring and abnormal sizes and shapes, J&C usually discards 60,000 pounds or about 9% of the 700,000 pounds of tropical fruits and vegetables it grows as unmarketable, said Denise Gomez, the company's marketing representative.
It's too early to say how much of that fruit would be salvageable and Gomez said she couldn't provide an estimate of how much fruit J&C plans to provide through the Imperfectly Delicious program that is designed to help the wholesaler and university prevent produce from being wasted.
The companies plan to offer the produce to the university foodservice operator for inclusion in campus venues including salad bars, soups and vegetable medleys.
J&C plans to offer boniato (sweet potato), guava, malanga blanca and mamey in the program.
Mamey would be used in smoothies and fruit salads while malanga and boniato chips and mash would be paired with seafood and other meat, Gomez said.
Compass is a contract foodservice company.