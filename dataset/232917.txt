AP-MN--Minnesota News Digest, MN
AP-MN--Minnesota News Digest, MN

The Associated Press



Here's a look at AP's general news coverage in Minnesota. Questions about coverage plans go to News Editor Doug Glass at 612-332-2727 or dglass@ap.org. Dave Kolpack is on the desk.
This information is not for publication or broadcast, and these plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
AROUND THE STATE:
EXCHANGE-EXOTIC ANIMAL FARM
HOUSTON, Minn. — A Minnesota farm features exotic animals such as a camel, peacocks, bison and wild boar. At Buffalo Gal, they sell a variety of unique meats — different bison cuts, Scottish highlander, elk, lamb, yak, and wild boar. Buffalo Gal is the last farm in Minnesota to raise wild boar. Today, Buffalo Gal is also home to a camel and a muster of peacocks roam the grounds. But Mike Fogel started the operation with a single bison in Wisconsin in 1976. By Matthew Lambert, Agri News. SENT IN ADVANCE: 704 words, photos.
EXCHANGE-THERAPY DOG
MANKATO, Minn. — A Minnesota man and his therapy dog bring cheer to nursing homes and assisted-living facilities. For more than seven years now, Loren Clobes and his Labrador mix have been visiting facilities in Mankato, North Mankato and St. Peter. Since Clobes retired from his manufacturing job five years ago, the pair has been providing little doses of animal therapy several days a week. By Kristine Goodrich, Mankato Free Press. SENT IN ADVANCE: 586 words, photos.
IN BRIEF:
TWIN CITIES-SNOWFALL RECORD
SPORTS:
SHARKS-WILD
ST. PAUL, Minn. — The Minnesota Wild return home from a three-game road trip to host the San Jose Sharks, who wrap up a four-game road trip in a key game for Western Conference playoff positioning. By Brian Hall. UPCOMING: 700 words, photos. Game starts at 7 p.m. CST.
T25-MINNESOTA-PURDUE
WEST LAFAYETTE, Ind. — No. 9 Purdue whether or not it will have a shot at earned a share of the Big Ten title when Sunday's game against Minnesota tips off.  It won't make a difference to the Boilermakers, the defending conference champs who are trying to close out the regular season strong. By Michael Marot. UPCOMING: 650 words, with photos. Game time is 3 p.m. CST.
___
If you have stories of regional or statewide interest, please email them to apminneapolis@ap.org. If you have photos of regional or statewide interest, please send them to the AP in New York via FTP or email (statephotos@ap.org). Be sure to call to follow up on emailed photos: 800-845-8450, ext. 1900. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.