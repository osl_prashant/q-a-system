Greenhouse grower Mucci Farms’ strawberries have been named by retailer Loblaw Cos. Ltd. the President’s Choice Fresh Product of the Year.
President’s Choice is Loblaws’ private label brand; the strawberries are marketed as Smuccies Sweet Strawberry brand by Mucci Farms. The PC-label fruit is available in Ontario stories.
To earn the Loblaw recognition, products must debut in Canada and prove to be a success by generating increased sales and profit according to a news release.
 “We’ve been growing traditional greenhouse items for over 50 years, but strawberries were our first attempt at growing an unconventional greenhouse product,” Danny Mucci, president of Mucci International Marketing, said in a the news release.
Mucci first test marketed the greenhouse berries in 2014. Mucci Farms continues to increase production of the berries, adding 36 acres in the fall of 2017 and a 12-acre third expansion to be completed this fall, according to the release.
The company plans even further expansion beyond that, with year-round supplies made possible by grow lights.
In a video played at the awards ceremony April 5 in Toronto, Dan Branson, senior director of produce, floral & garden at Loblaw Cos., said it is an “amazing tasting piece of fruit.”
“What’s important to both of us is that we really behave as partners in development,” Branson said in the video.