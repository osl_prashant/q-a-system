Diazteca, a vertically integrated mango grower in Mexico, is now offering conventional pineapples and organic mangoes.The company had 2,000 acres of mangoes in the state of Sinaloa certified in December 2016, a move triggered by demand.
“Large supermarket chains in the U.S. are conditioning the business — they won’t buy conventional mangoes unless they can also buy organic,” said Rod Diaz, marketing director at Diazteca.
The acreage yields 22,000 pounds per acre, due to the microaspiration irrigation system the company uses.  Diaz said the national average mango production in Mexico is only 8,000 pounds an acre for conventional and organic.
Diazteca expects to pack 4 million cases of mangoes through September, 1.5 million organic and 2.5 million conventional. Three years ago, the company packed 2 million cases of conventional.
The company also added conventional pineapple acreage, planting 250 acres of the MD2 variety in Isla Vera Cruz near the gulf coast in early 2016.
“Our goal is to plant 1,000 acres by 2018,” Diaz said.
The gulf coast location had logistical appeal to the company.
“Mexican pineapples are fresher, because it only takes 3 days to transport to the U.S., while South American pineapples generally take 12 days,” Diaz said. “We can harvest with higher maturity and ripeness, guaranteeing golden pineapples with 12 grade brix.”
Diazteca expects to market 2.3 million cases of pineapples this year.
Currently, Diazteca distributes 60% of its produce through Texas and 40% through Arizona.
“We are working on becoming a year-round supplier to the U.S. by entering into supply programs from Brazil, Ecuador and Peru,” Diaz said. “We’ll import to Philadelphia and Long Beach (Calif.).”