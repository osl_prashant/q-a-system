The Castroville, Calif., artichoke industry is gearing up for the 59th annual Castroville Artichoke Food & Wine Festival, June 2-3 at the Monterey County Fair & Event Center in Monterey.
The event helps teach attendees about artichoke growing, harvesting and preparation methods, but now also supports the local community through a dozen charities, according to a news release.
“We look forward to the Artichoke Festival each year because the event paints a clear picture of how this community comes together to support one another,” Diana McClean, senior director of marketing at Ocean Mist Farms and board member for the Castroville Artichoke Food & Wine Festival, said in the release. 
“The purpose of this organization aligns directly with Ocean Mist Farms’ values, which is part of the reason we enjoy sponsoring this annual event.” 
Festival proceeds benefit 501c3 community groups and efforts that align with the festival’s mission, according to the release. This year’s recipients:

Ashleigh Nicole Swain Memorial Scholarship Fund;
Bikers for Bikes;
North Monterey County High School Baseball;
North Monterey County High School Golf;
Marina Lions Club;
Ord Terrace Elementary School;
Agricultural History Project;
Peacock Acres;
North County Bulldogs;
North County Recreation & Park District;
Hope Horses & Kids; and
North Monterey County Middle School Library.

Festival attendees can enjoy dishes made with artichokes; a beer, wine and spirits garden; chef demos, live entertainment and field tours, according to the release.
More information is at artichokefestival.org.