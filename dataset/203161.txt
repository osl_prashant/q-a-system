Produce and agriculture leaders are pleased with the selection of Sonny Perdue as secretary of agriculture. 
 
Importantly, Perdue brings experience as former governor of a state with a diversity of agricultural production. 
 
Serving as governor of Georgia from 2003-11, Perdue knows well the issue growers face. 
 
As part of an agricultural trading company, Perdue also has worked actively in recent years in the agricultural realm. 
 
That will make him one of only a handful of agriculture secretaries who have worked in agriculture as an adult.
 
Once he is confirmed, produce industry leaders will be eager to talk with Perdue and his team about immigration reform and a new guest worker program, trade issues, federal nutrition programs and the farm bill that will begin to be debated in 2017-18.
 
Perdue must be the voice of reason for growers in the Trump administration. Particularly as it relates to the hot-button immigration issue, he must make the case that growers need every opportunity to hire legal immigrant farmworkers.
 
While farm labor is not, strictly speaking, part of any U.S. Department of Agriculture program, Perdue's influence on hard-line immigration enforcement elements of the Trump administration will be essential.
 
Given his good relationship with specialty crop growers in Georgia, we are optimistic he is up to the task.
 
Did The Packer get it right? Leave a comment and tell us your opinion.