(UPDATED, 5:26 pm) The complexity of the hot pepper supply chain created problems for top U.S. health officials in pinpointing a specific pepper type and source farm as the cause of a 2016 salmonella foodborne illness outbreak, according to a new report from the Centers for Disease Control and Prevention. 
The report, published on the CDC website, reviewed a nine-state outbreak of Salmonella Anatum infections detected by the agency in June 2016.
 
The outbreak involved 32 patients, with the onset of illness from May 6 to July 9, according to the CDC.
 
The outbreak strain was ultimately isolated from an imported Anaheim pepper, but federal health officials were not able to issue a consumer warning.
 
“The combined epidemiologic, laboratory, and traceback evidence indicated that fresh hot peppers were the likely source of infection, but a single pepper type or source farm could not be identified,” according to the report. “Because of the complicated supply chain for peppers and the extensive mixing of peppers from different suppliers, repacking, and reselling of product, FDA was unable to identify a single source farm or point of contamination for peppers.”
 
The Food and Drug Administration did place an unnamed Mexican consolidator/grower on import alert on June 21 of last year because of potential salmonella contamination on Anaheim peppers.
 
An import alert temporarily halts imports at the border until the importer gives evidence the product is free from pathogens. There were only two outbreak-associated illnesses reported after the import alert was issued.
 
The investigation highlights the importance of using epidemiologic and traceback data along with whole-genome sequencing results during foodborne outbreak investigations, according to the CDC.
 
Open-ended interviews and restaurant-specific recipe reviews can be helpful in identifying ingredient-level exposures, according to the report. “Both the complexity of the hot pepper supply chain, as well as the difficulties of identifying specific pepper types through epidemiologic investigations create challenges to investigating outbreaks linked to fresh hot peppers,” the report concluded.
 
The CDC said a 2009 study of Salmonella in fresh salsa found that chopped jalapeños were more supportive of Salmonella growth than some other raw vegetable ingredients when stored at 53 degrees to 69 degrees Fahrenheit, whereas no growth in salmonella was detected at 39 degrees. The study also said that salmonella survival and growth varied with salsa formulation and were inhibited only in recipes containing both fresh garlic and lime juice.
 
One industry food safety leader said the CDC investigation faced a number of challenges, including the fact that hot peppers are commonly used as an ingredient in many foods.
 
“Supply chains are complex and I don’t think that is a surprise to anybody,” said Jennifer McEntire, vice president of food safety and technology, United Fresh Produce Association. “I’m not sure in this case that you can blame the complex supply chain for the way this outbreak evolved and for the way the investigation evolved.”
 
One issue in the investigation, she said, may have been consumers’ lack of familiarity with the types of peppers they consume, notably the Anaheim pepper. ““I think there is a comment in the report that most people didn’t recollect eating Anaheim peppers and most people didn’t even know what they were,” she said. “I don’t even know if I know what they are,” McEntire said, noting the Anaheim pepper's resemblance to the jalapeño pepper.
 
Matthew Wise, team lead for CDC’s Outbreak Response Team, said the investigation was complicated.

 
“Some of the challenges I think we that we ran into in this investigation are similar to ones that we have run into in the past,” he said, noting the complexity of the supply chain is one such issue.
 
“On the epidemiology side of things, anytime you have a produce item (used as an ingredient) with lots of other items, it brings up complications,” he said. 
 
“People have known there was a hot pepper, but may not have known what hot pepper it was,” he said. Those are frequent challenges for these types of outbreaks, he said. 
 
“In terms of looking forward, there is a couple of things we did in this investigations that can help, and one of them is doing open-ended interviews,” he said. Working with restaurants to learn the ingredients in their dishes was also helpful in the investigation, he said.
 
In addition, he said all public health agencies are moving to greater use whole genome sequencing as more of a routine test to help investigate outbreaks.
 


Even with the difficulty of the investigation, McEntire said the use of whole genome sequencing did allow health officials to match the illnesses to the strain of salmonella on an Anaheim pepper.
 
Using whole genome sequencing, health officials will solve outbreaks that in previous years might have never been matched with a commodity. “I’m fearful that people will think our food supply is less safe but I don’t believe that is the case,” she said.