Food stamp administrators worry about food box proposal
Food stamp administrators worry about food box proposal

By JULIET LINDERMANAssociated Press
The Associated Press

WASHINGTON




WASHINGTON (AP) — State directors in charge of administering food stamp programs worry that the U.S. Agriculture Department's proposal to replace some cash benefits with a pre-assembled box of goods will impede their ability to provide assistance to needy families in their areas.
The "Harvest Box" proposal was unveiled last month in the Trump administration's 2019 budget, and is part of an effort to reduce the cost of the program by roughly $213 billion over a 10-year period.
Administrators of the program say they already are efficient, allowing recipients to purchase whatever foods they want directly from retailers, which benefits families, retailers and local economies.
The administrators worry that switching to the food boxes would be a logistical nightmare — and that they'd be blamed if something goes wrong.