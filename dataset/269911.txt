AP-AR--Arkansas News Digest 1:30 pm, AR
AP-AR--Arkansas News Digest 1:30 pm, AR

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Arkansas. Questions about coverage plans are welcome and should be directed to the AP-Little Rock bureau at pebbles@ap.org or 800-715-7291.
Arkansas Supervisory Correspondent Kelly P. Kissel can be reached at kkissel@ap.org or 501-681-1269.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
................
POLITICS & GOVERNMENT:
ARKANSAS EXECUTIONS-JUDGE
LITTLE ROCK, Ark. — An Arkansas judge prohibited from hearing execution cases has again demonstrated against the death penalty outside the governor's mansion, lying down on a cot in a scene identical to the protest that prompted his disqualification last year. Pulaski County Circuit Judge Wendell Griffen on Tuesday night participated in a vigil held by death penalty opponents to mark the one-year anniversary of Arkansas putting four inmates to death. By Andrew DeMillo. 400 words, with photos.
FROM AP MEMBERS:
BUFFALO RIVER-HOG FARM
LITTLE ROCK, Ark. — A 6,500-hog farm located near the scenic Buffalo National River has filed for a different permit in hopes of remaining operational after state environmental officials rejected an earlier application. 250 words.
IN BRIEF:
— NORTH LITTLE ROCK-HOUSE FIRE — Authorities in North Little Rock say three people are dead and a teenager was hospitalized after an overnight house fire.
— MAILBOX-SUSPICIOUS DEVICE — Arkansas police say the U.S. Postal Service and the Bureau of Alcohol, Tobacco, Firearms and Explosives were called to investigate a suspicious device found inside a mailbox.
— TARGET DRIVE-UP SERVICE — Target stores in Florida and Texas are now offering drive-up service in a move similar to what's being done by Arkansas-based Walmart.
___
If you have stories of regional or statewide interest, please email them to pebbles@ap.org and follow up with a phone call to 800-715-7291.
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867.
For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
The AP-Little Rock