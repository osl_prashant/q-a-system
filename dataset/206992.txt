Chicago Mercantile Exchange October lean hog futures fell sharply to a contract low on Thursday, weighed down by a months-long decline in cash prices and abundant supplies, traders said.Deferred hog futures contracts as well as most live cattle and feeder cattle contracts also eased on technical selling and profit-taking.
Traders were squaring positions ahead of U.S. Department of Agriculture data due after the close of trading on Friday in which the government will report how many cattle were placed on feed in August and how much meat was in cold storage.
CME October hogs dropped 1.325 cents to 57.325 cents per pound, finishing just above the earlier contract low of 57.075 cents. The most-active December hogs contract fell 2.125 cents to 57.800 cents, easing from Wednesday's two-week high.
The CME Group's index of the cash hog market has been on a downward trajectory since July and was hovering at the lowest levels since April.
USDA said hogs in the top cash market of Iowa and southern Minnesota were down 85 cents to $49.43 per cwt.
"Just too many," Kooima and Kaemingk Commodities Inc broker Scott Varilek said of hogs.
A few analysts expected USDA on Friday to show 573.5 million pounds of pork in cold storage in August and 426.5 million pounds of beef.
A Reuters poll of analysts expected the USDA to show cattle placed in feedlots for fattening in August down 2.9 percent from last year, suggesting that fewer animals would reach slaughter weight in February and March.
Cattle futures which surged by the most in 11 months on Wednesday declined on Thursday as investors locked in profits ahead of the USDA reports and as dealers awaited cash cattle sales in the Plains.
"The pretty big jump yesterday has people holding off," Varilek added. "Some bullish Cattle on Feed estimates were priced in."
CME October live cattle eased 0.850 cents to 110.100 cents per pound and most-active December live cattle down 0.050 cent to 116.200 cents.
CME October feeder cattle were off 1.400 cent to 155.875 cents per pound.