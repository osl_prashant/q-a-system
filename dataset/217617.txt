The Canadian government is sending three cabinet ministers to the United States to stress the merits of NAFTA, officials said on Thursday, paving the way for key talks that could either sink or save the trade pact.
Canada and Mexico object to major changes that Washington wants to make to the North American Free Trade Agreement, and time is running out to settle differences before the negotiations are scheduled to wrap up at the end of March.
Officials will hold the sixth and penultimate round of talks in Montreal from Jan. 23-28.
Two Canadian cabinet ministers are already in the United States and one more will travel this weekend.
Canada, which sends 75 percent of its goods exports to the United States, has been reaching out to U.S. politicians and the business community for almost two years to bolster support for free trade.
“We recognize we are heading into an important period. We are certainly preparing for all scenarios, and we’re hopeful that we can make progress,” a spokesman for Foreign Minister Chrystia Freeland said when asked about the latest visits.
“That’s what we’re focused on achieving, including in Montreal,” Alex Lawrence, the spokesman, said in an e-mailed statement.
News releases announcing the ministerial trips make clear the cabinet members have been instructed to “promote trade and NAFTA.”
Public Safety Minister Ralph Goodale wraps up a two-day trip to Kentucky on Friday, the same day that Environment Minister Catherine McKenna finishes a visit to San Francisco. Officials initially indicated McKenna would travel this weekend but later clarified her trip had already started.
Agriculture Minister Lawrence MacAulay is due to visit Tennessee from Friday to Monday.
Canadian officials are increasingly pessimistic about the prospects for the 1994 trade treaty, which U.S. President Donald Trump blames for the loss of hundreds of thousands of U.S. manufacturing jobs and a big trade deficit with Mexico.
Prime Minister Justin Trudeau, citing “a level of unpredictability that’s out of our control,” told Global Television late last month that “no deal is better than a bad deal for Canadians.”
In a meeting held in Washington last month between the fifth and sixth rounds, negotiators left untouched the thorniest subjects of the sourcing of auto parts, dispute settlement and an expiry clause.
Trade experts predict that unless Canada and Mexico make some concessions in Montreal, Trump will be more tempted to issue a notice of withdrawal from NAFTA.