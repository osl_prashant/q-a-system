Ranchers who want to reduce calf loss at calving and to learn how to properly assist cows at calving should plan to attend “Assisting the Beef Cow at Calving” programs at six locations in December, with Robert Mortimer, a nationally known veterinarian from Colorado State University.

Mortimer will discuss handling calving difficulty, with emphasis on decision making and the hows and whys of techniques for providing assistance.

Mortimer developed a program strongly emphasizing hands-on experience in calving management and produced a video with Elanco and Beef Today on “How to Save More Calves at Calving.”

The cost is $20, which can be paid at the door. Pre-registration at least two days prior to the meeting is requested to ensure enough program materials are available.

Below are dates, locations and contact information for pre-registration with the local host:

Dec. 11 at Bridgeport: Prairie Winds Community Center, 1- 3:30 p.m.; contact Aaron Berger at 308-235-3122 or aberger2@unl.edu
Dec. 11 at Gudmunsen Sandills Lab near Whitman: 6:30-9 p.m.; contact Bethany Johnston at 308-645-2267 or bjohnston3@unl.edu
Dec. 12 at Broken Bow: 4-H Building, Custer County Fairgrounds, 1 pm – 3:30 pm; contact Troy Walz at 308-872-6831 or twalz1@unl.edu
Dec. 12 at O’Neill: Holt County Courthouse Annex, 6:30-9 p.m.; contact Amy Timmerman at 308-336-2760 or atimmerman2@unl.edu
Dec. 13 at Kearney: Buffalo County Extension Office, 1-3:30 p.m.; contact Brent Plugge at 308-236-1235 or brent.plugge@unl.edu
Dec. 13 at North Platte: West Central Research and Extension Center, 6:30-9 p.m.; Randy Saner at 308-532-2683 or rsaner2@unl.edu