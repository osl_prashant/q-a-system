PaperWorks Industries Inc. has promoted George Downey to director of sales for its folding carton business unit.
 
Downey is a senior-level professional with more than two decades of experience in management, operations, sales, and marketing. He has new product development and launch experience,
according to a news release. He joined PaperWorks in April 2016 as a senior account manager, responsible for folding carton sales on the East Coast.
 
"Since joining PaperWorks almost a year ago, George has demonstrated an exceptional ability to work with brand owners and private label companies to deliver solutions that support their objectives. That, coupled with his solid leadership skills throughout his business and military career, make him ideally suited to head our sales and business development efforts in North America," said Brandon Clairmont, senior vice president of sales and marketing, in the release.
 
PaperWorks is a full-service provider of recycled paperboard and specialized folding cartons for packaging applications.