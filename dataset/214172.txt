Q:  What are some of the steps you have taken to transition into a leadership role on your family’s farm operation?




 



Spencer Endrud


© Spencer Endru



 




A:  “Our farmyard, bins and shops aren’t worth a lot to anyone else except
me, so I got a pretty fair price on that doing a contract for deed. The way we did the machinery is we had an auctioneer come out and appraise everything, so I’m doing a lease-to-own based on that value. My sister and cousins don’t have an interest to come back to farm, so it was either me or nobody. I rent all the land that I farm. My two main landlords are my mom and uncle. In these tough years, I’ve been restructuring some land rental agreements towards net profit sharing, and on an immediate family basis finding a deal that’s fair to my mother but also fair to my sister, who’s not active on the farm.” - Spencer Endrud, 28 Buxton, N.D.
 

 








Smith Stoner


© Smith Stoner



 




A:  “When I first came home, my dad, kind of before his health started declining,  slowly started letting me start subleasing land from him. My grandfather had set up a trust for all the land we lease from. So we did that and then he increased my responsibility of running the whole show on the farm making seed and herbicide decisions. The next year, it was all our tillages and day-to-day activities while I was slowly picking up a little more every year. We did a lease-to-own deal. We did 10 years and got everything appraised. All our land and structures will still stay in the family trust that we have set up. My grandmother put me in as a partner.” - Smith Stoner, 28 Holly Bluff, Miss.









Andy Larson


© Andy Larson



 




A:  “I got an MBA and I saw some awfully high salaries, and here I was going back to making about $20,000 a year the first year I worked. It was an opportunity to make a living and work in the family business. I really craved responsibility. We run a 500-plus-head cow herd, and so that was kind of my initial responsibility. I think it’s very important when you come back to at least cede some of that responsibility to the next generation, No. 1. From an ownership standpoint, we restructured the business into an LLC. We started transferring shares into my account, and then over time, we’ve slowly worked my grandfather out of the business.” - Andy Larson, 37 Green, Kan.

More Resources For Transition Planning
There’s no better time than the present to begin the work of passing your farm operation onto the next generation. For more information about succession planning including toolkits and videos, visit FarmJournalLegacyProject.com.