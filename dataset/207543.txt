Ranchers in Kansas and Oklahoma learned more about how grasslands recover after wildfire in a joint educational seminar hosted by Extension personnel from Kansas State University and Oklahoma State University, at Snake Creek Ranch, on the state border. According to Extension sources, plentiful precipitation will be the greatest help in getting grass regrowing.“We’ve had such a fantastic spring in terms of rainfall in both Kansas and Oklahoma…I think these plants are going to recover very well,” Goodman said. “We’ll see how the rest of the summer goes in terms of rainfall, but so far we’re doing fantastic. I’m looking forward to seeing what these rangelands will look like in September and October.”
Rancher Mark Luckie, owner of Snake Creek Ranch, had 16,000 acres burned.
“We had 12 ½ miles of fence to replace, and approximately 80,000 feet of major repairs (to fence),” Luckie said. “We have two fencing crews running right now. It’s going to be another 60 days until we’re back to where we were.”

Walt Fick, a range management specialist with K-State Research and Extension, said that most areas he drove through seemed to have sufficient forage in the pastures to begin putting cattle on areas that burned.
“The grass is starting and as long as we have some warm, sunny days, those pastures can be grazed,” Fick said. “If it continues to rain, they can still have a fairly good production year.”
Perennial grasses typically benefit from being burned, so long as there is adequate moisture to follow. But the intensity of a wildfire always makes recovery a little tougher, according to Fick.
“History has told us that following wildfires, we might see some reduced forage production, but I think that’s still yet to be determined based on how much rainfall we get in the early summer period,” he said. “If it turns out dry, then that situation may become more critical to where we will need to back off on our stocking rates.”
Click here to read more from Kansas State University Extension.
 

The Howard G. Buffett Foundation has generously agreed to match, dollar for dollar, all donations to the Drovers/Farm Journal Foundation Million Dollar Wildfire Relief Challenge, up to $1 million by July 31. Donations will go directly to those impacted by the fires.You can make a tax-deductible donation online or by mail:
www.WildfireReliefFund.org
Wildfire Relief Challengec/o Farm Journal FoundationP.O. Box 958Mexico, MO 65265(Make checks payable to theFarm Journal Foundation withwildfire relief in the memo line.)