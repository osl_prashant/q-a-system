If the old expression “buy the rumor, sell the fact” holds any credence, then the Dow Jones Industrial Average’s impressive run-up to 24,000-plus on Nov. 30 might leave investors nervous.
If Congress and the president can agree on a tax reform package, is it all downhill from there? Will positive news — signing of the tax bill — deflate the ever-rising stock market?
That question for heavyweight equity investors will have to wait, but a more important issue looms for agriculture. Will tax reform automatically translate to better returns for growers and the entire supply chain?
Most farm groups have been supportive of the tax reform package, perhaps most notably for its provisions on the estate tax.
Media reports indicate that the House and Senate tax bills double the current estate tax exemption for individuals to $11 million. The House bill scraps the estate tax in 2024, though the Senate version keeps it in place.
Rep. Adrian Smith, R-Neb., wrote a recent column for The North Platte Telegraph spelling out what he considered the major benefits of the House tax plan for farmers:
> Producers can continue to deduct property taxes on agricultural land;
> Smith said repealing the death tax and keeping the stepped-up basis (readjustment of the value of an appreciated asset for tax purposes when inherited) in place are also wins for producers and small business owners in the House bill; and
> The House bill raises the cap on Section 179 expensing to $5 million and allows for full and immediate expensing for five years to help producers make more upfront investments. Section 179 of the IRS tax code allows businesses to deduct the full purchase price of qualifying equipment and/or software purchased or financed during the tax year.
However, not all farm interests are sold on the tax reform bills.
In a Nov. 29 letter to the Senate, the progressive National Farmers Union said it opposed the Senate’s bill.
Citing deficits that will result from the tax reform bill, the farmers union said budget pressures could result in mandated cuts to farm program payments, including Agricultural Risk Coverage and Price Loss Coverage programs.
The National Farmers Union is concerned, according to a news release, “with the massive, $1.4 trillion increase to the federal deficit, potential elimination of farm safety net funding, worsened quality and affordability of health care for rural Americans, and several provisions important to running a family farm operation.”
Is the new tax reform package — in the words of the union — “irresponsible” and “regressive”? Or is it a overdue stimulant the nation’s small business community?
I feel most growers and produce operators “buy” the concept of tax reform and what it will mean for their businesses. The next couple of weeks will reveal if the package can match the promise.
Tom Karst is The Packer’s national editor. E-mail him at tkarst@farmjournal.com.
What's your take? Leave a comment and tell us your opinion.