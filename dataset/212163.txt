Weather wasn’t an issue for most crops across the Mid-Atlantic region, but watermelon grower-shippers were a bit concerned by a rainy spate that hit the region in May.“The weather for the last 10 days has been absolutely horrible,” Kevin Evans, president of the 150-member Mar-Del Watermelon Association, said May 30.
“Some of the earliest crop is pre-bloom, so the early set would be a little rough on anything that’s been out 30-35 days or short-season stuff, because it’s coming up on pollination time.”
Most members in the association, who are in Maryland and Delaware, reported 2½ to 4 inches of rain in the last week of May, Evans said.
“They (the crops) want sunshine every day and to be managed with irrigation,” he said. “They don’t like staying wet.”
Growers are eager to get their fruit to market, because returns could be positive, at least for a time, Evans said.
“By the time we come on, July 15-20, it sounds like there could be some gap there,” he said.
“I’d heard Indiana had some trouble with planting, so that helps us out. The biggest thing is getting the pipelines cleared coming out of Georgia. South Carolina doesn’t have a big volume, but Georgia is pretty big. We’re hoping there’s a gap, because that can help the market out.”
The Mid-Atlantic watermelon deal should be into heavy volume by Aug. 1, with hopes to keep shipping through Sept. 10-15, Evans said.
The association will be ready with 40-65 promotions, “between local, regional and nationwide,” during that time, Evans said.
At Hales Farms in Salisbury, Md., half of the fields had been planted by May 27, said Will Hales, owner.
“We’ve had some unusual spring weather — cooler mornings in May and probably the most wind in May,” he said.
“We’ve had quite a few cloudy days, but we haven’t had as much torrential downpours as in the past that would dump 6 to 8 inches, so we’ve been very fortunate on the amount of rain we’ve received.”
Cool morning temperatures were the most unusual component of the spring weather conditions, Hales said.
“Through May 16, I’d say, maybe 11 or 12 days it was below 50 (degrees) in the morning,” he said.
“We had quite a few of them that were between 44 and 48 (degrees) — very unusual for the first half of May.”
But the crop endured, Hales said.
“Everything is looking good — looks like there’s a good stand, good livability.”
Growers are optimistic about the summer market conditions, Hales said.
“The past few years have not been great — average to below-average at the best,” he said.
“We’re hoping to have a stronger market and see where that leaves.”
Hales said his operation and Laurel, Del.-based Coastal Growers, a group with which he is associated, were about halfway through planting.
He said he expected a normal start to the season.
“It usually starts in mid-July and goes through mid-September, but I don’t have a good guess for this year,” he said.
“Everything’s kind of backed up just a little bit. I’d think it still comes around July 20-25.”
The 2016 deal started around July 26-27, Hales said.
“Most of the time, we start around July 20, so we’re not too far off,” he said.
“If anything, we might be like five days off of that. And if we get some warm nights, you can make up some time.”