Beef prices in Australia are set to rally as the nation's cattle herd hits a 20 year low.State officials call the herd severely depleted as it lost about 3 million head over the last three years, a 40% reduction.
Drought and strong demand from the export market have help to drive the upward trend in prices.
Analysts say it will take at least two years to rebuild the herd.


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.