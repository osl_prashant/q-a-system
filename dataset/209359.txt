World Dairy Expo features the best and the brightest during its world-class seminars. This year’s Expo Seminars include topics on robotic milking systems, A2 milk, transition cow health, mycotoxin in feedstuffs, consumer perceptions, cover crops and future farm labor.Continuing education credits are obtainable by members of the American Registry of Professional Animal Scientists (ARPAS) and the American Association of State Veterinary Boards – RACE Program (RACE).
Seminars will be showcased Tuesday through Saturday in the Mendota 2 meeting room, located in the Exhibition Hall. Additionally, all seminars will be recorded and available for online viewing upon completion. A special thank you to the following sponsors for making Expo Seminars possible: Compeer Financial, Quality Liquid Feeds, Inc., Phibro Animal Health Corp. and Feed Supervisor Software.
Following is the schedule of Expo Seminars:
Tuesday, October 3, 1:00 p.m.
“Building A Stronger Dairy Producer-Banker Relationship”
Arthur Moessner, Vice President – Dairy Team Lead, American AgCredit
Sponsored by: Compeer Financial
Continuing Education Credits: ARPAS (1), RACE (1)
 
Wednesday, October 4, 11:00 a.m.
“Economics of Robotic Milking Systems”
Dr. Larry Tranel, Dairy Specialist, Iowa State University Extension and Outreach
Lance and Jonna Schutte, Owners, Jo-Lane Dairy
Doug Gernes, Owner, Gernes Dairy, LLC
Sponsored by: Quality Liquid Feeds, Inc.
Continuing Education Credits: ARPAS (1), RACE (1)
 
Wednesday, October 4, 1:00 p.m.
“Making Sense of Dairy and Anti-Inflammation: Yogurt, Obesity and A2 Milk”
Dr. Bradley Bolling, Assistant Professor, Department of Food Science, University of Wisconsin-Madison
Continuing Education Credits: ARPAS (1), RACE (1)
 
Thursday, October 5, 11:00 a.m.
“Health and Immunity in Transition Cows”
Dr. Marcus Kehrli, Director, National Animal Disease Center – USDA-ARS
Sponsored by: Phibro Animal Health Corp.
Continuing Education Credits: ARPAS (1), RACE (1)
 
Thursday, October 5, 1:00 p.m.
“Limiting Mold and Mycotoxin Problems in Dairy Herds”
Dr. Lon Whitlow, Professor Emeritus, North Carolina State University
Sponsored by: Feed Supervisor Software
Continuing Education Credits: ARPAS (1), RACE (1)
 
Friday, October 6, 11:00 a.m.
“Consumer and Public Perceptions of the US Dairy Industry: Implications for Practices, Policy and Market Demand”
Dr. Christopher Wolf, Professor, Michigan State University
Continuing Education Credits: ARPAS (1), RACE (1)
 
Friday, October 6, 1:00 p.m.
“Cover Crop Management for Dairy Producers”
Dr. Karla Hernandez, Forages Field Specialist, South Dakota State University Extension
Continuing Education Credits: ARPAS (1), RACE (1)
 
Saturday, October 7, 11:00 a.m.
“Who Will Work on America’s Farms in 2025?”
Dr. Don Albrecht, Director, Western Rural Development Center
Continuing Education Credits: ARPAS (1), RACE (1)
 
For over five decades, the global dairy industry has been meeting in Madison, Wis. for World Dairy Expo. Crowds of nearly 75,000 people from more than 100 countries attended the annual event in 2016. WDE will return Oct. 3-7, 2017 as attendees and exhibitors are encouraged to “Discover New Dairy Worlds.” Visit worlddairyexpo.com or follow us on Facebook and Twitter (@WDExpo or #WDE2017) for more information.