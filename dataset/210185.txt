Pepin Heights Orchards owner Dennis Courtier looked back this fall on the beginnings of Honeycrisp, which 20 years ago started the evolution of the apple category.
These days, it is difficult to imagine a produce department without Honeycrisp.
The variety is tough to grow and pack, but consumers have embraced it and continue to pay a premium for it.
F.o.b. prices reported Oct. 11 from Yakima, Wash., for Washington Extra Fancy grade were mostly between $70 and $76 for tray-packed cartons of 88s, 80s and 72s, according to the U.S. Department of Agriculture.
Because of the high price Honeycrisp commands, companies keep investing in it.
The variety accounted for 7% of the 2016-17 Washington crop, roughly 9.3 million boxes, according to the Washington State Tree Fruit Association. Just four seasons earlier, Honeycrisp made up only 2.6% of the crop, or roughly 3.3 million boxes.
While the majority of apples produced in the U.S. are grown in Washington, the story of Honeycrisp started in Minnesota.
More than two decades ago, Courtier was on the hunt for flavorful fruit that would differentiate his Lake City, Minn., business.
“We were going broke trying to compete in an industry selling commodity apples in the late 1980s,” Courtier said in a news release. “We looked to new variety innovation to save the company and other small growers like us.”
University of Minnesota apple breeder David Bedford introduced Courtier to a test variety with some promise.
“It had an explosively crisp texture unlike anything I had tasted before,” Bedford said in the release. “We felt that it needed to be evaluated in a commercial orchard setting as soon as possible.”
Courtier agreed to test it, and eventually he was able to convince retailers to do the same.
“Those first few years were an uphill slog — and then it took off like a rocket,” Courtier said in the release. “I am forever grateful to the retailers who ultimately gave us a chance and saved our business and others like us.”