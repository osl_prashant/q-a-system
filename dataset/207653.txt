Midwest Produce Expo attendees have the opportunity to visit three modern Kansas City area grocery stores on a retail tour.
Produce Retailer Editor Pamela Riemenschneider serves as host of the Aug. 16 tour.
“These are all some of the newer stores in Kansas City, and they represent the ways Midwest chains are rethinking the way they appeal to modern consumers, especially millennials,” she said.
Cosentino’s Market
This 58,000-square-foot store serves as the anchor retail for BluHawk, a new mixed-use development in southern Overland Park, Kan. 
Fresh, prepared and convenient are the store’s foundation, called by local media “part traditional grocery store, part fast casual restaurant.” 
In addition to the in-house prepared foods — of which there are many — the store also offers Cosentino’s new Market Meals, a meal kit prepared by in-house chefs, and Mindful Meals, grab-and-go with healthful breakfast, lunch and dinner options created in a collaboration with Shawnee Mission Health. 
The Prairie Village, Kan.-based company has 29 stores, under the Cosentino’s, Price Chopper, Apple Market and Sun Fresh Market banners. 
Hen House
This 75,000-square-foot store in Leawood, Kan., was expanded and remodeled to reflect a changing consumer profile — one looking for enhanced grab-and-go, organic and prepared foods, with options like made-to-order waffles and fresh pita breads.  
This location is the chain’s volume leader, said Gregg Frost, vice president of operations for Ball Food Co., owner of the store, but it maintains the neighborhood feel started by Sidney and Mollie Ball in Kansas City in 1923. 
Ball Food operates about 30 stores in the Kansas City metropolitan area under the Hen House, Price Chopper, Sun Fresh Market and Payless Discount banners. 
Hy-Vee
From its Misfits “ugly” produce line to its in-store eateries, West Des Moines, Iowa-based Hy-Vee Inc. has its sights set on the latest trends in retail, fresh produce, health and wellness. 
The company’s Blue Springs, Mo., store provides tour participants with a look at one of the company’s most exemplary locations, with an expansive produce department and the Market Grille in-store dining. 
Hy-Vee operates 240 stores across the Midwest.