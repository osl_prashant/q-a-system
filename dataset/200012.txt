How is planting going in your area?Today on AgDay, Betsy Jibbon shares a picture taken from the field in Waterloo, Iowa, as shereportson the I-80 Planting Tour. She says farmers are kicking up dust with adequate planting conditions and warm temperatures.
A farmer in Randolph County, Ark., says farmers are back in the fields‚Äîat least the fields that are dry enough. There are still several fields in Arkansas too wet to work just yet.
Watch the full segment here:

<!--

    function delvePlayerCallback(playerId, eventName, data) {
        var id = "limelight_player_351368";
        if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
            DelvePlayer.registerPlayer(id);
        }

        switch (eventName) {
            case 'onPlayerLoad':
                var ad_url = 'http://oasc14008.247realmedia.com/RealMedia/ads/adstream_sx.ads/agweb.com/multimedia/prerolls/agwebradio/@x30';
                var encoded_ad_url = encodeURIComponent(ad_url);
                var encoded_ad_call = 'url='   encoded_ad_url;
                DelvePlayer.doSetAd('preroll', 'Vast', encoded_ad_call);
                break;
        }
    }

//-->


<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->

Want more video news? Watch it on AgDay.

What kind of planting progress are you seeing in your area? Leave your crop comments and field pictures on AgWeb's Crop Comments page, plus see what other farmersacross the countryare saying .

Post Your Crop Comment