Some college housing puts students in the same dorm or floor with students that share similar interests, like an honors dorm. At the University of Wisconsin-Platteville, students can live, learn, and work on a real dairy farm.
Travis Franks didn't grow up on a farm, but he worked on one throughout high school, choosing to live in the Cooper Living and Learning Center as his housing option.
"There's a lot of opportunities to learn here at the [Pioneer] farm," said Franks. "Being out here, you're connected to that."
Franks is closely monitored by his advisor, Dr. Tera Montgomery, and sees this opportunity as a way to plan for the future.
"Being out here, closer to the farm, we get more hands-on experience, and it definitely makes us more marketable for after we graduate to find a job," said Franks.          
Gaining classroom knowledge coupled with practical experience on the farm is preparing Franks for a future in the dairy industry.
"It's really an awesome opportunity for them to experience rural living, the farm life, the opportunities that the farm gives them," said Montgomery. "They've got plenty of classes on campus where they can really interact with the general population as well."
The Cooper Living and Learning Center is an asset that will help students the experience necessary to hit the ground running after graduation, according to Montgomery.
"It's a constant learning environment, and it's not just something they're doing," said Montgomery. "It's something that they're really immersed in all of the time."


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.