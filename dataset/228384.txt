Toronto eats Chilean berries
Two of Toronto’s hottest brunch restaurants used Chilean blueberries in their pancakes on Jan. 28 to promote the Chilean season. Guests were invited to post a photo of their dish for a chance to win a gift card, said Eastern Canada merchandiser Susanne Bertolas.
This year’s promotion, which continues through March, also includes sampling of Chilean blueberries in major Toronto grocery chains, Bertolas said.
Karen Brux, managing director for North America for the Chilean Fresh Fruit Association, said volumes to North America are still playing catch-up after a late start to the season.
 
Canadian Fruit and Produce adds specialties
Melon specialist Canadian Fruit and Produce Co., Toronto, is diversifying its fruit imports with yellow pitaya, sweeter and more flavorful than its pink dragonfruit cousin, and soursop, both from Ecuador.
Both fruits are available year-round and renowned for their health benefits, said vice president Randy Weinstein.
“We’re trying something different and business has been fantastic,” Weinstein said, citing interest from independents, chains, high-end specialty stores and Asian stores.
The company is also importing packages of fresh soursop leaves, used in tea to boost the immune system.
 
Fresh Advancements boosts fresh-cut
For the second year, sales of Lone Star fruits and vegetables from Texas are on the rise at Fresh Advancements at the Ontario Food Terminal. CEO Steve Bamford also reports increased volumes on Mexico produce.
Bamford said fresh-cut produce from the company’s processing operation continues to grow “with tons of new SKUs,” and an expansion from 30,000 to 90,000 square feet.
On the farming side, Bamford Family Farms now owns four Ontario apple farms.
 
Freshline Foods pairs with Paw Patrol
Paw Patrol, the Canadian animated canine adventure show, is encouraging kids to eat healthy snacks. Freshline Foods’ vice president Noel Brigido said the Mississauga, Ontario, company has partnered with Nickelodeon on Paw Patrol-branded apple slices.
Since launching three months ago, sales and comments have been phenomenal, Brigido said. Of the four varieties of gala and granny smith slices, naturally-flavored peach and grape are the best sellers.
 
Fresh Taste Produce succeeds with Rockits
Rockits are taking off at Fresh Taste Produce Ltd. in the Ontario Food Terminal.
Packed like four tennis balls in a plastic tube, the miniature apples come from Chelan, Wash.-based Chelan Fresh and are a cross between two gala varieties. They were first grown in New Zealand in 2010.
The unique package has created a great deal of interest since December, said chief operations officer Julian Sarraino.
It’s also a strong year for citrus, he said. Fresh Taste continues to have success with the Flying Carpet brand of clementines from Morocco, and he said interest in cara cara and blood oranges is picking up.
 
F.G. Lister packs in house
Rather than outsource packaging, F.G. Lister and Co. Ltd. at the Ontario Food Terminal, has been bagging its own produce for the past six months, from clementines to grapes, said president and CEO Tony Fallico.
Lister’s offshore programs are strong, he said, includng pomegranates and orri clementines from Israel and pomegranates from Peru.
Fallico said Lister is also importing avocados and blueberries from Camposol in Peru.
 
Gambles expands specialties
Gambles Group at the Ontario Food Terminal has begun sourcing direct containers of specialty produce weekly from Honduras, Costa Rica, Dominican Republic and Colombia, said Tom Kioussis, vice president of sales and marketing.
Kioussis said the company continues to grow, and now comprises three different operating units: Gambles Produce, Torizon Logistics, and Go Fresh Produce, which focuses on branding, offshore sourcing and distribution services.
“Alongside our shareholder partners Total Produce, Gambles Group continues to expand our reach throughout Canada, developing global partnerships to satisfy the needs of our local customers,” he said.
 
Koornneef to build warehouse
Koornneef Produce Ltd., with a stall at the Ontario Food Terminal, plans to build a 65,000-square-foot warehouse in the town of Lincoln not far from its greenhouses and warehouse in Grimsby, Ontario.
President Fred Koornneef said new housing developments are encroaching on the family’s original facility, leaving no room for expansion.
The new facility will allow the grower-shipper to pack more vegetables and Niagara fruit for local growers in addition to its sweet peppers, mini cucumbers and private-label grape tomatoes.
 
Mike & Mike’s in Walmart
Woodbridge, Ontario-based Mike & Mike’s Organics is now in Walmart with its packaged organic lemons, ginger, garlic and shallots, said director of business development Robert Kuenzlen.
Along with selling root vegetables from Ontario’s organic farmers, Mike & Mike’s is bringing in Foxy’s organic “supergreen” BroccoLeaf from California.
As part of its inward focus this year, Kuenzlen said Mike & Mike’s is working with the Canadian Organic Trade Association and Canadian Organic Growers, which recently won government funding for a Canadian Organic Regime review, considered a big win for the industry.
 
100km enjoys brisk business
Toronto foodservice produce distributor 100km Foods Inc. is having a busier winter than expected, said co-owner Paul Sawtell, thanks in part to the city’s annual Winterlicious restaurant festival, Jan. 26 to Feb. 8.
Sawtell said chefs were asking for unusual locally grown roots such as burdock root, salsify (oyster root) and black radish along with greenhouse-grown lettuce and microgreens. He’s also selling edible flowers grown indoors by Trend Aqua Fresh Inc. in Niagara-on-the-Lake, Ontario.
 
Streef extends sweet spuds
Streef Produce president Peter Streef is serving his second term as a director on the Ontario Asparagus Growers marketing board.
The Streef family grows 50 acres of the spring crop and markets 500 acres, which represents 15% of its business at the Ontario Food Terminal.
Streef said he’s also extending his Ontario sweet potato program.
The company is also expanding its Mexican program from October to June, importing greenhouse peppers, cucumbers and tomatoes.
 
Tomato King expands organic
Tomato King at the Ontario Food Terminal has expanded its organic line from 20 to 50 SKUs from all over the world, said partner Vince Carpino. Berries remain the biggest category, he said.
The company has also expanded its offshore grape deal with Chile, Argentina and South Africa, which Carpino said is going well. He said Tomato King plans to expand its warehouse space this year and offer more new product lines. ?
 
Whole Foods grows in Toronto
Toronto now has three Whole Foods Markets.
Produce buyer-merchandiser Stuart Coleman said the retailer is working with more Ontario greenhouses to provide local tomatoes, cucumbers and lettuce over the winter.
Whole Foods has also introduced packages of ready-to-blend produce for its new smoothie program.
Customers new to the smoothie game or looking for a different flavor combination can buy a package of fresh-cut fruit and vegetables with names like Earth Warrior, Green Rookie, or Spicy Sunrise, add their favorite liquid and blend.