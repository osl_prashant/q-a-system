Last week was a crazy week in the markets. The Dow Jones Industrial Average saw one of the worst weeks since 2009.

“Anything could happen at any time, and when everybody’s expecting a certain thing, it just doesn’t happen in the stocks,” said Andy Shissler, S&W Trading on AgDay.

The market swings taught some traders like Shissler some lessons. Joe Vaclavik, president of Standard Grain, says the long road to the top of the stock market with low volatility “turned on a dime” both in the stock market and in grains.

“USDA told us soybean exports aren’t good, corn exports are good and improving, so we’ve got some things changing in the grains a little bit,” he said.

Looking ahead, Shissler thinks people are apprehensive about Argentina’s weather models.

“Their weather looked like we should be rallying, and then we weren’t,” he said. “If it doesn’t [rain], we’ll move up in beans and corn, and we’ll hit some numbers that need to be sold.”

As for Vaclavik, he’s watching weather too, but as it relates to the wheat market.

“What we’ve got going on in the U.S. Plains is another thing [compared to Argentina],” he said. “We desperately need some rain in Kansas, Oklahoma, North Texas. The wheat market is going to relate to whatever happens there.”

Watch their full commentary on AgDay above.