Supporters of the Trans Pacific Partnership (TPP) are increasingly concerned that the window to tap into new Asian markets is closing.
Both Donald Trump and Hillary Clinton say they oppose TPP, and that just leaves three months for President Obama to convince Congress to get the deal done. But with increasing resistance to TPP in Congress, analysts believe the chances of passage are growing slim.
At stake for American farmers and ranchers is an increase in food exports that would amount to about $5 billion annually.
Producer groups such as the American Farm Bureau, the National Pork Producers Council and the National Cattlemen's Beef Association believe TPP is the biggest commercial opportunity for agriculture in modern history. NCBA President Tracy Bruner says his organization has called on Congress to pass the agreement. In a statement issued earlier this year, Bruner said "TPP would not only lower the taxes on U.S. beef into critical markets like Japan and level the playing field with our competitors, it would provide a boon to the entire U.S. economy."
According to estimates by the US International Trade commission, overall beef exports would be about $876 million higher once TPP is fully implemented and that it would have a moderate impact on U.S. beef imports.
Bruner said the markets represented by TPP "add value to every head of cattle raised and fed in the United States."


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.