BC-US--Cotton, US
BC-US--Cotton, US

The Associated Press

New York




New York (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Monday:
(50,000 lbs.; cents per lb.)
COTTON NO.2Open    High    Low    Settle     Chg.May       82.08   82.57   81.50   81.78  Down  .05Jul       82.22   82.83   81.90   82.22  unchAug                               77.70  Up    .04Oct                               78.88  Down  .08Oct                               77.70  Up    .04Dec       77.51   77.92   77.32   77.70  Up    .04Dec                               77.88  Up    .03Mar       78.00   78.09   77.60   77.88  Up    .03May       78.11   78.14   77.92   77.92  Up    .03Jul       78.14   78.17   77.90   77.90  Up    .03Aug                               73.39  Down  .11Oct                               75.28  Down  .11Oct                               73.39  Down  .11Dec       73.60   73.60   73.39   73.39  Down  .11Dec                               73.48  Down  .11Mar                               73.48  Down  .11May                               74.03  Down  .11Jul                               74.11  Down  .11Aug                               72.51  Down  .11Oct                               73.68  Down  .11Oct                               72.51  Down  .11Dec                               72.51  Down  .11