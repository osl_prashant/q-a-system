Brazilian meat industry groups denied on Thursday problems with beef and pork shipments to Russia, after reports that food safety bodies there were evaluating possible irregularities that could lead to an import freeze.
Reuters reported on Wednesday that Russia’s agriculture safety watchdog was mulling a ban on all pork and beef imports from Brazil after finding the feed additive ractopamine in some shipments.
Ractopamine is banned in Russia, though some countries deem it safe for human consumption.
“All Brazilian meatpacking companies are obliged to check cattle for ractopamine... Our inconformity problems, from what I can remember, do not include that,” Antonio Jorge Camardelli, head of beef exporters association Abiec, told Reuters.
He said Brazil ships between 7,000 and 9,000 tonnes of beef to Russia every month and companies are “comfortable” regarding the health status of the cargos.
ABPA, an association representing pork producers, also denied the possibility of shipments containing the additive. Russia buys around 40 percent of all Brazilian pork exports.
Russia is currently in talks with Brazil over the possible start of wheat exports.
Brazil’s agriculture ministry did not return a request for comment.