BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Tuesday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2815  Up    89May         2769    2835    2769    2835  Up    89Jul                                 2827  Up    77Jul         2717    2837    2717    2815  Up    89Sep         2746    2851    2744    2827  Up    77Dec         2745    2835    2738    2813  Up    69Mar         2720    2802    2713    2780  Up    60May         2712    2791    2706    2771  Up    59Jul         2726    2771    2726    2769  Up    58Sep         2764    2770    2764    2770  Up    58Dec         2742    2767    2742    2767  Up    54Mar                                 2775  Up    54