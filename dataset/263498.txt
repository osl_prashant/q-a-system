Manure offers many soil health benefits and nutrients for crop production. Pork producers need to retain as much value as possible from manure applications to keep balance sheets in the black during expansion. The key is to know exactly how much manure you are applying.
Federal and state regulations require manure management plans, but farmers should also responsibly measure manure application rates in the field. Calibrating applicators ensure the right amount of product is spread to adhere to the management plan and for the upcoming crop.
There is a simple process to calibrate applicators from the Michigan State University Extension: 
A. Calibrating by volume (liquid manure)
1. Determine capacity (in gallons) of the liquid tank wagon. Since manufacturers often “round up” the capacity, the gallonage provided by the manufacturer for your tank wagon may be high.
2. Calculate the land area covered with one tank wagon load as:
Width (ft) x distance (ft)/43,560 ft^2 per acre = total area in acres
3. Divide the volume of manure in one load by the acreage covered:
total gallons/total acres = gallons applied per acre (gal/ac)
4. Multiply the manure application (gal/ac) by the nutrient content of the manure (lb/1,000 gal) to determine nutrient application/acre.
Gal/ac x lb nutrient/1,000 gal = lb nutrient/ac
5. Adjust application rate to obtain desired nutrient rate per acre.
Desired nutr. Rate (lb/ac)/current nutr. Rate (lb/ac) x current appl. Rate (gal/ac)
B. Calibrating by weight (liquid manure)
Use this option if you have access to a drive-on scale because it is the easiest, and most accurate, method.
1. Weigh the loaded tank wagon and tractor, apply the manure, and re-weigh the tank wagon and tractor. Subtract the empty weight from the loaded weight to get the “lb of manure applied.”
2. Determine the amount of land area treated (Step A.2).
3. Calculate the weight of manure applied per acre:
Lb of manure applied/total area (acres) = lb. of manure/acre
4. To determine the gallons applied per acre:
Lb manure applied/acre / 8.3 lb./gal = gal of manure/acre
5. Multiply either the weight or volume of manure applied per acre by the nutrient content in lb per 1,000 gal. or per ton.
6. Adjust manure application to obtain the desired nutrient rate per acre (Step A.5).

Because liquid manure has many variations, it’s important to sample and test for the nutrient content. Plus any change in feed ingredient should be analyzed, as a change in the manure is likely.