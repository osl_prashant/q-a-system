The Latest: Lula's lawyer: ex-leader will not resist arrest
The Latest: Lula's lawyer: ex-leader will not resist arrest

The Associated Press

SAO BERNARDO DO CAMPO, Brazil




SAO BERNARDO DO CAMPO, Brazil (AP) — The Latest on the looming arrest of Brazil's ex-President "Lula" (all times local):
6 p.m.
A defense attorney for former President Luiz Inacio Lula da Silva says that the former leader will not resist his arrest.
Lawyer Jose Roberto Batochio told Folha de S. Paulo newspaper Friday that "Lula" will not "go to the slaughterhouse crestfallen" and instead will turn himself in to authorities "out of his free will."
Da Silva defied a 5 p.m. deadline to turn himself into police in the city of Curitiba so he can begin to serve a 12-year sentence on a corruption conviction.
He has been with thousands of supporters since late Thursday at a union in the Sao Paulo suburb of Sao Bernardo do Campo.
Clashes could break out if da Silva is forced out of the union building Friday night.
___
5:01 p.m.
Ex-President Luiz Inacio Lula da Silva has defied a court order to turn himself into police.
The former leader had until 5 p.m. Friday local time to present himself to authorities in Curitiba. The city is about 260 miles (417 kilometers) southwest of the Sao Paulo.
Instead, da Silva hunkered down with supporters at a metallurgical union in the Sao Paulo suburb of Sao Bernardo do Campo.
Federal judge Sergio Moro, seen by many in Brazil as a crusader against endemic graft, on Thursday had ordered da Silva's arrest.
Last year, Moro convicted da Silva of corruption. Moro wants da Silva to begin serving his sentence of 12 years and one month.
Da Silva has always denied wrongdoing. He says he is being persecuted so that he won't be able to run for president in October.
___
4:47 p.m.
Brazil's top appeals court has rejected the latest plea by former President Luiz Inacio Lula da Silva to stay out of jail until he has exhausted all appeals of his corruption conviction.
The decision was announced in a court document Friday, less than an hour before da Silva's 5 p.m. deadline to turn himself in to authorities.
___
4:33 p.m.
A Brazilian newspaper is reporting that an airplane and helicopter are ready to transport former President Luiz Inacio Lula da Silva to prison in the city of Curitiba.
"Lula" continues to be hunkered down at a metallurgical union in the Sao Paulo suburb of Sao Bernardo do Campo. That's about 260 miles (417 kilometers) southwest of Curitiba.
The Estado de S. Paulo newspaper says Friday that if da Silva decides to travel to Curitiba on a private plane, the federal police will be waiting for him at the airport and will transport him to prison by helicopter.
A plane is also waiting for him if he turns himself in to authorities in Sao Paulo, the outlet says.
Neither scenario looked likely since Lula remained in Sao Bernardo do Campo about a half hour before the 5 p.m. deadline that he was given to present himself.
___
3:35 p.m.
Two sources close to former President Luiz Inacio Lula da Silva tell The Associated Press that he will not travel to the city of Curitiba to turn himself in to authorities.
The sources said Friday that "Lula" is considering either waiting for police at the union where he has gathered with supporters or presenting himself in Sao Paulo. They spoke on the condition of anonymity because they were not authorized to share internal deliberations being discussed.
The former leader has by 5 p.m. local time to present himself in Curitiba.
It remains to be seen whether he will defy the arrest order in an attempt to wait for a higher court to rule on a last-minute appeal.
Clashes could break out with supporters if authorities come to arrest him.
___
2:36 p.m.
Demonstrators for and against the arrest of former President Luiz Inacio Lula da Silva are taking to the streets across Brazil.
Da Silva has a 5 p.m. Friday deadline to turn himself into authorities after a federal judge issued an arrest order to begin serving a sentence of 12 years and one month for a corruption conviction.
The Globo television network's G1 internet portal said demonstrators took to the streets in more than a dozen Brazilian states Friday to show their support for the former leader.
G1 said detractors traded insults with supporters of "Lula" in the political capital of Brasilia. Police also seized knifes and sticks.
Da Silva left office with high approval ratings and remains popular among many Brazilians. He had hunkered down at a metallurgical union hours before he was supposed to show up at a jail in the city of Curitiba.