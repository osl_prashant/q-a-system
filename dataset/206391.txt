The Ministry for Primary Industries said on Wednesday that it strongly suspects a third farm in the South Island has been infected with a bacterial cattle disease that can have a serious effect on animal health.
Two farms in the Van Leeuwen Dairy Group tested positive in July for the Mycoplasma bovis, which is spread by close contact between animals and does not pose a food safety risk or any risk to humans.
Some animals in a third farm, with a direct connection to one of the infected farms, have tested positive for the disease but further tests are still under way, the ministry said in a statement.
"The disease is being well contained on the known properties and we are confident our control measures are sufficient to contain it there," Geoff Gwyn, the director of response at the ministry said.
Mycoplasma bovis is common in many countries and can lead to conditions such as udder infection, pneumonia and arthritis in affected cattle.
Gwyn said the latest affected farm has been put under restrictions and no animals had left the property since July 20.
"However it is understood that before this, some animals were moved to a number of other farms. MPI (the ministry) is contacting those properties and is testing animals with urgency."
The third farm affected had received some animals from one of the Van Leeuwen Dairy Group farms, a large scale dairy business, before the disease was detected in the country, he said.
News in July of the country's first confirmed cases briefly knocked the New Zealand dollar given the importance of the cattle industry for the economy.