Grocery delivery service Instacart launched partnerships with Kings Food Markets, Lowes Foods, Food Lion and several other retailers in of October.
Kings Food Markets, along with Balducci’s Food Lover’s Market, plans to offer grocery delivery to more than 1 million households in New Jersey, New York, Connecticut, Maryland and Virginia, according to Instacart.
Food Lion will have the service for shoppers in certain areas of Virginia and North Carolina.
Stew Leonard’s plans to offer delivery to more than 350,000 households in parts of Connecticut and New York, and Lowes Foods will make the service an option for more than 350,000 households in areas in North Carolina.
Superior Grocers, which will serve parts of southern California, will be the first Hispanic retailer in the area to use Instacart.
Instacart is also now working with Graul’s Market, which offers the service in the Baltimore area.