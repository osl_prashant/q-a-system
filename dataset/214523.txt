USDA Weekly Export Sales Report
			Week Ended Nov. 2, 2017





Corn



Actual Sales (in MT)

Combined: 2,938,700
			2017-18: 2,364,500
			2018-19: 574,200 



Trade Expectations (MT)
2017-18: 1,200,000-1,600,000
			2018-19: 500,000-700,000 


Weekly Sales Details
Net sales of 2,364,500 MT for 2017-18 were up noticeably from the previous week and 92% from the prior 4-week average.  Increases were reported for Mexico (1,166,300 MT, including decreases of 2,000 MT), South Korea (468,000 MT), Japan (337,500 MT, including 39,800 MT switched from unknown destinations and decreases of 300 MT), unknown destinations (213,000 MT), and Colombia (132,700 MT, including 75,100 MT, switched from unknown destinations).  Reductions were reported for Brazil (60,700 MT).  For 2018-19, net sales of 574,200 MT reported for Mexico (577,700 MT), were partially offset by reductions for Peru (3,500 MT). 


Weekly Export Details
Exports of 489,800 MT were primarily to Mexico (171,900 MT), Colombia (115,200 MT), Japan (69,500 MT), Honduras (39,500 MT), and Peru (35,400 MT).


Comments and Performance Indicators
Old-crop sales were much stronger than anticipated while new-crop sales were as expected. Export commitments at the start of the 2017-18 marketing year are running 25% behind year-ago compared with 31% behind last week. USDA projects exports in 2017-18 at 1.850 billion bu., down 19.3% from the previous marketing year.




Wheat



Actual Sales (in MT)

2017-18: 781,700



Trade Expectations (MT)
2017-18: 350,000-550,000


Weekly Sales Details
Net sales of 781,700 metric tons--a marketing year high--for delivery in marketing year 2017-18 were up noticeably from the previous week and from the prior 4-week average.  Increases were for Iraq (450,000 MT), Taiwan (87,400 MT), China (60,000 MT), the Philippines (55,500 MT), Nigeria (40,000 MT), and Vietnam (39,700 MT, including 39,000 MT switched from unknown destinations).  Reductions were reported for unknown destinations (79,000 MT).  


Weekly Export Details
Exports of 298,000 MT were down 21% from the previous week, but up 5% from the prior 4-week average.  The primary destinations were Mexico (55,500 MT), the Philippines (40,300 MT), South Korea (33,300 MT), Nigeria (33,000 MT), and Guatemala (32,100 MT).


Comments and Performance Indicators
Sales handily topped expectations. Export commitments for 2017-18 are running 5% behind year-ago versus 5% behind the week prior. USDA projects exports in 2017-18 at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual Sales (in MT)

Combined: 1,161,000
			2017-18: 1,160,600
			2018-19: 400 



Trade Expectations (MT)
2017-18: 1,300,000-1,800,000
			2018-19: 0-50,000 


Weekly Sales Details
Net sales of 1,160,600 MT for 2017-18 were down 39% from the previous week and 34% from the prior 4-week average.  Increases were reported for China (1,156,900 MT, including 716,000 MT switched from unknown destinations and decreases of 23,800 MT), Mexico (196,000 MT, including decreases of 5,000 MT), the Netherlands (84,600 MT, including 74,000 MT switched from unknown destinations), Saudi Arabia (71,500 MT, including 66,000 MT switched from unknown destinations), and Germany (65,300 MT).  Reductions were reported for unknown destinations (707,500 MT), Bangladesh (11,500 MT), and Burma (500 MT).  For 2018-19, net sales of 400 MT were reported for Japan.  


Weekly Export Details
Exports of 2,520,200 MT were primarily to China (1,790,800 MT), Mexico (157,100 MT), the Netherlands (84,600 MT), Saudi Arabia (71,500 MT), and Egypt (66,400 MT).   


Comments and Performance Indicators
Sales for 2017-18 were lighter than anticipated, while 2018-19 sales were near the low end of expectations. Export commitments are running 15% behind year-ago compared with 16% behind last week. USDA projects exports in 2017-18 at 2.250 billion bu., up 3.5% from year-ago.




Soymeal



Actual Sales (in MT)
2017-18: 212,900


Trade Expectations (MT)

2017-18: 100,000-300,000



Weekly Sales Details
Net sales of 212,900 MT for 2017-18 were reported for Mexico (111,300 MT, including decreases of  1,200 MT), Thailand (50,000 MT, switched from unknown destinations), Japan (25,900MT), Canada (22,400 MT, including decreases of  500 MT), Peru (19,400 MT), and Guatemala (18,200 MT, including 14,100 MT switched from unknown destinations).  Reductions were reported for  unknown destinations (44,100 MT), Bangladesh (38,000 MT), and the French West Indies (4,200 MT). 


Weekly Export Details
Exports of 195,500 MT were reported to Colombia (39,900 MT), Mexico (35,900 MT), Peru (31,400 MT), Guatemala (18,500 MT), Honduras (17,900 MT), Canada (17,100 MT), and Panama (8,000 MT).


Comments and Performance Indicators
Sales met expectations. Export commitments for 2017-18 are running 6% ahead of year-ago versus 5% ahead of year-ago last week. USDA projects exports in 2017-18 to be up 3.5% from the previous marketing year.




Soyoil



Actual Sales (in MT)

2017-18: 15,900 



Trade Expectations (MT)
2017-18: 8,000-30,000


Weekly Sales Details
Net sales of 15,900 MT for 2017-18 were reported for South Korea (5,000 MT), Colombia (4,000 MT), Jamaica (3,500 MT),  Mexico (2,300 MT), and Canada (900 MT). 


Weekly Export Details
Exports of 7,000 MT were reported to Mexico (3,400 MT), South Korea (3,000 MT), and Canada (400 MT).








Comments and Performance Indicators
Sales were in line with expectations. Export commitments for the 2017-18 marketing year are running 46% behind year-ago compared with 48% behind year-ago last week. USDA projects exports in 2017-18 to be down 17.6% from the previous year.




Cotton



Actual Sales (in RB)
Combined: 236,500
			2017-18: 205,300
			2018-19: 31,200 


Weekly Sales Details
Net sales of 205,300 running bales for 2017-18 were down 2% from the previous week and 9% from the prior 4-week average. Increases were reported for Pakistan (67,500 RB), Bangladesh (28,100 RB), Turkey (27,800 RB), Vietnam (24,200 RB, including 1,800 RB switched from China, 1,300 RB switched from Taiwan, and decreases of 2,600 RB), and China (11,500 RB, including decreases of 8,800 RB). Reductions were reported for Japan (1,400 RB) and Nicaragua (300 RB). For 2018-19, net sales of 31,200 RB were reported for China (24,200 RB), Pakistan (4,800 RB), and Japan (2,200 RB).


Weekly Export Details
Exports of 124,300 RB were up 43 percent from the previous week and  29 percent from the prior 4-week average.  The primary destinations were reported  to  Vietnam (27,800 RB), China (21,400 RB), Mexico (20,400 RB), South Korea (11,900RB), and Thailand (9,300 RB). 


Comments and Performance Indicators
Export commitments for 2017-18 are running 38% ahead of year-ago compared to 38% ahead last week. USDA projects exports in 2017-18 will decline 2.8% from the previous year to 14.5 million bales.




Beef



Actual Sales (in MT)

Combined: 22,300
			2017: 16,800 
2018: 5,500



Weekly Sales Details
Net sales of 16,800 MT reported for 2017 were up 1% from the previous week and 12% from the prior 4-week average.  Increases were reported for Japan (8,400 MT, including decreases of 1,100 MT), South Korea (2,700 MT, including decreases of 500 MT), Hong Kong (1,600 MT, including decreases of 200 MT), Mexico (1,300 MT, including decreases of 100 MT), and Taiwan (1,000 MT, including decreases of 200 MT).  For 2018, net sales of 5,500 MT were reported for Hong Kong (3,900 MT), Japan (1,000 MT), and Taiwan (300 MT).  


Weekly Export Details
Exports of 14,900 MT were down 16% from the previous week and 7% from the prior 4-week average.  The primary destinations were Japan (4,200 MT), South Korea (3,900 MT), Hong Kong (2,100 MT), Mexico (1,600 MT), and Canada (1,000 MT). 


Comments and Performance Indicators
Last week, USDA reported export sales totaling 20,000 MT for 2017 and 2018, combined. USDA projects exports in 2017 to be up 10.9% from last year's total. 




Pork



Actual Sales (in MT)

Combined: 8,100
			2017: 7,800 
2018: 300 



Weekly Sales Details
Net sales of 7,800 MT reported for 2017--a marketing year low--were down 70% from the previous week and 56% from the prior 4-week average.  Increases were reported for Japan (4,300 MT), the Philippines (700 MT), China (600 MT), Canada (600 MT), and Honduras (500 MT). For 2018, net sales of 300 MT were primarily for South Korea (200 MT) and Colombia (100 MT).


Weekly Export Details
Exports of 23,200 MT were down 2% from the previous week, but up 4% from the prior 4-week average.  The destinations were primarily Mexico (8,300 MT), Japan (3,900 MT), South Korea (3,600 MT), Canada (1,600 MT), and Hong Kong (1,200 MT). 


Comments and Performance Indicators

Last week, USDA reported pork export sales totaling 26,300 MT. USDA projects exports in 2017 to be 9.0% above last year's total.