Boskovich adds organic squash
Boskovich Farms Inc., based in Oxnard, Calif., is growing organic squash for the first time this season, said Darrell Beyer, organic sales manager.
“We’re just trying to build on the base, cover a few more items, try and offer a one-stop shop,” he said.
Squash volume will be minimal to start, Beyer said.
“Maybe a couple loads a week. It all depends on how the harvest starts,” he said.
 
CMI Orchards hires marketing VP
George Harter, a retail veteran from Kroger, recently was hired to the position of vice president of marketing for Wenatchee, Wash.-based CMI Orchards.
Harter has taken over a position vacated by Steve Lutz, who shifted into a new role with the company as senior strategist.
 
Dole supports pineapple growers
For the third consecutive year, Westlake Village, Calif.-based Dole Food Co. offered a monthlong promotion during April to boost its support of organic pineapple growers worldwide and enlist its wholesale and retail customers in showing support, said Bil Goldfield, director of corporate communications.
Throughout April, Dole donated a portion of sales proceeds of Dole organic pineapples purchased by a wholesaler or retailer to the Organic Farming Research Foundation, a Santa Cruz, Calif.-based organization that works to foster the improvement and widespread adoption of organic farming systems, Goldfield said.
“This is an effort by Dole to support local and small organic farming with more knowledge and tools to help improve operations and product,” he said.
 
Homegrown Organic rolls out freeze-dried
Porterville, Calif.-based Homegrown Organic Farms is developing a line of freeze-dried fruit, said Scott Mabs, CEO.
So far, the company has launched Homegrown Organic Farms-labeled freeze-dried apples, grapes and blueberries, with strawberries planned, Mabs said.
The products, offered in 1.2-ounce bags, retail in a $4.99-5.99 range.
They’re available in a good number of retailers across the U.S., Mabs said.
 
Oppenheimer names organics manager
Vancouver, British Columbia-based The Oppenheimer Group hired Chris Ford as its organics category manager.
Ford, who has been involved with organic produce since 1994, is based in Salinas, Calif. He started with Oppenheimer March 1, he said.
Ford came to Oppy with more than two decades of fresh produce and organics expertise gained in buying and management roles with such retailers as Whole Foods Markets, Wild Oats and Alfalfa’s Market.
Ford’s experience also includes leadership positions on the supply side, with six years as a commodity manager for Earthbound Farm, San Juan Bautista, Calif., and, most recently, as vice president for Sutherland Produce Sales in El Cajon, Calif.
Oppy’s organic portfolio includes such items as greenhouse-grown OriginO sweet bell peppers, cucumbers and tomatoes, Divemex sweet bell and mini peppers, as well as kiwifruit, citrus, mangoes, apples, pears and berries.
 
Organics Unlimited expands in Mexico
San Diego-based Organics Unlimited Inc. is increasing its organic acreage in Mexico, said Mayra Velazquez de Leon, president and CEO.
“We’re in the process of transitioning land — I’d say about 100 hectares (247 acres) for next year,” she said.
“It’s going to be going little by little into more and more, not only transitioning our own land but contracting growers. That kind of helps us grow at a faster pace.”
The growth is mostly in banana production, Velazquez de Leon said.
Organics Unlimited now either represents or owns 1,235 acres in Mexico and 864 in Ecuador, Velazquez de Leon said.
Organics Unlimited ships bananas year-round, with a distribution base in San Diego.
The company also is launching its GROW label in Japan this year, Velazquez de Leon said.
GROW is an Organics Unlimited program that is comparable to Fair Trade, Velazquez de Leon said.
The first load of GROW-labeled products is scheduled to reach retail shelves in Japan by the end of May, Velazquez said.
 
Stemilt Growers boosts organics
Wenatchee, Wash.-based Stemilt Growers LLC is in the process of transitioning more land to organic production, said Roger Pepperl, marketing director.
“We have some converting this fall,” he said.
“We have a lot converting in the next three years. We’ll double in organics in the next four years and most of that is in apple production itself.”
Pepperl estimated Stemilt’s balance of conventional/organic production is about 75%/25% on apples; 85%/15% on pears; and about 90%/10% on cherries.
 
Sundance Organics increases oranges
Oceanside, Calif.-based Sundance Organics has added acreage for valencia orange production, said Bill Hahlbohm, owner.
“We’ve taken quite a bit more acreage of valencia oranges — maybe another couple of hundred acres,” he said.
That gives the company about 600 acres of organic valencias, Hahlbohm said.
The expansion is a result of demand, he said.
“Everybody wants them, and there’s a long season for valencias,” he said.
The season runs roughly from April to January each year.
The company also is growing about 20% more star ruby grapefruit and the same percentage increase in lemons this year than last, Hahlbohm said.
“We always have a lot of meyer lemons, too, during the season, and our minneola tangelo deal is working out well,” he said.
 
Viva Tierra expands California program
Mount Vernon, Wash.-based Viva Tierra Organic Produce is expanding its California organic program, especially in apples, this year, said Matt Roberts, sales manager.
“There’s been a lot more transition and we’ll have a lot more volume to keep up with demand, when it is kind of tight in the July-August period,” he said.
“Growing that (apple) crop, we’ll have more domestic production during that time. We’ll have some good growers in Central Valley (California) — Sacramento south to Reedley — to fill that gap.”
 
World Variety offers produce kits
Los Angeles-based Melissa’s Produce recently has launched two organic value-added products: Organic Garden Grab Bag and Organic Soup Starter Kit.
“The thinking is, people are so pressed for time, it’s a good grab-and-go, simpler meal solution in a bag,” said Robert Schueller, director of marketing.
“We’re appealing for the pressed-for-time shopper.”
The Soup Starter Kit features russet potatoes, carrots, zucchini, celery stalk and yellow onion in a 2½-pound tote.
The Organic Grab Bag, a 2-pound tote, contains a green bell pepper, red bell pepper, cucumber, zucchini, carrot, head of garlic and a shallot.
The products debuted nationally in December, Schueller said.
The suggested price is $3.49-3.99, Schueller said, and each is available year-round.