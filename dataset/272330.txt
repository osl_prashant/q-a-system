Volunteers plant nearly 6,000 trees for Arbor Day
Volunteers plant nearly 6,000 trees for Arbor Day

The Associated Press

DURHAM, N.H.




DURHAM, N.H. (AP) — Nearly 6,000 aspen and willow trees were planted by Sunday in honor of Arbor Day at a river preserve in Durham.
More than 100 volunteers contributed to the efforts, which started on the holiday Friday with about 60 students from Phillips Exeter Academy. The trees were planted across six acres at the Lamprey River Preserve.
Shrubland plants also were planted, including dogwood, alder and winterberry. The short plants create habitat for some rare species of birds, including woodcocks, according to Joanne Glode, stewardship ecologist at The Nature Conservancy.
"We thought there was no better way of celebrating Arbor Day than by planting a tree," Glode said.
The Nature Conservancy teamed up with the Strafford County Conservation District and a local farmer from Lee, Dorn Cox, to organize the volunteer event.
Cox leases the land from the environmental organization and is hoping the planting will help establish some fast-growing crops, to be harvested and sold within the next few years.
The project was funded by a grant through the Moose license plate program, which uses revenue from the sale of the license plates to support the protection of critical resources in New Hampshire.