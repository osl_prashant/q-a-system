Tyson Foods expanding safety programs to 12 poultry plants
Tyson Foods expanding safety programs to 12 poultry plants

The Associated Press

OMAHA, Neb.




OMAHA, Neb. (AP) — Tyson Foods is expanding the safety programs it has been testing in its beef plants to 12 poultry plants, allowing workers there to stop the line if they have safety concerns.
The Springdale, Arkansas-based company said Thursday it developed the measures in cooperation with the United Food and Commercial Workers International union.
The policies allow workers to stop the production line if they see a safety issue, and workers are involved in plant safety committees. They are part of Tyson's ongoing effort to reduce injuries related to the dangerous work in a meatpacking plant.
Tyson Fresh Meats President Steve Stouffer said there has been a decrease in injuries and turnover at plants using the measures over the past three-to-five years.
The number of injuries and illnesses that Tyson reported per 100 employees declined from 8.16 in the company's 2015 fiscal year to 6.58 in 2016 and 5.08 last year.
"We're proud of the progress we've made," Stouffer said
Mark Lauritsen with the union said it's clear Tyson values worker input.
"We have workers who know they can speak up" Lauritsen said. "It's beyond the lip service you might see at some companies about having workers involved."
The new policies will be added to other safety efforts Tyson has made in its poultry plants in recent years, including adding more than 300 trainers and improving safety communications.
Tyson officials say the company has been working closely with the union for three decades since it began developing ergonomics programs. They discussed that partnership during a visit to the company's plant in Dakota Dunes, S.D., on Thursday.