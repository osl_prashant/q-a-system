This market update is a PorkBusiness weekly column reporting trends in weaner and feeder pig production. All information contained in this update is for the week ended March 30, 2018.

Looking at hog sales in September 2018 using October 2018 futures the weaner breakeven was $30.37, down $0.04 for the week. Feed costs were up $1.29 per head. October futures increased $0.63 compared to last week’s October futures used for the crush and historical basis is unchanged from last week. Breakeven prices are based on closing futures prices on March 30, 2018. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was above average this week with 36,500 head being reported. This is 110% of the 52-week average. Cash prices were $40.75, down $2.16 from a week ago. The low to high range was $32.00 - $49.00. Formula-priced weaners were down $1.21 this week at $39.54.
Cash-traded feeder pig reported volume was below average with 14,050 head reported. Cash feeder pig reported prices were $73.42, down $1.14 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through March 30, 2018.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential increased $2.12 this week to a projected loss of $10.38 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com