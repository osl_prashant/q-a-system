USDA Weekly Export Sales Report
			Week Ended June 15, 2017





Corn



Actual Sales (in MT)

Combined: 652,800
			2016-17: 124,000
			2017-18: 528,800



Trade Expectations (MT)
650,000-1,050,000


Weekly Sales Details
Net sales of 528,800 MT for 2016-17 were down 12% from the previous week, but up 16% from the prior 4-week average. Increases were reported for Japan (249,600 MT, including 57,500 MT switched from unknown destinations and decreases of 13,100 MT), Mexico (82,200 MT, including 25,000 MT switched from unknown destinations and decreases of 7,300 MT), Israel (55,500 MT, including 50,000 MT switched from unknown destinations), South Korea (52,800 MT, switched from unknown and decreases of 10,700 MT), and Spain (48,500 MT, including 40,000 MT switched from unknown destinations). Reductions were reported for unknown destinations (118,600 MT), Colombia (4,300 MT), and the Dominican Republic (3,700 MT). For 2017-18, net sales of 124,000 MT were reported for Mexico (120,000 MT) and El Salvador (4,000 MT). 


Weekly Export Details
Exports of 1,211,500 MT were up 22% from the previous week and 6% from the prior 4-week average. The primary destinations were Mexico (378,100 MT), Japan (336,300 MT), South Korea (127,000 MT), Israel (67,500 MT), and Peru (49,500 MT).


Comments and Performance Indicators
Sales were near the lower end of expectations. Export commitments for 2016-17 are running 19% ahead of year-ago compared to 20% ahead the week prior. USDA projects exports in 2016-17 at 2.225 billion bu., up 17.2% from the previous marketing year.




Wheat



Actual Sales (in MT)

2017-18: 542,900



Trade Expectations (MT)
300,000-500,000 


Weekly Sales Details
Net sales of 542,900 metric tons were reported for delivery in marketing year 2017-18. The primary destinations were Japan (196,500 MT), Taiwan (94,900 MT), the Philippines (81,000 MT, including 55,000 MT switched from unknown destinations, 5,000 MT switched from Indonesia, and decreases of 3,600 MT), Algeria (59,500 MT, including 60,000 MT switched from unknown destinations and decreases of 2,000 MT), and Thailand (45,500 MT). Reductions were reported for unknown destinations (114,300 MT), Indonesia (5,000 MT), and Belize (1,700 MT). 


Weekly Export Details
Exports of 717,800 MT were reported to South Korea (119,900 MT), the Philippines (113,000 MT), Algeria (89,500 MT), Japan (73,200 MT), and China (62,600 MT). 


Comments and Performance Indicators
Sales were a bit stronger than expected. Export commitments for 2017-18 are running 8% ahead of year-ago versus 7% ahead the week prior.USDA projects exports in 2017-18 at 1.0 billion bu., down 3.4% from the previous marketing year.




Soybeans



Actual Sales (in MT)

Combined: 115,000
			2016-17: 111,200
			2017-18: 3,800



Trade Expectations (MT)
350,000-750,000 


Weekly Sales Details
Net sales of 111,200 MT for 2016-17 were down 67% from the previous week and 72% from the prior 4-week average. Increases were reported for Indonesia (72,000 MT, including 63,500 MT switched from unknown destinations and decreases of 6,600 MT), Bangladesh (57,900 MT, including 57,700 MT switched from unknown destinations), Cuba (21,600 MT), Canada (21,200 MT, including decreases of 1,000 MT), and Mexico (11,600 MT). Reductions were reported for unknown destinations (91,200 MT), China (1,200 MT), and Barbados (800 MT). For 2017-18, net sales of 3,800 MT were reported for Canada (3,300 MT) and Thailand (500 MT). 


Weekly Export Details
Exports of 296,100 MT were down 39% from the previous week and 21% from the prior 4-week average. The destinations were primarily Indonesia (80,700 MT), China (66,900 MT), Bangladesh (59,000 MT), Canada (32,700 MT), and Japan (12,300 MT). 
 



Comments and Performance Indicators
Sales were much lighter than expected. Export commitments for 2016-17 are running 18% ahead of year-ago, compared to 20% ahead the previous week. USDA projects exports in 2016-17 at 2.050 billion bu., up 5.9% from year-ago.




Soymeal



Actual Sales (in MT)
Combined: 132,200
			2016-17: 86,600
			2017-18: 45,600


Trade Expectations (MT)

50,000-300,000 



Weekly Sales Details
Net sales of 86,600 MT for 2016-17 were down 48% from the previous week and 31% from the prior 4-week average. Increases were reported for Mexico (27,900 MT), the Dominican Republic (20,600 MT), Guatemala (12,500 MT, switched from unknown destinations), El Salvador (10,000 MT, including 5,500 MT switched from Guatemala and 4,500 MT switched from unknown destinations), and Malaysia (9,500 MT). Reductions were reported for unknown destinations (27,700 MT), Japan (600 MT), and Honduras (200 MT). For 2017-18, net sales of 45,600 MT were reported primarily for Mexico (38,300 MT), Ecuador (7,000 MT), and Canada (300 MT). 


Weekly Export Details
Exports of 161,800 MT were down 25% from the previous week and 10% from the prior 4-week average. The destinations were primarily the Dominican Republic (41,100 MT), Mexico (36,000 MT), Guatemala (16,000 MT), Canada (15,500 MT), and Colombia (13,100 MT). 


Comments and Performance Indicators
Sales were as expected. Export commitments for 2016-17 are running 2% ahead of year-ago compared with 3% ahead of year-ago the week prior. USDA projects exports in 2016-17 to be up 0.3% from the previous marketing year.




Soyoil



Actual Sales (in MT)

Combined: 17,400
			2016-17: 13,300
			2017-18: 4,100



Trade Expectations (MT)
8,000-40,000


Weekly Sales Details
Net sales of 13,300 MT for 2016-17 were down 56% from the previous week and 34% from the prior 4-week average. Increases were reported for South Korea (5,000 MT), Jamaica (3,500 MT), Mexico (2,800 MT), and Venezuela (2,000 MT, switched from unknown destinations). Reductions were reported for unknown destinations (2,500 MT). For 2017-18, net sales of 4,100 MT were reported for Mexico. 


Weekly Export Details
Exports of 15,200 MT were down 9% from the previous week, but up 17% from the prior 4-week average. The destinations were primarily South Korea (11,000 MT), Mexico (3,300 MT), Canada (600 MT), and Honduras (100 MT). 


Comments and Performance Indicators
Sales were within expectations. Export commitments for the 2016-17 marketing year are running 1% behind year-ago, compared to steady with year-ago last week. USDA projects exports in 2016-17 to be up 2.7% from the previous year.




Cotton



Actual Sales (in RB)
Combined: 594,300
			2016-17: 167,500
			2017-18: 426,800


Weekly Sales Details
Net upland sales of 167,500 RB for 2016-17 were up noticeably from the previous week and from the prior 4-week average. Increases were reported for Vietnam (101,300 RB, including 900 RB switched from Taiwan, 3,800 RB switched from Japan, and decreases of 8,400 RB), India (18,800 RB, including 5,200 RB switched from Taiwan and decreases of 500 RB), Indonesia (17,100 RB, including 2,400 RB switched from Japan), and Mexico (14,900 RB). Reductions were reported for Taiwan (6,300 RB), South Korea (6,200 RB), and Japan (2,400 RB). For 2017-18, net sales of 426,800 RB were reported primarily for Vietnam (156,900 RB), China (145,200 RB), Indonesia (53,900 RB), and Thailand (21,100 RB). 


Weekly Export Details
Exports of 259,400 RB were up 11% from the previous week, but down 17% from the prior 4-week average. The primary destinations were Vietnam (58,300 RB), Turkey (58,200 RB), Indonesia (30,500 RB), China (24,200 RB), and India (21,900 RB). 


Comments and Performance Indicators
Export commitments for 2016-17 are 64% ahead of year-ago, compared to 65% ahead the previous week. USDA projects exports to be up 58.5% from the previous year at 14.500 million bales.




Beef



Actual Sales (in MT)

2017: 16,600



Weekly Sales Details
Net sales of 16,600 MT reported for 2017 were up 96% from the previous week and 60% from the prior 4-week average. Increases were reported for South Korea (3,400 MT, including decreases of 400 MT), Japan (3,100 MT, including decreases of 2,900 MT), Hong Kong (3,000 MT, including decrease of 100 MT), Mexico (2,200 MT, including decreases of 100 MT), and Canada (2,100 MT, including decreases of 100 MT). Reductions were reported for Cote D’Ivoire (100 MT) and Brazil (100 MT). 


Weekly Export Details
Exports of 13,700 MT were down 5% from the previous week, but unchanged from the prior 4-week average. The primary destinations were Japan (4,300 MT), South Korea (3,000 MT), Mexico (1,700 MT), Hong Kong (1,600 MT), and Canada (1,400 MT). 


Comments and Performance Indicators
Weekly export sales compare to 8,500 MT the week prior. USDA projects exports in 2017 to be up 10.0% from last year's total.




Pork



Actual Sales (in MT)
2017: 24,700


Weekly Sales Details
Net sales of 24,700 MT reported for 2017 were up noticeably from the previous week and up 49% from the prior 4-week average. Increases were reported for Mexico (21,500 MT), Canada (2,700 MT), Japan (2,300 MT), Taiwan (800 MT), and Colombia (500 MT). Reductions were reported for China (3,400 MT) and Honduras (300 MT). 


Weekly Export Details
Exports of 19,900 MT were down 5% from the previous week and 8% from the prior 4-week average. The destinations were primarily Mexico (7,200 MT), Japan (3,200 MT), China (1,800 MT), Canada (1,700 MT), South Korea (1,500 MT). 


Comments and Performance Indicators

Export sales compare to a total of 12,300 MT the prior week. USDA projects exports in 2017 to be 9.8% above last year's total.