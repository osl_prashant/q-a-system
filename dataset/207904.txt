The numbers show sustainability sells produce, said Andrew Mandzy, director of strategic insight with New York-based Nielsen, which tracks retail sales trends.“Many consumers are trying to be responsible citizens of the world, and they expect the same from corporations, so when it comes to purchasing, they are doing their homework,” Mandzy said.
Before they make buying decisions, shoppers check labels, glean websites for business and manufacturing practices and pay attention to public opinion on specific brands in the news or on social media, he said.
To that end, Mandzy said, 39% of consumers are willing to switch to brands that are more transparent, according to Label Insight’s 2017 Consumer Demand for Transparency Study.
Consumers will also pay a premium for sustainable practices, Mandzy said.
 
Worldwide concern
“Sustainability is a worldwide concern that continues to gain momentum — especially in countries where growing populations are putting additional stress on the environment,” he said.
Within the UPC produce category, the average price for a product marketed as sustainable is 35% more than the average product, Mandzy said.
According to Nielsen Product Insider, for the 52-week period ending Jan. 28, sustainable packaging material use grew 2.5%, or $73.4 billion; sustainable farming — organic, GMO-free, organic ingredients — grew 11.4%, or $31.2 billion in sales; and production methods — including renewable energy and sustainability-certified — increased 4.2% or $24.9 billion.
“Overall, results confirm conventional wisdom that the market for sustainable goods continues to expand,” Mandzy said.
Results vary by region, demographics and category, but it has become clear that there is opportunity for “significant brand growth” for produce suppliers “willing to listen and respond to a new kind of consumer,” Mandzy said.
 
Lack of clarity
The industry is responding, but the direction of change isn’t always clear, grower-shippers say.
“One of the most difficult things in terms of a comprehensive program continues to be the lack of clarity relative to our customers’ expectations,” said Eric Halverson, executive vice president of technology with Grand Forks, N.D.-based potato grower-shipper Black Gold Farms.
In absence of that clarity, the produce industry has invested in data collection, Halverson said.
“This gives us information but it still doesn’t really drive decision making outside of what we have always done to increase productivity,” he said. “The question is do our customers’ customers want us to make choices that don’t increase productivity and really just add cost? My sense is that it depends on the perceived value of a choice and the sustainability bang for the buck.”
Sustainability transcends one or two approaches, said Samantha Cabaluna, vice president of brand marketing & communications with Salinas, Calif.-based Tanimura & Antle.
“Sustainability really is a much broader topic, in our view. It’s a mindset that permeates how we approach our business,” she said.
That mindset embraces environmental concerns, labor, energy conservation and waste reduction, among others, Cabaluna said.
Mayra Velazquez de Leon, president of San Diego-based Organics Unlimited, agreed.
“Honor the term and implement all the aspects into your sustainability program: organic, taking care of our planet, recycle, carbon footprint, social responsibility, solar power, clean oceans, etc. There is so much we can do, but starting to make a change and implementing as we go makes a big difference,” she said.
Sustainability is a “farm-by-farm” process, said Bil Goldfield, director of corporate communications with Dole Food Co., Westlake Village, Calif.
“New practices are first tested — primarily in our own company farms — and then rolled-out to our production base of sourcing farms,” he said.