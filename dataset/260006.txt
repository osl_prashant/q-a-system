BC-Merc Early
BC-Merc Early

The Associated Press

CHICAGO




CHICAGO (AP) — Early trading on the Chicago Mercantile Exchange Tue:
Open  High  Low  Last   Chg.  CATTLE                                  40,000 lbs.; cents per lb.                Apr      121.75 121.85 121.05 121.37   —.18Jun      113.45 113.55 112.55 112.90   —.37Aug      110.95 111.05 110.00 110.35   —.47Oct      114.10 114.17 113.07 113.45   —.60Dec      117.10 117.15 116.10 116.32   —.78Feb      118.10 118.10 117.27 117.50   —.60Apr      117.75 117.75 117.37 117.37   —.78Jun      111.15 111.15 111.15 111.15   —.82Est. sales 18,239.  Mon.'s sales 85,855 Mon.'s open int 361,541                FEEDER CATTLE                        50,000 lbs.; cents per lb.                Mar      142.27 142.42 140.97 141.37   —.80Apr      142.67 142.82 141.17 141.67   —.90May      144.12 144.27 142.52 142.97  —1.13Aug      148.87 149.05 147.35 147.77  —1.30Sep      150.10 150.10 148.55 148.77  —1.43Oct      150.22 150.22 148.85 148.87  —1.45Nov      149.82 149.85 148.55 149.07  —1.00Jan      144.50 144.50 144.50 144.50   —.85Est. sales 4,809.  Mon.'s sales 13,590  Mon.'s open int 52,627                 HOGS,LEAN                                     40,000 lbs.; cents per lb.                Apr       67.67  68.20  67.42  68.17   +.50May       71.42  72.10  71.22  72.10   +.88Jun       77.20  77.97  77.12  77.90   +.85Jul       78.10  78.70  78.05  78.67   +.70Aug       78.27  78.77  78.25  78.72   +.40Oct       67.15  67.47  67.05  67.47   +.32Dec       62.55  62.70  62.20  62.55        Feb       66.57  66.75  66.22  66.57   —.23Apr       69.82  69.82  69.82  69.82   —.35Est. sales 15,662.  Mon.'s sales 60,318 Mon.'s open int 226,864,  up 3,160     PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.