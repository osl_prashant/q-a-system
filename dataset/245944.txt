When you buy a new car, you buy insurance to protect it. The same can be said about seeds. When agriculturalists discovered their irreplaceable value, they realized seeds needed to be protected. As a result, there are 20 gene banks in the U.S. alone that hold hundreds of thousands of modern, historical and wild relatives of crops.
These seeds hold the keys to future genetic changes. “Almost all the soybean disease resistance we have now originated from the seed collection and was crossed into commercial varieties,” explains Randy Nelson, a USDA–Agricultural Research Service (ARS) geneticist at the University of Illinois who manages their seed bank.

Nelson’s and the 19 other U.S. gene banks keep the varieties in temperature-controlled and data-tracked facilities. Curators manage supply, check out varieties to researchers, maintain genetic purity and develop collections by acquiring germplasm.

In the U.S., seed bank preservation is essential to the future of agriculture, particularly for soybeans.

“Soybean production as it is today would not be possible without this germplasm,” Nelson says.

Researchers have barely tapped into the genetic diversity of soybeans available through gene banks. Soybean breeders have used less than 1% of the germplasm available, Nelson adds.
In the U.S. system, there are close to 600,000 different varieties, Nelson says. His collection holds nearly 22,000 kinds of soybeans and other soybean relatives.

In Ames, Iowa, Candice Gardner and staff conserve 53,000 varieties of more than 1,400 crop species for USDA–ARS. The Ames facility, established in 1947, was the first U.S. seed gene bank. It’s like a time capsule.

“We have corn seed in our cold room that’s 50 years old and will still germinate at 85%,” Gardner says. She also notes viability and shelf life of seed varies by species.
Soybean seeds at the Illinois facility can maintain viability for more than 15 years. “To make sure we are sending good seeds we regrow them every 10 years,” Nelson says. “We grow more than 2,000 varieties a year.”

Maintaining genetic purity is critical—a challenge Nelson says is easier to do with self-pollinating crops. Crops such as corn are more complicated since they can easily cross pollinate. “In maize, everything has to be control pollinated,” Gardner says. “We hand pollinate maize, while insect-pollinated species are grown in screened cages with bees and flies.”
Those managing U.S. gene banks must maintain genetic integrity of each variety when regrowing seed stock. Genetic purity is critical because researchers rely on these varieties as they search for new seed technologies.

“Historically and still today, the biggest use of the seed collection is for disease resistance,” Nelson says. Researchers can look for specific resistant qualities to cross into varieties.

Disease resistance isn’t the only quality researchers seek; as interest in seed composition increases, so does the value. Researchers are looking for genes responsible for stress tolerance, yield, composition, nutrition and other traits that bring value.

Gene banks hold the genetic keys to agriculture’s future success. There are safety nets in place to preserve seed viability. In the U.S., gene banks have backup collections stored at the National Laboratory for Genetic Resources Preservation in Fort Collins, Colo., where some seeds can last up to 75 years. Norway’s Svalbard Global Seed Vault stores copies at -18°C and holds 825,000 crop varieties. Even if power fails, the vault can safely store seed for an additional 25 years.