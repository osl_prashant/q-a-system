SALINAS, Calif. — A dwindling supply of farm workers in the U.S. won’t necessarily bring quick fixes in automation of harvesting and other tasks, but the steady advance of science will bring more labor-saving solutions for growers in the next decade, panelists said at a farm labor workshop at the Forbes AgTech Summit.
 
Moderated by David Mancera, business advisor with Kitchen Table Advisors, the June 29 session was called “Help Wanted: how labor is shaping the farm of tomorrow.”
 
The farm labor population is aging and second-generation immigrants aren’t interested in working on farms, said panelist Javier Zamora, owner of California-based JSM Organics, which grows in California’s Central Coast. Policies of the Trump administration are keeping levels of undocumented immigrants crossing into the U.S. low, and all of those factors combine to create problems in finding farm labor, he said.
 
Panelist Dan Steere, co-founder and CEO, Abundant Robotics — a firm that is close to commercializing an automated apple picker — said that the current tight farm labor conditions are nothing new, as apple growers have wanted the ability to automate apple harvest for more than 50 years.
 
“The best engineers in the world had not yet designed an automated harvester in the 1960s, 1970s, 1980s and even in the early 2000s,” he said. “There are finally underlying technologies that let us start to address some of these technical problems,” he said. 
 
While rising farm labor rates are a reality, that doesn’t change the capability of automation technology, Steere said.
 
“Most of the problem in bringing automation to agriculture in Salinas or in the orchards of Washington state isn’t related to wages that the automation is competing with, it is related to whether or not automation can do the task at all.”
 
Steere said he believes that in the next decade there will be great advances in automation technology to handle difficult tasks, to manipulate commodities in the way growers need to replace field labor.
 
“I think you will see a significant impact over the next five to ten year time frame, In two years, not so much,” he said.
 
While some might see automation as suddenly displacing farm workers, agriculture has progressively become more efficient with labor for 200 years, Steere said.
 
Taking root
 
“I think we are in the infancy of really putting the technology like transplanters, weeders, cutting machines and putting them to work and feeling comfortable about doing commercial acreage sized work,” said panelist Brain Antle, president of PlantTape USA, Tanimura & Antle’s automated transplant company.
 
While there are machines that have automated planting, thinning and weeding lettuce, Antle said work needs to be done in automating handling and field packing of vegetables after harvest,  
 
“Nobody is really close to getting that side of it,” he said. “There is nothing to replace the skilled labor of a guy that is field packing the product,” he said.
 
While such machinery might exist in a lab, Antle said it is another thing to weld it on to a moving machine and have it work 24 hours a day under harsh conditions.