The impact of an animal disease outbreak will have serious consequences for producers, as well as local and state economies. The response needed will involve the coordination and collaboration of a number of professions, industries and agencies. Ensuring that responders understand the impact, the terminology used, and actions required will ensure a more rapid and coordinated response.
The Animal Disease Emergencies course overviews key concepts pertaining to a response to livestock and poultry disease emergency. It highlights the actions needed to detect, contain and control these diseases to better prepare responders for their tasks during the response.
When
The course will be available in February 20, 2018.
Target Audience
The Animal Disease Emergency course is an awareness level course open to anyone – professionals or students – that may be involved in an animal disease response affecting livestock or poultry.
This includes veterinary, animal health, wildlife management, emergency management, traditional responders (e.g., law enforcement, fire or public works), and public health. Livestock producers and industry associations will also find the information in this course helpful.
Course Details
This course consists of two interactive lessons:
(1) Steps in an Animal Disease Emergency, and
(2) Response Tasks
Four case studies are provided to highlight information learned in the course:
(1) Foot and mouth disease,
(2) Avian influenza,
(3) African swine fever, and
(4) New World screwworm.
Resources for additional information and training are also provided throughout the course.
The course is completely web-based. You do not need to be online at any specific time. Access the materials whenever it is convenient for you. Work at your own pace, take breaks when you need to.
Each lesson takes around 60 minutes to complete and includes a corresponding study guide and quiz.
A certificate of completion is provided upon completion of the two lesson quizzes. A score of 70% or better must be achieved on each quiz to obtain the completion certificate.
 
Course Instructor
Glenda Dvorak, DVM, MS, MPH, DACVPM
Dr. Dvorak is currently the Assistant Director for the Center for Food Security and Public Health (CFSPH). She has developed or contributed to a number of animal health emergency response projects and resources at the local, state, and national level, including the USDA Foreign Animal Disease Preparedness and Response (FAD PReP) Guidelines, and the USDA-APHIS National Veterinary Accreditation Program (NVAP) training modules. She is also a co-instructor for a public health emergency preparedness course for the University of Iowa, College of Public Health.
Dr. Dvorak received her DVM from Iowa State University completed her MPH degree from the University of Iowa, and is a Diplomate of the American College of Veterinary Preventive Medicine.
Registration/Cost
Registration is now open. The cost for the course is $100. You will have 90 days to access and complete the course.
Register Now
Veterinary Continuing Education Credit
This course provides current information and resources on emergency response measures for animal disease emergencies and has been approved by AAVSB RACEfor 4 hours of continuing education (non-interactive on-line; veterinarians or veterinary technicians).
Participants should be aware that some boards have limitations on the number of hours accepted in certain categories and/or restrictions on certain methods of delivery of continuing education. Please contact the AAVSB RACE program if you have any comments/concerns regarding the validity or relevancy of this course to the veterinary profession.
Questions?
Contact CFSPH at 515.294.7189 orade@iastate.edu .
 
Development of this course was made possible in part through funding from the Center of Excellence for Emerging and Zoonotic Animal Diseases (CEEZAD) at Kansas State University, a Department of Homeland Security Center of Excellence.