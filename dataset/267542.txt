China-US trade dispute looms over Montana beef export deal
China-US trade dispute looms over Montana beef export deal

By MATTHEW BROWNAssociated Press
The Associated Press

BILLINGS, Mont.




BILLINGS, Mont. (AP) — A brewing trade war between China and the U.S. comes at an inopportune time for Montana ranchers, who are seeking to close a multi-year deal to export up to $200 million of beef through China's largest online retailer, industry representatives said Thursday.
The export agreement between the Montana Stockgrowers Association and Chinese e-commerce giant JD.com was touted as a landmark agreement when it was announced in November, soon after the lifting of longstanding restrictions on Chinese imports of U.S. beef.
But suddenly looming over the export partnership is the fast-escalating trade dispute between the world's two largest economies.
Chinese regulators on Wednesday included beef among U.S. products targeted for a potential 25 percent increase in import duty. The move came hours after President Donald Trump's administration unveiled plans to impose tariffs on $50 billion in Chinese imports.
Terms of a contract with JD.com need to be finalized by the end of June to have cattle ready for delivery in 2018, Miles City rancher Fred Wacker said Thursday.
Final negotiations are just getting started, Wacker said, adding that he remains hopeful the trade dispute will be resolved and won't upend the exports deal.
Prospects for a quick end to the dispute dimmed Thursday. Trump said in a statement released by the White House that China had chosen to "harm our farmers and manufacturers" and that an additional $100 billion in tariffs were now being considered.
Wacker's Cross Four Ranch is expected to be the lead supplier of cattle to JD.com.
"I would sooner we not have this situation and it's not helpful to us," Wacker said. "This is a bump in the road, but there are always bumps in the road on these deals. I have high hopes we will be able to resolve it and that we will be able to move forward."
JD.com did not immediately respond to questions from The Associated Press about the export deal's status.
Since the Chinese market was reopened to U.S. beef after a 13-year absence, exports in January reached their highest monthly volume to date at 902 tons (819 metric tons) nationwide, according to the U.S. Meat Export Federation.
Federation President Dan Halstrom said in a statement that additional tariffs would "put at risk" further growth in that market just as U.S. beef sales have been gaining momentum in China.
Spurred by beef's rising popularity, particularly in urban areas, China's 1.37 billion residents were projected to eat roughly 17.6 billion pounds (8 billion kilograms) of the meat in 2017, according to the U.S. Department of Agriculture's Foreign Agricultural Service. That's up more than 40 percent in the past five years.
Also included in last November's preliminary agreement with Montana ranchers was a potential $100 million investment in a new slaughterhouse in the state to process beef destined for China.
Construction originally was anticipated to start as early as this spring, but that's not going to happen, said Montana Stockgrowers Executive Vice President Errol Rice.
The parties to the deal agreed instead that it would be best to first start supplying beef to JD.com's retail platform and develop a marketing plan for it, he said. The slaughterhouse would follow later, and in the interim Montana cattle would be shipped to Nebraska for processing before being shipped overseas.
"By the end of summer, we'll have cattle ready to go and all the logistics in place to move cattle to market," Rice said. "No doubt about it this (trade dispute) will probably be a hurdle we will have to overcome in our talks with JD.com to figure out how to navigate this."
___
Follow Matthew Brown on Twitter at @matthewbrownap