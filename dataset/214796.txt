As avocado volume from Mexico increases, Avocados From Mexico has released a short film that traces avocados from grove to consumer and has scheduled a full lineup of seasonal programs “to engage shoppers and create avocado fans,” said Maggie Bezart-Hall, vice president, trade and promotion.
AFM debuted a short film titled “Avocadoland” at the Produce Marketing Association’s Fresh Summit in New Orleans in October. Two additional episodes are scheduled to launch in late November.
AFM partnered with Tastemade, a Santa Monica, Calif.-based entertainment company, to make the film, which shows how avocados travel from where they’re grown in Michoacan all the way to the consumer, Bezart-Hall said.
“Not only can users view the video, but they can also enjoy ... an interactive video platform that AFM integrated in the video experience,” she said.
“The technology allows users to click on the hotspots or ‘tags’ that appear on the screen along the video for an immersive experience.”
It allows viewers to explore additional content and to share through their social networks, she added.
The film can be viewed at theavocadoland.com.
On the promotions side, AFM’s Season’s Eatings program, which runs from Nov. 10 through Dec. 31, is designed to bring families together this season and inspire them to create their own avocado toast recipes for a healthful, delicious breakfast, Bezart-Hall said.
Next, Guac Nation, which will focus on “the Big Game-watching party occasion,” challenges shoppers to create their own unique guacamole recipes.
It runs from Jan. 3 until Feb. 4. Deadline to order display materials is Nov. 14.
Then comes Fanwich, which will encourage fans to “up their sandwich game” for the collegiate basketball championships. Deadline for ordering materials is Jan. 2.
Finally, Cinco Delicioso is aimed at “bringing flavor to the fiesta” with Avocados From Mexico and Tabasco brand pepper sauce, she said. Deadline to order materials is Feb. 7.
Display materials for all promotions may be ordered at AvocadosFromMexico.com/trade.
“Avocados From Mexico shopper engagement initiatives attract consumers, create opportunities to discover, engage and inspire consumers to act, further growing the category and driving traffic to the produce department,” Bezart-Hall said.
“It is critical for us to understand our consumers and become a part of their lives, particularly during key seasonal occasions,” she said.