David Nour, CEO of The Nour Group, talked about the advantages of influencer marketing at a Sept. 19 workshop at Fresh Summit.

NEW ORLEANS — Successful influencer marketing can deepen loyalty, bring new business opportunities and reduce transaction costs for produce marketers, David Nour told a Produce Marketing Association Fresh Summit workshop on Oct. 19.
Working with bloggers and other influencers can pay returns, said Nour, CEO of the Nour Group, during an 80-minute session called “How Influencer Marketing Can Help Grow Your Business.”
“Influencer marketing is fundamentally about relationships, relationships you build with those influencers and the relationships they have their networks and populations,” he said.
An influencer is a person who expresses a relevant message that is meaningful enough to elicit action from others, he said.
Marketers must have a specific goal when they pursue influence marketers, he said. “You can’t be everything to everybody, you have to fix something to focus on,” he said.
More than standard advertising, peer recommendations carry a lot of weight, he said. 
Nour said 92% of consumers trust recommendations from other people, even from those they don’t know. Seven in ten consumers report online customer reviews are a trusted source of insight; 47% consult blogs for new trends and ideas, he said.
Dour said Google considers social media mentions when it decides how to rate companies in search results. Finally, he said dollars spent on influencer marketing bring a $6.50 return on investment for every dollar invested.
Possible uses of influencer marketing could be during a product launch, to create brand awareness, generate leads or during crisis management, he said. 
At the same time, influencer marketing must be authentic, he said. Any compensation received by influence marketers from brands must be disclosed, according to the Federal Trade Commission.
The FTC’s Endorsement Guides provide that if there is a “material connection” between an endorser and an advertiser – in other words, a connection that might affect the weight or credibility that consumers give the endorsement – that connection should be clearly and conspicuously disclosed, unless it is already clear from the context of the communication.
Consumers also may get frustrated when website content is not personalized, and will give only three to five clicks or a total of ten seconds before they leave a website.
Nour said authentic story telling resonates with today’s consumers, with real people relating real experiences. Don’t try to create a perfect campaign, but put it into action and get feedback, he said.
He said social media trends for this year include the rising use of augmented reality, continued investment in influencer marketing, a focus on Generation Z consumers, expansion of live streaming and increased brand participation on social media messaging platforms.