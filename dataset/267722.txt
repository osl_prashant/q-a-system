BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Friday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2498  Down  41May         2508    2516    2440    2453  Down  55Jul                                 2519  Down  38Jul         2542    2551    2494    2498  Down  41Sep         2565    2570    2515    2519  Down  38Dec         2567    2573    2522    2525  Down  37Mar         2547    2555    2514    2517  Down  37May         2546    2546    2520    2522  Down  36Jul         2550    2550    2527    2529  Down  36Sep         2555    2555    2536    2536  Down  36Dec         2541    2545    2541    2542  Down  36Mar                                 2560  Down  40