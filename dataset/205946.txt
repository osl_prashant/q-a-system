Don Wirth is searching for slimy vampires that steal the lifeblood from his fields. With Mt. Hood and the majestic Cascades over his shoulder, Wirth lifts a roof shingle and checks the improvised trap to reveal three inch-long creatures sheltering from an early afternoon sun. Hiding by day and feeding by night, the sleeping assassins are the most deceptive of crop killers: slugs.
Slugs are an accepted part of the bill for many agriculture operations, but as numbers rise, particularly in the Midwest, producers are reckoning with a new level of damage. A mild slug presence, sheltered by increasing no till acreage, can usher in a wave of replants, major yield loss and expensive bait control. The precise financial cost of slugs to U.S. agriculture is a great unknown, but it certainly reaches over several hundred million dollars each year, and more significantly, is consistently creeping upward.
Across 3,000 acres on the floor of northwest Oregon’s gravid Willamette Valley, Wirth grows a dizzying variety of crops for his Saddlebutte Ag seed company: annual ryegrass, tall fescue, peas, mustard seed, kale, clover, chicory, plantain, forage cabbage and many more. He can drive by a crop field at 40 mph and spot the whitish cast of a slug infestation. “Corn, soybeans or grass, I don’t think there is a crop slugs can’t hurt. I’ve seen an entire 160-acre field of perennial ryegrass get killed,” he says. “The whole block faded to brown as the slugs moved across.”
Wirth, 71, has spent up to $60,000 per year on slug control and attributes an increased gray slug presence to multiple factors, one of which traces back to a benchmark 1988 field burn. On an Aug. 3 afternoon, post-harvest burning cast a dark cloud of smoke across I-5 outside of Eugene, essentially creating a blind tunnel for incoming drivers. The thick smoke led to a chain reaction of fiery collisions resulting in seven deaths (multiple children) and 37 injuries. The gruesome 21-vehicle pileup became the catalyst for a near-total burn ban on Oregon farmland.
Many farmers attribute a rise in slug populations to the burn ban, but Wirth believes the absence of fire to control pest numbers is only a partial explanation. “I don’t know the whole answer to bigger slug numbers, but I think cover crops and no till are creating a protection blanket of sorts. I think that’s also what’s going on in the Midwest,” he says.
Scouting Necessity
Slugs take a $60 million bite out of the Oregon seed grass industry each year and Wirth says vigilance is an absolute necessity. “If you sit pat and don’t scout, you’ll soon have a mega problem,” he warns. “This is a prehistoric creature that knows how to survive. Put a slug in the freezer for a few weeks and when you take him out, don’t be surprised if he crawls off.”
Slugs become active at dusk, moving to feed or mate through the night. With a typical lifespan of one year, slugs lay eggs in autumn and spring, and offspring are ready to mate in six months. Slugs are hermaphrodites and when necessary, can self-fertilize and don’t need a mating partner.




 


“I don’t know the whole answer to bigger slug numbers, but I think cover crops and no till are creating a protection blanket of sorts,” says Oregon producer Don Wirth.


© Saddlebutte Ag







Two molluscicides are labeled for slugs in Oregon, metaldehyde and iron-based pellets. However, Wirth hasn’t had great success with either treatment. “If we kill 15% of the slugs, that’s a success, even though we’re really only killing the big ones, and nothing ever seems to work on the eggs,” he explains.
In the wild, slugs feed on preferred plants, but in cropping situations slugs aren’t mobile enough to pick and choose. Far beyond feeding damage, slugs also vector plant viruses and disease. (In addition, slug slime and feces reduce market value of some crops.)
Biological Controls
Rory McDonnell, a renowned slug and snail expert with Oregon State University, says slugs are notoriously difficult to manage: “It’s either metaldehyde, iron phosphate or chelated iron pellets. With the advent of no till and conservation tillage, tillage is out for many producers.”
McDonnell is uncovering novel biological controls. European farmers have had success combating slugs with a lethal nematode worm yet to gain U.S. approval. McDonnell is currently researching a U.S. strain of the nematode to make sure it doesn’t kill off-target organisms. In addition, he’s looking at plant extracts (caffeine and essential oils) for use in slug control, and new attractants to place in killing zones for more targeted use of pesticides.
Slug presence has gotten progressively worse in the Willamette Valley over the past decade as the pests thrive in fields with increased surface residue, according to McDonnell: “It’s difficult to interpret, but I think we’re dealing with a combination of causes. No burning, no tillage and increased drainage from wet fields have all enhanced slug habitat.”
No till residue build-up is beneficial to slugs. It keeps the ground relatively moist for the slimy creatures, provides cover during daylight hours and offers excellent habitat for egg-laying.
“It’s critical for growers to monitor populations,” McDonnell adds. “A couple of slugs under a piece of wood are no problem, but when numbers rise, you better put out bait. Walk around after dark with a flashlight and you’ll get a good idea if you need to bait.”
Increases in Midwest
Roughly 2,400 miles away from the Willamette Valley, Kelley Tilmon anticipates increased farmer concerns over slug presence in the Midwest. Tilmon, an entomologist with Ohio State University Extension, sometimes sees 20% losses in corn and soybean infestations, but has noted plenty of occasions when replanting was necessary, particularly when slugs feed on seeds along the seed trench
“I expect the trend for no till to increase and therefore slug populations to increase,” Tilmon explains. “Growers should consistently scout. A fall population means a spring presence.”
She recommends simple trapping stations in fields: Roof shingles spray painted white ensure a relatively cool temperature and attract slugs to the underside. “On crops, check for holes in leaves. Slugs feed in long, narrow strips in corn, and on the edges of leaves in soybeans,” Tilmon describes. “There are no official thresholds to rely on, but keep an eye on defoliation and slug numbers to roughly measure problem levels.”
Kentucky Damage
Unprecedented slug damage hit Kentucky particularly hard in 2017, and caused heavy replanting in some counties, according to Raul Villanueva, an entomologist with the University of Kentucky Extension. At least 30,000 acres were replanted in Kentucky due to slug damage, and Ohio County producers were forced to replant 70% of total soybean acreage, according to Villanueva. He recorded up to 90% total soybean damage at fields spread across six counties. Villanueva attributes part of the slug population jump to a warm winter in 2016 and the lack of a natural cold-weather kill.
“Farmers didn’t realize what was happening until it was too late in some cases. Certainly, most of the damage was in no till fields and also in fields previously planted with corn. Slugs definitely have more opportunity to hide in no till conditions,” he says.




 


Feeding damage from slugs is clearly visible on soybean seedlings.


© Raul Villanueva







Daniel Fleishman, BASF innovation specialist, says slugs are a constant presence on Kentucky farmland, but never in the high numbers seen in 2017. He calls 2017 the “perfect storm” for slugs due to a variety of circumstances: plentiful rain in the summer of 2016, a mild winter, early warm-up in February, and a wet spring. “We had guys who had to replant twice due to slugs,” Fleishman says. “You could scrape residue with a knife and see four or five quarter-inch slugs.”
Slugs may force Kentucky growers to change cover crop management, according to Fleishman. “We burn down our covers and plant right in, but maybe guys will have to burn early and give plant matter a chance to decay more before planting in,” he says.
If harvest is followed by a wet fall and mild winter, Fleishman expects Kentucky fields to play host to unprecedented slug numbers: “Slugs are a completely different problem than anything we’ve had to deal with before and we’ve got a lot to learn.”