I open the fridge for a late night snack. I see some leftover pizza, Greek yogurt and there’s the clamshell of strawberries. 
What will motivate me to make my choice? Is it disease prevention?
 
Of course it’s not. No one thinks that way.
 
I’ve been concerned about the produce industry’s inability to raise consumption over the past few years. Don’t people know fruits and vegetables are good for them?
 
And then I think about my own consumption. I know they are good for me and I don’t always choose them. I especially don’t choose to eat fruits and vegetables when people tell me I should because they’re healthy for me.
 
No one likes a nag.
 
I eat them when I know they will taste good, or when I know I’ve had less than half my diet for the day in fruits and vegetables, or when I feel a little sluggish and soft and know eating junk will make me more so.
 
Let’s look at these motivations: pleasure, guilt, ego.
 
Disease prevention? Please.
 
At the Produce for Better Health Foundation annual meeting in Scottsdale, Ariz., April 5-7, it was a time for re-evaluating our methods for motivation.
 
Longtime CEO Elizabeth Pivonka left the group earlier this year, and Scottsdale was the first event for new CEO Wendy Reinhardt Kapsak to meet the industry.
 
She and I talked about her first six weeks and her big picture priorities. 
 
Kapsak said her biggest one has to be raising consumption, and one way to do that is to communicate a better definition of health.
 
“Disease prevention is a flat idea,” she said. “Health is a lifestyle.”
 
It’s also defined many different ways, and fruit and vegetable consumption fits into most of them.
 
Another thing she said to all attendees at her April 6 state of the industry address was, “We have to meet consumers where they are.”
 
This is important in two different ways:
 
We have to meet consumers’ motivations where they are. It’s not enough to market fruits and vegetables based on what consumers should do. As consumers, we’re not rational about many other purchases. Why would we be with salads and apples?
 
Marketers need to get more basic with consumers, back to the WIFFM: What’s in it for me?
 
In my example above from my own fridge, I wanted something that tasted good, that resolved my guilt from straying from a healthy diet, and that won’t make me look and feel soft and fat. 
 
For a single 25-year-old, she may want to look healthy and fit, feel good about herself by buying organic, and not have time to prepare a meal, so she buys fresh-cut.
 
For a 60-year-old, he needs to balance eating a cheeseburger by eating a salad, eat a baked potato with his steak because it tastes good, and eat local in-season berries because he takes pride in his hometown.
 
Fresh Trends
Everyone has different motivations, and it’s safe to assume they’re more self-centered than altruistic. So what?
 
We have to meet consumers where they buy food. In our just-released Fresh Trends 2017 consumer survey, we asked them what type of store they primarily shop for fresh produce. 
 
The No. 1 answer was regional supermarket (Safeway, Kroger, etc.) at 39%, followed by chain superstore (e.g., Wal-Mart) at 23%, farmers market at 13%, specialty market (e.g., Whole Foods, Trader Joe’s) at 10%, chain discounter (e.g., Aldi) at 8%, warehouse store (e.g., Costco, Sam’s Club) at 5%, and other at 2%.
 
Farmers markets get the love in blogs and New York and San Francisco media, but 85% of consumers in our survey buy fresh produce most often in supermarkets and other mainstream food stores.
 
This is just retail, but fresh produce marketers need to motivate consumers in these environments, and considering how local, value-added and organic continue to grow, we’re hitting many of those motivating factors, even as consumption remains flat.
 
Everyone’s motivation may be different, but it’s also similar in that it’s usually all about them.
 
Greg Johnson is The Packer’s editor. E-mail him at gjohnson@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.