BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Friday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMar                                 2313  Up    63Mar         2283    2320    2272    2320  Up    50May                                 2336  Up    61May         2280    2340    2251    2313  Up    63Jul         2301    2363    2274    2336  Up    61Sep         2317    2379    2290    2352  Up    62Dec         2326    2379    2301    2362  Up    61Mar         2337    2382    2316    2369  Up    60May         2329    2377    2328    2377  Up    58Jul         2361    2389    2361    2388  Up    58Sep         2373    2401    2373    2401  Up    59Dec         2391    2416    2389    2416  Up    59