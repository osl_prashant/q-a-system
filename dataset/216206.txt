A mild fall has been a blessing for many ranchers. As the first day of winter looms around the corner, a ranch manager must try to adhere to some rangeland management principals when managing pastures this winter.
Soil Surface Protection
Due to much of Central and Western South Dakota experiencing drought conditions this past summer, the opportunity for winter grazing may be very limited or not available at all. A ranch manager must try to ensure that enough residual plant height and vegetation cover of the soil surface is available through the winter to aid in recovery of the rangeland. In order to capture as much snowfall as possible and protect the soil surface from exposure, a rancher should strive for at least 50 to 60% organic material cover on the soil surface (Figure 1, Figure 2) and at least 4-6 inch residual stubble height for native grasses (Figure 3). This goal may be difficult to obtain as some of the hardest hit drought areas in Western South Dakota had 4-6 inches of total plant growth for the year.




Fig. 1. Dormant pasture in Tripp County with adequate residual cover (11/29/17).

Fig. 2. Dormant pasture in Tripp County with adequate residual cover (11/29/17).




Fig. 3. Dormant pasture in Tripp County with adequate residual plant height (11/29/17).
 
Grazing & Stocking Rates
In Central and South-Central South Dakota, August rainfall did bring relief to hard hit pastures from drought conditions during the spring and summer (Figure 4). However, the “green up” of grasses (Figure 5) in late summer and early fall must be evaluated carefully and grazing closely monitored. When the growing season is quickly coming to an end, rangeland plants are storing their energy reserves and overgrazing will disrupt this process to an already drought-stressed plant (Smart, 2017.) Proper stocking rates when grazing the fall green-up will help ensure adequate plant height for snow capture and cover on the soil surface.
Ranches that have adequate grass stockpiled and available for winter grazing, the “take half leave half” rule of thumb still holds true. Again, leaving sufficient cover and residual plant height is essential to avoid the damaging effect of spring rains on unprotected soil from overgrazing during the winter (Gates 2015).

Fig. 4. Drought stressed pasture in Tripp County (7/27/17).
 

Fig. 5. Late summer “green-up” pasture in Tripp County (8/22/17).
 
Hay Selection
Feeding hay on native rangeland this winter is another aspect of range management that a rancher must carefully monitor. Due to the drought conditions this summer, many ranchers may have to locate hay from other areas of South Dakota or out of state. A rancher must be very careful as to the type of plant species in the hay. Especially if feeding hay with bale processors or rolling bales out in the pasture. If the hay is comprised of invasive grass species, feeding this hay may inadvertently deposit a seed bank of undesirable invasive grasses that can negatively affect pastures comprised of native grass species. For example, feeding smooth bromegrass bales on pasture comprised of drought stressed native grasses can be detrimental to the native plant community and possibly lead to smooth bromegrass taking over the pasture in the following years. However, if a rancher would like to get more native plant diversity in pastures dominated by introduced grass species, feeding native grass hay bales will help deposit that native seed bank back into those pastures.
The Bottom Line
A ranch manager must remember the importance of managing the rangeland resources this winter after experiencing the drought conditions of last summer. This will ensure the quickest rangeland recovery possible for next year’s grazing season.

References:

Gates, R. 2015. Rotational Grazing During Winter. SDSU Extension, Brookings.
Smart, S. 2017. Unpublished. SDSU Extension, Brookings.