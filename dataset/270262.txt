Excerpts from recent South Dakota editorials
Excerpts from recent South Dakota editorials

By The Associated Press
The Associated Press



American News, Aberdeen, April 19
County needs to fix Gmail problems ASAP
For months, Brown County officials and employees have not been receiving much of the email sent to them from Google accounts.
If your email is "somethingsomething2018@gmail.com," for instance, and you have tried to reach a county official this year, she or he probably hasn't seen that message.
That's a problem.
County officials and the information technology chief must make this a priority.
The issue was first discussed at a county commission meeting two weeks ago, where it was learned that the problem had been ongoing for months — and continues today.
Brown County Chief Information Officer Paul Sivertsen is stumped.
"I've had the state look at this, and we're set up the best we possibly can be and it's all a Google issue," he said.
Could be. But we have a feeling that Google knows how to do email.
Various studies indicate that 60 percent of email users have Google Gmail as their main service. And only about 15 percent of Americans don't have any internet service.
Thus, most of us not only use internet services such as email, but most use Gmail.
Much more likely is that the problem is on the county's side.
County officials have discussed bringing in someone who could help solve the problem.
Commissioner Nancy Hansen asked Sivertsen if he knew how much it would cost to hire somebody who could work on the issue. Sivertsen didn't estimate the cost, but said it would have to be someone highly dedicated to sifting through bits of data to pinpoint the exact cause.
That seems like his job.
Sivertsen is the county's highest-paid employee, and we take no issue with that. We'd all love to be the best-paid person where we work.
But this is his problem to deal with, and he should have a better grasp on what's wrong. Or, at the least, should have sought a solution sooner.
With that higher pay comes a higher level of responsibility.
What if the county's telephones were not working for months?
That would be a problem many county residents would be upset about. In fact, it would be likened to an emergency, a disruption of services and the ability to effectively do business for county government.
We think most would be looking for an emergency work-around in hours or days. Not months.
Not getting Gmails? It's the same thing.
To their credit, county commissioners are accessible. And their phone numbers are listed on the county's website at brown.sd.us/commission/home. But sending an email in this day and age should be simple.
The hindered communication has already caused serious issues for many county departments. As one county official said of not getting Gmail, "We look stupid."
As a test, American News reporter Shannon Marvel sent emails through Gmail last week to six county department heads. Only one department head replied back. That means the other five didn't get the message (or didn't check their email or respond — different problems).
Gmails sent from the "contact us" webpages for various county departments do get though, workers say. But it really shouldn't be that difficult.
That the problem has festered for months and has aggravated people and groups the county works and does business with is what's most disappointing.
But the problem is significant enough that it needs attention now.
County business is being hindered. Our business.
This is a big deal.
___
Rapid City Journal, Rapid City, April 19
Arena forum needs to be civil, fair
It could be great and will certainly be interesting. In one corner will be Rapid City Mayor Steve Allender and city officials; in the other, four former mayors. The topic: the proposed new Rushmore Plaza Civic Center arena.
The key question, however, is will this be the objective fact-gathering session that organizers promise or an opportunity for opponents to criticize the plan in a public forum likely to garner considerable media attention?
The two-hour event on May 1 is being organized by South Dakota Citizens for Liberty, the same group that circulated petitions to put the issue on the ballot after the city council approved plans to build a new civic center arena to replace the 40-year-old Barnett Arena.
Tonchi Weaver is spearheading the forum as was the case with the petition drive. In a story that appeared in Wednesday's Journal, she said the goal is to present "a balanced picture for the public." In an interview Wednesday morning, she said, "I want it to be fair."
It also is fair to say, however, that Weaver and Citizens for Liberty are at least perceived as opponents of a new 12,000- to 13,000-seat arena that according to the city will cost $130 million with another $50 million in financing costs.
In March, Weaver and others distributed information at the Black Hills Home Builders Association Home Show that claimed the city was misleading the public and the actual cost of a new arena was closer to $250 million.
Allender said at the time this was a misinformation campaign to encourage residents to sign petitions. "They're handing out inaccurate propaganda that shows that they want to kill the project," he told the Journal.
When pressed to name who did the analysis on the arena's costs, Weaver declined to disclose that person's identity. Allender, meanwhile, has done more than 40 public presentations on the project and the city has posted a power-point presentation on its website about it.
Weaver said she has attended three of the mayor's presentations and compares them to a grand jury proceeding where only prosecutors lay out their case. The forum, she said, will better serve the public.
Brian Fisher, formerly of KIMM Radio and described as "fair-minded" by Weaver, will moderate the forum. The format calls for opening and closing statements and allows members of the public to ask questions that are to last no longer than 90 seconds, a time limit Weaver said will be enforced.
The first question, Weaver said, will be posed by Mike Mueller, the president of Citizens for Liberty. It is a question that will likely set the tone for the forum.
Mayor Allender deserves credit for accepting the invitation to participate as there is little doubt that a number of arena opponents will show up at the forum.
It will be up to Citizens for Liberty, the moderator and the four ex-mayors — Alan Hanks, Jim Shaw, Ed McLaughlin and Keith Carlyle — to hold a forum that is fair, even-handed and most of all civil.
The city's plan to build a new arena has become an emotional issue in the community with some opponents going so far has to launch personal attacks on the mayor and suggest this is a legacy project for him.
That, of course, is not the case. Many people in the community believe it is essential for the city build a modern arena rather than spend $25-30 million to upgrade an old one. The mayor is simply fulfilling his duties by presenting a plan that will not raise anyone's taxes nor divert money from other funds to build it.
If the forum meets the goals outlined by Weaver, it can be a fair fight. If it turns into an assault on the mayor's proposal and character, it becomes a cage fight and no one wins if that's the case.
___
Capital Journal, Pierre, April 3
The wind is blowing, let's try not to get swept away
Wind power is a growing enterprise around the world and by many accounts, this "gold rush" might be coming soon to a county near you.
Actually, we know there's at least one company working to establish a large wind farm on the east end of Hughes County. The promises are great. There will be hundreds of thousands of dollars of taxable property added to the county tax base. Farmers and ranchers will get many thousands of dollars' worth of stable lease payments all for the low, low cost of signing an easement for the use of an acre or two of land for up to 30 years. Tax dollars and cash payments, we're told, will flow like milk and honey.
That's a bit of hyperbole but, generally, the folks behind wind power projects do tend to promise quite a lot. They are, after all, hoping to cash in on what is essentially an unlimited resource. There's a lot, we're talking billions of dollars on the line here. As more states move to require that more and more of the electricity their citizens use come from "renewable sources," there will be even more money on the line. That, of course, assumes there isn't some other energy revolution sometime in the next few decades or that nuclear power doesn't undergo a renaissance.
In the short term, the giant multinational corporations who are the only folks with the means to spend hundreds of millions of dollars on building the hundreds of turbines it takes to approximate the electrical-generating capacity of a single coal or gas power plant, are trying desperately to get as many projects going as possible before a tax credit expires in 2019. The credit gives wind-power producers a break on the profits they make from selling the electricity they generate. Many more millions of dollars are on the line in the case of this Renewable Electricity Production Tax Credit.
The rush to get wind-power projects into construction means South Dakota, which has heretofore found itself lagging behind other wind-rich states, has become the focus of so many new wind projects. There are 25 proposed and two pending wind farms in the state. Each one of those projects covers hundreds, if not thousands of acres. Their size and expense should cause all of us a bit of concern.
For one thing, South Dakota doesn't have very good rules on who is responsible for tearing down turbines that are no longer used. A developer is required to submit a plan to the state Public Utilities Commission but there is no decommissioning bond requirement. Instead, the commission can choose to require a bond on a case-by-case basis.
A single turbine, some estimates suggest, can cost more than $25,000 to decommission, when you subtract the salvage value of its pieces and parts from the total cost. Using this estimate on the project proposed in eastern Hughes county, the cost to decommission the project would range between $3.7 million and $5 million. That's a lot of money.
What, if any, is the state's or a landowner's legal recourse if, in 10, 20 or 30 years, a wind-power company is forced to shut down and can't sell its turbines or afford to take them down? Has anyone asked? We live in an unpredictable world, so this is not an unfathomable circumstance. We've seen taxpayers on the hook for cleaning up old mining operations whose former owners went bankrupt and couldn't fix the damage they'd done.
On the topic of natural resources, states to the east such as Minnesota and Iowa who dived whole hog into wind farms didn't have much in the way of native prairie left when the wind turbines came. Pretty much every inch of arable land there had already fallen to the plow. There wasn't much to be concerned about when it came to disturbing grassland habitats.
In South Dakota, we run the risk of impacting many already struggling species, mostly birds, who require large, mostly undisturbed grasslands to survive. Greater prairie chickens are just one species who might be impacted and they are already declining in most of their current range. There are many song birds, too, that could be affected by the 500-foot turbine towers.
While the impact on birds is bad, what could and likely would be worse is what happens if and when wind farms help drive a species toward the federal endangered-species list. When that happens, everyone, whether they agreed to host a wind turbine or not, will face the consequences. This scenario is not as unlikely as it first may seem.
Oklahoma, Texas and western Kansas, thanks in no small part to the rapid expansion of wind farms, now are facing the prospect of the lesser prairie chicken being placed on the endangered-species list. An Invasive tree species as well as oil-and-gas development played a role in the lesser prairie chicken's plight but unless something changes, ranchers, wind developers and oilmen all will find themselves hamstrung by the endangered-species act within a few years.
We also can look to the debacle surrounding sage grouse, which require massive tracts of undisturbed sagebrush habitats. Energy development, including wind power, has played a large role in that species' decline. It took the threat of the endangered-species act for states and industry leaders to get on board with conservation efforts.
In both the case of the lesser prairie chickens and of sage grouse, no one stopped to think about when and where to develop until the endangered-species act was invoked. It is almost always advisable to avoid trouble rather than to rush headlong into it. Right now, it feels like we're rushing headlong into it.
The problem of harming a grassland bird or having to decommission a wind farm may feel far off, but if we want to avoid potentially serious problems down the line, we've got to answer these questions now. We need to take a measured approach on wind development and as a state, we should look for ways to hold developers accountable for their actions when the time comes.