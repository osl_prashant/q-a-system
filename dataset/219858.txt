“Arby’s: We have the meats.”
Maybe you’ve heard the slogan from the fast food chain that was previously only known for roast beef sandwiches and a cowboy hat logo. Arby’s has grown sales volume by 25% in the past five years, largely because of rebranding efforts that focused on innovation and bringing additional menu items into the restaurant.
Arby’s growth is the largest nationally in the quick service restaurant division, says Jim Taylor, Arby's chief marketing officer. Taylor spoke during the Cattlemen’s College at the annual Cattle Industry Convention in Phoenix sharing insights on Arby’s marketing success.
Beef is still a focus for Arby’s with 130 million lb. sold per year and the restaurant chain has more than 3,300 locations globally.
Taylor says Arby’s current growth came from tapping into what the company was from the start.
“A mistake marketers make is trying to be something they are not,” Taylor adds.
When Arby’s was founded by brothers Forrest and Leroy Raffel in 1964,  the cowboy hat logo was originally made to go with their original name “Big Tex.” They instead opted to go with a phonetic spelling of the initials of Raffle brothers, “RB’s.”
Taylor says Arby’s was the first fast casual restaurant selling a $0.69 roast beef sandwich that took longer to prepare than other fast food counterparts who sold $0.15 burgers at the time.
“Arby’s was focused on the quality of the food. The abundance of meat in the sandwich, how delicious it was and making a connection with people,” Taylor says.
The original restaurants didn’t have a drive through either. The company was sold several times following its first sale by the founders in 1976. In 2011, Arby’s was purchased by current owners Roark Capital Group., which would help reenergize the business.
For the majority of the more than 50 years in business Arby’s has really only sold roast beef. Consumers only actively search out roast beef about 4% of the time when dine at a quick service restaurant.
“In the end what built the Arby’s brand were big, meaty sandwiches,” Taylor says. “The core of the sandwich and the most important ingredient is the meat.”

Pork, chicken, turkey and fish sandwiches have also been added to the menu over the past few years. Arby’s wanted to be known for more than roast beef, but has still wanted kept beef as a focal point. There are currently four different types of beef that can be enjoyed at Arby’s including: roast beef, corned beef, brisket and USDA Choice top round Angus steak.
“We want people to have fun with food and explore. We want to be the place where you break your routine. Everybody loves burgers, but you get into a routine,” Taylor says.
Arby’s wants to be the place that offers surprising menu items that consumers will eat. Items they’d typically associate with pub food like sliders are now a mainstay at Arby’s. One surprising menu item has been venison sandwiches sold during hunting season in the fall.
“We are focused on delivering superior food. We want to ignite your senses in a way that makes it more craveable, more delicious than the burger guys,” Taylor says. “We also want to have superior service.”
In the last three years Arby’s has spent nearly one million people hours in training employees to provide great service to guests, while making the experience enjoyable for employees, too.
In the edgy “We have the meats.” marketing campaign, Arby’s has spent more than $100 million in advertising and poked fun at people who probably won’t try a roast beef sandwich anytime soon.
“Arby’s has taken a little bit of heat for making fun of vegetarians,” Taylor jokes. “I’ve got to tell you I think we have right to be offended because they eat our food’s food.”

The restaurant chain has nearly a half billion visits per year, so it appears the strategy isn’t hurting.
“We call our customers, ‘Proud American Carnivores,’” Taylor says. He adds American consumers’ appetite for beef has never been bigger.
Taylor is thankful to U.S. beef producers for raising quality cattle that his company can market to consumers across the country.
“I can promise you we are going to continue to push the envelope on how Americans can enjoy beef in new ways,” he says.