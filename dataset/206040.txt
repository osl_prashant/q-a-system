It’s music to a cowboy’s ears when a meat company CEO says, “Meat demand is growing rapidly around the world. We want the world to keep eating what it loves.” 
Those are the words of Uma Valeti, co-founder and CEO of Memphis Meats, a San Leandro, Calif.-based startup developing technology to grow meat from self-reproducing animal cells. That’s right, lab meat—the futuristic product that combines all of your calf-pulling, hay baling, fence-fixing labor into a stainless-steel cultivator dish. 
After touting the robust demand for meat, Valeti says, “However, the way conventional meat is produced today creates challenges for the environment, animal welfare and human health.”
 
You can disagree with Valeti if you wish, but don’t dare dismiss his efforts to bring “real meat—without the animal—to the table.” Memphis Meats says it plans to have products in grocery stores by 2021, and the business has attracted some high-profile investors, such as Microsoft co-founder Bill Gates, English entrepreneur Richard Branson and Cargill. Yes, that Cargill.
Cargill and Tyson (which owns a 5% stake in Beyond Meat, a plant-based meat alternative) recognize that technology makes it possible for consumers to have more protein choices. “For people who want a product from an animal welfare perspective, we want (Memphis Meats) to be there for them,” Sonya Roberts, president of Cargill Proteins, told The Wall Street Journal.
With this issue, Drovers marks the 25th anniversary of the release of the 1991 National Beef Quality Audit, the Beef Checkoff-funded report widely credited with launching a revolution that pulled beef back on the rails and stopped the erosion in beef demand. Quality, however, remains an elusive and changing target. 
Noted in the articles in this issue, America’s beef producers changed production and management systems over the past quarter-century that greatly reduced carcass non-conformities, increased quality grades and improved consumer satisfaction. 
Today’s consumers, however, demand more.
Multiple surveys of consumers underscore their increasing desire to know more about their food—where it comes from, how it was raised and the care given to the animals. Both the 2011 and the 2016 NBQAs list “how and where cattle were raised” in the top five challenges for the beef industry.
With access to information literally at their fingertips, consumer desires to know more about their food will only increase. And we can be sure that startups growing meat in a laboratory or grinding beans and nuts into a patty will continue to claim superiority in environmental and animal welfare over traditional livestock production.
Beef’s quality bar has been raised. Consumers are demanding to know—from every operation—what animals are fed, how they are treated and their impact on the environment. Are you ready to add transparency to your operation?  

Read other stories from the Beef's Quality Revolution series: 

A Generation of Quality Gains
25 Years of Beef's Quality Challenges
Meat, Millennials, Meal Kits
Consumers Shift Attention to Cultural Concerns
Adapt Facilities, Equipment to Your Cattle
Resources to Improve Your Operation