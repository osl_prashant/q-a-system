Oviedo, Fla.-based Duda Farm Fresh Foods is handling juice oranges, grapefruit, mandarins and tangerines.
The company has a Florida citrus program and an import citrus program.
Hurricane Irma affected the crops dramatically, reducing volume 29% for oranges, 65% for red grapefruit and 80% for tangerines and mandarins, according to estimates from John Holford, commodity manager for Florida citrus. Duda usually has grapefruit through March but this year made its last shipments in early January.
The company planned to begin packing oranges and tangerines the week of Jan. 15.
Duda has seen a strong market for the fruit.
 
Limoneira
Santa Paula, Calif.-based Limoneira has seen some scarring on lemons because of high winds, but demand has been strong.
The company continues to invest in growing its supply of the fruit.
“We’ve increased lemon planting over the last five years by more than 75% and are aggressively looking to do the same here in the U.S. and internationally,” said director of marketing John Chamberlain.
The company expects to do more influencer marketing this season.
“We will expand our global outreach by adding new opinion leaders in a variety of geographic regions in recipes, nutrition, beauty, lifestyle and green cleaning,” Chamberlain said.
Along those lines, Limoneira has created a series promoting lemons.
“We’ve introduced Tasti Tuesday videos, which are short-form videos by Megan Roosevelt that describe how to create delicious, easy and creative recipes,” Chamberlain said. “They’ve been very successful.”
Limoneira will also be doing Pair-It-Up Promotions with retail customers.
“Each of our lemon varieties has individual flavor profiles that bring out the best in certain foods and recipes,” Chamberlain said.
“In-store display units and merchandising will be in place to easily educate consumers and will have peel-off recipes and grocery lists. We have seen in test markets that this has helped grocery retailers increase sales of complimentary pairing items.”
 
Lone Star 
Mission, Texas-based Lone Star Citrus Growers has been shipping oranges and grapefruit.
Volumes have been good, but the “trucking apocalypse” has made first weeks of the year stressful, said vice president of sales Trent Bishop.
Demand has been strong, which is normal for this time of the season since grapefruit is often on shopping lists for people aiming to eat healthier in the new year.
Lone Star Citrus Growers expects to be doing more export business this season, replacing some of the volume normally supplied by Florida, which lost a significant amount of fruit because of Hurricane Irma.
 
Seald Sweet
Vero Beach, Fla.-based Seald Sweet International has been packing grapefruit, juice oranges, honey tangerines, tangos and royal mandarins.
Like others in the state, the company felt the wrath of Hurricane Irma.
“Quality has been a struggle this year,” said Florida citrus commodity manager GT Parris. “Trees were weak and fruit was extremely beat up.”
The import program has helped Seald Sweet fill the gaps, and the company has been impressed with quality from Mexico.
“We are running star grapefruit, juice oranges and mandarins,” Parris said.
“Volume is up about 20% and has room for growth. Quality has been excellent. Mexican fruit is being accepted more and more, especially when the customer gets their hands on the product and sees firsthand how great the quality is — this coming from lifelong Florida citrus grower-shippers. We see this commodity to continue to grow in the future.”
Seald Sweet has been running some of its imported fruit in its Florida packinghouse, which has helped those employed there continue to work despite the decreased volumes of Florida fruit.
 
Sunkist
Valencia, Calif.-based Sunkist Growers is shipping navel oranges, cara cara navels, blood oranges, its new Delite mandarins, minneola tangelos, pummelos and lemons.
“Sunkist volumes are strong, especially on larger sizes for oranges,” said director of communications Joan Wickham. “Quality has been good this year, with good flavor balance across the portfolio.”
The company recently doubled its mandarin acreage by adding a new shipper.
In addition, Sunkist has been working to consolidate management structures with Fruit Growers Supply, another cooperative.
 
Sun Pacific
Pasadena, Calif.-based Sun Pacific has been shipping its Cuties, navel oranges, cara cara navels, heirloom navels and lemons.
“All items are experiencing excellent quality with good color, visual appeal and great taste,” said vice president of marketing and business development Howard Nager.
Sun Pacific started with cara caras in December, and they will be available into May.
“We are packing the fruit into high-graphic boxes and bags,” Nager said. “They are available in 3-pound bags as well as bulk with promotable sizes of 72s and larger.”
Nager also gave a positive report on navel oranges.
“Quality has been excellent with 72s and larger in promotable supplies,” he said. “In addition, the winter months are ideal for promoting bags, which are available in 4-, 5-, 8- and 10-pound poly.
“Any of these can also be shipped in tri-wall bins for added display impact and generating incremental sales,” Nager said.
 
Wonderful
Los Angeles-based The Wonderful Co. is growing and harvesting oranges, mandarins and lemons in California; oranges and grapefruit in Texas; and lemons and limes in Mexico.
Vice president of marketing Adam Cooper said Halos shipments are up 12% over the first nine weeks of the season, the Texas grapefruit crop has seen double-digit growth, and lime markets have started the year strong.
“The quality on all varieties this season has been exceptional,” Cooper said.
“Good supply and quality has helped drive repeat orders and both California and Texas are enjoying the same levels of quality and supply.”
Wonderful is expanding its specialty citrus program, which includes items like cara cara navels, blood oranges and gold nuggets.
“We are excited to have special seasonal offerings to complement our year-round core varieties,” Cooper said.
With the acquisition of DNE World Pack, Wonderful gained distribution points in Florida and New Jersey.