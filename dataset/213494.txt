Hurricane Irma left Sunshine State citrus groves with dropped fruit, standing water and dashed hopes.While tomato, strawberry, and vegetable growers came through the storm in comparatively better shape, no part of the Florida produce industry was untouched by the Sept. 10-12 storm.
Industry leaders will lobby for federal relief efforts as loss estimates are determined, said Mike Stuart, president of the Florida Fruit & Vegetable Association, based in Maitland, Fla.
The Florida Farm Bureau predicted damage from the storm will run in the billions of dollars.
Beyond crops, finding labor is a concern after Irma. 
Stuart said industry representatives are working with the Department of Labor to build as much flexibility as possible into H-2A contracts. Finding workers and ensuring they have places to stay is a top concern, he said.
 
Irma replayed
While the northward track of Irma hugged the west coast of Florida’s peninsula and caused greater winds in the Gulf region and the southwest part of the state, heavy rain was experienced throughout the state.
On the east coast, Florida’s Indian River region — where 80% of the state’s grapefruit crop is grown — growers were still assessing damage and trying to drain standing water from groves, said Doug Bournique, executive director of the Indian River Citrus League.
Early damage estimates for grapefruit were running at 25% lost and could rise from there, said Kevin Spooner, managing member of Vero Beach, Fla.-based Southland Citrus LLC. 
Kimberly Flores, marketing director for Vero Beach, Fla.-based Seald Sweet said in an e-mail that company officials have heard preliminary estimates of 50% loss of the citrus crop in the central Florida region, with southwest Florida citrus also hit hard.
Though some published reports speculated that between 20% to 50% of Florida’s citrus crop could be lost, Bournique said it was too early to put a number on crop losses. “We don’t know the full effects and we won’t know for weeks,” he said.
The region experienced as much as 20 inches of rain and winds of 60 to 80 miles per hour.
While the wind blew off some fruit, Bournique said standing water of up to 18 inches was hurting the crop as much as the storm itself. The residual water does not allow the roots of the citrus trees to breathe and the trees become stressed.
Still, Bournique insisted that growers will harvest, market and export substantial volume of fresh grapefruit this season. The region exports four out of every five boxes of fresh grapefruit.
 
Strawberries spared
In west central Florida, Kenneth Parker, executive director of the Florida Strawberry Growers Association, Dover, Fla., said growers are expected to recover from Irma and plant strawberries in time to meet the normal marketing window from November through March.
“Our season is going to be fine — thank God we had not planted yet,” he said Sept. 14, noting the planting season starts about the third week of September and will continue through the third week of October.  About 10,000 acres of strawberries are expected to be planted this year, similar to last year.
Growers have to repair some wind and rain-damaged plastic, with damage estimates pegging that between 5% to 30% of plastic in strawberry fields will need to be replaced.
 
Veg and tomatoes 
Irma hit southwest Florida’s Immokalee Naples region hard.
“It’s a battle,” said Steve Veneziano, vice president of sales and operations for Naples, Fla.-based Oakes Farms Inc., noting the company had 240 workers cleaning the fields and another 100 working in the packinghouse. Labor supplies are very tight, he said Sept 14. 
The company had planted about 20% of its fall vegetable crops — including peppers, tomatoes, eggplant and squash — when Irma hit.
For southwest Florida, delays involved in replanting may mean 90% of crops that could have been harvested in mid- to late November will be pushed to January, he said. 
Other regions were less damaged by Irma’s wind and rains or had not yet planted.
“The good news for the vegetable industry was that it was early enough in the season that there wasn’t much in the ground,” said Stuart of the FFVA.
“We’re going to be fully in business, if a little late, but there’s no doubt we will be in full volume by the end of the year,” Stuart said.
While tomato growers in Florida’s Immokalee region were hard hit, growers in Dade County and Palmetto Ruskin had fewer concerns after Irma, said Reggie Brown, manager of the Florida Tomato Committee.
South Florida’s Homestead growers had not yet planted tomatoes when Irma hit, Brown said. 
For the fall season, only about 4% to 5% of the tomato crop had been planted, and most of those plantings were in the Palmetto Ruskin region south of Tampa. That region fared better than Immokalee, he said. 
While November shipments will be less than normal, tomato volume from Palmetto Ruskin and Immokalee will ramp up by Thanksgiving and Brown expects good volume of tomatoes in December.
 
Georgia report
In the Moultrie-Tifton region in Georgia, a lot of tomatoes, peppers, eggplant that were staked were blown over, said Charles Hall, executive director of the Georgia Fruit & Vegetable Growers Association. 
“Right now I think everybody is evaluating whether they can stand stuff back up that got blown over and whether some of the younger stuff can recover after being pushed over,” he said Sept. 12.
Brandi Corbett Hobby, in sales with South Georgia Produce in Lake Park, Ga., said vegetable crops had some damage and yields will be down, with roughly 30% to 40% of the company’s green beans lost. 
Continued power outage as of Sept. 14 was keeping the company from using its wells and getting into fields to spray for diseases and pests.