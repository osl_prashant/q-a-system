BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Mon.Fri.No. 2 Soft wheat4.61½4.54¼No. 1 Yellow soybeans10.14¼10.16¼No. 2 Yellow Corn3.61½e3.54¼eNo. 2 Yellow Corn3.69½p3.68¼p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted