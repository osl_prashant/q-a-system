With the FDA's new veterinary feed directive (VFD) rules taking full effect on January 1, we can begin to look at the nuts-and-bolts process for filing a VFD order.During the recent Academy of Veterinary Consultants (AVC) winter conference, a panel of veterinarians close to the issue discussed common questions and practical planning for compliance with the new VFD rules. One of the panelists, Zoetis technical services veterinarian Marilyn Corbin, DVM, PhD, outlined specific suggestions for filling out VFD forms.
Beginning January 1, producers intending to use medicated feeds covered by the VFD rule will need to work with their veterinarians on filling out those VFD forms, and how they go about it can affect their compliance with the law and their efficiency in navigating the process.
Paper versus digital
The VFD rule allows producers, veterinarians and feed distributors to use either paper or electronic forms for VFD orders. Regardless of the type of form, each party must keep the VFD order on file for at least two years. On that basis alone, electronic filing offers logistical and storage advantages over paper.
Generic paper forms are available from the FDA and various industry organizations. FDA offers several examples of blank and completed sample forms online.
Corbin notes that animal-health companies supplying VFD products, such as Zoetis, have produced VFD forms specific to each product. These forms will save time and help assure accuracy, since they already include the product name, caution statements and other information that the veterinarian or producer would need to fill in on generic forms.
At least two companies, GlobalVetLink and New Planet Technologies, provide electronic forms, distribution and filing for general use, and animal-health companies provide electronic forms for their VFD products.
GlobalVetLink, provides VFD management through its FeedLink system, and New Planet Technologies includes VFDs in its RxExpress system. Besides eliminating the need for paper forms and associated storage issues, these systems provide easy and accurate completion of forms in the field, electronic distribution of VFD orders, easy access to VFD records and options for managing non-VFD health records.
The VFD form requires the name, address and phone numbers for the producer and veterinarian, the drug to be purchased, the level at which it will be fed in grams per ton, duration of use, withdrawal time and, importantly, reason for use. All that information must comply with label specifications for the drug.
The form also includes information on the animals to be treated, including the approximate number of animals, production class and location. Corbin notes that the form asks for an "approximate" number of animals, which is important because a producer does not always know how many animals will require the medicated feed during the duration of the VFD order (six months maximum). In these cases, it probably is best to overestimate the number rather than underestimate. If you treat fewer animals than listed on the VFD and have feed left over, that is OK. You will, however, need to obtain a new VFD order to use that feed in a different group of animals or beyond the time limits of the original VFD.
The "premises" space on the form refers to the location of the animals to be treated, not the headquarters location of the farm or ranch. The veterinarian filing the VFD order must have a license to practice in the state where the animals are located.
Inspection process - be ready, just in case
FDA officials have indicated their enforcement activities will focus on education during the initial months as the new rules are implemented in 2017, unless they find violations that are severe or clearly intentional. But as veterinarians and producers complete and file their VFD forms, they should consider the possibility of inspection and prepare for it.
Corbin says FDA inspectors, or state inspectors under contract with the FDA, likely will initiate a VFD inspection at the feed distributor. Inspectors will use a checklist to guide them through a list of questions related to the VFD form. At the feed distributor, the inspector will ask to review three randomly selected VFD forms. Next, she says, the inspector will select one form to follow back to the veterinarian who submitted the form, and one form to follow forward to the producer who received the VFD feed. At those locations, the inspector might ask to review VFD forms and ask a series of questions related to the forms.
Questions specific to veterinarians include:
¬? Does the veterinarian have a valid license in those states where VFD feed is being fed?
¬? Does the veterinarian know that either the state or federal requirements for veterinary client patient relationship (VCPR) apply in each state?
¬? Can the veterinarian show any medical record(s) for the client's animals named on the VFD?
¬? Does the veterinarian keep copies of VFD orders for at least 2 years?
Questions specific to producers include:
¬? Does the client keep copies of VFD orders for at least two years?
¬? Did the client feed VFD feed to authorized number of animals on the VFD order?
¬? Did the client feed VFD feed for identified duration on the VFD order?
¬? Did the client stop feeding VFD feed prior to expiration date on the VFD order?
¬? Did the client follow withdrawal period for VFD feed, if any?
¬? Did the client follow any special instructions or caution statements on the VFD order, if any?
¬? If a combination VFD feed was fed, was its use consistent with affirmation statement on the VFD order?
¬? Does the client have labels for VFD feeds? If Yes,
- Does the feed label contain the VFD Caution statement?
- Did the drug level on the label match the drug level on the VFD form?
- Is drug level and indication on VFD form consistent with approval?
For more on preparing for the VFD rule, read VFD Plan at BovineVetOnline.com
For more information on the VFD rule and related regulations, visit the FDA's Veterinary Feed Directive website.
For more information on AVC, and access to recordings of entire presentations at AVC conferences, visit AVC-beef.org.
Next up this week: Lessons learned in VFD compliance.