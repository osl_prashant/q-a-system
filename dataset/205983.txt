Some questions are off limits during a job interview. While applicants are likely eager to provide as much information as they can to the employer, it is important to know what questions are not allowed to be part of the interview. In 1964 Federal laws hit the books which ensured fair practices in the hiring process, however, more than 50 years later, many interviewees are unsure of what questions are not allowable and how to respond if asked a potentially discriminatory question. It’s important to note that most employers are not intentionally asking illegal questions. Often the employer simply needs to rephrase the question to make it relevant to the job.
 
Interview questions related to a candidate’s age, citizenship, nationality or language, religion, disability, marital or family status are the most common offenses. However, this is not an inclusive list. Also off limits are questions about arrests without convictions or military discharge. The key to navigating these types of questions is knowing what personal information is off limits while determining what type of information the question is after and in turn addressing the intent of the question as it relates to your ability to do the job.
 
Age
Interestingly, the Federal law pertaining to age discrimination only protects individuals 40 or older. However, some states have enacted laws that protect against age discrimination of younger employees. For younger candidates, be prepared to address what the interviewer is getting at- do you have enough experience for the job? 
 
Citizenship/Nationality/Language
Without a doubt, it is illegal to be asked questions like, “Are you a U.S. Citizen” or “Is English your first language?” Even being asked “Where did you grow up?” can lead into murky territory. The direct and legal way to ask the question is “Are you legally authorized to work in the U.S.?” Language fluency may be addressed if it is relevant to job performance. As a candidate, it’s important to be able to determine if the question was a conversation starter (although poorly stated) or intentional and answer as you feel comfortable.
 
Religion
Likely the employer wants to know if an applicant will be able to work weekends and what holidays they observe in case it will interfere with scheduled business hours. A candidate is not required to disclose their religion or place of worship. Prior to the interview ensure that you can do the job and work the required hours in a way that aligns with your faith.
 
Disability
Employers can ask about a candidate’s ability to perform the job. These questions are often along the lines of being able to lift items, sit at a desk or travel in a vehicle. Inquiring into the details of mental or physical limitations that do not relate to the job requirements is not allowed. Candidates should answer questions honestly and be upfront about any necessary accommodations as it relates to the position.
 
Marital/Family Status
Questions on this topic come in many forms, all of which are illegal. Being asked whether you’re married, if you have children, if you plan to have children, or what you do for childcare are typically aimed at gauging absenteeism. These questions can also hint at longevity with the company. It is appropriate for an employer to ask if there are any commitments that would conflict with the necessary work schedule. It can be tricky for a candidate to politely handle these types of questions. The best approach is to not promise anything regarding your future and to ensure the employer knows that you are committed to the position.
 
Arrests without Conviction
Candidates can be asked if they’ve ever been convicted of a crime, however, it is illegal to be asked about arrests. Candidates should be aware that several companies conduct very thorough background checks and it will likely be to the candidate’s benefit to be upfront about anything these background checks may uncover.
 
Military Discharge
Employers can ask about what branch of the military a candidate served in, attained rank and applicable skills. Employers cannot ask about the type of discharge or military records. If these questions are approached, applicants should ensure the interviewer that there is nothing in their records that would impact their job performance.
 
If you find yourself walking out of an interview that included off limit questions, remember that the perceived discrimination was likely not deliberate. Too often the interview hosts are simply not aware of the law and how to appropriately phrase questions to ensure they relate to the job requirements. Reflect on the interview and determine if you feel like you’d be a good fit for the company if an offer is extended.
 
If you do feel the questions were intentionally discriminatory or that you did not receive the job because you did not answer an illegal question, you can learn more about your rights by checking with the U.S. Equal Employment Opportunity Commission.
Editor’s Note: This article first appeared in the www.agcareers.com newsletter and is used here with permission. This information is not legal advice and is for guidance only. Check with your legal counsel for the most relevant and up to date information. For more information, go to www.agcareers.com