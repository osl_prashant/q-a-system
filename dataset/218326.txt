Eddie Condes has joined Canadian greenhouse grower Double Diamond Farms as vice president of business development-USA.
Condes will work at the Kingsville, Ontario, company’s recently expanded Nogales Ariz., office, according to a news release.

He has been involved with greenhouse growers since 1994, with a year-long stint at Franklin Farms. He joined Eurofresh Inc. in 1995 as regional sales manager, and was director of global sourcing when he left in 2013.
Before working for Double Diamond Farms, he was director of business development for Greenhouse Produce Co. in Nogales.
“I’ve personally known Eddie for over 20 years in this industry, so I’m very excited to have someone with his skills and experience joining our team,” Double Diamond CEO Chris Mastronardi said in the release. “This gives us an opportunity to further expand our reach, which is critical to our mission of bringing the fresh flavor of greenhouse-grown produce to tables across North America.”