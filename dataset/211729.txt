Will there be a U.S. Department of Agriculture marketing order for organic products? 
As you might remember, the USDA published proposed rules on the marketing order and referendum procedures earlier this year, garnering nearly 15,000 comments all told.
 
Even though the comment period was closed months ago, I see that people are still making comments. In an Aug. 25 comment(the comment period closed April 19) Michael S. wrote on regulations.gov: 
 
“Stick your organic tax where the sun doesn’t shine!!!!!!!! We are taxed to death. Why don’t you create a tax against corporations that create cancer and harm peoples health like Monsanto, Dow, Coca Cola, Pepsi, GMOs. Anything that is good for the People, you idiots have to create a %#$## tax.”
 
 
Another comment, also late, was sunshine and rainbows about the organic assessment:
 
“Organic is a huge part of my value system. My family chooses to invest in organic food and lifestyle products because we believe that its a good choice for our health and the for the health of the planet. Even though we’ve been pleased to see a better variety of organic options available to us in recent years, the simple fact remains that we need MORE organic.”
 
 
So will there be a vote or not on this organic marketing order? There has been nothing official announced, but the USDA did publish its semi-annual regulatory agenda and included the organic marketing order on its “to-do” list.
 
 
From the document:
 
4. Organic Research, Promotion, and Information Order/Referendum Procedures
 
 Legal Authority: 7 U.S.C. 7411 to 7425; 7 U.S.C. 7401
 Abstract: This action invites comments on a proposed national research and promotion (R&P) program that would cover the range of organic products that are certified and sold per the Organic Foods Production Act and its implementing regulations as well as organic products imported into the U.S. under an organic equivalency arrangement. The proposed program would be financed by an assessment on domestic producers and handlers, as well as importers of organic products.
 
Timetable:
Notice of proposed rule making:  01/18/17  82 FR 5438
Comment Period End: : 03/20/17
Final Rule:  09/00/17
 
 
From the above timeline, it would appear we are not far off from a final rule that would set in motion a vote on the organic marketing order. That would give producers a chance to tell the USDA to stick the organic marketing order “where the sun don’t shine” or elevate the organic industry to establishment status at the USDA.
 
Can organic coexist with conventional, or does the attraction of organic food demand an either/or paradigm?
 
Check out these stories from Europe as you consider that questions:
 
 
"Growth of organic food sales stifled by ‘unjustified’ prices"
The story, at euractiv.com, cites a study by French consumer association UFC-Que Choisir that claims high retail markups on organic produce present an “obvious obstacle” to greater consumption of organic food.
 
 
 
“Toxic fruit and veg given free to millions of pupils as it’s revealed harmful pesticides in 84% foods tested”
 
This msn.com story refers to a  “damning report” by Pesticide Action Network UK, which states that a free fruit and vegetable program for students expose them to high levels of pesticides and urged government officials to “do more” to protect children from pesticides.
 
From the story:
 
The 16-page dossier warned: “Children in England are being exposed to a cocktail of pesticide residues in the fresh produce they receive through the Department of Health’s School Fruit and Vegetable Scheme (SFVS).
 
“These pesticides have documented potential to harm human health, especially the health of young children who are particularly vulnerable to their impacts.”
 
Could that type of ploy happen in the U.S.? Could supporters of organic produce criticize the USDA Fresh Fruit and Vegetable Program for distributing fresh produce with “cocktail of residues” and lobby for use of organic produce only for the program? I don’t think it is beyond the scope of possibility, though that type of broadside against conventional produce could not come from the marketing message of the imagined USDA organic marketing order. After all, USDA marketing order rules don't permit "disparagement" of other food products.
 
It is not as if the tension between organic and conventional is new. The fresh produce department has not exploded.
 
Still, the marketing message of organic fresh fruits and vegetables will gain greater attention if the expected referendum succeeds, and that presents uncertainty. So we will all wait for word of the vote, and wonder “what if?”