SALINAS, Calif. — Honored for his commitment to innovation for the specialty crop industry, Vic Smith, chief executive officer of the JV Smith Cos. was awarded a Forbes Impact Award at the Forbes AgTech Summit. 
Western Growers chairman and Duda Farm Fresh Foods senior vice president Sammy Duda presented Smith with the award June 29, praising his vision and leadership as a Western Growers board member.
 
“He has really been a proponent and leader, pushing us into innovation and technology to meet the challenges of our industry — and he is one of the reasons we are having this event today,” Duda said. “Like (Taylor Farms CEO) Bruce Taylor, he has invested a lot of time, money and effort to improve the ecosystem that will improve automation.”
 
Smith has worked to automate lettuce weeding and thinning in the desert growing regions.
 
Duda also said Smith has an ability to break down barriers to help keep the industry working together, particularly his advocacy of the Western Growers Center for Innovation and Technology in Salinas.
 
Smith thanked Western Growers president and CEO Tom Nassif, the staff and the board members of Western Growers and Forbes Media for the honor. He paid special tribute to Taylor, who drew together industry leaders almost five years ago to organize the first Forbes AgTech Summit.
 
“Silicon Valley and Salinas Valley was always a great concept, but it took a tremendous amount of effort to implement this,” Smith said.
Innovation has to continue, he said. 
 
“We know that we are not going to solve the critical issues we face unless we bring innovation and technology to bear, and those are our hopes for the future,” Smith said. “We need the innovators at this conference and the investment capital to meet the most important mission we have, to provide good healthy and nutritious food.”