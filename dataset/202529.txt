Retail success in the Texas onion deal requires consistency, with a bit of color, grower-shippers say.
"They always want to start as quick as they can on Texas 1015s," said Mike Davis, owner of TexMex, which grows on 400-500 acres in Weslaco, Texas. "The key for success at retail is putting up a quality pack - that usually takes care of itself.
Dennis Holbrook, president of Mission, Texas-based South Tex Organics LLC, said his organic onions are an additional asset for retailers.
"We promote a very mild, sweet yellow onion and accompany that with reds we grow here as well as some whites," he said. "So, we have organics, as well as colors and varieties."
The organic component has its hurdles, but it's worth the trouble, Holbrook said. "There's definitely some challenges in growing organic onions, but when you've done it for 30 years like we have, you figure a few of those puzzles out," he said.
Go Texan and other "homegrown" marketing channels are useful at retail, as well, said Don Ed Holmes, owner of The Onion House LLC in Weslaco.
"Some of the Texas chains here prefer doing the (Go Texan), so there is a little of that going on," he said.
The local food movement has some devoted followers among retailers, said David DeBerry owner DKD International LLC and partner in Southwest Onion Growers LLC, McAllen, Texas.

"'Local' helps, of course. Being the first new domestic onions helps, as well. However, none of that matters if we don't supply the right quality, appearance and packaging that retail customers demand," he said.
Jimmy Bassetti, owner of Edinburg, Texas-based J&D Produce Inc., agreed. "Homegrown is always a plus," he said.
However, retailers also are looking for certain types of onions, at least in some instances, Holmes said.
"In this Rio Grande Valley deal, it's more of an onion deal than a sweet onion deal," he said, noting that Vidalia, Ga.-based grower-shippers have "taken the lead" in the sweet category.
"Between Peru and their Vidalia deal, they've extended their season to almost year-round and subsequently were able to offer year-round pricing," he said.
Texas is just a two-month deal, which can be a disadvantage, Holmes said. "The people who do retail like a uniform price year-round," he said.
Seasonal onion growers in Texas look at their deal as analogous to a trip to Las Vegas, Holmes said.
"They're not interested in giving somebody a price for two months. They plant onions here as more of a gamble," he said.
A lot of south Texas onion growers rely more heavily on cotton, grain and sugar cane production, Holmes said.
"They just enjoy growing onions," he said. "These other crops are so steady, so it adds a little excitement."