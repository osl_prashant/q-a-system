Watermelon has moved from the “adoption” stage to the “proliferation” stage in the usage-menu adoption cycle, according to a recent MenuTrends Research study conducted by Datassential and commissioned by the Winter Springs, Fla.-based National Watermelon Promotion Board. 
The presence of watermelon on menus has increased 27% over the past four years, the study says.
 
Independent and chain foodservice operators are embracing watermelon’s “unique, yet versatile flavor profile,” according to a news release from the board.
 
Chefs can pair watermelon with salty, savory, bitter and umami flavor profiles, the release said.
 
The study says that watermelon is one of the fastest-growing fruits featured on salads with more than 100% growth over four years.
 
Chicken and pork are the proteins most commonly paired with watermelon.
 
The research also helps identify opportunities for new, innovative uses for watermelons, and the National Watermelon Promotion Board “can assist operators with recipe ideation to optimize consumption based on research-backed consumer preference,” Megan McKenna, director of foodservice, said in the release.
 
The research also showed that:
Watermelon ranks 24th among most popular fruits and 20th among fastest four-year growth.
Watermelon is most often found on all-day menus and has seen the largest growth on dinner menus.
With the exception of side items, watermelon is expanding across the menu, with appetizers experiencing the most rapid growth.
All regions of the U.S. are experiencing increased use of watermelon.