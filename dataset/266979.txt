BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Tuesday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2536  Down 133May         2600    2606    2477    2498  Down 137Jul                                 2551  Down 131Jul         2633    2639    2516    2536  Down 133Sep         2648    2651    2534    2551  Down 131Dec         2647    2650    2540    2555  Down 126Mar         2632    2636    2531    2544  Down 122May         2629    2638    2540    2548  Down 122Jul         2552    2554    2552    2554  Down 121Sep         2627    2627    2560    2560  Down 121Dec         2633    2633    2565    2565  Down 120Mar                                 2588  Down 120