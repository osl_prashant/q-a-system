Outgoing QPMA president Roland Lafont says the past year has whizzed by in a flurry of meetings and events, and he’s proud of what he’s accomplished with the help of the association’s permanent staff.
“It’s been the fastest year I’ve ever lived,” said Lafont, president of Vergers St.-Paul Inc.
He’s particularly excited about the growing relationship QPMA has developed with the provincial government.
New minister of agriculture, fisheries and food, Laurent Lessard, “knows the game, he’s easy to talk to and he’s enthusiastic about agrifood,” Lafont said.
Lucie Charlebois, minister for rehabilitation, youth protection, public health and healthy living, who plans to attend QPMA’s 2017 convention in Gatineau, is another strong ally, he said.
Last October, the Quebec government added a provision to its health prevention strategy that would see more than half the province’s population consuming at least five fruits or vegetables a day by 2025.
“Our ‘I Love 5 to 10 Servings a Day’ campaign will continue since repetition is the key to developing good habits over the long term,” Lafont said.
He notes that fruits and vegetables are about the only foods that don’t come with a warning from nutritionists about over-indulging.
“A diet rich in fruits and vegetables is a profitable investment in everyone’s health,” he said, “a fact that a study initiated under my mandate and presented at the convention will highlight.”
“Not only does everyone in our industry contribute to a healthier Quebec, but each member plays a key role in the province’s economic development through innovations and by instilling best practices that promote health, traceability, waste reduction, reduced impact on the environment and sound financial management,” he said.