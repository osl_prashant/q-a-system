It is well established that inoculants can be effective silage management tools. What is not always clear is what makes inoculants different, and what producers should look for when purchasing an inoculant. Here are a few of the factors that can be used when evaluating the differences between inoculants to find the right one for each situation:
Research
One of the most important questions to ask is "Is this inoculant backed by independent research?" Ensure that there is ample data for the specific inoculant in the target crop from independently conducted trials that can back up the inoculant"s claims. The trials conducted should also validate the efficacy of the product at the application rate that is on the label. This data should be properly analyzed and ideally published in a reputable journal or presented at a scientific conference.
Product Label
A typical inoculant product label includes the following: product form, number of bacteria, type of bacteria including strain designation, application rate, storage conditions, shelf life, manufacturing date and weight.

Verify that the application rate listed on the label matches the application rate tested in trials.
For front-end fermentation inoculants, ensure that the application rate is 100,000 colony forming units (CFU)/g forage or greater. This is the minimum CFU level recognized by university researchers and advisors for efficacy for these lactic acid producing bacteria (LAB).
For aerobic spoilage inhibitor inoculants the application rate varies, based on the product and crop type: look for independent validation of the product and application rate recommended.
See if the product contains enzymes. These will aid the bacteria in a rapid, efficient fermentation.
Read, understand and comply with the recommended storage conditions and shelf life.  

Manufacturing and Packaging
An often overlooked but important aspect of inoculants, as it can affect the efficacy and viability of the product.
Look for inoculants from a primary manufacturer, as they can guarantee the manufacturing quality of the product. Packaging is critical to inoculants as they are live organisms that can be killed if exposed to heat, moisture and air. Common ways to prevent this exposure include the use of high barrier foil packets or packaging in sealed tubs, using nitrogen flushing during packaging to minimize oxygen and including preservation agents, e.g. moisture scavengers, in the product formulation.
Inoculant Production Video
Ask the Silage Doctor at QualitySilage.com if your inoculants fit these criteria, or if you have further questions about what makes inoculants different, and for all your silage management needs.