1.       Safety First. Here’s what you need to know about how to safely view the eclipse in all of its stages. 

 
 
 
2.       What exactly will we see? It depends where you are. All of North America will see an eclipse of the sun—and those in the path of totality will be able to see a total solar eclipse.
3.       What’s the big deal? A total solar eclipse gives the rare opportunity to see the two most outer layers of the sun’s atmosphere– the corona and chromosphere. This occurs when the moon lines up directly between the sun and the Earth.  
4.       What is the corona? It’s the sun’s outer atmosphere, which is 1 million degrees hotter than the sun’s surface.
5.       What is the chromosphere? It’s the thin layer of the sun’s atmosphere and is only visible during a total solar eclipse (or a very sophisticated telescope).

6.       What does this mean? For those in the path of totality, expect a lot of extra people in your area. Hotels are completely booked. Law enforcement and public safety are issuing special instructions and guidelines. Schools districts all along the path of totality are closed for the day. Check your local media for more. Here’s a link to the Federal Highway Administration’s page dedicated to traffic and safety. 
7.       Is this historic? The last total solar eclipse to cross over the U.S. from coast to coast was on June 8, 1918. Its path was close to being the same as the path of totality for Aug. 21, 2017.  The most recent solar eclipse was on February 26, 1979, but it only crossed above five states in the northwest. The eclipse on August 21, 2017 will cross over 10 states.   

8.       Rural America has the best seat. Few major cities are in the path of totality—Nashville, TN is the closest.
9.       Does this effect agriculture? Since the path spans a lot of rural areas—you should be prepared for your small town’s population to potential be inflated for the day. And some in ag are taking advantage of the event to invite the public to their farms, such as these dairies. 
10.   When will I be able to see the eclipse begin and end? Here's an interactive map pinpointing the event by location.
 

Zoom and click on your location to see information on viewing the eclipse in your area.