South Dakota Wheat Growers has purchased a previously established 40-thousand-ton dry fertilizer plant in Kimball, S.D. The facility has been owned by Gavilon Fertilizer, LLC.

“Today we add a key asset to our farmer-owned cooperative that will allow us to continue to deliver on our mission to better serve our member-owners for generations to come,” says Wheat Growers CEO Chris Pearson.
South Dakota Wheat Growers is based in Aberdeen, S.D., and with this purchase, the Kimball plant will become the largest capacity dry fertilizer plant for the cooperative. Wheat Growers currently has six hub fertilizer plants located in Bath, Huron, Kennebec, McLaughlin, Oakes and Wolsey, with the largest being Wolsey with 31,200 tons of dry fertilizer storage. The Kimball facility is three times larger than any of the other existing plants for the cooperative.
South Dakota Wheat Growers is expecting to merge with North Central Farmers Elevator on Feb. 1.

“As we anticipate becoming a new cooperative on February 1, the addition of this state-of-the-art fertilizer plant allows us to very efficiently provide fertilizer to our farmers,” Pearson says. “It also opens the door to new opportunities in our South Region, by significantly increasing our storage capacity and expanding product availability to producers along the I-90 corridor. This will be key for our producers in that region.”

The Kimball plant includes access to a 110-car unit loop-track, plus high-speed automated fertilizer loadout and storage capabilities.

Gavilon Grain, LLC, which is based in Omaha, Neb., will continue to own and operate the adjacent 2.2-million-bushel grain elevator.