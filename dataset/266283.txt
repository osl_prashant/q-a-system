BC-IN--Indiana News Digest 6 pm, IN
BC-IN--Indiana News Digest 6 pm, IN

The Associated Press



Here's a look at AP's Indiana news coverage at 6 p.m. Questions about coverage plans are welcome and should be directed to the AP-Indianapolis bureau at 317-639-5501, 800-382-1582 or indy@ap.org. Tom Davies is on the desk, followed by Herbert McCann.
All times are Eastern.
A reminder: This information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Eastern. Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
— Adds new items PENCE EMAIL, INDIANA CONGRESSMAN-WIFE'S JOB, DOCTOR-SEXUAL ASSAULT-MICHIGAN STATE, CAR--INDYCAR-INDY 500-PATRICK, BKW--FINAL FOUR-WESTBELD'S RETURN
TOP STORY:
PENCE EMAIL
INDIANAPOLIS — Then-Indiana Gov. Mike Pence faced a firestorm of criticism three years ago after signing a "religious freedom" law critics decried as anti-gay. Now emails released this week to The Associated Press illustrate similar backlash from fellow conservatives when the eventual vice president agreed to change the law in the face of widespread boycott threats. "Indiana is fronted by a coward," reads a March 31 email to Pence's office, which was among more than 1,400 pages of documents obtained under Indiana's public records law. "I just watched your boss throw the ENTIRE Christian population in America under the Left's Gay Extortion Bus." By Brian Slodysko. SENT: 750 words, photos.
GOVERNMENT AND POLITICS:
INDIANA CONGRESSMAN-WIFE'S JOB
FISHERS — An Indianapolis suburb has renewed a $20,000-a-month legal services contract with the wife of Republican Senate candidate Luke Messer.  The city of Fishers pays Jennifer Messer $240,000 a year for part-time legal work she does from home in suburban Washington, where the family moved after Luke Messer's election to Congress in 2012. SENT: 130 words.
AROUND THE STATE:
DIESEL SPILL-SETTLEMENT
SOLITUDE — Marathon Petroleum Corp. has agreed to pay $335,000 for spilling nearly 36,000 gallons of diesel fuel into the Wabash River near the Indiana-Illinois border in 2016. The company's settlement with the U.S. Environmental Protection Agency calls for $109,000 to go to Illinois, which pursued civil penalties based on violations of state-level environmental regulations. The settlement comes as about 42,000 gallons of diesel fuel spilled last week into Big Creek near the Posey County community of Solitude. SENT: 130 words. UPCOMING: 250 words.
CHINA-US TRADE-INDIANA FARMERS
LAFAYETTE — Some Indiana farmers are worried that if China imposes tariffs on U.S. soybeans it could deal another blow to farmers who've been battered by soybean prices that have dropped by nearly half since 2012. More than half of the soybean produced in Indiana is exported and China is one of the main importers of Indiana soybeans, said Kevin Underwood, a Tippecanoe County councilman and a long-time soybean farmer. SENT: 120 words. UPCOMING: 250 words.
DOCTOR-SEXUAL ASSAULT-MICHIGAN STATE
EAST LANSING, Mich. — Michigan State University spent roughly $500,000 for a public relations firm in January to track media coverage and social media activity related to the case of disgraced former sports doctor Larry Nassar, a newspaper reported Wednesday. New York-based Weber Shandwick billed the university $517,343 for 1,440 hours of work done by 18 employees, according to documents obtained by the Lansing State Journal through a public records request. The firm outlined and evaluated related news coverage and engagement on social media posts. The work previously had been done by university employees, though some of that continued alongside the outside firm's work. SENT: 400 words, photos.
EXCHANGE-DISTRACTED DRIVING
GARY, Ind. — Students at Thea Bowman Leadership Academy are learning more than math, English and computer technology. They also are learning about the dangers of distracted driving. To drive home that reality, AT&T brought a virtual reality simulator to the high school. Students and staff — some of whom will be heading out for spring break travel shortly — experienced firsthand how dangerous it is to take their eyes off the road and glance at a cellphone. By Carmen McCollum. The (Northwest Indiana) Times. SENT: 550 words, photos pursuing.
IN BRIEF:
— INDIANA FIRE: A judge has set a $200,000 bond for a northwestern Indiana mother who faces neglect charges after her two young children died in an apartment building fire. Thirty-three-year-old Kristen Gober of Gary appeared in court Wednesday after she was charged Tuesday with two counts of child neglect causing death and other charges. With AP Photos.
— POLICE SHOOTING-INDIANA: Authorities say a police officer has shot and critically wounded a man in northern Indiana following a physical confrontation that also injured the officer.
— JEFFBOARD SHUTDOWN-REDEVELOPMENT: A southern Indiana mayor says the impending closure of a barge and towboat manufacturer could open riverfront property to new development in his Ohio River city.
— DELAWARE COUNTY CRASH: Eastern Indiana police say a fourth person has died following a February collision between two cars.
— GEOTHERMAL UTILITY BILL: The city of Evansville has placed a $1.5 million lien on an apartment building it says continues sending water into local sewers without paying.
— OFFICER DRAGGED-APPEALS COURT: An appeals court has upheld a Muncie man's convictions that stemmed from an attempted traffic stop in which he was shot by a police officer.
— CHILD DEATH-POOL: Police say the death of a 3-year-old boy in a pool outside of a home in Indianapolis is being investigated as an accident.
— HOLCOMB ADMINISTRATION: A high-level Republican staffer in the Indiana House of Representatives has been appointed by Gov. Eric Holcomb to lead the state's purchasing and property management agency.
SPORTS:
CAR--INDYCAR-INDY 500-PATRICK
Danica Patrick returned to Indianapolis Motor Speedway this week expecting to drive an Indy car for the first time since 2011. Weather prevented her from turning any laps, but she still debuted her new No. 13 Ed Carpenter Racing machine on Indy's famed Yard of Bricks. Patrick didn't mind her disrupted schedule.  "This is better for a couple reasons — I'm nervous to get back in the car," Patrick told The Associated Press in a telephone interview from her parents' snow-patched backyard in Indianapolis. By Jenna Fryer. SENT: 850 words, photos.
BKW--FINAL FOUR-WESTBELD'S RETURN
SOUTH BEND — About five hours after returning to Notre Dame after winning the Spokane Regional , Kathryn Westbeld limped Tuesday into her business class at Notre Dame — quantitative decision modeling. No one would have blamed the 6-foot-2 senior forward if she had made her way to the back of the room to find a seat and get more sleep. "I should have, but I was front and center," Westbeld laughed a little more than 14 hours after she scored 20 points to lead the top-seeded Irish to an 84-74 win over Oregon on Monday night, earning a trip to the Final Four.  SENT: 550 words, photos.
___
If you have stories of regional or statewide interest, please email them to indy@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.