Grains mostly higher and livestock mixed
Grains mostly higher and livestock mixed

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were all mostly lower Wednesday in early trading on the Chicago Board of Trade.
Wheat for May delivery rose .80 cent at $4.5820 a bushel; May corn was off 5.40 cents at $3.8420 bushel; May oats fell 3.60 cents at $2.2940 a bushel while May soybeans was declined 27.40 cents at $10.1560 a bushel.
Beef and pork were lower on the Chicago Mercantile Exchange.
April live cattle fell 1.52 cents at $1.1093 a pound; Apr feeder cattle was off 1.87 cents at $1.2958 a pound; April lean hogs was lost .87 cent at .5198 a pound.