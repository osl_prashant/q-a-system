Bilateral talks aimed at reducing the U.S. trade deficit with China have yielded some initial deals, but U.S. firms say much more needs to be done as a deadline for a 100-day action plan expires on Sunday.The negotiations, which began in April, have reopened China's market to U.S. beef after 14 years and prompted Chinese pledges to buy U.S. liquefied natural gas. American firms have also been given access to some parts of China's financial services sector. 
More details on the 100-day plan are expected to be announced in the coming week as senior U.S. and Chinese officials gather in Washington for annual bilateral economic talks, rebranded this year as the "U.S.-China Comprehensive Economic Dialogue."
"We hope to report further progress on the 100-day deliverables next week," a U.S. Commerce Department spokesman said on Saturday. "That will be the basis for judging the extent of progress."
The spokesman declined to discuss potential areas for new agreements since a May 11 announcement on beef, chicken, financial services and LNG.
Earlier in April, when Chinese President Xi Jinping met U.S. President Donald Trump for the first time at his Florida resort, Xi agreed to a 100-day plan for trade talks aimed at boosting U.S. exports and trimming the U.S. trade deficit with China.
The U.S. goods trade deficit with China reached $347 billion last year. The gap in the first five months of 2017 widened about 5.3 percent from a year earlier, according to U.S. Census Bureau data.
"It is an excellent momentum builder, but much more needs to be done for U.S.-China commercial negotiations to be considered a success," said Jacob Parker, vice president of China operations at the U.S.-China Business Council (USCBC) in Beijing.
There has been little sign of progress in soothing the biggest trade irritants, such as U.S. demands that China cut excess capacity in steel and aluminum production, lack of access for U.S. firms to China's services market, and U.S. national security curbs on high-tech exports to China.
The Trump administration is considering broad tariffs or quotas on steel and aluminum on national security grounds, partly in response to what it views as a glut of Chinese production that is flooding international markets and driving down prices.
North Korea has cast a long shadow over the relationship, after Pyongyang tested what some experts have described as an intercontinental ballistic missile on July 4.
Trump has linked progress in trade to China's ability to rein in North Korea, which counts on Beijing as its chief friend and ally.
"Trade between China and North Korea grew almost 40 percent in the first quarter. So much for China working with us - but we had to give it a try!" Trump said on Twitter after the North Korean missile test.
Trading Meat
American beef is now available in Chinese shops for the first time since a 2003 U.S. case of "mad cow" disease, giving U.S. ranchers access to a rapidly growing market worth around $2.6 billion last year.
More beef deals were signed during an overseas buying mission by the Chinese last week.
"There are hopes there will be even more concrete results," Chinese Foreign Ministry spokesman Geng Shuang told a daily news briefing in Beijing on Friday. He did not elaborate.
Critics of the 100-day process said China had already agreed to lift its ban on U.S. beef last September, with officials just needing to finalize details on quarantine requirements.
China, meanwhile, has delivered its first batch of cooked chicken to U.S. ports after years of negotiating for access to the market.
But unlike the rush by Chinese consumers for a first taste of American beef, Chinese poultry processors have not had a flurry of orders for cooked chicken.
Demand should improve once China is allowed to ship Chinese grown, processed and cooked chicken to the United States, said Li Wei, export manager at Qingdao Nine Alliance Group, China's top exporter of processed poultry.
Biotech Crops
Other sectors in China under U.S. pressure to open up have moved more slowly.
Beijing had only approved two of the eight biotech crops waiting for import approval, despite gathering experts to review the crops on two occasions in a six-week period.
U.S. industry officials had signaled they were expecting more approvals. U.S. executives say the review process still lacks transparency.
Financial services is another area where little progress has been made, U.S. officials say.
USCBC's Parker said it is unclear how long it will take for foreign credit rating agencies to be approved, or whether U.S.-owned suppliers of electronic payment services will be able to secure licenses.
The bilateral talks have also not addressed restrictions on foreign investment in life insurance and securities trading, or "the many challenges foreign companies face in China's cybersecurity enforcement environment," Parker said.
In an annual report released Thursday, the American Chamber of Commerce in Shanghai said China remained a "difficult market".