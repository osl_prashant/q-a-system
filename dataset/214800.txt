As the U.S. retail scene continues to evolve, avocados are turning up in a plethora of new places — from mainstream mass marketers to the internet and beyond.
The Avocado Category Channel Overview and its companion piece, the Avocado Channel Action Guide, from the Mission Viejo, Calif.-based Hass Avocado Board look at how the category is shifting across channels.
Available at hassavocadoboard.com, the study, conducted in 2016, concludes that while 73% of all avocado purchases (in dollars) were made in traditional grocery stores, there are a number of “emerging channels,” such as the internet and dollar stores.
The big winners, according to the study, were large-format outlets, which seem to have siphoned off some sales from traditional stores.
While some emerging internet and dollar store outlets may have made gains, annual avocado sales per household in these channels fell from the previous year — down 9.9% for internet and 2.4% for dollar stores, the study says.
Despite the setback, the board says, “The role of the avocado category in these channels may look very different in the future.”
Avocado grower-shippers seem to agree, with some seeing convenience stores as up-and-coming outlets for avocados.
“In the last few years, an increasing number of C-stores have made significant changes to bring fresh, healthy perishable items to their stores to meet their customers’ changing needs and position themselves as a healthy-food destination,” said Dennis Christou, vice president of marketing for Del Monte Fresh Produce NA Inc., Coral Gables, Fla.
Since hass avocados recently were determined to be “heart healthy” by the Food and Drug Administration, “they are ideally positioned to address consumer needs, which contributes to the expansion into C-stores, foodservice and other channels,” he said.
Sergio Cordon, avocado category manager for Robinson Fresh, Eden Prairie, Minn., attributes the growing presence of avocados in new channels to their increased popularity.
“There has been an increase in recipes on cooking shows, media and other means that are making avocados one of the most important items in the stores,” he said.
He said he sees avocados being sold online and bundled with other items to make dishes like guacamoles, soups or shakes.
Rob Wedin, vice president of sales and marketing for Calavo Growers Inc., Santa Paula, Calif., said some mail order companies have become good avocado customers, especially in the Northeast and Southeast.
“It’s interesting how many different formats we’re seeing on the retail side,” said Dana Thomas, president of Index Fresh Inc., Riverside, Calif.
“I’m impressed by the different ways to access the consumer and the different formats at retail.”
Mainstream supermarkets, big-box stores, an increasing number of smaller-format stores with fewer stock-keeping units as well as the internet all are sources for avocados today, he said.
“It appears that instead of moving all in one direction, we’re moving into several directions for a larger customer base,” Thomas said.
He was particularly fascinated by internet food sales.
“How big is that going to get?” he asked. “Will consumers be happy getting produce pre-chosen through the mail?”
There likely are consumers who might enjoy the convenience of internet shopping, he said.
Value-oriented stores — like dollar stores — sell a lot of smaller fruit in bags, said Gary Caloroso, regional business development director for The Giumarra Cos., Los Angeles.
“They’re seeing huge success with those,” he said.
He also is finding more avocados featured in convenience stores.
“You’re going to see more growth out of that area, for sure,” Caloroso said.