Ricketts unites farm, business groups behind tax package
Ricketts unites farm, business groups behind tax package

By GRANT SCHULTEAssociated Press
The Associated Press

LINCOLN, Neb.




LINCOLN, Neb. (AP) — Nebraska Gov. Pete Ricketts rallied major farm and business groups behind his tax package on Monday, despite continued uncertainty over its prospects in the Legislature.
Ricketts and other supporters acknowledged that the bill faces a "narrow path" to his desk. However, he argued it's the best option to help lower local property taxes and the state's corporate income tax. Lawmakers are expected to begin debating the proposal this week.
"It's incredibly important that we address property tax relief this year," Ricketts said at a news conference with leaders from Nebraska agriculture and business groups.
Nebraska Farm Bureau President Steve Nelson said that if the measure passes in this year's session, his group will withdraw its support from a campaign to place a property tax measure on the November general-election ballot.
"Now is the time to act on property taxes," Nelson said. "Nebraskans have asked for relief. They want it delivered."
The Nebraska Farm Bureau has been a major supporter of the ballot drive, although Nelson has said his group would prefer to address property taxes through the Legislature. Ricketts has criticized the ballot measure, saying it would create major disruptions in state government and require a massive state tax increase.
Even so, passing the bill might not stop the signature-gathering campaign that began in February. The Nebraska Farm Bureau has been a "significant partner," but other groups are also helping to bankroll the effort, said Trent Fellers, executive director of Reform for Nebraska's Future.
"Our focus right now is on the petition campaign," Fellers said. "At this point, there's really no reason for us to take our foot off the gas pedal."
The latest version of the bill would provide home and agricultural land owners with a refundable income tax credit equal to a percentage of their total property tax bill. The percentage would grow each year until reaching a cap of 20 percent by 2027 for agricultural landowners and 2030 for homeowners.
It also would lower the state's top corporate income tax rate until it matches the individual rate of 6.84 percent. The proposal would pay for the tax cuts by drawing $34.5 million from the state's emergency cash reserve in the first year. Ricketts said state officials would pay for it in future years by "managing the budget," but did not give specifics.
Critics said the bill offers no specific way to pay for the tax cuts beyond its first year, when it would draw from a cash reserve fund that's intended for emergencies and one-time expenses. Nebraska has faced several recent years of revenue shortfalls, driven in part by a struggling farm economy.
"It's going to spend money we don't have, and the money that it does spend is going to go disproportionately to very wealthy landowners," said Sen. Paul Schumacher, of Columbus, who voted against the bill when it was in committee.
Schumacher said the proposed corporate tax cut "isn't going to influence a single corporation" because it's too small to make a difference.
___
Follow Grant Schulte on Twitter at https://twitter.com/GrantSchulte