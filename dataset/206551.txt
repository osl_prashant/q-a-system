John Duarte is a California farmer who took on the federal government and lost.He’s not the first to do so and probably won’t be the last. However, his case should serve as a wake-up call to farmers, ranchers and land owners across the country.
Faced with millions of dollars in fines and after years of legal procedures, Duarte finally decided to settle with the government and avoid the risk of losing his family operation and jobs for hundreds of workers.
The price for the settlement is over $1 million and being forced to put the ground—his ground—back the way it was before he plowed it even though he still believes he did nothing wrong.
Current federal clean water rules lack clarity and often leave landowners at the mercy of government whims.
While the Duarte case looks like another win for big government and a loss for private property owners, it could have a positive effect as well.
The case points out the need for clear and fair rules and serves as an example of why so many are concerned with the proposed waters of the United States (WOTUS) rule by the Obama administration.
A public comment period is currently underway seeking input on a new rule.
You may question whether these comment periods do any good but at least they offer the chance to be heard.
The Trump administration has reached out to states and individuals seeking information for a new rule.
It’s vitally important that people take them up on their offer so John Duarte’s loss doesn’t become everyone’s loss.