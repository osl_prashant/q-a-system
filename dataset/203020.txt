House Agriculture Committee leaders welcomed new members and announced subcommittee assignments in early February.
 
"From promoting a strong farm safety net to rolling back burdensome regulations on our producers, our subcommittees will play an integral role as this committee goes to work this Congress," House Agriculture Committee chairman Michael Conaway, R-Texas, said in a news release. "The diverse background of my talented colleagues will be instrumental in crafting the next farm bill, and I look forward to working with each of these dedicated advocates."
 
Agriculture committee ranking member Collin Peterson, D-Minn., said reauthorizing the farm bill will be a top agenda item for the committee. "I hope we can all work together to get a new, good bill signed into law before the current bill expires in September 2018," Peterson said in the release.
 
Six Republicans are new to the committee:
Rep. James Comer, R-Ky., farmer and former agriculture commissioner of Kentucky;
Rep. Roger Marshall, R-Kan., a physician;
Rep. Don Bacon, R-Neb., retired U.S. Air Force Brigadier General;
Rep. John Faso, R-N.Y., Republican leader of the New York State Assembly;
Rep. Neal Dunn, R-Fla., a surgeon; and
Rep. Jodey Arrington, R-Texas, a businessman and former vice chancellor at Texas Tech University.
Six Democrats are new to the committee:
Rep. Dwight Evans, D-Pa., created Pennsylvania's Fresh Food Financing Initiative;
Rep. Al Lawson, D-Fla., represents an agriculturally strong district in north Florida;
Rep. Tom O'Halleran, D-Ariz., represents a rural district home to more than 13,500 farms and nearly 23,000 farm operators;
Rep. Jimmy Panetta, D-Calif., represents California's central coast region;
Rep. Darren, D-Fla.: represents a citrus-producing district; and
Rep. Lisa Blunt Rochester, D-Del.: representative of a state with a significant poultry industry.
Committee leadership
 
The Republican chair of the nutrition subcommittee - which develops policies and legislation relating to the Supplemental Nutrition Assistance Program and other feeding programs - is Rep. Glenn Thompson, R-Pa. The Democrat ranking member of the nutrition subcommittee is Rep. Jim McGovern, D-Mass.
The subcommittee on biotechnology, horticulture, and research - which develops policies and laws relating to fruits, vegetables, nuts and plants, organic agriculture and marketing orders - is chaired by Rep. Rodney Davis, R-Ill. The ranking member of the committee is Rep. Michelle Lujan Grisham, D-N.M.
California, Texas, and Florida members of the horticulture subcommittee include Rep. Jeff Denham, R-Calif., Rep. Ted Yoho, R-Fla., Rep. Neal Dunn, R-Fla., Rep. Jodey Arrington, R-Texas, Rep. Al Lawson, D-Fla., Rep. Jimmy Panetta, D-Calif. and Rep. Jim Costa, D-Calif.