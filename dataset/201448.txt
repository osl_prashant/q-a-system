With complaints rising across the countryside, certain U.S. states are taking action to manage suspected dicamba damage. Restrictions and out and out bans limit farmer and applicator options this season.
Note issues with dicamba are taking a national stage. “We are reviewing the current use restrictions on the labels for these dicamba formulations in light of the incidents that have been reported this year,” the Environmental Protection Agency (EPA) wrote in an emailed statement to Reuters.
Until EPA makes a declaration states will continue to make decisions regarding the product. Thus far Arkansas, Missouri and Tennessee have placed new restrictions on dicamba products.
Among the first to lay down new law on dicamba is Arkansas. The state recently passed a 120-day ban on sale and use of all dicamba products, including new formulations. This means the product cannot be used for the remainder of the 2017 season and the state is reviewing options for 2018.
Missouri initially followed in suit, calling for a stop sale on all dicamba products until finding an alternative solution. The ban lasted less than one week, with new application requirements for applicators including wind speed restrictions, a limited application timing window, all applications must be made by a certified applicator, keep accurate records of each application and applicators must complete an online notice of applicator prior to entering the field with a sprayer.
Most recently Tennessee joined states taking action regarding dicamba. Applications must be performed by certified private applicators or a licensed pest control applicator, use of older formulations is prohibited, dicamba can only be applied between 9:00 a.m. and 4:00 p.m. and over-the-top application of cotton after first bloom is prohibited.
Not every state is taking action to combat damage, but many states have experienced increased dicamba-related complaints. Illinois, Minnesota, Indiana and others are each investigating the larger than normal amount of complaints. See the map below, complied by University of Missouri Extension, with official complaints filed as of July 19.




dicambamap
© University of Missouri Extension