Ethanol spill at Richardton plant contained within factory
Ethanol spill at Richardton plant contained within factory

The Associated Press

RICHARDTON, N.D.




RICHARDTON, N.D. (AP) — State Health Department officials are monitoring cleanup of a spill at the Red Trail Energy ethanol plant just east of Richardton.
Officials say about 8,500 gallons of the fuel spilled when a valve on a hose was left partially open. The spill discovered Thursday was contained within the plant.
Health Department officials were at the scene inspecting the site.