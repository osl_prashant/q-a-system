Wenatchee, Wash.-based Stemilt Growers will offer a good supply of late season “Half a Mile Closer to the Moon” cherries from the firm’s high-elevation Amigos Orchard near Wenatchee.
 
Fourth-generation cherry grower Kyle Mathison continues to plant cherries at even higher elevations to push harvest back later in the season, according to a news release.
 
“For every 100 feet in elevation you go up, it pushes cherry harvest back one day,” Mathison said in the release. “This year, we’re looking at harvesting until Labor Day, and hopefully in future years, we can go even later.”
 
Stemilt’s “Moon cherries” are expected to be shipped in specially marked bags and clamshells by Aug. 10, according to the release. That’s about the time that Stemilt finishes up its Kyle’s Pick cherry program.
 
Half a Mile Closer to the Moon brand cherries all come from altitudes of 2,640 feet above sea level and higher, according to the release, with sweetheart, skeena and Staccato cherry varieties representing the top varieties at that elevation. Mathison is planting cherries as high as 4,000 feet, according to the release, so late season cherry volume from Stemilt is expected to expand in future years.
 
The full moon, slated for Aug. 7, will mark the beginning of harvest at Amigos, which Mathison said is considered a good omen for cherry quality, according to the release.
 
“One of the reasons these are called Moon cherries is because of my belief that harvesting cherries just before a full moon results in fruit with higher sugars and flavor complexity,” Mathison said in the release. “The extra gravitational pull from the full moon seems to energize the tree and its ability to build up and store carbohydrates which is passed onto the fruit in the form of higher sugars.”
 
The Amigos orchards overlook the Columbia River and are fairly close to the Mathison family’s original homestead on Stemilt Hill, according to the release.
 
Stemilt marketing director Roger Pepperl said in the release that retailers can use the A Half Mile Closer to the Moon cherry boxes to build displays, and additional in-store signage and digital content is available.
 
“Cherries are a huge contributor to annual produce department dollars, even though they aren’t available all year long,” Pepperl said in the release. “August can and should be a big month for cherry sales and bringing in fresh and high-quality cherries through our A Half Mile Closer to the Moon program is a great way to ensure late-season success.”