Applying a forage inoculant to a crop at harvest requires a leap of faith, as the producer will not see the resulting silage until it is opened weeks, or even months, later."To get best results, producers should use inoculants that are proven with independent, scientific research in the target crop," says Bob Charley, Ph.D., Forage Products Manager, Lallemand Animal Nutrition. "The trials should validate the efficacy of the product at the application rate stated on the label and, ideally, be published in a reputable journal or presented at a scientific conference."
Trials help build confidence in the efficacy of the product tested. However, not all research may be as valuable as it appears at first glance. Dr. Charley recommends making sure that the research is:
Performed with the specific product formulation. There can be wide differences between specific strains of inoculant bacteria, so discount generic data.
Fits the crop being ensiled. Inoculants can perform differently when applied on different crops due to variations in dry matter (DM) content, ensiling challenges and susceptibility to aerobic spoilage.
Conducted at independent research facilities, such as universities.
Published in reputable journals. Trials published in scientific journals are peer reviewed.
Validates the intended application rate. Data should be from studies using the level that will be applied following product recommendations.
"These five points should be validated for any inoculant product offered," Dr. Charley says. "Without data to show claims being made have been proven, it really is a case of 'buyer beware!'"
Producers should also consider their own local conditions, farm practices and silage history ‚Äî although not all these factors can be reflected in independent research. For instance, slow feedout rates may point to using an inoculant proven to increase aerobic stability.
"While it is a stretch to expect to see a specific trial showing the inoculant's performance under slow feedout rates in your state or region, producers facing those management challenges should absolutely look for data on the product's ability to enhance aerobic stability," Dr. Charley says.
Not all products are proven to affect aerobic stability. Lactobacillus buchneri 40788, applied at a minimum of 400,000 CFU per gram of silage or 600,000 CFU per gram of high-moisture corn (HMC), is the only bacteria reviewed by the FDA and allowed to claim improved aerobic stability. Products combining the high rate L. buchneri 40788 with a homolactic bacterium can help control the initial ensiling fermentation and keep the silage stable throughout feeding.
"It comes back to looking for the independent trials data to validate the efficacy you need," Dr. Charley recommends. "I ask producers to think back to challenges they experienced in previous years. Then, look for an inoculant that can help you overcome those obstacles. Asking for proof that an inoculant can do the job you need is your right, and can help ensure your inoculant investment is returned to you in the form of high quality silages."
###
Lallemand Animal Nutrition is committed to optimizing animal performance and well-being with specific natural microbial product and service solutions. Using sound science, proven results and knowledge, Lallemand Animal Nutrition develops, manufactures and markets high value yeast and bacteria products ‚îÄ including probiotics, silage inoculants and yeast derivatives. Lallemand offers a higher level of expertise, leadership and industry commitment with long-term and profitable solutions to move our partners Forward. Lallemand Animal Nutrition is Specific for your success. For more information, please visit www.lallemandanimalnutrition.com.
For more information, contact:
Lauren Kasten
Lallemand Animal Nutrition
(414) 464-6440
lkasten@lallemand.com

2016. Not all products are available in all markets nor associated claims allowed in all regions.