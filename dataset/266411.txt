Longstanding maple syrup tradition continues at sugar camp
Longstanding maple syrup tradition continues at sugar camp

By BETH HENRY-VANCEThe Inter-Mountain
The Associated Press

CASS, W.Va.




CASS, W.Va. (AP) — Making maple syrup has been a longstanding family tradition for a Pocahontas County man — one he's proud to continue at a sugar camp near Cass.
Joshua Shinaberry's grandparents and great-grandparents made maple syrup and hosted all-day pancake dinners for close to 30 years at Cassell's Sugar Camp on Back Mountain Road. Shinaberry said his great-great grandparents probably made maple syrup too — as it was something just about everyone did back in the 1800s.
"That's the way they got their sugar back then," he said in a recent phone interview, explaining the process involves collecting gallons and gallons of sap from maple trees when the temperature is right, and then boiling that sap water down, over an open fire, until it becomes syrup.
"You just boil it — gather it, and boil it," he said — noting you don't have to add anything, and you have a finished product. "It's fascinating to me — all you do is collect the sap, and you're there."
The syrup is carefully filtered and then bottled — and while the process is somewhat simple, it's very time-consuming.
The boiling process can be a 24-hour job for weeks on end, Shinaberry explained, saying he must keep the fire going and check the sap water just about every hour, around the clock. He boils the sap water until it evaporates to nearly the right density of syrup. Then he finishes the boiling process using gas heat, he said — noting the slower evaporation process gives syrup a richer flavor.
It takes anywhere from 40 to 60 gallons of sap water to be boiled down into 1 gallon of maple syrup, Shinaberry said, depending on the actual sugar content of the sap.
Besides the sugar content, the biggest factor is the weather.
For the sap to flow out of tapped maple trees and be collected, the temperature needs to rise above freezing during the day. But the temperature then needs to drop below freezing again — not stay too warm, like the high temperatures that the region experienced during February.
Shinaberry said he has about 2,300 taps in maple trees this year.
The old-fashioned way of collecting sap from a tapped tree included using a bucket, but today, plastic tubing can be hooked up to several trees, allowing the sap to flow downhill into a large container.
He said some trees on the 220-acre sugar camp property are large enough for three taps, but smaller trees often have just one or two taps, depending on their diameter.
Shinaberry has made maple syrup for the past 10 years, doing most of the work himself but getting some help from his father, father-in-law and other relatives. Last year, he made about 310 gallons, while this year was a little lower because of the warm weather last month.
"The 10 days of warm weather we had in February messed things up," Shinaberry said.
He estimated he'll total about 270 gallons this year, which will be sold to wholesalers or local markets.
The current sugar camp building was built in the 1970s by his grandparents, Paul and Gayle Mullenax. Shinaberry's great uncle, Neal Cassell, also played a large role in the operation.
Since the original sugar camp building is getting old, Shinaberry said he plans to put up a new, bigger building later this year, with larger living quarters.
He added he enjoys the tradition of making maple syrup.
"The extra money's good, but I really like doing what my grandparents did. It's a good feeling," he said, adding he hopes to one day host pancake dinners again, just like his grandparents did.
He also loves the actual product.
"I love maple syrup — it's one of my favorite things on planet earth," he said — adding he also loves the combination of the smells when the syrup is boiling, when the steam and the woodsmoke fill the air.
...
The tradition of making maple syrup is carried on by other local residents as well — in rural Randolph County, the small town of Pickens even hosts the annual West Virginia Maple Syrup Festival each March. This year's two-day festival is taking place today and Sunday, and it continues to grow in popularity.
Maple syrup, maple candy, maple sugar and pancake feeds are the highlight of the festival, along with crafts, demonstrations and live entertainment.
One popular stop with festivalgoers is Richter's Maplehouse, a maple sugar camp owned and operated by Mike Richter and his wife, Beth.
Built near the site of Mortimer Hicks' 1880's sugar camp, Richter's Maplehouse contains antique maple-syrup equipment from early neighboring camps. Besides a 150-gallon wood-burning sap evaporator, a bottling room, bucket-hung trees and miles of tubing lines, the Richter camp also offers demonstrations of the maple-syrup-making process and sales of maple products.
Anywhere from 200 to 500 people visit the camp this time of year, Mike Richter said, and he enjoys showing visitors the process of making maple syrup.
He added he enjoys making maple candy and maple butter — which always sells out before the festival — but more than anything, he enjoys being out in the woods.
"That's probably the best part of the process — tapping the trees is one of my favorite parts of the job, and getting the sap and watching it flow," he said.
Mike Richter said he's been making maple syrup for the past 37 years, and he has started to cut back a little. Instead of making a lot of maple syrup and products himself, he now works with other maple-syrup makers in Randolph, Upshur and Tucker counties.
Mike Richter added his least favorite part is that "you can't control the weather."
Despite February's warm temperatures, he said this maple season lasted about four weeks for him, which is close to average.
"If you get four to six weeks, that's normal," he said.
Richter's Maplehouse is located about 4 miles from Pickens, and he said anyone is welcome to stop by for a tour.
___
Information from: The Charleston Gazette-Mail, http://wvgazettemail.com.