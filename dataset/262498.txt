Deal reached to fix unintended tax break for certain farmers
Deal reached to fix unintended tax break for certain farmers

By STEVE KARNOWSKIAssociated Press
The Associated Press

MINNEAPOLIS




MINNEAPOLIS (AP) — Congressional committees and farm groups have crafted language to fix a provision in the federal tax overhaul that gave an unintended tax advantage to farmers who sell their crops to cooperatives instead of other buyers. Both sides now hope to include it in a massive spending bill.
The deal announced Tuesday is meant to keep the playing field level between co-ops and other crop buyers, ranging from independent local grain companies to agribusiness giants such as Cargill and ADM.
The agreement "achieves this goal and restores balanced competition within the marketplace," said a joint statement issued by Senate Finance Committee Chairman Orrin Hatch of Utah and Senate Agriculture Committee Chairman Pat Roberts of Kansas, plus GOP Sens. Chuck Grassley of Iowa, John Thune of South Dakota and John Hoeven of North Dakota.
"I look forward to working with my colleagues in the House and Senate to enact this solution as quickly as possible," House Ways and Means Chairman Kevin Brady of Texas said in a similar statement .
The complex language is the product of weeks of negotiations among committee leaders and staffers with farm groups, including the National Grain and Feed Association and the National Council of Farm Cooperatives. If Congress approves, the fix would be retroactive to Jan. 1.
The U.S. Department of Agriculture endorsed the agreement Wednesday.
"Federal tax policy should not be picking winners and losers in the marketplace," Under Secretary for Marketing and Regulatory Programs Greg Ibach said.
The agreement now needs to get folded into a $1.3 trillion spending bill that Congress must pass by March 23 to avert another federal government shutdown.
But negotiations over the package have been roiled by a host of divisive issues, such as abortion and President Donald Trump's proposed border wall. The bill needs at least some support from Democrats to pass the Senate.
Randy Gordon, president of the National Grain and Feed Association , urged his members Wednesday to lobby their lawmakers, saying time is of the essence.
But the National Farmers Union urged Congress to reject the language. The group advocates for smaller-scale farmers and has deep connections with the co-op movement.
"To repeal parts of this important tax break would be to strike at the single most important benefit family farmers received from tax reform," NFU President Roger Johnson said.