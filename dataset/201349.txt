Below we plug USDA's weekly crop condition ratings into our weighted (by production) Pro Farmer Crop Condition Index (CCI; 0 to 500 point scale). It shows the condition of the spring wheat crop declined marginally to 283.88 points, with conditions improving by a tiny bit in North Dakota. The near-steady rating signals last week's rains stabilized the drought-stricken crop.





Pro Farmer Crop Condition Index






Spring wheat




This Week




Last Week




Year-ago





Idaho (5.74%)*

20.94

21.57


23.02




Minnesota (12.99%)

52.48

52.61


46.67




Montana (14.95%)

34.09

34.09


61.77




North Dakota (50.80%)

140.71

139.69


179.19




South Dakota (10.36%)

19.68

20.09


34.60




Washington (4.22%)

13.29

13.62


17.23




Spring wheat total





283.88





284.38


366.21




* denotes percentage of total national spring wheat crop production.
Following are details from USDA's National Agricultural Statistics Service (NASS) state crop and weather reports:
North Dakota: For the week ending July 23, 2017, much needed rainfall was received in North Dakota, according to the USDA's National Agricultural Statistics Service (NASS). The amount of moisture received ranged from a quarter of an inch to two inches. However, much more is needed to help crop development, improve pasture conditions, and increase water supplies. Conditions have been favorable for spraying crops. Temperatures for the week averaged one to three degrees above normal in the east, while the west averaged three to five degrees above normal. There were 6.4 days suitable for fieldwork. Topsoil moisture supplies rated 31% very short, 36% short, 32% adequate and 1% surplus. Subsoil moisture supplies rated 24% very short, 38% short, 37% adequate and 1% surplus.
Spring wheat condition rated 19% very poor, 20% poor, 29% fair, 29% good, and 3% excellent. Spring wheat headed was 97%, near 98% last year and 93% average. Coloring was 56%, behind 69 last year, but ahead of 48 average.
Montana: Hot, dry, and windy conditions with very limited precipitation occurred for a majority of the state, according to the Mountain Regional Field Office of NASS. High temperatures ranged from the upper 80s to 10 degrees and low temperatures ranged from 32 degrees in Wisdom to the upper 50s. The highest amount of precipitation was recorded in Ridgeway with 0.81 of an inch of moisture while other stations recorded between zero and 0.61 of an inch of moisture. Crop conditions continue to deteriorate due to the hot, dry weather. Soil moisture conditions continue to decline with 92% of topsoil rated very short to short and 88% of subsoil rated very short to short compared with 34% of topsoil last year rated very short to short and 37% of subsoil last year rated very short to short.
Reporters note that barley and spring wheat have been cut for hay or grazed out due to dry conditions, and farmers are hesitant on selling more hay until the second cuttings of hay are finished.
Minnesota: Not available at this time.