San Diego-based Organics Unlimited is rolling out a new brand look in the coming months.
 
Beginning in August, the company plans to include new banana stickers, revised packaging and an updated look and messaging, according to a release. 
 
Retailers and distributors will see the new packaging soon on organic bananas, according to the release, with new stickers on all fruit by Sept. 1.
 
"Retail produce keeps moving more toward organic and sustainably grown," Mayra Velazquez de Leon, president of Organics Unlimited, said in the release. "Consumers want to know exactly what they are eating, where their produce comes from, how it was grown and the resulting impact on the environment and communities. 
 
Organics Unlimited aligned the new banana sticker slogans with its core values, she said in the release, emphasizing farming in a responsible, sustainable and eco-friendly way. 
 
The messages include: 
Sustainability is the fruit of healthy communities;
Eco-friendly organic produce from our eco-family; 
The most responsible banana you'll ever meet; and
Sustainable and ethically farmed from the ground up.
Organics Unlimited staff will visit buyers in coming months to unveil the new brand messages, according to the release, including updated point-of-purchase materials and sales sheets to assist in merchandising organic bananas.