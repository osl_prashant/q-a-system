The USDA will release its prospective plantings report in less than a week on March 29. Corn has been having positive news with a reduction in global stocks and strong exports.

Prices for December corn futures have been giving farmers hope. Although it’s not clear where these markets could go, Joe Vaclavik, president of Standard Grain, is watching forward sales.

Watch his full analysis with Tyne Morgan on AgDay above.