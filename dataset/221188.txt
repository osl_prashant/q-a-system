National editor Tom Karst and staff writer Ashley Nickle discuss the latest USDA report on pesticide residues and consider potential snags for the recent "Harvest Box" proposal by the Trump administration.
MORE: USDA: Pesticide residue levels show no risk to consumers
MORE: Trump Seeks to Deliver “Harvest Box” to SNAP Recipients
MORE: Packer Insight — What retailers think about the 'Harvest Box'