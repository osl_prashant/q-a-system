The Hass Avocado Board is conducting a call for nominations for open board positions.
 
Nominations for new board members will be accepted until March 29 and ballots will be mailed to producers and importers by April 19. The deadline for voting is May 17, according to a news release.
 
The board is beginning a five-year strategic plan with a goal of making fresh avocados the No. 1 consumed fruit in the U.S., according to the release.
 
"This is a call to serve, to share expertise in key areas that will help us maximize our very forward-focused plan," HAB chairman Chris Henry said in the release. "We know members of the industry possess the untapped skills and diversity to help move us forward."
 
The strategic plan sets six strategic priorities guiding growth efforts, according to the release:
building demand;
nutrition;
supply and demand;
sustainability; 
industry engagement; and 
quality.
The board election will feature three  open domestic hass avocado grower seats and two open hass avocado importer member seats, and their respective alternates, according to the release.
 
Members and alternates will be appointed by the USDA Secretary of Agriculture and will be seated at the board meeting on Nov. 15, according to the release. More information about the Hass Avocado Board election can be found online.