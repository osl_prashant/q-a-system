After years of proven performance in Europe, Bruin Sprayers were reengineered for durability required by North American farmers and custom applicators. Equipment Technologies launches the series of self-propelled hydraulic sprayers, which includes two models: HS700 (700 gallon) and HS1100 (1,100 gallon).
The machines are backed by a 5-year powertrain warranty.
The HS1100 models is outfitted with the 120-foot HS1100 boom that adjusts automatically from 60 to 70 inches of clearance, making it the tallest rear boom sprayer available in North America.
Additional features include:
·         A Relaxing Ride: The Bruin’s active pneumatic suspension on the front and rear, as well as hydraulic shocks and oscillating rear axle ensure a smooth, responsive ride even in rough field conditions and terrain.
·         Four Wheel Steer: Reduce soil compaction and crop damage by ensuring rear wheels stay on the same path as the front wheels. Slope compensation with back axle correction, allows for better control on hills and headlands.
·         Wireless Joystick: Make corrections and boom adjustments by a single operator from outside the cab.
·         Crab Steer Mode: Enhances mobility for the operator to more easily operate in tight spaces.
·         Narrow Transport: With axle configurations from 79 to 105 inches, the HS700 makes transport between fields hassle-free.
·         More Power Control Options: Operators can switch between conventional hydrostatic control, fuel-efficient eco mode and foot-pedal acceleration like an automobile, which is especially useful on the road.