LAS VEGAS - Independent grocers treated the education portion of the National Grocers Association annual expo as a SWOT analysis exercise.
 
Attendees now have a pretty good idea of these business' strengths, weaknesses, opportunities and threats, and they should have returned home from the Feb. 12-15 event in Las Vegas optimistic about their niche in the food market.
Strengths
Independent grocers are typically smaller than national chain stores, and that makes them more flexible, which pays off when it comes to offering local produce.
 
Nick Lenzi, senior vice president of operations for Busch's Inc., a 15-store chain in Michigan, said total produce sales at the company are down 0.4% from a year ago, but local produce sales are up 19.5% in the same time.
 
He said Busch's has partnerships with many Michigan growers, and some are so small that they market their fruits and vegetables in store "while supplies last," which creates consumer urgency. 
 
These smaller chains can also integrate themselves into communities better than larger chains.
 
Niemann Foods, a chain based in Illinois, but with 112 stores in four Midwestern states, recently opened a Harvest Market store in Champaign, Ill., which promises to "connect customers to the land," said CEO Rich Niemann. 
 
He said it's a mix of hospitality and entertainment, which caters to the college town's younger customer base. 
 
Niemann also said the store does nearly all its advertising and customer outreach through social media rather than weekly print advertising because that's where the store's customers are.
 
Roche Bros., a chain of 20 stores around the Boston area, opened a small format store in Medfield, Mass., called Brothers Marketplace. 
 
Paul McGillivray, vice president of customer experience, said it's 9,500 square feet and it offers mostly fresh food for fast, convenient shopping.
 
"We recreate the sense of community with in-store dining (serving as) an anchor for this small downtown," he said. 
Weaknesses
Many speakers addressed these as opportunities, but smaller chains face competition from larger chains that can beat them on price; online companies that appeal more to the younger consumers' buying methods; and younger consumers' lack of cooking knowledge. 
 
Steve Dillard, who recently retired from Associated Wholesale Grocers, said, "We're an ingredients-based business in a finished-goods society."
Opportunities
There's no doubt that younger consumers have a demand for value-added food that older generations never had.
 
Dan Koch, vice president of bakery and deli for AWG, said, "If there was one thing I'd invest my time in (as an independent chain), it's fresh-cut fruits and vegetables."
 
Ted Balistreri, co-owner of Sendik's Food Markets, a chain of 14 stores around the Milwaukee, Wis., area, said the company's value-added strategy has operated on two key elements: saving customers time and delivering high quality.
 
It opened a handful of 7,000-square-foot stores called Sendik's Fresh2Go, which offers mainly value-added fresh food, and what works there often works in its normal, larger stores.
 
"What keeps customers coming back?" he asked. "Taste."
Threats
The threat from online retailers - food and non-food - is real and not going away.
 
Erick Taylor, president and CEO of Pyramid Foods, which operates 52 stores under several banners in Missouri, Oklahoma and Kansas, asked the audience, "Who wants to not be involved in e-commerce? All of us! But it's the future."
 
He said the key to competing online is to first have a good product to sell, and then partner with suppliers or wholesalers. And if they don't have a 3-year plan for competing online, find new ones, he said.
 
Pyramid has done online grocery since 2010, he said, so it's made many adjustments to changing consumer demands.
 
Emily Coborn, vice president of fresh merchandising for Coborn's Inc., a 30-store chain based in Minnesota, said the company started cobornsdelivers.com, which offers consumers grocery and meal-kit delivery.
 
She said meal kits are designed by nutritionists and chefs to appeal to consumers in Minnesota.
 
"They're for people who are time-starved, but they can't be too edgy," she says. "They're mostly feeding families."
 
She said Coborn's uses the meal kits to introduce consumers to new items in the store, and they usually notice an uptick in store sales for items that are in meal kits.
 
Coborn said the chain mostly sees online ordering from existing customers, but it expects to bring in new customers as the market matures.
 
Better there than Amazon.com.
 
Greg Johnson is The Packer's editor. E-mail him at gjohnson@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.