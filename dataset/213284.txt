The Washington Jazz apple season is underway and harvest time is nearing for the Envy and Pacific Rose varieties, marketed in the U.S. by The Oppenheimer Group, Rainier Fruit Co. and CMI Orchards.
Sizing is down slightly on Jazz and Envy this season, as with many Washington varieties, but Joe Barsi, president of T&G North America, said the Jazz crop “is a beauty,” in a news release. 
The Jazz brand is owned by New Zealand-based T&G Global.
Apples are a $4.2 billion category in the U.S. and have grown 1.3% in volume and 2.1% in sales from mid-year 2016 to mid-year 2017, according to David Nelley, vice president of categories for Oppy.
“Sales of premium apples are up 48% since 2013,” he said in the release. “That’s the segment of the apple market Oppy specializes in and has been known for, for years. Jazz, Envy and Pacific Rose deserve credit for rejuvenating the category.”
Envy ranks fifth in sales among premium apple varieties, and sales increased more than 60% during the New Zealand season, Nelley said in the release.
This will be the first season Washington Jazz apples are marketed under refreshed branding, Barsi said in the release.
T&G Global and Oppy, which is partially owned by T&G, plan to exhibit their products Oct. 20-21 at the Produce Marketing Association’s Fresh Summit at booth No. 1339.