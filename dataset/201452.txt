Crop calls
Corn: 2 to 4 cents lower
Soybeans: 10 to 15 cents lower
Wheat: 2 to 6 cents lower
Soybean futures are the downside leader this morning on news reports China may target soybeans if the U.S. announces trade sanctions. Adding to pressure is a band of showers moving across Iowa this morning and cooler temps. Wheat futures are seeing spillover from corn and soybeans, as well as from expectations for a bumper Russian crop. This morning's weekly export sales tally was a disappointment for the corn and wheat markets, with soybean sales coming within expectations.
 
Livestock calls
Cattle: Firmer
Hogs: Firmer
Livestock futures are called firmer on followthrough from yesterday's gains. Traders are also encouraged by this week's better-than-expected cash markets. Cash cattle trade began yesterday across the Plains at $116 to $117, with a small number selling for $118. This compares to trade around $117 last week. This opens additional upside potential for live cattle as they hold a discount to the cash market. Traders will also be watching to see if the cash hog market can hold on after some packers raised bids unexpectedly yesterday. August hog futures hold around a $6 discount to the cash index, but fall- and winter-month contracts hold a much steeper discount.