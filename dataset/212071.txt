The Produce Marketing Association is offering a free web seminar on the importance of the Center for Produce Safety’s research to the industry. 
Online registration is available for the web seminar, which is scheduled for 2 p.m. Eastern on Aug. 9, according to a news release.
 
The release said PMA’s chief science and technology officer Bob Whitaker and vice president of food safety and technology Jim Gorny will provide insights on more than 30 research projects featured at the eighth annual Center for Produce Safety Research Symposium, which was June 20-21 in Denver.
 
The web seminar, according to the release, will include discussions of the latest research on assuring microbial quality and safety of agricultural water, soil amendments, produce wash water systems, and other topics.