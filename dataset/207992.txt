Rainier Fruit, Selah, Wash., hired Jeff Weidner as director of warehouse and grower relations, and promoted five sales and business development staff members.Weidner joined Rainier July 3. He has 39 years of experience and is responsible for building relationships with growers and shipping facilities.
“Rainier is not only an industry leader, but they have a winning culture that is known for making things happen,” Weidner said in a news release. “That is an exciting combination to be a part of when you have the tools necessary to succeed and you can expand the dialogue with each customer about what we can do for them. There are no cookie cutter answers when delivering best-in-class service and high quality apples, pears, cherries and blueberries.”
Five other staff members also received promotions. They are:
Randy Abhold, executive vice president of sales and business development;
Andy Tudor, vice president of business development;
Blake Belknap, director of domestic sales;
Troy Howard, director of export sales; and
Shane Zeutenhorst, director of foodservice sales.
“The business of customer service and the supply chain continues to evolve and it is dynamic in its need to not only serve the customer, but also deliver peak returns to growers,” Abhold said in the release. “Having Jeff on board, as well as a focused sales team allows us to create a more cohesive and transparent system that helps us react quickly to changing customer needs. Jeff’s experience in the industry and knowledge of customer and grower expectations will allow for swift changes when needed.”