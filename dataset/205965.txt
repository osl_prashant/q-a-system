The dicamba lawsuit line begins with Bill Bader and his claims of extensive crop damage, but take a number because the queue is growing longer as more farmers file complaints against BASF and Monsanto.
As the first producer to file suit against Monsanto related to dicamba damage across his operation, Bader is the bell cow of drift litigation. However, farmers in at least 10 states are involved in lawsuits claiming various levels of loss and damage due to dicamba-tolerant technology and the number of suits is climbing. How the cases will play out in court remains a matter of conjecture, but one fact is clear: Dicamba-related litigation has only just begun.
Todd Janzen, Janzen Agricultural Law LLC, in Indianapolis, Ind., says a common thread of the dicamba-related lawsuits relates to the allegation that newer formulations of dicamba are incapable of being routinely and safely applied to soybeans. “Unlike a traditional product liability suit, the legal hurdle for the farmers in these cases is they didn’t buy the dicamba and apply it to their own fields. In most cases, they have no contractual relationship with Monsanto or BASF. Instead, they allege they were victims of Monsanto and BASF placing a pesticide on the market that is unsafe,” Janzen says.




 




 


As fallout over drift damage continues through the 2017 harvest, more suits are surfacing in multiple states.


© Chris Benentt












As an illustration, Janzen cites product liability suits against car manufacturers that sell defective brakes: “If you purchased the car and the brakes failed, you have a good product liability suit against the car manufacturer. But, if as a result of the defect, you also injury a pedestrian, does the pedestrian have a claim against the car manufacturer? I don’t think that is a stretch, especially if the car manufacturer had some reason to know the brakes were defective.”
Bader Farms
In 2015, after Monsanto rolled out its dicamba-resistant Xtend crop system in an initial cotton-only debut, Bader’s orchards in Campbell, Mo., were severely damaged by off-target movement. A lawsuit (Bader Farms, Inc., et al v. Monsanto Company) filed by Randles & Splittgerber on behalf of Bader in November 2016, alleges $1.5 million in dicamba damage across 7,000 affected peach trees.
Bader’s 2016 damage estimates are even more alarming: 30,000 trees and a multi-million dollar financial hit. Bader’s suit claims when Monsanto released the Xtend system prior to an EPA label, the company was fully aware the technology would be abused. Bader’s attorneys have submitted an amended complaint to include BASF as a defendant and broadened the allegations to include Bader’s damages in 2017 and beyond.
Attorney Bev Randles says Monsanto knew the Xtend system would cause significant drift problems even before 2015: “As we get into discovery, we’ll be able to push back the date of Monsanto’s knowledge about dicamba abuse and damage.”
“Bill Bader tried to get Monsanto to do the right thing, but they pushed him off and told him they didn’t have the resources to send someone to look at his farm in southeast Missouri. They also said they were recording his conversations and refused to do anything,” Randles notes.
Scott Partridge, vice president of Global Strategy for Monsanto, says Bader’s accusations have no merit. “In Bader’s case, the product that caused him damage wasn’t Xtendimax with VaporGrip technology, it was someone else’s herbicide. The product that was applied and moved, according to Bader, belonged to someone else.”
Landers Plus Nine
In late January 2017, a second dicamba suit (Steven W. Landers, et al. v. Monsanto Company) was filed against Monsanto, again by Randles & Splittgerber, spearheaded by Missouri producers Steven and Dee Landers, but including farmers from nine other states: Alabama, Arkansas, Illinois, Kentucky, Minnesota, Mississippi, North Carolina, Tennessee, and Texas. Echoing the Bader suit, the Landers’ claims center on Monsanto’s release of Xtend technology prior to labeling. The Landers suit will be amended to include BASF, according to Randles.
“This involves producers in 10 total states who have suffered dicamba damage on everything from row crops to specialty crops,” Randles explains.
“We absolutely did not release dicamba before we had a label in place,” Partridge says.
Bruce Farms
Originating in Arkansas, a state in the epicenter of dicamba controversy, a third case was filed in July 2017 (Bruce Farms Partnership, et al. v. Monsanto Company, BASF SE, BASF Corporation), targeting Monsanto and BASF. Six east Arkansas farming operations repeat many of the claims made in the Missouri-based suits, but with a highly significant addition: Dicamba can’t be applied safely during in-season agriculture use, period.
As stated in Section 230 of the filing: “… dicamba-containing products cannot be applied with reasonable safety in agricultural areas using any typical or reasonably practical application techniques and conditions of use limitations. Given the well-recognized nature and patterns of cultivation in these (and other) regions, the proximity of other non-Xtend crops and plants, and the foreseeable weather patterns and timing of likely application, damage to nontarget crops and plants was inevitable and known to Defendants.”
“The evidence we have on hand is Monsanto’s public pronouncement that new dicamba formulations would be safe and less volatile,” says attorney Paul Byrd, Paul Byrd Law Firm. “This hasn’t been borne out in everyone’s field experiences. How could Monsanto not know? How did they test and where did they test?”




 


Robert Goodson, Phillips County Extension Agent with the University of Arkansas, examines dicamba-damaged soybean leaves.


© Chris Benentt







“The great irony here for farmers is the increase in dicamba-tolerant acreage,” Byrd adds. “More damage means a greater increase in dicamba-tolerant crops. I’ve got clients that bought this technology under duress just to protect themselves.”
Partridge says Monsanto has recorded “wonderful results” for almost 99% of Xtendimax applications in 2017. According to Partridge, Monsanto has investigated over 1,000 cases of off-target movement reported directly by growers or applicators: “They told us that across the farm belt, the label wasn’t followed correctly in 77% of instances with off-target movement. In Iowa, for example, 90% of the instances involving off-target dicamba movement, as reported to us by those who applied it, had a primary cause of improper buffer, wrong nozzles or wrong boom height. Again, 99% of our customers had no off-target movement and 77% of those cases with off-target movement were a function of not following the label.”
The Bruce suit accuses Monsanto and BASF of a “scheme and overall conspiracy to control the dicamba crop system while concealing, omitting and suppressing material facts of the dangers of its product to Plaintiffs, resulted in harm, which Defendants knew would result to the unsafe, dangerous and volatile nature inherent with its product.”
BASF officials say the charge is baseless: “We deny that BASF has in anyway conspired with Monsanto or anyone else. BASF has more than 50 years of technical experience with dicamba, and Engenia herbicide was brought to the market after years of research, farm trials and reviews by universities and regulatory authorities.”
Smokey Alley
On the heels of Arkansas’ Bruce lawsuit, another class action was filed on behalf of a group of farmers on July 19, 2017, once again in Missouri (Smokey Alley Farm Partnership et al. v. Monsanto Company et al.). The suit claims BASF, Dupont and Monsanto are involved in anti-trust activity through the widespread introduction of new dicamba technology and products, essentially forcing farmers to plant dicamba-tolerant acreage as a means of protection. (The suit also includes claims of drift damage.)
Attorney Paul Lesko says new dicamba formulations do not work as claimed by BASF and Monsanto: “We’re bringing claims of product liability because farmers are in a quandary. They want to plant seeds of their choice, but due to damage potential, have to consider buying dicamba-tolerant soybeans from a defensive position.”
“We also have anti-trust issues. As discovery is ongoing, we believe the evidence will show Monsanto and BASF knew more dicamba damage would lead to increased sales,” he adds.
“We don’t read the complaint as asserting antitrust claims against BASF and we deny that BASF has in any way conspired with Monsanto or anyone else to violate anti-trust laws,” responds a BASF official.
“We don’t violate laws and any suggestion of a conspiracy is ridiculous,” Partridge emphasizes.
B&L Farms
A day after the Smokey Alley suit, 14 east Arkansas producers filed a class action suit against Monsanto and BASF on July 20: B&L Farms Partnership et al v. Monsanto Company et al. The B&L suit contains a litany of charges related to irresponsible marketing, product liability, breach of implied warranty, deceptive trade practices and more allegations.
Fallout
Tiffany Dowell Lashmet, an Extension agricultural law specialist at Texas A&M University, says whichever case wraps up first could set precedent for the rest of the pack. “Even if the first case to conclude doesn’t set binding precedent, it could still serve as persuasive authority. The remaining cases will likely pivot according to the initial outcome,” she explains.
Lashmet also says the bulk of the suits could be dropped into one court for discovery purposes and potentially for bellweather trials if class certification is granted, in the same manner recent lawsuits against Syngenta (related to commercialization of Viptera and Duricade corn) were consolidated in multidistrict litigation (MDL).
“There’s no way to predict the outcome of these dicamba cases, particularly with so many jurisdictions and varying state laws in play. I don’t see any settlements coming soon, but if the plaintiffs found success early, then we could see settlements in other cases down the road,” Lashmet adds.
As the fallout over drift damage continues through the 2017 harvest, more suits are surfacing in multiple states. “People are waiting to see how things develop,” Byrd concludes. “When harvest is over, I wouldn’t be surprised to see more legal complaints over yield loss.”