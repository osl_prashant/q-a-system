For produce and agricultural interests, the votes are not assured and the jury is out on the proposed tax reform plan from Congressional Republicans.
While lower business tax rates and a six-year phase out of the estate tax drew strong praise, industry advocates also were looking at the loss of deductions and credits in the new plan, called the Tax Cuts and Jobs Act.
House Republican leaders have said they want to pass the bill by Thanksgiving and President Donald Trump said he would like to sign the bill into law by Christmas.
While praising some aspects of the bill, industry leaders declined to give a blanket endorsement of the tax reform plan in early November.
“We have such a diverse membership in terms of business structures that any tax reform proposal is going to impact our industry in multiple ways,” Robert Guenther, senior vice president of public policy for the United Fresh Produce Association, said in an e-mail. “That being said, we are pleased with the estate tax phase out, expansion of immediate expensing, cash method accounting, and corporate tax reductions.”
Guenther said United Fresh is concerned with the bill’s new rules on partnerships and other “pass-through” income for small businesses, along with new requirements on paying self-employment taxes. Guenther said taxes on imports also are a concern for U.S. companies doing business overseas or neighboring countries.
Dennis Nuxoll, vice president of federal government affairs for Irvine, Calif.-based Western Growers, said in an e-mail that the group hasn’t finished analyzing the details of the tax reform plan.
Nuxoll said the group has put together a committee with a cross-section of growers from multiple states to look at the tax changes. Western Growers has been working with those growers and their tax advisors to understand how the bill may affect their operations. Evaluating comments from that cross-section of growers, he said, Western Growers will use it to estimate the bill’s potential benefits for members. 
Pat Wolff, American Farm Bureau Federation senior director of Congressional relations, said in a blog post on the group’s website that the group is reviewing the plan to ensure the effective tax rate lessens the tax burden on U.S. farmers and ranchers.
Wolff said that currently, farms pay taxes under the individual tax code, which has rates as high as 39%. Under the tax reform plan, the special rate just for businesses that will cap out at 25%, she said.
“We don’t know if it will be good or bad for farmers in the end because farmers are being asked to give up a lot of deductions that they have for that new rate,” she said in the post.
The tax reform package would repeal the estate tax in six years, long sought by the American Farm Bureau and other agriculture groups.
The bill continues the deduction for business interest and allows farmers to continue to use cash accounting. Both of those are important provisions, Wolff said in the post.
The new lower business rate for farmers must be balanced with eliminations of tax deductions and credits that farmers use, she said.
“In the end, the bill should only be passed into law if it’s a benefit for farmers and ranchers,” Wolff said in the post.
Business-related highlights of the Republican tax reform plan, according to Republicans on the House Ways and Means Committee:

Lowers the corporate tax rate to 20%, down from 35%;
Reduces the tax rate on business income to no more than 25%;
Establishes safeguards to distinguish between individual wage income and “pass-through” business income;
Allows businesses to immediately write off the full cost of new equipment;
Protects the ability of small businesses to write off the interest on loans;
Preserves the Research & Development Tax Credit;
Makes it easier and less costly for American businesses to bring home foreign earnings to invest in creating jobs;
Eliminates incentives that reward companies for shifting jobs, profits, and manufacturing plants abroad; and 
Doubles the exemption on the estate tax immediately and repealing after six years.