Nutrition has been a focus of marketing efforts at the Wareham, Mass.-based Cranberry Marketing Committee for several years, said Michelle Hogan, executive director.
In fact, she said cranberries are considered the original "superfruit" in the U.S. because of its antioxidant polyphenol content.
Of course, that's part of the committee's marketing message, she said.
"Including health messages in marketing promotions is a great way to attract consumers seeking health benefits from their diets," Hogan said.
She noted a serving of fresh cranberries is a good source of vitamin C and fiber and provides antioxidant polyphenols.
It also has been noted that cranberries may help maintain urinary tract health.
"The nutritional component is huge for cranberry marketing," Hogan said.
Most people likely associate cranberry with urinary tract infection prevention, but Hogan says there are other benefits that may not be as well known.
"A serving of fresh cranberries is naturally low in sugar and sodium, a good source of Vitamin C, a good source of dietary fiber, and cholesterol- and fat-free," she said. "We want to make sure we are touting the nutritional benefits of cranberries at every turn so that consumers can see for themselves what a powerful addition they can be to a healthy diet."
Toward that end, the committee has arranged for a clinical study in Beijing China that will focus on obtaining positive results for H. pylori suppression through the use of cranberry whole berry powder. The ultimate goal being to create a new and sustained demand worldwide for cranberry juice concentrate.
The committee says results from this study also might help support future research in the area of cranberry and gut health.
The study is going to China because "there is a high population of Chinese people with H. pylori, and 20% of those develop stomach cancer, which is the third-leading cause of cancer mortality in the country," the committee said early in 2016 in announcing the study.
A small panel of experts, led by Amy Howell, a research scientist out of Rutgers University, will oversee the trial, the committee said. A media team will work closely with the planning committee to review trial goals/outcomes and journal selection, and to design and oversee global media outreach following trial publication in China.
"The most recent clinical study that the industry is going forward with this year is going to show some very interesting findings that (cranberries) inhibit the bad bacteria without inhibiting the good bacteria," said Bob Wilson, managing member of the Wisconsin Rapids, Wis.-based Cranberry Network LLC, which markets fruit grown by Tomah, Wis.-based Habelman Bros. Co.
Further studies are looking specifically at "positive effects of cranberries within the gut, where the body's immune system is so tremendously affected," he said.
The work in China is expected to enhance an already-eminent reputation for cranberries, said Brian Wick executive director of the Cape Cod Cranberry Growers Association in Carver, Mass.
"We're pinning a lot of expectations on positive results," he said.
Marketing cranberries for their nutritional benefits is ideal for today's health-focused consumer, said Tom Lochner, executive director of the Wisconsin Rapids-based Wisconsin Cranberry Growers Association.
"In general, consumers are looking for healthy products, but we're also marketing the versatility of cranberries and the unique flavor, as well," he said.
Nutrition always is a factor when marketing fresh cranberries - or any fresh produce, said Brian Bocock, vice president of product management at Salinas, Calif.-based Naturipe Farms LLC.
"Cranberries aren't just a pretty holiday fruit; they are a nutrient powerhouse," Bocock said.