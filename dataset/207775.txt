Strawberries were abundant in the summer, and that trend should continue as the season heads toward autumn, marketers say.“Right now, there’s still a lot of strawberries available in promotional quantities into September, and then we transition to a fall crop out of Oxnard and that goes into December,” said Jim Grabowski, director of marketing with Watsonville, Calif.-based Well-Pict Inc.
Well-Pict grows strawberries and raspberries for the late-season deal on about 700 acres in Oxnard, Calif., Grabowski said.
The late-season deal has a challenge not like any other, Grabowski said.
“The trouble is, this time of year, we have a lot of competition from other fruits out there,” he said.
“There’s a lot of people fighting for promotional spaces on the ad pages and space in the produce department. This is our time to work harder.”
Summertime strawberry promotions are ubiquitous. It falls on marketers to keep the momentum going in the late summer and fall, Grabowski said.
“It’s a matter of reminding people,” he said.
“They’ve been seeing (strawberries) all summer and, once you transition to fall, if you can keep retailers to keep them front and center, they still will move.
“It may not have to be the No. 1 item in the ad — the idea is to be in the ad.”
Strawberries have eye appeal, which counts for a lot in a retail produce department, Grabowski said.
“If strawberries can stay in a good position, prominent in the produce department, they still will sell,” he said.
Salinas, Calif.-based Naturipe Berry Growers has a fall crop in Santa Maria, Calif., said Craig Moriyama, director of berry operations.
“By design we set that back from a year ago because there was too much overlap (with the Salinas Valley crop),” he said.
“We pushed that back more toward an end-of-August, September and first-of-October-type deal. Oxnard will go in October and November with their fall strawberries.”
Production has been steady over the summer, with somewhat mild conditions, Moriyama said.
Everything looks good for the fall crop at CBS Farms in Watsonville, said Charlie Staka, operations manager.
“We are on track with that crop, and it looks like we’ll have good volume for the fall,” he said.
The late-season deal should begin under “stable” market conditions, said Jason Fung, berry category manager with the Vancouver, British Columbia-based Oppenheimer Group.
“Obviously, the Fourth of July and post-Fourth often has an effect on all items on produce,” he said.
As of July 14, strawberries from the Salinas-Watsonville District packed in flats of eight 1-pound containers with lids were $5-9 for size medium to large, according to the U.S. Department of Agriculture.
A year earlier, the same item was $8.
The start of the 2017 season was a bit rocky, slowed by some cool, wet conditions in the late winter and early spring, but the crop settled in nicely afterward and continues to produce good-quality fruit, said Cindy Jewell, vice president of marketing for Watsonville-based California Giant Berry Farms.
“We are not expecting any real shifts in volume for the next several weeks, as summer weather patterns have really settled in and volume will be somewhat consistent on a weekly basis,” she said.