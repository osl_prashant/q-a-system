Crop calls
Corn: 1 to 2 cents higher
Soybeans: 4 to 6 cents higher
Wheat: 2 to 5 cents higher
Corn and soybean futures enjoyed light short-covering overnight following yesterday's decline that resulted in sharp losses in soybeans. The U.S. dollar index is sharply lower again this morning and the Brazilian real is firmer following yesterday's plunge, although the real remains sharply below this week's high. The situation in Brazil is far from stable, making traders nervous as they even positions ahead of the weekend. Another round of stormy weather supported wheat futures overnight.
 
Livestock calls
Cattle: Firmer
Hogs: Firmer
Despite lower cash cattle trade, live cattle futures are expected to enjoy followthrough from yesterday's gains as traders focus on narrowing the discount nearby futures hold to the cash market. This week's cash trade occurred largely between $133 and $134, down from $137 to $138 last week. Hog futures also posted a recovery yesterday, which gives bulls the advantage this morning. But with hog futures trading at a premium to the cash index, buying should be limited. The cash hog market has been choppy this week, but overall has firmed.