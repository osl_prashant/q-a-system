The 2015 Dietary Guidelines for Americans recommend that Americans “make small changes” in their daily eating behavior to achieve longer term improvement to their healthy eating patterns. 
So make small changes, take baby steps and you will get results, behavior changes that you are aspiring to achieve. 
 
Sure sounds good. 
 
Sort of like “Take it easy. Take your time. You’ll get there sooner or later.” 
 
When the tough marketing challenge to increase (double!) the daily per capita consumption of fruits and veggies is put on the table, does this “small changes, baby steps” advice make sense to anyone in the produce world given we’ve been taking baby steps for years … and we are still stuck in the same consumption place where we were over a decade ago?  
 
We have a recognized daily shortfall and a consumption recommendation to increase our consumption of fruits and veggies by about 100%, and Americans are being advised and guided to get it done with “small changes and baby steps.” 
 
In case you haven’t figured it out, doubling our daily consumption will probably take a lot of baby steps, a journey well beyond most of our lifetimes.  Small changes. Baby steps. Going nowhere.
 
So, what about considering another way, one that even feels emotionally different. 
 
What about considering “BIG Changes, GIANT Steps?”  
 
Take a look at our consumption challenge to see if it qualifies for BIG stuff — and if the time is now to make BIG things happen. 
 
In our colorful world of eating fruits and veggies, Americans only eat about half the amount they should every day, only about 2.5 cups versus the 4.5 to 6.5 cups recommended by the Dietary Guidelines for Americans. 
 
It’s been stuck at 2.5 cups for over a decade in spite of multitude public and private programs built and promoted specifically to increase the daily per capita consumption of fruits and veggies. 
 
As mentioned before, per capita consumption has been “dead in the water” for over 10 years. Population growth has been the prime mover of overall volume sales increases. 
 
Does this challenge and years of failure look, feel, and sound like something needing Big change and Giant steps, and needing it ASAP? I think so.  
 
So, let’s go from small to big thinking, start now, and make every day for all Americans a new day of engagement along a colorful pathway to better health through fruits and veggies. 
 
Oh, and by the way, the per capita consumption for fruits and veggies in all forms is the intended mission of the Produce for Better Health Foundation. 
 
That is why we absolutely need the PBH to lead and carry the consumption marketing load for all of us. That is why we need the PBH to be great at what it should be doing, not what it has been doing.  
 
John Sauve is co-founder and partner in the Portland, Maine-based Food and Wellness Group LLC.
 
What's your take? Leave a comment and tell us your opinion.