Tracy Shinners-Carnelley has been promoted to vice president of research and quality for Winnipeg, Manitoba-based Peak of the Market, according to a news release. 
Shinners-Carnelley has been with the company for eight years and was previously director of research and quality, according to the release.
 
“Changing Tracy’s title from director to vice president recognizes her contributions to Peak’s senior management team and the significance she has in our company,” president and CEO Larry McIntosh said in the release.
 
Shinners-Carnelley was instrumental in opening Peak of the Market’s 40-acre research, education and quality enhancement field site in southern Manitoba, according to the release.