RFS | Carl Icahn | NAFTA 2.0 | WTO | Rural infrastructure | Perdue | E15 | U.K. election | Cotton policy  






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






EPA is nearing an announcement on RFS mandate volumes, while CFTC said it is not investigating White House advisor Carl Icahn. Meanwhile, Trump’s trade team pledges to work to improve the WTO. On the domestic front, the U.S. cotton industry wants a repeat of the cost-share payments program. We also have updates on Trump’s infrastructure plans for the rural sector, a coming Senate panel look at proposed year-round E15 sales and a bottom-line assessment of U.K. Prime Minister Theresa May’s bad day at the polls. 
— RFS mandate specifics coming. Guessing when EPA will announce the preliminary/proposed levels for corn-based and biodiesel (including advanced biodiesel) are just that, a guess. However, we are in the timeline that EPA typically announces the proposed volume requirements for 2018 ethanol and 2019 biodiesel. While corn-based ethanol should continue at the statutory limit of 15 billion gallons, the biodiesel levels and commentary from EPA could prove the most interesting, what with lingering trade disputes between the U.S. With Argentina and Indonesia. RFS leaks are traditional as EPA typically alerts lawmakers and their staffs, and industry stakeholders are usually also brought into the fold.

— White House advisor Carl Icahn not being investigated by CFTC. A group of Democratic senators continue to press the US government for documents and information on activities by White House advisor Carl Icahn on biofuel policy, with the latest request coming to EPA. But the Commodity Futures Trading Commission (CFTC) said this week it is not investigating biofuel credits and Icahn's activities in that market.
— NAFTA 2.0 update. Governors and premiers from Mexico, Canada and the U.S. will hold a July 14 summit in Rhode Island to discuss their priorities for renegotiation of NAFTA. This week officials from Mexico and the U.S. showed they could settle most differences regarding sugar policy, a development that at least signals similar fates for NAFTA issues ahead.

— Trump’s trade team pledges to work to ‘improve’ WTO. There continues to be a decided change in attitude among Trump administration trade officials regarding that sector’s policy initiatives. The Office of the U.S. Trade Representative (USTR) issued a short statement saying the U.S. “recognizes the importance of international trading systems” and is committed to “working with other members to improve the functioning of the WTO.” The statement, not attributed to any specific official, said that trade “has not always worked to the benefit of everyone” and certain “unfair trade practices” have harmed U.S. interests and created “large, persistent trade imbalances.” This marks a shift in tone from President Donald Trump’s campaign comments calling the WTO a “disaster” and his threat to withdraw from the organization. In his first WTO address as USTR, Robert Lighthizer said the WTO was so essential that if it had not already been invited, it would need to be. But he said the trade body is not working efficiently and it must be modernized to prevent unfair trade practices.
— Trump promises $25 billion for rural infrastructure. The Trump administration’s infrastructure initiative will include a program to put 1 million apprentices to work within two years. The initiatives promises $100 billion “for local prioritization of infrastructure needs,” $25 billion for rural infrastructure and $15 billion for “transformative projects.” The $100 billion would be sent to state and local governments to spend on their own projects, with little input from the federal government. This is the first accounting to date of how the $200 billion federal share of a proposed infrastructure package would be spent, though $60 billion of it is still unaccounted for. That money would go toward traditional transportation projects like roads, bridges and airports, as well as energy pipelines and other infrastructure. Still unclear is what rural infrastructure projects would get part of the $25 billion. Check this infographic link for more insight.
— USDA Sec. Perdue before Senate appropriations panel next week. On Tuesday, Sonny Perdue will come before the Senate Ag-FDA Appropriations Subcommittee. As usual at such hearings, his responses to panel member questions will be the key focus.
— Year-round E15 sales topic of Senate hearing next Wednesday. A Senate committee will consider legislation June 14 that would put in place a permanent waiver to Reid vapor pressure (RVP) requirements to allow E15 sales year-round. The Senate Environment and Public Works Committee hearing will be on the Consumer and Fuel Retailer Choice Act, introduced in March by Nebraska Republican Senator Deb Fischer. A companion bill was introduced in the House in March. EPA annually regulates RVP for gasoline and ethanol blends from June first until September 15. During these months, the EPA restricts the retail sale of fuels with ethanol above ten percent. The bill’s language would extend the RVP waiver to ethanol blends above ten percent.
— Bottom-line on UK election: Britain is a lot like America... polarized and unpredictable come election time. While UK Prime Minister Theresa May is trying to form a majority government, her days are numbered. She simply was not a good candidate and did not have an agenda voters liked. In fact, in a possible signal for this country and others, her major opponent wanted to spend, spend, spend. The removal of the U.K. from the EU is still assured, although some of the voters may have had buyers’ remorse on that account.

— Cotton lobbyists push for a repeat of cost-share program payouts. Cotton industry lobbyists are putting the final touches on a letter that will request the Trump administration to reinstate the Cotton Ginning Cost-Share Program until a new farm bill can provide an effective safety net for producers. Last month, Sens. Debbie Stabenow (D-Mich.) and Pat Leahy (D-Vt.) derailed an effort by key House Ag panel members to make cotton eligible for Title I safety net programs (PLC, ARC). Former USDA Secretary Tom Vilsack used the cost-share program to provide relief for 2015-crop cotton.


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.