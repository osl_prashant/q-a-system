Judge orders state to pay ASAP in citrus canker lawsuit
Judge orders state to pay ASAP in citrus canker lawsuit

The Associated Press

FORT MYERS, Fla.




FORT MYERS, Fla. (AP) — A Florida judge is ordering the state agricultural department to immediately pay residents their share from a class-action lawsuit filed after the state removed their citrus trees.
Lee Circuit Judge Keith Kyle's ruling Tuesday follows a saga in which the state destroyed nearly 34,000 residential trees under the failed citrus canker eradication program in 2000.
Nearly 12,000 Lee County households are part of the suit filed 15 years ago.
A spokeswoman for the agriculture department told the News-Press they're reviewing the ruling.
On Friday, Gov. Rick Scott approved a new state budget that includes more than $52 million to pay homeowners in Broward and Palm Beach counties whose trees were removed. The state ordered the destruction of even healthy citrus trees within 1,900 feet (580 meters) of an infected tree.
___
Information from: The (Fort Myers, Fla.) News-Press, http://www.news-press.com