PrimusLabs has achieved an additional accreditation for its pesticide residue lab in Santa Maria, Calif. 
“We are proud that our residue lab is now additionally accredited by A2LA to the ISO 17025:2005 standard for pesticide analysis of food,” Gosia Myers, vice president of laboratory services, said in a news release. 
 
“We see value in having been recognized internationally under this standard for the residue lab’s laboratory practices.”
 
Since 1987, PrimusLabs has been meeting consumer food safety needs worldwide in different foodservice industries including fresh produce, according to the release.
 
“Our achieving ISO accreditation is an acknowledgement of the efforts of the many people whoe have upheld standards of excellence in our laboratory for many years, and who continue to serve our clients with outstanding customer service and attention to detail,” Sean Kurokawa, residue lab manager, said in the release. 
 
PrimusLabs also has labs in Salinas, Calif.; Yuma, Ariz.; and Belle Glade, Fla.; as well as in Culiacan, Mexicali and Irapuato, Mexico.