Colorado vegetable growers say the weather has been their staunch ally this year.
“Statewide, things look great,” said Robert Sakata Jr., president of Brighton, Colo.-based sweet corn grower-shipper Sakata Farms and president of the Colorado Fruit & Vegetable Growers Association. “There was good snowpack this winter, so water supplies are good.”
It hasn’t been a totally smooth ride though, with some weather issues in early spring, he said.
“There was a late freeze on the West Slope, but they pulled out of that, and near Rocky Ford, they had some hail,” he said. “But, overall, we’ve been pretty lucky.”
Vegetables were looking better than normal, Sakata said.
Sakata Farms grows onions as well as corn, and both were looking good by the end of July, Sakata’s father, Robert Sr., said.
The company was starting to harvest corn in a small way the last week of July, about a week later than normal, he said.
Sakata Farms had a “beautiful” onion crop taking shape and on schedule for harvest in September, the elder Sakata said.
The outlook was positive for Olathe, Colo.-based Tuxedo Corn Co., as well, said Erik Westesen, operations manager.
Long periods of dry weather helped, Westesen said.
Harvest began around July 16, which was a few days earlier than normal, Westesen said.
As of Aug. 22, according to the U.S. Department of Agriculture, cartons/crates of bicolored sweet corn from Michigan were $9.50-10 sized 4-dozen minimum, while last year the same product was $12-14.85.
“We’re not having a problem moving the corn,” Westesen said.
Joshua Johnson, president of Denver-based Ringer & Son, also liked the quality of his corn crop.
“All the way around, very nice,” he said. “The corn came out a little bigger from the start, and we’re really pleased.”
Ringer & Son will harvest into mid-September, he said.
The onion crop was making good progress at Eaton, Colo.-based Fagerberg Produce Inc., said Alan Kinoshita, sales manager.
The company began packing and shipping product at the end of July.
The deal starts with yellows and reds, with whites following by about five to seven days, Kinoshita said.
The product should enter a “fairly strong” market, Kinoshita said.
On Aug. 22, USDA reported 50-pound sacks of yellow onions from Washington’s Columbia Basin and Oregon’s Umatilla Basin were $7.50 for jumbos and $6-6.50 for medium. A year earlier, the same item was $11-12 for jumbo and $9-11, medium.
In Pueblo, Colo., fifth-generation DiSanti Farms reported all was well with its 26 vegetable crops.
“We had a real wet spring and had a late snow, which was challenging, so we were behind in planting, but with this heat – 90s to triple-digits – everything has caught up,” said RoseAnn DiSanti, owner and manager.
DiSanti started harvesting green onions May 1 and will continue through September. Squash and zucchini began in late June and will run through the first freeze in October. The company’s season winds up with its last shipment of pumpkins at the end of October, DiSanti said.
The company also grows chilies, including Anaheim and certified Pueblo peppers, she said.