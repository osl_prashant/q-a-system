Chile’s navel orange season started late, but volumes were picking up by July 18, marketers said.
“We’re three or four weeks into the season, and it’s just now getting to the point where there’s enough supply to meet demand,” said Tony Liberto, import citrus manager with Reedley, Calif.-based Dayka & Hackett.
Weather problems — mostly steady rain — slowed down the navel crop, as it did with other citrus crops in Chile, Liberto said.
“There were some weather issues, and (the) crop will be down from last year,” Liberto said.
A positive tradeoff comes in fruit size, though, Liberto said.
“The fruit size is bigger than last year, and prices are higher than last year,” he said. “It’s been a strange year for navels. We’re just getting into that deal now.”
The navel and mandarin crops typically run until the end of October, but that likely won’t happen this year, Liberto said.
“I think for both mandarins and navels, they’re projecting to end maybe two to three weeks earlier this year, because the crop’s just going to come off all at once, and there’s less fruit on the trees,” he said.
“With the smaller navel crop, the prices will be higher this year.”
Despite the weather issues, orange shipments through week 28 (the week of July 10) were 35,591 tons, up 29% over last season’s 27,600 tons by the same time, said Hernan Garces, sales director for Wilmington, Del.-based Forever Fresh LLC.
 
Pricing
As of July 18, 15-kilogram containers of Chilean navel oranges were $24-30 for size 40s; $26-30, 48s and 56s; $24-28, 64s; $22-26, 72s; and $20-24, 88s, according to the U.S. Department of Agriculture.
A year earlier, the same product was $22-26, size 48s; $22-28, 56s; $20-22, 64s; $18-20, 72s; and $16-20, 88s.
“The fruit from our Chile inspection team is looking good, and we are excited to get this program started because pipeline is dry and customers are anxious to get some citrus in the stores,” said Patrick Kelly, citrus commodity manager for Eden Prairie, Minn.-based Robinson Fresh.
“The quality defects on fruit we are seeing is mostly protruding navels and light scar.”
Kelly said Robinson Fresh was sold out on most early volume, “which is great, and even short on 56s and larger.”
But, he said, “this is only the beginning and we have a long ways to go.”
Navel supplies had a gap between an early-ending California season and the late-starting Chilean deal, said Steve Woodyear-Smith, vice president of categories with the Vancouver, British Columbia-based Oppenheimer Group.
“There’s pretty strong demand in the U.S. and Canada on citrus in general, whether it’s lemons, navels or clementines,” he said. “It’s a pretty strong market.”
Woodyear-Smith said Oppy doesn’t expect navel volumes to recover much before the deal closes.
“We expect to see a continued shortage, so that’s probably a pretty good property to have,” he said.
Supplies should increase on murcotts, but navel and lemon volumes like will remain fairly tight, Woodyear-Smith said.
Nolan Quinn, commercial manager for Fresno, Calif.-based Summit Produce, said fruit quality was good.
“We’ve got some lemons and navels here now and good color and conditions,” he said. “Navels are peaking now.”