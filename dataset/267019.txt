Fairtrade avocados are now available from Prometo Produce, and the company expects to have year-round supplies of conventional and organic product.
Prometo has relationships with more than 50 avocado growers in Mexico, according to a news release, for a total of around 500 acres of avocado trees.
“We believe that trade should support a ‘vida digna’ — a dignified life — for the growers we work with,” Cynthia Cordova of Prometo Produce said in the release. 
“Along with buying on Fairtrade terms, we’re providing opportunities for farmers to invest in their community through the additional Fairtrade Premium paid on each box of avocados sold.” 
The company started in 2015 and was Fairtrade certified in 2016, according to the release. A Fairtrade premium of 12 cents per kilogram of avocados is invested in local communities as determined by the growing operations.
Prometo also has Fairtrade mangoes available February through October.