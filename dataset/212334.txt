A universal subsidy in fruit and vegetable prices could reduce U.S. cardiovascular disease mortality and a bigger price incentive offered to SNAP participants could do even more good, one recently published study in PLOS medical journal says. 
Called “Reducing US cardiovascular disease burden and disparities through national and targeted dietary policies: A modeling study,” the research bolsters the idea that government policies promoting dietary goals can play a big impact in health outcomes.
 
One of the conclusions of the study, according to the abstract, is that a modest universal reduction in fruit and vegetable prices (10% subsidy) was most likely to reduce cardiovascular disease mortality. A 30% fruit and vegetable subsidy offered to SNAP participants appeared most promising for reducing disparities in cardiovascular disease mortality.
 
From the report:
 
Our findings highlight the potentially powerful effects of fiscal measures targeting diet in the US. Dietary policies could potentially reduce cardiovascular disease, death, and associated disparities. A modest subsidy of fruits and vegetables for all, accompanied by a larger subsidy for SNAP participants, might be most beneficial in terms of reducing the disease burden and disparities.
 
---
 
Food safety doesn’t cost, it pays.
 
Aside from that homespun witticism, the question remains; how much does complying with food safety rules cost?
 
Discovering that labor is easily the biggest component of measurable food safety costs, the U.S. Department of Agriculture Economic Research Service report has issued a reported called “Food Safety Practices and Costs Under the California Leafy Greens Marketing Agreement.”
 
Authored by Linda Calvin, Helen Jensen, Karen Klonsky, and Roberta Cook, the 64-page report looked at food safety costs under the Leafy Greens Marketing Agreement, with a look towards how those costs might compare to complying with the produce safety rule under the Food Safety Modernization Act 
 
I’ll have more coverage of this study in an upcoming story for The Packer.
 
 
--
 
A recent blog addressed the question of whether shoppers trade touch and smell of shopping for produce in person for the convenience of online ordering.
 
The discussion thread at the LinkedIn Fresh Produce Industry Discussion Group has a few interesting responses to that question.    
 
Here are some excerpts:
 
 “it would depend upon consumers relying on “Reviews” for buying their fresh produce too.”
 
“I am old fashioned and like and enjoy the customer contact of picking for our customers melons, in-season peaches and nectarines, sharing a recipe cutting a sample so our customers can enjoy the fragrance and taste out of hand and the entire shopping experience that comes from using your senses”
 
“In UK, where online grocery has gained the greatest traction, numbers show a reluctance on the part of the consumer to embrace online shopping for fresh produce to the levels achieved by dry/packaged groceries.”
 
“It is an accepted way of life (in Manhattan, NY) to use FreshDirect, PeaPod, and 4 other smaller companies to get produce sight unseen. Do shoppers trade touch and smell, you bet they do AND in droves! “
 
What do you think? Can online sales of  fresh produce gain competitive heft with supermarket offers?