(UPDATED, Sept. 27) Southern California produce veteran Milt Kaplan, 93, died Sept. 22. 
He was buried Sept. 25 at the Hillside Memorial Park Cemetery in Culver City, Calif.
 
Kaplan’s mother, Dora, moved to the U.S. from the Ukraine at age 9. After peddling potatoes and onions in Idaho and other states, Dora Kaplan eventually moved to Los Angeles in 1908 and eventually opened Kaplan’s Fruit and Produce, which thrived into the 1980s.
 
Born in 1924, Milt Kaplan grew up in the produce business, recalling in an article on the company’s website that “since the day I was born, the only time in my life where I took more than a week off from working in the produce business was when I served our country in World War II.”
 
After working with Kaplan’s Fruit and Produce with his mother into the early 1980s, Milt started The Produce Place after his mother died, building it into a large fresh produce distributor in the Southwest.
 
His son, Ted Kaplan, is president and owner of Vernon, Calif.-based Professional Produce, which Ted began in 1994 after The Produce Place was sold. 
 
Until health problems prevented him from driving this year, Milt would show up to work at Professional Produce between midnight and 1 a.m. every work day, said Maribel Reyes, controller with Professional Produce. Milt was a partner with his son for more than three decades, Reyes said.
 
“Every time you would see him, (Milt) would speak to you about fruits and vegetables,” she said, recalling Milt’s love for mentoring younger sales people with the company.
 

Karen Caplan, president and CEO of Frieda’s Specialty Produce, Los Alamitos, Calif., said she remembers Milt was an icon on the Los Angeles Wholesale Produce Terminal in the 1960s and 1970s. 
 
“I grew up walking the Los Angeles produce market since the early 1970s, and my memories of him are still vivid,” she said in an e-mail.
 
Caplan, who was sometimes asked if Milt was her uncle because of their similar names (no, Kaplan is no relation), said Milt was a true salesman who “lived and breathed” the produce industry.
 
“It’s so great that his son, Ted, was able to keep Milt engaged and involved in the produce industry in his own business, Professional Produce,” Caplan said.