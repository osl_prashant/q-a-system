The American Association of Bovine Practitioners (AABP) Veterinary Practice Sustainability (VPS) committee has received a grant for more than $200,000 from the USDA National Institute of Food and Agriculture Veterinary Services Grant Program (VSGP). The VSGP is intended to relieve veterinarian shortage situations and support veterinary services in the United States.
AABP is using the grant to fund veterinary practitioners to participate in a series of two, two-day intensive practice analysis workshops a year apart. For the first-year 2018 workshops, application requirements include U.S. citizenship, veterinarians who have graduated veterinary school from 2008-2017, veterinarians with at least 10% of their practice income from food animals, and preference will be given to practice owners or those with access to practice financial data and veterinarians practicing in or adjacent to a USDA-designated Veterinary Medicine Loan Repayment Program area.
Presenters in year one include Dick Lewis and Dr. David Welch. The March and April 2018 workshops will be centrally located in Kansas City, Mo.
At the workshops, participants will:

Learn to analyze practice cash-flows to determine profitability and identify trends.
Use value proposition concepts to identify and assess current and future client needs.
Use partial budgeting to gauge cash-flows, and establish timelines.
Forecast to predict how cash-flows may change with new or existing services.
Learn about practice valuation for sale or transition.
Learn about practice acquisition financing.
Network with peers about opportunities and experiences.

“The comments from the 2017 attendees were overwhelmingly positive,” says workshop coordinator and instructor Dr. David Welch. “Follow-up communication demonstrated that participants are taking ownership of the concepts and applying them to their practices.” Comments about the workshop benefits from recent graduates who attended the first-year workshops included having a greater ability to delve into financial details, identifying areas of change and opportunities, having a better understanding how to review financial records, and defining goals and objective for the practice moving forward.
Workshop attendees are required to participate in pre-workshop assignments and conference calls, and will be asked to provide their practice financial data in order to obtain individualized analysis and training for their specific situation and needs. For those without access to financial data, examples will be provided to allow them to participate in the workshops if selected.
An application process will determine participants’ eligibility for this free training. Successful applicants will receive a $500 stipend to attend the training.
Find a detailed description and outline of the program, as well as an application, at http://aabp.org/next_gen/. Applications are due by 5:00 p.m. (EST) December 15, 2017.