The following analysis is from our reporting partners at Real Agriculture in Canada.
 
Since the U.S. opened the NAFTA talks, and even before that, U.S. Dairy Export Council President Tom Vilsack has been pushing hard for more dairy access in the Canadian market.
He has stressed the need for increased competition in North America, saying the consumer will ultimately win. I’ve heard him speak several times and the message has been consistent.
Until now.
Vilsack was on AgriTalk with Mike Adams this week, where he said that U.S. dairy wants change, but he stressed they do not want the death of NAFTA.
When I heard him say this, it reminded me of Vilsack in his previous life as U.S. ag secretary under Obama, when he tried to speak for all of American agriculture, and not just dairy.
Has he realized the Canadian dairy lobby isn’t going to give an inch? Has he given up on getting access to the Canadian market?

His tone on AgriTalk this week seemed softer than in recent weeks, like he had retreated from the aggressive stance coming out of the fourth round of talks in Washington.
But don’t be fooled.
Because we can’t forget about Mexico.
Mexico is the largest export customer of U.S. dairy products. Vilsack and the U.S. dairy industry have to walk a fine line between pushing Trump to take Canada to the brink of NAFTA breakup over dairy access to Canada, while not losing NAFTA and the valuable Mexican export market.
I have repeatedly called Vilsack the ‘nemesis of NAFTA’. I’m wrong. U.S. dairy is just like the beef, pork and oilseed industries. There are gains to be made, but also much to lose.
Canada has 34 million people. Mexico has around 125 million.
At $1.2 billion, dairy exports to Mexico last year were worth around twice as much as exports to Canada, and nearly 10 times what they were when NAFTA began — around $124 million in 1995.
Think back to spring and how Trump blasted NAFTA while wrapping himself in the apparent injustice of the Grassland dairy situation in Wisconsin. Based on Trump’s history of listening to the last person he talked to, Vilsack must be careful. Push Trump too hard on this issue and you might not get access to Canada, and lose Mexico.
A loss of NAFTA is a big loss for the American dairy farmer.