Pope celebrates Easter Mass in packed St. Peter's Square
Pope celebrates Easter Mass in packed St. Peter's Square

The Associated Press

VATICAN CITY




VATICAN CITY (AP) — Tens of thousands of faithful have undergone heavy security checks to enter St. Peter's Square to participate in Easter Sunday Mass celebrated by Pope Francis.
Francis opened Easter festivities with a Tweet to his global flock: "Our faith is born on Easter morning: Jesus is alive! The experience is at the heart of the Christian message."
Pilgrims from around the world and Italy gathered in the square decorated with spring flowers to hear Francis deliver the traditional "Urbi et Orbi" (to the city and to the world) Easter message from the central balcony of St. Peter's Basilica.
Security precautions included bag checks and metal detector wands for everyone entering the square. The Via Conciliazione avenue leading to the Vatican as well as smaller adjoining streets were closed to traffic.