Wetlands fight over western Wisconsin plan heads to Senate
Wetlands fight over western Wisconsin plan heads to Senate

By SCOTT BAUERAssociated Press
The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — The fight over a proposed $70 million sand processing facility in western Wisconsin moved to the state Senate on Wednesday, with a public hearing on a bill the Assembly amended to include environmental exemptions for the Georgia company.
The bill before the Senate committee does not yet include the provision meant to benefit Meteor Timber, but the hearing provides opponents of the move a chance to speak against it in the hopes that the Senate won't go along with the Assembly's change.
The activity in the Senate comes as an administrative law judge in Tomah was hearing testimony this week in opposition to the state Department of Natural Resources decision to grant a permit for the project in May.
Construction is on hold pending results of the appeal.
But the state Assembly last week, in a late-night move on its last planned day in session, approved an amendment to exempt Meteor from additional permitting requirements that could be ordered by the judge to protect nearly 16 acres of wetlands that the company plans to fill.
Essentially, the company could move ahead with filling the wetlands even as the appeal to the DNR permit was pending. The bill would have to pass the Senate and be signed by Gov. Scott Walker to take effect.
Meteor Timber is an Atlanta-based company that's planning a sand-processing plant to serve the frac sand industry near Interstate 94 in Monroe County. Environmental groups and others have been speaking out against the project, saying its plans to destroy a 16-acre wetland that includes a 13-acre rare white pine-red maple swamp would cause irreparable harm.
To make up for destroying those wetlands, the company's plans include restoring more than 630 acres of other land, including wetlands, near the 750-acre project site in Millston.
Opponents to the project allege that Meteor is gaming the system with the Assembly amendment to get around any adverse ruling. Company leaders have said environmentalists are trying to bully them and are being obstructionists.
Nathan Conrad, executive director of the pro-business advocacy group the Natural Resource Development Association, said the loss of roughly 16 acres of wetlands will be more than made up for with the restoration of the other 630 acres. Conrad has also defended the project, saying it will create 300 construction jobs and about 100 people will work at the facility that will process and ship industrial sand to oil exploration companies in Texas.
The Ho-Chunk Nation and Clean Wisconsin are challenging the DNR's issuance of the permit. They argue that Meteor's wetland mitigation plan won't make up for the loss of the rare pine-red maple swamp.
They and other opponents spoke out against it at an administrative hearing this week in Tomah. Former DNR employees testified that they had never seen a project like this approved before and that the wetlands slated to be filled are irreplaceable.
But that hearing would be effectively nullified by the Assembly amendment. The Senate committee hearing the underlying bill on Wednesday could choose to amend it to include the Assembly provision. The Senate could also choose to take up the Assembly version of the bill when it meets on its final day in session on March 20.
Wetlands act as natural flood control and water filters. They support a wide range of wildlife, are a key component of the ecosystem and in some cases can take hundreds or even thousands of years to develop.
___
Follow Scott Bauer on Twitter: https://twitter.com/sbauerAP