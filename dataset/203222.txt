In the town full of acronyms, CAERS is just another one, more obscure than most. Produce marketers should get to know it.
 
The Food and Drug Administration's Center for Food Safety and Applied Nutrition's (CFSAN) Adverse Event Reporting System is described by the agency as "one of the post-market surveillance tools that monitor the safety of foods and cosmetic products.
 
The FDA says "adverse event reports" relate to conventional foods, dietary supplements, and cosmetics come primarily from consumers and health care providers. 
 
Previously only available by Freedom of Information Act request, CAERS released a data file dump in December that includes adverse reports - instances of sickness and worse - from consumers, health care practitioners, and mandatory reports by industry from January 2004 through March 2016. 
 
The agency said CAERS captures any adverse events or complaints related to foods or cosmetics. That can include "minor to major" medical events, but also complaints about off-taste or color of a product, defective packaging, and other non-medical issues.
 
"However, it's also important to understand the information in the database is exactly as reported to the FDA, and the agency has not necessarily determined if the products(s) in question were the actual cause of the events reported," the agency said in a news release.
 
From January 2004 through the end September 2016, the FDA received 56,574 adverse event reports. Of these, 26,840 adverse events were reported for conventional food; 25,412 were reported for dietary supplements; and 4,322 were reported for cosmetic products.
 
The FDA will offer updates on a quarterly basis going forward, according to the agency.
 
The file covers thousands of cells on a downloadable spreadsheet, and it includes details that I never knew existed.
 
I don't recall ever feeling even off key, let alone seriously ill, because of fresh produce or virtually any other food I've consumed. But plenty of people in the U.S. do, and this CAERS database has the data to prove it.
 
With From fiber laxative to raw oysters, from peanut butter to produce, seemingly every type of FDA-regulated food is finding an "adverse" reaction somewhere. Adverse effects can range from nausea and stomach upset to hospitalization and death.
 
Not all adverse effects are related to pathogens.
 
For example, an entry for Jan. 1 of 2004 noted that a consumer (age unknown) age died from choking on a grape.
 
An entry from Jan. 11 of 2004 reports a "non-serious" adverse event of nausea related to consumption of Green Giant spinach.
 
Another entry from Nov. 18 in 2004 reported that a female who consumed Driscoll's strawberries reported a non-serious illness with symptoms of hypersensitivity, vertigo and nausea.
 
I'm not sure how any one can get vertigo from eating strawberries, but that is that kind of fine detail available in the report. 
 
For those who want a closer look at "adverse events" without downloading the report, I grouped all the vegetable and vegetable products in this Google spreadsheet  and the fruit-related events in this spreadsheet 
 
A FDA blog post describing the FDA's thinking behind why the agency is making reports of adverse events available to the public is found here.
 
 
Authored by Susan Mayne and Katherine Vierk, the blog post states:
 
Transparency in the actions we take as an agency, and our reasons for taking them, is an important value for FDA in its mission to protect public health.
That is why we are, for the first time, making public the data that FDA's Center for Food Safety and Applied Nutrition (CFSAN) receives about adverse events related to foods, including conventional foods and dietary supplements, and cosmetics regulated by FDA. This is information that was once only available through Freedom of Information Act (FOIA) requests, but will now be easily available to researchers, consumers, and health professionals.
This first posting of data from CFSAN's Adverse Event Reporting System (CAERS) includes data from reports submitted by consumers, medical professionals and industry from 2004 through September 2016. The term "adverse event" is an umbrella term for a number of poor outcomes, including bad reactions, illnesses or deaths. We plan to update this information quarterly to ensure that the public has the most current information available.
The goal of CAERS is to provide indications, or "signals" of potential hazards. FDA uses these adverse event reports to monitor the safety of foods, including conventional foods and dietary supplements, and cosmetics. This information can, and has, led to investigations of specific products, targeted inspections and product testing, import alerts, warning letters, and enforcement actions.
Examples of how adverse event data has been used to support multiple actions by FDA include recalls of HydroxyCut and OxyElite Pro dietary supplements, and investigations of cosmetic products, such as EOS lip balm and Brazilian BlowOut hair smoothing treatment.
A few caveats about CAERS: The data from the reports is what was reported to the agency. FDA has not necessarily determined that the events reported were actually caused by the product in question. And there often are gaps in the information provided, which should ideally include the product name, symptoms, outcome, consumer's sex and age, and the date the adverse event was experienced.
Going forward, FDA intends to modernize the system to make reporting adverse events as user-friendly as possible. You can expect to hear more about that in about a year. But in the meantime we didn't want to delay giving the public access to data we have. The CAERS data will be posted on fda.gov and is also available through OpenFDA, launched in 2014 to make it easier to access the agency's publicly available information.
We're hoping that this increased transparency will result in more detailed and complete reports that will help us to more rapidly identify red flags about a possible safety issue with products we regulate. Anyone can report a safety or quality issue with an FDA-regulated food (conventional foods and dietary supplements) and cosmetics. To do so, visit fda.gov.
 
 
 
TK: The motives for increased transparency sound valid enough. Any system that can that can identify food safety issues earlier would be valuable. It will be a measure of the data if the public, government and industry can in fact find enduring value in what is reported.