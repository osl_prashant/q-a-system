With cool, rainy weather for much of the Midwest in the summer of 2015, locally grown programs were a challenge for distributors like Kansas City, Mo.-based C&C Produce.
Regardless, C&C saw big growth in its local sales. Another Harvest to Market Open House event in December, in which C&C brings area buyers and its local suppliers together, was successful.
With excellent growing weather this year, the company is ready to take its local program to another level, said the company's Chris Shea.
"This has been a tremendous growing year. We've expanded our growers, and our grower partners are growing more for us. Volumes and quality have been good."
C&C expects about a 25% growth in its local sales this year, Shea said.
In the first week of August, Missouri watermelons were coming on in huge volumes for C&C. Just around the corner was the start of the company's local fall deals, with squash and pumpkins leading the way.
"Winter squash is to the point where we're supplying some of the biggies," Shea said.
C&C expects to source squash mainly from Missouri but also from Kansas.
Also moving up into the "biggie" category for C&C are organic sprouts sourced from Lawrence, Kan.-based ChloroFields, Shea said.
"We started working with them five years ago. Now we're distributing their products to major retail warehouses. They have great attention to detail, food safety and quality and have developed a loyal following."
New for this year, ChloroFields is adding an organic living basil product, Shea said.
"Our customers are very excited about that."
The Harvest to Market event has been a key to growing C&C's local program, said company partner Nick Conforti, who looks forward to holding the fourth annual version of the event this December.
"The transparency we have has taken the program to the next level," Conforti said.
Local has grown so much for C&C, the company now has a 10,000-square-foot space in its warehouse dedicated to local product, Conforti said.