Keep the produce stocked.
 
That’s kind of a vague statement, isn’t it? Yet I bet every store and produce manager has uttered the line in the midst of firing off directions every day. I know I’ve said it myself, or at least, “Keep it looking good!”
But again, what does it mean, exactly? I’ve worked grand openings where employees outnumbered customers and every apple, every packaged salad purchase was quickly followed by a clerk filling the void with a single item. “Keeping things perfect,” they’d say. 
A little overkill, but it happens. 
To contrast, many produce displays are built, then left to weather through several strong waves of customers shopping the display. And too many times, what’s left afterward is a handful of cull-worthy produce items, and a lot of green mat showing. This end result is very unappealing and repels any hope for sales at that point, when subsequent waves of customers enter the produce department.
Of course each of these replenishment examples is an extreme. Which raises the question, exactly when or how should a produce display be tended to? 
This may even seem a bit of an overreach. Overthinking the job, as some of my produce peeps may say. Fair enough, as seasoned clerks rarely need such direction. However, when you have someone filling in from outside the department (no pun intended), or you have a new clerk in the produce department, be assured they’re thinking along these lines: “What do I stock, how much and when?”
First, I’d coach that newer employee to never let a display sell down to the mats. I’d direct them to allow it to sell down naturally, and as it does so, frequently give the display a good straightening. This means rearranging the display so that it remains clean, culled, newly-faced, level and as fresh looking as possible.

Exactly when or how should a produce display be tended to? 

Then, when the display can be efficiently rotated and stocked with fresh product (typically when it is about half of stock capacity), promptly bring out enough new produce to quickly and thoroughly complete the task. 
Many times, the stocking task at hand includes several other produce stocking items too. That’s why it’s a common sight to see clerks with numerous commodities on their carts as they work. They’re busy!
If such normal stocking was this simple, it’d be a wonderful image, wouldn’t it? With animated bluebirds hovering around the smiling clerk as he or she is engaged in the task.
But in reality this is also where I’d also coach the employee to keep their head in a swivel, in react mode, looking out for safety hazards, for customers in need of assistance, and with hundreds of stock-keeping units to tend to, what they need to stock next.  
Armand Lobato works for the Idaho Potato Commission. His 40 years’ experience in the produce business span a range of foodservice and retail positions. E-mail him at lobatoarmand@gmail.com.