As in any business, time on the farm is of the essence. You are probably tuned into major time-critical activities, such as har­vesting crops or breeding livestock, but are you tuned into your daily time management? In short, do you really know what you do each day? Believe it or not, keeping a detailed diary for a few days can help you better understand your challenges and needs for better time management.Here’s how to do it: Record as much detail as possible for at least a week. You can use a pocket notebook or just a few notes on your smartphone. Make brief notes of what you have been doing and about how much time you spent on the task. Don’t forget to record “the little stuff”. That can chew a lot of time out of your day.
When you stop recording, take a look at your diary and ask yourself these questions:
?What was I doing?
? Are there routine and frequently repeated tasks in my day?
? Is there someone else on the farm that could perform some of these tasks as well or even better than I can do them?
? How long did I spend on tasks that someone else could have done?
?Which of my tasks are high priority to the business and which might be dropped or passed to someone else?
? Of the tasks that cannot or should not be passed to some­one else, what are implications to the farm business if I don’t get them done on a particular day? What if they don’t get done at all?
Next, list the tasks that need to be done each day and at the end of the day, ask yourself:
?What didn’t I get done?
?Why didn’t it get done?
? Does it need to be done at all?
? Did not getting it done really make a difference somewhere in the operation?
?Were some high priority tasks not completed because low priority items got in the way?
? How can I change for tomorrow?
Finally, congratulate yourself for what you did get done rather than beat yourself up for what you didn’t get done. Use the infor­mation to plan a more productive day tomorrow, with priority items getting more attention. Remember, the purpose of these exercises is to help you as the manager make more time to per­form the tasks you should be performing, because they are key to the success of the business.