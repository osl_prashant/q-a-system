It seems as if each time a retail chain announces expansion or upgrades, it focuses on fresh food.
 
And each time a report comes out about a chain struggling, it's falling short in fresh food.
 
Discount retailer Aldi plans to spend $1.6 billion remodeling and expanding its more than 1,300 U.S. stores in the next 3 years, and the chain said it needs to create more space for fresh items, including produce. 
 
Meanwhile, Whole Foods is jumping into fresh produce butchering, after several other chains began testing this quirky way of producing fresh-cut product. 
 
But overall business for Whole Foods has been down, and many people speculate it has to do with competitors matching Whole Foods on organic produce quality and quantity.
 
Retail chains are obviously reluctant to discuss when things aren't going well, but last year publicly traded Target Corp. acknowledged it was struggling in grocery and in particular in fresh food, as consumers questioned the chain's commitment to it.
 
One of the clear lessons is that fresh food, and fresh produce specifically, holds a key to differentiation in the marketplace, whether going for high income consumers like Whole Foods or more budget conscious, as Aldi typically serves.
 
And chains that don't make it a top priority will suffer.
 
Did The Packer get it right? Leave a comment and tell us your opinion.