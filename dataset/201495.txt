Click here for related charts.

CORN COMMENTS
The national average corn basis softened 3 1/4 cents from last week to 24 cents below September futures. The national average cash corn price improved 4 cents to $3.48 1/4.
Basis is much weaker than the three-year average, which steady with futures for this week.
 
SOYBEAN COMMENTS
The national average soybean basis firmed 7 3/4 cents from last week to stand 16 3/4 cents below November futures. The national average cash price improved 3 1/2 cents from last week to $9.56 1/2.
Basis is well below the three-year average, which is 80 cents above futures for this week.
 
WHEAT COMMENTS
The national average soft red winter (SRW) wheat basis softened 1 1/4 cent from last week to 4 cents below September futures. The average cash price is unchanged with last week at $4.55 1/2. The national average hard red winter (HRW) wheat basis softened 1/4 cent from last week to 73 3/4 cents below September futures. The average cash price declined 1 cents from last week to $3.90.
SRW basis is firmer than the three-year average of 24 cents under futures. HRW basis is much weaker than the three-year average, which is 52 cents under futures for this week.
 
CATTLE COMMENTS
Cash cattle trade averaged $117.30 last week, up 14 cents from the previous week. Cash trade has begun at $1 to $2 lower levels. Last year at this time, the cash price was $118.92.
Choice boxed beef prices dropped $5.41 from last week at $201.66. Last year at this time, Choice boxed beef prices were $200.87.
 
HOG COMMENTS
The average lean hog carcass bid softened $4.22 over the past week to $85.62. Last year's lean carcass price on this date was $68.79.
The pork cutout value declined $5.70 from last week to $94.31.  Last year at this time, the average cutout price stood at $75.64.