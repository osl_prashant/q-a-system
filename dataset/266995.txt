Lawsuit blames pork giant for noxious farm smells
Lawsuit blames pork giant for noxious farm smells

By EMERY P. DALESIOAP Business Writer
The Associated Press

RALEIGH, N.C.




RALEIGH, N.C. (AP) — A low-cost, high-volume livestock method pioneered in North Carolina by the world's largest pork corporation came under fire Tuesday as jurors began hearing a lawsuit from neighbors who say it is endangering their health and making their lives miserable.
The legal action is the first in a string of federal lawsuits against the hog-production division of Virginia-based Smithfield Foods. Its outcome could alter production methods and profits for the company, which controls everything from the feed the animals eat to when they are trucked to slaughter and how the meat lands on consumers' plates. Smithfield was bought in 2013 by a division of China-based WH Group, the world's largest pork producer.
In dozens of lawsuits, more than 500 neighbors say that for decades they have suffered headaches, sinus problems and intense, putrid smells that can't be removed from clothing or household fabrics.
"Smithfield has never done anything to change," Michael Kaeske, a Dallas, Texas-based lawyer, told jurors. "They're not going to do anything to change unless you make them."
The suit heard Tuesday is the first of several test cases against the company. It pits Smithfield against 10 neighbors of a farm raising 15,000 livestock hogs under contract with the pork giant. Kinlaw Farm and its owners are not defendants. Instead, the suit targets Smithfield, which determines the farm's operating standards.
"Smithfield controls what the growers do and they don't have the money to do anything different," Kaeske said.
Since pork producers began dramatically consolidating in the 1980s, sprawling operations holding thousands of hogs generating a small city's worth of urine and feces each day have become standard in North Carolina. The excrement is flushed into pits where it is broken down and then sprayed through a type of agricultural water cannon as fertilizer onto nearby fields.
DNA tests found a type of bacteria originating in swine digestive systems coating exterior surfaces at the homes of all 10 Bladen County plaintiffs, Kaeske told jurors.
Though less-malodorous technologies have been developed and applied at Smithfield operations elsewhere, North Carolina operations haven't changed, Kaeske said. For example, a lawsuit by Missouri's attorney general resulted in a 1999 settlement forcing a company that Smithfield later took over to cut in half the nitrogen applied to fields, at a cost of tens of millions of dollars.
The Missouri solution included covering open-air waste pits, Kaeske said, yet Smithfield's "lagoons in North Carolina remain uncovered."
"They continue to spray feces and urine near their neighbors."
Smithfield attorney Mark Anderson said agreements elsewhere, where temperatures and lagoon construction are different, shouldn't enter into this lawsuit. Jurors should consider only whether the 10 named plaintiffs have suffered so great a nuisance that they can't enjoy the use of their own property, he said. The farm never faced complaints from neighbors or state regulators until out-of-state lawyers showed up to press their agenda, Anderson said.
"This case is about this land, these neighbors, this farm," said Anderson, who practices in Raleigh and New York City. "Jurors know how to separate an agenda and an attack from the law."
___
Follow Emery P. Dalesio at http://twitter.com/emerydalesio. His work can be found at http://bigstory.ap.org/content/emery-p-dalesio