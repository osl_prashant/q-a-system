U.S. consumers’ hunger for organic produce options doesn’t seem to be slowing down, but production growth on organic onions has been somewhat slower than many other categories. 
Tim Waters, associate professor for Washington State University Extension, Franklin and Benton Counties, said there are myriad reasons slowing growers from increasing their organic onion offering, including weed control.
 
“Weed control in organic onions is an absolute nightmare,” he said. Controlling pests such as onion thrips can be a challenge with limited organic insecticide options and the potential for thrips to destroy large areas of organic crops as well.
 
Despite the hurdles, onion growers continue to increase organic production in Washington, Oregon and across the country. 
 
Dan Borer, general manager for Keystone Fruit Marketing Inc. in Greencastle, Pa., said the company offers organic yellow hybrid onions, which now account for 5% of its production.
 
“We’re growing our organic onion production every year,” he said. “Three years ago, we had no organic onions, then 1% and 5% of our crops. I expect during the next five years, we’ll double production every year.”
 
Borer said across-the-board demand for organics in the fresh retail market has pushed this change with retailers offering more steady supplies of organic onions to customers. 
 
Seattle-based onion distributor F.C. Bloxom Co. has seen demand for its organic onions increase the past two years, said Bryon Magnaghi, head of the company’s office in Walla Walla, Wash.
 
“The demand has increased mostly for organic yellows and reds. Not as many organic whites are grown,” he said.
 
Magnaghi said the company’s organic acreage has been stable the past couple years and so far, seems to meet demand during the season.
 
Shawn Hartley, president of Syracuse, Utah-based Utah Onions Inc., said organics account for 10% of the company’s onion crop and it plans to increase organic acreage every year moving forward.