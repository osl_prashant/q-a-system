Those interested in incorporating unmanned aircraft systems into their business operations and agricultural enterprises are encouraged to attend one of five introductory training sessions offered across the state by Nebraska Extension.
The sessions will be from 8:30 a.m. to 4 p.m. at the following locations:
Mead: Dec. 14, Agricultural Research and Development Center, 1071 County Road G. Register by Monday, Dec. 5.
Grand Island: Dec. 16, Hall County Extension Office, 3180 W. Highway 34. Register by Thursday, Dec. 8.
"Emerging technology such as unmanned aircraft systems have the potential to benefit all types of businesses, and especially applications of the agricultural industry," said Wayne Woldt, associate professor in the University of Nebraska-Lincoln's Department of Biological Systems Engineering and one of the training organizers. Woldt is a rated pilot who developed the Nebraska Unmanned Aircraft Innovation, Research and Education (NU-AIRE) laboratory and flight program, with a focus on research and education in unmanned aircraft systems.
The training will highlight information needed by an unmanned aircraft operator and pilot, including:
hobby flight, educational interpretation and best practices for privacy concerns;
Federal Part 107 rules for commercial flight and piloting; and
an overview of applications.
At the conclusion of the program, training attendees will have flight time with a small unmanned aircraft system.
In addition to Woldt, others conducting the training are Bill Kreuser and Jacob Smith. Kreuser is an assistant professor in the university's Department of Agronomy and Horticulture. He is a rated pilot who works with unmanned aircraft systems as part of his research and extension program to advance the use of this new technology for managing complex agronomic systems. Smith works with Woldt in the NU-AIRE laboratory. He is a commercial pilot, certified flight instructor, and unmanned aircraft systems expert.
Registration
Space is limited to 45 attendees at each session. For more information and to register see the  program brochure or contact Delhay at 402-472-9390 or bdelhay2@unl.edu. The brochure also includes maps to each training site.
The registration fee of $50 may be submitted online at http://nuaire.unl.edu or by mailing a check to Bonita Delhay, Biological Systems Engineering, 234 Chase Hall, University of Nebraska, Lincoln, NE 68583-0726. Checks should be payable to the University of Nebraska.
Continuing education credits (CEUs) have been applied for.