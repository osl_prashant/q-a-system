BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Wednesday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMar                              122.00  Up    .90Mar      120.00  121.05  120.00  121.05  Up   1.10May                              124.10  Up    .80May      120.75  122.40  120.55  122.00  Up    .90Jul      122.80  124.50  122.80  124.10  Up    .80Sep      125.20  126.55  125.05  126.20  Up    .65Dec      128.75  129.85  128.45  129.50  Up    .55Mar      132.30  133.15  131.85  132.80  Up    .45May      134.40  135.00  134.40  134.75  Up    .40Jul      136.90  136.90  136.25  136.50  Up    .35Sep      138.50  138.50  137.90  138.15  Up    .40Dec      140.45  140.65  140.45  140.65  Up    .35Mar                              143.20  Up    .35May                              144.95  Up    .35Jul                              146.65  Up    .35Sep                              148.25  Up    .35Dec                              150.65  Up    .35