By: Timothy Beck, Virginia A. Ishler, David L. Swartz and Robert C. Goodling, Jr., Penn State Extension
We closed the books on 2017 with an annual Class III price of $16.17/cwt. Looking ahead through 2018, the current predicted Class III price for the first 11 months is averaging $14.57/cwt. How does this difficult price outlook compare to the very difficult year of 2009, which most of us in the dairy industry remember well.
The Class III price for 2009 was $11.36/cwt. If you adjust it to current dollars to account for inflation over the past 9 years, the Class III price in 2009 would equate to $13.06/cwt. So, the predicted Class III price for 2018 is 11.5% higher than the Class III price in 2009. However, many of the expenses incurred on dairy farms have increased more than 11% in 9 years, and the majority of premium programs offered to dairy producers in 2009 are no longer available, so milk price basis is lower. Adding all those factors together paints a very difficult picture for 2018.
The only way to face the year with optimism is to be sure dairy producers really understand their cost of production. There are still a great many dairy producers in Pennsylvania that do not know their cost of production/cwt and have done very little to benchmark their costs to industry recommendations. Penn State Extension, the Center for Dairy Excellence and many lenders are skilled in helping producers determine their cost of production.
After a producer knows their numbers, then extension educators, private consultants, and dairy profit teams can work with producers to control costs to meet cost of production goals and examine opportunities to improve income. This is the only way to have optimism for the future.
The other factor that provides some optimism is that corn and soybean prices are the same or lower than in 2009. Basis (transportation, processing, etc.) is higher, but inventories are high, the 2017 harvest numbers set records for bu./acre, so grain prices are predicted to be subdued for the year. At this point, the only upward momentum for grain prices would come from weather patterns that threaten the South American grain crops.
Table 1: 12 month Pennsylvania and U.S. All Milk Income, Feed Cost, Income over Feed Cost ($/milk cow/day)

¹Based on corn, alfalfa hay, and soybean meal equivalents to produce 75 lbs. of milk (Bailey & Ishler, 2007)
²The 3 year average actual IOFC breakeven in Pennsylvania from 2014-2016 was $8.97 ± $1.76 ($/milk cow/day) (Beck, Ishler, Goodling, 2017).
Table 2: 12 month Pennsylvania and U.S. All Milk Price, Feed Cost, Milk Margin ($/cwt for lactating cows)

¹Based on corn, alfalfa hay, and soybean meal equivalents to produce 75 lbs. of milk (Bailey & Ishler, 2007)
²The 3 year average actual Milk Margin breakeven in Pennsylvania from 2014-2016 was $12.41 ± $2.38 ($/cwt) (Beck, Ishler, Goodling, 2017).
Figure 1: 12 month PA Milk Income and Income over Feed Cost

²The 3 year average actual IOFC breakeven in Pennsylvania from 2014-2016 was $8.97 ± $1.76 ($/milk cow/day) (Beck, Ishler, Goodling, 2017).
Figure 2: 24 month Actual and Predicted* Class III, Class IV, and Pennsylvania Average Mailbox Price ($/cwt)

*Predicted values based on Class III and Class IV futures regression (Gould, 2018).
Table 3: 24 month Actual and Predicted* Class III, Class IV, and Pennsylvania Average Mailbox Price ($/cwt)



Month
Class III Price
Class IV Price
Average PA Mailbox Price




Dec-16
$17.40
$14.97
$18.53


Jan-17
$16.77
$16.19
$18.86


Feb-17
$16.88
$15.59
$18.24


Mar-17
$15.81
$14.32
$17.91


Apr-17
$15.22
$14.01
$16.43


May-17
$15.57
$14.49
$16.32


Jun-17
$16.44
$15.89
$17.17


Jul-17
$15.45
$16.60
$17.40


Aug-17
$16.57
$16.61
$18.25


Sep-17
$16.36
$15.86
$17.83


Oct-17
$16.69
$14.85
$18.28


Nov-17
$16.88
$13.99
$17.97


Dec-17
$15.44
$13.51
$16.99


Jan-18
$13.85
$13.30
$16.01


Feb-18
$13.24
$13.17
$15.63


Mar-18
$13.31
$13.33
$15.74


Apr-18
$13.64
$13.66
$15.25


May-18
$14.07
$13.91
$15.60


Jun-18
$14.52
$14.10
$15.92


Jul-18
$15.17
$14.30
$16.38


Aug-18
$15.42
$14.47
$16.59


Sep-18
$15.58
$14.75
$16.80


Oct-18
$15.67
$14.97
$17.80


Nov-18
$15.59
$15.12
$17.83



*Italicized predicted values based on Class III and Class IV futures regression (Gould, 2018).
To look at feed costs and estimated income over feed costs at varying production levels by zip code, check out the Penn State Extension Dairy Teams DairyCents or DairyCents Pro apps today.
Data sources for price data:

All Milk Price: Pennsylvania and U.S. All Milk Price (USDA, 2018)
Predicted Class III, Class IV, and Pennsylvania Mailbox Price (average of the Eastern and Western PA mailbox Price) (Gould, 2018)
Alfalfa Hay: Pennsylvania and U.S. monthly Alfalfa Hay Price (USDA, 2018)
Corn Grain: Pennsylvania and U.S. monthly Corn Grain Price (USDA, 2018)
Soybean Meal: Feed Price List (Ishler, 2018) and average of Decatur, Illinois Rail and Truck Soybean Meal, High Protein prices, National Feedstuffs (Gould, 2018).

References:

Bailey, K. and V. Ishler. “ Dairy Risk-Management Education: Tracking Milk Prices and Feed Costs”. Penn State Extension. Accessed 9/20/2017.
Beck, T.J., Ishler, V.A., & Goodling, R. C. 2017. “Dairy Enterprise Crops to Cow to Cash Project,” the Pennsylvania State University. Unpublished raw data.
Dairy Records Management Systems. “DairyMetrics Online Data Report system”. Accessed 9/14/2017.
Gould, B. 2018. “Predicted Mailbox Prices (Eastern Pennsylvania”). Understanding Dairy Markets website. University of Wisconsin-Madison. Accessed 1/5/2018.
Gould, B. 2018. “National Feedstuffs: Soybean Meal, High Protein”. Summary of USDA AMS Grain Reports. Accessed 1/5/2018.
Ishler, V. “ DairyCents Mobile App”. Penn State Extension. #App-1010.
Ishler, V. “ DairyCents Pro Mobile App”. Penn State Extension. #App-1009.
Ishler, V. “Feed Price List”. Personal Communication. Accessed 1/5/2018.
Microsoft 2016. “Forecast.ets function”, Office Help Website.