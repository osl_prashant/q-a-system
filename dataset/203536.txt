Think of cranberries as a part of a whole, and cross-promotional ideas readily roll in, marketers say.
From traditional homemade cranberry sauce and breads to stuffing, turkey glazes, and side dishes, the cranberry is a central ingredient, and that facilitates cross-merchandising, said Kellyanne Dignan, senior manager of corporate communications with Lakeville-Middleboro, Mass.-based Ocean Spray Cranberries.
"From a recipes standpoint, we're able to promote the wide variety of uses for fresh cranberries, from holiday side dishes to desserts ... where we showcase not only recipes but also the use of fresh cranberries in holiday decor, from cranberry garland to use in floral arrangements and centerpieces," Dignan said.
Ocean Spray also has cross-promoted with other brands in the produce section with boxed cranberry bread and side dish mixes, she said.
"Couponing and in-store promotions are always a great way to give back to loyal consumers throughout the season," Dignan said.
The holiday season creates a melange of culinary ideas, and cranberry cross-promotions go hand-in-hand with them. However, those ideas shouldn't stop when the last turkey dinner is wrapped up, said Mary Brown, owner of Glacial Lake Cranberries and Honestly Cranberry in Wisconsin Rapids, Wis.
"Look at how many people make their own trail mix - you can cross-promote with any or all of those ingredients," she said. "People have to just be willing to look and see differently, which is hard for people."
Retailers can do the same, said Brian Wick, executive director of the Cape Cod Cranberry Growers Association in Carver, Mass.
"I do know from a presentation earlier this month, they were talking about some work with blueberries and cranberries and almonds," he said.
Promotional work with other commodities always is ongoing, as indicated by the Wareham, Mass.-based Cranberry Marketing Committee's work with USA Pears at the School Nutrition Association's annual national conference in July, said Michelle Hogan, the committee's executive director.
"Cranberries are such a versatile fruit that pairs well with so many other foods," she said. "We love taking advantage of these properties and teaming up with other commodities for cross-promotions."
At the conference, representatives from the Cranberry Marketing Committee cross-promoted recipes and held a recipe contest, the winner of which was due to be announced soon, Hogan said.
She said the committee also did promotional work in June with Washington apples, California walnuts and U.S. pecans as part of a U.S. Premium Ag Products promotion at Food Hospitality World in Bengaluru, India.
"I've heard some interesting ideas kicked around," said Bob Wilson, managing member of Wisconsin Rapids, Wis.-based The Cranberry Network LLC, which markets fruit grown by Tomah, Wis.-based Habelman Bros. Co.
One of those ideas involves cranberries cross-merchandised with kale for smoothies.
"I've heard some interesting conversations across those lines," he said. "If you have fruit people typically buy fresh and do in smoothie blends, couple it up."
The fresh cranberry category has seen increase demand for use in home decor, especially during the holiday season, and that fits into cross-merchandising, said Brian Bocock, vice president of product management with Salinas, Calif.-based Naturipe Farms LLC.
"Cranberries are being used for seasonal wreaths, vase fillers and even jewelry. In turn, this gives opportunity to cross-promote with categories like floral and home decor," Bocock said.
Cranberries have plenty of cross-promotional potential, and it's largely unexplored marketing territory, said Bob Von Rohr, marketing and customer relations manager for Sunny Valley International Inc., a Glassboro, N.J.-based shipper.
That's largely because of the nature of the fruit, Von Rohr said.
"You can't just eat a cranberry by itself," he said.