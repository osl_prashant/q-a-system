(Photo courtesy Wal-Mart Stores Inc.)

Wal-Mart e-commerce sales are up 50% from the third quarter in 2016, helping boost overall sales 4.2% to $123.2 billion.
That significant jump in e-commerce followed a 60% year-over-year increase in that category for the second quarter.
Most of the latest growth has come through Walmart.com rather than other partners, CEO Doug McMillon said in an earnings call Nov. 16.
“Existing customers have become advocates for popular initiatives like online grocery and free two-day shipping, and as a result, new customers, suppliers and partnerships are coming to Wal-Mart,” McMillon said. “The expanded assortment on Walmart.com has also contributed to growth. Over the past year, we’ve tripled the number of items on Walmart.com to reach more than 70 million (stock-keeping units) today.”
The company offers online grocery at more than 1,100 stores currently and plans to expand it to another 1,000 locations next year.
McMillon also noted that grocery overall has been strong for Wal-Mart.
“The food business in particular has accelerated and delivered the strongest quarterly comp sales performance in almost six years with our fresh meat, bakery and produce teams leading the way,” McMillon said.