If Emiliano Escobedo, executive director of the Hass Avocado Board, Mission Viejo, Calif., has anything to say about it, U.S. consumption of hass avocados will double by 2021.The industry’s goal is to see avocado per capita consumption rise from the current 7 pounds to 14 pounds in four years.
“The U.S. market is now the No. 1 consumer market in the world,” he said. “There is no other market that consumes that many avocados.”
The vision of HAB is to be the catalyst to make fresh avocados the No. 1 consumed fruit in the U.S.
The board has developed some programs to help achieve that goal.
Love One from the Start shows how avocados can help expectant moms and babies.
“We had a Mother’s Day bilingual radio tour in May featuring registered dietitians and HAB spokespeople,” Escobedo said.
The board works with spokeswoman Sylvia Melendez-Klinger, a registered dietitian, personal trainer and Founder of Hispanic Food Communications to share ideas about how to incorporate avocados into the early life of a child.
“One out of four babies born in the U.S. today is Hispanic,” he said, “so the bilingual component was critical.”
HAB will observe Hispanic Heritage Month — Sept. 15 to Oct. 15 — and promote the heart health benefits of avocados.
“Last year, the Food and Drug Administration ruled that avocados can now be promoted as heart healthy,” he said.
HAB has a strategic relationship with the American Heart Association and supports the association’s Healthy for Good movement, which promotes consumption of fresh fruits and vegetables to help reduce the risk of cardiovascular disease and stroke.
Finally, HAB has a Be Fruitful campaign planned for fall.
“It’s about elevating consumer awareness about the connection between avocados and weight management,” he said.
All of the board’s campaigns are supported by print, social media and banner advertising aimed at health professionals as well as consumers.