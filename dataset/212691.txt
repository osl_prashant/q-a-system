With sales of organic fruits and vegetables continuing to grow rapidly in the U.S., it’s not surprising that pepper suppliers are adding or increasing organic offerings.Leamington, Ontario-based NatureFresh Farms increased its organic bell pepper acreage by 100% last year to meet the increased demand, said Chris Veillon, director of marketing.
“We see the organic category continuing to grow,” Veillon said. “The demand for our organic peppers continues to be very strong.”
NatureFresh grows organic red, yellow, orange and green bell peppers across 9 acres in Leamington, he said.
“Organic greenhouse growing was only bound to happen, thanks to the more consistent climate making yields significantly higher,” said Sarah Pau, director of marketing for Pure Hothouse Foods Inc., Leamington, which markets the Pure Flavor brand.
“Pure Flavor has focused on increasing and strengthening our organic program and is now growing many organic crops,” she said, including sweet bell peppers.
The company plans to expand its organic offerings in 2018.
Pasha Marketing LLC, Mecca, Calif., a division of Richard Bagdasarian Inc., has offered organic peppers for many years, said Franz De Klotz, vice president of marketing.
“We just continue to grow the amount of acres to satisfy our current customer base,” he said, adding that organic acreage continues to increase.
The Oppenheimer Group, Vancouver, British Columbia, will finish its organic program with Divemex in Mexico in late May, said Aaron Quon, Oppy’s executive category director, greenhouse and vegetables.
“Our winter organic pepper season from Divemex was excellent on both quality and demand,” he said.
During the summer season, Oppy will have organic greenhouse peppers from Origin Organics Farms Inc. in British Columbia packed in the popular OriginO label.
Oppy cites FreshLook marketing data showing that organics now comprise about 4% of all greenhouse pepper sales, “indicating an opportunity for peppers to capitalize on the general increase in demand for high-quality organics.”
“Looking ahead, we anticipate greater volumes of organic peppers from Divemex when their season begins in October,” Quon said.
For the first time this year, Prime Time International, Coachella, Calif., will grow organic red and green bell peppers in all three of its California growing areas — Coachella, Arvin and the Oxnard area.
“We’ve been pushed in that direction by our customers,” said Mike Aiton, director of marketing.
The green pepper harvest started in Coachella in late April, and reds will begin in mid-May.
The company next moves to Arvin until August and will harvest out of the Oxnard area until Thanksgiving.
In the past, the company only grew organic peppers in Coachella.
Prime Time has six or seven months to “turn it around and make it a viable crop for us,” Aiton said.
Company representatives talked with customers before launching the organic program to gauge their interest.
“We’re optimistic,” Aiton said.
Producing organic peppers is costlier than growing conventional ones because yields are less, and more hand labor is required, he said.