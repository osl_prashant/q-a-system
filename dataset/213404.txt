Thursday Climate Corporation announced a global data connectivity agreement with AGCO. This provides an addition equipment connection option with the Climate FieldView platform.
The Monsanto subsidiary says this agreement paves the way for “cloud-to-cloud” data transfer to allow farmers to easily send data to AGCO for execution on AGCO equimpment.
Climate and AGCO offer three ways to connect:

Real-time data connectivity between the AGCO Connectivity Module and farmers’ Climate FieldView accounts
Expands compatibility across all lines of AGCO equipment for Climate Corporation’s Climate FieldView Drive device
Continues to enable Climate FieldView data connectivity with Precision Planting’s 20/20 monitors

“Through this new agreement with AGCO, more farmers around the world will be able to seamlessly get all their data in one place and maximize the value of that data with the Climate FieldView platform’s advanced analytics-based insights to make management decisions for their operations with confidence,” says Mike Stern, CEO at Climate Corporation.