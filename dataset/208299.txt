Chicago Mercantile Exchange live cattle closed higher on Thursday after investors bought October futures and simultaneously sold deferred months in a trading strategy known as bull spreads, traders said.Some market participants, also through spreads, purchased live cattle futures and at the same time sold CME lean hog contracts.
There are bullish signals that market-ready, or cash, cattle may be close to bottoming out, while cash hog prices have tracked lower, said traders.
"Rather than go flat out long cattle, which is a little riskier, (funds) were long cattle and short hogs," said CHS Hedging analyst Steve Wagner.
On Thursday packer bids for cash cattle in Nebraska and Texas were $101 and $102 per cwt, respectively, versus at least $107 asking prices, said feedlot sources. Last week cash cattle in the U.S. Plains brought $103 to $105.
Extremely profitable packer margins and improved wholesale beef values bode well for cash prices.
But processors will resist paying more for supplies given ample numbers of cattle that have become heavier as temperatures moderate in the Plains, allowing animals to grow quicker.
The U.S. Department of Agriculture's monthly meat export data showed July U.S. beef exports at 239.4 million pounds, up slightly from June and 10 percent higher than a year ago.
Some live cattle trading months broke through technical resistance levels, which triggered fund buying.
October live cattle finished 1.525 cents per pound higher at 106.225 cents, and above the 10-day moving average of 106.025 cents. December closed 1.325 cents higher at 111.125 cents.
Weaker corn prices and higher live cattle futures boosted CME feeder cattle contracts.
September closed 2.250 cents per pound higher at 145.550 cents. 
Hog Futures Slide
Slumping cash hog prices and softer wholesale pork values amid abundant supplies of heavier animals pressured CME lean hog futures, said traders.
Fund liquidation, spreading out of hogs into cattle and disappointing U.S. pork export data contributed to hog futures' losses, they said.
October ended 2.100 cents per pound lower at 61.450 cents, and December finished 2.000 cents lower at 58.275 cents.
Both contracts settled below their respective 10-day moving average of 62.165 and 58.392 cents.
USDA put July pork exports at 390.3 million pounds, down 13.0 percent from a month earlier and down 3.8 percent from a year ago.