Farm fire kills 14,000 young chickens in Mount Vernon
Farm fire kills 14,000 young chickens in Mount Vernon

The Associated Press

MOUNT VERNON, Wash.




MOUNT VERNON, Wash. (AP) — Officials say a fire at a Mount Vernon farm killed about 14,000 chickens that were about two weeks old.
The Skagit Valley Herald reports that Draper Valley Farms spokesman Bill See said the fire started in one of the farm's chicken houses.
Mount Vernon Fire Chief Bryan Brice says firefighters were called Monday evening to the blaze. He says it took about 45 minutes to get the fire under control because of difficulty accessing the structure.
He says firefighters had to carry fire hose along a muddy access road from the nearest fire hydrants, which slowed them down.
Burlington Fire Marshal Kelly Blaine is leading the investigation into the fire's cause.
___
Information from: Skagit Valley Herald, http://www.skagitvalleyherald.com