Nixa, Mo.-based Market Fresh Produce has hired Dan Thompson as senior director of operations and Mike Combs as national director of packaging supply.
 
Thompson  will be based at the Market Fresh Tampa, Fla., facility and Combs will be based at the Nixa, Mo., headquarters.
 
Thompson joins Market Fresh with nearly 20 years of experience. He was the vice president of operations at Tintometer Inc., according to a news release.
 
As the senior director of operations, Thompson's managing role includes production, shipping, receiving, delivery, sanitation, inventory control standards and compliance for safe work practice.
 
Combs joins Market Fresh with more than 25 years of sales and warehouse operations experience. Previously, he was sales manager at Veritiv Corp., according to the release.
 
Combs will be responsible for packaging development, order replenishment and facilitating efficient, cost-saving operations.