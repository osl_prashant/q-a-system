Overall meat demand continues to uphold breakout prices for pork packers and finishers.In the recent mid-year report from Sterling Marketing, Inc., pork packers continued to show an increasing net margin to $24.50 per head for July 2017.
Domestic and international demand keep both June and July lean carcass prices remained in the mid-80s, boosting margins for all levels of production. Sterling Marketing projects a pull back will occur later this year, likely September, as additional supplies come online before the holiday buying season.
Click here to see pork packer margins.
Farrow to finish producers saw similar strong margins for hogs marketed in July. Market hog live prices averaged $87.74 cwt, with an impressive $61.75 margin.
For the remainder of 2017, margins for farrow-to-finish producers are estimated to fall seasonally but remain black through 2018.
Click here to see farrow-to-finish producer margins.