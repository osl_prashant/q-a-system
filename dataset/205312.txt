In the event of a crisis, does your organization have an Emergency Action Plan? If so, is it actionable?Discover what it takes to create a safety-conscious culture by attending the Agricultural Retailers Association Crisis Management Workshop. This program is specifically designed for ag retail professionals charged with promoting organizational environmental health and safety.

Create An Action Plan
The workshop will assist you in creating or refining an Emergency Action Plan that is relevant, implementable, and sustainable for the needs of your business and local emergency responders.
Sponsored by FMC, this day and a half workshop will be held Feb. 27 to March 1 at the Tropicana Hotel in Las Vegas.
Led by environment health and safety expert Laura Casey and Dr. Jaye L. Hamby, a leader in agricultural education, the day and a half program features industry best practices, interaction scenarios, and a customizable Emergency Action Plan template.
The content of the workshop will revolve around three fundamental principles of crisis management: plan preparation (PREPARE), actions and activities required to manage a crisis (RESPOND), and requirements to keep a crisis plan sustainable (SUSTAIN).
The program will help attendees evaluate their organization's readiness, assess vulnerabilities, develop or refine a crisis plan, as well as provide opportunities to learn best practices and collaborate with peers.
Registration for members is $550. The non-member registration fee is $850. Space is limited to maintain small work groups.
ARA will book hotel reservations for each registered participant at the Tropicana. Payment for the hotel is the responsibility of the attendee. The room rate is $115 per night. A credit card will be required upon arrival.
It's recommended attendees fly into McCarran International Airport (LAS), which is a short cab ride from the Tropicana Hotel property.
Regisrations is available online at: www.aradc.org/trainingconferences/crisismgmt.