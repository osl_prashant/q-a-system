Fresh-cut companies breaking out into the trend of microwaveable vegetable packs have a new option from Temkin International Inc.
The flexible film packaging company has new steamable packaging called u-Vent.
Benefits of the pouch, according to a news release, include:

Leak-free seal during storage;
Microwaveable without piercing the package first;
Can be micro-perforated for modified atmosphere control and microwave vent;
Can be printed on; and
Allows steam to build up without bursting.

Temkin has facilities at its Payson, Utah, headquarters, and in Miami, Burlington, Ontario, and Bogota, Colombia. For more information, e-mail cellane@temkininternational.com or go to http://www.temkinfresh.com/.