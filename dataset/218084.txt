Calving season has started for some and will begin soon for others. Meanwhile, beef producers are preparing themselves for less sleep each night, as time spent observing cows day and night will soon start. Nearly 90% of beef producers regularly observe cows and heifers during calving (NAHMS, 2008), and research shows doing calving checks every 3 hours yields best results. Yet, the best calving check regimen is not always practical or a guaranteed way to eliminate calving problems or dystocia from occurring. It is estimated 11% of heifers and 4% of cows need some assistance during calving each year (NAHMS, 2008). Therefore, what can we do to prevent calving difficulty before it occurs so calving season will be a little less stressful for all parties involved?
Don’t Select for Dystocia
In the hours leading up to calving, there is often little anyone can do to prevent dystocia from occurring. However, looking back on your management decisions from the past the breeding season and fall, may give one insight into how calving will go. Here are some example areas to evaluate before calving starts:

Calving Ease Direct EPD (CED)
	Utilize calving ease bulls when mating to heifers, young or small cows that may have trouble calving. CED measures percent of unassisted births and takes into account the size and presentation of the fetus. Be careful not to over select for CED in mature cows as valuable calf weight will be given up and sacrifice potential revenue.
Pelvic Measure
	Measuring pelvic area in replacement or bred heifers before their first calving event will give producers an idea of which females may have trouble due to abnormal pelvis shape or size. The pelvis continues to grow and shape until cows are mature, so keep in mind the pelvis can change between first and third calving events.
Body Condition Score
	Pay attention to heifers and cows that are carrying extreme amounts of fat or are very thin. Over conditioned heifers are more likely to have troubles calving due to accumulation of fat in the pelvis hindering fetal passage through the birth canal. In addition, if heifers are too skinny their endurance during calving may be limited by the absence of energy resources. At any time, if a female stops making progress during calving for more than 15 minutes, assistance should be offered. Separate cows based on condition so not to overfeed fat cows and underfeed thin ones.
Pregnancy Detection
	If you had the ability to know when a cow would have twins, would you do it? Most of us say yes, but only 20% of beef operations palpate or ultrasound for pregnancy. Modern technologies are available and should be utilized to confirm pregnancy in cows and identify twins or large calves so producers can pay more attention to certain cows as calving nears.
Provide Exercise
	Cows need to be in shape come calving season, so providing adequate exercise prior to calving is important to their physical strength. Feeding cows in a loafing pasture and providing water a ¼ mile away is a good distance for cows to travel daily. If keeping cows in a lot during gestation, less energy will be used every day as they don’t have to walk so far. Design rations to meet requirements of cows based on location during gestation. Lastly, avoid moving cows close to calving as fetal growth is occurring very rapidly and calf position can flip if cows slip or fall.

Know the Signs
Not all dystocia is preventable even if you follow the practices listed above. However, if you observe cows struggling during labor, keep a close watch on her to observe her through the stages of labor and intervene when necessary (Table 1).
In Summary
Ultimately dystocia is not a fun event for anyone involved. Producers can do their best to estimate the size of the fetus, presentation in the birth canal and pelvic area in each specific cow, but only time will tell if cows will calve unassisted or need help. Take steps to understand the stages of labor and when assistance is needed to successfully navigate through a dystocia. We wish everyone the best going into calving season 2018!




Table 1. Stage of labor.


 
External Signs
Internal Events
Length
When to Intervene


Stage 1
Restless, nesting, stops eating and drinking, vaginal discharge observed
Cervix begins to dilate, Uterine contractions strengthen (1 /15 min. to ? 1/3 min.)
2 – 8 hours, potentially longer in heifers
>8 h, check for stillborn calves


Stage 2
Abdominal straining 1 – 3x/min., water bag appears, fetal limbs appear, calf is delivered
Cervix fully dilated, progress from head to chest and lastly hips, allow calf to take first breath
½ - 4 hours
Water bag has been visible for 2 hours and not trying, no progress >30 min, progress stops for >15 min


Stage 3
Passes placenta
Connection between placenta and uterus deteriorate
Completed 12 h post delivery
12 -24 h, check for retained placenta