Leafy green grower-shipper WP Rawl partnered with Jankowski Farms and the Young Professional Group of Flagler County, Fla., to host a farm-to-table dinner that raised more than $15,000.
The fundraiser, in its second year, benefits youth programs in Flagler County.
The Oct. 4 event was held at the WP Rawl’s Bunnell, Fla., facility that opened in 2012.
Local chefs prepared a five-course meal with fresh produce and proteins from local area farms.
“Supporting the communities from which we are a part of is very important to both our company and family,” said Charles Wingard, vice president of field operations for the Pelion, S.C.-based Rawl, in a news release. 
“Our grandparents and founders of our company instilled in us a deep respect for family traditions and a responsibility to provide leadership to the industry and community.”