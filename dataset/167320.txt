Two of the seven states in our weekly fertilizer survey softened this week including UAN32% and urea.
Our Nutrient Composite Index firmed 0.72 points on the week to 536.83 led by strength in anhydrous ammonia.

Nitrogen

Anhydrous ammonia once again led gains in the nitrogen segment this week although UAN28% was higher as well.
Urea slipped 63 cents to lead declines in the nitrogen segment as UAN32% fell 28 cents per short ton.
In this week's price action, UAN32% and 28% are now both priced below anhydrous ammonia on an indexed basis. Urea remains our highest priced form of N.
Nitrogen across the segment is priced well below expected new-crop corn revenue indicating a value buy.

Phosphate

Both DAP and MAP were higher although gains in MAP outpaced gains in DAP by 6 to 1.
MAP continues to work on unwinding the DAP/MAP spread although is currently at risk of overdoing it to the upside.
As the DAP/MAP spread widens, the risk for a wide spread to lead to a sharp acceleration of the current uptrend grows.

Potash 

Potash firmed moderately, adding $1.87 per short ton.
The largest gain was observed in North Dakota. Since current maps show limited snowcover over most of North Dakota alongside more seasonal temperatures, demand may have perked up last week in that state.
We still expect world oversupply to limit potash price risk to demand-based price discovery.

Corn Futures 

December 2017 corn futures closed Friday, April 28 at $3.85 putting expected new-crop revenue (eNCR) at $609.84 per acre -- higher $5.09 per acre on the week.
With our Nutrient Composite Index (NCI) at 536.83 this week, the eNCR/NCI spread narrowed 4.37 points and now stands at -73.01. This means one acre of expected new-crop revenue is priced at a 73.01 premium to our Nutrient Composite Index.





Fertilizer


4/17/17


4/24/17


Week-over Change


Current Week

Fertilizer



Anhydrous


$522.87


$523.49


+$6.21


$529.70

Anhydrous



DAP


$449.36


$447.28


+29 cents


$447.57

DAP



MAP


$467.76


$467.89


+$1.91


$469.79

MAP



Potash


$333.40


$333.87


+$1.87


$335.74

Potash



UAN28


$248.15


$248.23


+$4.12


$252.35

UAN28



UAN32


$279.10


$277.60


-28 cents


$277.31

UAN32



Urea


$360.62


$359.83


-63 cents


$359.20

Urea



Composite


536.82


536.11


0.72


536.83

Composite