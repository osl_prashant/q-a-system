Fagerberg builds worker housing
Eaton, Colo.-based Fagerberg Produce Inc. opened new housing it built for its H-2A workers this year, said Alan Kinoshita, sales manager.
The new building, which has a capacity for 20 workers, helped to secure enough workers, which had been a problem in the past, Kinoshita said.
 
MountainKing adds microwave pack
Monte Vista, Colo.-based Mountain King Potato of Monte Vista now offers a microwavable 1-pound potato pack of baby reds or baby golds, said Andreas Trettin, marketing director.
“We’re thinking about a medley or fingerling, as well,” he said.
The new product will be available by late November or early December, Trettin said.
 
Pleasant Valley Potato boosts fingerlings
Aberdeen, Idaho-based Pleasant Valley Potato has expanded its fingerling potato volume, said Ryan Wahlen, sales manager.
“We increased our acreage so we would have more to offer,” he said. “We added probably 40% relative to last year.”
The company now has 20 acres of fingerlings, Wahlen said. Demand warranted the additional acreage, he said.
 
The Onion House doubles packaging capacity
Weslaco, Texas-based The Onion House has doubled its packaging capabilities, said Don Ed Holmes, owner.
“We’ve noticed over the last two or three years that labor just gets higher and higher, so we’re actually going to more of a mechanized 3-pound operation,” he said.
“There’s more people asking for wineglass labels, and we’re upgrading our equipment to pack wineglass labels at a really rapid rate.”
The new setup, which will be fully operational in January, when The Onion House starts its Mexico deal, will have a capacity to pack six to eight loads per day, compared to a current three or four, Holmes said.
 
Weng Farms Inc. adds imports
Willard, Ohio-based Weng Farms Inc. is evolving a bit, said William Foster, salesman.
“Weng Farms is going to be not so much a day-to-day domestic onions company. It’s going to specialize in imports from other countries in onions, garlic and ginger,” Foster said.
“We feel we’re going to be a bigger importer than a domestic part of this. We’re set in (ports in) Philadelphia, Savannah (Ga.), an office in Ohio, and we feel the import is going to be more and more a bigger thing all the time.”