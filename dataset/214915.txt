Avocado grower-shippers say mesh bags have become a favorite among consumers looking for convenience. (Photo courtesy Robinson Fresh)

Bagged avocados continue to be a consumer favorite, and most major grower-shippers say they’re stepping up production.
At the same time, reusable plastic containers have a presence in the avocado industry, though not an overwhelming one.
The bagged avocado program at Del Rey Avocado Co. Inc., Fallbrook, Calif., just keeps growing and growing, said partner Bob Lucy.
“I’ve been amazed by it,” he said.
“It seems like every retailer, whether a big-box retailer or conventional store, has one or two sizes of avocado, and they’ll also have a bagged avocado of some configuration,” he said.
Calavo Growers Inc., Santa Paula, Calif., packs a “tremendous amount” of bags, said Rob Wedin, vice president of sales and marketing.
The company ships 2 million to 3 million bags per month, he said.
Calavo has 12 conventional bagging machines in California, New Jersey, Texas, Florida and in Jalisco, Mexico, he said.
Like at least one other grower-shipper, the company has tested a pouch bag for avocados but is not pushing it aggressively.
“We’re not jumping into it too quickly,” Wedin said.
“We’ve got quite a commitment to the bag that’s been in the market for quite a while,” he said. “It’s pretty efficient.”
Production of the pouch bag is at about half the speed of the current bag, he said.
Calavo can pack bags to order with any avocado size or count to accomodate retailer requests, Wedin said.
Giumarra Agricom International, Escondido, Calif., was the first company to include expanded nutrition information on its avocado bags a couple of years ago, and the program has grown every year, said Gary Caloroso, regional business development director for parent company The Giumarra Cos., Los Angeles.
“It represents a good chunk of our business,” he said.
Perhaps surprisingly, retailers have experienced incremental sales for bags, he said.
“They have not taken away from their bulk sales.”
Occasionally, bags are used for pre-ripened avocados, but they’re usually packed with green, hard avocados, he said.
Index Fresh Inc., Riverside, Calif., started using bags for its avocados in the mid-1990s and just introduced a new version, said president Dana Thomas.
“It’s been a long-term product line for us,” he said.
When the program started, shipping five pallets a week of hand-packed bags “was a big deal,” he said.
Now the company has bagging machines in California and Mexico and sources bagged fruit from Peru and Chile.
Most recently, Index Fresh introduced a pouch bag to complement its standard net bag.
It has a different look graphically and a different feel, Thomas said, but he doesn’t see it replacing the existing bag.
“We see it as a niche market add-on,” he said.
Meanwhile, use of RPCs to ship bulk or bagged product seems to vary by shipper.
Index Fresh packs a lot of RPCS, Thomas said.
“It’s something that we do for a wide variety of customers,” he said.
The company offers a 60- by 40-centimeter configuration in single and double layers, with and without trays, depending on a customer’s needs, he said.
The firm started packing RPCs several years ago and has incorporated them into its packing process as it upgrades packing machinery.
“For us, it’s a normal packing style,” Thomas said.
Del Rey Avocado packs RPCs on request but was only packing them for one customer this summer, Lucy said.
“It is a challenge to do the RPC,” he said, and the company has tended to shy away from them.
Lucy said he makes an exception when a large customer requests them.
Giumarra does some RPCs, Caloroso said, and business remains steady.
“It’s just part of our business,” he said. “It’s not a big deal for us.”