Waiting regulatory and shareholder approval for Amazon’s acquisition of Whole Foods Market, the Austin, Texas-based grocery reported record sales for the quarter ending July 2. 
For the quarter, total sales for Whole Foods increased 0.6% to a record $3.7 billion, according to a news release. Comparable store sales decreased 1.9%, according to the release. Net income was $106 million, or 2.8% of sales, according to the release. 
 
For the 40-week period ended July 2, total sales increased 1.3% to $12.4 billion, and comparable store sales decreased 2.4%.
 
“For the quarter, we delivered record sales and free cash flow, and returned $44 million in dividends to our shareholders,” John Mackey, co-founder and chief executive officer of Whole Foods Market, said in the release. “Our comparable store sales improved sequentially on a one- and two-year basis in the third quarter, and that momentum has accelerated 220 basis points in the fourth quarter, resulting in positive overall comps for the first three weeks.”
 
The release said the June 16 merger agreement between Amazon and Whole Foods Markets Inc. will mean that Whole Foods won’t update its outlook for fiscal year 2017 or longer-term targets. The parties expect to close the transaction during the second half of 2017, according to the release.