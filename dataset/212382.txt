Los Angeles, Calif.-based Hurst International says its Versaprint labeling system is well-positioned for compliance with future government and retail regulations. 
Introduced nine years ago, the Print on Demand Versaprint system will help the produce industry as government regulations mandate strict labeling requirements, according to a news release.
 
The system can already print lot numbers and grower codes for item-level traceability. 
 
While some customers use this feature for internal control and management, packers may soon be required to add this information due to government and retail regulations, Ari Lichtenberg, president and CEO of Hurst International, said in the release.
 
The Versaprint Thermal Printing technology allows packers to label up to 63 different graphics at speeds up to 15 fruits per second with the highest effective application rate in the industry, according to the release.
 
“Hurst installed the first Print on Demand system in 2008,” Lichtenberg said in the release. “That system is still in operation today, which is a testament to its efficiency and quality of design.” 
 
Hurst has 66 Print on Demand systems in operation for marketers in the citrus, apple, pear, stone fruit, avocado, pomegranate and kiwi industries, Lichtenberg said. 
 
“We have the Hurst Print on Demand System and it’s effective, reliable and easy to operate,” Jake Nixon, vice president of distribution, engineering and process improvement at Mission Produce, said in the release. “The high application rates of over 95% make clean-up easy, which reduces my production costs. Hurst’s fast, dependable professional service and customer support propel them as the technology leaders in the industry.”