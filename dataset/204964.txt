Herbicide resistant weeds are a growing problem for row crop farmers. Joyce Tredaway Ducar, Ph.D., an Alabama Extension weed scientist, said scouting fields is critical.
"Scouting is the only way to know which weeds are present, and their patterns in the field can help to understand why they are present," said Tredaway Ducar.
Adding scouting to your crop and weed management plan can help to maximize your crop yield and profitability.
She explained that herbicide resistant weeds are the results of a naturally occurring process and can be inherited.
"Herbicide-resistant individuals occur naturally in low numbers. These individuals have a herbicide resistance mechanism that allows them to survive the application of a herbicide," said Tredaway Ducar. "This resistance can be passed from one generation to the next."
That means a herbicide resistant plant will mature and produce seed. Every year, the number of resistant plants will increase.
If weeds are present after an application of a herbicide, Tredaway Ducar said farmers should determine the reason before automatically assuming that it is herbicide resistance.
"Investigate and rule out all other factors affecting the herbicide performance before suspecting herbicide resistance. Look into and rule out the field's history, the biology of the weed, the environment, application problems and crop cultural practices before considering herbicide resistance."
Two methods can be used to confirm herbicide resistance; field and greenhouse. Field confirmation is the easiest for producers to use.

Flag a minimum of 5, but preferably 20, of the healthiest weeds that survived the first herbicide application.
Find an area outside of the field that was not previously sprayed and contains the same species. Flag the same number of weeds.
Spray the weeds in both areas with the same herbicide in two strips - one labeled "rate" and another labeled "twice the labeled rate" (note that crops sprayed with greater than labeled herbicide rates must not be harvested for grain or animal use.)
Evaluate all plants 10 to 14 days after herbicide application

If resistance is suspected, farmers should contact their regional Extension crops agent or Tredaway Ducar so that seed can be collected to confirm resistance. Also shesaid seed should be collected at maturity.

"Seed should be falling off the seedhead when it is collected. It should be hard, like you would expect crop seed to be," she said. "Seed that is soft cannot to be used because it is not mature and won't germinate."
Tredaway Ducar and her colleagues are trying to determine what resistant weeds are present in Alabama. They have confirmed several resistant weeds including glyphosate-resistant horseweed, Palmer amaranth, common ragweed and ryegrass.
Shesaid that the best management practice to deal with herbicide resistant weeds is diverse control measures.

"The best strategies to manage herbicide resistance in weeds are established on the concept of diversity. That can be achieved by using mechanical (tillage), cultural (crop rotation), and biological practices in addition to herbicides and by applying several herbicides with different mechanisms of action and overlapping control."
Tredaway Ducarrecommends being proactive in dealing with weeds

"Weed management decisions based on proactive management can be more cost-effective over time compared to programs based on reactive management."
Strategies to delay herbicide resistance include:

Use of multiple herbicides with different mechanisms of actions
Tillage
Cultural tactics such as crop rotation, plant population and row spacing.

She said implementing management practices early can reduce the risks of yield loss.
"As a herbicide-resistant weed population increases in density and area, yield potential decreases. With a greater than 50 percent infestation, there can be a 50 to 90 percent yield loss."