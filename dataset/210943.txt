Milk production for 2018 is forecast higher on stronger milk prices and moderate feed prices.Commercial exports on fat and skim-solids bases are forecast higher on stronger global demand.
Fat basis imports are forecast modestly higher in 2018 while skim-solids basis imports are forecast lower relative to 2017.
Cheese and non-fat dry milk prices are forecast higher than 2017, but butter and whey prices are forecast lower.
The increase in the Class III price reflects higher forecast cheese prices which more than offset lower whey prices.
The Class IV price is higher as the higher non-fat dry milk price more than offset lower butter prices. The all milk price is forecast at $17.55 to $18.55 per cwt for 2018.
Forecast milk production in 2017 is lowered from the previous month on slower growth in milk per cow.
Fat basis imports are lowered from the previous month while skim-solids basis imports are increased.
Commercial exports are forecast higher for both fat and skim-solids bases.
Non-fat dry milk price forecasts are raised from last month while butter is lower. Cheese and whey prices are unchanged.
The Class III price is unchanged while the Class IV is higher than the previous month. The milk price is forecast at $17.35 to $17.85 per cwt.