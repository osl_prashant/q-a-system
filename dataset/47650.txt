DAP, MAP and potash were all higher again on the week.

DAP $24.65 below year-ago pricing -- higher $2.28/st on the week to $458.89/st.
MAP $36.61 below year-ago -- higher 74 cents/st this week to $457.62/st.
Potash $26.12 below year-ago -- higher $1.53/st this week to $332.77/st.


DAP was our upside leader in P&K this week firming $2.28 led by Minnesota which added $5.98 as Indiana firmed $7.66 and Wisconsin gained $6.52. Five states were unchanged as Missouri fell 87 cents and Kansas softened 4 cents per short ton.
Potash was led higher by Indiana, up $7.05, Wisconsin up $6.52 and North Dakota which added $4.26. Just two states were unchanged on potash as Missouri once again led declines, falling $1.50, Kansas fell 69 cents and Nebraska shucked 46 cents per short ton.
MAP was higher as well this week led by strength in Indiana to the tune of $7.40 and North Dakota which gained $2.17. Three states were unchanged as Nebraska, Missouri and Kansas comprised our decliners, falling $1.16, 88 cents and 49 cents per short ton respectively.
We noted in an earlier post that the southwest corner of our 12-state survey area posted notable losses this week in fertilizer prices and that fact is no less true of P&K. The magnitude of net declines is smaller in that part of the Midwest than it is for nitrogen, but phosphate has taken its overpriced condition very seriously over the past several months, and has proven it will not fully bow to nitrogen price action. The biggest decline in Missouri, Kansas and Nebraska in P&K this week is that $1.50 falloff in Missouri. Meanwhile, Missouri UAN28% is down $17.89 on the week.
We believe phosphate will post only mild gains overall until the rest of our fertilizer segment catches up to it and achieves comparable price levels. But anhydrous ammonia will have to firm well above our projected $550 per short ton (that figure is at the top end of our forecast NH3 price range) before phosphate takes notice, and aligns with nitrogen price moves. An official form phosphate production powerhouse Morocco said this week that production declines and decent demand have placed a price floor under phosphates for the time being. More importantly, what he did NOT say was that phosphate prices are due to skyrocket in the foreseeable future.
That leaves the door open for DAP and MAP to trundle along at current price levels until its lofty premium to nitrogen is righted. If nitrogen fails to generate sufficient upside momentum to get that done, it is unclear how phosphate will respond. What is clear, however, is that DAP and MAP do not mind holding on to an overpriced condition within the fertilizer space, and it is our belief that phosphate will have limited upside risk within a mild uptrend through summer.
As overpriced as phosphate is, potash is equally underpriced compared to the rest of the fertilizer segment. World supplies are strong, China appears to be waiting out seasonal price strength before booking annual benchmark import contracts and we expect seasonal upside risk to drain from vitamin K quickly once U.S. applications are complete.


By the Pound -- The following is an updated table of P&K pricing by the pound as reported to your Inputs Monitor for the week ended March 24, 2017.
DAP is priced at 47 3/4 cents/lbP2O5; MAP at 42 3/4 cents/lbP2O5; Potash is at 28 1/4 cents/lbK2O.





P&K pricing by the pound -- 3/29/2017


DAP $P/lb


MAP $P/lb


Potash $K/lb

 



Average


$0.47 3/4


$0.42 3/4


$0.28 1/4

Average



Year-ago


$0.50 1/4


$0.46 1/2


$0.29 3/4

Year-ago