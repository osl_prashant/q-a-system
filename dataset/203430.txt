Surprising a harvest crew with breakfast burritos, Wal-Mart gift cards and other items, Salinas-based Markon Cooperative is the latest Central Coast company to join the Labor of Love program and formally express appreciation to farm workers.

 
On the morning of Sept. 8, representatives of Labor of Love and Markon met a crew harvesting and packing romaine lettuce for the Markon Fresh Crop label.
 
Labor of Love is a program that was begun by the Yuma Fresh Vegetable Association during 2015 to thank workers for their service to the agriculture industry. The program is supported by a variety of growers from Yuma to Salinas.
 
"Harvesting is a difficult profession and requires a commitment and unique set of skills," Steve Alameda, grower and president of the association, said in a news release. "When we find the right workers, we want to make sure we take care of them, respect them and celebrate their invaluable contribution to the industry."
 
Tim York, president of Markon, told The Packer Sept. 8 the event was the first time Markon participated with Labor of Love in honoring field workers. The program will continue in the Salinas Valley through September, according to the news release. 
 
York said the workers were very appreciative of Markon's message. John Galveg, director of quality assurance for Markon, spoke to the workers in Spanish, he said. 
 
"We wanted to formally acknowledge the great work that they do and how important they are to get the right product in the box for us and that they are really of the front lines of not only quality but food safety too," he said.
 
Each worker received a breakfast burrito, a Wal-Mart gift card, water bottle and a Markon hat and jacket.
 
York said one of Markon's core values is that "people matter" and the event served to emphasize the reality that farm workers are essential to the fresh produce supply chain.
 
"This industry could not survive without our immigrant labor," he said. 
 
Markon provides fresh produce purchasing, logistics, information, and marketing services exclusively to its seven member distributors, which include Ben E. Keith Foods, Gordon Food Service, Gordon Food Service Canada, Maines Paper & Food Service, Nicholas & Company, Reinhart Food Service, and Shamrock Foods Company, according to the release.