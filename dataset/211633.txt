Shrugging off isolated showers in the San Joaquin Valley, California grapes will be in good supply through the end of the year and into January, marketers report.Estimated at 111.4 million 19-pound boxes, 60% of the California table grape crop will be marketed after Aug. 31, said Kathleen Nave, president of the Fresno-based California Table Commission. The 2016 crop totaled just under 109 million boxes.
“Harvest is in full swing and we are anticipating having plenty of promotable volume all the way through the end of the year and hopefully into January,” Nave said Sept. 12.
With 2017 harvest timing later than the last couple of years, shipments have been running behind year-ago levels.
Isolated thunderstorms have been an irritation but good grape supplies will continue into November and December, said George Matoian, sales and marketing director for Visalia Produce Sales Inc., Kingsburg, Calif.
Varieties of green, red and black seedless grapes will be in ample volume for promotion, Nave said. More than 80 varieties are grow in California, and the majority were still being harvested in September.
Exports have also been strong, and should account for about 40% of total volume. The commission is promoting in 29 export markets, with top sales to Canada, Mexico and China/Hong Kong.
Despite a wet spring, extreme heat in the summer and untimely rain in the fall, there are plenty of grapes to meet demand, said John Pandol, special projects director for Delano-based Pandol Bros. Inc.
With harvest starting about seven days later than last year and now tracking about 10 days behind, he said there will be active harvest through Dec. 1.
“We will see a more concentrated harvest in the back end (of the season),” he said Sept. 12.
Grape plantings in the last few years have run heavy to mid- and late season varieties, and inventories of grapes on hand will build in the weeks ahead, allowing for good availability and ample promotion opportunities.
Pandol said showers in the last couple of weeks could subtract 1% or 2% from the total grape crop, but not more than that.
“There is no question there will be California grapes into January,” he said.
Shipments of California grapes were running at about 4 million boxes per week since mid-July and should keep close to that pace until mid-November, Pandol said. California should ship about 1 million boxes per week throughout December, he said, with supplies finishing in January.
Grape ad activity in the Texas and Florida markets was down in September because of Hurricanes Harvey and Irma, but that hasn’t hurt the overall market, he said.
Retailers have been aggressive in setting ad prices near 99 cents per pound for grapes, but that doesn’t work considering higher wages and costs in California compared with a decade ago.
“You can’t buy a box of grapes (at $19 per box) and sell it for 99 cents per pound and come out positive,” he said. The general f.o.b. grape market was trading at $16-22 per box in mid-September, Pandol said.
Matoian speculated price cuts at Whole Foods, recently purchased by Amazon, may have spurred recent aggressive ad pricing by retailers.