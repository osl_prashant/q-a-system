Grains mostly lower and livestock lower
Grains mostly lower and livestock lower

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mostly lower Friday in early trading on the Chicago Board of Trade.
Wheat for May delivery was down 11 cents at $4.6820 a bushel; May corn was off 3.40 cents at $3.8360 bushel; May oats fell 7.40 cents at $2.44 a bushel while May soybeans rose 7.20 cents at $10.4540 a bushel.
Beef and pork were lower on the Chicago Mercantile Exchange.
April live cattle fell .25 cent at $1.2135 a pound; March feeder cattle was down 1.10 cents at $1.4040 a pound; April lean hogs was off .82 cent at $.6558 a pound.