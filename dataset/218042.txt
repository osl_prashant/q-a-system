Results from forage analysis aren’t just for ration formulations. The numbers can also help producers adjust management processes to avoid future problems altogether.
 
“The results of forage analysis can be really useful,” says notes Bob Charley, Ph.D., Forage Products Management, Lallemand Animal Nutrition. “For instance, it can show if the crop was harvested correctly or if silages are prone to spoilage.”
 
Dr. Charley advises producers to review these parameters:

Dry matter (DM): Lower DM levels, especially in alfalfa or other high protein silages, can compound issues like the presence of clostridia from the soil. Lower DM levels also require more acid production and a lower pH for stability.
pH: Lower pH levels do not necessarily make better quality silage. Forage analysis should show pH stabilization, even if it’s at achieved at a higher level.
Ash: Normal levels in the plant should be around six to eight percent, depending on the crop. Higher numbers can indicate slurry contamination. 
Acid detergent fiber (ADF), neutral detergent fiber (NDF) and lignin: These values should be within average ranges for the type of material harvested. If levels are higher than normal for the crop, it may be a sign the material was more mature than ideal. This can also lead to yeast and mold challenges.
Crude protein (CP): The higher the protein level, the higher the buffering in the material and the more acid is required to bring the pH down. This can facilitate complications from clostridia. 
Acid Detergent Insoluble Crude Protein (ADICP): High levels of bound protein (ADICP, greater than 10 percent of the CP) show there has been heating in the silage. 
Soluble protein: High levels of soluble protein indicate there has been protein breakdown, also called proteolysis. This can occur due to prolonged wilting in the field or inefficient silage fermentation.
Lactic acid: This is the main driver for pH drop and should be at a reasonably high level for the silage pH to rapidly stabilize. This number can vary with the crop ensiled and DM level. 

 
Lactic acid levels will be lower in silages treated with inoculants containing Lactobacillus buchneri. It also typically has higher concentrations of acetic acid and lower levels of lactic acid than untreated silage. Acetic acid helps inhibit the growth of spoilage yeasts that are responsible for silage heating. 
 
This process is one of the reasons Biotal® forage inoculants containing the specific strain Lactobacillus buchneri 40788 have been uniquely reviewed by the FDA for improved aerobic stability when applied at 400,000 CFU per gram of forage or 600,000 CFU per gram of high-moisture corn (HMC).
 
“There is no single number that indicates ‘good’ silage,” Dr. Charley notes. “Silage is a very complex biological system with inherent variability. Still, forage analysis results can help producers understand what happened during the harvest and ensiling process — and how to improve feedstuff quantity and quality the next time.”