Colorado potato grower Verlin Rockey, 72, has died. 
In the 1990s, Rockey, then of Rockey Farms, Center, Colo., helped promote and expand the market for specialty potatoes in Colorado’s San Luis Valley.
 
Rockey also was featured in an episode of Mike Rowe’s syndicated television show “Dirty Jobs” in 2006.
 
Rowe paid tribute to Rockey in a March 29 column on his website.
 
“Like so many people on ‘Dirty Jobs,’ Verlin Rockey embodied and a willingness to get dirty, with a burning curiosity about the world around him,” Rowe said in his column. “His intelligence, in other words, was no less formidable than his relentless work ethic, Verlin Rockey and his generosity with me and my crew was, and still is, very much appreciated.”
 
Rockey grew up in Center, Colo., and graduated from the University of Colorado with a bachelor of science degree in engineering physics in 1967, according to a news release.
 
Rockey is survived by his wife of 51 years, Diane Rockey of Austin, Colo.; a son, Craig Rockey of Austin; a daughter, Petrina Steward of Meridian, Idaho; a sister, Loraine Campbell of Newburg, Ore.; and 13 grandchildren.
 
He was preceded in death by a brother, Warren Rockey.
 
Son Craig Rockey, of Potato Garden, Austin, Colo., said on the company’s website the seed potato shipper shut down the week of March 24 to celebrate Verlin Rockey’s life, but had resumed shipping seed potatoes again by March 27.
 
An online obituary and guest registry is available at www.taylorfuneralservice.com
 
The Rockey family requested that donations be made to the Mike Rowe Works Foundation, an organization that offers scholarships for job training.