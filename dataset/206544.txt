As regulators in the U.S. and the European Union go through the process of deciding whether or not to continue to allow farmers to use Glyphosate, Monsanto is doing their best to clear the chemical of cancer causing claims. According to Politico, EPA and EU regulators have already said the chemical is safe in current forms, but Monsanto seems to be taking no risks in its effort to undermine the International Agency for Research on Cancer report.Most recently they’ve pointed to a deposition that reveals the IARC unfairly disregarded two important pieces of research from Germany that suggested the herbicide was safe.
 

 

Politico reports that Charles William Jameson, a scientist who specialized in animal studies at IARC and was a member of IARC's review panel for the herbicide, was not told of research from two German Scientists who found glyphosate does not pose cancer risks.
Read more, here.