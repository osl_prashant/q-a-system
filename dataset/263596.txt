3 counties labeled at high risk for bovine tuberculosis
3 counties labeled at high risk for bovine tuberculosis

The Associated Press

LANSING, Mich.




LANSING, Mich. (AP) — State officials have designated portions of three northern Michigan counties as "potential high-risk areas" for bovine tuberculosis.
The Michigan Department of Agriculture and Rural Development applied the label to parts of Cheboygan, Iosco and Presque Isle counties.
The move came after four free-ranging whitetail deer were found that tested positive for the fatal illness.
The designation requires that all cattle and bison herds located within a 6.2-mile (10 kilometer) radius of a bovine TB-positive deer be tested within six months.
Officials say the testing ensures the disease has not spread from deer to livestock, protecting Michigan's cattle industry and reassuring the state's trading partners.
The department is contacting affected herd owners to schedule testing. A public information meeting is scheduled for March 29 in Rogers City.