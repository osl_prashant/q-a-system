Operation Main Street (OMS) speakers are reporting an increase in confusion and concern among their audiences about the use of antibiotics in food animals. Speakers are asking for additional information on how to address these concerns and antibiotics questions.In response to that need, the National Pork Board has set up six special one hour OMS Speaker Antibiotics Update Training sessions at World Pork Expo.
The OMS Speaker Antibiotics Update Training will cover:
The latest consumer research
Updated National Pork Board messages and online resources
Updated Q&A in a workshop format
World Pork Expo Training Dates and Times:
June 8 (Wednesday):
10:00 a.m.
11:00 a.m.
12:00 p.m.

June 10 (Friday):
2:00 p.m.
3:00 p.m.
4:00 p.m.
No registration is required to attend the update training. Please attend the class that works best for you.
Training Location:All classes will be held at the 4-H Building (south annex) on the southwest corner of the Iowa State Fairgrounds in Des Moines.
We recommend bringing a laptop for note taking; training material will be provided on a USB flash drive.
Please contact Operation Main Street at 800-711-0747 if you have questions about the OMS Speaker Antibiotics Update Training sessions at World Pork Expo.