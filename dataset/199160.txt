Full implementation of the FDA's new veterinary feed directive (VFD) rules will commence at the end of this year, a deadline that is rapidly approaching. The new rules will place the use of medically important antibiotics delivered in feed under the oversight of veterinarians.On April 19, GlobalVetLINK hosted a webinar titled "Veterinary Feed Directives: Taking Action." The archived presentations now are available for viewing on your own time.
The webinar features several presenters with insights on different aspects of the VFD rule and how veterinarians and producers should prepare. These include:
Veterinary Feed Directive -Industry Concerns & Challenges - Richard Sellers, Senior Vice President,Public Policy and Education, American Feed Industry Association (AFIA).
Veterinarian Responsibilities & VCPR - Christine Hoang, DVM, MPH, CPH, Assistant Director, Division of Animal and Public Health, American Veterinary Medical Association (AVMA).
Action Plan - Prepare Now for Jan. 1 - Kaylen Henry, Product Manager, GlobalVetLINK.
The webinar qualifies for continuing education (CE) credit. In order to obtain CE credit for the webinar, you must have met certain requirements, including:
You must have watch the webinar on a computer to earn CE Credit. Audio only attendance does not meet CE requirements.
You must have beenpresent for at least 45 minutes of the 60 minute presentation.
If you watchedwith a group, you must complete the digital sign-in sheetto assure you receive CE Credit.
If for some reason you were not able to attend the webinar for the required amount of time, but you still wish to earn CE, you can watch the recorded webinar and submit a post-test.
View the webinar here.