Wenatchee, Wash.-based Oneonta Starr Ranch Growers has hired Dan Davis as organic category to manage the company’s expanding organic tree fruit volume. 
Davis brings nearly two decades of experience in organic fruit production to his new position, according to a news release. As category manager, he will be responsible for strategy and management of the supply base on all Oneonta organic programs, according to the release.
 
Davis was previously production coordinator at Viva Tierra Organic Inc. from 2008 to 2017, according to his LinkedIn profile.
 
“We are very excited about adding Dan to the team,” Oneonta marketing director Scott Marboe said in the release, “His knowledge and experience will really take our programs to the next level.”
 
Davis said in the release that Oneonta has a full line of organic commodities that will make it ideal for buyers to use the company as a “one stop shop for organic needs.”
 
Oneonta imports from New Zealand for year-round organic apple availability, according to the release.
 
 “This is the second year of a great partnership with Fern Ridge Fresh in New Zealand, and the program has increased dramatically,” Oneonta national marketing representative Bruce Turner said in the release. “Importing from the Southern Hemisphere gives us the ability to meet the growing retail demand for high quality organic apples, and we’re providing all the right varieties — royal gala, fuji, granny smith and Pink Lady.”
 
Turner said the company’s organic volume will increase this fall, and by 2019, Oneonta will have almost 2,000 acres of organic apple production in Washington.
 
Oneonta is adding organic red and green anjous, bartletts and boscs to its offerings this fall.
 
“The pears will be packed at Diamond Fruit Growers in the ‘Starr Ranch Organics Label,’” Turner said in the release. “And we have organic cherries in transition now which we will offer in 2018.”