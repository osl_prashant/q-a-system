In 2016 whether corn, soybean or cotton biotech acres rose or fell was highly dependent on state. Surprisingly, some key players in corn and soybean production saw a slight drop in biotech acres.2016 Corn Biotech Acres

<!--*/

.embed-container {position: relative; padding-bottom: 67%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}
/*-->*/


View larger map


Source: USDA Acreage Report
In 2016 there was relatively little overall change in corn biotech acres. Surprisingly Iowa, one of the biggest corn producing states, did see a drop in biotech acres overall. Missouri had the most notable jump from 89% in 2015 to 93% of corn acres planted to corn in 2016.

2016 Soybean Biotech Acres

<!--*/

.embed-container {position: relative; padding-bottom: 67%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}
/*-->*/


View larger map


Source: USDA Acreage Report
Soybeans seemed to see more growth in biotech acres in 2015 than corn. Missouri lead the pack with a four percentage point jump year to year.

2016 Cotton Biotech Acres

<!--*/

.embed-container {position: relative; padding-bottom: 67%; height: 0; max-width: 100%;} .embed-container iframe, .embed-container object, .embed-container iframe{position: absolute; top: 0; left: 0; width: 100%; height: 100%;} small{position: absolute; z-index: 40; bottom: 0; margin-bottom: -15px;}
/*-->*/


View larger map


Source: USDA Acreage Report
Cotton acres seemed to drop in the majority of states. California, however, jumped forward seven percentage points.