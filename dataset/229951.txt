Vermont, Iowa, Maryland, Indiana, Minnesota and Wisconsin: what do all these states have in common? These states all have winter manure application bans established (different conditions exist within each state). So, “Is winter manure spreading allowed in Michigan?” Yes, and Michigan State University Extension recognizes that the proper timing of manure spreading as well as the rate of application allows for maximum nutrient retention within soils all while protecting water quality resources via limiting nutrient losses during runoff events.
Spreading manure in the winter can be quite beneficial (and necessary in some scenarios) for livestock and cropping operations, but this practice is not without its environmental risks, especially if manure is not properly managed.
According to the literature review article Winter Manure Application: Management Practices and Environmental Impact from the North Central Region Soil Health Nexus, associated risks of winter manure application include:

Increased runoff of manure nutrients and contaminants due to the spring thaw if manure is applied in late winter
Soil productivity decrease and local water bodies are impacted as these manure nutrients and contaminants runoff

Risks aside, when properly applied, manure that is spread in the winter can:

Decrease the size and number of manure storages needed on the farmstead
Allow farmers to spread manure when logistics make sense for them
Reduce the amount of compaction on the soil due to the spreading equipment running over compressible soil

The Michigan Department of Agriculture and Rural Development’s (MDARD) Michigan Agriculture Environmental Assurance Program (MAEAP) and the Michigan Department of Environmental Quality (MDEQ) have developed a helpful information guide to help farmers determine if their manure spreading habits are a risk to environmental quality.
There are five questions in this guide that look specifically at:

Manure storage capacity
Slope and drainage of fields
Weather forecasts and seasonal conditions
Amount of snow on the field
Type of manure being applied

Each question has a “risk-o-meter” that is color coded from green to red allowing farmers to visualize whether their spreading habits are at a high or low risk.
Winter Manure: A Hot Topic in Cold Weather

Additional resources and tools that are available to help farmers evaluate (on a field-by-field basis) the risk of winter manure spreading include:

MDARD Right to Farm’s Generally Accepted Agricultural Management Practices for Manure Management and Utilization
MDEQ’s Concentrated Animal Feeding Operation permit requirements
The Manure Application Risk Index

More information on these resources can be found at the Michigan State University Extension News article “Resources available on winter spreading.”
With a good understanding of the benefits and risks associated with winter manure applications and access to available resources from Michigan State University Extension and the MAEAP Program, farmers are better equipped to make decisions on the right source of manure, rate and timing of application, and correct placement. This also allows farmers to meet their cropping system goals of increased production, increased profitability and enhanced environmental protection of Michigan’s waters and approved sustainability.