The Vidalia onion industry has plans for an aggressive promotional season, said Bob Stafford, interim director of the Vidalia Onion Committee in Vidalia, Ga.
The committee has signed public relations agency Porter Novelli’s Atlanta office to help promote the crop.
The committee also is working with the Georgia Department of Agriculture’s Georgia Grown program for some cross-promotions, Stafford said.
“We’re going to try to extend our promotions,” he said.
The committee also is redesigning its website, vidaliaonion.org, with Porter Novelli’s help, Stafford said.
“We’ve been kind of lax on that for several years, but we’ll do it right,” Stafford said.
The committee is reaching out to chefs as well as retailers, he said.
“That seems to be the big thing,” he said, referring to foodservice outreach.
There’s also a concerted effort to reach out to younger consumers, Stafford said.
“We’ve been chasing millennials, getting them used to using our onions,” he said.
The committee also wants to emphasize that the Vidalia onion is Georgia’s state vegetable, Stafford said.
Individual suppliers have their own plans, as well.
For Glennville, Ga.-based Bland Farms LLC, that means working with condiment, salsa and seasoning manufacturer Vidalia Brands Inc., said Delbert Bland, president of Bland Farms.
“We’re going to continue what we’ve been doing,” he said of the partnership. “We’ve done a lot of cross-merchandising with Vidalia Brands — coupons and demos between two companies.”
Cross-merchandising also is part of the promotional game plan for Reidsville, Ga.-based grower-shipper Shuman Produce Inc., said John Shuman, president.
“We will be promoting our cross-merchandising IRC campaign this Vidalia season, along with our Produce for Kids campaign,” Shuman said.
Shuman Produce also will promote a “How to Speak Southern” marketing effort that focuses on the South’s unique attributes, as well as “educate consumers on Vidalia sweet onions and our RealSweet brand of premium sweet onions,” Shuman said.
Metter, Ga.-based Hendrix Produce Inc. has some retail promotions this year, said Kevin Hendrix, vice president.
“We’ve got some bag promotions set up — a 3-pound bag, and we’ve got some with a 5-pound bag,” he said. “We’re promoting some mediums (medium-sized onions).”
Hendrix said his company’s past bag promotions have done well, and there’s no reason to change plans.
“The Vidalia is a big seller, and we have some (customers) that move a lot of bagged onions,” he said.
Lyons, Ga.-based L.G. Herndon Jr. Farms Inc. has some promotional plans, too, said John Williams, sales and marketing manager.
“We’re working with the wholesalers and retailers alike and even some non-traditional customers,” he said. “With the new wave of how consumers can get things at the touch of their fingertips, there are new opportunities to get our onions to the consumers.”