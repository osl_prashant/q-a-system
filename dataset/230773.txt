Bundy: Judge did US government a favor by throwing out case
Bundy: Judge did US government a favor by throwing out case

By SCOTT SONNERAssociated Press
The Associated Press

SPARKS, Nev.




SPARKS, Nev. (AP) — Southern Nevada rancher and state's rights activist Cliven Bundy said Friday a judge in Las Vegas did the U.S. government a favor last month when she threw out his criminal case stemming from an armed standoff with federal agents at his Bunkerville ranch in April 2014.
Bundy, 71, told a gathering of the Independent American Party of Nevada's state convention in Sparks that he would have preferred the trial to continue because he knows he would have been acquitted of any crimes in a decades-old feud with the U.S. Bureau of Land Management.
"That court (proceeding) was actually stopped because we would have embarrassed the government very much if it would have continued on for even two more hours," Bundy told a crowd of about 100 in a hotel-casino meeting room.
"Most people thought that was a great thing. But it would have been greater thing if they'd let that jury find us not guilty," he said.  "Because that would have set a precedent and it would have lasted 100 years."
"If they come back and try to take us to trial again, which they indicate they might, that's just fine. We'll exploit them a whole bunch more," he said.
Bundy joked that he got room service with three meals a day during his nearly 23 months in prison before U.S. Judge Gloria Navarro dismissed the cased.
"But I went in there an innocent man and I came out of there an innocent man," he said.
The third party, which focuses on state and property rights, includes many disaffected, former Republicans. It now has 65,000 active registered voters in Nevada — up from about 15,000 in 2002. It's currently the third largest political party in Nevada, making up about 4.5 percent of the active voters.
Bundy told reporters before the speech he doesn't have any aspirations to run for political office himself. He said he's backing Nevada's Independent American Party because neither Republicans nor Democrats truly represent American citizens.
Party chairman Joel Hansen, who earlier served as one of Bundy's lawyers, said Bundy's story is one of "tremendous courage and faith" standing up against federal "corruption and tyranny." He likened Bundy and his wife, Carol, to the American patriots who fought the British in the Revolutionary War more than 200 years ago.
"They fought the battle of Bunker Hill and he fought the battle of Bunkerville," Hansen said. "The BLM was exposed as criminals."
Several of the Independent American Party's leaders have been involved in legal disputes with the U.S. government similar to Bundy's dating to the 1990s.
In 2001, the party staged a fundraiser to cover legal costs of a Nevada rancher, Cliff Gardner, whose cattle were seized for failing to pay grazing fees on national forest land in northeast Nevada. Its 2002 Nevada gubernatorial candidate, David Holmgren, waged his own battle with the BLM over his cattle in central Nevada. Both were in the crowd Friday night.
The U.S. Bureau of Land Management began rounding up Bundy cows in April 2014 after obtaining court orders accusing Bundy for 20 years of failure to pay more than $1 million in back grazing fees and penalties to the federal government.
Andy Kerr, a longtime environmental activist and consultant in Oregon, is among those angered that Bundy cattle are still grazing within the boundaries of the Gold Butte National Monument President Obama designated in 2016 about 90 miles (140 kilometers) northeast of Las Vegas.
"Now free of the threat of federal criminal prosecution, the Bundy boys are getting some speaking gigs to tell their story, brag about how they beat the feds and how they are continuing to do so by not paying federal grazing fees," Kerr wrote on his Public Lands Blog on Friday.
But he said there may be a silver lining for conservationists.
"The more the Bundyites spread their bunk about the illegitimacy of the federal public lands ... the more the overwhelming majority of Americans remember they love America's public lands," Kerr said.