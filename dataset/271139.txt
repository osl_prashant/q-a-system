The latest QS World University Rankings place the University of California at Davis as the world’s top veterinary school. Cornell placed second, and several other U.S. and Canadian veterinary schools appear in the top 25.The rankings are prepared by Quacquarelli Symonds, a British firm widely considered to be one of the most influential international university rankings providers. QS rankings are based on reputational surveys and research citations. This year, QS evaluated 4,438 universities, then qualified 3,098 and ranked 1,117 institutions.
“The energy, passion, knowledge and skills that the UC Davis veterinary medicine community brings to their jobs every day is the secret to our success,” said Michael Lairmore, the school’s dean since 2011.
UC-Davis and Cornell also placed highly in the QS rankings for agriculture and forestry schools, with UC-Davis second after Wageningen University in The Netherlands and Cornell ranked third. See the full list of agricultural and forestry school rankings.
In veterinary science, additional universities in the top 25 QS rankings include the University of Pennsylvania, Texas A&M University, Michigan State University, the University of Minnesota, The Ohio State University, Colorado State University, North Carolina State University, The University of Wisconsin-Madison and Purdue University.
Following is the full QS list of global rankings of veterinary science programs.
1.       University of California, Davis
2.       Cornell University
3.       Royal Veterinary College, University of London
4.       University of Cambridge
5.       University of Liverpool
6.       University of Guelph
7.       University of Pennsylvania
8.       The University of Edinburgh
9.       Utrecht University
10.   Texas A&M University
11.   The University of Sydney
12.   Michigan State University
13.   University of Copenhagen
14.   University of Minnesota
15.   The University of Melbourne
16.   Wageningen University
17.   University of Glasgow
18.   The Ohio State University
19.   Colorado State University
20.   Ghent University
21.   North Carolina State University
22.   University of Wisconsin-Madison
23.   Massey University
24.   Purdue University
25.   Ludwig-Maximilians-Universität München