Denmark said this week it will build a 70 kilometer (43.5 mile) fence on its German border to keep out wild boars that can carry the deadly infection African Swine Fever (ASF) to farm pigs and threaten the country’s large pork industry.
While no case has yet been detected in Denmark, the spread of the disease in Eastern Europe is causing concern in the country, whose pig exports amount to 33 billion Danish crowns ($5.5 billion) per year.
The virus that causes ASF, is harmless to humans and other animals. But for feral (wild) and farm pigs, the disease is deadly in almost all cases within 10 days. There is no vaccine against it.
An outbreak of the disease in Denmark would shut down all exports to non-EU countries for a period of time, while only exports from the affected area in Denmark would be blocked from being exported to other EU member states, the government said.
The disease exists in Poland, the Czech republic, Romania, Estonia, Latvia and Lithuania and has recently moved closer to Denmark, according to the government.
The agreement this week between the government and its ally Danish People’s Party, which combined holds a parliament majority, also includes larger fines for illegal food imports and failures to clean animal transportation vehicles properly.
Hunters have also been given new options to hunt the nocturnal animals at night time.
Germany issued a decree last month to allow hunters to shoot wild boars year-round to stop the animals.
Editor’s Note: Reporting by Teis Jensen; editing by Jacob Gronholt-Pedersen and David Evans.