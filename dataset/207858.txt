Salinas, Calif.-based Moxxy Marketing received four 2017 American Packaging Design Awards from Graphic Design USA for work on Braga Fresh Family Farm’s Josie’s Organics label packaging.
The products recognized by Graphic Design USA were:

Josie’s Organics Vegetable Medley bags and display-ready cartons;
Josie’s Organics Sweet Kale Chopped Salad bags and display-ready cartons;
Josie’s Organics Asian Chopped Salad bags and display-ready cartons; and
Josie’s Organics Baby Leaf Lettuces and Salad Blends clamshell labels.

“Those Josie’s Organics bags and (display-ready cartons) don’t just jump out at you in the produce department — they can be seen from across the store,” Moxxy Marketing president Karen Nardozza said in the release. “Now that’s attention-getting packaging design.”
According to the release, Moxxy also received awards for Josie’s Organics’ website, videos, packaging, ad campaigns and trade show displays.