BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Wednesday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMay                              117.15  Up   1.30May      113.75  115.35  113.35  114.90  Up   1.35Jul                              119.30  Up   1.20Jul      116.05  117.70  115.65  117.15  Up   1.30Sep      118.35  119.95  117.90  119.30  Up   1.20Dec      121.90  123.20  121.40  122.80  Up   1.20Mar      125.30  126.85  124.95  126.35  Up   1.20May      127.85  129.20  127.50  128.70  Up   1.10Jul      130.00  131.50  129.75  130.85  Up    .95Sep      131.95  133.45  131.95  132.75  Up    .75Dec      135.00  136.35  135.00  135.50  Up    .50Mar      139.20  139.20  138.20  138.25  Up    .25May      140.95  140.95  139.95  140.10  Up    .35Jul      142.60  142.60  141.55  141.75  Up    .30Sep      144.15  144.15  143.05  143.35  Up    .30Dec      146.55  146.55  145.80  145.80  Up    .30Mar                              147.95  Up    .30