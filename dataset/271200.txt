• Don’t guess; use hard numbers. Use a minimum of blood samples from 12 calves; more samples give a better estimate of passive transfer rates.• Once the failure rate is established get hard numbers for critical control points: calf age at first feeding, quality and quantity of colostrum fed.
• Write protocols, train calf care workers to follow protocols, monitor protocol compliance.
• Test, test, test. Keep sampling blood until the passive transfer failure rate drops to the farm’s goal.
The conversation usually starts like this: “We are having more sickness in calves than we would like to see. I can’t understand why because we are doing everything right – colostrum management, sanitation, and feeding plenty of pasteurized milk.” I ask, “How recently have you checked the success rate of your colostrum management program?”
In a recent experience like this the first round of blood serum total protein results were: (for more on blood serum total protein see www.calffacts.com and select “Transfer of Immunity: How to Test For.)
 Above 5.0 = 5            33%
Between 4.5-4.9 = 3  20%
Between 4.4-4.0 = 5  33%
Below 4.0 = 2            14%
 For comparison, blood serum total protein standards for a commercial dairy expecting average calf health are:
Above 5.5                  50%
Between 5.0-5.4          40%
Below 5.0                   10%
Conclusion? Colostrum management program is not working well. Something needs to be fixed. Where to start? Critical control points: (1) How soon after birth is the first feeding of colostrum? (2) What quality of colostrum is being fed at first feeding? (3) What quantity of colostrum is being fed for first feeding?
My recommendations were:
1. Set up a protocol for measuring colostrum quality. Train staff to use either a Colostrometer or Brix refractometer on all colostrum ASAP after collection – mark quality on storage containers – use the highest quality available for first feedings.
If after testing we find that not enough high quality colostrum is available to feed 4 quarts within the first 4 hours to all calves check out the interval between calving and first milking for fresh cows – shorter times should result in higher quality colostrum. If no easy solution for quality presents itself consider using a colostrum supplement.
2. Set up a system to record times when calves are born and when the first feeding of colostrum took place. Large dairies set this goal at 90 percent receive their first feeding of colostrum within the first hour and all calves within the first 4 hours. Plan to summarize this information every week. If these goals are not met work out changes that will make compliance possible.
3. Use the same record-keeping system for when colostrum is fed to record the volume of colostrum consumed. If esophageal-tube feeders are going to be used be sure to include adequate training for calf care personnel. See www.atticacows.com within the left-hand menu choose the Calf Care Skills section, click on “Basic Calf Care Skills” and find #8 Feed a calf with an esophageal or tube feeder. This is an outline for teaching a person how to use a tube feeder.
Our goal is 4 quarts of colostrum (large breeds) within the first 4 hours of life. Sooner is always better. Regardless of whether the 4-quart volume is consumed in one or two feedings it is important to get accurate recording of actual volume consumed.
If a large volume (4 quarts) is fed in one feeding the method of feeding should not make a difference in the amount of antibodies that end up in the calf’s blood. That is, either bottle-feeding or esophageal-tube-feeding gives the same results.
If a small volume (2 quarts or less) is fed in two feedings the bottle-feeding method should give the better rate of antibody transfer compared to tube-feeding.
 4. Test, test, test. Keep checking blood serum total protein levels. If the practices above are adopted successfully it is not unrealistic to have 90 percent plus over 5.0 and 75 percent at 5.5 and above.