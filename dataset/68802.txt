Anhydrous is $72.08 below year-ago pricing -- higher $1.38/st this week at $521.32.
Urea is $10.32 below the same time last year -- lower $1.27/st this week to $363.04.
UAN28% is $28.95 below year-ago -- lower 13 cents/st this week to $248.76.
UAN32% is priced $24.04 below last year -- lower $5.87/st this week at $280.81.

Anhydrous ammonia posted our only gains in the nitrogen segment this week, firming $1.38 led by a $14.15 hike in Nebraska as Iowa firmed $2.54 and North Dakota firmed $1.75. Five states were unchanged as Kansas was our only decliner, falling $3.69 by the short ton.
UAN32% led declines in the nitrogen segment after firming sharply last week. Nebraska firmed $2.58 to lead gains as Iowa gained $1.36. Seven states were unchanged as Missouri -- our only 32% decliner -- fell 88 cents.
Urea fell this week led by Nebraska, which softened $7.21 as Wisconsin fell $4.03 and South Dakota dropped $3.75. Two states were unchanged as Iowa firmed $2.15 and Missouri gained $1.17.
UAN28% fell 13 cents to round out declines in the nitrogen segment. Wisconsin fell $19.48 as South Dakota softened $8.59 and Minnesota declined $1.05. Three states were unchanged as Missouri firmed $17.89 and Iowa gained $5.41.
The general trend in nitrogen was lower this week. We speculated last week that downward price action in Nebraska, Kansas and Missouri might have been a clue that the rest of the states in our survey would follow those southwestern states' example. This week's price action has some forms of nitrogen in those states firming rather sharply. But if we look at the nitrogen products that firmed in our southwestern states, we find the bulk of the upside action was in anhydrous and UAN28%. That would fit nicely with our analysis which suggests urea has set the high-water mark for nitrogen price action and that anhydrous and UAN28% will firm to meet that level before the end of spring applications.
UAN32% is priced roughly at the midpoint between urea and anhydrous. If our analysis is correct, anhydrous will continue higher to a level near $545 per short ton, as long as urea doesn't firm a whole lot farther. That would constitute a 4.8% rise in anhydrous prices from current prices. UAN28% would top out around $260 and 32% at $290, which is about $10 above today's 32% regional average bid.
Our nitrogen price margins expressed in the table below narrowed this week with UAN28% arriving at parity with NH3 both in our indexed figure and by the pound of N. Notice the chart at left. It depicts the four nitrogen products we survey with UAN solutions and urea indexed to NH3. The margins between our N products tighten before making a trend-defying move. It is also key to note that when urea is at a premium to anhydrous, the trend is up. When anhydrous captures the premium from urea, the trend is downward.
This leads us to believe that nitrogen will continue higher until anhydrous ammonia prices overtake urea, which will lead to overall nitrogen price softness. Of course, if urea springs higher, anhydrous has shown its willingness to pursue higher price points. We will be watching the margins closely for an indication of impending nitrogen price strength or weakness on the basis of the spread between urea and anhydrous ammonia.
December 2017 corn closed at $3.88 on Friday, March 31. That places expected new-crop revenue (eNCR) per acre based on Dec '17 futures at $614.92 with the eNCR17/NH3 spread at -93.60 with anhydrous ammonia priced at a discount to expected new-crop revenue. The spread widened 13.87 points on the week.




Nitrogen pricing by pound of N 4/5/17


Anhydrous $N/lb


Urea $N/lb


UAN28 $N/lb


UAN32 $N/lb



Midwest Average

$0.32 1/4


$0.40 1/4


$0.44 1/4


$0.44



Year-ago

$0.36 1/4


$0.41 1/4


$0.49 1/2


$0.47 1/2




The Margins -- UAN32% is at a 1 3/4 cent premium to NH3. Urea is 3 cents above anhydrous ammonia; UAN28% solution is priced at parity NH3. The margins continue to narrow.




Nitrogen


Expected Margin


Current Price by the Pound of N


Actual Margin This Week


Outstanding Spread




Anhydrous Ammonia (NH3)


0


32 1/4 cents


0


0




Urea


NH3 +5 cents


40 1/4 cents


+8 cents


+3 cents




UAN28%


NH3 +12 cents


44 1/4 cents


+12 cents


0




UAN32%


NH3 +10 cents


44 cents


+11 3/4 cents


+1 3/4 cents