Quebec’s love affair with potatoes shows no sign of fading.While 10-pound bags are still a big seller, the success of specialty spuds from Edmonton, Alberta-based Little Potato Co. is inspiring the province’s growers to try new varieties and package their own lines.
“Little Potato Co. kits continue to surpass objectives, showing double-digit growth month after month,” said Dino Farrese, executive vice president of Boucherville, Quebec-based product specialist Bellemont Powell.
Farrese said Quebecers love the ease and convenience of not having to peel potatoes and being able to cook them on the barbecue or in the microwave.
“It offers a fresh, quality side that people are going crazy for,” he said.
Gord Medynski, director of sales and purchasing for St. Ubalde, Quebec-based Patates Dolbec, said the company has tripled its acreage of creamer potatoes this year to about 80 acres after three years of successful trials.
“Creamers are definitely becoming more popular in Quebec,” Medynski said, “and they’re starting to change the mentality around potatoes — this is not grandma’s boring old recipe.”
Dolbec’s creamers will join its successful Parfaite (Perfect) line at Metro and appear in 1.5-pound company-branded bags.
Packed by usage, the current Parfaite line includes varieties marketed for mashed, baked, fries and a tray of foil-wrapped russets for the grill.
Test results on new varieties in all colors are also promising, Medynski said, and he hopes to gain exclusivity on several of them. At the same time, he said demand for round whites remains strong in the province.
At Québec Parmentier Inc., which comprises more than 25 Quebec producers and packers, the eight-month-old Mamzells family launched a new sibling in early June.
The Prêt à Cuire (Ready to Cook) foil tray of yellow-flesh potatoes with a micro-perforated seal is ready in five minutes in the microwave and can also be cooked in the oven or on the grill, innovation director Audrey Boulianne said.
“We didn’t include a spice mix because most people don’t use it or only use a quarter of it,” Boulianne said, “but we do suggest great flavor combinations.” 
The original Mamzells line, which includes flavor profiles such as nutty and buttery, has been tweaked to make it more consumer-friendly, she said. Pictograms of foods such as chicken or beef are now prominent on the package to help shoppers pair their protein with the right potato.
Mamzells’ packaging has also evolved from poly to foil to keep the potatoes fresh longer and keep out light to prevent the thin skins from turning green.