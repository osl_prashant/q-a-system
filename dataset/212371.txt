Have you ever looked closely at produce trends? Like which income bracket is more likely to buy watermelon?Produce Retailer’s editor Pamela Riemenschneider and The Packer’s editor Greg Johnson shared in depth produce trend data at The Packer’s West Coast Produce Expo.
The duo used the 2017 Fresh Trends, which surveys more than 1000 shoppers about their fresh fruit and vegetable purchases.
The crowd participated by voting on how they thought the demographics would lay out.
And as for that watermelon? Attendees thought it would most likely be purchased by households with income in the $25,000 to $49,000 range.
However, 63% of shoppers with a household income over $100,000 bought watermelon.
Visit Produce Retailer for more retail trends and consumer info.