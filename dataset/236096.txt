Reaching beyond the traditional marketing target of 25- to 44-year-old women shoppers, the U.S. Highbush Blueberry Council wants to increase demand from moderate consumers of blueberries.
The council, which administers the Blueberry Promotion, Research, and Information Order, is rolling out the 2018 marketing program called Positively Bluetiful, said Mark Villata, executive director with the Folsom, Calif.-based council.
“We see a lot of potential to increase the moderate users’ consumption,” Villata said.
The category of moderate users includes many ages and ethnicities, male and female, he said.
Moderate users are defined as shoppers consuming 7.6 fresh cups per year on average.
The campaign is a year-long, multi-channel digital advertising effort that will seek to place blueberry messages on websites and in apps that moderate users interact with on a daily basis, such as the weather app on a smartphone.
Social media promotions and other marketing activities will aim to maintain heavy users of blueberries and encourage moderate users to increase their consumption, Villata said.
 
Rise of the blues
Fueled by rising supply and steady positive buzz about the fruit’s benefits, blueberries have netted more and more consumers in the past two decades. Per capita retail availability of fresh blueberries has increased from 0.3 pounds in 1998 to 1.5 pounds in 2016.
In the first quarter of 2017, the United Fresh Produce Association’s FreshFacts report showed that dollar sales of blueberries per store were $1,279 per week, up 0.4% compared the same quarter a year ago.
Volume of blueberries sold during the first quarter of 2017 was up 6.2% from year-ago levels. Prices per unit were down 5.4% compared with the first quarter of 2016.
Blueberry sales accounted for 27% of total berry category sales in the first quarter of 2017, according to FreshFacts.
The Packer’s Fresh Trends 2017 showed 45% of 1,004 consumers polled said they consumed fresh blueberries in the past 12 months, compared with 30% for blackberries and 63% for strawberries.
Fresh Trends reported that 37% of men said they had eaten fresh blueberries in the past 12 months, compared with 52% of women.
Relative to income levels, Fresh Trends reported 59% of consumers making over $100,000 per year said they consumed fresh blueberries in the previous year, compared with 52% for those making $50,000 to $100,000; 40% for those making $25,000 to $50,000 per year; and 31% for making less than $25,000 per year.
By ethnic division, Fresh Trends reported that 48% of whites reported consuming blueberries, compared with 26% for blacks, 39% for Hispanics and 58% for Asians.
The health halo of blueberries is so well known that health is almost synonymous with the fruit, Villata said.
The “Positively Bluetiful” marketing message will emphasize taste, health benefits and the convenience of the fruit, he said.
 
Where to from here
Bruce Peterson of Peterson Insights Inc. said the health benefits of blueberries began to be top-of-mind for consumers about a decade ago.
“The market absolutely took over from a demand standpoint and never really looked back,” Peterson said.
Blueberries have become a staple for retailers and consumers, he said, and organic production also has steadily risen.
The only long-term caution might be the danger of oversupply, Peterson said.
“With more and more acreage given over to blueberries, at some point you might have production get on the other side of demand, and then it becomes a different story,” Peterson said.
In the future, Peterson said there may be an opportunity for retailers to stake out a private-label blueberry program.
“The argument (for private-label) is that you avoid the marketing cost of the marketing company and that you are able to get a better margin for your product,” he said.
“I’m a little surprised it hasn’t touched the berries yet, and I think the big word is ‘yet,’” Peterson said.
“Because so much production is coming on in blueberries, (the market) is going to have private-label move into it,” he said.
Retailers may also try to secure proprietary varieties in their private-label business, he said.