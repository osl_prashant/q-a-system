When someone asks you, “what do you do?” and you respond that you are an ag retailer, crop consultant or farmer, John Ellis says that is the wrong answer.
“You are a technologist who has chosen to deliver farm products,” Ellis told farmers and retailers attending the 2017 Farm Journal AgTech Expo in Indianapolis.
As a veteran of the mobile and automotive industries, Ellis says the technology revolution is coming at all levels, across all business sectors and quickly. As proof, he says it took 60 years for the telephone to be widely adopted in the U.S.; it took only 10 months for the iPad.
Ellis referenced a patent Google received in 2016 to push content to a car in exchange for information about that car. He believes the technology will transform the relationship between vehicles and data.

“Software companies are driving this, not the original equipment manufacturers,” he says. “The best example is Tesla, which founder Elon Musk says builds software and then wraps it with a car. The construct of a software-first company is so strong that if you fail to understand it, you’ll fail to succeed.”
He offered Expo attendees three steps that can help them plan for success in the years ahead: get comfortable being uncomfortable, think differently, and prepare for the data economy.
As Google’s geospatial technologist, or as he refers to himself, geographer in residence, Ed Parsons outlined at least five megatrends driving technology during his keynote.

The world is becoming more urban, not suburban. 
Digital natives have formed a new type of consumer.
Big data is still in its early days, but platforms are providing a massive difference in the shape of business. 
Disintermediation means that if you don’t add value to the process, then you will be removed.
Information has to be accessible.

Those megatrends will shape how new technologies are built. One of the building blocks will be remote sensing. He also adds that precise positioning is making large advances, particularly in smartphones.
“There’s no one silver bullet to go into ag tech,” Parsons says. “It’s going to come from small companies taking existing technologies and making them simple and less complex, which will make them more accessible.” 

Farm Journal Field Agronomist Ken Ferrie says there are technologies readily accessible to growers now that would be easy and profitable for them to adopt, such as row clutch shutoffs.
Ferrie told attendees that when it comes to adopting precision ag, keep these two tips in mind.
First, read the manual. “For half of the service calls with our customers we find the answers are in the manual,” he says. He adds he has noticed more and more farmers keeping precision ag and technology manuals in their tractor cabs—not stowed away at the office.
Second, find out if the technology provider can provide you with adequate support. Before you buy, call the company’s customer service line. Check the service center’s responsiveness and wait times.
“There’s nothing worse than a farmer sitting in a field who’s shut down for the day. So unless you’re a total geek, you need support,” he says. “You want a guy that will show up if you need him at 8 p.m. on a Friday.”
Ferrie also sees technology coming to the farm at a fast pace.
“The slow rabbit to shoot is multi-hybrid technology,” he says. “It can bring us the biggest gains in the field, but we have a lot more to learn.”