Eden Prairie, Minn.-based Supervalu Foundation plans to donate $1 million to nonprofits that provide hunger relief.
 
Supervalu Foundation said the money will be given to 34 nonprofit organization to benefit communities in 17 states and the District of Columbia.
 
Local organizations receiving funds include food bank affiliates of Feeding America, Meals on Wheels America, urban garden programs and emergency shelters, according to the release.
 
"Too many people in the United States today experience food insecurity," Supervalu president and CEO Mark Gross said in the release. "Today's donations provide needed funding to help feed our communities, which is part of our company's mission, and is an important contribution to the efforts to address hunger relief."