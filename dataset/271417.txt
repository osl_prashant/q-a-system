For all our collective faults and shortcomings, our well-documented cluelessness about civics and geography and our willful ignorance about the science and high-tech that fuels the modern lifestyles we take for granted, there’s one thing most of us have become very adept at: shaming other people.
Of course, there are those relatively bland attempts at ridiculing people’s (apparent) lack of technical sophistication (“You’re still using a flip phone??”) or awkward fashion sense (“Those shoes are so last year!”).
And as the Poynter Institute’s 2015 white paper on “public shaming” noted, there’s “a certain nobility” in shaming politicians who try to hide public documents or in exposing a corporation that abuses its work force.
That’s the “good” kind of shaming.
But then there’s the toxic kind, whether it’s about body shaming for not having the “ideal” physical appearance, political shaming for having the “wrong” perspective on policies or policymakers or perhaps the most prevalent, and damaging, type of shaming: making “immoral” food choices.
Such attempts at outright humiliation are exemplified in the increasingly virulent condemnation heaped upon anyone who doesn’t embrace the vegan lifestyle in its entirety. As the target of plenty of such abuse, I can testify that it gets pretty raucous and rancid out there in cyberspace.
The Welsh journalist and documentary filmmaker Jon Ronson made exactly that point in his recent book “So You’ve Been Publicly Shamed.” As he put it, social media shaming has become a social menace.
Let the Pushback Begin
Consider just one of the memes veganistas love to circulate (along with the requisite photo of Der Führer): “When you say it’s okay if you want to be vegan, but you should respect my right to eat meat, that’s like me saying, it’s okay if you want to have Jewish friends, but you should respect my right to kill them.”
Where do you go with that statement? Once you reference Adolph Hitler, all dialogue ceases.
Which is the way most veganistas want it: No debate, no discussion — nothing but doctrinaire condemnation of anyone who uses or consumes any animal products whatsoever.
(One of the more ridiculous posts I read in researching this topic was that of a vegan woman who proudly noted that she refuses to drink Guinness beer, since it’s filtered using a small amount of gelatin derived from fish bladders).
Finally, though, there is beginning to be serious pushback.
As Mike Hale, a columnist for the Monterey Herald, recently wrote, “I’m a sharp-toothed meat-eater happily married to a rabid herbivore. Animal welfare groups have begun to monitor my words, urging me to expound the virtues of vegetarianism while indoctrinating me with anti-meat sentiments. [But] carnivores have grown tired of defending their choices or made to feel as if we need to end our meal with the words ‘Forgive me, father, for I have sinned.’ ”
Amen, brother.
Although you have to search for them, there are many more such statements posted in response to the steady drumbeat of “meat is murdering people and the planet:”

Odysseyonline.com: “Listen, if you’re vegan/vegetarian, cool. Why in the world is that a valid excuse for you to tell carnivores that they will live a short life, they're disgusting, they have no morals [and] the whole world hates them? I don’t get why you should be so grossed out by someone eating a burger. Would you like it if someone came up to you and started to humiliate you for what you are consuming, especially out in public?”
HuffPost.com: “We can eat meat and still be advocates for animal rights. Most meat eaters want animals to be treated as humanely as possible, and … on the flip side, many fruits and vegetables come from farms that exploit low-wage migrant workers. Are you, veggie or meat eater, eating that produce? We’re talking about human beings, not animals.”
Prince.org (dedicated to all thing Prince): “I have some friends who are vegan. I respect their choice, never question, never shame them, never talk down to them. When I invite a huge group of friends over, I will cook a vegan dish, too. Yet they still have to give me dozens of talks about what’s in meat, it’s killing this and that, all these facts about animals, and on and on.”

And on and on, ad nauseum.
I hate to sound preachy, but along with the pushback, we omnivores need to take the high road. Becoming a vegetarian is a fine choice, done for the right reasons. No need to condemn anyone who goes veggie, any more than we’d feel it’s okay to condemn someone who announces they never plan to have children.
Equally important, we need to remember that a diet including animal products is normal and natural; we don’t need to apologize for consuming such foods.
And by all means, we shouldn’t even think about going to the “Hitler was a vegetarian” card.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.