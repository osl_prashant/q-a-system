Food security an issue in Kodiak
Food security an issue in Kodiak

By ALISTAIR GARDINERKodiak Daily Mirror
The Associated Press

KODIAK, Alaska




KODIAK, Alaska (AP) — Food security is a hot topic in Alaska — in Kodiak, even more so. According to a 2014 study commissioned by the Alaska Department of Health and Social Services, 95 percent of the food Alaskans purchase is imported, although that statistic is currently under review.
Rachel Miller, an assistant professor of business at Alaska Pacific University who focuses food systems, said that when it comes to food, the state of Alaska is like an island.
"While Alaska's not a true island, our economy often acts like it," she said. "If you depend on somebody else for food, you are food vulnerable."
Miller went on to explain that Kodiak is "like an island on an island."
She took a number of trips to Kodiak in 2017 to work on food sovereignty research with the Kodiak Area Native Association and helped facilitate the first meeting of the KANA food co-op. Miller pointed to the high level of use of Kodiak's food banks as a clear symptom of the island's vulnerability when it comes to food networks.
"There are people who are hungry, seeking meals," she said. "That is a significant indicator in a food-insecure community."
Miller said several factors make Kodiak vulnerable, one of which is the fact that most of the food has to be shipped in. She cited a recent event as an illustration of why Kodiak should be working toward becoming more autonomous.
"Most recently, everyone experienced the tsunami warning," she said. "Kodiak is a vulnerable community because of the natural elements."
There are, however, locals making efforts to tackle the issue by setting up food co-ops to encourage the purchase of local produce and ways to become more self-sufficient.
One such grower is Midge Short, who has been an avid gardener for decades. Short lives on Anton Larsen Island with her husband, Bruce. The couple grows copious amounts of produce, including leeks, broccoli, cabbage, kale, rutabaga, cauliflower, turnip, spinach, artichokes, beets, zucchini and more. Midge described their gardens as extensive and said they have raised boxes, tunnels, hoop houses and greenhouses. This means they can eat fresh all year.
Earlier this month, Short gave a presentation at Kodiak Public Library, offering tips on growing plants in Kodiak. The nearly 30 people in attendance ranged from a few who were right at the beginning of their gardening practices to a group that manages a local food co-op.
"It is about food security — growing as much of your own food as possible," said Short at one point during the presentation.
Short began the presentation by talking about how diverse the different parts of Kodiak are in terms of climate and soil. She informed those new to gardening that there would be some trial and error involved in figuring out what works in their garden.
"Tonight's presentation is essentially about our gardening where we're at, and you can just glean whatever you can from it and adopt it to your own growing area," she said. "Every farming garden is unique . you'll develop your own systems and find out what works for you."
Short then broke down their gardening system.
"Raised boxes are some of our favorite early spring boxes, because they're raised up off the ground so that the soil warms up before the other beds. You can vent from four sides," she said. "If you have slugs, bunnies — it's much harder to get into the boxes, so it's more protected. We have a lot of boxes."
Short described which ingredients are most effective for nutrient-rich soil, which is important for the makeup of a good planting bed.
"You start off with a type of soil, which is either loam or loam mixed with volcanic ash," she said. "Incorporate leaf mulch, kelp, compost, beach peat, bio char, wood ash, bone-meal . incorporate as many organic mulch materials as you can into your bed."
This is not the first presentation Short has given to other local growers. As she started on a section of the talk devoted to the virtues of comfrey, she acknowledged that anyone who's heard her speak on compost before will know she is "a comfrey freak."
The reason, Short explained, that comfrey is so good to incorporate into the compost is that it's a bio-accumulator. This is because it has deep roots, which can access untapped nutrients that other plants miss. You can then harvest the nutrient and mineral-rich leaves and add them to your compost.
"It is the main green of our compost," said Short. "What you want to do is incorporate as many ingredients into your compost as possible."
Short went over the process of making biochar, which involves collecting wood, getting it burning at a very high temperature, and then shutting off all the oxygen, which forces the embers to turn to charcoal.
"It will crackle and crackle and crackle, and that's the charcoal being made," said Short.
Short explained that the biochar must be sifted before it's added to the soil, and the leftover dust can be used as a deodorizer in chicken coops.
During the rest of the hour, the gardeners listened as Short explained the importance of keeping a gardening diary, gave an overview of the proper methods for chitting and planting potatoes, ran down the best dates for planting certain plants, broke down the method of reproduction for leaks and more.
Short noted afterward that one can only brush the surface of gardening techniques over an hour. Following the presentation, many audience members were hungry for more details and stayed for close to another 30 minutes, badgering Short for further tips.
In the audience were Kevin and Caroline Goodman, who said that they'd just bought a home that has a greenhouse and hadn't done a huge amount of gardening before. Kevin Goodman said that last year, they grew some broccoli and potatoes, but nothing major.
"We're not really organized, so we're trying to get organized so that we can have a real garden," said Caroline Goodman. "We've got a lot of seeds. We just need to work out the soil and the compost."
The couple said that Short's presentation was very useful (Caroline Goodman exclaimed that she wishes Short would write a book) and that they're planning on growing a wider variety of vegetables this year.
"Hopefully we can do some cabbage, broccoli, lettuce, kale . " said Kevin Goodman.
On the other side of the spectrum is Janelle Solinger, who works with Kodiak Bounty Cooperative. The coop is a small farm on Woodland Drive, run by Judy Hamilton and Oskar Klausner, which produces a huge variety of vegetables, as well as fruit, flowers, chickens, rabbits, and dairy goats. Solinger said, one of the reasons she attended was to get Short's advice on planting a winter garden.
"We haven't done that so far. So, definitely kind of encouraged to do a winter garden," said Solinger.
This will be the third year that KBC's operations. Every summer, the coop offers a 10-week season of weekly vegetables boxes. Last year this cost each customer $350 and lasted from July to September. Ani Thomas is another of the coop's growers. Thomas said she came along because every time she speaks with Short about growing produce, she learns something new. Thomas also spent a lot of time chatting and swapping tips with other growers present.
"There's a lot of high caliber growers here," said Thomas. "So, just the camaraderie of the growing community is nice."
According to Thomas, food security was absolutely a factor in the conception of the coop. She said that economics is just one of the factors that should be considered when it comes to eating locally.
___
Information from: Kodiak (Alaska) Daily Mirror, http://www.kodiakdailymirror.com