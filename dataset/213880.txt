Chiquita Brands, Ft. Lauderdale, Fla., brought Bananamania, its newest campaign, to New Orleans during the Produce Marketing Association’s Fresh Summit.
From Oct. 19-21, expo attendees could witness different banana-themed occurrences throughout the city, according to a news release. The company wanted to give people the illusion they were going bananas.
Banana happenings that occurred included:

 Chiquita banana-shaped luggage going around with other luggage on baggage carousels at the airport;
 Parents pushing strollers with a Chiquita banana “baby”;
 Banana front desk clerks at hotels during check-in;
 A banana jazz band performing in Jackson Square;
 Banana tourists on a Segway tour near the convention center;
 A banana commuter riding the streetcar; and
 A hot dog stand serving Chiquita banana hot dogs.

“Even with all of the wheeling and dealing going on, professional conferences can get a bit tedious,” Wieden+Kennedy creative lead Mike Egan said in the release. “We wanted to bring a little dose of fun to the proceedings, while showing all of the big players in the produce game that we’re committed to building Chiquita’s iconic brand.”