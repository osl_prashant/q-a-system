The QuickTox for QuickScan has been improved to exceed the new 2016 performance criteria from USDA/GIPSA, according to EnviroLogix. The first Flex kit for mycotoxins is for T-2/HT-2 (webpage); Aflatoxin, DON and Fumonisin will follow.Flex advantages over the original QuickTox kits:
Accuracy - test accuracy is improved by eliminating adverse environmental effects
Time savings - the majority of Flex test kits will feature shorter run times and fewer dilution steps
Efficiency - test a wider variety of matrices utilizing the same QuickTox Strips
Wider range - more accurate results across a broader range of test results
Paired with the proprietary software included in the QuickScan System, QuickTox strips quantify mycotoxins in the ranges most important to you.
QuickTox for QuickScan still offers:
Ease of use - simple procedure, less "shaking," fewer pipetting steps, and no drying step - read the strips wet!
Quantification - A unique barcoding system containing lot-specific standard curve data ensures accurate and precise mycotoxin measurement while eliminating the need for calibrators or standards.
Traceability - Instantaneous data and image storage onto a host PC (or network) means test results can be emailed, printed or analyzed for management decisions at any time.