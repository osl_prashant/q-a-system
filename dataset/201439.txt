When rolling through your corn or soybean fields in the combine you’re looking for yield, yield, yield, but when you start seeing weeds rather than crop, you could be in for an unfortunate surprise and a challenging next year.
“It’s really critical that farmers recognize that there is a significant cost to those weeds out there,” says Bob Hartzler, Iowa State University Extension weed scientist. “It might not even affect yields or harvest efficiency this year, but it does influence their ability to successfully control weeds in the future.”
Take the right steps to make sure you gain control of the growing weed seed banks in your fields. Figure out what weeds are out there, if they’re resistant, if there was herbicide application error and how you can avoid this problem in the future.
Determine what happened—how did these weeds get here? “In Indiana, a lot of weeds got away due to excess rain,” says Bill Johnson, Purdue University Extension weed scientist. Alternatively, states with droughty conditions might not have gotten enough rainfall to adequately activate herbicides—which could lead to escapes.
In addition to weather concerns, you need to look for patterns in your fields. Does it look like just one or two rows are weedy in each sprayer pass? It could be a clogged nozzle, for example. However, if you’re noticing just one or two types of weeds throughout the field it could be a sign of resistance—you might be able to send tissue samples into a university to test for specific types of resistance, such as University of Illinois with waterhemp.
Don’t always assume weeds that make it to harvest are resistant, though. Take the time to analyze what you did right and wrong before claiming resistance. “This year many applications were made in hot periods that stressed the crop and weeds, and if weeds are under heat and drought stress, the herbicide won’t be as effective,” Hartzler says. “You could also have some that emerged after the final herbicide application.”
Don’t accept the status quo of just a few escapes. In the case of waterhemp, a single escape can produce a quarter of a million seeds that plague your fields for up to five years.
“Escapes allow the weed seed bank to build up and can allow new herbicide resistances to develop,” Hartzler says. “The two simplest solutions are to go to more narrow row spacing or include a residual herbicide with post emergent application.”
Many farmers gave residual herbicides a try this year, but weather could have hindered some of their success. Regardless, Hartzler says it’s worth the effort because weed seed build up can happen quickly and take years to reverse.
“The big learning point here is we cannot minimize our trips over fields to control weeds—a year like this just highlights the shortcomings of that approach,” Johnson says. “If you have weeds now, focus on minimizing seed spread.”
Clean out your combine after going through weedy fields or save weedy fields for last. If you aren’t past the herbicide application window [see herbicide labels for instructions] consider a herbicide application. In some cases cultivation could help reduce weed seed production, too.
Watch for prolific weeds. It’s never good to find weeds growing at the end of the season, but if it’s waterhemp, Palmer amaranth, giant ragweed or marestail it could mean even more trouble next year.







With little competition for sunlight and nutrients, waterhemp can produce up to 1 million seeds, though it’s more commonly about 250,000 per plant. Waterhemp seeds stay viable in the soil for about four years with 10% survival after three years and less than 1% survival after four years, according to Purdue Extension.














Like waterhemp, Palmer amaranth can produce up to 1 million seeds without competition, but more commonly produces about 460,000 seeds per plant. Seeds buried deeper than 2” in the soil likely won’t germinate, and after three years in the soil only 20% of the seeds are still viable, according to University of Georgia Extension.














Giant Ragweed can germinate even after being buried up to 6” deep in the soil, but the weed seed bank can be depleted by 99% in just two years. Each plant produces between 1,900 and 5,500 seeds when in competition with corn and soybeans, according to Michigan State University Extension.














Winter and summer annual marestail [horseweed] produces about 200,000 seeds per plant that can travel up to 400’ by wind. Marestail germinates more in the fall than any other time and goes dormant when buried deeper than 1” in the soil. Seed longevity hasn’t been extensively studied, though there is a report of finding viable seeds after 20 years, according to Michigan State University Extension weed science.







Use these notes—what weeds are present, how they got there and how much weed seed was produced to plan successful weed suppression next season. After all, the only good weed is a dead one.