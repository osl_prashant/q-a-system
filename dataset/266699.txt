Alaska Fish and Game denies hatchery on Baranof Island
Alaska Fish and Game denies hatchery on Baranof Island

The Associated Press

JUNEAU, Alaska




JUNEAU, Alaska (AP) — The Alaska Department of Fish and Game has announced that a proposal to open a salmon hatchery on Baranof Island has been rejected.
The March 29 rejection ended a disagreement between Juneau businessman Dale Young and the residents of Baranof Warm Springs, the Juneau Empire reported .
The springs are home to a small, seasonal community of about a dozen cabins at the southern tip of Baranof Island — known for its numerous hot springs and scenic waterfalls.
Jim Brennan, who has owned a cabin there since the 1950s, said the hatchery would have been "way out of whack with the scenery and recreational values of the bay." Brennan said a petition opposing the hatchery garnered around 1,000 signatures.
Young could not be reached for comment.
The state department's reasoning for rejecting the proposal were laid out in a letter to Young dated March 29. Fishery stakeholders and public comment are given priority in the permitting process, according to the letter.
Opposition from commercial fishing groups, the public and the Northern Southeast Regional Aquaculture Association effectively doomed the project, according to the letter.
The response from the City and Borough of Sitka, which annexed Baranof Warm Springs, and local property owners has been "overwhelmingly" against the project, according to the letter.
___
Information from: Juneau (Alaska) Empire, http://www.juneauempire.com