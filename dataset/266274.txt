Minnesota company recalls 48 tons of canned chicken products
Minnesota company recalls 48 tons of canned chicken products

The Associated Press

MADELIA, Minn.




MADELIA, Minn. (AP) — A Minnesota food company is recalling 48 tons of canned chicken products that may be contaminated with hard plastic.
The U.S. Department of Agriculture's Food Safety and Inspection Service said Wednesday that Tony Downs Food Co. of Madelia issued the recall.
The canned chunk chicken breast items were produced on Nov. 28 and Nov. 29, 2017. The recall involves 12.5-ounce cans and 50-ounce cans of Member's Mark chicken breast with the establishment number "P-65" inside the USDA inspection mark. The items were shipped to Sam's Club stores nationwide.
Two consumers found the plastic in cans and returned the product. There are no reports of anyone getting stick from eating the products.
Consumers who bought the products are urged not to eat them but instead discard or return them.