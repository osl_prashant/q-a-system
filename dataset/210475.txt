Efficient fermentation is designed to create a more palatable and digestible feed which encourages dry matter intake and improves performance. Six phases occur during the silage fermentation process.
Phase I
As the forage is harvested, aerobic organisms predominate on the forage surface. During the initial ensiling process, the freshly cut plant material, and, more importantly, the aerobic bacteria, continue to respire in the silo structure. The oxygen utilized in the respiration processes is contained within and between the forage particles at the time of ensiling.
This phase is undesirable because the aerobic bacteria consume soluble carbohydrates that might otherwise be available for the beneficial lactic acid bacteria or the animal consuming the forage. Although this phase reduces the oxygen to create the desired anaerobic conditions, the respiration process produces water and heat in the silage mass. Excessive heat buildup can greatly reduce the digestibility of nutrients.
Another important chemical change that occurs during this early phase is the breakdown of up to 50% of the total plant protein. The extent of protein breakdown (proteolysis) is dependent on the rate of pH decline in the silage. The acid environment of the silage eventually reduces the activity of the enzymes that break down proteins.
Phase I ends once the oxygen has been eliminated from the silage mass. Under ideal crop and storage conditions, this phase will last only a few hours.
Phase II
After the oxygen in the ensiled forage has been utilized by the aerobic bacteria, Phase II begins. This is an anaerobic fermentation in which the growth and development of acetic acid-producing bacteria occurs.
These bacteria ferment soluble carbohydrates and produce acetic acid.
Acetic acid production is desirable because it can be utilized by ruminants and additionally initiates the pH drop necessary to set up the subsequent fermentation phases. As the pH of the ensiled mass falls below 5, the acetic bacteria decline in numbers as this pH level inhibits their growth. This signals the end of Phase II.
Phase III
The increasing acid inhibits acetic bacteria and brings Phase II to an end. The lower pH enhances the growth and development of another anaerobic group of bacteria, those producing lactic acid.
Phase IV
This is a continuation of Phase III as the lactic acid bacteria begin to increase, ferment soluble carbohydrates and produce lactic acid. Lactic acid is the most desirable of the fermentation acids and, for efficient preservation, should consist of greater than 60% of the total silage organic acids produced.
Phase IV is the longest phase in the ensiling process because it continues until the pH of the forage is sufficiently low enough to inhibit the growth of all bacteria. When this pH is reached, the forage is in a preserved state. No further destructive processes will occur as long as oxygen is kept from the silage.
Phase V
The final pH of the ensiled forage depends largely on the type of forage being ensiled and the condition at the time of ensiling. Haylage should reach a final pH of around 4.5 and corn silage a final pH near 4. The pH of the forage alone is not a good indicator of the quality of the silage or the type of fermentation that has occurred.
Phase VI
This phase refers to the silage as it is being fed out from the storage structure. This phase is important because research shows that nearly 50% of the silage dry-matter losses occur from secondary aerobic decomposition. Phase VI occurs on any surface of the silage that is exposed to oxygen while in storage and in the feed bunk.
High populations of yeast and mold, or the mishandling of stressed crops, can lead to significant losses due to aerobic deterioration of the silage. Proper management is vital to reduce these losses and improve the bunk life of the silage.
To read more details about the phases of fermentation, click here.