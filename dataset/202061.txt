In an effort to build a positive image for Ohio's farmers and increase demand for pork, the Ohio Pork Council (OPC) strives to serve as a resource for all things pigs, pork and farming.Many OPC projects and programs are geared toward disseminating positive information about modern farms and how food is grown and raised in agriculture today. Recently, OPC has been recognized, by two national organizations, for specific projects and their overall work to tell the story of agriculture.
At their recent annual conference in Oklahoma City, Professional Ag Communicators, a community for those who work in the multi-faceted area of agriculture and livestock communications and marketing, recognized individuals and organizations from across the nation for their work in communicating the story of agriculture.
Through the competitive awards program, OPC received the following recognition:
1st Place Agricultural Website (OhioPork.org)
1st Place Facebook Page (Ohio Hog Farmers)
2nd Place Video (Antibiotics & Hormones in Pork) - Digital Specialist of the Year
Earlier this year, OPC was also recognized by the National Agri-Marketing Association (NAMA) and received Best of NAMA Region 6 Merit Award for their #PinkPork Pinterest Sweepstakes Campaign. The campaign was targeted toward consumers and encouraged utilizing proper cooking temperature when preparing pork.
Source: Ohio Porkline