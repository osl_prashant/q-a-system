July 2017 natural gas opened today at $3.05 -- down 23 cents from last Friday.
Farm Diesel is 1 cent lower on the week at an average of $1.97 per gallon.
July 2017 WTI crude oil opened the day at $48.04 -- 71 cents lower on the week.
July 2017 Heating oil futures opened the day at $1.50 -- down 4 cents from our last report.
Propane is unchanged at an average of $1.24 per gallon regionally.

Farm Diesel -- South Dakota led this week's diesel decline softening 7 cents per gallon as Nebraska fell 6 cents. Six states were unchanged as Iowa gained 2 cents and Illinois firmed a penny per gallon.
We waded into the murky waters of the farm diesel outlook this week, forecasting June and July at an average of $2.00, 3rd quarter 2017 at an average of $2.05 per gallon and 4th quarter 2017 at an average of $2.03. You can read our post "Remainder 2017 Farm Diesel Outlook Projects Flat Price Action" by clicking the link. A lot will depend on crude oil futures, but we generally believe farm diesel will continue within a narrow range through the end of the year.
This week's lower diesel price is encouraging but there is risk that a late season demand surge could support slightly higher short-term prices. If you need to top off, now would be a good time to do that although there are equal chances lower crude oil futures will outweigh demand-based increases.



Distillate inventories reported by EIA firmed 0.4 million barrels to 146.7 mmbbl. Stocks are currently 2.9 mmbbl below the same time last year.
The regionwide low currently lies at $1.80 in Michigan. The Midwest high is at $2.19 in Illinois.





Farm Diesel 6/2/17


Three Weeks Ago


Previous Week


Change


Current Week

 



Iowa


$1.97


$1.90


+2 cents


$1.92

Iowa



Illinois


$2.18


$2.18


+1 cent


$2.19

Illinois



Indiana


$2.10


$2.10


Unchanged


$2.10

Indiana



Ohio


$1.99


$1.93


Unchanged


$1.93

Ohio



Michigan


$1.96


$1.80


Unchanged


$1.80

Michigan



Wisconsin


$1.92


$1.92


Unchanged


$1.92

Wisconsin



Minnesota


$2.12


$2.05


-4 cents


$2.01

Minnesota



North Dakota


$2.08


$2.02


Unchanged


$2.02

North Dakota



South Dakota


$2.05


$2.05


-7 cents


$1.98

South Dakota



Nebraska


$1.98


$1.98


-6 cents


$1.92

Nebraska



Kansas


$1.95


$1.92


Unchanged


$1.92

Kansas



Missouri


$1.94


$1.94


-3 cents


$1.91

Missouri



Midwest Average


$2.02


$1.98


-1 cent


$1.97

Midwest Average



Propane -- Propane held steady this week as Indiana fell 7 cents and Iowa and Michigan each softened 3 cents per gallon. Six states were unchanged as Ohio gained a nickel and Minnesota firmed a penny.
March propane exports surged above February and March 2016 at 1.01 million barrels per day. That's nearly twice the level seen a year ago and roughly three times larger than the five year average. That raises concerns for summertime pricing. We have written before that infrastructure enhancements at export terminals and world demand for U.S. propane are encouraging LP export activity, and our expectation is that the increased demand will lead to higher domestic prices.
Last year we booked harvest and winter supplies at a regional average below $1. I do not expect to be so lucky this year. Prices have not yet fully come down from the seasonal winter highs so we have some time to see where we end up, but, word to the wise, make some extra room in your propane budget as we expect this summer's lows to run around $1.10-$1.15.



According to EIA, last week, national propane inventories firmed 3.446 million barrels -- now 2.9 million barrels below the same time last year at 47.132 million barrels.
The regionwide low is at $1.05 per gallon in Nebraska, and the regionwide high is in Missouri at $1.44.





LP 6/2/17


Three Weeks Ago


Previous Week


Change


Current Week

 



Iowa


$1.11


$1.11


-3 cents


$1.08

Iowa



Illinois


$1.39


$1.39


Unchanged


$1.39

Illinois



Indiana


$1.37


$1.37


-7 cents


$1.30

Indiana



Ohio


$1.20


$1.20


+5 cents


$1.25

Ohio



Michigan


$1.42


$1.46


-3 cents


$1.43

Michigan



Wisconsin


$1.20


$1.20


Unchanged


$1.20

Wisconsin



Minnesota


$1.17


$1.17


+1 cent


$1.18

Minnesota



North Dakota


$1.19


$1.09


Unchanged


$1.09

North Dakota



South Dakota


$1.09


$1.09


Unchanged


$1.09

South Dakota



Nebraska


$1.05


$1.05


Unchanged


$1.05

Nebraska



Kansas


$1.34


$1.34


Unchanged


$1.34

Kansas



Missouri


$1.42


$1.45


-1 cent


$1.44

Missouri



Midwest Average


$1.25


$1.24


Unchanged


$1.24

Midwest Average