Anhydrous is $72.08 below year-ago pricing -- lower 26 cents/st this week at $521.06.
Urea is $11.38 below the same time last year -- lower 85 cents/st this week to $362.19.
UAN28% is $31.08 below year-ago -- higher 3 cents/st this week to $248.79.
UAN32% is priced $27.92 below last year -- higher 17 cents/st this week at $280.98.

Urea led declines in the nitrogen segment amid scant price movement. South Dakota led declines falling $10.30 and Illinois softened $1.14. Six states were unchanged as North Dakota urea firmed $1.39 as Minnesota gained 16 cents.
Anhydrous fell mildly on the week led by North Dakota which softened $4.33, Illinois fell 48 cents per short ton and Kansas fell 8 cents. Seven states were unchanged as Nebraska, our only gainer in NH3, added $1.63.
UAN32% firmed very mildly led by Illinois which gained $2.08 as Nebraska firmed 22 cents. Nine states were unchanged as Kansas fell 59 cents, our only decliner.
UAN28% firmed 8 cents by the short ton led by Nebraska which added $1.64. Here again, nine states were unchanged as South Dakota softened 94 cents and Illinois fell 39 cents.
Either the nitrogen market is placing a top or it is consolidating for another run higher. Demand will likely be the key. We believe UAN solutions have still not realized their full upside potential, as evidenced by the fact that urea and NH3 were down this week as UAN firmed. Granted, UAN's collective 20 cent rise cannot really be considered a major move. I believe the most important item here is the direction.
Both UAN28% and 32% firmed in the face of fading urea and anhydrous prices. Technically, UAN28% is still at a slim discount to anhydrous ammonia on an indexed basis which has historically been a signal of near-term price support for the entire nitrogen segment. Since sidedress season is still a ways off, it is possible that UAN28% is positioning itself to say "I told you so" in the event of a liquid nitrogen price spike.
If UAN28% is to firm to a level comparable to urea, we would expect it to top around $260 per short ton, roughly 12 bucks above today's price. At that level, on an indexed basis, UAN28% would actually be priced above urea. In the past we have noted that as a signal of downward near-term price action. So if UAN28% is coiling to spring above urea on the basis of post emerge demand, there is a precedent for considering that a harbinger of lower late-summer nitrogen prices.
On a less ethereal level, price action seems to confirm that nitrogen supplies are ample and that a 4 million acre decline in expected corn acreage in 2017 is keeping the lid on nitrogen prices. We are currently priced well below expected new-crop revenue, and if corn futures can hang in there at or above current levels, savings on nitrogen for 2017 corn acres will support profit margins on the farm.
December 2017 corn closed at $3.84 on Friday, April 7. That places expected new-crop revenue (eNCR) per acre based on Dec '17 futures at $608.14 with the eNCR17/NH3 spread at -87.08 with anhydrous ammonia priced at a discount to expected new-crop revenue. The spread narrowed 6.52 points on the week.





Nitrogen pricing by pound of N 4/12/17


Anhydrous $N/lb


Urea $N/lb


UAN28 $N/lb


UAN32 $N/lb



Midwest Average

$0.32 1/4


$0.40 1/4


$0.44


$0.44



Year-ago

$0.37


$0.41 1/4


$0.49 3/4


$0.47 3/4





The Margins -- UAN32% is at a 1 3/4 cent premium to NH3. Urea is 3 cents above anhydrous ammonia; UAN28% solution is priced at parity NH3. The margins continue to narrow.





Nitrogen


Expected Margin


Current Price by the Pound of N


Actual Margin This Week


Outstanding Spread




Anhydrous Ammonia (NH3)


0


32 1/4 cents


0


0




Urea


NH3 +5 cents


40 1/4 cents


+8 cents


+3 cents




UAN28%


NH3 +12 cents


44 cents


+11 3/4 cents


-1/4 cent




UAN32%


NH3 +10 cents


44 cents


+11 3/4 cents


+1 3/4 cents