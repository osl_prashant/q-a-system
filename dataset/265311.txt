BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Fri.Thu.No. 2 Soft wheat4.51¼4.46¾No. 1 Yellow soybeans10.03¼10.04¾No. 2 Yellow Corn3.57¼e3.56eNo. 2 Yellow Corn3.75¼p3.74p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted