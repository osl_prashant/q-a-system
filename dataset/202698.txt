California Giant Berry Farms has published a limited-edition cookbook for consumers. 
 
The 90-page book contains more than 30 recipes and is available for purchase via www.calgiant.com or lulu.com.
 
Farm, Family & Food was created to thank consumers within the California Giant database that engage with the brand multiple times a year and provide more insights into their shopping behavior and preferences, according to a news release.
 
"This has truly been a labor of love pulling stories, recipes and photography together to capture some of the family behind our brand," said Cindy Jewell, vice president of marketing, in the release. 
 
All cookbook sales proceeds will support the California Giant Foundation and the continued effort to provide kids great produce and other nutrition programs both locally and nationally, according to the release.