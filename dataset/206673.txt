As preparations to develop the next farm bill continue, the American Farm Bureau Federation penned a letter to key congressional agricultural committee members spelling out its top priorities.
“These recommendations are not set in stone,” the letter reads in part. “Rather, they are designed to provide the necessary flexibility to ensure that Farm Bureau is prepared to work with you in achieving the best possible farm bill that meets our key farm policy objectives while assisting you in meeting the challenges this important legislation will endure.”
In the letter, AFBF proposes the following:
Protect current farm bill spending.
Maintain a unified farm bill that includes nutrition programs and farm programs together.
Ensure any changes to current farm legislation be an amendment to the Agricultural Adjustment Act of 1938 or the Agricultural Act of 1949.
Prioritize top funding concerns, including risk management tools such as federal crop insurance and Title 1 commodity programs.
Ensure programs are complaint with World Trade Organization agreements.
The AFBF board also passed along a few farm policy recommendations based on these goals:
Let farmers choose the higher value – either the five-year Olympic Average yield for ARC-CO, or a simple 10-year average yield.
Increase the reference price used as a floor for ARC-CO by 5% for corn, soybeans, wheat, sorghum and other minor crops.
Designate cottonseed as an “other oilseed” so it is eligible for Title 1 commodity support programs, or support a cotton lint program.
Improve the Dairy margin Protection Program with a handful of new provisions.
Increase the annual cap on livestock insurance products from $20 million to $75 million.
AFBF president Zippy Duvall says U.S. farmers and ranchers need a strong farm bill now more than ever.
“Because of low commodity prices, many of America’s farmers and ranchers are struggling,” he says. “The risk management and safety net provisions of farm bills are most important in times like these.”