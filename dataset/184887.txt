Corn




The national average corn basis softened 1 cent from last week to 11 1/2 cents below July futures. The national average cash corn price softened 2 cents from last week to $3.62 1/4.
Basis is weaker than the three-year average, which is 1 cent above futures for this week.












Soybeans





The national average soybean basis firmed 4 1/4 cent from last week to stand 20 3/4 cents below July futures. The national average cash price softened 3/4 cent from last week at $9.49 1/2.
Basis is softer than the three-year average, which is 4 1/2 cents above futures for this week.












Wheat





The national average soft red winter (SRW) wheat basis firmed 1/4 cents from last week to 9 3/4 cents below July futures. But the average cash price dropped 22 cents from last week to $4.22. The national average hard red winter (HRW) wheat basis firmed 3 1/2 cents from last week to 80 1/4 cents below July futures. The average cash price dropped 20 1/2 cents from last week to $3.59 1/4.








SRW basis is weaker than the three-year average of 16 cents under futures. HRW basis is much weaker than the three-year average, which is 38 cents under futures for this week.











Cattle









Cash cattle trade averaged $144.60 last week, up $8.38 from the previous week. Cash trade began today at around $1.50 lower than week-ago. Last year at this time, the cash price was $126.59.
Choice boxed beef prices have firmed $11.99 from last week at $244.58.  Last year at this time, Choice boxed beef prices were $213.71.











Hogs








The average lean hog carcass bid firmed $6.50 over the past week to $67.64. Last year's lean carcass price on this date was $75.61.
The pork cutout value improved $4.79 from last week to $80.00. Last year at this time, the average cutout price stood at $82.68.