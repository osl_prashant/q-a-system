Agroson’s LLC of New York City has issued a recall related to Mexican papayas grown and packed by Carica de Campeche as a precaution, because other brands that also buy from the farm have tested positive for salmonella.
 
“As of today no illness have been reported from our maradol papaya Cavi brand but we are doing a voluntary recall in cooperation with the FDA,” the company said in a news release.
 
The Aug. 4 release, posted on the Food and Drug Administration website, said that Agroson’s LLC is recalling 2,483 boxes of Cavi brand maradol papaya, grown and packed by Carica de Campeche.
 
The recall notice said product was distributed to wholesalers in New York, Connecticut and New Jersey from July 16 to 19.
 
The papayas were available for sale until July 31, according to the recall notice. Consumers can identify the papayas by PLU sticker, cavi MEXICO 4395.
 
Wholesalers, according to the recall, can identify the product by codes found above the handle on the master carton. Codes include 3044, 3045 and 3050. The farm that grew the papayas is also listed on the upper left side of the master carton, CARICA DE CAMPHE.
 
No other papayas distributed by Agroson’s LLC are subject to the recall, according to the notice.
 
The recall has already been communicated to all wholesale customers who received the papayas, and recall checks are underway by Agroson’s LLC, according to the release.
 
The recall was put in motion after Agroson’s LLC was notified Aug. 2 by the FDA that several brands of maradol papaya from the farm, Carica de Campeche, had tested positive for salmonella, according to the release.  None of the brands were specifically Cavi brand, the recall notice said, but as a precaution FDA recommended a recall of all papayas imported in the month of July from this farm.
 
Agroson’s LLC has stopped importing papayas from the grower, Carica de Campeche, and the recall notice said the company is taking precautionary measures by testing samples of all of its imported produce for salmonella with a private lab.
 
Consumers who may have purchased the Cavi brand papayas are advised not to eat them. Questions about the recall can be directed to Agroson’s at 917-801-1495 from Monday through Friday, 9 a.m. to 4 p.m. Eastern.
 
The Centers for Disease Control has named maradol papayas from the Carica de Campeche farm in Mexico the likely source of the outbreak that has sickened 109 people in 16 states that have been infected with Salmonella Kiambu or Salmonella Thompson strains. One death has been reported and 35 people have hospitalized, according to the CDC.