EXCHANGE: Group help farmers refine their operations
EXCHANGE: Group help farmers refine their operations

By ISAAC SMITHThe Southern ILLINOISAN
The Associated Press

ANNA, Ill.




ANNA, Ill. (AP) — Farming is hard — it's not only physically taxing, but can also be difficult for the farmer to find themselves in the black financially after all is said and done.
A new series from Food Works hopes to change that for small farmers.
Starting March 5, the Tune Up Workshops in Du Quoin have sought to help local farmers refocus their efforts — what is profitable and what is not? What is the best market to be in? Is the farmer's life balanced?
"Whether you are growing vegetables, berries, mushrooms, or duck eggs, or producing goat's milk soap, bedding plants, jams or pickles, we've found that after a few years, small farm entrepreneurs often are ready to update their business plans, expand, prune unprofitable activities, add enterprises, or involve other farm partners," the Food Works website says of the series. It also says the events aim to help farmers "target their tune-up to their most pressing challenges, learn about tools to get the job done, and grow their network of mentors and allies to take their farm into its next decade."
Kathleen Logan Smith, Food Works' director, said her organization has offered an introduction to small farming for those with one to two years of farm experience, but came to the realization that this wasn't doing quite enough.
"What we discovered is the farmers who go through that program continue to move on, but then when they get to year four, five, and six, they need to go back and revisit their skills," Logan Smith said. "They are at a place where they are looking to either make more money from their operation or to add another family member to the operation."
So, she said she and her team came up with the Tune Up series, which has focused on profitability and marketing. During its final session on Monday, legal concerns, including insurance, were the topic. She said she has seen a lot of wide eyes during the presentations. One set came from Phil Mendenhall.
Mendenhall has done a lot things in his life — he is a licensed registered nurse, he's been a chef, worked on an oil rig, taught yoga and mowed lawns.
Now, he farms.
Almost 10 years ago when he bought his farm in Anna, he was still working as a nurse for both Touch of Nature and what is now NeuroRestorative, as well as mowing lawns. He said doing all these things together as well as trying to establish a successful — or at least sustainable — beef and haying business made farming full time seem at least doable if not easier. He thought to himself, "Something's got to give."
"My life was so complicated, I wanted to simplify it over the long run," Mendenhall said.
This made him look at his operation and think pragmatically about its future.
Mendenhall said his body is not what it used to be, and the vegetables are the most physically taxing and least economically viable component that he has.
He said he can watch $4 come out of his hay bailer every five seconds, or can work half an hour breaking his back picking green beans to maybe make the same amount.
He said in the first Tune Up session, he was told to write down every expense and look at the big picture.
"It was a big eye-opener for me," Mendenhall said, adding that he found his farm has "been running in the red most its life."
Logan Smith said this was the intent behind the Farm Tune Up workshops. She said some farmers simply got into the business because they grew things and decided to sell them, but didn't understand the business as much.
Through these workshops, farmers have been encouraged to drill down into the weeds and understand the nuances of their financials. What are balance sheets and income flow statements saying about the farm? How can those documents direct decision making? Logan Smith said this can't be the only factor in a farmer's choices, but can at least help make "informed decisions."
Like Mendenhall thinking about how physically demanding and financially challenging growing vegetables is for him, Logan Smith said farmers need to look at all sides of their operation to find ways to keep the farm sustainable for the long term.
"You've got to look and see what your resources are," she said. This includes growing things the market will buy.
"While you may be feeding all your friends for free, that's not a business."
The workshops will wrap up Monday, and Logan Smith said there will be a special panel of farmers who remember what it was like to be small — each will have between 15 to 20 years of experience and will share some of their success and failures. Logan Smith said these failures might be particularly beneficial.
"It's a whole lot cheaper to learn from other people's mistakes," she said. "That way you make fresh mistakes that are all your own."
Logan Smith said while this is the first year for the series, she is looking forward to tweaking the formula and offering the classes again next year.
"I like to see this as a seed," she said.
___
Source: The (Carbondale) Southern Illinoisan, http://bit.ly/2tYsCBX
___
Information from: Southern Illinoisan, http://www.southernillinoisan.com


This is an AP-Illinois Exchange story offered by The (Carbondale) Southern Illinoisan.