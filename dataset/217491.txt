Mother Nature appears to be the only one with a solution to a whitefly infestation that caused heavy losses to Georgia fall crops.
Snap beans, squash, cucumbers and eggplant are some of the commodities that have been affected by a skyrocketing whitefly population and accompanying viruses.
Back-to-back mild winters allowed the fly to flourish, and the situation has gotten bad enough that some growers are considering abandoning crops that are particularly susceptible to infestation, said Charles Hall, executive director of the Georgia Fruit & Vegetable Growers Association.
“It’s been a very, very major issue that growers have been dealing with,” Hall said.
He did not know of any companies that had not been affected by the pest, which hurts the productivity of plants and brings down quality as well.
Growers have few options to combat whiteflies. They can spray more, but that becomes very expensive, and it might still not work, Hall said.
“You could spray every hour probably and you still couldn’t control them all, there’s so many,” said Austin Hamilton, who works in farm operations at Norman Park, Ga.-based Southern Valley Fruit & Vegetable.
Steve Veneziano, vice president of sales and operations for Oakes Farms, which grows in Georgia and Florida, described the whiteflies as “absolutely horrific.”
They destroyed squash and did significant damage to eggplant this fall, Veneziano said.
Hamilton reported heavy losses in squash and cucumber. Whiteflies had done serious damage in fall 2016 as well, but this year was worse.
Terry England, a member of the Georgia House of Representatives, is working on getting more money in the budget for whitefly research, according to the Valdosta (Ga.) Daily Times. Until those efforts begin to produce results, the best hope for limiting the whitefly will be freezing temperatures.
Hall and others in the industry hope legislative assistance will provide funding to set up best practices and formulate more effective pesticides.
“If they don’t and the whitefly infestation continues, there are certain crops we’re just going to get out of the business with,” Hall said.