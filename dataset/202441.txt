Suppliers and retailers will have a couple of major strawberry occasions to help them lure consumers to the produce department just as California strawberry season starts to ramp up.
Easter (April 16) and Mother's Day (May 14) are the two biggest strawberry holidays, said Anthony Gallino, vice president of sales for California Giant Berry Farms, Watsonville, Calif.
"Everybody and their mother is looking for berries during those holidays," he said.
"Everybody will be running promotions for both periods."
"Easter is huge," agreed Stuart Gilfenbain, sales and marketing director for Eclipse Berry Farms LLC, Los Angeles. "Everybody promotes for Easter."
Mother's Day, which follows right behind, is big, as well, he said.
The year's first strawberry occasion is Valentine's Day, and while some growers suffered a washout for the Feb. 14 event this year, the day was a hit for Eclipse Berry Farms.
"Valentine's Day was outstanding this year," Gilfenbain said. "We had cool weather that gave us really large strawberries in Oxnard, so we were able to do lots of stems."
The company also sourced Valentine's Day strawberries with stems out of Mexico, he said.
The fact that Easter falls relatively late this year is good for strawberry suppliers because rain put a damper on the start of the season, said Paul Allen, president and sales director for Main Street Produce Inc., Santa Maria, Calif.
"We'll just be rolling into a little bit of volume," he said.
"We'll have good volume for Mother's Day," he added, though Easter supplies could be "pretty light."
Red Blossom Sales Inc., Salinas, Calif., is ready for the upcoming spring holidays, said Michelle Deleissegues, marketing director.
"Santa Maria will have terrific quality and supplies," she said.
"A late Easter just means more volume and usually better quality," she said. "April is prime strawberry season for California."
Gallino said he would like to see promotions continue from Mother's Day through Memorial Day.
"You have multiple areas going at one time," he said, which means ample volume and "generally a good, promotable price."
The situation is a bit different for the Fourth of July.
"You're competing with all the summer fruit (around the Fourth of July), but berries are definitely a driver in the category."
Looking ahead to September, Labor Day also is a strawberry occasion.
"We'll have great volume all the way through September," Gallino said. "The majority of retail and foodservice has to have them every week."
Main Street Produce likely will have triple the strawberry volume for Mother's Day than it will have for Easter, Allen said, so he hopes retailers will continue to promote between Easter and Mother's Day.
"Those are two really good holidays for the stores," he said.
Since picking in Santa Maria got pushed back by rain and colder weather, Gilfenbain expects 85% of the Easter volume from Eclipse Berry Farms to come from Oxnard with 15% to 20% to come from Santa Maria.
By Mother's Day, Oxnard should have 60% of the volume and Santa Maria 40%.
He said he expects to see increased sales of 2- and 4-pound clamshells for Easter because of numerous family get-togethers.
While orders seem to increase about 30% for Easter, the bump is about 15% or 20% for Mother's Day.
"Easter is much larger than Mother's Day, but Mother's Day is still a good merchandising opportunity that retailers take advantage of every year," he said.