Wisconsin farmers interested in growing industrial hemp
Wisconsin farmers interested in growing industrial hemp

The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — Wisconsin farmers strapped by stagnant dairy and grain markets are expressing interest in growing industrial hemp this year.
Brian Kuhn is the director of the plant industry bureau at the state Department of Agriculture, Trade and Consumer Protection. He tells the Wisconsin State Journal that more than 50 producers have applied since March for one-time licenses to grow industrial hemp.
Kuhn says farmers should be careful not to rely on the crop to solve their financial problems. He says farmers also must pay attention to rules for selling hemp, which differ from rules for selling corn, soybeans and other mainstream crops.
State lawmakers approved a research pilot program in November allowing farmers to grow industrial hemp. The crop is used for rope, clothing, milk, soaps, building materials and biofuels.
___
Information from: Wisconsin State Journal, http://www.madison.com/wsj