Chicago Mercantile Exchange lean hogs fell on Friday for a second straight day, hit by recent cash hog price weakness and mounting worries about a potential trade war between the United States and China, traders said. Beijing said on Friday it was considering imposing an additional tariff on U.S. products including an extra 25 percent duty on pork. The United States exports roughly one quarter of the pork it produces.
In January U.S. pork exports to China/Hong Kong totaled 31,997 tonnes, down 16 percent from a year ago, largely due to increased production there, according to the U.S. Meat Export Federation. “Tariffs are driving the news today in the marketplace,” said Rosenthal Collins broker James Burns.
To some traders prices for slaughter-ready, or cash, hogs may be of greater significance than the tariff issue because the Chinese have not been a major buyer of U.S. pork for some time, he said.
Packers balked at paying more for hogs as supplies begin to build seasonally due to warmer spring weather that allows pigs to gain weight faster.
Investors are monitoring domestic pork demand as grocers finalize Easter ham purchases while stocking meat cases to accommodate spring grilling demand. April hogs closed down 2.900 cents per pound at 58.425 cents. May finished 2.925 cents lower at 65.225 cents.
Cattle Retreat Before Report
CME live cattle futures sank to a four-month low led by weaker wholesale beef prices and investor caution before the U.S. Department of Agriculture monthly Cattle-On-Feed report at 2 p.m. CDT (1900 GMT), said traders.
Analysts polled by Reuters, on average, believe 4.2 percent more cattle entered U.S. feedlots last month than a year earlier.
CME live cattle absorbed this week’s disappointing prices for market-ready, or cash, cattle and the prospect of burdensome supplies ahead, said traders.
This week packers paid $125 to $126 per cwt for cash cattle in the U.S. Plains that a week earlier fetched $126 to $128.
Processors paid less for cattle in anticipation of increased supplies in the coming months, while supermarkets stocked up on beef to feature after the Lenten season, said analysts and traders.
April live cattle closed 2.100 cents per pound lower at 116.050 cents, and June ended 2.200 cents lower at 106.200.
CME feeder cattle futures tracked lower live cattle contracts.
March feeders ended 1.825 cents per pound lower at 135.700 cents.