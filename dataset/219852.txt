Purchasing Salinas, Calif.-based Mann Packing opens a number of doors for Fresh Del Monte Produce.
The $361 million deal is expected to close in the next couple of months, and the companies have expressed high hopes for the endeavor.
“This acquisition will allow Del Monte to diversify our business, leverage our distribution network and infrastructure and ultimately increase our market reach of premium vegetable options,” said vice president of marketing Dionysios Christou.
Bananas are a significant segment for Coral Gables, Fla.-based Del Monte, but fresh-cut has been a growing area for the company for several years.
MORE: Del Monte continues diversification with purchase of Mann Packing
Mann specializes in value-added, from its Culinary Cuts and Nourish Bowls to its Veggie Slaw Blends and snack trays. The company did $535 million in sales last year.
Christou said Del Monte does not expect any major changes in the Mann Packing organization, and Mann’s leadership will continue to be involved in the combined business.
In addition, the Mann's brand will continue.
“We plan to develop a joint brand strategy to ensure that both brands (Mann’s and Del Monte) are used appropriately,” Christou said. “Depending on the product, the Mann brand or Del Monte brand may be used, whichever makes the most sense and has the best fit for that particular item.”