Idaho professor using past to explain modern stereotypes
Idaho professor using past to explain modern stereotypes

By ALYSEN BOSTONMoscow-Pullman Daily News
The Associated Press

MOSCOW, Idaho




MOSCOW, Idaho (AP) — When Rebecca Scofield was growing up on a cattle ranch outside Boise, she never thought she would end up attending Harvard University.
The push to apply came from one of her professors while she was finishing her bachelor's degree in history at Willamette University in Salem, Oregon.
"She was like, 'Well, I think you should apply to this program at Harvard in Japanese studies,' and I was like, 'That's hilarious,' " Scofield said.
But she applied anyway, and Scofield, now a professor at the University of Idaho, spent the next six years earning her master's degree in Japanese studies and a doctorate in American studies from Harvard.
Scofield, who lives in Moscow, said she took an array of liberal arts classes while she was an undergraduate at Willamette University, but she eventually developed a passion for Japanese studies.
"There was a strong presence of Japanese study-abroad students on our campus at Willamette, and I fell in love with studying Japan and went and lived there for a year," Scofield said.
Scofield said she was inspired to study the American West when she stumbled across a cowgirl fashion store called Rodeo Clowns in Tokyo while conducting research on the fashion industry for her master's degree.
"They were selling jean shorts and cowgirl boots and flannel shirts to Japanese teenagers, and I was just like, 'What has happened?' " Scofield said. "I was literally on the opposite side of the world, and the American West was following me."
This interest led Scofield to study the rise of the "urban cowboy" phenomenon, where people who did not grow up in rural areas choose to dress in Western-styled clothing and to identify with the stereotypical ideal of what it means to be a cowboy, she said.
"This coalesced with the rise of the New Right and (Ronald) Reagan's ascension and this re-masculinized idea that we all need to be cowboys again and man up," Scofield said. "I've just always been baffled with the appropriation of this image by people who aren't working class, who've never done this (job) and just use it to code masculinity in a way that's deeply problematic."
Scofield experienced the downside of this stereotype firsthand. Her mother, the daughter of a cattle rancher, inherited a ranch in Emmett, Idaho, that she ran throughout Scofield's childhood.
"Our town refused to believe that my grandfather had left his ranch to his youngest daughter instead of one of his sons," Scofield said.
Despite growing up in a rural area, Scofield said she wasn't particularly interested in ranching and wanted to go a college out of state.
"I was always the farm kid trying to read off in the corner and not get caught," Scofield said. "Honestly, I could not leave the area fast enough."
But after years spent on the East Coast, Scofield said she began to miss home.
"(At Harvard), there's this East Coast focus that really sees the American West as an entire flyover region, and it was the first time I ever got fairly defensive about where I was from," she said.
Since her spouse was also from Emmett, Scofield decided to move back to Boise before she was hired as an assistant professor in history at the UI. Through a seed grant funded by the UI, Scofield was able to begin an oral history project focusing on the performance of gay-friendly rodeos. The project supported her book, "Outriders: Rodeo at the Fringes of the American West," a project that started as her Ph.D. dissertation at Harvard.
Scofield said she was intrigued by how rodeo participants put their lives on the line as a way to earn their place in their communities.
"It's how people define themselves as Western, how they prove themselves to be Western, even when other people have said they're not," Scofield said. "A lot of people I interviewed talked about this feeling of, because they were gay, it excluded them from being Western, so they had to flee their rural towns out of fear of being outed."
As a professor, Scofield said it is important for students to get a clearer picture of what life was like in the American West in order to reduce the stereotypes put forth by both literature and film.
"The reality is, there were people (engaged in same-sex relationships) in the American West in the 19th century, and there were people that we would consider transgender just walking around the world," Scofield said. "These ideas of what it means to be a cowboy are literally 20th century constructions put forth by Hollywood and Louis L'Amour, and they have nothing to do with what it actually meant to be a working cowboy and live in these sparsely populated areas."
By telling these stories, Scofield hopes her research will be able to combat stereotypes not only in the West but throughout the country.
"I hope this can be a place that accepts (my son) when he grows up to be whoever he is, whatever he wants to be," she said. "I hope that we can grow into a community that no longer operates on decades-old stereotypes."
___
Information from: The Moscow-Pullman Daily News, http://www.dnews.com