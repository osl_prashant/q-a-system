Crunch Pak, Cashmere, Wash., is encouraging people to eat apples at the movies by giving away movie-themed prizes during their Movie Magic Sweepstakes.
"Snacking occasions in many households are on the rise, and nearly impossible to avoid while watching movies with friends and family," Krista Jones, director of marketing for Crunch Pak, said in a news release. "People reach for our products while on the go, so why not include them as part of a delicious and healthy movie snack?"
From March 1 to April 12, participants can enter to win free movies for one year and weekly gifts, such as gift baskets with gift cards, movies, popcorn and Crunch Pak products and swag.
The promotion will also be publicized by food bloggers, and the company expects thousands of entries to the contest.
Crunch Paks snacks feature characters licensed from Disney and Marvel.
For more information about the Movie Magic Sweepstakes, visit www.crunchpak.com.