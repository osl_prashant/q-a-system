BC-Cash Prices, 1st Ld-Writethru,0342
BC-Cash Prices, 1st Ld-Writethru,0342

The Associated Press

NEW YORK




NEW YORK (AP) — Wholesale cash prices Thursday
                                         Thu.       Wed.
F
Foods

 Broilers national comp wtd av             .9142      .9142
 Eggs large white NY Doz.                   2.03       1.99
 Flour hard winter KC cwt                  16.40      16.55
 Cheddar Cheese Chi. 40 block per lb.     2.1325     2.1325
 Coffee parana ex-dock NY per lb.         1.2067     1.2043
 Coffee medlin ex-dock NY per lb.         1.3994     1.3976
 Cocoa beans Ivory Coast $ metric ton       2462       2462
 Cocoa butter African styl $ met ton        6596       6596
 Hogs Iowa/Minn barrows & gilts wtd av     62.57      62.91
 Feeder cattle 500-550 lb Okl av cwt      183.25     183.25
 Pork loins 13-19 lb FOB Omaha av cwt      90.80      92.24
Grains
 Corn No. 2 yellow Chi processor bid      3.81½       3.75¼
 Soybeans No. 1 yellow                   10.39       10.40¼
 Soybean Meal Cen Ill 48pct protein-ton  386.50      391.00
 Wheat No. 2  Chi soft                    4.99¼       4.97¼
 Wheat N. 1 dk  14pc-pro Mpls.            7.69¾       7.50 
 Oats No. 2 heavy or Better               2.80        2.80 
Fats & Oils
 Corn oil crude wet/dry mill Chi. lb.     .30          .30 
 Soybean oil crude Decatur lb.            .30½         .30¾
Metals
 Aluminum per lb LME                     0.9581      0.9688
 Antimony in warehouse per ton             8700        8700
 Copper Cathode full plate               3.1174      3.1607
 Gold Handy & Harman                     1321.00    1329.40
 Silver Handy & Harman                    16.488      16.465
 Lead per metric ton LME                 2419.50    2436.00
 Molybdenum per metric ton LME            15,500     15,500
 Platinum per troy oz. Handy & Harman     954.00     959.00
 Platinum Merc spot per troy oz.          952.30     962.10
 Zinc (HG) delivered per lb.              1.4909      1.5126
Textiles, Fibers and Miscellaneous
 Cotton 1-1-16 in. strict low middling     79.83      79.83
Raw Products
 Coal Central Appalachia $ per short ton   63.10      63.10
 Natural Gas  Henry Hub, $ per mmbtu        2.77       2.73
b-bid a-asked
n-Nominal
r-revised
n.q.-not quoted<29
n.a.-not available