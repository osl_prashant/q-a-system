When he was running for president in 2016, then-candidate Donald Trump made a campaign promise to fix the trade deficit. 

When he accepted the nomination at the Republican National Conventionon July 21, 2016, he said the U.S. will “no longer enter into these massive deals…we are going to enforce all trade violations, including through the use of taxes and tariffs, against any country that cheats.”

In his more than one year in office, President Trump is holding true to that pledge, vowing to fix the U.S. trade deficit with China by leveraging tariffs, beginning with steel and aluminum.

In that same speech, Trump said he would out an end to “China’s outrageous theft of intellectual property, along with their illegal product dumping, and their devastating currency manipulation.”

On Thursday, he announced his administration was looking into the possibility if $100 billionof additional tariffs on China would be appropriate after the country rolled out a list earlier this week targeting 106 U.S. products. Some of the items on the list include corn, soybeans, sorghum, whisky, some sparkling wines, cherries and apples.

Last week, China implemented a 25 percent tariff on pork. Out of all protein sectors in the U.S., roughly 16 to 17 percent is exported.

According to Jim Mintert, director of the center for commercial agriculture at Purdue University, “it’s underappreciated how important trade is in the meat sector.”

“Exports across the board in meats were trivial three decades ago,” he said. “Pork exports have increased 52-fold since the mid-1980s.”

According to Chris Hurt, ag economist at Purdue University, one can’t compare the pork tariffs to possible tariffs placed on U.S. soybeans. 

“Soybeans are much different than what pork is,” he said on U.S. Farm Report.

China produces 97 percent of their own pork, but China’s soybean production, on the other hand, is only 13 percent of what they need. 

Roughly 30 percent of all soybeans consumed in China come from the U.S. Hurt believes if China places a tariff on U.S. soybeans, it will restrict the flow of soybeans and could turn the U.S. from a preferential supplier to foreign markets to a residual supplier.

“The downward pressure on prices could be measured in dollars rather than cents in soybeans,” said Hurt.

Hurt said if there are tariffs placed on U.S. soybeans, other exporters such as Brazil and Argentina don’t have the ability to compensate and replace the soybeans China receives from the U.S., and this could cause food problems in China.

Hear how long it will take to rebuild a relationship and a loss of market share with any product with China? Hear the panel discuss their thoughts on U.S. Farm Report above.