Steam to float heavily above park as maple syrup is produced
Steam to float heavily above park as maple syrup is produced

By JOSEPH DITSSouth Bend Tribune
The Associated Press

LAGRANGE, Ind.




LAGRANGE, Ind. (AP) — By the time your pancake mops up the pure maple syrup on your plate, the sweet nectar has stewed over a fire — and stewed and stewed, as billows of steam remove water from the sap. Sometimes the workers have to boil it longer because the sugar content of the maple-tree sap is lower. It forces them to boil down more gallons of sap to create one gallon of syrup.
That's true this year at Maple Woods Nature Center in LaGrange, Ind., but not so much at Bendix Woods County Park in New Carlisle.
What's up with this East vs. West maple madness? Both parks are hosting their annual maple syrup festivals this weekend, with many chances to eat the syrup but also to learn about the science of sap.
Maple Woods naturalist Scott Beam says that hypotheses have been flying through the sugar bush — that something was amiss when leaves fell last autumn . that the drought of 2012 still has an effect . or that January saw three periods of thawing rather than just one. All just speculation. He's eager to meet with other maple syrup producers in a month to see what the consensus is.
Meanwhile at Bendix Woods, longtime volunteer and botanist Vic Riemenschneider says, "We're having an excellent year."
February and March consistently delivered prime temperatures for promoting the flow of sap out of the maple trees — freezing at night, thawing during the day. It hasn't been like last year's early onset of warmth that hastened up that season.
Riemenschneider, who grew up on a maple sugar farm in Ohio and is a retired Indiana University South Bend professor, has been testing the sugar content of maple trees at Bendix Woods. Two overall tests produced results that were about like last year's, and the past couple of years have been on the low side.
But he also found a tree up on a hill had somewhat higher sugar in its sap. A tree in a more open, exposed spot can produce more sugar because it catches more sun, stimulating the photosynthesis that turns sunlight into energy, he explains.
He also found two trees on either side of a trail that showed big differences in their saps' sugar content. Genetics can cause some maple trees to have more sugar in their sap.
But, he notes, other factors can cause the differences. Sugar content can fall if anything weakens the photosynthesis process, which is at its peak from the time trees leaf out in the spring until June or July. The soil could be too dry because of a lack of rain. Or there could be a lot of cloudy days that cut back the amount of sunshine. A lack of nutrients in the soil can also play a role.
Bendix Woods tells visitors that, on average, it takes 40 gallons of sap to make a gallon of syrup. Maple Woods needs closer to 50 gallons usually, though it reached a point this season where it needed 92 gallons of sap, says George DeWald, construction and maintenance foremen who spends all of his time on maple production during the season.
But this varies from week to week. DeWald has seen some recent improvement. Maple Wood set 609 taps this year in a 30- to 35-acre sugar bush, he said. All of them collect sap in buckets, which also catch precipitation, leaves and newly awoken moths who go looking for a night-time hiding spot in the buckets after a busy day of seeking a mate, Beam says. All of that stuff is filtered out several times over, DeWald says. Bendix Woods may have a more efficient system since it primarily uses a tubing system to run the sap from the trees to the sugar house.
Maple Woods might end up with an average year of production, which is about 125 gallons of syrup. On Monday, it was at 92 gallons and expecting a good run this week — during the weekend fest, too.
Bendix Woods produced 62 gallons of syrup as of Monday, which was already above the 10-year average of 58 gallons for the season, says park naturalist Leslie Witkowski. Volunteers will continue to cook syrup after this weekend, and Riemenscheider says the park could end up with 70 to 80 gallons this year.
Both parks will have enough syrup for visitors at the festivals, thanks to other local producers. Come hungry.
___
Source: South Bend Tribune, http://bit.ly/2FKN6zt
___
Information from: South Bend Tribune, http://www.southbendtribune.com


This is an AP-Indiana Exchange story offered by the South Bend Tribune.