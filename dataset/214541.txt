Conditions have played into the hands of the Texas citrus industry for the start of the 2017-18 season, grower-shippers and marketers say.
Hurricane Harvey swept into the state in late August, but the storm drifted north, more than 100 miles from Texas’ 27,000 acres of citrus.
Growers said that they had lots of rainfall apart from the hurricane.
“The season started for us about three weeks ago and unfortunately had two or three weather events that presented sort of a start-stop thing for us,” Trent Bishop, sales manager with Mission, Texas-based Lone Star Citrus Growers, said Oct. 23.
“In the long run, weather events are beneficial.”
Quality won’t be an issue this year, said Adam Cooper, vice president of marketing with Delano, Calif.-based Wonderful Citrus, which is the largest citrus grower-shipper in south Texas.
“This season, the … Texas crop has good quality and a wonderful taste profile. In addition, production has generally been stable over the years and in the last four years, we have increased our citrus tree planting in Texas,” he said.
Texas’ deal starts to peak as Florida attempts to recover from Hurricane Irma, which made landfall about a week after Harvey had reached Texas.
The combination had created favorable market conditions for Texas growers, Bishop said.
“Last year was good and stronger than normal, and this season I’d say much stronger than normal,” he said.
“The market was already strong before Hurricane Irma went into Florida, and that event seemed to ramp up demand even more.”
Dante Galeazzi, president of the Mission-based Texas International Produce Association, said the citrus season was off to a very strong start.
“I think the biggest driver is obviously what happened in Florida,” he said. “With the national supply level shortened, everyone is turning to Texas suppliers.”
In the season’s early going, demand seemed to be outpacing supply, Bishop said.
“Demand exceeds anything me or my competitors can supply,” he said.
“Once all the shippers in Texas get going, along with the Florida crop beginning in the next two weeks, I suspect we’ll probably see the whole thing soften just a little, but overall, I suspect the season will trend higher than historical f.o.b. averages.”
Thanks to some timely rains, this year’s crop was looking good, Bishop said.
“Overall, the crop is healthy and seems to be sizing just a bit larger than last year,” he said.
“Texas in general is on an off-year in volume and tonnage, as the crop cycle goes, so as a citrus crop, Texas will be down.”
Dale Murden, president of the Mission-based Texas Citrus Mutual, said there is some quantity to go with quality.
“Overall, our USDA estimates are up,” he said. “At this point, quality looks to be really good.”
The navel orange harvest started early in late August, Murden said.
“I was very shocked that the navels started at that time, but they did, and now grapefruit is rolling,” he said.
“As of Oct. 20, we were probably behind last year thus far on grapefruit. We’re ahead on earlies and navels.”
Rains in late September and early October likely slowed grapefruit harvesting activity, Murden said.
“We’re ahead on oranges because of earlier passes, earlier maturity and a good market,” he said. “I suspect we’ll start to catch up on grapefruit. We needed a little cool weather.”
Water shortages have been a concern for Texas growers in the past, but not this year, Murden said.
“It’s not an issue, but we always keep an eye on it,” he said.
“We were lucky to have rains in watershed, but anytime you share water with another country, it’s going to be an issue. Down here, you look to a hurricane to fill the lakes.”