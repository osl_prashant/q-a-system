When USDA's weekly crop condition ratings are plugged into Pro Farmer's weighted Crop Condition Index (CCI; 0 to 500 point scale, with 500 representing perfect), the HRW wheat crop fell 9.68 points from week-ago to 329.27 points. The SRW wheat crop also fell notably (6.64 points) to 366.28 points.




 
Pro Farmer Crop Condition Index





HRW




This week



Last
			week 



Year-ago


 


SRW




This week



Last
			week



Year-ago





Kansas *(39.07%)

121.90
130.89

134.20


Missouri *(9.94%)

35.57
36.47
35.46



Oklahoma (10.67%)

35.86
35.11

40.17


Illinois (9.79%)

34.57
37.11
36.23



Texas (9.94%)

32.62
33.21

36.12


Ohio
			(10.12%)

40.08
40.38
34.54



Colorado (10.39%)

34.50
34.60

33.11


Arkansas (3.84%)

14.26
14.03
19.50



Nebraska (7.08%)

22.81
23.94

25.38


Indiana (5.68%)

21.36
21.70
21.24



S. Dakota (6.26%)

21.15
21.40

20.84


N. Carolina (7.72%)

27.32
27.09
29.64



Montana (10.87%)

41.64
40.45

42.66


Michigan (10.70%)

38.52
38.73
32.56



HRW total

329.27
338.95

358.97


SRW total

366.28
372.92
370.74



* denotes percentage of total national HRW/SRW crop production.
Following are details from USDA's National Ag Statistics Service (NASS) crop and weather reports for key HRW wheat states:
Kansas: For the week ending May 7, 2017, temperatures averaged below normal for the second consecutive week, according to the USDA s National Agricultural Statistics Service (NASS). Most counties in the state received up to one inch of rain, however the northeast and northwest counties receiv ed little to none. Effects of recent snowfall and freezing temperatures were still being assessed. There were 2.0 days suitable for fieldwork. Topsoil moisture rated 0% very short, 1% short, 69% adequate, and 30% surplus. Subsoil moisture rated 0% very short, 6% short, 79% adequate, and 15% surplus.
Winter wheat condition rated 10% very poor, 17% poor, 30% fair, 37% good, and 6% excellent. Winter wheat jointed was 96%. Headed was 59%, behind 70% last year, but ahead of the five-year average of 51%.
Oklahoma: Precipitation levels were above their respective normal averages with the exception of the South Central district. Drought condition was rated at only four percent moderate to exceptional drought, down 13 points from last week. According to the OCS Mesonet, this is the lowest percentage of drought in the state since June 28, 2016. Statewide temperatures averaged in the low 60s. Topsoil moisture condition was rated mostly adequate to surplus while subsoil moisture condition was rated mostly adequate to short. There were 3.5 days suitable for fieldwork.
Winter wheat headed reached 90%, up 1 point from the previous year and up 7 points from normal.
Texas: Not yet available.
Following are details from USDA's NASS crop and weather reports for key SRW wheat states:
Ohio: Heavy rains continued throughout the week, preventing nearly all field work and leading to ponding in some fields. There were 0.7 days suitable for fieldwork in Ohio during the week ending May 7, according to Cheryl Turner, Ohio state statistician with the USDAs NASS.
Michigan: There were 1.6 days suitable for fieldwork in Michigan during the week ending May 7, 2017, according to Marlo Johnson, director of the Great Lakes Regional Office of NASS. Rainfall continued to delay spring planting this week. Temperatures dipping into the 20s and 30s at night have kept soil temperatures cold and hindered evaporation losses. Some frost and hail across the State have left some producers anxious about the upcoming crop season. There was some tillaged one toward the end of the week to get ready for corn and soybean planting.
A few producers managed to get some spray applications done for wheat on better-drained soils, otherwise not much fieldwork was accomplished the past week.
Missouri: Additional rain fell over the past week which caused flooding in some areas. Temperatures averaged 55.1 degrees, 4.9 degrees below n orm al. Precipitation averaged 2.12 inches statewide, 0.98 inches above normal. There were 1.4 days suitable for fieldw ork for the week ending May 7. Top soil moisture supply was rated 1% short, 50% adequate, and 49% surplus. Subsoil moisture supply was rated 1% very short, 1% short, 62% adequate, and 36% surplus.
Winter wheat headed reached 84%. Winter wheat condition was rated 1% very poor, 6% poor, 33% fair, 54% good, and 6% excellent.