Led by multi-million dollar losses to avocados and citrus, estimated product losses from December’s Thomas Fire in Ventura County, Calif., tops more than $25 million, but the total loss for this and future seasons is many times higher.
A report on the fire damage from Ventura County Agricultural Commissioner Henry Gonzales to the U.S. Department of Agriculture estimates that damage to current and future crops, machinery and equipment, dwellings and other structures is $171.3 million.

The Thomas Fire spread to more than 282,000 acres in Ventura and Santa Barbara counties since it began Dec. 4. It was more than 90% contained by early January.
The preliminary damage report, sent to USDA Farm Service Agency officials Dec. 22, said that the fire affected 10,289 acres of irrigated crops and another 60,000 acres of range land. Gonzales said in a cover letter for the report that a full assessment of the damage may not be complete until early summer.
Avocado growers lost more than 4,000 tons of production and suffered damages of more than $10 million, according to the report, and lemon grove losses exceed $5 million.
“We will have a better indication of the damage in the spring when blossom appear or do not appear due to the fire damage,” according to Gonzales’ letter. 
By commodity, the damage loss estimates for Ventura County crops were:

Avocados — $10.18 million: 1,250 acres damaged, 4,030 tons lost;
Lemons — $5.81 million: 400 acres damaged, 7,591 tons lost;
Oranges — $3.37 million: 135 acres damaged, 3,680 tons lost;
Mandarins — $491,022: 75 acres damaged, 431 tons lost;
Strawberries — $486,416: 9 acres damaged, 241 tons lost;
Grapefruit — $35,930: 3 acres damaged, 32 tons lost;
Raspberry — $55,420: 1 acre damaged, 10 tons lost;
Vegetables — $4.6 million: 120 acres damaged; and
Miscellaneous fruit — $1.39 million: 75 acres damaged.

Gonzales asked the USDA to make disaster assistance “promptly available” to growers, who without assistance may go out of business, he said. 
Gonzales said Jan. 4 that growers are quickly trying to replace irrigation systems damaged by the fire, because drought conditions prevail in the area.
“One of the most immediate needs now for recovery is the replacement of our highly efficient irrigation systems,” he said. “All of those systems need to be replaced ASAP because in addition to facing a fire emergency we are also facing a drought emergency now.”
Gonzales said there was no rain in December, which is usually one of the region’s wettest months.
Gonzales said USDA will use the damage estimates to release disaster assistance funds through the Farm Services Agency.