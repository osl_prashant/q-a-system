Harvest is in full swing, but the impact of a difficult planting season is still apparent from the combine perch. While phosphorus (P) might have been plentiful in the soil, a cold, wet spring tricked the nutrient from working its magic.
Several farmers told me that if their starter applicators got shut off or plugged, the corn that did not receive starter was a foot shorter a few weeks before tasseling," says Farm Journal Field Agronomist Ken Ferrie.

We’ll explain why in a minute. For now, understand that P helps cells elongate and divide, which is how plants grow. This unique role makes P a very important nutrient.

In this article, we’ll explain how P works. In certain seasons, the right amount of P can boost corn yield by 30 bu. to 40 bu. per acre.

Along with cell elongation and division, adequate P levels are required to transfer starches—especially sugars within a plant. "If a corn plant can’t transfer sugars, which are produced in the upper part of the plant, to other areas, the plant stops growing," Ferrie explains. "When that happens, plants take on a purple appearance. It’s similar to what happens to trees in the fall; when sugar is no longer being transferred, the leaves change color."

Corn plants need P early. "Because phosphorus is so important to early growth and development, the majority needs to be taken up early in the growing season," Ferrie says. "This is different from nitrogen, which plants can take up throughout the season."

The role of P in plant development involves two compounds in cells: ATP and ADP. "Phosphorus is constantly cycling between ATP and ADP and back," he says. "In this process, energy is released for growth. Think of it as a spring winding up and letting loose.

"It takes energy, which comes from the sun, to wind the spring or create ATP. When the plant converts ATP to ADP, that’s like letting go of the spring, and energy is released for growth and development. Then the process starts all over again," Ferrie says.

After plants pick up P from the soil, it can be consumed by animals or humans. That makes P essential to every living creature, including microbes.

Plant roots reach P three ways: interception, mass flow and diffusion. These processes occur simultaneously.

Roots intercept P as they grow through the soil. "During their rapid growth stage, corn roots grow 1" to 1½" per day," Ferrie says. "In the process, they encounter nutrients."

In mass flow, plant roots draw water from the soil, literally pulling the soil water to the plant. Nutrients are carried to the plants in the water.

In diffusion, P leaks into the soil water. The P ions move, or diffuse, from organic phosphate stored in the soil to the inorganic phosphate that plants can use by soil microbes.

Plants have several ways of regulating which nutrients come in and at what amounts. "It’s key to remember that taking up nutrients is an active metabolic process," Ferrie says. "It requires energy, and plants get that energy from phosphorus."

Organic versus inorganic form. There are two points to understand about P in your soil. The first is that P (like other nutrients) exists in organic and inorganic forms.

"In the organic form, phosphate ions are hooked to carbon chains inside microbes or crop residue," Ferrie says. "That phosphorus cannot be taken up by plants. It must be broken down, or composted, into the inorganic form.

"This process is run entirely by soil microbes," he continues. "Soil phosphorus will not diffuse into the soil solution until the microbes are up and running at full speed. That process is extremely sensitive to soil temperature. The microbes that release P don’t become very active until soil temperature reaches 60°F to 65°F.

"Even soils testing high in phosphate will have purple corn when the soils are cold. That’s why phosphorus in starter fertilizer made such a difference in 2013," Ferrie explains. "Unlike soil phosphorus, the phosphorus in the starter was not sensitive to soil temperature or microbial activity, so it was available to the plants."

The second thing to understand is that after you apply P fertilizer to the soil, the P changes from high solubility to low solubility. "The process happens fairly fast," Ferrie says. "That’s why only 10% to 30% of applied phosphorus is available to plants the first year."

Some of the remaining P gets tied up and becomes unavailable to plants. But a high percentage of it is consumed by soil microbes.

Hungry microbes. "In reality, we are applying phosphorus for the microbial processes that feed plants," Ferrie says.

Those microbes are very important. "Phosphate ions have a strong affinity with water in the soil," he says. "When we apply diammonium phosphate (DAP) fertilizer, the phosphate ions dissolve and combine with water to form phosphoric acid, which is what microbes and plants need. But the acid can also dissolve bases such as calcium, iron and aluminum in the soil."

"If the phosphoric acid remains in the soil very long, the phosphate ions can bond to other ions in the soil and become unavailable to plants," he says. "In some soils, this requires changing your fertilizer program to mitigate a tie-up and ensure that phosphorus is available when plants need it."

Knowing that, you can see why P fertilizer products vary in their availability to plants. "In calcaric conditions, monocalcium phosphate fer­til­izers tie up faster than ammonium phosphates," Ferrie says.

Another nutrient, zinc, is required for phosphorus uptake. "You must maintain the proper phosphate-to-zinc ratio," Ferrie says.

How much P is in my soil? As with all nutrients, managing P starts with a soil test. But it’s more complicated than reading numbers on your lab report.

"Soil tests do not tell you exactly how many pounds of phosphorus are in your soil," Ferrie explains. "The results are indicators you can use to try to predict how much phosphorus will come out of soil, and how easily. Soil is a dynamic medium in which microbes are constantly changing phosphorus from plant-available to plant-unavailable states and back again."

There are various ways to test soil for P availability. "Each test has pros and cons, depending on your area and the soil types you deal with," Ferrie says. "Ask your crop consultant or Extension specialist which test will work best in your location."

Even though phosphorus seems to be a moving target, with correct management there are significant opportunities to improve yield. In the next installment, we’ll learn how the right amount and timing of applications can help you gain that yield boost.