Grains mostly higher and livestock lower
Grains mostly higher and livestock lower

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mostly higher Monday in early trading on the Chicago Board of Trade.
Wheat for March delivery fell 1.20 cents at $4.5140 a bushel; March corn was up 3.40 cents at $3.7540 bushel; March oats rose 2.40 cents at $2.2760 a bushel while March soybeans advanced 22.60 cents at $10.3660 a bushel.
Beef and pork were lower on the Chicago Mercantile Exchange.
April live cattle fell .60 cent at $1.1575 a pound; March feeder cattle lost .22 cent at $1.3598 a pound; April lean hogs was off 1.90 cents at .5878 a pound.