GT Parris, who has more than 25 years’ experience in the produce industry, joined Vero Beach, Fla.-based Seald Sweet as account executive and commodity manager. 
He will concentrate on expanding Seald Sweet’s domestic and imported produce programs, according to a news release.
“We are excited about the energy and knowledge GT brings to our team,” Mayda Sotomayor-Kirk, managing director of Seald Sweet, said in the release. 
“His experience is a perfect fit for our company and a value to our customers and program development. We look forward to the growth he brings to our company.”
Parris’ experience with Florida citrus and other commodities will be a benefit to Seald Sweet’s recently expanded programs in Mexico and Greenyard Logistics’ new cold storage and packing plant in the northeastern U.S., according to the release. 
Greenyard Logistics is Seald Sweet’s sister company. 
Parris has been domestic sales manager for IMG Citrus Inc., North American sales manager for Greene River Marketing Inc., and was also at fruit importer Nexus Produce Inc.