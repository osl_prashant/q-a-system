More Daisy Girl Kanzi apples due
CMI Orchards LLC, Wenatchee, Wash., expects to see a major increase in volume of its Kanzi organic apples this season, said Loren Foss, organic manager.
"Last year, we had about 200 bins," he said. "We will have 2,000 this year."
Sold under the Daisy Girl Organics brand, the Kanzi is a bicolor apple with a "real powerful, tart flavor that really pops when you bite into it," he said.
The company also has increased production of its Ambrosia variety, Foss said.
 
Del Monte Fresh expands offerings
Del Monte Fresh Produce NA, Coral Gables, Fla., continues to expand its product offerings - such as bananas and avocados - to meet consumer demand, said Dennis Christou, vice president of marketing.
"Our entire line of organic banana products has been certified organic by the Quality Certification Services and are also Control Union-certified," he said.
They come in 40-pound boxes and include organic naked, organic banded, organic 2-pound banded, organic bagged and organic 3-pound bagged.
 
Demand grows for Dole organics
"Without a doubt, we are seeing the strongest growth in demand continuing for Dole organic bananas by both retailers and consumers," said Bil Goldfield, director, corporate communications for Dole Food Co., Westlake Village, Calif.
Dole started its organic program more than 20 years ago and is the largest grower and distributor of fresh organic bananas, Goldfield said.
The company's organic pineapples also are experiencing "explosive growth," he said.
Dole offers both Rain Forest Alliance and Fair Trade organic pineapples for interested retailers, Goldfield said.
 
Foxy Organic fennel available
Organic fennel, sold under the Foxy Organic label, now is available on a regular basis from The Nunes Co., Salinas, Calif., said sales manager Doug Classen.
The company also is looking forward to an expanded seasonal asparagus program that will kick off in February, he said.
The Nunes Co. offers about 40 items under the Foxy Organic label, including iceberg lettuce, broccoli, cauliflower and romaine hearts.
 
Category growing at Giorgio Fresh
Demand remains steady in the mushroom category, and organic mushroom sales continue to grow, said Bill Litvin, senior vice president of sales for Giorgio Fresh Co., Blandoon, Pa.
"More and more consumers are buying organic products on an ongoing basis, and fresh mushrooms, being a healthy, nutritious product, are at the heart of this growth trend," he said. Giorgio expects continued growth in its entire line of organic mushrooms.
The company's organic line includes white whole and sliced, baby bella whole and sliced, portabella caps and sliced caps, shiitake whole and sliced, royal trumpet, maitake and beech varieties.
 
Homegrown boosts blueberry deal
Porterville Calif.-based Homegrown Organic Farms has implemented a "category remodel" for its organic blueberry program, said Cherie France, marketing manager.
The company now offers organic blueberries year-round, thanks to domestic and international counterseason opportunities, she said.
Homegrown Organic Farms also launched a line of freeze-dried organic blueberries, apples and grapes under the Homegrown Organic Farms label in the fall.
The products have an 18-month shelf life and come in two sizes - 0.35 ounces and 1.2 ounces, which retail for $1.99 and $4.99, respectively.
 
Demand increases for Kern Ridge
Demand once again is on the rise for organic carrots after dropping off a bit during the recession, said Chris Smotherman, account manager for Kern Ridge Growers LLC, Bakersfield, Calif.
Kern Ridge plants more organic carrots in winter, but supplies remain tight as demand increases during this time of year and Canadian buyers look to U.S. sources for product, he said.
Supplies are especially tight for jumbo-sized carrots.
 
Lakeside to get new headquarters
Watsonville, Calif.-based Lakeside Organic Gardens is building the first phase of its new headquarters, said Katie Bassmann, marketing and communications representative. This phase includes a new cooling and shipping facility as well as a sales office, she said.
This first phase should be finished by April 1.
Additional phases will include administration offices and the headquarters for the Dick Peixoto Family of Cos., which consists of Lakeside Organic Gardens LLC, Pajaro Valley Laser Leveling and Pajaro Valley Irrigation.
 
Monterey has new specialty line
Monterey Mushrooms Inc., Watsonville, Calif., introduced a specialty organic line of mushrooms at the Produce Marketing Association's Fresh Summit in October, said Lindsey Roberts, marketing specialist.
The line includes king trumpet, beech, maitake, shiitake, oyster and enoki mushrooms in award-winning sustainable packaging that includes graphics with usage ideas, interesting facts and simple how-to instructions, Roberts said.
The paper tills are recyclable, biodegradable and compostable.
 
Oppenheimer sees 'robust growth'
Vancouver, British Columbia-based The Oppenheimer Group is experiencing robust growth in its organic program, said Rachel Mehdi, organics category manager.
"Winter is a lighter time of year for our organic program, but we still have some high-demand offerings, like Washington-grown Envy and Pacific Rose apples, available through January," she said. "We're looking ahead to offering imported organic apples and pears early this spring."
Fair Trade-certified organic sweet bell peppers and mini peppers from Mexican greenhouse grower Divemex are ramping up and will continue through May, she said.
Divemex, a grower of conventional and organic vegetables, plans to produce more than 50% of its volume organically for the first time in the 2016-17 season, she said.
Oppy also offers organic tropicals.
Peruvian mangoes will run through February, and the company will provide Italian organic green kiwifruit to East Coast customers through the winter, Mehdi said
 
OTA names two staff members
The Organic Trade Association, Washington, D.C., recently added two experienced government affairs professionals to its staff, according to a news release.
Kelley Poole was named vice president of government affairs, and Megan DeBates was selected director of legislative affairs and coalitions.
Poole most recently served as director of government relations for Verto Solutions LLC, Washington, D.C. She previously served in the Bush administration in several capacities.
DeBates most recently was senior legislative assistant to Oregon Congressman Peter A. DeFazio, who was an author of the Organic Foods Production Act of 1990 that established national organic standards.
"We believe that these two additions to our staff will aid us in our work with policymakers to make the voices and perspectives of organic heard and respected," Laura Batcha, OTA's executive director and CEO, said in the release.
 
Stemilt Growers updates website
Stemilt Growers Inc., Wenatchee, Wash., recently updated its Stemilt.com website, said Brianna Shales, communications manager.
The site includes "a robust organic section meant to educate consumers about how organic apples and other Stemilt fruits are grown and when they are available," she said.
 
Robinson Fresh opens new center
The Robinson Fresh division of C.H. Robinson Worldwide Inc., Eden Prairie, Minn., opened a service center in California in September, said Ray Griffin, director of global sourcing.
"Our new (Southern California) Service Center is a beautiful creation that will allow Robinson Fresh Organics to add value to many commodities through various repackaging capabilities," Griffin said.
The center has 15 loading doors and is staffed with Robinson Fresh personnel for sales and consolidation activity as well as overseeing organic operations and quality assurance, he said.
 
Veg-Fresh adds tomato package
The Good Life Organic brand from Corona, Calif.-based Veg-Fresh Farms LLC now includes sweet grape tomatoes packaged in fiber trays made from the firm's recycled corrugated produce boxes, which reduces plastic use and decreases the company's carbon footprint, says Kate Reeb, marketing director.
The Good Life Organic brand includes more than 100 organic items, including navel and valencia oranges, grapefruit, cara cara navels, minneolas, lemons, limes, green beans, Brussels sprouts, grapes, pineapples and 11 tomato varieties in a variety of packaging and sizes, Reeb says.
 
Viva Tierra revamps website
Viva Tierra Organic Inc., Sedro-Woolley, Wash., has revamped its website, said Addie Pobst, organic integrity and logistics coordinator.
The new site is scalable for various platforms, including smartphones, tablets and desktop computers, and displays information clearly on all formats, she said.
The site makes tasks like looking up Universal Product Codes and package dimensions easier and provides basic information on how organic systems work, Pobst said.
 
Wholesum trials tomato varieties 
Wholesum Family Farms, Nogales, Ariz., is conducting trials on a couple of new organic items, said Ricardo Crisantes, general manager.
First are Petites on-the-vine cocktail-size tomatoes. They're a small but high-flavor tomato that come in 12-ounce fiber trays packed seven per carton.
The company also is conducting trials on a brown and red tomato called Sienna on-the-vine.
"It's a sweet and savory taste with an heirloom tomato-type color," Crisantes said. It will be in trials during the winter and spring.
The Sienna likely is the only brown tomato sold bulk, not packaged, added marketing manager Jessie Gunn.
There also seems to be a trend toward purple produce, Crisantes said.
The company now offers organic Graffiti and Globe variety eggplant and is trialing Sabelle eggplant, which is a deeper shade of purple than regular eggplant and is "kind of pumpkin shaped."
Finally, the company recently launched a new website - wh.farm.