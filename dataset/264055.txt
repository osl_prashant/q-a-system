John Deere made his first plow in 1837, paving the way for one of the top machinery companies in the world.

On March 14, 1918, the company turned its attention to tractors, paying $2.1 million to the Waterloo Gasoline Engine Company and their Waterloo Boy tractor.

Today, that sale would be valued at $34,843,000 and in the first year in the tractor business, John Deere sold 5,634 of the kerosene-powered machines.

This tractor is on display in many places, including the John Deere Tractor and Engine Museum in Waterloo, Iowa and the American History Museum in Washington D.C. as part of the American Enterprise exhibit.

Machinery Pete attended the centennial anniversary of the event earlier this week.