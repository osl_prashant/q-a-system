The threat of frost now officially gone for the season, peach and blueberry growers in Georgia are focusing on getting what’s left of their crops to market.Other commodities, which weren’t touched by a March 15-16 freeze, appear to be headed for big-volume seasons, growers said.
“It looks like we’ve got somewhere in the 60-70% range, so we should do pretty good with that,” Juda Echols, partner in Alto, Ga.-based Jaemor Farms, said of his peach crop, which endured temperatures as low as 21 degrees during the depth of the freeze.
Echols said he lost some early varieties, and that will push back the start of the deal.
“We don’t expect to start harvest as early as usual — the first should come somewhere around June 10-15,” he said.
The peach deal usually runs through September, Echols said.
Echols said there probably won’t be a final count on freeze-related damage for at least a couple of weeks.
“If there is any faulty fruit left, it will fall off in May; we’re just waiting to see how many will fall off,” he said.
He said it is unlikely he’ll lose much fruit in May.
“No, I think we should be in pretty good shape,” he said.
The crop that remains will be a good one, Echols said.
“Should be a pretty good year on sizing,” he said. “For quality, so far, everything looks good, but a lot of that will depend on the weather from here on out.”
In Baxley, Ga., Miles Berry Farm lost nearly all its rabbiteye blueberries, and there are a few lingering issues with the highbush crop, said Allen Miles, owner.
“The highbush crop is fair, but we’re having some issues with sizing,” he said. “The fruit we’ve managed to pack looks good, but we’re probably off 30-40% on the highbush crop.”
Berries that sustained some freeze damage haven’t “sized up,” he said. “But, our sorters are getting them out. We’re getting good quality with the fruit that’s coming through. The sorting equipment does a wonderful job.”
Packing of highbush berries got underway about a week ahead of normal, Miles said. In a typical year, the deal would run into August.
But, Miles said, this isn’t a typical year.
“I don’t know,” he said when asked if he would have enough blueberries to reach August.
Blackberries and strawberries, on the other hand, appear to be headed toward normal volumes, Miles said.
“Blackberries look to be on target with a pretty good year,” he said.
Strawberries were looking good, too; now, though, heat is a factor, Mile said.
“If it gets much hotter, we’ll be out, because the berries won’t size,” he said. “They’ll turn red before they get big. But, that’s normal every year.”
Echols said his crews started picking strawberries about three weeks earlier than last year.
“They’re starting to come in heavy now,” he said, noting that the deal will continue until mid-June.
Echols said the freeze left only limited damage on his strawberry crop.
“Just a very few at the very beginning of the picking crop,” he said. “A few got frostbit, so they had to be picked and thrown down, but after that, it’s been very good.
Echols said his blackberry crop will come in by mid-June, and he’s expecting a “very good” crop.
“They’re just blooming now.”
Echols also said he was planning to begin planting vegetables during the week of April 24.
“The threat of frost has already passed, so it should be a pretty good year for most of the vegetables,” he said.
Weather has not been an issue with the Vidalia onion crop, said Delbert Bland, owner of Bland Farms in Glennville, Ga.
“The crop’s coming along very good,” he said. “We’ve got excellent weather, good quality and very good conditions at this point.”
Harvest had been underway for about two weeks, without any problems, Bland said.
“Volume is very normal; we feel comfortable with where we’re at,” he said.