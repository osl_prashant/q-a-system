AP-AK--Alaska News Coverage Advisory, AK
AP-AK--Alaska News Coverage Advisory, AK

The Associated Press



Here's a look at how AP's general news coverage is shaping up in Alaska. Questions about coverage plans are welcome and should be directed to the AP-Anchorage bureau at 800-770-7549 or at apanchorage@ap.org. News Editor Mark Thiessen can be reached at 907-272-7549 or mthiessen@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
Alaska at 8 a.m.
AROUND THE STATE:
SHEEP-GOATS-INFECTIONS
KENAI — Several wild sheep and goats tested positive for a pathogen that has caused respiratory disease in Lower 48 herds. SENT: 300 words.
JUNEAU-SHOOTING DRILL
JUNEAU — Firefighters and police officers in Juneau will train to rescue and treat victims during an active shooter situation. SENT: 230 words.
IN BRIEF:
— TRAIL DEATH — Hypothermia is suspected as the cause of death for a 46-year-old Wasilla woman found along a trail.
— CHILD SHOT — A 3-year old died following a shooting in a home in the North Slope community of Utqiagvik.
___
If you have stories of regional or statewide interest, please email them to apanchorage@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Alaska and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.
The AP-Anchorage