As a retailer, you serve customers who rely on you to help them avoid risky investments, especially in the current market environment. 
On the flip side, if you gave your customers a recommendation on an investment that has, according to six years of data, a 92 percent chance of positive return on yields, do you think they’d do it? And do you think the fact that it delivers positive ROI 78 percent of the time would help influence their decision even further?
If your customers did make that investment in Headline AMP fungicide in recent growing seasons, they’d be among the thousands of corn producers who’ve seen results just like these from this market-leading fungicide.
There is one challenge, however, that many growers have to get over, and that’s the mentality surrounding fungicide use versus herbicide use:
Herbicide scenario:
Observation: See a weed
Action: Spray a weed
Result: See a weed wither and die
Fungicide scenario:
Observation: See a crop canopy
Action: Spray a crop canopy
Result: Changes at the cellular level
Seeing (results) is believing, but seeing inside the plant is another matter, and that’s where Headline AMP fungicide does its magic in the form of Plant Health benefits. These include superior disease control, boosting the plant’s growth efficiency and maximizing tolerance to the various stressors on the plant — the types of benefits that give the plants the best chance for maximum yields.
The leading edge of overall efficacy for Headline AMP fungicide is its best-in-class control of a wide range of fungal pathogens. And for those who understand that the timing of fungicide applications makes all the difference — they proactively plan to apply and do so before symptoms are visible — they’re seeing the best possible results for their crops. 
Diseases infect a plant and start to develop at a cellular level within the plant before symptoms appear — the latent period. If you wait to see symptoms, you’ve given the disease a head start, and that’s the beginning of a tough stretch for the plants — and the growers. With gray leaf spot, for example, by the time symptoms are visible, the disease already has a nine-day start.
In a recent poll taken by Farm Journal Media of thousands of growers, they were asked what they wish they had done differently with the benefit of post-harvest hindsight. One of the top responses was, “Wish I’d made a fungicide application.”
It’s clear that growers who haven’t been using Plant Health and disease control solutions like Headline AMP fungicide are taking note of what’s happening in the fields of those who have. And what they’re all seeing is a level of performance that’s not a one-season wonder, but a consistently winning solution, field after field, acre after acre, season after season. 
Hundreds of trials and successive seasons of actual field successes with Headline AMP fungicide show a high level of consistency that takes a large degree of risk out of the equation.
Always read and follow label directions.
Headline AMP is a registered trademark of BASF Corporation. © 2018 BASF Corporation. All rights reserved. APN 17-CB-0011
 
Sponsored by BASF