This may be the year for two cuttings of tall fescue hay. That's good news.The bad news: The first cutting will make lousy hay, says Craig Roberts, University of Missouri Extension forage agronomist.
Erratic weather caused fescue to set seed early. Under stress, grass made seed instead of leaves.
"Those early seed stems must be cut to make good hay later in the second cutting," Roberts says.
The story came out during a weekly University of Missouri teleconference. Regional and state extension specialists trade what they see during the growing season.
Todd Lorenz, extension agronomist at Boonville, said farmers see their fescue setting seed at least two weeks early. "There's just no undergrowth there."
Unfortunately, farmers who fertilized pastures for early growth are hit hardest.
Roberts saw some fescue make seed a month early because of weather.
Lorenz said pasture growth is similar to 2007, when late cold weather stressed grass growth.
Roberts said the problem becomes what to do with stemmy first growth. In toxic tall fescue, which is most fescue in the state, seeds and stems make the most toxic forage.
"Even if it's not good hay, stems must be cut," Roberts said. For best livestock gains, seed stems are bad on grazing paddocks, not just hay ground.
"If weather cooperates, second cutting hay would be much less toxic. That's because seed heads are gone."
In wet weather, farmers ask about making haylage from poor-quality first-cutting hay. "Baleage or haylage, wet preservation in plastic bags, preserves the toxin," Roberts said. "Toxin in bales declines in storage. Hay becomes half as toxic as pasture or baleage."
A two-week outlook from Pat Guinan, MU Extension climatologist, shows heavy rain ahead for Missouri, 2 to 6 inches, with heaviest rain to the south.
"With rain, we'll have good forage growth if it doesn't turn to instant drought," Roberts added.
In other forage news, alfalfa has bad news and good news.
"We had lots of alfalfa winter kill," Wayne Flanary said from northwestern Missouri.
Ben Puttler, retired entomologist, said that in central counties, alfalfa outgrew alfalfa weevil populations this year. The plants are strong enough to fend off light populations.
Now a fungus that kills alfalfa weevil is coming on. Puttler led efforts to bring natural controls for weevils to the state.
Valerie Tate reported from Linneus that alfalfa she scouted in north-central Missouri has three weevils per stem. However, growth there is about 10 days behind mid-Missouri.
Puttler noted that alfalfa is about ready for harvest at 20-inch height. Harvesting kills weevils.
Rob Kallenbach, MU Extension agronomist, said May is the best time for spring planting of warm-season grasses. Those fill the summer slump in cool-season grass growth. Crabgrass or native warm-season grasses fill the gap.
Plantings in late May or June put seedlings in summer drought risk.
In other teleconference news, Wayne Flanary said northern Missouri farmers are "planting corn left and right."
Soybean growers wait for drier soils with higher temperatures. Soybean planting usually follows corn.
Farmers with forage or crop questions can contact their regional MU Extension agronomists for help.