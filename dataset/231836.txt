The U.S. Department of Agriculture has sanctioned six produce companies that have failed to pay Perishable Agricultural Commodities Act reparation awards.
The following companies and responsibly connected individuals are currently restricted from the industry, according to a news release:

Arizona Marketing Produce Distributors Inc., Phoenix, for failing to pay a $39,191 award in favor of a California seller. Lorin Hobbs was listed as the officer, director and major stockholder of the business.
Overland Xpress LLC, Cincinnati, for failing to pay a $28,562 award in favor of a Florida seller. Andoni Bledar and Jason Brown were listed as members of the business.
Lider Fresh Co., McAllen, Texas, for failing to pay a $5,600 award in favor of an Arizona seller. Franciso A. Feliz Marmolejos was listed as the officer, director and major stockholder of the business.
Tomato Specialties LLC, Nogales, Ariz., for failing to pay a $4,200 award in favor of a Texas seller. Isaac Castro and Yvette Castro were listed as members of the business.
Shorty’s Produce Inc., Denver, for failing to pay a $119,808 award in favor of a California seller. Eleno Cardenas was listed as the officer, director and major stockholder of the business.
PP’S Produce Distributors LLC, Dallas, for failing to pay an $8,809 award in favor of a Texas seller. Jose Abud and Daniel Esqueda were listed as members of the business.

Tomato Specialties and the Castros had already been cited by USDA for making false and misleading statements and failing to account properly in conjunction with 41 lots of tomatoes purchased from five sellers. The company also does business as The Avocado Co. International, according to the earlier USDA release, and is barred from the produce industry until Nov. 27, 2019, as a result of its PACA violations.