USDA Weekly Export Sales Report
			Week Ended July 20, 2017





Corn



Actual Sales (in MT)

Combined: 578,600
			2016-17: 92,000
			2017-18: 486,600



Trade Expectations (MT)
400,000-900,000


Weekly Sales Details
 Net sales of 92,000 MT for 2016-17--a marketing-year low--were down 80% from the previous week and 66% from the prior four-week average.  Increases were for Japan (192,700 MT, including 143,000 MT switched from unknown destinations and decreases of 42,400 MT), Mexico (57,500 MT), Colombia (12,400 MT, including 25,000 MT switched from unknown destinations, 11,300 MT switched from Panama, and decreases of 48,000 MT), El Salvador (9,000 MT, including 6,700 MT switched from Nicaragua), and Portugal (7,600 MT).  Reductions were reported for unknown destinations (194,500 MT) and Nicaragua (6,700 MT).  For 2017-18, net sales of 486,600 MT were reported primarily for unknown destinations (160,000 MT), Mexico (136,400 MT), and Japan (63,000 MT). 


Weekly Export Details
Exports of 894,300 MT were down 14% from the previous week and 12% from the prior four-week average.  The primary destinations were Japan (295,400 MT), Mexico (248,100 MT), Taiwan (79,300 MT), Portugal (72,300 MT), and Colombia (54,900 MT).


Comments and Performance Indicators
Sales were within expectations. Export commitments for 2016-17 are running 16% ahead of year-ago compared to 17% ahead the week prior. USDA projects exports in 2016-17 at 2.225 billion bu., up 17.0% from the previous marketing year.




Wheat



Actual Sales (in MT)

2017-18: 498,000



Trade Expectations (MT)
350,000-550,000


Weekly Sales Details
Net sales of 498,000 metric tons were reported for delivery in marketing year 2017-18 were down 26% from the previous week, but up 5% from the prior four-week average. Increases were for Taiwan (105,200 MT), South Korea (88,100 MT), Chile (67,300 MT, including 46,000 MT switched from unknown destinations and decreases of 700 MT), the Philippines (35,000 MT), and Mexico (28,200 MT).  Reductions were reported for the Dominican Republic (200 MT).  


Weekly Export Details
Exports of 476,600 MT were down 13% from the previous week and 18% from the prior four-week average.  The primary destinations were Japan (86,400 MT), Mexico (80,600 MT), Chile (69,300 MT), Peru (52,900 MT), and South Korea (50,700 MT).


Comments and Performance Indicators
Sales were near the upper end of expectations. Export commitments for 2017-18 are running 2% ahead of year-ago versus 2% ahead of year-ago the week prior. USDA projects exports in 2017-18 at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual Sales (in MT)

Combined: 835,200
			2016-17: 303,400
			2017-18: 531,800



Trade Expectations (MT)
400,000-1,300,000 


Weekly Sales Details
Net sales of 303,400 MT for 2016-17 were down 26% from the previous week and 8% from the prior four-week average.  Increases were reported for the Netherlands (139,700 MT, including 132,000 MT switched from unknown destinations), China (138,500 MT, including 126,000 MT switched from unknown destinations), Thailand (70,300 MT), Japan (27,300 MT, switched from unknown destinations), and Egypt (22,800 MT, including 20,000 MT switched from unknown destinations).  Reductions were reported for unknown destinations (135,800 MT) and Venezuela (600 MT).  For 2017-18, net sales of 531,800 MT were reported for China (192,000 MT), unknown destinations (123,300 MT), Vietnam (63,000 MT), and Japan (58,200 MT). 


Weekly Export Details
Exports of 573,700 MT were up 60% from the previous week and 65% from the prior four-week average.  The destinations were primarily China (141,600 MT), the Netherlands (139,700 MT), Japan (84,200 MT), Egypt (82,800 MT), and Mexico (52,300 MT). 


Comments and Performance Indicators
Sales were within expectations. Export commitments for 2016-17 are running 17% ahead of year-ago, compared to 16% ahead the previous week. USDA projects exports in 2016-17 at 2.100 billion bu., up 8.1% from year-ago.




Soymeal



Actual Sales (in MT)
Combined: 61,900
			2016-17: 6,700
			2017-18: 55,200


Trade Expectations (MT)

50,000-225,000 



Weekly Sales Details
Net sales of 6,700 MT for 2016-17 were down 84% from the previous week and 79% from the prior four-week average.  Increases were reported for Canada (19,400 MT, including 500 MT switched from Mexico), Colombia (17,100 MT, including 5,000 MT switched from Panama), Costa Rica (13,200 MT), and Mexico (9,400 MT).  Reductions were reported for Peru (32,500 MT), Ecuador (14,000 MT), unknown destinations (10,500 MT), and Guatemala (6,200 MT).  For 2017-18, net sales of 55,200 MT were reported primarily for Mexico (31,100 MT), Nicaragua (10,500 MT), and Ecuador (4,000 MT). 


Weekly Export Details
Exports of 115,000 MT were up 26% from the previous week and 25% from the prior four-week average.  The destinations were primarily Mexico (31,800 MT), Canada (19,800 MT), Colombia (17,800 MT), Malaysia (10,200 MT), and Jamaica (7,600 MT). 


Comments and Performance Indicators
Sales were within expectations. Export commitments for 2016-17 are running 2% behind year-ago compared with 1% behind year-ago the week prior. USDA projects exports in 2016-17 to be down 0.5% from the previous marketing year.




Soyoil



Actual Sales (in MT)

2016-17: 14,900



Trade Expectations (MT)
5,000-25,000


Weekly Sales Details
Net sales of 14,900 MT for 2016-17 were down 41% from the previous week and 2% from the prior four-week average.  Increases were reported for South Korea (28,000 MT, including 15,000 MT switched from unknown destinations), Guatemala (4,000 MT), Mexico (1,100 MT), and Canada (400 MT).   Reductions were reported for unknown destinations (15,000 MT) and the Dominican Republic (3,700 MT).


Weekly Export Details
Exports of 24,200 MT were down 12% from the previous week, but up 68% from the prior four-week average.  The destinations were primarily South Korea (15,000 MT), Mexico (4,700 MT), Guatemala (4,000 MT), and Canada (400 MT).


Comments and Performance Indicators
Sales were X expectations. Export commitments for the 2016-17 marketing year are running 7% behind year-ago, compared to 8% behind year-ago last week. USDA projects exports in 2016-17 to be up 7.0% from the previous year.




Cotton



Actual Sales (in RB)
Combined: 260,900
			2016-17: 28,300
			2017-18: 232,600


Weekly Sales Details
Net upland sales of 28,300 RB for 2016-17 were up 4% from the previous week, but down 77% from the prior four-week average.  Increases were reported for Pakistan (11,000 RB), Thailand (4,900 RB, including 1,400 RB switched from Japan), Bangladesh (4,400 RB), Indonesia (4,100 RB, including 2,600 RB switched from Japan), Mexico (4,000 RB), and Morocco (2,200 RB).  Reductions were reported for Japan (7,600 RB) and Ecuador (200 RB).  For 2017-18, net sales of 232,600 RB reported primarily for Bangladesh (63,800 RB), Turkey (39,700 RB), Vietnam (27,300 RB), and South Korea (24,000 RB), were partially offset by reductions for Taiwan (1,800 RB). 


Weekly Export Details
Exports of 326,800 RB were up 17% from the previous week and 27% from the prior four-week average.  The primary destinations were Turkey (88,500 RB), Vietnam (49,700 RB), China (30,400 RB), India (27,900 RB), and Bangladesh (25,900 RB). 


Comments and Performance Indicators
Export commitments for 2016-17 are 62% ahead of year-ago, compared to 62% ahead the previous week. USDA projects exports to be up 58.5% from the previous year at 14.500 million bales.




Beef



Actual Sales (in MT)

2017: 13,900



Weekly Sales Details
Net sales of 13,900 MT reported for 2017 were up 13% from the previous week and 5% from the prior four-week average.  Increases were reported for South Korea (4,300 MT), Egypt (2,300 MT), Mexico (1,800 MT), Japan (1,700 MT), and Canada (1,300 MT).  Reductions were reported for Indonesia (200 MT) and the Republic of South Africa (100 MT).


Weekly Export Details
Exports of 15,500 MT--a marketing-year high--were up 5% from the previous week and 10% from the prior four-week average.  The primary destinations were Japan (5,300 MT), South Korea (3,500 MT), Mexico (1,800 MT), Hong Kong (1,600 MT), and Canada (1,400 MT). 


Comments and Performance Indicators
Weekly export sales compare to 12,400 MT the week prior. USDA projects exports in 2017 to be up 10.2% from last year's total.




Pork



Actual Sales (in MT)
2017: 19,100


Weekly Sales Details
Net sales of 19,100 MT reported for 2017 were up 63% from the previous week and 26% from the prior four-week average.  Increases were reported for Mexico (9,300 MT), Japan (3,100 MT), South Korea (2,100 MT), Canada (1,700 MT), and Australia (1,100 MT).  


Weekly Export Details
Exports of 17,900 MT were down 1% from the previous week, but up 2% from the prior four-week average.  The destinations were primarily Mexico (7,600 MT), Japan (3,700 MT), Canada (1,700 MT), South Korea (1,100 MT), and Hong Kong (900 MT).


Comments and Performance Indicators

Export sales compare to a total of 11,700 MT the prior week. USDA projects exports in 2017 to be 9.7% above last year's total.