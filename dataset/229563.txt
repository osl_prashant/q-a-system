The avocado is truly at a rare point in popular culture, with restaurateurs willing to tie the fate of their business to it alone.
Few fruits have the fashion factor and health halo of avocados, so I suppose I should not be surprised, but to some degree I still am.
Avocaderia in Brooklyn, N.Y., recently received a $400,000 investment on the ABC show “Shark Tank,” where some of those on the panel evaluating the startup were concerned about the lack of variety on the menu given the possibility people could burn out on the fruit.
I do not see that as an issue at all — particularly in the trendy areas where locations of this restaurant will no doubt be placed — but I wonder how much consideration the owners of the business have given to the issue of avocado prices.
Reduced supply in 2017 pushed prices to incredible levels. The standard restaurant that uses avocados as one of many ingredients probably can scale back on the amount of the fruit used or raise prices slightly on dishes where it is necessary or prominent, but an all-avocado restaurant would be in a dire predicament.
For a place at which avocado is the star of every dish, navigating a prolonged price spike could be challenging.
Count me among the more risk-averse of my avocado toast-crazed generation.
Then again, getting billionaire Mark Cuban and real estate mogul Barbara Corcoran on board via “Shark Tank” is a way to mitigate the risk.
Avocaderia is not the only place with the fruit as its sole focus, however. 
Avocado Appetit in Chinatown, N.Y., and The Avocado Show, currently in Amsterdam and planning to expand to New York, have similar missions.
I like the idea of diversification, so the thought of anchoring my livelihood to a single specialty crop is frightening, quite frankly.
Of course, many of the growers who feed us all do exactly that, year in and year out — so maybe this isn’t so crazy after all.
Ashley Nickle is a writer for The Packer. E-mail her at anickle@farmjournal.com.