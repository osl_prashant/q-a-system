Markets for all classes of cattle remain 20% or more below a year ago, but Drovers indicators are a snapshot of the collective markets and their direction.Feedyards have turned profitable and packers have enjoyed several months of good earnings‚Äîboth positive indicators.


Production costs and animal performance continue through a good stretch of prices and weather.
Retail beef prices are weakening, which should help move more product. Retail prices for both pork and poultry are also lower, keeping the pressure on.
Beef cow and heifer slaughter tick up, suggesting expansion has slowed.




Note: This story appears in the January 2017 issue of Drovers.