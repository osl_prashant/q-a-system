This market update is a PorkNetwork weekly column reporting trends in weaner pig prices and swine facility availability.  All information contained in this update is for the week ended September 15, 2017.

Looking at hog sales in March 2018 using April 2018 futures the weaner breakeven was $45.72, up $4.59 for the week. Feed costs were up $0.33 per head. April futures increased $0.20 compared to last week’s April futures used for the crush and historical basis is improved from last week by $2.19 per cwt. Breakeven prices are based on closing futures prices on September 15, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was above average this week with 40,000 head being reported. That is 104% of the 52-week average. Cash prices were $23.10, up $0.56 from a week ago. The low to high range was $17.00 - $28.00. Formula priced weaners were up $0.34 this week at $37.77.
Cash-traded feeder pig reported volume was below average with 5,845 head reported. Cash feeder pig reported prices were $36.96, down $0.90 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through September 15, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential increased $4.03 this week to a projected gain of $22.62 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.