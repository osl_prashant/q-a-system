Corn




The national average corn basis firmed 2 1/4 cents from last week to 5 cents below May futures. The national average cash corn price declined 1/2 cent from last week to $3.54.
Basis is slightly weaker than the three-year average, which is 3 cents above futures for this week.












Soybeans





The national average soybean basis firmed 3 3/4 cent from last week to stand 19 cents below May futures. The national average cash price improved softened 3/4 cent from last week at $9.26 3/4.
Basis is softer than the three-year average, which is 1 1/4 cents above futures for this week.












Wheat





The national average soft red winter (SRW) wheat basis firmed 9 3/4 cents from last week to 6 1/4 cents below May futures. The average cash price declined 1 1/2 cents from last week to $4.01 1/2. The national average hard red winter (HRW) wheat basis firmed 1 3/4 cent from last week to 86 cents below May futures. The average cash price declined 2 3/4 cents from last week to $3.26 1/4.







SRW basis is firmer than the three-year average of 16 cents under futures. HRW basis is much weaker than the three-year average, which is 40 cents under futures for this week.










Cattle




 




Cash cattle trade averaged $131.60 last week, up $3.59 from the previous week. Based on today's online auction, cash cattle trade should be at least $1 higher this week. Last year at this time, the cash price was $126.45.
Choice boxed beef prices have firmed $4.01 from last week at $219.18.  Last year at this time, Choice boxed beef prices were $214.81.











Hogs





 




The average lean hog carcass bid dropped $1.96 over the past week to $59.93. Last year's lean carcass price on this date was $69.06.
The pork cutout value dropped $1.71 from last week to $72.84. Last year at this time, the average cutout price stood at $81.30.