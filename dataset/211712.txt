Joseph M. Procacci, president of Santa Sweets Inc. and chief operating officer of Procacci Bros. Sales Corp., has been named CEO of Procacci Holdings LLC.  
Long-time CEO Joseph G. Procacci — with nearly 70 years as leader of the company — is stepping down as CEO and transitioning to the role of CEO Emeritus, according to a news release.
 
“I’m honored to follow in my dad’s footsteps and humbled to lead what my dad always said was our greatest asset — our people,” new CEO Joseph M. Procacci, known as J.M. to employees and customers, said in the release. 
 
Citing the company’s mix of young talent and experience, he predicted a bright future for the firm. 
 
“Under my leadership, we will continue to honor the core values that took Procacci Bros. to the top of the produce industry, while also innovating and adapting to the evolving needs of our customer,” he said in the release.
 
Joe Procacci said in the release that it has been a blessing to have his son by his side in expanding the company over the years. “It’s with great pride and confidence that I turn over the future of the organization to his leadership,” he said in the release.
 
Mike Maxwell, president of Procacci Bros., praised father and son. “The transition from one generation to the next is a very natural process in a family-owned business,” he said in the release. 
 
“J.M. grew up in this business and has worked in all areas of this vertically integrated company,” he said. “I look forward to working with him in growing our business into the future and I wish Joe Procacci all the best in his retirement.”
 
Growing up to lead
 
J.M. Procacci started working alongside his father at the age of 10, according to the release, learning every part of Procacci Bros.’ business operations. He joined the company full-time after graduating from Saint Joseph’s University in 1974 with a degree in labor relations. 
 
Making quick contributions to Procacci’s tomato repack operations, J.M. led improvements in labor efficiency, quality assurance and food safety, according to the release. In subsequent years, J.M. led Procacci’s tomato sales desk, where he focused on developing supplier networks and capitalizing on logistical opportunities in Procacci’s distribution network.
 
He also has played a big role in the firm’s human resources functions, according to the release, including talent acquisition, employee healthcare and labor relations. 
 
Currently, J.M. is a Trustee of the Teamsters Local 929 Pension Fund and works with Union Committees to increase job opportunities.
 
In 2007, J.M. moved away from sales to lead Procacci’s vertically integrated growing operation, Santa Sweets Inc., which the release said is North America’s largest producer of grape and heirloom tomatoes. 
 
J.M.’s guidance of the operation led to outstanding progress in growing efficiencies, sustainable growing methods, organic production and labor practices, according to the release. 
 
Most recently, J.M. directed significant progress in the company’s farming operations in Mexico, according to the release. One project included developing more than 800 acres of table grape production and building 120 acres of shade house growing structures.