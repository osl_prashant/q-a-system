The current price of hay is a frequent question asked by callers to the area county extension offices. It is an important question to cattle producers wanting to turn a profit because hay costs represent a significant overall expense in raising cattle.
"The calls come from sellers as well as people looking to purchase hay. On the surface, this seems like a simple question. In reality, there are factors to consider when buying or selling hay," said Ted Probert, dairy specialist with University of Missouri Extension.
Hay Considerations
First, hay quality is an important factor. All hay is not equal from the standpoint of nutrient content. That means all hay is not equal regarding monetary value.
"Hay buyers should be aware of the nutritional needs of the animals they are feeding and look for hay that will meet those needs. Some classes of livestock have higher nutrient requirements than others," said Probert.
For example, lactating and growing animals have higher nutrient needs than dry cows.
"Purchasing hay of sufficient quality will be more practical and more economical than buying less expensive, poorer quality hay that requires supplementation to achieve animal performance goals," said Probert.
Ideally, from a buyer's standpoint, a nutritional analysis of the hay under consideration would be available. A hay test is a gold standard for determining feed value. In reality, hay tests are more often than not unavailable on hay offered for sale.
"In the absence of a hay test, buyers need to remember that maturity of forages, when harvested, is the primary factor influencing feed quality. The earlier harvest dates within a given season will almost always produce higher quality hay," said Probert.
Additionally, an inspection of the hay under consideration can be helpful according to Probert.
"From a seller's perspective, availability of a nutritional analysis on the hay offered for sale will be a plus - if the analysis confirms that the hay is of high quality," said Probert.
Pricing of Hay
Another consideration for hay transactions is how the hay is priced.
The fairest and most accurate means of pricing is by the ton. Pricing by the ton leaves little doubt as to what a buyer is getting for his or her money.
"A lot of sellers, though, prefer to sell by the bale. The problem with per bale pricing is that not all hay packages are of the same dimensions," said Probert.
A note to sellers: most informed buyers prefer to make their purchases by the ton. Doing business in this manner will be looked upon by many buyers as a plus, and a factor that may make the difference in whether or not a sale is made.
Missouri Hay Directory 
Individuals that would like to get an idea about current hay prices in Missouri can look at listings on the Missouri Hay Directory online at https://agmarketnews.mo.gov/hay-directory.
These listings are a joint venture of the Missouri Department of Agriculture and the University of Missouri.
The hay listings include sellers names, cities, counties and phone numbers. Bale type is included along with the number of bales and approximate weight and if the hay has been analyzed.
Individuals can list their hay using the menu options or by calling the Missouri Department of Agriculture at 573-751-5633.
Another good source of pricing information is the Weekly Missouri Hay Summary which can be athttps://www.ams.usda.gov/mnreports/jc_gr310.txt.