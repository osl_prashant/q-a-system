State lawmakers pass bill to guard cancer patient fertility
State lawmakers pass bill to guard cancer patient fertility

The Associated Press

ANNAPOLIS, Md.




ANNAPOLIS, Md. (AP) — The Maryland General Assembly has passed a bill aimed at preserving the fertility of those undergoing medical treatments that could cause infertility.
The Baltimore Sun reports the legislation requires insurance companies to pay for standard fertility procedures, such as sperm or egg freezing for people going through chemotherapy. The bill doesn't cover embryo freezing or the cost of storing frozen sperm or eggs, which can cost up to $500 per year.
Some fertility preservation treatments can cost as much as $20,000, and most insurance companies don't cover the procedures. An independent audit commissioned by the state found the bill's impact on insurance costs would be minimal. Gov. Larry Hogan spokeswoman Amelia Chasse says the governor may sign the bill into law pending an opinion from the attorney general's office.
___
Information from: The Baltimore Sun, http://www.baltimoresun.com