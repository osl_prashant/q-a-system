Firebaugh, Calif.-based tomato grower-shipper Red Rooster Sales is looking to the sun to power its business in a future of skyrocketing costs.
The company has installed 450 panels in its packinghouse and more in production areas, said Tom Frudden, sales manager.
“We have solar panels not just at the packinghouse, but it’s been really instrumental on the ranches,” he said.
The panels are particularly valuable in powering wells, he said.
“Solar has been a push for the last four years, just for the pure cost of what it costs to run those wells,” he said.
Investment in solar power has become an answer to a frustrating spike in operating costs, Frudden said.
“The cost of doing business here in California is taking a toll on business in general in the state, but it’s really making it challenging, with new regulations and new costs,” he said.
Red Rooster Sales installed panels in its packinghouse over the last three years, Frudden said.
When asked how much money solar power has saved the company, Frudden said he didn’t have an exact monetary figure handy.
“I can tell you this: We added the second (packing) line three years ago, and the cost of running the single line was such-and-such, and by adding the second line, our bill didn’t change,” he said.
The panels have enabled the company to invest in other areas, as well, Frudden said.
“We increased our de-greening capacity two years ago, adding an additional equivalent of another 48,000 boxes in the de-greening rooms,” he said.
Red Rooster has gone on a pretty rapid growth cycle for the last four or five years, basically doubling the volume the company did four or five years ago, Frudden said.
“When I came aboard six years ago, we did about 1.6 million packages — 90% rounds and 10% romas — it took the last couple of years to produce between 3.4 million to 3.6 million rounds and romas,” he said, noting that the percentages haven’t changed.
“Romas are there to basically complement the round program,” he said.