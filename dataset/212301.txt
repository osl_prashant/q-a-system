"Thick-skinned and worthless.” 
That is how grapefruit, which had long been growing in the Caribbean, was described in a U.S. gardening magazine in the 1820s. A cross between an orange and a pummelo, from its beginning grapefruit was a genetically modified organism.
 
A century later Texas growers were successfully growing grapefruit in the Rio Grande Valley. They had begun marketing fresh grapefruit with flesh colors ranging from pink to red. The fruit was colorful but bitter.
 
In 1929 a Texas grower, A.E. Henninger, noticed one of his trees produced ruby red fruit that was sweeter than any grapefruit he had ever tasted.
 
That tree, in an orchard planted with the pink marsh variety, was different — much different. It was a “sport.”
 
Sports are natural genetic mutations that appear randomly in orchards. They are GMOs from Mother Nature. Such mutations are a fundamental part of evolution.
 
Henninger named the new grapefruit Ruby Red and received a U.S. patent for it.
Variety improvement helped the Texas grapefruit industry develop to such an extent that its product — red grapefruit — became the Texas state fruit.
 
A half-century later, Texas A&M University researcher Richard Hensz developed an improved ruby grapefruit that was even sweeter and more brightly colored. He named it Rio red. Soon almost every Texas grower was producing Rio reds.
 
Working at a research station in Weslaco, Hensz used mutagenesis as a breeding tool. 
 
Mutagenesis scientists bombard plants with radiation or toxic chemicals to shatter and scatter genes. The process provides new plants with a wide genetic variation that would not occur in nature or with conventional cross-breeding.
 
Similar to natural sports, mutagenesis results are random. Like growers looking at possible sports, mutagenesis breeders search for gems among the many plants they produce.
 
Variety improvement helped the Texas grapefruit industry develop to such an extent that its product — red grapefruit — became the Texas state fruit.
 
About the same time, the potato became the Idaho state vegetable.
 
The Idaho potato industry was built around one variety. The lineage of that variety includes rough purple chili, garnet chili and early rose.
 
In 1873 in New England, a young man observed an unusual looking plant in his mother’s potato garden. From that plant he collected and planted 23 seeds, which were products of crossbreeding. The female parent was early rose, but the male parent is unknown.
 
One of the potato plants produced long, smooth tubers with white skins. The young man, Luther Burbank, sold it to a seed company that introduced it as the burbank variety in 1876. 
 
Later Luther moved to California where he became a prolific, famous plant breeder.
 
A sport of burbank became the russet burbank. It was resistant to some diseases and had a brown, netted skin. A seed company, L.L. May in St. Paul, introduced it in 1902.
 
The russet burbank slowly but steadily became accepted by growers and consumers and eventually replaced burbank. Since the mid-1900s it has been the most popular North American variety.
 
The Idaho potato industry developed with the russet burbank on stage. The long russet became so identified with Idaho that some people called it the Idaho potato. Some still do.
 
More than 100 years after its introduction, the russet burbank has been improved. Simplot Plant Sciences developed two generations of russet burbank that the company named Innate. They are sold in the fresh market under the White Russet brand.
 
Generation 1 has low acrylamide and is bruise resistant. Generation 2 has those traits plus two more — late blight resistance and cold storage capability. 
 
Innate potatoes also have significant sustainability advantages. Disease resistance means fewer fungicide sprays, and reduced bruising means more usable potatoes can be harvested on less acreage, further saving on water, carbon emissions and pesticides.
More than 100 years after its introduction, the russet burbank has been improved. Simplot Plant Sciences developed two generations of russet burbank that the company named Innate. They are sold in the fresh market under the White Russet brand.
 
Simplot scientists used what I call “green biotech” to develop Innate. They employed modern biotech techniques but only used genetic material from other potatoes to get the desired traits.
 
Unlike mutagenesis, the Simplot method used precision rather than randomly scattering genes.
 
And yet, Innate potatoes are heavily regulated. Simplot went through a long, expensive process to get regulatory approval. The U.S. Department of Agriculture, the Food and Drug Administration and Environmental Protection Agency all approved the biotech potatoes, but the process was onerous.
 
It is ironic that Rio red grapefruit and other plant varieties produced with mutagenesis are not regulated.
 
Even more ironic is that Rio red grapefruit are in the organic market channel, but White Russets are not allowed.
 
Joe Guenthner is an emeritus professor of agricultural economics at the University of Idaho. 
 
What's your take? Leave a comment and tell us your opinion.

Variety improvement helped the Texas grapefruit industry develop to such an extent that its product — red grapefruit — became the Texas state fruit.