89-year-old 'Flower lady of Staunton' leaves after decades
89-year-old 'Flower lady of Staunton' leaves after decades

By LAURA PETERSThe News Leader
The Associated Press

STAUNTON, Va.




STAUNTON, Va. (AP) — There's a house on Staunton's West Beverley Street that is always in bloom.
The house stands out. It's something everyone talks about, looks at and loves.
Now, the flowers are slowly disappearing and the house is up for sale — and the flower lady of Staunton has moved out.
Myrtle Cason, who's 89 years old, has lived in Staunton for more than 40 years.
She's called 502 W. Beverley St. home for quite some time. It's well known for the overabundance of faux flowers inside and out. The home even became a tourist destination, according to RoadsideAmerica.com.
"Sometime along the way she began decorating her home in her own special way. Although we can agree that we do not all share her eclectic taste, neighbors have seen people stop to take pictures at the home or selfies with the home in the background," said Ed Pickett of the Newtown Neighborhood Association. "Many children and youth have described the house as 'It's just so cool!' and have to check it out whenever they go by."
According to city records, Cason purchased the home in 2001. Now a sign hangs on the front porch that reads, "For sale by owner."
"At first, our interest was one of concern, because we had not seen her and we could tell she was not maintaining her special container garden the way she used to," Pickett said.
Cason has been moved into a nursing home, and the flowers have started to go away. Over the weekend, the Newtown Neighborhood Association lined the streets with faux flowers in her honor.
"We were relieved to learn that she was safely in a local nursing home," Pickett said. "That inspired us to do a little Myrtle container garden, partly for her, but if we are honest, mostly for us and for all those that can get a grin out of kitschy but cool or a spot of abundant blooms out of season in February."
The block between St. Clair and Madison, which Cason's house is on, is lined with potted fake flowers. The initiative started last weekend and finished March 1.
The display inspired memories of Cason.
Sharon Starcher used to live near Cason and remembers her tediously tending to her flowers. But it wasn't just flowers; it was frogs, fairies and butterflies tucked into the display.
"She was absolutely an interesting lady," Starcher said. "She really did have a colorful home. It was something that could not be missed. Myrtle and her flowers are something I will never forget about. Ever."
Molly Murphy, a resident of Newtown, noticed the flowers over the weekend.
"I didn't know her at all," she said. "Just appreciated the beauty she shared."
Travis Norton, who owns Norton Upholstery a block down from the home, said Cason was a hoot. But, he'd always lend a hand if she needed help bringing things inside her home.
"She was just one of those people that became a daily fixture in the cast of characters that I would see go up and down Beverley Street while I was at work," he said. "She would stop in sometimes and want to buy pillows but only if they had a floral design on them."
Norton said one day in 2014, he helped Cason set up an answering machine in her home.
"Keep in mind it's like 2014 and nobody has an answering machine anymore. But there it is, with the little cassette tape and everything," he said. "So, I finally got it working and she was so happy. She even offered me a dollar for helping her.
"Everybody thought the outside of her house was over the top but I can assure you, the inside was just as wild," Norton added. "She had this brass lion that was about 4 feet long. She made a point to tell me she painted its tongue red with nail polish. She will definitely be missed."