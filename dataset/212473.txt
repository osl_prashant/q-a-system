Even in a year of relatively light avocado supplies, there will be an abundance of promotions across North America for Peru’s peak summer season, the Peruvian Avocado Commission says.Peru is forecasting U.S.-bound shipments of about 150 million pounds of avocados for the peak season, which runs from early June to the end of August.
U.S. retailers will get their first peak supplies in June, and supplies should continue until about Sept. 2, according to the commission.
“With avocados being America’s favorite superfood, we expect the consumption of avocados to continue rising this summer, which is great because this is our largest crop to-date,” said Xavier Equihua, CEO and president of Washington, D.C.-based PAC. “Our primary goal this season is to supply the U.S. with top-quality fruit during the peak summer season, and to educate the public about the wonderful health benefits of Peruvian avocados.”
Promotions will focus on nutrition, among other assets, Equihua said.
 

Wal-Mart partnership

To start the season, the commission’s Avocados from Peru program will partner with Wal-Mart stores to participate in its annual Wellness Day June 17. More than 800 stores will feature and demo an avocado and blueberry smoothie, made with Peruvian avocados, plus blueberries, banana, spinach leaves, almond milk and Greek yogurt.
Wal-Mart will continue to demo avocado recipes throughout the summer, including an avocado peach crostini. To help promote demo days and help drive store traffic, Avocados From Peru has created a targeted geo-fencing mobile program that will activate within 5 miles around key stores.
Summertime demos also are scheduled at Costco stores, Equihua said.
Avocados From Peru will run a promotion with the CBS radio network and feature the AvoDog at the Baltimore Orioles stadium during Fourth of July week.
AFP’s 2017 marketing program introduces an experiential three-month long campaign that includes “high-profile partnerships,” increased trade and consumer advertising, an expanded menu of retailer support and incentive programs and social media programs, Equihua said.
 

Advertisements

The Peruvian Avocado Commission also will have co-branded newspaper ads and bus wraps; bins with graphics featuring the group’s Machu Picchu brand image; and funding for demos leading up to the July Fourth holiday.
In terms of consumer outreach, Avocados From Peru will run retailer co-branded ads inVogue, Vanity Fair and Women’s Health magazines, communicating a “health and beauty” message; relaunch a digital Avocados From Peru cookbook; and run radio spots in key markets.
Radio stations will run contests around Father’s Day and the Fourth of July, asking listeners to submit recipes.
The program will also partner with chefs, the president of Peru and the Peruvian Trade Commission, including a co-sponsorship with PromPeru of an event at the National Museum of the American Indian in Washington, D.C.
Digital advertising will feature videos highlighting Peru as “the land of superfoods.”
Social media programs on Facebook, Instagram, Pinterest and Twitter will include recipes, photographs and opportunities to share usage tips.
In the past, the commission has wrapped city buses in select East Coast cities with Peruvian avocado ads, and that likely will happen again through this season, Equihua said.
They’re known as “avo-buses,” he said.
“They take over the entire bus with a banner that says, ‘Avocados from Peru,’” he said. “We hire them for two months and usually they stay on the bus for four.”
Radio ads are tailored to each station’s format, as well, which “makes more sense” than one standard ad, Equihua said.
Marketers say the promotional program is effective.
“The folks on the Peruvian Avocado Commission are running some very nice promotional programs with those retailers that have signed on to support the Peruvian avocado when it gets here,” said Rankin McDaniel, president of Fallbrook, Calif.-based McDaniel Fruit Co.