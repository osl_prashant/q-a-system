Hialeah Tomatoes & Fresh Produce has purchased a Redland, Fla., packinghouse.
The Redland Warehouse is a 55,600-square-foot packing and refrigerated warehouse situated on more than 4 acres in the Redland agriculture district, according to a news release.
Hialeah paid Gulfstream Tomato Packers more than $3,4 million for the site, the release said.
The facility will be used for the distribution of fruits and vegetables, some locally grown and some imported, the release said.
Hialeah Tomatoes & Fresh Produce also operates another refrigerated warehouse in the Produce Center near downtown Miami.