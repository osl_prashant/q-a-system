As my favorite baseball team was determined not to make the playoffs, I can only think about the "what if." They are a good team, maybe even a very good team, but apparently, they are not a great team. The difference can be measured in inches.I believe they call football a game of inches. As a diehard baseball fan, I am coming to believe that baseball is truly the game of inches. And that is what separates the great teams from also-rans.
A critical pitch that was just an inch outside the strike zone and walked the batter.
A batter that missed hitting the ball squarely by an inch and instead fouled it off.
A tag that missed the stealing baserunner by an inch and he was safe.
A potential home run ball that hit the top of the fence and bounced back in for a ground rule double.
The glove that was just an inch too low and the ball tipped off of it into the outfield for a base hit.
Granted, if I played the game, it wouldn't be a matter of inches, but of yards! Inches are what separates good teams from great ones, not poor ones from great ones.
I would argue that same principle applies to dairy farms. In trying to encourage and challenge farmers, I have said, "let's keep moving from being a good dairy to being a great one!" What are the "inches" that separate them?
It is a percentage point less neutral detergent fiber (NDF) in the haylage.
It is a pound more of dry matter intake.
It is a decrease of 5 percent in the new (mastitis) infection rate.
It is the quarter ton yield difference from each acre of silage corn.
It is one more thing done by employees before they leave their shift.
It is an additional pint of colostrum intake on a regular basis by newborn calves.
It is routinely feeding colostrum a half hour earlier.
It is taking the 2 seconds needed to thoroughly clean teat ends before milking.
It is changing needles before every injection.
It is taking another second to be safe with all your farm operations.
It is taking a moment to thank your employees for their hard work and extra effort.
It is a thousand more small details. These are all little things, little things that make a difference. Managing a dairy is about getting a little more from employees, from fields, from cows and from yourself. Sure, you are already getting a lot; that is why you are good or even very good, but being a great dairy is about an "inch" more than that. That doesn't mean that we work "10 percent harder or longer", but that in our work we accomplish more.
One may ask if it is worth it to be "great." After all, the dairy goal is not to be called great or to win the World Series; the goal is to provide for the families involved and make a future for the next generation. So is "greatness" really necessary?
Maybe the answer is in the financial return during times of narrow margins. Does that matter to you? If income over feed costs improved by $0.75 per hundredweight because of attention to detail, might that be a good enough reason?
Maybe the answer is in the effort it takes and the enjoyment received from farming. Is that important? I have learned that two farmers can have the same somatic cell count (SCC), yet one has to work much harder than the other, with greater frustration to achieve that. The one may achieve it only by discarding milk from high SCC cows, treating more cows or culling chronic mastitis cows; whereas the other farmer has a lower new infection rate and therefore, deals with fewer infected cows. One might say that still they both achieved the same quality of milk, and I would answer that one enjoyed it more.
It is not for the sake of greatness that greatness should be pursued. It is for the sake of the business and the satisfaction. It is about knowing where you are at and setting your sights to achieve a little more through closer attention to detail, more timeliness and greater thoroughness. The pursuit of greatness isn't long; it is just inches!