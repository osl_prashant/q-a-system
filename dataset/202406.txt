The dollar and convenience store trend may be a downer for fresh produce consumption. That is at least one takeaway from a new report from the U.S. Department of Agriculture Economic Research Service  that looks at where consumers shop and how those purchase patterns respectively align with store formats.  
 
Called "Store Formats and Patterns in Household Grocery Purchases," the report was written by Richard Volpe, Annemarie Kuhns, and Ted Jaenicke.
 
USDA researchers observed the rise of the supercenter has garnered the most attention in the last 20 years. The USDA ERS calculated the share of food at home expenditures from 1999 to 2012 using Nielsen data and an IRI panel.  During that period, the supermarket share of food sales fell from 80% to about 62%, while the share for supercenters increased from 3% to 18%.
 
During 2008 to 2012, the decline in the share of supermarket sales slowed, as did the expansion of the supercenter.  However, club store sales share grew from 7% to 9% in those four years and the dollar store share climbed by almost a percent.
 
From the report:
 
Expenditures on fruits, vegetables, whole grains, and lean protein sources are highest at supermarkets and club stores, and lowest at convenience stores, drug stores, and dollar stores.
The store formats at which consumers shop influence what they purchase. Supermarkets and club stores positively correlate with higher dietary quality of groceries (fresh fruits, vegetables, etc.). On the other hand, convenience stores-the nearest retail food store for many  households in dense urban environments-and dollar stores correlate negatively with the purchase of healthful food. In general, these correlations are weak, but in both directions (toward more healthful and less healthful), they tend to be strongest for low-income households and weakest for high-income households.
Convenience stores are a prevalent format for many households, particularly those in dense urban environments, and they are associated with lower expenditures for fruits, vegetables, whole grains, and lean proteins. 
This correlation is valid for IRI households even though the sample does not fully represent lower income households. 
Dollar stores are making inroads in the grocery industry across the country, and although their share is still small, they are gaining rapidly. The format is likely to evolve with time, but at present, seems to adversely affect diet quality. 
More work is needed to properly disentangle the various stores included in the other category and identify what may be driving the associations connected with it.
Although proximity to a certain type of format may not drive the dietary quality of the foods a household purchases, the store format a household selects for the majority of its food purchases does significantly influence dietary quality. 
We provide broad evidence that store format selection is associated with particular food choices among Americans.
As the U.S. food retail industry continues to diversify, these impacts can grow in economic importance. 
Given that store format seems to be associated with the dietary quality of food purchases, significant relationships may exist between store formats and health outcomes. 
 
TK: What comes first, Cheetos or the convenience store? The lack of a strong fresh produce presence in many convenience and dollar stores may relate to the fact that consumers aren't going to those stores to buy romaine lettuce and cauliflower. Even so, produce marketers must work to enhance the visibility of fruits and vegetables at small retail formats because of these shopper trends.
 
To the positive,  chains like Dollar General are expanding their fresh produce offerings CNBC coverage said the chain is testing fresh produce in some of its stores, including locations it bought from Wal-Mart. What's more, CNBC said Dollar General will expand the fresh pilot to some of its smaller shops.
 
I was disappointed that the USDA report didn't specifically talk more about the growing impact of Aldi and pending expansion of Lidl in the U.S. 
 
The industry is obviously aware of consumer habits. Last year, the United Fresh Produce Association and the National Association of Convenience Stores partnered to publish a document that outlines options for convenience stores that want to carry more fresh produce.  Cheetos may be safe, but  fresh fruit and vegetable sales at convenience stores grew 14.4% in 2015.  That's a start.