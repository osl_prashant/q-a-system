Crop calls 
Corn: Narrowly mixed
Soybeans: 3 to 5 cents higher
Wheat: Steady to a penny higher
Soybean and wheat futures were firmer overnight, while corn held near unchanged. Similar price action is expected to open today's session. Trading volume is expected to be light today ahead of Thanksgiving. Some traders won't return until next Monday, though markets are open for an abbreviated trading session Friday. 
Fresh news is limited, meaning chart-based trade is likely to guide price action. For wheat (and potentially corn), that could lead to short-covering ahead of the holiday break. For soybeans, followthrough from the overnight session is likely to fuel additional buying this morning, though improving weather in Brazil could limit buyer interest.
 
Livestock calls
Cattle: Slightly higher 
Hogs: Lower
Cattle futures were boosted by short-covering late Tuesday, which is expected to result in followthrough corrective buying this morning. Cash cattle trade got underway at steady to $1 lower prices in the Plains, which was better than some feared. December live cattle futures ended yesterday just below the bottom end of initial cash cattle trade, so buyer interest is likely to be limited.
Hog futures closed low-range with sharp losses Tuesday. That's likely to spur followthrough selling this morning. But it wouldn't surprise us if corrective buying surfaces ahead of the Thanksgiving holiday, especially if initial seller interest is limited.