The Trump Administration appears to be in favor of allowing imports of lemons from Argentina, after twice delaying a rule blocking them in the U.S. 
Citing the need for White House review, the U.S. Department of Agriculture’s Animal and Plant Health Inspection Service in January and again in March issued 60-day stays of a Dec. 23 final rule allowing lemons from Argentina into the U.S.
 
President Donald Trump and Argentine President Mauricio Macri had a press conference at the White House April 27, and Trump responded to a question from a reporter about the status of the Argentina lemon rule.
 
“One of the reasons (Macri) is here is about lemons — and I’ll tell him about North Korea, and he’ll tell me about lemons,” Trump said in a video posted by Bloomberg. “I think that we’re going to be very favorably disposed. We’re going to be talking.”
 
California citrus leaders, including Joel Nelsen, president of California Citrus Mutual, earlier said the USDA final rule was flawed and that U.S. citrus producers will be hurt by Argentina’s lemon imports.
 
Rep. Julia Brownley, D-Calif., said in a April 27 statement that Trump should “go back to the drawing board” to make sure the rule excludes pests and protects American jobs.
 
“President Trump should not sell American workers and American citrus growers down the river as a favor to the President of Argentina,” she said in the statement. “We cannot introduce devastating, industry-killing pests and diseases into the country.”