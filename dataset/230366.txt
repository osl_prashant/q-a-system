Class AAA State Tournament Results
Class AAA State Tournament Results

The Associated Press

HUNTINGTON, W.Va.




HUNTINGTON, W.Va. (AP) — Thursday and Friday Results from the Class AAA high school wrestling tournament at the Big Sandy Superstore Arena:
Team Standings
1. Parkersburg South 240.00, 2. Huntington 116.00, 3. Parkersburg 104.50, 4. University 87.50, 5. St Albans 80.00, 6. John Marshall 79.00 7. Wheeling Park 76.50, 8. Spring Valley 71.50, 9. Greenbrier East 65.50, 10. Washington 52.50.
Individual ResultsFirst Round106
Jace Stockett, University, fall Nate Cox, Ripley, 0:48; Landen Hoover, Greenbrier East, fall Dominic Emswiler, Washington, 4:27; Garret Donahue, Parkersburg, fall Zach Riffle, Preston, 1:48; London Dotson, Riverside, fall Nathan Bowers, Musselman, 1:54; Michael Dolan, Spring Mills won by forfeit over won by forfeit overeit, No School; Devin Easton, Parkersburg South, fall Isaak Murphy, Wheeling Park, 3:45; Thomas Hartley, St Albans, fall Colton White, Jefferson, :43; Devin Doss, Huntington tech. fall Danny Yost, Brooke, 16-0.
113
Julius Hobbs, Buckhannon-Upshur, fall Trent England, Princeton, 0:55; Brayden Johnson, Parkersburg South, fall Jacob Curry, Hampshire, 4:59; Brandon Holt, St Albans, fall Calvin Matthews, University, 1:10; Luke Miller, Ripley, fall Joey Wilson, Hedgesville, 0:30; London Bowen, Huntington won by forfeit over Foreit, No School; Bryson Wilkins, Preston, fall Cobie Johnson, Riverside, 1:16; Jacob McCarren, Spring Mills, fall Levi Westfall, Parkersburg, 0:15; Ethan Gray, John Marshall, fall Tyler Huffman, George Washington, 1:14.
120
Jake Staud, University, fall Sierra Powell, Hampshire, 3:12; Alex Viars, Huntington, dec. Tyler Powell, St Albans, 10-8; Ethan Hardy, Washington, fall Bailey Radabaugh, Preston, 2:38; Eian Harper, Spring Valley, fall Cameron Stover, Woodrow Wilson, 1:36; Isaiah Isabell, George Washington, dec. Ethan Koontz, Ripley, 8-5; Maison Richardson, Wheeling Park, dec. Cody Shindledecker, Musselman, 10-9; Tucker Windland, Parkersburg South, fall Waymon Browning, Greenbrier East, 0:44; Hunter Dodson, Spring Mills, fall Breagan Pearson, Buckhannon-Upshur, 0:59.
126
Jacob Simpson, University, maj. dec. Treven Taylor, Hurricane, 12-3; Eric Dreyer, Washington tech. fall Zach Messer, St Albans, 15-0; Brayden Roberts, Parkersburg South, fall Canon Welker, Wheeling Park, 1:30; Cayden Hoover, Greenbrier East, fall Theo Yates, Jefferson, 4:49; Joey Miller, Musselman, fall Jordan Meadows, Capital, :14; Austin McCartney, Buckhannon-Upshur, fall Payne Salmons, Cabell Midland, 5:56; Jack Lorea, George Washington, fall Kyle Price, Hedgesville, 2:40; Tristan Adkins, Huntington, maj. dec. Peyton Shook, John Marshall, 14-0.
132
Billy Gooch, Wheeling Park, dec. Brock Perry, Capital, 11-4; Gavin Quiocho, Parkersburg South, fall Dimitri Emswiler, Washington, 2:22; Elijah Bailey, St Albans, fall Andres Ramos, Brooke, 0:34; Evan King, Ripley, fall Noah Whaler, Hedgesville, 5:36; Bo Moler, Parkersburg, fall Boco Kubovcik, Musselman, 1:19; Austin Love, John Marshall, dec. Zach Mullins, Greenbrier East, 6-2; AJ Dempsey, Huntington, dec. Marco Tapia, Jefferson, 5-2; Brise Bennett, University, dec. Izak Petry, Riverside, 3-1.
138
Tyler Cutright, Buckhannon-Upshur, fall Bryan Armstrong, Hampshire, 0:35; Isaiah Vaughn, Cabell Midland, maj. dec. Austin Dilly, Greenbrier East, 12-4; Gabe Leopardi, Musselman, maj. dec. Dakoyta Thatcher, John Marshall, 13-4; John Martin-Best, Parkersburg, fall Elijah Kinder, Riverside, 1:32; Noah Bailey, St Albans, dec. Gabe Dempsey, Huntington, 10-3; Stevie Mitchell, Wheeling Park, fall Timmy Shriver, Hedgesville, 3:10; Luke Martin, Parkersburg South won by forfeit over won by forfeit overeit, No School; Braeden Pauley, University, fall Buddy Stine, Washington, :45.
145
Hunter Burdette, Ripley, dec. Jacob Smithson, Buckhannon-Upshur, 4-3; Austin Atkins, St Albans, fall Devin Heath, Hedgesville, 1:30; Stephen Carder, Parkersburg, fall Max Camilletti, Brooke, 1:12; Isaac Isabell, George Washington, fall Raymond Buckler, Hampshire, :52; Taven Swick, Musselman, maj. dec. Antwon Burnett, Woodrow Wilson, 16-6; Andrew Shelek, Wheeling Park, dec. Zach Collins, Parkersburg South, 5-3; Colby Piner, Greenbrier East, fall Jose Gonzalez, Washington, 1:20; Daniel Long, Hurricane, dec. Jordan Wood, John Marshall, 1-0.
152
Owen Zeiders, University tech. fall Christian Beard, Riverside, 16-0; Richard Edwards, Huntington, fall Andy Felegie, Washington, 2:26; Levi Hobbs, Greenbrier East, fall Tucker Knisell, Preston, 1:04; Jalen King, Parkersburg, fall Hunter Staub, Hampshire, 3:40; Josh Humphreys (Se), Parkersburg South won by forfeit over won by forfeit overeit, No School; CJ Helms, Brooke, fall Caden Doub, St Albans, 5:33; Tyson Hall, Spring Valley, fall Jermaine Hardy, Martinsburg, 1:42; Brayden Elswick, George Washington, dec. Jeremiah Timberlake, Wheeling Park, 11-4.
160
Anthony Carman, John Marshall, fall Aiden Smoot, Hedgesville, 1:25; Robbie Holley, Cabell Midland, dec. Jayvon Hall, George Washington, 4-7; Roy Brannon, Spring Mills, fall Michael White, Morgantown, 0:33; Clayton Stewart, Huntington, fall Dustin Neal, Woodrow Wilson, 0:58; Jacob Hefner, Greenbrier East, dec. Chace Mathess, Parkersburg, 6-0; Gage Barnhart, Brooke, dec. Chris Fagga, Hampshire, 7-3; Zane Hinzman, Parkersburg South, fall Zach Stewart, St Albans, 1:42; Owen Keel, Jefferson, fall Josh Rudmann, University, 1:55.
170
Jacob Berisford, John Marshall tech. fall Ty Boyd, Parkersburg, 16-1; Czar Parrish, Hedgesville, fall Avante Burnette, Woodrow Wilson, 2:33; Drew Parde, Huntington, fall Dalton Yost, Brooke, 1:53; Leslie Campbell, Greenbrier East, fall Desmond Tucker, Spring Mills, 3:12; Cameron Pine, Washington won by forfeit over won by forfeit overeit, No School; Kaleb Simpkins, Wheeling Park, dec. Wyatt Linder, Parkersburg South, 10-3; Isaiha Casto, St Albans won by forfeit over won by forfeit overeit, No School; Jacob Starkey, Ripley, fall Alex Shriver, Morgantown, 2:47.
182
Blake Miller, Preston, fall Devan Gauldin, Woodrow Wilson, :48; Drew Dunbar, Parkersburg South, fall Austin Wright, Hampshire, 5:23; Jacob Thomas, Greenbrier East tech. fall Billy Dewall, Brooke, 17-0; Chase Stevens, Spring Valley, fall Jacob Biller, Jefferson, 4:17; Zane Lanham, Huntington, fall Gage Abrecht, Hedgesville, 1:18; Chris McCool, John Marshall, dec. Michael Armes, Riverside, 8-4; Jace Bradbury, Washington, dec. Logan Nay, Parkersburg, 9-2; Isaish Estabrook, George Washington, dec. Axel Gebhardt, Wheeling Park, 7-5.
195
Austin Loew, Wheeling Park, fall Spencer Sanon, Hedgesville, 3:34; Gerard Hall, George Washington, dec. Daulton Fullerton, Parkersburg South, 4-5; Dustin Swisher, Hampshire, fall Gabe McCardle, John Marshall, 2:16; Mason Fischer, Cabell Midland, fall Vaughn Thacker, Princeton, 2:52; CJ Wade, Parkersburg, maj. dec. Aaron Davis, Greenbrier East, 1-13; Josh Sanders, University, dec. Conor Wogan, Washington, 14-8; Cody Stanley, Spring Valley tech. fall Ian Sears, Capital, 4:57; Jacob Northcraft, Musselman, fall Jat Maria, Preston, 4:45.
220
Dylan Wood, Wheeling Park, maj. dec. Caleb Holbrook, Cabell Midland, 12-3; Zane Milburn, Musselman won by forfeit over won by forfeit overeit, No School; Braxton Amos, Parkersburg South, fall Ayden Bishoff, Preston, 0:27; Nathan Stiffler, Spring Mills, dec. Bruce Humphreys, Greenbrier East, 2-3; Ben Lambert, Hampshire won by forfeit over won by forfeit overeit, No School; Ben Gribble, University, dec. Payton Lunsford, Hurricane, 11-7; Colton Wright, Woodrow Wilson, fall John Stambough, Hedgesville, 1:05; BJ Haynes, Ripley, fall Kenton Conley, Brooke, 2:45.
285
Matthew Kincaid, George Washington, fall Neil McKnight, Morgantown, 3:18; Louden Haga, Parkersburg South tech. fall Mike Culinane, Martinsburg, 15-0; Austin Chapman, Riverside, fall Brysen Valentine, Preston, 1:54; Logan Osburn, Cabell Midland, fall Jamie Kilmer, Musselman, 4:22; Owen Porter, Spring Valley, fall Jonathen Lavely, Washington, 1:02; Brady Williams, John Marshall, dec. Drew Bashlor, Greenbrier East, 3-2; Justin Kelley, Huntington, fall Daniel Sherrard, Hedgesville, 1:38; Tyree Swafford, Woodrow Wilson, fall Calvin McGaha, Wheeling Park, 4:56.
Quarterfinal106
Jace Stockett, University, maj. dec. Landen Hoover, Greenbrier East, 8-0; Garret Donahue, Parkersburg, fall London Dotson, Riverside, 1:49; Michael Dolan, Spring Mills tech. fall Devin Easton, Parkersburg South, 15-0; Thomas Hartley, St Albans, fall Devin Doss, Huntington, 0:53.
113
Julius Hobbs, Buckhannon-Upshur, dec. Brayden Johnson, Parkersburg South, 9-7 OT; Brandon Holt, St Albans, fall Luke Miller, Ripley, 0:48; London Bowen, Huntington, fall Bryson Wilkins, Preston, 5:55; Jacob McCarren, Spring Mills, dec. Ethan Gray, John Marshall, 7-6.
120
Jake Staud, University, dec. Alex Viars, Huntington, 5-0; Ethan Hardy, Washington, dec. Eian Harper, Spring Valley, 5-2; Maison Richardson, Wheeling Park, dec. Isaiah Isabell, George Washington, 3-2; Tucker Windland, Parkersburg South, fall Hunter Dodson, Spring Mills, 3.46.
126
Jacob Simpson, University, dec. Eric Dreyer, Washington, 7-3; Brayden Roberts, Parkersburg South, fall Cayden Hoover, Greenbrier East, 3.54; Joey Miller, Musselman tech. fall Austin McCartney, Buckhannon-Upshur, 15-0; Jack Lorea, George Washington, dec. Tristan Adkins, Huntington, 5-3 OT.
132
Gavin Quiocho, Parkersburg South, maj. dec. Billy Gooch, Wheeling Park, 11-0; Elijah Bailey, St Albans, dec. Evan King, Ripley, 17-10; Bo Moler, Parkersburg, maj. dec. Austin Love, John Marshall, 11-2; Brise Bennett, University, dec. AJ Dempsey, Huntington, 9-5.
138
Tyler Cutright, Buckhannon-Upshur, dec. Isaiah Vaughn, Cabell Midland, 16-10; John Martin-Best, Parkersburg, fall Gabe Leopardi, Musselman, 3.48; Noah Bailey, St Albans, dec. Stevie Mitchell, Wheeling Park, 6-2; Luke Martin, Parkersburg South tech. fall Braeden Pauley, University, 22-7.
145
Hunter Burdette, Ripley, dec. Austin Atkins, St Albans, 6-0; Stephen Carder, Parkersburg tech. fall Isaac Isabell, George Washington, 22-5; Andrew Shelek, Wheeling Park, fall Taven Swick, Musselman, 0.44; Daniel Long, Hurricane, fall Colby Piner, Greenbrier East, 7.24 OT.
152
Owen Zeiders, University, fall Richard Edwards, Huntington, 5.03; Levi Hobbs, Greenbrier East, dec. Jalen King, Parkersburg, 10-3; Josh Humphreys (Se), Parkersburg South, fall CJ Helms, Brooke, 1.58; Tyson Hall, Spring Valley, dec. Brayden Elswick, George Washington, 11-9.
160
Anthony Carman, John Marshall, dec. Robbie Holley, Cabell Midland, 4-0; Clayton Stewart, Huntington, fall Roy Brannon, Spring Mills, 1.08; Jacob Hefner, Greenbrier East, maj. dec. Gage Barnhart, Brooke, 11-3; Zane Hinzman, Parkersburg South, fall Owen Keel, Jefferson, 1.33.
170
Jacob Berisford, John Marshall, maj. dec. Czar Parrish, Hedgesville, 12-1; Drew Parde, Huntington, fall Leslie Campbell, Greenbrier East, 3.45; Cameron Pine, Washington, maj. dec. Kaleb Simpkins, Wheeling Park, 15-2; Jacob Starkey, Ripley, dec. Isaiha Casto, St Albans, 7-0.
182
Drew Dunbar, Parkersburg South, dec. Blake Miller, Preston, 3-1 OT; Chase Stevens, Spring Valley, dec. Jacob Thomas, Greenbrier East, 7-2; Zane Lanham, Huntington, fall Chris McCool, John Marshall, 0.35; Jace Bradbury, Washington, dec. Isaish Estabrook, George Washington, 5-1.
195
Austin Loew, Wheeling Park, fall Gerard Hall, George Washington, 0.51; Dustin Swisher, Hampshire, maj. dec. Mason Fischer, Cabell Midland, 8-0; CJ Wade, Parkersburg, dec. Josh Sanders, University, 6-2; Cody Stanley, Spring Valley, dec. Jacob Northcraft, Musselman, 7-2.
220
Dylan Wood, Wheeling Park, fall Zane Milburn, Musselman, 0.24; Braxton Amos, Parkersburg South, fall Nathan Stiffler, Spring Mills, 0.27; Ben Gribble, University, dec. Ben Lambert, Hampshire, 10-7; BJ Haynes, Ripley, maj. dec. Colton Wright, Woodrow Wilson, 10-1.
285
Louden Haga, Parkersburg South, fall Matthew Kincaid, George Washington, 0.57; Austin Chapman, Riverside, fall Logan Osburn, Cabell Midland, 3.10; Owen Porter, Spring Valley, maj. dec. Brady Williams, John Marshall, 15-2; Tyree Swafford, Woodrow Wilson, fall Justin Kelley, Huntington, 2.24.
Consolation First Round106
Dominic Emswiler, Washington, dec. Nate Cox, Ripley, 6-2; Zach Riffle, Preston, dec. Nathan Bowers, Musselman, 8-6; Isaak Murphy, Wheeling Park won by forfeit over won by forfeit overeit, No School; Danny Yost, Brooke, dec. Colton White, Jefferson, 6-2.
113
Jacob Curry, Hampshire, fall Trent England, Princeton, 0.53; Calvin Matthews, University, fall Joey Wilson, Hedgesville, 0.29; Cobie Johnson, Riverside won by forfeit over Foreit, No School; Levi Westfall, Parkersburg, dec. Tyler Huffman, George Washington, 7-4.
120
Sierra Powell, Hampshire won by forfeit over Tyler Powell, St Albans; Bailey Radabaugh, Preston, fall Cameron Stover, Woodrow Wilson, 0.59; Cody Shindledecker, Musselman, fall Ethan Koontz, Ripley, 1:44; Breagan Pearson, Buckhannon-Upshur, fall Waymon Browning, Greenbrier East, 4.26.
126
Treven Taylor, Hurricane tech. fall Zach Messer, St Albans, 16-0; Canon Welker, Wheeling Park tech. fall Theo Yates, Jefferson, 19-4; Payne Salmons, Cabell Midland, fall Jordan Meadows, Capital, 1.35; Peyton Shook, John Marshall, fall Kyle Price, Hedgesville, 1.40.
132
Brock Perry, Capital, fall Dimitri Emswiler, Washington, 2.07; Noah Whaler, Hedgesville, fall Andres Ramos, Brooke, 3.35; Zach Mullins, Greenbrier East, fall Boco Kubovcik, Musselman, 2.10; Marco Tapia, Jefferson, dec. Izak Petry, Riverside, 8-7.
138
Austin Dilly, Greenbrier East, fall Bryan Armstrong, Hampshire, 0.11; Dakoyta Thatcher, John Marshall, fall Elijah Kinder, Riverside, 4.29; Gabe Dempsey, Huntington, fall Timmy Shriver, Hedgesville, 2.46; Buddy Stine, Washington won by forfeit.
145
Jacob Smithson, Buckhannon-Upshur, fall Devin Heath, Hedgesville, 1.29; Max Camilletti, Brooke, fall Raymond Buckler, Hampshire, 1.48; Zach Collins, Parkersburg South, fall Antwon Burnett, Woodrow Wilson, 1.43; Jordan Wood, John Marshall, fall Jose Gonzalez, Washington, 0.56.
152
Andy Felegie, Washington, fall Christian Beard, Riverside, 2.44; Hunter Staub, Hampshire, fall Tucker Knisell, Preston, 0.51; Caden Doub, St Albans won by forfeit over won by forfeit overeit, No School; Jeremiah Timberlake, Wheeling Park, maj. dec. Jermaine Hardy, Martinsburg, 10-0.
160
Jayvon Hall, George Washington, fall Aiden Smoot, Hedgesville, 0.54; Dustin Neal, Woodrow Wilson tech. fall Michael White, Morgantown, 17-0; Chace Mathess, Parkersburg, dec. Chris Fagga, Hampshire, 5-4; Josh Rudmann, University, dec. Zach Stewart, St Albans, 10-3.
170
Avante Burnette, Woodrow Wilson, dec. Ty Boyd, Parkersburg, 5-0; Dalton Yost, Brooke, fall Desmond Tucker, Spring Mills, 4.10; Wyatt Linder, Parkersburg South won by forfeit over won by forfeit overeit, No School; Alex Shriver, Morgantown won by forfeit.
182
Austin Wright, Hampshire, fall Devan Gauldin, Woodrow Wilson, 2.32; Jacob Biller, Jefferson tech. fall Billy Dewall, Brooke, 17-2; Gage Abrecht, Hedgesville, fall Michael Armes, Riverside, 2.22; Logan Nay, Parkersburg, dec. Axel Gebhardt, Wheeling Park, 6-4.
195
Spencer Sanon, Hedgesville, dec. Daulton Fullerton, Parkersburg South, 5-0; Gabe McCardle, John Marshall, dec. Vaughn Thacker, Princeton, 5-1 OT; Aaron Davis, Greenbrier East, dec. Conor Wogan, Washington, 8-6; Jat Maria, Preston, dec. Ian Sears, Capital, 4-2.
220
Caleb Holbrook, Cabell Midland won by forfeit over won by forfeit overeit, No School; Bruce Humphreys, Greenbrier East, fall Ayden Bishoff, Preston, 3.48; Payton Lunsford, Hurricane won by forfeit over won by forfeit overeit, No School; Kenton Conley, Brooke, fall John Stambough, Hedgesville, 1.39.
285
Neil McKnight, Morgantown, dec. Mike Culinane, Martinsburg, 7-2; Jamie Kilmer, Musselman, fall Brysen Valentine, Preston, 1.50; Drew Bashlor, Greenbrier East, dec. Jonathen Lavely, Washington, 1-0; Daniel Sherrard, Hedgesville, fall Calvin McGaha, Wheeling Park, 0.11.
Consolation Second Round106
Devin Easton, Parkersburg South, fall Dominic Emswiler, Washington, 2.21; Devin Doss, Huntington, fall Zach Riffle, Preston, 1.08; Landen Hoover, Greenbrier East, dec. Isaak Murphy, Wheeling Park, 9-7; London Dotson, Riverside, dec. Danny Yost, Brooke, 6-1.
113
Bryson Wilkins, Preston, dec. Jacob Curry, Hampshire, 7-5; Ethan Gray, John Marshall, dec. Calvin Matthews, University, 5-3; Brayden Johnson, Parkersburg South, dec. Cobie Johnson, Riverside, 8-2; Luke Miller, Ripley, fall Levi Westfall, Parkersburg, 4.08.
120
Isaiah Isabell, George Washington, fall Sierra Powell, Hampshire, 0.50; Bailey Radabaugh, Preston, fall Hunter Dodson, Spring Mills, 1.47; Alex Viars, Huntington tech. fall Cody Shindledecker, Musselman, 16-1; Eian Harper, Spring Valley, fall Breagan Pearson, Buckhannon-Upshur, 1.45.
126
Austin McCartney, Buckhannon-Upshur, dec. Treven Taylor, Hurricane, 4-2; Tristan Adkins, Huntington, dec. Canon Welker, Wheeling Park, 6-2; Eric Dreyer, Washington, dec. Payne Salmons, Cabell Midland, 11-10; Cayden Hoover, Greenbrier East, maj. dec. Peyton Shook, John Marshall, 8-0.
132
Austin Love, John Marshall tech. fall Brock Perry, Capital, 16-1; AJ Dempsey, Huntington, fall Noah Whaler, Hedgesville, 1.54; Billy Gooch, Wheeling Park, dec. Zach Mullins, Greenbrier East, 6-0; Marco Tapia, Jefferson, dec. Evan King, Ripley, 8-5.
138
Stevie Mitchell, Wheeling Park, dec. Austin Dilly, Greenbrier East, 9-2; Braeden Pauley, University, fall Dakoyta Thatcher, John Marshall, 0.39; Isaiah Vaughn, Cabell Midland, dec. Gabe Dempsey, Huntington, 10-5; Gabe Leopardi, Musselman, dec. Buddy Stine, Washington, 9-8.
145
Jacob Smithson, Buckhannon-Upshur, fall Taven Swick, Musselman, 3.37; Colby Piner, Greenbrier East, dec. Max Camilletti, Brooke, 5-2; Zach Collins, Parkersburg South, maj. dec. Austin Atkins, St Albans, 14-2; Jordan Wood, John Marshall, maj. dec. Isaac Isabell, George Washington, 9-1.
152
CJ Helms, Brooke, fall Andy Felegie, Washington, 3.25; Brayden Elswick, George Washington, fall Hunter Staub, Hampshire, 0.42; Richard Edwards, Huntington, fall Caden Doub, St Albans, 3.31; Jalen King, Parkersburg, dec. Jeremiah Timberlake, Wheeling Park, 3-1 OT.
160
Jayvon Hall, George Washington, dec. Gage Barnhart, Brooke, 6-3; Dustin Neal, Woodrow Wilson, fall Owen Keel, Jefferson, 2.50; Robbie Holley, Cabell Midland, fall Chace Mathess, Parkersburg, 2.04; Roy Brannon, Spring Mills, fall Josh Rudmann, University, 2.05.
170
Kaleb Simpkins, Wheeling Park, fall Avante Burnette, Woodrow Wilson, 2.59; Isaiha Casto, St Albans, fall Dalton Yost, Brooke, 0.26; Czar Parrish, Hedgesville, dec. Wyatt Linder, Parkersburg South, 4-3; Leslie Campbell, Greenbrier East, dec. Alex Shriver, Morgantown, 8-4.
182
Austin Wright, Hampshire Def Chris McCool, John Marshall; Jacob Biller, Jefferson, maj. dec. Isaish Estabrook, George Washington, 10-2; Blake Miller, Preston, fall Gage Abrecht, Hedgesville, 4.51; Logan Nay, Parkersburg, dec. Jacob Thomas, Greenbrier East, 4-2.
195
Josh Sanders, University, dec. Spencer Sanon, Hedgesville, 6-4; Jacob Northcraft, Musselman, fall Gabe McCardle, John Marshall, 2.20; Aaron Davis, Greenbrier East, fall Gerard Hall, George Washington, 1.59; Mason Fischer, Cabell Midland, fall Jat Maria, Preston, 1.10.
220
Caleb Holbrook, Cabell Midland, fall Ben Lambert, Hampshire, 4.45; Colton Wright, Woodrow Wilson, fall Bruce Humphreys, Greenbrier East, 0.36; Payton Lunsford, Hurricane, fall Zane Milburn, Musselman, 0.38; Kenton Conley, Brooke, maj. dec. Nathan Stiffler, Spring Mills, 14-2.
285
Brady Williams, John Marshall, dec. Neil McKnight, Morgantown, 3-2 OT; Justin Kelley, Huntington, fall Jamie Kilmer, Musselman, 1.53; Matthew Kincaid, George Washington, fall Drew Bashlor, Greenbrier East, 1.23; Daniel Sherrard, Hedgesville, fall Logan Osburn, Cabell Midland, 0.32.
Championship Semifinal106
Garret Donahue, Parkersburg, dec. Jace Stockett, University, 6-1; Thomas Hartley, St Albans, fall Michael Dolan, Spring Mills, 1.55.
113
Brandon Holt, St Albans, dec. Julius Hobbs, Buckhannon-Upshur, 5-1; London Bowen, Huntington, fall Jacob McCarren, Spring Mills, 1.22.
120
Jake Staud, University, dec. Ethan Hardy, Washington, 8-1; Tucker Windland, Parkersburg South, fall Maison Richardson, Wheeling Park, 3.36.
126
Brayden Roberts, Parkersburg South, maj. dec. Jacob Simpson, University, 8-0; Joey Miller, Musselman, dec. Jack Lorea, George Washington, 7-0.
132
Gavin Quiocho, Parkersburg South, fall Elijah Bailey, St Albans, 0.29; Bo Moler, Parkersburg, dec. Brise Bennett, University, 7-3.
138
John Martin-Best, Parkersburg, dec. Tyler Cutright, Buckhannon-Upshur, 7-2; Luke Martin, Parkersburg South, maj. dec. Noah Bailey, St Albans, 11-3.
145
Stephen Carder, Parkersburg, fall Hunter Burdette, Ripley, 2.12; Andrew Shelek, Wheeling Park, dec. Daniel Long, Hurricane, 10-5.
152
Owen Zeiders, University, dec. Levi Hobbs, Greenbrier East, 10-4; Josh Humphreys (Se), Parkersburg South, fall Tyson Hall, Spring Valley, 1.21.
160
Anthony Carman, John Marshall, dec. Clayton Stewart, Huntington, 9-3; Zane Hinzman, Parkersburg South, dec. Jacob Hefner, Greenbrier East, 4-3.
170
Jacob Berisford, John Marshall, dec. Drew Parde, Huntington, 4-2; Cameron Pine, Washington, dec. Jacob Starkey, Ripley, 9-3.
182
Drew Dunbar, Parkersburg South, fall Chase Stevens, Spring Valley, 1.42; Zane Lanham, Huntington, fall Jace Bradbury, Washington, 0.57.
195
Dustin Swisher, Hampshire, dec. Austin Loew, Wheeling Park, 3-2; Cody Stanley, Spring Valley, dec. CJ Wade, Parkersburg, 5-0.
220
Braxton Amos, Parkersburg South, fall Dylan Wood, Wheeling Park, 3.56; BJ Haynes, Ripley, dec. Ben Gribble, University, 3-1.
285
Louden Haga, Parkersburg South, dec. Austin Chapman, Riverside, 4-1; Owen Porter, Spring Valley, fall Tyree Swafford, Woodrow Wilson, 3.42.
Consolation Third Round106
Devin Easton, Parkersburg South, dec. Devin Doss, Huntington, 6-2; Landen Hoover, Greenbrier East, fall London Dotson, Riverside, 1.31.
113
Ethan Gray, John Marshall, dec. Bryson Wilkins, Preston, 6-5; Brayden Johnson, Parkersburg South, fall Luke Miller, Ripley, 5.59 OT.
120
Isaiah Isabell, George Washington, dec. Bailey Radabaugh, Preston, 8-4; Eian Harper, Spring Valley, dec. Alex Viars, Huntington, 8-5.
126
Tristan Adkins, Huntington, dec. Austin McCartney, Buckhannon-Upshur, 3-0; Eric Dreyer, Washington, dec. Cayden Hoover, Greenbrier East, 12-5.
32
Austin Love, John Marshall, dec. AJ Dempsey, Huntington, 4-1; Billy Gooch, Wheeling Park, dec. Marco Tapia, Jefferson, 3-2.
138
Stevie Mitchell, Wheeling Park, dec. Braeden Pauley, University, 2-1; Isaiah Vaughn, Cabell Midland, maj. dec. Gabe Leopardi, Musselman, 11-2.
145
Jacob Smithson, Buckhannon-Upshur, dec. Colby Piner, Greenbrier East, 7-1; Zach Collins, Parkersburg South, dec. Jordan Wood, John Marshall, 6-0.
152
Brayden Elswick, George Washington, dec. CJ Helms, Brooke, 16-9; Richard Edwards, Huntington, maj. dec. Jalen King, Parkersburg, 12-2.
160
Jayvon Hall, George Washington, dec. Dustin Neal, Woodrow Wilson, 8-5; Robbie Holley, Cabell Midland, dec. Roy Brannon, Spring Mills, 5-0.
170
Isaiha Casto, St Albans, dec. Kaleb Simpkins, Wheeling Park, 4-1; Leslie Campbell, Greenbrier East, dec. Czar Parrish, Hedgesville, 7-3.
182
Jacob Biller, Jefferson, dec. Austin Wright, Hampshire, 7-6; Blake Miller, Preston, fall Logan Nay, Parkersburg, 3.18.
195
Jacob Northcraft, Musselman, dec. Josh Sanders, University, 10-5; Mason Fischer, Cabell Midland, fall Aaron Davis, Greenbrier East, 2.45.
220
Caleb Holbrook, Cabell Midland, fall Colton Wright, Woodrow Wilson, 1.56; Payton Lunsford, Hurricane, fall Kenton Conley, Brooke, 0.33.
285
Brady Williams, John Marshall, dec. Justin Kelley, Huntington, 6-4 OT; Daniel Sherrard, Hedgesville, fall Matthew Kincaid, George Washington, .15.