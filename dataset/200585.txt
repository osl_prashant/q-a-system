Producers hoping to mitigate annual ryegrass growth for warm-season hay production have options and should start sooner than later, said a Texas A&M AgriLife Extension Service expert.Annual ryegrass, a cool-season forage, is often utilized by livestock producers for winter grazing, said Vanessa Corriher-Olson, AgriLife Extension forage specialist, Overton. However, East Texas hay producers often view it as an unwanted species that competes with Bermuda and Bahia grasses, she said.
Volunteer annual ryegrass can be common in East Texas hay meadows. Winter rainfalls can promote seed germination, and seeds can survive multiple years in regional soils.
Late season annual ryegrass can delay or prevent warm-season perennial forages, such as Bermuda or Bahia grasses, from breaking dormancy in April or May.
"This can delay our initial hay cutting," she said.
Corriher-Olson said ryegrass can be controlled with herbicides.
"Spraying is probably the most common control method," she said. "As with any herbicide application, timing is critical along with following label directions."
Prowl H2O, or pendimethalin, is a good pre-emergent herbicide labeled for dormant Bermuda grass and Bahia grass pastures and hay meadows. Treatments should be applied prior to rainfall, she said, to enhance soil incorporation and herbicide activation.
Glyphosate, the active ingredient in Roundup and other herbicides, and Pastora, which includes nicosulfuron and metsulfuron, are two postemergent herbicide options.
Ideally, ryegrass needs to be sprayed when plants are less than 6 inches in height in the fall, but second applications are recommended in February or March, Corriher-Olson said.
"Annual ryegrass is generally susceptible to postemergence herbicides in early winter prior to freezing temperatures and before seed-head emergence,"she said. "Unfortunately for Bahia grass growers, there are no selective herbicides available for postemergence control of annual ryegrass."
Spot treatments of glyphosate are recommended in Bahia grass for control, she said. For rates and any restrictions refer to product labels.
Producers can control ryegrass by grazing it, she said.
"If your hay meadow happens to be fenced and has a good source of water, grazing can be an excellent way to utilize a high-quality forage and remove it from the meadow," Corriher-Olson said.
Ryegrass also can be baled for hay, she said.
Baleage, or haylage, is forage baled at 50-60 percent moisture. It should then be preserved in an airtight plastic wrap as single bales or one long tube. This requires specialized equipment and diligence in maintaining the integrity of the plastic wrap.
Harvesting for a dry bale product can be tricky during wet springs, she said.
"A lot of people clear hay meadows of volunteer grasses and weeds during their first cutting," she said. "They may be battling ryegrass, and many consider that a 'trash cutting' because they value warm-season grasses like Bermuda grass. But ryegrass is a very good forage under the right conditions."
Another way to fight ryegrass is to promote warm-season grasses, she said. Maintaining some substantial Bermuda or Bahia grass stubble, 4 inches or more, could provide some shade to reduce ryegrass seed germination.
Maintaining a higher stubble height can also be beneficial for warm-season perennial forages' future growing season, Corriher-Olson said. Higher stubble height means more substantial root structure to capture deeper soil moisture and nutrients.
"This may not provide 100 percent control, but competition can help reduce undesired plant growth," she said.