Some farmland in North Texas could face urban expansion
Some farmland in North Texas could face urban expansion

By NANETTE LIGHTThe Dallas Morning News
The Associated Press

MCKINNEY, Texas




MCKINNEY, Texas (AP) — A 1-mile running track through a hay field outlines the Blake family's property.

                The Dallas Morning News reports to outsiders, it's nothing special — just a mowed-down path where Shannon Blake and her kids run.
But it's here — running along this piece of dirt — where Blake finds peace.
Before the Blakes moved to unincorporated Collin County less than two years ago, they lived in a typical two-story, suburban Frisco home. But that fast-growing community a half-hour north of Dallas had dramatically changed from when the Blakes moved there more than a decade earlier.
Cars raced down high-speed freeways. Apartment buildings stood alongside their neighborhood. Toyota transplants were arriving, and The Star in Frisco — home to the Dallas Cowboys —  was coming.
The family traded their suburban life for country living on 33 acres just outside McKinney's city limits.  And it's this same peaceful patch of land — the place with two creeks and a pond — that she, her husband and six kids have fought for the last year to protect from creeping development in booming Collin County.
They're fighting against the very growth that made them escape to the country in the first place.
"It's definitely not what we expected when we were moving out here," Blake said.
And the battle for people like the Blakes to hold onto some of the last bits of unpaved Collin County is far from over.
Collin County's population is expected to double before 2030 and surpass the number of people in Dallas and Tarrant counties with a population of more than 3.5 million by 2050.
More recently, the once tiny cotton farming community of McKinney reported a population of nearly 180,000 residents — an almost 7 percent increase from last year and a whopping 365 percent jump from 20 years ago.
By 2040, McKinney's population is expected to grow by another 100,000 residents to roughly 284,000, according to city estimates.
And that means most of the rural acreage around McKinney's city limits will ultimately be developed, said Michael Quint, executive director of development services for the city of McKinney.
"I actually had one resident come and ask me ... 'is all that farmland going to go away eventually? Is every square inch going to be developed?'" he said. "And the simple answer is yeah, a lot of it is going to be developed. That's just kind of the price we pay to live in such a high-demand region.
"It's impossible to think that this stuff is just going to be staying farmland forever."
A beat-up blacktop road north of the Collin County Courthouse leads to the Blake family's five-bedroom house built on a slope in the early 1960s.
Blake and her husband, Jason, bought the 33-acre parcel just outside McKinney's city limits. The couple had dreamed of a place where their kids could roam and explore.
They thought they found it.
They cleared overgrown bush and dead trees. Last year, a friend wed under a 100-year-old oak tree in their backyard.
Their oldest son built a fort in the nearby woods with his friends — a shack-like structure with a tin roof.  And in the evenings, the family raced each other across the hay bales lined up behind their house.
"I don't care if the city's all around me. We have a treeline and a creek that buffers us," Blake said. "We're in our happy little world, and we'd like it to stay that way."
But it's uncertain if it will.
Markers on their trees show where a developer has tagged them for possible removal for a sewage pipeline.
A year ago, an early study to build a bypass north of U.S. 380 showed a freeway possibly cutting through their property as the Texas Department of Transportation works to tackle the traffic gridlock that comes with explosive growth in the county.
But where exactly that freeway will be and when it will rise up isn't known yet, said TxDOT spokeswoman Michelle Raglon.
Last year, the Blakes also received word of another possible road running through where the family's kitchen table sits today.
And in the fall, the family and other county residents fought a McKinney City Council plan to forcibly add thousands of acres outside the city limits before a new state law dealing with annexation went into effect. Blake, her husband, Jason, and others packed meeting after meeting in the weekslong battle, calling the forced annexation a "land grab."
In November, McKinney City Council members unanimously voted to drop its annexation push. For now.
During that meeting, Mayor Pro Tem Rainey Rogers warned that probably wouldn't be the last time rural Collin County residents would have to fight to protect their property.
"One of these days, TxDOT is going to come knocking on people's doors ready to take your land because of some bypass," he said. "Ultimately, it's going to come. The state of Texas didn't give up the right to eminent domain on a property. Just kind of be aware of that."
This growth isn't anything new for Collin County, which has been steadily growing at a phenomenal clip for decades, said Clarence Daugherty, director of engineering for Collin County.
Tax incentives, good schools, public safety and the relocation of large corporations — notably Toyota North America's move to Plano — continue to lure families and businesses to the area. And as more people have come, development has inched farther north from Plano to McKinney.
Beyond McKinney, past where the Dallas North Tollway ends, is Celina. Though its population totals only about 11,000 residents now, that's almost double what it was in 2010, according to the city. And it's estimated to eventually reach about 350,000, rivaling Collin County siblings Frisco and McKinney.
Already, Celina is expanding so quickly that city staff can't keep updated maps on hand.
"Celina has such a large land mass. It's like playing monopoly, and we don't want to make a mistake on any step," Mayor Sean Terry said.
The ONE McKinney 2040 Comprehensive Plan, which is still in the draft phase, plots in its preferred scenario the Blakes' property and other rural areas in the Honey Creek Entertainment District — a mixed-use development of retail stores, restaurants, office spaces and residences to attract young and retiring professionals.
"These are farms, and none of these people want to move," Blake said of the plan. "So none of that fits unless we're all gone."
Daugherty predicts pockets of acreage may remain in the county. He pointed to a long holdout of suburbia's reach in Plano — the Haggard family, one of the city's founding families who settled in the area in the 1800s and has farmed the land for generations.
The Haggards have parceled off their land slowly but continue to farm on some vacant tracts such as one at the Dallas North Tollway and Spring Creek Parkway.
About 8 miles away in the heart of Plano, you can still spot llamas, cows and donkeys grazing on a roughly 60-acre tract of farmland owned by Rodney Haggard. Several years earlier, he sold about half of the family's historic homestead to be developed into a housing subdivision.
"We still primarily want to keep as much land as we can," said Haggard, managing partner at the real estate firm Fairview Farm Land Co., who still owns about 150 acres scattered throughout the area.
"But when growth comes to our part," he said, "we try to take advantage of that some, too."
But Daugherty, the county engineer, said large expanses of acreage like the ones the Haggards have held on to will be unlikely.
"I guess it's always possible that something will happen to make development stop before it engulfs all of the land," he said.
McKinney Mayor George Fuller thinks the state's new annexation law — which went into effect Dec. 1 and requires voters' approval before their land can be annexed — could be that "something."
He said development will happen, but it will be different and with less city oversight since the city no longer has the ability to unilaterally annex property.
The new law limits the ability of cities like McKinney to strategically grow and manage that growth in terms of overseeing that infrastructure is in place and that safety codes and ordinances are adhered to, he said.
"When you halt that, you stop managed growth. I would imagine that again in five and 10 years, we'll be talking, and the Texas miracle will no longer be the Texas miracle. It will be the Texas miracle that we're reading about in history books," he said.
"And we'll be able to identify how the growth and how that growth produced the jobs and the economy it did, and then a law was passed, and that growth slowed and stopped."
Already, the law has changed the way McKinney thinks about its expansion looking forward. Previously, Fuller said the city would have extended roads into the extraterritorial jurisdiction with plans to later annex that property into the city. Now, it won't.
But Quint, McKinney's development director, has said the council's decision to drop its forced annexation push before the new law went into effect isn't expected to hurt the city's expansion plans.
"Even though we're not annexing it today, it's still in our ETJ," he said of the land just outside city limits. "So whether that happens 10 years or 100 years from now, we're still planning for those areas to be in our city limits."
Shannon Blake doesn't understand why cities can't develop, while also allowing country farms to remain.
"We knew that the city would grow around us. We just didn't want them to grow through us," she said.
___
Information from: The Dallas Morning News, http://www.dallasnews.com


This is an AP Member Exchange shared by The Dallas Morning News