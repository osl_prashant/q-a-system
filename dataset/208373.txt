Chicago Mercantile Exchange live cattle posted modest gains on Friday after investors tweaked positions before the U.S. Department of Agriculture's monthly Cattle-On-Feed report at 2 p.m. CDT (1900 GMT), traders said.Analysts polled by Reuters expect the report to show 6.2 percent more cattle entered feedlots in July than a year ago.
Packers paid less for slaughter-ready, or cash, cattle this week, which pulled futures from session tops.
August, which will expire on Aug. 31, ended unchanged at 105.950 cents. Most actively traded October finished up 0.100 cent at 106.925 cents, and December ended 0.125 cent higher at 109.925 cents.
This week cash cattle in the U.S. Plains moved at $106 to $107 per cwt, down from $109 to $110 last week.
Ample seasonal supplies of cattle at heavier weights discouraged packers from paying more for animals but allowed grocers to shop for beef at lower cost.
"The decline in wholesale beef prices says clearly that we produced more beef. It's more of a supply-side than a demand-side issue," said Livestock Marketing Information Center Director Jim Robb.
Beef demand typically encounters headwinds in September, following the end of the summer grilling season and as consumers focus more on back-to-school items than expensive cuts of meat.
Traders will gauge Hurricane Harvey's impact on meat sales and cattle production in parts of Texas - the country's top cattle producing state.
Many of the feedlots in Texas are located well north of the storm, but a significant number of cows and calves are on grazing land in southwestern parts of the state.
Firmer back-month live cattle contracts lifted CME feeder cattle for a second day in a row.
August feeders, which will expire on Aug. 31, closed down 0.225 cent per pound at 141.375 cents. Most actively traded September closed 0.750 cent higher at 142.925 cents, and October finished up 0.550 cent at 142.950 cents.
Hogs Close Weaker
CME lean hogs ended moderately lower, pressured by downward-spiraling cash prices and the morning's softer wholesale pork values as supplies continued to build seasonally, said traders.
October led declines after investors sold that contract and simultaneously bought deferred months in a trading tactic known as bear spreading.
October ended 0.700 cent per pound lower at 63.075 cent, and December finished down 0.300 cent at 58.950 cents.