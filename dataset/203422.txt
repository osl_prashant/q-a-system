Allen Lund and his wife Kathie were the first two employees of The Allen Lund Co., established in 1976.
It made sense for Lund to enter the transportation brokerage business. He's the son of a trucker, running heavy equipment by himself across the country at 14 years old. Lund first worked for C.H. Robinson in his hometown of Salt Lake City, After two years, he was transferred to Los Angeles to open and manage an office, which he did for seven years before opening his own business in La Canada, Calif.
In 1980, when deregulated transportation led many companies to begin hauling produce, Lund lost the niche he'd specifically gone into business for. Lund began trucking other commodities to ensure the survival of his company, though he still considers himself a produce man at heart.
When asked why he thinks The Allen Lund Co. has succeeded in business, Lund immediately responded, "The employees." When pressed, he added, "I had a plan, I followed it, and it worked."
The order in which he gave credit - first to his employees, then to himself - exemplifies the virtues by which he's run his company.
"If your word isn't good in produce, you're not going anywhere," Lund says. He learned this early in his career, when the time-sensitive nature of produce meant a heavy reliance on others to follow through with their promises. Lund believes that a focus on integrity is a major reason why The Allen Lund Co. has thrived throughout the years.
Lund also sees charity as essential to uniting his company.
Instead of having a 40th anniversary celebration, the company has been celebrating the milestone all of 2016 through its 40 Acts of Kindness program. Offices across the country are all participating by giving back to their communities. albeit in different ways.
One of the 40 Acts, the Fit-Bit Challenge, set a company goal to walk 40,000 miles before memorial day. All donations raised for this act go toward purchasing a track chair for the Wounded Warrior Foundation.
Another effort has sponsored transportation costs for a shipment of school supplies through Mission Haiti, a Christian nonprofit organization.
But Lund's finds the most value in the industry he works in and people he works with.
"I love that it's based in agriculture and that we've hired wonderful employees," says Lund.
Several of those employees are his children.
David, Kenny, and Eddie Lund are all vice presidents with the company, son-in-law Steve Doerfler is chief financial officer, and son-in-law Kirk Peterson is a Delta pilot who flies the ALC plane. His third son-in-law, Mike Clapp, is a high school counselor at Saint Francis High School, where Lund serves on the board of directors.
(From left) David Lund, Kenny Lund, Allen Lund, Eddie Lund and Steve Doerfler in 2015.
"I've been able to live the American Dream," said Lund. "It's been a wonderful ride."
Given the reputation that ALC has earned in the transportation brokerage industry, Lund and his family have certainly built something worth celebrating.