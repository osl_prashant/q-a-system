Crop calls 
Corn: Fractionally to 1 cent higher
Soybeans: 1 to 2 cents higher
Wheat: Fractionally to 1 cent lower
It's a new month, but the story for the crop markets remains the same. Back-and-forth action is limiting trade in the corn market, although futures are closer to important support levels at the bottom of the trading range as the calendar flips to November. Soybean futures were firmer overnight, but remain in the downtrend established from last month's high. And then there's wheat, which is plagued with a bearish technical and fundamental situation. The U.S. dollar index is firmer this morning as investors wait on the Federal Open Market Committee's statement this afternoon.
 
 
Livestock calls
Cattle: Higher
Hogs: Mixed
Cattle futures are called higher amid ongoing technical buying, as well as improvement in the beef market that's strengthening the argument for higher cash trade. But significant cash volume will likely be delayed until Friday as packers try to avoid paying sharply higher prices again this week. Meanwhile, hog futures are expected to see a mixed tone on a combination of followthrough buying versus profit-taking given the overbought situation of the market, as well as ideas the cash market is working on a top. Cash hogs softened a bit yesterday and more of the same is expected today.