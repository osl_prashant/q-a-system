Iowa agriculture secretary Northey approved for USDA post
Iowa agriculture secretary Northey approved for USDA post

The Associated Press

DES MOINES, Iowa




DES MOINES, Iowa (AP) — Iowa Secretary of Agriculture Bill Northey has been confirmed by the U.S. Senate to become an undersecretary in the U.S. Department of Agriculture.
Northey, a Spirit Lake farmer, has been Iowa's agriculture secretary for 11 years.
He becomes the undersecretary of farm production and conservation, a position created in May during reorganization of the USDA.  Northey will oversee the Farm Service Agency, Natural Resources Conservation Service and the Risk Management Agency.
The timing for his resignation and swearing-in hasn't been finalized.
Gov. Kim Reynolds will name his replacement.
The nomination has been lingering since President Donald Trump named him in September primarily because Sen. Ted Cruz of Texas kept it on hold as a protest over ethanol policy.
The nomination was approved Tuesday by Senate voice vote.