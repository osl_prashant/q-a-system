Policemen, protesters hurt in Cambodian land dispute
Policemen, protesters hurt in Cambodian land dispute

The Associated Press

PHNOM PENH, Cambodia




PHNOM PENH, Cambodia (AP) — At least seven policemen and two protesters were hurt Thursday in a clash after villagers in northeastern Cambodia blocked a national highway to protest being forced off land they have occupied for at least two years.
Officials and NGO workers said about 200 villagers in Kratie province who have been living on land that was given to a concessionaire to develop into a rubber plantation blocked the road for two hours.
Land disputes became a critical issue in Cambodia in the early part of last decade, as great blocs of land were granted as concessions for logging, rubber and other economic development projects. Violent and sometimes fatal conflicts between villagers, who rarely held formal land titles, and the authorities, acting on behalf of the concession holders, became common to the point that they were considered to be a threat to political stability.
In 2012, Prime Minister Hun Sen issued a directive suspending new land concessions to private companies and ordering a review of existing ones, though it is not clear the order was effectively implemented.
He also spearheaded a massive land redistribution program which critics characterized as unfair, open to corruption and politically motivated. The program crested ahead of a general election.
In recent years, large violent confrontations over land have become less frequent.
The chief of Snuol district, where Thursday's confrontation occurred, Kong Kimny, said the protesters were wounded by police bullets, and the police officers were hurt by villagers using slingshots and rocks. He said police opened fire with warning shots when they were attacked, accidently wounding the villagers. Seven villagers considered the protest's leaders were arrested, he added.
He and a staff member of the human rights group ADHOC, Bei Vanny, both denied reports of any deaths.
Bei Vanny said the protesters were demanding that authorities provide them land for housing and farming before kicking them off of the land on which they had been squatting.
District chief Kong Kimny said the authorities had held talks with the villagers six times since 2015, but they continued to refuse to get off the privately held land. He said villagers often attacked the concession company's tractor when it tried to clear forest land near their homes.
A 53-year-old villager contacted by phone said the protest occurred because the company holding the concession kept clearing land where the villagers had planted bananas, rice and mangos while refusing to talk to the villagers. He spoke on condition of anonymity because he feared for his safety.
The identity of the company holding the concession could not immediately be determined.
___
This story has been corrected to fix spelling of district's name to Snuol instead of Chhnoul.