One of Secretary Sonny Perdue’s top priorities for 2018 is getting a farm bill through Congress.

That work has to start in the House of Representatives and the Senate.

Sen. Pat Roberts (R-Kan.) sits down with AgDay host Clinton Griffiths to discuss the work on the bill on AgDay above.