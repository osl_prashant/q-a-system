Before Michigan produce makes it to retail, some Michigan retailers first visit their suppliers’ growing operations.
“A few retailers come to the farm and take pictures, and they use those pictures to promote the locally grown aspect of Michigan asparagus,” said Aaron Fletcher, sales and logistics for Hart, Mich.-based Todd Greiner Farms.
“I think retailers do a good job in the Midwest. Everybody puts the Michigan name on asparagus when it comes available. There are ads and storefront displays.”
Tim Spiech, operations officer for Lawton, Mich.-based Spiech Farms, said every year there are retailers that send photographers out to its farms and other farms as well and they update their signage annually.
Henry DeBlouw, president of Capac, Mich.-based Mike Pirrone Produce, said every retailer wants to be first to market.
“We work with all retailers, but Kroger is a big company for us that showcases pictures of our farm and local products. They do a very good job in showcasing our product.”
Dave Miedema, president for the Byron Center, Mich.-based E. Miedema & Sons, also said Michigan retailers do a fine job of promoting Michigan-grown items.
Jordan Vande Guchte, salesman for Hudsonville, Mich.-based Superior Sales, said his company saw a lot more draw to bringing the grower to the customer through advertisements, placards and in-store billboards.
“Sometimes, it’s in the form of a short biography that is handed out to customers, and they can get a sense of where their produce is coming from,” he said.
“I think the more you get a customer connected to where their produce is coming from, it gives them more of a value to it.” Spiech added that the Michigan Asparagus Advisory Board will launch an in-store audio campaign that tells shoppers about Michigan asparagus and what they can do to prepare it.
Nick Huizinga, general manager for Byron Center, Mich.-based Hearty Fresh said all of its products are labeled “Michigan grown” and “Pure Michigan.”
“We always like to make it stand out,” he said.
“The retailers really jump on that bandwagon.”