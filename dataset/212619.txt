Retailers across the U.S. have found peppers to be an expanding category.Heinen’s Inc., Warrensville Heights, Ohio, is only a couple of hours away from an Ohio greenhouse facility where Leamington, Ontario-based NatureFresh Farms grows peppers for the chain’s 23 stores, said produce buyer Terry Romp.
“The quality is outstanding,” Romp said. “We get them three times a week.”
When peppers are on ad for $1.99 per pound, they’re merchandised in a display that’s about 8 feet wide and 4 feet deep, he says. The rest of the time, the display is about half that size.
When peppers are on ad, they’re displayed in their usual place in the produce department and in another location up front.
“As soon as you walk into the store, they kind of hit you in the face,” Romp said.
The stores offer red, yellow, orange and green peppers.
The green ones are displayed separately because they’re usually priced less, Romp said. The green display is a couple of feet wide by 2 or 3 feet deep.
During the winter, Heinen’s sources peppers from Mexico.
Throughout January and February, Romp said he featured excellent-quality, field-grown peppers from Mexico on ad for as little as 99 cents.
Typically, peppers are on ad about once a month at Heinen’s.
“Red is our biggest-selling pepper,” Romp said, probably because the red variety tastes sweeter than the others.
“The price goes up when we go into the greenhouse, but quality is so outstanding that people really don’t bat an eye at it,” he said.
The Riverbank, Calif., location of Modesto, Calif.-based O’Brien’s Market, a group of three stores, features loose green, red, orange and yellow bell peppers, said produce manager David Christensen.
The store merchandises loose peppers and 2-pound bags of packaged product in a display about 3.5 feet wide and 4 feet deep.
Most customers choose to select their own peppers from the bulk display, Christensen said.
Red is the most popular color, most likely because of the combination of sweetness, color and flavor the peppers offer, he said.
Prices vary by variety, with orange and yellow the most costly followed by red then green.
The store features one variety or another on ad about once a month.
Felton’s Meat & Produce, Plant City, Fla., merchandises its peppers in three-packs of large and extra-large red, yellow and orange varieties, said produce manager Ron Marshall.
Sometimes he gets them prepackaged from the warehouse, but he usually packs his own in-store and often puts out a variety of color options.
He features three packs on ad about once a month for about $1.99 versus $2.99 regular.
He also merchandises loose red and green peppers.
He tries to offer a different size or type of pepper in his loose display than what is in the overwrapped packages.
For example, he might feature the elongated Le Rouge variety in his bulk display of red peppers.
He aims for jumbo-size green peppers, but when they come in smaller, he often bags them and sells them at a discount.
Mini sweet peppers also are good sellers.
Heinen’s sells mini peppers year-round out of Mexico and from NatureFresh greenhouses.
The local Amish population grows organic mini peppers in the summer and packs them in wooden quart berry baskets, he said.
The stores also sell mini peppers in 1-pound bags.
Mini peppers are “very good sellers” at O’Brien’s Market, where Christensen features them in 1-pound bags of mixed orange, yellow, red and green varieties.
Mini sweet peppers also sell well at Felton’s, Marshall said.
They’re usually sold in 12- or 16-ounce bags, but sometimes Marshall offers bulk mini peppers, as well.
When he packs his own, they typically sell for $1.89 per pound.