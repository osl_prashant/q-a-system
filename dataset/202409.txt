Kansas City, Kan.-based Associated Wholesale Grocers Inc. reported record consolidated net sales of $9.18 billion and an all-time high year-end patronage distribution of $201.7 million.
 
AWG's net sales of $9.18 billion was an increase of 2.7%, according to information released March 22 at its 90th-anniversary shareholders meeting.
 
Total distribution to members - including promotional allowances, year-end patronage and interest - was $546 million. Stock value increased to $2,000 per share, up 4.4% over 2016. Total members' investment and equity ended the year with a new record at $544 million, up 14%, which the company said was "indicative of AWG's strong balance sheet."
 
AWG has a compounded annual sales growth of 8.7% and a compounded patronage growth rate on returns to members increase of 11.8% over the last 50 years, the company said.
 
"This is your company and we serve only one master in this business â€“ our member retailers," president and CEO David Smith said in the release. "Our ongoing mission is to provide our member-retailers all the tools, products, and services they need to compete favorably in all markets served."