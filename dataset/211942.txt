Short of going the H-2A route, labor remains a problem in the onion industry.
“Labor is very challenging,” said Shay Myers, general manager of Nyssa, Ore.-based Owhyee Produce.
“Basically, it’s an employees’ market where we struggle to fill key positions and positions for general labor.”
Kay Riley, general manager for Nyssa-based Snake River Produce Co., said, “It will be difficult, I’m certain.
“That’s a problem that’s kind of chronic and getting worse.”
John Vlahandreas, a Salem, Ore.-based onion sales manager for Idaho Falls, Idaho-based Wada Farms, struck a more confident tone.
“I think labor is always going to be a battle, but I don’t think we’ll have too much trouble with labor this year, not in the Northwest.
“California is a different story. We struggle with labor there every year.”
Jon Watson, president of Parma, Idaho-based J.C. Watson Co., said labor was “not an issue” because his company was participating in the H-2A program for all its onion topping labor.
The company also grows hops and peppers and uses H-2A workers for those crops as well.
“Our shed crews are coming back,” he said.
“We’re in pretty good position.”