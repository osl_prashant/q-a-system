Job increases slightly more than expected while unemployment ticks lower






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






The Employment report numbers today gave the Federal Reserve most if not all the reasoning it needed to boost interest rates in June. 
April Nonfarm Payrolls: 211,000
Expectation: 185,000 
What drove the increase: Gains were noted in leisure and hospitality (55,000 jobs as food service employment continued to rise. Over the past 12 months the sector has seen 260,000 jobs added.
Health care and social assistance also saw gains of 37,000 and employment in financial activities increased 19,000.
Mining also posted another gain of 9,000, having added 44,000 since bottoming in October 2016.
Retail services also posted a bit of a recovery, adding just over 6,000 jobs in April. But that makes up little of the more-than 55,000 jobs lost in the sector over February and March as bricks and mortar retailers shuttered operations.
The private sector added 194,000 jobs over the month, a sharp increase from the 77,000 level the prior month and with overall employment rising 211,000, it signals some of the government hiring freeze expiration is starting to show. The increase in government hiring of 17,000 was larger than the combined total for February and March of 12,000.
April Unemployment: 4.4 percent
Expectation: 4.6 percent
What fed the decline: The "simple" answer is the number of those employed grew faster than the labor force – the number employed rose 156,000 while the labor force increased just 12,000.
The 4.4-percent level is the lowest since May 2007 and you have to go back to May 2001 to find a rate lower than that.
The labor force participation rate ticked lower to 62.9 percent after being at 63 percent each of the past two months. The level of those not in the labor force increased by 162,000 over the month.
The so-called U-6 unemployment rate was at 8.6 percent in April, down from 8.9 percent in March and is the lowest level since late 2007.
Wage growth was noted, with a rise of 7 cents per hour which pushed the annual increase to 2.5 percent. Still a rather tame rise and suggests perhaps the labor market may not be as tight as the headline figures indicate.
Revisions to February increased the level to 232,000 (219,000 previously) while March was revised down to 79,000 (98,000 previously) for a net decline of 6,000 over the two months. That put job gains the past three months at an average of 174,000.
PERSPECTIVE: The rebound was a bit above expectations but the revisions to February and March were somewhat of a balancing factor. Market response has been relatively quiet as traders were largely expecting the data. The unemployment rate decline takes it to the lowest point in nearly a decade.
This will not shift the expectations that a rate increase in June is likely – the CME FedWatch tool was around 78 percent prior to the report and did briefly move above 80 percent but has since returned to around 78 percent. It also confirms what the Fed signaled when the Federal Open Market Committee (FOMC) meeting concluded Wednesday – the recent patch of soft economic data was likely transitory.
The wage growth may also be a little more reassuring the Fed as they look ahead to the June meeting. But it is a tight jobs market and the May update will be the next important marker ahead of the June FOMC session.


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.