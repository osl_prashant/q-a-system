According to an article on MarketWatch, Chipotle Mexican Grill Inc. sales plummeted another 23%, which led to the company's first loss as a public company. Chipotle offered its IPO in January 2006 and was profitable until last year, when a number of health outbreaks at its restaurants around the country sent its profits into a tailspin.Steve Ells, co-chief executive of Chipotle, continues to try to put a positive spin on the numbers. In the article, he said, "As our sales are on a gradual path to recovery, we remain focused on our mission of changing the way people think about and eat fast food. The best approach to re-building our business is to proudly serve safe and delicious food...which is exactly what we will continue to do."
Smart Analyst explains the company posted a net loss of $26.4 million, compared to a net income of $122.6 million in the same quarter in 2015.
Chipotle lost credibility with farmers when its marketing campaign positioned all other food sources except its own as suspect in terms of antibiotic use and safety.
Read the entire article here.
Related Articles
Chipotle mulls stepping back from some food-safety changes
Bloomberg: Chipotle faces no-win scenario as crisis taints every move
Chipotle faces criminal investigation
Chipotle sees legal d?©j?† vu
Dear Chipotle: Would you like some crow with that karma?
Americans aware of Chipotle outbreak eat there less often 
Chipotle stock falls after CDC says it is looking at more E. coli cases
Chipotle misled customers? Naturally!
Chipotle caught in pork hypocrisy
Chipotle says removal of pork items to hurt 2015 sales
Is Chipotle playing the public for chumps?
Patsche: Chipotle, there is no pork shortage