Nebraska college opens $4.2M renovated culinary school
Nebraska college opens $4.2M renovated culinary school

By JEFF KORBELIKLincoln Journal Star
The Associated Press

LINCOLN, Neb.




LINCOLN, Neb. (AP) — The colors were striking — the bright orange of a roasted sweet potato and the hunter green of grilled kale set against the stark white of the round dinner plate. Pan-roasted, medium-rare rack of lamb flanks the potato and kale.
An earlier course, a pork torchon, featured two lightly breaded TD Niche pork medallions atop watercress, with a drizzling of a mustard sauce, sided by pickled shallots.
These may sound like dishes found at Billy's, Venue, Dish or one of Lincoln's other fine dining restaurants.
But they were not.
These were prepared and served by Southeast Community College students Feb. 1, opening night of the school's new, open-to-the-public restaurant, Course, on campus.
The restaurant, with its white-clothed tables, black-carpeted floors (to absorb noisy chatter), pendant lighting and open kitchen, is part of a $4.2 million renovation of SCC's culinary/hospitality program, the Lincoln Journal Star reported.
Brandon Harpster, director of SCC's Great Plains Culinary Institute, said the goal is to provide the public an experience comparable to what they may enjoy at Billy's or Dish, while, at the same time, train students in the culinary arts.
"Fine dining is the most challenging," he said. "What the students learn here will translate to all sorts of dining."
Nobody is happier about SCC's new digs than Shaelee Luebbe, a 19-year-old from Lincoln who entered the school's culinary program 18 months ago.
Prior to the renovation, classwork was a "little bit difficult," Luebbe said. With classrooms on and off campus, "we were so spread out. And having to work alongside the cafeteria workers when they were trying to get ready for lunch time ."
She paused.
"With the new kitchen, everything is right there for you," she said.
"Everything" includes shiny, state-of-the-art new equipment such as an oven with the ability to bake multiple foods at different temperatures.
"When you bake macarons, it's important the temperature is right on," she said. "If the oven is off 25 degrees, it throws it off."
Luebbe speaks from experience. She laughed when asked if some of her macarons hadn't turned out.
"A couple of times, yes," she said. "It's heartbreaking."
SCC's culinary program is one of two in the state accredited by the American Culinary Federation Education Foundation — with Metropolitan Community College in Omaha being the other.
The school trains students to work in restaurants, retirement communities and/or hospitals, learning everything from basic knife skills to baking pastries to waiting tables.
"It runs the gamut," said Harpster, who also is executive chef at Single Barrel. He graduated from SCC's culinary school in 2000.
SCC's culinary program averages 90 students, but with its new facilities, Harpster said the school's goal is to grow that to 120 by next year.
Students began studies in January in their new facilities, which include two teaching labs and a bakery with three wood benches for kneading dough ("We never had one before," Harpster said) and walk-in, convection and artisanal ovens.
There's a TV studio-style demonstration kitchen, complete with three cameras, to allow the school to produce videos.
There are even little things that most people wouldn't care about, but the instructors and students can't stop discussing, such as the food pantry and the large, walk-in cooler that's no longer outside on a loading dock.
"Our students will be better prepared to go into the food service business than ever before," Harpster said.
When Raul Marroquin, a 21-year-old from Lexington, began the program in the fall of 2015, he didn't cook, couldn't cook. Now, he can make a sauce with the best of them.
"I wasn't familiar with cooking seafood or fish, and I've learned how to poach a fish," he said. "I can cut a lobster. I know how to debone a chicken and game hen."
Since working in the new kitchens, Marroquin can't get over how much room he has to operate. No more standing shoulder-to-shoulder with his classmates. No more waiting for a turn at a stove top.
"I get excited talking about it," he said. "We have all this brand-new equipment. We even have a new style of dishes, these awesome-looking plates."
Course operates as a full-service restaurant, allowing the students to work in real-life conditions. The restaurant is available to the public by reservation only for lunch and dinner. The current quarter is sold-out.
The school will begin taking reservations in April for the next quarter.
Starting in July, the restaurant will be open Monday through Thursday for lunch and Thursday evenings for dinner.
"We are built to grow," Harpster said. "We always had great curriculum, and we always had great faculty. We just needed a great space. Now we have it."
___
Information from: Lincoln Journal Star, http://www.journalstar.com


An AP Member Exchange shared by the Lincoln Journal Star.