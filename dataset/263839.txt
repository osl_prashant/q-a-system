Opposition to offshore drilling hardening in Massachusetts
Opposition to offshore drilling hardening in Massachusetts

By STEVE LeBLANCAssociated Press
The Associated Press

BOSTON




BOSTON (AP) — Opposition to a Trump administration proposal to allow oil and gas drilling in coastal waters, including those off the Atlantic coast of Massachusetts, continues to grow on Beacon Hill.
Just this week, Massachusetts Attorney General Maura Healey announced she's considering taking legal action against the administration to protect "the people, economy and natural resources of Massachusetts from the grave risks posed by unprecedented oil and gas leasing."
"Despite concerns from the fishing industry, clean energy developers, marine scientists and thousands of residents up and down the coast that depend on a healthy ocean, this administration has repeatedly ignored the serious economic and environmental risks of offshore drilling," Healey said as she filed comments with the U.S. Bureau of Ocean Energy Management opposing the plan.
Healey isn't alone.
Fellow Democratic attorneys general from a dozen coastal states, including neighboring Rhode Island and Connecticut, have also written Interior Secretary Ryan Zinke protesting the drilling plan.
Other critics include Republican Gov. Charlie Baker, Democratic U.S. Sen. Edward Markey and the state's entire Democratic congressional delegation as well as members of the fishing and tourism industries and environmental groups.
Zinke continues to defend the plan, which faces fierce opposition in states along the entire West Coast and much of the East Coast. Florida was dropped from the plan after the state's Republican governor and lawmakers pointed to risks to the state's tourism business.
Speaking at an energy-industry conference earlier this month, Zinke said he would listen to local objections. He also said state lawmakers have some leverage over drilling in federal waters since they would have to approve pipelines and terminals to handle the oil.
"You can't bring energy ashore unless you go through state water," he said.
Supporters of the plan, including representatives of the oil and gas industry, say expanding drilling opportunities in coastal waters will provide more affordable energy and help ease the country's reliance on foreign energy sources.
But critics on Beacon Hill remain staunchly opposed.
In her comments to the Bureau of Ocean Energy Management, Healey outlined the state's opposition to the offshore drilling proposal.
Healey said the proposal threatens Massachusetts' $7.3 billion fishing industry, the 90,000 jobs it supports and the state's 1,500 miles of coastline marked by destination beaches.
"The United States does not require expanded offshore fossil fuel extraction to meet future energy needs, nor can our nation afford the increased greenhouse gas emissions and other environmental risks that would result from such development," Healey wrote, citing a study that found climate-related property damage and associated emergency costs in Boston could reach $94 billion between 2000 and 2100.
The opposition to drilling isn't new in Massachusetts.
The Massachusetts attorney general's office successfully obtained an injunction in the 1970s against an offshore lease sale. Congress eventually imposed a moratorium that protected the federal waters from oil or gas leasing through 2008.
The federal push for offshore drilling also comes at a time when Massachusetts is growing more aware of the potential risks posed by climate change — particularly along its coastline — and is taking steps to move away from fossil fuels.
Massachusetts has already ramped up production of solar and wind energy and is working to put into effect a 2016 state law requiring utilities to solicit long-term contracts with providers of offshore wind and other forms of renewable energy, including hydroelectricity.
Just last month, the Massachusetts Senate Committee on Global Warming and Climate Change unveiled a bill that sets an overall goal of making the state 100 percent reliant on renewable energy by 2050.
The measure works toward that goal in part by pushing for more offshore wind power and hydroelectricity, increasing storage capacity for renewable energy, and expanding access to curbside charging stations for electronic vehicles.