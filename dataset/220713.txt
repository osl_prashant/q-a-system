Chicago Mercantile Exchange live cattle futures ended higher on Wednesday, supported by lagging cattle weights and expectations for higher cash market prices this week.
Packers will be battling for a smaller number of market-ready cattle on showlists after recent frigid weather in the U.S. Plains and heavy snow in Iowa impeded cattle performance.
"Cattle have stopped gaining weight, the weights are down, and they can't move them because of the weather in some areas," said Oak Investment Group President Joe Ocrant.
"Eventually this weather is going to break and the cattle are going to start gaining weight and you're going to have a boatload of cattle coming in," he said.
Investors are waiting on packer bids to develop following trades last week in the southern Plains at $125 to $126 per cwt. Some bids of $126 per cwt were reported on Tuesday against offers of $130, Ocrant said.
CME February live cattle closed up 0.825 cent per pound at 127.650 cents. April ended 0.450 cent higher at 125.225 cents and June gained 0.400 cent at 117.050 cents.
Feeder cattle futures also advanced, with March futures up 0.900 cent per pound at 148.175 cents and April 0.925 cent higher at 150.425 cents.
Lean hog futures climbed on technical and fund buying and spillover support from rising cattle.
February futures, which expire on Friday, ended up 0.150 cent at 73.500 cents.
The actively-traded April contract settled up 1.225 cents per pound at 70.650 cents after testing chart resistance at its 200-day moving average.