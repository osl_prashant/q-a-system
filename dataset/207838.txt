Maradol papayas are getting a lot of attention in the past few days, and not the good kind. 
The Packer’s coverage of the food borne illness outbreak linked to Caribeña brand maradol papayas distributed by Grande Produce of San Juan, Texas is grabbing plenty of clicks.   Earlier coverage is found here and here. 
 
 
Looking at Google Trends (pictured), it is easy to see the escalation in search engine traffic beginning the afternoon of July 21.
 
While there has no recall issued on the FDA website as of the middle of the day Monday, one would presume that something may be coming this week.
 
Other papaya marketers are rightly concerned that the public may paint all varieties and sources of the tropical fruit with the same broad brush.
 
In fact, HLB Specialties issues a news release today reminding both the trade and consumers that its papayas were not implicated by health investigators. From the company's release:
 
Pompano Beach, FL – In light of the recent Salmonella outbreak of July 2017, one of the largest papaya importers into the USA, HLB Specialties, is cautioning retailers and the media to make a clear distinction between the different papaya varieties, countries of origin, growers, and brands. The outbreak is limited to one specific papaya variety, Maradol, which is grown in Mexico. Papayas of other varieties and countries of origin have not been linked to the outbreak and are safe for consumption. 
Papayas are grown in many countries, including Mexico, Guatemala, Brazil, and the USA (Hawaii). Maradol Papayas are the large kind, weighing approximately three pounds and usually have a fully yellow skin when ripe. Tainung papayas are also large and similar to Maradol in size and weight, but they are greener and ready to eat when only half yellow. The small Brazilian Golden Papaya variety weighs around one pound and is also very sweet.
The company hopes to educate consumers on the different types of papaya in order to keep the public informed about the source of the outbreak. Melissa Hartmann de Barros, Director of Communications at HLB Specialties, notes “The safety of the consumers is our highest priority. We share their concern and caution, especially because people were sickened by the outbreak. But we also want to provide as much information as possible, so that shoppers can make an educated decision when buying papayas. Consumers can rest assured that the other large papaya variety Tainung as well as the small Brazilian Golden variety are not implicated and are safe for consumption.”
Food Safety is a very important part of HLB Specialties. The company follows all food safety guidelines put in place by FDA as well as third party certifying agencies that continuously monitor their growers’ work.
Salmonella is a bacteria found in raw foods that have been handled with poor hygiene. It is not a problem linked to a specific kind of food, but with incorrect handling, most likely in post-production with the use of unsanitary water or an unclean packing house. https://www.foodsafety.gov/poisoning/causes/bacteriaviruses/salmonella/.
In an effort to provide more information to the media and consumers, HLB has made several posts on their social media channels and contacted all major produce industry publications. To follow their posts, please see their Instagram, Facebook, and Twitter pages by searching @hlbspecialties or clicking on the links.
HLB Specialties carries organic and conventional Tainung papayas, Brazilian Golden papayas, mangoes, avocados, limes, as well as other specialty items such as rambutan and goldenberries. For more information please visit www.HLBinfo.com follow @hlbspecialties on Facebook, Instagram, and Twitter or call 954-475-8808.
 
 
TK: Kudos to HLB Specialties for being proactive with their message. So far, only one brand of maradol papayas has been implicated - so there is no need for health officials, retailers or consumers to paint the industry with the same broad brush.


<!--
 trends.embed.renderExploreWidget("TIMESERIES", {"comparisonItem":[{"keyword":"maradol papayas","geo":"","time":"now 7-d"}],"category":0,"property":""}, {"exploreQuery":"date=now 7-d&q=maradol%20papayas","guestPath":"https://trends.google.com:443/trends/embed/"}); 
//-->