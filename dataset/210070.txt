Lyndon Kelley keeps an ever-growing file of irrigation accidents and pivot wrecks spanning 30 years in time. Jammed in the fat file is a trove of photos that producers wish would remain buried. “One thing I know about the pictures,” Kelley says, “the farmers involved don’t want to be reminded of their mistakes.”
Crops don’t wait for irrigation, and now means now when it’s time to water. Solid end-of-season irrigation maintenance goes a long way in preventing problems the following crop year during crunch-time. Before the grip of winter sets in, checks and repairs are vital.
“A good list of needed repairs and a plan for tackling those repairs prevent in-season system breakdowns,” emphasizes Kelley, irrigation educator at Michigan State University-Purdue University Extension. “It’s a bottom-line savings of time and money. Flip-over accidents consistently happen, but the causes can be caught early and fixed.”
Across his career, Kelley has seen the aftermath of mishaps that easily could have been avoided. “I’ve seen pivots with water accumulation started at the end of fall that didn’t turn off at the end and flipped over. I’ve seen harvest equipment tip over pivots. And then there’s always freezing damage from systems that aren’t properly drained.”
Jonathan Aguilar, water resources engineer at Kansas State University, echoes Kelley and says post-season irrigation maintenance is particularly important in areas where freezing temperatures occur. “Draining is the easiest thing to do, but failure to drain sometimes becomes the most expensive repair,” Aguilar explains.
A significant experience issue plays into pivot maintenance, particularly in Michigan. Roughly 20% of Michigan producers are relatively new irrigation users (within the last decade) and don’t have experience with the causes and consequences of flip-overs or microswitch failures. “Hire a company to do an inspection,” Kelley advises. “They can pick out potential problems quickly and know to look for the small things that catch up with you when corn is head-high.”
Kelley also recommends taking advantage of fall opportunities when irrigation companies offer incentives with lower pricing or reduced labor costs for fall checkups or a winterization plan. If a workhorse pivot requires too many repairs, Kelley says growers may consider breaking down the machine and retiring it to smaller fields or dry corners. Fall months also can be an optimal time to buy new equipment due to larger rebates or price modifiers.




 


“A lot of fall irrigation maintenance deals with preparation for what’s coming,” Lyndon Kelley says. “If you wait until spring, it’s too late for anything but patchwork.”


© Chris Bennett







Kelley offers a checklist of 13 proactive measures to bolster end-of-season irrigation maintenance).
1. With the pivot in motion, listen to each tower drive system for gear box issues.
2. Check tires: Inspect wheels for loose bolts and wear.
3. Listen to the traveler drive system at the points of greatest stress (beginning of run for hard hose; end of run for soft hose) to identify weakness.
4. Turn water on and make a list of all leaks and worn sprinklers.
5. Check pressure gauge. A working gauge should hit zero when pressure is relieved, and fluctuate when the endgun or sprinkers are operated.
6. Note gauge pressures from regular season and compare with end-of-season performance.
7. Use a flow meter to measure output to ensure flow matches system design.
8. Compare application chart with actual application.
9. Test all irrigation controls.
10. Check barricade stops for integrity.
11. Take advantage of yield maps to improve irrigation design, maintenance and management.
12. Compare irrigated and non-irrigated yields to determine future irrigation investment.
13. Review the list and compare repair options versus buying new.
“A lot of fall irrigation maintenance deals with preparation for what’s coming,” Kelley concludes. “If you wait until spring, it’s too late for anything but patchwork.”
For a detailed explanation from Kelley, see Create the irrigation repair list as you end the season