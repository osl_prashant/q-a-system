See a related recall by Midwest retailer Meijer here.
Sparta, Mich.-based Jack Brown Produce Inc. is recalling gala, fuji, Honeycrisp and golden delicious apples.
The whole apples were packed and shipped by one of its suppliers, Nyblad Orchards Inc., on Dec. 11 through Dec. 17, according to a news release posted on the FDA website.
The apples were recalled because they have the potential to be contaminated with Listeria monocytogenes, an organism which can cause serious and sometimes fatal infections in young children, frail or elderly people, and others with weakened immune systems, according to the release.
The recall was the result of a routine sampling program at the Nyblad Orchards facility which revealed that finished products contained Listeria monocytogenes, according to the release.  
Jack Brown Produce has ceased the distribution of any further products processed at Nyblad Orchards, the release said, as the FDA and the company continue their investigation as to what caused the problem.
According to the release, the recalled apples were distributed through retail stores in Michigan, Georgia, Missouri, Indiana and Ohio. 
The recalled product, according to the release, consists of the following types of apples sold or distributed by Jack Brown Produce under the brand name “Apple Ridge” including:

Honeycrisp apples in two-pound clear plastic bags;
Gala, fuji, and golden delicious apples in 3-pound clear plastic bags;
Fuji and gala apples in 5-pound red-netted mesh bags; and
Gala, fuji and Honeycrisp apples that were tray-packed/individually sold.

The release said potentially-affected product can be identified by the following lot numbers printed on the bag label and/or bag-closure clip:

Fuji: NOI 163, 165, 167, 169, 174
Honeycrisp: NOI 159, 160, 173 
Golden Delicious: NOI 168 
Gala: NOI 164, 166 on either the product labels and/or bag-closure clip.

The release said consumers who have purchased gala, fuji, Honeycrisp and golden delicious apples under the brand name “Apple Ridge” on or after Dec. 11 are urged to destroy the product and contact Jack Brown Produce for a full refund.
Consumers with questions may contact the company at 616-887-9568 (Monday-Friday, 8:00 a.m. to 5:00 p.m. ET) and ask for Lisa Ingalls.