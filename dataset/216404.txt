Rice acreage is ready to jump, but the dance floor is a swirl of jostling factors capable of pushing the market pace.
Three million planted acres may be the 2018 rice mark, a significant bump from approximately 2.4 million harvested acres in 2017. However, price per bushel concerns, soybean acreage, lender attitudes, foreign market questions, and input costs all weigh heavy in determining how much U.S. farmland gets dotted with rice seed in 2018.
Jarrod Hardke, Extension rice agronomist with the University of Arkansas System Division of Agriculture, says the current mid to low $5 per bushel price range (compared with a low $4 per bushel range in January 2017) reflects tightening of supply and demand, particularly following reduced U.S. rice acres in 2017. The low-acre year and tightening supply is affecting the price point and incentivizing rice acres in 2018, he explains.
What price is a “must” for growers? A high $4-range is either a loss or breakeven point. Hardke points to a $5 minimum: “Growers feel the need to get at least $5 per bushel, not including any payments or uncontrollable factors. Depending on the specific situation, $5 stays above the cost of production. Less than $5 is flirting with losing money, particularly with an unexpected drop in yield.”
As rice’s standard rotational partner, soybeans play a major role in determining acreage numbers. The acreage “yoyo” goes up and down with relatively consistent speed, but enough price support can jerk the string in either direction. In the current low $10-range, soybeans could factor against rice before spring planting. “The soybean situation isn’t phenomenal at $10, but it’s good,” Hardke notes. “However, if soybean prices climb toward planting season, the rise would definitely influence growers to stay in soybeans.”




 


Half of U.S.-produced rice is exported and the perpetual foreign market wild card is hovering over 2018.


© Chris Benentt







Hardke says lenders play a big rice acreage role. Prior to 2017 planting, if a grower needed $700 per acre for rice, versus $350 per acre for soybeans, a lender might be expected to push away from low price per bushel rice: “In 2018, if rice is up a bit and beans aren’t climbing, there is more competition for lenders.”
Half of U.S.-produced rice is exported and the perpetual foreign market wild card is hovering over 2018. Several memorandums of agreement (MOAs) with multiple countries are in play and a few more tender purchases would have a large influence on the market. China deliveries may begin in 2018, but initial loads will likely be small, Hardke explains: “That’s a huge wild card. Overestimating or underestimating what China will take could have tremendous influence.”
Hardke doesn’t minimize the significance of input costs prior to planting, particularly with high urea prices and diesel trending at $2.20 per gallon. “Urea and diesel are huge for rice, and if they continue to climb, we’ll have a harder time with an acreage increase.
Where will all of these factors send U.S. rice acreage?
“I see the possibility of 3 million total rice acres. Of that, I’d estimate 2 million acres will be long-grain. These are difficult cues to read, but rice acreage is coming back in 2018, it’s just a question of how far it will go.”