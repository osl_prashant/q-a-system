Western Europe remains on track for a big wheat crop this year but a rain-drenched spring in France has raised doubts about the overall size and quality of this summer's harvest.The risk of damage from torrential rain has led analysts to cut expectations for France and Germany, the European Union's top two wheat growers, although this has been broadly offset by improving prospects in other countries, like Spain.
Consultancy Strategie Grains last week left unchanged its monthly EU soft wheat production estimate at 146.7 million tonnes, while the EU's crop monitoring service on Monday lowered slightly its yield forecast for the EU harvest.
Last year, the EU reaped a record 151 million tonnes.
France is the biggest question mark after storms and heavy rain lashed its main northern wheat belts.
The downpours have intensified already high disease pressure on French wheat. The main threat is now fusarium, also known as head blight or scab, that can hit both yield and quality, although it was seen as too early to gauge any losses before harvesting starts next month.
"We have customers reporting fusarium across much of the country," Paul Gaffet of agricultural consultancy ODA Groupe said. "But we really don't know how to quantify it. Everything looks more uncertain this year."
ODA thinks the soft wheat crop could be anywhere between 36.5 million and 38.5 million tonnes. Fellow analysts Strategie Grains last week cut their French forecast by 800,000 tonnes to 38.5 million, while traders put the crop between 37.5 million and 38.5 million.
France harvested a record 41 million tonnes last year.
In Germany, rain in the south had raised quality worries but may have benefitted dry areas in key northern production zones.
"Overall the picture is still positive but there is worry about possible loss of protein content in some regions, especially south Germany where rain was heaviest," one German grains analyst said.
Germany's 2016 wheat crop will fall 2.9 percent on the year to 25.77 million tonnes, the country's association of farm cooperatives estimates, still above Germany's 2010-2015 average of around 25 million tonnes.
In Britain, sunny weather was needed to boost yields after rain and allow the harvest to reach last year's output of 16.4 million tonnes.
"Wheat crops should be well into their yield forming phase now and everyone wants to see some more sunshine to drive some energy into the crop, so that is a concern," said Jack Watts of Britain's Agriculture and Horticulture Development Board.
Poland was still heading for a smaller harvest due to winter frost and springtime dryness.
The dryness could hit yields of spring wheat, which has a bigger share of area this year after frost damage to winter wheat, said Wojtek Sabaranski of Sparks Polska.
Sparks Polska sees wheat output falling 10 percent from last year to around 10.7 million tonnes.