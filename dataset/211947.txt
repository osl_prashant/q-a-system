Del Monte extends global footprint
The Del Monte Fresh avocado program has seen significant year-over-year sales growth, said Dennis Christou, vice president of marketing for Del Monte Fresh Produce NA Inc., Coral Gables, Fla.
“We are working to expand our global footprint of avocados through new packing facilities, personnel and customer-specific programs,” he said.
The company recently added more than 40 ripening rooms for its avocado operations, he said.
 
Del Rey Avocado expands in N.J.
Del Rey Avocado Co. Inc., Fallbrook, Calif., is undergoing a major expansion of its facility in Vineland, N.J., said Bob Lucy, partner.
The company is adding 25,000 square feet of cold storage, ripening facilities and a bagging center to the existing nearly 20,000-square-foot site at Del Rey Farms.
“It’s a good size for what we need to do right now,” Lucy said, and there will be room to expand.
The company also will start its Morro Bay, Calif., avocado program with Shanley Farms in August. Avocado volume is down significantly in California this season, but the Morro Bay program will continue nonetheless, he said.
“Customers appreciate the quality,” Lucy said.
 
Giumarra opens port location
Giumarra Agricom International, Escondido, Calif., has expanded its Escondido, Calif., location and has a new facility in the Port of Long Beach with packing and bagging capability and offices, said Gary Caloroso, marketing director.
The port location is convenient for the company’s West Coast berry division and its Southern Hemisphere office, he said. Besides avocados, the facility will handle berries, grapes and stone fruit from Chile and Peru.
 
HAB tabs industry affairs director
The Mission Viejo, Calif.-based Hass Avocado Board has named John McGuigan as director of industry affairs, according to a news release.
He has worked for Ayco Farms, Dulcinea Farms LLC and Sunkist Growers Inc.
He will focus on continued development of supply and demand data, quality and sustainability, the release said.
 
Index Fresh building facility
Index Fresh, Riverside, Calif., is building a facility at the Pharr-Reynosa International Bridge in Pharr, Texas, said Dana Thomas, president.
Construction started on the 60,000-square-foot cold storage, ripening and bagging facility in early April and should be finished by mid-November, he said.
The company used an outside vendor for those functions.
The location, which will handle product from Mexico, will accommodate 2,600 pallets and will have room for 10 ripening rooms and bagging machines.
 
Global sourcing focus for Mission
Oxnard, Calif.-based Mission Produce Inc. is focusing on growing its capacity globally, said Robb Bertels, vice president of marketing.
The company recently entered a formal marketing and distribution agreement with Colombian avocado grower/packer Cartama, the largest producer of hass avocados in Colombia, he said. Colombia is the eighth country where Mission has invested capital and infrastructure.
 
New Limeco adds 400 avocado acres
New Limeco LLC, Princeton, Fla., has added more than 400 acres of Florida avocados this season because of increased demand, said Eddie Caram, general manager. The company owns and manages nearly 1,200 acres in Florida.
 
Peruvian avocados sponsor festival
Avocados From Peru was a major sponsor of “Kaypi Perú” (“This is Peru”), a festival July 27 that highlighted the nation’s cultural heritage, according to a news release.
Avocados From Peru donated ripe avocados that were used by The Smithsonian Museum of the American Indian’s chef to create unique dishes for the July 27 reception and the museum cafeteria during the festival.
 
Robinson Fresh boosts ripening
Robinson Fresh, Eden Prairie, Minn., has been working on pre-ripening programs that cater to consumer demand for ripe-and-ready product, said Sergio Cordon, avocado category manager.
“We aim for our preripening to take place at shipping and warehouse locations as close to the customer as possible so the final product arrives ready to eat,” he said.
Robinson Fresh has found avocados and the overall tropical category to be an area of growth for the company.
 
Simply Avocado ready to launch
Wholly Avocado from MegaMex Foods, Orange, Calif., is about to launch a line of products under the Simply Avocado brand, said Jay Alley, vice president of sales.
Simply Avocado is a convenient substitute for fresh avocado, he said, and offers “ripe, great-tasting avocado year-round.”
Flavors include Sea Salt, Garlic Herb, Roasted Red Pepper and Jalapeño Lime.
“The flavor simplicity creates usage versatility and makes a great spread, topping, dip or perfect on its own,” he said.
It’s packed in cartons of eight 8-ounce trays with a suggested retail price of $3.99.
Simply Avocado will be available starting in September, Alley said.