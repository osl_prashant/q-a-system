New Zealand’s Fonterra reduced its farmgate milk price forecast for the 2017/18 season by NZ$0.35, or 5.2 percent, to NZ$6.40 ($4.42) per kg of milk solids, citing volatility in the global dairy market.
The price cut would equate to New Zealand dairies receiving $14.86/cwt when coveting to U.S. milk solid measurements. 
Chairman John Wilson said in a statement on Wednesday that strong production out of Europe and continuing high levels of skim milk powder stockpiles as a result of a European Union intervention has created a downward pressure on global prices.
That offset strong demand for dairy in China, other parts of Asia and Latin America, Wilson added.
Last week, the world’s biggest dairy producer had flagged that its arbitration with French company Danone SA had an impact on its earnings guidance for the season but none on the farmgate milk price.
A arbitration tribunal had ordered Fonterra to pay Danone a smaller than expected 105 million euros ($124.05 million) over a contamination scare.
Fonterra also said on Wednesday that sales volumes in the first quarter fell 20 percent to 3.9 billion liquid milk equivalent (LME), due to poor performance in its dairy ingredients business.
Revenue rose 4 percent over last year to NZ$4 billion.
($1 = 1.4489 New Zealand dollars) ($1 = 0.8465 euros)