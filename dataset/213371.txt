East Grand Forks, Minn.-based Folson Farms has added two potato storage bins this year, each with a capacity of about 50,000 cwt., increasing the company’s storage capacity by about 20%. Photo courtesy Northern Plains Potato Growers Association
A&L Potato re-enters spuds
East Grand Forks, Minn.-based A&L Potato Co. is growing potatoes for the first time since 2003, said Randy Boushey, president and CEO of the established potato packing operation.
The company has rented 500 acres in East Grand Forks for its own production, Boushey said.
Boushey said he saw an opportunity to get into production when land became available in the wake of a bankruptcy liquidation by McM Inc. in February.
“We got 100% of our red potatoes from (Ron McMartin), and it kind of opened up a hole for us,” he said.
Zach Druer runs the farming operation for A&L, and Frank Vargars oversees production, Boushey said.
A&L still handles potatoes from other growers, about 700 acres, Boushey said.
 
Associated Potato upgrades, tries organics
Grand Forks, N.D.-based cooperative Associated Potato Growers Inc. continues to upgrade its food safety procedures, said Paul Dolan, president.
“One thing we continue to do is to take the necessary steps in our food safety side of things to be in compliance with all the regulations that need to be in place,” he said.
“We recently got word from Wal-Mart that we needed to step up our grower audits to the next level, and we’re accomplishing that.”
Associated also recently installed a polisher in its Dayton, N.D., plant.
“That will give us a polisher in all three of our facilities (Grand Forks, Dayton and Grafton, N.D.),” Dolan said.
The company also installed a palletizer in its Grand Forks facility, Dolan said.
Associated also is trying out organic potatoes for the first time, Dolan said.
“We are stepping into organics in a very small way — we have one grower that’s growing 50 acres of organic dark red norlands,” he said.
 
Black Gold reprises Redventure promo
Grand Forks, N.D.-based Black Gold Farms plans to feature its Redventure campaign again at the Produce Marketing Association’s annual Fresh Summit convention and expo, Oct. 19-21 in New Orleans.
The company will have product samples at its booth, No. 4436.
“The idea is that this will not only motivate retailers to focus on red potatoes, but get consumers excited about different ways that red potatoes can be cooked and consumed,” said Leah Halverson, director of new business development.
Black Gold Farms also has shifted to all-irrigated production, said Eric Halverson, executive vice president of technology.
 
Folson Farms boosts storage
East Grand Forks, Minn.-based Folson Farms has added two new bins, each with a capacity of about 50,000 cwt. of storage, this year, said Bryan Folson, president.
The addition upped the company’s storage by about 20%, Folson estimated.
“We had outside storage but decided to get it on-site — gentler on the potatoes, nicer that way,” he said.
 
Northern Plains ramps up marketing plans
The East Grand Forks, Minn.-based Northern Plains Potato Growers Association will be more active in advertising this year, said Ted Kreis, marketing director.
“We increased our ad budget and coordinated all our displays to match our advertising,” he said.
The association’s 52nd annual Potato Bowl Week was Sept. 5-9 in Grand Forks.
The week featured various events, including the annual Potato Bowl game in which the University of North Dakota defeated Missouri State, 34-0.
“Our Potato Bowl Week was very successful,” Kreis said.