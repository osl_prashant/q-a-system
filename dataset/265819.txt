Fishermen suit against Atlantic marine monument moves ahead
Fishermen suit against Atlantic marine monument moves ahead

By PATRICK WHITTLEAssociated Press
The Associated Press

PORTLAND, Maine




PORTLAND, Maine (AP) — Organizations suing to eliminate the first national marine monument in the Atlantic Ocean have gotten the OK to proceed with a suit designed to reopen the area to commercial fishing, which environmentalists fear could jeopardize preservation efforts.
The fishing groups sued to challenge the creation of the Northeast Canyons and Seamounts Marine National Monument created by President Barack Obama in 2016. It's a 5,000-square-mile area off of New England that contains fragile deep sea corals and vulnerable species of marine life such as right whales.
The fishermen's lawsuit had been put on hold by a review of national monuments ordered by President Donald Trump's administration in April 2017. Court filings at U.S. District Court for the District of Columbia say the stay was lifted in mid-March and the litigation can proceed.
Marine national monuments are underwater areas designed to protect unique or vulnerable ecosystems. There are four of them in the Pacific. The Northeast monument, the only one off the East Coast, is also an area where fishermen harvest valuable species such as lobsters and crabs.
"To lose a big area that we have historically fished has quite an impact on quite a lot of people here," said Jon Williams, a New Bedford, Massachusetts, crabber and a member of plaintiff group Offshore Lobstermen's Association. "It'll raise attention to it a little bit, which it needs."
The court ordered the federal government, which is the defendant in the case, to respond by April 16. A spokeswoman from the federal Department of Commerce declined to comment.
The lawsuit's ability to move forward will hopefully prod the federal government to make a decision about the future of the monument, which is unpopular with commercial harvesters, Williams sad. But a coalition of environmental groups is also intervening in the case in an attempt to keep the monument area preserved.
"These public waters belong to the American people, and we stand ready to defend them from encroachments on the centuries-old corals, endangered whales and other precious marine life this special ocean area encompasses," said Brad Sewell of one of the intervening groups, the Natural Resources Defense Council.