European scientists want consumers to stop eating so much beef and dairy, saying the methane and nitrous oxide being emitted is to blame for global warming.In a recent paper, researchers from Chalmers University of Technology in Sweden assert that cutting beef consumption by 50% would help the European Union reach its target of limiting global climate change to 2¬?C by 2050.
An analysis was performed to determine what sectors of agriculture play a role in greenhouse gas emissions and if any changes could be made to slowdown global warming. According to the study, beef production accounts for 36% of greenhouse gas emissions and 48% of emissions for both methane and nitrous oxide.
Production of dairy products like milk, cheese and butter was estimated to account for 23% of greenhouse gas emissions and 28% of emissions for both methane and nitrous oxide. The study put 90% of dairy cow emissions into the creation of dairy products and the remaining 10% into beef production.
"Technologically, agriculture can improve in productivity and through implementation of specific mitigation measures. Under optimistic assumptions, these developments could cut current food-related methane and nitrous oxide emissions by nearly 50%," the researchers wrote.
The scientists recommend consumers reduce their beef consumption to help curtail the effects of climate change. Eating pork and poultry does not seem to make as much difference; researchers say the environmental targets could still be met even with high consumption of pork and poultry. As for dairy, high levels of consumption could be maintained, but technological changes will be needed, according to the study.
Scenarios were laid out by the researchers of what food demand and major diets could do to impact change on global warming by 2050. The diet changes included the following:
Less meat
Dairy beef
Vegetarian
Climate carnivore
Current diet (based on Swedish eating habits)
Baseline (increasing meat consumption, with less dairy and carbohydrate)

In the researchers' analysis of the various scenarios, protein consumption could be maintained in the "less meat" diet if legumes, oil and cereals are consumed at levels to balance the diet.
More from AgDay:


<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->

Want more video news? Watch it on AgDay.