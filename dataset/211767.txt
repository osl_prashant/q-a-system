Even as rains continue to inundate Houston and other parts of Texas (see coverage), some in the produce industry are thinking back 25 years, when Hurricane Andrew inflicted similar damage to Southern Florida.
 
The terrible fury of Hurricane Andrew welcomed Mike Stuart to Florida.
 
Coming from Western Growers Association in Southern California, Stuart began his first day in the Orlando, Fla., office as president of the Florida Fruit & Vegetable Association the morning of Aug. 24, 1992. Hours before, Category 5 Hurricane Andrew had roared across the southern tip of the Florida peninsula, hitting the produce-strong Homestead region hard. 
 
“It was literally my first day in the office,” Stuart said Aug. 23. “I got to the office about 7 a.m. and the phone was ringing off the hook — everybody wanted to know what was going on, and of course nobody knew anything.”
 
Stuart said phone lines in south Florida were down so it was hard to immediately find out what damage the storm had done to the fruit and vegetable industry.
Somehow, Stuart was able to connect late that morning with Pal Brooks, president of Homestead-based Brooks Tropicals Inc. and hear some of the devastating reports.
 
“You forget a lot of stuff, but I remember that day,” Stuart said recently.
 
Brooks Tropicals was nearly blown away.
 
“As a lifelong resident of Homestead, it was a hurricane like none that I ever experienced,” Brooks said. “It helped me understand what a tornado in the Midwest would be like.” 
With winds of more than 160 mph, Hurricane Andrew took about an hour to move through Homestead.
 
After it was done, Hurricane Andrew had destroyed 26,000 homes and damaged 100,000 more in south Florida. Total damage caused by the hurricane was estimated at $26 billion.
 
“Damage was in about a 20-mile swath,” he said. “It was equivalent to a tornado that was 20 miles wide.” Trees and other landmarks around the region disappeared.
 
The company’s 175,000-square-foot-packinghouse in Homestead lost its roof.
 
In 1992, Brooks was growing 12 tropical fruits in the Homestead region, and Andrew cut that to two. 
 
“I lost about 80% of my net worth in one hour,” he said.
 
The hurricane caused disruptions that influence how trade is done today.
 
Lorraine Conlin, director of sales and service at Radiant Customs Service Inc., New York, was operating her own company, Marathon Freight Services Inc., New York, when the hurricane hit. Her customs brokerage was barraged by clearance requests from Florida brokers. With delays in Miami, importers began shipping direct to John F. Kennedy International Airport in New York.
 
“Some of the clients handled back then are still clients today and continue to use New York’s JFK airport and the Newark airport as port of entry,” Conlin wrote in an e-mail.
 
 
Damage and recovery
Brooks said lime trees were wiped out in south Florida.
 
“The lime trees had been planted on rootstocks that in hindsight are not able to hold the trees to the ground with winds above 100 miles per hour,” he said.
 
“When we went to visit lime groves after the storm passed, we found all the trees blown out of the ground and rolled to the edge of the grove like tumbleweeds,” he said.
 
Limes were replanted with better root stock after Hurricane Andrew and were beginning to reach good production levels by the late 1990s. However, canker was discovered and quarantine rules dictated an infected tree would result in the destruction of every tree in a half-mile radius.
 
“The end result was that the entire (lime) industry got plowed out and we were not able to come back after that.”
 
Mangoes also had a difficult time coming back, though some are still grown in South Florida.
 
Avocado groves were resilient compared with lime groves, but acreage after Hurricane Andrew dropped from 8,000 acres to 5,000. Today’s avocado acreage is close to 7,000 acres, Brooks said.
 
With urban encroachment, water management issues and the threat of tropical storms, Stuart said it takes a tough-minded producer to make it in South Florida. 
 
Brooks said Hurricane Andrew came after a period of about 25 years with no hurricanes. Today, south Florida’s Homestead region has gone about 12 years since the last hurricane. The calm won’t last forever, Brooks knows.
 
“If you grow tree crops in the Homestead area, hurricanes are a fact of life,” he said.