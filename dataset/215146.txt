It’s no secret the farm economy is in a downturn. Have you ever compared the cycles in the farm economy to the seasons of the year? Ag Economist Dr. David Kohl does.
“That period from 2006 to 2012, she was the summer,” he explains. “High commodity prices in every major commodity.”
He then says the period from 2013 to today is what he considers to be the Fall season.
“In other words, commodity prices are in an elongated kind of reset,” he says. “People have been pretty resilient because they had built up working capital and of course land values who were strong, so we had some equity on the balance sheet.”
Unfortunately, Kohl says 2018 to 2021 is going to be like a cold winter.
“What I'm starting to observe in this winter period is that I've got a certain group of producers that have made adjustments and they're making a little money,” he explains. “I've got another group, I call them kind of the bridge, and they’re looking for a refinance. Then I've got another 30 percent of producers out there at the end of the pier and those people burning through land equity etc.”
Fortunately, spring is coming. He anticipates it to be somewhere from 2021 to 2025.
“I call it the spring of the year because what will happen is we'll weed out the some of the inefficient, some of the less progressive type of managers,” he says adding those businesses will be purchased by the other 40% of producers.
“So we're going through these four seasons, and this is the elongated downturn,” he says.