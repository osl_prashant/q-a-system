Are you familiar with Morrissey?
He’s the controversial singer-songwriter-social activist who’s been described  --  by critics who actually like him!  --  as “notoriously pale, petulant and reclusive.”
The British-born (although he’s Irish), 56-year-old former lyricist and lead singer of the rock group The Smiths cut a swath through the contemporary music scene in the 1980s and ‘90s as one of the leaders of what was labeled “indie rock.” Certainly his albums have been outside the mainstream, with such titles as “Viva Hate,” “Ringleaders of the Tormentors” and “Maladjusted.”
An embraceable pop star he’s not.
Despite the musical trailblazing with which he’s been credited, he’s better known for his aggressive stance against raising livestock and eating meat. Inside the little bubble world in which so many vegans exist, Morrissey is  --  literally  --  a rock star.
Now, he’s contemplating a move that goes beyond his usual posturing: He’s thinking of running for mayor of London.
BBC News reported that the “sulky crooner” has been approached by the Animal Welfare Party and asked to stand as mayoral candidate on its behalf. The party’s leader, Vanessa Hudson, said Morrissey has the credentials to make a successful run for the office.
“We’d like to see the mayoral contest include the views of a candidate who would seek to champion London, not only as a world leading city for people but for animal welfare, too,” Hudson said in a statement.
I suggest that most political analysts would question how much leverage that position could command.
Inside the ravings
But beyond the advocacy of a vegetarian diet, Morrissey’s mindset triggers a more serious concern. Consider what he wrote on his own website:
“There must be a governmental voice against the hellish and archaic social injustice allotted to animals. The sanctimonious disaster of animal agriculture cannot be allowed to go on forever. There is no such thing as humane slaughter, and if you believe that there is, then why not experience it for yourself? If animal serial killer [chef] Jamie Oliver feels so passionate about including 'kid meat’ (young goat) into the human diet, would he consider putting forth one of his own kids (children) for general consumption?
“The meat industry, after all, shows no compassion towards the planet, towards climate change, towards animals, towards human health. It is diabolically contrived and is the world’s number one problem.”
Okay, it is easy to dismiss Mr. Morrissey as a raving idiot who makes no sense, but there is a deeper disconnect here than merely favoring plants versus animals as a food source.
In condemning animal agriculture, he’s also condemning all of agriculture, because the cultivation of food crops has progressed in the same direction as livestock production: Efficiencies of scale, the use of technology and the mechanization of much of the manual labor formerly required to produce even the most basic of foodstuffs.
In spewing his invective about disasters and injustice and problems, he’s ignoring some other words equally relevant to a discussion of modern food production.
Abundance. Nutrition. Affordability. Choice. Variety. Convenience.
I could go on.
Thanks in large measure to “the sanctimonious disaster of animal agriculture,” the developed world has conquered hunger, famine and starvation for the first time in 10,000 years of human history. But let’s bring it a little closer to home.
It might be hard to believe for today’s affluent, social media-embracing, techno- savvy citizens of Europe and North America, but a couple generations ago, more than 10 million people in Europe and Asia starved to death during World War II. Farms were destroyed by the fighting, transportation lines and distribution infrastructure were crippled by relentless bombing and severe shortages of energy supplies meant that there was no food for millions of people in Russia, China and Europe.
We’re apparently so far removed from that recent history that some of our so-called thought leaders now feel comfortable bashing the very agricultural technology that finally took hunger and famine off the table.
I’m not suggesting that modern agriculture cannot be critiqued, that the production of both animal- and plant-based foods couldn’t be improved with respect to environmental impact, resource consumption or even animal well-being.
Any production system driven by high volume and high efficiency triggers trade-offs, whether it’s manufacturing, retailing or agricultural production. Smaller operators get driven out of business, some of the individual “craftsmanship” gets lost and variety and choice are frequently compromised.
But on balance, anyone on Earth would vastly prefer those negatives as opposed to hunger, famine and starvation.
We’re fortunate, even though we rarely realize it. We don’t have to worry about dying due to such a severe shortage of food that there is literally nothing to eat.
And that is due to the very system Morrissey and his ilk label “a disaster.”
Go ahead, Londoners. Elect this loon as your next mayor. Thanks to mad cow and foot-and-mouth disease, British agriculture has been devastated in recent years.
Put Morrissey into a position a power, and you can kiss the whole enterprise goodbye.
Dan Murphy is a food-industry journalist and commentator