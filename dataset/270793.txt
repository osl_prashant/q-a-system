Quick question: Has retail “shrink” for fresh produce increased or decreased in the past 10 years? 
What would you say? If you say “decrease,” it might be because you believe that improved packaging technologies and new transportation efficiencies would favor less loss at the retail level. 
 
I would certainly lean in that direction.
 
If you speculate that shrink has increased, you could support that argument with the contention that U.S. retailers are “carrying a greater assortment of fresh produce in abundant displays, sometimes far more than is sold, and they are struggling to find trained labor to tend these fresh commodities in the store. Further, retailers have become better at tracking shrink due to improved inventory control technologies.”
 
That quote and those talking points, in fact, are from a new U.S. Department of Agriculture Economic Research Service study called “Updated Supermarket Shrink Estimates for Fresh Foods and Their Implications for ERS Loss-Adjusted Food Availability Data.” 
 
The study’s authors are Jean Buzby, Jeanine Bentley, Beth Padera, Jennifer Campuzano and Cara Ammon, according to the USDA. “Shrink” is defined to include food loss, reductions due to theft and accounting errors.
 
So what did the study find? Surprisingly — at least to me — the study found that retail shrink for fresh fruits and vegetables has increased in the past decade.
 
The study looks at supermarket shrink estimates for fresh fruit, vegetables, meat, poultry and seafood in U.S. supermarkets in 2011-12 by comparing supplier shipment data with point-of-sale data.
 
 The data, the USDA said, came from one large national and four regional supermarket retailers. The study was more robust than a previous effort, using data from 2,900 stores rather than 600 stores in 2005-06.
 
Here’s the kicker: The report said the adoption of the new estimates in the loss-adjusted food availability statistics would decrease 2012 per capita estimates of annual retail availability of fresh fruit by 4.7 pounds (4.3%) and fresh vegetables by 1.7 pounds (1%). The average fresh fruit shrink estimates at retail were estimated at 12.6% for 2011-12, according to the report, up from 11.4% in 2005-06. 
 
The average fresh vegetable shrink estimate at retail was pegged at 11.6% in 2011-12, about 20% more than 9.7% in 2005-06.
 
Again, this report doesn’t answer the question of “why?” 
 
Some comments from the Linkedin Fresh Produce Industry Discussion Group on this topic speculate on the role of organic produce (slower turns, higher shrink), changes in consumer preference (red delicious is more sturdy than a Honeycrisp, a navel more durable than a clementine), local fruits and vegetables (no reason, just a convenient scapegoat), fixed-weight packages (entire package gets tossed rather than culled and repacked) and a longer supply chain.
 
Looking to future research, the authors said a better understanding of retail food loss is needed, especially related to the extent of theft in retail stores and food donations by retailers.
 
In the context of this new report that ups the shrink estimate for fresh fruits and vegetables, there is a Huffington Post article about a change.org petition (now with more than 90,000 supporters) that seeks to compel Wal-Mart to sell ugly produce.
 
“Now we are asking you to call on Wal-Mart to join the movement against food waste by agreeing to sell cosmetically ‘less than perfect’ produce,” writes Jordan Figueiredo.
 
While USDA researchers are at it, they should check out whether selling ugly produce at Wal-Mart would increase or decrease food waste and shrink. The answer may surprise but not delight fans of ugly produce.
 
tkarst@farmjournal.com
 
What's your take? Leave a comment and tell us your opinion.