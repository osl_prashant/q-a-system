A decision by the Environmental Protection Agency will allow California citrus growers to continue to have access to an insecticide used to control the Asian citrus psyllid and other pests. 
The EPA denied an petition requesting that the agency revoke all tolerances and cancel all registrations for the insecticide chlorpyrifos, which is registered for use on citrus, vegetables and many other crops.
 
“We believe that sound science should prevail in the regulation of crop protection tools,” California Citrus Mutual president Joel Nelsen said in a news release.
 
Nelsen and members of the group’s executive committee met several times with EPA staff last year to convey the importance of chlorpyrifos in citrus production, according to the release.
 
California Citrus Mutual and other grower groups have urged the EPA to evaluate the product and all pesticides with transparent, science-based studies, according to the release.
 
“We believe this decision is a step in that direction,” Nelsen said in the release.
 
Acting March 29, EPA compelled by a court order to render a decision by March 31.
 
American Farm Bureau Federation president Zippy Duvall also praised the EPA action.
 
“Farmers nationwide depend on chlorpyrifos in managing their crops,” Duvall said in a news release. “It is widely and safely used for a wide range of crops, including alfalfa, citrus, vegetables, soybeans, almonds and others.”
 
Duvall said chlorpyrifos has been a part of environmentally friendly integrated pest management programs for almost 50 years.
 
The chemical is still subject to registration review and any concerns about its safe use can be addressed in that process, Duvall said in the release.
 
Andrew Kimbrell, executive director of the Center for Food Safety, criticized the EPA move in a news release. Kimbrell called the decision a “frightening indicator” that the Trump administration will put corporate profits before children’s health.