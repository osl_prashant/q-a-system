The U.S. Department has named three new members to the nine-member Mushroom Council.
The new members, serving three-year terms, are:

Curtis Jurgensmeyer, Miami, Okla.
Joe Caldwell, Reading, Pa.
Jane Irene Rhyno, Ontario, Canada

The board consists for two growers from Region 1 (all states except California and Pennsylvania, four growers from Region 2 (Pennsylvania), two growers from Region 3 (California), and one importer.