Any good farm owner/manager knows what it means to get animals “off to a fast start.” Why shouldn’t new farm employees be afforded that same fast start? They need quality information, assistance and attention in the first few weeks of their employ­ment. That process is called “orientation.”
A good orientation starts as soon as the new employee signs the paperwork and can run 90 days or more once they have started. Orientation is not the same as training. Training involves learning specific tasks and skills, but orientation brings the employee into the culture and working systems of the farm.
Employees need to know the basics of coming to work and being an employee on the farm. A simple tour of the farm is a good place to start. That is also the time to talk about other basic topics.
What to talk about on the first day:
Be sure to cover these subjects before there is even a minute of work done.
?Where do I park my car?
?Where do we each lunch? Where do I keep my lunch? When do we have breaks?
?Is there a staff locker room and shower? Where is the bathroom?
?Where do I report for work? Who is my immediate supervisor? With whom do I work? Who else works on the farm?
?Where are time cards kept? When are they turned in? From whom do I receive my paycheck and when?
?If I am unable to come to work, who do I call?
?What are expectations of language, conduct, dress, safety practices, and locations of safety equipment?
?Philosophy and expectations of animal care.
What to talk about later:
?Employee evaluation
?Discipline and grievances
?Expected behavior and conduct
?Business goals of the farm (and how the employee fits in)
?Values the farm holds
Who should conduct orientation?Orientation doesn’t have to fall entirely on the farm owner. The owner or key manager should conduct the first part of ori­entation, but further stages may be turned over to a trusted, experienced employee. That gives the new person someone with whom they can develop a relationship and can go with questions during the early stages of their employment. If a new employee feels safer asking questions that may be important to them and the farm, it can prevent later mistakes or potentially costly situations.
As new employees get to know the rest of the staff, they can all be a part of on-going orientation. It will help build the team on the farm.
Key to successLook for ways to get new employees engaged as quickly as pos­sible in the farm. The sooner they don’t feel like an outsider, the sooner they will be high-performing employees.
Orientation -- it’s a tall order for a fast start, but its rewards can be high. It has been found that getting an employee successfully through the first two years greatly enhances the likelihood of them being a long-term employee. Nurturing the new employee in their first two years is just another part of building a successful dairy business. It can pay big dividends in long-term, high-per­forming team members on your farm.