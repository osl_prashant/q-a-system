NAPLES, Fla. - Audit fatigue may be close to ending. Growers and packers participating in food safety training at the Florida Joint Tomato Conference heard some good news about the ending multiple audits.
During the Sept. 5 session, Reggie Brown, manager of the Maitland-based Florida Tomato Committee and executive vice president of the Florida Tomato Exchange, said a single audit to be in compliance with food safety regulations is close to fruition.
"We are on the verge of making some of the most productive progress we have seen in a long time," he said.
Led by the Washington, D.C.-based United Fresh Produce Association, the industry has worked to develop a single, harmonized audit to satisfy numerous buyers' food safety certification requirements.
The Global Food Safety Initiative-benchmarked audit has been approved by GlobalG.A.P. and should be approved by other organizations, said Dave Gombas, United Fresh's retired senior vice president, who also spoke at the session.
"I am delighted to tell you I believe we are there," Brown said. "I was informed four days ago the harmonized audit that a lot of energy was spent on, the national platform of a single harmonized audit, has certification now."
Such an audit is acceptable to foodservice buyers and retailers including Wal-Mart.
Costco, however, is one exception, Brown said.
A partnership between Florida's tomato industry and the Florida Department of Agriculture and Consumer Services will allow the agency to conduct GlobalG.A.P. audits.
"We are on the verge of being able to have you schedule audits with FDACS' fruit and vegetable inspection people to get a single audit that will comply with all your audit needs," Brown said. "We encourage you to step up and use that platform to accomplish that to ensure your resources in food safety are not spent in doing multiple audits for multiple customers."
T-GAPs and T-BMPS, tomato industry-recommended rules passed into law by Florida lawmakers, require inspections, good agricultural practices and best management practices.
The audits would meet requirements of the harmonized audit, Brown said.