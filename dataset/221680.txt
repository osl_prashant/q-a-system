Animal care starts with having confidence in facilities and ventilation systems that safeguard health in all weather conditions. Testing emergency backup equipment at the start of a new year is a good preventative practice. This prepares the farm for uncertain weather events and allows caregivers to quickly respond, thus protecting pigs from potential negative effects by keeping them comfortable, fed, and watered.
Testing & Recordkeeping Considerations
January has brought consecutive days of below freezing temperatures. These temperatures may result in farms utilizing backup generators to keep ventilation, electricity, or water flowing throughout the barns.
Whenever backup equipment is used, it is helpful to have a written record (Table 1) that includes: the date, whom started it, time of day, the equipment used, and any helpful comments with repairs or maintenance needed. 



Table 1. Example record sheet for emergency backup equipment.


Date
			(MM/DD/YY)
Caretaker 
			Name/Initials
Time of Day
Comments


01/01/18
AB
11:00 am
Generator used for 3 hrs during power outage; consider oil and filter change.



 
Why should this information be recorded?
Records demonstrating that emergency backup equipment for the ventilation system has been tested or maintenance performed should be documented at least twice a year as part of the Pork Quality Assurance (PQA) Site Assessment criteria. There are two questions in the Records and Facility sections of the PQA Site Assessment report that address this topic.

Question 15: “Is there a written record of emergency backup equipment being periodically tested?”
Question 31: “Does the site have an operational emergency backup system?”

Creating and maintaining equipment records
The PQA Plus manual has example record sheets to use for creating personalized log sheets for emergency backup equipment. For more information on emergency backup ventilation system testing and other emergency action plan criteria in the PQA Site Assessment, refer to the PQA Site Assessment Guide or contact a PQA Advisor.
To keep the barn organized, have either one record sheet per piece of backup equipment or one log for all pieces of equipment. If electronic recordkeeping is preferred, consider setting calendar reminders to notify managers when the next system test is scheduled. Many barns have computerized emergency backup systems that automatically perform tests. In this case, make sure to have a report that can be printed or easily shared during a PQA Site Assessment to verify the system is operational.

In Summary
All caregivers in a barn should be aware of how to check and use emergency backup equipment along with where the records for them are located. Review manufacturer’s recommendations for scheduled services to the various equipment. Take time this New Year to revisit your emergency backup protocols and talk with all caregivers about how to handle various weather emergencies that impact the functionality of the barn.
If you have already had to use your emergency backup equipment this year, write it down. If you have been fortunate enough to not have used it yet, consider conducting a test of the system. Pigs rely on their caregivers to ensure their needs are met each day, regardless of the weather or uncertainties that impact the conditions inside the barns.