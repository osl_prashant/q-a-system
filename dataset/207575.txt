In July, Basel, Switzerland-based Syngenta marked 150 years in the vegetable seed business. 
Sluis and Groot, now owned by Syngenta, was founded in Holland in 1867 as a cabbage seed exporter, according to a news release. Syngenta today offers 2,500 varieties of seeds within 30 crop species, according to the release.
 
“We are very proud of our long heritage in vegetable seeds,” Matthew Johnston, head of vegetable seeds and flowers for Syngenta, said in the release. 
 
“For generations, we have been innovating to meet the needs of growers across the world. Today, more than ever, we are focused on bringing this innovation to our customers and to the entire value chain to help meet the consumer demand for high quality, tasty, nutritious vegetables available every day of the year.”
 
Syngenta has vegetable seed research and development facilities in California, Washington, Wisconsin, New York, Minnesota and Florida, and 2,400 worldwide employees dedicated to its vegetable seed business, according to the release.