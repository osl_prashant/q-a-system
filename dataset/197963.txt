Crop calls
Corn: 1 to 2 cents firmer
Soybeans: 1 to  2 cents firmer
Wheat: Fractionally to 1 cent firmer
Crop futures benefited from light short-covering overnight following yesterday's fund-driven selloff. Soybean futures initially had a negative reaction to news Moody's Investors Service downgraded its rating on China's debt. Meanwhile, traders aren't overly concerned about recent wet weather delaying planting, although heavy rains are in the forecast for Indiana and Ohio the next several days. Soybean traders are also noting some concern about heavy rains in western areas of Canada delaying canola planting.
 
Livestock calls
Cattle: Lower
Hogs: Higher
While beef prices dropped yesterday, pork cutout values rose $1.45 on movement of 401.97 loads to signal retailers have switched their focus. This bodes well for the cash hog market and is expected to lift hog futures this morning. Cattle traders will be monitoring the midmorning online auction for cash direction. The discount nearby live cattle futures hold to last week's cash trade clearly signals traders believe the cash market has topped.