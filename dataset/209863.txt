Members of the Ontario Berry Growers Association are the newest users of Croptracker, a traceability software for the agricultural industry.
Croptracker was selected after working with the organization during a yearlong process to develop berry crop processes and strategies, according to a news release.
Berry growers can use this software to track employee labor and payout calculations, the release said, as well as manage and adjust piecework rates.
“Croptracker is a very intuitive program that provides growers with food safety traceability and so much more. It is an Ontario product that understands the needs of growers,” Kevin Schooley, executive director of the association, said in the release.
The program is free to members of the association.