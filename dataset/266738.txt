Dow dives 600 points as China puts tariffs on US goods
Dow dives 600 points as China puts tariffs on US goods

By MARLEY JAYAP Markets Writer
The Associated Press

NEW YORK




NEW YORK (AP) — U.S. stocks are tumbling Monday after China officially raised import duties on U.S. pork, apples and other products. It's a fairly small move but investors are worried it could be step toward a trade war that harms global commerce and company profits.
Meat producer Tyson Foods is among the biggest losers on Wall Street. Investors are also dumping some of their recent favorites, including technology companies like Microsoft, and Amazon, the target of numerous critical tweets from President Donald Trump over the last few days.
KEEPING SCORE: The Standard & Poor's 500 index skidded 71 points, or 2.7 percent, to 2,569 as of 1:36 p.m. Eastern time. The Dow Jones industrial average lost 623 points, or 2.6 percent, to 23,480. The Nasdaq composite slumped 212 points, or 3 percent, to 6,850. The Russell 2000 index of smaller-company stocks fell 34 points, or 2.3 percent, to 1,494.
U.S. markets were closed Friday for the Good Friday holiday. The benchmark index lost 1.2 percent in the first quarter of 2018 following nine straight quarters of gains.
TRADE FEARS: China raised import duties on a $3 billion list of U.S. goods in response to U.S. tariffs on imported steel and aluminum. Tyson Foods slumped $4.42, or 6 percent, to $68.77.
A bigger dispute looms over Trump's approval of possible higher duties on Chinese goods. There are a number of points of contention between China and Washington, Europe and Japan over a state-led economic model they complain hampers market access, protects Chinese companies and subsidizes exports in violation of Beijing's free-trade commitments. Meanwhile the U.S., Canada and Mexico continue to hold talks about potential changes to NAFTA.
The price of gold climbed 1.2 percent to $1,343.60 an ounce and silver jumped 2 percent to $16.60 an ounce as some investors took money out of stocks and looked for safer investments.
THE QUOTE: After a month of public negotiations between the U.S. and several other countries, Monday marked the first time another country has formally placed tariffs on U.S. goods in response to the Trump administration's recent trade sanctions. Kate Warne, an investment strategist for Edward Jones, said the step by China is small but significant.
"The fact that a country has actually raised tariffs in retaliation is an important step in the wrong direction," she said. "The tariffs imposed by China today lead to greater worries that we will see escalating tariffs and the possibility of a much bigger impact than investors were anticipating last week. And that could be true for Mexico as well as for China."
PRIME TARGET: Amazon fell another $70.84, or 4.9 percent, to $1,376.50. After peaking at almost $1,600 a share last month, Amazon has slumped with the market recently. Trump repeatedly criticized the company of late over issues including taxes and Amazon's shipping deals with the U.S. Postal Service. Much of Trump's criticism has come after unfavorable reporting in The Washington Post, which is owned by Amazon founder Jeff Bezos but is a separate company from Amazon.
Warne, of Edward Jones, said investors are being cautious for now, but it's not clear if anything will come of Trump's badmouthing the company.
"There isn't an agency that goes through Trump's tweets and acts on them," she said.
Despite its recent losses, Amazon stock is still up about 18 percent in 2018. It wasn't the only market favorite to fall out of favor Monday. Microsoft dropped $2.97, or 3.3 percent, to $88.30 and Google's parent company, Alphabet, shed $31.13, or 3 percent, to $1,006.01. Boeing slid $8.25, or 2.5 percent, to $319.63.
WALMART GOES SHOPPING? Health insurer Humana rose following continued reports Walmart could buy the company or create a new partnership with it. The Wall Street Journal reported on the possible deal last week. Humana is a major provider of Medicare Advantage coverage for people age 65 and older. Humana gained $10.99, or 4.1 percent, to $279.82 and Walmart slid $3.47, or 3.9 percent, to $85.50.
Walmart has declined to comment.
TESLA SLOWS: Tesla stock declined after the electric car maker said Friday that the vehicle in a fatal crash last week in California was operating on Autopilot mode, making it the latest accident to involve a semi-autonomous vehicle. Earlier this month, a self-driving Volvo SUV being tested by ride-hailing service Uber struck and killed a pedestrian in Arizona.
Tesla fell $13, or 4.9 percent, to $253.13. Nvidia, a chipmaker that reportedly stopped its own work on products for semi-autonomous cars after the recent incidents, lost $9.17, or 4 percent, to $222.42.
BONDS: Bond prices recovered after an early dip. The yield on the 10-year Treasury note stayed at 2.74 percent after a sharp decline last week.
COMMODITIES: Benchmark U.S. crude lost $1.71, or 2.7 percent, to $63.23 a barrel in New York. Brent crude, used to price international oils, slid $1.33, or 1.9 percent, to $68.01 a barrel in London.
Copper rose 2 cent to $3.05 a pound.
CURRENCIES: The dollar declined to 105.99 yen from 106.50 yen. The euro dipped to $1.2288 from $1.2306.
OVERSEAS: Trading in France, Germany and Britain was closed for Easter. Japan's benchmark Nikkei 225 lost 0.3 percent and South Korea's Kospi fell almost 0.1 percent. The Hang Seng in Hong Kong was closed as well.
____
AP Markets Writer Marley Jay can be reached at http://twitter.com/MarleyJayAP . His work can be found at https://apnews.com/search/marley%20jay