Conditions were back to normal March 5 after a storm March 2 created high water at the New England Produce Center.
Water reached the height of car doors on March 2 and then again March 3 after a winter storm struck the Boston area with wind and rains.
The New England Produce Center never lost power, said Rich Maggio, representative with Gold Bell Inc., Chelsea, Mass.
“We had a big Nor’easter (March 2) but it was warm enough that there was no snow and we kept going,” Maggio said March 5.
“We left a little early Friday but were back in Saturday (March 3) and we are back to normal today,” he said.
The rains in March followed flooding in January when high waters caused the market to close early on Jan. 4.
The National Weather Service said that Boston may experience another potential significant winter storm on March 7.