Editorials from around New York
Editorials from around New York

By The Associated Press
The Associated Press



Recent editorials of statewide and national interest from New York's newspapers:
___
The Rome Sentinel on safety and self-driving cars
March 30
Emerging technologies often come with some risk, particularly in the mobility sector. Early airplanes fell from the sky far more often than they do today. The first trains frequently jumped their tracks. And the Model T had a tendency to run off the road.
It is to be expected that pioneering self-driving vehicles will suffer similar mishaps as they are tested in real world conditions.
And that happened earlier this week in Tempe, Arizona, where an autonomous car operated by the Uber ride sharing company struck and killed a pedestrian. The victim, who was crossing the street with her bicycle, was hit by the driverless vehicle, which was in autonomous mode.
Police are still investigating, and they say it is possible the death may not have been preventable even with a real driver behind the wheel.
The woman was apparently outside the designated crosswalk when she was hit.
Still, the fatal accident has heightened concerns about testing autonomous vehicles on streets and highways that are in everyday use. And it highlights the need for uniform regulations to assure that the development of the technology does not endanger public safety.
Currently, there is no federal framework to regulate self-driving car safety. Congress should catch up with the rapid pace at which these vehicles are being developed and placed on the road for testing.
Michigan's Democratic Sen. Gary Peters is pushing a bill that would apply some basic safety standards to autonomous vehicles before they can be tested on public roads.
His bill would require manufacturers to submit safety evaluation reports to the secretary of transportation addressing how well the vehicles performed on tests of crash worthiness, cybersecurity and other measures.
It would also establish a committee of expert to establish the testing standards and how the test data will be shared throughout the industry.
The bill is a good start toward bringing some order to the testing of self-driving vehicles without overly restricting the ability of manufacturers to live test their products.
Eventually, autonomous vehicles should greatly reduce traffic fatalities and injuries. That is a major reason for developing them. It's an exciting technology, and bringing it to market offers tangible societal benefits.
Online: https://bit.ly/2ItgnzJ
___
The New York Daily News on the New York City Housing Authority
April 3
Hundreds of thousands of public housing residents who just endured a frigid winter of hell are ever-so-cautiously tiptoeing into a spring of hope — hope that reliable heat and hot water and better living conditions are at long last on their way.
Along with a too-long answer to the NYCHA version of an age-old joke: How many government officials does it take to replace a broken boiler? How about dozens of broken boilers and leaky roofs and countless moldy walls?
Start with Gov. Cuomo, who, bless him, insisted on adding $250 million for NYCHA fixes to the newly passed state budget along with fast-track design-build construction authority. He also signed an executive order Monday to get heating and other systems repaired via private construction management, with expedited contractors to be ready to roll in a mere three months.
More power to him, because under sluggish, bureaucracy-cowed Mayor de Blasio and NYCHA chairwoman Shola Olatoye, progress has been positively glacial.
In only a slight stretch, Cuomo is getting it done by declaring the Housing Authority's conditions to be an imminent disaster on the order of a hurricane or terror attack. Close enough, given the risk to public health of fraudulent lead-paint inspections that left kids at risk of poisoning, mold that leaves asthmatics wheezing and cold that pierces residents young, old and frail.
Add to the lightbulb-repair brigade Council Speaker Corey Johnson, who with NYCHA tenant leaders suing for fixes will help decide on a construction manager for the new funds and $300 million previously budgeted by the state. Don't forget Councilman Ritchie Torres, whose relentless interrogation of Olatoye exposed the vast extent of her failures on both heat and lead paint.
Enter Deputy Mayor Alicia Glen, who, under heat from the state, rushed over a list of 14 NYCHA developments whose 63 boilers and associated heating systems will cost $247 million to fix.
And de Blasio, who's technically in charge but doing far too little to act like it. Cuomo now invites Big Bill to join Johnson and the tenants to select a manager — unless they can't agree, in which case City Controller Scott Stringer will step in.
But wait — here come the feds.
For nearly three years and three holders of the office, the Manhattan U.S. Attorney has been investigating false reporting by NYCHA to the U.S. Department of Housing and Urban Development on its handling of mold and lead paint. A report, and likely a monitor, are in the offing any day now.
And last month, HUD Secretary Ben Carson informed Olatoye that she would have to get Washington's pre-approval on all expenditures of federal fix-it funds.
That's a whole lot of hands who collectively, finally managed to elevate the NYCHA crisis to code red at a time when Olatoye and de Blasio dug in to deep and destructive denial. While, let's face it, elevating their political profiles.
Enough jockeying for position by political pros. Now, let the plumbers and steamfitters and painters and engineers and the rest of the people whom the residents are counting on get to work.
Online: https://nydn.us/2H7MC84
___
The (Utica) Observer-Dispatch on supporting local farmers
April 3
Career opportunities abound today for young people. They can become accountants, scientists, engineers, environmentalists, bankers, mechanics, carpenters, electricians ...
Or they could be a little of each and become a farmer.
Farmers wear all of those hats and more. Yet given the skills they need, the investments they make, the demands on their time and the valuable services they provide, regulations can handcuff them.
Nevertheless, David Fisher says he wouldn't want to do anything else.
Fisher, a fourth generation dairy farmer from St. Lawrence County, is president of the New York Farm Bureau. It's because of people like him that we, the consuming public, need to pay close attention to what our farmers do and insist that government not milk them dry with a regulating system that makes it difficult for them to do what is already a very tough job.
Without dedicated farmers, we're all in trouble.
During a visit with the Observer-Dispatch Editorial Board recently, Fisher discussed any number of obstacles facing farmers today, not the least of which are labor costs. The regulations can be ridiculous.
For instance, Oneida County Farm Bureau Vice President Mike Candella, a third-generation vegetable producer in Marcy, said he is required to pay unemployment insurance on the seasonal workers he hires from Guatemala. But when those workers return to that country following the harvest, they never collect the benefits. Somebody's making a good buck off that — and it's not the farmer or the worker.
Fisher said having seven family members to run his dairy operation in northern New York helps, but he feels for smaller operations that can't rely on such numbers.
"I can't imagine a young person starting up (in the dairy farming industry)," said Kevin Angell, who family has operated a dairy farm in Durhamville for six generations. "If (milk's) not at a viable price, then how are you going to get young people interested in this? And if you don't have young people interested in this, then what's going to happen to these small farms?"
That's one of the issues Oneida County hopes to deal with in the near future. In his recent State of the County message, County Executive Anthony Picente noted that agriculture is the largest industry in the county, and while it has made progress in some areas, serious issues remain. Farmers are aging out — more than half are over the age of 55 — and finding young people to take their place is a challenge. Programs are being put in place to reach out to young people, but unless other regulations change, encouraging newcomers might be difficult.
Dairy farmers, Picente said, face obstacles such as frozen credit from feed and seed producers as well as difficulty getting loans and financing. A forum planned this year with Cornell Cooperative Extension hopes to address some of these issues and create an agenda to advocate on dairy farmers' behalf to representatives in Albany and Washington, Picente said.
And the whole issue of milk pricing is another farming bugaboo. Unlike most producers of goods, farmers have little say in the price they get for their product because milk pricing is federally regulated by the U.S. Department of Agriculture. It's a complex formula based on numerous variables, such as how processors will sell butter, cheese and powdered milk, that determines the price.
Right now is not a good time for dairy farmers, Fisher said, because there's too much milk in a market and not enough people to buy it. That's another dilemma. In many operations, when business is good, producers reinvest to increase production; in the dairy business, that can turn sour when the investment fails to pay off.
In the meantime, what can you do?
First, talk to government representatives who might be able to influence farm policy. Lowering the cost of doing business in New York state would be a good start.
Next, consumers should educate themselves as to where their food comes from. Jeff Williams, the Farm Bureau's public policy director, says most of us take food for granted and sometimes are influenced by bad information passed around on social media. Talk to farmers, he said, and get it straight. You can do that most every week at area farmers' markets.
Finally, support farming initiatives. Remember that it's a larger industry than appears. When we lose our farms it not only hurts those in the direct line of fire — farmers — but it ripples throughout the regional and state economy. Equipment businesses, mechanics, fuel and grain suppliers and veterinary practices all depend on local farms.
Most of all, so do we.
Online: https://bit.ly/2JlZ8lk
___
The Press Republican on low turnout in the special election
April 3
The story was disheartening to responsible citizens — almost unbelievable, except that it reflected something of what we have all known for all too long to be all too true: A sorry majority of people in the North Country don't take their right to vote seriously.
Two residents of Clinton County Legislature's Area 3 sought to pick up the torch left by Sam Dyer when he vacated his post Jan. 2 representing that district to assume the office of Beekmantown town supervisor.
Republican Mark Henry and Democrat Jerry Marking answered the call of public service and agreed to run for the vacant spot. Clinton County government is overseen by 10 legislators who make critical decisions weekly and sometimes more often to create smoother, easier and less burdensome lives for all 81,215 residents.
Running for legislator is difficult enough. It requires committing lots of time away from candidates' families, hobbies and interests in order to learn what to do and how to do it. It also requires knocking on many, many doors, introducing oneself to potential voters and constituents and eliciting from them what their concerns are about how their government is conducted.
But running is not even the hardest part. If elected, the candidate-turned-legislator must then commit to twice-monthly meetings of the full legislature, service on various committees requiring still more meetings, taking phone calls at all times of day and night from constituents who may want all kinds of information or may want to make all manner of demands or complaints.
And, for that, the compensation is a somewhat measly $17,500 a year ($20,000 for the job of chairman). It's hardly adequate compensation for all the time and work that must be devoted to the job.
The outcome of the Henry-Marking election was too close to call on election night; they were a scant 10 votes apart. The final result will hopefully be known after the absentee ballots are counted today.
The appalling factor here is that fewer than 20 percent of eligible voters bothered to even turn out to vote. That other 81 percent should be ashamed of themselves.
Does the right, the privilege of casting a ballot mean so little to them that it was too much trouble to travel to a polling place and spend 30 seconds or so filling out a ballot?
We read and hear these days about the sham election that recently kept Vladimir Putin in office in Russia. Yet our right to participate in a legitimate local election nevertheless means so little to us that only slightly more than 19 percent make the time and expend the effort.
To the almost 1,000 people who voted, congratulations and thanks.
And, to Marking and Henry, many, many thanks for what you have offered to do for your friends and neighbors in the towns of Beekmantown and Chazy. It's discouraging and somewhat pathetic, though, that most of them have so little regard for public responsibility — yours and their own.
Online: https://bit.ly/2H8HssG
___
The (Albany) Times Union on President Donald Trump's VA pick
April 1
The health and welfare of millions of veterans rest on what feels more like a casting call.
Donald Trump has long promoted himself as a champion of military veterans, but when he finally had a chance last week as president to demonstrate that they were more than political props for him, he failed miserably.
In the same cavalier way in which he has appointed so many people who have turned out to be disasters in their jobs, Mr. Trump named his White House physician as secretary of veterans affairs. He thus thrust a man with no experience running a big bureaucracy to the helm of the second-largest federal agency. If the Senate goes along with this, Dr. Ronny L. Jackson, a Navy rear admiral, would go from monitoring the president's health to overseeing a department of more than 366,000 employees.
By all accounts, Dr. Jackson is a nice guy. But this is as absurd as taking a pleasant cashier off the register to be CEO of McDonalds, or plucking a friendly delivery boy off his route to run FedEx.
Dr. Jackson's main qualification to head a department bigger than any branch of the military or the Department of Homeland Security seems to be his talking up of the president's health in January, when he credited Mr. Trump with what he assumed must be "incredibly good genes." Mr. Trump could hardly have been more bombastic himself.
All this might be delightfully surprising in a reality TV show sort of way were the stakes not so high. The VA's mission is not one to take lightly. It's responsible for the health care of more than 9 million veterans served by its 145 hospitals, 300 vet centers, and more than 1,200 outpatient sites. It also handles disability and education benefits, home loans, life insurance, pensions and vocational rehabilitation for millions of vets. Choosing a leader for it should not be treated like a casting call.
Dr. Jackson would succeed David J. Shulkin, who was trying to reform a department in need of modernization and marred by scandal over long waiting lists. Dr. Shulkin, however, had a scandal of his own stemming from his overseas travel, giving Mr. Trump a ready excuse to fire him.
There are concerns that this is part of a conservative small-government quest to end the VA as veterans have known it and privatize its work. A lack of competence at the top would no doubt bolster the case for a radical overhaul. That, however, is not something that should be done in haste or on ideological whim any more than the pick for the secretary of veterans affairs should be the last person to check the president's vitals.
Mr. Trump, who never served in the military thanks to five deferments, may see this all as part of the chaos on which he seems to thrive, and which has left his administration with more high-level turnover in 14 months than some presidents have had in entire terms. But the veterans he claims to cherish don't need what passes for excitement in Mr. Trump's life. They've had quite enough, thank you. That's the message the Senate should send back to Mr. Trump if he doesn't rethink this ridiculous appointment.
Online: https://bit.ly/2Ek7sxX