Grain mostly higher and livestock lower
Grain mostly higher and livestock lower

The Associated Press



Wheat for March was up .75 cent at 4.8975 a bushel; March corn rose 1.25 cents at 3.8425 a bushel; March oats was fell .75 cent at $2.5875 a bushel; while March soybeans gained 4 cents at $10.3325 a bushel.
Beef and pork wer lower the Chicago Mercantile Exchange April live cattle was off 1.57 cents at $1.2155 a pound; March feeder cattle fell .35 cents at $1.4217 a pound; while April lean hogs lost .18 cent at $.6767 a pound.