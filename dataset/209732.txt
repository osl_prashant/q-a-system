Livestock producers in the Texas Panhandle were put on notice after several reports of fumonisin contamination in recently harvested grain. More than 700 farmers, insurance adjusters and end-users attended two emergency meetings in Dimmitt and Dumas, Texas, last week, as leaders from Texas AgriLife Extension Service, USDA Risk Management Association, Office of the Texas State Chemist, and Texas A&M Veterinary Medical Diagnostic Laboratory shared the warning signs and risks. Watch the video above for more coverage from Clinton Griffiths on AgDay.
More than 50% of the corn grown in Texas comes from the High Plains region. The primary market for this corn is the cattle feeding industry, and according to the latest “The Impact of Agribusiness Texas High Plains,” it accounts for about $635 million in annual sales in the region. 
 “We’re seeing variably infected corn fields in the Panhandle,” says Dr. Cat Barr, Texas A&M Veterinary Medical Diagnostic Laboratory toxicologist. “This means field to field and kernel to kernel.”
So far, only the early maturing corn has been harvested. More than 60% of the crop is still in the field. Grain elevators are required to test each load. While elevators conduct a quick test that measures fumonisin levels above or below 30 ppm, there are only two certified testing facilities in the High Plains at this time – the Amarillo Grain Exchange and Plainview Grain Inspection. More labs are expected to open this week.
Fumonisins are toxins produced by two species of Fusarium fungi, according to Dr. Tom Isakeit, Texas AgriLife. Not all molds growing on ears produce mycotoxins. Visual assessments cannot diagnose if the toxin is present. Which means every load of corn will require testing and, potentially, blending.
Elevators are working with State officials to formulate blending plans, Barr says. Acceptable levels for livestock feeders:

Horses, up to 5 parts per million (ppm) fed as 20% or less in diet
Hogs, up to 20 ppm fed as 50% or less in diet
Ruminants older than 3 months of age, fed for slaughter, 60 ppm, fed as 50% or less in diet
Breeding ruminants, up to 30 ppm fed as 50% or less in diet
Poultry raised for slaughter, up to 100 ppm fed as 50% or less in diet

Click here for clinical signs of livestock illness.
“The good thing about fumonisin is it doesn’t increase in storage,” Barr adds. “The levels will not decrease in storage, but it won’t increase.  We’ll just need to make sure our horses don’t get into the cattle feed from this harvest.”