Tomato growers aren’t complaining about the weather in California and Mexico’s Baja California, but they say the markets could be better.
“We’ve been through, really, six terrible months of tomato markets, out of the East, West and Mexico, said Joe Bernardi, president of San Diego-based Bernardi & Associates, which grows tomatoes in five regions of the U.S., including Southern and Northern California. “The whole deal all the way around has been depressed tomato markets for six months,” he said.
But things may be looking up, as the spring and summer deal in Baja heats up, Bernardi said.
“Florida has seen lighter supplies, and the price has jumped. Nogales is finishing up,” he said. “Overall, in pretty much all the tomato varieties, we’re seeing the highest prices we’ve seen in six months.”
Short supplies are pushing prices up, and supplies may remain light for a while, Bernardi said.
“I anticipate short supplies for the next three to four weeks,” he said May 11. “Florida won’t have any product; Baja won’t be a big factor for the month of May; and Northern California won’t be getting started until, probably, mid-June.”
A prolonged spate of wet weather through late winter and early spring pushed some plantings back, so some volume will be later than usual this year, Bernardi said.
“We’ve had people start the last couple of years as early as June 1, and the typical start is around June 10,” he said. “This year, it will be more like June 19.”
The wet weather has created some problems, but overall it’s been a blessing, Bernardi said.
“It’s been rough, but much-needed,” he said. “This year, Northern California will be irrigating with clean, good fresh water, and that should increase yields. Overall, the underground aquifers are resupplied. We have a huge snowpack that’s ready to come down and keep our reservoirs filled. It was much-, much-needed and, hopefully, we’re in store for another winter like it this year.”
Adequate moisture is the top priority, agreed Tom Frudden, sales manager with Firebaugh, Calif.-based Red Rooster Sales, which grows tomatoes primarily in California’s San Joaquin Valley, with a one-month deal in September out of Hollister, Calif.
“I guess the No. 1 thing is we’re so excited to basically be out of the drought. That’s a big deal,” he said. “The last four years — especially the last two — we had to rely on well water.”
Wells get the job done, but nothing compares to fresh water that flows from the mountains, Frudden said.
“Although well water will grow tomatoes, what comes up from those wells is quite different from what comes out of that snowpack. There’s nothing like that runoff,” he said. “I’m feeling really good about conditions this year. I truly believe it’s going to make a difference in quality.”
Frudden said he was anticipating harvesting tomatoes in the San Joaquin Valley by June 19-20.
“We would be very fortunate to start by June 15, at the absolute earliest,” he said. “The rains and cool weather have set us back.”
As a result, tomato volume through June likely will be comparatively light for the industry, Frudden said.
“You might see a few guys pop up their heads the week of (June 26),” he said. “With the rains, the farther north you go, the wetter it was. It has a lot of heavy clay-based soil, and when that gets wet, it just takes a long time to dry out.”
Normal volumes probably won’t be moving until around July 10, Frudden said.
The Coachella, Calif., deal started May 8, said Jeff Dolan, field operations manager with the Newman, Calif.-based DiMare Co.
“It looks good,” he said. “I’m standing in the field and pleasantly surprised. Weather is absolutely fantastic.”
Rains that hit other areas of the state didn’t touch the Coachella Valley, Dolan said.
“Down here, no problems at all,” he said.
San Diego-based Expo Fresh LLC had not planted rounds nor harvested romas, said Steve Harsh, salesman.
“We hope to have a good crop, but it’s too early,” he said.
The romas, rounds, cherry and grape tomatoes in Baja were looking good, said Tony Mandel, sales manager at San Diego-based Pinos Produce Inc.
“Everything we have, the quality is good,” he said.
All of Pinos’ tomatoes are grown either in greenhouses or shadehouses, Mandel said.
“That helps,” he said. “We’re looking for a good crop.”