AP-UT--Utah News Digest-  6 pm, UT
AP-UT--Utah News Digest-  6 pm, UT

The Associated Press



Good evening. Here's an updated look at how AP's general news coverage is shaping up in Utah.
Questions about today's coverage plans are welcome and should be directed to Brady McCombs at 801-322-3405 or apsaltlake@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date. All times are Mountain.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
UPCOMING FRIDAY:
SCHOOL SAFETY-UTAH
SALT LAKE CITY — The Utah State Board of Education is set to discuss Friday what measures are in place to keep schools safe and what more could be done just days after thousands of students walked out and demanded safer campuses amid a climate of fear after the deadly Florida school shooting. By Brady McCombs. UPCOMING: 500 words following 1:30 p.m. MT meeting.
TOP STORIES:
SENATE-UTAH-ROMNEY
SALT LAKE CITY — Eleven Republicans have stepped forward before a Thursday evening filing deadline to challenge former Republican presidential nominee Mitt Romney in the Utah Senate race, including a state lawmaker who says an "establishment insider" can't fix problems in Washington. By Michelle L. Price. SENT: 680 words.
POLYGAMOUS TOWNS-CHILD LABOR
SALT LAKE CITY — A Utah contractor with ties to a polygamous group must pay $200,000 in back wages to children who were forced to pick pecans for long hours in the cold without pay, after a U.S. appeals court upheld a ruling on this week. By Brady McCombs. SENT: 510 words.
SNOWMOBILER RESCUED
SALT LAKE CITY — A snowmobiler who spent four nights alone in freezing temperatures in the Utah wilderness without food or water said he didn't think he would survive.  David Hales said Wednesday from a hospital in Heber City that he trekked about seven miles (11 kilometers) on his hands and knees before he was rescued Tuesday. SENT: 370 words.
GREAT OUTDOORS BANDIT
BOISE, Idaho — A serial bank robber in Idaho and Utah nicknamed the "Great Outdoors Bandit" has been sentenced to more than five years in prison and ordered to pay $31,000 in restitution. Witnesses described him as outdoorsy. SENT: 370 words.
REFERS:
WESTERN INVADERS
DENVER — Weeds, feral cats, insects and other pests are invading the U.S. West, and state governors released a list of the worst offenders Thursday in hopes of helping people recognize and eradicate the invaders before they spread. The Western Governors' Association cataloged the top 50 invasive species in their region, saying the pests have already caused billions of dollars in damage to agriculture and infrastructure. SENT: 500 words.
GUNS-REALITY CHECK
IOWA CITY, Iowa — The campaign for tighter gun laws that inspired unprecedented student walkouts across the country still faces an uphill climb in a majority of states, an Associated Press review of gun legislation found. The AP survey of bill activity in state legislatures before and after the Parkland, Florida, school shooting provides a reality check on the ambitions of the "Enough is Enough" movement. It suggests that votes like the one in Florida, where Republican lawmakers defied the National Rifle Association to pass new gun regulations, are unlikely to be repeated in many other states, at least not this year. By Ryan J. Foley. SENT: 970 words, with    AP Photos NY414, NY413, NY412, NY411, NY414, NY411, RPWR201, VTBUR202, NY413, NY412.
With:
STUDENT WALKOUTS-GUN VIOLENCE
UNDATED — They bowed their heads in honor of the dead. They carried signs with messages like "Never again" and "Am I next?" They railed against the National Rifle Association and the politicians who support it. And over and over, they repeated the message: Enough is enough. By Collin Binkley. SENT: 1,200 words, with AP Photos PAPIT500, NY414, ORAST401, NMSAN104, NY412, COGYP101, WYCHE107, GAAUG400, NMSAN103, NMSAN101, WVHUN104, WVHUN102, WVHUN106, PAPIT501, PAPIT502, PAPIT503, INMIC101, WX202.
GUN CONTROL-ECHOES OF CAPONE
CHICAGO — It was 1934. Mobsters armed with fully automatic "Tommy guns" had left a trail of bloodstained sidewalks and pockmarked walls across the country, and the new president had narrowly escaped assassination the year before. It was time for action on gun control. And the National Rifle Association seemingly agreed. "I do not believe in the general promiscuous toting of guns," then-NRA President Karl T. Frederick told members of the House Ways and Means Committee. "I think it should be sharply restricted and only under licenses." By National Writers Allen G. Breed and Sharon Cohen. SENT: 970 words, with AP Photos RPAB102, RPAB100, RPAB103, RPAB104, RPAB101.
ACLU-IMMIGRATION LAWSUIT
WASHINGTON — The American Civil Liberties Union is suing the Trump administration to stop it from detaining immigrants who have a solid case for seeking asylum in the United States. By Ashraf Khalil. SENT: 130 words. Developing.
With:
IMMIGRATION-COURTHOUSE ARRESTS
BOSTON — Civil rights and indigent defense groups asked Massachusetts' highest court Thursday to stop federal agents from arresting immigrants targeted for deportation at courthouses, saying the practice is scaring victims, witnesses and others away from halls of justice. By Alanna Durkin Richer. SENT: 670 words.
OBIT-DAVID WYMAN
NEW YORK — David S. Wyman, a leading scholar of the U.S. response to the Holocaust whose "The Abandonment of the Jews" was a provocative, best-selling critique of everyone from religious leaders to President Franklin Roosevelt, died Wednesday at age 89. By National Writer Hillel Italie. SENT: 910 words, with AP Photo NYET406.
SPORTS:
BKN-SUNS-JAZZ
SALT LAKE CITY — The Utah Jazz take a seven-game winning streak into their game against last-place Phoenix on Thursday night. UPCOMING: 700 words, photos. Game starts at 7 p.m. MT.
SOC-WCUP-NORTH AMERICAN BID
NEW YORK — Chicago, the home of the U.S. Soccer Federation, dropped out of the North American bid to host the 2026 World Cup as 23 cities were chosen to be included in documents to be submitted to FIFA on Friday. Six other U.S. cities were cut: Charlotte, North Carolina; Glendale, Arizona; Las Vegas; Minneapolis; Salt Lake City; and Tampa, Florida. By Sports Writer Ronald Blum. SENT: 830 words.
BKC-NIT-BYU-STANFORD
STANFORD, Calif. — Reid Travis scored 25 points and grabbed 14 rebounds, Michael Humphrey added 11 and 14 and Stanford topped BYU 86-83 on Wednesday night in the first round of the NIT. SENT: 250 words, with AP Photos CATA101, CATA103, CATA102, CATA104, CATA113, CATA112.
BKC-NIT-UC DAVIS-UTAH
SALT LAKE CITY — Justin Bibbins scored 15 of his 21 points in the second half, Utah coach Larry Krystkowiak was ejected in the first half and the Utes overcame an 11-point deficit to beat UC Davis 69-59 on Wednesday night in the first round of the NIT.  SENT: 250 words.
IN BRIEF:
— DAY CARE INJURIES — Jurors began deliberations over whether three toddlers injured in a Utah day care were deliberately harmed by their baby sitter, or if a series of accidents were grouped and labeled a crime.
— WORKER DIES-HEAD INJURY — Authorities say a worker died after a tunnel he was installing at a Geneva Rock operation in Draper fell on him.
— BODY IN POND-FACEBOOK — Ogden police were prompted to pull a body from a pond after a photo of the remains was posted on Facebook.
— NEVADA POACHING-WISCONSIN HUNTERS — Two Wisconsin men who illegally poached a bull elk near the Nevada-Utah line have been sentenced to 10 days in jail and ordered to pay thousands of dollars in fines and penalties.
— GRAND CANYON-WATER RESTRICTIONS — Due to a series of breaks in the Transcanyon Waterline, Grand Canyon National Park has implemented water conservation measures.
__________________
If you have stories of regional or statewide interest, please email them to apsaltlake@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.