Global dairy prices surged in the second auction of the year as buyers anticipated sluggish supply from the world's largest milk exporter, New Zealand.
The Global Dairy Trade Price Index climbed 4.9 percent - the largest gain in more than a year - with an average selling price of $3,310 per tonne, in the auction held early on Wednesday.
The lift was largely on the back of the world's biggest dairy processor, Fonterra , slashing its New Zealand milk collection as the country struggles with unusually dry weather.
The index had risen 2.2 pct at the previous sale, according to GDT Events, snapping a losing streak that had left farmers worried that they would receive a lower payout in 2018.
Nevertheless, analysts cautioned the gains might ease off in coming months given global supply for some products remained strong.
"Poor milk production from NZ should be supporting prices for WMP (whole milk powder) ... as NZ is the key supplier," said Amy Castleton, analyst at AgriHQ.
"But global milk production is continuing to grow – notably that coming out of Europe and the U.S. – so plenty more SMP (skim milk powder), butter and cheese will make its way onto the global market in coming months."
Whole milk powder jumped 5.1 percent at the latest auction, and skim milk powder also posted strong gains of 6.5 percent.
Auction results can affect the New Zealand dollar as the dairy sector generates more than 7 percent of the nation's gross domestic product.
Despite the stellar result, the currency slipped 0.34 percent overnight to $0.7273 as it consolidated after a rally in the past week.
A total of 23,319 tonnes was sold at the latest auction, falling 8.2 percent from the previous one, the auction platform said on its website.
A number of companies, including Dairy America and Murray Goulburn , use the platform to sell milk powder and other dairy products, with roughly half of buyers based in China as traders there seek to supplement flagging domestic milk supplies.
The auctions are held twice a month, with the next one scheduled for Feb. 6.