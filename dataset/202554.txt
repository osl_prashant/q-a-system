I grew up in a family where the general rule in place at the dinner table (yes, we all sat down for dinner together, an alien concept to some of my peers even then) was to eat what was served to us, including vegetables.
 
There was some room for negotiation - "Can I just eat three mushrooms, Mom?" - but picky eating was not really a thing in our home.
 
Something that made my parents' mealtime rule a reasonable expectation was my mom's skill at incorporating produce into meals in tasty ways.
 
Spaghetti sauce included mushrooms, onions and green peppers. 
 
Soups and stews were full of carrots, celery, potatoes and more. Fresh salad made a regular appearance as a side, and broccoli was a staple side dish or casserole ingredient.
 
Granted, I have some memories of swallowing down bites of eggplant parmesan and oh-so-slimy okra, but most of us kids took for granted that vegetables were just as good to eat as everything else served to us.
 
Dessert being reserved for those who polished off their dinner probably also played in to us eating omnivorously.
 
I credit my parents' approach (and excellent cooking) with developing a taste for diverse foods and international flavors.
 
When living in Taiwan and China for several years, I greatly enjoyed all sorts of local food, including eggplant.
 
As a parent myself now, I'd like my kids to grow up thinking lots of fresh produce in meals and snacks is normal.
 
By default, we tend to think that whatever we grew up with in our families or homes is "normal" and everyone else who does things differently is strange, which is why multi-million dollar government and nonprofit efforts to increase per-capita consumption of fruits and vegetables have been an uphill battle.
 
If someone grows up eating a lot of fast food, from-scratch home cooking will probably seem weird. 
 
"Who would go to all that trouble? Nobody eats that stuff!" 
 
If your go-to snack as a kid was a soft drink and Nacho Cheese Doritos, apple slices instead might seem less than exciting.
 
The Packer has frequently reported over the years on the efforts of the Let's Move Salad Bars to Schools project since it began in 2010 in support of former first lady Michelle Obama's Let's Move! Initiative. 
 
With nearly $12.5 million raised so far and more than 4,800 salad bars already donated, I think salad bars in school cafeterias can make a significant difference in "normalizing" fresh produce. 
 
Hey, even the daily exposure to quality fresh fruits and vegetables might help kids entertain the idea that normal people actually eat those things, since a lot of kids have an anti-vegetable stance.
 
Whether for adults or kids, being coaxed and prodded to "eat your veggies" often has the opposite of the intended effect. 
Choking down something you think is nasty because of its purported health benefits is a really sad way to eat, and not terribly effective in the long run.
Being told to do something you don't want to do – for your own good, no less – usually makes you really excited to do it, right? 
 
Not so much. 
 
Choking down something you think is nasty because of its purported health benefits is a really sad way to eat, and not terribly effective in the long run.
 
A Harvard School of Public Health study from 2012 detailed the results of a two-year Chef Initiative pilot program, where a chef developed recipes and menus and trained school cafeteria staff for a year in two Boston schools. 
 
The program's goal was to prepare school lunches that meet nutritional standards and actually taste good.
 
The Harvard researchers evaluated the effectiveness of the program by comparing what students at the two "chef schools" chose, and threw away, with the choices and plate waste of students at two control schools.
 
The results of the small-scale study were encouraging and pretty much what you'd expect: kids will eat tasty food even when it happens to be good for them!
 
"Naysayers to healthy food in schools frequently say that kids won't eat better food. We showed that if it tastes good, they see how the food is made, and see the same foods for a whole school year, they do eat better," Eric Rimm, associate professor in the departments of epidemiology and nutrition in the Harvard School of Public Health and senior author of the study, said in a Harvard report about the study. 
 
"We still have a long way to go, but this is a very positive first step."
 
There obviously is a place for gutting it out and breaking bad eating habits when we're convinced of the need to do so, but I'm more a fan of making healthy food taste great to start with, and that doesn't mean smothering broccoli in cheese sauce or eating a little salad with your ranch dressing. 
 
The world's largest cookbook (the internet) - and the huge increase in time-saving produce packs from the fresh produce industry - offer plenty of options to make fresh produce a normal part of meals that kids will devour. 
 
Daniel Vanderhorst is The Packer's sections editor. E-mail him at dvanderhorst@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.