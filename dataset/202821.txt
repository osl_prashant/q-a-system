Canadian processor Freshline Foods has launched a line of vegetable noodles.
The products are called Veggie Foodles, according to a news release. Available varieties are beet, butternut squash, carrot, green zucchini, root blend, turnip, yam, yellow zucchini and zucchini blend.
Veggie Foodles are available for retail and foodservice.
"They're (a) fantastic way to up your vegetable game," Noel Brigido, vice president of Freshline Foods, said in the release. "The possibilities are endless."