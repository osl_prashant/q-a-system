No matter where avocados are grown, more and more of them are ending up in consumer bags at the supermarket, grower-shippers say, and reusable plastic containers are becoming more popular too.
"We're selling about 2.5 million consumer bags a month," said Rob Wedin, vice president of sales and marketing for Calavo Growers Inc., Santa Paula, Calif.
That figure has remained fairly steady.
"It gave us the confidence level to pack bags not only in California but also in Mexico," he said. "And we pack a lot of bags in Dallas, New Jersey and Florida."
Calavo packs about 15% of its avocados in consumer bags, Wedin estimated.
"We've invested in a lot of bagging operations," he said.
The number of bags packed inches up as volume increases, he said.
A number of bags packed for Calavo in Michoacan, Mexico, are shipped to the U.S. through Laredo, Texas, he said.
"Bags have continued to grow in popularity," agreed Phil Henry, president of Henry Avocado Corp., Escondido, Calif.
Although bags usually are associated with smaller fruit, Henry Avocado will pack any size or fruit count that customers request in bags, Henry said.
Customers who want to offer consumers the best value usually request bags with smaller sizes, he added.
"It's good for retailers to have multiple offerings for their customers," Henry said - bags, loose product and multiple sizes.
Giumarra Agricom International, Escondido, Calif., is excited about its bagged avocado offering, said Gary Caloroso, marketing director.
"We aggressively promote nutrition on our avocado bags," he said, in line with the Hass Avocado Board's Love One Today" program.
"The bag is very popular," he said. "It's done very well."
Giumarra's bags include a healthy avocado recipe, he said.
Sales of bagged avocados continue to grow at Mission Produce Inc., Oxnard, Calif., said Robb Bertels, vice president of sales and marketing.
"It gives consumers an option," he said.
Bags also enable retailers to merchandise avocados in more than one location in the supermarket.
It's nice to have bags with four or five smaller avocados to merchandise with larger bulk fruit, he said.
"They can pick one or two avocados and also grab a bag," he said.
The company packs mesh bags with a header card and strap but may bring something new to the table in the not-too-distant future.
"We're looking at some other options that may be more appealing at retail," Bertels said.
Bags don't seem to be as popular for Florida avocados as they are for the hass variety.
New Limeco LLC, Princeton, Fla., has tried shipping consumer packs for a couple of chains in the past, but though mesh bags with three Florida avocados sold OK, they did not sell as well as loose avocados, said Eddie Caram, general manager.
The company will pack them on request, he said, but has not packed any so far this season.
"This avocado seems to sell better as a loose pack," he said.
Brooks Tropicals LLC, Homestead, Fla., does offer its Florida-grown Slimcados in a mesh bag, said Mary Ostlund, marketing director.
They're packaged four per bag, each weighing just over 1 pound, she said.
Grower-shippers say concerns about bagged product cannibalizing sales of bulk avocados are unfounded.
"Retailers who offer both are giving consumers a choice, so it's offering any opportunity for more sales," Caloroso said. "Retailers who carry both really see a lot more profit."
Reusable plastic are another avocado packaging option that is experiencing growth.
Calavo packs a lot of RPCs, Wedin said.
"They've always been great for us," he said.
The number of RPCs the firm ships is based on customer demand.
The company has integrated RPCs into its packing operation so they can handle them as efficiently as any other box, he said.
"We're happy to pack in lot of box styles," Wedin said.
The firm doesn't have a problem working with RPCs, he said.
Mission Produce has enjoyed steady RPC business, Bertels said.
"We have been using RPCs for quite some time, so their use is well-established and integrated into our operations globally," he said.