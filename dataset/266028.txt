Senator: Attempt to benefit mining company is dead
Senator: Attempt to benefit mining company is dead

The Associated Press

MADISON, Wis.




MADISON, Wis. (AP) — A key Republican says a proposal that would clear the way for a Georgia company to build a $70 million frac sand processing plant in western Wisconsin is dead because of opposition from several state senators.
Sen. Alberta Darling tells The Associated Press on Wednesday that the measure the Assembly passed last week that would allow Meteor Timber to destroy a rare hardwood wetland won't pass the Senate.
The provision benefiting Meteor was attached to an unrelated Darling bill in a move by Assembly Republicans in the closing minutes of their last session day that caught Democrats off guard.
Darling says after a "very, very intense" discussion among Republican senators, it was clear there was too much opposition to pass the bill with the Meteor amendment. Darling says she is disappointed, but the bill is dead.