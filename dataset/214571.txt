We all love the podcast “Stuff you should know” and other like-minded productions such as “How Stuff Works” and “Stuff you missed in history class” and so on.
So I asked members of the Fresh Produce Industry Discussion Group to imagine a podcast called “Stuff you should know about produce.” What topics should be covered in the first few shows if the decision was up to them?
Folks are fired up about this topic, with 22 responses as of Nov. 9.
Here are a few responses so far, with initials of the commenter preceding their thoughts.
D.A.: One of the many, many points would be that although we enjoy an abundance of fresh produce items year round there are still seasons especially specific to locale.
R.P.: I would broadcast our issue in America on consumption. Can it be moved by better merchandising, better products being embraced, continue growing better products and selling higher quality rather than focusing on price. There is a misconception on produce value. I believe our industry can change things.
K.M.: Growers and family farms in the U.S. fruit and veg industry. We all know that many consumers are more interested in where and who their produce comes from. But I’d bet that people would be surprised at the relatively small size of the family farms in the specialty crop industry especially when compared to the average acreage in a farm growing for corn commercially for example.
M.O.: Educate the public on how to best handle/store produce. Fruits and vegetables are unique to the species and should be treated as such. Shelf life matters!
C.Y.: At a CPMA session many years ago the question was asked — if you can’t afford to buy organic fruit and veg what do you do? Eat as much as you can, organic or not.
My own input was to focus on the consumer with a “stuff you should know” podcast about how produce gets to from the farm to the fork. Get one of those GoPro cameras to ride with a newly picked apple from harvest to produce bin.
Speaking of “stuff you should know,” check out this quote from a smartly written and a fact-filled book called “Best Before: the evolution and future of processed food,” by Nicola Temple. I’m about halfway through the book.
In her chapter on fresh produce, she writes: 
“My greater concern lies in the various methods that are used to slow the decay of fresh-cut produce. With the best intention of increasing the shelf life of a product, an opportunity to deceive consumers has been created. In prepackaged produce this could be resolved by having a ‘cut on’ date as well as a ‘best before’ date.”
As a consumer, what falls within the limits of acceptable methods to extend the shelf life of fresh produce? That is a question the industry must answer, even as it sprints ahead to create multiple ways to extend shelf life by new chemistry and technology.
Tom Karst is The Packer’s national editor. E-mail him at tkarst@farmjournal.com.
What's your take? Leave a comment and tell us your opinion.