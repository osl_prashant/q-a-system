For the outgoing Produce Marketing Association chairman, the produce industry's health and outlook remains strong.
Russ Mounce, vice president of produce and floral and divisional merchandising manager for Bentonville, Ark.-based Sam's Club, lists fostering supply chain partnerships, improving global connections and helping grow floral among his accomplishments.
In partnership with the United Fresh Produce Association, the Joint Committee on Responsible Labor Practices met throughout the year to address the critical industry issue.
The effort made a lot of progress but much remains to do, he said.
"It is great to see the collaboration between the associations, retailers, growers and suppliers addressing issues," Mounce said. "We address complex issues more effectively when we have all parties working toward solutions together."
In his work as leader of the Newark, Del.-based PMA, Mounce traveled internationally to PMA Fresh Connections events, including stops in Australia and Tasmania, where he met growers of different commodities.
The visits provided ideas Mounce said he can use with his business as well as benefit the industry.
His tenure in PMA leadership allowed him to make many industry contacts throughout the world.
"We do more together than we do separately," Mounce said. "Whether PMA or another association, those are opportunities to bring companies and people together. If I went there (Australia) on my own, I would not have had the opportunity to meet so many growers in one area to learn about that marketplace."
From a leadership standpoint, he encourages other industry members to become involved in the organization.
"The thing about this industry is I am surrounded by a wealth of industry knowledge and experience from across the supply chain that has supported me personally and professionally," Mounce said. "The produce industry is strong. We are in a good place.
"When you start talking about fresh produce items and varieties, I don't care where you're at, people get excited about it."
Mounce's leadership benefited the supply chain, said Cathy Burns, PMA's president.
"As the youngest PMA board of directors chair ever, his energy and fresh thinking always made him an impactful presence around the table," she said. "He was committed to listening to members' ideas, interests and feedback to discover new ways PMA could serve its members with greater value.
"Russ is also highly committed to fostering supply chain partnerships, which was a priority for him during his chairmanship. His leadership and insight helped establish the Joint Committee on Responsible Labor Practices. I think it's safe to say the supply chain grew a little more cohesive, a little more collaborative thanks to Russ' vision and dedication to the business of produce and floral."
Incoming chairman John Oxford, CEO of Raleigh, N.C.-based L&M, said Mounce has effectively led the organization.
Oxford said he likes Mounce's humility and industry passion.
"It's evident in what he does and I have tremendous respect for him," Oxford said. "I am especially grateful to Russ. He has been a tremendous leader for PMA. We have made a lot of progress on a number of priorities, notably regarding supply chain partnerships. He's also raised the profile in getting more activity with the floral community."
The state of PMA remains sturdy, he said.
"It's stronger than ever," Mounce said. "It continues to grow. We have strong foundations through our four pillars which are connecting globally, science and technology, industry talent and issues leadership. At Fresh Summit, attendees will hear how we're helping grow our members' businesses and increasing consumption to build demand."
On floral, sold-out events in Anaheim, Calif., and Miami helped build industry growth.
Mounce said the industry should expect to see a larger floral presence at this year's Fresh Summit.
PMA's partnership with the Food Marketing Institute to commission research to gather floral data has made much progress, he said.
Mounce first became involved in the PMA in 2008 on a strategic planning task force and later worked on a membership committee. He joined the board in 2013.
Mounce entered the retail industry in 1992 as a cart pusher and stocker for a Springdale, Ark., Wal-Mart Supercenter.
Shortly after, Mounce worked in the store's produce department and became an assistant produce department manager.
Former PMA chairman Bruce Peterson, Wal-Mart Stores Inc.'s senior vice president and general merchandise manager of perishables, would visit the store and provide Mounce merchandising tips.
At a nearby Sam's Club, Mounce began a business relationship with former PMA chairman Bob DiPiazza, the club store's senior vice president and general merchandise manager
In 2001, an assistant buyer position opened at Sam's Club and Mounce moved into buying and advanced through the buying tiers to senior buyer in 2007.
In 2009, he was promoted to senior director of produce and floral and in 2015, was promoted to his current position.
Mounce is scheduled to relinquish his chairman position at the Oct. 14-16 Fresh Summit Convention & Expo at the Orange County Convention Center in Orlando, Fla.