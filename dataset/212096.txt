Pricing wasn’t ideal last year for potatoes grown in Washington and Oregon, but growers expect the market to bounce back this year. 
On June 28, the U.S. Department of Agriculture reported prices of $15.50-17 for 50-pound cartons of russets, size 40-70s out of the Columbia Basin in Washington and the Umatilla Basin in Oregon. 
 
Prices for size 80-100 were $7.50-10. Prices for all sizes of russets were mostly in the $8-10 range a year ago.
 
Chris Voigt, executive director for the Washington State Potato Commission, Moses Lake, Wash., said last year was a horrible year for fresh potato growers in the state in regard to pricing. 
 
“Our growers were selling below the cost of production for most of the year,” he said. 
 
“But as supply tightened this spring, we’ve started to see prices come up for cartons. There is still a big oversupply of consumer bags on the market that is keeping prices very low. It’s a great deal for consumers but makes it hard for our growers to keep farming.” 
 
Ticking upward
Prices have climbed the past two months, said Dan Strebin, owner and manager of Strebin Farms LLC, Troutdale, Ore., who expects prices to continue to climb in a strong market this summer.
 
Dale Hayton, sales manager for Valley Pride Sales, Burlington, Wash., said demand for the company’s colored potatoes seems strong. 
 
“Most of what I am seeing on the retail shelves right now is very light in color, especially on the reds,” he said. 
 
“The demand for high-colored potatoes is strong and I don’t expect any other areas that will be producing between now and our startup will have much color. That should help us when we start our harvest season.”  
 
The early summer market seems to have steadily built on the momentum of the spring, rather than significantly dropping off, said Myron Ayers, sales manager for Norm Nelson Inc., Burlington, Wash., which produces Double-N Potatoes.