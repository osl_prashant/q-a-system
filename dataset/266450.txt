North Dakota expects record soybean, canola, chickpea crops
North Dakota expects record soybean, canola, chickpea crops

The Associated Press

FARGO, N.D.




FARGO, N.D. (AP) — North Dakota farmers are expected to plant 7.1 million acres of soybeans this year, matching last year's record.
Meanwhile, they're expected to plant 11 percent fewer acres of corn, at about 3.1 million.
That mirrors the national trend. The federal Agriculture Department says in its annual prospective plantings report that U.S. farmers will seed more soybeans than corn for the first time in 35 years, due primarily to profitability.
Acres of North Dakota's staple spring wheat crop are expected to be up 20 percent this year, to 6.4 million. Sugar beet acres are pegged at 199,100, down 7 percent.
Canola and chickpea acres are forecast at record highs. Sunflower acres also are expected to be up.
Other crops with projected declines in acres are durum wheat, barley, dry beans, dry peas, lentils, flaxseed and oats.