This market update is a PorkNetwork weekly column reporting trends in weaner pig prices and swine facility availability.  All information contained in this update is for the week ended May 26, 2017.

Looking at hog sales in November 2017 using December 2017 futures the weaner breakeven was $24.91, up $0.23 for the week. Feed costs were up $0.13 per head. December futures increased $0.18 compared to last week’s December futures used for the crush and historical basis is unchanged from last week. Breakeven prices are based on closing futures prices on May 26, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash traded weaner pig volume was below average this week with 28,019 head being reported which is 76% of the 52-week average. Cash prices were $29.51, up $0.39 from a week ago. The low to high range was $18.00 - $34.50. Formula priced weaners were down $1.12 this week at $35.58.
Cash traded feeder pig reported volume was above average with 18,650 head reported. Cash feeder pig reported prices were $52.86, up $1.15 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through May 26, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential decreased $0.17 this week to a projected loss of $4.60 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.