The National Sorghum Producers (NSP) is expressing their disappointment after the Chinese government imposed what amounts to a tariff on U.S. sorghum imports.

A preliminary ruling by China’s Ministry of Commerce (MOFCOM) says U.S. sorghum was being sold at improperly low prices, which hurt Chinese farmers. 

In a statement released on Tuesday, NSP said they are “deeply disappointed” by China’s MOFCOM decision.

“National Sorghum Producers, alongside our producers, stakeholders, and partners, has cooperated fully with China’s antidumping and countervailing duty investigations, including submitting several thousand pages of data demonstrating conclusively that U.S. sorghum is neither dumped nor causing any injury to China,” read the statement.

China, one of the biggest foreign markets for U.S. sorghum growers, is offering importers of U.S. sorghum to post bonds of 178.6 percent of the value of their goods to cover possible anti-dumping duties while the probe is completed.

“None of this information appears to have been seriously considered or used in today’s preliminary determination, which is neither fair nor appropriate,” the statement said.

In February, China began its investigation, with some calling it a warning shot after President Donald Trump increased tariffs on Chinese-made washing machines and solar modules.

Justin Knopf, a sorghum producer in Salina, Kansas, reacts to the tension around trade is impacting prices on AgDay above.