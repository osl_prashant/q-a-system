AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 6:15 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
UPCOMING TOMORROW:
COLORADO LEGISLATURE-SEXUAL MISCONDUCT
DENVER — Democrats in the Colorado House are vowing to press ahead with an apparently doomed vote Friday on whether to make one of their own the nation's second lawmaker expelled since the rise of the #MeToo movement.
DEATH PENALTY-MENNONITE JAILED
DENVER — A panel of state appeals court judges are set to hear from the attorneys for a defense investigator in a death penalty case who has been jailed indefinitely after refusing to testify for prosecutors.
TOP STORIES TODAY:
COLORADO LEGISLATURE-SEXUAL MISCONDUCT
DENVER — An effort by Colorado Democrats to make one of their own the nation's second state lawmaker expelled over sexual misconduct allegations since the rise of the #MeToo movement nearly derailed Thursday amid Republican objections to how the complaints have been handled. A few Democrats also expressed concerns about whether the case against Rep. Steve Lebsock was being unnecessarily rushed to an expulsion vote set for Friday. By James Anderson. SENT: 600 words, photos.
With: COLORADO LEGISLATURE-SEXUAL MISCONDUCT-THE LATEST
YOUTH SERVICES REFORM
DENVER — The number of assaults in Colorado youth detention centers has dropped as efforts to reform the system are taking hold, according to state data. The state Division of Youth Services has seen a decrease in assaults per month on staff members by the youths, dropping from 22 in December 2015 to 12 in January, The Denver Post reported Monday. SENT: 260 words.
OF COLORADO INTEREST:
HOUSE EXPLOSION-EVACUATIONS
DALLAS — Natural gas service was shut down Thursday to thousands of Dallas homes following a series of leaks that has brought repeated evacuations in the wake of a house explosion that killed a 12-year-old girl. Gas service will be discontinued for up to three weeks to about 2,800 homes northwest of downtown as gas lines are replaced and other work is done by more than 120 Atmos Energy crews, authorities said at a news conference. By David Warren. SENT: 550 words, photos.
DEATH WITH DIGNITY
HONOLULU — A measure that would allow terminally ill patients to request prescriptions for lethal doses of medication will soon be getting a vote by the Hawaii House of Representatives. The House Health and Human Services Committee and the Judiciary Committee on Wednesday both voted in favor of the measure called the "Our Care, Our Choice Act." HHS Committee Chairman Rep. John Mizuno said the proposal now has the strongest protections of any state. SENT: 230 words.
IN BRIEF:
— DEATH-PENALTY-MENNONITE-JAILED — Colorado appeals court judges rejected a request to release a defense investigator as she appeals her jail sentence for refusing to testify for prosecutors in a death penalty case.
— SEXUAL ASSAULT-AIR FORCE ACADEMY — The Defense Department's internal watchdog plans to investigate the Air Force Academy office that supports sexual assault victims after a previous inquiry found it was derelict in its duties because of poor management.
— INMATE DEATH — Authorities say a woman being held in a Colorado jail has died after being found inside a cell with a trash bag over her head.
— DEAD INMATE-LAWSUIT — The brother of a dead inmate who alleges the inmate was beaten after extending his middle finger at one of the deputies is suing two Pueblo County Sheriff's Office deputies and a medical technician.
— NBA AGENT KILLED — State and county officials are planning to meet to discuss ways to improve a Colorado intersection where a crash killed a longtime NBA agent.
— CONVICTION VACATED-MAN RELEASED — A Colorado man who was sentenced to more than 300 years in prison for sexually assaulting six children was released after an appeals court determined that his right to a speedy trial had been violated.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for March advanced 21 cents at 5.0550 a bushel; March corn was up 4.25 cents at 3.7875 a bushel; March oats was rose 6 cents at $2.69 a bushel; while March soybeans gained 12.50 cents at $10.5750 a bushel.
SPORTS IN BRIEF:
— COACH REBUKED-COLORADO MARIJUANA — Texas Wesleyan University has fired its baseball coach after he told a high school player from Colorado that the team doesn't recruit from the state because players there fail drug tests.
— BRONCOS-PARKS — Colorado prosecutors say they have dismissed charges of harassment and non-physical domestic violence against Broncos safety Will Parks.
— ENGLAND-NEW ZEALAND — England and New Zealand will play each other in Denver the first of three rugby league tests in the United States over the next three years.
— BKW-COLORADO-UTAH — Kennedy Leonard had 15 points, eight rebounds, four assists and five steals and Colorado beat Utah 66-56 on Thursday to open play in the Pac-12 Conference Tournament.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.