U.S. lean hog futures surged about 4 percent on Tuesday, boosted by chart-based buying and stronger demand from pork packers, traders and analysts said.Hog futures on the Chicago Mercantile Exchange notched the largest percentage gains in more than two months as prices continued rising from multimonth lows reached on Aug. 29.
Two new pork processing plants began slaughtering this week in Iowa and Michigan, increasing competition for hogs, the traders said.
"(Packers) are short of hogs and looking around," Archer Financial Services broker Dennis Smith said.
CME October hogs settled up 2.300 cents at 63.750 cents per pound, a 1-1/2 week high.
The contract surpassed the support level of about 62.400 cents, triggering technical buying, said Rosenthal Collins Group broker James Burns.
Cash hog prices were down 16 cents in the top markets of Iowa and Minnesota after rising earlier on Tuesday, U.S. Department of Agriculture (USDA) data showed.
Signs that wholesale pork prices were nearing a seasonal low further underpinned hog prices. The pork cutout that tracks values of cuts such as hams and pork bellies was up 49 cents to $84.96 per cwt, according to the USDA.
CME live cattle and feeder cattle futures were mostly lower as ample U.S. cattle supplies continued to weigh on prices.
CME October live cattle finished 0.725 cent lower at 104.425 cents per pound, its lowest settlement price since March 15. CME October feeder cattle were down 1.050 cents to 142.500 cents per pound.
USDA said choice-grade wholesale beef gained $1.10 to $192.45 per cwt and select-grade beef was down 18 cents to $190.47 per cwt.