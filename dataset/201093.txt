$5,884.22 PER ACRE Jasper and Poweshiek Counties, IA
June 22: 256.0 total acres in 4 tracts, approximately 1/2 mile southeast of Lynnville, IA.

Tract 1: 54 total acres with 34 tillable with a CSR2 of 70.9, currently in row crop production, and the balance being 19.84 acres of pasture.
Tract 2: 90 total acres with 88 tillable with a CSR2 of 65.1, currently in row crop production, and the balance being a farmstead with farmhouse and outbuildings.
Tract 3: 70 total acres with 40.43 tillable with a CSR2 of 44.5 currently in row crop production, and the balance being pasture and recreational timber.
Tract 4: 42 total acres with 40.37 tillable with a CSR2 of 46.3, approximately 16 acres are currently in row crop production, and the balance being pasture.

Dale Bolton, Sullivan Auctioneers, LLC, Hamilton IL, 217-847-2160.