Shaking off hurricane damage to the Houston region, Texas markets enter 2018 on a strong business note but pressured by high truck rates.
Hurricane Harvey hit Texas in late August and caused extensive damage, including some that still lingers.
“We’ve had four months removed from everything and we still have some restaurants going out of business, waiting on insurance money that never came,” said Brent Erenwert, chief innovation officer for Houston-based Brothers Produce. But mostly, the hurricane is in the rear-view mirror, he said.
“Texas is very strong, as strong as it has ever been,” he said Jan. 15. 
Going into 2018, high truck rates are grabbing the industry’s attention in Texas, Erenwert said.
The combination of the electronic logging device mandate and hours of services requirements create headaches for the industry.
One question for the coming year is whether receivers will be prevented from getting product from shipping regions because of the truck shortage, Erenwert said.
The onset of federal food safety regulations may also be a competitive advantage for firms that have a good handle on compliance, he said.
Value-added and fresh-cut produce continues to show sales gains, Erenwert said, and that trend will continue in 2018, he said. 
Smaller fresh-cut produce packs for school meals — expanding from apples and carrots to include other fresh produce — will continue to gain popularity.
 
Business outlook
Part of the story for Texas is favorable population trends.
From 25.14 million people in 2010, the Texas population grew 13% to 28.3 million people by July 1 last year.
That is a faster growth clip than the entire U.S., which grew 5% in the same period, from 308.8 million in 2010 to 325.7 million in 2016.
One fast-growing Texas market is Austin, which grew 16.9% from 2010 to 2016, from 811,000 people in 2010 to 948,000 in 2016.
The population of Dallas grew 10% from 2010 to 2016, while Houston’s growth’s was a tick behind at 9.7% from 2010 to 2016. San Antonio grew by more than 12% from 2010 to 2016, according to the U.S. Census.
 
Hispanic audience
Another key demographic that relates to produce preference is a very high percentage of Hispanic consumers. The Hispanic or Latino population in Texas in 2016 was put by the Census Bureau at 39.1% of the population, more than double the U.S. average of 17.8%.
The state also serves as an increasingly important pipeline for produce from Mexico.
“The only new trend I have seen is, from the damage in Florida — and the fact of new highways built (in Mexico), I have seen a lot more produce coming through Texas rather than Nogales,” said Mark Frazier, president of San Antonio-based W.G. Frazier & Son Inc.
He said it is uncertain if that trend will continue, since much of the cucumbers, bell peppers and tomatoes coming through Texas in recent months were related to hurricane damage in Florida.
High truck rates have also been accompanied heavy volume of Mexican produce through the state, with rates of $9,000 to $10,000 per load not uncommon for Midwest and East Coast destinations. 
Bad weather, the holiday and the electronic logging device mandate also played a role, he said. 
The company does large volumes of mixed greens, avocados and broccoli from Mexico.
Long term, Frazier said that Texas will continue to be in a strong position, with big import volume from Mexico expected to continue indefinitely unless the North American Free Trade Agreement falls apart.
“Texas is lucky because we have Mexico, we really are,” he said.