Marketing year average price for wheat, oats came in under reference price; barley prices just above


 
Eligible wheat and oats producers in the Price Loss Coverage (PLC) program for 2016 crops will receive a payment in October as the marketing year average price for both commodities came in under the reference price for the program, according to USDA.
Eligible wheat producers will receive a payment of $1.61 per bushel on wheat and 34 cents per bushel on oats. Barley producers, however, will not receive a payment as the marketing year average price for barley at $4.96 per bushel is above the $4.95 reference price.




Item


Wheat


Barley


Oats




Dollars Per Bushel




Reference Price 


5.50


4.95


2.40




National Loan Rate 


2.94


1.95


1.39




Marketing Year Average (MYA) Price 


3.89


4.96


2.06




Higher of Loan Rate or MYA Price 


3.89


4.96


2.06




PLC Payment Rate 


1.61


0.00


0.34




 
However, those payments will not go to producers until after October 1 based on provisions in the 2014 Farm Bill.
As for the Agriculture Risk Coverage County (ARC-CO) program, USDA noted the higher of the either the national average loan rate or the marketing year average price for the covered commodity will be used to calculate actual crop revenue once yields are known for a specific county and covered commodity.