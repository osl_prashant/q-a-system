Children can learn about the science of plants in the middle of winter thanks to the University of Tennessee Gardens. They host a number of educational programs for kids year round, including greenhouse gardening.

Charles Denney from the University of Tennessee shows how cold weather lessons could become spring season growth on AgDay above.