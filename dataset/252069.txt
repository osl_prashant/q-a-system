The wheat picture in Oklahoma and Kansas is in a “critical” situation. Farmers will soon have to determine what they want to do with the crop: will they try and salvage it or will it get tore up?
 
Usually a wheat crop is killed a couple times before a harvest, but according to Mark Gold, founder of Top Third Ag Marketing, this year is looking more serious than other years.

“We’ve got this nice rally caused by the fund short covering—we’ve got the new crop prices at good levels,” he told AgDay host Clinton Griffiths.

With these strong prices and a questionable crop in the ground, some farmers will need to do some critical thinking. Gold said the “perfect opportunity” to buy a put option.

“That put doesn’t commit you to selling grain,” he said. “It puts a floor in on the grain. If you know you don’t have the crop, you can always sell that put, recapture some of that premium back.”

Gold is known for saying that farmers have one opportunity to market grain at profitable levels. While this rally is happening early in the year, he says to take advantage of every one you can.

“You always have more grain to sell out there,” he said. “We know how farmers are, so we want to look at these opportunities and take advantage of them.”

Hear his full comments on AgDay above.