The Grower-Shipper Association of Central California's documentary "Historical Narratives of Salinas Valley Agriculture" is premiering at Corral de Tierra Country Club.
The premiere is set for 6:30 p.m. on Sept. 8 at the country club, which is in Corral de Tierra, Calif. A reception precedes the viewing.
Past association chairman Henry Dill came up with the idea for the project because of his desire to preserve the history of the local agriculture industry. GSA and Hartnell College's Film Production class spent eight months compiling and producing the documentary, which will give viewers first-hand accounts of the individuals who spent their lives building and shaping Salinas Valley agriculture. The sixteen industry veterans interviewed for the project include:
Jack Armstrong
Jim Bogart
Ed Boutonnet
Andy D'Arrigo
Carl Dobler
Denny Donovan
Ed Given
Bill Gularte
Raymond Gularte
Tom Hubbard
Lloyd Koster
Vic Lanini
Bob Nunes
Tom Nunes
Bill Ramsey
Hugo Tottino
Tickets for the premiere, which must be purchased in advance, are $75.