Sonic Drive-In will be the first major quick-service restaurant to test a mushroom-ground beef blended burger.
The company plans to market its new Sonic Slinger not as a healthier burger but simply as its juiciest yet.
The Slinger, which is 25-30% mushroom and weighs about three ounces, will be tested in multiple markets this summer.
“We are excited to see Sonic take the Blend to the QSR sector as a next major step in consumer adoption,” said Bart Minor, president and CEO of the Mushroom Council. “According to foodservice veterans we have spoken with, they consider this a huge, positive advance in the menu development cycle.”
Scott Uehlein, vice president of product innovation at Sonic and previously a corporate chef for health and wellness resort Canyon Ranch, started the ball rolling on the project in 2015 when he reached out to the council.
“It was awesome validation that we were on to something,” Minor said.
Minor and council chefs visited Sonic’s culinary innovation facility center to assist in the development of the burger.
“We worked together on all sorts of different blends: percentages, other flavor add-ins, different grinds and more,” Uehlein said.
Sonic also tackled how to make the burger feasible for its restaurants as well as its test kitchen.
“As a quick-service restaurant brand, speed of service is critical,” Uehlein said. “The primary development issues centered around the patty cook time, which we overcame by leveraging the expertise of our manu-facturer partner. With their help, we were able to adjust the actual patty-making process in order to get cook time where we needed it to be in order to deliver a delicious Sonic Slinger Burger to guests quickly.”
The product gained buy-in from the top of the company food chain during a new product presentation that included the burger. Marketing and consumer insights representatives and the president of the company were there, and the reception was overwhelmingly positive.
Alicia Mowder, senior director of brand marketing for Sonic, spoke during the Menus of Change Leadership Summit June 20-22 in New York about the process of moving the burger toward market, from focus groups to concept testing to taste testing to operations testing to market testing.
Sustainability and nutrition are part of the appeal of the blended burger movement, but flavor has been the reason Sonic has gone forward.
“If it tasted like any other burger, there was no reason to do it,” Mowder said. “(It got) really strong scores, some of the best that we’ve had in multiple years (for) a burger product.”
A spokesman for Sonic did not say where the burger will be tested or in how many locations it will be offered, but it could be in all locations if all goes well.
“We’ve put a large amount of investment behind this, and we think that this will be a home run and ready for launch probably nationally in the next six months,” depending on whether it performs as expected, Mowder said.