AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:15 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
UPCOMING TOMORROW:
COLORADO PRIMARIES
DENVER — Colorado's major party assemblies on Saturday feature a surprisingly open Republican governor's contest with candidates courting delegates loyal to President Donald Trump and a Democratic competition whose candidates offer an anti-Trump vision for the state.
TOP STORIES TODAY:
NATIONAL PARKS-FEE INCREASE
WASHINGTON — The Interior Department is increasing fees at the most popular national parks to $35 per vehicle, backing down from an earlier plan that would have forced visitors to pay $70 per vehicle to visit the Grand Canyon, Yosemite and other iconic parks. A change announced Thursday will boost fees at 17 popular parks by $5, up from the current $30 but far below the figure Interior proposed last fall. By Matthew Daly. SENT: 530 words, photos.
MINE WASTE SPILL-CLEANUP
DENVER — The U.S. Environmental Protection Agency is firing back at a mining company that accused the agency of letting untreated mine wastewater get into a southwestern Colorado river. Doug Benevento, the EPA's Denver region director, said Thursday the criticism from Sunnyside Gold Corp. was meant to distract attention from Sunnyside's responsibility to help with a Superfund cleanup of the area. By Dan Elliott. SENT: 130-word APNewsNow, photos. Will be expanded.
COLORADO BALLOT-PETITIONS
DENVER — A federal appeals court on Thursday reinstated Colorado's Amendment 71, which makes it more difficult to change the state constitution, weeks after a judge's order blocked parts of it from being enforced. The stay issued by the 10th U.S. Circuit Court of Appeals puts the voter-approved measure, known as "Raise the Bar," temporarily back in effect for any constitutional initiatives proposed for the November 2018 ballot, the Colorado secretary of state's office confirmed. By Brian Eason. SENT: 500 words, photo.
OF COLORADO INTEREST:
DRY SOUTHWEST
ALBUQUERQUE, N.M. — Drought is tightening its grip across the American Southwest as extreme conditions spread from Oklahoma to Utah, according to new federal data released Thursday. On the southern high plains, Oklahoma remains ground zero for the worst drought conditions in the United States. About 20 percent of the state is facing exceptional drought conditions — the worst possible classification. By Russell Contreras and Julian Hattem. SENT: 430 words, photos.
BANNER HEALTH-MEDICARE SETTLEMENT
PHOENIX — Phoenix-based Banner Health will pay more than $18 million to settle a federal lawsuit accusing the health provider of submitting unnecessary and falsified Medicare claims, the U.S. Justice Department said Thursday. The lawsuit, which was filed in U.S. District Court in Arizona, was brought by a former employee. Cecilia Guardiola was a corporate director of clinical documentation who sued under the whistleblower provisions of the False Claims Act. By Terry Tang. SENT: 440 words.
PENSION CRISIS-STATES
CHERRY HILL, N.J. — A public employee pension crisis for state governments has deepened to a record level even after nearly nine years of economic recovery for the nation, according to a study released Thursday, leaving many states vulnerable if the economy hits a downturn. The massive unfunded pension liabilities are becoming a real problem not just for public-sector retirees and workers concerned about their future but also for everyone else. By Geoff Mulvihill. SENT: 730 words, photo.
UBER-RIDER SAFETY
DETROIT — Uber will start doing annual criminal background checks on U.S. drivers and hire a company that constantly monitors criminal arrests as it tries to do a better job of keeping riders safe. The move announced Thursday is one of several actions taken by the ride-hailing company under new CEO Dara Khosrowshahi, who said that the changes aren't just being done to polish the company's image, which has been tarnished by driver misbehavior and a long string of other embarrassing failings. By Tom Krisher. SENT: 880 words, photo.
MISSILE SITE SMOKE
HARRISBURG, Neb. — The U.S. Air Force is investigating what caused an electrical problem at a western Nebraska missile alert facility that forced two officers to be treated for smoke inhalation. Area firefighters and medics responded Tuesday afternoon to the facility near Harrisburg in Banner County. The facility is a support and living area where a missile launch crew is on alert and is geographically separated from the Minuteman III missiles, which are several miles away, said Lt. Mikayla Gomez with the 90th Missile Wing. SENT: 200 words.
IN BRIEF:
— COLORADO IMMIGRATION LAWSUIT — The state Supreme Court has denied an appeal by a Colorado sheriff ordered to release people who are wanted for possible deportation by federal authorities but have paid bond.
— COLORADO STATE FAIR-GENERAL MANAGER — The Colorado State Fair has a new general manager.
— WELL PROTEST — A Colorado State University student arrested after he locked himself to a bulldozer in protest at an oil and gas site in Greeley has filed a motion to dismiss a lawsuit brought against him by the oil company that owns the site.
— NURSE ABUSE — A nurse convicted in Colorado of groping patients has pleaded guilty to similar accusations in western Nebraska.
— ASPEN SNOWMAKING — Aspen Skiing Co. is planning to expand its snowmaking efforts for the next ski season after experiencing what officials say was one of its more challenging years.
— DENVER HOSPITAL-INFECTION RISK — Surgeries are resuming at a Denver hospital a week after it shut down its operating room because of problems with the cleaning of its surgical instruments.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for May rose 6.25 cents at 4.81 a bushel; May corn was up 1.75 cents at 3.8875 a bushel; May oats was unchanged at $2.3750 a bushel; while May soybeans advanced 13 cents at $10.6075 a bushel.
SPORTS:
AVALANCHE-PREDATORS
NASHVILLE, Tenn. — The Nashville Predators, winners of the Presidents' Trophy, now start what they hope is a return to the Stanley Cup Final, starting Thursday night with Game 1 against the Colorado Avalanche who squeaked into the postseason winning the final game of the regular season. By Teresa M. Walker. UPCOMING: 650 words, photos. (Game starts at 7:30 p.m. MT)
PREDATORS-CATFISH TANK
NASHVILLE, Tenn. — Tossing catfish onto the ice has been a popular hockey tradition in Music City and the Nashville Predators finally have their own catfish tank inside the arena. Team President Sean Henry unveiled the new tank hours before the Predators opened the NHL postseason against Colorado in their first-round Western Conference series. The team put the first of four catfish into the tank and No. 2 was on the way. SENT: 370 words, photos.
NUGGETS WRAPUP
DENVER — Nikola Jokic only had a basic outline of his offseason plans: Rest, work out in Denver, return home to Serbia and rest some more. It's not more elaborate since the Nuggets big man figured he'd be working for a while longer. By Pat Graham. SENT: 800 words, photos.
ROCKIES-NATIONALS
WASHINGTON — The Washington Nationals and Colorado Rockies open a four-game series Thursday night. UPCOMING: 700 words, photos. (Game started at 5:05 p.m. MT)
____
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.