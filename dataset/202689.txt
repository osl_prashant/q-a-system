Online registration is open for the Forbes AgTech Summit, scheduled for Salinas, Calif., on June 28-29.
 
The upcoming summit, the third annual event so far, is expected to attract about 600 leaders in agricultural technology and innovators from the Silicon Valley, according to a news release.
 
Western Growers members receive a $250 discount on the regular admission, according to the release. 
 
Last year, industry leaders who spoke at the event included Bruce Taylor, chairman and CEO of Taylor Farms, and Kevin Murphy, CEO of Driscoll's.