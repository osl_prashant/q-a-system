USDA Weekly Export Sales Report
			Week Ended May 4, 2017





Corn



Actual Sales (in MT)

Combined: 222,600
			2016-17: 277,700
			2017-18: -55,100



Trade Expectations (MT)
750,000-1,150,000


Weekly Sales Details
Net sales of 277,700 MT for 2016-17--a marketing-year low--were down 64% from the previous week and 66% from the prior four-week average. Increases were reported for Mexico (109,400 MT, including decreases of 3,900 MT), Japan (89,500 MT, including 42,500 MT switched from unknown destinations and decreases of 53,600 MT), Venezuela (30,000 MT), Jordan (26,300 MT, including 25,000 MT switched from unknown destinations), and Ireland (19,600 MT, switched from unknown destinations). Reductions were reported for unknown destinations (76,800 MT), Guatemala (12,600 MT), South Korea (5,200 MT), and El Salvador (700 MT). For 2017-18, net sales reductions of 55,100 MT resulted as increases for Nicaragua (16,400 MT), were more than offset by reductions for Japan (71,500 MT). 


Weekly Export Details
Exports of 722,900 MT were down 41% from the previous week and 43% from the prior four-week average. The primary destinations were Mexico (302,900 MT), Japan (198,200 MT), South Korea (61,300 MT), Canada (26,700 MT), and Jordan (26,300 MT).


Comments and Performance Indicators
Sales fell well short of expectations. Export commitments for 2016-17 are running 34% ahead of year-ago compared to 37% ahead the week prior. USDA projects exports in 2016-17 at 2.225 billion bu., up 17.2% from the previous marketing year.




Wheat



Actual Sales (in MT)

Combined: 249,200
			2016-17: -24,200
			2017-18: 273,400



Trade Expectations (MT)
250,000-650,000 


Weekly Sales Details
Net sales reductions of 24,200 metric tons--a marketing-year low--for delivery in marketing year 2016-17 were down noticeably from the previous week and from the prior four-week average. Increases were reported for Indonesia (66,500 MT, including 62,000 MT switched from unknown destinations and decreases of 1,000 MT), China (63,000 MT, including 60,000 MT switched from unknown destinations), Egypt (51,600 MT, including 50,000 MT switched from unknown destinations), Chile (32,400 MT, including 30,000 MT switched from unknown destinations), and Algeria (30,900 MT, switched from unknown destinations and decreases of 28,900 MT). Reductions were reported for unknown destinations (315,400 MT), Japan (34,700 MT), Nigeria (4,500 MT), and Malaysia (700 MT). For 2017-18, net sales of 273,400 MT were reported primarily for China (60,000 MT), Mexico (56,000 MT), unknown destinations (46,000 MT), and the Philippines (45,000 MT). 


Weekly Export Details
Exports of 595,800 MT were down 4% from the previous week and 9% and from the prior four-week average. The destinations were primarily Indonesia (133,900 MT), Nigeria (74,600 MT), China (63,000 MT), Egypt (51,600 MT), and Algeria (37,900 MT).


Comments and Performance Indicators
Sales were just below expectations. Export commitments for 2016-17 are running 36% ahead of year-ago versus 39% ahead the week prior. USDA projects exports in 2016-17 at 1.035 billion bu., up 33.5% from the previous marketing year.




Soybeans



Actual Sales (in MT)

Combined: 451,400
			2016-17: 381,400
			2017-18: 70,000



Trade Expectations (MT)
300,000-700,000 


Weekly Sales Details
Net sales of 381,400 MT for 2016-17 were up 20% from the previous week, but down 4% from the prior four-week average. Increases were reported for unknown destinations (89,400 MT), Japan (60,000 MT, including 58,500 MT switched from unknown destinations and decreases of 200 MT), Bangladesh (58,300 MT, including 55,000 MT switched from unknown destinations and decreases of 700 MT), the Netherlands (57,700 MT, switched from unknown destinations and decreases of 8,300 MT), and Indonesia (31,400 MT, including decreases of 100 MT). Reductions were reported for Cuba (600 MT). For 2017-18, net sales of 70,000 MT were reported for China (60,000 MT) and Japan (10,000 MT).


Weekly Export Details
Exports of 346,000 MT were down 46% from the previous week and 45% from the prior four-week average. The destinations were primarily Pakistan (69,700 MT), Japan (63,900 MT), the Netherlands (57,700 MT), Bangladesh (54,300 MT), and Colombia (20,400 MT). 


Comments and Performance Indicators
Sales met expectations. Export commitments for 2016-17 are running 23% ahead of year-ago, compared to 23% ahead the previous week. USDA projects exports in 2016-17 at 2.050 billion bu., up 5.9% from year-ago.




Soymeal



Actual Sales (in MT)
Combined: 161,800
			2016-17: 137,100
			2017-18: 24,700


Trade Expectations (MT)

40,000-215,000 



Weekly Sales Details
Net sales of 137,100 MT for 2016-17 were up 32% from the previous week and 8% from the prior four-week average. Increases were reported for the Philippines (93,200 MT), Colombia (21,900 MT, including 10,000 MT switched from unknown destinations), Panama (7,600 MT, including 7,100 MT switched from unknown destinations), Canada (6,800 MT, including decreases of 100 MT), and Mexico (4,600 MT). Reductions were reported for unknown destinations (14,100 MT) and Guatemala (4,500 MT). For 2017-18, net sales of 24,700 MT were reported for Mexico. 


Weekly Export Details
Exports of 125,700 MT were down 46% from the previous week and 36% from the prior four-week average. The destinations were primarily the Philippines (49,100 MT), Mexico (19,200 MT), Canada (14,800 MT), Colombia (14,000 MT), and Panama (7,600 MT). 


Comments and Performance Indicators
Sales met expectations. Export commitments for 2016-17 are running 1% ahead of year-ago compared with 1% ahead of year-ago the week prior. USDA projects exports in 2016-17 to be up 1.1% from the previous marketing year.




Soyoil



Actual Sales (in MT)

2016-17: 29,000



Trade Expectations (MT)
0-35,000


Weekly Sales Details
Net sales of 29,000 MT for 2016-17 were up noticeably from the previous week and 92% from the prior four-week average. Increases reported for unknown destinations (15,000 MT), Jamaica (4,000 MT), France (4,000 MT), Colombia (2,500 MT), and Mexico (2,300 MT). 


Weekly Export Details
Exports of 18,100 MT were down 36% from the previous week and 34% from the prior four-week average. The destinations were primarily Guatemala (4,400 MT), Jamaica (3,500 MT), Colombia (3,500 MT), and Mexico (2,800 MT).


Comments and Performance Indicators
Sales were within expectations. Export commitments for the 2016-17 marketing year are running 12% ahead of year-ago, compared to 11% ahead last week. USDA projects exports in 2016-17 to be up 2.7% from the previous year.




Cotton



Actual Sales (in RB)
Combined: 307,000
			2016-17: 160,600
			2017-18: 146,400


Weekly Sales Details
Net upland sales of 160,600 RB for 2016-17 were up 5% from the previous week, but down 20% from the prior four-week average. Increases were reported for Turkey (59,400 RB, including decreases of 2,200 RB), Vietnam (43,600 RB, including 300 RB switched from Japan and decreases of 1,300 RB), China (15,700 RB, including decreases of 300 RB), South Korea (15,600 RB, including decreases of 1,100 RB), and Peru (10,400 RB). Reductions were reported for Japan (3,900 RB) and Taiwan (3,700 RB). For 2017-18, net sales of 146,400 RB were reported primarily for Indonesia (47,200 RB), Vietnam (27,700 RB), China (17,500 RB), Bangladesh (13,200 RB), and South Korea (12,900 RB). 


Weekly Export Details
Exports of 412,800 RB were up 16% from the previous week and 13% from the prior four-week average. The primary destinations were Turkey (86,900 RB), Vietnam (60,800 RB), Indonesia (54,100 RB), India (39,600 RB), and Bangladesh (32,700 RB). 


Comments and Performance Indicators
Export commitments for 2016-17 are 73% ahead of year-ago, compared to 73% ahead the previous week. USDA projects exports to be up 58.5% from the previous year at 14.500 million bales.




Beef



Actual Sales (in MT)

2017: 10,800



Weekly Sales Details
Net sales of 10,800 MT reported for 2017 were down 35% from the previous week and 41% from the prior four-week average. Increases were reported for South Korea (4,500 MT, including decreases of 400 MT), Japan (2,000 MT, including decreases of 1,500 MT), Mexico (1,400 MT, including decreases of 100 MT), Canada (1,100 MT, including decreases of 100 MT), and Taiwan (700 MT, including decreases of 100 MT). Reductions were reported for Panama (100 MT). 


Weekly Export Details
Exports of 12,800 MT were unchanged from the previous week, but down 5% from the prior four-week average. The primary destinations were Japan (4,500 MT), South Korea (2,600 MT), Mexico (1,600 MT), Hong Kong (1,200 MT), and Canada (1,100 MT). 


Comments and Performance Indicators
Weekly export sales compare to 16,600 MT the week prior. USDA projects exports in 2017 to be up 10.0% from last year's total.




Pork



Actual Sales (in MT)
2017: 17,600


Weekly Sales Details
Net sales of 17,600 MT reported for 2017 were up 28% from the previous week, but down 28% from the prior four-week average. Increases were reported for Japan (4,100 MT), South Korea (4,000 MT), Hong Kong (2,400 MT), Mexico (2,400 MT), and Canada (1,300 MT). 


Weekly Export Details
Exports of 22,600 MT were down 10% from the previous week, but up 7% from the prior four-week average. The destinations were primarily Mexico (7,300 MT), Japan (3,800 MT), South Korea (3,000 MT), China (2,800 MT), and Canada (1,600 MT). 


Comments and Performance Indicators

Export sales compare to a total of 14,100 MT the prior week. USDA projects exports in 2017 to be 9.8% above last year's total.