Emmanuel Faber, CEO of Danone, wants the world's biggest yogurt maker to play a central role in the revolution sweeping the global food industry as it tries to respond to a consumer shift towards healthy eating.As more consumers, notably the "Millennial" generation, opt for healthier diets and a more socially responsible way of life, Danone, along with rivals such as Nestle, have been seeking to adapt.
Faber unveiled a new company "signature" dubbed "One Planet. One Health" for Danone on Thursday in Berlin at the annual meeting of the Consumer Goods Forum, a gathering of the world's biggest retailers and packaged food companies.
"A revolution is cooking, what are we going to do about it?" Faber told the meeting, warning that consumers will turn their backs on big food companies if they do not do more to address issues like obesity, inequality and climate change.
"We are losing them. They are getting out of our shops, out of our brands. They are going for food without the food industry. Not only without us, but maybe against us," he said.
Danone has bought U.S. organic food producer WhiteWave in a $12.5 billion deal, bringing the company more into line with healthier eating trends.
The deal also aims to boost growth at Danone, whose shares trade at a discount to rivals. The company's depressed valuation was highlighted this week as a reason for it being touted as a potential bid target.
Faber told Reuters that Danone, which has no large controlling shareholder, was "no more and no less than usual" vulnerable to a possible takeover bid.
Danone is seeking to build on the WhiteWave deal with a campaign to promote itself as a leader in terms of healthy eating habits.
"The global industrial food system is reaching its limits," Faber told Reuters in a phone interview before his speech in Berlin. He said evidence of this included obesity and malnutrition, wasting water and food, soil depletion, and climate change.
"Everywhere people want to regain control over their food," said Faber, a rock climber and campaigner for corporate social responsibility.
Buying Into the Future
WhiteWave's products have outsold mainstream packaged food businesses in recent years, highlighting the consumer shift toward natural foods and healthier eating. The deal should also help Danone to cope with tougher market conditions in dairy products in Europe, and babyfood in China.
WhiteWave makes Danone the world's biggest producer of organic food and gives it a stronger foothold in North America, which is becoming its biggest market, accounting for $6 billion, or around 25 percent of group sales against 13 percent previously.
Faber said he hoped the new Danone signature would help to address a general consumer mistrust of big, corporate brands.
"Small brands communicate on their intentions, they are activists. It is key that big brands also state their intentions," he said.
Faber, the first Danone CEO from outside the founding Riboud family, is pushing on with a dual economic and social agenda, which - like that of many blue-chip companies - aims to not only boost shareholder value and profits but also meet other targets on the environment and social policies.
"The big risk is to avoid transforming ourselves and end up only cutting costs to return cash to shareholders," he said.
A pledge at the annual shareholder meeting in April for Danone to be certified as a for-profit corporation that commits to positive social and environmental goals - was in line with that strategy, he said.
Bid Talk
Bid speculation around Danone pushed its shares sharply higher this week. Broker Exane said it could be an acquisition target for Kraft Heinz, also citing PepsiCo and Coca Cola as credible suitors.
Analysts at Berenberg wrote in a research note that investors would need concrete evidence of Danone's progress in its new areas.
"We believe investors will need to see further evidence of organic growth and margin momentum to agree with the CEO that Danone is 'uniquely placed to embrace the food revolution' and for its valuation discount to the sector to close fully."
Faber is confident Danone will deliver. "I am absolutely convinced our strategy creates value for the long-term but also the short-term," he said, adding he expected sales growth to improve in the third quarter.