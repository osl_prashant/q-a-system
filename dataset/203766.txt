Lima, Peru-based Camposol Holding said second quarter sales were off 6.1% because decreased volume of several commodities more than offset increased shipments of blueberries.
 
The company said the volume of all commodities sold in the second quarter of this year were totaled 17,597 metric tons, off 16.3% from year-ago levels, according to a news release. Despite increased volume of blueberries, the company said lower shipments of peppers, asparagus, artichokes, grapes, avocados, shrimp and other seafood products contributed to the sales decline, according to the release. The average price of commodities sold was $3.08 per kilogram ($1.40 per pound), up 12% from a year ago, according to the release.
 
"Second quarter results are in line with our expectations including the effects of our recently declared discontinued operations," Manuel Salazar Diez Canseco, CEO of Camposol Holding Ltd., said in the release. "We are keeping our renewed focus on our fresh and frozen business and a positive outlook for the year, especially on our avocado and our growing blueberry business."
 
The release said the company expects to continue its diversification strategy by increasing the production in the fruit and vegetable segment (blueberries) and seafood segment (conversion of shrimp ponds), as well as continue to emphasize direct sales to retailers.
 
The release said long-term growth prospects for exotic fruits and vegetables remain excellent, with avocados and blueberries consumption growing in key markets.