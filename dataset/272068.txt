Markets Right Now: Stocks move lower in midday trading
Markets Right Now: Stocks move lower in midday trading

The Associated Press

NEW YORK




NEW YORK (AP) — The latest on developments in financial markets (all times local):
11:45 a.m.
Major U.S. stock indexes are lower in midday trading as investors worry that growing costs for critical materials along with rising interest rates will slow down the economy.
U.S. bond yields are rising again Wednesday, setting more four-year highs, while oil prices are at three-year highs.
Goodyear Tire & Rubber fell 4.9 percent after warning of higher expenses for raw materials.
The S&P 500 index lost 8 points, or 0.3 percent, to 2,626.
The Dow Jones industrial average fell 103 points, or 0.4 percent, to 23,920. The Nasdaq composite gave up 15 points, or 0.2 percent, to 6,991.
Bond prices fell. The yield on the 10-year Treasury rose to 3.02 percent, the highest level since January 2014.
___
9:35 a.m.
Stocks are opening slightly lower, extending the market's losses, after several companies reported weak results or warned of higher costs.
Goodyear Tire & Rubber fell 2.9 percent early Wednesday after warning of higher expenses for raw materials.
Entergy fell 1.9 percent after reporting results that missed forecasts.
The S&P 500 index lost 11 points, or 0.4 percent, to 2,623.
The Dow Jones industrial average fell 97, or 0.4 percent, to 23,928. The Nasdaq gave up 25 points, or 0.4 percent, to 6,979.
Bond prices fell. The yield on the 10-year Treasury rose to 3.02 percent, the highest level since January 2014.