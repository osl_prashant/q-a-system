(UPDATED, Aug. 14) U.S. Vice President Mike Pence made hass headline news while visiting Colombia on his South American trip.
 
“Today I announce that the United States has reached an agreement for Colombia’s hass avocados to enter the US market,” Pence said in an Aug. 13 news release published on the website of Colombia’s Ministry of Commerce, Industry and Tourism. “I want to congratulate President (Juan Manuel) Santos and his government,“ Pence said during a news conference following his meeting with Santos during his visit to Colombia, according to the release.
 
The U.S. Department of Agriculture proposed market access for Colombia avocados in October and reopened a public comment period in January. According to the release, Colombia has sought avocado market access to the U.S. for more than a decade.
 
On Aug. 14, the USDA issued an online version of the final rule allowing U.S. imports of Colombia avocados, which will be posted in the Federal Register Aug. 15.  The rule is effective Sept. 15.
 
“The United States is a market with a lot of potential that in 2016 imported about $2 billion —  almost 90% were (brought in) from Mexico,” Colombia’s Minister of Commerce, Industry and Tourism María Claudia Lacouture said in the release. 
 
The opening of the U.S. market could double exports of Colombian hass avocados, according to the release. Colombia’s avocado exports are primarily shipped to Europe.
 
Colombia’s avocado exports increased from $171,000 in 2011 to more than $35 million in 2016, according to the release. Harvest of hass in Colombia continues most of the year, with peak supply between October and March.
 
Winning U.S. access will likely boost production of and investment in the avocado sector in Colombia, according to the release. Acreage of avocados has grown 126% in the last five years.
 
In July, Oxnard, Calif.-based Mission Produce announced it has a formal marketing and distribution agreement with Colombian avocado grower-packer Cartama.
 
In the assessment leading up to the comment period, the USDA reported Colombia would export 10,000 metric tons of hass avocados a year to the U.S. if approved.
 
U.S. growers are expected to suffer economic losses of $4 million to $6 million, or about 1% of the 2014-15 value of U.S. avocado imports.