For organics, foodservice remains a sleeping giant.
Higher costs and inconsistency of supplies often are cited as reasons restaurants tend to bypass organic produce when assembling their menus and filling out purchase orders.
There are signs that the giant is wakening, however, some suppliers say.
"We continue to grow our supply for the industry and continue to promote not only the retail part, but we're also seeing inroads with the foodservice side," said Doug Classen, sales manager for the Salinas, Calif.-based The Nunes Co.
Perhaps the most notable growth sector is quick-service outlets, Classen said.
"What we're experiencing now is the demographic is of the organics user is also looking to venture out and pick up pre-prepared salads and the like at fast food-type restaurants and so they can eat healthy," he said. "The mindset is that they want to go ahead and be able to eat within the organic realm of what's produced out there, so we are getting interest from the foodservice side so they can supply that to the consumer."
Organics, nevertheless, still is little more than a niche business for foodservice, said Rachel Mehdi, organics category manager for Vancouver, British Columbia-based The Oppenheimer Group.
"For foodservice concepts that are targeting high-end, health and environmentally-minded consumers, organics are a great fit with their clientele, and they are willing to pay the premium," she said.
Cost remains a factor, Mehdi said.
"It is also much easier for organic items that have more comparable prices to their conventional counterparts to be used in foodservice," she said. "It's great for a restaurant to show they carry organics and to advertise it on their menus."
Often, it's easier to do with items that don't require a substantial premium, such as organic spinach, Mehdi, said, citing one example.
"As organic produce continues to grow in popularity and production - assuming the premiums aren't quite as high as they are now - it will be more feasible for a wider array of foodservice entities to jump on the organic bandwagon."
Reliable availability may be the most stubborn barrier to organics' success in foodservice, said Scott Mabs, CEO of Porterville, Calif.-based Homegrown Organic Farms.
"If they go to something on their menu, they need it 365 days a year, so that doesn't even allow you to get to price in the conversation," he said.
Restaurants also continue to "fight the costs," which continue to rise, Mabs said.
Fallbrook, Calif.-based avocado grower-shipper Del Rey Avocado says there are ways to get fruit into one foodservice outlet or another.
Store delis are an example, said Patrick Lucy, vice president of organic sales.
"It's taking a little bit longer on the organic side, but there has been more demand for No. 2 organic, mostly by retailers, for in-store guacamole and sandwiches," he said.
Gains are slower in other foodservice outlets, but they are aware of the possibilities, Lucy said.
"More chefs are trying to feature organics," he said. "You are seeing on foodservice orders maybe five or six cases here or there where, a couple of years ago, you maybe didn't see that."
Location also can affect organic sales, said Earl Herrick, owner, president and founder of Earl's Organic Produce, San Francisco.
"I know my point of view is not shared universally, but we enjoy a very specific situation here," Herrick said. "There's a big economic boom in a longstanding sophisticated food culture. We represent a little bit of an outlier in that way."
Chefs are feeling very comfortable offering more organic options because all the customer feedback is positive, Herrick said.
"Retailers are getting more organic, and people are asking for more, and restaurants are offering it," he said.
Interest is spreading across all formats of restaurants, Herrick said.
"Mostly , I started with the white-tablecloth because they saw it as a way to maybe distinguish themselves, but with that demand, others have adopted it," he said. "It trickles down to every facet of what we do."
Chris Smotherman, salesman for Bakersfield, Calif.-based Kern Ridge Growers LLC, offered an alternative view.
"The one we've seen the most is for juicing, like the juice bar, as far as foodservice," he said. "I think those people are able to absorb the cost because of the product," he said.
White-tablecloth restaurants are heavily devoted to organic produce, but others are catching on, said Roger Pepperl, marketing director at Wenatchee, Wash.-based Stemilt Growers LLC.
"You will see entry in the everyday chains in the coming five years. It will happen fast," he said.