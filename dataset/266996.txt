The future of agriculture technology, which includes everything from robots who harvest fruits and vegetables to sophisticated tracking with blockchain, will be highlighted at the Produce Marketing Association’s Tech Knowledge Symposium.
The event is scheduled for May 2-3 in Monterey, Calif., and is the fifth Tech Knowledge Symposium. Registration is available online at the PMA website.
Science and technology represent the underpinnings of growth for the industry, said Bob Whitaker, PMA chief science and technology officer, on April 3.
“If we are going to be able to grow demand, if we are going to make these connections we want to make, if we want to be more sustainable, a lot of the answers lie in our ability to identify the appropriate business technologies and adapt them to our business strategies,” he said.
The Tech Knowledge event will help industry leaders do that, he said.
The agenda includes think-tank sessions, allowing attendees to talk about critical business pain points, according to the release.
Highlights of the event include:

Futurist and author Patrick Schwerdtfeger speaks on “Leveraging Technology Trends,” highlighting blockchain, collection and data analysis, artificial intelligence, robotics and other key technologies.
A panel talk about the “Technology Tidal Wave: Is it new?” with Steve Church, CEO of Church Brothers, Salinas, Calif.; Alec Leach, president of Taylor Farms, Salinas; and Vic Smith, CEO and owner of JV Smith Cos., Yuma, Ariz.
A series of “Lightning Learning Lab” sessions featuring 5-minute presentations on new tech products and services, moderated by Ed Treacy, vice president of supply chain efficiencies for PMA.
Mike Steep, executive director of the Stanford Engineering Center for Disruptive Technology and Digital Cities, speaks about “How to Get Ahead of Disruptive Technology Now.”
Alison Grantham, director of food strategy and sustainability for Blue Apron, will speak about “Innovation in Tech: Evolution of Produce Buyers’ Perspectives.”

PMA will announce the 2018 winner of its annual leadership award, the Science & Technology Circle of Excellence Award.
“Tech Knowledge is a great event to help you think beyond the day-to-day of your business, and focus on what can be,” 2017 Tech Knowledge attendee Mike McGee, vice president of finance for L&M Cos., said in the release.