The Environmental Working Group has again released a version of its Dirty Dozen list, prompting industry objections that the list has been discredited by scientists, is not based upon risk and has now been shown to potentially discourage consumption of organic and conventional fruits and vegetables.
 
"In light of new science and information about how safety fears are impacting low income consumers, it is concerning that EWG still releases a Dirty Dozen list in 2017," Teresa Thorne, executive director of the Alliance for Food and Farming, said in a news release.
 
"If EWG truly cares about public health, it will stop referring to popular produce items that kids love as "dirty" and move toward positive, science-based information that reassures consumers and promotes consumption."
 
For 2017, the EWG said strawberries remain at the top of the list of the "Shopper's Guide to Pesticides in Produce, with spinach moving to second place, according to a news release. The list ranks of conventionally grown produce with the most pesticide residues, according to the group.
 
After strawberries and spinach, the 2017 Dirty Dozen list includes nectarines, apples, peaches, celery, grapes, pears, cherries, tomatoes, sweet bell peppers and potatoes. The group  released its first Dirty Dozen list in 1993.
 
"Eating plenty of fruits and vegetables is essential no matter how they're grown, but for the items with the heaviest pesticide loads, we urge shoppers to buy organic," Sonya Lunder, EWG senior analyst, said in the release. It is especially important to reduce young children's exposures to pesticides, she said in the release.
 
The group's Clean Fifteen list of produce least likely to contain pesticide residues includes sweet corn, avocados, pineapples, cabbage, onions, frozen sweet peas, papayas, asparagus, mangoes, eggplant, honeydew melon, kiwis, cantaloupe, cauliflower and grapefruit, according to a news release.
 
Industry pushback
 
"Any report that tells people to avoid eating apples is giving harmful advice," said Jim Bair, U.S. Apple Association president and CEO, in a statement. "Instead, we should be more concerned with increasing consumption of fruits and vegetables."
 
Bair said the American Heart Association, American Cancer Society, American Diabetes Association, the U.S. Centers for Disease Control and Prevention, Academy of Nutrition and Dietetics and Dietary Guidelines for Americans all advise consumers to eat more fruit.
 
The industry-backed Alliance for Food and Farming said it has requested reporters view the Dirty Dozen list in the context of the U.S. Department of Agriculture's Pesticide Data Program (PDP) Report, the report that EWG uses to help create its list. 
 

"This report shows that when pesticide residues are found on foods, they are nearly always at levels below the tolerances set by the U.S. Environmental Protection Agency," the USDA said in its 2015 annual report. Over 99% of the products sampled through PDP had residues below the EPA tolerances, according to the report.
 

The alliance said an analysis by a toxicologist with the University of California's Personal Chemical Exposure Program concluded that a child could eat excessive quantities of fruits and vegetables and suffer no effects from pesticide residues. For strawberries, a child could eat 181 servings or 1,448 strawberries in a day and still not have any effects from pesticide residues, Thorne said in the release.