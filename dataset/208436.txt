You’ve probably never heard of the Kuskokwim River.Yet the 700-mile-long waterway in southwest Alaska is the country’s longest free-flowing river and its ninth-largest, as measured by the discharge volume at the mouth of the river as it empties into the Bering Sea.
That size and breadth of the Kuskokwim helps explain how a gray whale could end up some 60 miles upstream last week, trapped in shallow water.
That whale’s fate can best be described as “controversial,” as it’s difficult to take sides in the aftermath of what happened to the wayward mammal.
According to reporting by radio station KYUK, a National Public Radio affiliate that describes itself as “Public Media for Alaska’s Yukon-Kuskokwim Delta,” the story was a feel-good tale focused on what took place on the banks of the river.
“Meat and blubber from the whale killed Thursday evening in the Kuskokwim River is currently being distributed to surrounding villages,” the story on the station’s website stated.
“As of 7:45 pm Saturday, about 40 people, including elders, adults, and children, surrounded the whale while several people worked to butcher the meat. The meat was first distributed to elders, who were seen driving away on four wheelers, carrying pieces of white blubber in trash bags.
There’s a lot to unpack there.
Putting the ‘Party’ in Hunting Party
First of all, it’s necessary to back up a bit.
The whale didn’t die from “natural causes,” which often happens to members of the species that get trapped in shallow estuaries or rivers. Once the whale was spotted, more than 40 boats manned by local residents spent an hour-and-a-half on the river, as KYUK described it, “using guns, seal harpoons, and whatever they had on hand [but] were unequipped to kill an animal of this kind.”
The whale was finally dispatched, but its carcass was too heavy to drag to land, and it sank to the bottom of the river.
Amateur video posted by the radio station showed dozens of outboard boats racing after the whale, while people fired rifles and threw seal harpoons at the creature.
Tellingly, after the whale was killed, only about five or six boats remained on the river trying to bring the animal ashore, which raises the question: Was this a ritual that mimicked ancient hunting traditions, or simply an exciting adventure, in which people got to participate in the “thrill” of hunting some mighty big game?
I don’t think the use of power boats and hunting rifles were part of Native hunters’ traditional methods of procuring sustenance.
After two days of trying, homemade hooks welded together in a local machine shop caught deep enough in the carcass that was lying in 30 feet of water to finally drag the giant animal from the bottom of the river, the station reported.
Once it was brought to the beach — with the help of a front loader — and before locals began cutting into the carcass, Napaskiak Honorary Chief Chris Larson said a prayer of thanks. The whale was given fresh water, and Larson noted that “the whale will be enough to feed the village,” especially after a summer that saw a low king salmon harvest.
At that point, the story got even more incongruous.
Defining ‘Right’ and ‘Wrong’
To butcher the whale, locals recruited local fire chief Bill Howell, who operates a meat processing business called Bill’s Meats, to help with the butchering. As the station reported, “Howell soon arrived with knives and began carving whale steaks.”
The meat was first distributed to tribal elders, several of whom were seen “driving away on four-wheelers, carrying pieces of white blubber in trash bags.” Again, hard to imagine how that fits into the Natives’ cultural traditions.
KYUK’s reporter noted that one woman, when asked how she would cook the whale meat, replied, “I’m not sure. I’m gonna start with Crisco.”
Other locals acknowledged that they’re Googling how to prepare the meat, or saying that they will call friends and family for advice.
As one would imagine, opponents of the killing and butchering have argued that whales are not a traditional food for indigenous tribes. Other tribal members supported killing the whale, stating that “whatever the river brings them is subsistence food.”
On one hand, killing a whale is always going to spark controversy among a majority of the population. You think Americans are distanced from agriculture and food production? That’s only happened over the last 50 years.
You’d have to go back to the 1830s to discover an era where ordinary citizens accepted without question the hunting and harvesting of whales, and that’s because they depended on whale oil to light their homes and businesses, not because they were eager to start cooking up a slab of blubber in a pan of melted lard.
Traditional cultures need to be respected. There is much wisdom to be gained from people who have spent centuries living off the land, and the sea.
But just as we no longer set fire to the prairies to drive wild game toward hunting parties, we no longer hunt down whales because they remain on the edge of extinction — plus, we’re not hurting for alternatives to sperm oil.
When power boats, modern rifles and front loaders are integral to a “ceremony,” it’s tough to argue that killing a marine mammal that inadvertently strayed upriver is a re-enactment of some ancient ritual.
Then again, it’s equally difficult to make the case that a better outcome to this incident would have been letting the whale slowly and “naturally” succumb, sink to the bottom of the river, and eventually rot.
Personally, I find merit on both sides of this controversy.
I just don’t want to be the taster who has to sample three-day-old whale blubber fried in Crisco.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.