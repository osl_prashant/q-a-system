China is upping the ante on weed control. The country changed allowable foreign material in soybean imports from 2% to 1% and has listed nearly 50 weed seeds forbidden from entering the country—this means no more letting weeds sneak through on the combine.
On David Nichols’ 3,600-acre Tennessee Delta farm, weed control is top of mind as resistant and hard-to control weeds threaten his crops.
“We went back to the old fundamentals we used 20 or so years ago before Roundup Ready,” Nichols says. “We’re mixing modes of action, using pre-emergent herbicides and trying new technologies.”
You have the chance to start your season off on the right foot. Use strategic planning, scouting and timely weed management to control weeds and reduce your weed seedbank for future seasons.
Find your target weeds. “Know what you have,” says Bill Johnson, Purdue Extension weed scientist. “If you don’t remember or you’re on new ground, talk to neighbors or CCAs.
“If you have marestail in the eastern Corn Belt assume it’s glyphosate and ALS resistant,” Johnson says. “If you have waterhemp, assume resistance to glyphosate and ALS with a better than 50% chance of PPO resistance; with ragweed assume glyphosate and ALS; and Palmer amaranth assume glyphosate and ALS, and PPO in the south.”
Plan a multistep attack. “We recommend a planned two-pass program with residuals in each application,” says Frank Rittemann, Bayer product manager for selective corn herbicides. “Have two effective modes of action in each application.”
Don’t rely on a single application to take care of all weeds.
“You don’t want to skimp on preemergent herbicides because you have so few options in post, especially in soybeans,” says Gordon Vail, Syngenta herbicide technical product lead. “If you skimp, you’ll have more weeds for the post application.”
Pre-emergent herbicides are your first line of defense. Make sure your application is timely so any growing weeds die, and consider residuals.
“When managing small seeded broadleaves, the best thing to do is not let them emerge since once they do they grow so fast,” says Ken Bennis, Dow AgroSciences market development specialist. “The best way to do that is spray more than one residual.”
Using at least two effective modes of action helps delay resistance. After pre-emerge, get out and scout to make sure application was effective and to time post application.
“Post emerge you could now have just three to five days and if you miss it you might not get your weeds controlled,” Vail says.
In a perfect scenario, you’ll treat weeds at or before they reach 4", but if weather gets in the way make sure you have a contingency plan.
“Things can change a lot; if I scout on May 25 and it rains for a week, some of these weeds will go from 2" to 6". I need flexibility to control those weeds,” Bennis says.
Understand label requirements and make sure they’re effective no matter when they’re sprayed—pre, post or even late. Don’t waste money on an application that won’t work—find an alternative control method.
“We try to sit down and plan out our program—and a plan just in case that one doesn’t work,” Nichols says. “In tight margins, we do have to consider the prices of chemicals, too, but being profitable means a clean crop.”
With stricter regulations on foreign matter and weed seed for exports, weed control is all the more critical.