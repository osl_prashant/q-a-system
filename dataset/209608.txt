“Cows are tremendous metabolic athletes,” says Garret Oetzel, a veterinary professor at the University of Wisconsin.But to support their 100+ pounds of milk performance in early lactation, they need energy, minerals and vitamins to help meet the large outf low of nutrients via milk after calving. Without such support, they are prone to six metabolic disorders that can knock them out of the game.
Hypocalcemia
Hypocalcemia, or milk fever, is one of the most commonly seen early lactation metabolic problems. While the number of hypocalcemia cases is going down, Oetzel says, subclinical cases are still very high.
At the start of lactation, cows have a difficult time maintaining calcium homeostasis because colostrum and milk are high in the mineral. They might draw down bone calcium to survive, and a negative balance can exist for as long as four months.
Hypocalcemia affects the cow by decreasing skeletal muscle function leading to a higher risk of injury. Smooth muscle function can also be reduced, leading to lower gastrointestinal motility and even a displaced abomasum.
A down cow should be treated with an IV of calcium, while cows that are standing with hypocalcemia should get oral treatment, Oetzel says.
Supplementation of magnesium through the diet is important because it helps with processing Vitamin D and lowers the risk of hypocalcemia, Oetzel adds. Cows need a dry matter diet with 0.30% to 0.45% magnesium or roughly 40g to 50g of magnesium per day.
Hypophosphatemia
Following hypocalcemia, cows can incur hypophosphatemia when the parathyroid hormone causes urinary and saliva loss of phosphorus. The salivary phosphorus pools in the rumen and can’t be readily utilized.
To fix hypophosphatemia, a cow must first be rid of hypocalcemia. When this happens almost 95% of cows will be fine, Oetzel estimates.
Those cows that don’t come out of it when hypocalcemia conditions are corrected will need treatment. An oral dosage of 0.5 kg of sodium monophosphate in 7.5 liters of warm water or an IV of 30 grams of sodium monophosphate in 300 mL distilled water are treatment options.
Hypomagnesemia
For producers who run cattle on grass, hypomagnesemia —also known as grass tetany—can be something to look for in the spring when grazing lush pastures. Fresh forage growth can be high in potassium but low in magnesium.
In these instances, the high rate of potassium intake might be limiting magnesium uptake.
The condition can occur in confinement when cattle are fed forage originating from soils low in magnesium, but high in potassium. To prevent hypomagnesemia, Oetzel recommends the diet have a potassium to magnesium ratio less than 4:1 and to feed additional magnesium as needed.
Hypokalemia
Switching from having too much potassium compared to magnesium, cows can get too little potassium and develop hypokalemia. Low blood potassium levels can lead to a f laccid paralysis where cows are unable to move followed by a sudden death.
Hypokalemia traditionally happens during the first two to three weeks of lactation and in cows with a history of ketosis. For cows with blood potassium levels between 2.5 and 3.5 mEq/L and still able to get up on their own, an oral potassium chloride treatment of 100g once daily should correct symptoms.
Down cows with blood potassium lower than 2.2 mEq/L need to be treated orally with 200g of potassium chloride twice daily.
Ketosis
The prevention of hypokalemia means the prevention of ketosis, Oetzel says.
Along with subclinical hypocalcemia, ketosis is the most common metabolic disorder seen in dairy cows, he adds. Ketosis is the elevated concentration of blood ketones. It typically occurs during the first six weeks of lactation. Around 30% to 40% of dairy cows will develop ketosis at some point in their lives.
Oetzel says negative impacts from ketosis include a drop in milk production, more displaced abomasums, increased culling and lower breed back rates.
A cow-side test can be performed to measure blood BHB and determine if ketosis is happening. Milk and urine tests are available, too.
For treatment of a mild ketosis case, Oetzel recommends using an oral glucose precursor, either 300 mL of propylene glycol or the same dose of glycerol once daily. In severe cases, an IV of 50% glucose at 250 mL followed by oral treatment is recommended.
Displaced abomasum
Prior metabolic diseases such as hypocalcemia and ketosis are primary culprits for displaced abomasum.
“We can reduce displaced abomasum with early detection and early treatment (of hypocalcemia and ketosis),” Oetzel says.