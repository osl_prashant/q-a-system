By: Evan Anderson, University of Florida Extension
If you grow or purchase hay to feed livestock, you probably know that not all hay is created equal. There are a number of factors that contribute to the quality of the bale you end up with. If you’re relying on hay to provide your animals with the nutrition they need, it pays to take care when managing your hay pastures or deciding which hay to purchase.
First, the type of forage plays a role. Different crops have different average levels of crude protein and total digestible nutrients; in general, legumes and cool-season grasses are often higher quality than warm-season grasses. The way forage crops are treated while growing makes a difference, too. Fertilize your forage properly while it’s growing, and what you add will translate to more available nutrients for your livestock. Next, hay should be cut at the proper time. Let the forage grow too long, and it becomes tough, full of lignin and stems. This not only reduces the quality of the hay, but also makes it less palatable.
To determine the quality of your hay once it’s made, get it tested. A forage testing lab can tell you exactly what’s in your final product, ensuring that you are able to tailor your feeding program to give your livestock the right nutrition.
All this might seem like a lot, but it’s just the beginning. A surprising amount of quality can be lost, from even the best hay, after it has been cut and baled. Proper storage has a huge impact on not only the quality of the hay,  the health of your livestock, and to your wallet.

You can lose up to a 50% of the nutrients from improperly storing hay.
What does proper hay storage look like? Start by making or buying well-made bales that are dense, so they can shed water and reduce weathering losses. A loose bale lets water and air in, which leeches out nutrients. In a 5 foot diameter bale, the outer 4 inches accounts for 25% of the bale. If only that outer layer becomes weathered, you’ve lost up to one-quarter of the money you spent.

Source: OK State Round Bale Hay Storage BAE-1716
To help avoid this, wrap or cover the bales. Yes, bales that are high in moisture may need to dry in the sun for a day or two. Moisture levels above 20% are dangerous; mold can grow in damp bales, which can lead to sick animals or even spontaneous combustion of the bale. Once they’ve dried, however, they should be moved to shelter as soon as possible. A roof overhead is best, so a good pole barn will pay for itself eventually. If that’s not an option, try covering the bales with a tarp or plastic. Keep them off the ground if possible, on racks, tires, gravel, or at least on well-drained soil.  Treat your hay well, and the extra work and investment will pay off in the long run.

Source: OK State – Round Bale Hay Storage BAE-1716
 
For more information, contact your local Extension office, or use the following links to fact sheets related to this subject:

Minimizing Losses in Hay Storage and Feeding
Round Bale Hay Storage
Factors Affecting Forage Quality
Implications of Round Bale Dimensions on Hay Use
Harvesting, Storing, and Feeding Forages as Round Bale Silage
Forage Testing