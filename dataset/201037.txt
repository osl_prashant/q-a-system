Crop calls
Corn: 1 to 2 cents higher
Soybeans: 3 to 5 cents higher
Wheat: 4 to 8 cents higher

Wheat futures have taken the lead to the upside in the crop markets this week and that remained the case overnight. Concerns that rains were too late across areas of the Northern Plains this week to "save" the crop continue to provide support. Traders are also noting rains slowing harvest in Kansas this week as a concern given the already tight supply of high-protein wheat for blending. Soybean futures have returned to the top of the two-week trading range, but corn is still working on weekly losses. Updated weather models will influence those markets, as some rains were taken out of the forecast for the weekend in yesterday's weather model run, which contributed to the late-session recovery. Also supportive for corn this morning, USDA announced Mexico has purchased 120,000 MT of corn for 2017-18.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
This week's cash cattle market deterioration is priced into futures, which should limit pressure as traders even positions ahead of the weekend. But bears clearly hold the near-term advantage as technical damage has been done this week. Cash cattle trade started the week $3 to $5 lower and was as much as $9 lower yesterday... and may not be done going down as volume has only been moderate. The opposite is true for the cash hog market, as bids have improved as the week progresses. Momentum could slow today as packers have supplies secured for tomorrow's kill, but a firmer tone in the cash market should limit pressure on hog futures this morning.