Technical traders like Mike Florez, of Florez Trading, watch futures charts rather than fundamentals indicators like yield and harvest progress. This week, the charts are pointing toward movement in wheat markets. Florez says tools like the Bollinger bands and timing cycle are matching up. Recently, Florez walked AgDay TV host Clinton Griffiths through what to watch for from a technical standpoint and how to use these tools together. 
See the video above.
To read and watch previous videos from Mike Florez and Clinton Griffiths click the links below:
Watch Charts Like a Trader 
Forget the Fundamentals, Use the Charts