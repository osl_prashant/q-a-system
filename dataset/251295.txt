Blizzards in the Dakotas and New England is bracing for its second nor’easter in a week make it seem as though warmer, drier weather might never make it.

But be careful what you wish for.

Michael Clark, meteorologist for BAMwx.com, thinks a stretch of dry weather is in store for the next month to month-and-a-half. On AgriTalk After The Bell, he told host Chip Flory it will progressively get drier, and some places might not see rain at all.

“A lot of research has hinted that drier risks would evolve as we rumble into spring and summer,” he said. “It’s interesting to see how dry the models are turning over the next 15 days.”

March is usually a topsy-turvy month when it comes to weather, but Clark is expecting the temperatures to soon moderate.

Part of the reason behind this change is the north Pacific Ocean. Clark says there is a lot of ridging and high pressure in that area. He has his eye on a target a little bit further away: the Indian Ocean.

Some weather models are showing some “unique phases” that correlate to dry years, like 2012.

“Everything going on in the atmosphere is starting to push us towards a drier regime,” said Clark. “There’s a lot going on that’s working toward that interestingly enough.”

With the dry, warmer weather on the horizon, Clark says he wouldn’t be surprised if the majority of farmers start getting in the fields the first week of April.

“Weather-wise, I do think that can happen,” he said. “There are plenty of risks for drier weather in our forecasts.”

This dry weather could happen before we know it, but how long will it last? According to Clark’s research, into the growing season. The drought and dryness in the northern and southern Plains can spread.

“There’s no sign of any relief [there],” he said.

Clarks explain the El Niño-Southern Oscillation (ENSO) weather pattern and how that’s another indicator of a dry year as well as the weather patterns in the Indian Ocean on AgriTalk After The Bell above.