Chicago Mercantile Exchange live cattle closed moderately higher on Tuesday, following late-day short-covering and bargain hunting after three sessions of losses, said traders.
Futures initially drew more support from their lower price compared to last week's slaughter-ready, or cash, cattle returns.
Market advances were limited by funds in CME's livestock markets that track the Standard & Poor's Goldman Sachs Commodity Index. Those funds "rolled," or periodically sold February positions and simultaneously bought deferred months.
Tuesday was the second of five days for the roll process.
February live cattle finished 0.450 cent per pound higher at 117.675 cents. April ended up 0.225 cent at 119.525 cents.
Packers last Friday and so far this week paid $119 to $120 per cwt for cash cattle in the U.S. Plains, down from mostly $123 the week before.
Feedlots sold livestock for less money on concerns that warmer weather in the U.S. Plains this week might unleash cattle that had backed up in feedyards during last week's arctic blast, said analysts and traders.
Bitter cold tends to slow down animal weight gains and impede transportation of cattle to packing plants. Moderating temperatures allow livestock to gain weight quicker and is conducive for sending animals to market.
Bullish futures traders believe improved packer profits and strong wholesale beef prices may encourage processors to pay more for supplies this week.
Market bears said improved weather conditions, and the lack of significant cattle sales to packers last week, might weigh on cash prices.
Wednesday's Fed Cattle Exchange sale of 711 animals could set the tone for overall cash prices in the Plains this week.
CME feeder cattle ended mostly higher after traders sold the January contract and simultaneously bought deferred months.
January feeder cattle closed down 0.975 cent per pound to 145.475 cents. March ended up 0.825 cent to 142.750 cents, and April finished 0.650 cent higher at 142.675 cents. 
Hog Futures End Mixed
CME February lean hog futures closed up modestly, underpinned by strong cash and wholesale pork prices as packers and grocers shore up inventories for the week, said traders.
They said profit-taking and future's overbought technical condition limited market advances and pressured some trading months.
February hogs settled up 0.200 cent per pound at 73.175 cents, and earlier hit a new high for the contract of 73.450 cents. April ended down 0.025 cent at 76.775 cents.