BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Tuesday:
OpenHighLowSettleChg.ORANGE JUICE15,000 lbs.; cents per lb.May139.60139.70137.45138.35—.85Jun139.35—.75Jul140.70140.70138.35139.35—.75Aug140.15—.65Sep140.10140.15139.20140.15—.65Nov141.00141.00140.05140.90—.50Jan141.60—.50Feb142.20—.40Mar142.20—.40May142.95—.40Jul143.05—.40Sep143.15—.40Nov143.25—.40Jan143.35—.40Feb143.45—.40Mar143.45—.40May143.55—.40Jul143.65—.40Sep143.75—.40Nov143.85—.40Jan143.95—.40Feb144.05—.40Mar144.05—.40Est. sales 1,024.  Mon.'s sales 1,614Mon.'s open int 13,385,  up 225