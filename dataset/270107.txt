AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:15 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
UPCOMING TOMORROW:
SCHOOL GUN PROTESTS-COLUMBINE
DENVER — Students pushing for gun control are expected to walk out of classes on Friday, the 19th anniversary of the Columbine shooting, at high schools across the country — but not at Columbine. Classes will be canceled at the Colorado school that has long been a symbol of the school violence the youth movement is trying to end, just as they have been every April 20 since the 1999 massacre, with the school sticking with its tradition of holding a day of service.
TOP STORIES TODAY:
COLORADO RIVER-WATER DISPUTE
Tension over the drought-stressed Colorado River escalated into a public feud when four U.S. states accused Arizona's largest water provider of manipulating supply and demand, potentially threatening millions of people in the United States and Mexico who rely on the river. The four states — Colorado, New Mexico, Utah and Wyoming — plus Denver's water utility said the Central Arizona Project was trying to avoid a reduction in its share of the Colorado River while others are voluntarily cutting back to avoid a crisis amid a prolonged drought. By Dan Elliott. SENT: 750 words, photos.
XGR-IMMIGRANT DRIVER'S LICENSES
DENVER — The Colorado House on Wednesday passed a bipartisan bill aimed at boosting the ranks of the state's agricultural workforce by letting immigrants who are in the country illegally renew their driver's licenses online or by mail. The bill is supported by dairy, cattle and produce farmers and seen as an effort to overcome objections that granting the licenses validates efforts to enter the U.S. illegally. By James Anderson. SENT: 560 words, photo.
OF COLORADO INTEREST:
WILDFIRES
OKLAHOMA CITY — Frazzled residents in a part of rural northwestern Oklahoma paralyzed by days of wind-whipped wildfires expressed fear a blaze could overrun their home Wednesday, as firefighters battling the deadly fires there and in Colorado, Kansas and New Mexico found some relief at the promise of easing of dangerous weather conditions. Hundreds of people across the region have been forced to evacuate their properties, homes have been swallowed by the fires and cattle burned to death as they stood in rivers and streams, presumably seeking respite from the flames. Two people have died and at least 9 injured in the Oklahoma fires. By Ken Miller. SENT: 620 words, photos.
HOMEBUYING SEASON-RISING RATES
LOS ANGELES — Higher mortgage rates are making the already challenging task of buying an affordable home even tougher for many Americans this spring. In metro areas such as Denver, buyers are rushing to close a deal before mortgage rates get too high. In Dallas, some are embracing longer commutes to find homes they can afford. And in places such as Los Angeles, where the number of homes for sale is down sharply from a year ago, sellers routinely receive multiple offers. By Alex Veiga and Larry Fenn. SENT: 1470 words, photos and graphic. (Abridged version also available)
With: HOMEBUYING SEASON-SURVIVAL GUIDE
CHINA-SORGUM TARIFF
OMAHA, Neb. — American sorghum farmers fear they will lose their largest export market if China follows through with a tariff on their crop. China imposed preliminary anti-dumping tariffs of 178.6 percent on U.S. sorghum this week as part of its ongoing trade dispute with the U.S. By Josh Funk. SENT: 380 words, photo.
MOLECULAR EXERCISE
BATON ROUGE, La. — About 120 scientists from around the U.S. are in Louisiana as part of a six-year, $170 million National Institutes of Health program to study the molecular nitty-gritty of exercise. Decades of research have shown that exercise is good for people, but scientists don't know what's going on at the body's most basic level. SENT: 470 words.
IN BRIEF:
— COLORADO WILDFIRES-THE LATEST — A southern Colorado wildfire destroyed two more homes Wednesday morning, bringing the total lost to 12.
— STOLEN SNO-CAT-DUKES OF HAZZARD — A 27-year-old Colorado man has been arrested in the theft of a Dukes of Hazzard-themed Sno-Cat on a trailer from outside a restaurant near Vail last month.
— HOSPITAL-TRAUMA CARE STATUS — A Colorado Springs hospital has become the only medical facility outside the Denver area to be designated with the state's highest trauma care status.
— POT SHOPS-FAKE IDS — Breckenridge municipal court says local marijuana shops helped in at least 178 fake ID cases since January 2017. The Summit Daily reported Tuesday that Breckenridge pot shops have been tough on underage shoppers coming in with fake IDs.
— NOT REAL NEWS-MCDONALD'S MARIJUANA — Colorado McDonald's franchises not converting play areas into pot-smoking centers. With AP Photo.
— MARIJUANA-NEVADA-UTAH — The legal sale of recreational marijuana on the Nevada-Utah line may not be banned after all. The mayor of West Wendover, Nevada is using his veto power to put the question back on the table.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for May rose 9 cents at 4.7525 a bushel; May corn was up 2.75 cents at 3.83 a bushel; May oats gained 7.25 cents at $2.36 a bushel; while May soybeans was off 4.25 cents at $10.4175 a bushel.
SPORTS:
PREDATORS-AVALANCHE
DENVER — The Colorado Avalanche trail Nashville 2-1 heading into Game 4 on Wednesday night. The home team has won every game so far in this first-round series. By Pat Graham. UPCOMING: 750 words, photos. (Game starts at 8 p.m. MT)
ROCKIES-PIRATES
PITTSBURGH — David Freese stressed the need of a culture change inside the Pittsburgh Pirates clubhouse during spring training, one that focused more on creating a greater sense of urgency and accountability when things go awry. Given a rare start with his club facing the prospect of getting swept by Colorado, Freese backed up his words with his bat. The veteran third baseman worked a two-out walk in the fourth to start a rally and added a two-run double as the Pirates had little trouble with the Rockies in a 10-2 win on Wednesday. By Will Graves. SENT: 780 words, photos.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.