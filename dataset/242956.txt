AP-VT--Vermont News Digest 6 pm, VT
AP-VT--Vermont News Digest 6 pm, VT

The Associated Press



Vermont news from The Associated Press for Saturday, March 3, 2018.
Here's a look at how AP's general news coverage is shaping up today in Vermont. Questions about today's coverage plans are welcome, and should be directed to the northern New England desk at 802-229-0577.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking news and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow.
UPCOMING:
FOOD AND FARM-DAIRY FARMERS-SUICIDE
MONTPELIER, Vt. — Accompanying the routine payments and price forecasts sent to some Northeast dairy farmers last month were a list of mental health services and the number of a suicide prevention hotline. The Agri-Mark dairy cooperative got the resources out to its 1,000 farmers in New England and New York following the suicide of a member farmer in January, and one the year before. By Lisa Rathke. AP Photos GFX5903-0302182134, BX601-0202121028.
NORTHEAST STORM
BOSTON — People along the Northeast coast braced for more flooding during high tides Saturday even as the powerful storm that inundated roads, snapped trees and knocked out power to more than 2 million homes and businesses moved hundreds of miles out to sea. Areas from Maryland to Maine remained under flood warnings. Officials in eastern Massachusetts, where dozens of people were rescued from high waters overnight, warned of another round of flooding during high tides expected around noon. AP Photos NY120-0302181540, NJATL201-0302181223, PAPOE201-0302181558.
IN BRIEF:
VERMONT YANKEE: The head of the Amtrak passenger rail service says efforts are underway to help ensure the service on the Vermonter train will continue.
FATAL SHOOTING:  A judge has declined to dismiss a manslaughter charge against a Vermont man who told his friends a gun was unloaded before it was used to kill one of them.
AMTRAK VERMONT: The head of the Amtrak passenger rail service says efforts are underway to help ensure the service on the Vermonter train will continue.
WINTER HIKES: The Green Mountain Club is holding its 22nd annual winter trails day with winter hiking, animal tracking and other outdoor winter activities. The event takes place Saturday at its headquarters in Waterbury Center with guided winter hikes happening throughout the day on nearby trails.
COASTAL STORM-VERMONT: Vermont's Green Mountain Power is sending 43 line workers, supervisors and mechanics south to lend assistance after a major coastal storm. Officials said power was restored to 8,000 Vermont homes and businesses on Friday, and resources are now available to assist elsewhere.
DAIRY DRIVER FIRED: Officials say a dairy truck driver caught on surveillance camera urinating near dairy cows in Vermont has been fired. Agriculture Secretary Anson Tebbetts said investigators are looking into the matter. Local police also have been contacted.
HIKER FALLS THROUGH ICE: The search continues for a hiker who fell through the ice near Bingham Falls in Vermont. Officials say two people were hiking Friday near the falls when one fell through the ice on Friday on the West Branch of the Little River in Stowe. A rescue effort continued until nightfall. On Saturday, the effort was shifting to a recovery operation.
___
If you have stories of regional or statewide interest, please email them to apvermont@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867.