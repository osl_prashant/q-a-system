U.S. soybean growers are trying to play to President Trump’s pain point of a growing trade deficit and argue that avoiding a trade war can help the trade gap.

According to Politico, last week Trump asked his cabinet secretaries to hit China with steep tariffs in response to allegations of intellectual property theft, and those tariffs would be placed on more than 100 Chinese products.

In a letter to President Trump, the American Soybean Association (ASA) urged him to either modify or reverse his 25 percent tariffs on steel and 10 percent tariffs on aluminum imports.

For the last 20 years, U.S. soybeans have contributed more to the U.S. trade balance than any other agricultural product, according to the ASA.

Soybean trade between the U.S. and China is valued at roughly $14 billion each year.

“U.S. supply is one of the most important resources for all the soybean importers or buyers from China because the U.S. market is very open—there’s the futures market in the U.S.—the systems have been very efficient,” said Patrick Yu, president of Cofco Corp.
In an article from Bloomberg, Yu says the Chinese government is encouraging businesses to buy U.S. soybeans.

Patrick Delaney, CEO of the ASA, says Chinese retaliation would send shockwaves through the farm economy since China purchases 52 percent of the U.S. crop.