Consumer demand for chili peppers continues to grow, as the category breaks through cultural and culinary barriers, suppliers say.“It has been for years now. In fact, back in 2014, we decided to do a cookbook just on the category alone, as Americans’ taste for chili has increased beyond the Hispanic and Asian consumer,” said Robert Schueller, director of public relations for Los Angeles-based World Variety Produce, which markets under the Melissa’s brand.
As it published the Melissa’s The Great Pepper Cookbook, the specialty produce supplier has watched the fresh chili category increase 10% to 15% annually over the last five years, Schueller said.
Others say their own chili sales have risen consistently.
“Overall, our peppers are trending upwards,” said Andrea Dubak, marketing specialist with Calgary, Alberta-based Thomas Fresh. “Jalapenos are the most popular, due to consumer knowledge, followed by poblano peppers,” she said.
Oxford, N.C.-based Bailey Farms Inc. focuses on specialty peppers, which include chilis, Bellafina Baby Bells and mini-sweets, said Randy Bailey, president.
“The category of chilis is growing somewhat due to the interest in adding more heat and flavor to food,” he said. “Sales are no longer skewed heavily to ethnic markets.”
Shipments of frozen chili peppers have shown steady growth, but so have fresh volumes, said TJ Runyan, president of Mesilla Valley Produce in Las Cruces, N.M.
“For us, it’s on a continual growth pace,” he said. “More chains are carrying it and in more restaurants — you see a lot more there.”
Runyan grows chili peppers in the heart of New Mexico’s Hatch chili region, roughly a 20-square-mile area around Hatch, N.M., near the Rio Grande.
“Even if it’s just seasonal during the Hatch season, you’re seeing more and more (product) carried year-round, especially in the Southwest, outside the New Mexico border. As green chili or Hatch peppers, it becomes more a day-to-day thing, so you’re seeing some market maturity with some stuff, and as it makes it way east and north onto more menus, it’s driving fresh sales behind it.”
The Southwest and the West are the biggest markets for Hatch peppers, but sales are spreading across the U.S., Runyan said.
“We’ve got people as far away as Boston and Florida wanting peppers,” he said.
Chris Franzoy, owner of Hatch-based Young Guns Produce Inc., Billy the Kid LLC and the Hatch Chile Factory, says he has seen the same sales trend for his products, particularly during the summer peak production season.
“Most retailers across the country promote our peppers during August,” he said.
Arrey, N.M.-based Desert Springs Produce, reports steady growth across the U.S.
“It’s gaining popularity up along the Northeastern Seaboard, as well as the Midwest,” salesman Bill Coombs said, noting that his company grows mild, medium, hot and extra-hot peppers. “Demand every year increases. We look forward to another good year.”
Hatch varieties have carved a niche for themselves, said Edward Ogaz, co-owner of Anthony, N.M.-based Seco Spice.
“A lot of our Hatch varieties seem to be in big demand,” he said. “People think of Hatch.”
There is chili production outside of New Mexico, as well.
Raleigh, N.C.-based L&M Cos., for example, grows peppers in Florida and Georgia and ships product in from Mexico, said Hurley Neer, sales and operations manager for Eastern vegetables.
“We’re finding that chilis are working across the country. There’s good demand for them in several markets,” he said.
L&M tries to provide numerous variety options, to suit the tastes of specific marketplaces, Neer said.
“Cubanelle is an item we do a lot of export to Puerto Rico. They’ll take loads of that item, but they may only take a pallet of jalapeno,” Neer said.
Overall, chili peppers have transcended the niche classification, Neer said.
“It’s more a staple across the country now. You don’t have a particular region you focus on,” he said.
Alex Jackson Berkley, senior account manager with Los Alamitos, Calif.-based Frieda’s Inc., agreed.
“Chili peppers are no longer a specialty item, and more and more you will see them integrated into retail displays with other colored, mild peppers,” she said. “We are also seeing the incredible growth in mini sweet peppers and long mild peppers.”
Ultra-hot peppers, such as the Trinidad scorpion, Carolina reaper and ghost peppers, have developed “a cult-like following, and retailers are no longer afraid to have these peppers as part of their offering,” Berkley said.