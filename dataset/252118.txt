Missouri turkey farm quarantined after bird flu detected
Missouri turkey farm quarantined after bird flu detected

The Associated Press

ST. LOUIS




ST. LOUIS (AP) — The U.S. Department of Agriculture says the first commercial case of bird flu in the country this year has been confirmed at a turkey farm in southwestern Missouri.
USDA spokeswoman Lyndsay Cole said Thursday that the H7N1 avian influenza, a low-pathogenic form, was detected through pre-slaughter testing on a farm in Jasper County that houses 20,000 turkeys. The state put the farm under quarantine.
Cole says two other commercial poultry properties within 6 miles of the farm tested negative for influenza.
The low-pathogenic flu is different from the high-pathogenic virus that resulted in the loss of nearly 50 million birds in the Midwest in 2015. Cole says the low-pathogenic virus poses no risk to the food chain.
Jasper County is about 140 miles south of Kansas City, Missouri.