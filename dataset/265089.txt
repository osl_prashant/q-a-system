AP-CO--Colorado News Digest, CO
AP-CO--Colorado News Digest, CO

The Associated Press



Colorado at 6:45 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
UPCOMING TOMORROW:
CHEMICAL WEAPONS-DESTRUCTION
DENVER — The U.S. Army wants to change the way it will destroy some of its chemical weapons stockpile in Colorado, switching to closed detonation chambers instead of a highly automated, $4.5 billion plant built to handle the job. The shift worries some members of a government-sanctioned citizens advisory group, who say it will be harder to control gases produced by detonation.
STUDENT GUN PROTESTS-INFRASTRUCTURE
DENVER — Hiding from a gunman at their high school, a small group of teenagers resolved on their own to launch a crusade against gun violence. Now they have joined forces with a well-financed and energized liberal infrastructure that's been waiting for sympathetic faces for the gun control movement — leading to attacks that the kids have become pawns of well-heeled activists.
TOP STORIES TODAY:
TERRORISM SUPPORT-COLORADO
DENVER — U.S. prosecutors told a jury Thursday that a Philadelphia man frustrated by mistreatment of Muslims used codes and other deception to hide a $300 payment in support of a terrorist group, but defense attorneys said the native of Uzbekistan was only paying down a debt to an acquaintance. Opening statements marked the start of a Denver trial that is expected to include testimony from international witnesses using aliases and disguises, transcripts of phone and email conversations in at least three languages and a team of translators dedicated to helping the defendant understand the proceedings. By Kathleen Foody. SENT: 470 words.
COLORADO SENATE-SEXUAL MISCONDUCT
DENVER — Colorado's Democratic Senate minority leader stepped down from that post Thursday, citing frustration with majority Republicans' handling of workplace harassment allegations. Sen. Lucia Guzman of Denver has struggled this Legislative session with Senate Republican leaders over the handling of sexual misconduct allegations against three Republican lawmakers. By James Anderson. SENT: 510 words, photo.
INMATE ESCAPE
DENVER — Denver police officers who opened fire killing one man and injuring another after a high-speed car chase erroneously believed an escaped inmate was a passenger, authorities said Thursday. The driver was killed and a passenger survived, but police quickly learned that the man they were hunting, Mauricio Venzor-Gonzalez, was not inside, Denver Police Commander Barb Carver told reporters. Police had released few details about the Monday evening shooting until Thursday, but described both men in the car as "associates" of Venzor-Gonzalez. By Kathleen Foody. SENT: 690 words.
OF COLORADO INTEREST:
BUDGET BATTLE-WILDFIRES
WASHINGTON — A spending bill approved by the House includes a bipartisan plan to create a wildfire disaster fund to help combat increasingly severe wildfires that have devastated the West in recent years. The bill sets aside more than $20 billion over 10 years to allow the Forest Service and other federal agencies end a practice of raiding non-fire-related accounts to pay for wildfire costs, which approached $3 billion last year. By Matthew Daly. SENT: 710 words, photo.
ENDANGERED FISH-COLORADO RIVER
FLAGSTAFF, Ariz. — An endangered fish that makes its home in the Colorado River basin no longer is at the brink of extinction. The U.S. Fish and Wildlife Service said Thursday it will consider reclassifying the humpback chub as threatened within the next year. By Felicia Fonseca. SENT: 520 words, photos.
AUSTRALIA-HELICOPTER CRASH
BRISBANE, Australia — A helicopter carrying American tourists crashed at a coral-viewing site on Australia's Great Barrier Reef, killing two passengers from Hawaii and injuring two others from Colorado, police said. The helicopter pilot pulled one passenger from the wreckage after Wednesday's crash but desperate attempts to revive the 65-year-old woman failed, Police Inspector Ian Haughton said Thursday. A 79-year-old man also died despite bystanders' attempts to resuscitate him, Haughton said. SENT: 240 words, photos.
IN BRIEF:
— FLAMING T-REX — The co-owner of a dinosaur-themed park in southern Colorado thinks an electrical malfunction caused a life-sized animatronic Tyrannosaurus Rex to burst into flames. (With AP Photo)
— MOTHER KILLED-THE LATEST — The second trial for a Colorado man accused of killing his wife is continuing after the judge refused to declare a mistrial.
— SHIPPING MARIJUANA — A third man has pleaded guilty to trying to FedEx 11 pounds (5 kilograms) of marijuana from Colorado to a Mississippi home.
— BIRTHING CENTER-DEATHS — A natural birthing center in North Carolina has stopped delivering babies after the deaths of three newborns in the past six months.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for May rose 2.25 cents at 4.5575 a bushel; May corn was up 1 cent at 3.76 a bushel; May oats was off 2.75 cents at $2.2875 a bushel; while May soybeans was unchanged at $10.2975 a bushel.
SPORTS:
ROCKIES PREVIEW
DENVER — Charlie Blackmon didn't need much baiting: The lure of a fishing contest in the middle of spring training was incentive enough. So the quirky Colorado Rockies outfielder/avid outdoorsman slipped into some waders and grabbed his fishing rod for a game to see which player could most accurately cast at cans set up on the turf. Anything for team bonding. By Pat Graham. SENT: 750 words, photos.
KINGS-AVALANCHE
DENVER — The Colorado Avalanche host the Los Angeles Kings. (Game starts at 7 p.m. MT)
BKC-COLORADO STATE-MEDVED
FORT COLLINS — Former Colorado State assistant coach Niko Medved is returning to Fort Collins to lead the Rams men's basketball program after spending a year as Drake's head coach. Medved replaces Larry Eustachy, who stepped down last month, ending a "climate assessment" of the program led by athletic director Joe Parker. SENT: 220 words, photo.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.