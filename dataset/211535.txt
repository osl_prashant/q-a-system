With a southern drawl and one-liners at the ready, Charles Wingard was one of the stars of the United Fresh Produce Association’s 2017 Washington Conference. 
The conference had another strong year, with more than 450 attendees and 140 Hill visits, according to United Fresh. 
 
Wingard, vice president of field operations for Walter P. Rawl and Sons, Pelion, S.C., and chairman of United Fresh the government relations council for United Fresh, was showcased during a Sept. 19 session called “Crafting our message on Capitol Hill.”
 
He was the only industry member on that panel, which included United Fresh execs Tom Stenzel, Robert Guenther, Jennifer McEntire and Mollie Van Lieu.
 
You can tell that Wingard has done his homework on legislative issues throughout the year and not just when he finds himself in DC.
 
In fact, he said Walter P. Rawl likes to connect with lawmakers and their staff in the home district, including meeting them for breakfast or lunch when they are in town. Letting politicians see the farm first hand is another go-to move.
 
He told the story of letting one Capitol Hill lawmaker or staffer (he didn’t say who) drive a tractor during through a cornfield during cultivation season.
Evidently, the politician wasn’t an experienced farm hand and knocked over several hundred feet of corn plants. While that blunder lost the farm several hundred dollars, Wingard said he got more mileage out of that 600 feet of corn than almost anything else.
 
Some of Wingard’s sayings:
“These guys (United staff) are good, but they can’t tell your story like you can tell your story.”
“Today is a short-term deal. When you go home you can work on this in the long term, and by that I mean you need to build and enhance your relationships with senators and representatives back home.”
On accounting for opposing views: “I’ve never cut a piece of wood so thin that it didn’t have two sides.”
“Find something to thank (lawmakers) for. If you can’t find anything else, just thank them for their service.”
“The time to fix a leaking roof is when the sun is shining, not when it is raining.”
“It’s all about building relationships. Build relationships that they can trust, so they can pick up the phone and hear our side of the issues.”
“There is only one place to find success before work and that’s the dictionary. If we want to be successful, we’ve got to work and think about what we need to do today to make tomorrow easier.”
On immigration reform: “We don’t care where the hoops are (we have to jump through) as long as they are reachable.”
 
Wingard told attendees that he likes to bracket his message to lawmakers with feel-good talk first, the tough message in the middle and good again at the end.
 
He likes to leave lawmakers and their staffs with something to think about, perhaps a one-liner like “We want a deal that allows Americans to be Americans.” Or, on immigration, “We want them to find a reasonable solution to a serious problem.”
 
Speaking after his Hill visits, Wingard said lawmakers were receptive to the farm labor issue. 
 
“I think everybody recognizes what the solution has got to be but I don’t know that everybody is coming together on how to get the solution politically across the finish line,” he told me.
 
Guenther said that there just might be an opening for an ag labor fix to be packaged with legislation to address the “Dreamer” issue, the Deferred Action for Childhood Arrivals program that will end next March without Congressional intervention. The program protects undocumented immigrants who came here as children from deportation.
 
Whether the solution will come with Rep. Bob Goodlatte’s proposed reform of H-2A or some other bill, there is hope. Contrary to the inaction by Congress for more than a decade, a solution for farm labor reform seems doable. As Wingard says, all we want is a reasonable solution to a serious problem.