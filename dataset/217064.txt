Imagine owning a lumberyard. A contractor comes to your business and says, “I’m going to build a subdivision, and I’ll pay you for this lumber when I sell the homes.” Then imagine the housing market crashes and all of those homes are foreclosed on before you get paid. That’s counterparty risk.

That scenario is starting to play out in agriculture, says Tommy Grisafi, a market analyst with Advance Trading. Suppose a neighboring dairy farmer offers to buy your extra 100 acres of corn for silage and says he will pay when it's in the silage bunk, only no payment materializes. Maybe the numbers don’t work out or the farmer says he’s got to wait for his next milk check.

“Sixty or 90 days later, you wonder if you’re going to get your money,” Grisafi explains. “You just let someone else’s financial problems become your financial problem.”

This kind of situation is most likely to happen with neighbors, friends and family you’ve known for a long time, he says. “That guy you’ve known for 20 years has always been good for [payment]. What if, one day, he’s not good for it?” Grisafi asks. “What do you do then?”
Safeguard Finances. Early preparation can prevent disaster, says Shannon Ferrell, an ag law professor and educator for Oklahoma State University Extension. The first step to avoid this situation is to do homework on the other party.

“There’s tons of information available about other people,” Ferrell says. “You can do a lot of record-checking with free online tools.”

Secondly, business deals—even those with neighbors—should involve a written contract. Although some might see written contracts as a sign of lack of trust, Ferrell says it’s not a trust issue. “I say good fences make good neighbors, especially in the small agriculture community,” he says. “Written agreements save more relationships than they jeopardize.”
Three Reasons to Have a Contract
Although it’s true many farmers still make deals on a handshake, getting a deal in writing is important, especially in tough economic times. That’s according to Shannon Ferrell, an agricultural law professor and educator for Oklahoma State University Extension.
Here are three reasons Ferrell thinks having a written agreement is a must.

Here are three reasons Ferrell thinks having a written agreement is a must.

It allows you time to think through the transaction. When you write a contract for a business agreement, you and your counterparty are required to think through every facet of the deal. “You think about what can potentially go wrong,” Ferrell says.
There’s a written record of negotiations. During a business transaction, there is usually quite a bit of back-and-forth negotiation. When you have a written contract, all of those details are spelled out so there’s no he-said-she-said issue.
It’s an easy way to resolve conflict. A contract settles disagreements immediately because you have a written agreement to fall back on. “You have an automatic resolution,” he says.

“It’s kind of like that old Ronald Reagan saying, ‘Trust but verify,’” Ferrell says. “A handshake is a good idea, but so is having a written agreement.”