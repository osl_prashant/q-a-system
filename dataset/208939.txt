U.S. cattle and hog futures gained about 1 percent on Friday, lifted by technical buying despite declines this week in wholesale prices for beef and pork, traders and analysts said.Live cattle gained for the second straight session as futures continued to recover from the roughly two-week low reached on Tuesday. Lean hogs hovered near Wednesday's three-month highs.
"Lean hog futures are showing little direction in thin, choppy pre-weekend trading as chart resistance blocks the upside and firm pork prices limit selling interest," analyst firm Brock and Associates said in a client note.
Chicago Mercantile Exchange June lean hogs finished 0.350 cent higher at 79.500 cents per pound and most-active July hogs were up 0.900 cent to 80.175 cents per pound.
Investors continued to liquidate positions in front-month hog and cattle contracts, resulting in bear-spreading gains.
CME June live cattle futures were up 0.525 cent at 123.450 cents per pound and most-active August were up 1.400 cents to 121.050 cents per pound.
CME August feeder cattle gained 1.100 cents to 150.450 cents per pound, while the thinly traded front-month May contract was up 1.075 cents at 143.350 cents.
Retailers have largely wrapped up meat buying for the beef and pork they will put on sale next week ahead of the U.S. Memorial Day holiday on May 29, the unofficial start of the summer outdoor grilling season.
Wholesale beef and pork prices have likely peaked for the season as the retailer buying slowed, analysts said.
The U.S. Department of Agriculture showed slightly lower beef prices and slightly higher pork prices.
"Beef has topped out for the moment," said Archer Financial Services broker Dennis Smith.
He added that some investors were squaring up their positions ahead of two USDA reports due next week - monthly Cold Storage and Cattle on Feed, data that may trigger increased volatility in futures.
"Demand for pork has been outstanding. The cold storage report could be the nudge" for higher prices, he said.