The International Dairy Foods Association appreciates the Food and Drug Administration's (FDA) announced intention to extend the compliance dates for the new Nutrition Facts label and Serving Size rules."On behalf of our members, we thank the Food and Drug Administration for extending the compliance timeline for the new Nutrition Facts label and Serving Size rules. Dairy foods companies are committed to giving consumers the information they need to make informed choices, and appreciate the extra time to be sure that the information on the labels is complete and accurate," said Cary Frye, IDFA vice president of regulatory and scientific affairs.
In the past, IDFA has urged government officials to align the compliance dates for the Nutrition Facts label changes with the U.S. Department of Agriculture's disclosure standard for bioengineered foods, which is required by law to be issued by July 2018. Having two separate compliance deadlines would require food and beverage manufacturers to go through the expensive and time-consuming process of changing the label twice on every food package in the United States.
"Our member companies are hopeful that once FDA announces the new implementation timeline they will be able to avoid the confusion and extra cost incurred by changing their product labels twice – first to comply with the changes to the Nutrition Facts label and again when the U.S. Department of Agriculture specifies how genetically engineered foods and ingredients need to be labeled."
Although FDA announced its intention to extend the Nutrition Facts label compliance date to provide additional time for implementation, no timeline was set. FDA said it will provide details of the extension through a Federal Register notice at a later time.