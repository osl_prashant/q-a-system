A small community in Wisconsin is about to trade its fields of corn and soybeans for a new high tech manufacturing facility.

Foxconn, a Taiwanese tech company, announced plans to build a new plant earlier this week in Mount Pleasant, Wisc., 25 miles south of Milwaukee.

This will be a $10 billion investment, employing 13,000 people.

The Wisconsin legislature approved a $3 billion incentive package for Foxconn.