USDA Weekly Grain Export Inspections
			Week Ended August 24, 2017




Corn



Actual (MT)
805,756


Expectations (MT)

650,000-850,000



Comments:

Inspections are up 85,543 MT from the previous week and the tally was near the upper end of traders' expectations. Inspections for 2016-17 are up 23.1% from year-ago, compared to 25.1% ahead the previous week. USDA's 2016-17 export forecast of 2.225 billion bu. is up 17.0% above the previous marketing year.




Wheat



Actual (MT)
670,748


Expectations (MT)
450,000-650,000 


Comments:

Inspections are up 78,501 MT from the previous week and stronger than expected. Inspections are running 12.4% ahead of year-ago versus 11.9% ahead last week. USDA's export forecast for 2017-18 is at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual (MT)
716,171


Expectations (MMT)
550,000-750,000 


Comments:
Export inspections are up 47,926 MT from the previous week, and near the upper end of expectations. Inspections for 2016-17 are running 13.0% ahead of year-ago, compared to 13.7% ahead the previous week. USDA's 2016-17 export forecast is at 2.150 billion bu., up 10.7% from year-ago.