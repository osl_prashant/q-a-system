BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Wednesday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeApr        12.48   12.48   12.18   12.21  Down .33May                                12.34  Down .32Jun        12.58   12.61   12.32   12.34  Down .32Sep        13.02   13.02   12.76   12.78  Down .28Dec                                14.07  Down .14Feb        14.19   14.32   14.03   14.07  Down .14Apr        14.25   14.48   14.25   14.30  Down .07Jun        14.36   14.58   14.36   14.43  Down .04Sep        14.70   14.82   14.63   14.69  Down .02Dec                                15.30  Up   .02Feb        15.27   15.39   15.24   15.30  Up   .02Apr        15.23   15.33   15.19   15.24  unchJun        15.33   15.34   15.22   15.26  Up   .01Sep        15.52   15.53   15.46   15.48  Up   .02