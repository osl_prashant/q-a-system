It’s fitting a one-in-a-lifetime total solar eclipse crossed part of the 2017 Farm Journal Midwest Crop Tour path today. Will this historical astronomical event align with a grain-market-altering event?“Last week I started referring to USDA’s August Crop Production as an eclipse—it cast a shadow of doubt over the markets,” says Dan Hueber, author of “The Hueber Report” blog on AgWeb.com and a guest on the Aug. 21 episode of Market Rally Radio. “We haven’t come out of that yet, so maybe the data we get this week could shed a few rays of sunlight back on the markets. I do not recall a recent year in which so many were anticipating the Crop Tour numbers.”
Last year, Hueber notes, the majority of Midwestern crops we so uniform across a wide swath of the country that the general assumption was that any and all numbers would be large.
But this year, weather challenges at planting and during pollination make many suspect USDA’s current national averages of 169.5 bu. per acre for corn and 49.4 bu. per acre for soybeans.
Listen to Hueber on Market Rally


 

“With many already in varying states of disbelief with the USDA figures, we are bound to see markets gyrate as each new yield report is released this week,” Hueber says of the Crop Tour’s daily state yield estimates.
Yet, the market has likely much-reduced its expectations, Hueber notes. Since USDA didn’t lower its national yield estimates by as much as many private estimates expected, the market has tempered its expectations.
“The market is already looking for a reduction from what USDA has forecast,” Hueber says of the Crop Tour data to be released this week.
Will the Crop Tour expectations be enough to jolt the markets? Hueber says a rally might be beyond reasonable expectations, but the Crop Tour data could at least “stop the bleeding” in the grain markets.
From-the-Field Reports
Also appearing on today’s Market Rally Radio were Crop Tour hosts, Brian Grete and Chip Flory.
Grete, Farm Journal’s Pro Farmer editor, is on the eastern leg of the tour. So far on his route, his crew has pulled eight corn and soybean samples in west-central Ohio. The average yield for the eight corn was 148.5 bu. per acre.
Listen to Grete on Market Rally


 
“But that doesn’t tell the story,” Grete says. “The story is: extreme variability. We saw a low of 59 bu. per acre and a top of 222.6 bu. per acre. “There is a huge swing from bad to good.”
Even within a field, Grete says, his team saw variability due to maturity level. Many fields they sampled had been replanted.
On the western leg, Flory, Farm Journal Pro editorial director, also saw variability on the crop samples his group took in South Dakota.
“We saw a few corn fields that had a lot of problems during pollination,” Flory says. “There’s very low yield potential in some of those fields, yet others showed potential all the way up to 220 to 225 bu. for corn in South Dakota.”
Listen to Flory on Market Rally

The good-looking fields still have a long way to go, Flory notes, but the potential is there.
“But, you can’t pass judgment on this Tour by looking at one route,” Flory says.