The Missouri Cattlemen's Association (MCA) elected it's 2018 leadership during the 50th Annual Missouri Cattle Industry Convention and Trade Show, Jan. 5-7, 2018. Greg Buckman, Hallsville, Missouri, will serve as the 2018 MCA President.
"I'm looking forward to serving Missouri's cattlemen as president in the upcoming year," Buckman said. "The Missouri Cattlemen's Association has seen great achievements over the past few years. From minimizing the tax burden on Missouri farm and ranch families to stepping up and helping our neighbors to the West when wildfires scorched thousands of acres."
Bobby Simpson, Salem, Mo., will serve as the association's president-elect. Marvin Dieckman, Cole Camp, Mo., was elected vice president. Matt Hardecke, Wildwood, Mo., will continue to serve as treasurer and David Dick, Sedalia, Mo., will serve as secretary. Butch Meier, Jackson, Mo., will chair the MCA Executive Committee as past president.
Regional vice presidents were elected based on the region they reside in across the state.



Region
Vice President
Hometown




Region 1
Adam Kuebler
 


Region 2
Chuck Miller
Olean, Mo.


Region 3
Charlie Besher
Patton, Mo.


Region 4
Tony Washburn
King City, Mo.


Region 5
Bruce Mershon 
Lee's Summit, Mo.


Region 6
Clay Doeden
Stockton, Mo.


Region 7
Traves Merrick
Miller, Mo.



 
Fair Share Donations Help Fund Legislative Work
MCA president Butch Meier said MFA Incorporated and Quality Liquid Feeds (QLF) are in the fourth year of a promotional partnership, to increase "fair share" dollars in the association.
These supporters matched all fair share contributions into the association, turning producers’ 50 cents per head into a dollar per head. Member contributions ended up totaling nearly $16,000.
MCA Executive Vice President Mike Deering said fair share dues of 50 cents per head are on top of membership dues. These funds go toward strengthening the association's junior programs and legislative efforts.
 
Awards and Recognition
Cattleman of the Year: Ben Eggers, Mexico, Mo.
Pioneer Award: Curtis (Doc) Long, Butler, Mo.
CattleWoman of the Year: Marylin Lesmeister, Montrose, Mo.
2017 Legislators of the Year: Sen. Dan Hegeman and Rep. Sonya Anderson
Outstanding County Affiliate: Southwest Cattlemen’s Association
Runner-up: Polk County Cattlemen’s Association
Outstanding Affiliate for Beef Promotion: Vernon County Cattlemen’s
 
Missouri Beef Queen Contest
Kenadee Barnitz, of Lake Spring, Missouri, was crowned the 2018 Missouri Beef Queen. She represented the Dent-Phelps County Cattlemen’s Association. She also received a $1,000 scholarship.
First runner-up: Hailey Gilbreath, Missouri State University.
Second runner-up: Cora Besselman, St. Charles County Cattlemen
Third runner-up, Julie Brinker, Franklin County Cattlemen's
 
FFA Beef Speaking Contest
First: Madelyn Derks, King City FFA (watch her speech below)
Second: Matthew Morgan, Lamar FFA
Third: Trinity Crouch, Eldon FFA
Finalists: Lauren Robnett, Laddonia Community R-5 FFA, Madison Coleman, Hermann FFA, and Abbagale Franklin, Perryville FFA.

Scholarships
MCA presented $27,000 in scholarships to state youth. Each selected student received a $1,000 scholarship funded by Missouri's Cattlemen Foundation and its many supporters.
Kenadee Barnitz, Lake Springs; August Bertz, Mayview; Caleb Bleich, Jamestown; Kaylee Calvin, Silex; Crayton Crawford, Hamilton; Bailey Crouch, Lincoln; Madelyn Derks, King City; Rayne Faulconer, Warsaw; Sara Gholson, Jackson; Glendon Griesbaum, Palmyra; Hattie Grisham, Eldon; Kaelyn Johnson, Savannah; Nolan Kiehl, Meadville; Dallas Klieboeker, Stotts City; Donnell Klieboeker, Stotts City; Alexandria Lock, Carrollton; Jack Long, Cole Camp; Bobbi Long, Wheatland; Audrey Martin, Bucklin; Brianna Rhodes, Troy; Daniel Ross, Clark; Larna Schnitker; Kylie Selway, Kahoka; Adam Weaver, Canton; Ezekeial Webb, Willow Springs; Hannah Wheeler, Osceola; and Jane Zuroweste, Truxton.