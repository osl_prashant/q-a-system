New rule bans vessels from releasing sewage into Puget Sound
New rule bans vessels from releasing sewage into Puget Sound

By PHUONG LEAssociated Press
The Associated Press

SEATTLE




SEATTLE (AP) — Recreational and commercial vessels will not be able to release treated or untreated sewage into Puget Sound waters under new rules approved by the state aimed at improving water quality.
The Department of Ecology on Monday officially designated a new "no discharge zone" in Puget Sound to protect shellfish beds, public beaches and sensitive marine environments from harmful bacteria.
There are dozens of such zones across the country, but this is the first in the Pacific Northwest.
The state estimates about 215 commercial vessels and 2,000 recreational boats — or roughly 2 percent of vessels — would need to add holding tanks.
Treated sewage discharges contain fecal bacteria concentrations that are many times higher than state water quality standards, and even small amounts of sewage discharges over or near shellfish beds can cause enough pollution to require harvest closures, the agency says.
Starting May 10, most boats will not be allowed to pump sewage — whether treated or not — into waters across about 2,300 square miles (5,957 square kilometers). The zone extends from near Sequim on the Olympic Peninsula to south Puget Sound to the Canadian border, and includes Lake Washington and Lake Union.
State officials say marine sanitation devices commonly used by vessels don't adequately treat sewage.
So boaters will need to pump out toilet waste at stationary or mobile pump-out facilities, hold waste in tanks, or wait until they are out of the zone.
The state petitioned the U.S. Environmental Protection Agency for the zone designation, which the agency affirmed in 2017.
The new rule does not apply to so-called graywater that drains from sinks or baths. Tugboats, commercial fishing vessels, federal research vessels and some others would have until 2023 to comply.
Currently, boaters are allowed to pump out treated sewage anywhere in Puget Sound. Federal law allows vessels to dump raw sewage only in waters more than 3 miles from the coast.
A number of environmental groups supported the "no discharge zone" as a step toward protecting Puget Sound.
"This is long awaited and a lot of work has gone into this," said Chris Wilke of the Puget Soundkeeper Alliance. "It's a significant day for Puget Sound."
Critics have worried that the rule is too broad and would be costly for many who would have to retrofit vessels to accommodate waste holding tanks.
Retrofits for tug boats and commercial vessels could range from negligible to $161,000, according to a consultant for the department. Adding a holding tank on a recreational boat is estimated to be about $1,500.
Some who opposed the zone in comments to the Environmental Protection Agency said the state should pay more attention to larger issues of pollution from stormwater, failed septic systems and other issues.
Others said the retrofit costs would create economic hardships particularly for those who rely on their vessels to make a living. Still others worried whether there were enough pump-out facilities for a range of vessels.
The agency said there are more than 173 pump-out stations for recreational boaters, and another 16 commercial pump-outs. It is working with others to add more commercial pump-outs.