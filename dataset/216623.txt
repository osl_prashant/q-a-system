Demand has proven the key to holding livestock markets in the black during 2017. 
Despite increasing beef production, cattle feeders experienced a booming market last spring that padded closeouts at times in excess of $400 per head. Demand, industry analysts say, was the driving force that kept pulling cattle forward and in the process a steep decline in carcass weights. 
That windfall for feedyards fueled the desire for replacement cattle, supporting prices for feeder cattle and calves at unanticipated levels. 
“Strong demand for beef kept pulling cattle forward, and feedyards had the incentive to bid up replacements,” says Sterling Marketing president John Nalvika. “That put a lot of dollars in ranchers pockets for feeder cattle that we didn’t think would be there at the beginning of the year.”
Now the question on rancher’s minds is, “Can demand continue to support the growing supplies?”
The short answer is, “Yes, but probably not without a modest decline in prices.”
Price risk is always possible, but analysts say risk and volatility is reduced at this point in the cattle cycle. That’s because prices are well below the peaks of a couple years ago, and market fundamentals have replaced much of the market’s emotion.
Generally, analysts expect 2018 cattle prices will be somewhat lower on an average basis than in 2017. That’s due to further increases in supply of beef and all proteins. 
“In four short years, we’ve seen total red meat and poultry supplies increase 10%, recovering from the drought-fueled low set in 2014,” Nalivka says. He projects per capita red meat and poultry supplies will near 222 pounds in 2018, or 20 pounds more than the low of 202 pounds in 2014.
Specifically to beef, feeder cattle numbers will be up for the third consecutive year, on-feed numbers will increase and total beef production is projected to jump another 4% in 2018 to record highs.
“We are still in the stage of the cattle cycle where supplies are going to get larger for another two or three years,” CattleFax CEO Randy Blach told cattlemen at the National Angus Convention. But, he noted, the cattle industry has navigated those hurdles well.
“We’ve got everything going our way - exports are better than anticipated, domestic demand has been good, consumer spending and consumer attitudes have all been very positive - but it doesn’t take much and we could start to move back the other way.”
Blach told attendees at the Kansas Livestock Association convention that increasing domestic household income and growing consumer confidence has led to the highest-quality beef supply in decades. 
“The Prime-Choice spread has stayed at very large levels despite the increase in supply,” he said. “That’s demand. That’s the market telling us they want more of the good stuff.” 
Although the demand picture is positive, Blach said feeder cattle prices will stay flat, ranging from $135 to $160/cwt in 2018. He anticipates modest, yet solid profit margins in the calf market.
Maintaining a strong export market for beef will be critical for domestic prices in 2018. 
“Demand surpassed expectations in 2017,” Nalivka says. “Retail beef prices in 2017 were 10 cents cheaper while per capita consumption was increasing 1.5 pounds. That’s excellent demand.”
And a 12% gain in U.S. beef exports in 2017 shows robust demand, especially following a 13% gain in 2016. 
“Exports amounted to 11% of production in 2017,” Nalivka says. “I project we’ll see another 4% gain in exports in 2018, which will be record high. It would mean we’re exporting 3 billion pounds of mostly high-value beef.”