Workers at the Sanderson Farms plant in Palestine, Texas, are cutting up chickens for export to Mexico, a market that has become a pillar of the town’s economy and a key focus of the U.S. poultry sector.
The facility’s roughly 1,000 employees process a quarter of a million chickens a day, with parts zipping along racks to a packing area where legs and thighs are prepared for delivery to the United States’ southern neighbor.
But the Mexican market may find its way to the chopping block if talks to renegotiate the North American Free Trade Agreement collapse and President Donald Trump makes good on a threat to pull out of the 1994 deal with Canada and Mexico.
The stakes are high for the U.S. poultry sector, which exports products worth more than $1 billion a year to Mexico.
Mexico could slap a 75 percent tariff on U.S. chicken and turkey under its commitments to World Trade Organization rules, compared to the 20 percent tax on imports the Latin American nation could apply to other major U.S. exports like pork.
“It would effectively prohibit us from selling product to Mexico,” Sanderson Farms Chief Financial Officer Mike Cockrell said. He added that the loss of the market would be comparable to the impact of Russia’s 2002 ban on U.S. chicken imports.
The bulk price of chicken leg quarters in 10 U.S. states including Texas sank to 18 cents a pound in March 2002, down from 26 cents a pound before the Russian ban, according to data from the U.S. Department of Agriculture.
Mexico accounted for about 4.5 percent of Sanderson Farm’s gross sales in the 12 months through October.
Mexican officials have made clear they don’t want consumers to face the price hikes that would be triggered by tariffs, although they have also said global trade rules would allow import taxes in the absence of NAFTA.
“There is nothing standing in the way,” said Raul Urteaga, the top trade official in Mexico’s agriculture ministry and one of the people involved in the NAFTA negotiations of the 1990s. “What I can’t say is what the tariff would be.”

Mexican, Canadian and U.S. officials will meet in Montreal on Tuesday for talks to modernize the 24-year agreement, with negotiators aiming to reach a deal before Mexico’s presidential campaign kicks into gear ahead of a July election.
Even a 25 percent tariff would make it hard to sell in Mexico, leading to a glut of chicken legs in the U.S. market, Cockrell said.
“Nobody really goes to the Olive Garden to buy a leg quarter,” Cockrell said, referring to U.S. consumers’ general preference for white chicken meat rather than the dark meat found on leg quarters.
Lost sales to Mexico would add to the pain already felt from the virtual closure of the Chinese market to U.S. chicken.
Beijing has lowered the stiff anti-dumping tariffs on U.S. broiler chickens that it enacted in 2010, though Washington maintains China has not gone far enough to comply with a WTO ruling against the tariffs.
Waiting in the Wings
The United States exports about 20 percent of its poultry production - it sent about $800 million in chicken and turkey meat to Mexico in 2016. Mexico buys more U.S. chicken than the next two markets combined.
If NAFTA disappears, U.S. poultry could face increased competition from Brazil, which is the world’s top chicken exporter and has tariff-free access to the Mexican market through 2019, as well as the possibility of lost jobs.
Palestine Mayor Steve Presley says he is worried about the economic future of his town of 18,000. Sanderson Farm’s plant opened in 2015.
The poultry industry, for its part, has responded by lobbying U.S. officials.
James Sumner, president of the USA Poultry & Egg Export Council, said he has met with U.S. Trade Representative Robert Lighthizer to discuss the issue. Zippy Duvall, a Georgia chicken grower who is the head of the American Farm Bureau Federation, met briefly with Trump in Nashville on Jan. 8, said AFBF lobbyist Dave Salmonsen.
Texas, Georgia, Arkansas and Mississippi are the top four states sending poultry meat to Mexico. Each is a Republican stronghold that voted heavily for Trump in the 2016 presidential election.
Some observers speculate the potential impact of a NAFTA collapse on agriculture could prompt the Trump administration to ultimately stick with the deal.
“It wouldn’t be in the Republicans’ interest to hit a big part of their constituency in their pockets,” Caroline Freund, a fellow at the Peterson Institute for International Economics, said earlier this month in an interview.