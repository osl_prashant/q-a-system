State denies oyster growers' request to use pesticide
State denies oyster growers' request to use pesticide

The Associated Press

SOUTH BEND, Wash.




SOUTH BEND, Wash. (AP) — State regulators have denied a request by oyster growers to use a pesticide to control burrowing shrimp in oyster and clam beds in southwest Washington.
The Department of Ecology said Monday that new scientific research and data now show the pesticide imidacloprid would have too great an impact on other wildlife and the environment.
It's a reversal from 2015 when the agency approved a similar permit to the Willapa-Grays Harbor Oyster Growers Association after finding the pesticide was unlikely to result in significant harm to the environment. The association withdrew that permit weeks later amid public outcry.
In 2016, a smaller group of about a dozen oyster growers applied to reinstate that permit, triggering additional environmental review.
The growers have said that failing to control burrowing shrimp will have a significant impact on the shellfish industry and local economy.