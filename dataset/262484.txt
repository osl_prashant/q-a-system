BC-Merc Table
BC-Merc Table

The Associated Press

CHICAGO




CHICAGO (AP) — Futures trading on the Chicago Mercantile Exchange Wed:
Open  High  Low  Settle   Chg.CATTLE                                  40,000 lbs.; cents per lb.                Apr      123.00 124.30 122.77 123.00  +1.10Jun      114.00 114.80 112.90 113.25   +.10Aug      111.45 112.25 110.57 110.77        Oct      114.22 114.65 113.30 113.70        Dec      117.00 117.50 116.35 116.80   +.18Feb      118.40 118.80 117.62 118.12   +.27Apr      118.40 118.82 117.75 118.15   +.08Jun      112.30 112.45 111.52 112.00   +.10Aug      110.25 110.25 110.00 110.00   +.45Est. sales 79,868.  Tue.'s sales 87,714 Tue.'s open int 362,117,  up 576       FEEDER CATTLE                        50,000 lbs.; cents per lb.                Mar      142.17 143.60 141.65 142.05   +.50Apr      143.22 144.85 142.35 142.72   +.42May      143.30 145.00 143.12 143.62   +.50Aug      148.55 149.65 147.95 148.80   +.75Sep      149.67 150.65 148.85 149.72   +.47Oct      149.82 150.72 148.95 149.70   +.33Nov      149.55 150.32 148.90 149.17   —.08Jan      146.00 146.47 145.00 145.07   —.28Est. sales 15,386.  Tue.'s sales 16,012 Tue.'s open int 51,925                 HOGS,LEAN                                     40,000 lbs.; cents per lb.                Apr       67.45  67.72  66.62  66.87   —.85May       71.70  72.25  71.25  72.05   +.20Jun       77.30  78.55  77.00  78.35   +.88Jul       78.40  79.75  78.10  79.55  +1.13Aug       78.75  80.22  78.45  80.10  +1.38Oct       67.55  68.52  67.20  68.47   +.92Dec       62.45  63.25  62.40  63.15   +.43Feb       66.65  67.32  66.57  67.27   +.45Apr       70.52  70.67  70.52  70.67   +.55May                            76.67   +.55Jun                            79.25   +.45Jul                            79.40   +.45Est. sales 46,793.  Tue.'s sales 71,994 Tue.'s open int 230,506,  up 3,642      PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.