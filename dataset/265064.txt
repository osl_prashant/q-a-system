Acreage, South American weather, commodity prices and a plethora of other things have been on the minds of farmers all winter.

Dry weather in South America gave soybeans the opportunity to rally. That has been one of the biggest stories in grains as of late, but it might have run its course.

“We’re going to get to a point where that’s old news, and maybe we’re starting to approach that point,” said Joe Vaclavik, president of Standard Grain.

The next thing that will be catching the attention of farmers and traders will be demand as the South American growing season wraps up.

“We start to talk more about demand and, more importantly, about U.S. production, what sort of crops are we going to produce this year, and we’re going to get into that conversation shortly,” he told U.S. Farm Report host Tyne Morgan.

Last week, Allendale, Inc. released its annual acreage survey, predicting a record high 90.142 million acres of soybeans and 88.514 million acres of corn to be planted in the U.S. in 2018.

“I would be shocked if corn acreage was below 90 million,” said Vaclavik. “It would be a good thing and it would open up the door to a lot better prices this summer in corn if we could keep that acreage number low.”

Since December corn futures are strongly above the $4 mark, Vaclavik doesn’t think corn acres will dip that low. Another thing that’s adding to the good news in the corn market is a decrease in supply.

Mike Florez of Florez Trading says even though corn is technically overbought, pullbacks in corn can be well-supported.

“We’re going to come in down markets for a four, five weeks or so, but I think that’s just a break in a bull market,” he said.

Hear Vaclavik and Florez digest the markets on U.S. Farm Report above.