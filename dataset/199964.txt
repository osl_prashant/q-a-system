A slow-moving storm dumped more rain on the waterlogged U.S. South on Thursday after heavy downpours killed at least four people this week, prompting evacuations and rescues from inundated areas.Three people died over the past two days in northern Louisiana, deluged with some of the heaviest rainfall. Two men and a 6-year-old child were killed after either ignoring flood warnings or entering treacherous areas without signs, the Louisiana Department of Health and Hospitals said.
A 30-year-old man drowned on Tuesday as he tried to drive across a flooded area in southeastern Oklahoma, the National Weather Service said.
Forecasters at the NWS received reports of 10 to 15 inches (25-38 cm) of rain in northeastern Texas, parts of Arkansas and Louisiana over a day and a half.
Particularly hard hit was Monroe, Louisiana, with more than 17 inches (43 cm), said Bob Oravec, lead forecaster with the NWS Weather Prediction Center.
As the system moves east, flooding is a concern in areas of Mississippi, western Tennessee and Alabama, along with Louisiana, he said.
"It's going to continue to be a pretty high-impact storm," Oravec said, noting that rains could linger into Saturday.
Videos on social media showed fish swimming on flooded sidewalks near the University of Louisiana-Monroe campus.
The Louisiana National Guard soldiers said it had helped rescue more than 360 people stranded in their homes and on roads by high waters since Wednesday. The guard also said it retrieved 70 dogs, 16 chickens and a guinea pig.
The governors of Louisiana and Mississippi declared a state of emergency in affected regions, and many schools and state government offices were set to be closed through Friday.
Arkansas Governor Asa Hutchinson declared 11 counties as disaster areas.
People in as many as 3,500 homes in Bossier City, Louisiana, were told to evacuate, the Shreveport Times reported.
The National Weather Service issued a flash-flood watch for parts of Texas, Louisiana, Mississippi, Arkansas and Tennessee.
Authorities in central Texas are searching for a rancher who disappeared on Wednesday night while attempting to wrangle cattle from rising floodwaters in Falls County, the Waco Tribune reported.