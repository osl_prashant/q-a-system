Teddy Koukoulis, director of blueberry operations for Plant City, Fla.-based Wish Farms, said the firm will have close to 4 million pounds of Florida blueberries in 2018, up about 20% from a year ago.
The firm has about 250,000 pounds of organic blueberries from Florida this year, he said, which matches well with the firm’s organic strawberry volume.
“We’re excited about the organic opportunity we have,” he said. “Organic demand is growing and continues to grow, so it is important to be a player in the organic market.”
The firm expects organic volume from the Southeast, including Florida, Georgia, and North Carolina, until July.
Wish Farm expects to market about 12 million pounds of blueberries from Florida, Georgia and North Carolina this year.
Wish Farms will promote blueberry recipes across social media platforms, encouraging consumers to engage with the brand and try new berry recipes, said Amber Maloney, director of marketing.