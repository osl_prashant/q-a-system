Whole Foods Market has promoted Christina Minardi to executive vice president of operations, and she will be among those leading the integration with Amazon.
She will also oversee four regions and work on e-commerce initiatives and other programs, according to a news release. Minardi has been with the company since 1995, most recently serving as president of the northeast region, overseeing 40 stores.
Last year Fortune magazine named her one of its Most Innovative Women in Food and Drink.
“I’m thrilled to welcome Christina to the leadership team, and I look forward to working with her to build an exciting future,” Whole Foods CEO John Mackey said in the release. “I know she will be an integral part of building our relationship with Amazon and helping drive our shopper experience.”
Minardi is the third executive vice president of operations for Whole Foods, joining Ken Meyer and David Lannon.
Amazon finalized its acquisition of the organics-focused retailer in August.
In a Securities and Exchange Commission filing this month, Whole Foods reported its fourth-quarter sales were up 4.4% from the same period in 2016. Year-over-year gains in the other three quarters ranged from 0.6% to 1.8%. The company’s fiscal year concluded Sept. 24.
It is unknown whether the fourth-quarter increase related to the acquisition by Amazon, but the e-commerce giant has made a point of slicing prices on key items at Whole Foods and then trumpeting the moves in an effort to drive traffic from consumers previously deterred by the retailer’s “Whole Paycheck” reputation.

Want to know more about organic produce? Register for The Packer’s inaugural Global Organic Produce Expo, Jan. 25-27, in Hollywood, Fla.