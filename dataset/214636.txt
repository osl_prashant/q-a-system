United Fresh Produce Association is offering training for the Foreign Supplier Verification Program in early December.
The Dec. 6-7 training is set for San Antonio, Texas, according to news release, and will offer participants a chance to acquire FSVP training and an FSVP certificate to satisfy Food Safety Modernization Act requirements.
The training courses are hosted by United Fresh and developed by the Food Safety Preventive Controls Alliance, according to the release. The course will be taught by Jennifer McEntire, vice president of food safety and technology for United Fresh, according to the release.
Registration fees are $695 for United Fresh members and $995 for non-members, according to the release.
Information and registration is available online.