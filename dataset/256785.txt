Arizona farmers, brewers fight drought with craft beer
Arizona farmers, brewers fight drought with craft beer

The Associated Press

CAMP VERDE, Ariz.




CAMP VERDE, Ariz. (AP) — A new conservation-minded operation to grow and process malt barley in north-central Arizona intends to provide an in-state source of a key ingredient for craft beer brewers while reducing agricultural demand for Verde River water.
The state's first barley malt house is slated to open in the Verde River Valley this month, supplying a key beer ingredient grown with water pulled from an overworked river that is crucial to metro Phoenix's water supply, The Arizona Republic reported .
Sweet corn, alfalfa and other crops typically suck water from the Verde all through summer and, in years past, have drained miles of the river between a diversion ditch and the point downstream where the runoff returns to the river.
Hauser and Hauser Farms, with the help of Nature Conservancy and corporate donors, have converted .22 square miles (.57 square kilometers) of those crops to winter-planted barley.
"To grow corn you need a lot of water in June, July, August," Zach Hauser said last week while hosting journalists on a tour with the Institute for Journalism & Natural Resources.
"With barley," he said, "we're done with water in May."
Barley couldn't pay its way if Verde Valley farmers had to sell it for livestock feed. Malt barley commands a higher price, but until now Arizona breweries had to buy it from Idaho or other locations because no one was malting in the state.
Chip Norton will open Sinagua Malt In March, to solve that problem. It's a small business collaboration, but it's also a lifeline for a river that Norton grew up floating and loving before he watched it decline from overuse.
Sinagua will supply the malt as an ingredient for breweries, but will not brew beer itself.
___
Information from: The Arizona Republic, http://www.azcentral.com