During the holidays the employees at Pipestone Veterinary Services look forward to connecting with family and friends and enjoying holiday meals. However, many individuals in Midwest communities are food insecure, and without organizations like Feeding America, these families would have limited to no protein to serve their families.
Pipestone (a veterinary services and sow management company) recognizes this need, and recently teamed up with local grocery stores on a project called Give a Helping Ham.  

Pipestone at the Ottumwa Hy-Vee in Ottumwa, Iowa
“There are an astounding number of families in our community that are food insecure,” Luke Minion, DVM, CEO of Pipestone, said in a news release from the company. “Give a Helping Ham is an easy way to get the community involved in giving back and providing families with some nutritious protein that they may not otherwise have.”

Pipestone in Sycamore, Ill., with the Northern Illinois Food Bank.
For every pound of ham purchased from a participating store Nov 14 through Nov. 24, Pipestone donated a pound of pork in return. In total, 15 grocery stores participated in several different communities participated, with significant results: 618 lbs. were donated to the Pipestone Food Shelf; 5,062 lbs. went to the Food Bank of Northern Illinois; 11,127 lbs. were distributed to Food Bank of Iowa; 13,609 lbs. went to the Northeast Iowa Food Bank; and 52,169 lbs. was given to Feeding South Dakota.
Because of the program, Pipestone donated more than 82,500 lbs. of pork, which equates to about 330,000 pork servings to local food banks.

Pipestone at one of the Sioux Falls, Hy-Vee stores, in Sioux Falls, SD.
“This is the largest one-time donation of protein Feeding South Dakota has ever received,” said Matt Gassen, CEO of Feeding South Dakota, in the release.  “We are so thankful for Pipestone and their work on this program.  There are so many families that will enjoy the donated pork loin during the holidays and we couldn’t be more thankful to Pipestone for making that possible.” 
Food banks/shelves need more than monetary donations to continue to serve local communities. Pipestone encourages you to consider helping out as a volunteer or contact your local food shelf to see what their needs may be.