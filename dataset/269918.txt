BC-US--Sugar, US
BC-US--Sugar, US

The Associated Press

New York




New York (AP) — Sugar futures trading on the IntercontinentalExchange (ICE) Wednesday:
(112,000 lbs.; cents per lb.)
SUGAR-WORLD 11Open    High    Low    Settle   ChangeApr        11.71   11.76   11.60   11.74  Up   .09May                                11.91  Up   .09Jun        11.84   11.94   11.77   11.91  Up   .09Jul                                12.14  Up   .06Sep        12.09   12.17   12.03   12.14  Up   .06Dec                                13.35  Up   .08Feb        13.29   13.38   13.25   13.35  Up   .08Apr        13.42   13.54   13.42   13.53  Up   .10Jun        13.54   13.66   13.50   13.64  Up   .12Sep        13.71   13.88   13.68   13.86  Up   .16Dec                                14.48  Up   .17Feb        14.30   14.48   14.30   14.48  Up   .17Apr        14.43   14.54   14.43   14.54  Up   .18Jun                                14.61  Up   .18Sep                                14.81  Up   .18Dec                                15.16  Up   .18Feb                                15.16  Up   .18