Fertilizer prices were generally lower with potash posting our only weekly gains.
Our Nutrient Composite Index softened 5.90 points on the week, now 40.50 points below the same week last year at 498.29.

Nitrogen

Anhydrous ammonia led overall declines this week led by Kansas, Michigan and Missouri.
UAN28% finally relented and softened along with the rest of the nitrogen segment.
On an indexed basis, UAN32% is now the least expensive fertilizer in our entire survey.
Urea fell nearly as much as anhydrous and UAN solutions were both about $6.50 lower.

Phosphate

Phosphates were lower with DAP leading declines.
Both DAP and MAP posted wild volatility from state to state although gains and declines netted very little movement.
The DAP/MAP spread is slightly wider than our expectations and nearly unchanged on the week again at 21.47.
Wholesale DAP and MAP are mixed on the week with DAP into NOLA down slightly and MAP off $1 per tonne.

Potash 

Potash firmed $3.05 per short ton this week led by gains in Nebraska.
On an indexed basis, the premium potash prices hold to anhydrous ammonia widened to $33.92 on a combination of firmness in vitamin K and softness in NH3. In fact, our indexed potash figure has it priced above all four of the forms of nitrogen in our survey this week.

Corn Futures 

December 2018 corn futures closed Friday, August 18 at $4.02 putting expected new-crop revenue (eNCR) at $638.63 per acre -- lower $10.17 per acre on the week.
With our Nutrient Composite Index (NCI) at 498.29 this week, the eNCR/NCI spread narrowed 1.97 points and now stands at -140.34. This means one acre of expected new-crop revenue is priced at a $140.34 premium to our Nutrient Composite Index.





Fertilizer


8/7/17


8/14/17


Week-over Change


Current Week

Fertilizer



Anhydrous

$471.46
$470.37

-$7.58

$462.79
Anhydrous



DAP

$444.30
$439.30

-83 cents

$438.48
DAP



MAP

$461.04
$460.13

-18 cents

$459.95
MAP



Potash

$332.06
$332.57

+$3.05

$335.62
Potash



UAN28

$236.00
$237.03

-$6.71

$230.32
UAN28



UAN32

$254.57
$250.38

-$6.45

$243.93
UAN32



Urea

$325.16
$324.02

-$7.40

$316.62
Urea



Composite

508.67
504.19

-5.90

498.29
Composite