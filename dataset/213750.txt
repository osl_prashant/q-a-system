After being sworn in April 25, Agriculture Secretary Sonny Perdue made his first official trip outside the Beltway to Kansas City and pledged the agency would expand trade opportunities and have a customer-first focus on the needs of farmers.Speaking April 28 to a crowd of nearly 500 at Hale Arena at the American Royal, Perdue talked for about 25 minutes and then answered questions for 15 minutes from a group of farmers and agriculture leaders from Missouri and Kansas.
“My oath to you is that I’m going to the very best job that I possibly can,” he said. “I believe that USDA is doing a good job and they have great career employees but I’m not satisfied with just doing good — we want to do great.”
After the speech, Perdue said he is in the process of looking at candidates for the deputy secretary and under secretary posts at the U.S. Department of Agriculture.
“I hope to be able to submit names for the president’s appointment very, very soon,” he said.
Perdue said the agency is striving for diversity, including agriculture sector, geography, gender and other considerations. “We want the USDA to look and act like America and make sure that American agriculture is served well.”
Trump’s farm focus
Perdue said President Trump’s April 26 farm roundtable event at the White House brought together a diverse group of farmers from across the country.
President Trump told the group, which included a few produce growers, that he is most concerned about the criminal element of illegal immigrants in the U.S., Perdue said, not about long-time immigrants working on U.S. farms and ranches.
“(Farm workers) are not the people he is after, he wants to get the criminals who are harming American people out of this country and I’m all for him on that,” Perdue said.
Trump also told members of the farmer roundtable that the administration would reduce the burden of unnecessary regulations from farmers. Perdue is chairman a new interagency task force that was created by Trump’s executive order promoting agriculture and rural America.
The president made it clear to the group that he understands the importance of trade for farmers, Perdue said.
“The president understands the benefits of NAFTA in some agriculture sectors,” he said.
Given President Trump’s desire to act quickly, Perdue estimated that the NAFTA negotiations could take as little as six months to achieve results. The decision to renegotiate NAFTA for the benefit of Americans will help farmers, he said.
“I continue to be amazed at the negotiating strength of this president and I’m glad to serve a guy like that.”