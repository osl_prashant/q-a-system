Iowa Secretary of Agriculture Bill Northey has been confirmed by the U.S. Senate to become an undersecretary in the U.S. Department of Agriculture.
Northey was nominated six months ago, but his confirmation was held up by Texas Senator Ted Cruz (R).
This comes at a time when President Trump is meeting with cabinet secretaries and senators from opposing sides of the ethanol debate.
The president also met with both Agriculture Secretary Sonny Perdue and EPA administrator Scott Pruitt to discuss possible changes to the Renewable Fuel Standard a day prior.
Republican Iowa Senators Chuck Grassley and Joni Ernst say Northey’s sudden confirmation was not a “bargaining chip” despite the timing of the confirmation.
Listen to audio below: