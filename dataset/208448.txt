Chicago Mercantile Exchange live cattle futures fell on Monday on expectations for lower prices for slaughter-ready, or cash, cattle this week amid ample supplies, traders said.The decline triggered pre-determined sell orders, and funds sold the October contract after it slipped beneath its 200-day moving average of 107.183 cents.
August ended 0.675 cent per pound lower at 109.050 cents, and October closed 0.800 cent lower at 106.600 cents.
A week ago, cash cattle in the U.S. Plains brought $114 to $116 per cwt, compared to $116 to $119 a week earlier.
Processors avoided bidding up for cattle as ranchers and feedlots rushed them to market earlier than planned, prompted by two bearish U.S. government cattle reports late last month.
For the week ending Aug. 12 the U.S. Department of Agriculture estimated the cattle slaughter at 641,000 head, 10.7 percent more than a year ago which produced 8.5 percent more beef.
"I think we're in the process of working through these big supplies through the rest of the year in an orderly fashion," said Cassandra Fish, author of industry blog The Beef.
Market participants are tracking wholesale beef demand for clues that grocers are buying meat for U.S. Labor Day holiday grilling advertisements.
CME feeder cattle finished higher for a second straight session. Futures were bullishly undervalued, or at a discount, to the exchange's feeder cattle index for Aug. 11 at 145.90 cents.
Market bulls believe the rush of cattle to market now, including heifers and calves, will result in fewer cattle later.
August feeders closed 0.525 cent per pound higher at 142.300 cents. 
Hogs Close Firmer
CME lean hog futures settled moderately higher, mainly because of their discounts to the exchange's hog index for Aug. 10 at 85.25 cents, traders said.
Uneasiness about cash and wholesale pork prices as supplies continue to grow seasonally capped market advances.
August, which expired at noon CDT (1700 GMT), settled down 0.200 cent per pound to 84.450 cents.
October ended 0.550 cent to 69.175 cents, and December finished up 0.250 cent to 63.500 cents.