World Dairy Expo is pleased to announce the recipients of the 2018 Expo Recognition Awards to be formally presented at the 52nd annual event in October. The honorees were nominated and selected by their peers for their contributions and excellence in the dairy industry and their community.
The 2018 honorees are as follows:
Dairy Woman of the Year

Jeanette Sheehan, Sheeknoll Farms, Rochester, Minn.

Dairyman of the Year

Pete Kappelman, Meadow Brook Dairy Farms, LLC, Two Rivers, Wis.

Industry Person of the Year

Dr. Dan Hornickel and Dr. Chris Keim, Sunshine Genetics, Inc., Whitewater, Wis.

International Person of the Year

Alastair Pearson, World Wide Sires China Co. Ltd, Beijing, China

These dairy leaders will be recognized at World Dairy Expo’s Dinner with the Stars, October 3, 2018, in the Exhibition Hall at the Alliant Energy Center. Banquet tickets are available by contacting the WDE office at 608-224-6455 or wde@wdexpo.com. Prior to the banquet, the WDE Welcome Reception returns to give all Expo stakeholders and attendees the opportunity to celebrate and interact with these honorees while enjoying complimentary hors d'oeuvres.
Serving as the meeting place of the global dairy industry, World Dairy Expo brings together the latest in dairy innovation and the best cattle in North America. Crowds of nearly 70,000 people, from 100 countries, will return to Madison, Wisconsin for the 52nd annual event, October 2-6, 2018, when the world’s largest dairy-focused trade show, dairy and forage seminars, a world-class dairy cattle show and more will be on display. Visit worlddairyexpo.com or follow us on Facebook, Twitter, Instagram, Snapchat or YouTube for more information.