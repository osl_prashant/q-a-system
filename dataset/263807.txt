This market update is a PorkBusiness weekly column reporting trends in weaner and feeder pig production. All information contained in this update is for the week ended March 16, 2018.

Looking at hog sales in September 2018 using October 2018 futures, the weaner breakeven was $42.66, up $4.80 for the week. Feed costs were down $0.51 per head. October futures increased $2.08 compared to last week’s October futures used for the crush and historical basis is unchanged from last week. Breakeven prices are based on closing futures prices on March 16, 2018. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was below average this week with 30,995 head being reported. This is 92% of the 52-week average. Cash prices were $50.44, up $1.10 from a week ago. The low to high range was $30.00 - $68.00. Formula-priced weaners were down $3.32 this week at $40.66.
Cash-traded feeder pig reported volume was below average with 5,510 head reported. Cash feeder pig reported prices were $84.54, down $2.97 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through March 16, 2018.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential decreased $7.54 this week to a projected loss of $19.02 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com