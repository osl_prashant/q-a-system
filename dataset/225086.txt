Strong prices are being reflected in strong cattle demand, and to Chip Nellinger of Blue Reef Agri-Marketing, the feedlots have the upper hand, while the packers are short bought.

The current situation is favorable, but there could be some problems in the next four to five months.

“The issue that everybody knows, the last five or six cattle on feed reports, big numbers are out ahead of us this summer and into fall,” he said on AgDay. “We’re going to have to really sharpen our pencil on these deferreds. From June on, it’s going to get really dicey.”

Nellinger doesn’t think beef prices will tank, but there will be some headwinds on the horizon if demand continues at its current pace.

Hear Nellinger’s full thoughts on AgDay above.