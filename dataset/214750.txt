Rick Stein with FMI (left) and David Sherrod with SEPC announced a new partnership between the two organizations.

The Food Marketing Institute will debut its annual The Power of Produce report at the Southeast Produce Council’s Southern Exposure show.
“Food retail continues to be the produce stronghold, so the opportunity to debut research that offers an in-depth look at produce through the eyes of the shopper at the premier platform for the produce industry complemented our goals for reaching an insights-hungry audience,” Rick Stein, vice president of fresh foods for FMI, said in a news release.
Southern Exposure has drawn as many as 2,000 suppliers and more than 500 buyers in previous years. FMI will present its latest report at the 2018 event, which is scheduled for March 1-3 in Tampa, Fla., and unveil future retail reports at the annual show.
“The Southeast Produce Council is excited to announce this partnership with FMI,” David Sherrod, president and CEO of the council, said in the release, “Their reputation of proven research results is widely used and regarded as one of the best tools for retail and wholesale professionals today.
Sherrod said the Power of Produce report will be a draw for Southern Exposure attendees.
“We will work hand-in-hand with FMI to deliver this timely report each year,” he said in the release.