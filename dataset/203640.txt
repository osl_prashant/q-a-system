All Fresh GPS adds sales executives
All Fresh GPS LLC is looking forward to the contributions of two produce industry veterans as it kicks off its 2016 Michigan apple season.
In July Comstock, Mich.-based All Fresh added Nick Mascari and Joe Aronica to its staff. Mascari is the company's vice president of sales, Aronica director of new sales and business development.
All Fresh partner Scott Swindeman, who is also vice president of Applewood Orchards Inc., Deerfield, Mich., said All Fresh needed more hands on deck to handle increased production.
"We have a grower base that's been very aggressive planting, and a lot of that is coming into production," he said. "We needed to get more good manpower, and with both Nick and Joe, we feel very comfortable that we'll be successful in what we're doing. They both bring a lot of experience to the table."
Mascari worked most recently for Indianapolis Fruit Co. His produce industry career also has included stints at Dole and Chiquita/Fresh Express.
Aronica's 23 years in the industry has included jobs at the Washington Apple Commission, Rainier Fruit Co. and Sage Fruit Co.
 
BelleHarvest boosts waxing capabilities
BelleHarvest Sales Inc. has enhanced its apple-waxing capabilities and installed a new inventory management and accounting software system in time for the 2016 season.
Belding, Mich.-based BelleHarvest has doubled the length of the brushes it uses for waxing, which will not only improve the waxing process itself but the cleaning and drying processes that precede it, said Chris Sandwick, the company's vice president of sales and marketing.
Sandwick said the upgrade is one way BelleHarvest can keep up with the innovations made by its grower partners.
"We expect to have a real premium tray for our customers."
BelleHarvest's new software system, meanwhile, will add efficiencies and help the company supply better information to its growers.
"It allows us to be much more thorough at collecting and analyzing data," Sandwick said. "It helps us drill down to get better and better recommendations to our growers about what they should do to maximize their profitability."
 
Higher wages help Glei's retain workers
Damon Glei, president and owner of Glei's Inc., Hillsdale, Mich., said his company was optimistic about labor heading into the 2016 season.
"So far it looks good - three quarters of our labor is already here," Glei said Aug. 18. "We may actually be in better shape than we have been in the past."
Glei's Inc. increased wages significantly last year to ensure it got enough workers, Glei said. Those wage levels have been retained and in some cases increased even more, and the company seems to be reaping the rewards of it with a stable work force heading into the heart of the 2016 season.
 
Riveridge poised for huge Honeycrisp volume boost
Galas have displaced red delicious as the top apple variety packed by Sparta, Mich.-based Riveridge Produce Marketing Inc., said Don Armock, the company's president.
And it may not be long before Honeycrisps take over the No. 2 spot from red delicious.
"The day is coming, quickly," Armock said. "In the next three or four years, possibly. There are a tremendous amount of Honeycrisps in the ground."
This year alone, Riveridge could double or even triple its Honeycrisp packouts, following a "disastrous" year for packouts of the variety in 2015.
"The packouts this year will be some of the best we've had."