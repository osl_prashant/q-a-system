The Packer has added two writers to its editorial staff and two in its sales department.
Kate Walz joins as staff writer. She began as a journalist at her junior college newspaper. Walz earned a bachelor's degree in mass communication from MidAmerica Nazarene University and started a career in television - producing documentary and reality television shows before moving to local news.
She worked in digital marketing while pursuing her master's degree from the University of Central Missouri, where she taught public speaking before returning to journalism at The Packer.
The Packer also added Chris Sitek as staff writer. He is a recent graduate of the University of Kansas and has been a writer for the student newspaper as well as having done extensive work in radio both at university stations and at Kansas City-area sports and community radio.
Rohwer
Gage Rohwer has been promoted to account executive for California, having previously served as advertising coordinator. Before joining The Packer, he worked in the music venue business in Los Angeles. He is a graduate of University of Nebraska-Omaha with a degree in communications.
Wall
Sarah Wall is the new advertising coordinator. Most recently she worked as an ad traffic coordinator for Anthem Media in Leawood, Kan. Prior to that, Wall worked for Tri-Star Publishing in Overland Park, Kan.
She is a graduate of Colby-Sawyer College in New London, N.H., with a bachelor's degree in English.