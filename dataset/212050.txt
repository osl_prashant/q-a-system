Onion harvesters in New Mexico got into the fields earlier than usual this year — in some cases, way earlier, growers said.“Because of the warm winter, we’re harvesting earlier than we ever have,” said Marty Franzoy, manager/owner of Hatch, N.M.-based grower-shipper Skyline Produce.
The Hatch area, for example, saw only 15 days where low temperatures were at or below freezing through January and February and 15 days of 60-degree highs in January, according to the National Weather Service.
Rain was ample, and the combination set up conditions for a “beautiful crop,” Franzoy said.
“We’ve got good quality, size and yields,” he said.
Las Cruces-based Barker Produce is anticipating “better than usual” yields this year, said Brandon Barker, vice president.
“I like our yields. We haven’t had a good yield the last couple of years because we’ve been hit by hailstorms,” he said.
Springtime growing conditions were largely dry, said Longino Bustillos, spokesman with the New Mexico Department of Agriculture.
“But the onion fields I have seen look pretty good,” he said.
Growers said acreage is in line with 2016, with summer non-storage onion acreage at 6,200, compared to 5,700 a year earlier, according to the U.S. Department of Agriculture. Volume was 3.54 million cwt., with a per-acre yield of 580 cwt.
The total value of utilized production was $94.47 million in 2016, compared to $106.62 million in 2015, the USDA reported.
Hatch-based Shiloh Produce Inc. packed red, yellow and white onions by mid-May — weeks ahead of a normal early-June start, said Stormy Adams, owner.
“We had a really mild winter, so the crop basically has never stopped growing, and that’s why the size is so good,” he said.
Big onions were the story at Adams Produce Inc. in Hatch, said Scott Adams, owner.
“The early crop is big — jumbo, colossal and super(-colossal) will dominate,” he said. As we get into the tail end, we’ll have more jumbo and medium.”
An ideal size mix likely would be “20% mediums, probably 50% to 60% jumbo, and the balance colossal and super,” Adams said.
The size mix appears to be right for the customer base at Arrey, N.M.-based Desert Springs Produce LLC, said Bill Coombs, salesman.
“We have a strong retail and foodservice following and need a good mix for our customers,” he said.
The crop at Hatch-based Hatch Valley Produce was sizing up well, said Debbie Porter, co-owner.
Pleasant Grove, Utah-based National Onion Inc., which bases its New Mexico production in Las Cruces, was about 10 days ahead of schedule with its crop there, said Steve Smith, president.
“Yield looks normal — looks like it will be pretty good,” he said. “There’s a fair amount of jumbos coming, and quality looks excellent.”
Market conditions were looking up, too, Franzoy said.
In the USDA’s first report of the season May 23, 50-pound sacks of yellow onions from southern New Mexico shipping for $9-12 for super-colossal; $9-11, colossal; $8-10, jumbo; and $6-8, medium.
A month into the season, June 26 prices for 50-pound sacks of yellow onions had dropped to $8-9 for super-colossal and colossal; $7-8, jumbo; and $9-10, medium. Year-ago prices ranged from $11-14 for all sizes.
White onion prices June 26 were $12-14 for 50-pound sacks of jumbos, the same as a year ago.
Red globe type onions were $6-8 for 25-pound sacks of jumbos, down from $12 a year ago.