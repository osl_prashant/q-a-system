BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Thursday:
OpenHighLowSettleChg.ORANGE JUICE15,000 lbs.; cents per lb.Apr138.80—.60May139.05139.20138.15138.80—.60Jun139.00—.40Jul139.05139.30138.65139.00—.40Sep139.60139.60139.25139.60—.30Nov140.35140.35140.00140.30—.35Jan141.00—.40Feb142.05—.50Mar142.05—.50May142.50—.50Jul142.60—.50Sep142.70—.50Nov142.80—.50Jan142.90—.50Feb143.00—.50Mar143.00—.50May143.10—.50Jul143.20—.50Sep143.30—.50Nov143.40—.50Jan143.50—.50Est. sales 355.  Wed.'s sales 624Wed.'s open int 12,269,  up 57