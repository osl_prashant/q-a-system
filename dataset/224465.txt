The following content is commentary and opinions expressed are solely those of the author.
During President Trump’s first year in office, there have been several regulation rollbacks. While he was campaigning, Trump vowed to nix two regulations for every new one.

John Phipps, U.S. Farm Report commentator, discusses occupational licensing and the consequences of it.

Watch John’s World every weekend on U.S. Farm Report.