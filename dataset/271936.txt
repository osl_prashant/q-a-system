US pecan growers seek to break out of the pie shell
US pecan growers seek to break out of the pie shell

By EMILY SCHMALLAssociated Press
The Associated Press

FORT WORTH, Texas




FORT WORTH, Texas (AP) — The humble pecan is being rebranded as more than just pie.
Pecan growers and suppliers are hoping to sell U.S. consumers on the virtues of North America's only native nut as a hedge against a potential trade war with China, the pecan's largest export market.
The pecan industry is also trying to crack the fast-growing snack-food industry.
The Fort Worth, Texas-based American Pecan Council has been formed in the wake of a new federal marketing order that allows the industry to band together and assess fees for research and promotion.
Critics call such orders government-backed cartels. But council members hope to elevate the pecan to the ranks of the almond and pistachio.