As North American consumers learn more about mushrooms, they look increasingly to specialty varieties, marketers say.
The sales value of non-traditional mushrooms — including shiitakes, oyster, hen of the woods (maitake) and others — increased by 4% last year, American Mushroom Institute spokeswoman Lori Harrison said, quoting figures from the U.S. Department of Agriculture.
“As more recipes and restaurant meals feature shiitakes, oyster, hen of the woods and others, consumers will try them at home,” Harrison said.
“Retailers can also start looking for ways to pair up mushrooms with other produce or items that complement each other to achieve a certain taste profile.”
Watsonville, Calif.-based Monterey Mushrooms Inc., offers specialties, in addition to the traditional white, said Mike O’Brien, vice president of sales and marketing.
Criminis/baby bellas — young portabella mushrooms — look much like white button mushrooms but are darker and have a stronger taste, O’Brien said.
“They have become very popular,” he said.
Some specialties were once considered “wild” mushrooms, but that’s not necessarily the case any longer, said Michael Basciani, chief operations officer of Avondale-based Basciani Mushroom Farms.
There’s no such thing as an “off-variety,” said Greg Sagan, executive vice president of sales and marketing with Temple, Pa.-based Giorgio Fresh Co.
“Mushrooms come in dozens of commercially available varieties, and the specialty mushroom varieties continue to grow at a faster rate than the traditional white and brown button mushroom,” he said.
Shiitake continues to lead the specialty subcategory, with sliced shiitake at retail getting a great consumer response, Sagan said.
He also said oyster mushrooms, enoki, beech, maitake and pom poms are trending upward and mentioned nameko and pioppini as relatively recent arrivals in the U.S.
Sagan said there continues to be a large group of foragers for wild mushrooms, although he added that those varieties are available from commercial farms.

 
“While these can be quite tasty, the commercial mushroom industry tries to promote the use of mushrooms grown in specialized mushroom farms, which have high standards of food safety and quality,” he said.
Specialty mushrooms, especially shiitake and oyster, are also gaining market share as consumers are more willing to try various mushrooms and gain appreciation for the different taste profiles and health benefits of each type, said Bill St. John, sales director with Gonzales, Texas-based Kitchen Pride Mushroom Farms Inc.
“Although they cost a little more, many people find them well worth the price,” he said.
Shiitake and oyster haven’t broken through as “day-to-day” mushrooms, but as more recipes and restaurant meals start using them, they will become more well-known and accepted by consumers, said Brian Kiniry, president of Kennett Square, Pa.-based Oakshire Mushroom Farm, which markets its product under the Dole banner.
“I believe retailers can start looking for ways to pair up mushrooms with other produce or items that complement each other to achieve a certain taste profile,” he said.
Several mushroom varieties are commercially grown but have not nearly reached the level of use as have shiitake and oyster mushrooms, St. John said.
“Brown beech, white beech, maitake, lion’s mane, and king oyster (or king trumpet) are a few such mushrooms that are very good and available on a smaller scale,” he said.
Price may have a say in how much sales some specialty varieties can garner, said Kevin Donovan, sales manager with Kennett Square-based Phillips Mushroom Farms.
“Shiitakes continue to grow and increase in sales, and you’re seeing some of the other varieties, such as the king oyster, continue to increase in popularity, but there’s a price point where it’s only going to get popular to a certain extent,” he said.