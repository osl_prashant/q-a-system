The list of the next Secretary of Agriculture seems to never end. President-elect Donald Trump and his aides have interviewed several candidates for the position including former governor of Georgia Sonny Perdue, former Texas A&M University President Elsa Murano, North Dakota U.S. Senator Heidi Heitkamp, and recently Indiana farmer Kip Tom.Whoever takes the position will need "to be the voice for those who come from rural America," said Sen. Jerry Moran, R-Kan. to AgriTalk host Mike Adams.


<!--
LimelightPlayerUtil.initEmbed('limelight_player_974847');
//-->

Moran is "anxious" of who the next person will be to lead the top agricultural position in the U.S.
"It seems as if there's very few people within the Trump transition team that are at the Department of Agriculture interacting with people that work there today," said Moran. "The two cabinet positions that remain open are the two I pay the most attention to: Veterans Affairs and USDA."
He reflected on his time chairing the Senate Agricultural Appropriations Subcommittee, which funds the USDA. He explained whoever heads the cabinet "has to be the person to go to bat" at the Office of Management and Budget, the Trade Representative, and the EPA on behalf of rural Americans.
"We want a Secretary of Agriculture who has broad support from Republican and Democrat members of Congress and the American people," said Moran.
Listen to Moran discuss the important positions the next Secretary of Agriculture will represent on AgriTalk above.