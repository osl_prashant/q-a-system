The land market, including the auction market, is finishing out 2017 strong.
“We are not only experiencing an increase in auction activity, but also an increase in our private treaty listings for sale too,” says Randy Dickhut, AFM, Senior Vice President Real Estate Operations at Farmers National Company. “Landowners who have been thinking about selling for awhile have decided to move forward and talk to our agents about getting their property sold.”
Of note, Dickhut says the trend continues that most sellers are inheritors and beneficiaries—not recently active farmers.
Right now, Sullivan Auctioneers in Hamilton, IL has more than 11,000 acres total coming up for auction and listed on their website.
And the heat in the market may only continue to strengthen.

“Farmers National Company's land auctions this past week have also been heating up in bidding activity,” Dickhut explains.
He shares two regional reports.
Paul Schadegg, sales manager, reported that the recent auction by Farmers National of seven tracts in Furnas County, Nebraska, which ranged from good dryland to pasture, generally brought at or above the market. And the overall auction price was well above local expectations.

Sam Kain, National Sales Manager, reported that his auctions in Iowa this past week generally brought a higher price for the sellers than expected. He said bidding activity was good and interested buyers were in the room.