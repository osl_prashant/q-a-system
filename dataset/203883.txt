Avocados have become synonymous with guacamole, a product that is not exactly new to the culinary scene.
"Guacamole, or some form of it, goes back to the Aztecs somewhere around 500 B.C.," said Jay Alley, vice president at MegaMex Foods, Saginaw, Texas, which markets the Wholly Guacamole brand of prepared guacamole.
"It has been gaining ground ever since," he said.
A large percentage of avocados that are purchased ultimately go into making guacamole, he said.
Guacamole plays a huge role around the Super Bowl, where he said more than 100 million pounds are consumed on one day.
Alley attributes much of the growth in the popularity of guacamole to the rise in popularity of Mexican food.
The fact that it's a healthful option also has is attractive to shoppers.
"In the last 10 years, consumers have found that avocado/guacamole is good for you," Alley said, "having 20 minerals and vitamins."Santa Paula, Calif.-based Calavo Growers Inc. began producing guacamole, pulps and other avocado-related products in the late 1950s, said Rob Wedin, vice president of sales and marketing.
At first, the company produced them in facilities in California, but for the past 10-15 years, they've been made at a Calavo Foods plant in Michoacan, Mexico.
The facility is "a big operation with hundreds of employees," he said. It operates 20 hours a day or more.
"The popularity of the different products that come out of there is pretty remarkable," Wedin added. "It's getting closer and closer to capacity."
The company is getting more involved with specialized packaging and organic guacamole, he said.
"Organic has become a popular Calavo Foods item, just like it's popular in our fresh pack," he said.
While many people enjoy making their own guacamole recipe at home, today's fast-paced lifestyles have prompted some to seek out pre-made product at their local supermarket.
Calavo's processed products include three hand-prepared varieties of guacamole - authentic, pico de gallo and caliente - that are made with no preservatives, just fresh ingredients like hass avocados, fresh onion, garlic, jalapeno peppers, tomatillo, serrano peppers and cilantro, Wedin said.
They are sealed fresh in pouches or trays and stay fresh for up to 50 days, he said.
For foodservice, the company provides fresh, refrigerated guacamole, avocado halves and pulp packed in tubs, pails, pouches, canisters, singles and easy-squeeze options.
Ultra-high pressure technology helps ensure that the products keep well frozen or refrigerated, he said.
Calavo Foods accounts for 5% to 10% of Calavo's total sales, Wedin said.
"Pre-made guacamole allows consumers to have guacamole anytime, anywhere, with little prep time and convenient packaging," Alley said.

"It's great for snacks, with dinner or large gatherings."
When you pick up guacamole at the store, pay attention to the ingredients, Alley suggested.
Buy guacamole that lists avocado as the first ingredient and that does not contain fillers, like xanthan gum, sugars or preservatives.
Alley sees a lot of potential for pre-made guacamole, like the company's Wholly Guacamole.
"With only 11% category household penetration, very few consumer are aware of the category and the convenience that a product like Wholly Guacamole provides," he said.
Wholly Guacamole has size options ranging from 2 ounces to 10 pounds with multiple flavor profiles from mild to spicy.
"It's preservative-free, natural and meets the needs of today's busy consumer," he said. "It provides a flavorful way to eat healthy fats and comes in a variety of flavors as well as our convenient mini packaging."
He encourages retailers to feature Wholly Guacamole to generate trials and recommends secondary displays to help drive impulse purchases.