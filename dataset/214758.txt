The 2018 Mid-Atlantic Fruit and Vegetable Convention is set for Jan. 30 to Feb. 1.
The convention will be at the Hershey Lodge and Convention Center in Hershey, Pa., according to a news release.
This year’s event will include pre-convention workshops, farm market bus tour, a trade show and educational sessions.
Charlie Arnot, CEO of The Center for Food Integrity, is the featured speaker for the event, the release said. Arnot will give a presentation titled “Values, Trust and Science: Building Trust in Our Post-Truth Tribal World.”
Workshops include:

 FSMA Grower Training;
 Vegetable Biocontrols;
 Urban Community Farming;
 Managing Your Farm Business; and
 Pennsylvania Pesticide Applicator License Training.

Registration is available at www.mafvc.org or by calling (717) 677-4184 or (717) 694-3596.