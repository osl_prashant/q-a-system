On his west-central Illinois diversified crop farm, Steve Turner is putting pen to paper to nail down the best time to buy inputs—and he’s reaping substantial savings.
“Communication is instrumental to my success,” Turner notes. He says starting conversations earlier with input suppliers has clued him in on discounts and trends that could lead to an uptick in prices, such as recent hurricanes’ impact on fuel prices. For annual costs such as chemicals, seed, fertilizer and fuel, follow the markets and talk with suppliers to capitalize on opportunity, he advises.
Chemical prices are basically flat, but producers likely need to earmark more dollars for chemicals. With herbicide-resistant weeds running rampant, many farmers are using mixed modes of action, multiple times a year—resulting in higher chemical expenses overall.
To help offset a higher chemical bill, talk with your suppliers, who will likely incentivize early orders. Turner prepays herbicide and fungicide in December and January and can save 6% to 8% overall.
You could be money ahead to prepay—even if it means taking out an operating loan. An operating loan will charge around 4% to 5% interest, but you often get up to 8% to 10% off by prepaying your chemical and seed purchase, says Ryan Bristle, an Iowa farmer and associate at Russel Consulting Group. “Look at generic formulations of certain chemicals to save some money as well,” he adds.



Steve Turner
© Lori Turner



David Widmar, ag economist at Purdue University, reminds farmers to consider their seed traits and what that might mean for chemical needs later in the season. For example, your herbicide-tolerance package impacts your overall herbicide needs and the same goes for Bt traits and any rescue insecticide treatments.
Watch fertilizer price trends to jump on seasonal lows. Fertilizer prices are about the lowest they’ve been in five years, says Davis Michaelsen, Pro Farmer’s Inputs Monitor editor. “Nitrogen has gone about as low as it’s going to go,” he adds. With that in mind, Michaelsen says now is a good time to book nitrogen for fall and spring needs.
With the exception of urea, the U.S. imports very little nitrogen, which helps save on costs. In late October, anhydrous prices were ranging between $380 and $475 per short ton.
Wait to buy urea, Michaelsen recommends. “We’ll probably see a price dip in November or December.”
Turner injects a liquid UAN solution through his irrigation systems and plans to wait until early 2018 to buy. “I seldom buy fertilizer before Jan. 1 for accounting, cash flow and storage reasons,” he says. “UAN can get very volatile based on weather, and we’ve saved $60 to $80 per ton by prepaying in January or February.”
Phosphate and potash prices are moving sideways. DAP is between $435 and $460 per short ton; MAP ranged from $435 to $470, and potash was between $315 and $350 in late October, Michaelsen says. “Despite the fact they’re priced above nitrogen, they’re still considered a value at current market prices.”



Seize the Opportunity
© Pro Farmer inputs monitor



Most phosphorus and potassium are imported, which adds uncertainty to retail prices. “We’re going to see increased volatility on fertilizer [prices],” Michaelsen says.
With prices staying relatively low you have an opportunity to maximize fertilizer efficiency. “Use calculators that show your maximum return for nitrogen—they show at what point you have a diminishing return for application,” Bristle says. “Instead of just applying for no reason, find where it provides incremental return.”
Fuel prices are up from 2016. Of all inputs, fuel is the most influenced by forces outside agriculture.
“It seems like global crude oil supplies are beginning to rebalance, which will increase farm diesel prices a little, but I don’t see a huge crazy jump in crude or heating oil futures,” Michaelsen says.
At press time, farm diesel was averaging $2.16 per gallon, 13% higher than 2016’s low of $1.87. He expects farm diesel to top out at $2.25 and drop to around $2.10 at a low, so hold off on buying until winter.
“Usually we see [farm] diesel prices come down just ahead of the first of the year as harvest is wrapping up,” Michaelsen says. “Look for opportunity there, but I don’t see us getting below $2.”
Turner will be looking in December, January and February to lock in at least half of what he needs for fuel.
“That will act as a hedge to keep myself covered,” he says. “If I’m going into spring and don’t have diesel, I don’t want to get caught paying a higher price if weather or something happens to make it jump.”
Propane prices recently jumped, due in part to higher-than-normal demand to dry crops. In addition, export demand for propane is strong.
“We’re 30¢ per gallon over a year ago at $1.32, which is fairly significant,” Michaelsen says. “Prices are well above my projections for a ‘normal’ year.” If harvest brings more demand, farmers could see propane jump another dime per gallon.
Don’t expect big changes in seed prices. Seed’s share of total expenses has taken a drastic climb since the early 2000s, from less than 6% of total expenses to about 14% now, according to USDA. In terms of dollars that’s from $60 per acre to upward of $110 per acre.
“This year, I think the 30,000-foot view on seed is prices will be flat to ever so slightly lower than last year,” Widmar says.  
Discounts for early cash orders or volume provide incentives for farmers, but the closer you get to spring the smaller the discounts become.
“I place a preharvest order to get going, because the company I use offers good discounts, and then I finalize my exact order as soon as possible after harvest,” Turner says. “I order all of my seed from one company to get bigger volume discounts—$20 to $35 off per bag of seed corn.”
“Good deals” on machinery might sour without planning. As farmer sentiment improves, machinery dealers are seeing more activity on their lots, Widmar says.
By the end of 2017, farmers will have made about $30 billion in capital expenditures—37% less than 2014’s high. Machinery dealers will likely provide farmers with opportunities, because they need to move inventory.
Deal or no deal, be disciplined when buying, Widmar advises. “You need to think about the farm’s needs before jumping on a good deal—think strategically about how you spend capital,” he adds.