Global commodities trader Cargill Inc reported a 14 percent rise in quarterly profit on Wednesday on higher demand for beef.
The company, which is one of the world’s largest suppliers of ground beef, said net income rose to $973 million in the first quarter ended August 31, from $852 million, a year earlier.
As beef prices have come down, consumers have bought more, according to Cargill.
The company’s animal nutrition and food ingredients units, which have driven the quarter’s profit, have been showing positive results for the past five straight quarters.
Excluding one-time items, Cargill reported quarterly net income of $888 million, up from $827 million a year earlier. Revenue came in at $27.3 billion, slightly up from the $27.1 billion it posted last year.