U.S. fresh-market apple holdings of 115.8 million (42-pound) cartons on Jan. 1 are up 12% more than a year ago and 19% more than the five-year average of 97.6 million cartons.
Total apples in storage on Jan. 1 were 162.6 million cartons, according to the U.S. Apple Association’s monthly storage report.
That makes total holdings on Jan. 1 up 13% from a year ago and 19% above the five-year average, according to the association. 
Washington state accounted for 90% of the remaining fresh market apples from the 2017 crop in storage Jan. 1.
Top U.S. fresh market variety holdings on Jan. 1, compared with last year, were:

Red delicious: 28.4 million cartons, down 11%.
Gala: 21.7 million cartons, up 10%;
Granny smith: 15.7 million cartons, up 54%;
Fuji: 13.1 million cartons, up 15%;
Golden delicious: 6.6 million cartons, up 8%;
Cripps pink/Pink Lady: 4.6 million cartons, down 12%;