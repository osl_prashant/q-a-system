Many people love cheese, but how certain are you when it comes to selecting the right cheese for a certain event?
Liz Dueland has worked with Wisconsin artisan cheese, and is sharing her knowledge for these products at private events.

Watch the story from the Wisconsin Milk Marketing Board on AgDay above.