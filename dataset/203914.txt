Increasing marketing efforts to kids on and around salad bars may be an effective way to increase vegetable consumption, a new study claims.
 
Called "Marketing Vegetables: Leveraging Branded Media to Increase Vegetable Uptake in Elementary Schools," the study was authored by Andrew Hanks of Ohio State University and David Just and Adam Brumberg of Cornell University. The results of the study were published in the July issue of Pediatrics.
 
The study, conducted in 10 elementary schools in an unnamed large urban district, found 239% more students used the salad bar when it was decorated using colorful banners with vegetable cartoon-characters and fun, nutrition education videos, according to a news release. 
 
For the study, researchers at the Cornell Center for Behavioral Economics in Child Nutrition Programs evaluated two marketing strategies, according to the release:
Using a vinyl wrap-around banners that depicted vegetable cartoon characters; and 
Nutrition education videos narrated by the same characters.
 
Giving kids a choice about choosing a salad bar is important, the author said, because they are more likely to stick to their choice if they can make it freely.
 
"Therefore, kids who are nudged to choose vegetables using fun marketing campaigns are more likely to eat them than those who are forced to take them," co-author Just said in the release.
 
For the six-week study, researchers used Founders Farms trademarked Super Sprowtz cartoon characters - taking on the shapes of broccoli, carrots, spinach, peas and other vegetables - on vinyl wrap-around banners and video segments, according to the release. On the banner and in the video, the vegetable cartoon characters explain the benefits of healthy food choices, according to the release.
 
The study found that in schools where just the banner was used, 90% more students visited the salad bar. The effect was even greater when both banners and videos were used. With that combination, 239% more students selected vegetables from the salad bar, according to the release.
 
Researchers said the study results point to opportunities for using branded media to encourage healthier choices for children.
 
"Vegetable marketing in schools is a low-cost win-win solution for food providers, school meal programs, and students," lead author Hanks said in the release. 
 
"The results of this study highlight how the persuasiveness of marketing media can be leveraged in a positive way by encouraging children to make more nutritious choices."