In its fifth year, the Tour de Fresh cycling event supporting Salad Bars to Schools is rolling out a new ride route, a new jersey design and a festival at the finish line. 
The event is also honoring five produce industry leaders who have supported the race since its inception, according to a news release.
The leaders have supported The California Giant Foundation’s efforts with the event and have travelled nearly one thousand miles, raising $83,700 together in the past five years.
They are:

Mike Deusebio, West Coast produce operations for C&S Wholesale Grocers, riding for Lipman Produce;
Adam Linder, strategic account manager for CHEP;
Jeff Church, vice president of sales for Church Brothers;
Raina Nelson, vice president of sales for Renaissance Food Group; and
Ray Connelly, riding for EBX Logistics.

“Tour de Fresh is truly a movement that marries the passions each individual in this industry possesses — agriculture, nutrition, and the future generations,” Church said in the release. 
“I’m honored to have the opportunity to participate in Tour de Fresh the past five years and look forward to the next five!”
Tour de Fresh is also honoring longtime sponsors including Tsamma, Church Brothers, Pajaro Valley Fresh, Sakata Seeds, Renaissance Food Group, CHEP and Tanimura & Antle, according to the release.