Salinas, Calif.-based Taylor Farms has agreed to purchase a minority stake in Cashmere, Wash.-based Crunch Pak.The partnership opens up regional distribution opportunities across the country for Crunch Pak and gives Taylor Farms access to Crunch Pak’s fruit-slicing technology.
Crunch Pak has been processing apples at facilities in Cashmere and Selah, Wash., and in Rosenhayn, N.J. The company produces the majority of its retail packages in Cashmere and does most of its processing for foodservice in Selah. The Rosenhayn location handles mostly retail products.
As part of the transaction, Taylor Farms CEO Bruce Taylor joins the board of directors at Crunch Pak. Sales, marketing and management roles at Crunch Pak will not change.
Wenatchee, Wash.-based Dovex Fruit Co. and its partners, which will supply Taylor Farms with raw fruit product, retain their majority stake in Crunch Pak.
Crunch Pak has national accounts and has been discussing for years the need for regional distribution, said Tony Freytag, senior vice president of sales and marketing for the company.
Taylor Farms has three facilities that slice apples — in Salinas, Nashville, Tenn., and Swedesboro, N.J.
As the market develops, Crunch Pak will have the opportunity to process and ship from some combination of those facilities as well as its own.
Freytag said that even though Crunch Pak products have good shelf life, being closer to customers is important as the marketplace changes.
“People don’t want large inventories,” Freytag said. “They want to be able to react to changes much faster.”
The improved proximity to customers is expected to increase the company’s speed to market, increase turn and decrease shrink.
“We bring leading-edge technology in slicing the best-quality fruit to the table, but we need faster distribution to market,” Crunch Pak president Mauro Felizia said in a news release. “In today’s business environment, logistics, innovation and collaboration are important. Our partnership with Taylor Farms will give us that synergy.”
While that partnership is still in the early stages, Freytag said the goal is for Crunch Pak to produce at and distribute from some Taylor Farms facilities this fall, in time for back-to-school business.
Taylor Farms could be doing some processing in Crunch Pak facilities as well, though that has yet to be determined. The companies will collaborate on how to maximize the efficiency of all the facilities.
It remains to be seen whether this partnership will lead to Taylor Farms-branded fresh-cut items in the market, Freytag said.
Taylor said in the release that there is an opportunity for the company to do more with fresh-cut items.
“Our investment in Crunch Pak gives us the fresh fruit-slicing technology we need to move into that category,” Taylor said in the release. “We look forward to working closely with Crunch Pak as we explore new ways to deliver an unmatched quality of fresh sliced fruit products across a variety of channels.”
Taylor Farms plans to provide more information at a later date.
The companies plan to meet in Salinas the week of April 17 to discuss the details of the new partnership.
Taylor Farms uses sliced apples in its Apples & Greens Chef Crafted Salad and in its Healthy Harvest snack pack.