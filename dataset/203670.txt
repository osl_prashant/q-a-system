Overall, distributors in the largest U.S. metropolitan region characterize business as favorable.
Wholesalers on and off the Hunts Point Terminal Market in the Bronx sell and distribute to a wide variety of retail, foodservice and wholesale customers that serve many grocery stores and eateries of all sizes throughout the Tri-State region.
"The produce business has been good," said Sheldon Nathel, vice president of Nathel & Nathel Inc. "Sales are doing well and the produce economy is very strong. People are tending to eat healthier and our sales are up."
Movement remains strong at E. Armata Inc.
"Sales have been good," said Paul Armata, vice president. "We have had nice and steady growth. We're not looking to run too hard with anything and want to keep sales consistent even in high and low markets."
Produce business is positive at Baldor Specialty Foods Inc.
"The produce economy is strong," said Mike Muzyk, president. "It is consistent and I would say it's healthy. We are blessed because of our location. We have survived the economic debacles, and because Wall Street law firms and large banks are here, they found a way to survive which ultimately helps the restaurant business and helps our business."
Business is in the black at A&J Produce Corp., which sells to customers that visit the Hunts Point market as well as distributes to clients from Washington, D.C., to Boston.
"Sales are good and produce movement has been good," said Pete Pelosi, buyer and logistics. "We had a good 2015 when everything meshed well for us."
Despite the business optimism, customers remain a little hesitant on spending, said Alfie Badalamenti, vice president of Coosemans New York Inc.
"People are afraid to make moves with their money," he said. "With all the things going on, people are nervous to invest their money and aren't sure what will happen, so they stay still."
For the most part, New York's economy is growing, said Bruce Klein, director of marketing for Secaucus, N.J.-based Maurice A. Auerbach Inc.
"There's still unemployment, but things are improving," he said. "Consumer spending hasn't really come back. People are still buying product but they're being very careful on what they're buying. They shop around and the main thing is people are really watching what they buy."
Business is strong but not without its challenges, said Benjamin Friedman, owner of Englewood, N.J.-based Riviera Produce Corp.
"For the larger players, the economy is good, but there are more and more obstacles for the smaller guys and the independents to grow," he said. "For the economy overall, everything is steady but there's not a lot of sunshine. The economy is still challenging. We just operate every day, but business is good."
The talk on the street is mixed, said Ciro Porricelli, partner with Jerry Porricelli Produce.
"On the city's economy, it depends who you talk to," he said. "With some purveyors, they say the restaurants are hurting and are getting collections. But when you talk with others, they're doing a good job."
The economic concern is real, said Herb Joynt, business development for Holtsville, N.Y.-based J. Kings Foodservice Professionals Inc.
"Customers are more concerned about the economy," he said. "One issue is raising the minimum wage and how it will affect their businesses. They are very open to suggestions on how we can help them make sure they are running their businesses properly and efficiently, whether through evaluating new products or how they can eliminate costs out of the system. It's something everyone's facing."
In New Jersey, produce movement is expanding, said Joe Granata, director of produce sales for West Caldwell, N.J.-based FreshPro Food Distributors.
"Business has been solid. It's pretty good," he said. "We've had a lot of new business. The overall economy has also been good. It's fairly steady without too much change."
Business is good for A.J. Trucco Inc., said Sasha LoPresti, director of business development.
"Produce sales are consistent and the economy is strong," she said. "As time progresses, our customers are doing more business with us and we are growing every year."