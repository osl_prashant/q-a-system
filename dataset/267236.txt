BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Wednesday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2525  Down  11May         2471    2517    2440    2475  Down  23Jul                                 2543  Down   8Jul         2514    2566    2488    2525  Down  11Sep         2531    2581    2507    2543  Down   8Dec         2531    2584    2510    2548  Down   7Mar         2510    2575    2507    2539  Down   5May         2516    2579    2513    2544  Down   4Jul         2518    2577    2518    2550  Down   4Sep         2553    2556    2553    2556  Down   4Dec         2558    2560    2557    2560  Down   5Mar                                 2582  Down   6