Corn is finally beginning to break out of ‘cold storage’ in eastern South Dakota and much of the Midwest after periods of cold temperatures. With planting just wrapping up, many producers have expressed concerns about evaluation of chilling injury in their corn crop.Stand Count
At this point, a stand count is an easy evaluation of damage and can be accomplished by following these simple steps:
Identify your row width.
Measure the specified length for your row width (Table 1).
Count the emerged plants within the length of row.
Multiply by 1000 to find plants/acre.
Repeat this process across your field in random locations several times and average.
Table 1. Guide to Quick Stand Counts (1/1000 acre)
Row Width
Length of Row
38”
13’ 9”
36”
14’ 6”
30”
17’ 5”
22”
23’ 9”
20”
26’ 2”
15”
34’ 10”
 
Management Considerations
If you see a problem area within your field, you should consider taking stand counts 4-5 times within that general area, and 4-5 times within the unaffected area of your field for comparison purposes. Poor stand can be caused by several factors including soil temperature, chilled water, moisture issues, insects, vertebrate pests, plant disease, wheel tracks or compaction, and any improper planter adjustments.
If you feel you have a stand issue in your field, there are many considerations to take into account before replanting.