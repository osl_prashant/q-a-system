Not reaping the same benefits of new variety development as the apple industry, the evolution of the Northwest pear industry will lean on advancements in packing and packaging technology, industry sources said.
Demand for pouch bag packaged pears has increased, said Ed Weathers, vice president sales manager, Duckwall Fruit, Hood River. "It has reinvigorated demand for bagged pears," he said, noting that bagged pears have not been a popular item in recent years.
Besides the growth in pouch bags, the primary carton for pears remains the 4/5-bushel of wrap-packed fruit.
"We are seeing more retailers and customers going to a euro style, 2-liter tray," he said. In addition, the two-fifths bushel, two-layer wrapped pack is popular with specialty pears, he said.
After apple and cherry industries packers have benefited from optical sorters and sizing, David Garcia, president of Diamond Fruit Growers, Hood River, said the pear industry is waiting for similar technology to help pear packers. "We are looking for that in the pear industry," Garcia said. The impact will be reduced labor, he said.

Wrapped pack endangered?
The majority of pears are hand wrapped in 4/5-bushel cartons, which requires a lot of labor, which typically tends to be older workers. "We have to see what type of package is going to replace that packed carton," Garcia said. Pear skin is sensitive and tends to scuff in a naked pack on a long journey, so wrapped packs may be hard to replace in export channels.
In the U.S. market, Garcia said larger-sized pears are tough to place in pouch bags.
"I see domestic changing to some type of package that doesn't require so much labor," he said.
The shape of pears makes it difficult for defect sorters to get all the necessary camera angles for electronic sorting, said Andy Tudor, director of business development for Rainier Fruit Co. in Yakima, Wash. But he said manufacturers are getting close to solving the problem.
Looking ahead, Weathers said the pear industry may experience slow and steady growth. "I don't see any large spikes in pear production, simply because of the time it takes to get pears into production," he said.
Most of the pear ground is already planted so there isn't a lot of room for additional acreage. "It is more increases in the productivity of the land versus planting more land," he said.