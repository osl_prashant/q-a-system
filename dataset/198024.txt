Reiterates intention to hit Mexican sugar, Canadian lumber with duties if trade talks fail






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






Getting the North American Free Trade Agreement (NAFTA) renegotiations with Canada and Mexico completed by January 2018 marks the "best window" for the discussions to conclude, according to US Commerce Secretary Wilbur Ross.
Focus on January comes as Ross acknowledged political matters figure to impact negotiations if they move well into 2018 – Mexican and US elections. "Their elections are mid-year. The closer you get to it the more complicated it would become, particularly in terms of getting Mexican congressional approval," Ross said at an event held at the Bipartisan Policy Center. Plus, the US fast-track negotiating authority needs to be reconfirmed in July 2018, as US mid-term elections loom in November. "Those will undoubtedly have some impact on congressional views," Ross said.
The White House issued a denial on any NAFTA timeline. The denial came amid a report in the Mexican press that Ildefonso Guajardo, Mexico’s economy secretary, told a closed-door gathering of business officials that he and Lighthizer agreed on the Dec. 15 date during a bilateral meeting earlier this month. Guajardo was quoted as saying he and Lighthizer had "analyzed calendars" and chosen the mid-December deadline because it would give both countries' legislatures enough time to approve the updated deal before the Mexican presidential election in July 2018.
USTR said no date has been set. "The sooner we can conclude negotiations, the sooner we can address the concerns of the president and the American people with NAFTA,” Emily Davis, a USTR spokeswoman, said.
While tipping his hand little on the administration's focus on the NAFTA talks, Ross said, their first guiding principle will be to "do no harm" in the process. “There were some things that were achieved under NAFTA and under other trade agreements,” he remarked.
A second rule for the talks would be to bring in concessions Canada and Mexico made as part of the Trans-Pacific Partnership (TPP) agreement, Ross observed. "We would view those as a starting point for discussion,” he said. Digital trade issues and gaining access in the services sectors would also be areas where the administration wants to see advances.
In a similar vein, Ross indicated they want to bring into NAFTA some basic areas on trade matters that would be used in future pacts. The goal there is to have "basic principles we would like to have followed in subsequent trade agreements rather than starting each one with a blank sheet of paper."
On lowering trade deficits with Canada and Mexico, Ross said the US would push both to buy more US goods than the same or comparable items from other countries. "For example, some of the agricultural products they buy come from Brazil," he stated. "Well, they could just as well give us a little better market share than we have now."
Ross also repeated the US was prepared to hit imports of Mexican sugar and Canadian softwood lumber with antidumping and anti-subsidy duties if negotiated settlements cannot be reached in those disputes. There is a June 5 deadline for the US-Mexico sugar deal to be wrapped up.
Meanwhile, Reuters reported Canada is ready to provide around C$850 million ($630 million) in aid to help the softwood lumber industry after the US imposed duties shipments into the US. Most of the aid will be loans and loan guarantees, the report noted, with a formal announcement seen today (Thursday).


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.