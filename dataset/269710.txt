BC-Cotton
BC-Cotton

The Associated Press

NEW YORK




NEW YORK (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Tuesday:
OpenHighLowSettleChg.COTTON 250,000 lbs.; cents per lb.May83.2883.5882.0683.13—.12Jul83.2883.4981.9782.95—.27Sep78.40—.14Oct78.95—.27Nov78.40—.14Dec78.5078.6877.9278.40—.14Jan78.46—.21Mar78.6178.6978.1778.46—.21May78.3878.5478.3578.51—.10Jul78.0678.3678.0678.31+.05Sep73.55+.05Oct75.32+.04Nov73.55+.05Dec73.5073.5573.5073.55+.05Jan73.84+.28Mar73.84+.28May74.26+.28Jul74.34+.28Sep72.68+.28Oct73.91+.28Nov72.68+.28Dec72.68+.28Jan72.72+.28Mar72.72+.28Est. sales 38,604.  Mon.'s sales 37,480Mon.'s open int 268,410