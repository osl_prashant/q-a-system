Animal rights activists flew a drone around a livestock research facility and allegedly near feedlots in Nebraska this week.The animal rights group Showing Animals Respect and Kindness (SHARK) shared on both Facebook and Twitter that members of the organization were photographing livestock at the U.S. Meat Animal Research Center outside Clay Center, Neb. Photos taken on Wednesday show sheep at the U.S. Department of Agriculture’s (USDA) research facility and there is also a photo of a Homeland Security officer with his vehicle.

The Research Center was targeted because of a 2015 story by The New York Times that pointed out possible abuse at the USDA facility.
SHARK animal rights activists with were seen driving a dark colored Dodge truck with Illinois plates. The group is based out of Geneva, Ill.
While in Nebraska there were reports made to the Nebraska Cattlemen’s Association (NCA) that the vehicle and a drone were seen near multiple feedlots. Counties where sightings were made include Custer, Franklin and Clay.
Officials and members of NCA believe the activists were not only trying to get drone footage of feedlots, but specifically deceased cattle from the April 28-30 blizzard that hit the Great Plains.
NCA executive vice president Pete McClymont says this is unfortunately the type of world we live in now and he wants livestock producers to be aware of these threats.
“These people aren’t interested in helping animals,” McClymont says. “They want to further their cause which is not part of research to help increase food production or produce food for a world that needs more meat protein.”
Producers should be on the lookout for suspicious vehicles and notify local authorities of any strange activity they believe to be activist related. It is not advised to confront possible animal rights activists yourself.
In some states near Nebraska, like Iowa, Kansas and Missouri there are laws in place to protect farmers and ranchers from undercover videos and photography by activists groups. Nebraska failed to pass similar legislation in 2012 and 2013.
McClymont doesn’t have an official estimate of cattle killed during the April 28-30 snowstorms in Nebraska.
“We didn’t have as severe of conditions as other places,” McClymont says.
There were some cattle deaths in Nebraska, but not to the same extent seen in Southwest Kansas or Oklahoma Panhandle. 
If any livestock were lost during recent weather events they should be picked up by a rendering service as soon as possible and covered until pickup to reduce the chance of activists negatively using images.