Just do it. 
How simple is that phrase? I know, it is a well-recognized part of a shoe company’s advertising campaign as well.
 
A similar line is popular with the New England Patriot’s coach Bill Belichick: Do your job. That goes back to the year 2000. However, it sure seems I’ve heard produce managers apply this command longer ago than that. 
 
As far as I can remember, anyway.
 
I recall another version of the “do your job” direction when I worked briefly for a produce brokerage around 20 years ago. We sourced, bought and sold produce from numerous states. The pace was grueling, the hours were long and it seemed we were constantly dispatching or checking up on wayward produce trucks as they made numerous pick-ups and drops in distant markets.
 
The advice I received going into that job was simply “Do what you say you’re going to do, when you say you’re going to do it, and you’ll be successful.”
 
For me, that was sending produce price quote sheets to buyers in the wee hours of the morning. Then I’d call and let them know I had a truck working in the source state, when I planned to load in which area, and what day later that week they could expect product to arrive. 
 
I assembled orders, figured my trailer cube and weight capacity, tried to get everything loaded (sometimes requiring two or more trucks at a time) and into our market at a competitive rate and on time.
 
The operative word, of course, is tried. Things rarely work like clockwork in that often unpredictable facet of the produce business. However, the closer I came to doing what I said I’d do, the better the business.
 
In the retail produce aisle, it’s the same process, just on a different scale.
 
If I was supervising stores that had a half dozen assistant produce managers vying for a single upcoming promotion, the accountability factor weighed pretty heavily in deciding who got the job.
 
Who followed up on the request to build and forward a worthy display photo to share with the company? Which assistant volunteered for a challenging special project? Or which one simply called back when they said they would? 
 
The one whose name consistently matched up with these questions (and more) was usually the person that was awarded their own produce stand, regardless of seniority.
 
No matter what level a person works, it’s more than just nice knowing who to call when something needs to get done — it’s a vital part of any successful business. Action always speaks louder than words.
 
“You can’t build a reputation on what you’re going to do.” — Henry Ford
 

Armand Lobato works for the Idaho Potato Commission. His 40 years’ experience in the produce business span a range of foodservice and retail positions. E-mail armandlobato@comcast.net.
 
What's your take? Leave a comment and tell us your opinion.