SRI International wants to change harvesting of apples and other tree fruit through robotics, and has created a technology company designed to reach that goal.
The Menlo Park, Calif.-based SRI has created Abundant Robotics, a company it spun off to commercialize and build on several years of agricultural robotics research, according to a news release.
Abundant Robotics' initial prototype was designed with SRI technology to enable automation of apple harvesting.
Future developments may address other produce and other steps in the production process, according to the release.
"Bringing automation to orchard agriculture is an important step in addressing nutritional demands and making high-quality fruit available to a growing population," Manish Kothari, SRI Venture's president, said in the release. SRI Venture focuses on "high-value venture opportunities," according to the company's website.
"While orchard yields have significantly improved over the last two decades, labor productivity has not," Dan Steere, Abundant Robotics' CEO, said in the release. "Our goal is to deliver robotic systems to ease the hardest jobs in agriculture. The first automated apple harvesting system that doesn't bruise or damage the produce will be a huge breakthrough in an industry that has been dependent on the challenges of seasonal labor."
The project is supported through research funding from the Yakima-based Washington Tree Fruit Research Commission.
The commission, SRI and Abundant Robotics recently closed its initial investment round, according to the release.
Michael Eriksen, Curt Salisbury and Dan Steere, Abundant Robotics' co-founders, share early backgrounds in agriculture. At SRI, they joined together to apply robotics expertise to agricultural production, according to the release.
SRI works primarily in advanced technology and systems, biosciences, computing and education.
The company produces technology through licensing, spin-off ventures and new products.