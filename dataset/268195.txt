BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Tue. Apr 10, 2018          USDA Market NewsDaily California EggsPrices are unchanged. Trade sentiment is sharply lower. Demand into allchannels is mostly moderate. Supplies are light to moderate. Offerings areheavy. Market activity is moderate. Monday's shell egg inventories declined0.4% in the Southwest and 1.3% in the Northwest.Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                283EXTRA LARGE          277LARGE                275MEDIUM               184Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 10-Apr-18 11:24 AM E MP