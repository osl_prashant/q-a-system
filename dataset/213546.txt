Salford announces its Valmar 9620, boasted as the world’s largest capacity pull-type pneumatic boom applicator, is now in full production.The newest addition to Salford’s Valmar Air Boom lineup offers a number of features to help customers see return on their investment, including (but not limited to):
660 cu. ft. hopper capacity covers at least 110 acres per hour with less down time for fill ups
70 ft. boom width to cover more acres in less time, with accuracy
ISOBUS compatibility to follow prescription maps with left/right section control, allowing maximized yields with minimal waste
Micro hopper in each model with optional secondary metering for custom two product blends
Hydraulic booms for folding and manual height control
Heavy duty walking tandem axle
The 9620 is the result of a marriage between two of Salford’s already established product lines – Salford’s Valmar Air Boom line, and Salford’s BBI MagnaSpread lineup. Salford acquired Valmar in May, 2015. Salford’s BBI MagnaSpread line is made up of high capacity, hydraulic pull-type spinner spreaders. With a combined 60 years on the market, the technology in these units has been tested under differing conditions, seasons, and even generations of farmers across North America.
The 9620 combines the reliable, accurate metering and renowned air flow technology of the Valmar, and the heavy duty, high efficiency hydraulics and high capacity of the MagnaSpread.