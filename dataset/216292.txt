Frank Forcella was surrounded with apricot pits saved in an endless line of 5-gallon buckets. Apricot trees don’t fare well in the cold of Minnesota, but Forcella scratched out a hobby harvest each year, and hit a bumper crop in 2007. More apricots; many varieties; and a massive jump in his mother lode of pit buckets. What to do with a never-ending pit collection?
Commercial fruit processors often grind a portion of shell pits for use in commercial sand blasting as a soft grit. As an ARS research agronomist, Forcella believed organic grit might be an effective weed killer. His determination to break from convention has resulted in a four-row grit blaster capable of obliterating weeds.
Initially, Forcella bought a cheap, handheld sandblaster and hooked it to an air compressor at the North Central Soil Conservation Research Lab in Morris, Minn. He tested grit on greenhouse seedlings and found a split-second blast destroyed the weeds. Next, he purchased a bigger blaster, mounted it on an ATV, and switched to corn cob grit. Driving down rows and manually gunning down weeds, Forcella knew the system had potential. He wrote a funding proposal for a larger machine and was approved for a Sustainable Agriculture Research and Education (SARE) grant.
High-velocity fertilizer
Dan Humburg, an ag engineer at South Dakota State University, and grad student Corey Lanoue built the tractor-hauled grit applicator. “Typically, when you’re sandblasting, operations are very power hungry. Trying to find the optimum balance between the application rate and still satisfy the farmer was difficult. We tried to maximize efficiency and ultimately that’s turning compressed air into abrasive velocity,” says Lanoue, currently a design engineer with Vermeer.
With four rows and eight nozzles firing grit at 100 lbs. per square inch, Forcella runs the system through corn field plots twice every season, each nozzle blasting a 4” band. (First, when corn is 4-6” high at the 1- to 3-leaf stage. Second, when corn is 1’ tall at the 5-leaf stage.) His field trials have shown 80% to 90% season-long weed control. In addition, Forcella has successfully tested a weed-and-feed method for organic growers, using cottonseed and canola seed meal. Essentially, he’s killing weeds by spraying high-velocity fertilizer.
What about crop damage? “One of the things we were worried about was disease. Make a hole in a crop plant and open a door to crop disease. However, there are no problems because the crops aren’t hurt at all. The grit is aimed at the base of the corn plants and nails the small weeds. Weeds must be below a couple of inches or the system won’t work,” Forcella notes.




 


Costs to run the grit applicator system range from $50-$100 per acre due to diesel and grit expenses.


© Frank Forcella







When a broadleaf weed – such as Palmer amaranth – is hit with grit, the stem is severed and the root cannot regrow. The root system withers due to a lack of photosynthesis. However, when grasses germinate, the growing point is still below the surface. Blasting grit will eliminate the leaves, but the grass continues growing. Specific grasses must be hit at least twice to kill them, according to Forcella. “We’re going over twice anyway to compensate for early-germinating and late-germinating broadleaf weeds.”
Pigweed slayer
Forcella’s grit applicator has gained attention from Sam Wortman, a weed scientist with the University of Illinois Department of Crop Sciences. Wortman is working with South Dakota State University to build a device specifically for vegetable crops on a commercial scale. Manuel Perez, with the Department of Agroforestry Engineering at the University of Seville in Spain, is designing a grit-blasting system with robotics for application in olive orchards and vineyards.
Costs to run the grit applicator system range from $50-$100 per acre due to diesel and grit expenses. “A conventional grower doesn’t want to spend over $50 per acre for weed control, but that’s pretty good for an organic grower,” Forcella says. However, he notes changes in agriculture may open the conventional door to the weed blaster – particularly the spread of herbicide-resistant Palmer amaranth. “Palmer continues to creep upwards. When I see pictures from the South of workers chopping Palmer and throwing it in carts – it looks like images from 100 years back. In situations where there are no reliable alternatives, a grit applicator could work for a conventional farmer.”
From apricot pit to grit applicator, Forcella has found success. “As a weed scientist, I wondered if you could kill weeds with grit. On its face, it’s certainly a crazy idea, but I couldn’t shake the thought. This is still small-scale, but it works.”