BC-Wheat KX
BC-Wheat KX

The Associated Press

CHICAGO




CHICAGO (AP) — Winter Wheat futures on the Chicago Board of Trade Thu:
Open  High  Low  Settle   Chg.WHEAT                                   5,000 bu minimum; cents per bushel    Mar      466½  471¼  460    470¾   +5  May      483¼  486¾  475¾  486¼   +4½Jul      500½  504½  493    504¼   +4¾Sep      521¼  522¼  510½  522     +5  Dec      538¼  542½  530½  542½   +5½Mar      548    553½  541¼  553¼   +5¼May                           556     +5¾Jul      547    553¾  547    553¾   +7  Sep                           560½   +6¾Dec      570    576    570    576     +6¾Mar                           576     +6¾May                           576     +6¾Jul                           576     +6¾Est. sales 54,106.  Wed.'s sales 80,898 Wed.'s open int 303,751