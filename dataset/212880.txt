(CORRECTED April 28) President Donald Trump said he’s optimistic NAFTA can be renegotiated, signalling a change from the desire to scrap the free trade agreement. 
According to a summary of Trump’s April 26 call with Mexico’s President Peña Nieto and Canadian Prime Minister Trudeau, he agreed not to terminate NAFTA “at this time” and the leaders agreed to proceed swiftly on renegotiating NAFTA.
 
“It is my privilege to bring NAFTA up to date through renegotiation,” Trump said in a news release. “It is an honor to deal with both President Peña Nieto and Prime Minister Trudeau, and I believe that the end result will make all three countries stronger and better.”
 
“I received calls from the President of Mexico and the Prime Minister of Canada asking to renegotiate NAFTA rather than terminate,” Trump said in a April 27 tweet. “I agreed subject to the fact that if we do not reach a fair deal for all, we will then terminate NAFTA.”
 
Only the day before, there were reports through Politico citing White House sources that Trump was strongly considering an executive order that would pull the U.S. out of NAFTA, as he had done in January with the Trans Pacific Partnership. The TPP was a 12-nation pact negotiated by President Barack Obama but not yet ratified by Congress when Trump pulled the plug.
 
Sigh of relief
 
Industry leaders welcomed the news of Trump’s interest in  renegotiating NAFTA, especially after recent reports that Trump was considering pulling the U.S. out of NAFTA.
 
“We have benefited greatly from NAFTA and we want to see it continued,” said Mark Powers, president of the Northwest Horticultural Council, Yakima, Wash. “We believe that it can be improved, but the majority of what is being discussed really isn’t focused on agriculture.”
 
Powers said the Trump administration appears focused on provisions addressing manufacturing goods, environmental and labor provisions of the deal.
 

Western Growers president and CEO Tom Nassif said in a statement that the group agrees with Trump’s decision to not immediately withdraw from NAFTA.
 
“To facilitate enhanced competition for U.S. fruit and vegetable farmers, we ask the Trump Administration to work with our North American trading partners to further work on science-based standards, harmonize sanitary and phytosanitary standards and eliminate other non-tariff trade barriers,” Nassif said in the statement.”We look forward to participating in the effort to modernize NAFTA for both agriculture and the broader American economy.

 
Powers said Trump will have to formally alert Congress about his intention to renegotiate NAFTA, which he had not done as of April 27. That starts a 90-day clock before the deal can be renegotiated. When complete, it will be presented to Congress for an up or down vote, according to terms of the president’s negotiating authority outlined in the Trade
Promotion Authority. 
 
“I am optimistic they are going to find a way to address the various issues and to come out with a successfully renegotiated NAFTA,” Powers said.
 
If the process fails and U.S. withdraws from NAFTA, Canada and Mexico would have to be given at least six months notice, Powers said. If NAFTA were revoked, Mexican tariffs would snap back to pre-1994 levels of 20% for apples, pears and cherries. Canada’s tariffs would likely be zero on fruit, since the U.S. and Canada had a separate free trade agreement prior to NAFTA.
 
Powers said that agriculture-related changes to NAFTA may mirror in some respects provisions from the Trans Pacific Partnership, notably regarding to dispute resolution. Agriculture groups are providing comments to the U.S. Trade Representative and the USDA on provisions the groups want in a revised NAFTA.
 
Kam Quarles, vice president of public policy for the National Potato Council, said NAFTA renegotiation will gain momentum when Robert Lighthizer is confirmed as the next U.S. Trade Representative. That is expected soon; the Senate Finance Committee unanimously approved his nomination April 25 and the full Senate may vote to confirm him in early May.
 
The National Potato Council sent a letter to lawmakers earlier in April outlining the potato industry’s priorities in a renegotiated NAFTA, and the White House has been circulating a text of their priorities.
 
Even though there are individual trade issues with both Mexico and Canada, both are very valuable export markets for the potato industry, Quarles said. 
 
“Simply walking away from NAFTA would be destructive for agriculture and for a variety of industries, but re-looking at an agreement that is 25 years old is actually a good thing and ought to be done,” Quarles said April 27. “It’s a positive that they want to take this seriously and get better terms of trade for American producers.”
 
Note on correction: The article originally had the timeline for renegotiating incorrect.