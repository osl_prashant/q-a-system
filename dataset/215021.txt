Ag Against Hunger’s annual “The Produce Express” campaign raised $10,000 for the organization and its programs.
Ag Against Hunger works with growers, shippers and processors to provide produce to local food banks, assistance programs and schools in California’s Monterey, Santa Cruz and San Benito counties, according to a news release.
This year, the organization worked with Valley Produce to sell the produce at the Los Angeles Market, the release said.
Grower-shippers that contributed vegetables were:

Green Giant;
Mann Packing;
Taylor Farms;
Tanimura & Antle;
Dole; and
Ocean Mist.

According to the release, Smartway Transportation covered freight costs and transported the produce to the market.