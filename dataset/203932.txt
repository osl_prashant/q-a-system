Hungenberg Produce Inc. is growing organic carrots for the first time this year.
The Greeley, Colo.-based carrot and cabbage grower-shipper expects to harvest about 60 acres of organic carrots in 2016, said Jordan Hungenberg, salesman and food safety director.
"We just jumped in cold turkey and are learning as we go. I thought organic would be a fad, but it's not going away. We felt we were a little behind."
As of late July, the first organic crop looked good, with no insect or disease pressures.
"It's been so dry, we've been able to control the water," Hungenberg said.
There have been higher costs associated with organic, he said, particularly in weed control measures.
"With the short growing season, it's harder to till the weeds."
Hungenberg Produce is in talks with Colorado Safeway and Kroger stores to buy the company's organic carrots this year. Product will ship in bulk and possibly some cello packs and be certified by both the U.S. Department of Agriculture and the Colorado Department of Agriculture, Hungenberg said.
 

Good growing weather, lots of water

On the conventional side, meanwhile, Hungenberg Produce has enjoyed good late spring and summer growing weather and abundant water supplies, Hungenberg said.
The company began shipping conventional carrots and cabbage July 12. Hungenberg Produce is growing about 1,200 acres of carrots and 400 acres of cabbage, similar to last year, Hungenberg said.
"It's going really well. Sales have been decent."
Hungenberg Produce expects to ship cabbage through Halloween and carrots through Thanksgiving.