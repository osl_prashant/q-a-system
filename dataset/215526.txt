Sponsored Content
Desiring to control erosion, build organic matter, or reduce nutrient loss, farmers in your area may have seeded cover crops this fall. Have you talked to them about their plans to terminate these beneficial cover crops next spring? 
Selecting the best herbicide(s) depends on a number of factors. A bulletin from Purdue Extension titled “Successful Cover Crop Termination with Herbicides” does a good job of listing and explaining these factors:

“The cover crop species
The cover crop growth stage
Other weed species present
The production crop to be planted
The weather conditions at application”

Another article from Iowa State Extension has a number of important reminders specific to herbicide control of cover crops. The authors recommend increasing application rates of contact herbicides, such as paraquat and glufosinate, if the overwintered cover crop has a dense canopy. Also always check herbicide labels for restrictions for the subsequent crop. 
Glyphosate products such as Credit® Xtreme are popular herbicides for cover crop termination. Depending on which cover crops are used and whether other weed species are present, a tank-mix partner may be recommended. Credit Xtreme has excellent tank-mix compatibility. 
Sponsored by Nufarm