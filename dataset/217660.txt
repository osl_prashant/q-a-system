The United Fresh Start Foundation is taking online bids for auction items to raise funds for the foundation in the “Bid for Kids Auction.”
Bids for auctions items began Jan. 3 and will continue through 8 p.m. Pacific Jan. 16, according to United Fresh.
Information about the process is available online. The online auction is combined with in-person bidding at the Foundation Gala dinner and reception, set for 6 p.m. Jan. 16 at the Rancho Bernardo Inn, San Diego, Calif.
The event will honor H-E-B senior vice president Hugh Topper, who will be presented the United Fresh Lifetime Achievement Award.