Leamington, Ontario-based NatureFresh Farms is getting ready to kick off its first tomato season with all 45 acres of its Delta, Ohio, greenhouse operation under production.  
“Having all three of our 15-acre growing phases start this September will increase our ability to service our customers’ demand for locally grown tomatoes through the winter season,” Peter Quiring, president and owner, said in a news release.
 
NatureFresh Farms uses high pressure sodium lighting in its greenhouses to enable production from fall through spring, according to the release.
 
The company expects its OhioRed locally branded line and its TOMZ snacking tomatoes line to be available throughout that time period, according to the release.
 
OhioRed includes tomatoes on the vine, red and green beefsteaks, and romas; and TOMZ includes red, yellow and orange grape tomatoes; red cherry tomatoes; mixed medley and cocktail tomatoes.
 
“Demand has pleasantly increased over the year for our Ohio product, and this growth will guide us toward increasing future production to meet the demand,” Quiring said in the release.
 
NatureFresh plans to exhibit the full line of OhioRed and TOMZ offerings at the Produce Marketing Association’s Fresh Summit, Oct. 19-21 in New Orleans. The company is scheduled for booth 5029.