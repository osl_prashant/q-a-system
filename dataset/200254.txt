Dairy cows are getting bigger. And that's not a good thing.Walk any pen of mature cows and you can't help but be struck by the size of these beasts. It doesn't matter if you're in a registered herd chasing classification scores or a commercial herd milking thousands of cows. Cows are getting bigger.
In fact, Holstein cows are gaining about 50 lb. of body weight every decade. Mature cows that used to scale at 1,400 lb. are now pushing 1,500 or even 1,600 lb.
And it won't result in increased milk production nor better feed efficiency, says Lou Armentano, a University of Wisconsin dairy nutritionist. "Size and yield are not genetically correlated," he says.
"But we are currently selecting for big cows, reducing positive [genetic] pressure on yield while selecting against feed efficiency. This is a lose-lose scenario."
Bigger cows take more feed to reach their mature body size and more feed to maintain that body size once they mature. As a result, feed efficiency drops over the lifetime of the animal.
Increasing milk production helps, but it becomes less and less of a factor as milk per cow increases. When production jumped from 10,000 lb. per cow to 20,000 lb, there was a huge increase in feed efficiency. In other words, one cow produced as much as two.
"But when you go from 20,000 lb to 30,000 lb, you don't get the same increase in efficiency because the maintenance requirement is less of the total feed requirement," Armentano explains.
The maintenance requirement becomes even less as milk production climbs from 30,000 lb to 40,000. "The dilution of maintenance is one of diminishing returns," he says.
Why are cows bigger?
The mystery is why cows are getting bigger when selection indexes, such as Net Merit$ (NM$), have a negative weight for body size. Kent Weigel, a dairy geneticist also with the University of Wisconsin, has two possible explanations:
"One explanation is producers who like bigcows might ignore NM$ in their sire selection decisions, or at least that part of it.They might select high NM$ bulls (or high TPI bulls), but among the bulls that rank highly for the index they also pick those with high PTA for overall type score, which would favor large frame size," he says.
"The other explanation is that large frame size, and especially stature, is correlated with udder depth.So when farmers select bulls with high udder composite values, they are actually selecting bulls that sire taller than average daughters," Weigel says.
The expected change in body size due to selection for NM$ pretty close to zero, about -0.07 PTA units per year, mainly because of the positive correlation between udder depth and stature."When producers put a little extra weight on PTA type, it's more than enough to offset the expected change, and cows get bigger instead of smaller," he says.
Solution is two fold
The first solution to slowing the increase in cow body size is to not use bulls that sire bigger daughters. Farmers need to be aware of the positive correlation between udder depth and stature, and PTA type and stature.
High, tight udders are still critically important, but the push for better udders should not be compounded by also selecting for higher PTA type.
The second solution is more long term: Selecting cows with higher feed efficiency. "And you can't do that without feed intake measurements," says Armentano. That's a long-term project now being studied by universities both here and in the Netherlands.
The goal is to get actual feed intake and efficiency estimates and then incorporate those numbers into genomic selection to infer which bulls sire daughters that are more efficient. That will take a reference population of at least 8,000 cows, and perhaps even more. Getting that data is not easy, fast nor cheap.
"The one thing we can do now is to stop breeding for bigger cows," says Armentano. "Bigger is not better." 

Note: This story appears in the December issue of Dairy Herd Management.