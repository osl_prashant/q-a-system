David Hollinger, on one of many trips to Philadelphia to buy produce for his family's Hollinger Farmers Market, had an idea: Other markets in his town had begun paying him to buy their produce, and he was making good money from doing so.
Within two weeks of conceiving Four Seasons Produce, David, his father and a third partner secured a loan and the company opened for business. David Hollinger became the sole owner within several years.
Current Four Seasons CEO Ronald Carkoski joined the company in large part because it was still family-owned.
After years of working at Gateway Foods, Carkoski didn't like the changes after that family-owned company sold to a major corporation.
"A friend of mine came back from an interview with Four Seasons and told me they were looking for a buyer," says Carkoski. David Hollinger hired Carkoski as the director of procurement, promoted him to vice president of operations in 1998, and finally to CEO in 2002.
When it became apparent that the company needed to redefine its purpose and direction, Carkoski took action.
He put together a committee to develop a new mission statement, which yielded its current statement: "Growing ideas, producing excellence". He also identified four non-negotiable ways of running a company: integrity, exceptional partnership, dynamic leadership and a winning culture.
"It was just formalizing what we already saw and knew about the company," Carkoski says of the changes. "One person can't take credit for all that, it's a group effort."
Perhaps the greatest legacy from this group effort is Four Seasons' learning map experience.
An annual third-party survey consistently revealed that employees didn't understand the direction the company intended to go, so Carkoski and his team developed the learning map experience, a five-year vision for the future compressed and refined into three goals. Those goals were then incorporated into a mural, which is now in every building of the company.
New employees participate in an interactive explanation of the mural during their first day. After 90 days, once they understand more about how Four Seasons functions, they revisit the learning map. Since implementing the experience, survey results have improved dramatically.
To celebrate its 40th anniversary, Four Seasons hosted a company picnic on June 18. It featured catered food, music, a petting zoo and rides for kids.
"It was a perfect day. Out of 10, it was a 12," Carkoski says.
Although Four Season's September golf outing is an annual event, the company plans to do something special this year.
"We'll be having an upscale dining experience on the driving range of one of the golf courses,"
The proceeds of that golf tournament will go to the Make-A-Wish Foundation, which Four Seasons has partnered with for 24 years.