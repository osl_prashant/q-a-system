BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Tuesday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2505  Up    33May         2433    2516    2430    2480  Up    35Jul         2462    2537    2457    2505  Up    33Sep         2472    2549    2472    2516  Up    29Dec         2472    2546    2472    2516  Up    29Mar         2478    2534    2476    2505  Up    29May         2469    2533    2469    2510  Up    28Jul         2520    2533    2515    2518  Up    27Sep         2524    2541    2523    2526  Up    27Dec         2534    2551    2533    2535  Up    26