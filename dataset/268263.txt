Veolia to pay state $1.6 million to settle spill allegations
Veolia to pay state $1.6 million to settle spill allegations

The Associated Press

BOSTON




BOSTON (AP) — Wastewater treatment plant operator Veolia North America has agreed to pay $1.6 million for a sewage spill that caused shellfish bed closures in Plymouth Harbor.
The town of Plymouth sued the company following the discharge of hundreds of thousands of gallons of untreated wastewater into the harbor, which officials say was the fault of Veolia.
The seafood bed closure was due to the risks posed to humans from eating shellfish in contaminated waters.
Attorney General Maura Healey's 2016 complaint says Veolia failed to properly maintain the system of pipes that carry wastewater from customers to the treatment facility, which eventually caused the spill to start in Dec. 2015.
The settlement is believed to be the largest ever paid for such alleged violations of the state's Clean Waters Act.