Pro Farmer Crop Consultant Dr. Michael Cordonnier left his corn yield estimate unchanged at 167 bu. per acre and his soybean yield peg at 48.0 bu. per acre, but has a neutral to lower bias toward both crops. He notes the conditon of the crops improved slightly last week due to much-needed rain, but rains did not solve all the dry concerns.
Cordonnier says one of the concerns during pollination season this year will be the corn in the eastern Corn Belt that had to be replanted due to spotty stands or being drown out. He says this portion of the crop probably won't pollinate until August when there is a greater possibility of hot and dry conditions. "The nation's corn crop this year might reach 50% pollination on July 15 through 17," he notes. "Pollination is the most important time for corn of course, but several weeks prior to pollination and several weeks after pollination are also very important. The size of the ear is determined prior to pollination and the number of kernels filled successfully is determined after pollination."



Cordonnier 2017 Production

Harvested
			acreage 


Yield


Production




million acres


bu. per acre


billion bu.



Corn

81.6


167.0


13.63



Soybeans

89.1


48.0


4.27




Meanwhile, Cordonnier says recent rains were beneficial for newly planted soybeans. He left his crop peg unchanged, noting, "It's a little too early to be really concerned about the soybean crop." He adds, "Soybeans can experience adverse conditions during the month of June, but still do OK if the weather during July and August is beneficial."
Specifically, Cordonnier outlines the following areas of concern for the U.S. crops:

Pockets of relatively light rainfall over the past 30 days (less than 2 inches) in an area starting in southeastern South Dakota and stretching from northwestern Iowa to southeastern Iowa and extending into west-central Illinois and central Illinois. Approximately from Sioux Falls, South Dakota, to Des Moines, Iowa, to Springfield, Illinois.
Even though eastern North Dakota and northeastern South Dakota did receive rain over the past week, it has not been enough to fully recharge the soil moisture. Therefore, they will continue to need additional rainfall in the weeks ahead.
Spotty stands and less than desirable plant populations for the corn especially in the eastern Corn Belt.
Late-planted corn in the eastern Corn Belt that will probably pollinate sometime in August.
Delayed emergence of some of the late-planted soybeans in areas that had been dry or where there is a crusting problem.