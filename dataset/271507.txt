We’ve learned a lot about animal welfare over the past 20 years, but opportunities for continued improvement remain, according to a new report from the Council for Agricultural Science and Technology (CAST). The report, titled “Scientific, Ethical, and Economic Aspects of Farm Animal Welfare,” outlines research and advancements in understanding of animal welfare since CAST’s 1997 land­mark report on the well-being of agricultural animals.
The 1997 report listed six priority areas for animal-welfare research:

Bioethics and conflict resolution.
Responses of individual animals to the production environment.
Stress
Social behavior and space requirements.
Cognition.
Alternative pro­duction practices and systems.

Since then, scientists have developed better ways to define, evaluate and quantify different aspects of animal welfare
But as with many scientific enquiries, more answers tend to lead to more questions, and researchers increasingly realize the need for a multidiscipline approach toward understanding both the science and the ethics of animal welfare.
The authors of the CAST report list several new or emerging priorities for agricul­tural animal welfare, including:  

Engineering of animal housing and production practices that better accommodate species-typical behaviors.
Improved understanding of the implications of globalization.
Antimicrobial resistance.
Urban farming.
Assessing animals’ mental states, both positive and negative.

The report’s authors note that addressing the challenges posed by increasing public concern about animal welfare requires new knowledge, new approaches, greater inclusiveness and improved communication between scientists, policymakers and the public. Future needs also include:

Increase capacity for scientific research on animal welfare in the United States.
Increase focus on transdisciplinary aspects of animal welfare research.
Develop coordinated mechanisms for policy setting.
Effective communication about animal welfare is necessary to advance public understanding and improve application of the related science.