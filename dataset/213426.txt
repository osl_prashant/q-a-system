After the operation expanded in 2012, the Schillings were able to increase efficiencies, become more competitive and attract better customers and suppliers. It also forced Clare to delegate some responsibilities.
“When we were at 2,500 sows and going to 5,000, my vet, Aaron Lower, said I would have a really hard time with the transition because you become a lot more hands-off and more employee-based,” she says. “He was right.”
By personality, Clare admits she’s a doer: “I like to do things on my own and be part of the process, but with 5,000 sows, it was impossible to be involved in the same way.”
She adapted her style and gave more responsibility to key managers.
“You have to trust people, put faith in them, and acknowledge they’re there for the right reasons,” she says. “If you believe and trust in them, they reciprocate that trust and will meet your expectations.”
Clare admits it’s a work in progress. She has learned how important it is to recognize differences in culture, education and background—all of which go well beyond the language barrier.
“I’ve been immersed with these guys for so long I’m learning to communicate more effectively and it’s gotten us to where we are now,” she says.
 
Added benefits
Clare and Drew provide housing for employees when possible, and this has been an attractive benefit, leading to increased retention rates.
“At the beginning we had much higher turnover,” Clare says. “We found that employees want to live close to where they work, and they want a ‘landlord’ they can trust.”
About 80% of their employees are in living accommodations provided by the Schillings. All are within 15 minutes of where they work.
“We have people on site at each farm, and I feel that’s important,” Clare says.

Read more: 

Little Things Count in Hog Production 
CD Bell Farm Focuses on Trust and Delegation
The Choice to Come Home to Farm
From One Wedding Present to 8,000 Sows