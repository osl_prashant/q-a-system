Traders, preparing for Thursday's USDA crop reports, expect the agency to continue with its trend last month of increasing the size of the corn crop and lowering the size of the soybean crop. This also aligns with "better-than-expected" yield reports for corn.




 


Yield -- in bu. per acre


Production -- in billion bu.






Commodity


Avg.


Range


USDA Oct.


Avg.


Range


USDA Oct.




 




Corn


172.4


171.7-174.0


171.8


14.333


14.250-14.459


14.280




Soybeans


49.3


48.9-49.9


49.5


4.408


4.375-4.467


4.431




Meanwhile, only "fine-tuning" is expected to USDA's 2017-18 corn, soybean and wheat carryover projections from last month. Traders expect USDA to trim cotton carryover by 200,000-bales.




 


2017-18


2016-17






Commodity


Avg.


Range


USDA Nov.


USDA




in billion bushels
 




Corn


2.366


2.286-2.438


2.340


2.295




Soybeans


0.420


0.377-0.461


0.430


0.301




Wheat


0.957


0.940-0.980


0.960


1.181




Cotton (mil. bales)


5.60


5.30-6.10


5.80


2.75




USDA will release its reports at 11:00 a.m. CT on Thursday, Nov. 9.