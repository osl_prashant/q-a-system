92-year-old man rescued after tumbling into a well
92-year-old man rescued after tumbling into a well

The Associated Press

FREMONT, Calif.




FREMONT, Calif. (AP) — A 92-year-old man in Northern California tumbled 26 feet into an old well on his property and was trapped in cold water over an hour before being rescued by firefighters.
Fremont Fire Battalion Chief Gary Ashley says it was his first "bona-fide well rescue" in 24 years as a firefighter.
Spry at 92, Ernest Silva told KGO-TV that the floor of the well's pump house gave way as he went to get water for his vegetable garden on Sunday morning.
Silva fell into the well's depths and tread water before propping himself up using his feet and back.
Silva's daughter called 911 after discovering him.
Firefighters lowered a rope from a ladder truck to pull him out.
Silva was treated for a cut to his head and hypothermia.
___
Information from: KGO-TV.