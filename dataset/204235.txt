Choosing seed hybrids and varieties represents one of the most expensive and important decisions a farm operator makes each year. ASFMRA asked four seed experts to share seed selection advice."We can't predict the environmental conditions during a growing season, so review research trial results, and consult with local seed professionals to determine the best seed options and placement on your farm," says Bill Backhaus, CCA, region 60 manager for Wyffels Hybrids Inc., Griswold, Iowa.
"Seed prices continue to greatly vary, but a farm manager's decision for selecting seed should be based upon proven results over multiple sites, hybrids and varieties tested in multiple environments and years, genetic diversity and local service after sale."
B.J. Schaben, Iowa district sales manager, Pfister Seeds, recommends managers revisit past seasons.
"Take a look at historical diseases and insects that affect your fields," Schaben says. "Ask yourself, 'What were the best hybrids/varieties placed in specific fields and their strengths and weaknesses.' If you want to consider how to lower your seed price, consider finding a flex hybrid that will 'flex' out at lower populations. For example, reducing 4,000 plants per acre could cut six units of seed corn off your bill, and in most cases, using flex hybrids as part of your mix won't sacrifice yield and can improve standability."
Selection criteria varies by geography, says Chad Kalaher, CCA, FMA and field agronomist for Beck's Hybrids in eastern Illinois and western Indiana.
"For example, soybeans with an excellent score for iron deficiency chlorosis are critical in parts of Iowa," says Kalaher. "Also, fields with a history of soybean cyst nematode challenges may be a key factor for seed and seed treatment selection. For corn, good ratings for green snap, root strength and stalk strength are especially significant for hybrids on dark, highly productive prairie soils. On-farm drying and storage capacity may also be part of selecting the right hybrid based upon maturity."

Mitigate risk and evaluate the bottom line
For corn and soybeans, Kalaher adds that it's a good idea to spread risk across genetics and maturity groups. Planting no more than 20% to 25% of a crop's acres to a single product helps to manage risk.
"This will typically mean at least four to five corn hybrids and four to five soybean varieties across the farm," Kahaler says. "With a transition rate of new soybean varieties to corn hybrids at about 2:1, many farmers will plant a corn hybrid about twice as many years as they would a soybean variety. Most should plan to plant at least two new soybean varieties and one new corn hybrid each growing season."
Changing dynamics have led to LG Seeds adjusting its perspective shared on input costs.
"Years ago, we based decisions on seed costs for the whole farm, and then, it changed to looking at costs per acre," says Paul Handsaker, LG Seeds account manager. "Today, we need to look at cost per bushel for inputs. Making your seed purchase should not be ruled only by price. Value and consistency for your farm should offer more comfort and, ultimately, the best bottom line when choosing seed."

TOP TIPS FOR SEED PURCHASES
Paul Handsaker, LG Seeds account manager, advises farm managers to think about seed selection the way they do about
other purchase decisions. He cites these rules of thumb:
‚Ä¢ Develop good relationships with your seed company, and make sure it supports and services your purchases.
‚Ä¢ Make sure that you receive the needed traits and products that you order.
‚Ä¢ Ensure correct product placement for clients.
‚Ä¢ Look for consistency in the seed company's products and services.
‚Ä¢ Purchase the value of the company, incentives and the seed for the best return per bushel-not the lowest price.
‚Ä¢ Choose a seed company that researches and grows proprietary genetics for best seed selection and farm success.