BC-Cotton
BC-Cotton

The Associated Press

NEW YORK




NEW YORK (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Tuesday:
OpenHighLowSettleChg.COTTON 250,000 lbs.; cents per lb.Mar81.0081.7681.0081.76+.06May82.1582.4481.1582.25+.10Jul82.7782.9581.8282.79+.04Sep77.05+.18Oct78.47+.19Nov77.05+.18Dec76.8277.0576.6877.05+.18Jan77.23+.18Mar77.0677.2376.8677.23+.18May76.7577.0176.6577.01+.28Jul76.4576.7976.4576.79+.36Sep71.95+.50Oct74.62+.52Nov71.95+.50Dec71.5071.9971.5071.95+.50Jan72.33+.43Mar72.33+.43May73.14+.39Jul73.37+.29Sep73.03+.02Oct73.19+.15Nov73.03+.02Dec73.03+.02Est. sales 24,356.  Mon.'s sales 31,521Mon.'s open int 259,340,  up 931