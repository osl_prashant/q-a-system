Perdue to visit China | CBO update | Lumber dispute | NAFTA hearings | WOTUS rule | Brazil and beef | EU on trade  






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






— USDA Secretary Sonny Perdue to be in China June 30-July 1. The trip will be to mark the first shipment of American beef to the Asian market in 13 years, ending a ban. Perdue plans to cut Nebraska prime rib with U.S. Ambassador Terry Branstad and urge Chinese officials to buy more American farm products. Perdue will meet with Han Changfu, the minister of agriculture, and Wang Yang, the Chinese vice premier.
— New CBO budget baseline coming this week... The Congressional Budget Office (CBO) on Thursday is scheduled to release updated 10-year government spending projections, including the cost of farm programs and food stamps (SNAP). The updated estimates will play a role in the debate and program costs of a new farm bill.
— U.S. to impose more tariffs on Canadian lumber imports. The U.S. will impose further punitive tariffs on imports of softwood lumber from Canada, escalating a longstanding trade dispute that’s already led to higher timber prices. However, officials said they hope to negotiate a settlement before the full impact of the penalties is felt. Preliminary antidumping duties of as much as 7.7% will be levied on Canadian producers, the U.S. Department of Commerce said. The move follows the government’s decision in April to slap countervailing tariffs of up to 24.1% on shipments from Canadian companies. Canada is the world’s largest softwood lumber exporter and the U.S. is its biggest market. Lumber futures in Chicago have jumped this year amid concerns that the trade battle will disrupt supplies. Canada has denied the U.S.’s allegations, and Prime Minister Justin Trudeau at one point said he would carefully consider retaliatory measures against the U.S. The U.S. last year imported more than $5 billion worth of softwood lumber from Canada.
— NAFTA 2.0 hearings today. Today begins three days of hearings to garner comments on the Trump administration’s plan to renegotiate the North American Free Trade Agreement (NAFTA). More than 130 witnesses are slated, including all the major farm and commodity groups. After U.S. Trade Representative Robert Lighthizer’s mid-May notification to Congress that the pact would be renegotiated, his agency got more than 12,000 public comments on the matter, crashing the server and forcing more days of hearing comments. 
— Another day, another expectation of a WOTUS rule announcement. Reports have again surfaced that EPA will announce initial details of a new Waters of the U.S. (WOTUS) rule. It previously announced it will follow a two-step process to review and eventually revise the rule. EPA and the Corps will eventually propose a new definition that would replace the WOTUS rule, but it must go through notice-and-comment procedures. EPA and the Corps have been reaching out to stakeholders to discuss what a revised rule might look like. In mid-April, EPA met with state and local officials in Washington, D.C. to outline plans for replacing the rule. 
— Brazilian specialists to confer with USDA officials in early July. A team of specialists from Brazil's Agriculture Ministry will be meeting the first week of July with their U.S. counterparts to discuss ways to resume beef exports to the U.S. Agriculture Minister Blairro Maggi may accompany the team if he can schedule a meeting with USDA Secretary Sonny Perdue during the same period, Eumar Novacki, the ministry's executive secretary said. Perdue isn't scheduled to come to Brazil until September, Novacki said. The U.S. suspended all imports of fresh beef from Brazil because of “recurring concerns about the safety of the products” and said the country won't resume imports until Brazil's Ministry of Agriculture “takes corrective action which the USDA finds satisfactory.” The suspension was triggered by abscesses found by U.S. inspectors. The abscesses were a reaction to front-end-region vaccinations against foot-and-mouth disease. The Agriculture Ministry opened a quality-control investigation that could last up to 60 days to determine if the bovine reaction to the vaccine was caused by how the vaccine was made, by specific components in it, or by some other factor, Novacki said. Meanwhile, USDA’s Foreign Agricultural Service will lead a five-day trade mission to Brazil in September. 
— EU threatens retaliation if trade partners shut markets. The European Union is warning the U.S., China and other trade partners that it will retaliate in kind if European exporters and investors are blocked in their partners’ countries. EU officials stressed “reciprocity” as a new guiding light for trade and investments. European Trade Commissioner Cecilia Malmstroem repeated her June 20 threat that the EU would retaliate if the U.S. decides to block imports of European steel for national security reasons, using Section 232.


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.