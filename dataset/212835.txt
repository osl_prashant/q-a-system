Whole Foods Markets named Rainier Fruit Co., Selah, Wash., as a Supplier of the Year Award recipient.Rainier’s partnership with the retailer “continues to expand and evolve” with investments in new growing techniques and packing facilities, according to a news release.
Other produce companies in the annual awards program are:
Wholesum Harvest, for Excellence in Standards;
Driscoll’s, for Outstanding Quality Assurance; and
The Oppenheimer Group, for Distinguished Service.
“Our Supplier Award winners set the gold standard for the industry, and we’re proud to celebrate their achievements,” Don Clark, Whole Foods Market’s global vice president of procurement for non-perishables, said in the release