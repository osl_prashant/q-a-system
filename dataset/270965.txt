The U.S. Department of Agriculture has deregulated the ranger russet and atlantic varieties of the second generation genetically modified Innate potato developed by J.R. Simplot. 
They join the Innate second generation russet burbank potato, deregulated last year, according to a news release.
 
The potatoes were modified for late blight resistance — the second generation trait approved — along with the first generation benefits of low acrylamide potential (reducing the presence of a possible carcinogen in fried potatoes), reduced black spot bruising, and lowered reducing sugars, according to the USDA.
 
“The introduction of late blight resistance in Innate varieties is a game changer, one that has the potential to dramatically reduce the environmental impact of potato growing by reducing pesticide use,” Neil Gudmestad, professor and Endowed Chair of Potato Pathology at North Dakota State University, said in a news release.
 
Simplot said in the release that the company is anticipating the completion of the EPA registration and FDA consultation before the additional varieties of second generation of Innate potatoes are introduced into the marketplace.
 
According to the release, the second generation Innate potatoes were modified by adding only genes from wild and cultivated potatoes.
 
Innate second generation potatoes will reduce waste associated with bruising, blight and storage losses by reducing waste at multiple stages of the value chain, including in-field, during storage, processing, and in foodservice, according to the release.
 
The company said research suggests that these traits will “translate to less land, water and pesticide applications” to produce a crop.
 
Researchers consulted by Simplot, according to the release, said late blight protection trait can result in a 50% reduction in fungicide applications annually to control late blight disease.
 
In addition, the release said lower asparagine in the second generation Innate varieties means that accumulation levels of acrylamide can be reduced by up to 90% or more when these potatoes are cooked at high temperatures.