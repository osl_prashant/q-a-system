Asian stocks rise after Wall Street gains for 2nd day
Asian stocks rise after Wall Street gains for 2nd day

By JOE McDONALDAP Business Writer
The Associated Press

BEIJING




BEIJING (AP) — Asian stock markets followed Wall Street higher Wednesday after Beijing added to a swelling trade dispute with Washington by hiking tariffs on U.S. sorghum.
KEEPING SCORE: The Shanghai Composite Index gained 0.2 percent to 3,073.41 while Tokyo's Nikkei 225 gained 1.5 percent to 22,173.80. Hong Kong's Hang Seng rose 0.7 percent to 30,274.26 and Sydney's S&P-ASX 200 added 0.3 percent to 5,861.10. Seoul's Kospi climbed 0.9 percent to 2,480.08 and benchmarks in Taiwan, New Zealand and Southeast Asia also advanced.
WALL STREET: Technology and consumer-services companies, retailers and health care stocks contributed to a broad rally. Strong company earnings and outlooks, as well as some encouraging economic data, helped put investors in a buying mood. The Standard & Poor's Index rose 1.1 percent to 2,706.39. The Dow Jones industrial average gained 0.9 percent to 24,786.63, nudging the blue chip average into positive territory for the year. The Nasdaq composite climbed 1.7 percent to 7,281.10.
TRADE TENSIONS: China imposed preliminary tariffs of 178.6 percent on U.S. sorghum in an anti-dumping investigation. The United States told the World Trade Organization it has agreed to discuss with China the Trump administration's tariff increases on steel and other Chinese goods. President Donald Trump has threatened to raise tariffs on up to $150 billion of Chinese goods due to disputes over technology policy, market access and Beijing's trade surplus with the United States. China responded with its own list of U.S. goods for retaliation. The dispute has fueled fears it might dent global economic growth if other governments respond by raising their own import barriers.
ANALYST'S TAKE: "Shares rose despite another ratcheting up of trade barriers, this time from China," said Michael McCarthy of CMC Markets in a report. "The measured response of U.S. trading partners has soothed market worries."
CHINA AUTOS: Beijing announced it would allow full foreign ownership in its auto industry in five years, ending restrictions that irked Washington and other governments. The Cabinet's planning agency said it would start by allowing full foreign ownership of producers of electric cars, with similar action later on commercial and traditional passenger vehicles. China is the world's biggest auto market but until now global automakers have been required to work through partnerships with state-owned partners, an arrangement that forces them to share technology with potential competitors.
U.S. ECONOMY: The International Monetary Fund upgraded its economic outlook for the United States in 2018, forecasting that the U.S. economy will grow 2.9 percent this year, up from the 2.7 percent it had forecast in January and from the 2.3 percent growth the economy achieved last year. And the Federal Reserve said that U.S. factory output rose slightly last month.
CHINESE ECONOMY: The world's second-largest economy grew by a stronger-than-expected 6.8 percent over a year earlier in the quarter ending in March. That was in line with the previous quarter, but activity weakened toward the end of the period. Forecasters expect growth to slow this year as Beijing tightens controls on bank lending and a real estate sales boom to curb rising deb.
JAPAN TRADE: Export growth in March edged up to 2.1 percent over a year earlier from the previous month's 1.8 percent. Imports contracted 0.6 percent from a year earlier, led by a 17 percent plunge in shipments from China. The country's global trade surplus was $7.4 billion.
ENERGY: Benchmark U.S. crude added 51 cents to $67.03 per barrel in electronic trading on the New York Mercantile Exchange. The contract gained 30 cents on Tuesday to settle at $66.52 per barrel. Brent crude, used to price international oils, rose 51 cents to $72.09 per barrel in London. It advanced 16 cents the previous session to close at $71.58 per barrel.
CURRENCIES: The dollar gained to 107.38 yen from Tuesday's 107.00 yen. The euro slipped to $1.2370 from $1.2373.