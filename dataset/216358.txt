Global dairy prices sank to a 15-month low at an auction on Wednesday, which could hit see farmers take a hit to their incomes in the new year.
The GDT Price Index dipped 3.9 percent, with an average selling price of $2,969 per tonne, in the final auction of the year, held in the early hours of the morning.
Prices had risen in the second quarter on strong global demand and as production eased, but have since begun to drop as global supply has picked up.
The index rose 0.4 pct at the previous sale, but had fallen in five of the last six auctions, according to GDT Events.
"(The) dairy auction disappointed as a synchronized upswing in global milk supply pressures all product prices lower," said Con Williams, rural economist at ANZ Bank.
"All up, the result will raise doubts over Fonterra's latest forecasts of NZ$6.40."
Dairy giant Fonterra announced this month it was cutting its forecast payout to New Zealand farmers for the 2018 season from NZ$6.75 per kilogram of milk solids, citing volatility in global dairy markets.
Of particular surprise in the latest auction was the 2.5 percent drop in prices for whole milk powder, New Zealand's largest goods export. Futures markets had forecast a rise of as much as 5 percent, according to analysts.
The auction results can affect the New Zealand dollar as the dairy sector generates more than 7 percent of the nation's gross domestic product.
The currency was trading down 0.5 percent at $0.6982 in the wake of the auction.
A total of 29,592 tonnes was sold at the latest auction, an increase of 0.3 percent from the previous, the auction platform said.
GDT Events is owned by Fonterra, but operates independently from the dairy giant.
U.S.-listed CRA International Inc is the trading manager for the Global Dairy Trade auction.
The auctions are held twice a month, with the next scheduled for January 2.