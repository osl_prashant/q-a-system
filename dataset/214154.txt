Farmers National Company is reporting the land market is experiencing a seasonal increase in the number of properties for sale.
“Auctions and private treaty listings normally pick up during November and early December as harvest winds down,” says Randy Dickhut, AFM, Senior Vice President - Real Estate Operations at Farmers National Company. “These are sellers who want to get a sale closed before year end or want to get their farm sold during the time frame when farmers are looking for additional land to farm for next year.”
As we enter the last two months of the year, Farmers National Company and its agents report a good uptick in sale activity.
When describing the buyer interest in land, Dickhut uses the phrase, “cautious, but adequate in most areas.” And transactions are being bolstered by tax incentives.
“Part of the buying interest is being supported by the 1031 tax deferred exchange buyer who sold other rental property and is looking for a replacement property. On average right now, about 10% of the buyers of Farmers National Company listings are in a 1031 transaction,” he says.

One wild card that may bring some additional land onto the market would be from lender encouraged sales.
“In some regions, there could be additional farms for sale as the farmer or landowner works to improve their cash flow or balance sheet. Time will tell whether we will see some additional farms sell as farmers and lenders tally final crop yields after harvest,” Dickhut says.

Here's a link to the auction calendar assembled by Farmers National.