Perdue to food box critics: Give the idea a chance
Perdue to food box critics: Give the idea a chance

By JULIET LINDERMANAssociated Press
The Associated Press

WASHINGTON




WASHINGTON (AP) — Agriculture Secretary Sonny Perdue on Thursday defended a proposal that would replace a portion of food stamp benefits with pre-assembled boxes of shelf-stable goods delivered to recipients' doorsteps — an idea one lawmaker called "a cruel joke."
The idea was first floated last week in the Trump administration's 2019 budget proposal, tucked inside a larger plan to slash the Supplemental Nutrition Assistance Program, or SNAP, by roughly $213 billion — or 30 percent— over the next 10 years.
The backlash was fierce and immediate, from nutrition experts, advocacy organizations and lawmakers alike. Rep. Jim McGovern, D-Mass., top Democrat on the House Agriculture Nutrition Subcommittee, said the proposal is "cruel and demeaning and an awful idea" that would strip families of the ability to choose which groceries they buy. Rep. Barbara Lee, D-Calif., wrote on Twitter that as a single mother who once relied on food stamps: "I can't overstate how offensive this proposal is. Low-income families need more access to fresh produce & healthy foods, not less."
Rachel Millard, a spokeswoman for House Agriculture Committee Chair Mike Conaway, R-Texas, said last week that the committee heard from more than 80 experts during 21 hearings on SNAP, and none mentioned the idea of a food box.
Speaking to reporters Thursday after delivering the keynote speech at USDA's largest annual meeting, Perdue recognized that the idea took members of Congress by surprise, but said his staff consulted with a variety of experts while developing the plan.
"Our food nutrition group, I challenged them to come up with new ideas about how we could do more. How can we get a healthy, nutritious staple diet to people who need that? They were creative in their approach," Perdue said, adding that his staff "consulted with people in the delivery business, people in the food bank business who do this on a regular basis. And there's a lot more consulting to do."
Perdue said the plan is a work in progress, but it is fundamentally rooted in the idea that new technology can create opportunities.
"Think about the box of staples being delivered to the home," he said. "There are a lot of advantages here and I believe if people will listen to us I can talk about those advantages, and give them an opportunity to find holes in the program. But this is a new idea, it's innovative and we have to determine to work out the details."
Michigan Sen. Debbie Stabenow, top Democrat on the Senate agriculture committee, said the plan, dubbed America's Harvest Box, "isn't a serious proposal."
But Perdue said that although it would likely be "very difficult to roll out" nationwide, he is committed to continuing to talk about the idea with Democratic and Republican members of Congress. He added that he sees a potential partnership with the U.S. Postal Service, and a possible scenario in which families have a chance to choose which foods go into their box each month.
He said he hopes Congress will allow the department to develop a pilot program.
"There are a lot of things to talk about, and rather than just dismissing it offhand let's decide how we can improve," Perdue said. "And if Congress would allow us to have a pilot, we'd welcome that and demonstrate what we can do."
Millard said the House committee will "do due diligence" to consider all proposals related to SNAP.
"We'd have to see if we can pull something like that off, and if we could, does it make the most sense?" she said.
The administration's budget proposed massive cuts to USDA. It included a plan to tighten work restrictions for SNAP recipients by scaling back states' ability to grant waivers and raising the age limit for recipients exempt from the work requirement.
USDA said it will begin soliciting public comments Friday regarding potential changes to the department's rules for able-bodied SNAP recipients without children.