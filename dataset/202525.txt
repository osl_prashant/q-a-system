In any issue involving crossing the U.S.-Mexico border, Texas onion growers in the Rio Grande Valley know they are on the front lines, so they have much on their minds as the U.S. re-evaluates policies with Mexico, said Bret Erickson, president of the Mission-based Texas International Produce Association.
Labor ranks atop the list of worries, Erickson said.
"Labor is always a concern," he said. "As every year has gone by, it seems the labor supply gets tighter and tighter, not just in Texas and south Texas. The produce industry in general continues to see a real tightening of the labor market and available labor supply."
U.S.-Mexico relations have figured prominently in the early weeks of the Trump administration, with the new president announcing plans to move forward with a border wall and renegotiate the North American Free Trade Agreement, as well as talk of tariffs on Mexican goods bound for the U.S.
That creates an atmosphere of uncertainty for Texas onion grower-shippers, particularly those along the Rio Grande, Erickson said.
"Obviously, most of the focus is on border security and keeping people out and getting people out that shouldn't be here," he said.
There hasn't been much progress on other issues - labor-related matters top the list - that directly affect Texas growers, Erickson said.
"I'm hearing more talk about keeping the bad guys out and/or removing the bad guys and folks that shouldn't be here," he said.
Grower-shippers in south Texas are looking for alternatives outside the H-2A guest-worker agricultural program, Erickson said.

"I'd certainly like to hear more emphasis on how we're going to develop a better guest-worker program than the H-2A, which folks are forced to use," he said. "It's full of red tape and a lot of layers, and getting the people you need at the time you need is very challenging."
Changes in trade policy with Mexico likely would complicate matters for Texas' onion growers, some of whom also participate in Mexico's onion deal, Erickson said.
"Mexico onions have a big influence on the Texas market and a lot of Texas guys are growing onions in Mexico," he said.
For years, Texas' onion acreage has been shrinking, but that process could reverse if Mexican products are slapped with a new tariff, Erickson said.
"Depending on how it does ultimately work out, if the Trump administration does ultimately work with Congress and starts to institute tariffs or border taxes or whatever terminology they want to apply, I think that we could see something of a bounce in onion acres in Texas going forward," he said.
Growers say they're waiting to see how the situation plays out, even for the 2017 onion deal.
"We'll let you know how things go when things get started," said Mike Davis, owner of Weslaco, Texas-based grower-shipper TexMex.
There is more "uncertainty" in south Texas, where labor and other issues regarding the U.S. and Mexico are concerned, said J Allen Carnes, president and owner of Uvalde, Texas-based Winter Garden Produce Inc.
"When you're talking about ramping up security-type measures and enforcement, it makes you nervous," he said.
South Texas grower-shippers are a bit more fortunate than colleagues in other regions of the U.S., Carnes said.
"There are people who have the legal means to cross the border on a daily or weekly basis," he said.
Carnes said NAFTA probably needs updating, anyway.
"When NAFTA was passed, some of the production capabilities and pipelines out of Mexico were somewhat limited. Now, they're able to do a lot more in greater windows and it shrunk some of the domestic windows, which is not really a good thing," he said.