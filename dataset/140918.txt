Crop calls
Corn: 2 to 3 cents lower
Soybeans: 3 to 6 cents lower
Winter wheat: 1 to 3 cents lower
Spring wheat: Mixed
Selling resumed in corn and soybean futures overnight following yesterday's bout of short-covering. Yesterday afternoon's progress data from USDA showed corn planting 17% complete and soybean planting 6% complete -- both more aggressive than traders expected. Many producers will be in the fields today ahead of a midweek system that's expected to bring heavy rains into the weekend. Meanwhile, winter wheat futures were pressured by slight improvement in the winter wheat crop. 
 
Livestock calls
Cattle: Mixed
Hogs: Steady to weaker
Cattle futures are expected to see a combination of followthrough selling and short-covering to result in a mixed start to the day. Cattle should find some support from the beef market, as Choice values rose $1.51 on 114 loads to start the week. Additionally, yesterday's Cold Storage Report showed beef stocks at the end of March below expectations. Meanwhile, pork stocks came in heavier-than-expected, which is expected to weigh on hog futures following yesterday's corrective gains. The cash hog market is called steady to weaker amid after pork cutout values slipped $1.04 yesterday on moderate movement of 298.99 loads.