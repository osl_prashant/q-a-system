A cold winter across the U.S. meant a slow start to this year’s crawfish season in Louisiana. 

Warmer weather has spurred growth and the number of crawfish being caught.

Craig Gautreaux from the Louisiana State University AgCenter has the story on AgDay above.