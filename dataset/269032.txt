Twenty-one wildfires are actively burning in Oklahoma on more than 200,000 acres. Oklahoma is at a major fire risk due to drought in the state, slow grass growth from cold weather and wind conditions.
The Oklahoma Department of Agriculture Food and Forestry (ODAFF) and Oklahoma Forestry Service (OFS) hosted a press conference this morning to outline the fire risk in the state.
In Woodward Co., the 34 Complex Fire has burned 115,000 acres after starting on April 12. The 34 Complex Fire is a 0% containment. Another fire in the county called the Roadside Fire has burned 3,500 acres and is at 0% containment.
The next county south is Dewey Co. where the Rhea Fire has burned 82,000 acres and is also at 0% containment. The Rhea Fire has moved north into the town of Vici and OFS officials say the fire has burned several structures.
A few turkey hunters are reported to have been trapped in the Rhea Fire. Two hunters were successfully rescued, while a third has been life-watched out after being missing for a day.
Areas in both Dewey and Woodward Co. have had evacuations due to the wildfires.
Other fires in the state include:

Shaw Fire in Roger Mills Co., burned 3,500 acres, 0% contained
Anderson Road in Logan Co., burned 60 acres, 50% contained
66 Fire in Logan Co., burned 150 acres, 50 % contained
Oklahoma Protection Area in 15 eastern Oklahoma counties, 15 fires burned an estimated total 750 acres

Incident management teams have been ordered for both the 34 Complex Fire and Rhea Fire, says Mark Goeller, fire management chief for OFS.
“Air tankers and air craft have been ordered to start working those fires as soon as possible this morning before the fire weather conditions preclude the use of aircraft,” Goeller says. He adds that when conditions are like they are currently with high winds and low humidity it is difficult for aerial assets to assist effectively.
The largest risk for more wildfires is in northwest Oklahoma where the two major wildfires are burning, but the fire risk is moving east through the state.
“Our state, local and municipal fire departments are on high alert and ready for today. There is the possibility to see an event somewhat similar to what we saw on April 9, 2009,” Goeller says. On April 9, 2009, wildfires burned 300,000 acres in one day in Oklahoma.
OFS is asking for a “no burn day” in Oklahoma. It is advised that people don’t weld or use charcoal grills. Officials with OFS advise those hauling a trailer to insure that no chains or wires are dragging on the road for fear of a spark igniting.
It is believed that several of the current wildfires were started by powerlines arcing during high winds, but is vital to reduce the risk of any fires caused by people.
“We are prepared for the worst and we are hoping for the best,” Goeller says.
A state of emergency is expected to be declared by Gov. Mary Fallin later in the day for 52 counties primarily west of Highway 75 where the highest fire risk is at.
The press conference from ODAFF and OFS can be watched below:


 
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


Historic Fire Danger in Oklahoma. Oklahoma Forestry Services and Oklahoma Emergency Management hosted a press conference on the outbreak of wildfires in Oklahoma.
Posted by Oklahoma Department of Agriculture Food and Forestry on Friday, April 13, 2018



 
Here is some other social media footage of the wildfires in Oklahoma:


 
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

Marty Close Call
CLOSE CALL: Marty Logan had an extremely close call while tracking some wild fires in Woodward Oklahoma earlier today. Marty was trying to rescue some cows from the fire, when the flames got dangerously large. Check out the video!
Posted by KWTV - NEWS 9 on Thursday, April 12, 2018





 
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

Posted by Scott Kline on Thursday, April 12, 2018