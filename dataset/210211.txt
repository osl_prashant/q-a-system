Apple breeder Bruce Barritt talks with “Washington Grown” host Kristi Gorenson about the Cosmic Crisp apple variety.
 
An episode of TV’s "Washington Grown" series featuring Washington apples is scheduled to air this weekend.
The Washington Apple Commission, with the help of production company North By Northwest, created segments for the show, according to a news release.
"Washington Grown" showcases agricultural products from the state and shows “behind the scenes” footage of growing and production, the release said, as well as visits to local restaurants. The show is in its fifth season.
The episode includes a visit to Legacy Orchard and an explanation of the pollination process, according to a news release.
It will also feature a talk with Cosmic Crisp apple variety breeder Bruce Barritt and a history lesson with a visit to the Wenatchee Valley Museum and Cultural Center.
The show ends with a visit to the Wenatchee Apple Blossom Festival, with an interview with the Apple Blossom queen.
The episode airs 5 p.m. Pacific Saturday on KIMA, 2:30 p.m. Sunday on KOMO4 and 7 p.m. Sunday streaming live from "Washington Grown’s" Facebook page.
On Oct. 23, the video will be available on YouTube.