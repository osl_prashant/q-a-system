Giving consumers ideas on more ways to use pineapple likely will move more fruit through the retail sector, marketers say.
Retailers can sell a lot of pineapples by pairing them with other items, said Alan Dolezal, vice president of sales for the tropical division of Coral Gables, Fla.-based Fyffes North America Inc.
"Cross-merchandising and cross-promotion with other tropical items, such as bananas, are a proven sales builder, as are creative display contests tied to specific events and/or featuring tropical themes," Dolezal said.
At point of purchase, demos remain a very effective vehicle in terms of generating both initial impulse sales and garnering repeat purchases, Dolezal said.
"Recipe and serving suggestions are always beneficial, and pineapple can be emphasized both as a stand-alone food or as an ingredient in a variety of healthy recipes," he said.
One of the toughest challenges in marketing pineapples at retail is educating consumers on how to select and enjoy the tropical fruit, said Bil Goldfield, spokesman with Westlake Village, Calif.-based Dole Food Co.
"It is a common misconception that pineapples will continue to ripen after harvest, but the truth is Dole pineapples are picked at their peak of ripeness," he said.
Dried-looking fruit or pulling leaves from the crown is more a sign of age than ripeness, Goldfield noted.
"Keeping the display stocked with fresh, ripe pineapples and knowledgeable produce staff at the point of purchase are the best way for retailers to drive pineapple sales," he said.
In-store cutting demos and materials can also educate consumers on the correct way to cut a pineapple and help dispel the myth that pineapples are hard to prepare and enjoy, Goldfield said.
When sampling events are combined with new recipe ideas, the result can be "an impressive increase" in pineapple sales, Goldfield said.
"Using social media to share these recipes, usage ideas and education is a great delivery vehicle, and one in which Dole puts considerable effort," he said.
Retailers can best position pineapples for optimal sales by incorporating "eye-catching" point-of-sale materials on shelves, cross-merchandising and improved labeling graphics and information," said Dennis Christou, vice president of marketing with Coral Gables, Fla.-based Del Monte Fresh Produce.
"Special packaging designed for convenience and displays close to the checkout and entrance of the store are other great tactics," he said.
Del Monte also encourages retailers to be mindful of seasonal demands, such as summer road trips, tailgating events and holiday-themed displays, Christou said.
"As always, enhanced display sizes will also increase sales of pineapples," he said.