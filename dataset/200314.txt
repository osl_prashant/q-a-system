Members of the Louisiana Forage and Grassland Council heard about the effects of recent floods on pastureland at their annual meeting on Dec. 2.Producers from across the state attended the annual meeting that featured presentations from U.S. Department of Agriculture and LSU AgCenter professionals, along with private producers who were affected by spring and summer flooding in their areas.
AgCenter forage specialistEd Twidwellsaid the overall message of the meeting was conservation, and the lineup of speakers covered a wide area of expertise on the topic.
"This organization is basically made up of three primary groups," he said. "The public entity consists of the AgCenter and the U.S. Department of Agriculture; agribusiness, which includes seed and chemical companies, and then we have producers."
A variety of speakers were on the program to discuss how the recent flooding has affected pastures and what can be done now to avoid some of the problems during future floods.
"We have speakers here today from Texas and Arkansas who are reporting on the conservation research they are conducting in their areas," Twidwell said.
Nathan Haile, with the USDA Natural Resource Conservation Service in Temple, Texas, discussed his long-term study that looks at different grazing schemes as they relate to conservation, Twidwell said.
"Dan Pote, with the USDA Agricultural Research Service in Booneville, Arkansas is doing a similar study, hoping to show how much conservation pays," Twidwell said. "Conservation has been around for many years, but there hasn't been a lot of research conducted to reinforce how much conservation efforts help."
AgCenter agentAndrew Granger, along with Hank Moss, a Vermilion Parish producer, discussed the aftermath of saltwater flooding on pastures after Hurricane Rita in 2005.
"We lost over 5,000 head of cattle following the storm surge," Granger said. "The flooding was devastating, but our producers are recovering."
AgCenter forage specialistWink Alisonsaid some of the worst flooding in the state this year was along the Red River in northwest Louisiana, an area that has flooded the past two years.
"What we're finding is that bermuda, bahaiagrass, wheat, oats and ryegrass didn't fare so well in the flooded conditions," Alison said. "However, there are different things that can make a difference."
Alison said one pasture of bermudagrass that was under water for six of the past 11 months seems to be coming back.
Herbaceous mimosa also seems to be surviving in areas that were inundated, he said.
Harrison John-Louis, a producer with cattle in Iberville and St. Martin parishes, said he didn't experience any flooding on his pastures, but rain was still his biggest problem this year.
"I fertilized, and the grass grew. But then the rains came, and I couldn't get in to cut," John-Louis said. "The quality of my hay this year is probably not what it should be because I cut a little premature."
John-Louis praised the work of the council, the AgCenter and NRCS for the help they provide to producers.
"I have to say the value of these three organizations to me is information, information and more information," he said. "AgCenter agents Stan Dutile in Lafayette and Andrew Granger in Abbeville are the best for us producers. They provide all the information we could ask for."
Louisiana State Conservationist Kevin Norton, with NRCS, gave an update on his agency's work.
"We're always accepting applications for our conservation programs," Norton said. "We're currently in the signup period for the Conservation Stewardship Program for 2017."
Norton reminded the producers the deadline to sign up for the program is Feb. 3.
"We have some new options for livestock and forage producers," he said. "This past year was a very successful year in NRCS, with over $100 million in conservation payments to producers."
These are cost-share programs, with producers investing more than $25 million of their own money, Norton said.