In the last issue, the Class III price for March and April closed at $15.81 and $15.22/cwt, respectively. For the months of May and June, the Class III price was projected to be stagnant at $15.20 and $15.30/cwt, respectively. The Class III component price for the month of May actually closed slightly higher at $15.57/cwt, and then increased about 90¢/cwt in June ($16.44/cwt). The price for July is expected to be similar to June at $16.58/cwt, followed by an over $1/cwt drop to $15.29/cwt for the month of August.The Class III prices for this summer have been quite optimistic, especially when we look back at the price of milk in June 2016 (Class III $13.22/cwt). The increased price is also in spite of total milk produced for the Federal Milk Marketing Order No. 33 (includes Ohio) being increased about 7% compared to one year ago. The higher amount of milk produced is probably sparked from both higher pounds of milk per cow and more cows being milked in Ohio. According to the USDA, the national production average is estimated to be 65 lb/day per cow with a total of 9.39 million milking cows compared to 62 lb/day per cow with a total of 9.33 million milking cows one year ago. The USDA projects cow numbers will continue to rise nationally up to 9.44 million, but the average cow production to decrease and slow overall growth in total milk production.
If the USDA is right and we extrapolate national trends to Ohio, one can expect the Class III price to stay around the $14.50 to 16.00/cwt range for the remainder of the year. However, why average milk production (lb/cow) would decrease below the previous year in Ohio is unknown. Rather, I would project production to continue to increase and surpass last year’s production. The resulting milk price would then be expected to be at the lower end of this predicted range. Realize predicting the price this far in advance is a long shot at best. Many economic (and political) changes will likely occur within the next 4 to 5 months and can affect the price.
Nutrient Prices
As in previous issues, these feed ingredients were appraised using the software program SESAME™ developed by Dr. St-Pierre at The Ohio State University to price the important nutrients in dairy rations, to estimate break-even prices of all commodities traded in Ohio, and to identify feedstuffs that currently are significantly underpriced as of July 25, 2017. Price estimates of net energy lactation (NEL, $/Mcal), metabolizable protein (MP, $/lb; MP is the sum of the digestible microbial protein and digestible rumen-undegradable protein of a feed), non-effective NDF (ne-NDF, $/lb), and effective NDF (e-NDF, $/lb) are reported in Table 1.
In light of my prediction for milk prices to stay stagnant or decrease, nutrient prices continue to remain relatively low as they have been for the past three years. For MP, its current price ($0.37/lb) has dropped slightly from May’s issue ($0.40/lb) and is about 23% lower than the 5 year average ($0.48/lb). The cost of NEL increased about 1¢/Mcal to 9¢/Mcal, but the NEL price is still lower than the 5-year average of 11¢/Mcal. The price of e-NDF and ne-NDF are nearly identical to last month at 5¢/lb and -7¢/lb (i.e., feeds with a significant content of non-effective NDF are priced at a discount), respectively.
To estimate the cost of production at these nutrient prices, I used the Cow-Jones Index with cows milking 70 lb/day or 85 lb/day at 3.7% fat and 3.1% protein. In the last issue, the average income over nutrient costs (IONC) was estimated at $9.23/cwt for a cow milking 70 lb/day and $9.60/cwt for a cow milking 85 lb/day. For July, the IONC for our 70 and 85 lb/day milk yield for cows will be about 12% higher than May at an estimated $10.40/cwt and $10.78/cwt. These IONC may be overestimated because they do not account for the cost of replacements or dry cows; however, they should be profitable when greater than about $9/cwt. Overall, farmers producing milk in Ohio should be making money.
Table 1. Prices of dairy nutrients for Ohio dairy farms, July 25, 2017.
Economic Value of Feeds
Results of the Sesame analysis for central Ohio on July 25, 2017 are presented in Table 2. Detailed results for all 26 feed commodities are reported. The lower and upper limits mark the 75% confidence range for the predicted (break-even) prices. Feeds in the “Appraisal Set” were those for which we didn’t have a price. One must remember that Sesame compares all commodities at one specific point in time. Thus, the results do not imply that the bargain feeds are cheap on a historical basis.
Table 2. Actual, breakeven (predicted) and 75% confidence limits of 26 feed commodities used on Ohio dairy farms, July 25, 2017.

For convenience, Table 3 summarizes the economic classification of feeds according to their outcome in the SESAME™ analysis. Feedstuffs that have gone up in price or in other words moved a column to the right since the last issue are red. Conversely, feedstuffs that have moved to the left (i.e., decreased in price) are green. These shifts (i.e., feeds moving columns to the left or right) in price are only temporary changes relative to other feedstuffs within the last two months and do not reflect historical prices.
Table 3. Partitioning of feedstuffs, Ohio, July 25, 2017.

Bargains


At Breakeven


Overpriced


Corn, ground, dry


Alfalfa hay – 40% NDF


Blood meal


Corn silage


Bakery byproducts


Canola meal


Distillers dried grains


Beet pulp


Citrus pulp


Feather meal


Gluten meal


41% Cottonseed meal


Gluten feed


Soybean meal - expeller


Fish meal


Hominy


48% Soybean meal


Molasses


Meat meal


Soybean hulls


Tallow


Wheat middlings


Whole cottonseed


44% Soybean meal


 


Wheat bran


Whole, roasted soybeans

As coined by Dr. St-Pierre, I must remind the readers that these results do not mean that you can formulate a balanced diet using only feeds in the “bargains” column. Feeds in the “bargains” column offer a savings opportunity, and their usage should be maximized within the limits of a properly balanced diet. In addition, prices within a commodity type can vary considerably because of quality differences as well as non-nutritional value added by some suppliers in the form of nutritional services, blending, terms of credit, etc. Also, there are reasons that a feed might be a very good fit in your feeding program while not appearing in the “bargains” column. For example, your nutritionist might be using some molasses in your rations for reasons other than its NEL and MP contents.
Appendix
For those of you who use the 5-nutrient group values (i.e., replace metabolizable protein by rumen degradable protein and digestible rumen undegradable protein), see Table 4.
Table 4. Prices of dairy nutrients using the 5-nutrient solution for Ohio dairy farms, July 25, 2017.