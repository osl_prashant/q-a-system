New York corn grower-shippers say weather issues along the East Coast could create some gap opportunities for them this season.“I think there’s going to be a lot of holes where people couldn’t plant when they wanted to, so I think there’s going to be a fair gap between the Southern deal and Northern deal,” said Tony Piedimonte, owner of Holley, N.Y.-based James J. Piedimonte & Sons Inc. and Anthony J. Piedimonte/Cabbco.
“Sometimes, that could be a problem where prices are so high, but hopefully, with some of the homegrown deals, it will keep the volume going,” he said.
New York suffered from drought conditions in 2016 and received only limited rain the previous year, but the rains returned this year, which could help corn yields, growers said.
“This year, starting in May, about a day a week is about all we get to plant. We go like crazy when we can,” said Jason Turek, partner in King Ferry, N.Y.-based Turek Farms.
Turek Farms, which has about 4,000 acres for all vegetables, planted only about two-thirds of its typical corn acreage this year, about 300-400 fewer acres than usual, but the final crop could come close to the norm, Turek said.
“It’s hard to say,” he said. “Once we get going, we’ll see what our yields are. It was so dry last year, yields were off.”
Gary Balone, manager of Eden, N.Y.-based Eden Valley Growers, said the corn was shaping up well.
“It looks good in the field, and we maybe will start July 10-12,” he said.
 
Green beans
The wet weather through the spring planting season likely will limit green beans, at least in some cases, some growers said.
“We’re taking a break on green beans here,” Turek said. “They do not like wet weather.”
Piedimonte said volume on green beans should be adequate, if not abundant.
“Because so many of the green beans end up in packages, we’ve got it shrunk down to where we don’t grow more than we can market. I just don’t think there’s an abundance that there used to be,” he said.
“If you don’t have a source who can move them in a bag, it’s kind of tough anymore.”
In other words, it’s prudent to grow to meet customers’ needs, he said.
“You can’t sell all this stuff anymore, so we look at our marketplace and grow pretty much to our marketplace and don’t gamble on extra product,” Piedimonte said.
As for prices, wirebound crates of yellow sweet corn from south Georgia were $8.95-10.95 June 29, according to the U.S. Department of Agriculture. A year earlier, the same product was $8.95-9.95.
New York’s harvested sweet corn acreage in 2016 totaled 26,600 acres, with 2.5 million cwt., the USDA reported. Corn for the fresh market made up $44.6 million of a total crop value of $53 million.
Bushel carton/crates of round green-type beans from South Georgia were priced at $28.35-30.85, as of June 29. A year earlier, they were $30.35-30.85.
Harvested green bean acreage across New York was 28,300 acres in 2016, for a total of nearly 2 million cwt. The value of the fresh market crop was $36.6 million, of a total of $59.3 million, according to the USDA.