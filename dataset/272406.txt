The Latest: Blast of cold air, snow hit France
The Latest: Blast of cold air, snow hit France

The Associated Press

BERLIN




BERLIN (AP) — The Latest on weather in Europe (all times local):
3:50 p.m.
Snow is falling on Normandy, as a blast of cold weather hit France and jolted it out of a surprise heat spell.
Shorts one week, snow boots the next — that's the kind of extreme weather France is having this spring. After an exceptionally frozen March, France had one of its hottest Aprils on record, according to national weather authorities.
That is, until Monday, when temperatures plunged toward freezing across parts of northern France. Snow hit low-lying areas around the Normandy city of Rouen, and floods and fierce winds threatened regions in the north and on the Atlantic Coast.
Temperatures are forecast to rise slightly for nationwide May Day marches Tuesday — though chances of rain loom over several areas.
___
12:15 p.m.
Hundreds of Haflinger horses are enjoying their first days out on the pasture in Germany after a public run marking the end of the winter season.
Staff at the Meura stud farm in central Germany drove the distinctive chestnut-colored breed from their stalls to their summer gazing grounds on Sunday.
The event, which has become something of a tradition, drew thousands of spectators to the rural area of Thuringia state amid glorious weather.
First bred in what is now northern Italy in the 19th century, Haflinger horses are a popular riding breed.
___
11:35 a.m.
Firefighters are pumping rainwater from basements and clearing flooded road tunnels after a storm swept through parts of western Germany overnight.
Germany's far-west Aachen region was the hardest hit, with rescue services receiving hundreds of calls. Aachen police say nobody was harmed in the storm.
France, Belgium and Luxembourg were also affected by heavy rain and hail — weather that is more often seen during the summer than the spring.
Recent days have seen unseasonably warm weather in parts of western Europe.
The storm was moving north Monday toward Denmark.