The Produce Marketing Association's Center for Growing Talent is offering a conference for mid-level managers in September.
 
Called the High Performance Management Conference, the event is slated for Sept. 19-22 in Chicago, according to a news release.
"Every industry executive wants what this experience delivers: It encourages our mid-level managers to expand their thinking, and teaches them to tackle business challenges from a more strategic point of view," the center's board chairman-elect Leonard Batti, president of Taylor Farms Florida, said in the release.
The conference features speakers including: 
Networking expert Michael Goldberg;
Emmy award-winning journalist Bob McNaney;
Corporate trainer and best-selling author Dr. Andy Neilliel; and 
Produce Marketing Association CEO Bryan Silbermann.
Event information and a link to a registration form is available online.