Giumarra has new port facility
Giumarra International Berry West Coast is working at full capacity from its location in the Port of Long Beach, Calif.
Tom Richardson, vice president of global development for the Giumarra Cos.’ division, said the facility will be the hub for all of the company’s West Coast and export operations.
 
Naturipe sees organic increase
Naturipe Farms LLC, Salinas, Calif., has produced record volumes on both conventional and organic strawberries for several weeks, and high volumes are expected to continue weekly into mid-fall, Vinnie Lopes, vice president of sales-west, said June 8.
“In past years there has often not been enough organic production to penetrate broader markets,” Lopes said.
“That is now a notion of the past.”
Lopes said the company has enough organic strawberry production for weekly promotional opportunities.
“We are now offering larger pack sizes than ever before on organic and shipping them fresh daily,” he said.
Naturipe produces California conventional and organic strawberries year round with peak supply from March through October, Lopes said.