The calendar says April, but Old Man Winter is still making his presence known nearly four weeks after the official start of spring.

Over the weekend, blizzard warnings stretched from southwestern Nebraska to Michigan, blanketing fields with inches of snow. Strong wind and heavy snow closed major highways in parts of South Dakota, Iowa and Minnesota. In Wisconsin, some dairies even had roofs on barns collapse.

Farmers and ranchers were caught in the midst of it, praying their livestock would make it through the harsh conditions.

April 15, 2018 Central Lake MI @joecharlevoix @NWSGaylord @americanherf @AgDayTV pic.twitter.com/FVNemK37CM
— Logan (@renglebrecht) April 15, 2018

And in the worst times, the best things happen, showing how tough cattle can be.

If there’s one thing that never ceases to amaze me, it’s the resiliency of cattle. In a matter of 5 hours she’s back to life. This scenario right here always reminds me that it’s not over until it’s over. #YouMightBeARancher pic.twitter.com/Xdi5helAB0
— stephanie stray cow (@surfingkelpie) April 14, 2018


Hard night for these cows #blizzard pic.twitter.com/27RmtRUhYu
— Henry (@HenryHjbeel) April 14, 2018

In some cases, farmers had to dig their cattle out of the barn. 



 
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


The incredibly strong winds that accompanied Blizzard Evelyn produced huge snow drifts in rather unusual places. A...
Posted by Kinnard Farms on Monday, April 16, 2018





 
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


Winter storm Evelyn packed a punch! Thankful that no one was hurt in the barn collapse. All animals were moved safely!...
Posted by Pagel's Ponderosa Dairy LLC on Monday, April 16, 2018





 
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


Day 2 of winter storm Evelyn!! Everyone stayed dry and warm, the girls were ready for milking, and our team was working hard to keep things flowing smoothly today! #WSEvelyn #grateful #farm365
Posted by Wagner Farms Inc. on Sunday, April 15, 2018



With these conditions, some are questioning what month it is.

Still battling frozen waters on this

16th day of apriluary pic.twitter.com/sT1BbxvFhc
— Mike Berdo (@BerdoMike) April 16, 2018


So when is this global warming thing supposed to kick in? #SpringBlizzard2018 pic.twitter.com/Gfx2M8ZvLV
— Les Anderson (@lesanderson10) April 15, 2018


Wait a minute. Was it #PurdueSpringFest ? or #PurdueWinterFest? ?? pic.twitter.com/4MEI62BIO2
— Purdue Agriculture (@PurdueAg) April 16, 2018

While snowy conditions aren’t ideal in April, it’s not the first time this has happened.

My place April 14th 2014. We been here before. #plant18 pic.twitter.com/hleYy2hTdW
— BJ Hayes (@brhayes21) April 16, 2018

Instead of complaining, some are working ground as the snow falls.

Doing something about the snow instead of complaining about it @PitstickFarms @kyleplattner @sf28430 @Cornfrmr @BetsyJibben @sellis1994 @MGigger @farmers_edge @FBNFarmers @GoddessofGrain @NDfarmer81 @CNHIndustrial @summersmfg @MGigger @MaxROIFarmer @TheChadColby pic.twitter.com/I1ZpJMq8lW
— Paul R. Anderson (@ndcorngrower) April 16, 2018

Hopefully this will be the last big snowfall of the year.

How the day started! 6 hrs later everything is plowed and Fed. pic.twitter.com/h3tX24cnVh
— T.J. Manning (@4manningranch) April 14, 2018