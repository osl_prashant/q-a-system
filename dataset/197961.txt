Microscopic organisms can make or break your crop
In a single teaspoon of soil there are 1 billion microbes. Even though you can’t see microbes, they play a key role in your crop’s success or failure. 

“It’s important to keep microbes happy—they can’t function alone and so much of what farmers do depends on them,” says Bob Perry, general manager, Perry Agricultural Laboratory, a soil testing lab in Bowling Green, Mo.

Your tillage practices, fertilizer applications, soil pH and compaction levels all affect soil productivity. Boost soil health—and yields—by keeping microbes happy.

Microbes play an essential role in nutrient uptake. “Soil organisms recycle nutrients, break down crop residue and help build the soil,” says Eileen Kladivko, Purdue University professor of agronomy. 

The type of residue plays a role in decomposition. “Coarse forms such as straw, seed coats, etc., are high in carbon and need to be balanced out with nitrates,” Perry says. Soybeans are a 30:1 carbon-to-nitrogen ratio while corn is 60:1 and wheat is 100:1.

To break down carbon-heavy residue, microbes need energy in the form of nitrates. When you apply nitrates, only 40% of applied nitrogen makes it directly to the plant. Most of the nitrogen is used by microbes, which later becomes beneficial for the crop.

Simple steps can ensure your soil is a healthy environment for crops and microbes. “When we talk about soil health a lot of it is understanding the soil is living, and microbes are like livestock that need food, shelter, water and air,” says Matt Stukenholtz, supervisor of chemistry at Midwest Laboratories. “These living organisms benefit me—what can I do to promote them?”

Have enough nitrogen to manage your residue carbon loads. Talk to your fertilizer dealer for nitrogen tips if you’re unsure of application rates. When figuring out nitrogen, phosphorus and potassium application rates, don’t forget about zinc, copper, sulfur and other micronutrients important to soil and microbial health.
Soil needs oxygen. This could come from light tillage, but if soil erosion is a risk, full tillage might not be the best idea. Improve drainage to avoid ponding, which depletes oxygen levels, and consider cover crops. “When we keep the soil covered it makes a better environment for microbes,” Kladivko says. “Living root systems are essential for soil biology.”
Run through the what ifs, Perry says. “If I use this chemical with this fertilizer, this will happen. Find out how it affects soil health,” he adds.

Good soil health can’t be attributed to a single influence—stay in tune with what your soil needs. “The danger in not improving soil heath is it either improves or degrades—it’s never stagnant,” Kladivko says. 

The Role of Organic Matter
Organic matter plays many different roles in soil—from increasing microbial activity to improving water-holding capacity. According to Rafiq Islam, Ohio State University soil scientist, soil organic matter plays a key role in soil health by:
1. Providing food and energy for soil microbes. Microbes are essential to break down carbon, which helps plants take up nutrients.
2. Adding nutrients to the soil to boost crops throughout the growing season as the organic matter breaks down.  
3. Regulating soil ecological functions, which helps improve cation exchange capacity and manage pH balance.
4. Improving soil’s moisture retention and structure. This will keep water in the soil during dry years and help with drainage in wet years.
Wondering what you can do to boost soil organic matter? Ohio State University has an online tool to calculate how much organic matter is in your fields.