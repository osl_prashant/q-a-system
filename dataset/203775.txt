A case can be made that the current sustainability movement took root with organics, produce marketers say.
"Organics producers were some of the earliest adopters of sustainable farming and continue to be recognized leaders, but mainstream producers are much more focused on sustainability, as well," said Bruce Turner, national marketing representative with Wenatchee, Wash.-based Oneonta Starr Ranch Growers.
Turner said his company has focused on sustainability of both conventional and organic production for many years.
"(Integrated pest management), water conservation, chemical use reduction, mulch-based nutrition and more are staple farming techniques used on all of our orchards," Turner said.


Less pesticide, fertilizer

Organics long has been a building block to sustainability at Westlake Village, Calif.-based Dole Food Co., spokesman Bil Goldfield said.
"The fact that Dole pioneered organic farming and has more than 20 years of organic farming experience makes organics a key part of our sustainability plan," he said.
Goldfield said staffers who had developed the Dole Latin America organic fruit farming program borrowed some of the most effective organic practices and applied them to the company's conventional production.
"This adoption of techniques and practices has allowed us to reduce traditional fruit farming inputs such as pesticides, fertilizers and water, and care better for the environment in our operations," he said.
Stemilt Growers LLC, a Wenatchee, Wash.-based fruit grower-shipper, has a similar philosophy, said Roger Pepperl, marketing director.
"They become a piece of sustainability - nothing more," he said. "Organics can sometimes teach you ways to reduce other non-organic chemicals or find softer ways of approaching a problem."
All products need to be sustainable, Pepperl said.
"However, there is a halo effect that organic puts on your sustainability program," he said.


Many facets

A comprehensive sustainability program is multifaceted, said Samantha Cabaluna, managing director of brand communications for San Juan Bautista, Calif.-based Earthbound Farm, which is owned by WhiteWave Foods Co., Denver.
"But organic farming can make a big contribution to those efforts because it keeps toxic and persistent chemicals out of the air, water and worker environment," she said.
Rachel Mehdi, organics category manager with Vancouver, British Columbia-based The Oppenheimer Group, said sustainability and organics are "inextricably" connected.
"Both focus on the same goals, including delivering a wholesome product while focusing on the longevity of the land and preservation of the soil," Mehdi said.
Organic practices naturally achieve sustainable results, she said.
Scott Mabs, CEO of Porterville, Calif.-based Homegrown Organic Farms, agreed.
"There are certain parts of organic that are inherent with sustainability - the soil, how we look at the ecosystem - so there's inherent aspects of sustainability that are associated with organics," Mabs said.


Land, community, employees

Organic certification doesn't touch on areas that a comprehensive sustainability program covers, including worker and community benefits and relations.
But, it can, he said.
"At Homegrown, we believe sustainability is how you care for the land, the community, the business and your employees," Mabs said. "If you don't care about it, then it's just, 'How do I make a dollar?'"


In the right direction

Organic production is not an end-goal of sustainability, but it can be important ingredient in a sustainable operation, said Tom Deardorff, president of Oxnard, Calif.-based Deardorff Family Farms.
"It's a step in the right direction, as far as we're concerned, and it's helped us achieve some goals around sustainability," he said.
It's one step among many choices, in a pursuit of sustainability, said Jennifer Shaw, head of North America sustainability for seed company Syngenta's AgriEdge Excelsior program in Raleigh, N.C.
"It's all about the science and determining which practices best drive the desired outcomes," she said.
At Watsonville, Calif.-based Lakeside Organic Gardens, organic production is an integral part of a comprehensive sustainability program, said Katie Bassman, marketing coordinator.
"It's building up the soil, the organic matter, building up the environment," she said.