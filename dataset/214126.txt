Cow depreciation is frequently the second or third largest expense to the cow-calf enterprise after feed. Depreciation is a non-cash expense that is often overlooked by cow-calf producers. 

Depreciation for a cow is calculated as the following:

Purchase Price or Replacement Cost – Salvage Value/Productive Years in the Herd

To demonstrate how significant this expense can be, examine an example of current bred replacement heifer prices against today's cull cow values.

Bred Two-Year-Old Heifer = $1800
Average Cull Cow Value = $800
Depreciation without death loss = $1000/head ($1800 - $800)

The average number of productive years for most cows in a herd is somewhere from 3-5 years assuming a 10 - 20% cowherd replacement rate. Using five years, depreciation is $200 per head per year. At four years it is $250 per head per year and at three years it is $333.33. If you add in death loss at 2% on an average cow herd value of $1300 then depreciation expense jumps to $226 per head for five years, $276 for four years and $359.33 for three years. Cow depreciation is a significant expense!

Aggressively identifying ways to reduce depreciation expense should be a goal for cow-calf producers. Depreciation can be reduced one of three ways.

1. Reduce replacement heifer development costs or the purchase price for bred heifers or cows.
2. Increase the salvage value of cows that are leaving the herd.
3. Increase the number of years a cow is productive in the herd.
Purchase Price or Replacement Cost

Cow-calf producers purchasing bred females need to evaluate the cost of those females against expected productivity and revenue that will be generated from them. When most cow-calf producers think of buying bred replacements, they probably are thinking of purchasing bred heifers. However, it may be that purchasing a different age group of cows would be more profitable and provide greater management flexibility.

Cow-calf producers who raise and develop their own replacement heifers should enterprise replacement heifers separately from the cowherd to identify all of the costs involved. A producer should know their costs to produce a weaned heifer calf. At weaning the producer should on paper "sell" the weaned replacement heifers to the replacement heifer development enterprise at market value. The replacement heifer enterprise "buys" the weaned heifers and then develops them into bred heifers that can be "sold" back to the cow-calf enterprise. Once the bred heifers are ready to enter the herd, the cow-calf enterprise then "buys" these bred heifers at market value.

While these transactions only occur on paper, and may seem unnecessary, it brings clarity to where expenses and value are being generated in the operation and which enterprises are profitable. Tracking all expenses that go into developing a bred replacement heifer is important to be able to identify opportunities to optimize development costs. For more information on developing replacement heifers see the UNL NebGuide "Reducing Replacement Heifer Development Costs Using a Systems Approach" (http://go.unl.edu/gdjf: PDF version, 1.04MB).

Salvage Value

In the depreciation equation, increasing the "salvage" value of cows leaving the herd often provides the greatest opportunity to reduce depreciation. Frequently cow-calf producers pregnancy test and cull non-pregnant cows in the fall of the year. Other cows are frequently culled at this time as well for reasons such as age, attitude, udders, structure, lumps, bumps, etc. This time of the year is also historically when annual cull cow values tend to be lowest for the year.

Here are two examples of ways that value can be added to cows leaving the herd increasing their worth and thus reducing depreciation expense.
1. Have a long breeding season and a short calving season. The use of pregnancy diagnosis tools such as palpation and ultrasound can identify how far along a cow is in her pregnancy. Cows that will calve later than the desired time period can be sold as bred cows and usually bring a premium to non-pregnant cows.
2. Capture additional value from non-pregnant cows by adding weight and selling into a seasonally better market than the fall. The value per pound of weight gain may surprise you. This is especially true if you can move a cow from "Lean" into a "Boner/Breaker" classification in a market where prices are increasing.
Productive Years in the Herd

Evaluate ways to cost effectively reduce cowherd turnover. The first reason cows are removed from the herd is because they are not pregnant. Young cows, especially those that are two or three years of age are often the most vulnerable. Older cows toward the end of their productive life are vulnerable as well. Tools such as hybrid vigor, genetics that fit resources, health programs, development systems and strategic feeding/supplementation can be used to cost effectively reduce cowherd turnover.

Cow depreciation is a significant expense. Cow-calf producers who aggressively manage to cost effectively reduce this expense will improve profitability.