Mississippi agriculture official expected to go to US Senate
Mississippi agriculture official expected to go to US Senate

By EMILY WAGSTER PETTUSAssociated Press
The Associated Press

JACKSON, Miss.




JACKSON, Miss. (AP) — Mississippi Gov. Phil Bryant is expected to appoint state Agriculture Commissioner Cindy Hyde-Smith to succeed a longtime U.S. senator who is resigning because of poor health.
Bryant scheduled an announcement Wednesday in Hyde-Smith's hometown of Brookhaven. Three state Republican sources have told The Associated Press that he chose the 58-year-old Hyde-Smith to succeed Sen. Thad Cochran, who is 80, is stepping down April 1. The sources spoke Tuesday on condition of anonymity because the announcement was not yet official.
Hyde-Smith would immediately begin campaigning for a Nov. 6 nonpartisan special election to fill the rest of Cochran's term, which expires in January 2020.
She won a state Senate seat in 1999 as a Democrat, then switched to the GOP in late 2010 and was elected agriculture commissioner in 2011.