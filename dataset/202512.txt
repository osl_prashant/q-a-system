According to the North American Blueberry Council 2016 Crop Report, as of Oct. 31, North Carolina had produced 34 million pounds of blueberries last season. The full season numbers for 2015 were 36.9 million pounds.
Wish Farms, Plant City, Fla., will produce about 4 million to 5 million pounds of blueberries at its Ivanhoe, N.C., operations this season, said Teddy Koukoulis, director of blueberry operations.
Volume per acre should remain consistent with last year, he said, though Wish Farms expects an increase of 1.5 million pounds over last season in North Carolina.
"We have an additional farm in Ivanhoe now," he said. "We packed there last season in a split deal with another marketer."
Based on weather patterns so far, production should be normal, Koukoulis said.
"Everyone will be early to on time unless there is some late cold weather," he said. "There are still two more full moons before Easter. Late cold weather could set North Carolina back a week or two."
Koukoulis said early demand has been good, with good placement in major retailers. "Without Costco, Ahold and the Publix of the world, it's hard to move large volumes," he said.
Wish Farms will offer all sizes of packaging for its southern and northern highbush varieties and some of its rabbiteye product.
Koukoulis is also optimistic about pricing, hoping it will sustain for a longer period of time than the past few years.
Eric Crawford, president of Fresh Results LLC, Sunrise, Fla., said the company grows blueberries in Rocky Point and Ivanhoe, N.C.
"North Carolina could experience a slight decrease (in volume) due to the impact of last year's hurricane on the crop," he said.