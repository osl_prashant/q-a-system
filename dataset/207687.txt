Small-scale asparagus growing operations in Peru and other countries might have some additional worries when requirements of the Food Safety Modernization Act go into effect next year, suppliers say.Importers of Peruvian asparagus have an additional challenge this year in making sure all FSMA requirements are met, said Maria Bermudez and Patricia Compres, partners in Miami-based Advance Customs Brokers & Consulting LLC.
Different rules under the act have staggered implementation deadlines, and importers are scrambling to make sure they meet all requirements, Compres said.
“Everybody’s trying to figure out what they need to have to comply with the rule,” she said.
“The (Food and Drug Administration) wants to make sure you have all the documents and are following all the rules. It is important.”
The final Foreign Supplier Verification Programs rule requires importers to verify that their suppliers are producing food using processes and procedures that offer the same level of public health protection as the preventive controls requirements in the FSMA’s preventive controls and produce safety rules, and current good manufacturing practices rules for human food and animal food, and that the food is not adulterated and is properly labeled with respect to allergens, according to a document from the FDA.
The importer must provide its supplier’s name, e-mail address and Unique Facility Identifier recognized as acceptable by the FDA for each line entry of food product offered for importation into the U.S.
The compliance dates for importers subject to the FSVP rule differ according to a number of considerations, including the size of the foreign supplier and which regulations apply to it.
Many customers of The Perishable Specialist, Miami, already have in place processes that can easily be tweaked in order to be FSVP compliant, said president and CEO Frank Ramos.
“Asparagus from Peru is also a product that is exempt from FSVP but not exempt from the transmission of their Unique Facility Identifier. For asparagus from Peru, the transmission of the importer’s UFI began on May 30,” he said.
Asparagus is part of a list of produce commodities that are not covered under the FSMA’s produce safety rule, which includes potatoes, squash, beets, sweet potatoes, many beans and nuts, and more.
The company has ensured its importer customers are fully compliant with the transmission of their UFI, which is due at the time of the entry filing, Ramos said.
Ramos said the FDA recognizes the D-U-N-S number, which stands for data universal number system and is assigned and managed by Dun & Bradstreet, as an acceptable UFI for compliance with the FSVP rule.
 
Small growers
Small growers in Peru may face the challenge of burdensome costs associated with compliance, Compres said.
“For our growers in general, this will be tougher on them to meet all requirements, but the larger ones have everything in place,” she said.
“Packinghouses are maintained, and rules are adhered to. But packing plants are going to have to set up and make investments that they may not be able to afford.”
How the enforcement of rules ultimately affects volumes of asparagus headed to the U.S. from Peru is an unanswered question, Compres said.
“The biggest growers, the largest growers, have the biggest volume,” she said.
“Little guys” will have to forge alliances with bigger operations to ensure they meet rule requirements, Compres said.
“The majority of asparagus coming to the U.S. comes from large growers,” she said. “To make all these plans for having a quality assurance program, to be proactive, just documenting all this stuff is a big thing to them.”
Tightened rules could jeopardize some smaller growers, said Pablo Gonzalez, account manager with Seven Seas Global Produce Network, a Miami-based division of Tom Lange Co.
“What will happen is little growers will sell their product to growers who have certifications and will pack at their packinghouses so they bring it here, so you may see growers going out of business in Peru and Central America,” he said.
Fabian Zarate, sales manager with Coconut Creek, Fla.-based Tambo Sur, agreed.
“Obviously, the smaller growers, their infrastructure is not there, so it will affect them,” he said.