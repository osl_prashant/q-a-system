Chicago Mercantile Exchange lean hogs on Wednesday slid from a 10-month high, weakened by profit-taking and less money paid by packers for slaughter-ready, or cash, hogs, said analysts.
February closed 0.500 cent lower at 71.025 cents. April ended down 0.100 cent to 75.275 cents.
Processors need fewer animals that backed up on farms during Christmas holiday plant closures, which will be repeated when they close on New Year’s day, traders and analysts said.
Some hogs are heavier and plentiful due to nutrient dense newly-harvested corn, a Midwest hog buyer said. And unseasonably warm temperatures in the Midwest prior to this week caused pigs to grow faster, he said.
He added that the onset of bitter cold weather now in most of the nation’s hog-growing region will slow animal weight gains. Those hogs are consuming feed to generate body heat rather than pack on pounds.
Farmers are also avoiding opening doors to swine buildings in order to retain heat, the Midwest hog buyer said.
Mainly Weaker Live Cattle Futures
Most CME live cattle contracts closed moderately lower after investors sold deferred months and bought December in anticipation of steady-to-firmer cash prices this week, said traders.
December live cattle, which will expire on Friday, closed 0.525 cent per pound higher at 122.025 cents. Most actively traded February ended 0.475 cent lower at 121.000 cents, and April finished down 0.225 cent to 121.700 cents.
Cattle are raised outside, so traders expect this winter’s first blast of Arctic air across the U.S. Plains to slow animal weight gains, said U.S. Commodities president Don Roose.
Lighter cattle makes them less available to packers, giving feedlots more leverage in negotiating prices.
Other supportive cash price factors include higher packer margins, improved wholesale beef demand and tight cattle supplies in parts of the Plains.
Feedlots in Kansas and Texas are asking $124 to $125 per cwt for cash cattle that last week in the U.S. Plains traded from $119 to $120. Packers have not responded with bids.
None of the 637 cattle available at Wednesday’s Fed Cattle Exchange attracted buyers. Last week, a few cattle there sold for $120 per cwt.
By stops and technical buying sent CME feeder cattle higher.
January feeder cattle closed 0.775 cent per pound higher at 145.175 cents.