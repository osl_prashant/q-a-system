Hearing concerns about food safety compliance issues directly from growers, senior staff with the Food and Drug Administration toured two Delaware produce farms Aug. 5. The outing was co-hosted by officials with the Newark,Del.-based Produce Marketing Association and the Delaware Department of Agriculture. 
 
The agency's new Deputy Commissioner for Foods and Veterinary Medicine, Stephen Ostroff, participated in the tour. Ostroff took over as FDA's top food regulator on June 1. Other senior officials who were part of the tour were: Samir Assar, produce safety division director;
 
Ted Elkin, Center for Food Safety and Nutrition deputy director for regulatory affairs; and Rebecca Buckner, interim director for Food Safety Modernization Act operations.
 
Leanne Skelton, U.S. Department of Agriculture Agricultural Marketing Service liaison to FDA, also participated in the tour, according to the release.
 
Produce industry leaders on the tour, according to the release, included: Cathy Burns, PMA president; Jim Gorny, PMA vice president of food safety and technology; Bob Whitaker, PMA chief science and technology officer; and Ed Kee, Delaware agriculture secretary.
 
The tour stopped at Coastal Growers in Laurel, Del., and Fifer Orchards in Wyoming, Del., according to the release.
 
Gorny said the visits, featuring diverse produce farms that must soon comply with the produce safety rule, offered the opportunity for growers and packers to talk about the challenges they face in food safety compliance. In particular, Gorny said several industry members questioned how third-party audits and FDA inspections will work together.
 
Grower-shippers want to get credit for all the food safety work they have done on the farm and in the packinghouse, Gorny said. Growers hope the FDA will lessen "audit fatigue" for firms that are complying with food safety rules, he said. In other words, growers are wondering if third-party audits will reduce the need for FDA regulatory inspections. 
 
"The FDA and the state departments of ag are very interested in that, because it will help them appropriately allocate limited food safety inspection resources," Gorny said. "How we do that is what we will talking about a lot in the coming years."
 
Gorny said the FDA has a rule for third-party accreditation of auditors, but now that is exclusively reserved for foreign inspections. 
 
He predicted the role of domestic third-party audits will continue to be a topic of discussion.
 
For their part, Gorny said FDA officials told industry leaders they are working on providing industry food safety guidance, with some documents under review now before they are released.
 
"These tours are part of PMA's ongoing work to connect the policymakers who regulate our industry with fruit and vegetable producers' realities and experiences, to encourage them to develop real-world workable solutions to our industry's unique food safety needs," PMA's Burns said in the release.