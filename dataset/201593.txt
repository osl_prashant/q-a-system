While politicians on both sides of the aisle have differing opinions of the TPP, cattlemen across the U.S. are hoping to get the trade deal signed as soon as possible.Colin Woodall, vice president of government affairs for the National Cattlemen's Beef Association spoke on AgriTalk Tuesday, telling host Mike Adams about the $400,000 a day the industry loses because they don't have TPP.
Since the U.S. doesn't have access into the Japanese beef market, they are losing to the Australia. Australia's tariff will decrease starting on Jan. 1, 2017, and willresult in greater losses for the U.S. According to Woodall, NCBA found this has a "significant impact" on Congress who are seeing a daily quantifiable impact of not having TPP in place.


<!--
LimelightPlayerUtil.initEmbed('limelight_player_195482');
//-->

For the NCBA, passing TPP is the number one issue during the lame duck session. Woodall said there's been a lot of time, effort, and resources that have gone into visiting and talking with members of Congress to get a vote. He's optimistic it can get passed.
"We are not giving up," said Woodall. "If we do not get this done by the end of the year, we doubt we will ever have the opportunity to get TPP."
The election is a major reason why congress won't vote on TPP, said Woodall. Even with a new administration or a new congress, he doesn't believe TPP will have a chance.
"Not voting on it is not an option for the U.S. cattle industry," said Woodall.
Listen to Woodall on AgriTalk above.