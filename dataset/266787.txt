Tyson Foods, Tesla and Live Nation sink; Humana climbs
Tyson Foods, Tesla and Live Nation sink; Humana climbs

The Associated Press

NEW YORK




NEW YORK (AP) — Stocks that moved substantially or traded heavily Monday:
Humana Inc., up $11.87 to $280.70
Reports said the health insurer is in talks about a sale or partnership with Walmart.
Tyson Foods Inc., down $4.55 to $68.64
The Chinese government said it will place a tariff on some U.S. products, including pork.
Tesla Inc., down $13.65 to $252.48
The electric car maker said semi-autonomous mode was engaged when one of its cars was involved in a fatal crash last week.
Live Nation Entertainment Inc., down $3.97 to $38.17
The New York Times reported that the Justice Department is probing the concert-promotion and event-ticketing company.
Commvault Systems Inc., up $6.20 to $63.40
Activist investment firm Elliott Management disclosed a 7.4 percent stake in the security firm.
Exxon Mobil Corp., down $1.39 to $73.22
Energy companies slumped with oil prices Monday.
Cisco Systems Inc., down $1.88 to $41.01
Big technology companies and other recent winners declined Monday as investors continued to worry about rising trade tensions.
UnitedHealth Group Inc., up $3.20 to $217.20
The health insurer said it will work with several other companies to see if blockchain technology can reduce their costs and improve their data.