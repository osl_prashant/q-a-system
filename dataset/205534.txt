These 5 pairs of growers and retailer partners exemplifythe use of 4R nutrient stewardship practices on the farm
This year's advocates farm 13,200 acres of land using 4R Nutrient Stewardship practices.

The 2017 Advocates include:
Gary Reeder, Palmetto, Fla.
Dennis Coleman, Crop Production Services, Parrish, Fla
Kyle Brase, Edwardsville, Ill.
Joe Huebener, CHS Shipman, Shipman, Ill.
Grant Strom, Brimfield, Ill.
Adam Dexter, West Central FS Inc., Williamsfield, Ill.
Dave Legvold, Northfield, Minn.
Ken Thomas, Farmers Mill & Elevator, Inc., Castle Rock, Minn.
Lynn Fahrmeier, Wellington, Mo.
Scott Bergsieker, MFA Incorporated, Lexington, Mo.
Now in its sixth year, the 4R Advocate Program has recognized 30 agricultural producers and retailers, farming 144,425 acres in 17 states. These forward-thinking individuals serve as examples by championing sound nutrient stewardship.
"4R Nutrient Stewardship is a top priority for the fertilizer industry, which is committed to enhancing environmental and economic sustainability across their operations - from the production plant to on-farm application," said Chris Jahn, TFI President. "Today, we honor those in the industry who are exemplifying this commitment and using the 4R principles to increase yields, improve soils, and decrease fertilizer's environmental footprint."
The winners will receive an expense-paid trip to the 2017 Commodity Classic, where they will be honored at an awards banquet hosted by TFI. While at Commodity Classic, the advocates will represent the 4R program at the trade show and participated in various interviews. Throughout the year they will also be part of TFI's outreach efforts to promote nutrient management practices by hosting farm field days, participating in conference panels, and speaking on behalf of 4Rs to their farming peers.
The 4R Advocate program is one of many facets of a high-priority campaign to raise awareness and adoption of 4R Nutrient Stewardship practices. Fertilizer is a component of sustainable crop production systems, and the fertilizer industry recognizes the need to efficiently utilize these nutrients.
4R Nutrient Stewardship is an innovative and science-based approach that offers enhanced environmental protection, increased production, increased farmer profitability, and improved sustainability. Implications of the 4R nutrient stewardship system will spread far and wide through agriculture and society as a whole. For fertilizer use to be sustainable, it must support cropping systems that provide economic, social, and environmental benefits.
To help address this challenge,TFI works with theInternational Plant Nutrition Institute, theInternational Fertilizer Industry Association,and theFertilizer Canada to advance the 4R nutrient stewardship initiative.