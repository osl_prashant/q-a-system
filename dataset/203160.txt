Another severe snowstorm in the Treasure Valley region has caused the collapse of more onion storage buildings, and some grower-shippers expect prices to jump as a result.
Estimates regarding the amount of snow ranged from 12 to 18 inches, notably more than what was forecast, industry members said. Several companies shut down production Jan. 20 in order to dedicate all resources to removing snow from roofs.
"This is a bona fide catastrophe," said Kay Riley, general manager at Nyssa, Ore.-based Snake River Produce.
Owyhee Produce, also based in Nyssa, had four buildings collapse Jan. 18 and Jan. 19, including the company's main storage facility, said general manager Shay Myers.
About 20 millions pounds of onions were lost, but the packing shed had minimal damage, so Owyhee planned to resume shipping on a limited basis the week of Jan. 23.
Myers said he expects the market to spike 20%-30% as a result of the damage from two major snowstorms in the region in less than two weeks. The previous snow, followed by rain that saturated it, also caused numerous collapses.
Ontario, Ore.-based Murakami Produce has been fortunate to not lose any buildings yet, said onion sales manager Chris Woo, though that could change.
"We're crossing our fingers," Woo said. "I really feel bad for the people that have had buildings go down. It's terrible down here."
Murakami shut down production for an afternoon and sent workers to shovel snow off the roofs of buildings instead.
"We've been lucky," Woo said. "Hopefully we stay lucky."
 Cleanup following the collapse of the packed product storage building onto US-26
With the storms causing so much loss for numerous companies, some have invoked "act of God" clauses in contracts with customers because they cannot fill orders, Woo said. Murakami Produce has been receiving calls from quite a few of those customers and has been able to fulfill some of the orders.
"We just have to be selective," Woo said, noting the company is making sure to take care of its loyal customers. "We can only do so much."
Woo said onion prices have already been going up as a result of the weather-induced chaos. Around the holidays the price was between $4 and $5, and this week it has been $8 and will probably go up further, Woo said.
Riley said it was difficult to estimate how much the snowfalls will affect overall volume and prices. He added, however, that the disruption has become "much more significant" with the second wave of building collapses.
Snake River Produce lost the facility in which it stores packed product Jan. 19. The building had about 20,000 bags of onions inside, Riley said.
"I've got trucks showing up this morning to load," Riley said, "but they're all going away."
 Damage to onion bin storage building
Snake River Produce plans to convert another building to storage. It had lost another storage building, which had contained trucks but no onions, in the earlier snowstorm.
After this latest interruption, the company hopes to resume business Jan. 23, Riley said.
Nyssa-based Golden West Produce has lost six buildings - three in each storm, said secretary Patty Henderson. The latest damage included the packing shed, a loss Henderson called "devastating."
A storage facility connected to the packing shed will be demolished because it is already damaged and is near a street.
Golden West is working with insurance adjusters to determine which of the damaged buildings can be saved and which are ruined.
Ontario-based Frahm Fresh Produce lost its main storage shed and was working to save its packing shed, which was standing but missing a wall.
Weiser, Idaho-based Four Rivers Onion Packing lost its packing shed in the first storm, and Weiser-based Haun Packing lost in the earlier storm a storage building that had some equipment inside.
Herb Haun, general manager at Haun Packing, said the company didn't lose any more buildings in the second blast of snow but said the weather had generally made it difficult to operate. The company shut down for several days to focus on getting the snow off the roofs of its facilities, and transportation has been a mess.
Haun said 30 to 40 onion buildings have collapsed in the last couple of weeks.
So far, no injuries have been reported in connection to the dozens of roof failures.