When USDA's weekly crop condition ratings are plugged into Pro Farmer's weighted (by production) Crop Condition Index (CCI; 0 to 500 point scale, with 500 representing perfect), it shows the condition of the corn and soybean crops improved slightly.
USDA reports as of Sunday that just 28% of the nation's corn crop was harvested, a six-point gain from the previous week and well behind the five-year average of 47%. Traders expected harvest to come in at 31% complete.
Illinois has 47% harvested (64% on average), with Indiana at 34% (46%), Iowa at 13% (41%), Minnesota at 7% (38%), Nebraska at 17% (39%), Ohio at 21% (32%) and South Dakota at 12% (39%).
As expected, USDA reports 49% of the nation's soybean crop was harvested as of Sunday, a 13-point gain from the previous week, but behind the five-year average of 60% complete. With harvest nearly half complete, the bulk of hedge-related pressure should soon be behind the the market.
Illinois has 63% harvested (58%), with Indiana at 52% (52%), Iowa at 32% (66%), Minnesota at 45% (82%), Nebraska at 33% (67%), Ohio at 54% (53%) and South Dakota at 48% (78%).





Pro Farmer Crop Condition Index






Corn




This week



Last week



Year-ago





Colorado (1.03%*)

4.17
3.76

3.78




Illinois (15.40%)

55.91
56.07

62.85




Indiana (6.64%)

23.17
23.11

26.32




Iowa (17.72%)

63.26
63.08

67.70




Kansas (4.29%)

15.12
15.03

14.46




Kentucky (1.57%)

6.21
6.21

6.34




Michigan (2.35%)

8.26
8.05

9.06




Minnesota (9.66%)

37.87
37.77

38.40




Missouri (3.81%)

14.13
14.13

13.95




Nebraska (11.62%)

42.43
42.55

45.03




North Carolina (0.71%)

2.88
2.88

2.73




North Dakota (2.70%)

9.30
9.16

9.81




Ohio (3.80%)

14.03
13.88

13.64




Pennsylvania (0.98%)

4.24
4.09

3.58




South Dakota (5.62%)

18.03
17.41

19.91




Tennessee (0.89%)

3.80
3.80

3.39




Texas (2.06%)

8.19
8.19

6.92




Wisconsin (3.61%)

13.82
13.57

14.39




Corn total













365.02
























362.81













385.90









Pro Farmer Crop Condition Index






Soybeans




This week



Last week
			for sale


Year-ago





Arkansas (3.78%*)

14.09
14.09

13.62




Illinois (13.85%)

49.17
49.59

55.97




Indiana (7.41%)

26.24
25.87

29.51




Iowa (13.35%)

47.94
47.80

52.66




Kansas (3.96%)

12.87
12.95

14.27




Kentucky (2.14%)

8.28
8.32

8.70




Louisiana (1.59%)

5.73
5.73

5.72




Michigan (2.38%)

7.84
7.91

9.09




Minnesota (8.82%)

32.91
33.09

34.65




Mississippi (2.38%)

9.32
9.32

9.33




Missouri (5.86%)

21.56
21.33

22.09




Nebraska (7.46%)

26.57
26.80

29.66




North Carolina (1.50%)

5.62
5.69

4.89




North Dakota (5.24%)

17.93
17.67

18.28




Ohio (6.14%)

21.75
21.69

22.90




South Dakota (5.93%)

20.35
19.93

21.19




Tennessee (1.86%)

1.00
1.00

1.00




Wisconsin (2.29%)

9.00
8.84

8.70




Soybean total













352.34
























351.76













379.24




* denotes percentage of total national corn crop production.
Iowa: Most Iowa farmers fell further behind on harvest due to rain throughout much of the state during the week ending October 15, 2017, according to the USDA, National Agricultural Statistics Service. There were only 2.0 days suitable for fieldwork ; the second week in a row Iowa has had less than 3 days suitable for fieldwork. Many activities were delayed due to the wet conditions, but when the weather allowed farmers harvested corn for grain and soybeans, planted cover crops, and hauled manure. Topsoil moisture levels rated 4 percent very short, 7 percent short, 71 percent adequate and 18 percent surplus. Subsoil moisture levels rated 9 percent very short, 16 percent short, 68 percent adequate and 7 percent surplus.
Ninety-four percent of corn had reached maturity or beyond, six days behind last year and two days behind the 5 - year average. Just 13 percent of the corn crop for grain has been harvested, the smallest percentage harvested by this date since 2009 and over 2 weeks behind average. Moisture content o f corn being harvested for grain averaged 21 percent. Corn condition rated 61 percent good to excellent. Ninety - six percent of soybeans were dropping leaves, one day ahead of last year and two days ahead of average. Thirty-two percent of the soybean crop h as been harvested, the lowest percentage harvested by this date since 1985. Only east central Iowa farmers have harvested over 50 percent of their soybean crop. Soybean condition rated 63 percent good to excellent.
Illinois: Cooler temperatures across the state last week. There were 3.7 days suitable for fieldwork during the week ending Oct. 15. Statewide, the average temperature was 61.8 degrees, 5.9 degrees above normal. Precipitation averaged 2.61 inches, 2.14 inches above normal. Topsoil moisture supply was rated at 12% very short, 19% short, 56% adequate, and 13% surplus. Subsoil moisture supply was rated at 11% very short, 30% short, 55% adequate, and 4% surplus.
Corn mature was at 95%, compared to 100% last year. Corn harvest was 47% complete, compared to 64% for the 5-year average. Corn condition was rated 4% very poor, 7% poor, 26% fair, 48% good, and 15% excellent.
Soybeans dropping leaves was at 94%, compared to 96% last year. Soybean harvest was 63% complete, compared to 55% last year. Winter wheat planted was at 51%, compared to 40% last year.
Nebraska: For the week ending Oct. 15, 2017, temperatures averaged near normal across eastern Nebraska, but four to eight degrees below normal in the west, according to the USDA's NASS. Precipitation of less than an inch was scattered across a majority of the State; however, a few southeastern counties received over an inch of rain. Wet fields continued to slow harvest. There were 3.6 days suitable for fieldwork. Topsoil moisture supplies rated 2% very short, 7% short, 80% adequate and 11% surplus. Subsoil moisture supplies rated 4% very short, 14% short, 76% adequate, and 6% surplus.
Corn condition rated 4% very poor, 9% poor, 23% fair, 46 good, and 18 excellent. Corn mature was 92%, near 95 last year and 93% for the five-year average. Harvested was 17%, behind 32% last year, and well behind 39% average.
Soybean condition rated 4% very poor, 9% poor, 26% fair, 49% good, and 12% excellent. Soybeans harvested was 33%, well behind 59% last year and 67% average.
Minnesota: Minnesota farmers took advantage of the 4.2 days suitable for fieldwork and made good progress in soybean, sunflower and sugarbeet harvests during the week ending October 15, 2017, according to USDA's NASS. Muddy field conditions made harvest a challenge in some areas. This continued to lag harvest progress behind the 5-year average for all crops except dry edible beans. Harvest continued for corn for grain and silage, and potatoes. Topsoil moisture supplies rated 0% very short, 2% short, 74% adequate and 24% surplus. Subsoil moisture supplies rated 2% very short, 5% short, 75% adequate and 18% surplus.
Eighty-nine percent of the corn for grain crop had reached maturity. Harvest progress of corn for grain at 7% complete was 2 weeks behind the 2016 pace and 22 days behind average. Corn moisture content of grain at harvest was 25%. Corn harvested for silage was 12 days behind average with 89% harvested. Corn condition improved 1 percentage point to 81% good to excellent.
Nearly all of the soybean crop was dropping leaves with 45% harvested, remaining well behind last year. Soybean condition improved slightly to 70% good to excellent.