The road to success was not an expected or easy one for Maggie Holub. Still, she followed her passion and fought through life’s twists and turns to find success.
Holub grew up working on her family’s farm. In 2014, when her father passed away from cancer, she had an opportunity to start farming half of the family’s owned land. Her sister and brother in law would farm the other half.
“We didn’t have a succession plan in our farming operation and it was up to my mom, my sister and myself to figure out what to do,” she told AgriTalk host Chip Flory on Tuesday.

While she has an agribusiness degree, Holub says she still had a lot to learn about farming. A passion for learning and willingness to ask people to mentor her, have helped her achieve success.
“I’m a learner by nature,” she says. “I love to learn, that’s one of my number one strengths.”
Holub has grown her operation to include rented land. This year she will grow 700 acres of corn and soybeans.
Maggie Holub is the 2018 Top Producer Horizon Award winner. The Horizon Award is sponsored by DuPont Pioneer.