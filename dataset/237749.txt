For the third year, the U.S. Apple Association is promoting Munch Madness, asking consumers to fill out brackets of their favorite apples in a takeoff of the NCAA basketball tournament.
In a change from the previous “tournaments,” the promotion starts with 16, and not 32, varieties. Starting March 1, the four rounds allow consumers to vote on their favorites:

Vitamin-C Sixteen, March 1-7;
Edible 8, March 8-14;
Fiber Four, March 15-21;
National Chomp-ionship, March 22-28.

The winner will be announced March 30. Last year, Envy won the competition.
“Munch Madness is the perfect opportunity for the public to combine its favorite spring pastime of filling out college basketball brackets with its favorite fruit, apples,” Tracy Grondine, director of consumer health and media relations for the apple association, said in a news release. “This fun, interactive game engages the public, while educating them about many different apple varieties and their flavor profiles.
Consumers can vote at appleVSapple.com starting March 1. The association’s social media channels — Facebook (@USApples) and Twitter (@US Apples) — will promote the campaign using #MunchMadness.
The apples are ranked according to dollar sales in the traditional bracket formats: In the first round, the leading apple (gala) “plays” the 16th-ranked variety (Pacific Rose).
The other matchups in the Vitamin-C Sixteen:

Honeycrisp/jonagold;
fuji/empire;
granny smith/SweeTango;
red delicious/braeburn;
Pink Lady/Envy
Golden delicious/Jazz; and
Ambrosia/mcintosh