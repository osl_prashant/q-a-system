As harvest rolls along commodity prices haven't given much incentive to sell summer crops off the combine. Jim Bower, founder of Bower Trading tells AgDay host Clinton Griffiths at these grain price levels it's pretty hard to make money in farming.
"Hopefully things will get better as we go into 2018," says Bower. "Low prices cure low prices."
He sees several areas with the potential to improve global demand, which will eventually pull prices higher.
With China discussing expanding their ethanol usage and production,  Bower thinks it could have an impact down the road.
Domestically, price potential is going to depend on getting real yield numbers from the field.
"We've got to see exactly what the yields truly are," says Bower. "Estimates are only good for so long."
He says the final result may be better than what analysts and USDA expected or they might not.
"Because of the dryness and heat that we saw in parts of Iowa, Illinois, Indiana and Ohio during the course of summer, I think the government is still probably a little bit too high on their yield estimate," says Bower. "The [yield monitor ] on that combine will tell."
Altogether, Bower says it adds up to patience as a marketer. 
"As far as strategy, I like to sell and defend," says Bower.  "I've always been pro-grain bins and pro tracking the basis."
Bower believes by January grain markets will find a better footing. 
"The bin doors are going to be slammed shut once this harvest is in, and they're going to wait to see if they get better prices in 2018," says Bower. "You have to be patient."