West Pak Avocado's West Pak Cares, a community involvement campaign, hosted a company-wide blood drive at the company's corporate offices in Murrieta, Calif.
The event took place on Aug. 18, and the donated blood was provided to the San Diego Blood Bank, according to a news release.
The blood drive yielded 28 units of blood from 32 eligible donors, surpassing the company's original goal by 18%.
"We're honored to sponsor this much-needed event through the San Diego Blood Bank," said Doug Meyer, West Pak's senior vice president of sales and marketing, in the release.
Blood provided by this drive will be transfused into patients struggling with life threatening medical conditions, such as cancer and other blood disorders.
"It's efforts like this that truly make a difference in the lives of others and we're very proud to be a part of it!" Meyer said in the release.