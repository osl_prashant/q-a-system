OSU boosting its Newport campus with marine studies center
OSU boosting its Newport campus with marine studies center

By ANDREW THEENOregonian/OregonLive
The Associated Press

NEWPORT, Ore.




NEWPORT, Ore. (AP) — For more than 50 years, Oregon State has had an unassuming outpost on a spit of land near the mouth of Newport's Yaquina Bay.
Much of the university's coastal campus looks like an aging high school, with its bunker-like hallways and drab wood roofing. But inside its walls is an internationally known marine laboratory that is an invaluable asset to the coast's second-largest city and its commercial fishing industry.
Often overlooked statewide, Oregon State's research center is poised for the most significant construction project in the coastal campus' history.
OSU breaks ground March 15 on a $58 million new building at its Hatfield Marine Science Center in Newport. The new marine studies center represents a dramatic increase in space, resources and ambition that the school hopes will result in a similar jolt in interest in the fate of the ocean, marine life and coastal communities.
It's the perfect time, Hatfield director Bob Cowen said in an interview last month, to put hundreds of students in the middle of nature's laboratory.
"We can put our students in touch with politicians, we can put them in touch with fishermen, we can put them on their boats, we can put them in a seafood processing lab," he said.
OSU already sends students to the coast, but at full build-out, the new building would allow roughly 500 full-time students to study at the coast during the typical school year, five-times the current figure.
The school also anticipates another 750 students to be part of the Marine Studies Initiative at the Corvallis campus.
The project comes as Oregon Health & Science University and the University of Oregon move forward with glitzy projects largely made possible by $500 million gifts from Nike co-founder Phil Knight.
Oregon State's expansion is funded by private philanthropy and $24.8 million in state bonds. The school plans to hire as many as 50 new professors, half in Corvallis and half at the coast, by 2025.
The university project will have a ripple effect on Yaquina Bay.
The Newport research campus is also home to five federal and state agencies, including more than 150 National Oceanic and Atmospheric Administration researchers and scientists.
NOAA, which has its largest West Coast research fleet and its Pacific Ocean operations in Newport, sees huge benefits in partnering with up-and coming-scientists on salmon protection, ensuring fisheries are sustainable, addressing ocean acidification and developing renewable energy.
"Our longtime partnership with OSU has brought tremendous value to the state, the community and NOAA, and the marine studies initiative will increase that value many times," said Barry Thom, NOAA Fisheries' west coast regional administrator, in an email. "Collaborative research with OSU helps strengthen our management decisions, and we expect the initiative to shape a talented future generation of NOAA staff and scientists."
For the university, the project is also about bridging the 50-mile distance from Corvallis to the coast.
"We can become a destination of choice for people who really have a passion for climate and the environment," said Ed Ray, the school's president.
It's also about capitalizing on the assets already under OSU's nose as well as big-ticket projects in the pipeline.
Newport is a deep-water seaport, the school was recently awarded a $122 million grant to design and build the next-generation federal research vessel and the school is creating a wave energy facility off the coast backed by a $40 million federal energy grant. OSU is also raising $10 million for the facility and is seeking $3 million in state funding during the short session.
Jane Lubchenco, an Oregon State marine ecologist since 1977 and a former NOAA administrator under President Obama, said the Hatfield location has excelled for decades as a place for small groups of students to learn and research.
She said it's exciting to see that change. "We're going to enter a whole new level of educational opportunities."
TSUNAMI ZONE
To date, much of the buzz about the coastal project centers on its location — inside a tsunami-inundation zone on Yaquina Bay.
That decision drew the ire of the then-state geologist, Vicki McConnell, and from more than a dozen Oregon State University faculty members who said the threat of a Cascadia Subduction Zone rupture should've foreclosed the option to build on the existing site.
In February 2016, they wrote the location would "threaten lives, damage buildings and hobble the research capacity of this flagship institute."
But university leaders said they heard the criticism and moved forward with the site after what they described as a thorough engineering review. They said the current site was the best option for several reasons.
OSU pumps nearly one million gallons of fresh seawater from the bay into laboratories for research each day, pumping water to higher ground would be expensive. The school also said the new building would serve as a "vertical evacuation zone" for the hundreds of students and staffers of OSU and federal and state agencies already working there.
The school is building new student housing outside the tsunami zone.
"We don't want to necessarily send a signal that we're running away from the coast," Cowen said.
RUN TO COAST
Oregon State, in fact, wants students and faculty to start running toward the coast.
It started the Marine Studies Initiative, led by long-time oceanography professor Jack Barth, to create a cohesive and wide-ranging course load of marine-related classes across the entire university.
OSU is encouraging its 11 academic colleges and the Graduate School and Honors College to consider holding upper division courses at the Newport campus.
Barth said the possibilities are endless. OSU envisions classes looking at resilience in the wake of tsunamis, how art plays a role in rural coastal communities and finding ways to share narrative stories of the research work already underway by researchers at the Newport outpost.
He's already heard from one professor who asked about teaching a math class to juniors and connecting it to wave activity.
"People are starting to come to us and ask how we can get involved," Barth said.
Lubchenco said the university has struggled to offer a cohesive pitch to woo students to the coast.
"The Marine Studies Initiative really presents a golden opportunity for OSU to not only showcase what it has — that is world class science and research — but do a much more effective job of commingling that with business, with public health," she said, and do so "in an integrative and cohesive effort of not only teaching and research but also engagement and outreach."
OSU AND COASTAL COMMUNITY
Directly across the bay from the Hatfield campus, Laura Anderson's Local Ocean Seafoods readied for a lunch crowd on a January morning.
Anderson, an Oregon State alum and daughter of a commercial fisherman, has built a successful restaurant thanks to the fresh bounty right outside her door.
She said the Hatfield expansion is going to be a good thing for the region's economy. "It's going to be noticeable having 400-plus students during the school year," she said.
Oregon State is already intertwined with the region.
The Oregon Sea Grant, paid for by federal and university dollars, is housed at the Hatfield center. The outreach program reached 40,000 Oregon schoolchildren last year, offering field trips and developing curriculum to teach students about marine issues. The Hatfield visitor center, with its tide pool exhibit and octopus tank and other educational offerings, draws roughly 150,000 people each year.
Michele Longo Eder, a retired attorney and co-owner of a commercial fishing vessel, said the local industry already has a symbiotic relationship with the university.
Longo Eder, who is an OSU trustee, said her boat and others has worked closely with scientists on examining mortality rates for Dungeness crab and on other research issues.
Those close ties are only going to be closer, she said, and issues like warming seas and ocean acidification raise the importance of collaborating with the school.
"We have a smart fleet of commercial fishermen who know that ocean health is important," she said.
The expansion is not without challenges. Newport has a housing shortage, according to Anderson, and that will be tested despite plans for new dorms.
Anderson said it's also important to have a community discussion of how Newport can absorb the influx of students and provide ample activities for young people.
These are all good problems for the city to have.
"It's really exciting on many levels," Anderson said of the expansion.
WHAT'S NEXT
Barth, the professor and head of the Marine Studies Initiative, said the Newport program has self-selected for humble hardworking types through the years.
"We used to joke that if we had somebody here doing amazing things and they were not shy about bragging, 'oh they'll leave soon,'" Barth said.
OSU doesn't plan on ditching its humble nature, but it is increasingly optimistic its star will continue to rise in marine circles.
David Conover, a former director of the Division of Ocean Sciences at the National Science Foundation and the current vice president of research and innovation at University of Oregon, said the Hatfield center already has a good national reputation in oceanography in particular.
"There's not that many institutions that are large enough to sustain an ocean-going research vessel," he said.
Cowen, the head of the Hatfield center and president of the National Association of Marine Laboratories, said he came to Newport in 2013 because he saw its star was on the rise.
He said the upward trajectory is palpable.
Oregon is home to one of the world's premier marine science and oceanography schools, Cowen said. And the state should be proud of it.
The Scripps Institution of Oceanography at the University of California-San Diego and Woods Hole Oceanographic Institution in Massachusetts are OSU's main contemporaries, according to Cowen.
The new project should allow for OSU to dream bigger.
"People often say our goal is to be the Woods Hole of the West," Cowen said.
That's not how he views it. "Actually, they'll be the Hatfield of the East."
___
Information from: The Oregonian/OregonLive, http://www.oregonlive.com