"We can rebuild him. We have the technology. We can make him better than he was. Better, stronger, faster."
 
Just how quickly will automation help ease the farm labor crisis? Will the six million dollar ag robot arrive in two, five or ten years?
 
I posed a question to the Fresh Produce Industry Discussion Group today about the promising age of ag robots.
 
 
I asked: 
 
What is your attitude about the issue of robots/automation in the field and warehouse? In your particular business, do you think robotics/automation will transform how you do your work? What type of innovations do you anticipate? How long will it take for robotic harvesting to make sense economically?
 
 
One thoughtful response so far, from Alan in California:
 
"Automation in row crops is already here with thinning and weeding. A good start !.. The next step is really in Harvesting with a lead from European research and Universities. It is coming slow in the country but it is definitely on the radar of all mid- to large size growers. Despite a great momentum in automation / robotics investment in U.S. ag, it appears that many projects do not relate to the area of harvesting. Possibly not sexy enough and with a limited market to recover R&D investments ! I believe that automation will indeed change working habits (no more packing in the field ?). For larger operations depending so much on H2A labor, ROI should be relatively quick. The challenge is more the on the technical side as there might not be one universal harvester across all crops."
 
 
The Packer will have some coverage this week of the effort to build a robotic apple picker, but the span of interest in automation is broad.
 
In a 2016 report, " Planting the seeds of a robot revolution: how autonomous systems are integrating into precision agriculture," analyst Sara Olson of Boston-based Luxresearch said investment in the sector is growing.
 
From technology like auto-steer for tractors to assistive exoskeletons for reducing worker fatigue during harvesting, the range of research is wide, she said.
 
The Luxresearch report said the most significant drivers of robot adoption center on limited labor availability, regulatory pressures and increased accuracy and precision associated with robotic solutions. Regulatory pressures from worker exposure to pesticides may become more of a focus in the future, according to the report.
 
Fruit and tree nut growers with large farms can expect to need nearly 100,000 hours of labor on an annual basis, according to the report, or a crew of more than 50 employees working full-time year round.
 
The report said specialized commercial solutions so far that have been developed include a grapevine pruning robot, a mechanical weeding robot, a strawberry harvesting robot and a lettuce thinning robot.
 
The Luxresearch report said lettuce thinning platforms used in Europe are likely to break even with human labor by 2028. Regulations relating to ag chemicals makes the automated lettuce weeder cost-competitive with human labor already, the report said.
 
In Japan, a strawberry harvesting robot is about equal in cost to human labor, the report said, provided that several farms share the investment in the robotic picker.
 
The Luxresearch said robotic solutions offer improved accuracy and precision in most tasks - perhaps 1% to 5% better than human labor - and that could be significant for big farms, according to the report.
 
The industry may have to wait a while for its robotic savior, but the day is closer today than yesterday.