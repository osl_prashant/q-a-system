Idaho grower Dan Moss, CEO of Rupert, Idaho-based Moss Farms, was named The Packer’s 2016 Potato Man for All Seasons. 
The Packer is accepting nominations for the 2017 Potato Man of the Year/Potato Man for All Seasons award.
The annual award is sponsored by The Packer in conjunction with the National Potato Council.
The 2017 award winner will be announced at the 2018 National Potato Council’s Annual Meeting banquet, Jan. 12 at the Rosen Shingle Creek Hotel in Orlando, Fla.
The Potato Man of the Year recognizes an individual who has gone above and beyond the call of duty during the past year to further the potato industry. 
Past winners include Chris Voigt of the Washington State Potato Commission and Justin Dagen, former NPC president.
The Potato Man for All Seasons is a lifetime achievement award that recognizes individuals who during their career in the potato industry have given freely of their time to improve the well-being of the potato industry.
Idaho grower Dan Moss, CEO of Rupert, Idaho-based Moss Farms, was named The Packer’s 2016 Potato Man for All Seasons. Neil Gudmestad, a potato pathologist with North Dakota State University, was recognized as The Packer’s 2015 Potato Man for All Seasons.
Nominations are available online or by sending an e-mail request to tkarst@thepacker.com. The deadline to submit nominations is Dec. 10. E-mail completed nomination forms to Tom Karst, National Editor of The Packer, at tkarst@thepacker.com, or mail the completed forms to Tom Karst at The Packer, 10901 West 84th Terrace, Suite 300, Lenexa, KS, 66214.