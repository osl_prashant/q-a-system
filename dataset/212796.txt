Los Angeles, Calif.-based Marina Capital Group, a mergers & acquisitions and strategic consulting firm, is launching Agri Capital Co. 
Funded with $250 million from the Marina Capital Group, the new company is dedicated to investment in food and agriculture — particularly on identifying private equity opportunities in middle-market food and agriculture businesses, according to a news release.
 
“We are excited about Agri Capital Co. We believe the food and agriculture sectors offer compelling investment opportunities along the entire food supply chain — from grower production to distribution and consumption,” Hal Berger, managing partner of Agri Capital, said in the release.
 
Berger said agriculture growth will be shaped by several factors in the years ahead:
Continued global population growth, with the world’s population expected to grow from 7.2 billion now to 9.6 billion by 2050;
Transition from a carbohydrate-based diet to a protein plant and fruit diet; and
Limited arable land and water- based diet resulting from continued income growth;
Berger said those global trends point to strong investment opportunities for the company in coming years.