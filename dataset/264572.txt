Drone company wants to launch at Casselton airport
Drone company wants to launch at Casselton airport

The Associated Press

CASSELTON, N.D.




CASSELTON, N.D. (AP) — An international defense and electronics company wants to launch a crop-surveying drone from the Casselton Regional Airport.
Airport Authority chairman Bob Miller tells KFGO the Israeli company, Elbit Systems, would gather crop information across the Upper Midwest that would be analyzed and sold to farmers.
Elbit wants to lease hanger space for the large drone it flies, and install equipment it needs to fly it. The FAA requires a chase plane to provide visual supervision when the drone flies. Lease costs are still being negotiated.
The company would have nine employees based in Casselton during the months the flights are made.
___
Information from: KFGO-AM, http://www.kfgo.com