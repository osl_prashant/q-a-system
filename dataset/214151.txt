Photo courtesy Chilean Blueberry Committee

The Chilean Blueberry Committee continues to play an important role in promoting sales of Chilean blueberries in the U.S. during the winter.
“Display promotions and demos continue to be popular with retailers, and we also partner with a number of retailers who like to include Chilean blueberries as part of a bigger Chilean fruit festival,” said Karen Brux, managing director of North America for the San Carlos, Calif.-based Chilean Fresh Fruit Association.
“We employ three merchandisers who manage our promotions throughout the U.S. and Canada, and we really try to get as creative as possible, drawing in shoppers and getting them excited about buying fresh Chilean blueberries during the winter months,” she said.
The committee helps retailers with display and sampling programs and now is working with Anaheim, Calif.-based Northgate Markets on the chain’s kids’ cooking program called Miguelitos Cocina Club.
Early this year, the Chilean Blueberry Committee helped sponsor kids classes in each of the chain’s 41 stores. Kids made blueberry yogurt swirls and received blueberry activity booklets.
The committee also posts numerous use ideas and short videos on social media patterned after those on Tasty, the online publication known for its viral recipe videos.
“We share these on our Fruits from Chile social media platforms, and also send them to retailers so they can share with their shoppers,” Brux said.
“We have also started partnering with both regional and national retailers on digital coupon programs that are consistently generating double-digit redemption rates,” she said.
The committee always is on the lookout for new, creative marketing programs, Brux said, and in early 2017 ran an online promotion with Canada’s main grocery delivery service, Instabuggy, along with a contest with a regional retailer that gave away tickets to Blue Rodeo, a popular Canadian band.
The committee also added a number of new use ideas to its portfolio of images and introduced a line of point-of-sale cards for retail.
The nutrition and flavor profile of blueberries makes them a perfect fit for foodservice, Brux said. But despite their popularity, there still is “limited knowledge about the superior quality of blueberries available during the winter months.”
The committee is attempting to change that by participating in a few key events each year to help raise awareness of Blueberries from Chile and highlight opportunities to incorporate Chilean blueberries into winter menus, she said.
The committee also has designed and produced its first foodservice booklet, highlighting opportunities for all Chilean fresh fruits, including blueberries.
In addition, the committee sponsored the first MISE expo in Atlanta in August that targeted about 100 executive chefs from hotels across the U.S.
As part of its commitment to stay abreast of the latest technological advancements that can help provide quality blueberries around the world, the Chilean Blueberry Committee holds, sponsors and attends numerous seminars and conferences that are relevant to the blueberry industry, Brux said.
Top berry researchers and specialists from around the globe will address the scientific and technological advances in the industry during the third annual Chilean Berry Congress Nov. 28-30 in Chile.
The Nov. 29 agenda will focus specifically on blueberries and will include a presentation from Bernadine Strik, professor of horticulture at Oregon State University, who will talk about Oregon’s experience with organic blueberries and on pruning management for young and adult orchards, she said.
Committee representatives also attended the International Blueberry Organization conference in China Sept. 10-12, Brux said.
The IBO is a global organization that brings together blueberry producers and marketers, affiliated businesses, social groups and governmental organizations to advance the health and sustainability of the blueberry industry.