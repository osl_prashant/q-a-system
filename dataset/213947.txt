Good morning!
Light position evening overnight... Corn futures are trading steady to fractionally lower after chopping narrowly on either side of unchanged overnight. Soybean futures are steady to a penny higher. Winter wheat futures are mixed, with the SRW market favoring the upside. Spring wheat futures are fractionally to 2 cents lower. The U.S. dollar index is just above unchanged.
Export sales report expectations... The report will be released at 7:30 a.m. CT.



Commodity

2017-18
			(MT)



Corn
800,000 to 1,200,000


Wheat
300,000 to 500,000


Soybeans
1,200,000 to 1,600,000


Soymeal
100,000 to 250,000


Soyoil
8,000 to 30,000



Delayed start to Australian wheat shipments... Australian wheat traders have just inked their first deal to export the 2017-18 wheat crop, emphasizing the country's weather struggles and stiff export competition this season. It will supply Vietnam with 30,000 MT of wheat. Typically, exporters begin selling Aussie wheat in July and traders already had around 3 MMT to 4 MMT on the books at this point last year, according to sources cited by Reuters. But they added that the recent drop in Aussie wheat prices along with rising needs from Asian mills should help Australia to secure more deals in the weeks ahead.
China sets import quotas for grains... China has set its 2018 low-tariff import quotas for corn, wheat and rice at levels basically in line with this year's quotas. They are set at the following levels, according to a statement from the National Development and Reform Commission:

Corn: 7.20 MMT
Wheat: 9.64 MMT
Rice: 5.32 MMT

These quotas are the subject of a World Trade Organization case brought by the U.S., who argues they limit its imports of grains and violate China's global trade obligations.
More crop estimates from Black Sea region... Ukraine's ag ministry today narrowed its estimate of the country's 2017 grain harvest to 62 MMT, versus a previous range of 61 MMT to 63 MMT. This would be down 4 MMT (6.1%) from last year's crop. Meanwhile, a USDA ag attache in Kazakhstan cut its 2017-18 wheat crop estimate for the country by nearly 1.5 MMT to 13.5 MMT, citing a reduction in planted area. But the post details that improvement in seed, weather and fertilizer application helped boost the quality of Kazakhstan's wheat crop. It expects the country to export 7.5 MMT of wheat in 2017-18, steady with the year prior.
NAFTA 2.0 update... Top Trump administration officials have reportedly told Canada and Mexico there is "no give" on the part of the U.S. when it comes to negotiations, but veteran trade policy observers say this is yet another of the Trump team's negotiating tactics. Meanwhile, USDA Secretary Perdue in Indianapolis yesterday revealed he met Tuesday with President Donald Trump, U.S. Trade Representative (USTR) Bob Lighthizer, Commerce Secretary Wilbur Ross and others for an update on NAFTA 2.0. Perdue said they believe "all three countries really want a deal at the end of the day." Like others, including Ross, Perdue said negotiations are still in the early stages and that most of the hard lifts in decisions will come in the waning days of talks.
Section 199 deduction a big tax reform focus for Congress and farm cooperatives. The National Council of Farmer Cooperatives (NCFC) opposes a GOP proposal to eliminate the Section 199 deduction as part of their tax reform effort. The current language includes a 9% tax break for most manufacturers. The majority of co-ops have been passing on the benefits directly to their farmer members, returning an estimated $2 billion annually to communities across the country, according to the industry trade group. "It's a popular deduction across a diverse range of business sectors, but I think many sectors have determined that if the corporate tax rate is reduced, that will more than compensate them for losing the Section 199 deduction," said Chuck Conner, president and CEO of NCFC, referring to the Republican's plan to reduce the corporate tax rate from 35% to 20% percent. "In the case of cooperatives, we would get little to no benefit."
Goodlatte ag guest-worker bill clears panel by just one vote... A bill pushed by Rep. Bob Goodlatte (R-Va.) that would create a new H-2C program that Republicans argue would allow agricultural employers to hire 10s of thousands of foreign laborers, but a move Democrats say would drive down wages to the detriment of American workers, barely advanced out of the House Judiciary Committee yesterday, 17-16. Even if the bill gets through the House, by no means a certainty, Senate Democrats would likely filibuster the measure because of major opposition to parts of the bill. After approving the guest-worker bill, the panel on a 20-10 party-line vote backed legislation by Rep. Lamar Smith (R-Texas) that would mandate employers use the E-verify electronic system to check a potential employee's legal status.
Trump says he is open to DACA deal, with a big caveat: funding for "the wall"... The president said he's willing to strike a deal to protect so-called "dreamers" covered under the Deferred Action for Childhood Arrivals (DACA) program, if the agreement includes border wall funding. The White House last month released a list of immigration reform principles, which included policy changes such as reducing family immigration and targeting sanctuary cities.
Hold put on Northey nomination... Republican Sens. Ted Cruz (R-Texas) and Mike Lee (R-Utah) on Wednesday put a hold on the nomination of Bill Northey to an undersecretary post. While there was a last-minute glitch on Northey's official title last week, his nomination was expected to be noncontroversial. According to a spokesman for Lee, the senator had lifted his hold as of Wednesday afternoon, but a Republican Senate aide said Cruz's hold was still on as of late in the afternoon. Some reports conjecture that the hold (which most think will be temporary) is a reaction to how the two Iowa GOP senators persuaded Trump to have Environmental Protection Agency Administrator Scott Pruitt back off from changes to the Renewable Fuel Standard program.
Brazil to accelerate imports of agrochemicals and fertilizers... Brazil's ag ministry has reached an agreement to use a system of the national revenue service to speed along imports of agrochemicals and fertilizers through Port of Santos, the country announced yesterday. It add that the system will eventually be available at the Port of Parangua and Rio de Janeiro, as well. The agreement is expected to benefit BASF SE, Bayer AG, Ihara Science Corp, Adama Agricultural Solutions and DuPont.
Futures signaling a higher bias toward cash market... Futures are signaling a higher bias toward this week's action, with showlists reportedly tighter this week and boxed beef values on the rise amid improved movement. But no sales occurred at the online Fed Cattle Exchange auction Wednesday and so far just 144 head have changed hands at $108 in the Iowa/Minnesota market, down $2 from the week prior. Other feedlots are holding out for better prices relative to last week's $110 to (mostly) $111 action.
Momentum still to the upside in lean hog futures... Cash hog bids strengthened on Wednesday and weight data for the Iowa/southern Minnesota market signals aggressive slaughter numbers are finally making a dent in supplies. The ongoing positive signals from the cash market have kept futures in an uptrend, but spring contracts have moved into overbought territory according to the nine-day Relative Strength Index.
Overnight demand news... Jordan purchased 50,000 MT of hard milling wheat from optional origins. It also issued another international tender to buy 100,000 MT of hard milling wheat from optional origins. Algeria purchased around 250,000 MT of durum wheat in a tender, with some saying the tally was actually closer to 300,000 MT. Taiwan bought around 65,000 MT of corn, likely from Argentina. The country also issued an international tender to buy 86,000 MT of milling wheat from the United States. Three South Korean buyers purchased a total of around 180,000 MT of soymeal in separate deals last week, likely from South America. Saudi Arabia tendered to buy 475,000 MT of hard wheat.
Today's reports:

7:30 a.m., Weekly Export Sales-- FAS
7:30 a.m., Drought Monitor -- USDA/NWS
2:00 p.m., Livestock Slaughter -- NASS