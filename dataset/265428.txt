AP-AR--Arkansas News Digest 1:30 pm, AR
AP-AR--Arkansas News Digest 1:30 pm, AR

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Arkansas. Questions about coverage plans are welcome and should be directed to the AP-Little Rock bureau at pebbles@ap.org or 800-715-7291.
Arkansas Supervisory Correspondent Kelly P. Kissel can be reached at kkissel@ap.org or 501-681-1269.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
................
GOVERNMENT AND POLITICS:
ARKANSAS PERSPECTIVE-ANALYSIS
LITTLE ROCK, Ark. — The Arkansas judge who put the brakes on the state's medical marijuana program didn't do so because of an objection to legalizing pot's use for certain patients. In fact, he was downright apologetic about putting the program on hold because of objections to the licensing process. By Andrew DeMillo. SENY: 790 words.
MEMBER FEATURES:
EXCHANGE-GANGSTER MUSEUM-BASEBALL
HOT SPRINGS, Ark. — A baseball gallery opening inside downtown Hot Springs' Gangster Museum of America should pique the interest of history buffs and fans of the sport as it reveals some Hot Springs history from long ago. The gallery's grand opening is scheduled for March 24, just in time for the inaugural Hot Springs Baseball Weekend, set for March 23-24. The baseball gallery is included in the admission to the Gangster Museum. By Lindsey Wells, Sentinel-Record. SENT IN ADVANCE: 629 words.
EXCHANGE-ARKANSAS-FERAL HOGS
HORATIO, Ark. — Feral hogs are everywhere in Arkansas and have become "non-native invaders." Extension services of Little River and Sevier counties provided a free Feral Hog Control Workshop March 15 in the Horatio Elementary School cafeteria in Arkansas. The workshop included discussion of natural history and damage identification, as well as rules and regulations regarding the hogs. The extension service also used a trail camera, feral hog surveillance and successful trapping demonstrations. By Jim Williamson, Texarkana Gazette. SENT IN ADVANCE: 628 words.
IN BRIEF:
— ARKANSAS-POWER LINE PROJECT: The U.S. Department of Energy has withdrawn support for a $2.5 billion power-line project through Arkansas opposed by landowners who feared they would be forced to sell their property against their will.
— HANGING JUDGE-STICKPIN STOLEN-ARKANSAS: A stickpin that belonged to the Old West's legendary "Hanging Judge" has been stolen from a museum in Arkansas.
— HOME INTRUDER-FATAL SHOOTING-ARKNASAS: Authorities in rural Arkansas say a homeowner has shot and killed a suspected intruder.
___
If you have stories of regional or statewide interest, please email them to pebbles@ap.org and follow up with a phone call to 800-715-7291.
If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867.
For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
The AP-Little Rock