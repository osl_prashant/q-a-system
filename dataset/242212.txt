Central Oregon agriculture seeks subscription support
Central Oregon agriculture seeks subscription support

By SUZANNE ROIGThe Bulletin
The Associated Press

BEND, Ore.




BEND, Ore. (AP) — Some people call them community-supported agriculture, but farmers call them lifelines to cash at a time when they're spending more than they're earning.
Each spring farmers shell out cash to get ready for planting season, but have to wait months before they see a cent of any return. Some get loans; others dip into savings, said Megan French, who co-owns Boundless Farmstead with David Kellner-Rode. And others reach out to the community of customers who are willing to shell out $400 to $600 to subscribe for 20 weeks' worth of vegetable, meat or flower delivery.
That's what community-supported agriculture is all about, French said. Consumers invest money upfront and the farmer promises to deliver locally grown vegetables to the consumer from a neighborhood farmer.
Friday was the day the industry tried to encourage the community to pony up for community-supported agriculture, a day where more consumers buy in their share of farm products, and farmers like French are hoping consumers will sign up for a subscription.
At Boundless Farmstead, a 20-acre farm in Alfalfa, she hopes to sell 25 contracts to raise the funds she needs to buy equipment and seeds for spring planting. So far she's garnered about 17 from people who will receive a box of produce weekly from June 7 through Oct. 18.
"We give you your share of food for a week," French said. "It forms a community. Your food is picked hours before you take it home, compared to an unknown amount of time in a grocery store."
There is little risk to the consumer. Farmers have always come through with the delivery of produce, said Elizabeth Weigand, owner of Agricultural Connections, a Bend subscription service for veggies that come from several farms.
"It's a closed loop. The money stays in the community," Weigand said. "We're all vested in the community. Community supported agriculture builds economic robustness."
Nationwide about 305 farms function as community-supported agricultural operations, according to Small Farm Central & Member Assembler, a web-based hub for these operations.
Central Oregon is home to approximately 2,300 farms. They are not big money-makers, according to a U.S. Department of Agriculture census report. Most are under 50 acres, and 70 percent earned less than $10,000 in gross sales in 2012, the year of the most recent Census of Agriculture by the USDA. Sixteen percent sell directly to consumers at farmers markets or community-supported agriculture programs, for example, compared to 19 percent of farms statewide.
Community-supported agriculture is not just for individuals, but also for businesses, said Phil Lipton, owner of the French Market restaurant in Bend. Using locally sourced food enables the restaurant to capture a fresh taste, Lipton said.
"I hope to see it get more developed here," Lipton said. "As more people become aware of the need for fresh produce, they will ask for more of it to be grown here and that will support agriculture."
That's where the connections come in, French said. As a farmer, she gets to meet her customers and suggest recipes for specific foods that she grows. It's a connection she embraces because she values farming. Her farm grows 50 different varieties of vegetables.
"Community-supported agriculture allows us to share the risk and share the success," French said. "It is also about eating closer to the land in the place you're at."
Clare Sullivan, a Deschutes County extension agent, said the connections made between farmer and consumer are the most important reasons for buying into a subscription service. Each of the estimated dozen farms that offer a subscription service in the county run their programs differently, Sullivan said. Some farms offer pickup at a location in town, others directly from the farm and still others work with a middle man like Agricultural Connections that sells weekly or entire seasons worth of produce.
"Buying directly from the farmer you realize what is possible here," Sullivan said. "In central Oregon it's a harsh growing climate. It's nice to know who you're supporting.
"It's morphed over the years, but it's really about health and a generational shift, wanting to know where your food comes from."
Bend resident Stan Gofinch believes it's important for his health and for the health of his community to know where his food comes from and how it is raised. This is his second year in a row purchasing a subscription for his vegetables.
"I know who grows my food and where it's grown," Gofinch said. "There's a face behind my food. It's no more expensive than going to a specialty market, and everything is fresh. It's sustainable."
___
Information from: The Bulletin, http://www.bendbulletin.com