Last week the House Agriculture Committee released their version of the 2018 Farm Bill (HR2). What do you think it costs? According to estimates from the Congressional Budget Office (CBO), total farm bill spending for all titles from 2018 to 2028 would be $867 billion.
The CBO analysis shows HR2 would increase mandatory spending on programs by $500 million over 10 years and would result in $7 million in deficit savings for the same period. However, overall mandatory spending would increase by $3.2 billion between fiscal years 2019-2023, the life of the legislation, Pro Farmer’s Jim Wiesemeyer points out.
“Costs would drop sharply over the next five years by $2.7 billion,” he says.
 
What are some money savers in the bill?
Conservation. According to Wiesemeyer, the bill would eliminate the Conservation Stewardship Program (CSP) to save $12.6 billion over 10 years, of which $7.7 billion would go toward expanding the Environmental Quality Incentives Program (EQIP). “While most of the rest of the CSP savings would be kept in the conservation title for other programs, $795 million would be moved into other sections of the bill,” he says. “The phase-out of CSP would save $12.6 billion over a decade, while spending in EQIP would rise to nearly $7.7 billion.”
Infrastructure. Spending on rural infrastructure and economic development would decline by $517 million, Wiesemeyer says.
Crop insurance. According to Wiesemeyer, crop insurance funding slides by $161 million under HR2.
 
What are the spending increases? 
Horticulture. Funding would increase by $10 million.
Farm Programs. Spending on farm programs would increase by $193 million.
Extension. Extension programs gain $250 million under the bill.
Trade. The house bill boosts trade promotion programs by $450 million.
SNAP. Funding for the Supplemental Nutrition Assistance Program increases by $436 million under HR2. However, Wiesemeyer says it actually saves money in the long run. “Tightened eligibility rules for SNAP helped garner $1.2 billion to match supermarket incentives for SNAP recipients to buy fruits, vegetables and dairy products,” he says. For nutrition: $463 million in direct spending is offset by $465 million in revenue. Net impact: the nutrition Title 4 saves $2 million over 10 years.”
Miscellaneous. A variety of programs benefit from a $566 million funding increase.
When the rubber meets the road, Wiesemeyer says the House farm bill is budget neutral, including for nutrition. In fact, overall, the farm bill proposals save $7 million over 10 years.