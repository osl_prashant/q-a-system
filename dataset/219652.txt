The chemicals, tank mix and ground you run across affect how nozzles wear and their propensity to clog. Regular evaluations should help your application team catch any major issues.
“Where you could run into trouble is with dry powder AMS, which really eats things up quickly—it’s like running gritty sand through sprayers and spray tips,” says Steve Fischer, inside sales rep and precision expert at Fertilizer Dealer Supply. “You can’t use AMS with dicamba, but you use it with Roundup. Atrazine and other gummy chemicals can cause issues with clogging and getting stuck in booms periodically.”
Clogs are easy to recognize, you’ll visually see them blocking spray.
With rate-control systems you could still have an accurate rate displayed across sections, but individual nozzles could be off.
“Rate controllers measure flow rate right before the boom splits to the nozzles—so even if a nozzle is plugged or worn, the sprayer still sees the right gallon per acre,” says Bob Wolf, owner of Wolf Consulting and Research LLC. “We did a test with six blocked nozzles on a sprayer, and the rate control still thought it was right when in some cases nozzles were putting out twice as much while plugged nozzles put out zero.”
Take time to check for wear. “Check for wear that affects the spray pattern because streaks and nonuniform coverage could make the application ineffective,” says Will Smart, president of Greenleaf technology.
Incorrect application rates add up. As little as 15% overspray could result in $182,000 in extra cost, according to Spraying Systems Company. Their calculations are for a system with a flow of 100 gal. per minute, water cost at $2.75 per 1,000 gal. and chemical cost of $1 per gallon diluted 10:1 on a sprayer operating 2,080 hours annually.
“You want to check spray tips every season at a minimum,” says Tim Stuenkel, Tee Jet global marketing manager. “A midseason check could be beneficial, too.”
Stuenkel shares these five steps to check tips:

Use water to run the check.
Bring the boom up to typical operating pressure.
Based on the capacity of the tip, have a container marked in ounces to collect water from the tip.
Collect water for 30 to 60 seconds.
Check to see if the flow rate is within range of the manufacturer’s recommendations, which is found in catalogs.

[Click here for 4 steps to ensure spray success and learn more about spray patterns]
Equipment manufacturers also offer calibration tools to check nozzle flow rates, as an alternative.
“Be sure to check multiple tips along the boom for differences in wear,” Stuenkel adds. “Replace any nozzle when it is 10% over nominal flow rate.”
Even if flow rate is within range, you’ll still need to observe the spray pattern because wear can degrade the pattern from what you need.
“Most of the time it changes your flow rate, but there is also a chance it’ll affect droplet size and pattern—but that’s often not as big of a change,” says Craig Bartel, Wilger sales manager.