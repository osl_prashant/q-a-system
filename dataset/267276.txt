On the brink: US and China threaten tariffs as fears rise
On the brink: US and China threaten tariffs as fears rise

By PAUL WISEMANAP Economics Writer
The Associated Press

WASHINGTON




WASHINGTON (AP) — The world's two biggest economies stand at the edge of the most perilous trade conflict since World War II. Yet there's still time to pull back from the brink.
Financial markets bounced up and down Wednesday over the brewing U.S.-China trade war after Beijing and Washington proposed tariffs on $50 billion worth of each other's products in a battle over the aggressive tactics China employs to develop its high-tech industries.
"The risks of escalation are clear," Adam Slater, global economist at Oxford Economics, wrote in a research note. "Threats to the U.S.-China relationship are the most dangerous for global growth."
There's time for the two countries to resolve the dispute through negotiations in the coming weeks. The United States will not tax 1,300 Chinese imports — from hearing aids to flamethrowers — until it has spent weeks collecting public comments. It's likely to get an earful from American farmers and businesses that want to avoid a trade war at all costs.
Also, China did not say when it would impose tariffs on 106 U.S. products, including soybeans and small aircraft, and it announced it is challenging America's import duties at the World Trade Organization.
Lawrence Kudlow, the top White House economic adviser, sought to ease fears of a deepening trade conflict with China, telling reporters that the tariffs the U.S. announced Tuesday are "potentially" just a negotiating ploy.
"We're very lucky that we have the best negotiator at the table in the president, and we're going to go through that process," said White House press secretary Sarah Huckabee Sanders. "It will be a couple months before tariffs on either side would go into effect and be implemented, and we're hopeful that China will do the right thing."
The prospect of a negotiated end to the dispute calmed nerves on Wall Street. After plunging in early trading, the Dow Jones industrial average ended up rising 231 points, or nearly 1 percent, to 24,264.
The sanctions standoff started last month when the United States slapped tariffs on imported steel and aluminum. On Monday, China countered by announcing tariffs on $3 billion worth of U.S. products. The next day, the United States proposed the $50 billion in duties on Chinese imports, and Beijing lashed back within hours with a threat of further tariffs of its own.
Things could easily escalate. The U.S. Treasury is working on plans to restrict Chinese technology investments in the United States. And there's talk that the U.S. could also put limits on visas for Chinese who want to visit or study in this country.
For its part, China conspicuously left large aircraft off its sanctions list Wednesday, suggesting it is reserving the option to target Boeing if relations deteriorate further.
Douglas Irwin, a Dartmouth College economist who has just written a history of U.S. trade policy, said the tit-for-tat tariffs are shaping up as the biggest trade battle since World War II.
"It's huge," he said.
In 1987, the Reagan administration triggered shockwaves by slapping tariffs on just $300 million worth of Japanese imports — that's million with an "m'' — in a dispute over the semiconductor industry. Those tariffs covered less than 1 percent of Japanese imports at the time.
The tariffs the U.S. unveiled Tuesday apply to nearly 10 percent of Chinese goods imports of $506 billion.
And during the dispute three decades ago, Japan, a close U.S. ally, chose not to retaliate. It eventually gave in to U.S. demands.
"What we've seen with China is very different," Irwin said. "When the steel tariffs went in — boom, they came back with retaliation. ... They were not going to take it lying down."
Making matters trickier, the dispute over Chinese technology policy strikes at the heart of Beijing's ambitions to become the global leader in cutting-edge technologies like artificial intelligence and quantum computing.
In August, President Donald Trump ordered the Office of the U.S. Trade Representative to investigate China's tech policies, particularly longstanding allegations that it coerces U.S. companies into handing over sensitive technology to gain access to the Chinese market. The tariffs proposed Tuesday were the result of that investigation.
The U.S. also accuses China of treating U.S. companies unfairly when they try to do business there and of encouraging Chinese hackers to break into U.S. corporate computer systems and steal trade secrets.
The Trump administration is coming under intense pressure to de-escalate the dispute. American farmers, who disproportionately supported Trump in the 2016 election, are especially outspoken in seeking trade peace. After all, China buys nearly 60 percent of American soybean exports.
"American farmers are waking up this morning to the prospect of a 25 percent tax on exports that help sustain their farming operations," said former U.S. Sen. Max Baucus, co-chair of Farmers for Free Trade. "We urge the administration to reconsider escalating this trade war."
Some analysts predict Beijing will ultimately yield to U.S. demands because it relies far more heavily on the U.S. market than American businesses rely on China's.
"It is no coincidence that the Chinese have not yet set the date when these new tariffs will become effective," said Raoul Leering, head of international trade analysis at ING. "It is likely that China will, in the end, cut its losses and be willing to give Trump something."
But Robert Holleyman, a former U.S. trade official, said he worries that both sides will impose tariffs — and they will stick indefinitely.
"That would hit American consumers in the pocketbook and would reduce access by American businesses and farmers to the largest market in the world," he said.
And the Eurasia Group consulting firm warned in a research note about fear that the dispute "could spiral dangerously out of control, given that this trade action is really less about trade and more about China's rise as a technology leader."
___
AP writers Zeke Miller and Darlene Superville in Washington contributed to this report.
___
Animated explainer on trade disputes:

                https://youtu.be/qWF5DF_XQYk
              
____
On Twitter follow Wiseman at https://twitter.com/paulwisemanAP