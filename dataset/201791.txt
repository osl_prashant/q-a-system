The World Trade Organization (WTO) recently issued a decision that could provide further momentum for U.S. beef exports to Indonesia. The ruling addressed a complaint filed by the Office of the U.S. Trade Representative (USTR) and its counterpart agency in New Zealand, which contended that Indonesia's import restrictions on a range of products - including beef - violate its obligations as a WTO member.As this complaint was making its way through the WTO process, the Indonesian government eased several of its restrictive measures on imported beef, which resulted in increased trade in 2016. Through November, U.S. exports to Indonesia reached $33 million - a new single-year record. But according toJoel Haggard, U.S. Meat Export Federation (USMEF) senior vice president for the Asia-Pacific, the WTO ruling is still important because it may help address the volatile and unpredictable nature of Indonesia's import policies. Haggard says this would increase U.S. exporters' level of interest in the market and allow Indonesian importers to secure the products most in demand.


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.