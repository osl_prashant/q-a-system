Carrier Transicold plans to offer a new transport refrigeration system.
The Athens, Ga.-based Carrier Transicold is working to release a telematics system that is designed to deliver advanced capabilities for transport refrigeration units, according to a news release.
The system is being developed to help fleets manage their refrigerated assets by enabling remote refrigeration unit monitoring, control and diagnostics, data management and other capabilities, according to the release.
With field trials nearing completion, the company expects to fully release the system in North America in 2017, followed by introductions in Europe and other regions, according to the release.
The system can be factory-installed and will be supported by Carrier Transicold's dealer network.
The program's two-way wireless communications system was developed for truck and trailer units, David Appel, Carrier Transicold's president, said in the release.
"Adoption of telematics by the transport industry has evolved rapidly and is now an important tool for fleet management," he said in the release. "We launched this initiative in response to customer demand for an original equipment manufacturer-provided system for TRUs (transport refrigeration units)."
Orbcomm Inc. developed the system.
Orbcomm's experience with remote monitoring complements Carrier Transicold's transport refrigeration expertise, according to the release.
Carrier Transicold is a part of the Farmington, Conn.-based UTC Climate, Controls & Security, a unit of United Technologies Corp. in Hartford, Conn.