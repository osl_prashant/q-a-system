Brexit pushes European dairy giant to make cuts
Brexit pushes European dairy giant to make cuts

The Associated Press

COPENHAGEN, Denmark




COPENHAGEN, Denmark (AP) — A major European dairy cooperative says that currency swings caused by Brexit are among the reasons it has to cut costs by over 400 million euros ($495 million) over the next three years.
Denmark-based Arla Foods, the maker of Lurpak butter, says "two unexpected developments" — the pound's drop after Brexit and a shift in commodity prices — are forcing it to act.
CEO Peder Tuborgh says the savings program is "to the benefit of its farmer-owners and further strengthen the company's investment capability."
Tuborgh said savings include changing work routines, trimming bureaucracy and cutting costs.
The cooperative is owned by farmers in Denmark, Sweden, Britain, Germany, the Netherlands and Belgium. It employs some 19,000 people.