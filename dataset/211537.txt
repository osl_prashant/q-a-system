Veggie Noodle Co. expects production at its new facility to be in full swing in early October.In that building, the Austin, Texas-based company will produce items like cauliflower and broccoli rice along with its signature noodles, according to a news release.
The facility is more than 40,000 square feet and gives Veggie Noodle Co. more than five times its current production capacity.
An investment by San Francisco-based private equity firm Encore Consumer Capital provided funding for the construction of the facility.
A grand opening will be Oct. 3, with a ribbon-cutting ceremony using super-long vegetable noodles, according to the release.
The company’s products can be found in Whole Foods, Kroger, Meijer, Sprouts, Target, Safeway and other retailers.