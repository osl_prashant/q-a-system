Dole Food Co. has augmented its chopped salad line with one of the country’s most popular flavors: Caesar.
“In response to consumer demand, we recently introduced a new salad kit that brings together two of the most popular salad kit combinations — Caesar and chopped,” said Bil Goldfield, director of corporate communications for the Westlake Village, Calif.-based company.
“Caesar remains America’s No. 1 salad kit flavor, and Dole Chopped Salad Kits continue to have strong acceptance into household meal occasions.”      
As with all Dole Chopped Salad kits, he said, the latest varieties were developed leveraging years of consumer research into chopped salad attitudes, flavor preferences and other trends.
“The new (stock-keeping units) deliver exactly what consumers say they want in a chopped salad: an on-trend, taste-first experience featuring layers of flavor, texture, color and crunch in every bite,” Goldfield said.
The kit includes crispy quinoa, crumbled garlic bread crouton crumbles, ground black pepper, shredded parmesan cheese, bite-sized romaine and Dole’s Signature Caesar dressing, according to a news release.
The kit was a United Fresh 2017 Fresh Innovation Award finalist in the Best New Vegetable Product category.
Pacific International Marketing Inc. offers consumers a spring mix.
“We’re staying busy addressing some of the new configurations our customers are asking for,” said Henry Dill, sales manager for the Salinas, Calif.-based company.
“Spring mix is an item that people are using, and it is a stable commodity, especially in foodservice. The big box stores sell 1-pound tubs of it.”