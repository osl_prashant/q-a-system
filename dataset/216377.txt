On Tuesday, the House of Representatives passed the tax reform bill. Early Wednesday morning, the Senate also passed the bill. The House will have to vote on the bill again on Wednesday because of the Byrd Rule which limits the kind of provisions that can be included in a bill via reconciliation. Long story short, it’s widely expected Congress will pass tax reform, and President Trump promised to sign the bill in time for Christmas. What does all of that mean to you?
Here’s a quick list of things Paul Neiffer, the Farm CPA says you should know about tax reform:


 The corporate tax rate of 21% down from 35% but most farmers only pay 15% so this is a 40% increase


You can fully deduct all farm assets purchased between September 28, 2017 and December 31, 2022


Section 179 is set at $1 million


Reduction in overall tax rates by 5-10%


Almost an automatic 20% deduction for net farm income. (Perhaps including self-rental, we are not sure on this yet.)


Doubling of lifetime estate tax exemption to $11.2 million (2018)


Almost all farmers should be able to deduct all interest expense


Net Operating Losses can only be carried back 2 years and can only offset 80% of income going forward.


Meals are only 50% deductible for farmers who provide meals to employees on-site


No more  Domestic Production Activities Deduction (DPAD) deduction, but 20% deduction is twice as much anyway


Section 1031 exchanges applicable to real-estate only, but 100% bonus wipes out gain anyway (other than state issues)


As you can see there are likely some wins and losses for your tax bill. We recommend visiting with your accountant to determine how these changes will specifically impact your business.