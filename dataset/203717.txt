While talk about moving the merchants in the Hunts Point Terminal Market into new and modern facilities continues, the wholesalers are expressing doubts that construction of such a facility could ever happen.
"A new market? It's never going to happen," said Ciro Porricelli, partner with Jerry Porricelli Produce.
Porricelli points to the 2014 agreement signed by market officials and the city's Economic Development Corporation which renewed the wholesale market's lease for seven years.
He also pointed out improvements made by large wholesalers including S. Katzman Produce Inc. and E. Armata Inc.

"They're not wasting it and when you see guys spending that kind of money, you know (the market) won't be going anywhere."
Because the market operation is located near waterfront property, Porricelli said one would think the city might want the market to relocate.
The historic Hunts Point neighborhood location, however, proves advantageous as the facility is close to the metropolitan region's boroughs, he said.
"There's always talk about moving the market or making a new market, but eventually, if the congressmen and all those people don't get involved, it will never be a real movement," said Alfie Badalamenti, vice president of Coosemans New York Inc. "If it's just the mayor talking about it, it won't help. Others need to get involved."
Badalamenti pointed to the longtime market discussion.
"How many years have we been talking about a new market in New York?" he asked. "It's been 10 years. Eventually, the market needs help."
E. Armata Inc. operates from 24.5 market units and has made large investments into its market presence, said Paul Armata, vice president.
The company wants to do the best it can in the market's existing facility, he said.
"This is vital for the business that we're in," Armata said. "A new market? That's a big question. Location-wise, it should stay here. How would you get all of these merchants into the new market over time or at the same time? I don't foresee any change."
Nathel & Nathel Inc. has also upgraded its market operations.
"There's been a lot of talk about this market moving or rebuilding," said Sheldon Nathel, vice president. "This has been going on for 13-14 years. A lot of the people on the market seem to be putting a lot more money into their stores lately. We followed suit. Who knows where this market will be in five years?"
Jeff Young, a buyer for A&J Produce Corp., said market leaders remain committed to improving facilities.
"They are actively pursuing trying to get money from the federal government to upgrade this high-volume food supply area," he said. "With 22 million people in the Tri-State area and being the biggest market, it should be a priority."
Because of the tonnage that runs through the center and the merchandise that benefits the shippers, wholesalers and consumers, the market is trying to keep the facilities intact as much as possible, Young said.
Mike Cochran, sales manager and vice president of Robert T. Cochran & Co. Inc., said he hasn't heard much about a proposed facility.
"Everything here is antiquated and stuff breaks left and right," he said. "Electrical lines are exploding and transformers are breaking. We don't have the capacity.
"This market is pretty important to the people that do business here. Many make a living out of this market, which feeds quite a big portion of the Tri-State area and some beyond."
One of those businesses is Poughkeepsie, N.Y.-based My Brother Bobby's Salsa, a fresh salsa manufacturer.
The company has been in business since 1993 but is a relatively new customer to the market.
"It took me a couple of years before I figured out that the best way for me to stay viable is for me to buy my own produce where I can select from a variety of sources on the market," said Bobby Gropper, owner. "This market is critical to my success."
Market co-chairman Joel Fierman, president of Fierman Produce Exchange Inc., and Joe Palumbo, CEO of Top Banana LLC, didn't return calls from The Packer and Myra Gordon, executive administrative director of the Hunts Point Terminal Produce Cooperative Association Inc., declined an interview.