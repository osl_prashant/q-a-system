BC-Wheat KX
BC-Wheat KX

The Associated Press

CHICAGO




CHICAGO (AP) — Winter Wheat futures on the Chicago Board of Trade Wed:
OpenHighLowSettleChg.WHEAT5,000 bu minimum; cents per bushelMar489512¼489508¼+19¼May504¾528¾503¾522¼+17½Jul523¼545¼521¼539+16½Sep540559538¼554¼+15Dec558577557¾573+14Mar568585¾568582+13¼May583½+12¼Jul569¾582¼569¾579+10¼Sep580585580585+9Dec590600590598¾+9¼Mar598¾+9¼May598¾+9¼Jul570579570579+9¼Est. sales 118,813.  Tue.'s sales 87,058Tue.'s open int 301,350