By: Warren Rusche, SDSU Extension Beef Feedlot Management Associate
In an ideal world, every bale was harvested perfectly resulting in large quantities of bright green, high-quality hay. Designing cattle diets can be easy under those conditions. Unfortunately, conditions are far from ideal in much of the Dakotas this year. Hay will be short and producers will be forced to use some feedstuffs that may be unconventional or less than ideal. So how do we make use of those feeds?
Fixing Problems Using Supplements
The good news is that some quality issues can be fixed with proper supplementation. Ruminants have the unique ability to make use of relatively poor-quality feeds, as long as we provide the right supplements for the rumen microbes. The key is knowing the kind and amount of supplementation required.
To illustrate the point, Table 1 shows the amount of feed required for drylotted 800 pound yearling heifers gaining 1.3 pounds using poor, average, or high-quality grass hay, plus supplements.
Table 1. Heifer diets using three different kinds of grass hay.



Hay Quality:
Poor
Average
High


NEg, Mcal
20
24
26


CP, %
6.5
8.5
13


Hay, lbs
20
22
23


DDGS, lbs
4
2.25
--


Corn, lbs
--
--
1


ADG, lbs
1.3
1.3
1.3


Feed Cost per day, $1
$1.19
$1.21
$1.20


1Assumes hay at $100 per ton, DDGS at $96 per ton, and corn at $3.00 per bushel.
			 



Hay Quality & Supplements Needed
The expected performance and costs per day are relatively similar between the three kinds of hay. However, the supplements required to achieve those results are quite different. Poor quality hay requires almost twice the DDGS as average hay, while the higher protein hay achieved the same performance target with only a small quantity of corn grain.
Relying on book values can be extremely risky, especially during challenging growing conditions. Plants under drought stress mature more quickly than normal, resulting in more rapid declines in hay quality. The same scenario can play out when salvaging a small grain crop for forage. As the plant begins to produce heads, the quality and feeding value of the feed decreases rapidly. The only way to know exactly what you have is to collect a representative sample and have it analyzed. View Forage Hay Sampling Method on YouTube for tips on proper forage sampling.
Problems That Limit (or Prevent) Feed Usefulness
Some issues simply can’t be solved with a supplement. Feeds can contain harmful compounds or other issues that either limit the amount that can be used or in extreme cases prevent the feed from being used at all.
Nitrate Concerns
Nitrate concerns immediately come to mind during drought. Salvaged small grain crops harvested for hay are notorious for accumulating nitrates, but weeds such as kochia or pigweed can as well. The usefulness of feeds containing nitrates depends upon the concentration of nitrates. The only way to know for certain is to have these feeds tested. Producers can find more in-depth information on making use of feeds that contain nitrates in Nitrate Poisoning of Livestock: Causes & Prevention.
Other Problems
Other problems arise from arise from where the crop grew or how it was harvested. Ditch hay can be a useful feedstuff, but can contain a great deal of foreign material. This could cause problems with hardware disease in cattle. Prior pesticide applications can also limit the usefulness of a feed. Ash content is often overlooked. Researchers in North Dakota reported ash content in ditch hay samples as high as 37%. This represents an extreme case but illustrates that conditions along the road side can affect the usefulness of the feed.
The Bottom Line
Feed does not have to be perfect to be useful. The key to making smart feeding decisions is knowing what the imperfections are and adjusting accordingly.