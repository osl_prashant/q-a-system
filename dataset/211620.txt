Los Alamitos, Calif.-based Frieda’s Inc. wants retailers to get into the spirit of fall by showcasing “spooky foods” in their autumnal produce displays. 
“Halloween is the perfect time to showcase your tropical fruits and specialty vegetables as shoppers will be looking for unique, spooky items for their Halloween celebrations,” Alex Jackson Berkley, Frieda’s senior account manager, said in a news release. 
 
This year Frieda’s is promoting jackfruit, organic finger limes, colored cauliflower and ghost peppers as #spookyfoods, along with Buddha’s hand citron, horned melons, dragon fruit, rambutan, red cactus pears and blood oranges, according to the release.
 
“Build out your ‘spooky foods’ displays with creepy-looking fruits and vegetables on both dry and refrigerated cases, and have fun with it,” Jackson said in the release.
 
“Why should the center aisles have all the Halloween fun?”