Salmonella in chicken salad kills 1, sickens 265 in 8 states
Salmonella in chicken salad kills 1, sickens 265 in 8 states

By DAVID PITTAssociated Press
The Associated Press

DES MOINES, Iowa




DES MOINES, Iowa (AP) — Chicken salad made by an Iowa food processing company and distributed by Fareway Stores in the Midwest sickened 265 people in eight states and caused one death in Iowa from salmonella contamination, the U.S. Centers for Disease Control and Prevention said.
The outbreak which sickened people from Jan. 8 through March 20, appears to have ended, the CDC said in an update posted on Friday. Ill people ranged in age from less than 1 year to 89 years. Sixty-seven percent were female.
The contaminated food left some people with serious illnesses, said Seattle food safety attorney Bill Marler. He and Iowa attorney Steve Wandro represent 48 people filing lawsuits against Triple T Specialty Meats of Ackley, Iowa, which made the chicken salad and Fareway Stores, a Boone, Iowa-based grocery chain that distributed the food under its store brand name.
Out of the 265 illnesses, 94 people were hospitalized, a high number for such an outbreak Marler said.
"There's a woman in Nebraska who was in ICU for a week and a gentleman in Iowa who has lost his large intestine. He's now facing another surgery in a month or two to try to reconnect that. So there are obviously some pretty significantly sick people," Marler said.
Wandro said several of those sickened are in their 70s or 80s with weakened immune systems who have had to go to skilled care facilities for several weeks to recover.
Triple T Specialty Meats spokesman Dave Dutton said the company isn't commenting.
"We're still receiving information and we want to make sure we have all that information before we make any public comments," said Dutton, lawyer from Waterloo, Iowa.
Asked if the company has determined the source of the salmonella contamination he said, "that's the job of testing companies and we haven't seen all the reports as yet."
Salmonella infection usually occurs when a person eats food contaminated with the feces of animals or humans carrying the bacteria.
Fareway did not immediately respond to a request for comment but says on its website that it did not produce the chicken salad and said "we believe that this incident is not the result of anything done by Fareway."
Lawsuits already on file in federal court in Des Moines allege product liability violations for distributing defective food and negligence for failure to ensure the food distributed was safe to eat. They seek money to compensate the victims for "general, special, incidental and consequential damages incurred" and attorney fees.
The illnesses hit Iowa hardest with 240 sickened and one dead, the CDC said. Illinois reported 10 cases, Nebraska had five, Minnesota had four and South Dakota had three illnesses. Indiana, Mississippi and Wisconsin each reported one case linked to the chicken salad.
Iowa Department of Public Health spokeswoman Polly Carver-Kimm said a woman in eastern Iowa died from the same strain of salmonella identified in the chicken salad outbreak but she stopped short of attributing it to the chicken salad since the woman wasn't interviewed about what she ate before she died.
CDC spokeswoman Brittany Behm confirmed Monday the Iowa woman died of the outbreak strain which meets the agency's criteria for inclusion in the outbreak statistics.
On Feb. 9, Fareway stopped selling chicken salad in all of its stores after the Iowa Department of Inspections and Appeals contacted the company about illnesses. Iowa public health officials issued a consumer advisory on Feb. 13 warning that chicken salad sold at Fareway may be contaminated with Salmonella. Eight days later Triple T Specialty Meats recalled more than 20,000 pounds of chicken salad after samples from two Fareway grocery stores in Iowa tested positive for the same strain of Salmonella that was sickening people.
___
Follow David Pitt on Twitter: https://twitter.com/davepitt