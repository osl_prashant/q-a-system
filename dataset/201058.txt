Below we plug USDA's weekly crop condition ratings into our weighted (by production) Pro Farmer Crop Condition Index (CCI; 0 to 500 point scale). It shows the condition of the spring wheat crop declined 9.53 points to 317.36. The reading is now 65.23 points lower than year-ago.
The condition of the North Dakota crop declined by 4.57 points from last week and is 28.49 points lower than year-ago.





Pro Farmer Crop Condition Index






Spring wheat




This Week




Last Week




Year-Ago





Idaho (5.74%)*


21.00


21.98


23.68




Minnesota (12.99%)


53.52


53.91


46.04




Montana (14.95%)


41.12


43.06


63.91




N. Dakota (50.80%)


160.01


164.58


188.50




S. Dakota (10.36%)


23.10


24.44


39.38




Washington (4.22%)


15.61


15.82


17.19




Spring wheat total


317.36


326.89


382.59




* denotes percentage of total national spring wheat crop production.
Following are details from USDA's National Agricultural Statistics Service (NASS) state crop and weather reports:
North Dakota: For the week ending June 18, 2017, temperatures averaged one to three degrees above normal, according to the USDA's National Agricultural Statistics Service. The eastern half of the State received one to two inches of rain, which was much needed. The western half of North Dakota was still experiencing dry conditions. There were 4.9 days suitable for fieldwork. Topsoil moisture supplies rated 18% very short, 25% short, 51% adequate, and 6% surplus. Subsoil moisture supplies rated 12% very short, 26% short, 57% adequate and 5% surplus.
Spring wheat condition rated 6% very poor, 18% poor, 34% fair, 39% good, and 3% excellent. Spring wheat jointed was 67% percent, behind 78% last year, but ahead of 57% average. Headed was 9%, behind 22% last year and 14% average.
Montana: Despite the state receiving precipitation in all regions there was no relief for some eastern parts of the state, and windy conditions negated much of the moisture gained according to the Mountain Regional Field Office of the National Agricultural Statistics Service, USDA. Storms continued to slow fieldwork in western areas of the state, while in eastern Montana some operators began selling off livestock due to poor range conditions. Dry conditions continue to be a challenge in eastern Montana with Jordan at only 22% of normal precipitation since April 1, 2017; and the highest temperature of the week reported in Nashua at 86 degrees. North Central areas of the state had the highest precipitation with some areas receiving as much as 2.11 inches of moisture.
Minnesota: Despite thunderstorms and rainfall throughout the state, Minnesota farmers were able to find 3.8 days suitable for field work during the week ending June 18, 2017 according to USDA's NASS. Early week rainfall paired with warm, dry conditions towards the end of the week allowed for overall crop advancement. Pea harvest for canning has begun. Field activities for the week included planting, spraying, and cutting hay.
Topsoil moisture supplies rated 1% very short, 7% short, 84% adequate and 8% surplus. Subsoil moisture supplies rated 5% short, 86% adequate and 9% surplus.
Spring wheat increased to 71 percent jointed or beyond, bringing it two days ahead of the five year average. However, spring wheat that had reached the heading stage still fell 2 days behind average at 22 percent. Spring wheat condition declined slightly to a rating of 89 percent good to excellent.