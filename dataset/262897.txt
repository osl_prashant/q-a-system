USDA: Ag disaster from drought in 4 states
USDA: Ag disaster from drought in 4 states

The Associated Press

NEW ORLEANS




NEW ORLEANS (AP) — The U.S. Department of Agriculture has declared an agriculture disaster because of recent drought in 25 Louisiana parishes , three Mississippi counties , 61 counties in Arkansas and 60 in Texas .
Farmers and ranchers in those counties — and in adjacent counties in those and other states — can qualify for low-interest loans if they can prove losses from the drought.
Louisiana parishes declared primary disaster areas are Bienville, Bossier, Caddo, Caldwell, Catahoula, Claiborne, DeSoto, East Carroll, Franklin, Grant, Jackson, LaSalle, Lincoln, Madison, Morehouse, Natchitoches, Ouachita, Red River, Richland, Sabine, Tensas, Union, Webster, West Carroll and Winn.
In Mississippi, Issaquena, Sharkey and Warren counties are primary disaster areas.