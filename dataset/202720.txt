Instead of "Fruits and Veggies - More Matters," how about a more direct approach? "Eat fruits and veggies or die" perhaps?
 
That may not pass muster as a generic industry media message to encourage consumption but the message isn't that far off. 
 
After all, a recent study by the Imperial College in London makes the case that increased fruit and vegetable intake influences mortality. 
 
Called "Fruit and vegetable intake and the risk of cardiovascular disease, total cancer and all-cause mortality–a systematic review and dose-response meta-analysis of prospective studies," the study was summarized by a news release from the college.
 
The release said the UK research showed that fruit and vegetable intake above five-a-day shows major benefit in reducing the chance of heart attack, stroke, cancer and early death. The study evaluated 95 studies on fruit and vegetable intake.
 
 
From the release:
 
The team found that although even the recommended five portions of fruit and vegetables a day reduced disease risk, the greatest benefit came from eating 800g a day (roughly equivalent to ten portions - one portion of fruit or vegetables if defined as 80g).
The study, which was a meta-analysis of all available research in populations worldwide, included up to 2 million people, and assessed up to 43,000 cases of heart disease, 47,000 cases of stroke, 81,000 cases of cardiovascular disease, 112,000 cancer cases and 94,000 deaths.
In the research, which is published in the International Journal of Epidemiology, the team estimate approximately 7.8 million premature deaths worldwide could be potentially prevented every year if people ate 10 portions, or 800 g, of fruit and vegetables a day.
 
 
 
Compared with not eating any fruits and vegetables, eating up to 800g fruit and vegetables a day – or 10 portions – was associated with:
24% reduced risk of heart disease;
33% reduced risk of stroke;
28% reduced risk of cardiovascular disease;
13% reduced risk of total cancer; and 
31% reduction in dying prematurely.
 
What's more, the researchers identified fruits and vegetables that seemed to deliver the most benefits.  The study said the commodities that may help prevent heart disease, stroke, cardiovascular disease, and early death are apples and pears, citrus fruits, salads and green leafy vegetables such as spinach, lettuce and chicory, and cruciferous vegetables such as broccoli, cabbage and cauliflower.
 
Researchers found green vegetables, such as spinach or green beans, yellow vegetables, such as peppers and carrots, and cruciferous vegetables, may reduce cancer risk.
 
"We wanted to investigate how much fruit and vegetables you need to eat to gain the maximum protection against disease, and premature death," Dagfinn Aune, lead author of the research from the School of Public Health at Imperial said  in the release. "Our results suggest that although five portions of fruit and vegetables is good, ten a day is even better."
 
This UK study is nothing but supportive of the goal of increasing fruit and vegetable consumption.
 
John Sauve, partner with the Maine-based Food and Wellness Group and creator of the Colors of Health promotion strategy, said in an e-mail that the research should light a fire under the industry and be a source of motivation for the consuming public. Sauve said the industry shouldn't be afraid of urging consumers to set the bar high.
 
From John's e-mail:
 
And as for the statement about putting "pressure" on people to eat more creating "unrealistic expectations," that is such a line of BS. 
That's like telling your kids that striving for D's in school is just fine or that it's OK to do one sit up a day. In case you don't remember, the 5 in 5 A Day was the minimum servings amount recommended by the DGA … through 2005.  Now the average is more like 10 servings … 5 cups … 800g … double where we are now. Sounds a lot like the research. 
 
 
What do you think? Are we doing enough?  How can the industry move the needle, raise the bar, or move the ball down the field (or insert other analogy) in regard to increasing fruit and vegetable consumption? Whatever strategy is embraced, Imperial College in London study gives yet another argument to raise expectations.