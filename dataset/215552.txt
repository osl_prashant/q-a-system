Expanding a dairy operation to improve efficiencies is a big step. Producers that are ready to grow have thought about genetic decisions, facility upgrades, milk production and profitability. In addition to these important factors, it"s critical to make sure the silage inventory is ready too. Availability and quality of silage can greatly affect milk production — and profitability — for any operation.
First, know how much silage will be needed after expansion goals are met. To estimate this figure, calculate fresh tons of silage needed by:

Multiplying the number of head by the pounds of silage fed (fresh weight) by the number of days fed.
Then, divide by 2,000.
Be sure to add each group of animals fed different levels of silage.
This figure can be converted to tons of dry matter (DM) required using an average DM figure, such as 35 percent.

Next, make sure you have adequate storage to keep this level of inventory. Piles, bags and wrapped bales offer economical storage options with minimal capital investment. University of Wisconsin researchers found that there are no economies of scale above 758 tons of DM stored. 1
Finally, keep an eye on quality of silage. Maintaining low DM losses can help improve the bottom line by conserving quantity of forage and nutrients. Crop planting, growth and harvest timing are all important considerations.
Choosing a proven inoculant also can help keep DM losses low, ensure good fermentation and improve feed-out stability. Inoculants aren"t just for growers that have encountered challenges in the past. Tailoring inoculant choices to the crop to be ensiled, the preferred storage method and potential ensiling challenges can help improve quality and quantity of silage — helping to ensure your operation is ready to expand.
Ask the Silage Doctor at QualitySilage.com, @TheSilageDoctor and at facebook.com/TheSilageDoctor if you have questions about managing your silage stocks.
1 Hutjens M. Management of tower silos. In: Silage Management Handbook. Lallemand Animal Nutrition.