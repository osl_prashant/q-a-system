In 2017, U.S. pork exports recorded the largest year ever in volume, with sales to more than 100 countries around the world. U.S. pork and pork variety meat exports totaled 5.399 billion pounds valued at $6.486 billion, up 6% and 9% respectively from 2016.

“Exports continue to be an important piece of the puzzle for adding to producers’ bottom line,” said Craig Morris, vice president of international marketing for the Pork Checkoff. “Recognizing the importance of exports, the National Pork Board recently approved nearly $8.7 million for 2018 export market activities, the most significant financial investment of Checkoff dollars in international marketing efforts to date. With more high quality U.S. pork available than ever, we are redoubling efforts to build on the momentum of the past year.”

Pork variety meats were the shining star during 2017. Exports tied the 2011 record, with 82% of edible variety meat exported. Pork variety meat exports totaled $1.17 billion, setting a new total value record and surpassing $1 billion for the first time. Together, China and Mexico accounted for 86% of U.S. pork variety meat exports. In 2017, total edible pork variety meat exports added $9.67 in value to every hog marketed, according to the U.S. Meat Export Federation.

U.S. pork and pork variety meat exports accounted for 26.6% of total pork production, with 22% of muscle cuts exported, in 2017. Export value returned an average $53.47 per head back to producers, up 6 percent from 2016.

The top six markets by volume were Mexico (1.768 billion pounds), China/Hong Kong (1.09 billion pounds), Japan (868 million pounds), Canada (459 million pounds), South Korea (382 million pounds) and South America (229 million pounds).

The top six markets by value were Japan ($1.626 billion), Mexico ($1.514 billion), China/Hong Kong ($1.078 billion), Canada ($792 million), South Korea ($475 million), and South America ($268 million).