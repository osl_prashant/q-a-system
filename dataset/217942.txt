Costa Rica fresh produce exporters expect volume to grow in the next year, boosted by tropicals and exotic produce.
From January through October 2017, statistics show Costa Rica exported close to $2.4 billion in agricultural exports to all countries, 4% higher than the same period a year ago.
The U.S. was the main destination country for Costa Rican exporters, taking $1.09 billion from January to October, according to a news release.
“Costa Rica remains a strong, reliable and versatile exporter of produce,” Pedro Beirute, CEO of Procomer (Costa Rica’s trade promotion agency), said in the release. “We expect to close out 2017 with over $2.7 billion in ag exports.”
Bananas and pineapples top Costa Rica exports, followed by good volumes of yucca, melons, chayote, ñame, and other fruits, according to the release.
Procomer export statistics indicate banana export volume rose 25% from 2015 to 2017. Pineapple volume grew by 14%, yucca was 12% up, watermelon 57% higher and chayote volume rose 33%.
“We expect growth in these highly demanded products to continue in 2018,” Beirute said in the release.
Costa Rica looks to tropical and exotic product growth in 2018. “The U.S. marketplace continues to demand new and unique products due to the increase in ethnic diversity in the population as well as U.S. consumers’ expanding palate,” Beirute said in the release. 
Greater volume in the future is expected for ginger, rambutan, a variety of specialty melons, beets, cabbage, carrots, pumpkin, root products, and more organics, according to the release.
Ready for the food safety regulations
Costa Rica exporters are ready for implementation and enforcement of the Foreign Supplier Verification Program (FSVP) of the new Food Safety Modernization Act, according to the release.
“Costa Rica’s produce export sector is built on a history of regulatory and quality compliance,” Beirute said in the release. 
“Our exporters have for decades proven their ability to meet the most stringent standards to export to Europe as well as the U.S. Many of our exporters are GlobalGAP compliant and many have ISO, Kosher, USDA Organic, and Fair Trade certifications, among others.”
Costa Rican exporters also receive support from Procomer to help bolster compliance, according to the release.
“Procomer has been working hand-in-hand with suppliers to the U.S.,” Beirute said. “We facilitate certificated audits and train exporter personnel in risk management and preventive controls to help them comply with quality and food safety standards as well as good agricultural practices.