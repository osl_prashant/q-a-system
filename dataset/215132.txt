If you are looking to put some money into real estate, there's a home in San Antone. Legendary country music singer George Strait is selling his nearly 8,000-sq. ft. mansion.
Valued at $3.9 million and located within the community of The Dominion, the adobe estate sits on 12.2 acres of land overing incredible views over Texas Hill Country. World renowned artist, Bill Tull, of Arizona, relocated to San Antonio for two years along with to build this substantial estate. His work is evident in the 14 hand-sculpted fireplaces, stained glass windows and a custom designed copper bar top.
 
Want a look inside? Strait’s daughter-in-law Tamara Strait, a real estate agent, has develop a website where you can do a full walkthrough of the home.
Other top features:

Main House: 3 Bedrooms, 4 full baths, and 2 half baths
Casita: 1 Bedroom, 1 bath, living room, kitchenette, and fireplace
Infinity Edge Pool/Spa with mosaic finish overlooking picturesque views of Downtown San Antonio & Texas Hill Country
Sport Court
Gourmet kitchen features upscale stainless appliances and gas cooking
Work out room with sauna
Walk in safe room
Lavish owners retreat features separate his and her bathrooms and closets
14 Hand sculpted masonry fireplaces
Custom designed fireplace screens throughout
Hardwood and 4" overlay concrete floors
Imported adobe materials
Saguaro Cactus rib interior shutters
Stained glass designed by Bill Tull
Custom Painted Artist Murals
Patio offers built-in grill, sink, ice maker, and refrigerator

 
Haven't got a worry, haven't got a care
Haven't got a thing to call my own
Though I'm out of money, I'm a millionaire
I still have my home in San Antone
—"Home in San Antone"; songwriter Fred Rose; © Sony/ATV Music Publishing LLC