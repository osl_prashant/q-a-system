Yemeni immigrants focus on future in US amid war back home
Yemeni immigrants focus on future in US amid war back home

By JEFF KAROUBAssociated Press
The Associated Press

DEARBORN, Mich.




DEARBORN, Mich. (AP) — People from Yemen have recently begun to see long-term futures in the U.S. and are making their culture part of their businesses.
It's partly a result of the normal socio-economic evolution among first- and second-generation immigrants. But it's also driven by protracted war, poverty and famine back home.
Ibrahim Alhasbani (IH'-bruh-heem ahl-hahs-BAH'-nee) left Yemen a few years ago and now owns a cafe in the Detroit suburb of Dearborn. He shares Yemen's centuries-old coffee tradition and makes his coffee made from beans harvested on his family's farm in Yemen.
He's part of a new wave of Yemeni entrepreneurs.
Yemeni have been coming to the U.S. for more than a century, but in recent years they have been planting stronger roots, raising their profile and looking outward.