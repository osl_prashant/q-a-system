Livestock manure contains beneficial soil-building ingredients and plant nutrients, but they could be wasted if the manure spreader isn’t calibrated correctly.
“Whether you are spreading the manure yourself or having a custom manure hauler do the job for you, calibrating the spreader should be a top priority,” says Mary Berg, the North Dakota State University Extension Service’s area livestock environmental management specialist at the Carrington Research Extension Center.
“These valuable nutrients may be wasted by overapplication or crop yield goals may not be met due to underapplication because of a lack of spreader calibration,” she adds. “Manure is money, so let’s not waste it.”
One calibration option is called the sheet method. This method works well for solid manure applications, Berg says.

Here are materials you’ll need:

Tarps/sheets (at least three) of a known area (3 feet by 7 feet 4 inches, for example). Landscaping fabric works well because applied manure will not slide off as easily as it will on a plastic sheet.
5-gallon bucket
Scale

Procedure:

Weigh the bucket and a sheet.
Lay the sheets in a row and anchor them with a few rocks or stakes.
Start the tractor and turn on the spreader. Allow time for the spreader to start spreading.
Record your tractor gear, engine’s revolutions per minute and spreader settings.
Drive over the sheets, applying manure on them.
Retrieve the manure-covered sheets and weigh them in the bucket. Subtract the weight of the bucket and sheets.
If you’re using a sheet measuring 21.8 square feet (3 feet by 7 feet 4 inches or 4 feet by 5 feet 6 inches, for example), then the weight in pounds of manure on the sheet is equal to tons per acre.

The application rate is: Rate (tons/acre) = (pounds of manure on sheet x 21.8) ÷ sheet area (square feet).

Visit http://tinyurl.com/ManureSpreaderCalibration-NDSU for more information on calibration methods.
To find a North Dakota custom manure hauler, visit https://www.ag.ndsu.edu/lem, NDSU’s livestock environmental management website.