The National Grain and Feed Association is supporting the 199A grain glitch fix in Congress made possible by the omnibus spending bill, the National Farmers Union remains a major critic.

The group is urging Congress to reject the proposal, saying it’s too much in favor of corporations that already had their tax rate cut from 35 percent to 21 percent.

However, they could get their way. On Friday, President Trump is threatening to veto the $1.3 trillion spending bill because it doesn’t have a solution for the Deferred Action for Childhood Arrivals program (DACA) or funding for a wall between the U.S. and Mexico.

Private grain buyers say Section 199A was bad for business. According to Matt Tamano, general manager of the Poet plant in Portland, Ind. says businesses and producers are waiting to see what happens.

“We’re confident there’s going to be a fix—that’s what we’ve been told,” he said. “It’s critical that it is changed, it is fixed.”

Hear his full comments on AgDay above.