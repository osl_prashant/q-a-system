This market update is a www.porkbusiness.com weekly column reporting trends in weaner pig prices and swine facility availability. All information contained in this update is for the week ended November 17, 2017.

Looking at hog sales in May 2018 using June 2018 futures, the weaner breakeven was $65.59, down $1.70 for the week. Feed costs were up $0.38 per head. June futures increased $0.83 compared to last week’s May futures used for the crush and historical basis is declined from last week by $1.46 per cwt. Breakeven prices are based on closing futures prices on November 17, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was above average this week with 43,701 head being reported. This is 120% of the 52-week average. Cash prices were $47.06, up $4.42 from a week ago. The low-to-high range was $32.00 - $57.00. Formula-priced weaners were up $0.58 this week at $42.41.
Cash-traded feeder pig reported volume was below average with 3,838 head reported. Cash feeder pig reported prices were $56.21, up $1.09 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market.
 

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through November 17, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential decreased $6.12 this week to a projected gain of $18.53 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.