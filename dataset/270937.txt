After two years of a US Youth Soccer partnership, the National Mango Board is looking to social media engagement to promote mangoes in 2017.The soccer campaign was a unique marketing strategy that looked to connect with children and parents, said communications manager Angela Serna.
“At games, we were really trying to inspire the kids and moms with the message that mangoes are a super fruit for your superstar,” she said.
In 2016, the board provided 4,000 mangoes at 10 youth soccer tournaments and garnered 1.4 million impressions through US Youth Soccer newsletters.
This year, the board plans to reach consumers through social media platforms including mommy bloggers and YouTube kid stars.
They will partner with the youth YouTube celebrities to present mango recipes and instructional cooking segments.
“We are also targeting moms and influencers, showing mango is a fun fruit for kids to taste and enjoy,” Serna said.
The National Mango Board will also supply mango samples at family fairs around the country.
“We target the whole family and always include educational messaging about how to choose, taste and cook with mangoes,” she said.
Serna said the board will measure the influence of the campaigns through tracking impressions, views and engagement across the social media platforms.