Since 1970, Kenny Brinker has been helping to manage the family farm. Today, he passes the business on to the next generation. Read more about his legacy in these stories from Farm Journal's Pork:The Legacy of Brinker Farms: Part One
Building a Viable Succession Plan: Brinker Farms Part Two
Diversify and Thrive: Brinker Farms Part Three
To follow the history of Brinker Farms, click through the interactive timeline below.