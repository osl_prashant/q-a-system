The Latest: Border rancher 'elated' with Guard deployment
The Latest: Border rancher 'elated' with Guard deployment

The Associated Press

PHOENIX




PHOENIX (AP) — The Latest on President Donald Trump's order for the National Guard to deploy to the U.S.-Mexico border (all times local):
5:05 p.m.
A fifth-generation Arizona rancher who is an outspoken supporter of President Donald Trump's border wall is "absolutely elated" by the president's plans to deploy the National Guard at the border.
Jim Chilton's 50,000-acre (20,200-hectare) ranch in Arivaca, Arizona stretches 14 miles (23 kilometers) along the border and is separated from Mexico by a four-strand barbed wire cattle fence.
Chilton says he often sees people sneak from Mexico into the United States across his property but they usually get away because the closest Border Patrol station is 80 miles (129 kilometers) away.
He says he saw none from 2006 to 2008 when former President George W. Bush sent National Guard members to the border.
___
4:05 p.m.
Arkansas Gov. Asa Hutchinson said his state is willing to send National Guard troops to assist President Donald Trump's plans to send up to 4,000 National Guard members to the U.S.-Mexico border.
Hutchinson said he spoke Thursday with Homeland Security Secretary Kirstjen Nielsen about the border plans.
Hutchinson told The Associated Press that Arkansas Guard members "who are experienced and who have done that before" could be sent.
Hutchinson is a Republican and a former undersecretary of the Homeland Security Department.
Hutchinson tweeted that the effort would "add to our nation's security at an important time."
___
3:55 p.m.
New Mexico Gov. Susana Martinez was among those on a call with other border governors and federal officials to discuss efforts to bolster border security.
The Republican governor on Thursday reiterated her support for National Guard troops assisting the Border Patrol in New Mexico to combat illegal crossings and drug smuggling.
Her spokesman Mike Lonergan says officials "cannot allow criminal activity to come across the border where it will only increase crime in New Mexico's communities."
During the call, Martinez also talked about the need to keep law enforcement officers who are members of the National Guard on the job in New Mexico's cities and counties.
It's unclear whether the troops would be mobilized from volunteers.
Martinez's office says New Mexico officials with the federal government in coming weeks to determine how many troops might be deployed and their specific tasks.
___
3:50 p.m.
Montana Gov. Steve Bullock says he'll never deploy National Guard troops "based simply on the whim of the President's morning Twitter habit."
Bullock, a Democrat, said in a statement Thursday that the responsibility of sending Guard soldiers anywhere is one of the most difficult things he faces.
Trump signed a memorandum on Wednesday that would allow the use of National Guard troops to help fight illegal immigration and drug trafficking on the U.S.-Mexico border.
That morning, the president wrote in a tweet that the nation's border laws are weak and "we will be taking strong action today."
Bullock administration officials say the request for troops would have to come directly from the governor of a border state, and Montana had received no such requests as of Thursday.
___
2:09 p.m.
President Donald Trump has said that he wants to send between 2,000 and 4,000 National Guard members to the US-Mexico border.
Speaking on Air Force One Thursday, Trump gave his first estimate on guard levels.
Asked about the cost, he said the administration was looking at it.
Trump says he plans to keep the guard members there until a "large portion of the wall is built."
Former President George W. Bush sent 6,400 National Guard members to the border between 2006 and 2008.
They performed support duties aimed at freeing up federal agents to focus on border security. T
___
1:55 p.m.
A U.S. official says the National Guard's arrival on the Mexican border would put more Border Patrol agents on the "front lines."
Ronald Vitiello (vi-TEL-oh), U.S. Customs and Border Protection's acting deputy commissioner, tells Fox News that the guard would replace agents who are not doing "enforcement work."
He wasn't more specific, but Border Patrol agents are often assigned to process arrests, manage short-term detention facilities and perform other administrative and support work.
Vitiello says guard members will be in jobs that do not require law enforcement work, an apparent reference to being on patrol and making arrests.
He cautions against immediate deployment. He said the Pentagon has work to do with state governments, and Customs and Border Protection needs to deliver a complete list of what it wants.
___
12:15 p.m.
Oregon Gov. Kate Brown says she won't let National Guard troops from her state be stationed at the Mexican border if President Donald Trump requests them.
Brown is a Democrat and in a Tweet Wednesday said she was "deeply troubled by Trump's plan to militarize our border."
Trump this week said the situation at the U.S.-Mexico border had reached "a point of crisis" in his proclamation directing National Guard deployment to the southern border.
Brown is a frequent Trump critic. She says Oregon hasn't been contacted by federal officials about border troop deployment.
___
11:55 a.m.
California Gov. Jerry Brown's administration says it needs answers from President Trump's administration before deciding whether to commit National Guard troops to help protect the border with Mexico.
That includes where money for the deployment would come from, how long it would last, and if there are clearly definable objectives.
The Democratic governor's decision is a big unknown after other border state Republican governors welcomed the Guard's deployment.
California has opposed most of Trump's policies and Trump's administration is suing over three state immigration laws.
Brown's administration pointed Thursday to a California National Guard statement saying it's promptly reviewing the request, as it did similar federal requests for staffing in 2006 and 2010.
The California National Guard already has 55 members helping fight drug trafficking on the southern border.
___
11:45 a.m.
U.S. Homeland Security Secretary Kirstjen Nielsen has said it's unclear how many National Guard troops will be deployed to the border with Mexico following President Donald Trump's proclamation ordering the deployment.
She said Thursday that determination won't happen until specific missions are set at specific locations and U.S. officials discuss deployment with the governors of the four U.S. states that share the border with Mexico.
Trump issued the proclamation Wednesday and said the deployment will fight illegal immigration and drug smuggling.
___
11:00 a.m.
U.S. officials say they have not determined yet whether National Guard troops sent to the border with Mexico to fight illegal immigration will be armed.
Marine Lt. Gen. Kenneth F. McKenzie told reporters at the Pentagon Thursday that it has not yet been determined how many, if any, of the National Guard troops participating in the border security operation will be armed.
President Donald Trump on Wednesday said the situation at the U.S.-Mexico border had reached "a point of crisis" in his proclamation directing National Guard deployment to the border.
Homeland Security Secretary Kirstjen Nielsen has said she had been working with governors of the southwest border states to develop agreements on where and how many Guardsmen will be deployed.
___
10:45 a.m.
National guard contingents in U.S. states that border Mexico say they are waiting for guidance from Washington to determine what they will do following President Donald Trump's proclamation directing deployment to fight illegal immigration and drug smuggling.
The National Guard in Texas said in a statement Thursday said the deployment is in "very early planning stages."
With troops in all states, the National Guard has been called on by past presidents and governors to help secure U.S. borders.
The Texas Guard says it has "firsthand knowledge of the mission and operating area" that will allow it to move seamlessly into the new role.
Governors of the border states of Arizona and New Mexico have welcomed deployment of the Guard along the southwest border as a matter of public safety.
___
9:00 a.m.
Mexican senators and presidential candidates put aside differences to condemn U.S. President Donald Trump's decision to deploy National Guard troops to the border.
The country's Senate passed a resolution Wednesday calling for the suspension of cooperation on illegal immigration and drug trafficking in retaliation for Trump's move.
Presidential candidate Ricardo Anaya went further, saying Mexico should limit anti-terrorism cooperation until the National Guard is withdrawn. Anaya is the candidate of a left-right coalition in the country's July 1 presidential election.
Ruling-party candidate Jose Antonio Meade said that "independently of our political differences, it is time for all the presidential candidates to unite in defense of the sovereignty and dignity of the nation ... to reject and repudiate thus kind of measure."
The Mexican government said guard members "will not carry weapons or have immigration or customs duties."