SEOUL (Reuters) - South Korea has confirmed a case of foot and mouth disease at a hog farm, the country’s first discovery since February last year, its agriculture ministry said on Tuesday.
The case was reported on Monday at a farm in Gimpo, about 30 km (18.64 miles) northwest of Seoul, the Ministry of Agriculture, Food and Rural Affairs said in a statement.
Foot and mouth disease is an infectious disease that affects cattle, hogs and other cloven-hoofed animals.
About 900 hogs in the farm were being culled, the ministry said, adding that the country’s foot-and-mouth disease alert status has been raised by a notch to contain the outbreak.
The ministry also said the disease was unlikely to be widespread as the country inoculates its hogs and cattle against the virus.
The most recent case was discovered in February 2017, prompting the country to raise its alert level to the highest, as the country was also grappling with a nationwide spread of bird flu.
Editor’s Note: Reporting By Jane Chung, Editing by Sherry Jacob-Phillips