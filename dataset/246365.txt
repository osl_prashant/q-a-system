5 types of apples, once thought extinct, are rediscovered
5 types of apples, once thought extinct, are rediscovered

The Associated Press

LEWISTON, Idaho




LEWISTON, Idaho (AP) — Five types of apples, once thought to be extinct, have been rediscovered in northern Idaho and eastern Washington.

                The Lewiston Tribune newspaper reported Monday that "apple detective" David Benscoter located the trees growing near a butte in the rolling hills of the vast Palouse agricultural area.
Benscoter worked with apple experts at the Temperate Orchard Conservancy in Oregon and Fedco Seeds in Maine to positively identify the apple types. They were compared to written descriptions from old books and antique watercolor paintings.
The newly rediscovered apples include the Shackleford, Saxon Priest, Kittageskee, Ewalt and McAffee varietals. An estimated 17,000 named apple varieties are thought to have originated in North America, but Benscoter says only about 4,000 still exist today.
"I just love the history of these old apples and what they meant to the first homesteaders that arrived here in eastern Washington and northern Idaho," Benscoter said. "The apple was the most important fruit you could have, and it could be used in so many ways."
He first became interested in hunting down the almost-gone and nearly forgotten fruit when he helped a neighbor with chores on her property. He found an old apple tree and began to search the internet to try to figure out what variety it bore.
By checking old county fair records in Whitman County, Washington, he discovered several apple types that were listed as extinct.
Since that time, he has discovered more than 20 varieties of apples that were once considered lost. He's hoping area residents will let him know if they have old apple trees in neglected orchards or growing in back fields that he can examine.
"Those apples have been forgotten about in the back of someone's field or an old orchard nobody has taken care of in a hundred years," Benscoter said. "I'm hopeful, and obviously the search has been somewhat successful, and so I think there are still many apples out there that can be found."
Apples have as many 50 different identifiers, including stem length, shape, size, color and structure.
Benscoter thinks he's found an additional seven apples in the region that were also thought to be extinct or extremely rare, but they have yet to be confirmed.
Those include the Autumn Gray, Surprise No. 1, Flushing Spitzenburg, Republican Pippin, Bogdanoff Glass, Flory and Early Colton.
___
Information from: Lewiston Tribune, http://www.lmtribune.com