Legislation clears provision for medical marijuana license
Legislation clears provision for medical marijuana license

The Associated Press

TALLAHASSEE, Fla.




TALLAHASSEE, Fla. (AP) — Legislation to change how a black farmer can receive a Florida license to grow medical marijuana is headed to the desk of Gov. Rick Scott.
The Senate passed HB 6049 on Thursday, which removes the provision that a black farmer has to be a member of Florida chapter of the Black Farmers and Agriculturalists Association to be eligible for the license.
The process of the state's Office of Medical Marijuana Use issuing five new licenses has been held up after a black farmer filed a lawsuit after he was unable to join the BFAA.
The Legislature last year stipulated that one of the new licenses be awarded to a member of the Pigford vs. Glickman lawsuit, in which the federal government was found to have discriminated against black farmers, and belong to the association.