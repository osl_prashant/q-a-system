(Bloomberg) -- Illegal spraying of a volatile weedkiller may be the cause of unprecedented damage to crops earlier this year, especially for soybean farmers in Arkansas.
Complaints arose this summer from U.S. farmers after an herbicide suited only for genetically modified crops was “drifting” across neighboring fields and leaving a trail of damage. A new version of dicamba, the offending herbicide, that’s supposed to be less mobile is produced by seed and crop-chemical giants Monsanto Co., DowDuPont Inc. and BASF SE. It’s fine for use on acres planted with the modified seeds, but can leave non-resistant plants stunted with wrinkled leaves. 
Currently, scientists and regulators are trying to determine the cause of the damages, which affected millions of acres, and whether these new formulations of dicamba are still too volatile for widespread use, or whether farmers’ misuse of the products is responsible. New data from BASF suggests that some farmers were applying generic or off-label dicamba, products that are known to be highly volatile and are illegal to use over crops.
Arkansas’ State Plant Board received almost a thousand complaints during this year’s growing season about dicamba damage to crops, more than any other state. Only BASF’s formula for the herbicide, called Engenia, was legally approved for use in the state with the resistant seeds, manufactured by Monsanto.
Illegal Spraying
BASF only sold enough of its product to cover about 52 percent of the dicamba-tolerant acres planted in the state, the company said in an email.
The numbers imply that a large quantity of off-label dicamba could have been used to fill the gap, said Chris Perrella, a Bloomberg Intelligence analyst. Such versions of the herbicide can be highly volatile, meaning the chemical vaporizes and can easily move to neighboring fields.
“They didn’t sell enough to cover all the fields by a wide margin,” Perrella said.
BASF made its calculation based on 1.8 million dicamba-tolerant acres of soybean and cotton seeds sold by Monsanto Co. in Arkansas and estimated an average of 1.2 sprays of herbicide per acre. The estimates also assume that all farmers opted to use a dicamba formula of herbicide.
Some weed scientists have conducted studies and said the dicamba formulations are still too volatile to contain. Those conclusions form part of the basis of a class action lawsuit against the manufacturers filed in federal court in St. Louis on behalf of about 20 soybean farmers.
Mike Smith, an attorney at Dover Dixon Horne PLLC, one of the firms involved in the class action, said a few factors may be influencing the low percentage BASF calculated. A mid-season ban on Engenia use by the Arkansas Plant Board could’ve have stemmed sales, he said. Some farmers also bought dicamba-tolerant seeds defensively because they were concerned about off-target movement, but never sprayed. That would artificially boost the number of tolerant acres in the calculation, he said.
Dicamba Renaissance
Dicamba use is experiencing a renaissance as weeds become more resistant to glyphosate, another active pesticide ingredient commonly known through Monsanto’s brand Roundup. U.S. farmers planted 20 million acres of Monsanto’s Xtend soybean seeds this year that tolerate both glyphosate and dicamba, and the company projects that number will double next year.
To combat dicamba’s inherent tendency to drift onto neighboring farms, chemical companies manufactured “low volatility” formulations. But when reports of damage surfaced, questions were raised about whether the products were working.
Monsanto has said the complaints are due to misuse of the chemicals, as opposed to problems with the product, and that more training is needed for farmers. The company has also said that dicamba products can’t be fingerprinted, so there’s no way to know which products caused a specific case of damage.
In Arkansas, only BASF’s Engenia herbicide had been approved for use in the 2017 growing season, while Monsanto’s low volatility dicamba product, called XtendiMax, was banned. On Friday, Monsanto filed a lawsuit against the state’s Plant Board for rejecting its petition to halt a ban in the 2018 growing season.
In an effort to stem dicamba damage in the next growing season, the Environmental Protection Agency earlier this month announced stricter rules for using dicamba on crops, including a “restricted use” classification on the chemical, which means only certified applicators with special training will be able to apply it.
By Lydia Mulvany
Updated on Oct. 25, 2017 10:07 a.m.
Published on Oct. 24, 2017 12:48 p.m.


 
 
 
 
 
 








(Bloomberg) -- Illegal spraying of a volatile weedkiller may be the cause of unprecedented damage to crops earlier this year, especially for soybean farmers in Arkansas.






Complaints arose this summer from U.S. farmers after an herbicide suited only for genetically modified crops was “drifting” across neighboring fields and leaving a trail of damage. A new version of dicamba, the offending herbicide, that’s supposed to be less mobile is produced by seed and crop-chemical giants Monsanto Co., DowDuPont Inc. and BASF SE. It’s fine for use on acres planted with the modified seeds, but can leave non-resistant plants stunted with wrinkled leaves. 






Currently, scientists and regulators are trying to determine the cause of the damages, which affected millions of acres, and whether these new formulations of dicamba are still too volatile for widespread use, or whether farmers’ misuse of the products is responsible. New data from BASF suggests that some farmers were applying generic or off-label dicamba, products that are known to be highly volatile and are illegal to use over crops.






Arkansas’ State Plant Board received almost a thousand complaints during this year’s growing season about dicamba damage to crops, more than any other state. Only BASF’s formula for the herbicide, called Engenia, was legally approved for use in the state with the resistant seeds, manufactured by Monsanto.






Illegal Spraying






BASF only sold enough of its product to cover about 52 percent of the dicamba-tolerant acres planted in the state, the company said in an email.






The numbers imply that a large quantity of off-label dicamba could have been used to fill the gap, said Chris Perrella, a Bloomberg Intelligence analyst. Such versions of the herbicide can be highly volatile, meaning the chemical vaporizes and can easily move to neighboring fields.






“They didn’t sell enough to cover all the fields by a wide margin,” Perrella said.






BASF made its calculation based on 1.8 million dicamba-tolerant acres of soybean and cotton seeds sold by Monsanto Co. in Arkansas and estimated an average of 1.2 sprays of herbicide per acre. The estimates also assume that all farmers opted to use a dicamba formula of herbicide.






Some weed scientists have conducted studies and said the dicamba formulations are still too volatile to contain. Those conclusions form part of the basis of a class action lawsuit against the manufacturers filed in federal court in St. Louis on behalf of about 20 soybean farmers.






Mike Smith, an attorney at Dover Dixon Horne PLLC, one of the firms involved in the class action, said a few factors may be influencing the low percentage BASF calculated. A mid-season ban on Engenia use by the Arkansas Plant Board could’ve have stemmed sales, he said. Some farmers also bought dicamba-tolerant seeds defensively because they were concerned about off-target movement, but never sprayed. That would artificially boost the number of tolerant acres in the calculation, he said.






Dicamba Renaissance






Dicamba use is experiencing a renaissance as weeds become more resistant to glyphosate, another active pesticide ingredient commonly known through Monsanto’s brand Roundup. U.S. farmers planted 20 million acres of Monsanto’s Xtend soybean seeds this year that tolerate both glyphosate and dicamba, and the company projects that number will double next year.






To combat dicamba’s inherent tendency to drift onto neighboring farms, chemical companies manufactured “low volatility” formulations. But when reports of damage surfaced, questions were raised about whether the products were working.






Monsanto has said the complaints are due to misuse of the chemicals, as opposed to problems with the product, and that more training is needed for farmers. The company has also said that dicamba products can’t be fingerprinted, so there’s no way to know which products caused a specific case of damage.






In Arkansas, only BASF’s Engenia herbicide had been approved for use in the 2017 growing season, while Monsanto’s low volatility dicamba product, called XtendiMax, was banned. On Friday, Monsanto filed a lawsuit against the state’s Plant Board for rejecting its petition to halt a ban in the 2018 growing season.






In an effort to stem dicamba damage in the next growing season, the Environmental Protection Agency earlier this month announced stricter rules for using dicamba on crops, including a “restricted use” classification on the chemical, which means only certified applicators with special training will be able to apply it.