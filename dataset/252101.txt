BC-US--Cotton, US
BC-US--Cotton, US

The Associated Press

New York




New York (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Thursday:
(50,000 lbs.; cents per lb.)
COTTON NO.2Open    High    Low    Settle     Chg.May       83.26   85.25   82.96   85.12  Up   2.26Jul       82.56   84.69   82.56   84.55  Up   2.00Aug                               78.49  Up    .44Oct                               79.78  Up    .93Oct                               78.49  Up    .44Dec       78.01   78.50   78.01   78.49  Up    .44Dec                               78.34  Up    .23Mar       78.10   78.40   78.10   78.34  Up    .23May       78.14   78.16   77.97   78.16  Up    .22Jul       77.78   77.82   77.78   77.82  Up    .16Aug                               72.64  Up    .08Oct                               75.36  Up    .03Oct                               72.64  Up    .08Dec       72.31   72.80   72.31   72.64  Up    .08Dec                               72.98  Up    .06Mar                               72.98  Up    .06May                               73.74  Up    .05Jul                               73.96  Up    .04Aug                               72.75  Down  .22Oct                               73.63  Up    .02Oct                               72.75  Down  .22Dec                               72.75  Down  .22