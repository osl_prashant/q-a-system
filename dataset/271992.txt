AP-CA--California News Digest 4:30 pm, CA
AP-CA--California News Digest 4:30 pm, CA

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in California. Questions about coverage plans are welcome and should be directed to the AP-Los Angeles bureau at 213-626-1200, losangeles@ap.org, or to the AP-San Francisco bureau at 415-495-1708, sanfrancisco@ap.org. Robert Jablon is on the news desk. AP-California News Editor Frank Baker can be reached at 213-346-3134 or fsbaker@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Pacific.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
NEW FOR 4:30 PM:
YEMENI BODEGA STRIKE
NEW YORK — Corner stores around New York City and elsewhere were shut down for a short time on Tuesday by their Yemeni-American owners to protest against the U.S. travel ban, the day before the Supreme Court was scheduled to hear arguments over whether it should be upheld. By Deepti Hajela. SENT: 400 words.
PECAN BRANDING
FORT WORTH, Texas — The humble pecan is being rebranded as more than just pie. Pecan growers and suppliers are hoping to sell U.S. consumers on the virtues of North America's only native nut as a hedge against a potential trade war with China, the pecan's largest export market. By Emily Schmall. SENT: 700 words, photos.
ATHLETICS-MANAEA NO-HITTER
ARLINGTON, Texas — Sean Manaea's father is from American Samoa, where he grew up playing cricket and rugby and didn't really know anything about baseball. The younger Manaea was born in Indiana, loved baseball from an early age and blossomed into a hard-throwing 6-foot-5 lefty who in his last start for the Oakland Athletics threw a no-hitter against the Boston Red Sox. By Stephen Hawkins. SENT: 800 words, photos.
Also:
— WEBBY AWARDS — Lin-Manuel Miranda, "Game of Thrones" and a key figure in the #MeToo movement are among the winners of this year's Webby Awards. Photos planned.
— INMATE SLAIN — Officials say they are treating the death of a California state prison inmate as a homicide.
— COLD CASE KILLING — Authorities have arrested a suspect in the stabbing death of a 75-year-old retiree more than three decades ago in Southern California.
— TAINTED JEWELRY — A Los Angeles company accused of selling jewelry for children that contained dangerous levels of toxic lead and cadmium has been fined $1.6 million.
— BAY AREA-SWEAGE WATER FINES — The U.S. Environmental Protection Agency says the city of Oakland and a water district are being fined $350,000 for allegedly allowing sewage water to flow into the San Francisco Bay.
TOP STORIES:
CONGRESS-CALIFORNIA-TOO MANY DEMOCRATS
SACRAMENTO — Fresh-faced Democrats are packing U.S. House races nationwide, inspired to run in the Trump era with hopes of seizing long-held Republican seats in a much-anticipated "blue wave."  But in California, Democrats will find out in June if all that enthusiasm has a downside. By Kathleen Ronayne.  SENT: 825 words, photos.
CALIFORNIA GUN DEATHS-STUDY
SAN FRANCISCO — Gun deaths have fallen in California over a 16-year period ending in 2015, driven largely by a decline in gang violence and falling homicide rates among black and Hispanic male victims, a recent study of firearm violence has found. By Paul Elias. SENT: 420 words, photos.
CALIFORNIA RESTAURANT STABBING
VENTURA — A California police department will review its procedures after a restaurant patron with his 5-year-old daughter on his lap was fatally stabbed inside a crowded beachfront steakhouse by a man authorities say was homeless. Officers should have responded in person to initial reports about the man rather than simply monitoring his actions via a security camera on the pier, Ventura police Chief Ken Corney said. SENT: 325 words, photos.
NORTHERN CALIFORNIA
RFK FUNERAL TRAIN
SAN FRANCISCO — The assassination of Robert F. Kennedy Jr. 50 years ago this June fractured the nation just two months after the assassination of Martin Luther King, Jr. and five years after his brother John F. Kennedy was killed.  But RFK's funeral, particularly the train that took his body from a memorial service in New York City to his resting place at Arlington National Cemetery in Virginia, brought the country together in a new, unprecedented way: An estimated 2 million ordinary Americans spontaneously gathered beside railroad tracks to honor him as the train passed by. By Juliet Williams.  SENT: 500 words, photos.
STORAGE UNIT-BODIES
SALINAS, Calif. — A jury in Northern California convicted a 20-year-old man of torturing, starving and beating two children whose bodies were found in a storage unit. SENT: 300 words, photos.
Also:
— VETERAN SHOT — A Northern California city has agreed to pay $2.65 million to the family of a mentally ill Gulf War veteran fatally shot by police in 2014 after his family called 911.
— BUILDING FIRE — Authorities say a major fire ripped through an apartment building under construction in Northern California, injuring two people and forcing the evacuation of 250 people from a neighboring building.
SOUTHERN CALIFORNIA:
ARMENIAN MARCHES-CALIFORNIA
LOS ANGELES — Thousands of Armenian-Americans took to the streets of Los Angeles on Tuesday to commemorate the deaths of an estimated 1.5 million Armenians under the Ottoman Empire a century ago. The demonstrations came hours after Armenian Prime Minister Serzh Sargsyan resigned following 10 days of large anti-government protests in that country. By Krysta Fauria.  SENT: 350 words, video. UPCOMING: photos.
LOS ANGELES FIRE-SHOOTING
LOS ANGELES — One person was fatally shot at the scene of a house fire early Tuesday and a suspect was arrested after holing himself up for two hours inside the burned home south of downtown Los Angeles, authorities said.  Firefighters heard gunshots after quickly knocking down the flames in the two-story structure shortly around 4:30 a.m. in the Harbor Gateway neighborhood, Fire Department officials said. SENT: 350 words, photos.
Also:
— PARKED CAR DEATHS — Southern California investigators say all three men found dead inside a parked SUV were shot in the head.
— DEAD WOMEN — Authorities are investigating the deaths of two women believed to be mother and daughter who were found dead at a Southern California home.
— FATAL ROBBERY SPREE — Three men pleaded not guilty to offenses stemming from a fatal 2017 robbery spree in Oxnard.
BUSINESS
FACEBOOK-SPELLING OUT WHAT'S FORBIDDEN-GLANCE
NEW YORK — Facebook has revealed for the first time just what, exactly, is banned on its service in a new Community Standards document released on Tuesday. It's an updated version of the internal rules the company has used to determine what's allowed and what isn't, down to granular details such as what, exactly, counts as a "credible threat" of violence. The previous public-facing version gave a broad-strokes outline of the rules, but the specifics were shrouded in secrecy for most of Facebook's 2.2 billion users. Not anymore. Here are just some examples of what the rules ban. Note: Facebook has not changed the actual rules — it has just made them public. By Technology Writer Barbara Ortutay.  SENT: 700 words, photos.
YAHOO-DATA BREACH-FINE
WASHINGTON — The company formerly known as Yahoo is paying a $35 million fine to resolve federal regulators' charges that the online pioneer deceived investors by failing to disclose one of the biggest data breaches in internet history. The Securities and Exchange Commission announced the action Tuesday against the company, which is now called Altaba after its email and other digital services were sold to Verizon Communications for $4.48 billion last year. The Sunnyvale, California-based company, which is no longer publicly traded, neither admitted nor denied the allegations but did agree to refrain from further violations of securities laws. SENT: 450 words, photos.
E-CIGARETTE-CRACKDOWN
WASHINGTON — Federal health officials are cracking down on underage use of a popular e-cigarette brand following months of complaints from parents, politicians and school administrators. The Food and Drug Administration said Tuesday it has issued warnings to 40 retail and online stores as part of a nationwide operation against illegal sales of Juul to children. By Health Writer Matthew Perrone. SENT: 600 words, photos.
Also:
— ISRAEL-TWITTER BLOCK — A hard-line Israeli lawmaker is sarcastically thanking Twitter for briefly freezing his account following a post he wrote about wishing a teen Palestinian protester had been shot.
ENTERTAINMENT:
CINEMACON-STATE OF THE INDUSTRY
LAS VEGAS — Two film industry leaders told theater owners Tuesday that are optimistic about the movie and theatrical exhibition business despite concerns about declining attendance and competition from streaming services. New MPPA chief Charles Rivkin and John Fithian, the president and CEO of the National Association of Theater Owners, delivered a state of the industry speech at CinemaCon, saying the strength of the movies being released will dictate box-office sales. By Film Writer Lindsey Bahr. SENT: 550 words.
SPORTS:
COLLEGE CORRUPTION
RALEIGH, N.C. — College basketball played an entire season amid a federal corruption investigation that magnified long-simmering troubles within the sport, from shady agent dealings to concerns over athletes who'd rather go straight to the pros. Now it's time to hear new ideas on how to fix the complex, wide-ranging problems. By Basketball Writer Aaron Beard.  SENT: 775 words, photos.
With:
— COLLEGE CORRUPTION-WHAT'S NEXT?
DRAFT-RAIDERS PREVIEW
ALAMEDA — The approach Oakland Raiders general manager Reggie McKenzie takes into the NFL draft is no different with Jon Gruden as coach than it was in previous years with Jack Del Rio and Dennis Allen at the helm. "The board will still be doing the talking," McKenzie said. "It really will." By Pro Football Writer Josh Dubow.  SENT: 725 words, photos.
DRAFT-CHARGERS PREVIEW
COSTA MESA — For any NFL team with a star quarterback of a certain age, the draft is an annual chance to anoint a successor. It's also a chance for fans, pundits and the rest of the league to spend several months enthusiastically discussing whether that team will make the move. By Sports Writer Greg Beacham.  SENT: 950 words, photos.
Also:
— RAMS-MOVES — The Los Angeles Rams have picked up their fifth-year contract options for 2019 on running back Todd Gurley and cornerback Marcus Peters.
___
If you have stories of regional or statewide interest, please email them to losangeles@ap.org or sanfrancisco@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.