National editor Tom Karst and staff writer Ashley Nickle review the big stories of the week, including Mexican produce labeled as Canadian and high demand for mushrooms and limes.
MORE: Mushroom demand outpacing supply
MORE: Hot lime market to ease by April
MORE: Ontario greenhouse group responds to Amco verdict
MORE: Sonic’s Slinger opens the door for ‘shrooms