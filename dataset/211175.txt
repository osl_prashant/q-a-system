Chicago Mercantile Exchange live cattle futures landed in deeply bearish territory on Monday, hit by plentiful supplies and seasonally tepid wholesale beef demand, said traders.Fund liquidation accelerated market losses, they said.
August ended 2.700 cents per pound lower at 112.750 cents per pound, and October closed down 2.975 cents to 111.125 cents.
Both contracts finished below their respective 10-day moving averages of 113.743 and 113.170 cents.
More cattle are for sale than last week and packers are thought to have purchased enough cattle in advance for later delivery, said KIS Futures vice president Lane Broadbent regarding Monday's market selloff.
"If we have good beef demand, that will go a long way to helping us through this process. But if demand remains stubborn or lower, it's not going to be much of a fun ride," he said.
Last week wholesale beef values, or cutout, inched up from recent lows, giving the impression it had bottomed out seasonally, a trader said.
But, increased cattle supplies, and competition from abundant pork and chicken, present challenges for beef heading into the early-September U.S. Labor Day holiday, he said.
Investors wait to see how Monday's bearish technical and fundamental market factors impact slaughter-ready, or cash, cattle prices later this week. Last week packers in the U.S. Plains paid $116 to $119 per cwt for cattle.
Roughly 1,600 animals are listed for sale at Wednesday's Fed Cattle Exchange. Livestock there last week brought $116.000 cwt.
Technical selling, higher corn prices and CME live cattle futures fallout sent the exchange's feeder cattle market down sharply.
August feeders ended 3.700 cents per pound lower at 146.250 cents. 
Higher Hog Futures Close
Fund buying and Monday's steep wholesale pork price climb lifted CME lean hogs, said traders.
Investors simultaneously bought lean hog contracts and sold live cattle futures.
And CME lean hogs were bullishly underpriced to the exchange's hog index for Aug. 3 at 86.69 cents, the traders said.
August, which expires on Aug. 14, closed up 0.175 cents per pound to 83.400 cents. Most actively traded October ended 1.350 cents higher at 68.125 cents, and above the 20-day moving average of 67.346 cents.
Retailers bought pork in the event of a shortage after some packing plants on Monday closed for a floater holiday. Packers in August usually give employees time off in exchange for work during winter holidays.