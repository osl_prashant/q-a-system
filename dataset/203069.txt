Where pineapples are concerned, convenience leads to sales, marketers say, which typically means selling fresh-cut fruit.
"More value-added products are helpful to sales, making it simpler to eat a pineapple," said Gustavo Lora, category manager of pineapples with Robinson Fresh, fresh produce division of Eden Prairie, Minn.-based C.H. Robinson Worldwide Inc.
Since pineapples aren't as friendly of a fruit as, say, a banana, when sold as-is, it's important to break that barrier with user-friendly packs, Lora said.
"We're seeing convenient packs, such as chunked and cubed growing annually at 19% and 39%, respectively," Lora said.
The consumers' priority leans heavily toward ready-to-eat product, he said.
Retailers would do well to take note of that trend, Lora said.
"The best displays for pineapple include education to teach customers how to cut pineapples properly and when pineapples are ready to eat," he said.
The fruit doesn't necessarily need to have a gold exterior, Lora said.
"Pineapples are harvested ripe, so green is just fine - it doesn't affect the internal condition," he said.
Fresh-cut is a growing segment for Coral Gables, Fla.-based Del Monte Fresh Produce's pineapple business, said Dennis Christou, vice president of marketing.
"In recent years, we've found that fresh-cut pineapple varieties have helped increase the overall per-capita consumption of pineapples," he said. "With the rise of the on-the-go consumer looking for healthy options to accommodate their busy lifestyles, varieties must entail innovative packaging with wide-ranging product assortment to succeed in this category."
Christou cited, for example, Del Monte's Gold Extra Sweet Pineapple line, which includes whole pineapples, as well as fresh-cut cylinders, chunks, rings and spears in a multitude of sizes and packaging.
"We also launched our new Del Monte Fresh Cut Grab-N-Go pineapple cups, which have resealable packaging and fit conveniently in car cup holders," Christou said.
Mike Anderson, vice president of international procurement with Idaho Falls, Idaho-based Kingston Marketing Associates LLC, agreed.
"At Kingston Fresh, we are experiencing great increases in our sales with our fresh-cut and foodservice customers," he said.
That portion of the company's business is highly competitive and demanding, as the product has to be cut according to the customer's specifications, Anderson said.
Anderson credits support Kingston Fresh has gotten from its growers, as well as the attention to detail among workers at the farm level, for its ability to meet the needs of its customers.
Pre-cut and cored pineapples continue to sell well, said Alan Dolezal, vice president of sales for the tropical division of Coral Gables, Fla.-based Fyffes North America Inc.
"Shoppers are buying more pre-cut fruit than ever before," he said. "Many consumers enjoy pre-cut and cored pineapples because they are convenient and easy to take on the go."
New varieties also add value, said Karen Caplan, president and CEO of Los Alamitos, Calif.-based Frieda's Inc., which brings baby pineapples to the U.S. from South Africa.
"We ship them via air, so the customer base is a bit limited by the higher (freight) cost," she said.
Education about new varieties is a way to add value, said Robert Schueller, spokesman with Los Angeles-based specialty purveyor World Variety Produce, which markets under the Melissa's label.
"We do signage all the time for South African baby pineapples, as well as any new fruit to the U.S.," he said. "Signage is very key as a communication tool."
In the case of baby pineapples or Melissa's newest entry, the MonteLirio white-flesh variety, the pineapple is going to look different, and signage is particularly crucial, Schueller said.
"You have to market that in the display, especially when the retailer is not demoing it," he said. "People are going to pass by it and say, 'I don't know what it is,' so that education starts, and we go from signage to (point-of-purchase) materials to brochures, recipe cards, promotional and end-cap displays and attractive introductory pricing - that's a typical common thing retailers do when introducing the fruit - and really get the word out there."