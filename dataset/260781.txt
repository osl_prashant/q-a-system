BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Tuesday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMar         2583    2587    2551    2551  Down  14May                                 2555  Down  14May         2541    2559    2521    2532  Down  14Jul         2564    2583    2545    2555  Down  14Sep         2574    2590    2558    2568  Down  12Dec         2564    2578    2550    2560  Down  11Mar         2550    2560    2532    2542  Down   8May         2552    2565    2537    2547  Down   9Jul         2569    2575    2547    2556  Down  12Sep         2580    2585    2562    2567  Down  12Dec         2595    2603    2579    2583  Down  12