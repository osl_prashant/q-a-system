Georgia grower-shippers expect favorable late-summer weather to help produce typical fall sweet corn and green bean harvests.
Though July and August temperatures were high, the region experienced adequate amounts of rain, which were enough to keep the ground moist and should produce strong crops, said Jon Browder, sales manager for Belle Glade, Fla.-based Pioneer Growers Co-op, which grows and ships from Bainbridge, Ga.


Corn

Pioneer expects to finish plantings in mid-September and begin harvesting in early October, as usual, Browder said.
"The quality should be very nice," he said in late August. "We should have good volumes once we get started. It seems that we will have a good season."
Summer demand has been strong, and Browder said New York and Michigan typically overlap production with Georgia's start.
Michigan usually ends by late September while New York typically harvests through first freeze in late October, he said.
As the deal transitions to South Florida, Georgia production usually ends in mid- to late November.
Fresh Link Produce LLC, Lake Park, Ga., plans to begin harvesting in late September and finish in mid- to late November, said Steve Sterling, general manager.
"The corn looks very good," he said in late August. "There will be good and steady supplies. It will be a good item to have for the fall."
S.M. Jones & Co. Inc., Belle Glade, looks to begin the first week of October, said Ted Wanless, chief operating officer.
"Weather has been a typical challenge," he said in late August. "Rains and heat fairly are normal for summer weather.
New York and Ohio should begin finishing by late September. We project a slight overlap."


Green beans

Generation Farms, Lake Park, plans to begin harvesting green beans in early October.
The company began planting in early August and Jamie Brannen, general manager of produce, said he eyes a favorable season.
"We should have good quantity and good quality," he said in late August. "Bean demand this summer has been good. In the spring, the deal was so big, so prices were soft."
Brannen said he expects the transition from northern areas to Georgia to go well.
Generation Farms plans to harvest from 800 acres, similar to last year.
J&S Produce Inc., Mount Vernon, Ga., began planting Aug. 10 and expects to begin harvesting on time on Oct. 1.
Joey Johnson, president, said the company, which packs for 50 growers in 15 counties, plans to harvest consistent weekly volume through Thanksgiving.
"Some plant sooner, but we find we don't get good stands if we plant too early," he said. "There's a lot of overlapping if people try to go in too early. Buyers should expect good supplies and good quality. The quality will be good as long as we have good weather."
J&S expects to harvest from 400 acres which also includes some KY and cranberry beans.
Lake Park-based South Georgia Produce Inc. planned to start harvesting Sept. 15, as usual.
"The beans look good," saleswoman Brandi Hobby said in late August. "They have great stands. We're expecting more demand."
Hobby said Georgia's typical Nov. 10 finish is ideal because on Nov. 5 South Georgia Produce usually begins harvesting in South Florida near Immokalee, Fla.
Fresh Link is expecting an Oct. 1 start with volume to commence immediately, Sterling said.
Fresh Link's Fairfield, N.C., deal, in the northeast part of the state, usually begins in late September and typically overlaps with the company's Georgia production, he said.
"The beans have had good weather for planting," Sterling said in late August. "There should be good supplies and good quality."
Pioneer planned to start in early October.
The transition from New York, New Jersey, Michigan, Pennsylvania and Tennessee usually works well, Browder said.
"I expect us to have normal volumes as usual," he said in late August. "We should have good quality depending on weather."