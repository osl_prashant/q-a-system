Offering the latest and greatest services only matters if people know about them. 
A recent survey showed 39% of shoppers didn’t know whether their primary grocer offered online ordering, and 37% didn’t know whether their primary grocer had an app.
 
That matters because people generally try out these new options if they know they are available.
 
Market Force Information surveyed more than 12,000 people for its study on grocery and found engagement in the programs and awareness of them are highly correlated.
 
The vast majority of people who know about their grocer’s app have used it, according to the survey. The same goes for loyalty programs.
 
“Click and collect” and delivery are still in their infancy with many grocers, but more people are trying those programs as they become available.
 
Eight percent of those surveyed said they had ordered online and had their groceries delivered in the last 90 days, up from 5% last year. The number of people who said they used the pickup model in the last 90 days also grew, from 2% to 5%.
 
Those percentages, as one might expect, are higher for some age groups than others. Among those 25-34 years old, 10.5% said they had groceries delivered in the last 90 days and 7.8% said they had used grocery pickup in that span. Among those 35-44 years old, 9.1% had used delivery and 7.1% had used pickup.
 
Wal-Mart and Kroger lead the “click and collect” model, according to the study. Seeing that finding reminded me that the last time I visited a Wal-Mart — a Neighborhood Market in the Kansas City area, where The Packer is based — a large banner hanging from the ceiling proclaimed the online ordering option. 
 
I have since tried out the company’s pickup service.
 
As retailers add services to meet the convenience needs of their customers, they would do well to publicize those time-saving opportunities. 
 
Maybe the image of the app could be printed on the receipt with the message “Save Time — Order Online” underneath it, instead of the company logo. 
 
In-store signage can be used to increase awareness of new offerings, or employees can tell customers using coupons at the register that they can download the app to get discounts without cutting out coupons and having to carry them around.
 
An important point to note — investing in an app for coupons shouldn’t diminish the importance of printed circulars at all. More than 75% of those surveyed said they still review them to find good deals, and many of those people said they planned what to shop for and where to shop based on those circulars.
 
The app is just another opportunity for convenience, perhaps the most-used buzzword in discussion of industry trends today.
 
In addition, apps and loyalty programs in particular should be able to provide data about customers, which can allow retailers to better understand who they are appealing to most and what groups they need to work harder to reach.
 
How much of the population will switch in the coming years to online grocery shopping remains to be seen, but if retailers are offering that service, they should make their customers aware of it so they can better grasp what the demand for it is — and likewise for their other programs, too.
 
Ashley Nickle is a staff writer for The Packer. E-mail her at anickle@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.