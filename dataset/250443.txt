Fishing industry in Thailand fixing some abuses, agency says
Fishing industry in Thailand fixing some abuses, agency says

By KAWEEWIT KAEWJINDAAssociated Press
The Associated Press

BANGKOK




BANGKOK (AP) — Thailand's fishing and seafood industry has made some improvement in working conditions, including less physical violence, but problems such as unfair pay and deception in contracting persist, a survey conducted by the U.N.'s International Labor Organization found.
The European Union in 2015 gave Thailand a "yellow card" on its fishing exports, warning that it could face a ban on EU sales if it didn't reform the industry. Thailand's military government responded by introducing new regulations and setting up a command center to fight illegal fishing.
The ILO report released Wednesday on "Ship to Shore Rights" recommends the Thai government strengthen its legal framework, ensure effective enforcement, establish higher industry standards and enhance workers' skills, knowledge and welfare.
"We want competitiveness in the global seafood trade to mean more than low prices and high quality," Graeme Buckley, ILO country director for Thailand, Cambodia, and Laos, said at a news conference. "We want it to mean decent work for all the industry's workers, from the boat to the retailer."
A Pulitzer Prize-winning Associated Press investigation in 2015-16 that uncovered severe rights abuses affecting migrant workers in Thailand's fishing and seafood industries helped focus attention on the problem. The AP's stories helped free more than 2,000 enslaved men from Myanmar, Cambodia, Thailand and Laos, and led to more than a dozen arrests, amended U.S. laws and lawsuits seeking redress.
The ILO said changes in Thailand's legal and regulatory framework had contributed to positive developments since the group's last survey of workers in 2013.
It said only 6 percent of fishing boat workers in 2013 had a signed or written contract with their employers, but a study undertaken in 2017 found 43 percent of the respondents recalled signing a contract.
"Another possible sign of progress is the type of abuses reported," said the ILO. "Although 12 percent of all workers surveyed this year reported harassment or verbal abuse — and 7 percent faced threats of violence at work — reports of physical violence were relatively few, at 2 percent of all workers surveyed."
The AP investigation in 2015 had documented multiple cases of physical abuse and depravity.
The apparent gains for workers in the industry were offset somewhat by persistent abuses noted by the ILO.
"One third of workers reported being paid less than the legal minimum wage, before any deductions were made," the report said. "As many as 53 percent of respondents cited deductions made to their monthly earnings."
Evidence of forced labor "including deception in recruiting or contracting, wage withholding, and widespread identity document retention among fishers," was another concern noted in the report.
The ILO surveyed 434 workers in 11 Thai provinces in March-April 2017. The report said 125 Cambodians, 287 Burmese and 22 Thai nationals who worked either on fishing boats or in seafood packaging factories took part in the research, which asked their "personal demographics, how they were recruited, if they had a contract, what they earned, what their working conditions were like, hours worked, satisfaction with their accommodations, benefits received and how they reported grievances."
It warned, however, that the survey's results were not representative of Thailand's entire fishing and seafood processing industry.
The survey, in particular, did not include workers on long-haul boats, which fish in international waters and where abuses are more likely to occur. Those boats do not return to port as often, which the ILO said made interviewing those workers difficult. It interviewed only workers on short-haul fishing boats, those at sea less than 30 days at a time, but said there are now few Thai-flagged vessels engaged in long-haul fishing.