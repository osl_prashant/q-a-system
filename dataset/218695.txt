The National Cattlemen’s Beef Association today unveiled its 2018 Policy Priorities, which will guide the group’s lobbying efforts in Washington over the coming year. The document was released at the annual Cattle Industry Convention in Phoenix.

This year’s priorities focus on five main categories: the 2018 Farm Bill, Trade and Market Access, Regulatory Reform, Antimicrobial Use, and Fake Meat.

Some of this year’s priorities are familiar to longtime industry watchers. Like last year, NCBA will work to ensure that the pending Farm Bill includes full funding for a foot-and-mouth disease vaccine bank, protects conservation programs like the Environmental Quality Incentives Program (EQIP) and prevents market-disrupting policies like mandatory Country of Origin Labeling (COOL).

Likewise, the group’s regulatory-reform efforts will again focus on finding a permanent solution to an electronic logging devices mandate, modernizing the Endangered Species Act, and replacing the 2015 Waters of the United States (WOTUS) rule.

New to the Priorities list this year is an emphasis on antimicrobial use - specifically the aim to secure clean Animal Drug User Fee Act (ADUFA) reauthorization and continuing the Key Technologies Task Force action steps on antimicrobials. Another new emphasis in 2018 will be a focus on protecting the industry and consumers from fake meat and misleading labels on products that do not contain real beef.

“With tax reform, regulatory rollbacks, and new access to the Chinese market, we had some big victories in Washington last year, but this is no time to take a break, and 2018 promises a mix of new and familiar challenges,” said incoming NCBA President Kevin Kester, a fifth-generation California rancher. “We’re going to continue to ensure fair access to foreign markets, fight against unnecessary regulation, make sure the Farm Bill addresses our needs, and guarantee that consumers have the ability to purchase a safe, healthy, and accurately labeled protein source.”

The group’s full 2018 Policy Priorities document can be seen here.