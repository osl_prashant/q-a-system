Albuquerque school tackles teen obesity
Albuquerque school tackles teen obesity

By SHELBY PEREAAlbuquerque Journal
The Associated Press

ALBUQUERQUE, N.M.




ALBUQUERQUE, N.M. (AP) — Camron Chavez, 12, can tell the meals at South Valley Preparatory School are a little different.
He notices that the food he gets at school is healthier and that he feels more full throughout the day.
"I don't really eat food like this at home," Chavez said during his lunch period.
The difference is South Valley Prep's healthy eating initiatives for its students, who are primarily Hispanic, from low-income homes and living in a state ranked No. 46 in the nation for overweight and obese children.
A State of Obesity report ranked New Mexico 46th — with first being the healthiest — after finding that 24.9 percent of 10- to 17-year-olds in New Mexico were overweight or obese in 2016.
In the wake of this, South Valley Prep allows no junk food on campus during school hours for students and staff, offers a menu of healthy dishes with a variety of grains and local veggies, and takes kids to a farm every week for "land-based physical education."
In between gobbling down some sesame noodles, Chavez told the Journal that the food he gets at his state-authorized charter school tastes "sweeter" and "doesn't have any pesticides."
The middle-schooler learned about pesticides while on a visit to the Sanchez Farm — something a group of students at the school does each Friday.
The group rotates every week, giving each kid a chance to visit the farm.
FARM TO TABLE
At Sanchez Farm, farmers teach students about food, some of which lands on their plates at school, and show them where healthy food comes from in a hands-on way.
"I like the food isn't just microwaved and it's not just from packages," Chavez said.
Too often kids think food comes only from fast-food chains or in an aisle of a store, said food service member Anthony Shemayme.
Shemayme, who was passing out organic blue cornbread to students, said the lessons the kids get on the farm make them more autonomous and hones their work ethic.
"It's fun, but it's hard work," 12-year-old Rosie Kenton said about the farm days.
Kenton looks forward to the trips, saying she likes being able to take care of her body.
The Title I school could have its meals entirely paid for by government funding. But instead, South Valley Prep opts to put about $10,000 of its own budget into the meal program, which is a combined effort between catering company Swan Kitchen and nonprofit La Plazita.
Principal Charlotte Trujillo said some schools are able to make money on government-funded lunches, putting leftover funds into their own budgets.
She recognized that her school could use the money elsewhere but said she really believes in the importance of the project, which is partially paid for through grants.
PROOF IS IN THE PUDDING
And while the principal said it's not easy to get 156 middle-schoolers to eat healthy, she's noticed kids being less sluggish, participating more in class and getting fewer stomach aches after lunch.
South Valley Prep serves up a substantial amount of food, providing breakfast, lunch and even dinner to some of the students. Mondays through Thursdays, the school serves an average of 30 dinners a night and about 120 lunches a day, according to Trujillo.
South Valley Prep's menu changes depending on the season, but typical meals include taco salad, beans and rice, enchiladas, quinoa and beef and potato burritos.
The endgame of the project is both keeping kids healthy and removing as many barriers to learning as possible.
Trujillo noted that it's a balancing act: spurring a conversation around what kids are putting in their bodies while also letting them have fun.
"If we have a school dance and want to sell nachos or if we want to host a bake sale, then we do," she said.
Students can also bring their own lunches to the school as long as it's not junk food.
The farm-to-table program is only in its third year, but Trujillo said she plans to keep it going as long as she can.
BODY WEIGHT ISSUE
In 2015, the New Mexico Department of Health found healthy body weights are also an issue in the grades leading up to middle school. More than one in three third-grade students and one in four kindergarten students in the state were overweight or obese, the report found.
It also found the rates are highest among Hispanic and American Indian students. For adolescents as a whole in 2015, 15.6 percent were obese, compared with a national rate of 13.9 percent.
And research shows income levels are correlated to childhood obesity rates.
The Food Research and Action Center found some of the income-related hurdles families face include: lack of full-service grocery stores and farmers' markets, access to transportation to haul a lot of groceries home, and healthier foods often come with a higher price tag. And stress levels, fewer physical activities resources and limited access to health care are also factors, according to FRAC.
"Really, these families don't have the access to this kind of food," Trujillo said.
___
Information from: Albuquerque Journal, http://www.abqjournal.com