U.S. Department of Agriculture’s recent crop progress report shows 24 percent of the sorghum is already planted, one point ahead of average. Louisiana making the most progress, with planting pace running 29 points ahead of the average pace. Texas is a close second, with planting 21 points better than average. 
 
As other states prepare to plant, the trade tension between the U.S. and China is looming over their decision. In mid-April, China ramped up its accusations, temporarily slapping a 178 percent tariff on U.S. sorghum.
 
“At 178 percent tariff, it pretty much totally stopped everything immediately,” said Tim Lust, National Sorghum Producers (NSP) CEO. 
 
Lust says it was the tariff announcement that acted as a barricade for ships already making the trek to China. 
 
“There was a 24-hour notice given for entry into China,” said Lust. “It wasn't a 24-hour notice for ships being loaded in the United States; it was a 24-hour notice of the ships entering into the harbors in China.”
 
Reuters reported last week as soon as the tariff news hit, several ships loaded with sorghum turned around at sea, heading back to the U.S. 
 
“Over the last few days, we've been able to find new home for a lot of that sorghum that was on the water,” said Lust. “Certainly Spain and Saudi Arabia have stepped up and made large purchases of sorghum.”
 
While some of the shipments are being exported to other destinations, a portion of that grain may need to be absorbed domestically, likely in the U.S. feed supply. Lust says the sorghum industry is also working furiously to get a sorghum oil pathway approved, allowing it to be used for biodiesel. 
 
“One of the issues is while that pathway has been a preliminary rule, it's not been made final,” said Lust. “Part of the discussions we've had with EPA is the next step becomes very important, because there are ethanol plants that won't buy sorghum today because they're not allowed to sell their sorghum oil to biodiesel.”
 
At the same time, Lust says National Sorghum Producers (NSP) is fully cooperating with China's ongoing anti-dumping and counter vailing duty investigation. 
 
“I think the challenge for us is we did submit over 2,000 pages of documentation about the relationship we have - that's a very win, win relationship - with our customers in China.”
 
NSP is adamant – and has been adamant from the start- that the U.S. is not dumping sorghum into China illegally. As China's accusations stew, Lust says it's the investigation that’s consuming the sorghum industry today. 
 

“We've done a tremendous amount of playing defense and dealing with the cards that were dealt to us,” said Lust. “There's an extensive process that's laid out as part of these cases, and it takes a lot of hours and a lot of resources.”
 
Lust says it's not just the U.S. sorghum industry wanting to see the case resolved. Those sentiments also reside with major buyers of U.S. sorghum. 
 
“We have a set of customers asking these same questions right now about how are they going to get their hands on grain they need, what is it going to do to their cost of production and what's it going to do to food costs in China,” said Lust. 
 
It’s a case Lust acknowledges is politically charged. As President Trump's top economic advisors plant to head to China next week—to discuss major issues like trade—Lust understands politics may be the solution. 
 
“As our negotiations get going between our two countries, there's an opportunity for us to be able to be removed as a part of that, and that is a goal and ask and an ask of certainly our organization, but our customers in China, as well,” said Lust. 
 
The weight of China is trickling over to farmers. When the tariff news hit, prices fell, with some farmers questioning their decision to plant sorghum this year. While it may impact final acreage this year, trade is a wildcard. Lust says trying to outguess trade situations is never a safe bet. 
 
“There's a lot of people working day and night to make sure that their opportunities to grow a profitable crop continue,” said Lust. “We want growers to know that there have been hiccups before. It's not the first challenge this industry or agriculture has ever had. We made it through those, and we'll make it through this one again.”