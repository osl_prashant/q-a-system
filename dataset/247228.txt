President Trump’s proposed steel and aluminum tariffs is drawing criticism from those in the agricultural industry, but also from his own political party.

Although House Speaker Paul Ryan opposes the plan, Trump says the U.S. won’t be “backing down” on the 25 percent tariffs on steel imports and 10 percent on aluminum.

Roberto Azevedo, director-general of the World Trade Organization (WTO) is also expressing his concerns about these increases, and the WTO is concerned about the escalation risk.

Michelle Rook reports that U.S. farmers are assessing how negative a trade war could be for U.S. agriculture on AgDay above.