Artificial intelligence can be found in your home with Amazon Alexa, Google Home, and robots, and now it can soon be used to arm dairy cows.

A Dutch company has married artificial intelligence with motion sensors to create intelligent diary farmer’s assistant (IDA) for the cow to wear.

Carrie Antlfinger of the Associated Press has the story on AgDay above.