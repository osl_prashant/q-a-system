With its patented shelf life-extending coating — Edipeel — approved for organic and conventional produce, Apeel Sciences is marketing to commercial shippers in the U.S. and small growers in developing countries. 
Commercial use of Apeel in the U.S. has begun with a group of organic growers in Santa Barbara, Calif., Apeel CEO James Rogers said in June. Having received the Food and Drug Administration’s GRAS (generally recognized as safe) designation in February, the product also received approval for use on organic produce and is listed by the Organic Materials Review Institute, Rogers said.
 
The company is scaling up production this summer and Rogers said the product will be used on fresh produce in packinghouses this year, although he did not disclose the names of shippers who plan to use Apeel.
 
Winning confidence from investors for the product and marketing plan, the company recently moved into a larger facility for its 85-member staff in Santa Barbara.
 
The company’s website describes the postharvest treatment as “edible products from natural plant extracts” that allow organic and conventional growers improve quality and shelf life. The company has raised tens of millions of dollars in funding to commercialize the concept, Rogers said.
 
The product, manufactured in powder form, is applied as a liquid to fruits and vegetables with a spray bar or similar methods in a packinghouse setting.
 
Building momentum
 
In searching for an answer to hunger problems and the related issue of food waste, Rogers said he used the inspiration for Edipeel in the way the steel industry used a thin barrier on iron to create stainless steel.
 
“If perishablility is the cause of people going hungry and the cause of perishablility is water loss and oxidation — and steel had the same problem and solved it with a thin barrier — could we take that idea and apply it to fresh produce to reduce perishablility?”
 
Rogers created the proprietary product by using materials found in the produce itself to create the thin, tasteless barrier on the outside of the produce. The road to commercialization, Rogers said, began with a new venture competition five and a half years ago at the University of California at Santa Barbara that won $10,000.
 
“I used that (money) to incorporate the business as a legal entity, where we were then able to apply for financing from some of the grant-making organizations,” he said. Apeel received $100,000 from the Bill and Melinda Gates Foundation.
 
“We used that capital and leveraged that to raise $1.25 million in private financing in January 2014,” Rogers said.
 
From there, Apeel used the money to develop proof of concept that the technology would work. Using that proof of concept to raise $6 million in a round of financing that closed in 2015. Rogers said that Apeel has used the money to scale manufacturing from the milligram to the kilogram level.
 
Apeel then received $1.5 million from the Bill and Melinda Gates Foundation to make the product available to smaller growers in developing countries.
 
“We took the body of that work and used that to raise a $33 million round of financing that we closed in November of last year that we are using to scale out and develop the business,” Rogers said.
 
In essence, he said, Edipeel can be used in place of cold storage, which extends shelf life and reduces the respiration rate of fruit and vegetables.
 
“By controlling the properties of that barrier, we can develop a modified atmosphere inside that produce itself, which then travels with the produce throughout the value chain,” he said. Apeel is working with produce suppliers in places like Kenya and Nigeria.
 
“They hardly have roads, let alone an electric grid, let along refrigeration equipment to operate a cold chain,” he said.
 
“We are able to come in there with a little pouch of our material, have them boil some water, mix in the (material), let it cool and dip their mangoes in there and have it last for an extra 10 days,” Rogers said.
 
Rogers said the technology allows producers in those countries to export and ship farther.
 
“We have been very fortunate to get to work with people that really need it in the developing countries,” he said.
 
In the U.S., there also is commercial value in being able to extend the shelf life, he said.
 
“Up until now, we have had one knob to turn, and that’s the temperature knob,” he said.
 
“What we are saying is there is a couple of knobs you can turn, and when you can turn those knobs in combination, you can save energy, you can improve quality and you can create a new type of value chain,” he said.
 
Rogers said the benefits of controlled atmosphere have been limited to the amount of time the produce was in the environment. 
 
“As soon as you open the CA container, you lose the CA and fruit starts to rot,” Rogers said. “And that’s a problem for the retailer and it’s a problem for the end consumer,” he said.
 
With Apeel, Rogers said CA is inside the product itself.
 
“Now not only does the shipper get the benefit of the product, but also the retailer and also the end consumer,” he said.
 
Applications
 
Apeel has worked with about two dozen types of produce, Rogers said. The product adds five to seven days of shelf life for bananas, asparagus, avocados, green beans, mangoes, tomatoes and other commodities, he said.
 
The product is different than ethylene inhibitors or products like waxes, Rogers said.
 
“(Apeel) offers an overall extension of shelf life, not just an insurance product that you won’t get premature ripening,” he said. 
 
“We are just slowing down the rate of respiration, and the (produce) ends up lasting longer,” he said.