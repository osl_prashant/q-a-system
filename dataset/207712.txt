Pacific International hires two salesmen
Salinas, Calif.-based Pacific International Marketing Inc. has hired Bill Wynne for sales of conventional and organic produce.
Wynne worked as a salesman for Vilmorin, a seed company in Salinas. He has 29 years’ experience in the produce industry.
The company also hired Jerome Carlisle as a sales assistant. He is a newcomer to the produce industry, having previously worked as a bartender and linen salesman.
 
Tanimura & Antle invests in workers
In 2016, Tanimura & Antle opened 100 two-bedroom units for workers at its Salinas, Calif., campus.
The units can house 800 people at a monthly cost of $125 per bed, said Anthony Mazzuca, senior director of commodity management.
The company also offers full benefits to its harvest crews (not contract labor).
These benefits include 401(k), health, vision, dental and the opportunity to participate in the Employee Stock Ownership Plan that Tanimura & Antle launched earlier this year.