BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Fri. Apr 20, 2018          USDA Market NewsDaily California EggsAPRIL 20, 2018Benchmark prices are unchanged. Asking prices for next week are unchangedfor Jumbo, Extra Large and Large and 2 cents higher for Medium and Small.The undertone is steady to about steady. Offerings are mostly moderte.Demand is in a full range of light to fairly good. Supplies are light tomoderate. Market activity is slow to moderate. Small benchmark price $1.27.CALIFORNIA:Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                212EXTRA LARGE          202LARGE                196MEDIUM               147SOUTHERN CALIFORNIA:PRICES TO RETAILERS, SALES TO VOLUME BUYERS, USDA GRADE AA AND GRADE AA,WHITE EGGS IN CARTONS, DELIVERED STORE DOOR,CENTS PER DOZEN.RANGEJUMBO                199-211EXTRA LARGE          190-197LARGE                184-191MEDIUM               135-142Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 20-Apr-18 11:01 AM E MP