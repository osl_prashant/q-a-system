Gilroy, Calif.-based Uesugi Farms plans to launch an organic sweet corn overwrapped tray in June, said Pete Aiello, co-owner.
"It's four ears of corn in a Styrofoam tray with the overwrap," he said.
After the rollout in June, the product will be available year-round - from April through October out of California and November through April from Mexico, Aiello said.
The company is still working out details, such as packaging designs, crop plans and pricing, Aiello said.