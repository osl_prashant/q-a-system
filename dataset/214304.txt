Rao Mandava, president of Inteligistics, Pittsburgh, Pa. and Erick Kithinji, lead electrical project engineer for Inteligistics show the company's new Pulse unit.
NEW ORLEANS — Pittsburgh, Pa.-based Inteligistics is commercializing a new service that provides real-time monitoring of remaining shelf life for fresh produce.
Called Pulse, the system is built on a cellular-connected unit that can provide multiple internal product temperature probes, GPS tracking, multiple sensing options (shock, light, ethylene and more), real-time access to data and centralized cloud storage, said Rao Mandava, president of Inteligistics.
“Consumers are demanding better quality and that’s driving the industry,” he said. “It’s only a matter of time before you have to establish what the product has been through in terms of conditions.” 
Mandava said the use of shelf life data can improve the bottom line and increase customer satisfaction.
The company has commercialized the Pulse platform this year has hundreds of units deployed, with talks to put in place many thousands more, he said. The company is talking with grower-shippers, transportation companies and retailers about the Pulse service, he said.
Cost of the Pulse service is about $70 per month per unit leased, and that cost includes software and access to all data, he said. Discounts are available with volume, 
The company recently completed a pilot with a retailer — Mandava did not identify the retailer — that tracked produce all the way from the harvest point to the store, with the retailer using Pulse data on shelf life and then validating the results based on samples.
Having access to shelf life data is key for retailers, he said.
“First-in first-out won’t cut it; this is a real way of getting better quality while minimizing waste,” he said.
The company already offers several options in temperature visibility solutions for the supply chain, Mandava said.
The company’s InteliTemp platform tracks temperatures from harvesting to cooling. InteliCool delivers temperature monitoring during cooling, and InteliShip specializes in temperature monitoring during shipment.
Mandava said Inteligistics works with about 15 grower-shippers with the InteliTemp platform monitoring product conditions from cutting to cooling.