Capricious weather patterns in the middle of the U.S. don't mitigate consumer enthusiasm for produce grown around the St. Louis area, but they can have a halting effect on supplies, suppliers say.
And, there was plenty of product to be had this year - perhaps too much, in some cases, said Dominic Greene, vice president of operations and sales manager with St. Louis wholesaler United Food and Produce Co. Inc.
"We're seeing the usual amounts of corn, and the market is fairly depressed right now; that has probably spurred some of the promotions in the area regarding corn," he said.
Normally, St. Louis area-grown produce "commands a premium from growers," but this year, suppliers were ample to prompt extra ad promotions, Greene said.
"We've seen success in green peppers and other commodities, where in prior years, you probably wouldn't see an ad promotion," he said.
The tomato market also was "flush" with product this year, Greene said.
There might have been even more product available, were it not for occasional episodes of extreme weather, Greene said.
"Some of the corn got burned up and some watermelon has had some issues, as well, and that has affected the supply of melons down through (extreme southeastern Missouri), so the weather hasn't been ideal," he said.
However, he noted, there has been plenty of product of "reasonable quality," Greene said.
"You got these periods with the burn-up in the fields, you do see perishing items, but retailers have been moving product they get fairly well, we think," Greene said.
In all, the local deal went smoothly, said Dan Pupillo, president of St. Louis wholesaler Midwest Best Produce Co. Inc.
"Rain hampered us early in the deal, but we finished out the last month pretty smoothly," he said, noting that his company was about to finish the Missouri deal and was in the midst of its Indiana program, which likely would finish by early September.
Weather was a pressing issue particularly early in the deal, said Dale Vaccaro, president and owner of Vaccaro & Sons Produce Co.
"It was better earlier, and we had a pretty good hot spell and that knocked some of the local stuff down a little, but it's OK," he said, noting that corn and bell pepper supplies were ample but tomatoes were down, due to heat issues.
"Overall, it has been a pretty good season," Vaccaro said.
R.J. Barrios, buyer for Sunfarm Foodservice Inc. in St. Louis, said the local deal was less productive this year than in others.
"Our St. Louis area is not producing as it was last year," he said. "Tomatoes aren't popping like they were last year. Melons are very bland. It's been hot, humid, a swamp. We've gotten a lot of rain."
Steve Duello, produce director at Dierbergs Markets in Chesterfield, Mo., said the local deal started late and likely would finish earlier than usual - by Labor Day or "soon thereafter."
He said the local deal, in general, turned out well.
"It continues to be more and more popular, as customers like the fact that we support the local economy, local small family farms and those types of things, Duello said.
Temperatures in the low 90s are typically part of the deal, but 2016 had a wetter bent than other years, which had some effect on the crops, Duello noted.
"It's been a pretty good typical summer but probably a little more moisture than usual," he said.
Too much rain and heat were a caused a shortage in some categories, said Greg Lehr, produce director at Straub's Markets in Clayton, Mo.
"Weather drives supply, and that is your problem," he said. "You trend in and out of it and try to stay as consistent as you can, but it's tough with the weather."
St. Louis' largest retail chain, Schnuck Markets Inc. sources produce from about a 300-mile radius, said Steven Mayer, vice president of produce.
He said 2016 turned out better than the previous year.
"Last year was a tough year for local because it would rain and get hot and rain and get hot and rain and get hot; This has been a better year, so I'd say we're quite happy with the results," he said.
Schnuck's stores feature up to 30 locally grown items each year, Mayer estimated.