Routine laboratory forage analysis of corn silage typically includes percent starch determination for the feed. Nutritionists utilize this information to partition Non-Fiber Carbohydrate (NFC) portions into percent starch, percent sugar and other NFC components to optimize production and animal health.Grain per ton of 65% silage ranged from zero to 11 bu. per ton for individual samples in a University of Wisconsin (UW) study. Previous studies have shown an increase in grain per ton of silage for traditional grain hybrids as silage tonnage per acre increased.
How can the corn silage starch test be used to estimate grain yield?
All that is needed is the yield of corn silage and the starch analysis. Corn grain is 72% starch on a dry matter (DM) basis. The grain is the only source of starch in the silage. If the total yield of starch can be determined it can be directly converted to grain. An example would be a field that yielded 25 tons of 65% moisture forage. A forage test indicates the silage contains 33% starch. The DM yield of starch is:
 
DM starch yield  = as-fed silage yield × % dry matter × % starch (DM basis)     (example)           = 25 tons × 2,000 lb./ton × 0.35 (1-moisture) × 0.33
                                  = 5,775 lb. starch
 
Grain yield          = lb. starch ÷ (% starch content) ÷ (DM % of shell corn/56 lb./bu.)
     (example)        = 5,775 lb. starch ÷ 0.72 ÷ 0.845
  = 169.5 bu./acre
 
or simplified       = lb. starch × 0.0293
 
How can the silage starch test estimation of grain yield be used?
The results of this estimation are accurate for the yield on the date of harvest as corn silage. This information can help dairy herd managers estimate the amount of supplemental corn that will be needed to feed the herd prior to the grain harvest. It is an accurate estimation of the actual grain yield and should be used for reporting grain yields for Farm Service Agency (FSA) programs such as Loan Deficiency Payments (LDP) or to establish crop yield history for crop insurance.
What factors influence accuracy of the estimation?
In field conditions the estimation of gross tonnage of corn silage is often the biggest source of error. Loads of silage delivered to the silo must be weighed and counted. Wagons or trucks may be of different size and filled to different degrees. It is important to weigh a significant number of loads to establish accurately the weight of silage delivered to the silo.
Alternately the volume of the silage mass in the pile, bag, bunker or tower can be estimated and combined with a measurement of average density to come up with gross yield of silage. The volume of irregularly shaped piles is difficult to measure and density is often not uniform. Tower silos need time to settle to estimate the settled volume of feed.
In summary, starch analysis is a useful, inexpensive, fast and convenient tool to more accurately estimate the grain of yield of corn harvested as silage.
To read more about starch analysis, click here.