The Houston Livestock Show and Rodeo saw the top two market steers bring a total of $776,000 and the Reserve Grand Champion set a new world record.
A Charolais steer owned by Cameron Conkle, Allen, Tex., won Grand Champion at the Houston Livestock Show and Rodeo in competition with 1,739 other steers.
The steer, named Loki, sold Saturday for $410,000 in the Houston Livestock Show and Rodeo Junior Market Steer Auction. Buyers were Emily and Robert Clay; Becky and Kelly Joy; Julie and Alan Kent; and Debby and Jeff Young.
Debby Young said her group chooses to purchase livestock at the Houston Livestock Show and Rodeo auctions to support the hard-working youth of Texas.
“We give from our hearts, and we’re proud of Cameron and all these kids who work so hard,” Debby Young said.
Conkle is a junior at Allen High School located north of Dallas, and is a member of the Allen FFA.
The Reserve Grand Champion Junior Market Steer was shown by Jett Hale, a 10-year-old from the Roberts County 4-H in Miami, Texas. His steer was purchased by a group calling themselves the Champagne Cowgirls for $366,000.
The sale of the Reserve Grand Champion beat a record for the second place steer pricing that was set in 2001. 
The Houston Livestock Show and Rodeo promotes agriculture by hosting an annual, family-friendly experience that educates and entertains the public, supports Texas youth, showcases Western heritage and provides year-round educational support within the community. Since its beginning in 1932, the Rodeo has committed more than $450 million to the youth of Texas.