BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Thursday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2584  Down  45May         2581    2607    2545    2556  Down  41Jul         2613    2635    2573    2584  Down  45Sep         2625    2648    2585    2595  Down  49Dec         2622    2648    2586    2593  Down  52Mar         2609    2632    2572    2578  Down  53May         2621    2622    2575    2582  Down  53Jul         2629    2629    2585    2589  Down  53Sep         2630    2632    2595    2595  Down  55Dec         2637    2639    2599    2599  Down  57