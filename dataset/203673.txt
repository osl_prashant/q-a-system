The Tampa, Fla.-based IFCO Systems has supported charitable causes including Feeding America and other food banks.
On Aug. 11, as part of IFCO's annual Aug. 10-11 management meeting in Tampa, company workers helped Feeding Tampa Bay by volunteering time to sort food at the food bank.
Feeding Tampa Bay provides food to food banks and organizations in 10 counties in the Tampa region.
The organization serves meals to more than 700,000 needy people in west Central Florida, according to a news release.
"We are very grateful to companies like IFCO who work hard to help us fulfill our mission," Megan Carlson, Feeding Tampa Bay's volunteer services manager, said in the release.
IFCO's food bank group organized the volunteer effort well and kept participants who sorted many different types of food on-task, Dan Walsh, president of IFCO's North American operations, said in the release.
"We were very happy to help," Walsh said in the release. "There are few things more rewarding than working to help those who struggle with hunger. We are delighted to have the chance to contribute and to have our entire IFCO management team participate in this effort."
Feeding Tampa Bay distributes food donated by farmers, procesosrs, supermarkets and community food drives to hungry people through a network of more than 500 faith-based and other nonprofit hunger relief organizations.
Since 2011, the organization has supplied more than 100 million pounds of food, which equates to more than 3 million meals a month.
IFCO is a pallet and reusable plastic container provider.