BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Friday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMar                              120.15  Down  .15Mar      119.75  119.75  118.85  118.85  Down  .15May                              122.40  Down  .20May      120.35  121.30  119.85  120.15  Down  .15Jul      122.70  123.50  122.10  122.40  Down  .20Sep      125.00  125.70  124.35  124.60  Down  .25Dec      128.60  129.05  127.70  127.95  Down  .30Mar      131.70  132.45  131.20  131.40  Down  .30May      133.95  134.35  133.40  133.60  Down  .30Jul      135.85  136.20  135.50  135.50  Down  .30Sep      137.50  137.85  137.15  137.15  Down  .35Dec      140.00  140.35  139.65  139.65  Down  .35Mar      142.50  142.85  142.15  142.15  Down  .40May                              143.85  Down  .45Jul                              145.55  Down  .45Sep      147.30  147.65  147.15  147.15  Down  .45Dec                              149.55  Down  .45