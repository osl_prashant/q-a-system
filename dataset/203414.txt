Devine Organics LLC is expecting a big increase in its Mexican asparagus program this season.
Organic and conventional volumes could be up 25% for Fresno, Calif.-based Devine, according to a news release.
Shipments from Piedras Negras should begin arriving the week of Sept. 19, with Obregon product following by early October.
In addition to shipping more Mexican asparagus in 2016-17, Devine also expects to increase volumes of its Mexican organic celery and broccoli, which will ship from the San Luis region.
Promotable volumes of those commodities should be available through mid-May.
Earlier in September, Devine announced it was nearing completion of solar panels at its Coalinga, Calif., packing and cooling operation.
Devine grows and markets more than 2 million cartons of fruits and vegetables from California and Mexico annually.
In addition to organic asparagus, celery and broccoli, Devine ships organic sweet corn, onions, garlic, shallots, blueberries, cantaloupes and honeydews.
Double D Farms, which launched Devine as its organic sales arm in the mid-1990s, also grows conventional asparagus, onions and blueberries.