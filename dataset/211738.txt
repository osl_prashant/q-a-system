Cold storage and logistics company Americold is building a 15.5-million-cubic-foot automated facility with 57,600 pallet positions. 
The facility, at Americold’s Rochelle, Ill., campus near Chicago, increases Americold’s global capacity, according to a news release.
 
“Working with some of our key partners, we identified the opportunity to update and expand our campus just an hour east of Chicagoland, and to offer both automated and conventional storage and distribution options,” Americold president and CEO Fred Boehler said in a news release.
 
The company broke ground on the facility Aug. 30, and plans are to complete it by December 2018.
 
The facility will be 140 feet high, housing an automated storage and retrieval system attached to a conventional warehouse, according to the report.