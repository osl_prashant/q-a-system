From the threat of animal disease outbreak, growing importance of export markets and the innovative ways beef is marketed to consumers around the globe, the U.S. livestock industry is realizing that animal ID might soon become a necessary part of doing business. 
The Chipolte Crisis should be a wakeup call for all beef industry stakeholders.
Click the image below to read the full story: