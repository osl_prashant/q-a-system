5-9-16 Coachella Grapes from Farm Journal Media Ag & Produce on Vimeo.California’s 2016 table grape season kicked off the first week of May in the Coachella Valley, where grower-shippers say quality is exceptional, and volume should exceed last year’s.
 
“It’s probably the best quality I’ve seen in the last 15 years,” said Bob Bianco, partner at Bakersfield, Calif.-based Anthony Vineyards.
 
Peter Rabbit Farms in Coachella started picking the flame seedless variety May 5 and perlettes May 9, said salesman Ed Lopez.
 
Sugraones and summer royals were scheduled to follow May 12.
 
“This is probably as early as we’ve ever gotten started,” Lopez said
 
Richard Bagdasarian Inc. in nearby Mecca also started picking flames May 5, said president Nick Bozick.
 
He expected volume to pick up by the end of the second week of May.
 
“Color is moving a little slower than we would like on the flames,” he said.
 
“We had an outstanding February and outstanding March, but it cooled down in April, which is why the fruit is slowing down a little,” he said.
 
Anthony Vineyard started picking May 6, Bianco said.
 
“The deal will really get rolling” by the second week of May, he said.
 
He anticipates “very good sizing, good weather and good supplies” on flames and sugraones.
 
Growers seemed particularly pleased with this year’s sugraone crop, after several seasons of light volume on that variety.
 
“Most ranches seem to be producing fairly well this year,” said Lopez, who predicted near-normal levels of sugraones from the Coachella Valley.
 
But he said volume of sugraones from Mexico is expected to be lighter than normal.
 
The Fresno-based California Table Grape Commission expects Coachella Valley growers to produce more than 5 million 19-pound box equivalents of table grapes this season, which would be an increase over last year.
Bianco said volume might hit 6 million boxes.
 
Meanwhile, supplies of Chilean grapes were reported to be winding down sooner than usual because of weather-related problems.
 
“Their storage levels are really low compared to historical levels,” Bozick said.
 
Some years, suppliers have had up to 5 million boxes of Chilean grapes in storage, Lopez said.
 
“This year, the inventory is much lower,” he said.
 
The good news for California growers is that the Chilean deal seems to be ending with good markets and tight supplies.
 
“It’s dovetailing perfectly into our production,” Lopez said.
 
Coachella Valley growers typically wind down during the first half of July, when production moves to California’s San Joaquin Valley.