For some in snow covered farm country it’s hard to believe, but planters are rolling. Check out these photos from farmers in South Texas where the 2018 growing season is officially underway.

Have to start at some point....#plant18 #pioneer #corn #southtx pic.twitter.com/iaiyRkIBfU
— Colin Chopelas (@c_chopelas) February 6, 2018


Here we go! Planting is starting in the #RioGrandeValley. #plant18 pic.twitter.com/iOI6xv8oKa
— Texas Farm Bureau (@TexasFarmBureau) February 6, 2018


Some have enough moisture. Others don't. See how #plant18 is going for those who started this week in the Rio Grande Valley: https://t.co/RkXFuCN1O2. #farm365 pic.twitter.com/aHKK8Edj9o
— Texas Farm Bureau (@TexasFarmBureau) February 7, 2018


A host of my Sugarbeet peeps are at the #ASGA18 meeting listening to great leaders. I’m happy to be working getting ready for #plant18 that is going to start for us in a week if this sunshine keeps it up. ? pic.twitter.com/L8PTkxkm1E
— Cody Bingham (@thecodybingham) February 7, 2018


My buddy Spence Pennington in deep south Texas outside Raymondville. Guys in north Texas won't plant until end of April. Texas is BIG. pic.twitter.com/O5oxCYEWak
— Allen Meissner (@bigaljack) February 7, 2018

How much planting is happening this week? We won’t know until tomorrow when USDA will release their first crop production report for the year.