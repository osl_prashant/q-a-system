[Note: The following piece was originally written 15 years ago by this columnist and is reproduced with permission.]
We’ve all read the poem, seen the movie and in some cases, traveled to the North Pole in our imagination.
But those storybook scenes don’t show what it’s really like, the tension that hangs in the air like a cold fog at Santa’s Workshop when the Christmas countdown is finally on, when he and his elves are struggling through those last hectic meetings to finalize the list of toys for all the good girls and boys.
It can get crazy, especially when Santa has to choose “appropriate gifts” for those who’ve been naughty — like the people who oppose the meat industry, for instance.
Let’s listen in on Santa and his personal elf assistant on Christmas Eve:
Santa: [Sitting at a cluttered desk, rubbing the bridge of his nose]. “I thought we’d have this all done by now.”
Personal Elf Assistant: [Wearing an XXS muscle shirt that says, “Christmas Cheer, Right Here”] “What, you mean these?” [jerks his thumb at a pile of letters on the floor] “Relax. We got a full 24 before you have to shove off.”
Santa: [Glances at bulges in boxes marked “Nice,” “Naughty” and “No Chance”] “I’ll have a migraine by then.” [Looks up] “Would you stop with the sorting already? You’re making me feel like I’m running a Nike sweatshop here.”
Elf Assistant: “Hey, if the shoe — ”
Santa: “Okay, zip it. That’s about as fresh as that carton of ’nog that’s been sitting on the workbench since last Thursday. Besides, [gets a twinkle in his eyes] we have to finish picking gifts for the lucky losers on ‘Santa’s Special List.’ Ho, ho, ho!”
Elf Assistant: [Tilts head, stares at Santa] “Can I tell you something? That ‘ho-ho’ schtick quit being funny when the sun stopped shining around here. And that was in October!”
Santa: “Aw, c’mon. Chill out … ha, ha. ‘Chill’ out. Get it?”
[Silence]
“Nothing? Whew, rough room. All right, I’ll tell you what.” [takes a long swig from a small flask]. “Why don’t you march those pointy-toed slippers of yours into the workshop, and let’s do this thing.”
[They both leave the office and enter the crowded workshop. Over the blaring of Christmas carols, elves are wandering around, poking through boxes of toys. One group is clustered around a big plastic punchbowl, laughing, while two older elves with long white beards are sitting on a couch fighting over the TV remote]
Santa: “Hello?” [clears his throat] “Excuse me.” [raises his voice] “Hey, you guys wanna pretend you’re on deadline, huh?” [Elves look, up startled]. “Thank you!”
Head Elf: “Uh, we were just getting set for that meeting you mentioned.”
Santa: “Right.” [looks across the room] “Yo, could we get a little ‘Silent Night’ with the stereo? Great. All right, everyone listen up. I got good news and I got bad news.
                Head Elf: “What’s the good news?”
                Santa: “I’m gonna be saving a lot of money on bonus checks this year.”
[Elves stare at Santa in shock]
                Santa: “Relax. I’m kidding. That’s what I do, remember? Jolly Old Saint Nick?” [cold silence] “Okay, okay lighten up. Seriously, what I meant to say was that you can forget about stuffing all those stockings with coal.” [Elves start whispering to each other; some start high-fiving] “This year, I’m giving the knuckleheads who haven’t been so nice what they really deserve for the holidays.”
Worker Elf: [chugs down a cup of punch] “Sweet! So they get what’s comin’ to ’em, right?”
                Santa: “What did I just say?”
                Worker Elf: “Sorry.”
                Santa: “Okay, gimme those letters we pulled.” [An elf runs up, hands Santa a sheaf of printouts]. “Normally, demands like these are roasting on an open fire by now, but I decided to get creative instead. These are letters from people who totally don’t get the whole peace on earth, goodwill thing. Even worse, they’re from adults.”
                Second Worker Elf: “Wait a second. Are you telling me you’re getting Dear Santa letters from adults?”
                Santa: “No, doofus. They’re emails we intercepted. Same dif. Okay, here’s one from a vegetarian who writes that she wants ‘A world where people eat only good foods that don’t come from exploited animals.’ ”
                Elf: “What are you giving her?”
                Santa: “I’m making sure every single present she opens contains the only gift that’s appropriate for her: fruitcake!”
[Laughter all around]
                Santa: “Here’s another letter. This one’s from a lawyer who’s been filing lawsuits against any meat company that conducts a recall. ‘What I do isn’t about money,’ he writes. ‘I just want justice.’ ”
                Worker Elf: “Whoa, what’s he getting?”
Santa: “A great big dose of justice: An IRS audit.”
Another Worker Elf: “Ha, ha! Make sure it’s grift-wrapped!”
Santa: “Uh, how about you making sure it’s gift-wrapped, wise guy?”
Elf: “Oh … right.”
Santa: [shuffles through some more printouts] “All right, here’s another one. Oh, this is beautiful. This is from a disgruntled meat inspector who says he wants one of those cell phones with a color screen so he can access the Internet and, as he writes, ‘At least do something constructive while I have to stand around this frickin’ plant.’ ”
Worker Elf: “I’m guessing he’s not getting the phone.”
Santa: “No, I’m giving him the one gift he’ll appreciate.”
Worker elf: “Which is?”
Santa: “A promotion. He thinks his job is frustrating now. Wait until he has to deal with dozens of people just like him!”
[Elves look at each other, nodding their heads]
Santa: “Here’s a classic one from somebody who’s campaigning for family farmers. He writes, ‘Huge corporate farms are ruining our agricultural system.”
Worker Elf: “What are you going to give him?’
Santa: “A nice piece of property, about 20 acres, with an old barn on it.”
Worker Elf: “What?? That’s an awfully nice present.”
Santa: “Yeah, but I’m also ‘arranging it’ so that he gets laid off from his job, and he actually has to make a living off his land.”
Elf: “Ouch. That’s harsh!”
Santa: “Yeah, so’s Nature. Whoa — look at the time. If I’m not mistaken, we’ve got a sled to pack up, boys.”
[Everyone starts to scurry around]
Personal Elf Assistant: “Wait a second. What was the bad news?”
Santa: “Huh?”
Personal Elf Assistant: “You know. You said there was good news, and there was bad news. What’s the bad news?”
Santa: “Oh, that ‘Santa’s Special List?’ You’re on it.”