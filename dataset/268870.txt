Cowboy boots trekked across Capitol Hill this week, as members of the National Cattlemen’s Beef Association (NCBA) visited legislative officials to share how harmful fake meat could be to the beef industry.
On Thursday, Danielle Beck, NCBA director of government affairs, joined AgriTalk host Chip Flory during the organization’s annual Cattlemen’s Legislative Conference to discuss the organization’s policy priorities.
Beyond trade and environmental discussions, this year fake meat, labeling and food safety were priority concerns for cattlemen.
USDA Food Safety and Inspection Services (USDA-FSIS) Petition 1801 is a request from the United States Cattlemen’s Association asking USDA “to restrict the definition of meat to actual products raised by ranchers and farmers. And then create a new standard of identity for beef.”
NCBA originally chose not to pursue standards of identity, thinking that this would not get the job done, Beck said. Read the official statement from NCBA.
“When we talk about fake meat, we are actually talking about two very distinct products, that require distinct and different approaches,” Beck said.
Plant-based imitation meat products currently on the market fall under the Food and Drug Administration (FDA). Lab-grown synthetic meat products that are not yet on the market, however, do not have a clear regulatory path forward.
Listen to the clip below to hear Beck lay out NCBA’s goals moving forward.
``````
“Unfortunately, FDA has a long standing history of ignoring the law,” Beck said. “If we look at what happened with the dairy industry, and what they’ve been doing—soy milk and almond milk and their efforts to address that—that fight started back in 2000.”
18 years later, FDA has yet to enforce the law, and even if you create a standard of identity for beef under USDA-FSIS, that won’t do anything to help ensure FDA will inforce the law, Beck explained
Even several years ago as a young Congressional staffer, Beck remembers the producers that visited and who took the time to educate her about beef and cattle production.
“It’s really important that each leader knows where beef producers stand on every issue when they go in to vote,” she told Flory.