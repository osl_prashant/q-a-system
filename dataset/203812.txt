<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


KANSAS CITY, Mo. - A veteran Kansas City area distributor is the 2016 winner of The Packer's Jan Fleming Legacy Award.
Arnold Caviar, owner of Kansas City, Kan.-based Liberty Fruit Co., was presented the award at the newspaper's fifth annual Midwest Produce Expo Aug. 16.
Caviar, who joined his family's company in 1971 and later bought it, with his wife Carol, in 1982, is the fourth winner of the Fleming award, which honors produce industry leaders for their leadership, mentoring skills and service to the community.
The award is named for Jan Fleming, a longtime executive at her family company, Chicago-based distributor Strube Celery and Vegetable Co., and a pioneering woman in the produce industry. Fleming died in 2013.
In presenting the award, Shannon Shuman, The Packer's publisher, cited Caviar's work ethic and willingness to do any job at his company, including unloading trucks and cleaning docks at 2 a.m.
"He's no stranger to hard work, and he's part of the fabric of what makes Kansas City and the Midwest great."
When Caviar bought Liberty Fruit, the company had three trucks, Shuman said. Now it has more than 70 and distributes produce to more than 1,600 foodservice clients, 400 retailers and 150 wholesalers.
"If you work hard and spend time, you can be successful, no matter what you take up," Caviar said.
Where Caviar and Liberty Fruit really stand out, though, Shuman said, is in their charitable giving. Children's Mercy Hospital, Little Sisters of the Poor, Operation Breakthrough and Make a Wish are among the biggest beneficiaries.
The company's annual charity golf tournament has raised $4 million in the 16 years it's been held, with the majority going to Children's Mercy.