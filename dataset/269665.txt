Iceland company to resume commercial hunting of fin whales
Iceland company to resume commercial hunting of fin whales

The Associated Press

REYKJAVIK, Iceland




REYKJAVIK, Iceland (AP) — A whaling company in Iceland is preparing its fleet to bring commercial hunting of fin whales back to the country.
Whaling company Hvalur hf (Whale Inc.) said Tuesday it is readying two vessels for the 100-day whaling season that starts in June.
Fin whale hunting stopped in Iceland in 2015, when Japanese authorities refused to import Iceland's catch because of insufficient research about health code requirements.
Whale Inc. manager Kristjan Loftsson told The Associated Press the company is working with Japanese officials on developing methods to fulfill standards for fresh meat imports.
The fin whale population is considered critically low outside the Central North Atlantic region surrounding Iceland, which was the only country where the mammals were hunted commercially.
The latest counts put the region's fin whale population at 40,000.