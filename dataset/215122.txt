Avocado unit sales dropped and dollar sales soared during the weeks leading up to Father’s Day, Fourth of July and Labor Day this year.
The Hass Avocado Board reported that retailers brought in $144.3 million between the three holidays, up 12% from 2016.
Fourth of July and Labor Day both saw four-year highs in dollar sales, with $49.6 million and $46 million, respectively.
Average unit prices were $1.33 for Father’s Day (up 23%), $1.35 for Fourth of July (up 19.5%) and $1.59 for Labor Day (up 39.5%).
Conversely, unit sales decreased noticeably. Father’s Day volume dipped 4.2%, Fourth of July volume slipped 7.8%, and Labor Day volume plummeted 22%.
“Shoppers are looking for delicious, healthy options for summertime holidays and events, and avocados are a natural fit,” Emiliano Escobedo, executive director of the Hass Avocado Board, said in a news release. “HAB supports retailers and marketers of avocados with category insights for summertime holidays and year-round.”
Overall, avocado volume sold fell to 102.3 million units, down 11.2% from 2016.