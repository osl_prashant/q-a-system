Sanfillipo promotes operations manager
George Toruno has been named operations manager at Sanfillipo Produce, Columbus.
“George has been with us for over 10 years,” said Jamie Sanfillipo, partner and sales manager. “He started as just a bright-eyed kid looking for a job and has worked his way up.”
 
Premier eyeing Columbus move
Premier ProduceOne is pondering a move for its Columbus operations, said Tony Anselmo, chief marketing officer.
“We’re busting at seams,” Anselmo said of the company’s 35,000-square-foot facility.
Premier ProduceOne, which also has a 50,000-square-foot facility in Cleveland and a 25,000-square-foot facility in Dayton, likely needs a 75,000-square-foot facility in Columbus, he said.
In addition to increasing sales in general, Anselmo said the company’s increasing attention to organics is fueling the need to expand.
“You have to increase the number of slots for everything,” he said. “We used to have four or five slots for lettuce. Now with the expansion of organic we have 10. Organics have become a large category for us. We’re dedicating more buyers and more time to it.”