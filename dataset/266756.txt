Feds: Decision on key wetlands plan moving up almost 2 years
Feds: Decision on key wetlands plan moving up almost 2 years

The Associated Press

NEW ORLEANS




NEW ORLEANS (AP) — The Army Corps of Engineers says it's advancing the permit decision date nearly two years for a key part of Louisiana's plan to rebuild shrinking wetlands.
The Corps had told state officials it could need until October 2022 to assess the environmental impact of a project to build land by sending Mississippi River mud and sand into open water. Monday, it said the new deadline is November 2020.
A conservation coalition called Restore the Mississippi Delta says the change is a significant accomplishment since wetland loss is measured by the hour.
Commercial fishermen say they may sue if the project is approved, arguing river water could spoil saltwater or brackish-water fisheries.
The governor said in January the Trump administration had agreed to speed up the project's permitting process.