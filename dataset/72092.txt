Key report falls well shy of expectations






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






The U.S. jobs report released this morning will not impact what the Federal Reserve has in store for U.S. interest rates. As the Wall Street Journal noted, the report is “mostly just weather-related noise.”

March Nonfarm Payrolls: 98,000

Expectations: 175,000
What drove the data: The data showed an increase for professional and business services of 56,000 which was about in line with average gains the past 12 months. The recovery in the US oil industry continues, as mining (where oil industry jobs are counted) increased 11,000 in March, primarily support services. Mining employment is now up 35,000 since a low hit in October 2016.
Employment in financial activities continued to trend up in March with an increase of 9,000 which puts the gains for the past 12 months at 178,000.
Construction, a source of 59,000 jobs in February, rose just 6,000 in March. A big decline was noted in retail trade, with 30,000 jobs lost in March. Jobs at general merchandise stores fell 35,000 and have declined 89,000 since a recent peak in October 2016.

March Unemployment: 4.5 percent

Expectations: 4.7 percent
What fed the decline: The unemployment rate decline to 4.5 percent from the 4.7-percent mark in February reflected a rise in the number of those not in the labor force and the number of employed workers grew more than the labor force did.
The labor force participation rate held steady at 63.0 percent.
A broader measure of unemployment – the U-6 rate – measures the total unemployed plus those that are employed only part time for economic reasons and those who have indicated that they want and are able to work. That fell below 9 percent for the first time since before the financial crisis.
Wages posted a rise of 5 cents, or 0.1 percent, which was less than expected. That took the year-over-year increase to 2.7 percent, down from a 2.8-percent rate in February. The average workweek held steady at 34.3 hours.
Revisions to prior numbers saw February revised down to 219,000 (235,000 previously) and January was revised down to 216,000 (238,000 previously), putting the job gains 38,000 less than previously reported. The three-month average of job gains now stands at 178,000.


Comments: There will be much discussion about the factors that went into the jobs data, including that the heavy winter weather that hit during the survey week. And, the subdued rise in construction jobs came after February was the second warmest on record. The revisions also take some of the shine off the January and February results, but even revised lower, those remain as still-solid results for the US jobs market. Still, the retail side is likely to continue as a potential drag in the future. With more retailers announcing the closure of brick and mortar stores, that will keep this is a potential area where job losses or minimal job creation could be seen in the months ahead.
A much-lower-than-expected result surprised markets, prompting a decline in the US dollar index, a fall in US Treasury market yields and a move higher in gold futures. But this likely changes little for the Fed's outlook since the unemployment rate signals we are likely near full employment at this stage and stands at the lowest level since May 2007. However, the odds for a June rate increase fell to 66 percent in the wake of the report compared to 71 percent prior it the release.
This report will also at least temporarily remove the ADP data as being viewed as an accurate predictor of the broader government data. The past two months had seen the ADP figures predict a robust general jobs growth, but that prediction failed this time around. Some note the disparity between the government and ADP data was the largest this month since 2013.
The data will also potentially be viewed as a "one-off" event, much like the May jobs update in 2016 that fell well short of expectations.
Bottom line remains, however, that we still have a solid jobs market despite this disappointing or below-expectation result.






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.