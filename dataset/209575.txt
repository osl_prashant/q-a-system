Cleaning calf facilities starts with water, but it will take more than H2O to remove harmful bacterial layers that negatively impact calf health and farmers’ bottom lines.Water is needed in almost any aspect of cleaning and disinfection, says Don Sockett, veterinary microbiologist at the University of Wisconsin. Facility designs should include ways to move water out easily, using slopes and drains.
“When you design your facilities, you have to think about how you’re going to clean them,” Sockett says.
However, don’t use a high-pressure washer to clean calf pens because of cross contamination.
“High pressure washers are good to remove soils, but they are not very good at removing the biofilm layer,” Sockett says.
Biofilm is the enemy of calf raisers, Socket adds. Bacteria lives in communities called biofilms. These communities harbor many pathogens, up to 95% of the organic load.
“If you’re cleaning is not efficiently removing the biofilm layer, you’re really not accomplishing much,” Sockett says.
Bacteria associated with respiratory disease, scours, roto virus and crypto will hide in biofilms. It is important to control the spread of these bacteria because they can be financially devastating. For instance, a study from Quebec, Canada showed mycoplasma bovis reduced average daily gain in pre-weaned dairy calves by 40%.
“That’s a lot of money in lost milk and future returns,” Sockett says. “So it is really important to remove the biofilm.”
Biofilms are composed of carbohydrates, proteins and fat. This is similar to components in milk. Sockett says producers should apply similar sanitation methods used for milking parlors when cleaning calf facilities or feeding equipment.
Here is an outline for cleaning feeding equipment like bottles or buckets:
Clean large particles off
Rinse with luke warm water (90°F)
Manually wash with a brush for 2-3 minutes using hot water (at least 140°F) mixed with chlorinated alkaline detergent containing an 11-12 pH
Rinse with cold water
Rinse a second time with cold water mixed with 2-3 pH acid and 50 ppm solution of chlorine dioxide
Dry
Sanitize with a 50 ppm solution of chlorine dioxide within 2 hours of use
It is important to check and see if the soap you are using is the right pH. Sockett points out that many soaps don’t fall into the right pH category. “You want a caustic soap with a pH between 11 and 12.”
Having the proper soap will help emulsify the fats, while breaking down or solubilizing the carbohydrates and proteins.
The acid rinse aids in removing the mineral deposits created by hard water.
A similar guideline can be used for calf pens or trailers:
Clean up the large filth like manure, bedding or feed
Soak with water. Don’t use a high-pressure washer during any of the process because of cross contamination
Alkaline foam cleaning using an 11-13 pH
Soak with water
Rinse with water
Acid foam cleaning using a 3-4 pH
Soak with water
Rinse with water
Dry
Disinfect
Sockett prefers using handheld foamers to do cleaning in pens and trailers since they can help restrict the flow of chemicals and maintain a proper pH.
“I like to use handheld foamers because they use them in car washes so I know they aren’t going to be as harsh on the environment,” Sockett says.
Another important consideration for cleaning are brushes. Sockett recommends using new brushes at least quarterly each year. If you have higher bacterial loads or are using brushes more regularly they could be replaced monthly.
“Trust your procedures,” Sockett says. “Your goal is not to eliminate pathogens from your premise. You want to reduce the numbers so that when animals are exposed they won’t get the disease.”
 
Note: This story appears in the May 2017 issue of Dairy Herd Management.