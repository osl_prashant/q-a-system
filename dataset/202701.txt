East Coast Transport, a Paulsboro, N.J., third-party logistics company, presented 3 Rivers Logistics with its 2016 Carrier of the Year award.
 
The Carrier of the Year award recognizes the provision of outstanding services, as well as the characteristics and values East Coast believes all logistics companies should possess, according to a news release.
 
"3 Rivers Logistics was chosen for the 2016 Carrier of the Year award based on consistently achieving operational excellence of five critical principles: service, communication, flexibility, value and administration," Paul Berman, vice president of logistics for East Coast Transport, said in the release. "(East Coast Transport) is proud of the relationship between the two companies, and we look forward to the future with them."
 
Springdale, Ark.-based 3 Rivers Logistics was founded in 2006, according to the release. 3 Rivers provides truckload and less-than-truckload brokerage services for both refrigerated and dry loads, according to the release.