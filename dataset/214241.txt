Businesses of all sizes and sectors discover that they function best when they “merge their business interests with the interests of customers, employees, suppliers, neighbors, investors, and other groups affected, directly or indirectly, by their respective operations,” writes Joel Makower in his book, Beyond the Bottom Line: Putting Social Responsibility to Work, (1994). A business’s reputation and policies can impact financial performance, reduce turnover and retraining costs, increase efficiencies, reduce waste and energy costs, and for retail businesses, increase sales and stock prices. Here are examples of what some businesses are doing to support agriculture and social causes.

Iowa Select Farms
As owners and leaders of Iowa Select Farms, Jeff Hansen and his wife, Deb, established the Deb and Jeff Hansen Foundation in 2006. The foundation’s philanthropic activities are extensive, as shown by these examples:
Pork Care Packages: Provided to more than 12,000 Iowa Army, Marine, Navy and Air Force troops as a thank-you for their service
Operation Christmas Meals: Feeds 4,000 Iowa families.
Children’s Cancer Connection: Donation of $300,000 to establish the Hansen Home for Hope in Des Moines, Ia.
Hunger Relief: 22 tons of pork donated to food pantries

The Maschhoffs
This pork business focuses on scholarships in animal agriculture, including the following:
The Maschhoffs Pork Production Scholarship: Offered through the National Junior Swine Association this program provides youth participating in purebred and commercial swine activities the opportunity to interact with one of the nation’s largest pork producers. Two scholarships, $1,500 and $1,000, are awarded annually to NJSA members enrolled in an agricultural program at a recognized college or university
The Maschhoffs sponsor a $1,000 scholarship through the Iowa Foundation for Agricultural Advancement for any student studying animal science while attending a college or university in Iowa
Through the SIU Carbondale College of Agriculture Heritage Scholarship Program, the company offers an annual $1,000 scholarship to a student in agricultural sciences with interest in animal science, agriculture production and/or environmental issues in agriculture.
John Wood Community College: This $500 scholarship is presented annually to a freshman or sophomore student at JWCC who shows a desire to work in the livestock industry and who wishes to continue to study animal sciences.
Master of Science in Animal Science: The University of Illinois at Urbana-Champaign and The Maschhoffs have partnered together on numerous research projects in swine production science. The Master of Science in Swine Production degree at the University of Illinois, also is sponsored by The Maschhoffs.


Blain’s Farm & Fleet
Blain’s Farm & Fleet supported the American Heart Association (AHA) with a donation of more than $313,000 over a year’s time, ranking the company third retailer nationally in the retail category, just behind Wal-Mart and HSN, and ninth nationally for funds raised. The company has now raised more than $4 million for AHA.
In 1993, co-founder of Blain’s Farm and Fleet, Bert Blain, and his daughter Jane Blain Gilbertson were asked to co-chair the first Heart Walk in Janesville. Bert passed away just days before, making the reason for the Heart Walk even more meaningful to those who knew him.
“It’s an honor to share our family’s story and spread the awareness and importance of the fight against heart disease,” said Jane Blain Gilbertson, now president and CEO of the company.


John Deere Foundation
The John Deere Foundation works with local chambers of commerce and development groups to leverage the strengths of communities, making them more attractive to potential employers. It focuses primarily on solutions for world hunger, education, and community development. In addition to many other activities, the foundation is the title sponsor for the John Deere Classic, which has raised more than $62.45 million for local charities.  
 

Vermeer Corporation
The Vermeer Charitable Foundation gives to many organizations and causes. The third-generation company has an environmental focus: Contributions are made yearly to The Nature Conservancy in Iowa, the Pheasants Forever’s “Reload Iowa Grassroots Conservation Campaign,” and Trees Forever. It also supports many civic, church and educational needs in central Iowa, from which it draws its employees.
 

Culver’s
Culver’s restaurants came together to help FFA members get the recognizable blue jacket that represents membership in the organization. The restaurant chain raised nearly $20,000 recently and will present 153 deserving FFA members with FFA jackets this fall.

Olive Garden
Last year, Olive Garden restaurants donated enough chicken to local food banks to feed 244,621 people.
A new partnership with Feeding America will add 5.5 million more meals to help families facing hunger in communities across the country. Together, Feeding America and Olive Garden share the belief that no family should go without a meal.
“Our restaurants are committed to giving back to the communities they serve, and our team members are proud of the local impact they make every day through the Olive Garden Harvest program,” said Dave George, president of Olive Garden.
What Can You Do?
There are many other businesses – agriculture and otherwise – that support charities or offer assistance in disasters.
We have much to be thankful for in the pork industry – show your gratitude by sharing with others. Share what you do in the comments section below, and look for our follow-up article on the Donor Bill of Rights.