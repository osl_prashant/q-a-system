Moxxy Marketing has promoted Roger Smith to director of digital media.
He joined the marketing agency in 2014 as a web developer.
“We quickly learned Rogers’ talents go far beyond tech and digital,” Moxxy president and CEO Karen Nardozza said in a news release.
Smith’s roles include developing over-arching strategies and overseeing the execution of digital media tactics, according to the release.