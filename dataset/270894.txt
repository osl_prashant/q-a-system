If you had the choice of dipping into a clamshell of tender fresh blueberries and plopping one in your mouth or opening up a bag of rock-hard blueberries that sit on an icy ledge in your freezer, what would you choose?  
There you have the main problem for marketers of frozen fruits and vegetables - the product they sell just isn’t as inviting compared with fresh. That’s nobody’s fault, and certainly the consumer shouldn’t be browbeaten for loving fresh over frozen.
 
But the marketers of frozen fruits and vegetables are a determined lot, always making the case that, hold on, frozen is just as good as fresh. In fact, when you consider the science, frozen comes out on top!
 
This ginned-up enthusiasm just isn’t catching on with consumers.
 
The latest press clipping to consider comes from Business Insider with a headline  “Fresh fruits and veggies aren’t always healthier than frozen ones — here’s why”
 
The earnest article cites a study published recently Journal of Food Composition and Analysis, that “finds that frozen fruits and vegetables are, in many cases, more nutritious because fresh produce loses vitamins when left sitting in the fridge, even after just a few days.”
 
Here is the link to the study, and from the abstract:
 
In the majority of comparisons between nutrients within the categories of fresh, frozen, and “fresh-stored”, the findings showed no significant differences in assessed vitamin contents. In the cases of significant differences, frozen produce outperformed “fresh-stored” more frequently than “fresh-stored” outperformed frozen. When considering the refrigerated storage to which consumers may expose their fresh produce prior to consumption, the findings of this study do not support the common belief of consumers that fresh food has significantly greater nutritional value than its frozen counterpart.
 
 
TK: Do consumers prefer fresh because they believe fresh produce “has significantly greater nutritional value than its frozen counterpart.” Yes, it is true, consumers are misunderestimating frozen fruits and vegetables, to quote a Bushism.
 
A study published in 2015 at the Management International Conference, titled “Consumption of Frozen Fruit and Vegetables in the Context of Malnutrition and Obesity; New Brunswick, Canada” reported differences in consumer perceptions of fresh and frozen.
 
The report said New Brunswick consumers reported that fresh produce accounted for 79% of their total fruit and vegetable purchases, with the median respondent purchasing between 20% and 30% of their fruits and vegetables in frozen form. When the survey asked consumers why they chose fresh over frozen, it wasn’t all about nutrition. In fact, researchers found that convenience and tasted were ranked first by most respondents. 
 
From the report:
 
The aim is to ascertain what attributes are most important to consumers in choosing between fresh and frozen F&V. The four criteria were; convenience, perceived nutritional value, price, and taste. Convenience and taste were ranked first by most respondents; perceived nutritional value was ranked first by 30% of respondents. Price was not ranked first by any respondent. When the first and second ranks are aggregated, perceived nutritional value becomes the most important; 80% of respondents had this as their first or second criteria. Convenience was second with 70%. 
 
But what to do? The authors propose:
 
Fresh produce in supermarkets is typically subject to lengthy transport and storage delays, with detrimental impacts on nutritional value. Publicizing the deterioration of fresh produce would be counter-productive to consumption of F&V, but if the nutritional benefits of frozen produce were more commonly known, substitution to frozen produce could increase.
 
 
TK: Some authors propose “rethinking” fresh in view of the food waste issue, and this story in the Washington Post is headlined “We think fresh is best but to fight food waste we need to think again” 
 
The story says 50% of the food waste generated comes from fresh fruits and vegetables, either because of decay or cosmetic issues. The implication is that food waste would decline if we would consume more frozen and less fresh.
 
Can frozen produce be lifted up without hurting fresh?  It is doubtful.
 
The Produce for Better Health Foundation did consumer research published in 2016 called “Primary Shoppers’ Attitudes and Beliefs Related to Fruit & Vegetable Consumption 2012-2016.”
 
For 2016 results, the study found that 74% of consumers consider fresh fruit healthy, compared with 30% who believed that about frozen fruit. Likewise, 66% of consumers polled said they thought fresh fruit “tastes good,” compared 24% of consumers who thought the same of  frozen fruit. More than 60% of consumers polled believed fresh fruit is a “good snack” compared with just 20% who believe that about frozen.
 
On the plus side for frozen, nearly half (47%) of consumers said they thought frozen fruit “keeps well,” compared with just 10% who said that about fresh fruit. And, somewhat surprisingly, 47% of consumers thought frozen fruit was easy to use, compared with 39% of consumers who said the same about fresh fruit.
 
 
From the PBH report:
 
Healthy, tastes good, good snack and family preferences were the main reasons primary shoppers purchased fresh produce. For pre-cut, pre-washed fruit and vegetables, ease of use was the main purchase driver. Ease of use and shelf-life were the key purchase drivers of frozen and canned fruits and vegetables. Dried fruit was purchased because it is a good snack.
 
 
TK:   Nutrition is important, but taste and sensory appeal are more critical to consumers when they choose fresh over frozen fruits and vegetables. The issue of food waste may be the arena where fresh is most vulnerable to processed alternatives, but no single well-reasoned  academic study will move the needle of consumer preference away from sumptuous fresh options and toward the icy ledge of frozen produce any time soon. In fact, human nature being what it is, fresh will never lose its winning appeal.