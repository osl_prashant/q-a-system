New York Apple Association president and CEO Jim Allen plans to retire Jan. 2, 2017 â€“ his 66th birthday.
Allen has led the association - which represents 675 apple growers in the state and is the apple industry's second-largest state marketing group - since 2000, according to a news release. He started with the association as retail promotion director in 1996.
Allen, whose career has included stints at the New York State Department of Agricultural Inspection Service, Comstock Foods, Keystone Fruit Marketing and Sun Orchard Fruit Co., has been active in representing the industry on national committees and boards, having worked with the U.S. Apple Association and the U.S. Apple Export Council, among others.
Allen, who grew up on a potato and vegetable farm in Waterville, N.Y., and has a degree in food technology and marketing from Morrisville State College, has received numerous awards for his work promoting the fruit, including recognition in 2008 as a member of The Packer 25. In 2002, The Packer named him Apple Man of the Year.
Allen and his wife of 33 years, Ellen, have four children and two grandchildren. They live in Webster, N.Y.
The Packer interviewed Allen just before the 2016 harvest.

Q: How would you compare the New York apple industry now to what it was like when you started?
A: The industry has grown rapidly over my 20 years, in production and in infrastructure, with new plantings and new state-of-the-art packing and storage facilities. A huge switch from processing apple production to new fresh apple plantings.


Q: What is the most significant change?
A: New varieties and new planting systems.

Q: What is your proudest accomplishment as the association's president?
A: Gaining the respect and support of the New York state legislation that led to receiving state financial support in every state budget since 2000, except one year.
We have received over $10 million, and each year we have successfully reported back to our legislators with a complete accounting of how every penny was spent to promote New York state apples and apple products.
Our personal visits and our transparent reporting of use of funds each year gained the support of the full legislation for allocating funds to the apple industry, in promotion and in research.
This alone has been one of my proudest accomplishments in New York state. In addition, when I took this position the industry - growers and shippers - were very fragmented and, in many cases, unwilling to work together within the state, and, for sure, outside the state with other apple areas, such as Michigan, Pennsylvania and Washington.
One of my goals was to help form a more unified industry and by reaching out to other state agencies and organizations, such as (New York Farm Bureau), (New York State Horticultural Society), and with national groups such as U.S. Apple and United Fresh, we were able to be successful, and today the industry is much stronger.

Not to say that we do not have some folks that continue to foster provincial and proprietary positions, but overall I feel that most of the industry is looking beyond their own orchards and recognizes the value of collectively working together.

When we tackle food safety, immigration, labor shortages, trade barriers and import and export issues, we can only hope to be successful as one voice and one message.

Q: If you hadn't gone into produce, what would you have done?
A: Great question. I can't imagine doing anything different than being involved in agriculture, from my childhood to now - or, as George Constanza would say, "an architect."


Q: What is a leader's most important function? How have you striven to meet that goal? How close have you come to meeting all your leadership goals?
A: I think gaining the respect and the confidence of those that you work for is vital to any measure of success.
When I took this job, I was given advice by a then-board member grower, who said, "Don't ever forget who you work for, and never think that you are above them."
I have tried to follow that and have always tried to have my bosses take ownership of our decisions that we made together.


Q: What was the most exciting aspect of this job? 
A: Introducing and executing new consumer and trade promotions for NYS apples.
Under my watch we expanded our consumer outreach to include massive TV and radio promotions across the state and the metro New York markets. Being involved with the concept, the production, and then the execution of media advertising is exciting.
Having world-renowned and recognized soccer phenom Abby Wambach promote New York state apples was a big success.

My biggest takeaway from this job will be the wonderful friends that I have met around the world in this industry.
Here in the U.S., working with my apple partners in all states and all levels has been very rewarding. I have traveled almost around the world on export and trade missions from Cuba to China, Russia to the Far East, throughout the U.K. and EU and have met so many wonderful industry colleagues.

When you do 20 PMA conventions and 15 or so United Fresh and FMI conventions, along with untold number of regional produce shows, you meet thousands of folks that share a common passion for the produce industry. It is a fabulous network of great people.


Q: Are there any unmet goals in this job? In your career? 
A: Looking back, I suppose I could always say the expanding markets and growing market share is always a unmet goal. (You) never can have too many markets, but with increased production, especially from the West, maintaining markets was and is a huge task.
Disappointments in my career? Yes! But, perhaps the biggest disappointment for me has been the inability of gaining any sort of immigration reform for agriculture.
(The industry has) worked diligently for 15 years or longer to try to solve our growing labor issues in the industry across the country.
Countless trips to D.C., untold number of letters and phone calls and, for me, a number of editorials in both national trade and consumer trade pubs advocating for immigration reform. Today we are in the same place as we were 15 years ago.

Q: What do you hope is your lasting legacy in the apple industry, or the produce business in general?
A: To be remembered as a good guy, trying to do the best for his state and for the industry and hope that after meeting someone new, that I would leave them with a good impression and that they would have a desire to meet me again.

Q: What are your plans for retirement?
A: TBD...