MAP and potash firmed slightly as DAP softened on the week.

DAP $33.96 below year-ago pricing -- lower $1.09/st on the week to $446.06/st.
MAP $36.85 below year-ago -- higher 13 cents/st this week to $461.25/st.
Potash $16.41 below year-ago -- higher $1.12/st this week to $334.27/st.


Potash led the P&K segment higher with Kansas up $16.75 and Wisconsin firming $2.34. Only Ohio was unchanged as North Dakota $6.60 and Minnesota down $2.25.
MAP followed potash higher led by Kansas which firmed $4.59 and Michigan added $2.34. Four states were unchanged as Minnesota fell $4.22 and Indiana softened $3.50.
DAP was led lower by Nebraska which fell $8.65 as Minnesota softened $3.76. Three states were unchanged as upside action was confined to an $11.67 hike in Kansas.
As anhydrous ammonia works this week to pull fertilizer prices lower, potash bucked the trend once again and headed higher. In fact, on an indexed basis, potash prices have slipped to a slight premium to NH3. This does not surprise us given the discount potash has held to the rest of our fertilizer segment since January 2016. Fundamentally, upside risk for potash is limited by ongoing oversupply. Wholesale potash prices are steady to lower as of MosaicCo's last report for the week ended June 23.
So after a year and a half of lagging the rest of the fertilizer segment on price, potash's move to meet up with anhydrous could easily be chalked-up to a long awaited corrective move.
Phosphate needs to correct to anhydrous as well, however. Phosphate has held a premium to the rest of the fertilizer segment since September 2015. The fundamentals are much more complicated for phosphate than for potash. Phosphate rock prices, wholesale industrial ammonia prices and sulphur prices all have to be considered alongside production fundamentals. Since potash has made its long-awaited correction to acknowledge the whim of anhydrous, there is a glimmer of hope that DAP and MAP will do the same.
On an indexed basis, phosphate prices are narrowing the gap overall, but sharp price pressure has been hard to come by. As we watched the margin between DAP and MAP normalize, we suggested that could lead to a more normalized relationship with the rest of the fertilizer segment. So far, no dice.
As it stands this week, let me remind you that phosphate is still priced well below expected new-crop revenue... even at Friday's December 2017 close at a meager $3.75.
For now, we see no urgency to book phosphate or potash for fall or spring 2018 application. Anhydrous ammonia will likely limit the upside for potash, but we are watching P&K closely for indications of upside risk.


By the Pound -- The following is an updated table of P&K pricing by the pound as reported to your Inputs Monitor for the week ended June 23, 2017.
DAP is priced at 46 1/2 cents/lbP2O5; MAP at 43 1/4 cents/lbP2O5; Potash is at 28 1/4 cents/lbK2O.





P&K pricing by the pound -- 6/23/2017


DAP $P/lb


MAP $P/lb


Potash $K/lb

 



Average


$0.46 1/2


$0.43 1/4


$0.28 1/4

Average



Year-ago


$0.50


$0.46 3/4


$0.29

Year-ago