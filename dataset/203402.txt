To accommodate consumers who crave convenience, several salad and veggie snack tray suppliers are taking the extra step of including a pouch, packet or cup of dip or dressing with their products.
Litehouse Inc., Sandpoint, Idaho, works with most major salad companies and provides mostly private label dressings to accompany snacks and meal solutions, said Luke Miller, director of value added sales.
"We do a lot of (research and development) work for our partners," Miller said.
Salad makers typically come to Litehouse with specific ideas about the dressings or a mix of ingredients they want and ask for suggestions, he said."We partner with them to develop flavor profiles that go with certain salads and veg mixes."
One of the current category trends is using chopped vegetable mixes as a base rather than using a traditional leafy green base.
"We've seen a huge increase in this type of business," Miller said.
Including dips or toppings with salads, snacks or other items is the "ultimate in convenience," said Gina Nucci, director of foodservice for Mann Packing Co. Inc., Salinas, Calif.
"Having everything you need for a healthy meal that's ready to eat in minutes makes the day-to-day decision making of 'what's for dinner' that much easier," she said.
Snacking also is a growing segment.
"We wanted to offer a variety of snacking solutions for different occasions and for different consumers, and with complementary snacks you don't normally see in produce," she said.
Mann's snacking product line includes:Veggie Ranch – carrots, celery, and broccoli served with ranch dip;Veggies 4 Kidz – carrots, celery, and cheddar crackers served with ranch dip;Cheddar Trail – carrots, celery, cheddar cheese cubes and trail mix served with ranch dip;Veggie Hummus – carrots, celery, and broccoli served with hummus;Organic Veggies – organic carrots, organic celery, and organic broccoli served with organic ranch dip;Cheddar Pretzel – carrots, celery, broccoli, cheddar cheese cubes, and pretzels served with ranch dip; andHoney Turkey Cheddar – carrots, broccoli, cheddar cheese cubes, and honey turkey bites served with ranch dip.
Cashmere, Wash.-based Crunch Pak has a line of single-serve, low-calorie produce snack items called DipperZ that feature a fresh dip and/or dressing with apples or carrots, said Krista Jones, director, brand marketing and product innovation.
"Crunch Pak DipperZ are available in several iterations, such as sweet or tart apples with caramel dip, sweet apples with yogurt dip and baby peeled carrots with ranch dip," she said. 
Snackers, another single-size product the company makes, also includes a dip in a size that is ideal for lunch boxes, Jones said.
"Snackers are interesting combinations of fruits and other sides -- such as dips and cheese -- in a portable mini-tray," she said. "And, because the tray is hard form, the food inside is protected during transportation in a lunch box."
Crunck Pak offers products that include dips or dressings in order to promote healthful eating.
"We want to encourage kids of all ages to eat more fruits and vegetables," Jones said. "Anything that makes healthy eating fun and appealing is something we support."
Salinas-based Dole Fresh Vegetables launched its Country Ranch Salad Kit in June, and it should be available nationwide by August, according to a news release.
The product features cornbread croutons, white cheddar cheese, romaine lettuce, carrots, red cabbage and roasted sunflower seeds and comes with Dole's Country Ranch dressing, which is made with buttermilk.
Suggested retail price is $3.99.
Dole already has nine other offerings in its premium kit line.
The kit is a new twist on traditional ranch salads, CarrieAnn Arias, vice president of marketing, said in the release.
"Up until now, there haven't been many kit options available for salad lovers to satisfy their craving for ranch dressing," she said. "Our new Dole Country Ranch Salad Kit delivers an exclusive new combination of indulgent tastes, textures, toppings and ingredients, plus added nutrition from fresh vegetables and an unmistakable buttermilk ranch flavor payoff."
The new product can serve as a side salad, main dish or entrée with the addition of protein, she said.