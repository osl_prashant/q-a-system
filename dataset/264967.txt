Santa Maria, Calif.-based Gold Coast Packing has a new line of riced vegetable products.
The Caulifornia Riced Vegetable Sides include cauliflower, broccoli, carrots and a sauce. The flavors options are Garlic Herb and Butter, Ginger Garlic and Spanish Rice.
“The success and feedback we received from our first Caulifornia products inspired us to take that line to the next level,” Karl Lipscomb, who works in product development, said in a news release. “This line is for those who want a healthy alternative but don’t want to spend so much time with all the prep work. The sauces can be paired with any meal. The possibilities are endless.”
The kits have a shelf life of 16 days and can be cooked in less than four minutes. 
The Ginger Garlic option is vegan, while the Garlic Herb & Butter and Spanish Rice kits are gluten-free, according to the packaging.
The products are currently available at Mariano’s in Illinois, and Gold Coast plans to expand into other markets.