Chilean fruit exporters are running a trial ad campaign in Times Square in March, and the target audience is much wider than New Yorkers.
With millions of visitors to New York each year, it’s likely many American foreign tourists who have access to Chilean fruit at home will see the ads on the large Times Square billboard.
The campaign is funded by the Chilean Fruit Exporters Association (ASOEX) and ProChile, the export promotion program of the Ministry of Foreign Affairs of Chile.
The March campaign coincides with Chile’s blueberry, stone fruit and grape seasons, according to a news release.
Of the 2.6 million tons of fresh fruit exported from Chile to all countries, 35% went to the U.S. and Canada, according to ASOEX.
The Times Square billboard showcases two videos. One highlights Chilean grapes and the other is a broader overview of the fruits available for export. The Fruits from Chile Facebook page is also promoting the digital billboard.
ASOEX plans to evaluate the success of the Times Square ads to see if digital billboards should be a part of future communication campaigns.