Corn and soybean futures were higher going into USDA’s August crop reports, but a strong negative reaction to the data resulted in sharp losses for the week. Traders will continue to monitor weather for late-season impacts, but USDA’s August crop estimates signal there is more supply-side “cushion” than the market expected. Forecasts indicate conditions will remain cool and mostly dry into the middle of next week. Slightly warmer temps and increased rainfall chances are in the current outlook for the middle of this week through the end of the 15-day window. Wheat futures were also tripped up by USDA’s report data, as the declines in crop size and carryover weren’t as much as anticipated.
Pro Farmer Editor Brian Grete provides weekly highlights:



Live cattle futures broke down technically, violating support at the June and July lows. Lean hog futures firmed and the cash hog market weakened to narrow futures’ discount to the cash index.
Notice: The PDF below is for the exclusive use of Farm Journal Pro users. No portion may be reproduced or shared.
Click here for this week's newsletter.