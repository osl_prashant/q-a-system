Good morning!
Short-covering lifts corn and soybeans overnight... Corn futures are a penny higher as of 6:30 a.m. CT after favoring the upside during a quiet overnight session. Soybeans also benefited from short-covering overnight and are currently up 8 to 9 cents. Winter wheat futures are narrowly mixed, while spring wheat is down 1 to 3 cents amid ongoing harvest pressure. The U.S. dollar index is firmer this morning, while crude oil futures are down slightly.
Export sales data out today... Following are expectations for USDA's Weekly Export Sales Report for the week ending Aug. 10.



Commodity

2016-17
			(MT)


2017-18
			(MT)



Corn
0 to 200,000
400,000 to 600,000


Wheat
NA
300,000 to 600,000


Soybeans
50,000 to 250,000
300,000 to 600,000


Soymeal
0 to 100,000
60,000 to 200,000


Soyoil
8,000 to 25,0000
0 to 10,000



Trump wants major NAFTA changes; Canada and Mexico prefer tweaks... Officials from the U.S., Canada, and Mexico began talks to renegotiate the North American Free Trade Agreement in Washington on Wednesday. U.S. Trade Representative Robert Lighthizer said the initial NAFTA has been a failure for "countless Americans" and that the U.S. will be pushing for major changes. In stark contrast were comments from Canada and Mexico, who focused on the benefits for all parties involved. Bottom line: The U.S. offered a combative tone while Canada and Mexico made it clear they won't be easy pushovers. Expect contentious talks ahead with no guarantee one of the participants will not walk away from the process. Get more details.
Storage crunch in Brazil... Brazil brought in a record-shattering soybean crop that farmers have been reluctant to sell given low prices. This has filled up the country's bins and created a storage problem for the its also-impressive safrinha corn crop. Outdoor grain piles are growing and silo bags are once again in high demand. In Mato Grosso, the country's top grain producing province, the storage gap is estimated around 39 MMT, according to the growers' association Aprosoja. Already, concerns are growing that there may not be enough space for the 2017-18 soybean crop if the massive corn crop is not cleared out in time.
Bannon comments on his push for the U.S. to take a tougher stance on China trade... "The economic war with China is everything," Steve Bannon, a chief strategist for President Donald Trump, said in an interview with The American Prospect posted online Wednesday. "And we have to be maniacally focused on that. If we continue to lose it, we're five years away, I think, 10 years at the most, of hitting an inflection point from which we'll never be able to recover."
Trump reportedly agrees to seek South Korea's OK before taking action on North Korea... In the aforementioned interview, Bannon also undercut Trump on North Korea when he said, contrary to Trump's threat of fire and fury, "There's no military solution [to North Korea's nuclear threats]. Forget it. Until somebody solves the part of the equation that shows me that 10 million people in Seoul don't die in the first 30 minutes from conventional weapons, I don't know what you're talking about." Meanwhile, South Korean President Moon Jae-in said Trump agreed to discuss options, including a U.S. military strike, with him before taking any action to rein in North Korea's nuclear and missile programs. His remarks came after Pyongyang's announcement that it would hold off on a threat to launch missiles at the U.S. Pacific territory of Guam.
Government to make critical ObamaCare subsidy payments... The U.S. government will make crucial ObamaCare-related subsidy payments to health insurers in August, according to a White House spokesman, despite threats by Trump to end them. The decision came a day after the Congressional Budget Office warned that ObamaCare premiums would soar an additional 20% above projected price increases if the payments are ended in 2018.
EPA extends comment period 30 days on rescinding WOTUS rule... The public will get another month to comment on EPA's proposal to repeal the Obama-era waters of the U.S. (WOTUS) rule, with comments now due Sept. 27. When the proposal was officially released, the 30-day comment period was a point of contention for supporters of the Obama-era rule. The original Obama rule, also called the Clean Water Rule, was open for more than 200 days.
Still waiting for cash cattle trade... The cattle market is still waiting for cash trade to begin and a total lack of sales at the online Fed Cattle Exchange auction yesterday provided little direction. So far this week, just a few hundred head of cattle have changed hands in the Iowa/Minnesota market at $110 and in Nebraska at $111, down from last week's average cash price of roughly $115 and just slightly above where futures are trading. Also of note, while boxed beef prices dropped 83 cents (Select) to $1.44 (Choice) on Wednesday, the softer prices spurred impressive movement of 201 loads.
Bearish reversal could lead to more pressure today... Lean hog futures posted bearish reversals on Wednesday, signaling followthrough selling is likely today. So far, futures have not done major technical damage, but that could change if followthrough selling is heavy today. Yesterday's product market action could add to bearish sentiment as belly prices plunged $13.62, pulling the pork cutout value $2.31 lower.
Overnight demand news...  Tunisia bought 100,000 MT of milling wheat but it made no purchase in its tender to buy 50,000 MT of animal feed barley. Jordan made no purchase in its international tender to buy 100,000 MT of optional origin wheat. Egypt bought 295,000 MT of wheat from Russia and 60,000 MT o wheat from Ukraine.
Today's reports:

7:30 a.m., Drought Monitor -- USDA/NWS
7:30 a.m., Extended Weather Outlook -- NWS
7:30 a.m., Weekly Export Sales-- FAS