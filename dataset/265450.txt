USDA gives almost $9M to fix Maine city's wastewater system
USDA gives almost $9M to fix Maine city's wastewater system

The Associated Press

BATH, Maine




BATH, Maine (AP) — The U.S. Department of Agriculture is giving a Maine city nearly $9 million in grants and loans to overhaul its wastewater system.
Republican Sen. Susan Collins and independent Sen. Angus King say USDA Rural Development is giving Bath a $2.3 million grant, while the agency is also providing a $6.5 million Water and Waste Disposal Direct Loan. The senators say the city is using the money to begin a series of upgrades right away.
Bath's wastewater treatment facility is more than 45 years old. The senators say it's overdue for repairs, upgrades and maintenance despite a pair of prior upgrades in the 1990s.