Chicago Mercantile Exchange lean hog futures on Tuesday were weakened for a second day in a row as ample supplies pressured cash prices, said traders.
Fund sales and sell stops exerted more pressure on CME hog contracts, they said.
February hogs finished down 0.500 cent per pound to 66.900 cents, and below the 100-day moving average of 66.936 cents. April closed 0.575 cent lower at 71.675 cents.
Packers avoided raising bids for hogs that are readily available as plants prepare to close at least one day for the Christmas holiday, said traders and analysts.
Other than fill-in business, supermarkets bought most of the pork they need until they determine how much of it moves over the holiday, they said.
Investors squaring positions before taking off for extended year-end holiday vacations could stir market volatility in the coming days.
A few market participants exercised caution in advance of the U.S. Department of Agriculture's (USDA) quarterly hog report on Friday at 11 a.m. CST (1700 GMT).
Simultaneously USDA will release its monthly Cattle-On-Feed and cold storage reports. 
Mainly Lower Cattle Futures
Most CME live cattle contracts settled weaker on positioning before Friday's Cattle-On-Feed report, said traders.
Analysts, on average, expect Friday's report to show 5.7 percent more cattle were placed in feedlots in November than a year ago.
Expectations for steady to possibly firmer cash prices this week underpinned the December live cattle contract.
December live cattle finished up 0.025 cent per pound to 120.000 cents. February ended down 0.150 cent to 120.450 cents, and April closed 0.550 cent lower at 121.475 cents.
On Tuesday Nebraska feedlots did not respond to packer bids of $119 to $120 per cwt for slaughter-ready, or cash cattle.
Sellers in Kansas and Texas priced cattle at $122 per cwt, with no bids from processors.
Last week feedlots sold cattle in the U.S. Plains for $118 to $120 per cwt.
"I do think the market is stronger if feeders can just not get nervous," a feedlot manager said, citing tight cattle supplies in parts of the region.
Wednesday's Fed Cattle Exchange sale of 466 animals may offer an indication regarding this week's overall cash cattle trade.
CME feeder cattle felt pressure from sell stops, technical selling and lower deferred-month live cattle futures. January feeder cattle closed 2.450 cents per pound lower at 145.200 cents.