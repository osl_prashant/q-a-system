Trump, GOP score major legislative victory | Ag cooperatives note displeasure over tax language 



The Senate passed, 51-49, legislation to overhaul the tax code early Saturday morning, handing Republicans a major legislative victory. Vice President Pence presided over the final passage vote. 
Republican senators will now move to reconcile their legislation with the House’s proposal, passed in mid-November, as they try to get a final product to Trump’s desk by the end of the year. Broad differences between the House and Senate plans will still need to be resolved, though the final changes made Friday brought the Senate version closer in line with the House-passed bill. The House is set to take a vote Monday on going to conference with the Senate to work out a final tax plan.
President Donald Trump Saturday morning took questions from the press, according to pooler James Osborne of the Houston Chronicle. In remarks about the Senate tax overhaul bill, Trump said: "It was a fantastic evening last night. We passed the largest tax cuts in the history of our country and many other things along with it. Now we go onto conference and something beautiful is going to come out of that mixer. People are going to be very, very happy. They're going to get tremendous, tremendous tax cuts and tax relief, and that's what this country needs. Business tax all the way down from 35[%] to 20. It could be 22 when it comes out but it could also be 20. We'll see what ultimately comes out."
Sen. Bob Corker (R-Tenn.) was the only GOP naysayer on concerns over the impact on the deficit. No Democratic senator supported the legislation. “I wanted to get to yes. But at the end of the day, I am not able to cast aside my fiscal concerns and vote for legislation that I believe, based on the information I currently have, could deepen the debt burden on future generations,” Corker, who is retiring after 2018, said in a statement.
The bill as amended would cost $1.45 trillion over 10 years, according to the Congressional Budget Office. The deficit impact is likely somewhat smaller after taking economic growth impacts into account — a Joint Committee on Taxation (JCT) "dynamic" analysis of the pre-managers' amendment version found the cost had shrunk from about $1.41 trillion to $1 trillion over a decade.
Republicans have rejected the JCT analysis, saying it substantially underestimates the pace of economic growth under the GOP tax plan. Senate Majority Whip John Cornyn (R-Texas) had said the JCT dynamic score was "clearly wrong."
While Republicans hailed the action as another step toward passing the first major tax code rewrite since 1986, Democrats slammed the tax bill as a deficit-busting giveaway to corporations and wealthy Americans.
The bill would lower tax rates for individuals through 2025 and permanently cut the corporate tax rate from 35% to 20%. The bill’s tax cuts for individuals are temporary in order to comply with budget rules that the measure can’t add to the deficit after 10 years.
The bill would also repeal ObamaCare’s individual mandate, a priority for President Trump and many Republicans, and open up a section of the Arctic National Wildlife Refuge (ANWR) for drilling.
Sens. Ron Johnson (R-Wis.) and Steve Daines (R-Mont.) came on board when they were able to get the deduction percent increased for pass-throughs from 17.4% to 23% and that Johnson would be involved in further pass-through discussions as lawmakers work to get the bill to Trump’s desk. "A seat at the table. Not just input. Not just consulting, but a seat at the table," Johnson said when asked what leadership promised him in exchange for voting yes.
Agricultural cooperative sources are not pleased with the final Senate tax overhaul language. Lobbyists for the industry were pushing an amendment by Sen. John Hoeven (R-N.D.) that would have prevented a tax increase on farmers and their co-ops as it would have retained the “Domestic Production Activities Deduction” (DPAD), also known as Section 199, for agriculture. One source noted that the calculation under Section 199 is limited by the cooperative’s wage base — the new proposal bases it on the farmer’s wage base, which may be very small. This provision especially hurts Midwestern grain farmers, according to co-op contacts.
Included in the Senate-passed measure is language secured by Sen. John Thune (R-S.D.) meant to improve the underlying provisions in the bill that serve as a replacement for the elimination of Section 199. In the Senate bill, the Section 199 deduction is repealed as of Dec. 31, 2017 and replaced with: Patrons of farmer cooperatives would receive a 23% deduction on “qualified cooperative dividends,” defined to include patronage dividends, per-unit retain allocations, qualified written notices of allocations, or similar amounts. The deduction is limited to 23% of the patron’s taxable income. Individuals with income in excess of $250,000 ($500,000 for a joint return) would be subject to limit of 50% of wages.
The cooperative would also receive a 23% deduction on taxable income, limited to 50% of the cooperative’s wages.
The deductions expire December 31, 2025.
Also included was language offered by Sen. Pat Roberts that would allow farmer cooperatives to elect out of the interest deductibility limitation — the same treatment that farmers are getting under the bill. Cooperatives said this inclusion is a welcomed addition.
The following is a statement from National Council of Farmer Cooperatives (NCFC) President Chuck Conner on Senate passage of tax legislation:
“It is deeply unfortunate that the Senate failed to include continuation of the Domestic Production Activities Deduction (DPAD), also known as the Section 199 deduction, for agriculture in their tax reform bill. This action creates tremendous uncertainty as farmers plan for the coming year and they will need to quickly assess the impact of this legislation with their accountants and lenders.
“We would like to recognize the leadership of Senator John Hoeven of North Dakota, who throughout the Senate debate worked tirelessly to see Section 199 for agriculture retained. Senator Pat Roberts of Kansas was also instrumental in ensuring that farmer co-ops and their members are treated equitably in provisions dealing with the deductibility of business interest.
“We also commend Senator John Thune of South Dakota for his success in ensuring that farmer cooperative members are able to take of advantage of the deduction for flow-through income and that farmer cooperatives also receive a tax benefit.
“As the House and Senate work to reconcile their tax bills in the coming days, we look forward to working with Senators Roberts and Thune, as well as other supporters on the conference committee, to improve on the Senate provisions to ensure to ensure that the final bill does not raises taxes on farmers and their co-ops.”
Sen. Jeff Flake (R-Ariz.) decided to vote for the bill by getting a new approach to the bill’s expensing deduction, which allows businesses to write off the full cost of investments in equipment and facilities. The change calls for gradually phasing out the break after five years instead of abruptly canceling it. That adds $34 billion to the cost of the bill, but Flake said it would save money in the longer term by making lawmakers less likely to extend the break in the face of pressure from business interests. Flake also said the administration and Senate leaders had agreed to work with him toward a resolution for immigrants brought illegally to this country as children. Known as “dreamers,” these immigrants were granted temporary protections under the Obama administration, which Trump has announced he will revoke in March. Flake is a longtime proponent of reforming immigration laws and wants permanent protections for dreamers. He said Vice President Pence had committed to working with him on the issue, though without offering a timeline or a specific solution.
Sen. Susan Collins (R-Maine) said leadership had promised her the bill would protect certain deductions individuals use to lower their tax bills, including on matters related to medical expenses and tax payments to state and local governments. Collins also said leadership had agreed to support passing two bipartisan bills to help stabilize the health insurance system set up under the Affordable Care Act (ObamaCare).
More info on Senate-passed tax overhaul bill.

Child tax credit for low-income families. The Senate defeated, 29-71, an attempt by Sen. Marco Rubio (R-Fla.) and Mike Lee (R-Utah) to expand the child tax credit for low-income families, which would have been paid for by setting the corporate tax rate at 20.94%. 
Vice President Mike Pence broke a tie in favor of a proposal from Sen. Ted Cruz (R-Texas) to allow the use of 529 savings accounts to pay for elementary and secondary school costs, including private-school tuition.
Pass-through businesses. Sens. Ron Johnson (R-Wis.) and Steve Daines (R-Mont.) won bigger tax breaks for pass-through businesses such as partnerships and S corporations — they were able to get the deduction percent increased for pass-throughs from 17.4% to 23% and that Johnson would be involved in further pass-through discussions as lawmakers work to get the bill to President Donald Trump’s desk. More than half of U.S. business income goes to pass-throughs, and more than half of that goes to the top 1% of households.
Expand small business deductions to "agricultural or horticultural cooperatives" and master limited partnership distributions — which are mostly oil and gas pipeline operators.
Depreciation rules. Sen. Jeff Flake (R- Ariz.) secured more aggressive depreciation rules to encourage business investment after 2022.
Sen. Susan Collins (R-Maine) scored a $10,000 deduction for property taxes, an expanded but temporary deduction for people with large medical expenses, and a promise of future bipartisan health-care legislation to mitigate the effects of repealing the individual health-insurance mandate. To help pay for some of those changes, Republicans increased a new tax on companies’ stockpiled foreign profits to 14.5% for cash and 7.5% for illiquid assets, from 10% and 5% in a previous version.
AMT. The final Senate bill would scale down, rather than eliminate, the alternative minimum tax (AMT) for individuals and keep the current corporate AMT.
One Democratic amendment was adopted. Sen. Jeff Merkley (D-Ore.) on a 52-48 vote was able to strike language that would exempt certain colleges and universities that opt out of federal funding from a new excise tax on endowment funds. Sen. Patrick Toomey (R-Pa.) sponsored the provision in the underlying bill, which got hit with a wave of negative publicity tying the provision to Hillsdale College in Michigan and its ties to the family of Education Secretary Betsy DeVos. Republicans voting for the amendment were Susan Collins of Maine, John Kennedy of Louisiana, Deb Fischer of Nebraska and Lisa Murkowski of Alaska.
SPR tapped. Senate Republicans used the Strategic Petroleum Reserve (SPR) to help overcome a parliamentary snag in a portion of the tax overhaul bill that opens the Arctic National Wildlife Refuge (ANWR) to oil and gas drilling. The fix would direct the Department of Energy to sell 7 million barrels of oil in the SPR between the fiscal years of 2026 and 2027. The legislation would limit the sale to no more than $600 million. That funding would help make up for the Congressional Budget Office's reduced 10-year revenue projections for federal oil and gas lease sales in the reserve, a change triggered by a Senate parliamentarian's decision. The parliamentarian, at the request of Democrats, ruled that a provision in the ANWR title violated the Byrd rule because it undercut environmental review laws. The Byrd rule prevents items that don't have a direct budgetary impact from being included in a reconciliation bill unless backers can round up 60 votes. According to the Energy Department, the SPR currently holds about 688 million barrels.
Auto dealers won an exemption from new limits on business interest deductibility, similar to the House-passed bill.
Eliminate an existing tax perk that allows members of Congress to deduct up to $3,000 a year in living expenses. This was pushed by Sen. Joni Ernst (R-Iowa).
Capital expensing. Eliminate what Sen. Jeff Flake (R-Ariz.) referred to as a “budget gimmick” in the original measure that purports to save money by allowing 100% capital expensing to expire after 2022. Instead, the Senate tax bill would now provide for a gradual, five-year phase-out of full expensing beginning in 2023. Flake’s proposal would reduce expensing to 80% in 2023 and by another 20 percentage points each year until falling to 20% at the end of the 10-year period. That change would result in a $34 billion revenue loss over 10 years.

Differences between the Senate and House bills that will have to be worked out in conference include:

AMT. The Senate bill kept the AMT on corporations and raised the exemption for the individual AMT, instead of repealing each. The House bill completely repeals both.
SALT deduction. Both the House and the Senate limited the state and local tax deduction to a $10,000 deduction for property taxes. Some House members may want a higher deduction.
Pass-through rate. The Senate bill allows pass-through corporations (small businesses that file taxes on their owner's return) to deduct 23% of business income, while the House created a 25% rate for business income (and a 9% rate for lower-income individuals).
ObamaCare individual mandate. The Senate repeals it. The House doesn't, although it has said it will be kept in the bill in conference.
Estate tax. The House and Senate bills double the size of the estate tax exemption (the Senate tax language sunsets beginning in 2026). The House bill repeals the tax after 2024, but the Senate bill does not — both bills maintain a full stepped-up basis for inherited property.
Individual rates. The House has 4 rates and keeps the current top rate of 39.6%. The Senate has 7 rates and a top rate of 38.5%. The House bill’s tax rates are permanent while the Senate tax bill would temporarily cut tax rates for families and individuals until 2025.
Corporate tax rate reduction to 20% starts in 2018 in the House bill, 2019 in the Senate version. 
Mortgage interest deduction. The House lowers the cap to $500,000 while the Senate keeps it at $1 million.
Child tax credit: $2,000 per child in the Senate version and $1,600 per child in the House plan.
	 


Current tax brackets and those proposed in the House plan: 
Single Return




Current Law


House Proposal






Taxable Income


Tax Rate


Taxable Income


Tax Rate




Up to $9,325


10%


Up to $45,000


12%




$9,325 to $37,950


15%


$45,000 to $200,000


25%




$37,950 to $91,900


25%


$200,000 to $500,000


35%




$91,900 to $191,650


28%


Over $500,000


39.6%




$191,650 to $416,700


33%


 


 




$416,700 to $418,400


35%


 


 




Over $418,400


39.60%


 

 



Joint Return




Current Law


House Proposal






Taxable Income


Tax Rate


Taxable Income


Tax Rate




Up to $18,650


10%


Up to $90,000


12%




$18,650 to $75,900


15%


$90,000 to $260,000


25%




$75,900 to $153,100


25%


$260,000 to $1 million


35%




$153,100 to $233,350


28%


Over $1 million


39.6%




$233,350 to $416,700


33%


 


 




$416,700 to $470,700


35%


 


 




Over $470,700


39.6%


 

 



Current tax brackets and those proposed in the Senate plan:
Single Return














Current Law


Senate Proposal






Taxable Income


Tax Rate


Taxable Income


Tax Rate




Up to $9,325


10%


Up to $9,525


10%




$9,325 to $37,950


15%


$9,525 to $38,700


12%




$37,950 to $91,900


25%


$38,700 to $60,000


22.5%




$91,900 to $191,650


28%


$60,000 to $170,000


25%




$191,650 to $416,700


33%


$170,000 to $200,000


32.5%




$416,700 to $418,400


35%


$200,000 to $500,000


35%




Over $418,400


39.60%


More than $500,000


38.5%




Joint Return














Current Law


Senate Proposal






Taxable Income


Tax Rate


Taxable Income


Tax Rate




Up to $18,650


10%


Up to $19,050


10%




$18,650 to $75,900


12%


$19,050 to $77,400


12%




$75,900 to $153,100


22%


$77,400 to $140,000


22.5%




$153,100 to $233,350


24%


$140,000 to $320,000


25%




$233,350 to $416,700


33%


$320,000 to $400,000


32%




$400,000 to $470,000


35%


$400,000 to $1 million


35%




More than $470,000


39.6%


More than $1 million


38.5%




Senate also proposes new tax brackets for head-of-household filers, and for married couples who opt to file separate returns.