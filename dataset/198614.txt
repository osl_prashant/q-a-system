For most dairy managers, working with the animals is highly preferable to working with the people, according to Bernie Erven, Professor Emeritus at The Ohio State University. But as dairies grow, the "people" element becomes critical to ensuring the animals receive optimum care, and that the business is ultimately successful. "I've never seen a business succeeding while the people are failing," Erven told the audience at the 2016 Dairy Calf and Heifer Association annual conference. "Dairy businesses are complicated, and they are not 'bottom-up' enterprises. They require leaders to set standards create a positive tone for their working culture."
 Erven said managers need to be positive examples to team leaders and middle managers, then trust and empower those people to do their jobs. "Hold yourself accountable so others learn the importance of doing the same," he advised. "As a senior manager, you are both a teacher and a leader of leaders."
 He noted the importance of listening to middle managers, and both providing and soliciting feedback. He also underscored the need for senior managers to make expectations clear, and recognize valuable contributions that middle managers make to the business.
 Delegation, he said, is critical for both getting the work done, and for developing leadership in middle managers. Erven defined delegation as, "the act of entrusting authority and responsibility to another, while retaining ultimate accountability." He posed these true-false statements to managers to assess whether or not they are effective delegators:
(1) I tend to be a perfectionist
(2) I often work more hours than everyone else
(3) I often lack time to clearly and concisely explain how a task should be done
(4) I am often interrupted
(5) I still prefer to do the tasks I did before becoming a senior manager
(6) I have to spend a lot of time reviewing the work of people I supervise
(7) I usually am more committed than other managers
(8) I get upset when other people don't do something right
(9) I feel a need to keep a finger in every pie
(10) I like to be in control of how and when things are done
Erven advised that answering "true" to three or more of these statements is an indication that you probably have problems with delegation. He offered these tips for more successful delegating:
¬? Define the job being delegated
¬? Delegate to the right people
¬? Assess ability and training needs
¬? Explain assignments, importance, additional training, deadlines and why the person is chosen
¬? Describe in detail the authority and responsibility being delegated
"Delegation is not a natural skill," said Erven. "But it can be learned. You have to practice and improve it."