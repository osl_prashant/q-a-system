A May 11 auction of 196 acres of high quality farmground in Hancock County, Illinois brought a total of $2,134,104.00 for three parcels for an average per acre price of $10,704.24.
Tract 1: 52.02 acres tillable, PI rating 144.
Tract 2: 57.3 acres tillable with roughly 5 acres wooded and a small pond, PI rating 141.7.
Tract 3: 72.95 acres tillable with roughly 8 acres wooded, PI rating 134.6.
A May 4 auction in Monroe County, Missouri fetched a total of $215,000.20 for 2 parcels at an average of $5,155.88 per acre. The property is located approximately 1 1/2 miles north of Holiday, MO.
Tract 1: 36.70 acres; 23.47 acres tillable with the balance in hardwood timber and wooded draws with river frontage.
Tract 2: 5 acres with 1,800 sq ft ranch style home built in 2009, adjacent to tillable tract.
Both sales were handled by Dale Bolton, Sullivan Auctioneers of Hamilton IL, 217-847-2160.