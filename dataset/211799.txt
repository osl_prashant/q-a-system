One of the new tables in the extensive Consumer Expenditures report from the Bureau of Labor Statistics is data organized by the generation of consumers.The data offers helpful insights on what consumers spend on virtually all consumer goods, including fresh fruits and vegetables.
Checking out the spending by generation table, here are some top line observations:
The mean average spending per consumer unit (household average 2.5 people) in 2016 is:
For fresh fruit, the overall average is $288 per household, compared with $254 per household for fresh vegetables.
Biggest spenders for both fresh fruits and fresh vegetables, per consumer unit, is Generation X (born 1965 to 1990). Gen X spending for fresh fruit of $353 per consumer unit compares with $283 for Baby Boomers (1946 to 1964) and $238 for Millennials and younger (1981 or later).
For fresh vegetables, the average spend was $307 per year for Gen X, $262 for Boomers and $209 for Millennials.
For all the attention that Millennials receive, the latest Consumer Expenditures report shows, on a  household measure basis, Gen X is clearly the king of the hill of fresh produce consumption.