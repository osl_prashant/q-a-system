Southeast U.S. growers want to build a wall against cheap Mexican produce imports. 
And, at the start of renegotiation of the North American Free Trade Agreement, they have the ear of the Trump Administration.
 
One of the bullet points in the U.S. Trade Representative’s objectives for NAFTA renegotiation is “seek a separate domestic industry provision for perishable and seasonal products in anti-dumping and countervailing duty proceedings.”
 
U.S. Trade Representative Robert Lighthizer has previously worked on deals to protect the U.S. steel industry, said Gary Hufbauer, Reginald Jones Senior Fellow at the Washington, D.C.-based Peterson Institute for International Economics. 
 
That, combined with the protectionist slant of the Trump administration, could make the new anti-dumping tool a real possibility, Hufbauer said.
 
“The Florida growers love this idea. They want to be able to limit imports — particularly imports but other seasonal products as well which are coming in and overlapping with their growing season,” he said.
 
Hufbauer said a change in dumping regulations to allow for regional producers of perishable commodities to complain they are being injured by imports from Mexico or Canada could spark trade actions by growers across the country.
 
“Once you change the law to achieve that, then you could have cases for different crops and different parts of the country right through the year,” he said. “It would represent a huge change for perishable and seasonal crops.”
 
However, the gain for Southeast growers could spoil the fortunes of other U.S. produce exporters.
 
For U.S. exporters of apples and pears, who have found strong markets in Mexico over the last two decades, Hufbauer said there could be risks.
 
“If this is going to be the new rule within NAFTA, you can be sure Canada and Mexico will adopt it as well,” he said. “Exporters will be hurt by this, (along with) distributors of these products from Mexico and Canada.”
 
If Mexico and Canada were to agree to the new anti-dumping protections for seasonal produce, Hufbauer said the new tool would only be used for trade between the three countries. 
 
He said there is “no way” the World Trade Organization would allow the U.S. to put in place new anti-dumping tools unilaterally.
 
With the first of the  NAFTA negotiating sessions set for mid-August, Hufbauer said there seems little chance that the NAFTA trade talks can be finished by the first of next year, a goal of the Trump White House.