Grain higher and livestock lower
Grain higher and livestock lower

The Associated Press



Wheat for March advanced 21.25 cents at 4.8450 a bushel; March corn was up 4 cents at 3.7450 a bushel; March oats rose 3 cents at $2.63 a bushel; while March soybeans gained 7 cents at $10.45 a bushel.
Beef and pork were lower on the Chicago Mercantile Exchange February live cattle was off .52 cent at $1.2750 a pound; March feeder cattle fell 1.80 cents at $1.4475 a pound; while April lean hogs lost 2.73 cents $.6722 a pound.