Nathan Reed is rumbling north on a Lee County levee with Crowley’s Ridge rising a mile west and the old St. Francis River rolling two miles east. Wedged in the flats between levee and river, farmland flashes by in hazy green blocks as the gray Ford F-150 Raptor crunches gravel on the levee spine. Reed brings up his right hand in an arc and points across a non-GMO landscape of corn, cotton and soybeans. “Money and survival,” he says. “This is about the total price picture.”
In 2014, Reed fought for financial breath even after skinning inputs one by one. No matter how he shifted the figures, the pencil always pointed to the glaring expense of biotech seed. With an eye on cost control, he began switching portions of his ground to non-GMO production supported by a minimum till cover crop scheme, and the change led to farm-wide profitability.
With 6,500 acres split across Lee and St. Francis counties in northeast Arkansas, Reed’s crop roster includes non-GMO corn, cotton and soybeans, along with transgenic varieties as well. When Reed, 37, began farming solo in 2005, the expense framework was relatively forgiving. Cotton sold for 65 cents per pound; transgenic cotton seed was $60 per acre; Roundup smoked weeds; residuals were an afterthought; and Reed paid $120,000 for a new tractor. Fast forward a mere 12 years to 2017: Cotton at 75 cents per pound; transgenic cotton seed at roughly $140 per acre; residuals at $60 per acre; and Reed can’t buy an equivalent tractor for less than $240,000.
When cotton dropped into the high 50-cent range in 2014, Reed had $1.2 million invested in picker equipment. The hard math demanded change and he planted 250 acres of non-GMO cotton varieties developed by Fred Bourland with the University of Arkansas (UA) System Division of Agriculture. Successive years saw steady acreage increases and Reed planted 1,300 non-GMO cotton acres in 2017. Generally, he aims for yields at 1,200 lb. per acre on irrigated ground and 1,000 lb. per acre on gumbo and dryland.
Pen to paper, Reed estimates he saves $80 per acre with non-GMO cotton versus biotech cotton. He pays $25-$30 for non-GMO seed, treats it with Staple LX and sprays for worms, which brings his total to $75-$80. He then tries to push those savings toward $100 to $120 per acre. The cereal rye cover reduces water use by 30%-50% and saves a significant amount of fertilizer use. In addition, Reed is almost no till and the elimination of deep ripping saves $10-$12 per acre.




 


Reed checks non-GMO cotton planted into cereal rye. He estimates savings of $80 per acre with non-GMO cotton and tries to push those savings toward $100 to $120 per acre.


© Chris Benentt







“Nathan generally spends $60-$80 dollars less per acre in non-GMO cotton compared to his other cotton, but sometimes his savings are even more. He’s ensuring those savings with cover crops and reduced tillage to make a tremendous difference in weed control and soil health,” says cotton agronomist Bill Robertson, with the UA System Division of Agriculture. “He does run ragged in the spring and must be Johnny on the spot to keep pigweed under control. Non-GMO forces him to be extra sharp with weed control timing.”
Reed’s silt loam lacks soil structure and suffers from diminished water filtration, but cereal rye directly addresses the problem. Following irrigation on many farms, Robertson routinely hits dry soil with a soil probe at a mere 5”. On Reed’s cover crop ground after irrigation, Robertson can punch down 16” without hitting dry soil. “Nathan is doing a great job with the profile and he’s farming much more than the top 6”,” Robertson notes.
“The cereal rye makes such a tremendous difference for his soil profile. Doubling or even tripling your effective rooting zone makes life much easier for the crop to get the moisture and nutrients it needs,” Robertson explains. “A plant forced to do all the work in top few inches is going to get hurt.”
Reed’s cereal rye helps non-GMOs with weed control, but also works below the surface to improve soil health, according to Robertson: “It’s pretty simple: Improved soil health in corn, cotton, soybeans or any crop means improved profit.”
Instead of trying to make 1,300 lb. of cotton at 75 cents, Reed aims for 1,100 lb. per acre and maintains the same profit line. Due to his buckshot ground, it’s extremely difficult to average 1,300 lb. … but he can safely hit 1,100 lb. “I need a high gross crop to survive. The non-GMO fields compare very closely with GMO fields, but I’m saving costs in combination with cover crops,” he says.
In 2016 Reed teed off with non-GMO production. He boosted non-GMO cotton to 2,000 acres, all blanketed with cereal rye to choke Palmer amaranth. He also planted his entire soybean crop (1,200 acres) in non-GMO production, and had no irrigated yields below 60 bu. per acre and no dryland yields below 35 bu. per acre. “I took the things pointing toward profit and expanded in 2016,” he says. “It was more work, but that’s what farmers have to do to stay in business.”




 


Reed plants non-GMO soybeans on his rougher ground in a rice rotation. He pays roughly half the cost of dicamba-tolerant seed and gets a premium from Ozark Mountain Poultry.


© Chris Benentt







Reed plants non-GMO soybeans on his rougher ground in a rice rotation. He pays roughly half the cost of dicamba-tolerant seed and gets a premium from Ozark Mountain Poultry (OMP). “I can make 35 bu. dryland beans profitable. If I’d gone the other route, I’d be breaking even or losing at $9.50 soybeans,” he says.
Most of his soybeans are broadcast behind rice in heavy clays to get a fast canopy. He works the ground once, puts out Treflan and lays down a residual to choke Palmer amaranth. Reed pays close attention to the Palmer maxim: Get it early or cut it with a combine. “With non-GMOs, I can still basically kill anything but pigweed, but that’s no different than Roundup Ready crops,” he says.
“I’ve never given the beans a fair shake yet,” he admits. “They’re always on my worst ground.”
In 2017, a wet spring and a late flood at the beginning of May forced Reed to put in LibertyLink soybeans to safeguard against Palmer, but 75% of his soybean acreage remains non-GMO (800 acres; all irrigated).
Reed doesn’t save soybean seed. “Hauling back and forth for cleaning puts costs near $18 per bu.” he explains. “I pay $23 for new seed and use the cheapest generic fungicides and insecticides I can find for treatment.”
OMP’s premium schedule is based on farmer storage. Reed is a fiber farmer, but has grain bins due to a parallel rice history. He holds all his grain, including 650 acres of non-GMO corn. Non-GMO crops have allowed him to hold grain and get paid for storage. The further from harvest, the higher the premium: His 2016 soybeans didn’t start delivery until June 2017.




 


With 6,500 acres split across Lee and St. Francis counties in northeast Arkansas, Reed’s crop roster includes non-GMO corn, cotton and soybeans, along with transgenic varieties as well.


© Chris Benentt







Overall, the non-GMO food industry (not just soybeans, but all related crops) is estimated at some $300 billion in the U.S. and Canada with global markets surpassing $700 billion. With an estimated annual growth of more than 15%, worldwide markets are expected to reach about $1.5 trillion dollars within the next five years, according to Kelly Cartwright, executive director of the National Soybean & Grain Alliance (NSGA). The premiums, combined with cheaper seed, are driving the production/farm level system which, in turn, feeds the overall industry.
How does that growing demand translate to the field level? “For soybeans, our research and experience show that producers can potentially obtain a premium from $1 to as much as $3 for conventionally grown, non-GMO soybeans, depending on variety, current market demands and other variables. $1 to $2 is relatively common, though it can fluctuate with CBOT markets,” Cartwright says. “It varies depending on food grade compared to commodity grade, with food grade soybeans typically fetching a higher premium.”
Ask a crop question and expect an in-depth response from Reed (a licensed attorney). His answers come at a machine gun pace, punctuated with details on trials, innovations in progress and a healthy dose of self-deprecation. It’s difficult to find a sharper producer and his acumen is tapped through various roles with the Arkansas State Cotton Support Committee (ASCSC), Cotton Incorporated, and National Cotton Council (NCC). Reed doesn’t shy from out-of-the-box swings, and he searches for new agronomic techniques before they become common coin.
Whether furrow-irrigated rice, non-GMO crop systems, 200 acres of black oat cover for seed use for 2018, planting into 6’ tall green cereal rye, running moisture probe trials, or working with Blue River on a robot weed sprayer, Reed consistently shapes something new on his 6,500 acres. He is currently experimenting with non-GMO dryland cotton this year to test low-input yield on rougher land. The non-irrigated cotton needs to yield 733 lb. at 75 cents to break even, he projects: “I’m pushing to make 850 lb., but 1,000 lb. would be great.”
Reed relentlessly compares crop variety performance. He presently has a 10-variety cotton trial (through Robertson and UA Extension) measuring GMO versus non-GMO yield. It’s an apples-to-apples comparison: six top GMO varieties against four top non-GMO varieties.




 


"It’s pretty simple to take a pencil and know what you’ve got in a crop. If it ain’t in the black, something has to give.”


© Chris Benentt







Peak yields aren’t necessarily the best avenue toward profit, evidenced by Reed’s operation. “I have nothing against GMOs, but I can’t escape the seed price. If somebody out there is considering non-GMOs and cover crops, I recommend they walk in slowly. Hey, I’ve had some oops moments and I’ll have some more, but it’s not devastating when seed is about $20 per acre,” he says.
“You can’t be afraid to adapt and get ready for tomorrow,” he adds. “You know your fields. It’s pretty simple to take a pencil and know what you’ve got in a crop. If it ain’t in the black, something has to give.”