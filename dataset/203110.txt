The Food and Drug Administration and the Partnership for Food Safety Education have introduced a new resource for food safety educators.
 
Called the "The Consumer Food Safety Educator Evaluation Toolbox and Guide," the package was unveiled at the Consumer Food Safety Education Conference 2017, according to a news release.
 
The toolbox and guide contain tips, tools, and examples to help consumer food safety educators plan, develop, and evaluate their programs and activities. The free resource is available online for download.