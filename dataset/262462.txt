A year and a half ago, Dairy Herd Management carried a cover story, “Has the Time Come for Selective Dry Cow Therapy?” September, 2016.
We are still waiting for an answer. This February, the National Mastitis Council dedicated a half-day symposium to the subject of selective dry cow therapy. Advice was mixed.
The Dutch experience, where blanket dry cow therapy has been banned since 2012, shows it can be done (see sidebar). But in Denmark, where Streptococcus agalactiae infections were nearly eliminated, the disease has come roaring back along with increased antibiotic use.
What works—or doesn’t work—in small European herds might not be applicable to the U.S. That’s particularly true in large, commercial herds that are drying off 25, 50, 100 or even 200 cows every week.
Washington State University mastitis researcher Larry Fox says some well managed herds can employ a successful selective dry cow therapy program. But at this point, he says, there is no compelling reason to recommend it for everyone.
Still, more producers are looking at selectively dry treating their herds, particularly if they have low herd somatic cell counts (SCCs) and low rates of clinical mastitis. And in tight economic times, saving $8 to $20 per cow in tube costs alone is enticing. 
Producer Experience 
Mitch Breunig, Sauk City, Wis., has been selectively dry treating for about a year. “We went through the Food Armor Program, and selective dry cow treatment was a way to reduce our antibiotic use,” he says.
A couple of points: Breunig’s Mystic Valley Dairy is well managed, milking 450 cows with a herd average of just over 30,000 lb. per cow and somatic cell count level of 78,000 cells/mL.
Breunig is now dry treating less than 20% of his herd at dryoff, and new infections in non-treated cows have not spiked. But he quickly adds he’s learned a few things along the way. “We lost a couple of cows, probably to toxic gram negative mastitis, in the first 60 days we tried selective treatment,” he says.
At that point, he was using an internal teat sealant on all dry cows whether or not he was dry treating. Now, he uses the internal teat sealant only if he dry treats a cow. Breunig has four boxes cows must check to be candidates for no treatment:

Last cell count of lactation
Second to last cell count of lactation
Peak cell count during lactation
Any treatment for clinical mastitis

If any of these cell counts are much above 200,000 cells/mL or if she was treated for mastitis during the lactation, she will be dry treated. “With older cows, we definitely want to make sure she passes all the boxes so we don’t miss any,” Breunig says.
Breunig is also doing weekly somatic cell count testing at freshening. That alerts him to any spikes in somatic cell count, even though a fresh cow might not be showing signs of clinical mastitis. 
Why Do We Dry Treat
Before considering eliminating dry cow treatment, remember why dry cow treatment has been the recommended practice for at least the past 40 years. Treating cows at dryoff has two purposes: to cure existing infections and to cure new infections.
On point 1, the advantage of dry cow treatment is that it’s able to use a higher dose of antibiotic in a longer acting vehicle. Current antibiotics do a reasonably good job of killing contagious mastitis organisms. Where they can falter is in curing coliform infections and other environmental species.
On point 2, researchers have known for more than 30 years the average rate of new infections in untreated dry cows during the dry period is between 8% and 12%. Admittedly, there is also some concern that even long-lasting dry cow therapy can prevent new infections late in lactation. On the whole, however, blanket dry cow therapy does a better job than selectively treating cows in preventing new infections, Fox says.
Milk production increases, fewer cases of clinical mastitis, and lower milk somatic cell counts after calving are all associated with blanket dry cow therapy as opposed to selective therapy, he says.
There is also a significant “herd effect,” notes Fox, meaning that in some herds selective dry cow treatment will work better than others. That likely comes down to how dry cows are managed and the care each dairy farm takes in selecting cows for treatment or nontreatment.
Researchers also know as many as a fourth of teat ends do not fully close within six weeks of dryoff. That’s why the use of internal teat sealants has been recommended. However, the use of internal teat sealants without also using dry cow antibiotics and strict hygiene at the time of sealant insertion can also be problematic. The chance of infusing teats with new infections increases when teat ends are not properly or consistently cleaned and sanitized.
Without a sealant or dry cow antibiotics, teats are vulnerable to new infections at calving. So dry cow housing and management becomes an even more critical part of prevention. 
Antibiotic Resistance and Residues
The primary argument for selective dry cow therapy is it reduces the overall use of antibiotics and should thus reduce the risk of antibiotic resistance.
“This is a fear of many when antibiotics are used without cause. But in the case of blanket dry cow therapy the cause is clear: Complete dry cow therapy is used to cure existing infections and prevent new infections,” Fox says.
“Moreover, there is no evidence to indicate that blanket dry cow therapy has led to the selection of antibiotic resistant mastitis pathogens,” he says. Studies at the University of Tennessee and tracking studies by Zoetis have shown no patterns of increased antibiotic resistance.
In addition, the risk of antibiotic-tainted milk reaching consumers continues to decline. The percentage of positive tanker loads of milk has fallen from 0.03% in 2007 to just over 0.01% in 2016.
The problem is the percentage of producers with a violation has plateaued. It was at about 0.1% in 2007, and has hovered at 0.06% since 2012. “There are some herds with problems while the vast majority of herds are able to keep residues out of the milk supply,” Fox says.
He says the problem with residues might lie with a few dozen herds, and going to selective dry cow therapy for everyone likely won’t reduce the rate of residue violations. 
Cost-Benefit 
Research to date on the economic benefits of selective dry cow treatment is limited. In a University of Minnesota pilot project examining the practice, selective dry cow therapy saved $2.62 per cow.
That was after the additional cost of labor to sample, culture and treat lactating animals was compared to the cost savings of 48% less antibiotic use, says Sandra Godden, a veterinarian with the University of Minnesota. “Even if the selective dry cow therapy programs only just breakeven … it is important to know that they are at least economically feasible,” Godden says.
“No doubt, selective dry cow therapy can save money by a reduction in antibiotic purchased and administered,” Fox says. “A full-scale study comparing the cost-tobenefit ratio of selective versus complete dry cow therapy would be helpful.”
“Some herds with excellent management and excellent mastitis control [can] employ a program of selective dry cow therapy quite effectively,” he says.
“But it does not appear that at this time there is any compelling reason to recommend that all, most or even many dairy herds should choose selective dry cow therapy over blanket dry cow therapy.”
 
Note: This story ran in the March 2018 magazine issue of Dairy Herd Management.