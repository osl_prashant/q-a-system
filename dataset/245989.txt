BC-US--Cotton, US
BC-US--Cotton, US

The Associated Press

New York




New York (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Monday:
(50,000 lbs.; cents per lb.)
COTTON NO.2Open    High    Low    Settle     Chg.Mar       83.81   85.41   82.60   85.41  Up   2.69May       82.50   85.30   82.24   85.23  Up   3.14Jul       82.63   85.05   82.48   84.98  Up   2.63Aug                               77.95  Up    .90Oct       78.79   79.79   78.79   79.79  Up   1.32Oct                               77.95  Up    .90Dec       77.00   77.95   77.00   77.95  Up    .90Dec                               78.00  Up    .85Mar       77.29   78.00   77.27   78.00  Up    .85May       77.10   77.90   77.10   77.90  Up    .82Jul       77.38   77.64   77.38   77.64  Up    .71Aug                               72.49  Up    .43Oct                               75.29  Up    .54Oct                               72.49  Up    .43Dec       72.20   72.49   72.20   72.49  Up    .43Dec                               72.86  Up    .42Mar                               72.86  Up    .42May                               73.65  Up    .40Jul                               73.88  Up    .40Aug                               73.00  Up    .40Oct                               73.57  Up    .40Oct                               73.00  Up    .40Dec                               73.00  Up    .40