Harvest of clams continues to dwindle in New England
Harvest of clams continues to dwindle in New England

By PATRICK WHITTLEAssociated Press
The Associated Press

PORTLAND, Maine




PORTLAND, Maine (AP) — The harvest of soft-shell clams is dwindling along the coast of New England, where the shellfish are embedded in the culture as much as the tidal muck.
Soft-shell clams, also called "steamers" or "longnecks," are one of the northeastern U.S.'s most beloved seafood items, delighting shoreside diners in fried clam rolls, clam strips and clam chowders. But the nationwide harvest fell to a little less than 2.8 million pounds (1.2 million kilograms) of meat in 2016, the lowest total since 2000, and there are new signs of decline in Maine.
The Pine Tree State produces more of the clams than any other, and state regulators there say clam harvesters collected a little more than 1.4 million pounds (0.64 million kilograms) of the shellfish last year. That's the lowest total since 1930, and less than half a typical haul in the early- and mid-1980s.
The clam fishery is coping with a declining number of fishermen, a warming ocean, harmful algal blooms in the marine environment and growing populations of predator species, said regulators and scientists who study the fishery. It leaves clammers like Chad Coffin, of Freeport, Maine, concerned the harvest will decline to the point it will be difficult to make a living.
"It has been a gradual decline, and it's getting to the point where there's a tremendous amount of acreage that's not producing anymore," Coffin said. "It should drop significantly more over the next two years."
The clams are still readily available to consumers, but the number of harvesters digging for them has slipped to about 1,600 in Maine. It was more than 2,000 as recently as 2015. The clams are also harvested in smaller numbers in Massachusetts, New York and Maryland, and the haul has been more steady in those states in recent years, helping keep prices about the same.
The value of Maine's clams dipped by nearly $4 million last year, in part due to supply from areas outside the state, the state Department of Marine Resources announced in March. Department public health bureau director Kohl Kanwit attributed Maine's diminished harvest to clamming closures necessitated by algal blooms that render the shellfish unsafe to eat.
Growing numbers of crabs, fish and worms that eat the clams are another problem, said Brian Beal, a professor of marine ecology at the University of Maine at Machias. The growth of predators could be tied to rising ocean temperatures in the Gulf of Maine, which is a trend that figures to continue, Beal said.
"Seawater temperature is driving the biological and environmental factors that regulate clam populations," Beal said. "That spells doom and gloom for the clamming industry and probably for other industries as well."
Soft-shell clams are one of several types of clams people buy in grocery stores and restaurants. Clammers collect them by raking for them in tidal areas, while some other species are harvested by boats at sea.
The clams are the signature item of a popular summer festival in Yarmouth, Maine, and are central to beloved 1950s-era Maine children's story "One Morning in Maine" by Robert McCloskey.