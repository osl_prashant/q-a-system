This year’s California table grape deal may get a slightly later start than last year’s, but growers say the quality of the fruit should be worth the wait.
Although the Fresno-based California Table Grape Commission had not issued its official estimate for the season as of mid-April, president Kathleen Nave said there was no reason to expect a smaller crop than last year’s 110 million box units, “and many reasons to expect a slightly larger crop.”
Scarlet royal was the most popular variety shipped in 2016, followed by autumn king, flame seedless, crimson seedless and sugraone. The seeded red globe variety was No. 6, she said.
Bakersfield, Calif.-based Anthony Vineyards expected to start its Coachella Valley shipments the week of May 8, five or six days later than last year, said co-owner Bob Bianco.
“We’ve had some cooler weather this year, so I anticipate the quality to be very nice,” he said.
Indications were that the Chilean crop would wind down by the end of April, Bianco said.
“It looks like it will be a good start to the (California) season.”
The company will transition to its vineyards in the San Joaquin Valley around the Fourth of July.
In mid-April, Bakersfield-based Giumarra Vineyards Corp. was running approximately four days behind last year, “which was a historically early season,” said Mimi Corsaro-Dorsey, vice president of marketing and director of export sales.
“This puts our start date at June 26 on the Early Sweet, which is our first green seedless out of the San Joaquin Valley,” she said.
That will be followed by flame seedless.
Unusual heat could accelerate the start date, she added.
“The crop yield looks very good at this time, possibly above average,” she said, adding that quality and size should be good, barring weather anomalies.
Delano, Calif.-based Columbine Vineyards also was running a bit later than last year because of cooler weather, “but that can change quickly,” said Keith Andrew, sales manager.
The company expected to start July 1 with flames and sugraones.
Columbine Vineyards will ship the Krissy variety for the first time this year, Andrew said.
It’s similar to the flame seedless variety and should be ready for harvest by late July or early August.
In Arvin, Calif., Wayde Kirschenman, owner and president of Kirschenman Enterprises Inc., expected to start shipping sugraones and flames the third or fourth week of June.
“They look good so far,” he said in mid-April.
In all, the company ships about 15 varieties, including a new variety called ivory seedless as well as the Great Green, timco and Krissy.
He expected volume at Kirschenman Enterprises to be similar to last year.
Bianco of Anthony Vineyards said growers in the Coachella Valley should have plenty of table grapes on hand for the Memorial Day and Fourth of July holidays.
“Those two holidays are important for the desert,” he said. “There will be adequate supplies to fill all the needs.”
He said he was pleased with the May 29 date for Memorial Day this year.
“This year it comes much later, so it’s going to fit in perfectly with everybody’s marketing plans,” he said.