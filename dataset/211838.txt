KANSAS CITY, Mo. — C&C Produce is revamping its office space after a microburst storm in March forced the company to move into a temporary office two blocks away. 
Company partner Nick Conforti said floor to ceiling renovation of more than 12,000 square feet of office space is expected to be finished by October. 
 
The company’s three owners are John Conforti, Nick Conforti and Joe Cali.
 
“With the business starting in 1992, this is our 25th anniversary so we are tying it to the (office renovation),” he said, adding the company expects to host an event opening the new offices sometime this fall.
 
The office space has been redesigned to shrink the size of the computer room and create private offices for company executives.
 
Having to go back and forth between the temporary office and the company headquarters hasn’t been easy, but Conforti said the wait should be worth it. 
 
With a 200,000-square-foot facility that features 34 dock doors, C&C services customers on the wholesale side of the business north to Minneapolis, south to Dallas, west to Denver and east to the middle of Illinois.
 
The company’s retail and foodservice customers are mostly in a 250-mile radius, Conforti said, from Lincoln to Omaha in Nebraska to Des Moines in Iowa  to St. Louis, Springfield,  Branson  and Joplin in Missouri and west to central Kansas. With a split of business about 60% retail and 40% foodservice, Conforti said C&C has 43 refrigerated box trucks and about 32 tractor trailers.
 
Local produce remains a strength for the company, Conforti said, growing in response to demand. Sales through early August were 15% above year-ago levels, he said. Sales could be higher if supply of local product was greater, he said.
 
“We still see that as an area that our customers want,” he said. For example, he said C&C moved ten local peaches grown in Missouri to every one California peach this summer.
 
“You are $10 per case higher, but the flavor and the quality is just unbelievable,” he said. “At the end of the day, you supply your customers with what your customers want, and the customers are wanting that fresh peach.”
 
With its trucks supplying customers all over the Midwest, Conforti said the company has the benefit of being able to back-haul locally grown produce from several states on its own trucks.
 
“We always have trucks coming up 71 highway from Springfield and Joplin, we have trucks at that Highway 50 corridor and we have (product) off of Highway 36 and produce from Nebraska,” he said.
 
“It works out fantastic to be able to back-haul our own produce.” Conforti said produce is picked up from Missouri and Nebraska growers and is in the hands of customers the next day.
 
Organic sales also are higher in 2017, he said, running about 30% above last year’s season to date through early August. The company stocks about 100 stock-keeping units of organic produce. “Juicers are making a big push in organics and I think there are more people that are just specializing in nothing but organics, and I think they are having a lot of success,” he said.
 
Future growth
With successful relationships already with Target, Supervalu and Associated Wholesale Grocers, Conforti said he anticipates most of the growth for the company will come from the foodservice channel in the coming years.
 
Cool Creations, a separate fresh-cut division under the same ownership as C&C Produce, continues to expand its offerings, recently adding vegetable noodles to items including sliced and diced tomatoes, onions, peppers and other commodities. Conforti said mechanization has increased volume of fresh-cut available, with machines able to automate cutting of cantaloupes, honeydew and pineapple. C&C is planning to acquire a machine that can peel mangoes, papayas and kiwifruit at a rate of close to 60 per minute. For mangoes, Conforti said the machine can peel the fruit and also extract the seed.