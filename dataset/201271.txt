Tennessee is the latest state to limit use of dicamba pesticide from Monsanto Co. This marks the fourth state to halt its use over claims of lost crops due to drift.
Arkansas banned its use last week  amid problems with drift. Kansas is currently reviewing complaints. Earlier today, Missouri lifted it’s ban but added new application requirements. These changes included applying during winds speeds at 10 mph lower, between 9 a.m. and 3 p.m., and additional recordkeeping requirements. See more.
Monsanto is working to overturn the bans, Reuters reports. "In almost every technology in that first year there are kinks that you need to work out," Robb Fraley, Monsanto's chief technology officer, said on a news media call with Reuters.
When questioned by Farm Journal and AgDay reporters, Monsanto’s Robert Fraley provided a video statement Thursday: 

Dicamba is key to Monsanto's biggest-ever biotech seed launch, which occurred last year.