California's water supplies have improved dramatically over the past year.
The state, which had endured drought conditions for several years, has received record rainfall in some areas this year, especially in the northern part of the state, which supplies much of the water used by farmers, residents and businesses in the south.
Rankin McDaniel, owner and president of McDaniel Fruit Co., Fallbrook, Calif., thinks California's drought is over - at least for the short term.
Avocado trees are reacting to the improved conditions, he said, but "they don't respond in six weeks."
"It takes time for a tree to recover from an extended period of water stress," he said.
That won't happen until warm weather comes along and the sap in trees begins to move.
Growers won't know how trees are responding until the beginning of spring, he said.
But he said increased water supplies "should tremendously help the California avocado trees."
"We should have a pretty healthy crop for the 2018 season," he said.
One of the biggest effects of the drought on avocados is on the sizing.
"Water is an essential element to the size of the fruit," McDaniel said.
Many growers will be holding fruit on the trees until they size up this season, he said, even if prices are strong.
Another concern growers still have is the possibility of rain once major harvesting starts, said Gary Caloroso, director of marketing for avocados and asparagus for Giumarra Agricom International LLC, Escondido, Calif.
That could keep pickers out of the groves for a time, delaying the harvest.
But mostly, growers are optimistic.
"We've had a lot of rain, and growers are really happy about that," Caloroso said.
"The trees are happier," too, he said.
Caloroso said the drought didn't have a major effect on quality of California avocados.
"California always has consistently good quality," he said.
"We had a really good first part of the rainy season," Giovanni Cavaletto, vice president of sourcing for Index Fresh Inc., Riverside, Calif., said in mid-February.
"It feels like the drought is breaking, at least in the Central Coast area," he said.
Nipomo, about 160 miles north of Los Angeles, where his brother grows avocados, had received 4 inches of rain two years ago and had recorded 21 inches as of mid-February.
"That's quite a turnaround," he said. "It's been a nice development for us."
Effects of the drought varied from area to area and can depend on growers' individual irrigation practices, Cavaletto said.
Lack of rain was particularly severe in San Luis Obispo County, which receives no imported water, he said.
Growers there are 100% dependent on wells.
He said the drought likely is at least partially responsible for this year's short avocado crop in California.
He was optimistic that groves will come out of the drought going into the spring, and the result will be a good set for next year's crop.
There was good news for this year's crop, as well.
Cosmetic quality of the fruit is "as good as it has ever been" despite the drought, Cavaletto said.
Index Fresh already was doing light picking and some exports.
"We're getting rave reviews from both our domestic and export customers," Cavaletto said.
Early-season fruit "has good legs on it," he said, so many growers were holding out for a better spring market and improved sizing.
The rains also help flush out salts that have accumulated in the soil, he said.
But while supplies have improved, water woes aren't over for avocado growers.
"Availability isn't so much the issue as the continually rising cost of the water," McDaniel said.
"It boils down to the price of water storage for California growers, whether it's a drought or not," he said.
"California agriculture needs some sort of relief on the issue of water."