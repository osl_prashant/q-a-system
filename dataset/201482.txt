Crop calls
Corn: Fractionally higher
Soybeans: 6 to 8 cents higher
Wheat: 1 to 3 cents higher
Corn futures saw two-sided trade overnight, but favored a firmer tone after USDA lowered the percent of crop rated "good" to "excellent" by one point from last week. Impressively, soybean futures were firmer overnight despite USDA raising the percent of top rated crop by one point. Also, there's little in the way of meaningful rains in the near-term forecast from Iowa into the eastern Corn Belt. Wheat was firmer overnight on spillover from corn and soybeans and a weaker tone in the dollar index.
Livestock calls
Cattle: Lower
Hogs: Steady to firmer
Cattle are vulnerable to followthrough from yesterday's losses and building supplies. However, pressure should be limited as futures near oversold territory. Traders clearly have a negative bias toward the cash cattle market this week, but August futures hold more than a $5 discount to the average of last week's cash cattle trade, which according to USDA was $117.30. Meanwhile, hog futures are expected to be firmer this morning on light, followthrough buying. While the cash hog market is expected to be steady to $1 lower this morning amid ample supplies, August hogs, which expire next week, hold around a $3 discount to the cash index.