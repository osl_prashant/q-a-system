BC Tree Fruits Cooperative, Kelowna, British Columbia, is updating the branding for its Ambrosia apples in the U.S. and select export markets.
“This is an exciting variety that continues to impress as it grows in popularity. With our volumes expected to increase by over 150% within the next 7-9 years, we wanted to give our growers’ Ambrosia apples their own identity,” marketing manager Chris Pollock said in a news release. 
The new logo features the name Ambrosia in bold black type with a yellow and red apple shape in place of the “o,” according to the release. The tagline “Sweet Perfection” describes the sweet flavor of the apple.
The cooperative’s Ambrosias will be available late October through spring. 
BC Tree Fruits plans to officially unveil the new branding Oct. 20-21 at The Oppenheimer Group’s booth, No. 1339, at the Produce Marketing Association’s Fresh Summit in New Orleans. 
Oppy is the largest marketer of British Columbia Ambrosias in the U.S., according to the release.