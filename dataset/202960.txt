More than 80% of the greens Canadians toss into their winter salads arrive on a truck from California.
With the buy local movement gaining momentum and tech-savvy greenhouse operators seeking new crops beyond tomatoes and cucumbers, bouquets of living lettuce in a plastic sleeve are becoming a familiar sight in supermarkets across the country.
"The Naked Leaf brand we introduced last year - hydroponic lettuce sold with the root on - is gaining incredible momentum among major North American retailers," said Joe Spano, vice president of sales and marketing for Kingsville, Ontario-based Mucci Farms.
"We can't believe how well it's being received by the consumers and we can't keep up," Spano said. "It's astonishing."
Mucci uses a floating pond system, with rows of butter lettuce plants growing in individual plastic cups filled with peat moss, all under purple LED lights.
"When we harvest we put the entire pot in a package," Spano said. "It gives a longer shelf life and better flavor, and the consumer relates better seeing it in 'soil.' You can put it in a cup of water and keep it alive for days."
Feedback on Mucci's lettuce trio, consisting of frisee, green and red leaf lettuce in one narrow shotglass-sized pot, has also been fantastic, Spano said.
Though Europeans prefer their living lettuce in a sleeve for a fresh look, he said many North Americans still prefer it protected in a clamshell or sealed sleeve. Mucci offers all three options.
Leamington, Ontario-based Pure Hothouse Foods Inc. is adding baby romaine to its Living Lettuce lineup this spring under the Pure Flavor label, said director of marketing Sarah Pau.
"It's got the crunch of its larger romaine cousin with the flavor of a Boston or butter lettuce, and it's getting a lot of hype at Loblaws and in the U.S," she said.
Pau said each head of Pure Flavor's living lettuce is grown in a cube of coco husks, which can be placed on a plate of water every five days to rehydrate.
"(Consumers) can leave it on the counter and peel off the leaves as needed," she said. "It's perfect for a single person who can't get through a head of lettuce in a week and hates to waste food."
Everything from the lettuce to the growing medium is 100% compostable, Pau said, and the packaging can be recycled.
Greenbelt Microgreens, located in the protected green space circling Toronto, is growing an organically certified living lettuce trio in soil in its newly refitted (non-hydroponic) Woodhill greenhouse near Hamilton, Ontario.
"With 80% of our greens being imported, retailers like Sobeys are desperate for something local and organic year-round," said vice president Michael Curry.
"It's a good business opportunity for us."
Curry said his products cost about $1 more than imports such as Earthbound and OrganicGirl, but Greenbelt's sales send money into the local economy and the lettuce has excellent shelf life.
The lettuce can be sold whole in their pots or the leaves are harvested, washed and sold as spring mix.
The leaves are also blended with some of the company's 25 organic microgreens to create salad mixes.
Greenbelt offers local retailers a stand for its sleeve-wrapped pots.
"We're popular among young families shopping for organic, and a lot of millennials like to have something living in their kitchen that's fun to harvest," Curry said.