Sam Clovis already serves in the Trump Administration as a senior White House advisor and has been described as President Donald Trump’s “eyes and ears” at USDA. Now, ProPublica, the Washington Postand others are reporting that Clovis may soon be tapped to serve as USDA’s chief scientist.

There’s just one catch: Clovis doesn’t really have much of a science-based background.
The Office of the Chief Scientist at USDA “strives to inform policy and programmatic decisions within the Department by providing the best available scientific advice and input,” according to USDA. It is instrumental in ensuring that any science conducted by USDA reached policymakers and decision leaders at a national and international level. This position also oversees NASS, ERS and other agency economic bureaus.
According to the position’s description, the chief scientist must be selected “from among distinguished scientists with specialized training or significant experience in agricultural research, education, and economics.”
But as ProPublica reports, Clovis has a doctorate in public administration, and he was a tenured professor of business and public policy for a decade at Morningside College. During that time, he rarely published any academic work. Clovis is also notably a vocal climate change skeptic.
Some groups, including the Union of Concerned Scientists, have criticized the possibility of Clovis assuming this role.
“If the president goes forward with this nomination, it’ll be yet another example of blatant dismissal of the value of scientific expertise among his administration appointees,” said director Ricardo Salvador in a statement. “Continuing to choose politics over science will give farmers and consumers little confidence that the administration has their interests at heart.”
Clovis may score points with farm country on some of his positions such as his public support of crop insurance, which he says is “an important part of any farm bill we would approach.”
Meantime, late last week, AgriTalk reported that three other major positions at USDA have also likely been decided (but as-yet not officially announced), including:

Steve Censky, South Dakota farmer and current CEO of the American Soybean Association, to be Deputy Undersecretary.
Bill Northey, Iowa’s agriculture commissioner, to be the Undersecretary for the new Farm Production and Conservation.
Ted McKinney, Indiana’s state agriculture director, to be the Undersecretary for Trade and Foreign Agricultural Affairs.