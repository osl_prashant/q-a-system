PHOTO: NatureSweet division manager Carla Conte displays the company's new like of Brighthouse Organics.
HILTON HEAD ISLAND, S.C. – NatureSweet has a new organic product line.
Brighthouse Organics made its debut Sept. 29 during the Southeast Produce Council’s Southern Innovations Organics and Foodservice Expo.
A wrapped long English cucumber and a three-pack of vine-ripened tomatoes are available in the new line, with more to come. NatureSweet recently began shipping the products.
The company created a distinct identity for the new line to reach a different demographic.
“This consumer for Brighthouse is younger and more urban,” division manager Carla Conte said. “More likely to be single and want to live a lifestyle that has a lot of synergy with other organic items … the kind of consumer that’s looking for a product that uses less water, is protected and safe in a greenhouse.”
Packaging for the tomatoes caters to that consumer. Along with being shaped like a greenhouse, it has icons that communicate the tomatoes were grown indoors, that they organic and non-GMO, and the packaging is recycled and recyclable.
NatureSweet also notes on its Brighthouse Organic packaging that tomatoes are best not refrigerated.
The packaging also presents a benefit for retailers: getting the correct ring at the register.
Overall, Conte said the reception to the line has been very positive.
“There is a lot more demand for organic products than there is supply, and being a national company with a lot of scale and also another successful product line like NatureSweet tomatoes, the products can ship together, and that’s one of the big benefits, too,” Conte said. “There’s a lot of synergies there.”