Crop calls
Corn: Fractionally higher
Soybeans: 3 to 5 cents higher
Winter wheat: Fractionally to 1 cent lower
Spring wheat: 1 to 2 cents higher
Soybean futures benefited from short-covering overnight, while corn spent the session pivoting around unchanged. Another round of rain is making its way across the Corn Belt this morning, which is slowing harvest progress. A weaker tone in the U.S. dollar index added to price support, especially in spring wheat futures, while winter wheat futures were weaker on improved soil moisture across the Central and Southern Plains. Price action is likely to be lackluster as traders wait on this afternoon's crop progress and condition ratings as well as move to the sidelines for Thursday's USDA reports. Traders expect USDA to raise the size of the corn and soybean crops. On a positive demand note, USDA announced China has purchased 131,000 MT of soybeans for 2017-18.
 
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Cattle futures are expected to see a mixed start as traders wait on more cash cattle signals. While packers are enjoying profitable margins -- which bodes well for bulls -- this week's showlist is higher. Beef prices got off to a stronger start for the week, but movement was lackluster. Meanwhile, the cash hog market is expected to be firmer this morning amid strong packer demand. Pork cutout values were 90 cents firmer yesterday, but movement was disappointing. Nearby live cattle and lean hog futures are trading at a premium to the their respective cash indices, which reflects traders' higher cash bias.