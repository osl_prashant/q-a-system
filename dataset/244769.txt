Yemeni immigrants focus on future in US amid war back home
Yemeni immigrants focus on future in US amid war back home

By JEFF KAROUBAssociated Press
The Associated Press

DEARBORN, Mich.




DEARBORN, Mich. (AP) — Ibrahim Alhasbani is like generations of Middle Eastern immigrants in the Detroit suburb of Dearborn: He fled war, came with dreams and worked for others until he could strike out on his own.
Now, like an increasing number of people from Yemen who have come the U.S., he sees a long-term future outside the country he left and seeks to bring aspects of his native country into America.
"Here you build; over there you have memories," said Alhasbani, owner of Qahwah House, a cafe that serves coffee made from beans harvested on his family's farm in Yemen's mountains. "I live here, so this is the main thing. This is what's going to help first build my career, build my business ... and help the people over there."
Yemeni have been coming to the U.S. for more than a century — especially since the 1960s — but in recent years they have been planting stronger roots, raising their profile and looking outward — opening upscale restaurants and cafes and running for political office. And, in cases like Alhasbani, they are making Yemeni culture a key part of the business proposition.
It's a path that's not unusual for first- and second-generation immigrants in the U.S. For Yemeni, the shift is also a reaction to chaos in their homeland, where a devastating war has killed more than 10,000 people and displaced 2 million.
"People are coming here and bringing their resources here," said Sally Howell, an author and associate professor of Arab American Studies at University of Michigan-Dearborn. "In the past, they weren't really committed to here. Now the situation has been so bad in Yemen for so long, they're doing what other refugees and exiles do: They're acknowledging their future is here."
The highest U.S. population of Yemeni is in the Detroit area, where Syrian and Lebanese immigrants had already settled and became more prominent in business. Unlike their Arab neighbors, many Yemeni men came alone and didn't have relatives follow them, so they were more likely to go back and forth between the U.S. and their homeland.
"We're not going back to Yemen like we did before," said Rasheed Alnozili, publisher of The Yemeni American News. "We learn from Lebanese. They built here then they built there. We made a mistake: We built there, now we built here. ... We learned late, but we're still in process."
The New York City, San Francisco, Chicago and Buffalo, New York, areas also have Yemeni communities. About 43,000 people of Yemeni ancestry are in the U.S. according to a 2015 census survey. However, advocates say the number is much higher because of historical undercounting, and has significantly increased since that last survey because of deteriorating conditions in Yemen, which fell into chaos following its 2011 Arab Spring uprising that removed longtime autocrat Ali Abdullah Saleh, now allied with Shiite rebels who occupy much of the country. A Saudi-led coalition has been battling the rebels and Saleh's forces since March 2015.
Dearborn and the Detroit enclave of Hamtramck have several sit-down restaurants specializing in Yemeni cuisine. In Hamtramck, where a Yemeni serves on the majority-Muslim City Council, there are also shops devoted to Yemeni sweets and sub sandwiches.
"The real difference today is that Yemeni are representing Yemen to the public, not just having ethnic Yemeni restaurants in tightly knit Yemeni enclaves," Howell said.
At Qahwah House, the goal is not only to serve Yemeni-style coffee, with its alluring tastes of cardamom and ginger, but also to impart information about Yemen's history and culture. The country is, as the National Coffee Association notes, where coffee cultivation and trade began, though Ethiopia boasts being the birthplace of the discovery of coffee.
"That's why I want to ... be like a messenger for my country— give the right picture about the people over there, and the coffee," said Alhasbani, whose brother was blinded after a bomb exploded near his family's home several years ago.
In Hamtramck, Abraham Aiyash, a first-generation Yemeni who is running as a Democrat for state Senate, is inspired by those who have struggled and recalls an uncle who died last year in Yemen after an airstrike.
"I'll always remember that in the work that I do. ... I'll be reminded that people before me kind of had to toil and sweat to make this possible."
___
Jeff Karoub is a member of AP's Race and Ethnicity Team. Follow him on Twitter at https://twitter.com/jeffkaroub and find more of his work at https://apnews.com/search/jeff%20karoub .