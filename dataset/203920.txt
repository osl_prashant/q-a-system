Philadelphia-based AgroFresh Solutions Inc. is adding two new service centers and hiring 50 seasonal workers to help apple and pear growers this season. 
 
Marketing Harvista pre-harvest 1-Methylcyclopropene (1-MCP) technology and SmartFresh post-harvest 1-MCP technology, AgroFresh is adding new service centers in Sparta, Mich., and Bologna, Italy, according to a news release.  Harvista, used while fruit is on the treee, prevents ethylene recognition to slow the natural ripening process and allows growers to pick at optimal quality. SmartFresh helps manage fruit ripening by controlling ethylene in storage, according to the company.
 
Those two centers will add to existing facilities in Yakima, Wash., Curico, Chile, and Lleida, Spain, according to the release. The Wenatchee, Wash. Technical Center, also supports service activities.
 
"Our service centers are logistical focal points for coordinating product supplies and application services," Edgardo Castaneda, vice president of operations for AgroFresh, said in the release. "These two new locations will help us manage customer needs more effectively throughout the upcoming season."
 
The added in-field support will allow AgroFresh more capabilities to meet critical application timing and to expand acreage reach for its Harvista and SmartFresh solutions, according to the release.