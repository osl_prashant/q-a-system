Cincinnati-based Kroger Co. launched a new “Restock Kroger Plan” earlier this month, and is “restocking” its executive leadership around it.
Fred Morganthall, executive vice president of retail operations, is retiring Nov. 30, succeeded by Mike Donnelly, current executive vice president of merchandising. Donnelly will oversee merchandising and retail operations as the new executive vice president and chief operating officer, effective Nov. 1, according to a news release.
Donnelly, Mike Schlotman, executive vice president and chief financial officer, and Chris Hjelm, executive vice president and chief information officer, are the executives driving execution of Restock.
“Kroger is fortunate to have a leadership team that combines deep experience with creative new talent as we strategically resposition the company through our Restock Kroger Plan,” said chairmand and CEO Rodney McMullen, in a news release.
Restock, introduced at an investor conference earlier this month, is a “plan to redefine the way America eats.”
It has four primary directives:

Redefine the food and grocery customer experience;
Expand partnerships to create customer value;
Develop talent; and
Live Kroger’s purpose.

A detailed outline of the core values is available from Kroger’s website.
Kroger also announced more executive changes, including the retirement of Katie Wolfram, central division president.
Pam Matthews, current QFC (Quality Food Centers) division president, succeeds Wolfram, who spent 40 years with the company.
Suzy Monford, former CEO of Berkeley, Calif.-based Andronico’s Community Markets, is the new QFC division president. Monford, who previously was with Woolworths in Australia, and H.E. Butt Co. in Texas, was with Andronico’s for just over a year before it was purchased by Safeway Albertsons earlier this year.