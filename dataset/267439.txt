Beef products are also caught in the crosshairs of China’s list of potential tariffs after the U.S. was finally allowed to export beef to the country late last year after a 13-year drought after BSE was discovered.

According to Greg Henderson, editor of Drovers magazine, retailers and other meat buyers are on edge after prices took a dive into the red on Wednesday before rebounding later in the day.

“Trade benefits people,” said Derrell Peel, Oklahoma State University Cooperative Extension livestock marketing specialist. “Gains from trade is the basis for most economic process. We’ll figure it out eventually and go back.”

U.S. Meat Export Federation (USMEF), president and CEO Dan Halstrom issued the following statement: “Over the past nine months, interest in U.S. beef has steadily gained momentum in China…but if an additional tariff is imposed on U.S. beef, these constructive business relationships and opportunities for further growth will be at risk.”

According to Ian Sheldon, trade policy expert from Ohio State University, it takes years to regain trust, the bovine spongiform encephalopathy as an example.

In preparation of the possible Chinese tariffs on agricultural products, economist Matt Roberts of the Kernmantle Group says now is a great time to lock in feed prices.

Earlier this week, China fired back at the U.S. with a new round of proposed tariffs, totaling $50 billion, aimed at 106 products.