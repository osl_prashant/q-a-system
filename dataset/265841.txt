Green Giant Fresh has a new line of meal bowls with six distinct options.
The microwaveable meals allow on-the-go consumers who are pressed for time to have a fresh, healthful meal, according to the company, Salinas, Calif.
The bowls come in these options:

Burrito Bowl,
Fried Rice Bowl,
Pad Thai Bowl,
Rancheros Bowl,
Buddha Bowl, and
Ramen Bowl.

Green Giant Fresh, which showcased the meal bowls at the Southeast Produce Council’s Southern Exposure show in early March in Tampa, Fla., already has plans to expand the line, according to the news release.
The meals come with a sauce/seasoning packet and other ingredients, and proteins can be added to customize the bowls.
The bowls have different fresh-cut vegetable blends, including some from the company’s Cauliflower Crumbles line, and others from the Vegetable Noodles line.
“We know consumers are on the go and looking for healthy and nutritious foods, swaps and meal alternatives,” Jamie Strachan, CEO of Green Giant Fresh, said in the release. “Our meal bowls line was developed in response to this demand, then refined and perfected with our premium vegetable products, along with other desirable flavors that give each bowl its distinctive personality.”