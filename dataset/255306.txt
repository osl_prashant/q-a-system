Imported guard dogs deployed as part of US wolf-sheep study
Imported guard dogs deployed as part of US wolf-sheep study

By KEITH RIDLERAssociated Press
The Associated Press

BOISE, Idaho




BOISE, Idaho (AP) — Federal scientists say a four-year study involving nearly 120 guard dogs imported from Europe and Asia found the animals do well protecting sheep from wolves and better than traditional guard dogs deterring coyotes.
The U.S. Department of Agriculture supplied Cao de Gado Transmontanos (COHN day GAH'-doh TRANS'-mahn-tan-ohs), Karakachans (kah-RACK'-a-chans) and Kangals (KAN'-gahls) to guard sheep in Idaho, Montana, Wyoming, Washington and Oregon.
Scientists say they're still analyzing information from fieldwork that includes remote cameras and GPS collars from the dogs that can weigh up to 140 pounds (64 kilograms).
The dogs were gathered as puppies in Portugal, Bulgaria and Turkey and sent to the American West, where they spent four years guarding sheep.
Environmentalists say guard dogs can mean fewer wolves killed for livestock depredation.