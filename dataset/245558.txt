(Bloomberg) -- The man who runs China’s biggest consumer of American soybeans said he hopes his country will avoid a clash with the U.S. over trade, though his firm can line up alternative suppliers if necessary.
“We hope that China and the U.S. will not have a trade war, which is also in line with the interests of business and people in both countries,” Liu Yonghao, founder of New Hope Group, one of China’s largest pig feed companies, told reporters Saturday ahead of National People’s Congress meetings in Beijing. “Any trade war will have an impact on our business.”
U.S. President Donald Trump has announced he would slap tariffs on steel and aluminum imports, daring other countries to retaliate and tweeting that trade wars were “easy to win.” The American Soybean Association has said the metal tariffs could significantly endanger the U.S.-China soybean trade relationship. 
"In case of a trade war, we are able to find other solutions," said Liu, who is also chairman of New Hope Liuhe, a publicly traded animal feed producer controlled by New Hope Group. “We have to raise pigs, and citizens have to eat pork.”
Liu said he hopes the recent visit to the U.S. by Liu He, President Xi Jinping’s top economic aide, will reduce the trade frictions.
To read about Liu He’s trip to Washington to see business leaders, click here.
Pig feed operations are the largest users of soy meal, a major product of soy crushing, and China is America’s biggest soy buyer.
 
Copyright 2018, Bloomberg News