Anhydrous is $80.61 below year-ago pricing -- higher 63 cents/st this week at $523.49.
Urea is $15.83 below the same time last year -- lower 79 cents/st this week to $359.83.
UAN28% is $31.58 below year-ago -- higher 8 cents/st this week to $248.23.
UAN32% is priced $27.11 below last year -- lower $1.50/st this week at $277.60.

UAN32% led the nitrogen segment lower with Michigan falling $14.00 per short ton and Kansas softening $4.83. Five states were unchanged as Indiana firmed $3.99 and Iowa added $2.71.
Urea fell under the weight of a $10.60 decline in Kansas and North Dakota,which fell $3.38. Two states were unchanged as Iowa firmed $4.46, Missouri added $3.74 and Michigan gained $2.22.
Anhydrous ammonia was our upside leader in the nitrogen segment -- up a measly 63 cents per short ton on the week. Missouri led NH3 higher firming $18.98 as Michigan added $9.14. Three states were unchanged as Kansas offset gains softening $11.97 and North Dakota fell $8.87.
UAN28% was mildly higher as well led by Iowa which firmed $2.71 and Indiana which added 91 cents per short ton. Four states were unchanged as Michigan fell $2.24 and North Dakota softened 74 cents.
It was another week of very limited price action in the nitrogen segment. Higher NH3 and lower urea bids narrowed the spread between the two, but only slightly. Compared to urea, anhydrous ammonia continues resist running sharply higher, supporting last week's assertion that anhydrous will top well below our previous target of $550 per short ton.
That bodes well for UAN solutions although upside risk in UAN will not be fully expressed until post-emerge applications begin. That will be another month in the Midwest, but the early indications from urea and NH3 are that UAN will remain confined within the range established by the NH3/urea spread. That would imply sideways near-term price action for UAN28 and 32% with a mild upside bias based on demand-based price strength.
Overall, nitrogen has been on a sideways trajectory, even as demand for spring applications heats up. One thing to watch for is a pop in UAN solutions. If corn acreage was as undecided as the trade believed, there may be significant hand-to-mouth demand for UAN once seed is actually in the ground. Check with your preferred supplier if you have not already booked spring/summer UAN to ensure your nitrogen needs will be met when the time comes to apply.
December 2017 corn closed at $3.82 on Thursday, April 21. That places expected new-crop revenue (eNCR) per acre based on Dec '17 futures at $604.75 with the eNCR17/NH3 spread at -81.26 with anhydrous ammonia priced at a discount to expected new-crop revenue. The spread narrowed 20.95 points on the week.





Nitrogen pricing by pound of N 4/26/17


Anhydrous $N/lb


Urea $N/lb


UAN28 $N/lb


UAN32 $N/lb



Midwest Average

$0.32 1/4


$0.40


$0.44 1/2


$0.43 1/2



Year-ago

$0.37 1/4


$0.41 1/2


$0.50


$0.47 1/2





The Margins -- UAN32% is at a 1 1/4 cent premium to NH3. Urea is 2 3/4 cents above anhydrous ammonia; UAN28% solution is priced 1/4 cent above NH3. The margins continue to narrow.





Nitrogen


Expected Margin


Current Price by the Pound of N


Actual Margin This Week


Outstanding Spread




Anhydrous Ammonia (NH3)


0


32 1/4 cents


0


0




Urea


NH3 +5 cents


40 cents


+7 3/4 cents


+2 3/4 cents




UAN28%


NH3 +12 cents


44 1/2 cents


+12 1/4 cents


+1/4 cent




UAN32%


NH3 +10 cents


43 1/2 cents


+11 1/4 cents


+1 1/4 cents