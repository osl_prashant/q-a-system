Chicago Mercantile Exchange live cattle contracts on Tuesday closed in positive territory for the first time in 12 sessions, helped by short-covering and futures' discounts to early-week cash prices, traders said.August live cattle, which will expire on Wednesday, closed up 0.750 cent per pound to 111.500 cents. Most actively traded October ended 2.400 cents higher at 107.475 cents.
So far, market-ready, or cash, cattle in the U.S. Plains moved at $110 to $111 per cwt, said feedlot sources. That was down from mostly $115 last week with other sellers pricing cattle at $115 to $117.
Some investors may have come to the realization that fourth-quarter cattle supplies may not grow as much as last Friday's government monthly cattle report had suggested, said Doane Advisory Services economist Dan Vaught.
Recent futures declines encouraged producers to send cattle to market earlier than they had anticipated, which could result in reduced slaughter numbers after the Labor Day holiday, he said.
However, some traders remain skeptical given still-plentiful current cattle numbers that continue to weigh on wholesale beef prices ahead of upcoming holiday cook outs.
The afternoon's choice beef price sank $1.80 per cwt from Monday to $196.62. Select cuts fell $1.17 to $191.17, according to the U.S. Department of Agriculture.
Short-covering, lower corn prices and late-day CME live cattle strength boosted the exchange's feeder cattle contracts. September ended 3.375 cents per pound higher at 142.525 cents.
Higher hog futures closure
CME lean hogs drew strength from short-covering before the final trading day for the month on Wednesday and futures' discounts to the exchange's hog index for Aug. 26 at 66.61 cents, traders said.
They added that hog contracts were further supported by technical buying in the rally in neighboring cattle markets.
October ended up 0.675 cent per pound to 62.050 cents. December finished 0.775 cent higher at 57.100 cents, and above the 10-day moving average of 56.48 cents.
Investors keep close watch as cash prices continued their downward spiral due to abundant supplies that are expected to increase even more this fall as temperatures moderate, creating ideal conditions for raising hogs.
The government reported Tuesday morning's average cash hog price in Iowa/Minnesota dropped 62 cents per cwt from Monday to $61.65.