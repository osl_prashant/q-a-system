As West Pak Avocado Inc. founders prepare to take on new roles with an emphasis on industry relationships and expanding global markets, the company has released a video highlighting its past.
The video, “Growing a Ripe Legacy,” defines West Pak’s “dedication to innovation, growers and global expansion,” according to a news release.
Galen Newhouse and Randy Shoup, who founded the company 35 years ago, will be less involved in daily management as that role goes to CEO Mario Pacheco, who joined West Pak in 2016 after more than two decades at Chiquita Brands International.
Newshouse and Shoup will be involved in business development and continue to serve on the board.
Their focus will be in these areas, according to the release:

Growing industry relationships;
New ventures in global markets;
Industry innovation; and
Continued branding for the avocado.

The goals are outlined in the video.
“Developing a video that defines West Pak and our Avocado Family was a year in the making,” Newhouse said in the release. “With efforts spanning from our headquarters in Murrieta, Calif., to Michoacan, Mexico, the production involved creating never-before-seen footage inside our facilities and with members of our team.
“Our goal was to show the depth of our operations, the heart of our team and the lengths West Pak takes as a company to provide our customers with the finest fruit globally,” he said in the release.
The video is on the company’s website and YouTube.