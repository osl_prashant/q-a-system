McDonald’s has added to its Signature Crafted Recipe line, and the new sandwich includes kale and baby spinach.
On menus for a limited time, the Signature Sriracha is available nationwide. As with other items in the product line, customers can choose the bun and protein for the sandwich, which comes with Sriracha Mac sauce, onions, a tomato slice and white cheddar cheese, along with kale and baby spinach.
Those leafy greens have been part of the McDonald’s repertoire since 2015, when the company added them to its salad blend.
“We continue to evolve to meet our customers’ tastes and preferences, and kale and baby spinach are a great example of how we’re raising the bar with new and fresh ingredients,” McDonald’s spokeswoman Tiffany Briggs said.
The majority of the kale and baby spinach comes from Salinas, Calif., and Yuma, Ariz., the same as is the case for the lettuce in the other Signature Crafted Recipe sandwiches.
The price range for the Signature Sriracha is $4.99-$5.19.