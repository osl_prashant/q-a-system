Market reaction
Ahead of USDA's reports, corn futures were trading mostly 2 cents higher, soybeans were 3 to 5 cents higher, winter wheat futures were 2 to 3 cents higher, spring wheat futures were around 9 cents higher and cotton futures were 20 to 70 points lower.
In post-report trade, corn is fractionally mixed, soybeans are 1 to 2 cents lower, winter wheat is down 3 to 4 cents and spring wheat is 3 to 4 cents higher. Cotton futures are 40 to 90 points lower.
Pro Farmer Editor Brian Grete highlights the report:



 
Winter wheat production

All wheat: 1.824 billion bu.; trade expected 1.815 billion bu.
-- compares to 1.820 billion bu. in May; 2.310 billion bu. in 2016
All winter wheat: 1.250 billion bu.; trade expected 1.239 billion bu.
-- compares to 1.246 billion bu. in May; 1.672 billion bu. in 2016
HRW: 743 million bu.; trade expected 731 million bu.
-- compares to 737.5 million bu. in May; 1.082 billion bu. in 2016
SRW: 298 million bu.; trade expected 295 million bu.
-- compares to 296.7 million bu. in May; 345 million bu. in 2016
White winter: 209 million bu.; trade expected 214 million bu.
-- compares to 212.2 million bu. in May; 286 million bu. in 2016
USDA increased the national average winter wheat yield by 0.1 bu. per acre from May, pushing it to 48.9 bu. per acre. USDA now puts the average winter wheat yield in Kansas at 44 bu. per acre, up 2 bu. from last month. Yields were unchanged from May in Oklahoma and Texas. USDA made no change to estimated harvested acres.
The national average all wheat average yield is now estimated at 47.3, up 0.1 bu. from last month.
Spring wheat production is implied at 574 million bu., unchanged from last month. We'll get the first survey-based spring wheat production estimate in July.
U.S. carryover

Corn: 2.295 billion bu. for 2016-17; unch. from 2.295 billion bu. in May
-- 2.110 billion bu. for 2017-18; unch. from 2.110 billion bu. in May
Beans: 450 million bu. for 2016-17; up from 435 million bu. in May
-- 495 million bu. for 2017-18; up from 480 million bu. in May
Wheat: 1.161 billion bu. for 2016-17; up from 1.159 billion bu. in May
-- 924 million bu. for 2017-18; up from 914 million bu. in May
Cotton: 3.2 million bales for 2016-17; unch. from 3.2 million bales in May
-- 5.5 million bales for 2017-18; up from 5.0 million bales in May
Old-crop corn carryover is unchanged from last month, but is 7 million bu. above the average pre-report trade estimate. USDA made no changes to the supply- or demand-side of the 2016-17 balance sheet. The national average on-farm cash corn price is now estimated at $3.25 (unchanged) to $3.45 (down a dime from last month).
New-crop corn carryover is unchanged from last month, but is 25 million bu. above the average pre-report trade estimate. USDA made no changes to the supply- or demand-side of the new-crop balance sheet. The price projection is also unchanged at $3.00 to $3.80.
Old-crop soybean carryover is up 15 million bu. from last month and is 17 million bu. above the average pre-report trade estimate. The only change USDA made on the 2016-17 balance sheet for soybeans was a 15-million-bu. cut to estimated crush (to 1.91 billion bu.). Exports were unchanged at 2.05 billion bushels. USDA puts the national on-farm cash bean price at $9.55, unchanged from last month.
The only change USDA made to the 2017-18 balance sheet for soybeans was a 15-million-bu. increase in beginning stocks (old-crop carryover). The estimate at 495 million bu. is 10 million bu. above the average pre-report trade estimate. USDA left the national average on-farm cash bean price for 2017-18 unchanged at $8.30 to $10.30.
Old-crop wheat carryover was up 2 million bu. from last month and is in-line with trade expectations. The only change USDA made on old-crop wheat was a 2-million-bu. increase in estimated imports. The national average on-farm cash wheat price for 2016-17 was unchanged from last month at $3.90.
New-crop wheat carryover is up 10 million bu. from last month and is about 13 million bu. above the average pre-report trade estimate. Total new-crop supply is up 10 million bu. from May with slight increases in beginning stocks, production and imports. The national average on-farm cash price for 2017-18 is $3.90 to $4.70, up a nickel on both ends of the range from last month.
Old-crop cotton carryover is unchanged from last month. USDA made no changes to the supply- or demand side of the balance sheet. The national average on-farm cash price for 2016-17 is estimated at 68.5 cents, down a half-cent from last month.
On new-crop, USDA made no change on the supply-side of the balance sheet, but cut estimated exports by 500,000 bales, to 13.5 million. The result was a 500,000-bale increase in estimated 2017-18 cotton carryover. The national average on-farm cash cotton price for 2017-18 was unchanged from May at 54 to 74 cents.

Global carryover
Corn: 224.59 for 2016-17; up from 223.90 MMT in May
-- 194.33 MMT for 2017-18; down from 195.27 MMT in May
Beans: 93.21 MMT for 2016-17; up from 90.14 MMT in May
-- 92.22 MMT for 2017-18; up from 88.81 MMT in May
Wheat: 256.43 MMT for 2016-17; up from 255.35 MMT in May
-- 261.19 MMT for 2017-18; up from 258.29 MMT in May
Cotton: 89.34 million bales for 2016-17; down from 89.52 million bales in May
-- 87.71 million bales for 2017-18; up from 87.14 million bales in May
 
 
Global production highlights
Argentina beans: 57.8 MMT for 2016-17; compares to 57.0 MMT in May
-- 57.0 MMT for 2017-18; compares to 57.0 MMT in May
Brazil beans: 114.0 MMT for 2016-17; compares to 111.6 MMT in May
-- 107.0 MMT for 2017-18; compares to 107.0 MMT in May
Argentina wheat: 17.0 MMT for 2016-17; compares to 16.0 MMT in May
-- 17.5 MMT for 2017-18; compares to 17.0 MMT in May
Australia wheat: 35.0 MMT for 2016-17; compares to 35.0 MMT in May
-- 25.0 MMT for 2017-18; compares to 25.0 MMT in May
China wheat: 128.85 MMT for 2016-17; compares to 128.85 MMT in May
-- 131.0 MMT for 2017-18; compares to 131.0 MMT in May
Canada wheat: 31.7 MMT for 2016-17; compares to 31.7 MMT in May
-- 28.35 MMT for 2017-18; compares to 28.35 MMT in May
EU wheat: 145.47 MMT for 2016-17; compares to 145.47 MMT in May
-- 150.75 MMT for 2017-18; compares to 151.0 MMT in May
FSU-12 wheat: 130.24 MMT for 2016-17; compares to 130.24 MMT in May
-- 123.01 MMT for 2017-18; compares to 121.01 MMT in May
Russia wheat: 72.53 MMT for 2016-17; compares to 72.53 MMT in May
-- 69.0 MMT for 2017-18; compares to 67.0 MMT in May
China corn: 219.55 MMT for 2016-17; compares to 219.55 MMT in May
-- 215.0 MMT for 2017-18; compares to 215.0 MMT in May
Argentina corn: 40.0 MMT for 2016-17; compares to 40.0 MMT in May
-- 40.0 MMT for 2017-18; compares to 40.0 MMT in May
South Africa corn: 16.4 MMT for 2016-17; compares to 15.3 MMT in May
-- 12.5 MMT for 2017-18; compares to 12.5 MMT in May
Brazil corn: 97.0 MMT for 2016-17; compares to 96.0 MMT in May
-- 95.0 MMT for 2017-18; compares to 95.0 MMT in May
China cotton: 22.75 mil. bales for 2016-17; compares to 22.75 mil. bales in May
-- 24.0 MMT for 2017-18; compares to 23.50 MMT in May