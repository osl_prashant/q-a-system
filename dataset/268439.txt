Michigan on track for normal production
John Bakker, executive director of the Dewitt-based Michigan Asparagus Advisory Board, said the state should have roughly 21-22 million pounds of asparagus this year, which is a normal amount.
“We anticipate that about 40% of our crop will go to the processing market, either canned or frozen, about 60% fresh,” Bakker said. “The processing market wants some, same as last year. If we can get them 8 million pounds I believe that’s probably what they want.
“It’s a little too early to tell on demand,” Bakker said in late March. “Prices this winter have been exceptionally low, but they’re coming up of course for the Easter holidays here, so hopefully prices will come up a little bit on the grower level before we start harvesting.”
The board will focus its efforts on helping move the peak of production through the supply chain in an orderly fashion.
 
Peruvian importers meeting coming up
The Peruvian Asparagus Importers Association will meet May 17 in Miami to discuss the 2018-19 season.
“One of our industry and association focus points has always been on innovation and streamlining the logistics chain,” said Priscilla Lleras-Bush, coordinator of the association. “Our importers have razor focus on maintaining the cold chain logistics as well as providing excellent quality and value for their customers.”
Companies will discuss how best to maintain that quality and value at the upcoming meeting, she said. 
 
Todd Greiner Farms adds to packinghouse
Hart, Mich.-based Todd Greiner Farms Packing added 20,000 square feet to one of its packinghouses within the past year.
The space includes two large cold storage rooms, more space on the packinghouse floor, two more shipping and receiving docks, a new break room, bathrooms and office space, said salesman Aaron Fletcher.
“This major expansion to the packinghouse will allow us to operate more efficiently and increase the volume we are able to pack each year,” Fletcher said.
The company plans to pack 20% to 25% more fresh product this year compared to last year, he said. Todd Greiner Farms expects promotable volumes from mid-May to mid-June.
 
World Variety sees growing demand
Los Angeles-based World Variety Produce, which markets under the Melissa’s brand, has seen increasing demand for asparagus.
Sales of green asparagus were up 7% in 2017, and sales of white asparagus were up 10%, said director of public relations Robert Schueller.
The company expects volumes to be up for green and white this year. After Easter, some of the big promotional times for the vegetable are Memorial Day, Fourth of July, Labor Day, Thanksgiving and Christmas, Schueller said.