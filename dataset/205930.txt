USDA Weekly Grain Export Inspections
			Week Ended September 7, 2017




Corn



Actual (MT)
662,173


Expectations (MT)

700,000-900,000



Comments:

Inspections are down 154,886 MT from the previous week and the tally was just shy of traders' expectations. Inspections are running 59.5% behind year-ago to kick off the 2017-18 marketing year. USDA's 2017-18 export forecast of 1.850 billion bu. is down 16.9% from the previous marketing year.




Wheat



Actual (MT)
446,957


Expectations (MT)
300,000-500,000 


Comments:

Inspections are up 156,229 MT from the previous week and near the upper end of expectations. Inspections are running 2.4% ahead of year-ago versus 6.0% ahead last week. USDA's export forecast for 2017-18 is at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual (MT)
1,106,268


Expectations (MMT)
450,000-650,000 


Comments:
Export inspections are up 395,346 MT from the previous week, and well above expectations. Inspections for 2017-18 are running 4.0% behind year-ago at the start of the marketing year. USDA's 2017-18 export forecast is at 2.225 billion bu., up 3.5% from year-ago.