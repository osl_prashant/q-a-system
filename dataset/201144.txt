Crop calls 
Corn: Narrowly mixed
Soybeans: Narrowly mixed
Winter wheat: Narrowly mixed
Spring wheat: 5 to 9 cents higher

Spring wheat futures were the upside leader (again) overnight, which helped to limit selling in winter wheat, corn and soybean futures. While rains are moving across areas of the central Corn Belt this morning, dry conditions continued across the Northern Plains and it's too late for a "saving" rain for some areas. Meanwhile, traders are beginning to more actively even positions ahead of Friday's key acreage and grain stocks data from USDA, as these reports have produced sharp price responses in the past.
 
Livestock calls
Cattle: Lower
Hogs: Mixed
Cattle futures are called lower on followthrough from yesterday's losses as well as sharp weakness in the beef market. Choice beef values tumbled $4.66 and Select declined $2.40 yesterday. While packers' margins remain well in the black, the sharp drop in beef prices lowers traders' confidence the cash cattle market will stabilize this week. Meanwhile, hog futures are called mixed amid spreading, with traders beginning to even positions ahead of tomorrow afternoon's Hogs & Pigs Report. Yesterday's $1.77 gain in pork cutout values should limit pressure on futures, although traders are disappointed by this week's cash hog market.