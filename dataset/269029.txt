BC-USDA-Calif Eggs
BC-USDA-Calif Eggs

The Associated Press



HC—PY001Des Moines, IA          Fri. Apr 13, 2018          USDA Market NewsDaily California EggsAPRIL 13, 2018Benchmark prices are unchanged. Asking prices for next week are 71 centslower for Jumbo, 75 cents lower for Extra Large, 79 cents lower for Largeand 37 cents lower for Medium and Small. The undertone is steady to aboutsteady. Demand is moderate. Offerings are moderate to heavy. Supplies arelight to moderate. Market activity is moderate. Small benchmark price$1.64.CALIFORNIA:Shell egg marketer's benchmark price for negotiated egg sales ofUSDA Grade AA and Grade AA in cartons, cents per dozen.  Thisprice does not reflect discounts or other contract terms.RANGEJUMBO                283EXTRA LARGE          277LARGE                275MEDIUM               184SOUTHERN CALIFORNIA:PRICES TO RETAILERS, SALES TO VOLUME BUYERS, USDA GRADE AA AND GRADE AA,WHITE EGGS IN CARTONS, DELIVERED STORE DOOR,CENTS PER DOZEN.RANGEJUMBO                270-282EXTRA LARGE          265-272LARGE                263-270MEDIUM               172-179Source:   USDA Livestock, Poultry, and Grain Market NewsDes Moines, IA    515-284-4460  email: DESM.LPGMN@ams.usda.govhttp://www.ams.usda.gov/mnreports/HC—PY001.txtPrepared: 13-Apr-18 10:57 AM E MP