BC-USDA-SD Direct Fdr Catl
BC-USDA-SD Direct Fdr Catl

The Associated Press



SF—LS160Sioux Falls, SD    Fri Mar 03, 2018    USDA-SD Dept of Ag Market NewsSouth and North Dakota Direct Feeder CattleFeeder Cattle Weighted Average Report for 03/03/2018 Receipts: 77           Last Week 293          Last Year 1,281Compared to last week:  Feeder steers not tested this week. Feederheifers not tested last.  Supply includes 100 percent over 600 lbsand 100 percent heifers.  All sales FOB North and South Dakotawith a 2-3 percent shrink or equivalent and a 4-8 cent slide onyearlings and an 8-12 cent slide on calves from base weights.Feeder Steers: No Test.Feeder Heifers Medium and Large 1Head   Wt Range   Avg Wt    Price Range   Avg Price  Comments77     700        700       141.97         141.97 Current FOBSource:  USDA Market News Oklahoma City, OKJoe Massey 405-232-542524 Hour Market Report 1-405-636-2691www.ams.usda.gov/mnreports/sf—ls160.txt