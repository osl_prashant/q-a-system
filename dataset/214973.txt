By: Jerry Volesky and Dave Boxler, University of Nebraska Extension
There have been several inquiries from landowners in central Nebraska concerning damage to pasture and hay meadows from grubs. Affected areas have included subirrigated meadows in the Sandhills and Platte River valley and other pasture sites that are dominated by smooth bromegrass or Kentucky bluegrass (Figures 1 and 2: Figure 1: https://go.unl.edu/uupv and Figure 2: https://go.unl.edu/rspa).

BEETLE – GRUB LIFE CYCLE
The May/June beetle has a three-year life cycle and damages grasses by feeding directly on their roots and underground rhizomes (Figure 3 https://go.unl.edu/0hwt). It is also sometimes referred to as the three-year grub.

• YEAR 1 - The adult beetles emerge in May and June and lay their eggs in the soil. The hatched grubs feed until late fall and overwinter in the deeper layers of the soil.
• YEAR 2 - In the spring, the grubs move close to the soil surface and continue feeding on the roots all season. By the end of fall, the grubs reach maturity and burrow deep into the soil to overwinter.
• YEAR 3 - In the spring, the fully grown grubs move back to the surface to feed on the roots until July and then undergo pupation. Adults emerge in few weeks but remain in the soil for winter and emerge in the following spring.

The root feeding by grubs results in brown dead patches in the grasslands. The extent or size of damaged areas will vary depending on the infestation population. Several of the damaged sites observed in 2017 were 0.5 to 3 acres in size, but damage to the extent of about 25 acres has been reported. The presence of grubs in damaged spots can be verified by digging the soil or lifting off the dead sod. On heavier soils, grubs generally infest areas of pastures where shallow-rooted non-native cool season grasses such as smooth bromegrass or Kentucky bluegrass dominate. There is a preference for sites with more adequate and consistent soil moisture such as canyon bottom overflow sites or flat areas. On subirrigated meadows, other introduced species in addition to smooth bromegrass and Kentucky bluegrass are also susceptible to grub damage. Damage to pastures dominated by native cool- and warm-season grass mixtures or upland Sandhills sites is not common. 

PREVENTATIVE MANAGEMENT AND CONTROL INSECTICIDE
Reducing white grub infestations in pastures and meadows can be a difficult task since there is only one product available for application, Sevin SL. To be effective, Sevin SL must be applied directly to where the white grubs are feeding. To achieve maximum control, the infested area should be surveyed first, to determine the developmental stages of the larvae. This can be accomplished by using a shovel and collecting a soil sample. Sample at different locations in the infested area. C-shaped larvae that can fit in an area the size of a dime or something smaller will most likely be 1st or early 2nd instar larvae. Larvae at this developmental stage will be more susceptible to the insecticide treatment. C-shaped larvae that can fit in an area of a penny or nickel will most likely be 3rd instar or mature larvae, and will not be effected by an insecticide treatment. If the decision to apply Sevin SL is made, the application should target sometime between June 1 and June 15 when the larvae will be closer to the soil surface feeding on plant roots. Following are treatment recommendations for Sevin SL:

If the site will be grazed during the year;
1 quart of Sevin SL/acre applied with a minimum of 20 gallons of water/acre

If the site will not be grazed during the year;
1.5 quarts of Sevin SL/acre with a minimum of 20 gallons of water/acre

With both treatment scenario’s, the use of flood jet nozzles, and the mowing of existing vegetation within the treatment area prior to application will help the treatment penetrate into the soil where the larvae are located. However, employment of all these methods may not insure complete control.

The use of any insecticide in a pasture can also impact non-target beneficial species. Before making any insecticide applications, please access Bulletins Live Two!https://www.epa.gov/endangered-species/bulletins-live-two-view-bulletins to determine if the property lies within any endangered species habitat zone. This information can also be accessed through your local Extension Office. Before applying any insecticide, please read and follow label instructions.

NON-CHEMICAL
Prevention is always the best strategy in managing white grubs in pastures and meadows. Maintaining a healthy pasture with a good balance of native grasses and native broadleaf plants and controlling non-native cool season grasses through grazing rotations are sound steps toward reducing potential infestations. Pastures which have a higher percentage of non-native cool season grasses with nearby windbreaks or stands of trees appear to be more attractive to white grubs. In grass plantings or hayfields, mixing perennial legumes such as alfalfa with grasses are suggestions. Reducing overgrazing and increasing soil fertility are two additional steps which can help reduce white grub issues.

GRASS RECOVERY IN DAMAGED AREAS
Generally, the best advice is to allow the grubs to cycle. Some landowners express concern regarding the perceived damage that animals such as raccoons, skunks, or badgers may be causing in the pasture as they forage for grubs. However, the grubs are the true source of the damage and the activities of the foraging animals is beneficial in reducing the grub population. Natural predators such as birds, rodents and other small mammals also help by digging out the grubs. 

While grass growth and production in the subsequent year or two will be reduced after grub damage, recovery on subirrigated meadow sites can be relatively quick. There generally are some surviving grass plants in the damaged sites that are able to vegetatively propagate and reestablish the stand. On damaged upland sites with silt/loam soils, recovery could be slower depending on subsequent year precipitation and there is the likelihood of some weedy plant infestation. 

Reseeding could be a viable option if considerable acreage had severe damage or if a landowner waited a year and there was minimal recovery that occurred. Replanting soon after the grub damage has occurred can be challenging as the loose sod layer can cause planting issues and poor seed placement.

Grubs are the larvae of beetles and are commonly called white grubs. There are three beetles that can cause damage in pastures as grubs: May/June beetle (native), Green June beetle (native), and European Chafer (introduced). It is likely that most of the damage that has occurred this past growing season is from the grubs of the May/June beetle.