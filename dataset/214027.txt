DAP, MAP and potash were all lower on the week.

DAP $2.95 above year-ago pricing -- lower $1.35/st on the week to $444.28/st.
MAP $8.69 above year-ago -- lower $3.68/st this week to $455.31/st.
Potash $23.51 above year-ago -- lower $3.08/st this week to $325.39/st.


MAP led declines in the P&K segment with Missouri down $28.70, North Dakota off $15.20 and Nebraska softening $8.28. Two states were unchanged as Michigan gained $5.64 and South Dakota and Ohio each gained roughly $2.20 per short ton.
Potash was right behind MAP with North Dakota down $22.60, Missouri softening $21.90 and Ohio off $1.58. Only Wisconsin was unchanged with South Dakota gaining $3.14 and Kansas up $2.93.
DAP was lower as well, led by Missouri which fell $15.10 with Michigan softening $3.44 and Iowa off $1.17. Five states were unchanged as Minnesota gained $10.19 and Illinois adding 50 cents per short ton.
PotashCorp of Saskatchewan reported its Q3 results this week. Adjusted earnings came in well below the expected 12 cents per share at just 9 cents per share. But according to the company, the downdraft could have been much sharper had potash sales volumes not risen to 2.9 million tons from Q2's 2.4 million tons. Decreases in nitrogen and phosphate prices get the nod for the softer-than-expected report. Potash sales were actually the bright spot during the quarter and the higher sales volume created optimism that potash prices may begin to rebound.
Historically, retail potash prices have been slow to move in a normal year. We did see potash prices fall sharply a few years ago when a production war broke out in the FSU. Beyond that, vitamin K is generally a slow and steady competitor. This tempers PotashCorp's belief that potash prices may have finally found some legs. Keep in mind, from a very simplistic perspective, higher prices will likely spur increased production in the FSU, much like shale oil and crude futures.
It is important to point out that PotashCorp is working toward a merger with Agrium Inc. which will undoubtedly support higher share prices and give PotashCorp a degree of insulation from woes in the wholesale market as Agrium's operation includes both wholesale and retail operations. But that does not mean that retail potash prices will run higher. The winter offseason is right around the corner, and with snow falling in key growing areas, a significant potion of fall P&K applications may be pushed off to spring. That will mean supplies will remain strong, near-term demand will fall and the 8.2% rise in prices reported to your Inputs Monitor since November 2016 could quickly retrace to the downside.
For a decent discussion of PotashCorp's results, click here to read a good summary from SeekingAlpha.com. 
We will remain patient with retail potash as we expect to find opportunities to book below today's price sometime this winter.


By the Pound -- The following is an updated table of P&K pricing by the pound as reported to your Inputs Monitor for the week ended October 20, 2017.
DAP is priced at 46 1/2 cents/lbP2O5; MAP at 43 1/2 cents/lbP2O5; Potash is at 27 1/2 cents/lbK2O.





P&K pricing by the pound -- 10/27/2017


DAP $P/lb


MAP $P/lb


Potash $K/lb

 



Average


$0.46 1/2


$0.43 1/2


$0.27 1/2

Average



Year-ago


$0.46


$0.42 1/2


$0.25

Year-ago