Three days after President Trump was inaugurated, one of his first exercises of presidential power was withdrawing the U.S. from the 12-nation Trans-Pacific Partnership (TPP).

The remaining 11 nations are expected to sign the final trade agreement in March, and now U.S. farmers and ranchers are pushing for a last-minute rejoin.

In a letter to U.S. Trade Representative Robert Lighthizer, the U.S. Food and Agriculture Dialogue for Trade urged him to re-engage in trade talks with other member-nations of TPP, which is driven by Japan, Australia, and Canada.

“Once this happens, our sector will be placed at a substantial disadvantage as other countries gain entry into [agricultural] markets at substantially lower tariffs and under preferential terms,” the letter read.

Collin Woodall, senior vice president of government affairs with the National Cattlemen’s Beef Association (NCBA), says some countries in the TPP would like to see the U.S. rejoin the agreement, but for now, it looks unpromising.

“All of the people who are on board right now are doing everything they can to finish the negotiations on NAFTA and KORUS,” he said. “There’s not a lot of people sitting around looking at things to do. I don’t believe getting back to TPP is something we’re going to see in 2018.”

At the World Economic Forum in Davos, Switzerland last month, Trump made the suggestion of re-entering TPP “if it is in the interests of all.”

Legislatures from Australia, Brunei, Canada, Chile, Japan, Malaysia, Mexico, New Zealand, Peru, Singapore and Vietnam still need to ratify the deal.