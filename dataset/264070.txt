Trade relations between China and the U.S. are now on rocky waters as the Trump administration has turned up the heat on tariffs. The first came in March when the president announced a 25 percent tariff on steel imports and a 10 percent tariff on aluminum. 
“Steel and aluminum will see a lot of good things happen,” said President Trump. “We're going to have new jobs popping up - we're going to have much more vibrant companies.”
It's those who want to see domestic steel production grow—like Illinois Congressman Rodney Davis— who are supporting the president's move.
“Something had to be done when it came to steel,” said Davis. “We've seen government -supported, cheap steel being dumped into our country.”
Davis says it's that action that caused a steel plant in Granite City, Illinois to close. 
Trump's fair trade approach turned even more strenuous, as the president is demanding his top advisers to hit China with steep tariffs in response to allegations of intellectual property theft. Those tariffs could be applied to more than 100 Chinese products ranging from electronics to furniture. 
Soybean farmers are on edge, fearing retaliation on reliable soybean exports to China could be next. 
“We've heard the Chinese say directly that they would retaliate against American soybean imports to that country,” said Patrick Delaney, communications for American Soybean Association (ASA). “That’s a huge deal. They buy more of our beans than the rest of the world combined.”
“I think some retaliation is inevitable,” said Peter Meyer, senior director with S&P Global Platts Agricultural Commodities Analytics. “That's the way the Chinese would send the message. Unfortunately, it always seems that agriculture is the first one to go. We just seem to be the easiest target.”
It's that trade disruption that could have a ripple effect and become long-lasting. 
“History tells us that it takes a long period of time to cultivate a client and a very short period of time to lose that client,” said Meyer. “That's my biggest fear. While you may think that aluminum and steel have nothing to do with soybeans, they have everything to do with soybeans now.”
Analysts like Arlan Suderman of INTL FCStone say when it comes to soybean exports, China is in control. 
“It’s a buyers’ market from their standpoint,” said Suderman. 
He says the wildcard for China right now is South America, and if the country can source enough grain from countries like Brazil and Argentina. That’s as the most recent U.S. Department of Agriculture (USDA) projections slashed Argentina's soybean production estimates over dryness concerns. 
“I think they are just waiting to see just what is the size of the production there in Argentina,” said Suderman. “How much will that draw supplies from Brazil? What will be the available supplies for them because one thing they don't want to do is threaten the availability of protein for supporting their meat industry, because that has a direct impact on food inflation within China.”
Meyer thinks while South America isn't the main factor in China's decision on whether to retaliate, Brazil is becoming an even larger player. 
“I can say last week, when we saw the big Chinese buys, I would attribute that to the fact that the Brazilian harvest was behind compared to last year.”
Now he fears the U.S. soybean crop is a secondary supplier to China, falling behind Brazil. 
“China is going to import 97 million metric tons, they may get to 100 million metric tons this year,” said Meyer. “To see their numbers increase, and our share of that export number decrease, is very concerning.”]
It's Chinese demand that some in agriculture say can’t be ignored.
“In general, 95 percent of the world's population is living outside the United States, and a big portion of that in China itself,” said Kimbrely Atkins, U.S. Grains Council chief operating officer.
The ambivalent relationship with China is nothing new. Earlier this year, China launched an anti-dumping and anti-subsidy investigation into U.S. sorghum imports. A case that National Sorghum Producers (NSP) CEO Tim Lust says doesn't stand on solid ground.
“We've been very clear that we have a great relationship with our Chinese buyers,” said Lust. “This case was not initiated by our customers, and certainly we want to a fair record and a fair case to move forward.”
The move by China initially proving to be a big blow to U.S. sorghum prices as China is the top buyer of U.S. sorghum. 
“China's actions on grain sorghum certainly have put a shockwave into the market,” said Suderman. 
He says as shipments to china have remained fairly strong, there's still doubt hanging over the market, including the motive behind China's case. 
“Everything China does is to protect China, and they have a huge reserve of corn that they're trying to get rid of,” said Suderman. “They're not doing that, but not fast enough. That corn is going out of condition from all indication we can find, and with U.S. grain sorghum going into China, that's displacing corn that they'd like to see going into the feed stream, into the ethanol plants.”
Lust says while it's a battle that's not going away, ultimately costing the association time and money, he encourages producers to focus on the bright side. 
“I think obviously the fear of the unknown is significant and there is a lot of that going on right now, but I just want to reassure growers there are still a lot of positives happening in this industry,” said Lust. 
While agricultural groups understand the importance of a major buyer like China, the National Association of Wheat Growers (NAWG) are on the other side of the fence, supporting the administration’s recent World Trade Organization (WTO) case against China. 
“They're currently subsidizing their domestic wheat growers at exchange rate would roughly be about $10.40 a bushel,” said NAWG CEO Chandler Goule. “So, we will continue to work on that. That’s costing the U.S. wheat growers about $640 million in lost revenue a year.”
Goule is also striving to protect free trade, becoming vocal about the current administration’s actions possibly losing ground in the world trade arena. 
“My biggest concern though is we've lost some of our leverage and ability to negotiate because we left the [TPP] agreement, and now there is uncertainty out there in the international arena of if the U.S. comes back and reengages and doesn't get exactly what they want, are we going to pull back out again?” said Goule. 
While it's a balancing act, it's adding more uncertainty to a trade battle that seems to be gaining strength.