As the holiday season gets underway in earnest, Stemilt Growers is encouraging retailers to promote its Lil Snappers Artisan Organics pears.
“The holiday season is a great time to promote organic pears as it is a time of celebration,” Brianna Shales, communications manager at Stemilt, said in a news release.

“People are looking for healthy, specialty items for their holiday parties, and quick, healthy snacks for their kids, and pears are definitely part of that category.”
Stemilt’s Lil Snappers brand of small-sized fruit was originally geared toward parents, but the organic option has been popular with many types of shoppers, according to the release.
“The Lil Snappers program does exceptionally well with consumers who are looking for smaller portion sizes,” Shales said in the release. “In-store dietitians use this program to engage with diabetic shoppers who do not need large portion sizes, and the convenient grab-and-go style bag is perfect for any of today’s time-strapped shoppers.”
The Wenatchee, Wash., company expects to ship Lil Snappers organic bosc and organic red pears from now until January. Stemilt is also offering organic concorde pears, a green variety with an elongated neck and a vanilla-like flavor, according to the release.
The trio of varieties offer colorful merchandising options and are each available in 2-pound pouch bags, which offers a lower price point for consumers while still moving more volume than a typical organic pear purchase, Shales said.
Stemilt offers retailers marketing support including digital and print resources.
“Anything from geo-targeting on social media to store-specific advertisements is possible and Stemilt is happy to help,” Shales said in the release.