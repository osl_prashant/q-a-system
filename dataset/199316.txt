Ever heard of that old saying, "There's no such thing as bad publicity?"It certainly applies to an enterprise that is desperately seeking public awareness. For an unknown author eager to make it big, for example, getting your book banned by a bunch of uptight librarians because it's too salacious does wonders for sales.
Or another example: When the Hummer was first introduced as a civilian vehicle, automobile critics quickly dubbed it "too big, too powerful and too expensive" for any ordinary driver to use for cruising around town. Well, that's exactly what spawned its "must-have" status among the highly affluent, and among those who wished to pretend they were.
But bad publicity, when it never ends, when it rolls over an industry with the regularity of the tides, slowly erodes a product line's credibility. Even worse, it tends drive away the casual consumer ‚Äî especially if he or she has other readily available options.
About that "tide" of bad publicity? Here are headlines from just a single day's worth of the news cycle:
"Is jackfruit the pulled pork of vegans?"
"Are Carbs as Bad as Red Meat and Cigarettes When it Comes to Lung Cancer?"
And if those burning questions don't grab your attention, how about a more sober, scientific statement:
"How red meat spurs changes I the gut that can raise heart attack risk."
The first story, courtesy of Fox News, purports to show a parade of people on the streets of New York City sampling Thai curry-flavored jackfruit, which for the uninitiated, is a huge Asian tree fruit, often weighing as much as 75 pounds, that can be marinated and processed to resemble pulled pork.
Despite its awkward name ‚Äî but remember, there's no such thing as a bad name ‚Äî jackfruit has started showing up on the menus of trendy restaurants and even barbecue joints, such as Baltimore's Blue Pit and San Francisco's Sneaky's BBQ. At the Super Bowl last month in Levi's Stadium, fans could purchase a Jackfruit BBQ Sandwich.
Let's not get ahead of ourselves, but if "vegan alternative" to what is arguably the meat industry's most iconic product gains popularity ‚Äî and credibility ‚Äî it does not bode well for the future.
Jumping to conclusions
The other two stories are another couple of the endless variations on the theme that veggie activists love to play: Red meat will kill you.
In the CNN article about carbohydrates and lunch cancer, the story was actually about a survey of 2,000 Texans who had developed lung cancer. The researchers discovered that "carb lovers" ‚Äî people whose diets contained the most high glycemic index foods ‚Äî were 49% more likely to have been diagnosed with lung cancer than people who ate relatively few high-glycemic carbs.
For the record: "High glycemic index carbs" include white bread, white rice and white potatoes, in contrast to low glycemic index foods, such as pasta, oatmeal and sweet potatoes.
Yet the headline referenced "bad" red meat as a cancer causer, even though the study had absolutely nothing to do with meat-eating. Even the URL for the online post contained the string "are-carbs-as-bad-as-red-meat-and-cigarettes."
In fact, the researchers speculated that high glycemic index foods increase lung cancer risk because they cause blood sugar spikes that stimulate the secretion of insulin, which in turn can trigger cells-=-including cancerous cells‚Äîto speed up their growth rate.
See anything in there about red meat?
That's the kind of "bad publicity" that really is bead.
Finally, a new study published in the clinical journal Cell reported that choline, one of the B vitamins that is found in steaks and other animal foods, can activate microbes in the intestinal tract and potentially increase the danger of blood clot formation, and if patients are susceptible, thus increase the risk of heart attacks.
Lot of "ifs" and "potentials" in that summary.
Oh, but we don't need to wait for further studies that might more precisely elucidate these metabolic pathways and better quantify the actual risk, nor perhaps a clearer understanding of why an essential B vitamin is suddenly the "bad guy" here. Oh, no, no, no. We're ready to roll right now with a headline that connects red meat and heart attacks.
Read no further, Mr. or Mrs. Consumer. "Steak = death." End of story.
In truth, there is a lot more going on here than merely one nutrient (potentially) triggering heart attacks. Cardiovascular disease is a multifactorial development in which many biological, genetic and nutritional variables play a role. If it were caused by something as simple as eating too much choline, every doctor on the planet would be advising patients to cut down on its consumption.
Another equally reputable scientist, Carolyn Slupsky, a professor of Nutrition and Food Science at the University of California-Davis, said it best in refusing to label choline as the culprit.
"We need it choline for brain and liver function," she told Statnews.com, "[and] we still don't understand food very well."
But that's not a position anyone in the media is willing to acknowledge.
To them, any story that prompts an emotional response . . .well, to them it's all good publicity.
Dan Murphy is a food-industry journalist and commentator