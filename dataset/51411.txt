May 2017 natural gas opened today at $3.19 -- up 14 cents from last Friday.
Farm Diesel is 3 cents lower on the week at an average of $1.98 per gallon.
May 2017 WTI crude oil opened the day at $50.32 -- up $2.65 on the week.
May 2017 Heating oil futures opened the day at $1.56 -- up 8 cents from our last report.
Propane is down 4 cents at an average of $1.37 per gallon regionally.

Farm Diesel -- Diesel was led lower by Missouri which fell 31 cents per gallon as Iowa fell 12 cents and Indiana softened 3 cents per gallon. Seven of our twelve states were unchanged as Ohio led gains firming 5 cents and North Dakota added a penny.
Heating oil futures had a bullish week on the back of higher WTI crude futures pricing. Technically, both look technically bullish and with so much confusion on the production side, traders are likely to default to technical features for direction. Fund managers have trimmed their record long position in crude oil futures but bullish sentiments are gaining ground. The same is basically true of heating oil although bearish sentiment appears to have the upper hand.
I spoke with an analyst earlier this week who labeled the outlook for crude oil "friendly through the end of April", but was quick to remind that OPEC can quickly throw a monkeywrench into crude pricing, and, by default, farm diesel prices.
If the recent past is any indication, we could expect crude oil and related products to seek a range with crude tending to confine itself to about a $5.00 slot. Even though WTI crude firmed back above key $50 per barrel this week, a test of the upper and lower limits of that range is healthy for the marketplace.
Geographically, Missouri -- a bellwether state on price action -- fell an impressive 31 cents this week. We noted that missouri along with Kansas and Nebraska all sported declines in fertilizer pricing as most other states were higher this week. Since fieldwork begins earlier in those states than in the other 9 states we survey, sinking fertilizer prices there give a glimmer of hope that fertilizer prices will fall elsewhere in kind, once early demand is exhausted. With Missouri farm diesel down sharply this week, we will be watching closely next week to see if the Show Me state feels the downside is overdone. If not, the indication is that farm diesel prices across the Midwest will also fall once demand for spring fieldwork runs dry.

Distillate inventories reported by EIA fell 2.5 million barrels to 152.9 mmbbl. Stocks are currently 8.3 mmbbl below the same time last year.
The regionwide low currently lies at $1.92 in Wisconsin and Nebraska. The Midwest high is at $2.05 in Illinois.





Farm Diesel 3/31/17


Three Weeks Ago


Previous Week


Change


Current Week

 



Iowa


$2.06


$2.08


-12 cents


$1.96

Iowa



Illinois


$2.05


$2.05


Unchanged


$2.05

Illinois



Indiana


$2.06


$2.06


-2 cents


$2.03

Indiana



Ohio


$1.90


$1.90


+5 cents


$1.95

Ohio



Michigan


$1.98


$1.99


Unchanged


$1.99

Michigan



Wisconsin


$1.92


$1.92


Unchanged


$1.92

Wisconsin



Minnesota


$2.00


$2.03


Unchanged


$2.03

Minnesota



North Dakota


$1.99


$2.00


+1 cent


$2.01

North Dakota



South Dakota


$1.98


$1.98


Unchanged


$1.98

South Dakota



Nebraska


$1.93


$1.92


Unchanged


$1.92

Nebraska



Kansas


$1.96


$1.96


Unchanged


$1.96

Kansas



Missouri


$2.28


$2.28


-31 cents


$1.97

Missouri



Midwest Average


$2.01


$2.01


-3 cents


$1.98

Midwest Average



Propane -- Kansas firmed 4 cents per gallon as Missouri and Nebraska each gained a penny per gallon. Five states were unchanged as Indiana led declines, falling 45 cents per gallon. Iowa shucked 12 and Minnesota fell 2 cents.
It is interesting to note that as diesel fuel in Missouri fell hard this week as Nebraska, Missouri and Kansas fertilizers fell that those three states comprise our only price gains in propane this week. This is likely related to supply features rather than demand and it would make sense that, after a chilly, gloomy week in areas north and east of there, supplies may have been diverted north, supporting higher LP prices in the southwest corner of our survey region.
Continuing export activity will keep a hard floor under LP even during the U.S. offseason and it is likely that the summer slump will not cut nearly as deeply as last year's 96 cents per gallon. We will likely have to settle for a summertime LP price low closer to $1.10-$1.20.

According to EIA, last week, national propane inventories softened 1.538 million barrels -- now 20.030 million barrels below the same time last year at 42.795 million barrels.
The regionwide low is at $1.09 per gallon in South Dakota and the regionwide high is in Kansas at $1.62.





LP 3/31/17


Three Weeks Ago


Previous Week


Change


Current Week

 



Iowa


$1.15


$1.17


-6 cents


$1.11

Iowa



Illinois


$1.40


$1.40


Unchanged


$1.40

Illinois



Indiana


$1.74


$1.74


-45 cents


$1.29

Indiana



Ohio


$1.25


$1.28


Unchanged


$1.28

Ohio



Michigan


$1.43


$1.44


-1 cent


$1.43

Michigan



Wisconsin


$1.30


$1.30


Unchanged


$1.30

Wisconsin



Minnesota


$1.30


$1.29


-2 cents


$1.27

Minnesota



North Dakota


$1.27


$1.27


Unchanged


$1.27

North Dakota



South Dakota


$1.09


$1.09


Unchanged


$1.09

South Dakota



Nebraska


$1.24


$1.22


+1 cent


$1.23

Nebraska



Kansas


$1.62


$1.58


+4 cents


$1.62

Kansas



Missouri


$1.38


$1.44


+1 cent


$1.45

Missouri



Midwest Average


$1.35


$1.35


-4 cents


$1.31

Midwest Average



 
Our spread analysis indicates lower farm diesel ahead, but this week's increases in heating oil futures may have next week's spread leaning toward the upside.