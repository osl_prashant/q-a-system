Randy Dowdy says he’ll face major yield loss from natural gas pipeline project 
As he walked along muddy turnrows under pounding January rains, Randy Dowdy knew part of the topsoil from the farm that birthed the highest soybean yields in world history was gone. His 171.7 bu. soybeans and 521 bu. corn from fall harvest faded far into the past.

Today the topsoil on more than 40 acres has been stripped or flipped and replaced or mixed with fresh dirt. In agriculture, dirt is death and soil is life. Compounding the topsoil loss, 100 acres of wetlands caught much of the fertilizer-heavy slurry as it spilled off Dowdy’s Brooks County land in southern Georgia. The reconstruction bill is expected to top $1 million.

Dowdy signed an easement in 2015 giving Spectra Energy right of way across a mile of his land for the Sabal Trail natural gas pipeline, a 515-mile project running through Alabama, Georgia and Florida. The section of the project on Dowdy’s land began after fall harvest and was slated for completion the first week of 2017. 

On Dec. 6, Dowdy contacted Sabal Trail management, expressing concern about erosion and emphasizing the sensitivity of his ground. According to Dowdy, he continued to contact management and was assured construction would be done on schedule. 

“I texted again Jan. 9 and nothing was done,” Dowdy says. “No rebuilt terraces, cover crops or restoration.”








 


Top left: After hard January rains, topsoil laden with fertilizer washed into Randy Dowdy’s wetlands. Below: A breached sediment barrier at the mouth of a terrace is a violation of Georgia’s Soil and Water Commission’s Green Book.











The third week of January, the skies opened. Construction was ongoing and Dowdy’s unprotected topsoil was exposed to heavy rains. Across a 180-acre farm, two-thirds of the runoff was headed directly for the terraces. Sabal Trail’s sediment barriers at the mouth of each terrace acted like corks, backing up water into the fields until the watershed surrendered to gravity, escaping across barriers, over terraces and into a creek. Dowdy’s meticulously crafted elixir of protozoa, microbials and organic matter was whisked away. Bon voyage to soil health.

Who is to blame? Dowdy points to Sabal Trail and alleges regulatory violations. Sabal Trail declined interview requests citing privacy concerns. Parent company Spectra Energy didn’t respond to phone or email questions.
When Dowdy signed the easement, the agreement included a stipulation: Sabal Trail would return all land to its pre-construction condition, both in fertility and soil deposition. 

Dowdy’s ground runs at a steep 10% to 12% grade. Cover crops and terraces control water runoff and slow the flow to a 1% grade equivalency. The gas line runs mainly north to south, and Dowdy’s terraces run east to west. The gas line breaks through every terrace. 

Dowdy says the topsoil disaster was a direct result of Sabal Trail negligence in following the Georgia Soil and Water Commission’s Green Book (Manual for Erosion and Sediment Control in Georgia) regulations. “Sediment barriers in concentrated flows of water; no straw covers; no safety sediment fences; and many more violations,” Dowdy contends. 








 


"It’s one thing to rebuild terraces, haul in topsoil and straw, plant cover crops and spread chicken litter. It’s another thing to gain soil life from dead dirt." - Randy Dowdy, Brooks County, Georgia











At Sabal Trail’s request, he provided three restoration estimates. One: costs of topsoil purchase, extraction, hauling, grading, soil health applications and terrace reconstruction. Two: estimation of damage to wetlands. Three: long-term yield loss projections. According to Dowdy, Sabal Trail agreed to pay for topsoil restoration and allowed him to begin the process.
He hauled in eight to 18 trucks of lower grade topsoil per day and used one excavator, one motor grader, two bulldozers, two tractors and hay blowing equipment for a $25,000 price tag per day. Before Sabal Trail would write a check, Dowdy was required to sign a release waiving compensation for future yield loss and wetlands damage. “They knew I would spend $700,000-plus and were squeezing me, but there was no way I would sign,” he explains. 

In March, Dowdy filed a complaint with the Environmental Protection Division (EPD) of Georgia. EPD enforces Green Book regulations but only monitors potential construction project violations on a complaint basis: One representative in south Georgia covers nine counties. “We rely on people letting us know about issues. However, we investigate every single complaint we get,” says Burt Langley, EPD’s director of compliance.

Joe Freeman, environmental compliance officer with EPD, visited the site on March 10 and didn’t see any best management practices violations. “Mr. Dowdy has already undertaken the re-terracing of his fields, and the evidence is effectively covered. It may have been different if I’d seen things in December,” Freeman says.








 


While repairing an irrigation line, Randy Dowdy found jumbled soil deposition. “Even the soil that was saved and put back on my land wasn’t segregated.”











“The only people involved in reviewing compliance with permitting standards are on Sabal Trail’s payroll,” Dowdy responds. “Isn’t that the fox guarding the henhouse? What’s the point of having an agency that issues permits if they don’t personally police for compliance?”

On March 11, while fixing an irrigation line leak, Dowdy found  jumbled soil deposition—a violation of Sabal Trail’s agreement. Rance Harrod, irrigation manager at Nashville Tractor, ran an excavator: “I pushed off a couple inches of topsoil and hit at least a 6" layer of a hard clay and blackish dirt mix. The excavator was struggling, and the ground was coming up in chunks.”

In succession, Harrod scraped off 2" of topsoil, 6" of hard clay and 10" to 15" of various mixtures before digging into the expected bright orange Georgia clay. “How do other landowners know this hasn’t been done on their land?” Dowdy asks. “Farmers and landowners are just supposed to sign a release and the story is over?”

Dewey Lee, University of Georgia agronomist, says the ramifications of soil disturbance and erosion on Dowdy’s ground are incalculable. “It’s impossible to replace the positive effects of Randy’s management on his soils in a short period of time. Just in the disturbance, you lose aggregation, organic matter, fertility and nutrients,” Lee says. “The negative effects are immediate, but of far more concern, the long-term effects could last decades.”

Dowdy believes he’s facing a lifetime of yield loss on the affected ground due to the negligence of Sabal Trail. He’s hauled in more than 1,000 loads of new dirt and expects he needs at least 800 more. In part, the breadbasket topsoil of the world’s record soybean yield and some of the highest corn yields is being replaced by a forced substitute.