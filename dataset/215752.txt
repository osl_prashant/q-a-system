A winter cold snap arrives. Cow's blood flow slows. Cows start limping. It's time to check herds for fescue foot.
The first report of the disabling disease has come to Craig Roberts, University of Missouri Extension fescue specialist.
The case is from southwestern Missouri, Roberts says. A herd owner counts 20 limping cows in a herd of 100. That can happen anywhere in Missouri.
If caught early, cows can be taken off of toxic Kentucky 31 pastures and given other forage or feed.
If left on toxic pastures, limping cows will lose hooves. There's no cure, Roberts says.
When cold weather arrives, Roberts urges owners with herds on toxic grass to check them every day. Losses are serious.
The first sign is limping. Hooves on hind feet may show swelling where legs join hooves. If the junction shows red and necrotic, it may be too late. Advanced stages show gangrene and hoof loss.
That's too late to correct. Early detection is vital.
The toxic alkaloid in fescue is a vasoconstrictor, which shrinks blood vessels. That lowers blood flow to extremities, causing frostbite. Calves lose tips of their ears or switches from their tails.
Those losses are not fatal. However, they indicate that they are "fescue calves." That lowers their value as feeder calves.
Last year, the first case reported six cows lost, or 20 percent of the herd.
Both early cases were from southwestern Missouri. That area has more cow herds and more pure stands of toxic fescue. Northern pastures may be diluted with nontoxic grasses.
The fescue problem has been known for years. Fescue foot was the first alert to a toxic alkaloid released by a fungus between cell walls of Kentucky 31 fescue. The ergovaline toxin does more damage than fescue foot.
The toxin cuts milk flow, hurts calf gains, lowers pregnancies and more.
Farmers ask: "What can I do?" Roberts answers with a question: "How serious are you about doing something?"
The answer takes killing toxic fescue pasture. Then it requires seeding a nontoxic variety.
The fungus benefits the grass, protecting it from pests and grazing.
Plant breeders have inserted naturally occurring nontoxic fungi into fescue varieties, which are called novel endophyte fescue.
The Alliance for Grassland Renewal has conducted workshops for the past five years. They show how to completely eradicate the toxic fescue. That's not easy. Then they tell how to seed a new novel endophyte variety. It's a yearlong process.
Farmers who convert gain peace of mind, Roberts says.
Increased profits come after peace of mind.
Advice is at the Alliance website: www.grasslandrenewal.org.
Help is available from MU Extension plant specialists.
This year, the Alliance will reach into three new states across the fescue belt to the Atlantic coast.
Those workshops, including Missouri, will be in March.