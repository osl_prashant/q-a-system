Emails show FEMA silent as Puerto Rico sought generator fuel
Emails show FEMA silent as Puerto Rico sought generator fuel

By TAMI ABDOLLAHAssociated Press
The Associated Press

WASHINGTON




WASHINGTON (AP) — As hundreds of people stood in line for food and many went hungry during the days and weeks after Hurricane Maria hit Puerto Rico, Walmart Inc. and local supermarkets threw out tons of spoiled meat, dairy and produce.
Emails and text messages made public Tuesday in a letter sent by the top Democrat on the House oversight committee describe frantic efforts by officials at Walmart and the Puerto Rican government to get fuel for generators to prevent food from going bad.
From the Federal Emergency Management Agency came only silence.
Within a three-hour time span, Walmart officials were able to connect, through email and text messages, with a congressman's office and local Puerto Rican government officials. They passed on their urgent request for help, just two days after the hurricane made landfall.
Meanwhile, the letter states, FEMA remained unresponsive for days.
The fuel issue is another window into difficulties the agency has faced in responding to Hurricane Maria, along with providing Puerto Ricans with thousands of tarps for the homeless and millions of meals for the hungry.
Walmart ultimately disposed of an unclear amount of perishable foods, and local supermarkets reported that they lost tens of thousands of dollars in perishable foods, according to the letter sent by Rep. Elijah Cummings, D-Md., ranking member of the House Committee on Oversight and Government Reform, and Stacey Plaskett, the delegate from the U.S. Virgin Islands.
Their letter reiterates a request made to Oversight Committee Chairman Trey Gowdy, R-S.C., in October for documents from the Department of Homeland Security related to FEMA's preparation for and response to Hurricanes Irma and Maria in Puerto Rico and the U.S. Virgin Islands. Gowdy's office did not immediately reply to requests for comment.
Maria, which made landfall in the U.S. territory of Puerto Rico on Sept. 20, shut down ports, destroyed crops, and disrupted the power grid, leaving supermarkets without electricity or the fuel to run their generators. Hundreds of thousands of people were also left without easy access to food.
Two days after the hurricane, a senior Walmart official emailed Rep. Luis Gutierrez, D-Ill., to ask for help keeping food refrigerated in the few stores they had been able to get up and running.
"The problem is we're running out of generator fuel and need help getting the Governor's approval for me," the Walmart official wrote. "Have you guys been in touch with anyone from FEMA that we can contact to help? We want to keep this food fresh for people."
Walmart had opened three facilities to support the public and had plans to bring more on line, but the key to doing this was having power to run the operation and re-establish its supply chain, the Walmart official wrote.
Roughly an hour after that first email, Gutierrez's office forwarded the email to a Puerto Rican government official. And 12 minutes later, the Puerto Rican government official responded: "FYI I'm sitting with FEMA rep right now so we are taking care of this."
Walmart officials sent over a priority list of a dozen of their top stores — they operated 46 on the island — needing fuel to keep food from spoiling, in addition to their distribution center and home office.
"Fuel at this point is becoming a key concern as we are less than 24 hours left in maintaining power in most facilities," the Walmart official wrote.
The message was forwarded by the Puerto Rican government official to a FEMA official 26 minutes later. But by Sunday, two days after initially reaching out, there was still no response from FEMA. The Puerto Rican government official texted Walmart that FEMA had not responded to numerous requests.
"Did the hospitals get fuel?" the Walmart official asked.
"I think so. But I can't be sure. Our communication with FEMA on the specifics of certain things has been less than desired," the Puerto Rican government official said.
The following morning, the two exchanged messages again. The Puerto Rican government official informed Walmart that he had reached out to the island's emergency management agency, which they hoped could help.
"Our chief concern right now is with our distribution center," the official text messaged back. "We might have two days worth of fuel left. It is critical that we keep that going in order to preserve our fresh inventory. If that goes down it could take weeks to replenish which would have a big negative impact on the island."
A week after the hurricane, still with no reply from FEMA, Puerto Rican Gov. Ricardo Rossello personally reached out to FEMA to request emergency fuel for grocery stores to maintain perishable food supplies, the letter's timeline states.
A Puerto Rican government official informed FEMA officials in an email that "because of immediate threat to public health and safety, the Governor asked John Rabin (FEMA's Acting Regional Administrator for Region II) at 8:10 a.m. this morning to have FEMA deliver fuel to all grocery and large retail immediately."
It's unclear exactly how much food was lost or whether FEMA ultimately provided the emergency fuel.
Responding to a request for comment, FEMA spokesman Daniel Llargues said in an email Tuesday that the agency is aware of the letter and has consistently worked with the committee and will continue to do so.
"The protection of life and safety is our first priority in any response, including working closely with the government of Puerto Rico to support the fueling mission for critical infrastructure" such as hospitals and communications centers, Llargues said.
Llargues said that FEMA has distributed more than 13 million gallons of fuel to date. He did not provide any timeline nor did he confirm the details in the letter.
"It was tough at the beginning of that all the way through, it was tough, we didn't have power," Phillip Keene, a spokesman for Walmart, told The Associated Press. But he added, "We felt comfortable that people were acting in good faith."
Keene said he did not know how much food was thrown out and could not confirm details of the initial scramble for generator fuel. He said nearly all but a handful of stores were able to get back online within a matter of months, relatively quickly given the extent of the damage.
Manuel Reyes, the executive vice president of the Puerto Rico Chamber of Marketing, Industry, and Distribution of Food, told The Associated Press that the lack of fuel was a greater problem in the immediate aftermath of the hurricane due to logistics issues.
Reyes, whose organization represents grocers, food distributors and food manufacturers on the island, said large quantities of food spoiled because businesses ran out of fuel for their generators and there was no distribution system for delivering more. He said many businesses paid up to four times the real cost of fuel when buying on the black market, sometimes competing with large hotels, housing complexes or department stores.
"Since the beginning we made the local and federal governments aware of this, but as far as we know FEMA did not provide fuel or made trucks available to the private food distribution network," Reyes said. "We believe they did provide some fuel to hospitals. We were forced to establish our own distribution system for our members using retrofitted waste-water trucks in order to keep some stores opened and food from going bad."
Reyes said that after three to four weeks, gas stations began to normalize and businesses could get fuel easier.
"But by that time the emergency generators, which are not designed for continuous operation, began failing and we needed new ones or spare parts but few were available," Reyes said. "No help with that either."
___
Follow Tami Abdollah on Twitter at https://twitter.com/latams