Crowley rice company lands Lafayette lady in Hollywood
Crowley rice company lands Lafayette lady in Hollywood

By SHARI PUTERMANThe Daily Advertiser
The Associated Press

LAFAYETTE, La.




LAFAYETTE, La. (AP) — Candy Domengeaux, 43, is a married mother of two from Lafayette, Louisiana. She works for a local agency called the Graham Group, managing the marketing and advertising for Supreme Rice, a milling operation located in Crowley.
Domengeaux leads a relatively simple life - but earlier this month, she found herself in Beverly Hills dancing with Jamie Foxx at a party for the Academy Awards.
So how did this country girl wind up in one of the biggest cities in America, bumping elbows with Hollywood celebrities?
According to her, it all comes down to the power of social media.
"A couple weeks before the Oscars," she explains, "celebrity chef Bryan-David Scott commented on one of my social media posts and asked to get in touch with me. He wanted information about Supreme Rice, because he never saw a rice brand on Instagram and loved the marketing."
Turns out, he quickly acquired a taste for the product, too.
"He said, 'This is everything I want to use in my dishes,' " Domengeaux says. "He wanted to latch onto a product that was local, family-owned and philanthropic. He loved that about Supreme Rice. I sent him some samples, and when he got them, he told me, 'Candy, this rice is truly supreme.' "
Scott shared the rice with other celebrity chefs, and immediately, they were hooked.
"He said that he was cooking for some pre-Oscar dinners and wanted to use Supreme Rice. He told me that everyone was raving about it."
And that's when Domengeaux got the invitation of a lifetime.
"Scott told me that he wanted me and the client to come to Beverly Hills as VIPs for a party the Friday night before the Oscars," she says.
Throughout the weekend, Domengeaux indulged in some other festivities, including one of Hollywood's most star-studded bashes - producer Byron Allen's Oscar Gala Viewing Party for the Children's Hospital of Los Angeles (the fundraiser raised more than $1.5 million).
"I always loved the Oscars," Domengeaux says. "I have my own viewing parties every year, and it's always been a dream of mine to go — it's crazy that Supreme Rice got me there."
Not only did it get her there - it landed her on stage next to Jamie Foxx, with whom she was photographed dancing.
"First, we got a selfie," Domengeaux says. "I thought that was the highlight of my night. But after the party was over, I was on the side of the stage, Jamie was dancing to a song, and he reached down, grabbed my hand and pulled me up there."
Photos quickly made rounds on social media, and virtually overnight, Domengeaux's very innocent "dance" with Jamie Foxx was seen around the world.
"I had no idea that paparazzi was there, snapping pictures," she says. "My biggest regret was that I didn't have a bag of Supreme Rice in my hand!"
___
Information from: The Advertiser, http://www.theadvertiser.com