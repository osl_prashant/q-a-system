Why should American farmers care if Brazil impeaches its president?Because a fundamental political change in the government of their largest South American competitor could impact global markets.
"A change could make their currency (the real) go higher," said Don Roose of U.S. Commodities.
Although Brazil is mired in a steep recession, made even worse by China's falling imports, the cheaper real has been a boon for Brazilian farm exports, with the real's parity to the dollar at roughly 4 to 1.
If the real gained value, that would be good news for American farmers. Brazil's corn and bean exports along with other commodities have been increasing.
Regardless of what happens with the current impeachment situation, Brazil will remain a regional powerhouse, major competitor and exporter of soybeans and corn, whether or not 13 years of the leftist Workers Party rule is ended.
Impeachment Fight
Meanwhile, President Dilma Rousseff has vowed to fight impeachment next month in the Senate after a crushing defeat in Congress on Sunday. The political crisis has paralyzed the government.
Rousseff's opponent, former ally and Vice President Michel Temer, 75, who himself is embroiled in a corruption scandal, has promised more business-friendly policies like deregulation and opening up concessions on roads and other state-run infrastructure to private investors, according to analysts.
In an attempt to garner international support against her impeachment, Rousseff announced she would travel to New York Friday to address the United Nations, to denounce the impeachment attempt as illegal.
But her opposition already was meeting with top U.S. officials just a few days following Congress' vote to impeach her. Opposition leader Sen. Aloysio Nunes was scheduled to meet with Sen. Foreign Relations Committee Chairman Bob Corker, R-Tenn., and other U.S. officials.
Ironically, while she is gone, Brazil's vice president, who is leading the impeachment process, and vying to become president, will lead the government in her absence. Meanwhile, Brazil's Energy Minister Eduardo Braga announced he was quitting the government, bringing to nine the number of Cabinet officials out of 31 who have resigned, according to news reports.
Potential Impact of New Leadership
A new president could bring about a change in perception by investors, according to analysts.
"Trust, structural reforms and a new strategy to deal with the evolving contours of globalization are what Brazil needs to resume a trajectory of social and economic development," commented Prof. Marcos Troyjo, director of Columbia University's BRICLab.
However, despite the tough economic, political and infrastructure problems, "Brazil is a country where thing work even though they're not supposed to," explained Pedro H. Dejneka, President of Agr Brasil, a unit of AgResource in Chicago.
According to the magazine American Shipper, it costs Brazilian farmers almost twice as much as American farmers to export a metric ton of soybeans because instead of barge transportation, they have to rely on trucking produce over inadequate roads as far as 900 miles from growing regions to ports.
In spite of those conditions, "Brazil is a leading exporter of many commodities; it exports soybeans and corn. They get it done. The Brazilian ports are some of the best in the world," Dejneka observed. "If Brazil gets things done now, imagine if you get international logistics to match."
Next Steps
If the Senate agrees to vote by a simple majority next month, Rouseff, 68, could become the first Brazilian leader to be impeached in more than 20 years.
Severe economic problems have negatively affected her popularity in the country, but she has not been charged with a crime.
Her opponents allege she used a commonly employed budgetary sleight of hand to appear to lower the budget deficit during her 2014 reelection bid.
"A lot is being said about Brazil by people without understanding the full scope," Dejneka cautioned. "A lot of politics are still to be played out."
Watch the AgDay story here:

<!--

    function delvePlayerCallback(playerId, eventName, data) {
        var id = "limelight_player_351368";
        if (eventName == 'onPlayerLoad' && (DelvePlayer.getPlayers() == null || DelvePlayer.getPlayers().length == 0)) {
            DelvePlayer.registerPlayer(id);
        }

        switch (eventName) {
            case 'onPlayerLoad':
                var ad_url = 'http://oasc14008.247realmedia.com/RealMedia/ads/adstream_sx.ads/agweb.com/multimedia/prerolls/agwebradio/@x30';
                var encoded_ad_url = encodeURIComponent(ad_url);
                var encoded_ad_call = 'url='   encoded_ad_url;
                DelvePlayer.doSetAd('preroll', 'Vast', encoded_ad_call);
                break;
        }
    }

//-->


<!--
LimelightPlayerUtil.initEmbed('limelight_player_351368');
//-->

Want more video news? Watch it on AgDay.