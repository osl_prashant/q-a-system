The Chicago Federal Reserve just released its First Quarter 2016 Ag Letter showing that farmland values continue to trend lower. The district covers most of the I states in the corn belt and show the following year-over-year changes in farmland values:Illinois - Minus 5%
Indiana - Minus 2%
Iowa - Minus 5%
Michigan - Minus 7%
Wisconsin - Positive 1%
Overall - Minus 4%
This overall decrease continues the trend to seven consecutive quarters of negative growth. After factoring in the CPI index, this is the largest annual decrease since 1987.
Cash rents fell 10% which again is the largest drop since 1987 and this is after an almost as large as a decrease in the prior year. If you adjust cash rents for inflation, current cash rents are 13% below 1981 levels while "real" land values are still 38% higher than in 1981. These values have continued to widen for the last 7 years.
As you can guess, credit conditions continue to weaken. The repayment rate index was 32 which is the lowest since 1981. Farm equipment sales continue to trend down with large tractors being down 23% and combines down 17%.
Although these numbers do not look good, it appears we are still a long way from the 1980s conditions, but if these trends continue for a few more years, we may be getting closer.