A recent study serves as a timely reminder that consumers still see restaurant food as a treat. 
A Stanford University study says people are more likely to pick vegetables with indulgent descriptions than those labeled “healthy,” and describing menu offerings as healthful actually may be counter-effective, the study says, because people tend to associate food that’s good for them with being less tasty. 
 
The study also supports previous research that used creative labeling strategies, such as introducing superhero characters to promote vegetable consumption among children.
 
This all suggests that it’s possible to increase vegetable consumption in adults by using the same indulgent, exciting and tasty descriptors as more popular, though less healthful, foods, the study said.
 
The produce industry should not think it’s above these useful sales techniques and stories.
 
Successful restaurant businesses already sell the experience along with the food, and it’s never been more important than right now with consumers wanting more local and organic options and being concerned with the values behind the food.
 
The produce and foodservice industry should remember it’s good business to not just sell the vegetables but to sell the sizzle.
 
Did The Packer get it right? Leave a comment and tell us your opinion.