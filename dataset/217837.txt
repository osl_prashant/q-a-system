Attendees of the 88th annual American Society of Farm Managers and Rural Appraisers (ASFMRA) conference in Savannah, Ga., heard a hopeful but sober outlook for agriculture.
 “2018 is a pivotal year for agriculture,” said Terry Barr, senior director, CoBank, during a keynote speech. “It is when we find out if we have entered a stabilization zone or whether we begin another leg down.”
“The impact of energy prices on commodity prices will likely remain relatively benign with crude oil ranging from $45 to $60 a barrel,” he noted. “Meanwhile, world economies are still transitioning out of what their central banks and governments did to stimulate their economies out of the financial crisis. This means we will have sustainable, moderate growth in demand but not what’s needed for another explosive burst in demand for commodities.” 
Central banks have added $14 trillion to their balance sheets. The U.S. Federal Reserve added $3.5 trillion alone. “The Federal Reserve is the only central bank raising interest rates,” he said. “This is strengthening the U.S. dollar and putting the U.S. in a very uncompetitive position going forward,” he said.  
Overall, the next three years to five years will be challenging as the world works through the effects of the massive economic stimulus. “But the long-term outlook is still very positive for agriculture,” he said.
“The 1980s was a credit bubble.  Now, we’re in an asset bubble,” noted David Kohl, agricultural economist emeritus, Virginia Tech. “This recent run-up in farmland values has much more equity and working capital behind it. Marginal land is the first to correct, and we’re seeing marginal land values down as much as 25% in some areas.”
Kohl is also watching exports. “They are 20% of ag incomes,” he said. Impacting that income stream are the value of the U.S. dollar; U.S. economic growth; and interest rate policies of the Federal Reserve and the central banks of Europe, China and Japan. Any changes in U.S. trade policy could adversely impact this key source of ag revenue.
Looking ahead, Kohl projects that high-equity farms will be the top performers. From 2021 to 2025, he sees the economics shifting to operations that can leverage management and grow.
 
ASFMRA Annual Awards

Carl F. Hertz Distinguished Service Award: Thomas Vilsack, president of the U.S. Dairy Export Council and former Secretary of Agriculture
D. Howard Doane Award: Kirk D. Weih, AFM, Hertz Farm Management,
	Mt. Vernon, Iowa
Farm Managers of the Year, sponsored by Syngenta and AgPro: Timothy M. Cobb, AFM, and Allen B. Hatley, AFM, Hatley/Cobb Farmland Management, Spokane, Wash.
Appraisal Professional of the Year, sponsored by Rabo AgriFinance: Wendell C. Wood, ARA, Kokel-Oberrender-Wood Appraisal, Georgetown, Texas
ASFMRA President David W. GaNun, ARA, Farm Credit East, Lebanon, N.J., and president-elect Timothy E. Fevold, AFM, Hertz Farm Management, Nevada, Iowa 
Early Career Award, sponsored by Monsanto, Skye Root, AFM, ACC, vice president Westchester Group, Boise, Idaho
H. E. Stalcup Excellence in Education Awards: Management Award: James C. Farrell, AFM, CEO of Farmers National Company, Omaha, Neb.; Appraisal Award: Wendell C. Wood, ARA