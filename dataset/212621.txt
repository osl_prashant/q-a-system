TORONTO — At its 92nd annual convention, the Canadian Produce Marketing Association saw two things for the first time: a new attendance record and a non-Canadian take the chairman role.As the first international CPMA chairman, Rick Alcocer, Salinas, Calif.-based senior vice president of fresh sales for Duda Farm Fresh Foods, Oviedo, Fla., said he planned to make no executive orders but he did expect to tweet often.
CPMA CEO Ron Lemaire said the more than 4,000 attendees marked a convention record for the May 9-11 event, but the group isn’t trying to grow bigger every year.
“It’s a community show,” he said. “We have open space. We’re not trying to make it the biggest.”
He said CPMA hosted a wide range of attendees from Canadian retailers and foodservice buyers to retail dietitians to food bloggers to college students interested in careers in the produce industry.
Lemaire also said the May 11 award banquet was the largest ever with about 1,000 attendees who were treated to Canadian rock legends Barenaked Ladies, who noted their produce industry crowd with several fruit and vegetable references.
Attendees, called delegates at CPMA, found the expo as valuable as ever. 
 “Every retailer we interact with in Canada but one, we met with here,” said Kathy Stephenson, marketing communications director for Pear Bureau Northwest, Milwaukie, Ore.,
“The quality of attendee and exhibitor is excellent,” said Jim Richter, president and CEO of Amerifresh, Scottsdale, Ariz. “We continue to see value-added growing.”
“CPMA is a more intimate expo,” said Jeanette De-Coninck-Hertzler, sales manager of Okanagan Specialty Fruits, Summerland, British Columbia. “Retailers really want to engage in a conversation here.”
Next year’s annual meeting will be in Vancouver, British Columbia, April 24-26.