Farms, food and magical stone cottages in Italy's Puglia
Farms, food and magical stone cottages in Italy's Puglia

By CAIN BURDEAUAssociated Press
The Associated Press

VALLE D'ITRIA, Italy




VALLE D'ITRIA, Italy (AP) — There are no famous historical figures to discover in the Valle d'Itria in Italy's central Puglia, no city of note with theaters and great cathedrals. Instead, it's a quiet place of rolling green hills, meandering country roads, endless stone walls, earthy food and wine.
But there is one magical, must-see attraction: stone cottages with conical roofs called trulli, grouped together in the town of Alberobello, a UNESCO World Heritage site.
At its heart, a trullo is a simple farm building, a heap of field stones piled on top of each other, topped with a stone cone roof, called the candela. The structures have a fairytale quality to them, often enhanced by mystical symbols painted on the candelas that scholars trace to Jewish, Christian and pagan origins. Decorative pinnacles cap the roofs, shaped like spheres, disks, stars and crosses.
Nicola Loperfido, a 36-year-old craftsman, was making replicas of trulli in his workshop as he tried to explain their appeal. The circular shape evokes the cycles of life and seasons for a farmer. This circularity is expressed in other ways too: Old trulli villages had a circular central piazza called iazzile. The trullo, he said, symbolizes "the integration humans had with their land."
LAND AND STONE
The pleasure of the Valle d'Itria lies in timeless things: seasonal changes, the play of light on paddocks and olive orchards, the cascade of brilliantly pink almond blossoms in the spring, the mist and fog of winter, the solitary farmer working a field, bird song.
A visitor can go for hours, days even, just observing fields of durum wheat, legumes, olives, almonds, patches of chicory, tomatoes, zucchini, bell peppers, spinach, broccoli rabe; walking long stretches of stone walls girding red-colored fields; stopping at altars on the sides of roads in an endless maze of old sheep tracks called i tratturelli.
There's a strange geology afoot here too. The Valle d'Itria is one of the world's rare karst zones, consisting of layers of limestone a foot beneath the surface. To reach water, wells must be dug 350 meters (1,150 feet) or more. As an alternative, water has been collected in cisterns and ditches since ancient times.
Despite the rocky ground, farming is not just doable, it thrives. The limestone acts as a sponge, farmers say. The valley also enjoys, and suffers, from high humidity. It's good for agriculture, but can make daily life unpleasant, with water dripping down the inside walls of homes and a feeling of dampness for much of the year.
FOOD AND LODGING
People settled here centuries ago, fleeing the coasts, conflict and incursions by Saracens, Goths and others. Beginning in Roman times, the valley was divided into large estates given to Roman soldiers for their service. This system helped turn the area into one of Italy's most valued agricultural regions with distinctive olive oil, red and white wines and other products. Some of the region's foods are world-famous, like burrata cheese, capo collo meat sausage, fava bean dishes, almond biscuits, digestive liqueurs called rosoli and dry breads called frise and taralli.
"The vegetables and fruit have a better taste than anywhere else because of the goodness of the land," said Gennaro Santoro, a 75-year-old winemaker.
Agriturismos and masserias — farms that offer lodging — have sprouted up. Restaurants and roadside inns serve delicious country fare. And strolls through town centers are perfumed with the smell of food wafting out from kitchens.
ARCHITECTURE WITHOUT ARCHITECTS
The Valle d'Itria is a stupendous example of Mediterranean architecture with whitewashed buildings and curvaceous lines. But it's the genius of the stonework and trulli that's truly distinctive here.
Limestone structures are ubiquitous: castles, town walls, government edifices, churches. They were mostly built without mortar and often without lumber. Home walls are commonly 2 meters (6.5 feet) thick.
"A great masterpiece of architecture without architects," commented the Japanese architectural historian Hidenobu Jinnai in describing Cisternino, one of the valley's more picturesque hilltop towns.
"From the Adriatic to Ireland you find these kinds of structures," explained Leonardo Antonio Palmisano, an architect who specializes in trulli, as he oversaw work to incorporate an abandoned trullo into a new modern home near Locorotondo, home to the region's master trullo builders.
The most famous trullo is in Alberobello, the Trullo Sovrano. Today it is a museum.
Traditionally, trulli were simple homes for farmers and served agricultural purposes: a place to make wine, store tools and animals. They eventually were largely abandoned, considered too cold and damp to live in. But these days many have been bought and turned into modern homes.
Others lie in ruins throughout the valley, falling down and overrun by weeds and trees. Even in a dilapidated state, their carefully laid walls and pinnacles seem magical.