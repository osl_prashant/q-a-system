The Michigan Asparagus Advisory Board, Dewitt, Mich., conducted consumer and trade research through an online survey of 500 consumers from markets from the Midwest to the East Coast.
“The information was very useful to help us become more strategic in how we spend our marketing funds, to target key customers” John Bakker, MAAB executive director, said in a news release. “But more importantly, we provided the results to our members who, in turn, are using this to educate retailers on consumer purchase behavior and usage to maximize category returns.”
Consumers surveyed were primary household shoppers who purchased asparagus in the past 12 months.
Highlights of the research results are:

A third of consumers are purchasing more asparagus compared to the previous year;
64% purchase asparagus monthly or more;
more than half (53%) purchase asparagus year-round;
87% said health/nutritional benefits of asparagus positively affect their decision to buy
76% are more likely to purchase USA grown asparagus versus imports, with 55% willing to pay more for USA grown.

“Not only was the consumer information valuable, but the feedback from buyers provided us with important insights on the type of resources and expectations that they have for our product and suppliers.” Bakker said in the release. “Growing the category and raising awareness for Michigan asparagus takes the work of all involved and our goal is to implement their feedback to get them the content they need to help us drive sales.”
The full findings can be accessed at www.michiganasparagus.com.