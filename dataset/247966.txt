When producers spot a sick animal, they often begin to closely monitor water and feed intake. That instinct is right on target based on what we know about animals’ immune systems. While seemingly simple, these tasks help support a diverse community of microorganisms found in all animals, including humans, called the microbiome.
“The microbiome is a collection of different microorganisms that we find in the intestinal tract of animals, and we know these microorganisms are essential to immune development,” says Christopher Chase, DVM, Ph.D., professor in the Department of Veterinary and Biomedical Sciences at South Dakota State University. “Understanding how they interact with the gut, and particularly the epithelium of the gut, is really important.”
In the last five to 10 years, researchers have shown the epithelial cells of the gut and respiratory tract act as an “immune organ.”
“This means the cells are not just there to absorb and secrete, but also pick up signals — particularly signals from the microbiome,” Dr. Chase says.
The epithelial mucosa fights infection through four major ways:

Produce mucus to act as a barrier.
Create antimicrobial peptides, which are the body’s own antibiotics.
Produce IgA, the largest antibody produced in the body, which is not produced by epithelial cells, but it is exported and partially regulated in epithelial cells.
Formation of tight junctions so bacteria and other metabolites can’t get through.

As inflammation increases, these tight junctions begin to break down, which results in leaky gut.
When the intestinal tract leaks, bacteria and other foreign matter can get past the barrier. This causes an inflammatory immune response. The result can often be too robust and cause damage in other parts of the body.
“The other thing we know about the mucosa of the gut is that it’s related to adaptive immunity,” Dr. Chase says. “Long-term immunity is called adaptive immunity. In the gut, adaptive immunity is localized — it’s just in that particular area. It does interact. We’ve been able to measure the same response seen in the gut also in the lungs or reproductive tract.”
This research and insight can help producers manage the innate immunity within the animal. Potential ways to manage the microbiome include feeding probiotics and prebiotics; using nonsteroidal anti-inflammatory drugs (NSAIDs) to manage inflammation prior or during stress; and ensuring adequate hydration to help create sufficient mucus; and nutrition to fuel the animal’s natural immune response.
Some types of probiotics and prebiotics can help support gut health. In addition, these additives may also help send signals to the epithelium to maintain the anti-inflammatory response, Dr. Chase says.
“As we look at managing immunity, it’s important that we think about feeding probiotics and prebiotics that will help us with health, managing hydration and having good intakes,” he says. “If we do all those things, we’re going to help manage disease and the inflammatory response.”
Learn more about the microbiome from Dr. Chase by watching this video.