Dennis Lindberg has grown rice in Butte County for more than 75 consecutive years.In this video one notable quote is, "The best treatment a crop can have is the shadow of the man who owns it."

Dennis blogs for California Rice, and you can read all of his posts here.