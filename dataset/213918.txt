HAB had an illustrator draw people in an avocado-themed scene at a recent nutrition conference.

The Hass Avocado Board participated in the Academy of Nutrition and Dietetics’ Food & Nutrition Conference & Expo, giving presentations on the health benefits of avocados, providing recipe samples and promoting its #TeamGoodFat partnership with the California Walnut Commission.
“Each year, FNCE offers an excellent opportunity to engage with influential healthcare professionals who directly impact how Americans manage their overall health and wellness, especially when it comes to their diet,” HAB executive director Emiliano Escobedo said in a news release. “We were thrilled to return this year and share the results and advances of our nutrition research program.”
HAB nutrition director Nikki Ford led several educational sessions at the conference, covering topics including heart health and weight management.
The organization also had Miami fashion illustrator Gissi Jimenez on hand to draw attendees in an avocado-themed scene, according to the release.
HAB also spoke about its efforts with the California Walnut Commission to stir conversation about the role good fats can have in healthy diets.
The event wrapped up Oct. 24 in Chicago.