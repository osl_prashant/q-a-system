Ex-Clinton secretary to run for Mississippi US Senate seat
Ex-Clinton secretary to run for Mississippi US Senate seat

By JEFF AMYAssociated Press
The Associated Press

JACKSON, Miss.




JACKSON, Miss. (AP) — A Mississippi Democrat who was President Bill Clinton's first agriculture secretary officially announced his candidacy for U.S. Senate on Friday, saying he'll run in a special election to finish the term started by longtime Republican Thad Cochran.
Mike Espy, also Mississippi's first African-American elected to Congress since Reconstruction, had already said he had a "strong intention" to run.
The 64-year-old Espy, who lives in the Jackson suburb of Ridgeland, said in a statement released by his campaign that, like Cochran, he wants to be a "calming voice" that appeals to all Mississippians.
"It is in this same spirit that I offer my candidacy — to rise above party and partisan wrangling in an effort to appeal to all Mississippians — as we unite to show the nation, at the end of this second decade of the 21st century — just how far we have come," Espy said in the statement.
Cochran, 80, retired Sunday, citing poor health. Gov. Phil Bryant appointed fellow Republican Cindy Hyde-Smith to temporarily succeed Cochran. She will be sworn in Monday and will run in a special election in November.
Republicans are trying to maintain their slim Senate majority in this year's midterm election. Though Mississippi is a conservative state, Democratic leaders are pointing to their party's victory in a special U.S. Senate election in Alabama in December as proof that the party can compete in the solidly Republican South. The last time Mississippi had a Democrat in the Senate was in January 1989, when John C. Stennis retired.
The special election winner will serve until January 2021. Besides Espy and Hyde-Smith, two other candidates have announced: Republican state Sen. Chris McDaniel of Ellisville and Democratic Tupelo Mayor Jason Shelton. Candidates won't be identified by party on the ballot, but they are allowed to tell voters their political affiliation.
Other candidates could also emerge before the April 24 qualifying deadline. Republican Andy Taggart and Democrat Jamie Franks have both ruled out candidacies in recent days.
Espy in 1986 became the first African-American in modern times to win a congressional seat in Mississippi, and he has publicly supported both Democrats and Republicans in various races. Born in Yazoo City, Espy is a member of a prominent family that built a chain of funeral homes across Mississippi's Delta region, giving them contacts in African-American communities. Espy also appealed to white farmers who underpin the region's struggling economy, and that agricultural focus led Clinton to name Espy to his Cabinet. Espy was indicted for illegally accepting gifts and favors while secretary and trying to cover it up, but was later acquitted at trial of all charges. Jurors said prosecutors never proved Espy had any criminal intent or delivered any favor as a result of the gifts.
Since the verdict, Espy has worked as a lawyer. He would be the third African-American to serve as a U.S. senator from Mississippi. Hiram Revels and Blanche Bruce both served during Reconstruction in the years after the Civil War.
Mississippi also has a regularly scheduled election this year for a six-year Senate term. Republican incumbent Roger Wicker, who has been in the Senate since late 2007, faces a political newcomer, Richard Boyanton of Diamondhead, in the June 5 primary.
Running in the Democratic primary on the same day are state Rep. David Baria of Bay St. Louis; Jensen Bohren of Benton; Victor G. Maurice of Pass Christian; Jerone Garland of Kosciusko; state Rep. Omeria Scott of Laurel; and Howard Sherman of Meridian.
___
Follow Jeff Amy at: http://twitter.com/jeffamy . Read his work at https://www.apnews.com/search/Jeff_Amy .