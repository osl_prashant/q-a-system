BC-Orange-juice
BC-Orange-juice

The Associated Press

NEW YORK




NEW YORK (AP) — Frozen concentrate orange juice trading on the IntercontinentalExchange (ICE) Monday:
OpenHighLowSettleChg.ORANGE JUICE15,000 lbs.; cents per lb.May137.75139.00136.30138.50+1.05Jun139.15+.70Jul138.60139.50137.25139.15+.70Aug140.05+.75Sep139.15140.05138.15140.05+.75Nov139.75141.10139.20140.95+.75Jan140.00141.95140.00141.75+.75Feb142.20+.75Mar142.20+.75May142.95+.75Jul143.05+.75Sep143.15+.75Nov143.25+.75Jan143.35+.75Feb143.45+.75Mar143.45+.75May143.55+.75Jul143.65+.75Sep143.75+.75Nov143.85+.75Jan143.95+.75Feb144.05+.75Mar144.05+.75Est. sales 797.  Fri.'s sales 1,008Fri.'s open int 13,737,  up 28