Following is a snapshot of key data from this morning's USDA Crop Production and Supply & Demand Reports:




 


Yield -- in bu. per acre





 

USDA
 


Avg.


Range


USDA Sept.




Corn


171.8


170.1


168.7-171.5


169.9




Soybeans


49.5


50.0


49.1-52.1


49.9




 




 


Production -- in billion bu.





 

USDA


Avg.


Range


USDA Sept.




Corn


14.280


14.204


14.060-14.355


14.184




Soybeans


4.431


4.447


4.335-4.490


4.431




 




 


2016-17


2017-18






U.S. Carry


USDA


USDA
			Sept. 


USDA


Avg.


Range


USDA
			Sept.




in billion bushels (cotton million bales)




Corn


2.295


2.350


2.340


2.289


2.168-2.450


2.335




Soybeans


0.301


0.345


0.430


0.447


0.375-0.500


0.475




Wheat


--


1.184


0.960


0.946


0.928-0.971


0.933




Cotton 


2.75


2.75


5.80


5.70


5.20-6.30


6.00




 
 




Global Carryover


2017-18
October


2017-18
September


2016-17
October


2016-17
September




Million Metric Tons
 





Corn

200.96


202.47


226.99


226.96



Wheat 

268.13


263.14


256.58


255.83



Soybeans 

96.05


97.53


94.86


95.96



Meal 

12.42


12.88


12.89


13.32



Soyoil 

3.51


3.63


3.54


3.66



Cotton (mil. bales)

92.38


92.54


89.57


89.57




 




Global crop in MMT


2017-18
October


2017-18
September


2016-17
October


2016-17
September




Wheat



U.S.

47.37


47.33


62.83


62.86



Australia

21.50


22.50


33.50


33.50



Canada

27.00


26.50


31.70


31.70



EU

151.04


148.87


145.47


145.43



China

130.0


130.0


128.85


128.85



FSU-12

138.27


137.27


130.74


130.74




Corn



U.S.

362.73


360.30


384.78


384.78



Argentina

42.0


42.0


41.0


41.0



Brazil

95.0


95.0


98.50


98.50



EU

59.39


59.39


61.09


60.71



Mexico

26.20


26.0


27.57


27.4



China

215.0


215.0


219.55


219.55



FSU-12

46.55


47.75


47.36


47.36




Soybeans



U.S.

120.58


120.59


116.92


117.21



Argentina

57.0


57.0


57.8


57.8



Brazil

107.0


107.0


114.10


114.0



China

14.20


14.0


12.9


12.9




Cotton (mil. bales)



U.S.

21.12


21.76


17.17


17.17



Brazil

7.80


7.50


7.00


7.00



India

30.00


30.00


27.00


27.00



China

24.5


24.5


22.75


22.75



Pakistan

9.15


9.15


7.7


7.7




Pre-report expectations compiled by Reuters and Bloomberg.