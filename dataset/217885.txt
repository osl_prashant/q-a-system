The Environmental Protection Agency is pleased to announce that up to $3 million in funding for locally-focused environmental education grants will be available this week under the 2018 EE Local Grant Program. EPA will award three to four grants in each of EPA’s ten regions, for no less than $50,000 and no more than $100,000 each, for a total of 30-35 grants nationwide. Proposals are due March 15, 2018. The Requests for Proposals will be posted on www.grants.gov later this week.

In addition to other environmental topics, the 2018 EE Local Grant Program includes support for projects that reflect the intersection of environmental issues and agricultural best-practices, conservation of natural resources, food waste management, and natural disaster preparedness.

Funded projects will increase public awareness of those topics and help participants to develop the skills needed to make informed decisions. A Request for Proposals (also called a Solicitation Notice) containing details will be issued by each of the ten EPA Regions.

"By recognizing these locally-based learning and awareness opportunities, the Environmental Protection Agency is taking both a local and national leadership role in promoting sound agricultural conservation practices, environmental disaster preparedness, adequate food waste management and other important environmental best-practices," said Administrator Scott Pruitt. "Environmental education starts locally in our own backyards, classrooms and in the fields of farmers who work the land directly, and I'm proud to play a role in enhancing such learning opportunities."

Through this grant program, EPA intends to provide financial support for projects that design, demonstrate; and/or disseminate environmental education practices, methods, or techniques, as described in this notice, that will serve to increase environmental and conservation literacy, and encourage behavior that will benefit the environment in the local community/ies in which they are located.

Since 1992, EPA has distributed between $2 million and $3.5 million in annual grant funding under this program, supporting more than 3,700 grants.

Proposals are due by March 15, 2018. The full solicitation notices will be posted later this week at www.grants.gov and at http://www.epa.gov/education/environmental-education-ee-grant-solicitation-notice.

Find background on the EE Grants Program and resources for applicants at http://www.epa.gov/education/environmental-education-ee-grants.