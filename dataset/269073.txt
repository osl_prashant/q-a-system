BC-US--Coffee, US
BC-US--Coffee, US

The Associated Press

New York




New York (AP) — Coffee futures trading on the IntercontinentalExchange (ICE) Friday:
(37,500 lbs.; cents per lb.)
Open    High    Low    Settle     Chg.COFFEE CMay                              119.50  Down  .50May      118.00  118.95  117.10  117.30  Down  .60Jul                              121.55  Down  .45Jul      120.05  121.10  119.20  119.50  Down  .50Sep      122.30  123.15  121.25  121.55  Down  .45Dec      125.55  126.50  124.70  124.90  Down  .40Mar      128.85  129.85  128.15  128.40  Down  .35May      131.15  132.00  130.50  130.75  Down  .35Jul      133.20  134.05  132.65  132.90  Down  .30Sep      135.05  135.50  134.55  134.80  Down  .25Dec      138.20  138.20  137.30  137.60  Down  .15Mar      140.95  140.95  140.05  140.35  Down  .15May      142.70  142.70  141.80  142.15  Down  .15Jul      144.35  144.35  143.45  143.85  Down  .15Sep      145.95  145.95  145.00  145.50  Down  .20Dec      148.35  148.35  147.40  148.00  Down  .20Mar                              150.15  Down  .20