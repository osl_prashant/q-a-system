Manure has value. That value may result from improvements in soil quality, increases in yield, and replacement of commercial nutrient required for crop production. Previous articles on manure’s value have focused on its soil health, environmental benefits, and tools for estimating manure’s value. This article will focus on the economic benefits of manure. Key take home messages include:
1) Targeting fields requiring supplemental phosphorus (P) produces the largest economic value from manure. 
2) Targeting fields requiring supplemental potassium (K) significantly increase manure’s value. 
3) Additional value result from manure nitrogen (N) and micro-nutrients as well as from yield increases. However, these benefits are typically less important than P and K.

The assumptions used for this analysis are found at the end of Figure 4.

Nitrogen and Phosphorus Value

Manure is a supplemental source of P, organic-N, ammonium-N, and micro-nutrients commonly required by many fields. Cropland receiving surface applied manure (not incorporated) benefits from both the organic-N and P. The value of the nutrients in beef manure (open lot) is heavily influenced by the value of the P and to a lesser extent the organic-N (see Figure 1 and assumptions for all figures presented. All assumptions are found at end of article. ). Because feedlot manures and many solid manures contains little ammonium-N, incorporation to conserve N would produce little additional value.


For slurry manures such as captured by a beef barn with a pit below a slatted floor, immediate incorporation of manure is important for gaining value from the important ammonium-N content. Slurry manures generally conserve the ammonium-N fraction commonly lost from open lots. Figure 2 illustrates the value of incorporating beef manure with a below barn pit. Approximately 35% of its value results from the ammonium-N conserved by direct injection.


Note the importance of P to achieving value from both of these manures. Almost half or more of each manure’s value will only be realized by applying manure to fields requiring P supplementation (typically, fields with Bray soil P levels below 30 ppm). Thus, farmers wanting to gain the greatest value from manure should target those fields with low soil P levels. A 25 ton load of open lot beef manure has a fertility value of $350. However, 2/3 of this value will not be realized if applied to a field with high soil P levels. 

Potassium Value

To further enhance the value of manure, targeting those fields that have a K requirement offers additional value. Soil tests for highly productive fields are increasingly identifying a need for K supplementation. Manures are an excellent source of K. For the beef feedlot manure example shared in Figure 1, the manure’s value has almost doubled by applying it to a field with a K requirement (Figure 3).


Yield Response

Economic value can also be gained from a yield response to manure. Such yield responses can be a result of improved soil structure and greater drought tolerance of the soils receiving manure or from the increased biological activity in the soil producing a number of benefits such as greater nutrient availability to the plant. A recent worldwide literature review of 159 research comparisons of the nutrient replacement value of manure observed an average yield increase of 4.4%. Adding a 5% yield increase to a 200 bushel/acre corn crop will produce some additional value. However, note that this yield boost does not compare with the value of the P and K in manure (Figure 3) assuming a 5% yield increase is achieved. 

Similar benefits are observed for manures produced in other beef systems houses in open lots, bedded back barns, and barns with a deep pit as illustrated in Figure 4. 

Keys to Manure Value

Key to gaining the economic value from manure nutrients is the rate at which manure is applied. To receive the returns shown in this articles graphics, the following practices must be followed:
1. Manure should be applied at a rate that does not exceed the crop N requirements for a single year. Excess manure N application is likely to be leach beyond the root zone and be lost. See Determining Crop Available Nutrients from Manure or more information.
2. Manure applied at rates near the crops N requirement typically over apply P and K. However, these nutrients will continue to be available to crops in future years. To gain the manure’s P and K value, target those fields requiring supplemental P and K (see Nutrient Requirements for Agronomic Crops in Nebraska or your state specific recommendations). In addition, avoid re-applying manure to the same field until soil testing suggests need for supplemental P and K. 

Accessing the economic value of manure begins by targeting fields low in P and K.