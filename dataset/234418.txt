Steve Flies of Plainview, Minn. was looking for a 70 Wheatland Oliver, and found one in Iowa.

His brother, John, went to Dean Eriksson’s farm and found that Eriksson was selling his 15 tractors.

“We ended up buying them all,” said Steve. “We were just getting in the collecting, and that was our big purchase.”

One of the tractors in that collection was a 1965 Allis-Chalmers D21 that has come in 3rd and 4th places in some area tractor rides.

Watch his story on U.S. Farm Report above.