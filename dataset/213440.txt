John Shuman (right), president and owner of Shuman Produce, accepts The Packer’s Produce Marketer of the Year award from Editor Greg Johnson.
 
(UPDATED OCT. 26) NEW ORLEANS — John Shuman, president and owner of Shuman Produce, was honored as The Packer’s Produce Marketer of the Year.
Family-owned Shuman Produce is one of the largest shippers of Vidalia onions and year-round onions from Peru, Mexico, Texas and New Mexico, under the RealSweet brand, and it also offers sweet potatoes and vegetables.
Shuman was honored for his leadership in the category with innovative packaging, branding and in-store marketing.
“One of our biggest accomplishments is being able to grow within our core values, in a way that stays true to a family-owned business,” Shuman said after receiving the award Oct. 20 at PMA Fresh Summit in New Orleans.
“Innovative marketing is one thing we try to put our special touch on,” he said. “We learned a long time in order to separate yourself in the marketplace you gotta be more more than about how many acres you’re growing. You have to be able to say ‘Here’s how I can help change your business.’”
Shuman also founded the charity Produce for Kids, which just celebrated its 15th year, having donated more than $6 million to children’s hospitals and charities, with the help of many retail chains and produce company partners.
“We never expected it to be as successful as it’s become,” he said.
Jay Schneider, produce director of Acme Markets, Malvern, Pa., an Albertsons chain, said he’s done business with Shuman for more than 20 years and considers him one of the most likeable and honorable business partners.
“John is one of the few guys who doesn’t take his business for granted,” Schneider said. “He provides outstanding customer service.”
He said Shuman Produce works with Acme to provide strong point-of-sale material with high-graphic bags and boxes.
“In the spring, customers see his picture on the side of bins and they see his story and feel comfort in that,” Schneider said.
This spring Shuman Produce purchased the Vidalia onion operation that previously belonged to Plantation Sweets, including the 680-acre property and 94,000-square foot packing facility.
“We want to be in a position to take advantage of opportunities down the road,” he said about the acquisition. 
Shuman is the 46th Marketer of the Year, winning an award The Packer has given at Fresh Summit since 1972.