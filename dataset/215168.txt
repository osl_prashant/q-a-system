U.S. lean hog futures climbed to a three-week high on Tuesday, extending gains from recent days on speculation prices would rise higher next month and on calendar spreading, traders and analysts said.
Live cattle were narrowly lower and feeder cattle futures slightly higher at the Chicago Mercantile Exchange, in technically driven trade as each cattle contract failed to surpass Monday's peaks.
Demand for cuts such as pork hams and beef steaks and loins typically rises in December, buoying wholesale meat prices and futures for hogs and cattle.
Hog futures have increased in five out of the past seven sessions on bets the market will see those seasonal gains. Most-active CME February hogs settled up 1.425 cents at 71.525 cents per pound while December hogs were up 1.225 cents to 65.750 cents.
"There's some people buying into this for the near-term," Steiner Consulting Group analyst Altin Kalo said of hogs.
Lean hogs in the top cash market of Iowa and southern Minnesota were up $1.16 to $58.23 per cwt and wholesale pork fetched $82.23 per cwt, down 35 cents, according to U.S. Department of Agriculture data.
Choice-grade wholesale beef prices were down 94 cents to $208.63 per cwt, USDA said.
CME February live cattle were down 0.375 cents to 125.675 cents per pound and December cattle off 0.175 cent to 119.650 cents.
CME January feeder cattle were up 0.050 cent to 154.500 cents per pound.
Commodity Futures Trading Commission data released after the close of futures trading on Monday showed speculative investors cutting net long positions in lean hogs, live cattle and feeder cattle in the week ended Nov. 21.