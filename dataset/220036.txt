What were pineapples like before the Civil War?
That’s a question I’ll admit I never thought to ask, but it’s one answered (in part) in Susan Bright’s fascinating book “Feast for the Eyes: The Story of Food in Photography.”
Bright arranges her discussion by decade, starting off with an 1845 photo by British scientist William Henry Fox Talbot called “A Fruit Piece.” 
In it, apples, peaches and a couple of plums nestle in baskets atop a tartan-clothed table — the apples are smaller than our typical modern grocery store beauties, but otherwise the fruit could be the contents of any contemporary family’s countertop bowl in the transition from late summer to autumn.
 


 
What really arrests the attention, though, is the giant pineapple lurching out of the apple basket. If you squint you can tell that the leaves in the crown have more toothy edges than the gold pineapples we’re all used to now (they remind me of the baby pineapples sold by Frieda’s Inc. and World Variety Produce), but that’s the only externally visible difference from modern varieties.
Today, pineapple is ubiquitous; it comes on everything from fresh-cut fruit trays to frozen pizza, and, while it’s not the cheapest fruit in the produce aisle, it’s not something only the exceptionally well-heeled can afford. 
At the time when Talbot composed his photograph, however, pineapples were a symbol of wealth, since they were either imported or grown in private hothouses, and were even a source of rivalry between wealthy aristocrats with a passion for growing the fruit, according to Fran Beauman’s book “The Pineapple: King of Fruits.” 
That made me wonder: Where did Talbot get the pineapple in his photograph? Did he buy it at great expense to use again and again as a prop, or was it a gift from a wealthy friend or patron? Did he cut it open to see what it looked like inside — and did he eat it? What did it taste like?
Pineapples crop up frequently in Bright’s work, in still lifes from the 1860s and 1880s, gracing an upside down cake in a McCall’s magazine feast from 1948, and in the next-to-last image in the book, a 2011 piece by Daniel Gordon called “Pineapple and Shadow” in which the pineapple and its “shadow” are crafted from cut-out magazine photos.
For the photography buff, Bright’s book offers creative inspiration. For the foodie or food historian, it gives some insight into how our relationship with food has changed over the past 175 years or so (and how it’s remained the same). 
And for those passionate about produce, it provides an intriguing glimpse of what some fruits and vegetables looked like before we had the world’s bounty available year-round at the grocery store down the street.
It makes me want to go buy a pineapple.
Amelia Freidline is The Packer's copy chief and Opinion page editor. E-mail her at afreidline@farmjournal.com.