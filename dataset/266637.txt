BC-NC--North Carolina News Digest, NC
BC-NC--North Carolina News Digest, NC

The Associated Press



Hello! The interim Carolinas News Editor is Jeffrey Collins. The supervisor is Jonathan Drew.
A reminder that this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with updates.
TOP STORIES:
HOG SMELLS-LAWSUITS
RALEIGH — Drive past clustered hog sheds containing thousands of animals in the country's No. 2 pork-producing state on the wrong day and the reason hundreds of North Carolina neighbors are suing in federal court is clear: it really stinks. The question facing jurors in a federal lawsuit starting Monday is whether the open-air animal waste pits that proliferated over a generation generate intense smells, clouds of flies and noise. By Business Writer Emery P. Dalesio.  SENT: 750 words, photos.
FATAL POLICE SHOOTING-NORTH CAROLINA
WINSTON-SALEM — A white North Carolina police officer shot and killed a black passenger who displayed a gun after a late-night traffic stop led to a physical struggle, authorities said Saturday. SENT: 350 words, photo.
EXCHANGE-SCHOOLS-ARMED OFFICERS
NAGS HEAD — Dare County is one of just four districts among North Carolina's 115 to have a resource officer in every school, said Michael Anderson, community development and training manager for the N.C. Center for Safer Schools, part of the state's Department of Public Instruction. Expense, district size and lack of resources often prevent placing officers in elementary schools, he said. By Jeff Hampton of The Virginian-Pilot. An AP Member Exchange. SENT: 700 words moved in advance for use in Sunday editions.
IN BRIEF:
— FATAL WRECKS-CLOSURE — Two people were killed in a series of collisions that closed a North Carolina interstate for several hours. SENT
— UNC WILMINGTON-CHINA — UNC Wilmington says it has been approved to establish a dual degree program between the school and a university in China. SENT
SPORTS:
BKN--Hornets-Wizards
WASHINGTON — The Washington Wizards host the Charlotte Hornets. UPCOMING: 650 words, photos from 3 p.m. tipoff.
___
If you have stories of regional or statewide interest, please email them to apraleigh@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, (statephotos@ap.org) or call 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
The AP, Raleigh