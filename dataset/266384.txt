Corn and soybean acreage are both down in USDA's Prospective Planting report, and beans take the crown at 89 million estimated acres. 
 
AgDay national reporter Betsy Jibben is traveling across Eastern Nebraska and Western Iowa and talking with farmers about their acreage mix. Most say their rotation plan will stay in place.
 
Jibben talks with Mike Schropp, a farmer from Crescent, Iowa; Mike Maguire, a Logan, Iowa farmer and Luke Lauristen, a producer from Arlington, Nebraska.