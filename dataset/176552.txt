USDA Weekly Export Sales Report
			Week Ended April 27, 2017





Corn



Actual Sales (in MT)

Combined: 795,700
			2016-17: 771,600
			2017-18: 24,100



Trade Expectations (MT)
700,000-1,100,000


Weekly Sales Details
Net sales of 771,600 MT for 2016-17 were down 22% from the previous week and 15% from the prior 4-week average. Increases were reported for Japan (184,800 MT, including 51,700 MT switched from unknown destinations and decreases of 5,800 MT), Mexico (153,100 MT, including decreases of 23,800 MT), South Korea (123,600 MT, including decreases of 1,400 MT), Saudi Arabia (73,600 MT, including 70,000 MT switched from unknown destinations), and Taiwan (73,400 MT). Reductions were reported for unknown destinations (80,200 MT). For 2017-18, net sales of 24,100 MT were reported for Peru (15,000 MT) and unknown destinations (9,200 MT). Reductions were reported for Panama (100 MT). 


Weekly Export Details
Exports of 1,226,100 MT were down 11% from the previous week and 10% from the prior 4-week average. The primary destinations were Japan (390,900 MT), Mexico (229,500 MT), South Korea (189,000 MT), Peru (75,100 MT), and Saudi Arabia (73,600 MT).


Comments and Performance Indicators
Sales met expectations. Export commitments for 2016-17 are running 37% ahead of year-ago compared to 38% ahead the week prior. USDA projects exports in 2016-17 at 2.225 billion bu., up 17.2% from the previous marketing year.




Wheat



Actual Sales (in MT)

Combined: 821,800
			2016-17: 258,400
			2017-18: 563,400



Trade Expectations (MT)
200,000-550,000 


Weekly Sales Details
Net sales of 258,400 metric tons for delivery in marketing year 2016-17 were up noticeably from the previous week, but down 30% from the prior 4-week average. Increases were reported for the Philippines (66,900 MT, including 57,000 MT switched from unknown destinations and decreases of 2,000 MT), Thailand (52,000 MT), Nigeria (32,600 MT, including decreases of 900 MT), Malaysia (30,700 MT, switched from unknown destinations), and Mexico (29,800 MT, including decreases of 7,100 MT). Reductions were reported for unknown destinations (113,200 MT), Costa Rica (15,200 MT), Leeward Windward Islands (600 MT), and Barbados (500 MT). For 2017-18, net sales of 563,400 MT were reported primarily for unknown destinations (157,000 MT), Algeria (90,000 MT), South Korea (76,400 MT), and China (63,000 MT).


Weekly Export Details
Exports of 622,900 MT were up 2% from the previous week, but down 2% and from the prior 4-week average. The destinations were primarily the Philippines (164,900 MT), Japan (115,700 MT), Mexico (98,500 MT), Nigeria (65,100 MT), and Guatemala (34,300 MT).


Comments and Performance Indicators
Sales were well above expectations. Export commitments for 2016-17 are running 39% ahead of year-ago versus 39% ahead the week prior. USDA projects exports in 2016-17 at 1.025 billion bu., up 32.3% from the previous marketing year.




Soybeans



Actual Sales (in MT)

Combined: 331,300
			2016-17: 318,500
			2017-18: 12,800



Trade Expectations (MT)
400,000-800,000 


Weekly Sales Details
Net sales of 318,500 MT for 2016-17 were down 57% from the previous week and 25% from the prior 4-week average. Increases were reported for the Netherlands (144,800 MT, including 66,000 MT switched from unknown destinations), Indonesia (88,200 MT, including 68,000 MT switched from unknown destinations and decreases of 4,400 MT), China (69,600 MT), Bangladesh (55,000 MT), and the Philippines (27,100 MT, including 25,000 MT switched from unknown destinations). Reductions were reported for unknown destinations (99,000 MT), Mexico (16,200 MT), and Costa Rica (14,700 MT). For 2017-18, net sales of 12,800 MT were reported for Japan (6,000 MT), unknown destinations (5,500 MT), South Korea (1,300 MT). 


Weekly Export Details
Exports of 640,400 MT were down 9% from the previous week, but up 2% from the prior 4-week average. The destinations were primarily the Netherlands (144,800 MT), China (137,500 MT), Mexico (120,000 MT), Indonesia (92,900 MT), and the Philippines (39,400 MT). 
 



Comments and Performance Indicators
Sales were a bit lighter than expected. Export commitments for 2016-17 are running 23% ahead of year-ago, compared to 24% ahead the previous week. USDA projects exports in 2016-17 at 2.025 billion bu., up 4.6% from year-ago.




Soymeal



Actual Sales (in MT)
Combined: 109,900
			2016-17: 103,600
			2017-18: 6,300


Trade Expectations (MT)

50,000-250,000 



Weekly Sales Details
Net sales of 103,600 MT for 2016-17 were down 8% from the previous week and 35% from the prior 4-week average. Increases were reported for Bangladesh (25,000 MT), Honduras (10,000 MT, including 8,700 MT switched from unknown destinations and decreases of 900 MT), Venezuela (10,000 MT), Canada (9,700 MT), and Mexico (9,200 MT, including decreases of 100 MT). Reductions were reported for Guatemala (2,800 MT). For 2017-18, net sales of 6,300 MT were reported for unknown destinations. 


Weekly Export Details
Exports of 231,000 MT were up 35% from the previous week and 14% from the prior 4-week average. The destinations were primarily Colombia (62,300 MT), the Philippines (47,500 MT), Mexico (38,900 MT), Honduras (21,400 MT), and Canada (15,700 MT). 


Comments and Performance Indicators
Sales met expectations. Export commitments for 2016-17 are running 1% ahead of year-ago compared with 2% ahead of year-ago the week prior. USDA projects exports in 2016-17 to be down 0.5% from the previous marketing year.




Soyoil



Actual Sales (in MT)

2016-17: 10,600



Trade Expectations (MT)
0-32,000


Weekly Sales Details
Net sales of 10,600 MT for 2016-17 were up noticeably from the previous week, but down 42% from the prior 4-week average. Increases reported for Mexico (8,700 MT, including decreases of 100 MT), the Dominican Republic (900 MT), Trinidad (700 MT), and Canada (300 MT), were partially offset by decreases for Haiti (100 MT).


Weekly Export Details
Exports of 28,200 MT were down 14% from the previous week and 1% from the prior 4-week average. The destinations were primarily South Korea (23,000 MT), Mexico (4,500 MT), Canada (400 MT), and France (100 MT).


Comments and Performance Indicators
Sales were within expectations. Export commitments for the 2016-17 marketing year are running 11% ahead of year-ago, compared to 11% ahead last week. USDA projects exports in 2016-17 to be up 0.4% from the previous year.




Cotton



Actual Sales (in RB)
Combined: 249,600
			2016-17: 152,400
			2017-18: 97,200


Weekly Sales Details
Net upland sales of 152,400 RB for 2016-17 were up 32% from the previous week, but down 34% from the prior 4-week average. Increases were reported for Vietnam (48,400 RB, including 500 RB switched from Japan and 1,000 RB switched from South Korea), Indonesia (35,200 RB, including 1,200 RB switched from Japan and decreases of 100 RB), Turkey (28,800 RB), South Korea (12,500 RB, including decreases of 2,100 RB), and China (9,600 RB, including decreases of 100 RB). Reductions were reported for Thailand (4,100 RB), Japan (2,400 RB), and Colombia (100 RB). For 2017-18, net sales of 97,200 RB were reported primarily for China (37,400 RB), Vietnam (33,600 RB), Pakistan (8,800 RB), Thailand (6,200 RB), and Taiwan (4,500 RB). 


Weekly Export Details
Exports of 357,300 RB were up 18% from the previous week, but down 8% from the prior 4-week average. The primary destinations were Vietnam (74,300 RB), Turkey (60,700 RB), Bangladesh (35,000 RB), Indonesia (32,100 RB), and China (24,300 RB). 


Comments and Performance Indicators
Export commitments for 2016-17 are 73% ahead of year-ago, compared to 73% ahead the previous week. USDA projects exports to be up 53.0% from the previous year at 14.00 million bales.




Beef



Actual Sales (in MT)

2017: 16,600



Weekly Sales Details
Net sales of 16,600 MT reported for 2017 were down 22% from the previous week and 12% from the prior 4-week average. Increases were reported for Japan (8,000 MT, including decreases of 700 MT), Hong Kong (3,100 MT, including decreases of 100 MT), Taiwan (1,500 MT, including decreases of 100 MT), the Philippines (800 MT), and South Korea (800 MT, including decreases of 200 MT). Reductions were reported for Chile (100 MT) and the Republic of South Africa (100 MT). 


Weekly Export Details
Exports of 12,800 MT were down 7% from the previous week and 6% from the prior 4-week average. The primary destinations were Japan (4,000 MT), South Korea (2,800 MT), Hong Kong (1,600 MT), Mexico (1,500 MT), and Canada (1,100 MT). 


Comments and Performance Indicators
Weekly export sales compare to 21,200 MT the week prior. USDA projects exports in 2017 to be up 6.9% from last year's total.




Pork



Actual Sales (in MT)
2017: 13,800


Weekly Sales Details
Net sales of 13,800 MT reported for 2017 were down 40% from the previous week and 48% from the prior 4-week average. Increases were reported for Mexico (3,500 MT), Canada (2,700 MT), South Korea (1,700 MT), Japan (1,300 MT), and the Philippines (800 MT). For 2017-18, net sales of 300 MT were reported for South Korea. 


Weekly Export Details
Exports of 25,100 MT--a marketing-year high--were up 23% from the previous week and 22% from the prior 4-week average. The destinations were primarily Mexico (8,000 MT), Japan (3,900 MT), South Korea (3,300 MT), China (2,200 MT), and Canada (1,500 MT). 


Comments and Performance Indicators

Export sales compare to a total of 23,000 MT the prior week. USDA projects exports in 2017 to be 8.4% above last year's total.