Seller interest dried up and buyers resurfaced on the dip below $3.80 in December corn futures — roughly the bottom of the sideways trading range dating back to last fall. Similar price action occurred on the drop below $10.00 in November soybean futures. While corn and soybeans ended lower for the week, this price action signals traders still view these as “value” levels given falling crop ratings and a dry 10-day forecast. Wheat futures also posted weekly losses as traders removed more premium from the market despite deteriorating crop conditions and confirmation of major crop losses in the Northern Plains from the Wheat Quality Council spring wheat tour.
Pro Farmer Editor Brian Grete provides weekly highlights:



Cattle ended lower amid the bearish July 21 USDA reports, weaker cash trade and a negative news development with Japanese beef trade. August hogs firmed as traders mildly narrowed the discount the lead contract holds to the cash index. Fall- and winter-month hogs dropped on expectations of a sharp seasonal selloff.
Notice: The PDF below is for the exclusive use of Farm Journal Pro users. No portion may be reproduced or shared.
Click here for this week's newsletter.