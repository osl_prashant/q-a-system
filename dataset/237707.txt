BC-MO--Missouri News Digest 1 pm, MO
BC-MO--Missouri News Digest 1 pm, MO

The Associated Press



Hello! Here's a look at how AP's general news coverage is shaping up in Missouri.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Central.
TOP STORY:
MISSOURI GOVERNOR-INDICTED
ST. LOUIS — Missouri Gov. Eric Greitens will face a May 14 trial on a felony invasion of privacy charge after a St. Louis judge agreed to the date over the objections of prosecutors, who wanted the trial pushed back into the fall in part to allow further investigation. Circuit Judge Rex Burlison had previously set that date as the tentative beginning of a trial on charges that Greitens took an unauthorized photo of a woman with whom he was having an affair during a sexual encounter in the basement of his home in March 2015. A grand jury indicted Greitens last week. By Jim Salter, David A. Lieb And Summer Ballentine. SENT: 600 words.
— MISSOURI GOVERNOR-INDICTED-THE LATEST — The Latest on allegations against Missouri Gov. Eric Greitens.
AROUND THE STATE:
UNIVERSITY OF MISSOURI-RESEARCH FUNDING
COLUMBIA — University of Missouri's chancellor has announced a plan to double the university's funding for research. The Columbian Missourian reports that MU Chancellor Alexander Cartwright announced his plan Tuesday, which also calls for expanding research and creative activities that impact the state and beyond. UPCOMING: 300 words.
IN BRIEF:
— POACHING FINES — The Missouri House has voted overwhelmingly to raise poaching fines to protect the state's fledgling elk population.
— MISSOURI GOVERNOR-INDICTED-THE LATEST — The Latest on allegations against Missouri Gov. Eric Greitens (all times local): By Jim Salter.
— SICK CHIMP — A Missouri zoo says a baby chimpanzee that was born Jan. 9 has fallen ill with a life-threatening viral infection.
— HORSE DRAWN CARRIAGE REGULATION — Missouri cities would be able to impose tougher regulations on horse-drawn carriages but couldn't ban them under legislation that won preliminary approval in the state Senate.
— TYSON-CLEAN WATER VIOLATIONS — Tyson Foods Inc.'s poultry subsidiary has been ordered to pay a $2 million fine for discharges from a southwest Missouri plant that caused a fish kill.
— SPRINGFIELD HOMICIDE — Authorities have arrested a suspect in a homicide at a home east of Springfield.
— CHASE SHOOTING — Authorities say a man has been shot and killed in Kansas City after a crash.
— SPORTS BAR KILLING — A man has been charged with fatally shooting one man and wounding another at a suburban St. Louis sports bar after an argument that one witness said started over the weight of a dog.
SPORTS:
HKN--RED WINGS-BLUES
ST. LOUIS — The struggling St. Louis Blues lost their seventh straight game at Minnesota on Tuesday night. They'll try and end their losing ways when they host the Detroit Red Wings on Wednesday. UPCOMING: 600 words, photos. Game starts at 7 p.m. CT.
FBN--NFL COMBINE
INDIANAPOLIS — Jon Gruden is back from the broadcast booth and the highest-paid coach in NFL history. Unlike his colleague, Matt Patricia didn't balk at leaving Bill Belichick. Andy Reid is in the midst of another major roster shakeup and his protege, Doug Pederson, is basking in Philadelphia's first Super Bowl triumph. All are in Indianapolis at the NFL combine this week hoping to better their chances of winning in 2018. By Pro Football Writer Arnie Stapleton. UPCOMING: 650 words, photos by 6 p.m. ET.
Also:
— ROYALS-DUDA — Lucas Duda and the Royals have agreed to a one-year contract, giving Kansas City a replacement for Eric Hosmer at first base.
___
If you have stories of regional or statewide interest, please email them to apkansascity@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Missouri and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.