You hear these comments all of the time: “I can’t find enough good performers in our local market,” “I have challenging employees who are underperforming and wish they could do more,” or “I’m fearful I can’t find better help even though the team might suffer.”
If any of those sound like comments from you, then first, you’re not alone. Second, it is a nationwide issue throughout all industries. Third, there is hope!
In a talent-deficient market, employers are faced to do more with less. In a recent study, Ag 1 Source looked at how to align, strengthen and maximize personnel given these current economic circumstances.
Making the Best of Your Team Roster
In an ideal market, you would terminate an underperforming employee immediately. Unfortunately, with the unknown impact of the loss to your team, many managers remain frozen. In one of our recent studies, we were tasked to resolve this situation without incurring the expense to replace the underperformer.
Our Approach
1. Determine the attributes of the high-performing employees in the organization.
2. Determine how the attributes and actions of a problematic employee complement or restrict their effectiveness.
3. Gain an understanding of the boss’ style and how that style affects the performance of the team and the performance of the problematic employee.
4.  Analyze the high-performers’ performance trends, and define the organization’s qualifying “must-haves” to hire employees for the organization.
The Data Say It All
The individuals in the charts on the facing page represent the team for this manager. It includes customer service representatives (CSR), IT and inventory control.
The charts represent behavioral tendencies of each employee. The graphic is essentially a map of where employees are most comfortable conducting themselves. A point that is high on the chart indicates a trait that the individual is compelled to demonstrate regularly, and a lower point is not a strength or preferred approach.
For example, an employee who is high in extroversion will undoubtedly be a very outgoing person who seeks conversation, and a person low in extroversion will be the opposite, and he or she will be happiest in an environment with limited interaction with others.
Although the chart represents the behaviors of individuals, it also provides a manager with valuable insight for how to best align, manage and strengthen the effectiveness of each team member. With this information, a supervisor can better understand a person’s preferred method of working and communicating and enable better coaching and more effective communication.
Although each employee is clearly different, it is easy to see similarities in many of the team members, specifically the CSR group. More importantly, it’s even easier to identify an individual who may be quite different from the rest.
In this case, it is easy to see why CSR 4 didn’t seem to fit in with the rest and was failing to perform at par with the rest of his or her team. He or she may be a great person who could perform in another position, but he or she is in the wrong type of role based on his or her style.
Individuals perform best when talents, interests and behaviors align closely with the needs and demands for tasks. For this organization, the ideal CSRs were highest in pace or conformity (columns 3 and 4), which led them to being patient, good listeners, attentive to details, systematic and enjoying following standard procedures.
Compare this to the attributes of CSR 4, the problematic employee. This employee had naturally high traits in dominance and extroversion (columns 1 and 2). Naturally, this person sought to be in control, be more direct, have more decisiveness in his or her actions and want to speak his or her mind. He or she wanted to make the rules rather than conform to them.
Conclusions
The problem employee likely interviewed very well and connected well with others during the hiring process. However, his or her style was not a fit for the type of approach and tasks required for success in this position. 
This simple process enabled the manager to determine this CSR was not aligned with the job, and it allowed him to move this employee to a role that better fit his or her style without investing in more time and training that would have likely given no return.
Had the manager not used this tool, the likely impact on the organization could have been significant, considering:
• The wasted investment in training to educate the employee but not ultimately solve the problem.
• The impact of team morale and the overall performance of the team by keeping this person in that group.
• The inability for the supervisor to perform well due to the need to focus time on the problem employee instead of increasing the company’s performance.
• Lost business due to an underperforming employee.
• The long-term impact of an underperforming employee on the brand image of the organization.
By taking the time to understand the style and behavioral demands suited for each job in your organization, and using this simple, 10-minute tool to uncover the behaviors of your employees or applicants, you can effectively, align, build and maximize any team in your organization. This allows your organization to achieve more with less.
The Agricultural Retailers Association recently partnered with Ag 1 Source to help retailers address their talent challenges through its “Destination Employer” program. Find out how you can apply delivery methods like these. Contact Mike Koenecke at mikekoenecke@ag1source.com or (620) 327-0320 for more information. To access additional information about this program,
go to http://www.aradc.org/ag1source.