City wants to meet with aquaculture experts on pier project
City wants to meet with aquaculture experts on pier project

The Associated Press

SOUTH PORTLAND, Maine




SOUTH PORTLAND, Maine (AP) — Local lobstermen and aquaculture operators have been invited to weigh in on the possible redevelopment of South Portland's pier.
City officials say they see an economic opportunity at the Portland Street Pier, particularly in the field of farming shellfish like mussels and oysters. The Portland Press Herald reports a pier master plan concerning design and assessment is expected to be completed in June.
The Gulf of Maine Research Institute, which is working with South Portland on the project, says a meeting will be held March 19 with fishermen and aquaculture operators at their headquarters in Portland.
The city has owned the Portland Street Pier since the late 1800s, and the pier currently operates as a seasonal fishing pier generating about $20,000 in revenue annually for the city.
___
Information from: Portland Press Herald, http://www.pressherald.com