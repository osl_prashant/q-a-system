I realize that fall calving season has just really gotten started and I am already talking breeding for the next calf crop. However, getting a head start on the breeding season is critical. Just because the bull you used last season worked out just fine does not mean that he will be up to par this year. In addition, if the bull you plan to purchase has not had a breeding soundness exam (BSE), it is highly recommended that you do so. It takes the bull 60 days to produce a new set of spermatozoa, so if there are issues that can be addressed, you will have time for treatment of the bull and a follow up BSE, or time to locate a new bull. 

Failure to evaluate your bulls can result in huge economic losses in the form of a reduced calf crop, or no calf crop period. Yet, performing bull breeding soundness evaluations prior to the breeding season is one of the most neglected reproductive management practices in cow-calf operations. Classification of the bull’s fertility is fertile, sub-fertile, or sterile. Estimation are that about 20% of all bulls are sub-fertile, and while these bulls may eventually get cows pregnant, it will take much longer than fertile bulls to settle a group of cows. The result is that sub-fertile bulls produce calves that are born later, and are therefore younger and lighter at weaning. Sub-fertile bulls also produce fewer calves during a breeding season. In either situation, sub-fertile bulls produce fewer pounds of beef per exposed cow, affecting the economic profitability of a cow-calf operation.
A bull breeding soundness evaluation (BSE) is a uniform method of assessing a bull’s likelihood of accomplishing pregnancy in an appropriate number of open, healthy, cycling cows or heifers in a defined breeding season. A proper bull BSE should include the following four components:

Physical exam

Once the breeding season begins, a bull will literally forego his own nutritional needs and concentrate on breeding cows. Therefore, a physical exam and body condition scoring (BCS) will evaluate the physical characteristics of a bull necessary for structural soundness, mobility and athleticism in the pasture. This exam will also evaluate internal and external reproductive tract development or defects present.

Scrotal circumference

Evaluates testicular size and health, as well as estimating the bull’s sperm producing capacity. Bulls must meet minimum scrotal circumference measurements based on age in order to pass a BSE. The minimum requirements are as follows:




			Age

SC (CM)




15 mo


30




>15 < 18 mo


31




>18 < 21 mo


32




>21 < 24 mo


33




>24 mo


34





Sperm motility

Ensures that the bull is producing sufficient numbers of live sperm that have a progressive motility. Bulls must have at least 30 % motility to pass a BSE.

Sperm morphology

This portion of the evaluation is imperative. This evaluation ensures that the bull is producing sperm that are properly shaped and capable of fertilization. Multiple studies have found that poor sperm morphology is the reason that a large proportion (> 90%) of bulls fail a BSE. Bulls must produce at least 70 % normal sperm to pass a BSE.
The recommended minimum requirements for scrotal circumference, sperm motility, and sperm morphology are outlined by the Society for Theriogenology. Additional factors influencing the number of cows a bull can breed in a season include pasture size and terrain, physical soundness, age of the bull, libido, number of bulls in the group, etc.
Based on the results of the BSE a bull is then assigned to one of three classifications:

Satisfactory potential breeder (fertile) 

This classification indicates that the bull:

passed a physical exam
meets the minimum requirements for scrotal circumference
has at least 30 % sperm motility
produces at least 70 % normal sperm


Unsatisfactory potential breeder (sub-fertile or sterile)


The bull did not pass at least one of the four components of the BSE.


Deferred


The bull did not pass at least one of the four components of the BSE due to a condition that may resolve with time or treatment. A “deferred” bull should be rechecked at a later date determined by your veterinarian.  

A BSE does not evaluate a bull’s libido, nor does it ensure that a bull will remain a satisfactory potential breeder the entire breeding season, or the following breeding season. It is highly recommended to perform a BSE 45-60 days prior to every breeding season. If a bull suffers injuries to his feet, legs, reproductive tract, etc., such an injury may render him incapable of breeding your cows. Therefore, it is still extremely important to observe your bulls regularly to ensure they are doing their job. A BSE also does not guarantee that bulls are free of infectious diseases, so consult with your veterinarian on what diagnostic tests may or may not be appropriate for your bull. The extra pounds of beef per exposed cow will more than pay for the BSE, so contact your veterinarian for a bull BSE prior to every breeding season.