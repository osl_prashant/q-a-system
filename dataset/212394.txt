Iceberg lettuce prices likely are going to bounce up again, with possible minor gaps as summer approaches.The iceberg market, which eclipsed $50 a box April 26, drifted back to more normal levels, below $10 in May. But uneven planting schedules caused by changing weather will lead to a few gaps in the coming weeks, grower-shippers said.
“Markets have leveled off compared to the springtime high markets we had across the board,” said Mark McBride, salesman with Salinas, Calif.-based Coastline Family Farms.
Uneven weather will cause prices to spring upward again, at least until mid-June, McBride said.
“They may be looking for another bump upwards,” he said. “Realistically, it’s doing it now. It’s just doing it gradually.”
 

Prices

As of June 1, prices for cartons of iceberg lettuce from the Salinas-Watsonville, Calif., district were $9.25-9.65 for film-lined 24s; $10.25-10.65, 24s film-wrapped; and $6.50-8.55, 30s film-wrapped, according to the U.S. Department of Agriculture.
By comparison, on April 26 the same product was $45-52.55 for film-lined 24s; $45.45-53.55, 24s film-wrapped; and $42-48.56, 30s film-wrapped.
A year ago, the prices were $13.50-17.65 for film-lined 24s; $14.50-18.65, 24s film-wrapped; and $10.50-15.50, 30s film-wrapped.
Iceberg lettuce prices are riding a rollercoaster, McBride said.
“We’ve come through a pretty tough period there in the springtime,” he said. “The market has been about as reasonable the last two or three weeks as it’s been in a long time.”
Dan Canales, vice president of sales with Ippolito International, Salinas, said product moved briskly through the Memorial Day weekend, with the market having settled down.
“I’d say right now, it dropped down considerably, no doubt after that wild ride,” Canales said. “For a few weeks, it’s somewhat stabilized. We’ve had some incredible movement leading up to Memorial Day and we’ve already had an uptick in the market starting now.”
 

Weather

Some of that increase is weather-related, he said.
“I think a lot of that still is some off-schedule weather patterns that’s causing the lettuce to be a little ahead of itself,” Canales said. “When you get erratic weather patterns like that, (a volatile market) is just going to continue for several more weeks.”
Markets likely will push into the teens for a couple of weeks, Canales said.
“I’d say we have an interesting next probably two to three weeks — just enough to keep markets elevated a little bit — and a lot of that, of course, will rely on demand,” he said May 31. “As long as we keep demand level where it is, there will be enough consistency to create a better market.”
Supplies have been ample from most growers, but volume will be tight in the near future, said Greg Heinz, salesman with Salinas-based D’Arrigo Bros. Co. of California.
“We’re pretty light on iceberg this week, and that’s the projection for the next couple of weeks,” he said in late May. “We’ve been seven to 10 days ahead of schedule and we’ve seen lighter supplies, and I’ve heard other shippers were light.”
Prices could reach the “$12-14 range” over the next two weeks, Heinz said.
Smaller heads seem to dominate, Heinz noted.
“Seems like there’s a lot of 30-size lettuce due to certain varieties not producing a larger head,” he said.
Cooler weather, with some daytime high temperatures below 70 degrees, hasn’t helped, Heinz said.
“That’s part of the challenge,” he said. “Lettuce needs warmth. It’s a bit chilly, from a growing perspective.”
Prices will go up but nowhere near where they were in April, said Joe Ange, purchasing director with the Salinas-based Markon Cooperative Inc., a foodservice supplier.
“We’re going to see a little rebound to low teens, but more manageable and more normal levels on iceberg,” he said. “We are going to see markets kind of undulate at least through June.”