The produce industry continues to look at methods to raise fruit and vegetable consumption, and a few trends are emerging. 
Marketing and products that emphasize fruit and vegetable taste and quality, convenience or social responsibility seem to be selling better than others. 
 
A big picture goal to raising consumption is to normalize fruits and vegetables, especially for children, so that it’s considered a regular and essential part of eating every day.
 
We’re also seeing push-back against some other methods. The industry’s dismay at the Dirty Dozen’s effort to delegitimize conventional fruits and vegetables has been well documented.
 
Along those lines, we'd like to see less criticizing other foods.
 
While it’s true that most fruits and vegetables are among the healthiest food, plenty of others can be part of a normal diet. 
 
For instance, the U.S. Department of Agriculture recently gave a $1 million grant to research the effect of a fresh produce-themed TV show produced by New York-based Vedge’ Kids. 
 
The intentions are good but the shows’ story lines focus on fruit and vegetable characters overcoming villain characters representing candy, chips and cheeseburgers.
 
Plenty of kids and adults eat and enjoy candy, chips and cheeseburgers. The problem is when they do this and don’t eat enough servings of fruits and vegetables.
 
At the recent Produce for Better Health Foundation conference, PBH researchers revealed data that showed one barrier to consumption is produce’s health benefits.
 
That's right. Being healthy hurt produce consumption. It was even more pronounced in men. 
 
We still need to educate consumers but maybe not so much on health.
 
Did The Packer get it right? Leave a comment and tell us your opinion.