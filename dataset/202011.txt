What can you expect on June 8? As the 2016 World Pork Expo kicks off, here are the events, activities and seminars scheduled:Trade show runs from 8 a.m. to 5 p.m.
PORK Academy, sponsored by the Pork Checkoff
National Swine Registry ‚Äî America's Best Genetics Alley
PQA Plus® training at 9:15 a.m. and TQA® training at 1 p.m.
Children's activities‚Äî face painting and balloon sculpting, 9:30 a.m. - 3 p.m.
World Pork Open Golf Tournament, Otter Creek Golf Course, Ankeny, Iowa
World Pork Expo Junior National skillathon quiz
World Pork Expo Junior Nationalshows
The Big Grill, 11 a.m. - 1 p.m.
Wednesday business seminars include

9:30 - 12:00


Gut Physiology Development in Young Pigs - Why is Weaning Age and Digestibility So Important?Sponsored by: HAMLET PROTEIN
HAMLET PROTEINis hosting a Feed Your Brain seminar where experts in piglet nutrition and gut health will explore the theme of gut physiology development in young pigs - why is weaning age and digestibility so important? The program will discuss the biological reality of post weaning gut development and the implications on ingredient selection and whole diet formulation targets for optimal performance.
    


1:00 - 2:00


Oink. Sip. Love. The Science Behind Getting Baby Pigs to Drink from Day Two.Sponsored by: Tonisity, Inc.
Weaning is a critical turning point that has an effect on a pig's long-term performance. What if you could get ahead of this stressful period starting at birth? Learn about a first-of-a-kind isotonic protein drink that encourages fluid intake to improve hydration in suckling pigs while delivering necessary nutrients to improve pig health. Results will be shared from on-farm trials that show this new isotonic formulation decreases pre-weaning mortality, increases weaning weights, reduces the number of fall-behinds/removals while improving gut function and digestion.Fewer mortalities plus increased weights = more profit.
Tonisitywill host two sessions with plenty of time for Q&A. All swine producers, veterinarians and industry members are welcome to attend!


3:00 - 4:00


Oink. Sip. Love. The Science Behind Getting Baby Pigs to Drink from Day Two.Sponsored by: Tonisity, Inc.
Weaning is a critical turning point that has an effect on a pig's long-term performance. What if you could get ahead of this stressful period starting at birth? Learn about a first-of-a-kind isotonic protein drink that encourages fluid intake to improve hydration in suckling pigs while delivering necessary nutrients to improve pig health. Results will be shared from on-farm trials that show this new isotonic formulation decreases pre-weaning mortality, increases weaning weights, reduces the number of fall-behinds/removals while improving gut function and digestion.Fewer mortalities plus increased weights = more profit.
Tonisitywill host two sessions with plenty of time for Q&A. All swine producers, veterinarians and industry members are welcome to attend!


Wednesday PORK Academy seminars include

9:15 - 10:00


Changes on Antibiotic UsageSponsored by: Pork Checkoff
This session will provide an overview of regulatory changes for the responsible use of antibiotics food animals and what is means on the farm.


10:15 - 11:00


CybersecuritySponsored by: Pork Checkoff
As technology within the agriculture industry evolves at a rapid pace, producers need to be aware of their cyber security.Producers are at risk when they collect, process and store confidential information on computers, then transmit that data across networks to other computers. This presentation will cover topics to inform producers about cyber security, and how to protect themselves.


11:15 - 12:00


Common Swine Industry AuditSponsored by: Pork Checkoff
As packers have begun implementing on-farm audit programs for their suppliers, the National Pork Board has facilitated the development of the Common Swine Industry Audit to reduce burdens on producers and build audit process clarification across the industry. This session will highlight progress of the common industry audit, provide an opportunity for producers to ask questions of their peers who have experienced a third party audit and share resources available for producers to utilize in preparing for an audit.


1:30 - 2:15


Understanding the Biology of Seasonal Infertility to Develop Mitigation Strategies for SwineSponsored by: Pork Checkoff
Reduced productivity in the breeding herd following summer heat stress results in a 17% reduction in litters annually.However, producers have few tools to combat this problem.Research will be presented to detail the underlying biology related to the effects of heat stress on reproduction and may lead to mitigation strategies to reduce the impact on productivity.


2:30 - 3:30


Changes on Antibiotic UsageSponsored by: Pork Checkoff
This session will provide an overview of regulatory changes for the responsible use of antibiotics in food animals and what it means on the farm.