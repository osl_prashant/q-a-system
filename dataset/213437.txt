This market update is a Farm Journal's PORK weekly column reporting trends in weaner pig prices and swine facility availability.  All information contained in this update is for the week ended Oct. 20, 2017.

Looking at hog sales in April 2018 using May 2018 futures the weaner breakeven was $53.87, down $4.31 for the week. Feed costs were down $0.85 per head. May futures increased $5.22 compared to last week’s April futures used for the crush and historical basis is declined from last week by $3.56 per cwt. Breakeven prices are based on closing futures prices on October 20, 2017. The breakeven price is the estimated maximum you could pay for a weaner pig and breakeven when selling the finished hog.
Note that the weaner pig profitability calculations provide weekly insight into the relative value of pigs based on assumptions that may not be reflective of your individual situation. In addition, these calculations do not consider market supply and demand dynamics for weaner pigs and space availability.
From the National Direct Delivered Feeder Pig Report
Cash-traded weaner pig volume was above average this week with 39,666 head being reported. This is 106% of the 52-week average. Cash prices were $35.93, up $3.55 from a week ago. The low to high range was $23.00 - $43.00. Formula priced weaners were up $0.90 this week at $40.49.
Cash-traded feeder pig reported volume was below average with 6,325 head reported. Cash feeder pig reported prices were $47.78, up $4.64 per head from last week.
Graph 1 shows the seasonal trends of the cash weaner pig market. 

Graph 2 shows the cash weaner price and cash feeder price on a weekly basis through October 20, 2017.

Graph 3 shows the estimated weaner pig profit by comparing the weaner pig cash price to the weaner breakeven. The profit potential increased $0.76 this week to a projected gain of $17.94 per head.

Editor’s Note: Casey Westphalen is General Manager of NutriQuest Business Solutions, a division of NutriQuest.  NutriQuest Business Solutions is a team of business and financial experts in the livestock, row-crop and financial industries. For more information, go to www.nutriquest.com or email casey@nutriquest.com.