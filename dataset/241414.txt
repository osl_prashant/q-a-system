AP-RI--Rhode Island News Digest 6 pm, RI
AP-RI--Rhode Island News Digest 6 pm, RI

The Associated Press



Good afternoon. Here's a look at how AP's general news coverage is shaping up in Rhode Island. Questions about coverage plans are welcome and should be directed to AP Providence at 401-274-2270, or approvidence@ap.org.
Michelle R. Smith is on the desk until 8 p.m. AP New England News Editor Bill Kole can be reached at 617-357-8100 or bkole@ap.org. AP Providence Correspondent Michelle R. Smith can be reached at 401-274-2270 or mrsmith@ap.org.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. Advisories and digests will keep you up to date. All times are Eastern.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
UPCOMING TOMORROW:
COASTAL STORM
A fierce nor'easter is expected to continue to pound the East Coast with high winds, rain and snow downing power lines and flooding coastal areas. UPCOMING: 500 words, photos.
TOP STORIES:
COASTAL STORM
BOSTON — A relentless nor'easter pounded the Atlantic coast with hurricane-force winds and sideways-blown rain and snow Friday, flooding streets, grounding flights, stopping trains and leaving hundreds of thousands of people without power from North Carolina to Maine. Airlines canceled more than 2,800 flights, mostly in the Northeast. LaGuardia Airport in New York City grounded all flights, and John F. Kennedy, also in the city, grounded all but a few departures. By Sarah Betancourt. SENT: 586 words, photos.
— With: COASTAL STORM-THE LATEST: BOSTON — The Latest on a major late-winter storm pounding the East Coast. SENT: 2,200 words, photos. Will be updated.
IN BRIEF:
— POLICE-LEGOLAND HELP: PAWTUCKET, R.I. — Police have stepped up to help a Rhode Island girl who lost her birthday cake and Legoland tickets to thieves that broke into the family's apartment. SENT: 130 words.
— INSPECTOR GENERAL: PROVIDENCE, R.I. — Republican and Democratic lawmakers in Rhode Island are pressing for the creation of an Office of Inspector General to root out government abuse, fraud and waste. SENT: 130 words.
— DCYF-GROUP HOMES: PROVIDENCE, R.I. — A Rhode Island lawmaker says two troubled group homes should be immediately shut down or "even burnt down" after a scathing report from the state's child advocate. SENT: 130 words.
— DOUBLE FATAL CRASH: EAST GREENWICH, R.I. — Rhode Island State Police have charged a South Kingstown woman in connection with a crash in January that claimed the lives of two of her passengers. SENT: 130 words.
— DAIRY FARMING: SOUTH KINGSTOWN, R.I. — One of the last remaining dairy farms in Rhode Island may be forced to sell its cows after more than a century in business because of crushing debt. SENT: 130 words.
— SQUID FISHING: PORTLAND, Maine — Federal fishing regulators are keeping the quota for commercial squid fishermen about the same under new fishing rules that take effect soon. U.S. fishermen harvest shortfin and longfin squid in the Atlantic Ocean. SENT: 120 words.
— COUPLE SHOT DEAD: PROVIDENCE, R.I. — A woman picking up her grandson from a Rhode Island home says the 4-year-old told her he heard gunshots and his mother was dead. SENT: 130 words.
SPORTS:
BKC--T25-Rhode Island-Davidson
DAVIDSON, N.C. — The Rhode Island Rams look to bounce back from a 30-point loss earlier this week when the visit Davidson in the final Atlantic 10 Conference regular season game for both teams. By Steve Reed. UPCOMING. 600 words, photos. Game starts at 8 p.m. EST.
____
If you have stories of regional or statewide interest, please email them to approvidence@ap.org.
If you have photos of regional or statewide interest, please send them to the New York Photo Desk at 888-273-6867, via FTP or via email to statephotos@ap.org.
For access to AP Newsroom and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.