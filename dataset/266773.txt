Bull sale season is in full force around much of the country, and there are several things buyers will want to keep in mind before picking a sire to turnout on their cowherd.
Prior to selecting a bull, cow-calf producers should have solidified their breeding objectives.
Matt Spangler, Extension beef genetics specialist at the University of Nebraska-Lincoln, equates bull buying to grocery shopping. “You’ve got to have a list of what you need. If you don’t have a list of what you need, you’re going to buy a bunch of stuff that you didn’t need and spend way more for it.”
Three questions to answer before buying a bull include:

What are my breeding or marketing goals?
What traits directly impact the profitability of my enterprise?
Are there environmental constraints that need to be factored in?

Breeding objectives are dictated by marketing goals and environmental factors in particular areas of the country. A farmer-feeder running cows in Iowa who retains ownership on their own calves will probably be looking for a different type of bull than a rancher selling weaned calves in the Arizona desert. Also, farms and ranches developing their own replacement heifers will be sourcing different genetics than a terminal herd that buys replacements.
“The answers to those questions will dictate what traits are economically relevant for you,” Spangler says.
Evaluating your herd and answering those questions needs to be done prior to purchasing a bull, adds Shane Bedwell, chief operating officer and director of breed improvement for the American Hereford Association.
“There’s so many things that each and every operation is a little bit different at in determining your profit,” he says. “I think if we can’t answer these questions before we get to the sale then we’re not doing ourselves any good and we’re not progressing.”
Trait Selection
The majority of cow-calf operations retain heifers for their replacement program so fertility is going to be the most vital trait to selection. Spangler ranks fertility, growth and carcass as the order of economic importance when selecting bulls. When comparing bulls, producers have to not only consider traits but combinations of traits. Th ere are three different methods to take on multi-trait selection:
1. Tandem Selection: A producer selects for one trait at the level they want. Once it is achieved they move onto the next trait on the list.
“The problem with that is because traits are correlated to each other. Once I ignore the first trait and start selecting on the second one, there’s the potential erode progress I made in the first trait over time,” Spangler explains.
2. Independent Culling Levels: A threshold is set on every trait and bulls are crossed off the list that don’t meet the parameters set. Th is is the method most producers utilize.
“The problem with independent culling levels is what happens when a bull misses your threshold by just a little bit for one trait, but it’s vastly superior for everything else. Probably what you did is then you recalculated your threshold, so you can make sure he’s still made the list,” Spangler adds.
3. Economic Indexes: Theses values are a collection of EPDs, weighted by their relative economic importance.
Indexes can help identify animals that best fit the overall objectives of a cowherd, so a terminal producer can use indexes that put merit on carcass and feedlot performance. A maternal producer can select for traits that will result in better heifers.
“You’ve got to choose an index that fits what your objectives actually are,” Spangler says.
Maternal vs Terminal Traits
When comparing objectives of producers who are looking at maternal or terminal traits, there isn’t much overlap.
“There are sincere advantages as a commercial producer to say that ‘Here is what I am. I am not going to try and be all things,’” Spangler says.

For more on bull buying read part two: Bull Buying Considerations: Structural Soundness
 
Note: This is the first part of a two part series on bull selection that ran in the April 2018 magazine issue of Drovers.