Strawberry prices and volumes are comparable to last year at the same time.
In late August, flats of 8 1-pound containers with lids of medium California strawberries sold for $11-12, comparable to last year at the same time, according to the U.S. Department of Agriculture.
Season-to-date through Aug. 27, about 1.38 billion pounds of strawberries had shipped in the U.S., down from 1.4 billion pounds last year at the same time.
In the week ending Aug. 27, about 45.4 million pounds shipped, up from 31.7 million pounds last year at the same time.