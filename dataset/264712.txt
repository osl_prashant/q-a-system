March 20 was National Ag Day as part of National Ag Week. For America’s farmers and ranchers, that’s every day.

Vice President Mike Pence helped celebrate the 45th anniversary honoring agriculture at the USDA on Tuesday.

“To all of you who work in agriculture, whose labor and life’s work can be found ,we have so much more in the character of the nation,” said Pence. “Thank you.”

National Ag Day was created by the Agriculture Council of America (ACA) in 1973 to recognize the abundance of food, fuel and fiber produced.

National Ag Day is celebrated every March, and the date varies.