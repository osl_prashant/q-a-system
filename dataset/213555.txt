The growing season has been anything but normal for farmers in the Corn Belt. They have been plagued with flooding, replanting, drought, not enough heat and other issues. Following the Farm Journal Midwest Crop Tour, many farmers have been praying an early frost won’t come their way.But it could be knocking on their door much sooner than anticipated.
On AgriTalk, meteorologist Michael Clark of BAMWX.com told host Mike Adams parts of the northern and western Corn Belt could see a “strong cold front that could get sharply colder  next week.”

Between September 6 and 10, northern Illinois, northern Iowa, northern Nebraska, the Dakotas, Minnesota and Wisconsin could possibly see frost.
“There’s a lot of evidence of that,” Clark told host Mike Adams. “We’ve been talking about that since [Aug. 2] in our data analysis.”
Clarks said part of the reason for the cold front is because of Hurricane Harvey. He said normally a cold front follows a hurricane, and three things are lining up for areas to see a frost in early September.
A western U.S. ridge is forming. The heat in the Pacific Northwest causes the ridge to expand, which will dislodge colder air into the Plains.
A high pressure system near Greenland.
There’s a typhoon forming in the western Pacific Ocean which could add more of a risk of cold in September.
With these factors aligning, Clark said on the morning of Sept. 7, lows could fall between 34 and 39 degrees—between 15 and 18 degrees colder than normal—that would be a “legitimate threat” of an exceedingly early frost.
“I wouldn’t look for heat in September for soybeans,” said Clark.
There’s also another risk of a shot of cold air between Sept. 18 and 25, especially if the typhoon in the Pacific Ocean recurves. The central and western Corn Belt will continue to remain at risk for shots of cold air, but the eastern Corn Belt could warm up toward the end of September.
To sum it up, Clark said September will be colder and drier than normal, with a below average risk for rainfall. The only wildcard would be if the U.S. is hit with another hurricane and there are tropical remains to hit the ag belt.
“It’s certainly something we need to watch on a daily basis,” said Clark.
Hear Clark’s full report on AgriTalk above.