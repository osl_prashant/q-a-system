Often, a search for knowledge not only answers a question, but also raises new ones. That’s the case with a nine-year study, in which Farm Journal Field Agronomist Ken Ferrie is attempting to improve the health of abused soil.
The study involves two farms within a mile of each other. Farms A and B contain similar soil types and were managed in similar fashion. But the operator of Farm A has been using no-till practices for more than 35 years, while the former operator of Farm B allowed the soil to become acid and compacted. Various soil health measurements, including water infiltration, surface structure and soil respiration, confirm the superior health of Farm A’s soil, which accounts for its higher yield.
Under its new operator—who made multiple lime applications and vertical tillage passes to remove dense layers before converting the field back to no-till—Farm B is producing higher yields, although it hasn’t yet caught up to Farm A. In addition, soil organic matter content—one measurement of soil health—is trending upward.
More organic matter correlates to increased nutrient cycling and water availability, Ferrie explains. “Each percent of organic matter means 20,000 more gallons of water per acre stored in the soil,” he says.
That generates an obvious question: How high can the operator of Field B hope to build his soil’s organic matter content? As Ferrie compared the history of unhealthy Farm B to the healthier Farm A, another question arose: Why has the organic matter content of Field A, where the soil has been in excellent health for a long time, plateaued rather than continuing to increase?
In 2009, when the new operator began rehabilitating Farm B’s soil, Ferrie started to chart the effect on organic matter, or carbon content, as shown on the graph on page 22. The wavy lines show the organic matter content as recorded on biannual soil tests. This reading fluctuates because it includes active organic matter, the portion that changes rapidly because it reflects the amount of crop residue (corn versus soybeans, for example) and environmental factors.
Consequently, examining a few years of soil tests can provide an inaccurate, or exaggerated, impression of organic matter content. So Ferrie graphed a trend line to show in which direction organic matter content is moving and how rapidly over about 10 years. The trend line suggests improved soil health practices on Farm B are causing a rapid increase in soil organic matter content.
Comparing Farm B’s results to Farm A, where soil test results were available back to 1991, Ferrie noticed organic matter content of that farm’s healthy soil remained static, despite the continued use of excellent soil health practices. (To ensure consistent results, all organic matter tests were conducted by the same lab.)  
Hence the questions: How high can Farm B’s organic matter content rise, and why has Farm A’s organic matter content plateaued? Ferrie thinks the answer is that soil organic matter reaches what scientists call an equilibrium level based on the climate and management system.
Ferrie bases that opinion on a previous study in which he analyzed the health of soil in a cemetery that had not been disturbed for more than 150 years. “The soil in the cemetery contained 3.1% organic matter,” Ferrie says, “compared with 2.3% in the same soil type just yards away in farm fields surrounding the cemetery.
“With the cemetery being essentially a perpetual cover crop, why wasn’t there a greater difference? In all that time, why hadn’t the organic matter climbed to 10%?”
Ferrie believes Farm A’s soil organic matter has reached an equilibrium level, in which the soil is burning as much carbon as it produces. “Only a certain amount of carbon can be sequestered and maintained in any soil, based on its management and climate,” he says. “The soil in the cemetery also is in equilibrium. The cemetery soil’s organic matter content is probably about equal to that of native prairie. In the adjacent farm fields, cropping practices had caused organic matter content to decline until the soil reached a new equilibrium level.”
The graph suggests improved soil health practices on Farm B began having an impact in only two years, when organic matter readings began moving upward in 2012. It also suggests the soil might have begun moving to a new equilibrium level about 2015. Time will tell about that.
The equilibrium concept suggests it’s unlikely the red soils of the South, which contain less carbon, will never be black with organic matter like the soils of the northern Corn Belt, even if kept in permanent pasture. But the Farm A/Farm B comparison shows improved soil health practices can raise organic matter/carbon content to a new equilibrium level. In future years, the addition of cover crops on both farms will reveal whether adding that soil health practice can push the equilibrium level upward.
Ferrie’s soil health study also has yielded knowledge you can use to evaluate soil health changes on your own farm. Early on, his efforts produced more frustration than enlightenment. “The problem was that many soil health scores, as reflected in various tests, go up and down based on seasonal environmental conditions,” he explains.
For example, he says, “in a cover crop strip trial, the cover-cropped soil consistently tested higher than the non-cover-cropped soil in biological activity. But one year, the numerical scores for both treatments would be in the low range and the next year they would be in the optimum range. A year later, the cover-cropped soil again tested higher in biological activity, but the test’s numerical scoring system placed both soils in the optimum range.”
“To be useful, soil health test results must be repeatable in the same year and from one year to the next. Eventually we realized weather and other environmental factors were moving the numbers around.”
Ferrie is confident soil health is improving with cover crops and reduced tillage. It’s not the numerical score, but the relationship between new and old practices that matters when realizing the benefits.
Based on that realization, Ferrie now uses the farms’ normal practices as an “anchor point” and compares the difference between the new practice and the anchor point to determine the effect. In the cover crop study, covers were having a beneficial impact if the difference in soil health between cover and non-cover (the anchor point) areas increased over time, regardless of the numerical score.
“A number of soil health tests can be used in this way,” Ferrie says. “The anchor point can be an undisturbed fencerow or a pasture, or it can be an area where you continue to use the same management practices [without changing them because any change, such as a new tillage system, could start moving the soil to new equilibrium levels]. In the soil rehabilitation study, Farm A’s soils serve as the anchor point.”
Eventually, the study will reveal if and how fast the soil health gap bet-ween the two farms can be closed.

Understanding Organic Matter
All carbon, which makes up most of what we call organic matter, is not created equal, and that can make soil test readings confusing. Active carbon/organic matter is metabolized in a few months to a few years. Slow organic matter requires decades, and passive organic matter requires hundreds or even thousands of years.
“Most nutrient cycling involves active carbon,” says Farm Journal Field Agronomist Ken Ferrie. “The active portion, which makes up 10% to 20% of the organic matter in soil, quickly increases and decreases based on the management system and the climate.
“That’s why looking at just one or two years of soil test readings might provide an inaccurate picture of what’s happening in soil. Organic matter readings might vary by a one tenth of a point, or even more, just because of the volume of crop residue and weather conditions. Readings might even vary from month to month because the active organic matter cycles so rapidly.”
To see what really is happening with organic matter in your soil, look at multiple years of soil tests (the more, the better), Ferrie advises.

Tests to Measure Soil Health
Here’s a list of soil health tests Farm Journal Field Agronomist Ken Ferrie uses to measure the impact of improved management practices on soil health. Because the numerical ratings produced by the tests are influenced by cropping system and environmental conditions, the soil being tested should be compared to an anchor point. “The relationship between the two areas will show whether new practices are improving soil health,” he says.
Chemical Tests
Standard Fertility Test:

Organic Matter
Nitrate Nitrogen
P1 Phosphorus
P2 Phosphorus
Potassium
Magnesium
Calcium
pH
Cation Exchange Capacity
Percent Base Saturation

H3A Extraction—Haney Test:

Orthophosphate-P
Phosphorus
Potassium
Magnesium
Calcium
Sodium
Iron
Aluminum

Biological Tests
Water-Soluble Extract—Haney Test:

Nitrate Nitrogen
Ammoniacal Nitrogen
Orthophosphate-P
Carbon
Total Nitrogen

1-Day CO2 Carbon Burst
Organic Carbon/Nitrogen Ratio
Soil Health Calculation
MicroBiometer Test (from Prolific Earth Sciences)
Physical Tests
Available Water Capacity
Aggregate Stability

Soil Health Practices Boost Organic Matter
The graph shows the impact of improved soil health practices on Farm B’s abused soil. On all three soil types, organic matter began trending upward about two years after lime was applied, dense layers removed and no-till practices adopted. Research conducted by Farm Journal Field Agronomist Ken Ferrie suggests every soil reaches an equilibrium organic matter (or carbon) level based on management practices and environment.




Soil Health Practices Boost Organic Matter
© Crop-Tech Consulting