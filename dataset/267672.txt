Grains mostly higher and livestock lower
Grains mostly higher and livestock lower

The Associated Press

CHICAGO




CHICAGO (AP) — Grain futures were mostly higher Friday in early trading on the Chicago Board of Trade.
Wheat for May delivery rose 5.20 cents at $4.6940 a bushel; May corn was up 3.60 cents at $3.8820 a bushel; May oats fell .60 cent at $2.3240 a bushel while May soybeans was gained 6 cents at $10.2340 a bushel.
Beef and pork were lower on the Chicago Mercantile Exchange.
April live cattle fell .27 cent at $1.1323 a pound; Apr feeder cattle lost .90 cent at $1.3560 a pound; April lean hogs was off .70 cent at .5238 a pound.