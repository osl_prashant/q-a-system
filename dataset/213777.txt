BrightFarms is recalling fresh lettuces and herbs and herbs grown at its Chicago-area greenhouse for potentially being tainted with E. coli.
According to a news release from the company posted on the Food and Drug Administration’s website, the recall affects only products grown at that facility that were distributed to Roundy’s Supermarkets banners Mariano’s Markets in Illinois and Metro Markets and Pick ‘n Saves in Wisconsin.
BrightFarms issued the recall “out of an abundance of caution after receiving routine lab results and discovering that certain products may have been compromised,” according to the release. It did not say when the test was, who tested the products, or if the test was on a piece of equipment or other surface used in the packing process.
BrightFarms chose to take this action out of an abundance of caution after receiving routine lab results and discovering that certain product may have been compromised. Affected retailers have been instructed to remove all affected products from store shelves.
The salad items were packed in clamshells. “Best by” dates on the packages are Oct. 24-27.
Those items, all under the BrightFarms brand, are:

Baby spinach, 4 ounces and 8 ounces
Spring mix, 4 ounces and 8 ounces
Spinach blend, 4 ounces
Baby greens blend, 4 ounces and 8 ounces
Baby kale, 3 ounces
Baby arugula, 4 ounces
Baby romaine mix, 4 ounces

Recalled herbs were also packed on Oct. 24-27:

Basil, 0.75 ounces and 2 ounces
Thai basil, 0.75
Lemon basil, 0.75 ounces

The recall is limited to products grown at the company's Rochelle, Illinois farm. BrightFarms products from greenhouses in other regions are not affected.