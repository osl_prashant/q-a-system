Export demand is driving up prices for quality hay in the western U.S. According to RaboBank’s RaboResearch Hay and Forage report the influence of international traders are making the competition for alfalfa and other hay difficult for dairies in the western states.The western seven states of Arizona, California, Idaho, Nevada, Oregon, Utah and Washington produce 18% of the U.S. hay total, but those states account for 90% of U.S. hay exports.
The past two years hay stocks have built up as both milk and cattle prices have slid. Corn prices have been lower too, leading to added corn in some cattle raisers rations. These have all compounded a downward price trend for hay in 2015 and 2016.
The downward pressure on prices isn’t the end of the story, says James Williamson, dairy analyst, RaboResearch Food and & Agribusiness group.
“We’ve seen the top six hay importers – responsible for buying over 95% of U.S. hay exports –increasing their import volumes and paying a premium for higher-quality hay, supporting prices at their current levels. As a result, prices will likely continue moving in an upward trend,” Williamson says.
In the first quarter of 2017 hay exports were up 18% compared to the same time in 2016. At this same time exports to China, South Korea and Saudi Arabia have all gone up from the same period:
Saudi Arabia, increased exports 62%
China, increased exports 47%
South Korea, increased exports 18%
Hay exports to Saudi Arabia are expected to increase as the country plans to stop hay production by 2019 to help preserve water.
California has been through several years of drought prior to the 2017 growing season. Because limited water California has dropped hay acreage by 32% since 2008.
“Water restrictions aren’t limited to California,” Williamson says. “Increasing pressure to conserve water resources around the world, specifically in areas such as Saudi Arabia, will continue to drive demand alfalfa and other hay. This is going to be the new normal.”
Williamson believes dairy producers in the western states will need to consider alternative feed source or pay higher premiums for premium quality hay to remain competitive with international hay buyers.
RaboResearch projects international demand to increase up to 25% in the next three to five years which will drive up the price for premium quality hay.