Take advantage of the opportunity to make more money per acre—consider adding wheat into your corn-soybean rotation. Consider double crop soybeans for future seasons since they can be tricky, but pay off.
“Wheat can be on the farm today and successful with a systems approach,” says Wade Wiley, lead licensing and market analyst at Beck’s Hybrids. He built a model of the past 30 years of historical data from USDA and worked with CropZilla to learn what advantage a double-crop system could provide.
“According to 30 years of data, 73% of the time the double crop rotation was more profitable than the normal corn-soybean rotation,” he adds.
Wheat can be beneficial in corn and soybean rotations for a number of reasons. For example, winter wheat can act as a cover crop after corn is harvested, resulting in less erosion, fewer weeds and soil microbial health benefits—all while still being a cash crop.
“Wheat also can reduce soybean cyst population and egg bank,” says Dan Davidson, research and technical coordinator for the Illinois Soybean Association.
Before jumping in on double crop wheat-soybeans consider these best practices for soybean health outlined by the Illinois Soybean Association:

Use early wheat harvest systems
	
Buy varieties that mature three to seven days sooner but maintain yield


Harvest wheat at 18% to 22% moisture and dry on-farm or at the elevator
Consider wheat straw and its effect on planting
	
Hair pinning straw into the V trench can cause issues


Plant soybeans as early as possible
Consider narrow rows

According to CropZilla’s study, using a corn, wheat and double crop bean system adds a $13.29 advantage per acre when compared to just a corn-soybean rotation on a 3,000 acre farm.
Be sure to consider some limiting factors when double cropping. One such factor is herbicide carryover—if you use a long-lasting broadleaf herbicide in wheat that can be detrimental to soybeans. In addition, soil moisture and weeds at planting should be considered before seeding soybeans.
If you’re looking for a possible advantage—or just something different for your soils it could be beneficial to try double cropping. Your geography is unique, so be sure to speak with your local agronomist to determine your personal best management practices and if double cropping will work on your farm.