Couple turns love of coffee into successful business
Couple turns love of coffee into successful business

By JENNIFER GARDNERThe Charleston Gazette-Mail
The Associated Press

MORGANTOWN, W.Va.




MORGANTOWN, W.Va. (AP) — A certain distinct aroma coming from Ryan and Carly Lemley's quiet Cheat Lake home lets your nose know right away that there's something different going on here.
It's the unmistakable, rich mocha scent of home-brewed coffee. But this is not just a morning brew or afternoon pick-me-up. It's Ryan's day off from his full-time job as a recreation specialist at a federal prison.
In his free time, he roasts coffee for the couple's local business, Ridge Roasters, which has sold coffee across the state since June 2015.
"This is our coffee skillet," Ryan said, holding up a well-used frying pan. "You can probably see some residue, the oils and stuff in there.
"We started roasting on the stove and if you look above the stove you'll see a black spot from where the smoke burnt the ceiling."
The dark spot on the ceiling of their kitchen isn't the only indication of their special love affair with coffee. A step through the front door reveals the family's very own coffee bar, complete with a chalkboard scribbled with ratios for perfect brews.
The two originally began roasting coffee to create their own morning brews, but the hobby quickly grew into a business once they began sharing their creations with friends and family.
Ryan and Carly have enjoyed daily cups of Joe since they can remember. But only a few years ago, they found themselves inspired by the roasting process at a small Columbus cafe.
"We stopped by early in the morning. We were traveling home and ended up staying for well over two hours," Ryan said. "The baristas were amazingly welcoming, friendly and educational. It was a whole other world than what we were used to at the time. It opened our eyes to 'what could be in our community.'"
The two, high school sweethearts, grew up in Preston County. They felt a strong pull to live close to family as they raised their children and began their careers. Carly owns a dental practice in the area.
A coffee roastery would add something new to Morgantown, they thought.
"It's kind of frustrating to us to go to a nice restaurant in town where they serve locally-roasted coffee and the best that they could do was get it from Virginia," Carly said. "We feel like Morgantown was cooler than that."
Ryan credits his beginnings in the business to a meeting with another local roasting firm, Aroma of the Andes, a specialty coffee company owned by a family in Gilmer County. Owners Duane and Rachel Brown own a coffee farm in the Andes Mountains in Colombia, South America.
After a call to Duane, Ryan and Carly made a visit to the couple's home in Linn. They left with about five pounds of green coffee and tried their hand at roasting on the stove.
Ridge Roasters now sources some of its coffee from Aroma of the Andes, in addition to farms in Honduras, Guatemala, Brazil and Ethiopia.
They've found themselves engaged in a community of farmers, roasters and coffee drinkers.
With the skillet, Ryan could only roast about a pound of coffee beans in a single hour.
From the frying pan, the couple graduated to a rotisserie coffee roaster, which sat on a grill on their back porch for nearly a year.
Roasting in the elements quickly became a challenge.
"It got to the point where the wind just whips through here, so it blew my flame out," Ryan said.
They moved the rotisserie to an outdoor building at his parents' home about 30 minutes away.
They'd pick up the kids from school and head out for an evening of roasting.
"The time it took was longer," Ryan said. "It was hard to regulate temperatures."
The business was taking off and the couple needed to acclimate.
They didn't want to rent space. So, they built a roastery in their garage.
Fitness equipment and bicycles take up one half, while the roaster and sacks of coffee beans occupy the other.
The homeowners' association had few issues with it, they said, so long as it didn't mean customers constantly coming and going to the house.
Within about 50 feet of their home, the aroma is quite obvious — not that any of the neighbors have complained.
Ryan will spend several hours a day in the garage filling commercial orders. He can roast about 40 pounds of coffee an hour.
"My days off are Monday, Tuesday, Wednesday," Ryan said, counting the fingers on his hand. "When I'm off, I get the kids to school, come back and roast all day."
The coffee is sold to cafes in the Morgantown area, as well as in Harper's Ferry and Huntington.
For now, Ryan and Carly think of the business as more of a hobby.
Installing the roaster in their home meant more time spent with their children, ages 5 and 7.
"We don't want to take time away from our kids and we want them to see what hard work looks like," Carly said. "Being passionate about something and making it grow — it takes time and effort."
The couple is hoping to grow into more cafes and restaurants around the state. Ideally, they'd like to eventually make Ridge Roasters a full-time gig, at least for Ryan.
"This has been like a second full time job for both of us," Carly said. "I guess that says something about how much we enjoy this."
Ryan and Carly have found themselves immersed in the coffee roasting world, and loving every minute.
In early March, Ryan took a trip to Lenca Farms in Honduras to meet the farmers who grow the company's beans.
"I felt it was important to take the trip as I was able to make a connection with the coffee we purchase, roast, sell and brew," he said. "I don't think it's possible to fully appreciate coffee and what it is until you have the opportunity to take a trip to its origin."
Two years ago, he visited Brown's farm in Supata, Colombia — his first trip to South America. He remembers feeling "bright-eyed," he said.
With a five-gallon bucket strapped around his waist, he was able to get his hands on the cherry, and pick the fruit that houses the flavorful bean inside.
In both places Ryan learned the process from "seed to cup." It gave him an opportunity to connect with families who not only grow his coffee but who have dedicated their entire lives to the crop.
"Many of the areas in which coffee is grown around the world — it is a community," Ryan said. "A community of families and workers who work long hours with lots of labor to help produce an amazing cup of coffee."
There wasn't a day he wasn't offered lunch and dinner by the families, he said. Conversations ranged from discussing coffee to simply discussing life.
"We've had incredible experiences so far — we've had ups and downs — but we just want people to take time, slow down, think about where that cup came from, think about what went into that simple brew," he said.
His peaceful time spent on farms and roasting coffee in his garage is a world apart from his days spent at the prison where he works.
"Coffee to us is community," he said. "It's that quiet time in the morning if you have time. It's time to look at the day ahead, time to reflect on the day past if you consume coffee in the evening.
"It's a conversation topic or a beverage we have with family and friends. Coffee is quite simply amazing and most of all it brings people together."
Where to find Ridge Roasters:
Ridge Roasters coffee is brewed at Peace, Love, and Little Donuts; Monongalia County Ballpark; Apple Annie's Bakery; and Hill and Hollow in Morgantown; Butter It Up at the Market in Huntington; Battlegrounds Bakery and Coffee, and The Country Cafe in Harper's Ferry.
The company also sells a nitro cold brew, which is cold-brewed, put in a keg and infused with nitrogen gas. The Nitro Cold Brew is only sold on tap at Butter It Up at the Market in Huntington and the Monongalia County Ballpark.
___
Information from: The Charleston Gazette-Mail, http://wvgazettemail.com.