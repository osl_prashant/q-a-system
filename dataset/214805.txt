Stockton, Calif.-based Farmington Fresh is now the exclusive provider of sliced Opal apples.
The company expects to process more than 500 tons of the fruit this season, said marketing manager Garrison Rajkovich. Opals are grown by Broetje Orchards in Washington and marketed by Yakima-based FirstFruits Marketing of Washington.
Farmington Fresh is the largest supplier of sliced apples for school lunches in California and supplies retailers in the state, according to a news release.
Rajkovich said the company is also looking to expand distribution, with interest from retailers in the Northeast and in the Midwest.
The Opal is in peak production throughout the winter, and retailers can start with the program after Thanksgiving, Rajkovich said.
Available pack sizes for retail include 14-ounce and 4-ounce bags and a clamshell with six 2-ounce bags. Foodservice options include 3-pound bulk bags and 2-ounce single-serve bags designed for schools.
Farmington Fresh also offers peeled and sliced Opals.
The Opal is a cross between a golden delicious and a Topaz.
“Farmington Fresh was inspired by the innovation that my grandfather, Nick Rajkovich, brought to the fruit market in his day, and this one-of-a-kind variety is a perfect match to the legacy of excellence that he was known for,” Rajkovich said in a news release.
“The Opal-brand apple is truly unique, with the mouth-pleasing crunch of a Honeycrisp and an exceptionally sweet flavor profile. We’re thrilled to be partnering with the Broetje Orchards team to provide a product that fits perfectly with our focus on high quality and customer satisfaction.”