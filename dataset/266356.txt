Target recalls products sold at Ala Moana Center store
Target recalls products sold at Ala Moana Center store

The Associated Press

HONOLULU




HONOLULU (AP) — Target Corp. has voluntarily recalled several different types of frozen food sold March 23-27 at its Ala Moana Center store.
Target said that the products might be spoiled and contaminated with organisms or pathogens due to a temperature malfunction during shipment to Hawaii.
Employees removed all affected products from the store once the problem was discovered.
The items include ice cream, frozen vegetables and meats, prepared foods such as chicken strips, pizza rolls, lasagna, baked goods and much more.
No illnesses have been reported.
Target is working with the U.S. Department of Agriculture and the Federal Drug Administration.
Products sold at any other Target store are not affected by the recall.