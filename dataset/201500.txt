USDA Weekly Export Sales Report
			Week Ended August 3, 2017





Corn



Actual Sales (in MT)

Combined: 680,400
			2016-17: 52,000 MT
			2017-18: 628,400 MT



Trade Expectations (MT)
2016-17: 100,000 to 300,000 MT
			2017-18: 400,000 to 600,000 MT


Weekly Sales Details
Net sales of 52,000 MT for 2016/2017 resulted as increases for Japan (88,100 MT, including 92,700 MT, switched from unknown destinations and decreases of 5,700 MT), Mexico (87,600 MT), Peru (56,800 MT, switched from unknown destinations), Colombia (16,400 MT, switched from unknown destinations), and Honduras (13,500 MT, including 12,300 MT switched from unknown destinations). Reductions were reported for unknown destinations (248,100 MT) and Panama (500 MT). For 2017/2018, net sales of 628,400 MT were reported primarily for Colombia (275,000 MT), Mexico (194,100 MT), and unknown destinations (124,800 MT). Reductions were reported for Nicaragua (9,000 MT), Costa Rica (1,000 MT), and Taiwan (300 RB).


Weekly Export Details
Exports of 973,500 MT were down 11 percent from the previous week, but unchanged from the prior 4-week average. The primary destinations were Japan (328,100 MT), Mexico (245,400 MT), Colombia (108,900 MT), South Korea (74,200 MT), and China (68,800 MT). 


Comments and Performance Indicators
Old-crop sales were ligher than expected, but new-crop sales topped expectations. Export commitments for 2016-17 are running 14% ahead of year-ago compared to 15% ahead the week prior. USDA projects exports in 2016-17 at 2.225 billion bu., up 17.0% from the previous marketing year.




Wheat



Actual Sales (in MT)

2017-18: 464,300



Trade Expectations (MT)
250,000-450,000


Weekly Sales Details
Net sales of 464,300 metric tons for delivery in marketing year 2017/2018 were up noticeably from the previous week and 11 percent from the prior 4-week average. Increases were for Indonesia (115,000 MT, including 70,000 MT switched from unknown destinations), South Korea (70,000 MT), Algeria (58,000 MT), the Philippines (51,500 MT, including 45,000 MT switched from unknown destinations), and Chile (45,500 MT, including 30,000 MT switched from unknown destinations). Reductions were reported for unknown destinations (46,800 MT), Malaysia (9,000 MT), and China (1,700 MT). 


Weekly Export Details
Exports of 568,200 MT were down 3 percent from the previous week, but up 9 percent from the prior 4-week average. The primary destinations were Japan (155,300 MT), China (108,300 MT), the Philippines (71,500 MT), Taiwan (50,300 MT), and Chile (32,500 MT).


Comments and Performance Indicators
Sales topped expectations. Export commitments for 2017-18 are 2% below year-ago versus steady with year-ago the week prior. USDA projects exports in 2017-18 at 975 million bu., down 7.6% from the previous marketing year.




Soybeans



Actual Sales (in MT)

Combined: 684,300
			2016-17: 45,000 MT
			2017-18: 639,300 MT



Trade Expectations (MT)
2016-17: 100,000 to 300,000 MT
			2017-18: 250,000 to 450,000 MT


Weekly Sales Details
Net sales of 45,000 MT for 2016/2017--a marketing-year low--were down 81 percent from the previous week and 83 percent from the prior 4-week average. Increases were reported for the Netherlands (149,000 MT, including 140,400 MT switched from unknown destinations and decreases of 2,600 MT), China (80,200 MT, switched from unknown destinations), Indonesia (78,800 MT, including 50,000 MT switched from unknown destinations and decreases of 1,900 MT), Mexico (22,900 MT), and Peru (14,800 MT, switched from unknown destinations). Reductions were reported for unknown destinations (283,300 MT), Spain (65,000 MT), and Egypt (2,400 MT). For 2017/2018, net sales of 639,300 MT were reported for China (184,000 MT), unknown destinations (141,300 MT), Mexico (119,900 MT), and Spain (65,000 MT). 


Weekly Export Details
Exports of 612,300 MT were down 14 percent from the previous week, but up 28 percent from the prior 4-week average. The destinations were primarily China (187,000 MT), the Netherland (149,000 MT), Indonesia (75,000 MT), Pakistan (68,600 MT), and Egypt (57,600 MT). 


Comments and Performance Indicators
Old-crop sales were lighter than expected, but new-crop sales topped expectations. Export commitments for 2016-17 are running 15% ahead of year-ago, compared to 16% ahead the previous week. USDA projects exports in 2016-17 at 2.100 billion bu., up 8.1% from year-ago.




Soymeal



Actual Sales (in MT)
Combined: 142,500
			2016-17: 6,000 MT
			2017-18: 136,500 MT


Trade Expectations (MT)

2016-17: up to 100,000 MT
			2017-18: 50,000 to 150,000 MT



Weekly Sales Details
Net sales of 6,000 MT for 2016/2017 resulted as increases for Venezuela (30,000 MT, switched from unknown destinations), Canada (11,600 MT), Colombia (10,000 MT), and the Philippines (8,100 MT). Reductions were reported for unknown destinations (64,500 MT), China (30,000 MT), and Nicaragua (300 MT). For 2017/2018, net sales of 136,500 MT were reported primarily for the Philippines (43,000 MT), Colombia (41,500 MT), Mexico (21,900 MT), and El Salvador (12,000 MT). 


Weekly Export Details
Exports of 186,100 MT were down 2 percent from the previous week, but up 27 percent from the prior 4-week average. The destinations were primarily the Philippines (91,500 MT), Mexico (43,900 MT), Canada (15,700 MT), Israel (11,000 MT), and Guatemala (7,100 MT).


Comments and Performance Indicators
Old-crop sales were light, but new-crop sales were within expectations. Export commitments for 2016-17 are running 2% behind year-ago compared with 2% behind year-ago the week prior. USDA projects exports in 2016-17 to be down 0.5% from the previous marketing year.




Soyoil



Actual Sales (in MT)

2016-17: 27,700 MT



Trade Expectations (MT)
2016-17: 5,000 to 20,000 MT
			2017-18: up to 5,000 MT


Weekly Sales Details
Net sales of 27,700 MT for 2016/2017 were up noticeably from the previous week and 65 percent from the prior 4-week average. Increases were reported for Canada (21,900 MT), Jamaica (3,500 MT), Mexico (1,100 MT), and the Dominican Republic (1,100 MT). 


Weekly Export Details
Exports of 25,300 MT were down 20 percent from the previous week, but up 12 percent from the prior 4-week average. The destinations were primarily Canada (22,100 MT), Mexico (3,100 MT), and Lebanon (100 MT).


Comments and Performance Indicators
Old-crop sales topped expectations, but no new-crop sales were reported. Export commitments for the 2016-17 marketing year are running 6% behind year-ago, compared to 8% behind year-ago last week. USDA projects exports in 2016-17 to be up 7.0% from the previous year.




Cotton



Actual Sales (in RB)
2017-18: 75,800 RB


Weekly Sales Details
Net sales for 2017/2018, which began August 1, totaled 75,800 RB. Increases reported for Vietnam (25,100 RB), Thailand (15,800 RB), Mexico (10,000 RB), Pakistan (7,700 RB), and China (7,400 RB), were partially offset by decreases for Japan (2,400 RB). A total of 745,300 RB in sales were carried over from the 2016/2017 marketing year, which ended July 31. Exports for the period ending July 31 of 102,200 RB brought accumulated exports to 13,823,100 RB, up 64 percent from the prior year’s total of 8,419,600 RB. The primary destinations were Vietnam (22,800 RB), Turkey (21,700 RB), Mexico (12,800 RB), South Korea (8,500 RB), and Indonesia (5,800 RB).


Weekly Export Details
Exports for August 1-3 totaled 114,000 RB, with Turkey (30,400 RB), Vietnam (27,700 RB), China (14,700 RB), India (9,700 RB), and Mexico (8,800 RB) being the primary destinations. 


Comments and Performance Indicators
Export commitments for 2016-17 are 73% ahead of year-ago, compared to 61% ahead the previous week. USDA projects exports to be up 58.5% from the previous year at 14.500 million bales.




Beef



Actual Sales (in MT)

2017: 16,400 MT



Weekly Sales Details
Net sales of 16,400 MT reported for 2017 were up 44 percent from the previous week and 31 percent from the prior 4-week average. Increases were reported for Japan (9,500 MT), Hong Kong (1,900 MT), South Korea (1,500 MT), Mexico (1,300 MT), and Canada (1,000 MT). Reductions were reported for Egypt (400 MT) and Cote D’Ivoire (100 MT).


Weekly Export Details
Exports of 14,700 MT were down 3 percent from the previous week, but up 3 percent from the prior 4-week average. The primary destinations were Japan (5,400 MT), South Korea (2,900 MT), Mexico (1,700 MT), Hong Kong (1,500 MT), and Canada (1,000 MT). 


Comments and Performance Indicators
Weekly export sales compare to 11,400 MT the week prior. USDA projects exports in 2017 to be up 5.0% from last year's total.




Pork



Actual Sales (in MT)
2017: 18,900 MT


Weekly Sales Details
Net sales of 18,900 MT reported for 2017 were up 7 percent from the previous week and 30 percent from the prior 4-week average. Increases were reported for Hong Kong (4,200 MT), Japan (3,000 MT), Mexico (3,000 MT), South Korea (2,300 MT), and Canada (1,500 MT). 


Weekly Export Details
Exports of 17,300 MT were down 4 percent from the previous week, but unchanged from the prior 4-week average. The destinations were primarily Mexico (7,500 MT), Japan (3,300 MT), Canada (1,700 MT), South Korea (1,300 MT), and Hong Kong (1,100 MT). 


Comments and Performance Indicators

Export sales compare to a total of 17,800 MT the prior week. USDA projects exports in 2017 to be 3.6% above last year's total.