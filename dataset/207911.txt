Timing may be a bit off, but quality shouldn’t be a problem for growers of cabbage, onions and other vegetable crops in New York, growers say.Jason Turek, partner in King Ferry, N.Y.-based Turek Farms, said his cabbage crop was in “phenomenal” shape, thanks to abundant rainfall during the growing season.
“Cabbage loves water — our problem is we’re backed up on planting,” he said. “We’re running a few weeks late on pulling stuff out of the ground to planting.”
The cabbage crop was shaping up well, but it will be a little late this year because of rain at Stanley, N.Y.-based Hansen Farms LLC, said Eric Hansen, vice president.
The wet conditions complicate planting the crop, Hansen said.
“Sadly, we are in another extreme year. In this case it’s wet — I much prefer dry,” he said.
“We normally start fresh cabbage in early July, but we will be late July this year due to the wet spring. We are still selling storage cabbage from our facility.”
Hansen said the wet weather would cut his cabbage acreage down by about 20% this year.
“The crop we have planted looks great,” he said.
“The New York cabbage production has stood the test of time. Sure, we have had some extreme weather in the last few years, but we always produce a crop.”
Maureen Torrey, vice president of Elba, N.Y.-based Torrey Farms Inc., agreed that the abundant rainfall had helped the cabbage this year.
“The cabbage crop looks pretty good,” she said. “We’ve had good weather for it.”
The first cabbage would be cut in mid-July, Torrey said.
In Holley, N.Y., James J. Piedimonte & Sons and Anthony J. Piedimonte/Cabbco were still planting cabbage, along with other vegetables, in late June, said Tony Piedimonte, owner.
“We’re still planting cabbage, cucumbers, squash, corn and beans, so I imagine we’re going to run some of this stuff for a while yet,” he said.
The fields’ location could help the crops do some catching up, though, Piedimonte said.
“We’re a little closer to the lake, so get a little hustle from Mother Nature,” he said.
Rain has been plentiful, but it was a bit cool through June, Piedimonte said.
“We could stand, with all the rain, a little more heat,” he said in the last week of June.
“We had been in the 80s, but it cooled off — maybe 5 to 10 degrees from usual,” he said.
Eden, N.Y.-based Eden Valley Growers started its first cabbage cutting around June 26, said Gary Balone, manager.
“Looks excellent,” Balone said.
Eden Valley grows about 45 items, including bell peppers. Everything looked good, Balone said.
“It’s better than last year — just slow coming, in a nutshell,” he said.
Torrey, who grows on about 14,000 acres, agreed, saying her onions, cucumbers and squash appeared to be “very good.”
There are usual problems, but nothing serious, Torrey said.
New York’s vegetable deal, for many growers, runs into late November or early December.
Piedimonte said his normal plantings of cucumbers and peppers were done.
“We got eggplant and peppers in a couple of weeks late,” he said.
Marion, N.Y.-based Williams Farms LLC planted potatoes the last week of June, said Erica Buttaccio, assistant.
“Everything is going to be much later than typical,” she said.
Onion plantings were delayed this year, Buttaccio said.
“We planted about 103 acres of onions this year and didn’t get our first onion in the ground until May 12. Last year, we were done planting by May 12.”
Potatoes will also be later than last year’s early September start, she said.
“This year it’s going to be closer to the beginning of October,” she said, noting that the crop includes gold, white and red potatoes.