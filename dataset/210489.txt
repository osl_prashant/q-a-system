Q. What causes a fall slump in dairy production?


A.  It’s common for dairy producers to experience a “fall slump,” or a time when cows fail to reach their full production potential. This is usually caused by an abrupt change from old corn silage to recently fermented corn silage.


A contributing factor to a fall slump is lower starch digestibility in fresh corn silage. When harvested above 35 percent dry matter (DM), the starch in corn starts to become less digestible. Flint varieties also have lower starch digestibility than floury ones. 


To help improve starch digestibility, allow corn to ensile for at least four months before feeding. As silage spends additional weeks in storage, the starch becomes more digestible. You can also use a research-proven silage inoculant containing enzymes to help break down plant fiber and increase digestibility. Look for enzyme activity levels declared on the product label and research supporting effects on digestibility when choosing forage inoculants.


Fresh forages also can contain high levels of fermentable sugars, which can contribute to Sub Acute Ruminal Acidosis (SARA) in a herd, which can impair the animal’s ability to use the ration efficiently. Including an active dry yeast (ADY) probiotic can help optimize rumen function during this period.


Finally, silages are different from one silo to the next and from one year to the next. Change silos gradually over a two- or four-week period. Test new forages for DM, kernel processing score and nutrient content and adjust the ration based on the changing composition.


Understanding the reasons for a fall slump in milk production can help you make adjustments to keep milk production steady and maintain cattle health and fertility.


For additional silage tips, visit www.qualitysilage.com or Ask the Silage Dr. on Twitter or Facebook.