The first hours of a calf's life set the course of that calf's health for the remainder of its life. It's always critical to handle and feed colostrum properly, but because calves are under extra stress when they are born in the winter, it's extra important this time of year. Here are eight colostrum handling reminders from Dr. Sheila McGuirk. This list could even be printed as a reminder for employees.1. Colostrum must be clean and must be harvested early.
2. Clean colostrum doesn't mean sterile colostrum. We want good bacteria to be present; we don't want blood or manure.
3. A calf needs to consume enough colostrum to equal 10% of their body weight within four hours of birth.
4. Staff needs proper training to tube calves that might need it.
5. You can only refrigerate colostrum for three days. On the third day, stick it in the freezer if it's high quality.
6. When you thaw frozen colostrum, you need to warm it to body temperature before feeding it to calves.
7. Warm colostrum stabilizes the body temperature of a calf.
8. A calf nursing its mother is not effective!