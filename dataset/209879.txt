Aspen M. Silva, of Modesto, Calif., led Miss Nastias Y Not Me to the title of Grand Champion of the 2017 International Junior Jersey show on Wednesday, October 4 at World Dairy Expo. She was the winner of the Senior-Three-Year-Old Cow Class and then went on to be named Intermediate Champion, followed by the honor of Grand Champion. She took home the $500 Udder Comfort Grand Champion Cash Award and the Lillian & Keith King and Jim King Grand Champion Junior Show Award. Reserve Grand Champion and winner of the Five-Year-Old Cow Class was Hazelcrest Governor Molly, exhibited by Nicole Sanders of Hilmar, Calif. Molly took home the Lillian & Keith King and Jim King Reserve Grand Champion Junior Show Award.
104 animals were placed by official judge Mike Berry, Albany, Ore. and associate judge Corey Couch, Victoria, Australia.

Overall show results and top class placing are as follows:
Spring Heifer Calf:
1. Meadowridge King Sunflower-ET, Michael Riebe and Alleah, Emma & Jordyn Anderson, Cumberland, Wis.
2. Miss Triple-T Andreas Rapunzel, Colton Thomas, North Lewisburg, Ohio
Winter Heifer Calf:
1. Meadowridge Fizz Sparkle, Cole Kruse and Gene Henderson, Dyersville, Iowa
2. SV Healths Colton Judith, Renee Pierick, Westminster, Md. 
Fall Heifer Calf:
1. Riview Ready Set Glo, Alana McKinven and Amelie & Charlotte Borba, Canton De Hatley, Qué.
2. Avonlea Fizz’s Vendetta, Avonlea Genetics and Taylor & Will Vandermeulen, Brighton, Ont. 
Summer Yearling Heifer:
1. Avonlea Velocity Kiki, Avonlea Genetics and Taylor & Will Vandermeulen, Brighton, Ont.
2. SV Colton Highway-ET, Shelby Ostrom, Kaukauna, Wis. 
Spring Yearling Heifer:
1. LLR Premier Too Legit-ET, Madison Iager, Woodbine, Md.
2. Stoney Point Showdown Sabrina, Noah Bilz, Dorchester, Wis. 
Winter Yearling Heifer:
1. Meadowridge Bang Bang Hannah, Roger Riebe and Alleah & Jordyn Anderson, Cumberland, Wis.
2. Homeridge HG Buttons, Landree Fraley, Muncy, Pa. 
Fall Yearling Heifer:
1. Topline Tequila Champagne, Carly N Shaw, Fairplay, Md.
2. Pozzi Hired Gun Star-ET, Steven & Regina Pozzi, Petaluma, Calif. 
Junior Champion Female:
LLR Premier Too Legit-ET, Madison Iager, Woodbine, Md. 
Reserve Junior Champion Female:
Meadowridge Fizz Sparkle, Cole Kruse and Gene Henderson, Dyersville, Iowa 
Yearling Heifer in Milk:
1. Charlyn Tequila Eclipse, Dawson & Kylie Nickels, Watertown, Wis.
2. Family Hill Tequila Night, Gabbie Gregorio, Acampo, Calif. 
Junior Two-Year-Old Cow:
1. Meadowridge Kasanova Sara, Michael Riebe and Alleah Anderson, Cumberland, Wis.
2. Wildweed Iwrin Speckles, Samantha Pitterle, Fox Lake, Wis. 
Senior Two-Year-Old Cow:
1. Fire-Lake Premier Meg, Joshua, Hannah, Nicole & Rebecca Sanders, Hilmar, Calif.
2. Lookout Gunning For Fame, Corey Foster, Elizabeth Sutton and Haley Foster, Cleveland N.C. 
Junior Three-Year-Old Cow:
1. Lake-Point Premier Valyria, G, G, H & E Fremstad, Westby, Wis.
2. Oeh-My Premier Penelope, Chase & Willow Oehmichen, Abbotsford, Wis. 
Senior Three-Year-Old Cow:
1. Miss Nastias Y Not Me, Aspen M Silva, Modesto, Calif.
2. True View Edwin Takara, Jonathan Beiler, Fredericksburg, Pa. 
Intermediate Champion Female:
Miss Nastias Y Not Me, Aspen M. Silva, Modesto, Calif. 
Reserve Intermediate Champion Female:
Lake-Point Premier Valyria, G, G, H & E Fremstad, Westby, Wis. 
Four-Year-Old Cow:
1. LC Valentino Clover, Erin & Sophie Leach and Anna Hahn, Linwood Kan.
2. Reich-Dale Vaden Strollin, Hayden Reichard, Chambersburg, Pa.   
Five-Year-Old Cow:
1. Hazelcrest Governor Molly, Nicole Sanders, Hilmar, Calif.
2. Meadowridge Verbatim Claire, Roger Riebe and Alleah & Jordyn Anderson, Cumberland, Wis. 
Six-Year-Old and Older Cow:
1. Boddens Rosland, Benjamin Buske, Mayville, Wis.
2. Brickton Legacy Jalie, Abby Grimm, Milaca, Minn. 
Lifetime Cheese Production Cow
1. Willdina Jade Bee, River Valley Farm – Ben, Andy, Blessing, Grace Sauder, Tremont, Ill. 
Senior Champion Female:
Hazelcrest Governor Molly, Nicole Sanders, Hilmar, Calif. 
Reserve Senior Champion Female:
LC Valentino Clover, Erin & Sophie Leach and Anna Hahn, Linwood Kan. 
Grand Champion Female:
Miss Nastias Y Not Me, Aspen M. Silva, Modesto, Calif. 
Reserve Grand Champion Female:
Hazelcrest Governor Molly, Nicole Sanders, Hilmar, Calif. 
For over five decades, the global dairy industry has been meeting in Madison, Wis. for World Dairy Expo. Crowds of nearly 75,000 people from more than 100 countries attended the annual event in 2016. WDE will return Oct. 3-7, 2017 as attendees and exhibitors are encouraged to “Discover New Dairy Worlds.” Visit worlddairyexpo.com or follow us on Facebook and Twitter (@WDExpo or #WDE2017) for more information.