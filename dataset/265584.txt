McConnell wants hemp removed from controlled substance list
McConnell wants hemp removed from controlled substance list

By BRUCE SCHREINERAssociated Press
The Associated Press

FRANKFORT, Ky.




FRANKFORT, Ky. (AP) — The U.S. Senate's top leader said Monday he wants to bring hemp production back into the national mainstream by removing it from the list of controlled substances.
Senate Majority Leader Mitch McConnell told a group of hemp advocates in his home state of Kentucky that he will introduce legislation to legalize the crop as an agricultural commodity. The versatile crop has been grown on an experimental basis in a number of states in recent years.
"It's now time to take the final step and make this a legal crop," McConnell said.
Kentucky has been at the forefront of hemp's comeback. Kentucky agriculture officials recently approved about 12,000 acres to be grown in the state this year, and 57 Kentucky processors are turning the raw product into a multitude of products.
Growing hemp without a federal permit has long been banned due to its classification as a controlled substance related to marijuana. Hemp and marijuana are the same species, but hemp has a negligible amount of THC, the psychoactive compound that gives marijuana users a high.
Hemp got a limited reprieve with the 2014 federal Farm Bill, which allows state agriculture departments to designate hemp projects for research and development. So far, more than 30 states have authorized hemp research.
McConnell acknowledged there was "some queasiness" about hemp when the 2014 Farm Bill cleared the way for states to regulate it for research and pilot programs. There's much broader understanding now that hemp is a "totally different" plant than its illicit cousin, he said.
"I think we've worked our way through the education process of making sure everybody understands this is really a different plant," the Republican said.
McConnell said he plans to have those discussions with Attorney General Jeff Sessions to emphasize the differences between the plants. The Trump administration has taken a tougher stance on marijuana.