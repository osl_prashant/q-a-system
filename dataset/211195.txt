Bred heifer prices defied the typical seasonal slump during July with a $190 per head increase in the Drovers monthly auction summary. Other female classes saw mixed prices, though the variation higher or lower was modest.The July market confirms demand for higher quality bred heifers, and all females, continues strong. Female markets were influenced by a stronger than expected market for feeder cattle and calves, which was spurred by excellent feedyard profits the first half of 2017. However, analysts warn beef supplies will increase in the coming months, pressuring prices for all classes of cattle. 

 
Open heifers gained a modest $1 per cwt during July, with the young and middle-aged open females gaining $2 per cwt. A young, 1,000-lb. open cow cost about $920 last month, compared with about $945 a year ago. 
Bred heifers saw the lowest activity in Drovers nation-wide auction summary during July, with the average price at $1,440 per head. The price was about $140 higher than July 2016. Young and middle-aged bred cows averaged $50 per head lower in July, and $73 lower than July 2016. Aged, bred females were $60 per head lower in the July Drovers auction survey. 
Price trends for cow-calf pairs were strong. Cows with small calves showed a gain of $83 per pair, and the cows with large calves gained $60 per pair. Both categories are about $15 per pair higher than last year. Small or aged cows with calves declined $50 per set. Pairs suitable to go back to the country sold steady to $15 per pair more than during July of 2016.

Slaughter cows were steady to $1 per cwt higher last month. Utility and commercial cows traded 50¢ higher during July. Canner and cutter cows sold $1 per cwt higher.
 
This article was featured in the August issue of Drovers magazine.