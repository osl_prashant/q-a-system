Emergency personnel prepare for wildfires in parts of Texas
Emergency personnel prepare for wildfires in parts of Texas

By JON MARK BEILUEAmarillo Globe-News
The Associated Press

AMARILLO, Texas




AMARILLO, Texas (AP) — It was a bad day — a very bad day.
The Amarillo Globe-News reports earlier in the morning, wildfires broke out in the western part of the Texas Panhandle in Oldham County near Vega. They moved south into Deaf Smith County and threatened the outskirts of Hereford.
Then separate wildfires started in Randall County, and winds nearing 50 mph were moving fire dangerously close to the villages of Timber Creek, Palisades and Lake Tanglewood.
Then, to make it worse, in the eastern part of the Panhandle, wildfires in Wheeler County began to consume ranch land. That thick dry fuel fed fires that raged out of control for miles. The Panhandle was essentially in flames.
That terrible day was Feb. 13. If that escaped your attention, perhaps an explanation is needed. There were no real wildfires that day, only false scenarios set up as an exercise for more than 100 people and 50 agencies in the Texas Panhandle whose job it is to combat wildfires.
"That's one of the best ones I've been a part of in years," said James Amerson, Randall County Fire Chief.
'Tis the season — not for holiday greetings, but for natural disasters that are almost replacing tornadoes as a scourge on the Panhandle. March is the unofficial beginning of wildfire season, when spring winds combine with low humidity that can turn thousands of acres into a tinderbox.
Tuesday was the one-year anniversary of the tragic wildfires that consumed 480,000 acres in the eastern and northeastern Panhandle. The fires took the lives of four, three trying to rescue cattle on the Franklin Ranch near McLean, and the other trying to reach home and his wife along Highway 305 near Higgins. More than 1,500 cattle were killed.
For veteran firefighters and emergency managers, this season has an ominous feel, that factors have come together to make 2018 a near perfect storm for fires.
"We get into March and the weather patterns we've had, we're going to be looking at for sure a devastating wildfire season," said Cody Holloway, regional fire coordinator with the Texas A&M Forest Service based in Amarillo. "Last year, we had lot of fuel (grass) and a lot of this country burned. This year, it's even worse."
The benchmarks for wildfire devastation in the last dozen years are:
— March 2006, 13 dead, 10,000 horses and cattle dead, 840,000 acres burned in the eastern Panhandle.
— Feb. 27-March 1 2011, 1 dead, 70 area homes in Tanglewood complex and Willow Creek destroyed, $13 million in damage.
— Memorial Day weekend, 2011, 13 homes destroyed on edges of Amarillo, 1,000 acres burned south of Stinnett.
— May 2014, 225 homes and 118 vehicles destroyed, 2,583 acres burned in Double Diamond area near Lake Meredith and Fritch.
"I'll be honest, we're waiting for 'that one,'" said Chip Orton, emergency services manager for Amarillo and Potter and Randall counties. "We haven't had those extreme weather conditions yet, but they're coming."
Those conditions are winds around 50 mph and single-digit humidity. Tall grass, the fuel for fires, is in abundance because of the third-wettest August in history that saw 7.40 inches of rain. By the end of that month, the Amarillo National Weather Service reported 20.88 inches of rain for 2017.
But then it quit. Amarillo crushed the record of consecutive days without moisture of 75 with 126 days of no precipitation. The .03 in mid-February stopped the streak, but did nothing to alleviate drought. And that tall grass is now dry and a huge fire accelerant waiting on that one day.
"All indicators are this is going to be a very active year," Amerson said. "You're on edge, but this time of year, we all are. We're in the busy time of the year for us, but we're ready to go."
Agencies try to be proactive to limit fire damage. Since the first of the year, there have been as many as a half-dozen controlled or prescribed burns in the area, fires intentionally set and monitored to reduce the grass fuel that could help ignite a big one. In the past, controlled burns in the Panhandle have been rare.
"Like in 2006 and 2011, you can't get in front of those fires," said Mark Mitchell, manager of the Crutch Ranch, an 18-section ranch east of Borger. "You've got to be smarter and do things like this (controlled burns). If someone told me 10 years ago we're going to put fire on your ranch, I'd throw rocks at them. But it's so hard to get in front of these fires, this can be a good way to prevent trouble."
The Crutch Ranch is one of several large area ranches that have their own firefighting equipment. Since 2010, the Crutch Ranch has had its own state licensed fire department with nine on the roster, and seven that are active. Mitchell is fire chief.
"It started out as neighbor helping neighbor and has morphed into a lot more than that," Mitchell said. "We've started working with other ranches with training and being proactive."
The A&M Forest Service helps supplement local county, city and volunteer firefighting agencies in moving equipment across the state to areas where weather reports show danger. The closest branches, in addition to Amarillo, are in the Lubbock area, Childress and Wichita Falls.
"If we think we have a bull's-eye in, say, Canadian, we'll send a strike team of dozers and others up there and stay for a week if we have to," Holloway said. "We're pre-positioned. We'll park in a parking lot, stay in a motel, and wait for a fire if that's what it takes."
Weather forecasts and red flag warnings are the lifeblood to firefighting agencies and other first responders. Like forecasting storms, that has improved over the years.
"The tool they've created for us," Amerson said, "when they tell you it's going to be an outbreak day, they're nearly always right."
Mike Gittinger of the National Weather Service in Amarillo said much of those warning forecasts depend on the models.
"It can be very subtle things that affect that," he said, "but if the models are doing well, we can potentially see bad days four to five days out. Usually, if you start getting more than three days out, it can get iffy. But if we see a potential of an outbreak with high winds and low humidity, we may start sending briefings four of five days out to heighten awareness."
Gittinger said this fire season could extend into April and May. Because of a lack of ground moisture and rain, grass won't green up until later than normal. In 2011, it was the driest year on record with just more than 7 inches of rain, and fires reached the edge of Amarillo on Memorial Day weekend in late May.
"So windy days in April will continue to be a problem," he said. "April and May are going to be interesting."
The NWS set in motion an exercise in mid-February that involved representatives from all firefighting agencies in the Panhandle. High winds and low humidity sparked fires all over the area that forced agencies to act in real time.
The purpose? Agencies studied the effectiveness of weather briefs, got all in rooms to put faces with names and agencies, and then created taxing scenarios to see the effectiveness of communication.
"That's the key to everything," Orton said of communication, "and one of the things we struggle with the most. Are you completely prepared for a nuclear accident or an active shooter situation? The answer is probably 'no,' you never totally are. But we are as prepared as we can be. We plan, train and exercise as much as possible."
But for many in the Panhandle, confronting wildfires has gone beyond playing out scenarios to the real thing — putting out flames that can cover a mile in eight minutes.
"Whether you are paid or volunteer, experience is a huge role in how you move forward," Holloway said. "You are able to add tools to your tool box, so to speak. In being able to see a large fire, work that fire, get it suppressed and be ready for the next time, that's huge."
So many tasked with that across the Panhandle wait — hoping for the best, but expecting the worst.
___
Information from: Amarillo Globe-News, http://www.amarillo.com


This is an AP Member Exchange shared by the Amarillo Globe-News