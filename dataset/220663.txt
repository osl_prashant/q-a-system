Logging 35 million miles per year, the 400 drivers for Lachine, Quebec-based Trans-West specialize in hauling produce from U.S. shipping regions in the Quebec.
“What we tell our drivers all the time is that we don’t bring just any tomato or onion, we bring fresh produce for our grandparents, our children and our neighbors,” said Andre Boisvert, vice-president of technology and development for the company.
Trans-West has been named as “One of the 50 Best Managed Companies in Canada,” by CIBC and Deloitte for the past six years, and was recognized as the second safest carrier in North America by the Truckload Carrier’s Association (in the 25 million-49.99 million mile category) in 2016.
While the electronic logging device mandate in has created issues for some independent operators in the U.S., Boisvert said Feb. 13 that Trans-West has been able to comply with the mandate, relying on team drivers — two drivers in one rig — to haul produce nearly nonstop. Canada does not have the regulation.
“We managed to train everybody by Dec. 18 and we don’t have too many problems,” he said. “We use team drivers so we are not over-logged (for hours of service).”
With trucks running around-the-clock, the company has 25 to 30 daily departures from Quebec to California. The 3,000 mile run takes about 2 1/2 days, with a round trip taking between 5 1/2 to 6 1/2 days.
The company operates 150 trucks and 250 refrigerated trailers, and hauls loads from the Quebec area to the west coasts of Canada and the U.S., as well as Florida, then backhauls produce to Quebec and Ontario.
 
Expansion mode
Boisvert said the company owns some of its own trucks, typically running them for about three years and then selling them. In recent years, the company has leaned more heavily on leased Kenworth T680s from PacLease. Now with about 120 PacLease trucks, the company is adding 40 more, Boisvert said.
“Since our trucks averaged 300,000 miles a year, they were rarely ‘home,’ and we couldn’t afford breakdowns and the headache of what to do if a truck did go down,” he said.
If there is an issue on the road, PacLease can direct the truck to one of its locations for service, Boisvert said in the release.
 
Challenges
The company could easily double the number of trucks except for a driver shortage, he said Feb. 13. Finding drivers willing to be on the road for six days at a time and with the ability to sleep while the truck is running isn’t easy, he said. “We have the work (to double truck numbers), we have the customers for it. It is the drivers we have a problem with.”
Truck rates for produce shipments to Quebec have been strong early this year and Boisvert said market conditions are expected to ebb and flow with weather and volumes coming out shipping regions.
But with 40 more trucks on the way and new drivers being added, Boisvert said the company is looking forward to a strong year and more produce loads from the U.S.