BC-US--Cotton, US
BC-US--Cotton, US

The Associated Press

New York




New York (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Friday:
(50,000 lbs.; cents per lb.)
COTTON NO.2Open    High    Low    Settle     Chg.Mar       80.75   81.50   80.25   81.45  Up    .91May       79.37   81.42   79.37   81.34  Up   1.87Jul       80.38   82.15   80.31   82.07  Up   1.66Aug                               76.60  Up    .24Oct       77.02   78.04   77.02   78.04  Up    .62Oct                               76.60  Up    .24Dec       76.30   76.68   76.18   76.60  Up    .24Dec                               76.80  Up    .21Mar       76.49   76.88   76.41   76.80  Up    .21May       76.40   76.66   76.40   76.59  Up    .12Jul       76.30   76.40   76.30   76.38  Up    .06Aug                               71.50  Up    .01Oct                               74.13  Up    .07Oct                               71.50  Up    .01Dec       71.50   71.50   71.29   71.50  Up    .01Dec                               71.95  Down  .01Mar                               71.95  Down  .01May                               72.80  Down  .04Jul                               73.13  Down  .08Aug                               73.06  Down  .15Oct                               73.09  Down  .12Oct                               73.06  Down  .15Dec                               73.06  Down  .15