Frieda's Speciality Produce is catering to Lent-observing Latin shoppers with meatless options.
The Los Alamitos, Calif.-based company is promoting Soyrizo, SoyTaco, cactus leaves, fresh poblano and anaheim peppers as well as dried peppers including guajillo, ancho mulato, and pasilla negro.
"Lent is becoming a big food holiday as the culture and demographic continue to shift," said Karen Caplan, president and CEO of Frieda's Specialty Produce in the release. "There is an opportunity here for retailers to offer authentic Latin produce and capture that Hispanic buying power during Lent and afterward."
According to the release, 26% of Americans observe Lent and abstain from meat (except fish and seafood) on Fridays during the 40 days of Lent.

Lent starts Ash Wednesday, March 1.