The Newark, Del.-based Produce Marketing Association is taking applications until March 24 for the group's new Science & Technology Circle of Excellence Award.
 
The award aims to recognize "those who are solving business challenges by applying advances in science and/or technology to create better products, processes or customer value," according to a news release.
 
"Science and technology are playing an ever-important role in our industry, particularly to help us solve labor challenges, work smarter and create new business opportunities," PMA chief science and technology officer Bob Whitaker said in the release. "By recognizing innovators in this area, we hope to inspire our industry to follow their lead - that's why the award is named 'Circle of Excellence.'"
 
A panel, including PMA staff and industry executives, will select the award winner.
 
The award winner will be announced at PMA's Tech Knowledge Conference May 4-5 in Monterey, Calif., according to the release.
 
For more information and to apply for the Science & Technology Circle of Excellence Award, visit the PMA website.