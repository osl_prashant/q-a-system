Global dairy prices rose for the sixth time in a row in fortnightly auctions, a sign that the recovery in dairy in 2017 was on track.The Global Dairy Trade (GDT) Price Index climbed 0.6 percent, with an average selling price of $3,395 per tonne, in the auction held on Tuesday.
After two years of declining prices, farmers and analysts had been concerned that a 50 percent rebound during 2016 could be temporary. Prices were also dented at the beginning of the new year as global supply increased.
Much of the gain was from cheddar cheese, which rose 14.5 percent to its highest in nearly three years, while butter was up 3.3 percent.
Nevertheless, there were signs that prices might start to moderate as more supply of milk powder came on the market.
"Milk output from NZ is expected to lift this season and Fonterra has already made a small upwards revision in the volume of WMP [whole milk powder] expected to be sold on GDT next season," said Amy Castleton, analyst at AgriHQ.
GDT Events is owned by New Zealand’s Fonterra Co-operative Group Ltd , the world's largest exporter of dairy, but operates independently from the company.
Whole milk powder edged down 2.9 percent at the latest auction, in line with market expectations.
The auction results can affect the New Zealand dollar as the dairy sector generates more than 7 percent of the nation's gross domestic product.
The Kiwi rose to a three-month high of $0.7184 from around $0.7170.
A total of 22,004 tonnes was sold at the latest auction, an increase of 3.6 percent from the previous one.
U.S.-listed CRA International Inc is the trading manager for the twice-monthly Global Dairy Trade auction.
The auctions are held twice a month, with the next one scheduled for June 20.