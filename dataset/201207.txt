Anhydrous is $91.50 below year-ago pricing -- lower $3.23/st this week at $489.63.
Urea is $28.04 below the same time last year -- lower 81 cents/st this week to $334.69.
UAN28% is $39.87 below year-ago -- lower $6.06/st this week to $238.07.
UAN32% is priced $39.11 below last year -- lower 87 cents/st this week at $260.52.

UAN28% was our downside leader in the nitrogen segment this week with no state posting a higher 28% bid. Three of the twelve states in our survey were unchanged as Missouri softened $42.11, Nebraska fell $9.25 and Kansas dropped $7.18.
Anhydrous ammonia fell under the weight of Nebraska which softened $19.52 as Kansas fell $10.65. Five of twelve states were unchanged as Iowa firmed 22 cents per short ton, our only state to post higher prices.
UAN32% was led lower by Nebraska, which fell $6.64 as Kansas softened $2.55. Eight states were unchanged as, once again, Iowa posted our only gains, adding 7 cents per short ton.
Urea was lower on declines in Nebraska to the tune of $12.50 as Kansas fell $2.65 and Wisconsin softened $1.57. Three states were unchanged as Missouri gained $4.42 and Illinois added $3.35.
Reports have surfaced that world nitrogen stocks may begin to fall around the world. If low prices cure low prices, then foreign producers are writing what they believe will be the prescription to remedy lagging world nitrogen prices. Producers in Ukraine and Russia have reportedly enacted nitrogen plant shutdowns recently as profit margins have fallen under the weight of a mild world nitrogen oversupply. Adding to concerns is the startup of a Saudi phosphate project due later this year which will rely on ammonia for feedstock from supplies which currently serve the export market. Trinidad completes the hat trick of concerns as ammonia producers in that nation face growing uncertainty about natural gas availability.
We have seen this before, and before we get too nervous, it is important to remember that U.S. domestic nitrogen capacity is set to rise in roughly equal measure to the declines expected from FSU curtailments, increased phosphate manufacturing and troubles in Trinidad. This may insulate the United States from a dramatic price increase, especially since anhydrous ammonia is something of a boutique product in the U.S. I must also pose the current fundamentals in potash as something of an allegory here. FSU producers in 2013 went on a production binge which forced potash prices to plummet and led to North American producers curtailing production and laying workers off. Those North American producers had hoped they could stem the tide of oversupply by better managing production, and whittling away at world supplies. SO far, no dice.
The current situation for nitrogen is the opposite. As North American nitrogen production begins to increase, FSU producers are curtailing N production in an attempt to achieve what potash producers could not -- genuine worldwide supply rebalancing. This is supply-side management on display, and one wonders, if OPEC can't stem the crude oil oversupply by selectively moderating production, how can individual nitrogen producers expect to do any better? When one plant or company slows output, others have shown themselves to be more than willing -- given the economics work -- to fill the supply gap. The FSU producers have said they will continue to export nitrogen, just at slightly lower levels.
I suspect supply disruptions in Trinidad hold the greatest threat to U.S. retail fertilizer prices, but since that country exports raw, industrial ammonia, a disruption there would be felt more by producers of industrial products or phosphate fertilizer.
So, while I am not calling the news of nitrogen production slowdowns either gamesmanship or crisis, it does give us reason to remain diligent in watching wholesale markets. Retail nitrogen prices are giving some clear, proven signals of near-term price softness. We will wait until the market proves it has legs to firm on before we book for upcoming nitrogen needs. Having said that, when we pull the trigger on nitrogen for fall applications, I do feel it prudent to book some spring 2018 supplies at that same time. It will take awhile to rebalance world nitrogen supplies. Domestic production will fill the supply gap here in the U.S. and will blunt the upside risk. But there is real danger that nitrogen prices could firm ahead of spring 2018 applications, and that is where our concern in all of this lies.
December 2017 corn closed at $3.92 on Friday, June 30. That places expected new-crop revenue (eNCR) per acre based on Dec '17 futures at $621.69 with the eNCR17/NH3 spread at -132.06 with anhydrous ammonia priced at a discount to expected new-crop revenue. The spread widened 32.02 points on the week.





Nitrogen pricing by pound of N 7/5/17


Anhydrous $N/lb


Urea $N/lb


UAN28 $N/lb


UAN32 $N/lb



Midwest Average

$0.29 3/4 


$0.37 1/2


$0.42 1/2


$0.41



Year-ago

$0.35 1/4


$0.40 1/4


$0.49 1/2


$0.47





The Margins by lb/N -- UAN32% is at a 1 1/4 cent premium to NH3. Urea is 2 3/4 cents above anhydrous ammonia; UAN28% solution is priced 3/4 cent above NH3.





Nitrogen


Expected Margin


Current Price by the Pound of N


Actual Margin This Week


Outstanding Spread




Anhydrous Ammonia (NH3)


0


29 3/4 cents


0


0




Urea


NH3 +5 cents


37 1/2 cents


+7 3/4 cents


+2 3/4 cents




UAN28%


NH3 +12 cents


42 1/2 cents


+12 3/4 cents


+3/4 cent




UAN32%


NH3 +10 cents


41 cents


+11 1/4 cents


+1 1/4 cents