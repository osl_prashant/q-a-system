Grain mostly higher and livestock mixed
Grain mostly higher and livestock mixed

The Associated Press



Wheat for May was up 11 cents at 4.7250 a bushel; May corn rose 2.75 cents at 3.8125 a bushel; May oats fell 2.50 cents at $2.2425 a bushel; while May soybeans gained 1.50 cents at $10.2225 a bushel.
Beef was mixed and pork was lower on the Chicago Mercantile Exchange. April live cattle was off .62 cent at $1.2110 a pound; April feeder cattle rose .08 cent at 1.3910 a pound; while May lean hogs fell .55 cent at $.6745 a pound.