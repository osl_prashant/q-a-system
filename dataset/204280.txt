During a tough farm economy, it's easy to focus on the many challenges of business. Yet, is that daunting list blinding you to new business-growth prospects?"We have a tendency during economic downturns to not think about the opportunities," explains Mike Boehlje, ag economist at Purdue University. Down markets can open doors such as reduced values for assets including land and machinery, acquisitions, new business ventures and services, and changing needs of your customers in the area.
These changes provide a huge opportunity for strong operations to grow, says Jill Eberhart, principal with K-Coe Isom. Ag businesses must think strategically about which ventures to pursue.
Beyond having a firm understanding of your operation's financial standing and long-term goals, she says, you must also get support from employees and key stakeholders.
"You need to get ownership to buy into what it will mean if your operation grows, as sometimes you are sacrificing short-term profit for long-term growth," she says.
Also inform your key advisers, and seek counsel from them and peers about future growth.
"These people can help troubleshoot and challenge your plan," Eberhart says.
Since growth will likely take additional investment, she suggests sharing growth strategies with your lender to make sure they are comfortable with your vision. This will help you move quickly if someone approaches you to purchase their land or if someone offers you a partnership opportunity.

10 QUESTIONS FOR A BUSINESS BUILD-OUT
As part of due diligence in crafting your growth strategy, Purdue University ag economist Mike Boehlje advises a critical self-appraisal. Consider these questions before you commit to new or expanded ventures.
 1. Why grow? And why not? Do you plan to reduce costs, bring in new employees or invest cash? Could this new venture create too many responsibilities or spread resources too thin?
 2. What are your growth options? In what way should you expand your business? Can you add other stages of the value chain? Would the venture allow you to diversify or specialize?
 3. What strategic issues should influence your growth choices? Do you see a strong or new market potential? Does the venture maximize your team's competencies and capabilities? What is the competition?
 4. How should ventures be evaluated? What is the strategic fit? Do you expect more growth? How will you measure success?
 5. What skills and competencies do you need to grow? Skills can be more limiting than capital, Boehlje says. Will the new venture challenge your team too much? Do you need to develop greater management capacity?
 6. How do you finance the growth? What's the affect on equity or debt? Can you reduce the investment with a joint venture?
 7. What business model fits best? Should you consider a merger or acquisition, a franchise, a partnership or investor?
 8. How will expansion impact your current operation? Will it improve efficiencies and lower costs? Will it provide better asset utilization? Will it divert your leadership's attention?
 9. What are the start-up/scale-up challenges? Will you face construction delays or cash-flow shortages? How will it affect working capital?
10. What is your sustainable growth rate? What are your expected earnings and rate of return for the venture? What withdrawals will you make? How will it affect your debt use or leverage?
This article was published in the October issue of Ag Pro.
Click here to subscribe to Ag Pro magazine.