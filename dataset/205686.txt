The nation's top corn producing state saw another drop in farmland values this year, marking the third straight year of declining values in Iowa.Economists at Iowa State University said the last time that happened was during the 1980s.
The annual study shows prices fell 5.9 percent this year. The acreage price is less and $7,200 an acre.
Compared to the peak in 2014, that a nearly 18 percent plunge.

Click here for a video report from AgDay.
Click here for a video report from AgDay.