Houston-based Lone Star Fruit & Vegetable Inc. has posted a $60,000 surety bond to employ John Honeycutt, previously named in a Perishable Agricultural Commodities Act action.
Lone Star posted the bond with the U.S. Department of Agriculture per PACA regulations, according to a USDA news release.
Honeycutt was an officer of Third Coast Produce Co. Ltd., Houston, which violated Section 2 of the PACA, according to the USDA.
Third Coast was sanctioned by the USDA in 2012 for failing to pay $514,943 to 21 sellers for 207 lots of produce over the first half of 2010.