(UPDATED, Nov. 14) China is expected to demand more apple imports from the U.S. and other countries in 2017-18, according to a new report from the U.S. Department of Agriculture.
The report said that China’s apple production is nearly steady with a year ago. At 44.5 million metric tons, China’s apple output is just 1% higher than 2016, according to the report.
Chinese pear production is predicted to increase 2% to 19 million metric tons and grape production will rise 4% to 11.2 million metric tons.
 
China apple imports rising
The U.S. is one of the top suppliers of imported apples to China and will benefit from expanded demand this year, according to the USDA report.
China’s total apple imports are forecast at 80,000 metric tons in the 2017-18 marketing season, up 14% from 70,000 metric tons in 2016-17, according to the report. In 2016-17, the U.S. exported about 17,000 metric tons of apples to China.
Strong demand for high quality apples — combined with a drought in China that has limited supply of superior locally produced fruit — both will play a factor in the expected rise in Chinese apple imports, according to the USDA.
Demand from China for Washington apples starts in earnest in December and continues strong through April, said Todd Fryhover, president of the Washington Apple Commission.
Reds, galas and granny smith are the preferred varieties there, he said. 
“China has a very large middle class that is growing every year,” he said. “Consumers are gravitating toward higher value products.”
Meanwhile, the USDA projects that China’s grape imports will increase by 5% to 250,000 metric tons in 2017-18.
The demand for off-season table grapes is expected to continue growing, according to the USDA, with leading import supplies from Chile, Peru and Australia.
China’s pear imports are forecast at 6,000 metric tons in 2017-18, down 13% from the 2016-17 season.
Varieties of imported pears are not the same as locally-produced Asian pears and are not particularly favored by Chinese consumers, according to the report. Main imported pear suppliers include Belgium, Argentina, and the U.S., according to the report.