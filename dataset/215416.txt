The Hass Avocado Board has a new executive committee, following the U.S. Department of Agriculture’s new board member appointments.
Scott Bauwens of grower Simpatica, Camarillo, Calif., is the new chairman, succeeding Del Monte Fresh Produce’s Chris Henry, after two years in the position, according to a news release from the board.
Other executive committee positions are:

Vice-chairman, Jorge Hernandez, importer;
Treasurer re-elect, Laurie Luschei, producer; and
Secretary re-elect, Javier Medina, importer.

Board members and alternates chosen to serve are:

Susan Pinkerton, grower from Strata Holding LP;
Ben Van Der Kar, growers from Pinehill Ranch;
Sergio Chavez, importer from Prometo Produce;
C.J. Shade, grower from Shade Farm Management (alternate);
Will Carleton, grower from Las Palmitas Ranch (alternate);
Jeff Dickinson, grower from Dickinson Family Farms (alternate);
Bob Schaar, grower, (alternate);
Thomas Escalante, grower (alternate);
Jose Antonio Gomez, importer from Camposol Fresh USA (alternate); and
Nils Goldschmidt, importer from Terra Exports (alternate)