U.S. negotiators at talks to renew the North American Free Trade Agreement on Friday formally asked Canada to address a bilateral dispute over dairy pricing, a request the Canadians are set to resist, sources familiar with the talks said.
U.S. dairy producers are unhappy that Canadian farmers this year started selling milk proteins to domestic processors at a discount, curbing the flow of American imports. U.S. officials say Canadian farmers are overproducing milk, contributing to depressed world prices.
The United States wants Canada to act to curb output of the proteins, said one source, who asked to remain anonymous given the sensitivity of the situation.
Canada’s position remains that the farmers have done nothing wrong, added the source.
The dispute prompted U.S. President Donald Trump to declare in April that what Canadian farmers had “done to our dairy farm workers is a disgrace.”
Although the sources noted the American side had not made a demand for greater Canadian market access, U.S. officials said if such a request were to be made, it would be presented in a separate chapter of the talks.
They declined to comment when asked whether the U.S. team would ask for a bigger share of the Canadian market.
The United States has long protested against Canada’s system of tariffs and export limits designed to protect the domestic market. The so-called supply management system is not part of NAFTA.
The Dairy Farmers of Canada lobbying group said it still expected a U.S. demand for more access at some point during the negotiations.
“We do not see supply management as being on the table,” spokeswoman Isabelle Bouchard wrote in an e-mail, noting the government of Prime Minister Justin Trudeau has frequently promised to defend the dairy sector.