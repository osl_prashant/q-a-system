Quality is good on early-season California asian pears, but sizes are small.
Western Fresh Marketing Services Inc., Madera, Calif., began brokering yellow-skinned California asian pears in early July and began shipping brown-skinned fruit in late July, said salesman Joel Salazar.
That was a week to ten days earlier than normal, but the season still couldn't start soon enough, Salazar said, because of a very early end to the Chilean asian pear deal.
"It was good timing for California. There was no more fruit in the pipeline."
Los Angeles-based World Variety Produce, which markets under the Melissa's label, began shipping California asian pears the week of July 25 and expects to go through early March, said Robert Schueller, the company's director of public relations.
"Quality is great and pricing is in line the previous season."
On Aug. 9, fragrant 54s from China sold for $22-24 on the Los Angeles terminal market. Last year at the same time, yali 72s sold for $24-28.
New for Melissa's in asian pear packaging are 4-count clamshells, Schueller said.
Quality has been very good so far on California fruit this season, Salazar said. The only downside has been size on brown-skinned hosuis.
"There are a lot of 16s and 14s and some smaller two-layers."
Schueller agreed.
"Overall, fruit looks to be a bit on the smaller size this season, with not as many 12s and larger as last year."
Western Fresh's grower partner expected to wrap up its hosui harvest the week of Aug. 8. In mid-August, harvest of shinkos was expected to begin, and Salazar hoped the new variety would mean bigger sizes.
"People definitely want asian pears, but the guys who want larger fruit can't take" the smaller hosuis, he said.
Salazar was hoping the shinko harvest would yield plenty of 8s, 9s, 10s and 12s.
Kelton, Pa.-based I Love Produce will import asian pears from China for the fourth year this season, said Jim Provost, the company's president.
I Love Produce imports brown-skinned singos, which are popular with Korean consumers; and yellow-skinned goldens, which are popular with Chinese consumers, Provost said.
It will also be the third year I Love Produce is selling fruit under the Eat Brighter! brand. And new this year, Provost said, are Price Look-Up codes on packs of Chinese asian pears. Codes also will be on Chinese fuji apples marketed by I Love Produce.
I Love Produce partners with Hong Kong-based Shing Kee Lan Co. on its Chinese asian pears.
The company's program includes gift boxes, three-pack clamshells and loose bulk product, Provost said.