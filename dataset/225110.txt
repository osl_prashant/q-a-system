Tall fescue has been a popular grazing option for a large portion of the country for many years since its introduction from England in the late 1800s. The hardy, cool season annual is the most widely introduced grass in the U.S. Fescue accounts for an estimated 40 million acres of pasture and hay ground nationally.
Despite being abundantly used, fescue presents several problems when grazing toxic endophyte varieties such as Kentucky 31. Endophyte-infected fescue causes reproductive issues, stifles production and forces some cattle to be euthanized in extreme cases.
Producers have a variety of options to combat toxic fescue, but introducing novel endophyte fescues might get the most bang for their buck. A combination of university, government, industry and nonprofit groups have been promoting the use of novel endophyte fescue with workshops in areas where fescue is grazed.
Struggling with Toxicity
In states along the “Fescue Belt,” which border the Mason Dixon Line, fescue toxicosis can be prevalent.
Craig Roberts, University of Missouri Extension forage specialist, says fescue toxicity is the top forage problem for livestock in Missouri and costs the state’s beef producers an estimated $240 million in lost production per year. The Show-Me State has more than a third of the country’s fescue acreage at an estimated 17 million acres, according to the University of Missouri Extension, with more than 6 million in pastures.
Ergot alkaloids found in endophyte-infected fescue are responsible for causing problems in livestock. Ergot alkaloids are produced by a fungus that grows between plant cells, in the crown of the grass until it greens up. The endophyte will move into the seed.
“Fescue toxicosis is best described by its symptoms,” Roberts says.
Animal Health Risks
Vasoconstriction, constriction of the blood vessels, is the most common problem seen when grazing “hot fescue” that has a toxic endophyte. When blood flow is restricted, cattle have difficulty dissipating heat.
In the summer months cattle grazing fescue might spend more time in ponds or shaded areas trying to cool themselves down.
Because the flow of blood cannot reach extremities, there is also the risk of cattle losing hooves. This is often referred to as “fescue foot” and happens primarily in times of drought and cold weather.
“It is probably the most grotesque symptom, and many times it is misleading,” Roberts says.
Producers might think they’re in the clear because none of their cattle are exhibiting extreme symptoms such as fescue foot, but there could be more underlying problems.
“For every 10% of endophyte infected grass you have in a field, you’ll lose a tenth of a pound in gain on stocker cattle,” Roberts says.
Milk production in dairy cows goes down when grazing toxic fescue. Depressions in milk production are a minimum of 30% on hot fescue, says Stacey Hamilton, dairy specialist with University of Missouri Extension. For beef cows, lower milk output means lighter weaning weights for calves.
Reproductive issues occur while grazing toxic fescue, too. If a pasture has mostly infected grass (80% to 100% toxic endophyte) calving rates can drop drastically, up to 55% in spring-calving herds.
Lowering the calving rate increases the culling rate and reduces potential revenue, says Gene Schmitz, regional livestock specialist with University of Missouri Extension.
Part 2 of this story about establishing novel endophyte fescue will run on February 21. 
 
Note: This story appears in the February 2018 magazine issue of Drovers.