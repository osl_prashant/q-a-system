Sales success relies on your ability to communicate effectively with your prospects. The problem is we often get in our own way by not being intentional about the intent of a meeting.Kelly knew that the sales reps on her team were dedicated to their customers but they seemed to be running in circles.
There was a tremendous amount of "Sales Person Activity" but not enough "Sales Activity."
Kelly knew that based on the activity, her team should be closing more business. When she sat in on a week's worth of calls and meetings she determined that her team was lacking a clear focus on the intent of their meetings.
Know Your Intent 
Taking the time to properly prepare is the key to successful sales communications. As you begin intentionally preparing for a client interaction, ask a simple question:
What is the intent or outcome I want from this meeting?
As you consider the objective of the next interaction with your prospect, be very tactical. Your initial thought may be that you want them to:
Buy if they are a prospect or
Buy more or upgrade if they are already a customer
However, your intent must be a next step not the final step or goal.
Your intent falls into one of two categories: "Believe" or "Do."
Depending on the situation and where you are in the client relationship, you may need them to:
Believe and trust you and your value
Believe they have a problem you can help with
Believe they can achieve something
Believe their life will be better if something changes
As you are building rapport early in the relationship with your prospect, your intent is likely to be focused on "Believe". Your ability to move from "Believe" intents to action-oriented "Do" intents will depend on creating strong connections with your prospects.
Once you have a relationship with the prospect, your intent will be more "Do" or action-oriented shifting to:
Get a next meeting to gather more information
Get an introduction to a decision maker
Gain agreement on details to be used in a proposal
Gain commitment on a decision date
Close the sale
Keep in mind, this doesn't mean you don't ask for the sale in a meeting if the opportunity presents itself (just don't rush it). In the majority of cases your sales process requires multiple steps. At each step along the way, the more intentional you are the faster you will move through the process.
Primary and Secondary Intents
In some situations, you may achieve two intents in one successful interaction.
For example, one of Kelly's reps had been working for weeks to get a meeting with the CEO of his prospect company. The rep had multiple meetings with a key advisor to the CEO, and in each meeting would try to "sell" their service. His call to action was to ask for a meeting with the CEO. In each meeting, the advisor found ways to delay and avoid taking any action.
When Kelly asked what the Primary Intent of the meeting was, the rep, surprised by her question, told her "To get to the CEO so we can close the deal."
Kelly worked with her rep to prepare for the next meeting with a new intent: To help the advisor believe that they could help them and their company to achieve more success.
The rep met again with the advisor, and this time was not concerned with the Secondary Intent of meeting with the CEO.
By concentrating on the belief of the advisor, the focus of the conversation shifted. To the rep's surprise, the moment the advisor believed in his ability to understand their needs and deliver a viable solution, he scheduled a meeting with the CEO.
Not only had the Primary Intent been achieved, it was a requirement in order to achieve the Secondary Intent.
Be careful not to be deceived by Secondary Intents. For Kelly's rep, focusing on getting a meeting with the decision maker was actually preventing the achievement of their Primary Intent of getting the advisor on board.
When determining your Primary and Secondary Intent, it is important to consider who you are meeting with and their role in the decision hierarchy.
What is the Decision Hierarchy?
In many client situations you work with a number of people throughout the process. Understanding their role will make it easier for you to determine your intent and approach to each discussion or presentation.
Is the person you are meeting with an Advisor, Minor Influencer, Significant Influencer, or a Decision Maker?
The Advisor
It is common to be working with a Technical Advisor early in the sales process, but also for them to be part of the overall decision-making team.
In general terms, The Advisor is tactically-oriented, often skeptical and will have lots of questions.
Progress with advisors will be made by learning what drives them and providing them the technical information they need to move the project along.
The Minor Influencer
The Minor Influencer is likely to have some accountability for the success of initiatives and may be the supervisor of The Advisor.
The Minor Influencer may share the assessment of The Advisor with senior management, and may be looked to for recommendations on the viability of different options.
It is important that The Minor Influencer is comfortable with the details but also with the high level benefits. In order to achieve support, the emotional benefits to The Minor Influencer must be clear.
The Significant Influencer
A Significant Influencer may be a senior manager, budget holder, or key stakeholder that you need to create a connection with.
Typically, The Significant Influencer is concerned with the big picture and the overall impact of the recommendations. In addition, their vested interest in the success of the project and the company can be used to connect the emotional benefits.
The Decision Maker
Whether it is the owner of the business or a senior executive, they are responsible and accountable for overall success.
Depending on the size of the organization, you may only deal with The Decision Maker, or you may deal with any combination of the four roles of the Decision Hierarchy.
Win more sales with clear intent
When you are clear on who you are meeting with‚Äîand your intent is clear‚Äîyou will deliver a more effective message and win more sales. Start today by identifying the Primary Intent of each conversation or meeting, and the role of the person you are speaking with.
It's time for you to enjoy stronger sales through intentional presenting.
Mark A. Vickers is a Certified Professional Coach, and Certified World Class Speaking Coach. Mark helps you and your sales team improve performance through improved presentation and speaking skills. Mark's creative and engaging programs and coaching are designed to help your team become more effective quickly. For more information, visit: http://speakingisselling.com/.