Crop calls
Corn: 5 to 7 cents lower
Soybeans: 15 to 20 cents lower
Wheat: Fractionally to 3 cents lower
A political scandal in Brazil sent its currency tumbling overnight, which pressured soybean futures. Recent improvement in the real helped to keep U.S. soybeans competitive on the global market as well as limited farmer selling in Brazil. Corn and wheat futures followed suit as ripple effects quickly spread. Dalian soybean futures and Malaysian palm oil futures were lower overnight. Solid corn and soybean weekly export sales tallies will likely be ignored this morning, but a better-than-expected showing for wheat could provide some support.
 
 
Livestock calls
Cattle: Mixed
Hogs: Higher
Buying in the cattle market will be limited to short-covering as traders believe the beef market will soften seasonally. While cattle futures are trading at a discount to the cash market, this week's cash trade begun at $134 and $136, which is down from last week's trade that largely occurred between $137 and $138. Meanwhile, lean hog futures are expected to see a boost this morning from expectations pork buying will improve seasonally. The cash hog market is called steady to firmer this morning.