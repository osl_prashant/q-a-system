U.S. live cattle futures fell for a fifth straight session on Friday on technical selling and pressure from a weak cash cattle market following lower trade at U.S. Plains feedlots this week, traders said.
Funds involved tracking the Standard & Poor’s Goldman Sachs Commodity Index rolled December long positions mainly into February and April contracts, adding further pressure to the spot month.
Chicago Mercantile Exchange December live cattle ended down 1.900 cents per pound at a two-week low of 120.575 cents. February futures ended were 1.775 cents lower at 126.750 cents.
Actively traded February was pressured by technical selling on expectations that prices have already reached a near term high, said Schwieterman Inc broker Domenic Varricchio.
The contract topped its prior-week high on Monday and dipped below last week’s low on Friday before ending the “outside week” near the bottom of that trading range, a weak technical sign, he said.
After trading at lower levels this week, cash cattle prices are expected to weaken again next week as some unsold offerings are carried over to next week’s showlists. Packers will also be buying for slaughter in the U.S. Thanksgiving week, when fewer cattle are needed because of holiday plant downtime.
“We did not sell the showlist this week so we’ve got supplies building. It looks like we’re going to be another dollar lower or more after today’s close,” said Don Roose, analyst with U.S. Commodities.
Cash cattle in the Plains traded earlier this week at $124 per cwt or less.
Feeder cattle moved lower with the live cattle market, although declines were limited by low corn prices.
November feeder cattle closed unchanged at 158.475 cents per pound while January fell 0.675 cent to 157.175 cents.
Lean hog futures ended mostly firmer, with only the spot December contract closing lower on pressure from funds rolling positions out of the front month to deferred contracts.
CME December lean hog futures settled 0.700 cent lower at 62.475 cents per pound, lowest since Oct. 18. All other contracts were higher, with February rising 0.025 cent to 70.250.
Cash hogs traded down 3 cents to $60.04 per cwt in the top market of Iowa and southern Minnesota while wholesale pork prices were up 60 cents to $81.92 per cwt, according to U.S. Department of Agriculture data.