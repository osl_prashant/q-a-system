Agricultural advisors from Hillary Clinton and Donald Trump's respective campaigns went face-to-face on issues facing American agriculture,a candidate forum co-hosted by Farm Journal Media and Farm Foundation.Rural and cultural issues were discussed from representatives of each campaign, since agricultural issues have mostly been ignored during the presidential campaign.
Kathleen Merrigan, former deputy secretary of agriculture, represented the Clinton campaign while Sam Clovis, campaign co-chair, represented the Trump campaign.
A major point of discussion was the farm bill. Both Merrigan and Clovis agreed the nutrition title and the farm title should be kept together during the next farm bill.
Some of the other topics the two discussed were WOTUS, trade, and the impact of regulations on American agriculture.
Merrigan calls for more calibration and detail in discussions because she believes regulations "are not a bad thing."
"They level the playing field, they give certainty to businesses, they give certainty to our farmers and ranchers, and they build trust and confidence with the American public," said Merrigan.
On the other hand, Clovis said regulations impose themselves on smaller enterprises.
"It's not so much a regulatory regime, it's the fact of the matter when you write regulations to re-impose regulations, you create satiations that cause people to lose the competitive comparative advantage they have economically," said Clovis.
Clinton, Trump Camps Split on Regulations, Agree on Farm Bill


<!--
LimelightPlayerUtil.initEmbed('limelight_player_218039');
//-->

Want more video news? Watch it on MyFarmTV.