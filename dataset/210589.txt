Ration formulation programs have made it easy to develop rations. Plug in information for the feeds available and the cows, and the software calculates the ration. However, complex computer programs for ration formulation can’t and don’t tell you everything you need to know to get great results from the cows.Some nutrition models contain flaws or are more complicated than warranted in certain areas. Complex rumen models were developed to better predict changes to the nutrient supply to the cow by ruminal microbes, however, “We believe that they do not improve accuracy and only hinder the process of diet formulation,” explain Mike Allen and Mike VandeHaar, dairy nutritionists at Michigan State University. The programs provide much more information than needed for diet formulation. As a result, some people ‘can’t see the forest for the trees.'
“They spend too much time on details that don’t matter while ignoring factors that do,” says Allen. “More emphasis should be put on understanding effects of rations on energy intake and partitioning, production and nutrient requirements that are not considered by current ration formulation programs.” Most, if not all, diet formulation programs can be used to formulate diets as long as the nutritionist knows what information is useful and what is not.
Optimal diet formulation requires understanding variation in feeds and cows and working to reduce it, understanding and evaluating cow responses to diets and letting go of the many factors that just don’t matter. Ration formulation software should be considered your starting point. Then feedback from the cows should be used to tweak the ration. For best results, focus on the following factors.
REDUCE FEED VARIATION
Strive to increase the consistency of rations by reducing variation in feeds. Forages and some byproduct feeds have great variation in nutrient composition while other feeds such as dry corn and high protein soybean meal are more consistent. Each lot of purchased or harvested feed that might be variable should be tested for crude protein (CP) and neutral detergent fiber (NDF). Silages and wet feeds should be tested for dry matter content at least twice weekly and for CP and NDF twice monthly until the extent of variation is understood. Test forages and byproduct feeds for macro and micro minerals to better formulate mineral supplements.
Allen encourages nutritionists and producers to spend more time on activities that can help achieve their goals. Routine feed testing, monitoring variation in feeds, making sure feed mixing is accurate and uniform, ensuring that cows have access to feed most of the day and communicating with managers, feeders and cropping personnel are all important.
UNDERSTAND VARIATION AMONG COWS
Not all cows respond the same to a ration. Stage of lactation, parity, milk yield, DMI, body condition and genetic potential all affect cow response. In addition, cows’ metabolic priorities change throughout lactation. While these changes are difficult to accurately include in a computer program, top-notch nutritionists who understand and pay attention to cows can use qualitative knowledge to optimize nutrition.
GROUP FOR SUCCESS
Allen recommends three rations—fresh, high and maintenance. The fresh ration should be moderately filling to maintain adequate rumen fill and buffer with enough starch to provide glucose and glucose precursors for milk yield. Healthy cows that eat aggressively should be switched to the high ration after 10 days postpartum.
The high ration should be less filling, but contain greater starch to drive milk yield. Actual concentrations of forage NDF and starch will depend on cow response and space for cows within the group. When cows reach a body condition score of ~3.0 on a 5-point scale they should be fed a maintenance ration that maintains milk yield and minimizes weight gain. It should have less starch and a bit higher forage NDF. Evaluate cows’ body condition score at dry off and use that information to adjust the maintenance diet as needed.

Fresh


High


Maintenance


~22-24% forage NDF


~17-20% forage NDF


Up to 24% forage NDF


~24-26% starch


~28-32% starch


~18-22% starch


~17% CP with at least 40% RUP


~17% CP special RUP sources may not be needed


~15-16% CP

ACT ON COW FEEDBACK
In addition to testing and quantitatively balancing diets for nutrients, qualitative knowledge should be considered, too. The filling effect of a ration is a function of forage NDF, forage digestibility and forage fragility and must be a primary concern in ration formulation. Effective NDF should be adequate to form a rumen mat. Excessive particle length should be avoided as it can reduce feed intake and increase sorting. Starch content and fermentability must be considered. Starch drives milk yield but excessive amounts can reduce feed intake, ruminal pH, milk fat yield and partition energy away from milk yield to body condition. Unfortunately, assigning accurate numbers to all of these relationships in a ration program is very difficult.
APPLY QUALITATIVE KNOWLEDGE
Monitor milk yield, milk components, milk composition, DMI, BCS, rumen score, fecal consistency and cow behavior. Listening to and responding to what the cows tell you are critical. While all nutritionists employ feedback from cows, their ability to do so successfully varies with experience and knowledge.
“All of us have a natural tendency to hear what we want to hear. So when we see responses we expected or wanted, we remember,” explains VandeHaar. “When we don’t see a response, we forget and assume the lack of response was due to a problem we could not control. We forget that an increase in milk might also have been due to some factor other than diet.”
Ration formulation for lactating cows is a complex process. Nutrition software programs are an important part of that process, but not all information generated is useful. For best results, Allen and VandeHaar recommend nutritionists spend more time to better understand how rations affect nutrient intake and partitioning. These are the most important factors for ration formulation but are not predicted by current ration formulation programs.