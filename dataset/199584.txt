Performance record keeping for beef cattle production is essential for informed selection, culling and management decisions, which all have an economic impact on your operation. Weaning weight of a calf is an indication of the preweaning growth of the calf and the milking ability of the dam. In order to objectively compare the performance of an entire calf crop, actual weaning weights should be adjusted to a standard basis.To collect and adjust weaning performance records the following information is needed:
Dam identification number
Dam birth date or birth year
Calf birth date, sex and identification number
Calf weight collected individually between 160 and 250 days of age and the date in which the weight is collected.
Optional information to record can include sire identification, breed composition, body condition scores and calf muscle scores
Many breeding cattle are still evaluated and selected only on visual appraisal, even though it is well known that visual appraisal alone is a poor indicator of performance and most economically relevant traits. By using scales and other measuring devices, cattlemen can objectively measure and evaluate beef cattle superiority or inferiority for traits affecting performance and profitability.
The Guidelines for Uniform Beef Improvement Programs: Beef Improvement Federation (BIF), 9th Edition recommends that weaning weights are standardized to 205 days of age and age of dam basis. Weaning weights should be taken when a contemporary group of calves averages about 205 days of age. Adjusted weaning weights should be calculated for calves within an age range of 160 and 250 days of age. Records on calves weaned outside this range should be given a special management code and handled as a separate management group; they should not be adjusted for age of dam because appropriate correction factors are not available.
Adjusted 205-day weaning weight is computed on the basis of average daily gain from birth to weaning, using the following formula: 
Adj. 205 day Wean Weight = Wean Wt.-Birth Wt. X 205 + Birth Wt. + Age of Dam Adj.
 Weaning Age
If the actual birth weight is not available, the appropriate standard birth weight designated by the respective breed association of the calf's sire may be used (see table below).
BIF Standard Adjustment Factors for Birth and Weaning Weight
Age of Dam at Birth of the Calf
Birth Weight
Male
Female
2
+8
+60
+54
3
+5
+40
+36
4
+2
+20
+18
5-10
0
0
0
11 and older
+3
+20
+18
Note:
1) Standard birth weights are 75 lb. for males and 70 lb. for females.
2) Breed specific adjustment factors have been developed by individual breed associations from breed data. Factors used by individual breed associations are subject to change. Contact the respective breed association for more information about their birth and weaning weight adjustment factors.
3) See Chapter 3 of these guidelines for information on using adjustment factors in adjustment formulas.
Individual animal records for adjusted 205-day weaning weight and adjusted 205-day weaning weight ratio should be calculated and reported separately for bulls, steers and heifers. The weaning weight ratio is a valuable number and should be calculated with the following formula:
Adj. 205 day Weaning Wt. Ratio= Individual Adj. 205 Weaning Wt. X 100 
 Group Average Adj. 205 Weaning Wt.
Adjusted 205-day weaning weight ratios document each animal's percentage deviation from the average of its contemporaries and are useful in ranking individuals within each sex when making selections. For weight ratios to be of value, contemporaries should be herd mates, similar in age and raised under the same management and environmental conditions. Refer to www.beefimprovement.org for more information.