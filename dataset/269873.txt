BC-Merc Early
BC-Merc Early

The Associated Press

CHICAGO




CHICAGO (AP) — Early trading on the Chicago Mercantile Exchange Wed:
Open  High  Low  Last   Chg.  CATTLE                                  40,000 lbs.; cents per lb.                Apr      118.57 118.95 118.47 118.82   +.77Jun      105.30 105.97 105.30 105.50   +.60Aug      104.72 105.30 104.70 104.92   +.47Oct      109.05 109.50 109.02 109.22   +.42Dec      113.37 113.67 113.17 113.30   +.38Feb      115.25 115.57 115.10 115.10   +.35Apr      116.00 116.40 115.95 115.95   +.38Jun      109.37 109.97 109.37 109.50   +.53Aug      108.00 108.72 108.00 108.47   +.72Est. sales 13,549.  Tue.'s sales 50,122 Tue.'s open int 344,649,  up 44        FEEDER CATTLE                        50,000 lbs.; cents per lb.                Apr      138.92 139.32 138.55 138.75   +.03May      140.60 141.40 140.45 140.45   +.20Aug      145.35 146.25 145.35 145.47   +.40Sep      146.80 147.50 146.77 146.80   +.30Oct      147.25 147.92 147.20 147.20   +.28Nov      147.07 147.45 147.00 147.32   +.77Jan      143.82 143.97 143.55 143.70   +.60Mar      141.50 141.50 141.22 141.22   +.62Est. sales 2,829.  Tue.'s sales 13,219  Tue.'s open int 49,750                 HOGS,LEAN                                     40,000 lbs.; cents per lb.                May       68.00  68.90  67.85  68.90  +1.00Jun       76.80  78.00  76.77  77.97  +1.22Jul       79.40  80.40  79.35  80.37  +1.15Aug       79.05  80.15  79.05  80.05  +1.08Oct       67.95  68.70  67.92  68.40   +.48Dec       62.10  62.52  62.10  62.42   +.37Feb       65.75  66.15  65.75  66.00   +.30Apr       69.22  69.65  69.22  69.65   +.40May       74.00  74.00  74.00  74.00        Jun       78.00  78.00  78.00  78.00   +.60Est. sales 13,431.  Tue.'s sales 31,670 Tue.'s open int 235,386                PORK BELLIES                          40,000 lbs.; cents per lb.                No open contracts.