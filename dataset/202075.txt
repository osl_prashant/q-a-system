Editor's note: The following article was written byPORK NetworkEditor JoAnn Alumbaugh and published in theJune issue of PORK Network.Pork producers are becoming increasingly tech-savvy, and it's interesting to see how equipment, record-keeping tools, environmental controls and other devices have changed over time. Producers use computers to run their operations, and can easily make changes with their smart phones.
Farm owners who use advanced technology in their operations expect to hire workers who are prepared to use these tools in their daily tasks, without the need for extensive training. Most students have used computers daily for their entire lives, and they expect to use productivity-enhancing technology in their jobs to simplify tasks and improve their work performance. Still, simplicity is an important component of new technology.
Handheld recordkeeping
Newton Pork is a 600-sow breed-to-wean farm that produces early-wean pigs, which it sells to a local finishing barn. Owner and manager Sean Dolan uses a hand-held Nautiz X3 PDA device, to record data that is sent to PigCHAMP. The devices can handle the less-than-perfect barn environment because they're sealed against dust and water sprays.
"These are hog barns, so there is feed dust and skin dander floating around," Dolan says. "The conditions can sometimes be humid and unpleasant ‚Äî and we've dropped our computers in fecal material more than once. The handheld gives me the durability I need."
The handheld device has streamlined Dolan's data-entry process, and he says up-to-date data allows the operation to identify and manage production problems more effectively.
Dolan says he's working with his genetic supplier to add barcodes to semen dose labels used for insemination, so he can use the Nautiz X3's built-in barcode scanner to save even more time. "Scanning barcodes will allow us to more easily track individual boars used. That way, if we come upon a genetic defect, we can alert the supplier and they can remove that boar from their herd," he says.
Dolan uses the device to record life events for each sow ‚Äî including arrival, treatments, vaccinations, matings, farrowings, weanings and removal.
Environmental controls
Jayce Mountain Pork (JMP) near Fredericktown, Mo., has a new facility that incorporates many interesting technologies.
It utilizes a state-of-the-art upgradable system that allows owners to remotely manage their facilities using a computer, smart phone or tablet. It automates a large part of your company's management.
The system, called Maximus, provides efficient use of energy and resources. It uses graph reference for settings, making it easy to understand. It is compatible with most handheld devices and offers multi-room operation.
It also generates all alarms by text or email so the farm owners know immediately if a system is malfunctioning.
Cameras monitored from a central office
JMP also made a significant investment in 50 cameras placed in each room of the facility that allow managers to see nearly everything that goes on in the buildings.
Increased scrutiny on animal-care practices make cameras a good idea for new operations, even though JMP has complete confidence in its workforce.
It also allows them to monitor mechanical and environmental malfunctions.
Convertible farrowing crate
The convertible farrowing crate shown here was new at the Iowa Pork Congress earlier this year.
Made by Big Dutchman, the BD Swing crate is an option for producers who plan to retrofit an existing farrowing barn because it allows for a longer crate in a smaller pen. The crate can be positioned either straight, angled or diagonally.
The bow or finger bar sides adjust in width and the back door adjusts in length to accommodate either gilts or older parity sows.
V-shaped crate
This is a v-shaped crate used at JMP. It looks like a conventional crate until pigs are processed, then the bars are slid over to the sides at the back of the pen so the sow has room to turn around.
The packer to whom JMP sells requires that turn-around farrowing crates and group gestation pens be used.
Since this is a new operation, it's difficult to know how production performance will be impacted, but because sows in this operation get a lot of individual attention during farrowing, the farm is in the top 2 to 3% in the country for pigs per sow per year.