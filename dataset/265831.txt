Wisconsin farmer Daniel Briel and his 14-year-old son David Briel were killed in a silo collapse this weekend. Fox News reports the pair had been removing stuck silage from the sides of the silo with Daniel's older son, Caleb who made it out of the silo alive.
According to Fox9, rescuers spent 45 minutes pulling the father and son free using thermal imaging devices and extraction equipment.
“I have peace knowing that he was praying with our son,” Melissa Briel told FOX9. “I have peace knowing that he was holding onto our son.”
The family’s community has set up a GoFund me account to help pay for funeral expenses.
“God is good even through hard times,” Briel told FOX9. “I want people to take away that God is there and if you keep your faith in him he’s going to see you through.”