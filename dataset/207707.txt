Tanimura & Antle debuted its PlantTape automated transplanting machine for lettuce this season and is already reaping a hearty return on its investment.
“One thing that sets us apart is our ability to transplant much faster than the competition,” said Anthony Mazzuca, senior director of commodity management.
“We’ve seen yield increases and a reduction in water and spraying by about 20%. It adds an efficiency to our overall operations. We’re doing more with less, and of course labor has been the big hot topic in the valley this year.”
The Salinas, Calif.-based company bought PlantTape, a Spanish company, in 2014. PlantTape provides a greater density of plants per tray than the standard plug system, according to the company’s website, www.planttape.com.
The automated transplanting machine requires only three or four workers compared with 10-15 on a manual transplanting machine, Mazzuca said.
He said a typical transplant crew works at a pace of a half-mile an hour. PlantTape achieves 7 mph with 80% less labor.
“It’s quite a savings for us,” Mazzuca said.
“It’s not something we’re looking to keep proprietary at T&A. It’s something we’re definitely looking to put out there to agriculture in general.”