Veggie burger manufacturer to expand plant in Lawrence
Veggie burger manufacturer to expand plant in Lawrence

The Associated Press

LAWRENCE, Kan.




LAWRENCE, Kan. (AP) — A Lawrence-based veggie burger manufacturer plans to expand and add about a dozen new employees.
Hilary's Eat Well, which makes organic, plant-based foods, is planning a nearly $1.5 million expansion. It will add new production equipment to its current building and lease new space for its warehouse operations.
The Lawrence Journal-World reports the city commission this week approved a $10,000 grant for the project. Douglas County and several local economic development organizations plan to each add $10,000 grants.
The company was founded in 2005, when Hilary Brown created the veggie burger recipe. Several investment groups now own the company, which also produces veggie sausages and salad dressing.
It currently employs 40 people, including providing jobs to people who have been in prison, struggled with addictions or have been homeless
___
Information from: Lawrence (Kan.) Journal-World, http://www.ljworld.com