<!--

LimelightPlayerUtil.initEmbed('limelight_player_218039');

//-->


INCLINE VILLAGE, Nev. - The National Watermelon Association is setting its sights on meeting the industry's labor needs and continuing to invest in food safety efforts, executive director Bob Morrissey told conference attendees at the Feb. 22-25 event at Lake Tahoe.
During a Feb. 24 session Morrissey summed up 2017 as "unpredictable, unconventional, non-traditional, challenging and full of sweet opportunities."
With immigration enforcement among the key priorities of the Trump administration - as already evidenced by three executive orders and two enforcement memos - Morrissey urged members of the watermelon industry to make an effort to educate members of Congress this year, especially legislators in the U.S. House of Representatives.
Labor is the association's No. 1 policy agenda for the year, Morrissey said in an interview.
"Everything that we do as an association, everything that our members do, from day in and day out, needs labor," Morrissey said.
"With us, it's no longer a wait and see. We have to have a guest-worker bill or a guest-worker program that will provide a legal, viable workforce that will do the job on our farms and in our packing facilities. (...) Without labor, none of this matters," he said.
"If we don't have labor to plant, grow and harvest crops, we would have the biggest food crisis you've ever seen."
The association will continue its traditional advocacy by sending a group to Washington, D.C., in the fall to meet with legislators, Morrissey said during the Feb. 24 session.
Additionally, NWA has given its members access to Muster, an online mobilization tool that facilitates engagememt with policymakers on pertinent policy initiatives, he said.


Day of Watermelon

Finally, the association is organizing a grassroots effort through its nine regional chapters to educate legislators through a Day of Watermelon, a day when industry members would invite their district's House representative to spend some time learning about the industry firsthand, Morrissey said in the interview.
"If we can get them for one hour to ride in a truck with a farmer, and listen to that farmer who has the investment in the dirt and is going through all of these pains, fantastic things can happen," Morrissey said. "I can go to Washington every single week of the year, and the impact of that one ride in the truck would surmount all of those weeks."


Food safety

On the food safety front, the NWA in February published the third edition of its Commodity-Specific Food Safety Guidelines for the Fresh Watermelon Supply Chain, updated to reflect finalized rules under the Food Safety Modernization Act, Morrissey told members.
The association also invests in food safety research by donating funds to the Center for Produce Safety as well as lobbying for food safety research funding to be included in the 2018 farm bill, Morrissey said.