(Bloomberg) -- Saudi Arabia is cutting back on its dealings with some German companies amid a diplomatic spat with its top European trading partner, according to people with knowledge of the matter.Government agencies have been told not to renew some non-essential contracts with German firms following comments made in November by Germany’s then-foreign minister, Sigmar Gabriel, two of the people said, asking not to be identified because the matter is private. Essential business is continuing as normal, they said.Saudi Arabia’s tensions with Germany mark its first open spat with a European ally since 2015, when it briefly recalled its ambassador to Sweden. The kingdom has been adopting a more assertive foreign policy with the rise in power of 32-year-old Crown Prince Mohammed bin Salman. Among the contracts at risk are Deutsche Bank AG’s mandates in the kingdom, including a potential role on Saudi Aramco’s initial public offering, which could be the largest share sale ever, three of the people said.The bank, which is cutting jobs elsewhere, has been expanding in the kingdom to build a team of about 90 people on expectations that the nation’s stock exchange may be upgraded to emerging market status and the outlook for bond and stock sales improves.Easing TensionsThe appointment of a new German government this week, including Heiko Maas as foreign minister, could be an opportunity to ease tensions between the two countries, two of the people said.The kingdom’s Center for International Communication said it didn’t have any information about the matter and therefore couldn’t comment. Deutsche Bank declined to comment. A spokesman for the German government also declined to comment.Relations between the two countries started to deteriorate after the kingdom condemned remarks made by Gabriel in Brussels on Nov. 13 when he suggested that Lebanon was a “pawn” of Saudi Arabia after the surprise resignation of Lebanese Prime Minister Saad Hariri in Riyadh. Shortly afterwards, the Saudi government summoned its ambassador to Germany home for consultation.Prince Mohammed has been aggressive on national security matters, especially regarding Iran, Saudi Arabia’s rival for power in the region. Saudi Arabia has led a bombing campaign in Yemen since 2015 on behalf of a government ousted by Iran-backed Houthi rebels and cut off of ties with neighboring Qatar in 2017 in part over its relationship with Iran.Top Trading PartnerGermany is Saudi Arabia’s top European trading partner and third-largest source of imports worldwide, according to data compiled by Bloomberg.Siemens AG Chief Executive Officer Joe Kaeser has often touted his company’s close relationship with Saudi Arabia. Siemens last year received a blockbuster order for five gas turbines that will be used in a gas extraction plant in Fadhili and the company has a gas turbine manufacturing plant in the country’s Eastern Province. A spokesman for Siemens declined to comment on the political tensions.German steel and elevator giant Thyssenkrupp AG is a large supplier to the kingdom through its maritime systems division which makes submarines and has also secured large-scale contracts through its elevator division.Prince Mohammed is in the midst of a tour to the United Kingdom and the U.S. as he seeks to build a defense industry and attract more foreign investors. U.S. President Donald Trump will welcome him to the White House on March 20 after the prince agreed to a goal of 65 billion pounds ($91 billion) of mutual trade and investment in the coming years with British Prime Minister Theresa May last week.--With assistance from Birgit Jennen Alan Crawford Tony Czuczka and Oliver Sachgau To contact the reporters on this story: Vivian Nereim in Riyadh at vnereim@bloomberg.net, Dinesh Nair in London at dnair5@bloomberg.net, Matthew Martin in Dubai at mmartin128@bloomberg.net, Glen Carey in Riyadh at gcarey8@bloomberg.net. To contact the editors responsible for this story: Stefania Bianchi at sbianchi10@bloomberg.net, Alaa Shahine at asalha@bloomberg.net. ©2018 Bloomberg L.P.