The foodservice business remains healthy and competitive for distributors serving the establishments in the Tri-State regions.
New York hosts more than 24,000 eating and drinking establishments and tourists cite patronizing the city's restaurants and clubs as a major reason for visiting the city, according to a report by the New York City Hospitality Alliance.
A variety of eateries in Manhattan and the surrounding boroughs as well as the suburbs keep distributors busy.
"The business is strong and we are in the city like Baldor and Riviera," said Joel Panagakos, salesman for J. Kings Foodservice Professionals Inc., Holtsville, N.Y. "There's action going on and it's busy. We have a group of very good produce companies supplying customers. Yet there's business for everyone."
The region's independent restaurants must battle larger national chains which have successfully adjusted their menus to customer-friendly price points.
The independents don't need to mimic their competitors but must review their menus, change portion sizes and adjust the application of vegetables so they can serve their customers at sensible price points, Panagakos said.
Baldor Specialty Foods Inc. distributes produce to a number of foodservice segments including restaurants, hotels, country clubs and offices.
Mike Muzyk, president, characterizes business as consistent.
"Because of tourism, hotel occupancy is strong, which helps on that side," he said. "The business at corporate cafes, as they relate to trading houses on Wall Street and law firms - that remains consistent. We have enough of a diverse portfolio to keep the foodservice sector strong."
The healthy eating trend is contributing to increasing foodservice sales, said Benjamin Friedman, owner of Englewood, N.J.-based Riviera Produce Corp.
"The restaurant industry is healthy," he said. "Operators are still expanding and players from outside the region are trying to come in and open new concepts. Home meal delivery and replacement are segments of the industry that didn't exist five years ago and we supply those sectors."
Foodservice and drinking establishments are one of the metropolitan area's largest sources of private employment and represent more than $32 billion in sales.
Business is healthy, said Paul Armata, vice president of E. Armata Inc.
"It's been very good," he said. "Business is growing."
Heavy winter storms disrupted restaurant business and Ciro Porricelli, partner with Jerry Porricelli Produce, said purveyors warn distributors to pay closer attention to accounts receivables.
"I know some of the restaurant owners and I talk with them," he said. "When they have a snow day, they have to shut down and on top of that, have to pay extra to plow the parking lot, which adds insult to injury but is something that must be done. But the good ones will do well though it's a tough business."
The region's restaurants appear busy, said James Tramutola Jr., buyer with A&J Produce Corp.
"Every time I walk down the block, every single restaurant is packed," he said. "There's so much volume of people in this area, which is why the dynamics of this area is interesting. People who live here have to go somewhere to eat."
Sales remain strong in the segment, said Bruce Klein, director of marketing for Secaucus, N.J.-based Maurice A. Auerbach Inc.
"I think their business is up and it's definitely encouraging," he said. "The mid-priced restaurants, like the T.G.I. Fridays, the Houlihans, the Olive Gardens of the world, you see an uptick in business there. You go to a Capital Grille on a Tuesday night and those restaurants are also busy. They're doing better."
FreshPro Food Distributors in West Caldwell, N.J., distributes mostly to retail accounts but is beginning to enter the foodservice segment by serving some customers.
"We are trying to do some different foodservice venues and seeing if it's viable for us and something we want to try to get into long term," said Joe Granata, director of produce sales. "The foodservice business is tough and it's a whole different environment. There are a lot of well-established foodservice companies in the area."
The company has hired a business development manager to handle foodservice sales.