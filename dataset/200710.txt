Drones have been hot talk in agriculture for the past several seasons. But how popular are they, really? According to a recent Farm Journal Media Pulse poll that surveyed more than a thousand farmers and ranchers, use of this technology has definitely gained a firm foothold in the industry.
The Pulse poll simply asked, "Will you use a drone(s) on your operation this year?" Of the nearly 1,100 respondents, a third answered positively, with 21% saying they will operate the drones themselves, and another 12% opting for a retailer or other third-party entity to fly the drones.
Another 31% say they will keep an open mind about using drones on their operation in 2018, but weren't ready to pull the trigger this year. The final 37% say they aren't interested in using this technology.



Will you use a drone(s) on your operation this year?


¬©AgWeb


 
The Farm Journal Pulse polls farmers and ranchers for their straight-from-the-farm opinions and crop progress updates. By partnering withCommodity Update, the leading provider of agricultural information to mobile phones.
To learn more, visitwww.agweb.com/farmjournal/farm-journal-pulse.