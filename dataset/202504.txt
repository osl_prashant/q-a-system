DENVER - Consumer demand for convenience is accelerating, and that has implications for potato marketers.
Speaking at the Potatoes USA annual meeting during a March 14 domestic marketing committee session, Kim Breshears, director of marketing programs for Potatoes USA, reviewed recent consumer studies about potatoes that reveal consumers want reduced cooking time and greater ease of use. That could lead to more demand for quicker-cooking small potatoes and value-added potatoes, she said.
She said a multi-year consumer study the group conducts measures the changing attitudes and opinions of consumers about food and potatoes. The research has consistently shown that flavor, freshness and healthy attributes rank at top of consumer appeal.
"One thing in this year's study is the (consumer) notion that foods that can be prepared and served quickly and easily gained significantly in what consumers are looking for in food choices," she said. "This has moved up several notches in terms of order of priority. It is a key driver in terms of consumer choice."
Consumer preference for convenient food increased 4% from 2016 to 2017, she said.
Consumers were recently asked about "motivating messages" that would spur them to consume more potatoes.
She said the top messages, compared with last year, were:
News ways of preparing potatoes: 37% in 2017, 35% in 2016;
Different tastes and textures: 29% in 2017, same in 2016;
Shorter cooking time: 28% in 2017, 26% in 2016;
Health/nutrition message: 27% in 2017, 24% in 2016.
"The shorter cook time is the third most motivating message and that's a significant increase from last year," Breshears said.
Another trend that potato marketers should account for is the increasing consumer interest over the last four years for low-carb, vegetarian and gluten-free food.
"All of these have increased in importance among consumers and they were seeking out (those foods)," she said. "These are things we need to be mindful of as we are marketing our products."
Among fresh potato varieties used, consumers were asked how often they used different types of potatoes. Their responses, including the responses "often" and "occasionally" were:
white potatoes: 62% in 2017, 63% in 2016
russets: 61% in 2017, 60% in 2016;
potatoes: 58% in 2017, 58% in 2016;
yellow potatoes: 52% in 2017, 48% in 2016;
petite potatoes: 30% in 2017; 24% in 2016;
fingerling: 24% in 2017, 21% in 2016;
purple/blue potatoes: 23% in 2017, 20% in 2016;
"Smaller-type fresh potatoes with lower cooking times are gaining," she said, in addition to pre-packaged and value-added potatoes.
Potatoes are most served at home among all vegetables, topping green beans, corn and broccoli, she said.
More than 70% of consumers surveyed reported potatoes were served within the last week for dinner occasions, compared with 57% for green beans and corn.