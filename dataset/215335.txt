Chicago Mercantile Exchange live cattle futures slid to their lowest level in almost five months on Monday, led by the prospect that packers might pay less for supplies later this week, said traders.
December live cattle finished down 0.850 cent per pound at 116.375 cents, and February ended 0.800 cent lower at 121.175 cents.
Last week packers in the U.S. Plains paid $119 to $121 per cwt for slaughter-ready, or cash, cattle.
This week's U.S. Plains' showlist, a tally of the number of cattle for sale, has more livestock than a week ago. Packers are struggling to recoup lost margins, which could hurt cash prices.
And beef is not only competing with plentiful pork and poultry, but for the minds of consumers that are focused on year-end holiday gift buying, said traders and analysts.
"A larger showlist will help stack the deck in favor of lower cash cattle prices this week, said Cassandra Fish, author of industry blog The Beef.
"Boxed beef values, up at midday today, are in their last gasp, early December rally before a downtrend ensues at least until Christmas," she said.
CME feeder cattle futures drifted to a near three-month low following lower cash feeder cattle prices and weaker CME live cattle futures.
January feeder cattle closed down 0.375 cent per pound at 149.950 cents. 
Hogs End Mostly Higher
Deferred CME lean hog contracts finished higher after traders bought those months and sold December ahead of its expiration on Dec. 14, said traders.
December hogs ended 0.325 cent per pound lower at 64.950 cents. February closed up 0.950 cent to 71.675 cents, and April finished 1.000 cents higher at 75.450 cents.
"People might be thinking we're going to see fewer hogs beginning this spring," a trader said.
Ultimately hogs could be hard to come by after farmers rushed them to market earlier than they had planned, prompted by initial forecasts for increased supplies moving forward.
Outside of holiday ham business, and pork bellies being stored for spring and summer use, overall wholesale pork demand usually tapers off in December, the trader said.