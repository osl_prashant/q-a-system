Raising a healthy animal involves nutrition, housing and management. Preventing, diagnosing early and treating any illness will pay big dividends.With cattle in particular, there are many common diseases that should be monitored daily. Three diseases that are more prevalent this time of year include: bloat, pink eye and warts.
Bloat
Bloat is caused by a combination of factors. Gas is produced in the rumen as part of the normal process of digestion in cattle. The ruminant animal must eliminate the gas by belching to avoid a buildup of pressure. Bloat often occurs when cattle graze on green, young alfalfa or clover. It can also occur when they are fed a high-concentrate diet or when the diet is changed. Bloat is not contagious, but if you notice one animal exhibiting signs of bloat, check other animals that are under the same management conditions.
Signs
The first sign of bloat is a bulging of the area between the last rib and the hip. As gas pressure increases inside the rumen, the entire abdomen enlarges on both sides of the animal. This causes pressure and pain, resulting in difficulty breathing.
Prevention
Bloat can be prevented by avoiding rich feeds such as lush alfalfa and by feeding adequate amounts of roughage with concentrate. Forgetting to feed the animal or changing its feed abruptly can also cause bloat.
Treatment
Depending on the severity, it may require that the gas pressure be relieved quickly. The objective of treating bloat is to slow down fermentation in the rumen and help relieve the excess gas pressure. Veterinarians have treatment methods that they prefer depending on the severity. For emergency treatment, mineral oil may need to be delivered via the mouth. Another emergency treatment is passing a stomach tube or piece of garden hose into the rumen to release the gas. These practices require skill and should be done only under the supervision of an experienced individual.
Pinkeye
Pinkeye is an eye infection that commonly occurs during the summer months when the bacterium is spread by flies. In most severe cases, it can permanently blind an animal in one or both eyes. Pinkeye is considered a zoonotic disease, which means it can be spread from animals to humans.
Signs
At first, a clear discharge runs from the affected eye down the side of the face. The eye appears red and may bulge. A white spot will appear and may remain if the eye does not heal properly. Affected eyes are sensitive to bright light. Calves will generally exhibit a more frightened behavior, as they may have a more difficult time seeing their surroundings.
Prevention
Separate infected calves from healthy calves. Control flies by spraying, dipping or dusting calves and spraying calf pens. Dispose of manure frequently to eliminate places for flies to lay eggs. To avoid getting pinkeye yourself, wash your hands frequently and avoid touching your eyes.
Treatment
Keep affected calves in a cool, darkened pen and give them plenty of feed and water. Apply antibiotic ointments on the affected eyes under the direction of a veterinarian. Severe cases may require additional veterinary treatment.
Warts
Warts are viral infections of the skin. Although not a zoonotic disease, warts can spread among animals.
Signs
The infections cause growths that often look like cauliflower. Warts commonly appear and spread slowly on the neck, shoulders and head. Most warts are small, but in extreme cases, they become so large that they break off and can easily become infected.
Prevention
Isolating affected calves will help prevent the disease from spreading to others.
Treatment
Small warts often disappear without treatment, but larger warts will need to be removed and healed before exhibition. Ask your veterinarian for the best treatment. Many states, including Michigan, consider warts an infectious disease and forbid sale or exhibition of cattle with warts.
There are other diseases that may occur during the spring and summer months, so be sure to regularly observe your animals for any irregular behavior or changes in appearance. If you have an animal that is ill, provide it nutritious feed, plenty of water, a clean pen protected from dampness and drafts and isolate it from healthy animals. If the calf is not recovering properly, make sure to contact your veterinarian.