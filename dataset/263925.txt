Salmon could return to Columbia River after long absence
Salmon could return to Columbia River after long absence

The Associated Press

SEATTLE




SEATTLE (AP) — Officials say salmon soon could return to the Columbia River for the first time in seven decades.
KUOW-FM in Seattle reports Cody Desautel, director of Natural Resources for the Confederated Tribes of the Colville, says his group will "trap and haul" fish out of its hatchery and put them above Chief Joseph and Grand Coulee dams, so "there will be salmon above Grand Coulee Dam this year for the first time in 70 years."
Desautel says the plan hangs on one last federal permit from the National Oceanic and Atmospheric Administration.
If the final permit is approved, Colville fish managers will trap salmon at their hatchery and drive them around the dam by truck, where they'll be released back into the Columbia River. The tribe will keep track of where those fish go.
___
Information from: KUOW-FM, http://www.kuow.org/