Crowd-control plan for top Montana fishing stream rejected
Crowd-control plan for top Montana fishing stream rejected

By MATT VOLZAssociated Press
The Associated Press

HELENA, Mont.




HELENA, Mont. (AP) — Montana officials on Thursday rejected a plan to limit commercial fishing guides on the Madison River that flows out of Yellowstone National Park, a top fishing destination where the number of days spent angling has more than doubled in recent years.
The Montana Fish and Wildlife Commission unanimously voted against the proposal after multiple guides, outfitters and other anglers spoke out against it.
They generally agreed that something needs to be done to alleviate the crowding but believe the commercial guides were being unfairly singled out and didn't have enough input in developing the plan.
"I'm not sure that targeting merely commercial users in the Madison will get at the problem we're trying to resolve, which is some crowding and some conflicts during about a six-week period during the summer," said Richard Lessnar, a former executive director of the Madison River Foundation who lives in Cameron.
The plan proposed by the state Department of Fish, Wildlife and Parks aimed to address the increasingly crowded river, where the total number of angler days has gone from 88,000 in 2012 to 179,000 in 2016.
The most significant increase in use has been by out-of-state tourists, with about three-quarters of the anglers along the upper portion of the river near Yellowstone from out of state, according to a draft environmental study by the department.
Many of those tourists are using guides, and the number of fishing trips by commercial outfitters increased 72 percent between 2008 and 2017.
Fisheries officials say the crowds have also brought greater conflicts between anglers and more litter in the river, and that the increased traffic could eventually hurt the river's fish populations, which have remained stable up to now.
The plan proposed by the state Department of Fish, Wildlife and Parks would have capped the number of commercial fishing guides at the current level of about 200, which is one of the highest numbers of permitted guides on any river in the state, according to FWP fisheries manager Travis Horton.
The new rules also would have barred guides from certain stretches of the river on different days and prohibited commercial outfitters altogether in a portion of the lower river. Anglers also would have been barred from using vessels to access parts of the river designated for wade and shore fishing only and glass would have been prohibited on the river.
Outfitters and guides said restricting portions of the river on certain days would create bottlenecks on other stretches. Another unintended consequence would be that guided fishing trips could actually increase under the proposal if all 200 outfitters use their allotted 10 trips per day, they said.
Moreover, the plan doesn't address private, noncommercial anglers who make up more than 80 percent of the year-round use, they said.
FWP director Martha Williams said she heard the comments loud and clear.
"We're more than happy to hear the comments and go back to the drawing board to some degree," Williams said.