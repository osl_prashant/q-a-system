Each of us have special dates we celebrate on an annual basis — birthdays, anniversaries and other special holidays. For the cow herd, notable dates might include the start of calving or breeding season and weaning. An undervalued date in cow-calf production is the start of the third trimester.
Off the top of your head and without calculating back from calving, do you know when the third trimester starts for your replacement heifers or cows? I’m guessing it’s not on many people’s radar. If you have a March 1 calving herd with replacements calving before cows, the third trimester starts for both in November.
Let me share why I think we need to pay more attention to that date in spring calving herds. First, it marks a significant upward shift in nutrient requirements of the cow. The same diet that would allow an open cow to gain 0.7 lbs per day, merely maintains body condition and allows for the growth of the fetus in a mature cow in the third trimester of gestation. This upward shift in nutrient demand is commonly accompanied by the growth of a winter hair coat that makes it harder to see if the demand is not met and body condition declines.
If a pregnant cow weighs 1,275 lbs throughout the last trimester, she is actually losing weight because the fetus is growing. In the example shown in Table 1, this would amount to 87 lbs or roughly one body condition score during that period.



Table 1. Effect of pregnancy on body weight


Actual Weight
Months pregnant
Empty body weight


1275
6
1187


1275
7
1168


1275
8
1139


1275
9
1100



 
First-calf heifers or other young cows require energy for growth as well as to maintain pregnancy and need to gain even more weight during the last trimester. Failure to meet these energy demands impacts the fetus in utero, colostrum production and the postpartum interval to return to estrus and rebreed. Both the growth and performance of this calf and the age (weight) of the presumptive next calf can suffer the negative consequences.
Our failure to fully account for the magnitude of this energy demand is often the reason producers have trouble getting cows re-bred, particularly young cows. If we start increasing the energy in the diet at the beginning of the third trimester, we can make steady progress to attain the needed weight gain. If we wait too long, the energy concentration of the diet needed to achieve the gain goes up because we have fewer days to do so.
In the example in Table 2, a higher average daily gain is needed to achieve a body condition score of 5 by calving if the cow needs to regain body condition prior to calving (70 to 80 lbs per body condition change; 2.3 lbs per day) or we allow less time to achieve the needed fetal gain (3.8 lbs per day). If we let the start of the third trimester go by un-noticed, we can quickly be in a hole that we don’t have the feed resources to feed our way out of. Calves in the feedlot gain 3.8 lbs per day with the aid of high energy feedstuffs in their diet, not something we typically feed our cows. You can learn more about nutritional management of cows by body condition in the newly revised publication of that name.
 



Table 2. Weight gain needed during third trimester based on days available and total gain required


Current weight
Current BCS
Days to calving
Weight gain needed
ADG needed (lb/day)


Weight of fetus and fluids
Weight to increase body condition to 5


1250
5
100
150
0
1.5


 
4
100
150
80
2.3


 
4
60
150
80
3.8



 
Producers need to take note of the start of the third trimester because failing to do so is expensive either through higher feed costs, higher replacement rates, lower calf weights and/or poorer calf health. Now is the time to be planning ahead. Test forages and adjust rations accordingly as a part of the adjustments to be monitored at this time. Either when working cows or when doing a routine check, determine the average body condition score of the group and manage mature cows to calve in a body condition score of 5, and 5.5 to 6 for replacement heifers.
You can cross a mountain range over the highest peak or find a pass that is not near as high or challenging to cross over; either one gets you there. Take note of the start of the third trimester to improve calf health and performance and control production costs.
You may find these additional resources useful:
Guide to Body Condition Scoring Beef Cows and Bulls
Body Condition Score Card for Cattle
Body Condition Record Book
Nutritional Management of Cows by Body Condition