Green Leaf, Inc., a Fontanet, Ind., manufacturer of agricultural products, is introducing Lechler Inc.'s IDTA asymmetrical twin flat spray air induction nozzles to the U.S. and Canadian markets. The IDTA nozzle provides spray coverage on the front and rear sides of treated crops and turf, and it is designed to deliver these improvements at higher driving speeds.The explanation is that these improvements are achieved by the asymmetrical design featuring spray angles, pattern widths, and flow rates that work together for ideal target coverage and drift reduction.
Lechler's patented "pinch to remove" maintenance system allows the nozzle to be checked and cleaned quickly, which reduces down time. The nozzle is recommended for insecticides, fungicides, and plant growth regulators in agricultural and turf applications. The IDTA asymmetrical twin flat spray air induction nozzle is being promoted as setting a new benchmark in double flat fan technology. The nozzle will be available in ISO sizes 02 through 08 and available exclusively from Green Leaf and their retailers.
Features of the Lechler IDTA asymmetrical twin flat spray air induction nozzle as highlighted by Green Leaf are:
Higher drift reduction across the entire working range of flow and pressure.
One-piece nozzle fits bayonet style nozzle body connection systems.
Twin flat spray jets with 30-degree front and 50-degree rear directional patterns.
120-degree wide front pattern, with 60 percent of the flow rate.
90-degree wide rear pattern, with 40 percent of the flow rate.
A finer droplet spectrum to the front provides optimal coverage.
A coarser droplet spectrum to the rear reduces drift and captures fines.
Use for boom end and border applications. 
Increased coverage on vertical targets, with more deposition on front and rear sides of plants.
Ceramic core for long service life.
More about Green Leaf can be found at www.green-leaf.us