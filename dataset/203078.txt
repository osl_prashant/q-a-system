(UPDATED,  Jan. 31) President Donald Trump is considering a 20% tax on imports from Mexico to fund construction of a border wall, and United Fresh Produce Association president Tom Stenzel has warned such a move could start a trade war.
 
White House press secretary Sean Spicer told CNN and other media outlets in an informal briefing that Trump was considering a 20% tax on Mexican imports, along with a number of other ideas, to fund construction of the border wall.
 
White House chief of staff Reince Priebus later said on "Face the Nation" that a wall could be funded in a variety of ways, including by a tax on imported goods, a formula on import and export taxes and credits, paid for by drug cartels or by fees paid by people crossing illegally.
 
Stenzel said in statement that a 20% tax on Mexican imports would be bad for consumers and risky for U.S. produce exporters.
 
"It is very troubling for world food and agricultural markets for administration spokespersons to bandy about terms like a 20% tax on all imports from Mexico or other countries," Stenzel said in the statement.
 
"If the administration does choose to renegotiate trade agreements and ask Congress to work toward imposing such a tariff on foods, we risk provoking a trade war that would harm both American agricultural producers and consumers," he said.
 
Stenzel said consumers would bear added costs, especially for products like bananas, mangoes and other commodities not grown in the U.S.
 
"As the administration looks to incentivize manufacturing jobs in the U.S., we urge President Trump to consider the unique nature of food and not place a new 'food tax' on American consumers," Stenzel said.
 
Lance Jungmeyer, president of Nogales, Ariz.-based Fresh Produce Association of the Americas, said the group is concerned about any potential border tariff or fee imposed at the border and how that could hurt U.S. consumers.
 
"We want to understand what it will mean, and our main concern is that we don't want to see a situation where Americans are paying more unnecessarily for fruits and vegetables," he said Jan. 31. The FPAA is working with Arizona's Congressional delegation to make sure they know the importance of the issue.
 
Jungmeyer said that fresh fruits and vegetables have been crossing through Nogales from Mexico for more than 100 years and the trade will be resilient no matter what happens. However, Jungmeyer said a import tax could reduce shipments and hurt both consumers and retailers, especially in the winter months. 
 
Mexico is the second-largest source of all U.S. food imports, accounting for about $19.23 billion in 2014, or 16% of total U.S. food imports. Total imports of all goods from Mexico totaled $295 billion, according to U.S. trade statistics.
 
Mexico is the dominant source for U.S. imports of fruits and vegetables. U.S. imports of Mexican fruits and vegetables together accounted for nearly $10 billion in 2015. In 2015, Mexico accounted for 68% of all
 
U.S. fresh vegetable imports and 42% of all U.S. fruit imports, according to the U.S. Department of Agriculture.
 
A 20% tax on all Mexico food imports would total about $3.8 billion per year, nearly $12 per person in the U.S. Senate leader Mitch McConnell, R-Ky, has estimated the border wall will cost between $12 to $15 billion to build.
 
The top two fresh produce commodities imported from Mexico are fresh tomatoes and avocados. From January though November 2016, the U.S. imported $1.8 billion in fresh tomatoes and $1.6 billion in avocados, according to U.S. Department of Agriculture trade statistics. Based on those import values, a 20% tax just on fresh tomatoes and avocados would cost U.S. consumers about $680 million.
 
U.S. consumers will foot the bill if the U.S. puts in place a border tax, said Bret Erickson, president and CEO of the Texas International Produce Association. "Prices at the retail store will go up, the restaurant tab will go up and not only that you are also going to have reduced options of fruits and vegetables at the grocery store," he said.
 
Close to half of all produce consumed in the U.S. is imported, he said, and many of those fruits and vegetables come from Mexico, he said.
 
In addition, many jobs in the U.S. - including some at U.S. grower/shipper operations - rely on the import trade, Erickson said. A recent economic analysis of Mexican fresh produce imports by Texas A&M found last year that fresh produce imports contributed to close to $500 million in economic impact and 9,000 jobs in the U.S., Erickson said.
 
The proposed import tax will surely hurt American consumers and growers if it is enacted, said Jessie Gunn, marketing manager for organic grower and distributor Wholesum Family Farms, Nogales, Ariz. 
 
"It is going to be consumers who pay the increase especially in the winter time when the U.S. availability is (limited)," Gunn said. Mexico could also retaliate against U.S. farmers, Gunn said.
 
Gunn said the North American Free Trade Act has generally been positive for both U.S. and Mexican growers, with exports increasing for both groups since the mid-1990s.
 
"Definitely in ag, it is a two-way street," she said. 
 
Wholesum Family Farms has the majority of its growing operations in Mexico, but also has a production facility in Arizona.
 
"The American consumer, at the end of the day, is really going to suffer those price increases," she said.
 
The hope, Gunn said, is that the Trump administration and lawmakers will consider the good agricultural trade relationship between the U.S. and Mexico and scrap the proposal.
 
 
Trouble ahead?
 
Relations between the U.S. and Mexico are strained with Trump administration priorities on building the border wall and Trump's stated intention to renegotiate NAFTA.
 
Mexican President Enrique PeÃ±a Nieto canceled a meeting with Trump that had been planned for early February after Trump tweeted that Mexico's president should skip the meeting if Nieto continued to insist that Mexico wouldn't pay for the wall.
 
"The U.S. has a 60 billion dollar trade deficit with Mexico. It has been a one-sided deal from the beginning of NAFTA with massive numbers of jobs and companies lost. If Mexico is unwilling to pay for the badly needed wall, then it would be better to cancel the upcoming meeting," Trump's Jan. 26 tweet read.
 
On Jan. 27, Trump and Nieto had a productive phone call, according to a joint statement from the countries. The phone conversation focused on the "bilateral relationship between the two countries, the current trade deficit the U.S. has with Mexico, the importance of the friendship between the two nations, and the need for the two nations to work together to stop drug cartels, drug trafficking and illegal guns and arms sales," according to the statement.
 
A trade war with Mexico could hurt U.S. growers and ranchers, putting at risk a market that purchased nearly $18 billion in U.S. ag commodities in 2015 - 15% of all U.S. agricultural exports - including more than $560 million in U.S. fruit exports and $124 million in U.S. vegetable exports.
 
Mexico was the home for 12% of all U.S. fresh fruit exports and 5% of all U.S. fresh vegetable exports in 2015.