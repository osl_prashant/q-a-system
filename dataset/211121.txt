Each operation is different, and each marketing year is different. Retaining ownership of your calves through the finishing phase can be risky, but it also offers rewards for producers who know their genetics’ value and have a firm grasp on a marketing plan.  
This year offers an unusual mix of challenges and opportunities for calf producers. Cattle markets have performed better than anticipated much of the year, which means feeder cattle prices have been higher than anticipated. The market has slumped in recent weeks, but cattlemen see signs of optimism on the horizon. Chief among those is America’s beef exports, and the potential increases from the reopening of the Chinese market and its demand for high quality calves that are age and source verified.
 
An important factor in developing a successful retained ownership program is to find a feedlot that will partner in marketing calves and sharing data. A recent ranch gathering hosted by Dalebanks Angus at the Perrier family ranch near Eureka, Kan., included a panel discussion on collaboration and retained ownership.
 
In 1991, Tim Adams purchased his first bull from Dalebanks to help breed his herd of 20 cows on his ranch outside Wakefield, Kan. Through the years Adams has increased his herd to 380 cows and uses AI to improve his breeding program.
 
Adams knew he was investing in good genetics, but wasn’t seeing the prices at the sale barn he thought his cattle were worth. With some encouragement from Matt Perrier, co-owner of Dalebanks Angus, Adams began sending his cattle to a feedlot.
 
“If you truly want to get what they are worth and know what you’ve got, then you need to feed your own calves out,” Adams says.
 
In addition to running his own cows, Adams breeds a lot of cattle through his custom AI business. 
 
“Tim is as collaborative a person as I know,” Perrier says. “Tim goes out of his way to make sure his neighbors, friends and family members have all of the information available to make good decisions in their cow herds.” 
 
Many of Adam’s AI customers are retaining ownership on their cattle and are getting back carcass and feedlot performance data to aid in breeding decisions down the road. 
“They’ve been able to make better decisions on what to breed their cattle to the next spring or fall,” Adams says.

 
After retaining ownership of his calves for a few years, Adams has been able to command a better price from heifers he doesn’t keep as his own replacements.
 
“If you can sell a replacement heifer for $1,500 in November versus selling a fat steer for $1,600, you can do the math pretty easy,” Adams says. That math would exclude the additional costs of keeping the heifers on feed.
 
“You won’t be able to sell that heifer for that price unless you can provide some carcass information,” he says.
 
Adams feeds the majority of his calves through Tiffany Cattle Co. near Herington, Kan. Since 2007, Shane Tiffany and his brother, Shawn, have owned and operated the 19,000-head custom feedlot. 
“We’ve been so fortunate that we’re 100% customer owned cattle. We don’t own any of them ourselves. We get to feed some of the best cattle in the country,” Shane Tiffany says.
 
They started out with eight customers, and have grown to 180 customers in the past 10 years. The added customer base helped fuel the brothers’ decision to purchase McPherson County Feeders, 70 miles to the west, this summer.
 
With the new feedlot, the capacity will jump to 32,000 head and the plan is to continue building on what they started. “We work really hard in our company, we want to be known as the guys who can take above average cattle and generate above average prices,” Tiffany says.
 
For four consecutive years customers have averaged more than 90% Choice at their original feedlot.
 
“If you are the type of producer who has invested in some high power genetics, you’ve got to figure out what you’ve got,” Tiffany says.
 
One of their customers had a set of Angus-cross calves grading 80% Choice on the heifers and 84% Choice on the steers. The customer made the decision to change his genetics program by breeding with a terminal-cross sire in hopes of capturing more growth. After two years, the heifers had dropped to 69% Choice and steers were grading 75% Choice.
 
Tiffany consulted with his customer who decided to breed with Dalebanks Angus bulls the next season. The change paid off with the heifers grading better than ever at 90% Choice and steers hitting 99% Choice. It also paid off in improved feedlot performance.
 
Tiffany’s customer switched sires to get growth, “but at the same point in time the heifers gained 3.2 lb. per day—this year they gained 3.65 lb. per day. The steers went from gaining 3.8 lb. per day to 4.1 lb. per day,” Tiffany says. “We would have never known that if all we did was take them to the sale barn.”   
 
Adams had a similar experience the past three years after changing to a crossbreeding program on his predominately Angus cows. 
“I was above what Shane was saying, for many years I was in the high 90s percentagewise for Choice,” he says. By crossbreeding with a different sire, Adams had immediately “chopped of 13%” of his percentage of Choice-graded carcasses. 
 
Adams has been pleased with the data he gets from Tiffany. “If your feedlot won’t give you the carcass data back then go feed somewhere else,” Adams says.
 
Most years Tiffany recommends customers take their cattle to the feedlot. However, he regularly works with his customers to project out if the calves will earn more at the sale barn or being fed out, depending on what current market situations are like.
 

“The only way to get the true value of your cattle is to own them all the way,” Tiffany says.
“You’ve got to get carcass data, whether you feed them or sell them to someone, ask for the carcass data back. It is easy to get, and it is the only way to know what you’ve got.”
 
People in the beef industry are competitive, says Dan Thomson, veterinarian and professor at Kansas State University. “We want to do things a little bit better tomorrow than we did today.”
Thomson says when seeking out anyone to collaborate with we want to trust them because the beef industry still has a handshake business mentality. Also, when seeking out partnerships we want to work with people who have shared values.
 
“When you have quality cattle, and quality genetics you want to work with someone who has the shared value in quality cattle feeding,” Thomson says. “They should have the packer partnerships who have the shared value of hitting that high end market.”