About 26 million science, technology, engineering and math (STEM) related jobs go unfulfilled each year. Many students lose interest in these subjects as they enter middle school, and this year Bayer teamed up with 4-H to promote student interest in these subjects.“Bayer and 4-H are launching the ‘Science Matters’ partnership,” says Ray Kerins, Bayer senior vice president of corporate affairs. He says the $800,000 investment will bring Bayer employees and 4-H together for hands-on science activities to showcase why science matters for everyone.
“It’s up to us to reach out to these youth,” he adds. “We’re looking at 25,000 new touchpoints.”
Bayer will be sponsoring the 4-H Agri-Science Summit in the Washington D.C. that teaches students about modern ag practices, career opportunities and provides more than 30 hours of learning and problem solving. In addition the company will provide up to 200 scholarships and offer community grants to extend the program’s reach. Bayer is already a sponsor of the 4-H Youth in Action Award, which the company says will add another dimension to the Science Matters initiative.
 




Connecting students with STEM at a younger age could encourage them to pursue careers in one of the subjects.


© Bayer



“As science literacy wanes it concerns us at Bayer,” Kerins, a former 4-H member, says. "Advancing health and nutrition is what we do best and care about most at Bayer. We can't do that unless we grow tomorrow's innovators today and fill the critical pipeline of future STEM leaders—not only for agriculture, which is increasingly technologically-driven, but for all STEM-related fields.”
The company hopes to see young people, especially women and minorities, encouraged by this program to find solutions for humans, animals and crops globally.
“There are six million students in 4-H. Science Matters will extend the reach of our hands-on STEM programming, which is proven to grow 4-H'ers who are two times more likely than others to enter STEM careers,” says Artis Stevens, National 4-H Council senior vice president and chief marketing officer. “This can empower young people to learn by doing and apply what they learn in their life and community.