Man killed after being pinned between trailer and fence
Man killed after being pinned between trailer and fence

The Associated Press

ALEXANDER, N.D.




ALEXANDER, N.D. (AP) — Authorities say an 89-year-old man was killed after he was pinned between a livestock trailer and a fence post in northwestern North Dakota.
The North Highway Patrol says a 33-year-old Jacob Heen was backing up a tractor and trailer to a fence to load cattle in a pasture Friday morning near Alexander.
Troopers say the victim sustained serious injuries after being pinned and was transported to a Montana hospital where he died.
The man's name has not been released.