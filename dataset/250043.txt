BC-Wheat KX
BC-Wheat KX

The Associated Press

CHICAGO




CHICAGO (AP) — Winter Wheat futures on the Chicago Board of Trade Wed:
OpenHighLowSettleChg.WHEAT5,000 bu minimum; cents per bushelMar525¾527¼521521—8¼May539¼546¼532¾534¼—7¼Jul555½562¼549¾551—6¾Sep570½577¾565¾567½—5¾Dec588¼595¼583585½—5¼Mar596604592¼594¼—5May601¾601¾593½593½—5Jul591595585¼586¾—5Sep591¾—4¾Dec609610602¼602¼—4½Mar606¾—4May606¾—4Jul588¾—4Est. sales 70,771.  Tue.'s sales 60,660Tue.'s open int 300,111,  up 634