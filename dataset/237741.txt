South Dakota man raises Malaysian prawns as hobby
South Dakota man raises Malaysian prawns as hobby

By ELIZABETH VARINAberdeen American News
The Associated Press

IPSWICH, S.D.




IPSWICH, S.D. (AP) — In a series of tanks in the basement of a house in Ipswich, 10-legged creatures are growing.
Ky Tran has been raising Malaysian freshwater prawns as a hobby for about six years, learning by doing.
"When I was in Texas, I went to a farm that actually grows these guys," he said. "They grow them in June and they harvest in October. We bought some and we ate them, and that's when I got hooked."
Nobody wanted to share their secrets on how to grow prawns, so Tran started figuring out how to do it on his own.
He and his family moved to South Dakota from Texas about two years ago. There are problems keeping the pools and tanks heated in this climate, Tran said.
Even so, he hopes to expand his operation in spring, Aberdeen American News reported .
Tran also hopes to increase interest in raising prawns. He sells some live prawns through his website — ktprawns.com — for people to raise at home and has a YouTube channel where he describes different aspects of growing the crustaceans.
"As a country we've imported hundreds of thousands of tons of shrimp from overseas," he said. "I'm trying to encourage more people to do this."
Aquaculture is not a large portion of the agriculture industry in South Dakota, according to the U.S. Department of Agriculture's annual census. There were 14 farms that sold aquaculture products in 2012. That includes catfish, trout, other food fish, bait fish, ornamental fish, sport or game fish and other aquaculture products.
Aquaculture is the raising of aquatic animals or the cultivation of aquatic plants for food.
The total market value of aquaculture in South Dakota was just shy of $2.5 million in 2012 — the most recent numbers available. An aquaculture survey is done only every five years in the state.
For perspective, the market value of all agriculture products in South Dakota was more than $10 billion in 2012.
Regions of the country where freshwater prawn farming has been seriously attempted include Kentucky, northern Mississippi, Texas, Tennessee, California, the Carolinas and Florida, according to Greg Lutz, professor at the Aquaculture Research Station at Louisiana State University. He's also editor-in-chief of Aquaculture Magazine.
There has been interest in producing marine shrimp in low-salinity indoor systems throughout the Upper Midwest, Lutz said. However, those ventures are scattered in various places and few seem to last long enough to turn a profit, he said.
While millions of tons of marine shrimp are farmed each year around the world, there is almost no viable marine shrimp farming in the U.S. due to unsuitable climate conditions and high production costs, he said.
There are some scattered "mom and pop" freshwater prawn operations, mostly in inland regions, Lutz said. That is because most of the retail price of shrimp in those regions involves transportation and cold storage, allowing local producers to compete by marketing their products live or fresh.
___
Information from: Aberdeen American News, http://www.aberdeennews.com


An AP Member Exchange shared by Aberdeen American News.