USDA: Small, midsize farms decreasing in Nebraska
USDA: Small, midsize farms decreasing in Nebraska

The Associated Press

OMAHA, Neb.




OMAHA, Neb. (AP) — The U.S. Department of Agriculture says small and midsize farms across Nebraska have lost much of their land and revenue to bigger operations over the past two decades.
Many of these farms have been caught between falling prices and rising costs, the Omaha World-Herald reported . As a result, the number of smaller farms in the state has decreased while the number of larger farms has risen.
Farms with more than $500,000 in annual sales more than tripled between 1997 and 2012. But farms with revenue between $100,000 and $499,999 decreased 20 percent.
Josh Hladik's family operates a farm in Saunders County. The farm has about 1,000 acres, but Hladik said it likely won't be a sustainable business until they acquire an additional 500 acres. While Hladik has bid on land in at least three auctions, he's consistently been outbid by larger or more experienced operations.
"A publicly traded business would probably not be operating on the margins we're making now," Hladik said. "If the cost of business gets too high, the business decision would be to step aside."
Hladik works a second job selling seed and fertilizer to help make ends meet.
"If we didn't take pride in what we do and in our land we wouldn't be in the business," he said. "We like seeing tangible results from the hard work and the process we go through all year. It makes it worth it."
Farm consolidation means there are more efficient and productive farms, which give consumers a variety of low-priced food options, said Jay Rempe, a senior economist at the Nebraska Farm Bureau.
"The consolidation of farms and ranches has been occurring for years, at least since the end of (World War II), driven largely by adoption of new technology and economies of scale," Rempe said. "Beginning farmers don't have the resources to compete with larger farmers."
___
Information from: Omaha World-Herald, http://www.omaha.com