BC-Wheat KX
BC-Wheat KX

The Associated Press

CHICAGO




CHICAGO (AP) — Winter Wheat futures on the Chicago Board of Trade Wed:
Open  High  Low  Settle   Chg.WHEAT                                   5,000 bu minimum; cents per bushel    May      521½  522¾  509¼  516¾   —5  Jul      541½  541½  528¼  535¾   —4¾Sep      557½  559½  547½  555     —4½Dec      583    583¼  571    579¼   —3¾Mar      597    597    587¾  594¼   —2¾May      604½  604½  592½  601¾   —2  Jul      605¼  605½  595¾  605½   —1½Sep                           612½   —1  Dec      624¾  625½  620    625½   —1  Mar      625    631    625    631     —1  May                           631     —1  Jul                           624¼   — ¾Est. sales 75,330.  Tue.'s sales 87,198 Tue.'s open int 272,176