Following is a snapshot of key figures from USDA's Quarterly Grain Stocks and Acreage Reports:




Acreage


USDA


Avg.


Range


March 31, 2017


2016




in million acres



Corn

90.886


89.903


89.0-91.005


89.996


94.004



Soybeans

89.513


89.750


88.466-90.5


89.482


83.433



All wheat

45.657


46.070


45.7-47.404


46.059


50.154



Winter wheat

32.839


32.830


32.505-33.752


32.747


36.137



Spring wheat

10.899


11.206


10.99-11.589


11.308


11.605



Durum

1.919


2.002


1.90-2.219


2.004


2.412



Cotton

12.055


12.278


12.0-12.50


12.233


10.075




 




Grain Stock Expectations


USDA


Avg.


Range


June 1, 2016


March 1, 2017




in billion bushels



Corn

5.225


5.123


4.690-5.360


4.711


8.616



Soybeans

0.963


0.983


0.870-1.119


0.872


1.735



Wheat

1.184


1.137


0.968-1.185


0.976


1.655






Pre-report expectations compiled by Reuters newswire.