BC-US--Cocoa, US
BC-US--Cocoa, US

The Associated Press

New York




New York (AP) — Cocoa futures trading on the IntercontinentalExchange (ICE) Thursday: (10 metric tons; $ per ton)
Open    High    Low    Settle   ChangeMay                                 2560  Up    11May         2550    2567    2506    2535  Up    11Jul         2570    2589    2532    2560  Up    11Sep         2587    2600    2546    2574  Up    14Dec         2590    2600    2549    2577  Up    15Mar         2576    2584    2542    2563  Up    14May         2577    2577    2548    2569  Up    14Jul         2562    2578    2559    2578  Up    14Sep         2596    2596    2584    2586  Up    14Dec         2606    2606    2593    2595  Up    13