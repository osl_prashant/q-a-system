Click-and-collect and grocery delivery went mainstream in 2017.
Walmart and Kroger continued to develop their infrastructure and expand their offerings. Many other retailers — including Albertsons, Costco, Publix and HEB — partnered with third parties like Instacart.
“2017 was the tipping point year for e-commerce in grocery,” said Diana Sheehan, director of retail insights for Kantar Retail. “It was the year where both large-scale grocers and regional players said to the public, ‘This is here to stay, and this is something we’re going to be good at and get better at every year moving forward.’”
Sheehan also noted that a predictable split has started to happen regarding the question of whether consumers favor pickup or delivery. Click-and-collect has garnered more attention in suburban areas, while delivery has fared better in urban areas.
“For both regional players and for large national players, it means they have to compete in both places, in both fulfillment types,” Sheehan said.
Bill Bishop, chief architect for retail consulting firm Brick Meets Click, also described the rush to delivery and pickup as a significant retail story in 2017.
He said the acquisition of Whole Foods by Amazon prompted much of the scrambling.
“I think it had a huge impact because there’s been a degree of denial in terms of how serious people are about grocery shopping online,” Bishop said. “(The merger) was an electrifying experience for retailers.”
In response, supermarkets are working to provide the same grocery options Amazon aspires to offer, but beat Amazon to the punch.
Sheehan also said Amazon prompted traditional retailers to invest in the digital space. However, she views the nudge as having happened a couple of years ago.
“While Amazon in 2014 and 2015 was the catalyst to get the train out of the station, as that train starting moving, Amazon started falling behind,” Sheehan said, referring to the e-commerce giant struggling to grow Amazon Fresh. “Whole Foods was reaction.”
Either way, the partnership was one that most agree will revolutionize the space.
In the wake of the deal, Edward Jones analyst Brian Yarbrough described the Whole Foods acquisition as the biggest disruption to grocery retail since Walmart supercenters.
Another move likely aimed at keeping up with Amazon was Target purchasing grocery delivery service Shipt. More such deals could follow in 2018.
“Obviously there’s limitations to who can be acquired, but I think what we’re going to see in (2018) is really retailers taking a look at these third-party players, even if it’s Uber, and say, ‘What can we learn from them even if we can’t acquire them?’” Sheehan said.