As the first rounds of negotiations in NAFTA concluded, cattle industry leaders from the U.S. and Canada were happy to see no major upset regarding cattle and beef trade. Still, there are a few improvements U.S. and Canadian producers want to discuss.“There are some small provisions that could be improved, as far as just getting cattle and beef across the border. Any time we can look at a way to take that regulatory burden down, it’s a good thing,” says Colin Woodall, National Cattlemen’s Beef Association, vice president, government affairs. “But for us, we want to make sure the basic provisions that allow the full, unfettered access stay in place. That’s what we are pushing for.”  
One regulation John Masswohl, Canadian Cattlemen’s Association, director of government and international relations, would like to see change is a rule that live Canadian cattle must be born after March of 1999 in order to be exported to the U.S. Most animals are, but providing documentation of each animal’s birthdate is a burden for many producers.
Listen to Masswohl detail other issues Canadian producers are bringing to the NAFTA discussion:
 

 

 
 
Country of origin labeling is a revolving issue for many U.S. and Canadian producers.
“There is always that shadow of COOL [country of origin labeling] and people saying there is a way to bring this back,” Masswohl says. “There are some in the U.S. that will never let that go.”
Masswohl calls the measure “discriminatory,” and promises continue pushback if the issue comes up. Woodall adds this is not the time to reopen old discussion about the topic.
Hear Woodall share what NCBA is doing to prepare for the next round of NAFTA talks:
 

 

 
 
Moving forward, leaders are confident beef and cattle trade will remain in a good position.
“When it comes to provisions regarding beef and cattle trade, we feel pretty comfortable that the Trump administration has the right mindset,” Woodall says.
“One thing that always hits me when I’m up here is how similar our industries are, and how similar our membership is,” he adds. “We have many more similarities than we do differences and that helps us work together well.”
 “For the beef sector, it’s [NAFTA] actually been a very good agreement,” Masswohl adds. “The Canadian, the U.S. and the Mexican beef sectors would all agree with that assessment.”