Baxter Black’s previous commentary on leaving the ranch had hundreds of viewer responses pour in, most worried that Baxter was hanging up his hat on poetry, and that he wouldn’t be on U.S. Farm Report anymore.

Baxter will continue to be a mainstay on the program, and this week, he’s talking mules.

Watch Baxter Black weekends on U.S. Farm Report.