I was enjoying a quiet morning cup of coffee when Russ T. Blade pried himself out from the top drawer of my desk. 'Rusty,' as readers know, is the miniature and imaginary produce manager who drops by on occasion to talk shop.
 
Rusty: Is it my imagination or do you always have your nose in that business quote book?
 
Me: Huh? Oh, hey, here's a good one from Warren Buffett. Ahem. 'If you aren't willing to own a stock for 10 years, don't even think about owning it for 10 minutes.'
 
Rusty: I know you're no stock genius. But there is something to be said about investing.
 
Me: Like buying into our own produce retailing craft?
 
Rusty: Sort of. I remember Larry - an old pal. He used to adjust stubborn-fitting metal shelving in the store by using his spare change as spacers in the shelf standard brackets. It helped make the shelving nice and level.
 
Me: His own money?
 
Rusty: He confided that he had 'invested' about $15 in his pocket funds over the course of a decade or so. But it showed that Larry cared enough to apply a part of himself to the business.
 
Me: I think I've seen enough produce departments to agree that every store has little tell-tale signs of who the produce manager is, enough to see their signature in the store.
 
Rusty: I know that I prefer a certain look: Neat, clean vegetable wet racks, regularly scrubbed and reset each week according to what is on ad. I like my crew to follow a routine that includes regular culling and straightening of the produce displays. That sort of thing.
 
Me: I remember making sure that every spillover-type produce display used new, clean shipper boxes, and they all faced the same direction. This, followed by as much hand-stacking as time allowed.
 
Rusty: I can't believe we're agreeing on these things. Usually we're at odds on a point or two. But all the things a produce manager does with his displays, his attention to detail, heck - even the change in his pocket to level a shelf - that's part of his investment in the produce department.
 
Me: True. Even if a chain has set merchandising schematics, it's still up to the produce manager to make the department his own.
 
Rusty: To me it's all about planning, good ordering, consistency in stocking and rotation. It's investing in training, making sure that displays are full, level and inviting. It's insisting that all the signs are in place, clear and accurate.
 
Me: It's the manager's workplace, the investment needs to be maintained.
 
Rusty: Especially 'owning' something in the long run. By the way, if you're getting a coffee refill, I'm ready too.
 
Armand Lobato works for the Idaho Potato Commission. His 40 years' experience in the produce business span a range of foodservice and retail positions. E-mail armandlobato@comcast.net.
 
What's your take? Leave a comment and tell us your opinion.