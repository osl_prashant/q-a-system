Big news: Wayne Pacelle, president and CEO the Humane Society of the United States, was forced to resign last week amid allegations of sexual harassment, according to reporting in USA Today and several other national news organizations.
“The claims of sexual harassment sent tremors of discontent through the organization and prompted at least seven board members to resign,” the newspaper reported.
Pacelle had served as the leader and chief spokesperson of HSUS for the past decade and was widely regarded as one of the most visible and vocal proponents of animal activism in the country.
And now he’s gone.
Following his departure, HSUS named Kitty Block, an attorney and president of Humane Society International, as acting president and CEO.
“The last few days have been very hard for our entire family of staff and supporters,” Rick Bernthal, HSUS chairman of the board, said in a statement posted on the organization’s website. “We are profoundly grateful for Wayne’s service to the cause of animal protection and welfare.”
I think I’m on pretty safe ground when I state that most people in animal agriculture are profoundly grateful that he got fired . . . er, resigned.
The Price of Pride
Composing a post-mortem on the departure of people who were prominent in their field — in this case, the anti-industry activist community — is a little like writing somebody’s obituary: you’re supposed to say a few nice things about the dearly departed, rather than merely shoveling dirt on what, for Pacelle, is a seriously tarnished legacy.
So here goes, starting with the dirt.
Pacelle, to put it mildly, was as arrogant as he was articulate. Always a master of the diplomatic turn of a phrase (meant to placate moderates and position HSUS as “reformist,” rather than radical), he nevertheless triggered one inescapable reaction among virtually everyone he encountered, save his most ardent acolytes: I’m Wayne Pacelle.
And you’re not.
The good book says that pride goeth before a fall (actually before “destruction”), and there’s no better Exhibit A to illustrate that truism than Wayne Pacelle. Although he always portrayed his and HSUS’s mission as simply “improving the lives of animals,” it’s hard to dismiss the suspicion that his ultimate goal was a vegan society in which animal husbandry went the way of the horse and buggy.
To be sure, he was always cagey when confronted with the question, Are you out to eliminate livestock production? I asked him that exact question myself on several occasions, and he always sounded reassuring: Oh no, no; we just want to end the cruel treatment and needless suffering of food animals.
His go-to rejoinder when pressed on that issue was, “We’re not like PETA. They’re extremists. We’re not like them.”
Which is little like saying, “Star Wars: The Last Jedi isn’t anything like Star Trek II, III, IV, V or VI. They’re just sequels; The Last Jedi is the second installment of the third Star Wars trilogy.”
Right.
To be sure, Pacelle served his constituents well, turning HSUS into a powerhouse fund-raiser, while at the same time deflecting criticism that the group’s ubiquitous “help rescue poor, starving pets” ad campaigns disguised the fact that HSUS was separate and distinct from the hundreds of local Humane Society shelters around the country and in fact contributed but a pittance to on-the-ground efforts to promote adoption of rescued dogs and cats.
There’s no denying that he could be sincere, engaged and quite charming when the occasion served, which I’m speculating is why he (allegedly) got himself into trouble with inappropriate sexual advances.
He was a walking, talking embodiment of the timeless admonition: don’t hate the man; hate the mission.
Intelligent, well-spoken, charismatic, Pacelle was a formidable opponent, if for no other reason than it was difficult to portray him as a villain. Even when confronted with pointed condemnation of how HSUS raised, stashed and spent its multi-millions of dollars in donations, he never took it personally — at least not outwardly — and always tried to steer the conversation toward some (often imaginary) common ground.
Now, however, a far different kind of criticism has been lodged against him, and this time, deflection didn’t work so well.
Editor’s Note: The opinions in this commentary are those of Dan Murphy, a veteran journalist and commentator.