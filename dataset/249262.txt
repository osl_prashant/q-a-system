ILO finds progress in fixing Thai fishing industry abuses
ILO finds progress in fixing Thai fishing industry abuses

The Associated Press

BANGKOK




BANGKOK (AP) — A survey of working conditions in Thailand's fishing and seafood industry conducted by the U.N.'s International Labor Organization has found that new regulations resulted in progress in some areas, including less physical violence, but problems such as unfair pay and deception in contracting persist.
The European Union in April 2015 gave Thailand a "yellow card" on its fishing exports, warning that it could face a total ban on EU sales if it didn't reform the industry. Thailand's military government responded by introducing new regulations and setting up a command center to fight illegal fishing.
The ILO report released Wednesday recommends that the Thai government strengthen its legal framework, ensure effective enforcement, establish higher industry standards and enhance workers' skills, knowledge and welfare.