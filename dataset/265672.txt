New marijuana laws in Utah won't stop ballot initiative
New marijuana laws in Utah won't stop ballot initiative

By BRADY McCOMBSAssociated Press
The Associated Press

SALT LAKE CITY




SALT LAKE CITY (AP) — It will be legal next year in Utah for approved farmers to grow medical marijuana for researchers and dying patients under one of several new legislative measures signed by Gov. Gary Herbert this month.
The state will also monitor the safety of marijuana extract oils being sold in stores as part of a package of legislation that Rep. Brad Daw, the Republican who sponsored four of the five measures, said Monday moves the state forward at the right pace.
Advocates of broader marijuana legislation disagree.
They say they plan to get an initiative on the ballot in November that would allow voters to approve a state-regulated marijuana growing and dispensing operation to allow people with certain medical conditions to get a card and use the drug in edible forms like candy, in topical forms like lotions or balms, as an oil or in electronic cigarettes— but not for smoking.
Christine Stenquist, president of a group called Together for Responsible Use and Cannabis Education, said she applauds the move to regulate the sale of the oil extract that is used by people with severe epilepsy, but that most of the legislation is hollow.
"It isn't actually moving the ball forward, it's delaying the conversation," Stenquist said. "It's smoke and mirrors."
She said the group already has 117,000 verified signatures for the ballot initiative— more than the 113,000 needed by the April 15 deadline.
Under one of the new laws, farmers vetted by the Utah Department of Agriculture and Food will be allowed starting next year to grow marijuana that would be converted into forms such as pills, gel caps and oils, Daw said.
It can be used for research or for medical use by dying patients who, under another new law billed as the "right to try" legislation, can get medical marijuana with a doctor's note saying they have six months or less to live.
Daw is cautious about approving any use of full-strength marijuana, which he worries is addictive, but he said the risk is low in this case. "What worse thing is going to happen to them?" he said.
The benefit is two-fold, he said: It may help ease their pain while helping the state gather information about how best to use medical marijuana.
Stenquist detests what she considers a ridiculous law, which she dubs the "right to try if you promise to die" measure.
Another measure will allow for the growth and sale of industrial hemp by state-licensed companies. That movement got a big boost Monday from U.S. Senate Majority Leader Mitch McConnell, who said he'll introduce legislation to legalize hemp as an agricultural commodity and have it removed from the list of controlled substances.
The state will also set up a registration system that will require manufacturers of an oil called cannabidiol or CBD, a derivative of cannabis, to be registered to legally sell in Utah stores, said Jack Wilbur, spokesman for the Utah Department of Agriculture and Food. The goal is to ensure no harmful substances reach consumers.
The CBD oil has a chemical that may fight seizures and is designed not to produce a high.
The state passed a law in 2014 that allowed parents of children with severe epilepsy to use the oils to provide to their children. In the beginning, most parents traveled to Colorado to get the oil. But now an unknown number of stores, including smoke shops and health food stores, are selling the oils, Stenquist said.
A final measure provides funding for one staffer at state board that reviews research on CBD, and allows the board to review research from around the world.
"I think we moved the ball down the field," Daw said about the measures. "A lot of people tell me I moved too far. A lot of people tell me I'm not going far enough. I say, OK, I've probably hit the sweet spot."
Daw said the ballot initiative would make the dispensing and use of marijuana far too wide open.
Stenquist counters that Utah's slow approach is depriving people with chronic illnesses the chance at life-changing relief. The ballot initiative would create a system where people can get clean, tested marijuana and work with their doctors to see what helps them, she said.
"Patients won't have to go to a parking lot to get a bag," Stenquist said.