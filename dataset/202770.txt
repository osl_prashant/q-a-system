McAllen, Texas-based Fox Packaging is investing in a facility upgrade by June to speed production times.
 
The second generation family-owned business is expanding its production facility and investing in new printing and conversion machines, according to a news release.
 
"We highly value the long-lived relationships we have with our customers, and intend to continue to put them first as we evolve as a company," Craig Fox, vice president of Fox Packaging, said in the release. "We are committed to bringing the industry the best solutions, which fuels our passion for innovation to create more efficient machines and effective packaging."
 
Customers of Fox Packaging, a specialist in flexible packing technology, will benefit from shorter lead times and higher order capacity resulting from the company's new printer, press, and conversion machines, according to the release.