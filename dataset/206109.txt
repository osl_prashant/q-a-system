When you farm the ancient, steeply sloping coulees of southwest Wisconsin’s unglaciated Driftless Area, soil conservation is not a luxury, it’s your livelihood. Take care of that soil, and it will provide corn silage yields well above 20 tons per acre and grain yields of 275 bu.
Tim and Mark Keller, Mount Horeb, Wis., are just the second generation of Kellers to farm this ground. Kellercrest Holsteins was founded in 1968 by their parents, Dan and Jeanne. From the beginning, the Kellers used contour strips to farm the sides of the coulees, 90' wide strips with fields ranging from an acre and a half to 15 acres. The ground ranges in slope from 2% to 20%, probably averaging 10%, says Mark, who serves as the farm agronomist.
Constant manure applications over the past half century and cattle numbers that quintupled in a herd expansion 15 years ago, meant phosphorus levels also climbed. Even with the contours, heavy rains and conventional, bare ground tillage meant the land was susceptible to sediment erosion and phosphorus losses.
Milking 300-plus cows and being one of the largest dairy farms in their neighborhood also meant the Kellers had high visibility from both rural and urban neighbors. Their 600- plus acres of cropland, both rented and owned, also drains into two Class II trout streams, heightening scrutiny from fishermen and conservationists.
“In 2007, Dane County came to us to see if we might be interested in a project to reduce soil losses because we were one of the higher phosphorus farms in the area,” Mark says.
“We were already doing a lot of good things, such as contour strip farming since the 1960s, but we are always open to new suggestions. Dane County officials said they’d give us options on how to do things better, and we were all in…. We don’t want to lose any soil because it’s just money down the creek.” That was 10 years ago. Efforts since that time have documented results:

A 55% reduction in phosphorus loss levels during storm events.
Some 4,400 lb. of less phosphorus loss per year during those events.
About 1.3 tons per year reduction in sediment losses.
Possible decrease in downstream algae blooms of 135,000 lb.

As a result of this progress, Kellercrest Holsteins has received the 2017 Outstanding Dairy Farm Sustainability Award from the Innovation Center for U.S. Dairy.
Kellercrest Holsteins was recognized as a leader in the Pleasant Valley Watership Project, which is a collaborative effort among several farms, USDA’s Natural Resources Conservation Service, Dane County Wisconsin Land Conservation District, the University of Wisconsin, the Nature Conservancy and other local, state and national agencies. The U.S. Geological Survey estimates the conservation efforts employed in the project reduced phosphorus and sediment losses by half. 

The Kellercrest crew: Back row, from left, Kareen and Mark Keller, Sandy and Tim Keller. Front row, Sandy and Tim’s children, Andrew and Kim.
 
Ryegrass cover crop
To protect bare soil after corn harvest, a Dane County agronomist suggested experimenting with some type of winter cover crop. Mark worked with winter rye when he was a consulting agronomist prior to coming back to the farm.
“I knew winter rye would eat phosphorus very well,” he says. And the Kellers were short of feed anyway. So planting winter rye after the corn came off and harvesting it the next spring proved to be a triple win.
The rye is also a little lower in feed quality than alfalfa haylage, which allows them to more easily balance heifer diets for growth and body condition. Double-cropping the corn ground also provides more total tonnage of grown feed and reduces the amount of forage purchased.

Anglers have seen a quick response in fish species and abundance of trout in the Pleasant Valley watershed. 
 
Because of their high phosphorus soil tests, the Kellers also stopped all phosphorus fertilizer in 2009, relying solely on manure for that nutrient. That saves the Kellers about $18,000 a year in fertilizer cost, which adds up to $144,000 since 2009. They do use some potash as starter fertilizer and side dress with 45 to 50 units of 28% nitrogen when the crop is growing.
The Kellers upgraded to a 15", sixrow no-till corn planter in 2007. They had been using a 38" four-row planter, and doubling back to get 19" row spacing. “Since going no-till, our overall tonnage on corn silage has improved as well as our grain yields,” Mark says.
Kellers are proud of their farm’s long history of conservation. “We grew up this way, and we always understood we had to protect our soil. Our soil is our livelihood,” Mark says. 
 
Note: This story appeared in the September 2017 issue of Dairy Herd Management.