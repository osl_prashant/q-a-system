(Bloomberg View) -- I am of the view that incompetence, random error and sheer complexity explain most of the mistakes and strange events in our world, and that we shouldn’t readily jump to conspiracy theories. I’m pretty sure Neil Armstrong did walk on the moon, and still inclined to think (although not certain) that Lee Harvey Oswald acted alone, there is no Bigfoot and aliens have not recently visited humans on Earth.This exercise is not merely to pile up arguments for what you believe, but also to consider how and where you might be wrong. I recently raised this question with a few friends: Which is the most underrated conspiracy theory? Even if you think conspiracy hypotheses are all likely to be false, which one is most plausible -- at least relative to the probabilities assigned by the intellectual and media mainstream.To approach such an investigation, you might ask how much you believe improbable testimonies from witnesses who give every indication of being normal people. If you find sane witnesses persuasive, you might think there is some chance of UFO accounts being true (perhaps with a conspiracy-based coverup). There have been a variety of sober accounts of UFO visitations, most notably the story of Betty and Barney Hill.Unfortunately for this nomination, psychological research on self-deception and the literature on the unreliability of witness testimony suggest that our minds can talk us into believing all sorts of things happened that actually didn’t. So witnesses don’t sway me much. I notice also that UFO claims have plummeted since the advent of mobile phones with cameras (“Oh, did you get a photo of them?”). And so I must look elsewhere for the most plausible conspiracy theory. The Bigfoot and Yeti tales take a tumble for similar reasons, and I don’t think anyone actually saw Elvis or Jim Morrison walking around in the 1990s.Another way to search for true conspiracies is to scour history for deathbed confessions. Did any Cuban or Soviet agents, shortly before dying, blurt out that they knew the true story of President John Kennedy’s assassination? As far as I know, these admissions are hard to come by. That’s another reason for not believing in most conspiracy theories.We might instead look for a very improbable series of events and decide there might be a conspiracy behind them. When I read about the disappearance of Malaysia Airlines Flight 370, the accounts boggle my mind and I can’t come up with a rational explanation of what happened. I am tempted to explore the notion of a conspiracy between the pilots and foreign powers, the likelihood of terrorists, or what cargo the plane was carrying, and so on. Still, improbable events only get me so far toward believing in an actual conspiracy. Precisely because the story of the flight is so complex, and the possible hypotheses so varied -- plus I know relatively little about most matters of aviation -- I don’t budge very far from my basic agnosticism on the matter. A truly plausible conspiracy theory ought to be relatively straightforward.Another set of candidates are claims that a semi-secret group actually is controlling the world, such as the Trilateral Commission was once believed to do. Yet the most plausible versions of these views simply boil down to wealthy, well-connected people having a lot of influence. That just isn’t much of a secret. There’s no particular gain in plausibility from asserting that a specific cabalistic getaway, held on a particular weekend, is where the actual decisions are made.So what then do I think is the most probable and most underrated conspiracy?It’s possible that the fixing of major sporting events might still be going on, and perhaps has been going on for a long time. If Volkswagen AG will doctor its emissions software, or Wells Fargo & Co. will create millions of phony accounts for profit, is it so implausible that a few major athletes -- or better yet referees -- throw games or at least influence the point spread, either for money or to neuter mob threats to their families?We’re learning all the time that insider trading and financial corruption are quite widespread. We’ve also learned how many athletes were willing to take steroids and other performance-enhancing drugs, even when that is explicitly against the rules. Barry Bonds and Roger Clemens, arguably the greatest hitter and pitcher of their era in baseball, are at least reputed to have been steroid rules violators.Is fixing sporting events such a complicated story? No. Does it have a clear motive? Yes. Would we expect many deathbed confessions from those athletes and gangsters who promulgated it? Probably not.Now let me get back to listening to some Paul McCartney songs, because he is very much alive …This column does not necessarily reflect the opinion of the editorial board or Bloomberg LP and its owners.Tyler Cowen is a Bloomberg View columnist. He is a professor of economics at George Mason University and writes for the blog Marginal Revolution. His books include “The Complacent Class: The Self-Defeating Quest for the American Dream.”To contact the author of this story: Tyler Cowen at tcowen2@bloomberg.net. To contact the editor responsible for this story: Stacey Shick at sshick@bloomberg.net. For more columns from Bloomberg View, visit http://www.bloomberg.com/view.©2018 Bloomberg L.P.