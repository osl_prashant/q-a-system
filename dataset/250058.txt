BC-Cotton
BC-Cotton

The Associated Press

NEW YORK




NEW YORK (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Wednesday:
OpenHighLowSettleChg.COTTON 250,000 lbs.; cents per lb.Mar87.7587.7584.2084.20+.36May82.5083.7781.7882.86+.43Jul82.5683.6782.0082.55+.06Sep78.05+.07Oct78.85+.17Nov78.05+.07Dec77.8078.2577.5078.05+.07Jan78.11+.04Mar78.0078.2577.6178.11+.04May77.9878.0577.6077.94—.10Jul77.8377.8377.5077.66—.17Sep72.56—.04Oct75.33—.15Nov72.56—.04Dec72.5172.6072.5072.56—.04Jan72.92—.04Mar72.92—.04May73.69—.04Jul73.92—.04Sep72.97—.04Oct73.61—.04Nov72.97—.04Dec72.97—.04Est. sales 40,696.  Tue.'s sales 66,996Tue.'s open int 270,288