Fertilizer prices were mixed but mostly lower on the week with UAN28% and potash posting our only gains.
Our Nutrient Composite Index softened 2.18 points on the week, now 35.63 points below the same week last year at 506.49.

Nitrogen

Urea led overall declines in the nitrogen segment this week falling $1.14, just squeaking by anhydrous, which fell $1.09.
UAN28% was led higher by mild price firmness in Indiana, Ohio and Minnesota.
On an indexed basis, UAN28% widened its premium to anhydrous ammonia and to the rest of the nitrogen segment. Historically, when 28% is priced at the top of the N segment, it has presaged a downtrend. At this point, UAN28's premium is something of an anomaly since the current downtrend is well established and may be set to come to a seasonal end.

Phosphate

Phosphates were lower with DAP, once again, falling much more aggressively than did MAP.
From state to state, DAP was heavily weighted toward downside movers while MAP price action was more mixed.
The DAP/MAP spread is slightly wider than our expectations and nearly unchanged on the week at 20.83.
Wholesale DAP and MAP are mildly higher at terminals in Morocco, Central Florida and at NOLA, which may support mild gains in retail prices this fall.

Potash 

Indiana and Ohio urged our regional average price higher although price moves were a mixed bag.
On an indexed basis, the premium potash prices hold to anhydrous ammonia widened to $21.83 on softness in NH3.

Corn Futures 

December 2018 corn futures closed Friday, August 11 at $4.08 putting expected new-crop revenue (eNCR) at $648.80 per acre -- lower $6.78 per acre on the week.
With our Nutrient Composite Index (NCI) at 506.49 this week, the eNCR/NCI spread narrowed 4.60 points and now stands at -142.31. This means one acre of expected new-crop revenue is priced at a $142.31 premium to our Nutrient Composite Index.





Fertilizer


7/31/17


8/7/17


Week-over Change


Current Week

Fertilizer



Anhydrous

$475.02
$471.46

-$1.09

$470.37
Anhydrous



DAP

$446.48
$444.30

-$5.00

$439.30
DAP



MAP

$461.74
$461.04

-91 cents

$460.13
MAP



Potash

$332.43
$332.06

+51 cents

$332.57
Potash



UAN28

$235.46
$236.00

+$1.03

$237.03
UAN28



UAN32

$256.51
$254.57

-$4.20

$250.38
UAN32



Urea

$325.51
$325.16

-$1.14

$324.02
Urea



Composite

510.21
508.67

-2.18

506.49
Composite