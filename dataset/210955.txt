To work, microbes in silage inoculants need to be forage competent. Top-tier manufacturers ensure this in their silage inoculant strains by beginning the selection process in silages themselves. There are three major considerations:

Bacterial strains must be screened and selected for the desired properties
The dose rate must be defined
Efficacy must be demonstrated in different crops and storage structures

Beneficial organisms are those that produce lactic acid rapidly to reduce the pH quickly and/or produce acetic acid and/or propionic acid to inhibit spoilage yeasts and molds.
These beneficial organisms are mainly lactic acid bacteria, some of which may also help to make the silage anaerobic. The organisms need to be screened and selected for specific properties, for example: the ability to grow aerobically and to scavenge oxygen from the ensiled crop; rapid growth and lactic acid production (they are not always linked) across a range of moisture levels and temperatures. These screening programs also eliminate strains with negative properties, such as protein degradation.
Once the strains have been identified and screened, then manufacturers must define and validate the dose rate required to accomplish the goal in the crop(s).
Finally, the prototype product must be validated in different situations — different crops, different storage structures and across a range of environmental conditions.
This process is required so that the product works in “real-life” on farms. A reliable manufacturer should be able to share their process and scientific research with producers.
For additional information, visit www.qualitysilage.com or Ask the Silage Dr. on Twitter or Facebook.