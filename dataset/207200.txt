Chicago Mercantile Exchange lean hog futures closed higher on Wednesday after bargain buyers and investors covering short positions reversed Tuesday's slide to an 8-1/2 month low, traders said.Prior to Wednesday's session CME hogs had a relative strength index (RSI) reading of 17. An RSI below 30 suggests a market is technically oversold and due for an upward correction.
October ended 1.125 cents per pound higher at 61.350 cents, and December finished up 1.050 cents at 57.200 cents.
Lean hog futures rose in the face of weaker wholesale pork values and lower prices for slaughter-ready, or cash, hogs as supplies build seasonally.
"There is too much pork and packers are having a hard time finding a home for it. Also hog numbers are starting to ramp up, with weights increasing rather strongly," said Linn Group analyst John Ginzel.
Cash prices were further pressured by packers needing fewer hogs as plants prepare to shut down during the U.S. Labor Day holiday.
Live Cattle Gain Modestly
CME live cattle finished moderately higher on short covering while digesting early-week cash prices and recent wholesale beef price firmness, said traders.
August live cattle, which will expire on Thursday, ended 0.300 cent per pound higher at 105.550 cents. Most actively traded October finished up 0.025 cent at 106.125 cents.
A small number of animals at Wednesday's Fed Cattle Exchange sold at $105 per cwt. Cash cattle last week in the U.S. Plains brought $106 to $107.
Feedlots priced remaining cash cattle in the Plains at $108 to $110 with no response bid-wise from packers.
Processors seem satisfied with the current supply situation heading into next week's holiday-shortened work week that could disrupt the flow of product to grocers - which helped shore up wholesale beef prices, a trader said.
Cattle feeders will resist taking less money for their animals knowing packers are operating their plants with phenomenal margins, he said.
Market participants continue to track Tropical Storm Harvey which is forecast to bring showers to the Ohio Valley eastward over the three-day grilling holiday weekend, which could hurt meat demand in the region.
Technical buying, modest live cattle futures advances and higher cash feeder cattle prices lifted CME feeder cattle contracts.
August feeders, which will expire Thursday, closed up 0.300 cent per pound at 142.900 cents. Most actively traded September closed 0.725 cent higher at 143.750 cents.