Dave’s Specialty Imports will have its own strawberries from Florida this winter, as it will ship from about 100 acres.
Leslie Simmons, marketing manager for the Coral Springs, Fla.-based company, said Dave’s will have light supplies in December with peak volume January through March.
“We hope it complements our Mexican strawberry program and gives more options for our customers who desire a local or domestic product,” she said Nov. 30. “It also adds to our domestic program overall, as we’ve continued to increase domestic blueberry and blackberry volumes each year.”
She said Dave’s has partnered with Florida growers in the past but having its own acreage in Zolfo Springs in Central Florida is a big advantage.
Most of the berries will be packed in 1-pound clamshells, she said.