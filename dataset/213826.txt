Crop calls 
Corn: 1 to 2 cents lower
Soybeans: 2 to 4 cents lower
Wheat: 1 to 2 cents lower
Corn, soybean and winter wheat futures faded as the overnight session progressed, with traders taking into account slight improvement in corn condition and a quicker-than-expected pace of soybean harvest. Meanwhile, USDA reports 75% of the winter wheat crop was planted as of Sunday and will release its first national condition ratings next week. Key this morning will be if traders view overnight losses as a buying opportunity or if pressure builds from late-overnight price weakness.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Cattle futures are expected to see a mixed start as traders wait on cash signals. Futures did a good job of absorbing the negative Cattle on Feed Report yesterday, which paves the way to followthrough buying this morning. However, the beef market started the week poorly, which makes bulls cautious about this week's cash tone. Hog futures are also expected to see a choppy tone to start the day, with pressure limited by an expected firmer tone in the cash market. Traders are also taking yesterday afternoon's Cold Storage Report into consideration this mooring. The report showed frozen beef and pork stocks up from the previous month, but down from year-ago levels.