Wildfires burned in Texas, Colorado, Oklahoma and Kansas earlier this year, destroying thousands of acres in its path. 

Kansas ranchers were promised some help from the government for rebuilding fence burned by the blaze. However, those ranchers are still waiting on payments. Some say there is not enough government staff with the Farm Service Agency (FSA) to assist fast enough.

AgDay national reporter Betsy Jibben investigates and talks with ranchers Bernie Smith, in Beaver County, Okla., and Jenny Betschart, in Ashland, Kan.

She also spoke with Rep. Roger Marshall (R-Kan.) and Steven Peterson, acting administrator of the FSA.