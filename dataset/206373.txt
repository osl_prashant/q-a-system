A new report calls for stricter limits on the use of medically important antibiotics in livestock production. The report, from the Expert Commission on Addressing the Contribution of Livestock to the Antibiotic Resistance Crisis, was supported by the Antibiotic Resistance Action Center at the Milken Institute School of Public Health at the George Washington University and the Natural Resources Defense Council.
The Expert Commission includes a distinguished list of medical doctors, veterinarians and public-health experts engaged in antibiotic-resistance research. The report, titled “Combating Antibiotic Resistance: A Policy Roadmap to Reduce Use of Medically Important Antibiotics in Livestock,” is available online.
The authors note that the 2015 U.S. National Action Plan to Combat Antibiotic-Resistant Bacteria (CARB) focused largely on antibiotic use in human medicine and set measurable reduction goals for hospitals and clinics. For antibiotic use in food animals, the authors suggest he plan falls short by emphasizing a phase-out of antibiotics in animal feed or water for growth promotion purposes.
According to the report, about 70% of medically important antibiotics sold in the United States are sold for use in food-producing animals, not people. Furthermore, the U.S. ranks second globally among users of antibiotics in food animal production, accounting for roughly 13% of the world’s total.
The commission’s recommendations include:
Decreasing Livestock Use of Medically Important Antibiotics 
·         Set targets for reducing antibiotic use.
·         Phase out routine or programed use of medically important antibiotics.
·         Reduce the need for antibiotics by adopting non-antibiotic best practices, and by innovating new technologies, to maintain animal health and prevent disease.
·         Eliminate antibiotic use where efficacy can no longer be demonstrated.
·         Prioritize the use in veterinary practice of antibiotics that the WHO does not categorize as “critically important” for human medicine, such that:
A.      Antibiotics in the “Critically important” category are only used to treat animals sick with a specific bacterial disease. Use of the subset of critically important antibiotics the WHO refers to as “highest priority” also should require testing that confirms the bacterium involved is not susceptible to other antibiotics.
B.      Do not approve for food and animal use any “critically important” antibiotics, such as carbapenems, that are not currently FDA-approved for this purpose;
·         Bolster veterinary oversight of antibiotic use with other safeguards.
Monitoring Antibiotic Use to Reduce Antibiotic Resistance 
·         Develop a system for collecting detailed, comprehensive data on actual antibiotic use, and collect essential data.
·         Coordinate with and learn from the other countries in developing a comprehensive data collection system.
·         Adopt a metric for reporting data on antibiotic sales or use that better allows trends to be identified, explained and compared.
Enhancing Surveillance and Data Integration to Inform Antibiotic Use Policy 
·         Integrate available data into a single, comprehensive report.
·         Improve surveillance to detect new and emerging resistance threats.
A.      Expand surveillance for emerging resistance using next generation sequencing technology.
B.      Expand surveillance for important emerging pathogens,
C.      Pilot test approaches that comprehensively detect resistance in all bacteria in a sample.
D.      Expand surveillance at the state level.
“Until we become better stewards of antibiotics, both in human medicine and in livestock production, these life-saving drugs will continue to become less effective, and the effectiveness of any antibiotics developed in the future will be at constant risk,” the authors write.
The report includes several appendices with more detailed outlines of policies and tools to help achieve the panel’s goals and recommendations, and extensive endnotes linking to related research and literature.