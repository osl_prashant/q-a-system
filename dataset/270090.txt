Unless yields soar to never-before-seen highs, the United States this summer will certainly harvest the smallest corn crop in three years since the production volume may not be padded by a boost in plantings.
When the government’s corn planting intentions came in at an unexpectedly low 88 million acres last month, some market participants figured the actual acreage could eventually be higher since it was still early and U.S. farmers “love to plant corn.”
But neither the futures market nor the weather – in particular – are supporting that idea. Winter never loosened its grasp across the core U.S. Corn Belt, and 2018 is vying for the title of coldest Midwestern April on record since 1895.
As of Sunday, only 3 percent of the corn crop had been planted compared with an average of 5 percent. While it is too early to consider this delayed, very little – if any – progress will be made in the top producing states this week. Soils remain largely too cold and the Upper Midwest is bracing for yet another winter storm on Wednesday.
If U.S. producers plant fewer than 88 million corn acres in 2018, it would be the smallest area since 2009, and this outcome is still in play. But yield remains a wide-open ballgame.
NO EXTRA ACRES?
When corn planting progress is notably below average by the middle of May, market watchers start to genuinely worry that plantings will fall below the U.S. Department of Agriculture’s March target, and with good reason.
In the last two decades, there were 10 instances in which May 15 planting progress was below the long-term average of 75 percent. Final corn plantings came in below USDA’s March intentions in eight of those 10 years. (tmsnrt.rs/2JUUhHV)

This means that if 2018 progress still lags by mid-May, the 88 million-acre scenario may be the most generous.
One of the two slower planting years with rising corn acres was last year, but both the increase in acres and the planting delays were relatively minor. The other was 2009, and final plantings came in 1.4 million acres higher than the March projection.
In 2009, corn planting was severely delayed in parts of the Eastern Corn Belt and Northern Plains due to persistently cold and wet conditions. But in the five weeks from the end of April to the start of June 2009, CBOT December corn futures steadily climbed more than 20 percent from the lows, peaking at $4.73-1/2 a bushel on June 2.
This likely encouraged some farmers in the affected areas to stay the course, while inspiring other farmers, especially in fringe states, to expand on their original corn plans. The Chicago futures market was also suggesting better corn profitability relative to soybeans in the spring of 2009, which is not the case in 2018. (tmsnrt.rs/2JVCuQT)

Futures have ultimately drifted lower since USDA’s planting report, which was initially friendly for price. December corn settled at $4.06 a bushel on Tuesday, down 1 percent since March 29, even though many planters will remain idle for at least the next several days across the Corn Belt.
THE ‘Y’ WORD
Anyone trying to talk about U.S. corn yield in mid-April might risk some ridicule from the market. Luckily, there is no need to throw around reckless numbers right now since yield penalties are not guaranteed from late planting.
But summer weather becomes more important. Most recently, the generally favorable conditions during the summer of 2014 pushed corn yield to a record high, despite the lag in planting.
Seasons in which mid-May planting is close to average have statistically featured the most consistent results, with yields performing better than expected. (tmsnrt.rs/2ETOGy5)

Interestingly, crops that were planted quicker than normal, such as 2010 or 2012, might actually have more downside risk. This is somewhat ironic since early planting is perceived to be advantageous because the crop can pollinate before the hottest stretch of the summer.
Planting is fast when temperatures are warm and soils are relatively dry. But that weather pattern – if observed during the spring – is highly susceptible to persisting during the U.S. summer. This is exactly what happened in 2010 and especially in 2012, both of which were poor harvests.
If U.S. farmers plant the full 88 million-acre intention, national yield would have to break 180 bushels per acre in order to tie with last year’s 14.6 billion-bushel crop. The current production record of 15.1 billion bushels was set in 2016.
(The opinions expressed here are those of the author, a market analyst for Reuters)