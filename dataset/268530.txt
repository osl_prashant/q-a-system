Plan would allow drug testing for some food stamp recipients
Plan would allow drug testing for some food stamp recipients

By JULIET LINDERMAN and JONATHAN LEMIREAssociated Press
The Associated Press

WASHINGTON




WASHINGTON (AP) — The Trump administration is considering a plan that would allow states to require certain food stamp recipients to undergo drug testing, handing a win to conservatives who've long sought ways to curb the safety net program.
The proposal under review would be narrowly targeted, applying mostly to people who are able-bodied, without dependents and applying for some specialized jobs, according to an administration official briefed on the plan. The official, who spoke on condition of anonymity to discuss internal deliberations, said roughly 5 percent of participants in the Supplemental Nutrition Assistance Program could be affected.
The drug testing proposal is another step in the Trump administration's push to allow states more flexibility in how they implement federal programs that serve the poor, unemployed or uninsured. It also wants to allow states to tighten work requirements for food stamp recipients and has found support among GOP governors who argue greater state control saves money and reduces dependency.
Internal emails obtained by The Associated Press indicated that Agriculture Department officials in February were awaiting word from the White House about the timing of a possible drug testing announcement.
"I think we just have to be ready because my guess is we may get an hour's notice instead of a day's notice," wrote Jessica Shahin, associate administrator of SNAP.
Conservative policymakers have pushed for years to tie food assistance programs to drug testing.
Wisconsin Gov. Scott Walker, a Republican, sued the USDA in 2015 for blocking the state from drug testing adults applying for food stamps.
A federal judge tossed the suit in 2016, but Walker renewed his request for permission later that year, after Donald Trump had won the presidency but before he took office.
"We turned that down," said former USDA Food and Nutrition Service Undersecretary Kevin Concannon, who served in the position under the Obama administration from 2009 until January of last year. "It's costly and cumbersome."
The proposal is not expected to be included in a GOP-written farm bill expected to be released as soon as early this week, a GOP aide said.
Federal law bars states from imposing their own conditions on food stamp eligibility.
Still, some states have tried to implement some form of drug testing for the food assistance program, so far with little success.
Judges have blocked similar efforts in other states. In Florida in 2014, a federal appeals court upheld a lower court's ruling that drug testing SNAP recipients is unconstitutional.
But at least 20 states have introduced legislation to screen safety net program participants in some capacity, according to the National Conference of State Legislatures.
In December, Walker began moving ahead with a workaround, drug testing participants in the state's Employment and Training Program who also received food stamps.
USDA under Trump has not taken a public position on drug testing. But Secretary Sonny Perdue has promised to provide states with "greater control over SNAP."
"As a former governor, I know first-hand how important it is for states to be given flexibility to achieve the desired goal of self-sufficiency for people," he said. "We want to provide the nutrition people need, but we also want to help them transition from government programs, back to work, and into lives of independence."
The emails obtained by the AP suggest that a plan could be forthcoming.
The plan would apply to able-bodied people who do not have dependents and are applying for certain jobs, such as operating heavy machinery, the official said.
In a February 15 email to USDA officials, Maggie Lyons, chief of staff to an acting official at the Food and Nutrition Service, said, "We need to have a conversation about timing given budget and when the (White House) wants us to release drug testing."
If the administration moves forward, it would not be the first time drug testing was used in a safety net program.
At least 15 states have passed laws allowing them to drug-test recipients of Temporary Assistance for Needy Families, also known as welfare.
The discussion of the future of SNAP and potential changes to the program are set against the backdrop of the 2018 farm bill, slated for release as soon as this week. The bulk of the bill's spending goes toward funding SNAP, which often proves the most contentious part of negotiations; late last month, House Agriculture Committee Ranking Member Collin Peterson, D-Minn., issued a statement on behalf of Democrats denouncing "extreme, partisan policies being advocated by the majority."
Ed Bolen, senior policy analyst at the Center for Budget and Policy Priorities think tank, said requiring drug testing for food benefits will have consequences for already vulnerable populations. What's more, he said, implementing drug testing for SNAP recipients is legally murky.
"Are people losing their food assistance if they don't take the test, and in that case, is that a condition of eligibility, which the states aren't allowed to impose?" he said. "And does drug testing fall into what's allowable under a state training and employment program, which typically lists things like job search or education or on-the-job experience? This is kind of a different bucket."
The emails also show that USDA is weighing the possibility of scaling back a policy currently enacted in 42 states that automatically grants food stamp eligibility to households that qualify for non-cash assistance, like job training and childcare. The proposed change, which would impose income limits, could potentially affect millions.
Republicans tried to make similar changes when Congress passed the 2014 farm bill, but the cuts were rejected by Democrats and did not end up in the final bill.
Concannon, the former USDA undersecretary, said the Trump administration "is keen on weakening the programs developed to strengthen the health or fairness or access to programs and imposing populist requirements that aren't evidence based, but often stigmatize people."
The USDA in recent months has been under fire for its controversial plan to replace a portion of millions of food stamp recipients' benefits with a pre-assembled package of shelf-stable goods dubbed "America's Harvest Box." The food box plan was tucked into the Trump administration's proposed 2019 budget, which included cutting the SNAP program by $213 billion over the next 10 years. SNAP provides food assistance to roughly 42 million Americans