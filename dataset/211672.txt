As summer gave way to fall, sales of Michigan-grown fruits and vegetables were heating up. 
“We continue to build our local products,” said Dave Distel, CEO of Grand Rapids, Mich.-based Heeren Bros. LLC
Heeren carries more than 100 locally grown items, mostly for sale in the summer, Distel said.
 
“It’s peak season, with squash, zucchini, corn, radishes and a variety of others,” Distel said. 
 
The season’s first apples came off the trees in late August, with peak season getting underway about two weeks later.
 
The Michigan deal focuses primarily on staple items, and plenty of them, said Dominic Riggio, president of Detroit-based Riggio Distribution Co.
 
“We can develop some critical mass and volume and make progress with a lot of local produce,” he said.
 
“The margins aren’t the same as specialty or produce that comes from farther shipping points, but local is a value to us and we partner with a lot of local growers here and do a good job of marketing their stuff.”
 
In 2017, Michigan dealt with weather issues that affected some crops, including cherries and apples, but there still was plenty of product, wholesalers said.
 
“I don’t think Michigan is any different from any other state: People love to have product from the state they’re in,” said Nate Stone, chief operating officer of Detroit-based wholesaler Ben B. Schwartz & Sons Inc.
 
Stone said the state has done a great job of marketing its fruits and vegetables through its Pure Michigan program.
 
Local product is always available in-season to customers who want or need it, said Michael Badalament, in sales with Detroit-based wholesaler R.A.M. Produce Distributors.
 
Not everybody wants local, though.
 
“We still have the foodservice people who are more comfortable with the California stuff, and we provide that opportunity,” he said.