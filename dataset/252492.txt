Chicago Mercantile Exchange live cattle slid to a seven-week bottom on Thursday, led by fund liquidation, traders said.
Futures' discount to this week's slaughter-ready, or cash, cattle prices freed contracts from session lows, they said.
April closed 1.200 cents per pound at 121.775 cents. June finished 1.550 cents lower at 113.275 cents.
CME live cattle struggled to escape trade war fears despite President Donald Trump dialing back talk of stiffer metal tariffs for key U.S trading allies.
January U.S. beef exports accounted for roughly 12 percent of total domestic production, and pork about 25 percent, according to industry experts.
"The backdrop of uncertainty in global trade, in the news since late last week, hasn’t helped," said Cassandra Fish, author of industry blog The Beef, referring to Thursday's cattle futures selloff.
"Some thought the hint yesterday by the White House that Mexico and Canada might be exempt from any action would help a cattle futures rally today; it did not," she said.
Instead, futures found an ally in late-session buyers after packers in Nebraska and Kansas paid $127 per cwt for cattle that fetched $126 on Wednesday.
In the face of slumping futures prices, most feedlots resisted selling cattle based on rising packer profits, said traders and analysts.
Thursday was the second of five days that funds in CME's livestock markets that follow the Standard & Poor's Goldman Sachs Commodity Index sold, or "rolled," April futures into the June contract.
Technical selling, higher corn prices and CME live cattle futures losses slammed the exchange's feeder cattle contracts.
March feeders ended 2.025 cents per pound lower at 141.750 cents. 
Hogs Finish Mostly Weaker
Most CME lean hog contracts closed weaker on spillover cattle market pressure and trade war concerns, said traders.
Mexico and Canada are the top- and fourth-ranked markets for U.S. pork by volume, said industry experts.
April hogs drew support from higher cash prices, but slumping wholesale pork values capped market advances.
April hogs closed up 0.275 cent per pound at 68.075 cents. May finished down 0.250 cent at 72.350 cents, and June ended 0.350 cent lower at 78.250 cents.
Packers raised bids for hogs to maintain market share and take advantage of their good profits, a trader said.
Wholesale pork values were largely dragged down by sharply lower pork belly prices to entice end-users to store product for spring and summer use, he said.