Ag Department kills animal welfare rule for organic meat
Ag Department kills animal welfare rule for organic meat

The Associated Press

DES MOINES, Iowa




DES MOINES, Iowa (AP) — The Trump administration is withdrawing a federal rule that would have required organic meat and egg producers to abide by stricter animal welfare standards.
The U.S. Department of Agriculture announced Monday it has withdrawn the Organic Livestock and Poultry Practices final rule published in January 2017 by former President Barack Obama's administration.
The regulation was to ensure that organically grown livestock had enough space to lie down, turn around, stand up, fully stretch and had access to fresh air and proper ventilation.
The USDA says the rule exceeds the department's statutory authority and could increase food costs.
The American Society for the Prevention of Cruelty to Animals says nearly two decades of collaboration between farmers and consumers have been reversed and millions of animals will continue to suffer each year.