The U.S. Department of Agriculture's National Institute of Food and Agriculture is awarding four grants totaling more than $13.6 million to combat citrus greening disease.
The funding is made possible through NIFA's Specialty Crop Research Initiative Citrus Disease Research and Extension Program, authorized by the 2014 Farm Bill.
The awards went to:
Clemson University, Clemson, South Carolina, $4.3 million;
Regents of the University of California, Riverside, $5.1 million;
Iowa State University, Ames, $2.5 million; and
USDA Agricultural Research Service, Athens, Georgia, $1.8 million.
Citrus greening, also known as huanglongbing or HLB, is currently the most devastating citrus disease worldwide. It was first detected in Florida in 2005 and has since affected all of Florida's citrus-producing areas, leading to a 75% decline in Florida's $9 billion citrus industry.
Since 2009, the USDA has invested more than $400 million to address citrus greening, including more than $57 million through the Citrus Disease Research and Extension Program since 2014, according to the release. Past projects have included research by the University of Florida to help HLB-affected orchards recover fruit production, and work at the University of California to develop strategies for creating citrus rootstocks that are immune to HLB.