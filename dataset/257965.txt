Blue Bell returns to New Mexico after listeria outbreak
Blue Bell returns to New Mexico after listeria outbreak

The Associated Press

ALBUQUERQUE, N.M.




ALBUQUERQUE, N.M. (AP) — A Texas company that produces ice cream is making its return to central New Mexico after a listeria outbreak in 2015.
KRQE-TV reports Blue Bell Creameries is scheduled to make deliveries Monday to stores in Albuquerque, Belen, Los Lunas and Grants.
The company announced in December its plans to reopen its distribution facility in Albuquerque.
The Brenham, Texas-based Blue Bell pulled its ice cream off the shelves in New Mexico four years ago after it was linked to a listeria outbreak.
___
Information from: KRQE-TV, http://www.krqe.com