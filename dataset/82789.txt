DAP led overall declines in our weekly fertilizer price survey followed by urea as potash led gains.
Fertilizer prices were narrowly mixed, pressuring our Nutrient Composite Index 0.71 points lower on the week to 536.60.

Nitrogen

Anhydrous ammonia was our only decliner this week in the nitrogen segment but remains at a slight premium to UAN28% which typically signals a near-term move higher.
Urea fell 85 cents by the short ton this week but is still priced well above anhydrous ammonia on an indexed basis.
UAN28% & 32% each posted very mild gains.

Phosphate

Phosphates were mixed this week as DAP and MAP unwind the spread between the two.
Field activity will likely pick up this week which may support mild, demand-based price increases in phosphates.

Potash 

Potash continued to inch higher but strong world supplies will limit the upside.
On an indexed basis, potash is priced well below the rest of the fertilizers in our survey which may lead to longer-term corrective price support.

Corn Futures 

December 2017 corn futures closed Friday, April 7 at $3.84 putting expected new-crop revenue (eNCR) at $608.14 per acre -- softer $6.78 per acre on the week.
With our Nutrient Composite Index (NCI) at 536.60 this week, the eNCR/NCI spread narrowed 6.07 points and now stands at -71.54. This means one acre of expected new-crop revenue (eNCR) is priced at a 71.54 premium to our Nutrient Composite Index.





Fertilizer


3/27/17


4/3/17


Week-over Change


Current Week

Fertilizer



Anhydrous


$519.94


$521.32


-26 cents


$521.06

Anhydrous



DAP


$458.89


$456.75


-$4.69


$452.07

DAP



MAP


$457.62


$459.03


+14 cents


$459.17

MAP



Potash


$332.77


$332.43


+$1.23


$333.66

Potash



UAN28


$248.90


$248.76


+3 cents


$248.79

UAN28



UAN32


$286.68


$280.81


+17 cents


$280.98

UAN32



Urea


$364.31


$363.04


-85 cents


$362.19

Urea



Composite


539.22


537.31


-0.71


536.60

Composite