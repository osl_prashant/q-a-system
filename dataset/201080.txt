Auxin herbicides are devastating to sensitive crops—know your risk
Herbicide residue has a way of hiding in hoses, some more so than others. Of the hoses tested (pictured below) at Mississippi State University, the VersiGard synthetic rubber hose showed the greatest carryover herbicide damage, while the PMA 4086-08, an ethylene-centered hose, had better cleanout.

“The other [non-ethylene-centered]hoses had microscopic breaks in the interior lining where the herbicide could sequester and rejoin the solution later,” says Dan Reynolds, Mississippi State professor of weed science. “Farmers are especially likely to see herbicides rejoin the solution when using herbicides with good solvent and surfactant loads that tend to bring residues from tanks into the solution.”

Yield loss when using rubber hoses was as high as 19%; the areas where the ethylene-centered hose were used lost only 8% (neither had any kind of cleanout). The yield loss from the carryover dicamba in the VersiGard hose was equivalent to spraying a 1/256 rate on sensitive soybeans. 

The ethylene-centered hose can be ordered from Kuri Tec or John Deere. And yes, they’re the most costly. 








 


Mississippi State University research found yield loss ranged from 8% to 19% depending on hose type. Tests were done using auxin herbicides and no cleanout.












Triple Rinse to Avoid Damage
Just 0.000488 lb. of dicamba per acre can lead to a 10% yield loss in sensitive soybeans—and a half-pound rate can drop yields 97%, according the University of Arkansas. If 2,4-D hits sensitive cotton 30 days after planting, 0.025 lb. of active ingredient per acre can cause up to 100% yield loss, according to Oklahoma State University.

Dicamba manufacturers provide these guidelines for cleanout:

Immediately drain the sprayer after use—don’t allow the chemical to sit overnight prior to flushing.
Flush tank, hoses, boom and nozzles with clean water. Open boom ends while flushing.
Inspect and clean strainers, screens and filters.
Mix a cleaning solution with detergent or sprayer cleaner.
Wash all parts of tank, agitate sprayer and recirculate cleaning solution for at least 15 minutes and remove visible deposits.
Flush hoses, spray lines and nozzles with cleaning solution for at least one minute.
Drain pump, filter and lines (Engenia label); repeat the above six steps two more times (XtendiMax and FeXapan labels).
Rinse complete spraying system with clean water (Engenia label); after completing above procedures remove nozzles, screens and strainers and clean separately in cleaning solution (XtendiMax and FeXapan labels).
Clean and rinse exterior of sprayer (Engenia label); dispose of rinsate as specified by laws (XtendiMax and FeXapan labels).
Dispose of all rinsate in compliance with requirements (Engenia labels); drain sump, filter and lines (XtendiMax and FeXapan labels).
Rinse the complete spraying system with clean water (XtendiMax and FeXapan labels).