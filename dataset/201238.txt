Crop calls 
Corn: 3 to 5 cents lower
Soybeans: 8 to 10 cents lower
Winter wheat: 4 to 8 cents lower
Spring wheat: 2 to 5 cents higher

Corn and soybean futures started the overnight session highs after yesterday's USDA data showed crop conditions declined more than expected, but then softened amid profit-taking. Also this morning, scattered showers are moving across dry areas of Nebraska and western Iowa while a more active system moves rains from southeastern Iowa into central Illinois. Meanwhile, crop conditions kept spring wheat futures in positive territory, while winter wheat futures faced profit-taking.
 
Livestock calls
Cattle: Mixed
Hogs: Mixed
Following yesterday's losses, cattle and hog futures are vulnerable to followthrough pressure, but could also see short-covering to result in a mixed start. Pressure on cattle futures should be limited by the $4 discount August live cattle hold to the average of last week's cash trade. This week's cattle showlist is down slightly from week-ago, which could improve packer demand. Meanwhile, demand for cash hogs is much improved this week, signaling packers are short bought following last week's holiday interrupted schedule. Traders will be watching the pork market for signs of a top, as carcass values declined 11 cents yesterday on light movement of 197.38 loads.