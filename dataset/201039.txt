Can lawmakers, citizens tamp down political zealots?






NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.






The congressional baseball game played Thursday is an apt focal point to what is right and what is needed not only in Washington but throughout the nation. 
A political zealot struck first Wednesday morning at the Republican baseball practice, aiming his poison at lawmakers, congressional aides and lobbyists. The impacts are still being felt with all hoping for a speedy recovery of House Majority Whip Steve Scalise (R-La.); Matt Mika, a Tyson Foods lobbyist who was coaching the Republican team and is critically wounded, and for three others wounded. But the ugly incident has, for now, lowered the vitriol attitude in Washington, with leaders from both political parties acting responsibly. 
Can things really change? Just like those quoting baseball statistics, the data clearly suggest this horrid incident will likely be short-lived. Or will it? The zealot hit directly at one of Congress’ most likable individuals, Rep. Scalise. 
As in baseball, a good team starts with good management. That means leaders. Not only in Congress, but in their respective party operations, and White House officials, with the president being a very important voice. 
Political party leaders must now lower the tone of their remarks. They and their colleagues should still oppose policy initiatives they do not support, but there are far better ways to note opposition than what we have seen for far too long. 
Singles and doubles lead to runs, so initial steps can go a long way towards making the political arena a more civil atmosphere. Fans of good government are waiting. This is not a boxing match. 
Sometimes relief players are needed to come into a heated atmosphere to tamp down what in the past has been ugly incidents of ill-tempered remarks from many both inside and outside Washington, and in the media. 
While no one can police the vitriol via “social” media, leaders and others can pounce when they should elevate the matter to a more temperate discourse. Kick them out of the ugly game they want to play. 
Three important words. President Trump during a video appearance at last night’s congressional ball game ended his remarks with three familiar baseball words: “Let’s play ball.” In most sports, there is a clear winner, eventually. But that doesn’t mean the loser has no good players or ideas. Victories and defeats come and go. As in sports, it is how one reacts to “wins” and “losses” in building a better team for the future. Think of the Olympics when we support American participants. Washington should be no different. 


 




NOTE: This column is copyrighted material; therefore reproduction or retransmission is prohibited under U.S. copyright laws.