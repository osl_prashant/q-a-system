The following content is commentary and opinions expressed are solely those of the author.
Last week, Commodity Classic was held in Anaheim, California, just outside Los Angeles County.

With a population of 10 million people, this county is more populous than 43 states.

This week on John’s World, he talks about how much population matters to farmers as they plan the future.

Watch John’s World every weekend on U.S. Farm Report.