An unfamiliar concept for many at the start of 2017, blockchain now has sufficient interest from the produce industry that the Produce Marketing Association has formed a task force to follow its development and will be working with retailer pilots of the technology.
Walmart brought blockchain to the forefront when it conducted a U.S. trial with sliced mangoes and in August partnered with IBM, Dole and Driscoll’s to further test the digital ledger technology, which companies believe could significantly improve food safety and operational efficiency.
Ed Treacy, vice president of supply chain efficiencies for PMA, will serve as an industry representative on Walmart and Wegmans pilots, to provide insight on how the technology could work for produce.
PMA wants to ensure that retailers of all sizes are aware of blockchain capabilities.
“We do know they’re working with Dole and Driscoll’s, but they are very sophisticated from a technology and systems and data perspective,” Treacy said. “Not everybody is a Dole and a Driscoll’s.”
Companies are paying attention; a PMA webinar about blockchain had high attendance and even higher playbacks, Treacy said.
“I think the retailers are being very respectful of the entire industry by collaborating to figure out how we’re going to use this and apply it with standards and procedures and what information — rather than Walmart doing it one way and Wegmans doing it another way and Kroger doing it a third way,” Treacy said.
Companies represented on PMA’s task force include Mission Produce, Tanimura & Antle, Robinson Fresh and Domex Superfresh Growers.
United Fresh Produce Association and the Canadian Produce Marketing Association will have representatives on the task force as observers.
 
Big potential
Frank Yiannas, vice president of food safety for Walmart, has been a proponent of blockchain, and faster traceability is a big reason.
In its earlier pilot, Walmart traced the source of mangoes in seconds, compared to nearly a full week using conventional methods, Yiannas said during the PMA webinar.
“What we’re doing we don’t think just benefits Walmart or the Walmart customer,” Yiannas said.
“For example, we’ve heard from farmers that they like the idea of a blockchain solution because when these food scares happen, they’re all guilty until proven innocent, and (now with blockchain) they can clear their good name, they can continue to sell their mangoes or their papayas or their spinach.”
Blockchain should also be useful for sharing food safety information like certifications and audits, which blockchain — and frameworks like Trellis, developed by PMA — allows to be processed as data instead of just as a PDF.
“That (information) can move up the supply chain and then people can actually take that and plug it into their data warehouses and do proper analysis on it and truly assess the risk that they will have of handling and selling of the product,” Treacy said.
Enhanced efficiency is another bonus for blockchain, by enhancing visibility throughout the supply chain.
Yiannas gave an example from the pilot with mangoes. During the test, it was noted that a shipment sat at the U.S.-Mexico border for several days.
“If they can address that, and figure out the root cause of that and fix it, we can take probably three days out of the supply chain,” Treacy said.
Blockchain locks in shipment details at each point in the supply path, and may help eliminate transaction disputes — the timing of when a load was picked up, for example.
 
Questions remain
The biggest hurdle for blockchain will be getting companies throughout the supply chain on board, said Markon CEO and Center for Produce Safety chairman Tim York.
“Blockchain is a tool, and a great tool, to enable trackability, traceability, but only if we’re capturing and sharing that data right down the line, and not a lot of people are equipped yet to do that,” York said.
“Speaking for ourselves, I know that our legacy system does not have that capability of capturing that (data), so we’d have to find a third-party tool or company to work with to help us capture and pass on that data.”
The cost of implementing such a system is a concern, and some companies may be thinking that the amount required will be more than the normal expense of a recall.
“If they were convinced that that technology was going to make that process a whole lot easier and a whole lot less disruptive and efficient and cost-effective, I think they would have implemented that already,” York said. “I think what we’re lacking is convincing ROI on the technology.”
Walmart hopes to make that equation more attractive by wielding its influence on the development of blockchain for widespread use.
“Whenever we talk to people that are trying to provide solutions, we stress that the cost has to be a no-brainer,” Yiannas said during the webinar.
“We even stress the idea that some portions of data entry should be available for free, at no cost.” Whatever that final number, Yiannas expects it to be worthwhile.
“We’re convinced that on balance, when you look at the business challenges that I mentioned earlier, the cost of foodborne disease, the cost of recalls, the cost of food waste, the cost of food fraud, that on balance this should save the food system money and not cost the food system money,” Yiannas said.
Data security and privacy are other questions that have yet to be answered.
With large retailers exploring the capabilities of blockchain, however, it seems the technology has meaningful momentum.
“That could be the tipping point we’re looking for,” York said.