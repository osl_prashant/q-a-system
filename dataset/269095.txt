BC-Cotton
BC-Cotton

The Associated Press

NEW YORK




NEW YORK (AP) — Cotton No. 2 Futures on the IntercontinentalExchange (ICE) Friday:
Open  High  Low  Settle   Chg.COTTON 2                                50,000 lbs.; cents per lb.                May      83.83  84.45  83.06  83.41   —.28Jul      83.04  83.64  82.90  83.35   +.35Sep                           78.91   +.08Oct      79.45  79.45  78.93  79.11   —.18Nov                           78.91   +.08Dec      78.79  78.97  78.63  78.91   +.08Jan                           79.03   +.12Mar      78.91  79.04  78.72  79.03   +.12May      78.90  79.02  78.73  79.01   +.15Jul      78.60  78.73  78.48  78.72   +.15Sep                           73.50   +.08Oct                           75.77   +.15Nov                           73.50   +.08Dec      73.50  73.55  73.49  73.50   +.08Jan                           73.56   +.08Mar                           73.56   +.08May                           73.98   +.08Jul                           74.06   +.08Sep                           72.40   +.08Oct                           73.63   +.08Nov                           72.40   +.08Dec                           72.40   +.08Jan                           72.44   +.08Mar                           72.44   +.08Est. sales 57,733.  Thu.'s sales 48,849 Thu.'s open int 277,292,  up 3,023