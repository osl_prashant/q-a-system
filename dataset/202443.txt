A recent editorial in The Packer says the "Dirty Dozen" list published by the Environmental Working Group "plays dirty with consumer emotions with misleading, inaccurate information." 
 
While many of us in agriculture agree, the list also overlooks an important point: eating organic produce isn't always attainable or desired by consumers.
 
This is especially true in foodservice. 
 
What's more, the list comes in spite of fruit and vegetable consumption rates remaining low, meaning many Americans are missing out on the important health benefits produce provides. 
 
This point is at the crux of efforts by The Alliance for Food and Farming, an important tool the produce industry uses to push back against EWG. (Full disclosure, I am vice chairman of AFF.) 
 
The AFF strives to provide peer-reviewed studies, government reports and expert analyses to consumers so that facts, not fears, can guide their produce choices.
  
Among the alliance's concerns about inflammatory messaging from groups like the EWG is that fear about produce safety is becoming a new barrier to consumption of these healthy foods. 
 
Unfortunately, two peer-reviewed studies from Johns Hopkins Center for a Livable Future and the Illinois Institute of Technology's Center for Nutrition Research seem to verify the validity of those concerns. 
 
The AFF has impressive backing in agriculture, including Taylor Farms, U.S. Apple Association, California Fresh Fruit Association, Western Growers Association, Produce Marketing Association, United Fresh Produce Association, Florida Fruit and Vegetable Association and more - a list that EWG's Bill Walker called the "who's who of chemical agribusiness" in a March 13 blog. 
 
In addition to the important points made by AFF, Markon and its members have found little demand for organic produce in foodservice because of a variety of factors. 
 
Those restaurants that do seek organic options tend to be "chalkboard" kind of restaurants, e.g. those that can easily change menus. 
 
One challenge includes the fact that it is difficult to create dishes entirely from organic options. The easiest dish to assemble is a spring mix salad, which can easily be made with organic olive oil, organic vinegar and salt and pepper. 
 
And while the organic spring mix salad is simple to make, it still receives little interest from operators. 
 
In fact, only 7% of Markon spring mix sales are organic.
 
Demand for organics in foodservice is also taking longer to catch on than in retail because of the increased cost, availability and challenges with consistency, according to Mark Emery, Gordon Food Service's product development chef. 
 
"A consumer shopping at retail sees 'organic' on a product's label, and if that's important to them, they don't mind paying extra," Emery said. 
 
"Foodservice operators are already facing declining margins that make it tough to pay a premium for organic ingredients - unless they can pass that cost along to the consumer."
 
Many times, consumer demand just isn't there, unless it is in niche operators or organic-forward restaurants. 
 
David Werner, vice president of marketing for Ben E. Keith Foods, Fort Worth, Texas, agrees. 
 
"We have not seen organics grow in popularity like in retail," Werner said. 
 
"While in some accounts, offering organics has been a plus, many operators cannot justify the higher cost oftentimes associated with organics. Unless they can pass that cost along or justify the demand, many times they stick to conventional products."
 
Moving forward, growers and shippers will continue to see rising demand for organics in retail. On the foodservice front, the jury is still out. 
 
For those restaurant operators interested in organics, salads are a great place to start. Meanwhile, growers and shippers should continue to advocate for the value of increased produce consumption overall. 
 
Whether organic or conventional, the public stands to benefit by eating more fruits and vegetables - something that should be promoted at retail and foodservice operations alike. 
 
Tim York is CEO of Salinas, Calif.-based Markon Cooperative. Centerplate is a monthly column on "what's now and next" for foodservice and the implications for produce. E-mail timy@markon.com.
 
What's your take? Leave a comment and tell us your opinion.