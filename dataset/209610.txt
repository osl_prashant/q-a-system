The entry period is closed. Thank you for your entries. Do you have an innovative new product that is useful to dairy producers? We are looking for the best of the best in new products that will be game-changers for dairy producers in the areas of efficiency, functionality, and technology. Submit your entry to the 7th annual Dairy Herd Management Innovation Award contest!
The deadline for nominating products for the Dairy Herd Management Innovation Awards contest is Friday, July 7, 2017, midnight CST.
Submit an entry and earn a chance to be featured as one of the top innovators at the 2017 World Dairy Expo.
Eligible products must be new to the U.S. dairy marketplace since World Dairy Expo 2016. A company can submit more than one product.
Entries will be evaluated by Dairy Herd Management's panel of dairy farmers, agribusiness representatives, and university experts. Entries will be judged on their originality within the marketplace, usefulness and value to dairy farmers.
Nominated products will appear on www.dairyherd.com in August and September for our audience to vote on the People’s Choice Winner. The Top 10 products will be announced at World Dairy Expo 2017 and recognized within Dairy Herd Management's print, online and electronic products. 
Nominate your company’s new product or email all information to asayre@farmjournal.com. An email reply confirming receipt of your nomination will be sent within 48 hours, if you do not receive this acknowledgment, please notify Amanda Sayre.
Entries must include:
Company name.
Address, City, State and Zip code.
Contact person (including email and phone number).
Company or product Website.
World Dairy Expo exhibit location.
Name of new product.
Description of new product (no more than 250 words).
The reason for nominating. (If there are similar products on the market, please list them.)
A high-resolution photo of product or logo. (High-resolution color images in JPEG format are requested for all nominated products.)
Research supporting product claims is accepted and welcomed by the judging committee but is not required for consideration.