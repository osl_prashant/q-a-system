BC-CO--Colorado News Digest, CO
BC-CO--Colorado News Digest, CO

The Associated Press



Colorado at 5:15 p.m.
Thomas Peipert is on the desk and can be reached at 800-332-6917 or 303-825-0123. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
TOP STORIES:
COLORADO TRANSPORTATION
DENVER — A broad coalition of Colorado business groups has endorsed a $5 billion transportation measure moving through the state legislature, boosting its chances of passage even as top Democrats continued to express "reservations" with the plan. But while political hurdles remain, Monday's announcement eliminated a key roadblock that stymied political negotiations at the divided legislature for much of the year. By Brian Eason. SENT: 540 words.
MARIJUANA SHOP-UTAH STATE LINE
SALT LAKE CITY — Utah officials are wary after the first marijuana shop opened in the tiny town of Dinosaur, Colorado, just a few miles from the state line. Utah Highway Patrol Lt. Todd Royce reminded residents of Utah, where recreational marijuana remains illegal, that they will still be charged and prosecuted if they bring the substance into Utah or consume it in the state, the Deseret News reported. SENT: 300 words.
COAL MINING COMPANY
BILLINGS, Mont. — A Colorado-based coal company with mines in Montana and Wyoming is considering restructuring options, including filing for bankruptcy protection, after seeing demand drop and dealing with more than $1 billion in debt. Westmoreland Coal Co. said in its annual report to shareholders and the Securities and Exchange Commission that it may seek bankruptcy protection from its creditors, or that an involuntary petition for bankruptcy could be filed against the company. SENT: 740 words.
IN BRIEF:
— MARBLE-ETHICS VIOLATION — A state commission has ruled that a Colorado state senator violated ethics rules in 2017 when she moderated an oil and gas forum funded by the industry.
— COLORADO AVALANCHE DEATH — A member of a Colorado mountain search and rescue team died in a weekend avalanche outside the boundaries of the Aspen Highlands ski resort.
— FINANCIAL MARKETS-BOARD OF TRADE — Wheat for May gained 18.50 cents at 4.9075 a bushel; May corn was up 2.25 cents at 3.9075 a bushel; May oats rose 4 cents at $2.3725 a bushel; while May soybeans advanced 13.25 cents at $10.47 a bushel.
SPORTS:
HKN-PLAYOFFS-WEST PREVIEW
The Predators have the Presidents' Trophy, and the NHL-expansion Golden Knights carry the buzz entering the Western Conference playoffs, which have the potential of resembling nothing from the past. So step aside Chicago, which missed the playoffs for the first time in nine years, and say bye to the Blues, whose six-year playoff run is over. The changing of the West's guard begins at the top, where Nashville clinched its first Central Division title by running away with the league's best record. And then there's Vegas, which broke every measureable NHL expansion record for success by going 51-24-7 to claim the Pacific Division. By John Wawrow. SENT: 1,030 words, photos.
SURPRISING AVALANCHE
DENVER — Nathan MacKinnon was given a day off from practice, which created a little stir on social media. Turns out, nothing to fear. The Colorado Avalanche just wanted to give their top scorer and Hart Trophy candidate a little rest Monday before embarking on the postseason. By Pat Graham. SENT: 720 words, photos.
PADRES-ROCKIES
DENVER — Rockies righty Jon Gray faces the San Diego Padres on Monday night for a second straight start after tossing seven shutout innings against them five days ago. UPCOMING: 650 words, photos. (Game starts at 6:40 p.m. MT)
HKO-WORLDS-US
Patrick Kane has a history of making things a big deal. When Blackhawks teammate Adam Burish and his agent Bill Zito wanted to put in a charity game during the previous NHL lockout in 2012-13, Kane jumped on board and helped generate buzz for the event in the Chicago suburbs. Zito hasn't forgotten that and on Monday got to name Kane as the United States captain for the upcoming IIHF World Hockey Championship next month in Denmark. By Stephen Whyno. SENT: 580 words, photos.
IN BRIEF:
— CALIFORNIA-AD — California has hired Air Force athletic director Jim Knowlton to run its athletic department. Chancellor Carol Christ announced Monday that Knowlton will replace the departing Mike Williams and start his stint as AD at Cal on May 21.
___
If you have stories of regional or statewide interest, please email them to apdenver@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867. For access to AP Exchange and other technical issues, contact AP Customer Support at apcustomersupport@ap.org or 877-836-9477.
MARKETPLACE: Calling your attention to the Marketplace in AP Exchange, where you can find member-contributed content from Colorado and other states. The Marketplace is accessible on the left navigational pane of the AP Exchange home page, near the bottom. For both national and state, you can click "All" or search for content by topics such as education, politics and business.