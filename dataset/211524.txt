Dried fruit represents a healthy snack choice for consumers and healthy business growth for one of the leading companies in this field.
Patterson, Calif.-based Traina Foods Inc. has experienced explosive growth this decade thanks to its signature offerings of various dried fruit products, said Willie Traina, CEO.
“Sales are increasing — over the last seven years about 30% growth year over year in the dried fruit category,” he said.
“This is very large growth and we are seeing it across the board as well with organic, natural and conventional dried fruit.”
The company attributes this growth to the health craze, as more and more people learn the nutritional value of dried fruit.
“It’s a great snack, and it gives you the ability to store it and keep it when you hike and bike,” Traina said.
Traina Foods stays busy coming up with the next new dried fruit flavors. In the past three years, the company has launched a California Sun Dried Blueberry, California Sun Dried Strawberry and October Sun Plum (using a yellow plum).
The company also offers a variety of organic fruits.