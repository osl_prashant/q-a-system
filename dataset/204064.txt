While harvest is underway, positioning for next year is already under consideration.Informa Economicshas predicted soybeans will take a significant number of acres away from corn and wheat during the 2017 planting season.
Corn acres are projected to drop by 3.5 million acres to 90.9 million acres.
Wheat acres are projected to drop to 48.9 million acres, it's lowest mark since 1970.
Soybean acres are expected to climb nearly 6 percent to a new record of 88.4 million acres.
One Iowa farmer put all his acres into corn this season said he has a different plan for next year.
"Marketwise, when we made the decision we were staring at $8.75 soybeans and obviously we've had improvement since then," said Eric Hough. "We've already had talks on our operation that there will probably be some soybeans next year."

Hear from Hough Wednesday on AgDay to hear how the 2016 growing season was in western Iowa as theI-80 Harvest Tourcontinues.