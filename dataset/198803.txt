According to the USDA's latest Crop Progress report, 75% of the country's corn is in good or better condition - unchanged from last week's report.Looking at individual states, conditions are scattered across the Corn Belt.
The best reports in the country, without a doubt, are mostly in the northern Plains. These states are mostly reporting higher percentages of corn in good or better condition, including Minnesota (81%), North Dakota (81%) and Wisconsin (86%).
Looking at the percentages of corn in poor or very poor condition, the worst corn in the country is in Missouri (9%). See Figure 1 for a look at how Missouri compares to other Corn Belt states:

<!--
var embedDeltas={"100":717,"200":520,"300":460,"400":417,"500":400,"600":383,"700":357,"800":357,"900":357,"1000":357},chart=document.getElementById("datawrapper-chart-9oU1z"),chartWidth=chart.offsetWidth,applyDelta=embedDeltas[Math.min(1000, Math.max(100*(Math.floor(chartWidth/100)), 100))]||0,newHeight=applyDelta;chart.style.height=newHeight+"px";
//-->

Meanwhile the USDA also reported silking progress for the first time this season. Nationally 6% of corn has silked as of June 26. This is slightly ahead of last year (3%) but on par with the five year average of 5%.
SoybeansConditions declined slightly from last week's report, dropping by 1 percentage point to 72% of corn in good or better condition. Look at individual states, many of the same states to report the best or worst corn conditions in the country are also reporting similar conditions with soybeans:
Arkansas: The exception to the rule, 12% of soybeans are in poor or worse condition.
Michigan: 10% of soybeans are in poor or very poor condition.
Wisconsin: On the flip side, 1% of soybeans are in poor or very poor condition; 84% of soybeans are good or better condition.
Nine percent of soybeans are blooming this week, putting progress on par with both last year (7%) and the five-year average (7%).
Click here to read the full Crop Progress report.