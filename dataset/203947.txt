Black Gold Farms has begun harvesting red potatoes in Indiana.
Grand Forks, N.D.-based Black Gold's Indiana production is in Winamac, which is located about halfway between Chicago and Indianapolis, according to a news release.
Black Gold expects to harvest several hundred acres of norlands and dark red norlands through August.
Indiana's turn in Black Gold's year-round supply of reds comes after Texas, North Carolina, Arkansas and Missouri and before the Red River Valley.
"We see the Indiana crop as the perfect fit to ensure that we have a year round supply of Black Gold Farms grown quality red potatoes for our customers," Nathan Libey, Black Gold's Indiana farm manager, said in the release. "The crop looks great, and we're very excited to see this product on store shelves."