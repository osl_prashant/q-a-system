New rules designed to end scallop tussle off New England
New rules designed to end scallop tussle off New England

The Associated Press

PORTLAND, Maine




PORTLAND, Maine (AP) — Federal fishing managers say they've set new measures to try to resolve a dispute between members of the East Coast scallop fishery.
Small-boat scallop fishermen in the northern Gulf of Maine area have complained in recent years that bigger boats are dominating the fertile fishing grounds.
The two types of boats operate under different sets of rules.
The smaller boats have a possession limit of 200 pounds, while the big boats have no such limit. They're regulated instead by a limited number of days at sea.
The National Oceanic and Atmospheric Administration said Friday that it plans to prevent excessive fishing by prohibiting the boats from fishing in the northern Gulf of Maine when they are on days-at-sea program. They will still have some access to the area though.