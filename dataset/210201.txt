Trump and Iran | Farm bill timeline | FSA acreage data | Cotton AWP | Markets 



— Iran nuclear deal update coming from Trump today. President Donald Trump is expected to stop short of abandoning the multinational accord to curb Iran’s nuclear program later today, opting instead to formally disavow the deal. The Trump administration has concluded that Iran’s actions “severely undercut” regional and international security that the agreement was designed to achieve, the White House said. The White House said that since the Iran deal was inked, Iranian officials have shown a "disturbing pattern of behavior" and tried to "exploit loopholes and test the international community’s resolve." The White House criticized Iran for refusing to allow international inspectors into its facilities. "The deal must be strictly enforced, and the IAEA must fully utilize its inspection authorities," the White House said, signaling it does not view the P5+1 pact as dead.
President Donald Trump today will announce not only his decision on the nuclear deal, but also a new Iran strategy. It will be focused on "neutralizing the government of Iran's destabilizing influence and constraining its aggression, particularly its support for terrorism and militants," according to a fact sheet.
Under U.S. law, Trump faces a Sunday deadline to notify Congress whether Iran is complying with the accord that was negotiated over 18 months by the Obama administration and determine if it remains a national security priority. 
Reports note that Trump will not call for a re-imposition of nuclear sanctions on Tehran. He will urge lawmakers to codify tough new requirements for Tehran to continue to benefit from the sanctions relief that it won in exchange for curbing its atomic program. And he'll announce his long-anticipated intent to impose sanctions on Iran's Revolutionary Guard Corps by designating it a terrorist organization under an existing executive order. 
On to Congress. If the president refuses to certify that the deal serves U.S. interests, then the topic moves to Congress. But as many other things with this Congress, the issue will likely prove too contentious for any legislative solution. 
— House farm bill timeline session delayed. We will have to wait until next week to see a specific new farm bill timeline because a session expected Thursday was postponed because of work on the disaster aid bill and House lawmakers rush to exit Thursday. 
— Incremental acreage changes in Oct. FSA data. U.S. producers reported planting 87.301 million acres of corn, 88.712 million acres of soybeans, 43.189 million acres of wheat and 12.155 million acres of upland cotton for harvest in 2017, according to data released by the Farm Service Agency (FSA).
Producers have to file planted acreage data to FSA as part of their participation in US farm programs, including levels of acreage they were prevented from planting. The updated release of FSA data shows a total of 2.587 million acres were prevented from being planted in 2017, up slightly from 2.568 million acres in August but still well below the 2016 total of 3.412 million acres.




Crop


Prevent Plant
(Million Acres)


Total Planted*
(Million Acres)




.


Oct.
			2017


Sep.
			2017


Oct. 2017


Sep.
			2017




Corn


0.964


0.955


87.301


87.258




Upland Cotton 


0.115


0.114


12.155


12.151




Soybeans 


0.437


0.433


88.712


88.666




Wheat


0.620


0.616


43.189


43.170




* = includes failed acres.


 


 


 


 




There are still 10 states reporting 100,000 acres or more of prevented planting acres this year, with most of the states either in the south or in Plains HRW wheat country
Comments. The data was enough to prompt the National Ag Statistics Service (NASS) to adjust planted and harvested acres in the Crop Production report released Thursday — soybean harvested acreage was revised up 740,000 and corn harvested acres were trimmed 377,000 acres. NASS has also adjusted acreage on other crops over the past month, in part using the FSA data. NASS adjusted cotton and rice acres in their September Crop Production report and wheat and small grains in the Small Grains Summary released Sept. 29. The October report is typically when NASS will utilize the FSA data as one of the factors they use to make any acreage adjustments. NASS follows the data on a weekly basis but FSA only publicly releases the information once a month. The next update will come Nov. 12 but should show little change from the figures released this month.
— Cotton AWP edges up; import quota #25 for upland cotton announced. The cotton Adjusted World Price moved up to 60.44 cents per pound, effective today (Oct. 13), marking the fourth week in a row the AWP has been between 60 and 61 cents per pound. USDA will also establish special import quota #25 for 58,805 bales of upland cotton on October 19 and will apply to upland cotton purchased no later than January 16 and entered into the U.S. no later than April 16. 
— Other items of note: 

Bayer agreed to sell significant parts of its crop science business to BASF for €5.9 billion ($6.98 billion), which it said will partially finance its planned acquisition of Monsanto. Bayer said the assets to be sold include its global glufosinate-ammonium business and related LibertyLink technology for herbicide tolerance, nearly all its field-crop seeds businesses, as well as respective R&D capabilities. The businesses included in the agreement generated ~€1.3B in net sales for 2016. The deal is contingent upon the completion of Bayer's acquisition of Monsanto, but the takeover still faces scrutiny from European regulators.
Mexican and U.S. agricultural groups are holding a roundtable session to air their concerns that the proposals for seasonal produce will sink NAFTA. Speakers include Bosco de la Vega, head of a major Mexican agricultural organization, Tom Sleight, president of US Grains; and Tom Stenzel, president of United Fresh Produce. Ambassador James K. Glassman will moderate.
More expensive eggs result of housing restrictions. A study called “The Impact of Farm Animal Housing Restrictions on Egg Prices, Consumer Welfare, and Production in California,” recently released in the American Journal of Agricultural Economics, shows that California has improved conditions for egg-laying chickens, at a cost: it raised egg prices for consumers by 22% between 2014 and 2016. Link for details. 
AEI’s “U.S. Agricultural Policy in Disarray” confab takes place in Washington, D.C. this afternoon. Speakers include Joseph Glauber, senior research fellow at the International Food Policy Research Institute (former top USDA economist); Barry Goodwin, professor at North Carolina State University; Daniel Sumner, professor at the University of California, Davis; Scott Faber, vice president of the Environmental Working Group; and Vincent Smith, director of agricultural studies at the American Enterprise Institute (AEI).
"Xi Jinping has more clout than Donald Trump," the Economist writes in its cover editorial. "The world should be wary Do not expect Mr Xi to change China, or the world, for the better.” Link. Other key quotes: 

*  "The United States is still the world's most powerful country, but its leader is weaker at home and less effective abroad than any of his recent predecessors, not least because he scorns the values and alliances that underpin American influence."
*  "The president of the world's largest authoritarian state, by contrast, walks with swagger abroad. His grip on China is tighter than any leader's since Mao. And whereas Mao's China was chaotic and miserably poor, Mr Xi's is a dominant engine of global growth."
* "The world does not want an isolationist U.S. or a dictatorship in China. Alas, it may get both."
— Markets. Brent crude oil futures rose by nearly 2% to about $57.25 per barrel. In the U.S., investors await quarterly results from some big banks. 
The European Central Bank is considering reducing its pace of asset purchases to €30 billion ($36 billion) per month from January next year, while extending the program to September, according to Bloomberg, who sourced several officials familiar with the debate. This would reduce the monthly pace by a half, and continue the program well beyond its current December 2017 end date. Euro-area bonds are gaining this morning following the report, with Germany’s 10-year yield falling to the lowest level in more than two weeks.
China released figures showing imports last month jumped by a larger than expected 19% in dollar terms from a year ago.