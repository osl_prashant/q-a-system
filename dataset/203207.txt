The import tide is strong and it is hard to say when it will turn.
 
The latest from the U.S. Department of Agriculture on trade numbers for fruits and vegetables show that U.S. imports are performing much better than U.S. exports in the past year.
 
In the latest U.S. Agricultural Trade update, the government said fresh/frozen fruit imports from January though October 2016 were $10.378 billion, up 10% from the same period the previous year.  Imports of fresh/frozen vegetables from January through October last year were pegged at $8.272 billion, up a whopping 16% from the same period the previous year.
 
In contrast, U.S. exports were just holding their own, according to the USDA. U.S. fresh fruit exports from January though October were $3.842 billion, down 1% from the same period the previous year. Fresh vegetable exports for first ten months of last year totaled $2.036 billion, up 3% from the same period in 2015.
 
Exports to Canada haven't showed much growth in the past year. From January through November in 2016, U.S. fresh vegetable exports to Canada were off 3% in value but up 5% in volume compared with the previous year. Fresh fruit exports to Canada from January through November were off 1% in value but up 2% in volume.
 
Mexico has been much worse. U.S. exports of fresh fruit to Mexico from Jan-Nov 2016 were off 10% in value and down 21% in volume.  U.S. exports of fresh vegetables to Mexico were down 18% in value and off 9% in volume.
 
As recent Packer coverage illuminates, the exchange rate has played a part. The U.S. dollar has strengthened against the Mexican peso from 14.83 pesos/dollar in January 2015 to 21.47 pesos/dollar in mid-January. That would change the cost of a $20 carton of U.S. apples from 296 pesos in early 2015 to 429 pesos in mid-January this year. 
 
For Canada, the U.S. dollar has strengthened from $1.17C/USD in early 2015 to $1.31C/USD early this year. For Canadian importers, that makes a $20 carton of U.S. produce increase from C$23.40 in early 2015 to C$26.20 in mid-January this year.
 
Of course, the higher-trending U.S. dollar favors imports, making foreign goods less expensive.
 
The USDA said imports of fresh vegetables from Canada from January through November last year were up 9% in value and 13% in volume compared with the previous year.
 
Mexico's fresh vegetable shipments to the U.S. for the first 11 months of 2016 rose 19% in value and 13% in volume compared with the same months the previous year. Fresh/frozen fruit shipments to the U.S. from Mexico grew 12% in value and 4% in volume compared with the same period in 2015.
 
Could the dollar weaken against the peso in coming months? Some believe it could, using the strategy of buy the rumor, sell the fact. 
 
While Donald Trump's determination to bring jobs back to the U.S. from Mexico has weakened the peso, some believe his bark may be worse than his bite. Perhaps reality won't be as harsh as predicted. In any case, the high U.S. dollar is creating challenges for expanding fresh fruit and vegetable exports to our NAFTA partners.