Mexico’s fairly light flora loca avocado crop is winding down in August, while the larger aventajada crop will be ramping up in September.The 1.7 billion pounds of avocados that Mexico exported to the U.S. during the 2016-17 crop year was down about 10% from 2015-16, but exports to the U.S. during the coming year could be up by 20% compared to 2016-17, according to some estimates.
The official estimate had not been released as of early August.
Calavo Growers Inc., Santa Paula, Calif., expected to start its aventajada crop out of Mexico in mid-September, said Rob Wedin, vice president of sales and marketing.
 
Tight supply
The summer crop from Mexico was “not a super-light” crop,” he said, but it was “nothing too large” either.
“With supplies of California and Peruvian fruit declining in August and into early September, it’s going to be demand-exceeds-supply until we get to that new crop,” he said.
“Then we’ll see some balance.”
Lack of rainfall in late spring and summer stifled sizing on the early flora loca crop, said Bob Lucy, partner in Del Rey Avocado Co. Inc., Fallbrook, Calif.
Fruit was in the 60s and 70s range, but some had sized up to 48s in late July, he said. He expected sizing to continue to improve.
“They’ll get pretty cranked up in volume probably in the next few weeks,” he said in late July.
Giumarra Agricom International, Escondido, Calif., has fruit from Mexico every day of the year, said Gary Caloroso, marketing director.
Quality was fine on the flora loca crop, he said, and he expected volume to increase compared to last year for the aventajada crop, which runs from late August until early December.
“October through December should be a great time to promote,” he said.
Dana Thomas, president of Index Fresh, Riverside, Calif., agreed.
“From a retailer’s standpoint, there should be a lot of opportunity for promotions and programs,” Thomas said.
Index Fresh also ships avocados from Mexico year-round.
U.S. avocado imports out of Mexico come exclusively from the state of Michoacan, said Emiliano Escobedo, executive director of the Hass Avocado Board, Mission Viejo, Calif.
 
Hello, Jalisco?
Plans have been in the works to allow the import of avocados grown in the state of Jalisco, as well, but no agreement has been signed by the U.S. Department of Agriculture and the Mexican government, he said.
“Negotiations at the federal level have been taking place for quite a while,” he said.
The final rule allowing export certification has been published by USDA, he said, but the deal has not been finalized.
Calavo has a new packinghouse in Jalisco, Wedin said.
“We are getting some use out of it, but we’re not able to ship that fruit to the U.S.,” he said.
He attributed the delay in certifying Jalisco for exports to the U.S. on trade policy issues.
“We keep thinking it’s going to happen,” he said.