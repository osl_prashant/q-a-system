Chicago Mercantile Exchange lean hog future's selloff last Friday carried over into Monday, led by fund liquidation and weaker cash prices, said traders.April hogs ended 1.625 cents per pound lower at 65.650 cents, and below the 100-day moving average of 66.744 cents. May finished 1.900 cents lower at 70.325 cents, and below the 200-day moving average of 71.529 cents.
Packers paid less for hogs that are plentiful, said traders and analysts. Investor jitters seeped into the market in advance of the U.S. Department of Agriculture's quarterly hog report on Thursday, they said.
Monday morning's average price for slaughter-ready, or cash, hogs in Iowa/Minnesota was $65.90 per cwt in light volume, down 77 cents from Friday, the USDA said.
Weaker cash prices and firmer wholesale pork values, amid spring grilling and Easter ham business, enhanced packer profits.
Monday's average pork packer margins were a positive $21.65 per head, up from a positive $17.85 on Friday, as calculated by HedgersEdge.com.
 
U.S. government data on Monday morning showed the average wholesale pork price climbed $1.40 per cwt from Friday to $79.88, with higher prices for all categories listed except hams.