Perdue to food box critics: Give the idea a chance
Perdue to food box critics: Give the idea a chance

The Associated Press

WASHINGTON




WASHINGTON (AP) — Agriculture Secretary Sonny Perdue is defending a proposal that would cut food stamp benefits in half and replace them with a box of shelf-stable goods delivered to recipients' doorstep. The plan has drawn harsh criticism, with one lawmaker calling it "a cruel joke."
The idea was first floated last week in the Trump administration's 2019 budget proposal, tucked inside of a larger plan to slash the Supplemental Nutrition Assistance Program, or SNAP, by roughly $213 billion — or 30 percent— over the next 10 years.
Perdue on Thursday said the plan is a work in progress and asked critics not to dismiss it.
Perdue said, "we're looking at innovative ideas, we're looking to do more with less, we think it's a reasonable way to do that."