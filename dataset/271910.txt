BC-Cash Prices, 1st Ld-Writethru,0342
BC-Cash Prices, 1st Ld-Writethru,0342

The Associated Press

NEW YORK




NEW YORK (AP) — Wholesale cash prices Tuesday
Tue.        Mon.
F
Foods

Broilers national comp wtd av           1.0875      1.0875
Eggs large white NY Doz.                  1.36        1.36
Flour hard winter KC cwt                 15.20       15.40
Cheddar Cheese Chi. 40 block per lb.    2.2175      2.2175
Coffee parana ex-dock NY per lb.        1.1749      1.1606
Coffee medlin ex-dock NY per lb.        1.3801      1.3653
Cocoa beans Ivory Coast $ metric ton      2834        2834
Cocoa butter African styl $ met ton       7558        7558
Hogs Iowa/Minn barrows & gilts wtd av    58.28       57.80
Feeder cattle 500-550 lb Okl av cwt     172.00      173.50
Pork loins 13-19 lb FOB Omaha av cwt     91.09       86.00
Grains
Corn No. 2 yellow Chi processor bid     3.65¼       3.60½
Soybeans No. 1 yellow                  10.07¼      10.05¾
Soybean Meal Cen Ill 48pct protein-ton 376.80      379.10
Wheat No. 2  Chi soft                   4.73½       4.61½
Wheat N. 1 dk  14pc-pro Mpls.           7.52¾       7.48
Oats No. 2 heavy or Better              2.49¼       2.51¾
Fats & Oils
Corn oil crude wet/dry mill Chi. lb.    .29           .29
Soybean oil crude Decatur lb.           .29½          .29¾
Metals
Aluminum per lb LME                    1.1121        1.1171
Antimony in warehouse per ton            8275          8475
Copper Cathode full plate              3.1401        3.1473
Gold Handy & Harman                    1328.85      1324.30
Silver Handy & Harman                   16.728       16.651
Lead per metric ton LME                2348.00      2366.00
Molybdenum per metric ton LME           26,000       26,000
Platinum per troy oz. Handy & Harman    919.00       920.00
Platinum Merc spot per troy oz.         928.90       916.30
Zinc (HG) delivered per lb.             1.4582        1.4711
Textiles, Fibers and Miscellaneous
Cotton 1-1-16 in. strict low middling    77.94       80.51
Raw Products
Coal Central Appalachia $ per short ton   61.45       61.45
Natural Gas  Henry Hub, $ per mmbtu       2.74        2.72
b-bid a-asked
n-Nominal
r-revised
n.q.-not quoted<29
n.a.-not available