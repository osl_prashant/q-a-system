U.S. soybeans edged higher on Tuesday to trade near an eight-month top as investors worried about harvest rain in major grower Argentina and crop markets drew support from a rebound in crude oil.Corn ticked higher to hold close to Monday's five-month high as dry conditions in Brazil also raised production concerns. Wheat inched lower in a pause following a surge in the previous session when investment funds liquidated some of their large short positions.
Weather concerns in South America and shifts in fund flows have allowed grain markets to gain ground in the face of ample global inventories that have weighed on prices so far this season.
"The issues with weather in the Argentine harvest are helping push soybeans higher," Hamish Smith, a commodities analyst with Capital Economics, said. "The rebound in oil in the last day or so has also given some support to agricultural markets."
Chicago Board Of Trade most-active soybean futures rose 0.3 percent to $9.59-1/4 a bushel, near Monday's eight-month high of $9.64-1/4 a bushel.
Wheat edged down a quarter of a cent to $4.72-1/2 a bushel, having closed up 4.6 percent on Monday.
Corn added 0.5 percent to $3.83 a bushel, after hitting its highest in more than five months on Monday at $3.84.
Heavy rain in northeast Argentina has raised the risk of delays in harvesting and some production losses.
Weather forecasts called for more significant rain in some northern zones in the day ahead.
In corn, dryness has been hampering crop development in Brazil, and the dry spell could lead it to resume import deals to cover short-term needs.
A rise in oil prices, as investors looked beyond a failure by producing countries to agree on an output freeze, gave impetus to commodity and equity markets while also pushing the dollar lower.
Wheat futures consolidated after Monday's rally.
In a weekly report released after the close of trading on Monday, the U.S. Department of Agriculture (USDA) rated the U.S. winter wheat crop 57 percent good to excellent, matching expectations.
The report underlined favorable recent weather for U.S. winter wheat as rainfall has eased a developing drought in the southern Plains.
Monday's run-up in prices followed U.S. Commodity Futures Trading Commission data released after Friday's close that showed speculative investors, including hedge funds, holding a net short in CBOT wheat futures of 152,453 contracts, the biggest net short on the books since 2006.