AP-NH--New Hampshire News Digest 6 pm, NH
AP-NH--New Hampshire News Digest 6 pm, NH

The Associated Press



New Hampshire news from The Associated Press for Saturday, April 7, 2018.
Here's a look at how AP's general news coverage is shaping up today in New Hampshire. Questions about today's coverage plans are welcome, and should be directed to the northern New England desk at 207-772-4157.
A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking news and more newsworthy events may take precedence. Advisories, digests and digest advisories will keep you up to date.
Some TV and radio stations will receive shorter APNewsNows.
TOP STORIES:
DISAPPEARING CLAMS
PORTLAND, Maine — Maine's clam harvest reached its lowest point since at least 1930 last year. The New England staple is struggling with warming waters and growing numbers of predators, and the harvest is down coastwide. By Patrick Whittle, 500 words, photo.
IN BRIEF:
FATAL CRASH-INTERSTATE: State Police are trying to identify a motorist who drove into the rear of a tractor-trailer in a breakdown lane on Interstate 93. Troopers said Saturday that the white Chevy Tahoe had no license plates, and investigators were still trying to identify the driver.
SOLAR FARM ZONING: A proposal for New Hampshire's biggest solar farm doesn't pass muster with zoning laws in Concord. The Zoning Board of Appeals this week rejected the 54-acre solar farm because it had too many "impervious surfaces" that would cause rain to run off instead of soak into the ground.
WRONG-WAY DRIVER: New Hampshire state troopers say a wrong-way motorist nearly collided with three law enforcement vehicles on Interstate 93 before being arrested. Troopers say they stopped northbound traffic early Saturday in the Concord area as the motorist failed to heed efforts to get him to stop.
FRANKLIN PIERCE-ENVIRONMENTAL RESEARCH: Franklin Pierce University is hoping to turn more than 1,000 acres of undeveloped land on its rural campus into a living laboratory. The private university in Rindge has been awarded a nearly $650,000 grant from the National Science Foundation to help academically talented low-income students who are pursuing degrees in biology and environmental science.
FIRE SURVIVOR: A New Hampshire woman who saved herself by jumping from a burning, third-floor apartment said she didn't have time to contemplate. She said the fire was moving fast and that it was "do or die." Jill St. Cyr tells the Portsmouth Herald that she climbed through the window of her third-floor apartment onto a deck and jumped at least eight feet onto the roof of an attached laundromat.
___
If you have stories of regional or statewide interest, please email them to apconcord@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, 888-273-6867