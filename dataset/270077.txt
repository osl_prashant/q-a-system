BC-Cash Grain
BC-Cash Grain

The Associated Press

SPRINGFIELD, Ill.




SPRINGFIELD, Ill. (AP) — Truck and rail bids for grain delivered to Chicago. Quotations from the USDA represent bids from terminal elevators, processors, mills and merchandisers after 1:30 p.m. Central time.
Wed.Tue.No. 2 Soft wheat4.75¼4.66¼No. 1 Yellow soybeans10.26¾10.21No. 2 Yellow Corn3.65e3.62½eNo. 2 Yellow Corn3.83p3.80½p
e-terminal elevator bids.
p-processor bid
n.q.-not quoted