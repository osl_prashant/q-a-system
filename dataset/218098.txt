Most consumers don’t even know how much packaging technology improves their fruit and vegetable quality. 
There’s a saying that each fruit or vegetable begins to die once it’s picked or harvested, and that’s even more the case with fresh-cut. 
Good packaging delays this long enough for consumers to enjoy the fruit or vegetable.
I think we’re on the edge of the next big leap in packaging.
It’s one thing to deliver a high-quality product to consumers, but it would be even better know more about consumers through packaging. 
That will begin to happen when more produce companies operate in the fourth dimension of packaging. 
You know, 4D — as in digital.
Brian Wagner, owner of packaging consultancy PTIS, talked about how 4D improves on 2D (graphics and overall look) and 3D (structure and form), at the United Fresh Produce Association’s BrandStorm in November, and he expanded on it in a recent interview.
He said he hasn’t seen much 4D in produce packaging but sees some good opportunities.
“Premium apples have a big opportunity to escape the commodity game, the lower margin side,” he said, and apple marketers could use technology to add value to retailers and consumers on packaging and even on Price Look-Up stickers.
“Sensors are getting smaller, cheaper and faster, so it’s much more real than before,” Wagner said. 
He said this technology will allow grower-shippers to better monitor produce in the supply chain.
“We’ll be able to find and solve problems we don’t even know exist,” he said.
The other side is gathering all this information to be able to present to consumers, who increasingly want to know more about from where their food comes. 
“We’re getting to the fun stuff,” Wagner said, “connecting with consumers and retailers.” 
There’s not much of this going on now.
Last summer, Deloitte conducted a study for USDA’s Agriculture Marketing Service on consumer challenges with access to bioengineered food disclosure. 
It found that 53% of consumers surveyed said they care about bioengineered food, but nearly all displayed technological challenges that kept them from accessing the information by electronic or digital methods.
What that means is people weren’t using their quick-response scanners, and we know a higher percentage wants to know about their food in general aside from the GM angle.
The good technology news is that 77% owned a smartphone, and 97% of national and regional chain stores offer WiFi to shoppers in store.
If 4D technology could deliver consumers product information easier, they could take it, and most of them want it.
Produce companies have great fruits and vegetables and great stories to tell. This could be a cost-effective way to tell it to the masses.
Greg Johnson is The Packer’s editor. E-mail him at gjohnson@farmjournal.com.
Related content:
Consumers value value
Packaging: Special Report 2017
Packaging: Special Report: Purchase drivers
Packaging Special Report: What's hot?
Packaging Report: Single serving packs are all growth