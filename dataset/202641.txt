Straight-talking Chris Schlect is retiring soon, and we will miss his presence.
 
Schlect, after 36 years leading the Northwest Horticultural Council, is stepping down at the end of March. Mark Powers, current executive vice president of the council, will succeed Schlect as president of the organization.
 
Schlect started with the Yakima-based council in 1980.
 
I visited with him recently about his pending retirement and a few recollections from his long tenure.
 
When he joined the Northwest Horticultural Council, he replaced Ernest Falk. Falk in 1947 had become the first president of the Northwest Horticultural Council, so together the tenures of Falk and Schlect tally an impressive 70 years of leadership for the group.
 
Growing up on a local orchard, Schlect had roots in apple country. He was approached by Falk about joining the council in 1980 while Schlect was a young deputy prosecuting attorney in Yakima County. 
 
The travel and policy work for the industry were appealing, Schlect said. He applied for and won the job.
"When I first started, Europe and Scandinavia were the markets with greatest interest, but now North America and Asia are the areas of vital importance."- Chris Schlect
In his early days at the council, transportation was the No. 1 issue for the tree fruit industry, he said.
 
Federal regulations on railroad, trucking and ocean liner freight rates required a lot of attention during that era, he said.
 
"With deregulation, which came when I was entering the office, a lot of that work went away, thankfully," he said.
 
Immigration legislation took center stage with the Immigration Reform and Control Act of 1986. "That was our first major effort, with the whole West Coast ag and labor people, trying to get our views inserted into the federal law," he said. 
 
The Alar scare of 1989 was an example of the sometimes difficult challenges related to agricultural chemicals and public perceptions, he said.
 
Expanding trade was always a priority issue for the Northwest Horticultural Council, but the market opportunities shifted over the decades.
 
"When I first started, Europe and Scandinavia were the markets with greatest interest, but now North America and Asia are the areas of vital importance," he said.
 
The North American Free Trade Agreement of 1994 ushered in a new era of trade with Mexico and Canada.
 
Schlect said the Trump administration's intent to renegotiate the treaty means that Powers and the rest of the council's staff will be busy voicing the importance of exports to U.S. policymakers in coming months and years.
"In my job, I have the ability to speak for thousands of growers and warehouses, for an industry that is basically trying to make a living doing something that is very useful - growing apples, pears and cherries, packing them and getting them to market. Somebody needs to speak for those people, and in my mind, I might as well speak with clarity and directness." - Chris Schlect
 
Schlect's outspoken support of agriculture and the tree fruit industry is easy to find in The Packer's library. In 2010, he penned a column about the effects of undue regulation.
 
"A flood of mandates from politicians and bureaucrats, flowing down from the faraway peaks of power to the lowly below, they who must obey and live with them. Petty, but corrosive, incursions into the economic freedom that supports a vibrant society," he wrote. 
 
"When taken alone and in the abstract, they sometimes make policy sense. Trouble arises in their rigid application, unplanned compliance costs and cumulative deadening effect on business expansion."
 
In that 2010 piece - still relevant today - Schlect went on to detail how regulations from the Environmental Protection Agency, the Food and Drug and Administration, the Internal Revenue Service, Immigration and Customs Enforcement and even the U.S. Department of Agriculture can get in the way of growers. 
 
I asked Schlect recently about his approach.
 
"In my job, I have the ability to speak for thousands of growers and warehouses, for an industry that is basically trying to make a living doing something that is very useful - growing apples, pears and cherries, packing them and getting them to market," he said. 
 
"Somebody needs to speak for those people, and in my mind, I might as well speak with clarity and directness," he said. 
 
Schlect said regulators mean well but often don't think of growers and the burdens that new laws create for them.
 
"If there is a policy of the government that is misguided, it doesn't help to not state that publicly," he said.
 
Going forward, growers are concerned about farm labor issues, notably related to immigration enforcement in agriculture, Schlect said.
 
Escalating labor rates and the challenge of picking ever-larger tree fruit crops are top concerns, he said.
 
"As the price points increase because of regulations, the cost of labor and other things, it is not easy to grow and pack a carton of fruit that the consumer wants to buy at a price the consumer can afford to pay."
 
While the challenges of the Trump presidency make it tempting to stay active in the industry, Schlect said Powers and Northwest Horticultural Council vice president Kate Woods are well positioned for the years ahead. 
 
Schlect said he will remain in the Yakima area, where he has family ties, and enjoy his retirement.
 
"I will be reading and playing golf and if there is something that comes along where I can be useful I'll have my mind open to new opportunities - but mainly retirement," he said.
 
Tom Karst is The Packer's national editor. E-mail him at tkarst@farmjournal.com.
 
What's your take? Leave a comment and tell us your opinion.