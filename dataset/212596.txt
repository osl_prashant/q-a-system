Arlington Heights, Ill.-based Weber Packaging Solutions has introduced two new labeling materials that meet tough application challenges. 
The Flexlyte Arctic 300 is a white film labeling material specifically made for cold temperature applications, according to a news release. The material is a quick-tack, all temperature film that employs an aggressive adhesive that can be applied successfully to packages in wet, damp and freezer conditions, according to the release.
 
The Transprint 425 FL is a new version of Weber’s successful Transprint label materials, according to the release, and has a thin clear polyester release liner as opposed to the more common standard paper liner. The thinner, recyclable liner allows more labels per roll, according to the release, which allows more labels per roll. 
 
The labels are engineered for companies using Weber Model 5300 print-apply systems, according to the release.