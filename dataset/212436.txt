Good growing conditions for onions in California have produced strong yields of large, high quality onions, grower-shippers say.
“The crop looks very good, and yields are going to be above average for us. In Bakersfield, we are anticipating a very good summer production-wise,” said Steve Gill, managing partner at Oxnard, Calif.-based Gills Onions, which primarily supplies foodservice. Gill declined to disclose exact numbers for the harvest.
Gill said the company’s Imperial Valley harvest wrapped up on May 20, earlier than usual this year “before any hot weather could hit.”
Harvest will begin in Bakersfield June 5 and finish mid-September before moving to the Salinas Valley. Although hot weather is on the horizon for Bakersfield, Gill said the timing was good as “the crop is still growing and can use that heat to size up and mature.”
Vince Gomez, commodity manager at Salinas, Calif.-based Tanimura & Antle had a similar report on the firm’s proprietary Artisan Sweet Italian red onions.
“Quality has been excellent and sizing has leaned towards jumbo or greater this season,” he said.
Mike Smythe, salesman for Five Points, Calif.-based Telesis Onion Co., which grows reds, whites, yellows and “a handful of sweet yellows,” said the firm’s yield was up slightly this year.
“Weather’s been great — we love the rain. Everything is good and should come off in a timely manner,” Smythe said May 19.
 

Prices

Prices have dropped significantly for grower-shippers this year because of an oversupply of storage onions from last year’s strong harvest.
On May 23, the U.S. Department of Agriculture reported prices of $6-7 for a 50-pound sack of jumbo yellow onions from California’s Imperial Valley. Year-ago prices were $14-16.
The USDA showed a similar drop in the prices of jumbo red globe onions, from $12-14 for a 25-pound sack at this time in 2016, compared to $5-6 this season.
“It’s been a really bad winter for pricing because of all the storage onions that were in storage this last winter,” Gill said.
However, some growers have remained sanguine despite the oversupply.
Gomez said demand for Tanimura & Antle’s Artisan Sweet Italians is at “an all-time high, despite the remaining Northwest round red storage crop spilling into the fresh harvest season.”
“We expect an increase to the fresh onion market as storage supplies continue to drop,” he continued.
Telesis’ Smythe also said that demand has been very good.
 

Sizing down?

Yerington, Nev.-based Peri & Sons Farms, which grows all colors of conventional onions as well as organic reds and yellows at their operations in El Centro and Firebaugh, Calif., just wrapped up the harvest in El Centro on May 19.
Cindy Elrod, sales and new business specialist at Peri & Sons, said the company had a smaller size profile than they usually see in El Centro for their yellows and whites, with reds about average.
Peri & Sons was set to finish packing in El Centro around May 29, Elrod said.
At Firebaugh, where the company started clipping short day varieties May 20, “the report on sizing is all over … some fields have good size and some average to below-average size,” Elrod said.
The first onions out of Firebaugh are whites and yellows, with reds expected to be ready for harvest around June 5, she said.