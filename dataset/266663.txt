AP-WV--West Virginia News Digest, WV
AP-WV--West Virginia News Digest, WV

The Associated Press



Here's a look at how AP's general news coverage is shaping up today in West Virginia. Questions about today's coverage plans are welcome, and should be directed to the Charleston bureau at (304) 346-0897 or chwpr@ap.org. A reminder this information is not for publication or broadcast, and these coverage plans are subject to change. Expected stories may not develop, or late-breaking and more newsworthy events may take precedence. If circumstances change before 6 p.m., a new digest will be sent reflecting those developments.
Some TV and radio stations will receive shorter APNewsNow versions of the stories below, along with all updates.
From AP Members:
Church Abuse-Lawsuit
MARTINSBURG, W.Va. — A civil trial that started in January in West Virginia has ended in an undisclosed settlement in a lawsuit accusing The Church of Jesus Christ of Latter-Day Saints and local church officials of covering up years of sexual abuse by one man.
Exchange-Mystery Book
PARKERSBURG, W.Va. — Nicole Smith has written a mystery set in Parkersburg, featuring local sites and landmarks. The cover photo features a picture looking down the Quincy Hill Steps.
Exchange-Sugar-Camp
CASS, W.Va. — Making maple syrup has been a longstanding family tradition for a Pocahontas County man — one he's proud to continue at a sugar camp near Cass.
In Brief:
—Winter Storm-West Virginia, from Charleston: Parts of West Virginia are under a winter storm watch as forecasters say heavy, wet snow could hit eastern sections of the state.
___
If you have stories of regional or statewide interest, please email them to chwpr@ap.org. If you have photos of regional or statewide interest, please send them to the AP state photo center in New York, (212) 621-1900 for news and (212) 621-1918 for sports. For technical issues, contact AP Customer Support at apcustomersupport@ap.org or (877) 836-9477.