The process of shifting from a doer of farm tasks to a leader of people can be disorienting because it requires a fundamental change of perspective. Think of it like the difference between standing in a grounded hot-air balloon versus floating in the basket overhead.
Farm leaders have visibility into the long time horizon of their organizations. That represents not just a job change but a career change, explains Bob Grace, a leadership and organizational development consultant with The Leadership Effect. The St. Louis-based consulting firm advises clients in agribusiness and other industries.
Each day is about taking steps toward that vision by helping team members see the big picture.
“That different set of leadership skills is often missing from the organization,” Grace says.
Leaders still bring their technical knowledge to work, but their focus must change, adds Sallie Sherman, founder and CEO of Columbus, Ohio-based S4 Consulting. The firm helps businesses in industries such as energy and financial services solve complex management problems.
“You’re focusing on guiding and setting direction and nurturing others because when you’re a leader, you get your work done through other people,” Sherman explains.
Lay The Foundation
As a farm executive, you need to create a culture that nurtures leaders, starting by defining what good leadership looks like based on your company’s stage of development.
“Some people love the growth spurt, some people love the efficiency part, some people love the turnaround,” Sherman says. “The model of leadership that you need very much depends on what that business is trying to do.”
A formal system involving attraction, selection and attrition can help develop leaders, Grace adds.
Hold a series of learning sessions for interested team members that highlight existing leaders on your team. Have them share how they made the transition to leadership.
Career Criteria
Establish checkpoints for employees moving into leadership. Monitor their progress and reinforce learning, Grace says. Review work samples and provide them with temporary roles in which they can practice.
To select qualified team members to begin the leadership transition, career interviews with existing employees can be a good tool.
“Look for a desire to have a broader impact than on the immediate project,” Grace recommends. “Look for high-achievement drive and consideration of others.”
The leadership selection process should be rigorous. Consider multiple qualifications including technical depth, leadership skills and the person’s potential to move higher within the company. Panel interviews, third-party assessments and on-the-job simulations can be useful.
Once a leader is selected, use an onboarding process and implement a leadership buddy system.
How Leaders Should Behave
Every farm should set its own definition of good leadership. But several themes are consistent across organizations, experts Bob Grace and Sallie Sherman say. Leaders should:
• Communicate frequently the direction the farm is going.
• Model good behavior.
• Embody the culture they want to see in the organization.
• Know their team members and understand how they prefer to collaborate and communicate.
• Be self-aware and know when they are hindering progress.
Editor’s Note: Nate Birt is managing editor of Top Producer, a Farm Journal Media sister publication. To view Bob Grace’s presentation from the 2017 Tomorrow’s Top Producer conference, visit TomorrowsTopProducer.com.