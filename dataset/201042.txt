Wheat futures rallied this week, led by spring wheat contracts amid crop concerns in the Northern Plains. That also pulled winter wheat futures higher despite increasing harvest activity. The contra-seasonal price gains caused us to advise additional new-crop sales on June 16. The strength in the wheat market failed to inspire buying in corn and soybeans. Instead, those markets faced light price pressure as needed rains fell on areas of the Corn Belt. With dry pockets remaining, attention will stay on weather.
Pro Farmer Editor Brian Grete highlights this week's Pro Farmer newsletter below:



Cattle futures posted sharp weekly losses in reaction to $3 to $5 lower cash cattle prices in the Plains even though they continue to trade at steep discounts to the cash market. Lean hog futures were also lower, with the July and August contracts slipping slightly below the cash index. That signals traders anticipate a seasonal top in the cash market soon, though there has been no indication of such yet.
Click here for this week's newsletter.