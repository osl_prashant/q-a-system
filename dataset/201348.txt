Fertilizer prices were mixed on the week with UAN28% higher and all others lower.
Our Nutrient Composite Index softened 1.46 points on the week, now 50.62 points below the same week last year at 515.25.

Nitrogen

Anhydrous ammonia led overall declines this week and on an indexed basis, UAN32% and urea are at parity, which may signal UAN would rather follow urea than anhydrous ammonia, which is at a sharp discount to the rest of our nitrogen segment.
UAN28% was urged higher by Minnesota which added $9.04 by the short ton this week.
On an indexed basis, UAN28% remains our most expensive form of nitrogen this week although 28% has rarely been an upside leader. We generally expect UAN28% to spike below the rest of the nitrogen segment before a segment-wide move to the upside.

Phosphate

Phosphates were lower with MAP declining roughly twice as much as DAP.
This week's price action is more of the "see-saw" type action that indicates the market is looking for direction.
The DAP/MAP spread is at the narrow end of our expectations at 13.98.
Pressure on DAP and MAP stems from lower wholesale values although import values at NOLA firmed slightly on the week, which also suggests the market is looking for direction.

Potash 

Potash prices fell back this week on sideways wholesale price action.
On an indexed basis, potash holds a roughly 10-point premium to NH3.
Despite supply-side rebalancing efforts on the part of North American potash producers, overabundance around the world thanks to aggressive Russian and Belorussian production is keeping a lid on potash prices.

Corn Futures 

December 2018 corn futures closed Friday, July 21 at $4.19 putting expected new-crop revenue (eNCR) at $667.44 per acre -- higher $10.17 per acre on the week.
With our Nutrient Composite Index (NCI) at 513.79 this week, the eNCR/NCI spread widened 11.62 points and now stands at -153.64. This means one acre of expected new-crop revenue is priced at a $153.64 premium to our Nutrient Composite Index.
December 2018 futures did, however, gap sharply lower in overnight trade, slightly narrowing its premium to our NCI.





Fertilizer


7/10/17


7/17/17


Week-over Change


Current Week

Fertilizer



Anhydrous


$488.80


$487.69


-$2.76


$484.91

Anhydrous



DAP


$445.05


$446.84


-52 cents


$446.32

DAP



MAP


$460.72


$461.59


-$1.28


$460.30

MAP



Potash


$333.87


$334.49


-$1.24


$333.24

Potash



UAN28


$238.10


$239.09


+74 cents


$239.82

UAN28



UAN32


$258.28


$259.55


-$1.81


$257.74

UAN32



Urea


$333.57


$329.39


-96 cents


$328.44

Urea



Composite


515.06


515.25


-1.46


513.79

Composite