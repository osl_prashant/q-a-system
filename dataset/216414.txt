At the Farm Journal AgTech Expo, four AgLaunch startups participated in a Startup station.  
The Startup Station was on the interactive expo floor, and after the companies gave their pitches, they were able to receive feedback from farmers.
The four teams were DryMAX, Earth Sense, Rabbit Tractors, and Rantizo. 
“The key component of AgLaunch365 is connecting early-stage agtech companies with farmers to help inform product development and become engaged in the scale up of the company,” says Pete Nelson, President & Executive Director of AgLaunch. “Our partnership with Farm Journal Media is helping our teams get connected with leading farmers, while providing content on the innovation pipeline to its readers and listeners.” 

The expo was the culminating event for the agtech teams completing Phase I of AgLaunch365 and exploring participation in Phase II, where teams will continue customer discovery and build out field trials for the spring.
As part of the unique partnership with Farm Journal Media, AgLaunch and seven of the agtech teams that it supports are participating in the inaugural Farm Journal AgTech Expo. 
All of the AgLaunch365 that participated at AgTech Expo were:

DryMAX, based in Minnesota, is commercializing a low-energy, low-heat radio wave process for drying grain that keeps grain integrity and maximizes nutrition delivery. (www.drymaxsolutions.com).
EarthSense, based in Illinois, has developed TerraSentia, an ultra-compact, autonomous, easy-to-use robot with multiple sensors and embedded data collection and analytics software for plant phenotyping. They are working with the seed industry and crop R&D partners to test and optimize TerraSentia. (www.earthsense.co). 
Rabbit Tractors, based in Indiana, is developing an autonomous, mobile, multi-purposed, swarm-capable and high-ground clearance farm-production tractor for all pre-harvest activities. (www.rabbittractors.com).


HarvestYield, based in New York, is a fleet management application for custom farmers. (www.harvestyield.com).
Rantizo, based in Iowa, delivers targeted liquid sprays precisely where they are needed in agricultural applications through an electrosprayer mounted on a drone, delivering minimal amounts of active agent directly to the targeted area. (www.rantizo.com).
Global Ag Smarte Technologies, based in Tennessee, has developed the SmarteRoot technology which increases absorption of water and nutrients to increase plant health and produce greater yield. (www.globalagsmarte.com).
Stable’N, based in Illinois, is a nitrification inhibitor process that utilizes electricity delivered into the soil by a retrofit to existing fertilizer application equipment. (www.stablen.com).

Farm Journal AgTech Expo provides insights and information from key experts so farmers can harness the power of AgTech.