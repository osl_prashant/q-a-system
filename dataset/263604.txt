Chicago Mercantile Exchange live cattle on Thursday reversed Wednesday’s gains, weakened by fund liquidation and concerns about rising supplies, said traders.
More cattle tend to come to market as warmer spring temperatures allow livestock to gain weight faster. And, the most recent U.S. government monthly cattle report implies increased cattle numbers ahead.
April live cattle closed 1.150 cents per pound lower at 121.850 cents, and June ended 0.950 cent lower at 112.300.
CME live cattle futures remained undervalued compared to cash prices, which attracts periodic short-term buying in the market, said Oak Investment Group President Joe Ocrant.
But, ranchers are placing significantly more cattle in feedlots, partly because of the drought in the southern U.S. Plains, he said.
As of Thursday, packers in the Plains paid $126 to $128 per cwt for market-ready, or cash, cattle that a week earlier brought mostly $126.
Bouts of wintry weather in the Plains slowed cattle weight gains, which made them less available to packers, a trader said regarding this week’s steady-to-better cash prices.
Processors will bid up for cattle as long as their margins and beef demand hold up, he added.
The U.S. Department of Agriculture’s weekly export data showed U.S. beef sales for the week ending March 8 at 20,200 tonnes, up 3 percent from the prior week.
Lower CME live cattle futures pressured the exchange’s feeder cattle contracts.
March feeders ended 1.425 cents per pound lower at 140.625 cents.
Hogs Again End Mostly Higher
CME lean hogs closed mostly higher for second straight day on fund buying and investors that bought deferred months and sold April futures in a trading strategy known as bear spreading, said traders.
The spreads and uneasiness about cash and wholesale pork prices as supplies grow seasonally sent April futures to their lowest level since late August.
April hogs closed 1.150 cents per pound lower at 65.725 cents. May finished up 0.700 cent higher at 72.750 cents and June ended 0.825 cent higher at 79.175 cents.
While some packers paid up for hogs, others resisted with the prospect of increased supplies as temperatures moderate seasonally, analysts and traders said.
Grocers are stocking up on pork for spring grilling and hams for the Easter holiday, they said.
USDA’s export report on Thursday put U.S. pork sales in the latest week at 31,600 tonnes, up 59 percent from the week before.