Salinas, Calif.-based The Nunes Co. and Yerington, Nev.-based Peri & Sons are building a distribution facility in Nevada that is expected to open in May.
 
The companies, both large family-run organic fresh produce operators, said in a news release that the Walker River Cooling facility in Nevada will service retailers, processors, wholesalers and other buying operations.
 
"The Peri and Nunes families' 40-plus years of cooling experience and industry knowledge has gone into developing a best-of-class facility in terms of cooling mechanisms, temperature control, product flow and industrial safety and food safety," said Tom Nunes V, of the Nunes Co., said in the release.
 
Construction of the facility broke ground began in September last year.
 
The combined operations of the two companies have brought over 800 jobs to Lyon County to date, with additional job growth projected upon completion of the new distribution facility, according to the release.