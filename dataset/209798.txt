Cows aren’t the only ones who can enjoy a total mixed ration (TMR), at World Dairy Expo there are plenty of dairy delicacies to dine on throughout the week.
The GEA Ice Cream Booth offers plenty of flavors for people to enjoy in the Expo Hall area. Last year local FFA chapters were donated $13,500 through the program where members work the stand each day.
Here is a look at the menu from the Ice Cream Booth:
Tuesday

Strawberry
Cookies N Cream
Jumping Jersey Cow

Wednesday

Elephant Tracks
Chocolate Chip Cookie Dough
Black Cherry

Thursday

Pirates Bounty
Blueberry Waffle Cone
Butter Pecan

Friday

French Silk
Ship Wreck
Birthday Cake

Saturday

Caramel Collision
Mackinac Island Fudge
Strawberry Cheesecake

For the third year in a row, the Badger Dairy Club Cheese Stand will continue the “Featured Cheese of the Day” thanks to a sponsorship from the Wisconsin Milk Marketing Board.
Each cheese came from a local dairy processor who donated cheese and added a third option to the menu along with the traditional American and Swiss grilled cheese sandwiches.
Here is a list of the flavors being served, where it came from and what day to expect them:
Tuesday

Tomato Basil Cheddar
	
Provided by Renard’s



Wednesday

Pesto Havarti
	
Provided by Babcock Hall



Thursday

Buffalo Wing Monterey Jack
	
Provided by Nasonville



Friday

Smoked Gouda
	
Provided by Arena Cheese



Saturday

Brick and Colby
	
Provided by Widmer’s



 
For more on what is happening at World Dairy Expo listen to the following AgriTalk interview from MILK editor Mike Opperman: