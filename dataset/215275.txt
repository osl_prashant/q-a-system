New Zealand dairy firm Fonterra has been ordered to pay French company Danone SA $125 million (105 million euros) over a contamination scare, a smaller than expected sum, though it prompted Fonterra to cut its earnings forecast.
An arbitration tribunal made the ruling on Friday in a dispute between Fonterra, the world’s biggest dairy producer, and Danone over a scare involving a Fonterra ingredient used by the French food manufacturer in 2013, which led to a recall based on erroneous information.
Danone Nutricia, a unit of Paris-based Danone, recalled some types of infant formulas sold under the Karicare brand in 2013 after its milk powder supplier Fonterra said there was a risk of bacteria in its products that could cause fatal botulism. The scare was later declared a false alarm.
Fonterra temporarily halted trading in its shares on Friday ahead of the decision and later said it had cut its 2017/18 earnings per share forecast to 35 to 45 New Zealand cents from 45 to 55 cents.
“Fonterra is in a strong financial position and is able to meet the recall costs,” Fonterra CEO Theo Spierings said in a statement.
The ruling would not impact its farmgate milk price forecast, Fonterra added.
The company’s shares climbed 0.63 percent after the trading halt was lifted.
Analysts said investors had expected the payout could be much higher.
“We know that as part of the arbitration Danone was looking initially for 630 million euros,” said James Bascand, Wellington-based analyst at Forsyth Barr. “The market, since it’s opened up again, has looked at (the payout) not unfavourably.”
A Fonterra spokeswoman told Reuters the sum sought by Danone was confidential. Danone could not be immediately reached for comment.
Fonterra said in the statement the decision to invoke a precautionary recall had been based on technical information from a third party that later turned out to be incorrect.
It said it was reviewing the tribunal’s findings but that there were likely to be limited options for challenging the decision.
“Danone welcomes this arbitration decision as a guarantee that the lessons from the crisis will not be forgotten,” the French company said in a statement.
($1 = 1.4648 New Zealand dollars)