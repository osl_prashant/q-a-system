We often wonder how drug companies determine those creative, often tongue-twisting names for their products. For the manufacturer, a product’s name is a marketing tool. They want something distinctive, memorable and if possible, descriptive of the product’s features and benefits.
From the regulator’s standpoint, clarity trumps marketing in the name game.
Toward that goal, the FDA this week issued a draft guidance to help drug sponsors select names for new products. Draft Guidance for Industry #240, "Proprietary Names for New Animal Drugs," aims to reduce confusion and medical errors associated with drug names.
In the draft guidance, the FDA notes that end users including the prescribing veterinarians, veterinary technicians, food animal producers, animal owners, pharmacists or pharmacy technicians rely, in part, on the proprietary name to identify which product, among thousands, is intended for a given animal. Clarity is critical for preventing medical errors including incorrect drug selection, incorrect dosage or route of administration, or use in unintended species or classes. These types of errors, the guidance notes, could cause animal injury or lead to a lack of drug effectiveness, resulting in complication of a disease and possible death of an animal.
The draft guidance outlines and provides recommendations for avoiding naming attributes that could lead to errors including:  

Similarities in Spelling and Pronunciation: Names should be as distinctive as possible.
Inert of Inactive Ingredients: Proprietary names should not emphasize these.
Products with Multiple Active Ingredients: Names should not emphasize one or more active ingredients while excluding others.  
Same Proprietary Name for Products Containing Different Active Ingredients: Do not use the same proprietary name or the same root proprietary name for products that do not contain at least one common active ingredient contained in the original marketed product
Reuse of Proprietary Names: Do not reuse the proprietary name of a discontinued product.
Names That Include Reference to Product-Specific Attributes: FDA recommends avoiding incorporating product-specific attributes, such as manufacturing characteristics, dosage form, dosing interval or route of administration in a product name.
Modifiers as Components of a Proprietary Name: Companies sometimes use modifiers to distinguish product characteristics, such as “ER” for extended-release. FDA notes that inconsistent use of modifiers and the absence of a standardized meaning for such terms can be confusing to end users.

Read the full Draft Guidance for Industry #240 from FDA.