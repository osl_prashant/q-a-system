Trust. It’s what a child feels holding his or her parent’s hand crossing a street. It’s what you feel when your veterinarian advises you about your health care program. Trust is what you have in your employees and hopefully what they have in you. 
“Trust is an emotional strength that begins with the feeling of self-worth and purpose that we’re called to extend outward to others, like the radius of a circle, eventually reaching everyone on our team,” Robert K. Cooper and Ayman Sawaf write in “Emotional Intelligence in Leadership and Organizations.” Trust is one of the “great enablers of life,” the authors believe. “With it, we have the inner room to grow, to become emotionally fit, and to exercise and expand our sense of trust in building bridges from one issue to another, one idea to another, one person to another.”
 
Communication is the thread that enables leaders to create a culture of trust within their organization, notes business consultant Kellie Cummings. 
 
“The more I study trust, from an interpersonal and organizational standpoint, the more I see a deep connection between trust and communication. In fact, I often refer to trust, communication and leadership as the three-legged stool of modern business,” she says.
 
Once trust is established, leaders can achieve their goals more effectively and efficiently with the full faith and support of their team.
 
Author Judith Glaser believes “conversational intelligence” is a trait leaders are able to cultivate. In fact, she feels it might be more important than technical expertise, extensive training or even the highest IQ, for creating sustainable success.
 
Conversations take place against the backdrop of our brain chemistry, Glaser explains. “Our state of mind—and our level of trust and distrust—directly impacts what kinds of conversations we have and how we interpret them. Equally so, our conversations impact how much we trust someone, or don’t.
 
Brain chemistry is like a symphony, moving us to higher or lower levels of trust or distrust as we converse with others, Glaser says. The brain is where trust lives or dies, and if people are threatened during conversations, they activate the distrust networks, and if they feel trust, the opposite is true. 
 
According to Angelika Dimoka, Temple University, Fox School of Business, distrust takes place in the lower brain (the amygdala and limbic areas) and trust takes place in the higher brain (the prefrontal cortex).
 
In other words, the distrust, or fear network, can close down your thinking brain, giving power to your emotional and action brain. On the other hand, the trust network opens up access to your executive brain—the neocortex and prefrontal cortex.
 
Glaser is the CEO of Benchmark Communications and author of the book “Conversational Intelligence: How Great Leaders Build Trust and Get Extraordinary Results.” Her book helps make the latest research from neuroscience accessible and practical so it can be applied by leaders anywhere. 
 

Simple conversations aren’t really simple

Conversational intelligence is hardwired into every human being, Glaser explains. It enables people to navigate successfully with others. 
“Through language and conversations, we learn to build trust, to bond, to grow to each other, and to create our societies,” she says. “There is no more powerful skill hardwired into every human being than the wisdom of conversations.”
 
Conversations are not just the words used when engaging with others. Glaser’s 35 years of research show conversations are the “golden thread that keeps human beings connected relationally, neurochemically and energetically. Our brain has the ability to ‘signal’ us when the connection feels like distrust or when we feel trust,” she says. 
 

Three levels of conversation

According to Glaser, all human beings, from the time they were born, can access three levels of conversation. 
 
Level I Informational conversations:  “Informational conversations are transactional,” Glaser explains. “We are most interested in giving or receiving information. These conversations remain at Level I and don’t activate fear networks, stimulate questions about the impact of the transaction or lead to deep exploration of consequences or building strategies and plans.”
 
Level II Positional conversations: These conversations are designed to bring clarity and understanding, and influence how the other person feels and thinks, Glaser says. “We advocate our own opinions and inquire into others’ perspectives,” she says. “If this inquiry is based on shared curiosity and respect, conversations will be healthy, and the networks of trust will be activated.”
 
If one or more participants are more focused on making a point or taking a stand, Glaser says conversations can turn to debate, signaling to the brain that you are dealing not with a friend but a foe. In response, the brain releases cortisol and closes down, and the amygdala essentially becomes hijacked. 
 
Rather than jumping to conclusions, leaders can instead wait and see how the other person reacts. If the other person shows trust, fairness or reciprocity, you can sustain healthy brain chemistry and build trust, creating a culture where people are open to share, discover and co-create.  
 
Level III Relational conversations: These conversations build meaning and create connections, which release oxytocin, the bonding hormone. 
 
“When we care about what others think and feel, our brain senses not only safety, the prefrontal cortex reads oxytocin as a signal to trust and open up,” Glaser says. “As a result, our conversations become innovative, co-creational and energizing.”