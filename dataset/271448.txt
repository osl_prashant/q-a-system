As the last full week of April unfurls, several farmers still haven’t been able to drop a seed in the ground due to inclement weather.

Over the weekend, their patience was rewarded with clear skies and warm temperatures by Mother Nature as some were able to start rolling.

According to the USDA, roughly 20 percent of the corn crop was planted this time last year, and was delayed in May in several areas around the country from wet weather. 

Farmers proved they could handle the weather conditions and planted the rest of their acres in a short window. Some are wondering if it will be a similar story this year.

“There’s no question that the world’s focus is on the planting progress in the U.S., but the market knows two things,” said Brian Basting, commodity research analyst with Advance Trading, Inc. Those factors? Planting the crop in a short amount of time and volatility.

“We’re looking at a lot of volatility in these markets, and I think producers have got to be prepared for volatility—they’ve got to have a plan in place to manage upside and downside,” he said on AgDay.

Bob Utterback, founder of Utterback Marketing, believes feed buyers need to start protecting against a July weather scare, and grain sellers need a plan to get the old crop out of the bin.

“You’ve got to set time deadlines to move yourself in June, July—but if the market gives you an opportunity, how are you going to take advantage of it?” he said. “Don’t wait until the event occurs.” 

Hear Basting and Utterback’s full conversation on AgDay above.