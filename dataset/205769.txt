India, the world's second-biggest consumer of urea, is boosting production of the crop nutrient seeking to end imports in the next five years.
The South Asian nation, where agriculture makes up about 14 percent of the economy, produced 24.5 million tons of urea in the year ended March 2016, compared with consumption of around 32 million tons during the period, according to data from the fertilizer ministry. The country imported more than a quarter of what it consumed from Oman, China and Iran.
"We are in the process of reviving ailing plants, restart closed units, expand existing projects and build new ones," Dharam Pal, joint secretary at India's fertilizer ministry, said in an interview in New Delhi. "The target is to wipe out urea imports completely by 2022."
Increasing local supplies of the nitrogen fertilizer will help shield farmers against global price fluctuations and limit government subsidies, allowing for greater spending to spur the rural economy. The goal also ties in with Prime Minister Narendra Modi's push to boost domestic manufacturing, as he seeks to create more jobs in the world's second-most populated nation.



India imports a quarter of urea consumption.


¬©Bloomberg



Imports surged from near negligible levels in the fiscal year ended March 2001, as consumption outpaced domestic supplies, according to a report by Projects & Development India Ltd., a state-run consultant. Urea imports stood at 68,000 tons in the year ended March 2001, the fertilizer ministry told parliament in 2003.
Revival Blueprint
The ministry is studying proposals to revive loss-making Madras Fertilizers Ltd. and Fertilisers & Chemicals Travancore Ltd., Pal said. The plans, which seek to make both companies profitable by end of March, will need the cabinet's backing, he said.
The government is also planning to restart five idle facilities owned by The Fertilizers Corp. of India and Hindustan Fertiliser Corp. State-run energy firms Indian Oil Corp., Coal India Ltd. and power producer NTPC Ltd. will together execute a 180-billion rupees turnaround plan for three of these factories located in the eastern part of the country.
Madras Fertilizers rose 5.4 percent to 30.25 rupees at the close in Mumbai, gaining for the sixth trading session in a row. National Fertilizers Ltd. also added 5.4 percent to 78.20 rupees.Fertilisers & Chemicals Travancore advanced 6.9 percent to 41.10 rupees, while Rashtriya Chemicals & Fertilizers Ltd. ended 1.1 percent higher at 84.50 rupees.
Another plant in India's north-eastern state of Assam, among the oldest in India, will be shut down and replaced with a modern facility, Pal said. The Brahmaputra Valley Fertilizer Corp. will be able to produce more using the same amount of natural gas. A new urea plant of 864,600 tons per year will be built to replace the two existing units of 220,000 tons and 270,000 tons each.
The global demand for nitrogen fertilizers is expected to grow 5.6 percent to 119.4 million tons in four years through 2018, according to the Food and Agriculture Organization of the United Nations. Asian nations, led by China and India, are expected to account for 58 percent of this increase, the agency said.