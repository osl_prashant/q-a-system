Top EU court: Poland broke law by logging in pristine forest
Top EU court: Poland broke law by logging in pristine forest

The Associated Press

WARSAW, Poland




WARSAW, Poland (AP) — The European Union's top court ruled on Tuesday that Poland violated environmental laws with its massive logging of trees in one of Europe's last pristine forests.
The ruling by the European Court of Justice said that, in increasing logging in the Bialowieza Forest in 2016, Poland failed to fulfil its obligations to protect natural sites of special importance.
Poland's environment minister at the time, Jan Szyszko, argued that felling the trees was necessary to fight the spread of bark beetle infestation. Heavy machines were used in the process, causing additional damage to the forest.
Environmentalists say the large-scale felling of trees in Bialowieza destroyed rare animal habitats and plants in violation of regulations. They held protests and brought the case before the court last year.
The chief executive of the ClientEarth environmental organization, James Thornton, said the ruling was a "huge victory for all defenders of Bialowieza Forest, hundreds of people who were heavily engaged in saving this unique, ancient woodland from unthinkable destruction."
Poland has since replaced its environment minister and stopped the logging. The new minister, Henryk Kowalczyk, has said Warsaw will respect the EU court's ruling.
The European court ordered Poland to pay court costs. It didn't specify the amount.