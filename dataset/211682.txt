Freeways, hotels and restaurants are brimming with traffic, and the city of Houston is making a slow but determined recovery after suffering more than an estimated $20 billion in property damage from the Hurricane Harvey in late August and early September. 
All but a handful of Houston-area Wal-Mart and H-E-B stores were open for business a week after flooding in late August closed more than 100 Wal-Mart locations and 30 H-E-B stores.
 
“People are starting to go back to work, the curfew has been lifted and the freeways are packed,” said Wesley Hudson, national sales manager at Tomato Management Corp. and vice president of the Houston Fresh Fruit & Vegetable Association. “Things are getting back to normal.”
 
Homeowners are busy ripping up carpets and taking out drywall that was flooded, creating huge piles of trash that need to be hauled out of neighborhoods.
 
Hudson said Sept. 6 that nearly every hotel is full as displaced residents and relief workers create big demand for temporary shelter. 
 
Restaurants also are filling up. “People have been locked up in their homes and they are ready to get out,” he said.
 
While some large school districts won’t resume until the week of Sept. 11, foodservice and hotel business was returning quickly, industry leaders said.
 
Houston is a city of 12,000 restaurants and Melissa Stewart, executive director of the Houston Restaurant Association said Sept. 6 that most of them are open and operating. She predicted business for restaurant suppliers and operators would return to normal levels by mid-September.
 
“We are hearing right now that a majority (of restaurants) came through with no damage or minimal damage,” Stewart said. 
 
An undetermined number will be shut down for a period of several weeks to three months to rebuild after flood damage. Because of their greater number of units, quick service restaurants in the suburbs suffered proportionately more losses. “We didn’t have as much damage in the Galleria region or downtown,” she said.
 
With some larger school districts returning Sept. 11 or Sept. 13, some people have been working from home until then, creating shifts in restaurant traffic.  “By Sept. 11, we will be further along on the path of normal,” she said. “There is a great sense of hope we will overcome this.”
 
Storm damage estimates were incomplete as Sept. 7. The city suffered $5 billion in harm to infrastructure alone, including water treatment plants and pumping stations, said Alan Bernstein, communications director at the Mayor’s office. 
 
That doesn’t count losses to the tens of thousands of homes damaged by the flood. Reuters has estimated damage to homes in Texas’ Harris and Galveston counties at more than $20 billion.
 
The Houston Food Bank was distributing three to four times more product than usual Sept. 6 and expects that volume to increase, according to an update from the group. 
 
Using 124 trucks — 25% more than normal — the food bank is expected to deliver 1 million pounds of food a day soon. Continued donations are needed in the weeks and months ahead, the group said.
 
Produce suppliers have been generous in donating produce for relief efforts, said Brent Erenwert, president of Brothers Produce, Houston. The company has distributed some of that product to food banks and to shelters that need food. He said the people of Houston hope that Hurricane Irma will not cause problems for communities in Florida and are ready to give their help if it does.
 
The restaurant trade will be brisk in the weeks ahead, Erenwert predicted, with displaced residents living in hotels and eating out. “The only thing missing in our business now is the schools,” he said.