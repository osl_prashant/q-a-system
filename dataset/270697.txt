Americans told to toss romaine lettuce over E. coli fears
Americans told to toss romaine lettuce over E. coli fears

By TERRY TANGAssociated Press
The Associated Press

PHOENIX




PHOENIX (AP) — U.S. health officials on Friday told consumers to throw away any store-bought romaine lettuce they have in their kitchens and warned restaurants not to serve it amid an E. coli outbreak that has sickened more than 50 people in several states.
The U.S. Centers for Disease Control and Prevention expanded its warning about tainted romaine from Arizona, saying information from new illnesses led it to caution against eating any forms of the lettuce that may have come from the city of Yuma. Officials have not found the origin of the contaminated vegetables.
Previously, CDC officials had only warned against chopped romaine by itself or as part of salads and salad mixes. But they are now extending the risk to heads or hearts of romaine lettuce.
People at an Alaska correctional facility recently reported feeling ill after eating from whole heads of romaine lettuce. They were traced to lettuce harvested in the Yuma region, according to the CDC.
So far, the outbreak has infected 53 people in 16 states. At least 31 have been hospitalized, including five with kidney failure. No deaths have been reported.
Symptoms of E. coli infection include diarrhea, severe stomach cramps and vomiting.
The CDC's updated advisory said consumers nationwide should not buy or eat romaine lettuce from a grocery store or restaurant unless they can get confirmation it did not come from Yuma. People also should toss any romaine they already have at home unless it's known it didn't come from the area, the agency said.
Restaurants and retailers were warned not to serve or sell romaine lettuce from Yuma.
Romaine grown in coastal and central California, Florida and central Mexico is not at risk, according to the Produce Marketing Association.
The Yuma region, which is roughly 185 miles (298 kilometers) southwest of Phoenix and close to the California border, is referred to as the country's "winter vegetable capital." It is known for its agriculture and often revels in it with events like a lettuce festival.
Steve Alameda, president of the Yuma Fresh Vegetable Association, which represents local growers, said the outbreak has weighed heavily on him and other farmers.
"We want to know what happened," Alameda said. "We can't afford to lose consumer confidence. It's heartbreaking to us. We take this very personally."
Growers in Yuma typically plant romaine lettuce between September and January. During the peak of the harvest season, which runs from mid-November until the beginning of April, the Yuma region supplies most of the romaine sold in the U.S., Alameda said. The outbreak came as the harvest of romaine was already near its end.
While Alameda has not met with anyone from the CDC, he is reviewing his own business. He is going over food safety practices and auditing operations in the farming fields.
___
This story has been corrected to restore the full name of the U.S. Centers for Disease Control and Prevention.